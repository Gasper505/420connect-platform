## Requirements:

1. Java 7/8
2. Gradle
3. Redis 3.x
4. MongoDB 3.x (Optional)


## Ideal IDE: 
IntelliJ

## HOW TO RUN ON TERMINAL:
1. cd to java/420connect
2. Run start.sh or type: gradle :connect-services:run


## HOW TO HAVE YOUR OWN SERVER CONFIGS:
1. Copy connect.yml and name it with suffix ending in "-local.yml". EG, "custom-local.yml"
2. Make necessary changes to your new yml file, eg local mongo db
3. Run: gradle :connect-services:run -Dconfig=custom-local.yml




## FREQUENT STARTUP ISSUES:
1. If you have Jedis issues, it probably means your redis server isn't running.


## MAIN FRAMEWORKS
1. DropWizard 1.0.0
2. Jongo