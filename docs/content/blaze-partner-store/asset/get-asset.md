### Get Asset
Get assets by key

```endpoint
GET  /api/v1/partner/asset/{assetKey}
```
#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/asset/{assetKey}
```

```javascript
client.getAsst(function(err, asset) {
  console.log(asset);
});
```

**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`assetKey` | asset key | path | string
`handleError` | handle error | query | boolean

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```