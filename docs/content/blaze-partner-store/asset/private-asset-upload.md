### Asset Upload
Upload assets

```endpoint
POST  /api/v1/partner/asset/upload/private
```
#### Example Request

```curl
$ curl -X POST {path}/api/v1/partner/asset/upload/private
```
**Request Body**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`file` | upload file | formData | file
`name` | file name | formData | string
`assetType` | asset type | formData | string


#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "name": "string",
  "key": "string",
  "type": "Photo",
  "publicURL": "string",
  "active": true,
  "priority": 0,
  "secured": true,
  "thumbURL": "string",
  "mediumURL": "string",
  "largeURL": "string",
  "largeX2URL": "string",
  "origURL": "string",
  "assetType": "Photo"
}
```