### Find User
Find blaze user.

```endpoint
GET /api/v1/partner/user/find
```


#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/user/find
```
```javascript
client.findUser(function(err, user) {
  console.log(user);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`email` | user email | query | string
`phoneNumber` | user contact | query | string
`country` | user country | query | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```

#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "email": "string",
  "password": "string",
  "firstName": "string",
  "lastName": "string",
  "address": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "address": "string",
    "city": "string",
    "state": "string",
    "zipCode": "string",
    "country": "string"
  },
  "dob": 0,
  "primaryPhone": "string",
  "cpn": "string",
  "textOptIn": true,
  "emailOptIn": true,
  "medical": true,
  "searchText": "string",
  "sex": "MALE",
  "marketingSource": "string",
  "sourceCompanyId": "string",
  "verifyMethod": "Website",
  "verificationWebsite": "string",
  "verificationPhone": "string",
  "doctorFirstName": "string",
  "doctorLastName": "string",
  "doctorLicense": "string",
  "dlNo": "string",
  "dlExpiration": 0,
  "dlState": "string",
  "dlPhoto": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "name": "string",
    "key": "string",
    "type": "Photo",
    "active": true,
    "assetType": "Photo"
  },
  "recPhoto": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "name": "string",
    "key": "string",
    "type": "Photo",
    "active": true,
    "assetType": "Photo"
  },
  "recNo": "string",
  "recExpiration": 0,
  "recIssueDate": 0,
  "notificationType": "None",
  "consumerType": "AdultUse",
  "verified": true,
  "active": true,
  "memberIds": [
    "string"
  ],
  "memberStatuses": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "consumerId": "string",
      "memberId": "string",
      "accepted": true,
      "acceptedDate": 0,
      "reason": "string",
      "lastSyncDate": 0
    }
  ],
  "signedContracts": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "contractId": "string",
      "membershipId": "string",
      "signedDate": 0,
      "signaturePhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "assetType": "Photo"
      },
      "signedDigitally": true,
      "consumerId": "string",
      "memberName": "string",
      "employeeName": "string",
      "employeeSignaturePhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "assetType": "Photo"
      },
      "witnessName": "string",
      "witnessSignaturePhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "assetType": "Photo"
      }
    }
  ]
}
```

