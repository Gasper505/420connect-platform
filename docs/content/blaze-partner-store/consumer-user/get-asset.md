### Get Asset
Get Consumer Asset

```endpoint
GET /api/v1/partner/store/user/assets/{assetKey}
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/user/assets/{assetKey}
```
```javascript
client.getAsset(function(err, asset) {
  console.log(asset);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`assetKey` |(required) asset Key| path | string
`assetToken`| asset Token | query | string



#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
