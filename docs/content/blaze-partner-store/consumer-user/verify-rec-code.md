### Verify Rec Code
Verify store patient rec code


```endpoint
GET /api/v1/partner/store/user/verify
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/user/verify
```
```javascript
client.verifyRec(function(err, verificationStatus) {
  console.log(verificationStatus);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`recNo` |Rec number| query | string
`websiteType`| type of website | query | string



#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "patientName": "string",
  "recommendationExpDate": "string",
  "recommendationIssueDate": "string",
  "recommendationStatus": "string"
}
```