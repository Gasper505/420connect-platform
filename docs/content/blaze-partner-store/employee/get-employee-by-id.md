### Get Employee By Id
Get employee by id.

```endpoint
GET /api/v1/partner/employees/{employeeId}
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/employees/{employeeId}
```
```javascript
client.getEmployee(function(err, employee) {
  console.log(employee);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`employeeId` |employee id| path | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "firstName": "string",
  "lastName": "string",
  "pin": "string",
  "roleId": "string",
  "email": "string",
  "password": "string",
  "driversLicense": "string",
  "dlExpirationDate": "string",
  "vehicleMake": "string",
  "notes": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "writerId": "string",
      "writerName": "string",
      "message": "string",
      "enableOnFleet": true
    }
  ],
  "shops": [
    "string"
  ],
  "disabled": true,
  "phoneNumber": "string",
  "assignedInventoryId": "string",
  "assignedTerminalId": "string",
  "address": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "address": "string",
    "city": "string",
    "state": "string",
    "zipCode": "string",
    "country": "string"
  },
  "timecardId": "string",
  "timeCard": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shopId": "string",
    "dirty": true,
    "employeeId": "string",
    "clockInTime": 0,
    "clockOutTime": 0,
    "clockin": true,
    "sessions": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "terminalId": "string",
        "employeeId": "string",
        "timeCardId": "string",
        "startTime": 0,
        "endTime": 0
      }
    ]
  },
  "role": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "permissions": [
      "None"
    ],
    "name": "string"
  },
  "canApplyCustomDiscount": true,
  "insuranceExpireDate": 0,
  "insuranceCompanyName": "string",
  "policyNumber": "string",
  "registrationExpireDate": 0,
  "vehiclePin": "string",
  "vinNo": "string",
  "vehicleModel": "string",
  "vehicleLicensePlate": "string",
  "recentLocation": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shopId": "string",
    "dirty": true,
    "terminalId": "string",
    "employeeId": "string",
    "timeCardId": "string",
    "deviceId": "string",
    "name": "string",
    "loc": [
      0
    ]
  },
  "employeeOnFleetInfoList": [
    {
      "shopId": "string",
      "onFleetWorkerId": "string",
      "onFleetTeamList": [
        "string"
      ]
    }
  ],
  "appAccessList": [
    "AuthenticationApp"
  ],
  "tookanInfoList": [
    {
      "shopId": "string",
      "tookanAgentId": "string",
      "status": "AVAILABLE",
      "teamId": "string"
    }
  ],
  "lastLoggedInShopId": "string"
}
```