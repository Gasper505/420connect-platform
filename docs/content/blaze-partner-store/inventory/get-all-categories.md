### All Categories
Get all Categories

```endpoint
GET /api/v1/partner/store/inventory/categories
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/inventory/categories
```
```javascript
client.getAllCategories(function(err, categories) {
  console.log(categories);
});
```

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "name": "string",
      "cannabis": true,
      "photo": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "assetType": "Photo"
      },
      "unitType": "units",
      "active": true,
      "priority": 0,
      "lowThreshold": 0
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```