### Get Inventory
Get all inventory.

```endpoint
GET /api/v1/partner/store/inventories
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/inventories
```
```javascript
client.getAllInventory(function(err, inventory) {
  console.log(inventory);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`start` |start index of inventory array| query | string
`limit` |number of inventory desired | query | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "type": "Storage",
      "name": "string",
      "active": true
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```