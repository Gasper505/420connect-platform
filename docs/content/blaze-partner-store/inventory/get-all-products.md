### All Products 
Get all products

```endpoint
GET /api/v1/partner/store/inventory/products
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/inventory/products
```
```javascript
client.getAllProducts(function(err, products) {
  console.log(products);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`categoryId` |product category ID| query | string
`strain` |product strain| query | string
`term` |provide term _if any_| query | string
`start` |start index of products array| query | string
`limit` |number or products desired | query | string
`tags` |find products by tag _if any_| query | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "importId": "string",
      "categoryId": "string",
      "sku": "string",
      "vendorId": "string",
      "productSaleType": "Medicinal",
      "name": "string",
      "description": "string",
      "flowerType": "string",
      "unitPrice": 0,
      "weightPerUnit": "EACH",
      "unitValue": 0,
      "thc": 0,
      "cbn": 0,
      "cbd": 0,
      "cbda": 0,
      "thca": 0,
      "active": true,
      "genetics": "string",
      "medicalConditions": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "name": "string"
        }
      ],
      "priceRanges": [
        {
          "id": "string",
          "weightToleranceId": "string",
          "price": 0,
          "priority": 0,
          "weightTolerance": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "startWeight": 0,
            "endWeight": 0,
            "priority": 0,
            "unitValue": 0,
            "weightValue": 0,
            "weightKey": "UNIT",
            "enabled": true
          }
        }
      ],
      "priceBreaks": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "priceBreakType": "None",
          "name": "string",
          "price": 0,
          "quantity": 0,
          "active": true,
          "priority": 0
        }
      ],
      "assets": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "assetType": "Photo"
        }
      ],
      "quantities": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "inventoryId": "string",
          "quantity": 0
        }
      ],
      "category": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "name": "string",
        "cannabis": true,
        "photo": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "assetType": "Photo"
        },
        "unitType": "units",
        "active": true,
        "priority": 0,
        "lowThreshold": 0
      },
      "notes": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "writerId": "string",
          "writerName": "string",
          "message": "string"
        }
      ],
      "enableMixMatch": true,
      "enableWeedmap": true,
      "showInWidget": true,
      "taxType": "Default",
      "taxOrder": "PostTaxed",
      "customTaxInfo": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "cityTax": 0,
        "stateTax": 0,
        "federalTax": 0
      },
      "discountable": true,
      "vendor": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "importId": "string",
        "active": true,
        "name": "string",
        "phone": "string",
        "email": "string",
        "fax": "string",
        "address": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "address": "string",
          "city": "string",
          "state": "string",
          "zipCode": "string",
          "country": "string"
        },
        "description": "string",
        "website": "string",
        "firstName": "string",
        "lastName": "string",
        "notes": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "writerId": "string",
            "writerName": "string",
            "message": "string"
          }
        ],
        "licenseNumber": "string",
        "assets": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "assetType": "Photo"
          }
        ],
        "backOrderEnabled": true,
        "vendorKey": "string"
      },
      "lowThreshold": 0,
      "lowInventoryNotification": true,
      "medicinal": true,
      "byGram": true,
      "byPrepackage": true,
      "taxTables": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "name": "string",
          "active": true,
          "taxType": "Default",
          "taxOrder": "PostTaxed",
          "consumerType": "AdultUse",
          "cityTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          },
          "countyTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          },
          "stateTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          },
          "federalTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          }
        }
      ],
      "tags": [
        "string"
      ],
      "companyLinkId": "string"
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```