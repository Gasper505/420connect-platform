### Get All Brands
Get all brands                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      

```endpoint
GET /api/v1/partner/store/inventory/brands
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/inventory/brands
```
```javascript
client.getAllBrands(function(err, brands) {
  console.log(brands);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`term` | search term| query | string
`start` |start index of brands array| query | integer
`limit` |number or brands desired | query | integer

#### Response Status

```json
{
"code" : 200,
"message" : "successful operation"
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string",
      "active": true,
      "website": "string",
      "phoneNo": "string",
      "brandLogo": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      },
      "vendorList": [
        "string"
      ],
      "vendors": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "importId": "string",
          "active": true,
          "name": "string",
          "phone": "string",
          "email": "string",
          "fax": "string",
          "address": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "address": "string",
            "city": "string",
            "state": "string",
            "zipCode": "string",
            "country": "string"
          },
          "description": "string",
          "website": "string",
          "firstName": "string",
          "lastName": "string",
          "notes": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "writerId": "string",
              "writerName": "string",
              "message": "string",
              "enableOnFleet": true
            }
          ],
          "licenseNumber": "string",
          "assets": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "name": "string",
              "key": "string",
              "type": "Photo",
              "publicURL": "string",
              "active": true,
              "priority": 0,
              "secured": true,
              "thumbURL": "string",
              "mediumURL": "string",
              "largeURL": "string",
              "largeX2URL": "string",
              "origURL": "string",
              "assetType": "Photo"
            }
          ],
          "backOrderEnabled": true,
          "licenseExpirationDate": 0,
          "armsLengthType": "ARMS_LENGTH",
          "brands": [
            "string"
          ],
          "qbVendorRef": [
            {}
          ],
          "qbDesktopRef": "string",
          "companyType": "DISTRIBUTOR",
          "additionalAddressList": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "address": "string",
              "city": "string",
              "state": "string",
              "zipCode": "string",
              "country": "string"
            }
          ],
          "credits": 0,
          "mobileNumber": "string",
          "licenceType": "RECREATIONAL",
          "relatedEntity": true,
          "vendorType": "CUSTOMER",
          "dbaName": "string",
          "vendorKey": "string"
        }
      ]
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```