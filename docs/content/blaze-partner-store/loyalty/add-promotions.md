### Add Promotions
Add promotions

```endpoint
POST  /api/v1/partner/loyalty/promotions
```
#### Example Request

```curl
$ curl -X POST {path}/api/v1/partner/loyalty/promotions
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`request` | request | body | Object

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Request
```json
{
  "name": "string",
  "promotionType": "Cart",
  "promoSource": "Manual",
  "discountAmt": 0,
  "discountType": "Cash",
  "active": true,
  "stackable": true
}
```

#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "name": "string",
  "promoDesc": "string",
  "discountType": "Cash",
  "discountAmt": 0,
  "discountUnitType": "UNIT",
  "promocode": "string",
  "promoCodeRequired": true,
  "consumerFacing": true,
  "startDate": 0,
  "endDate": 0,
  "active": true,
  "maxAvailable": 0,
  "usageCount": 0,
  "enableMaxAvailable": true,
  "limitPerCustomer": 0,
  "enableLimitPerCustomer": true,
  "restrictMemberGroups": true,
  "memberGroupIds": [
    "string"
  ],
  "rules": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "ruleType": "None",
      "categoryIds": [
        "string"
      ],
      "productIds": [
        "string"
      ],
      "vendorIds": [
        "string"
      ],
      "productTags": [
        "string"
      ],
      "categoryId": "string",
      "productId": "string",
      "vendorId": "string",
      "unitWeight": "UNIT",
      "minAmt": 0,
      "maxAmt": 0
    }
  ],
  "promotionType": "Cart",
  "maxCashValue": 0,
  "enableBOGO": true,
  "enableDayDuration": true,
  "dayStartTime": 0,
  "dayEndTime": 0,
  "scalable": true,
  "targetPriceRange": true,
  "lowerPriceBound": 0,
  "upperPriceBound": 0,
  "mon": true,
  "tues": true,
  "wed": true,
  "thur": true,
  "fri": true,
  "sat": true,
  "sun": true,
  "promoCodes": [
    "string"
  ],
  "promoSource": "Manual",
  "stackable": true,
  "importId": "string",
  "priority": 0
}
```