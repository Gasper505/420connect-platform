### Get Rewards List
Get Promotion List

```endpoint
GET /api/v1/partner/loyalty/rewards
```

#### Example Request

```curl {path}/api/v1/partner/loyalty/rewards
```
```javascript
client.getRewards(function(err, rewards) {
  console.log(rewards);
});
```

**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`start` | start | query | integer
`limit` | limit | query | integer

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "rewardNo": "string",
      "slug": "string",
      "points": 0,
      "name": "string",
      "detail": "string",
      "active": true,
      "loyaltyType": "Regular",
      "published": true,
      "publishedDate": 0,
      "unpublishedDate": 0,
      "promotionId": "string",
      "productIds": [
        "string"
      ],
      "categoryIds": [
        "string"
      ],
      "vendorIds": [
        "string"
      ],
      "productTags": [
        "string"
      ],
      "discountType": "Cash",
      "discountAmt": 0,
      "discountUnitType": "UNIT",
      "startDate": 0,
      "endDate": 0,
      "rewardTarget": "WholeCart",
      "priority": 0
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```