### Get Member by Id
Get Member By Id

```endpoint
GET /api/v1/partner/members/{memberId}
```

#### Example Request

**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`memberId` | member id | path | String


#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "firstName": "string",
  "lastName": "string",
  "middleName": "string",
  "address": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "address": "string",
    "city": "string",
    "state": "string",
    "zipCode": "string",
    "country": "string"
  },
  "email": "string",
  "dob": 0,
  "primaryPhone": "string",
  "textOptIn": true,
  "emailOptIn": true,
  "medical": true,
  "searchText": "string",
  "sex": "MALE",
  "status": "Active",
  "notes": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "writerId": "string",
      "writerName": "string",
      "message": "string",
      "enableOnFleet": true
    }
  ],
  "contracts": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "contractId": "string",
      "membershipId": "string",
      "signedDate": 0,
      "signaturePhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      },
      "signedDigitally": true,
      "consumerId": "string",
      "memberName": "string",
      "employeeName": "string",
      "employeeSignaturePhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      },
      "witnessName": "string",
      "witnessSignaturePhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      }
    }
  ],
  "preferences": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string"
    }
  ],
  "identifications": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "licenseNumber": "string",
      "expirationDate": 0,
      "frontPhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      },
      "assets": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        }
      ],
      "state": "string",
      "type": "Drivers"
    }
  ],
  "recommendations": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "recommendationNumber": "string",
      "verifyWebsite": "string",
      "verifyPhoneNumber": "string",
      "state": "string",
      "issueDate": 0,
      "expirationDate": 0,
      "frontPhoto": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      },
      "assets": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        }
      ],
      "doctorId": "string",
      "verified": true,
      "verifyMethod": "DEFAULT",
      "doctor": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "importId": "string",
        "firstName": "string",
        "lastName": "string",
        "license": "string",
        "website": "string",
        "phoneNumber": "string",
        "email": "string",
        "fax": "string",
        "degree": "string",
        "state": "string",
        "searchText": "string",
        "address": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "address": "string",
          "city": "string",
          "state": "string",
          "zipCode": "string",
          "country": "string"
        },
        "active": true,
        "notes": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "writerId": "string",
            "writerName": "string",
            "message": "string",
            "enableOnFleet": true
          }
        ],
        "assets": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          }
        ]
      }
    }
  ],
  "lastVisitDate": 0,
  "startDate": 0,
  "importId": "string",
  "memberGroupId": "string",
  "memberGroup": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shopId": "string",
    "dirty": true,
    "name": "string",
    "discount": 0,
    "defaultGroup": true,
    "active": true,
    "memberCount": 0,
    "memberCountTextOptIn": 0,
    "memberCountEmailOptIn": 0,
    "discountType": "Cash",
    "enablePromotion": true,
    "promotionId": "string"
  },
  "recentProducts": [
    "string"
  ],
  "dlExpired": true,
  "recommendationExpired": true,
  "agreementExpired": true,
  "emailVerified": true,
  "consumerUserId": "string",
  "marketingSource": "string",
  "consumerType": "Other",
  "enableLoyalty": true,
  "loyaltyPoints": 0,
  "lifetimePoints": 0,
  "enabledCareGiver": true,
  "careGivers": [
    "string"
  ],
  "addresses": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "zipCode": "string",
      "country": "string"
    }
  ],
  "qbCustomerRef": [
    {}
  ],
  "banPatient": true,
  "qbDesktopCustomerRef": "string",
  "tags": [
    "string"
  ],
  "expStatuses": [
    "string"
  ]
}
```