### Get member limit
Get member limit

```endpoint
GET /api/v1/partner/members/{memberId}/limits
```

#### Example Request

**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`memberId` | memberId | path | String

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{}
```