### Get member remaining limit
Get member remaining limit

```endpoint
GET /api/v1/partner/members/{memberId}/limits/remaining
```

#### Example Request

**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`memberId` | memberId | path | String

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "consumerType": "Other",
  "state": "AL",
  "concentrates": 0,
  "nonConcentrates": 0,
  "plants": 0,
  "seeds": 0,
  "max": 0,
  "thc": 0,
  "duration": 0
}
```