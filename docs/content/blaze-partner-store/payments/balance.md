### Check balance of a Payment Card
Check balance of a Payment Card

```endpoint
POST /api/v1/partner/payments/balance
```

#### Example Request
```json
{
  "type": "Linx",
  "paymentCardNumber": "string",
  "linxCardNumber": "string"
}
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`request` | balance request body | body | Object


#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "status": "string",
  "balance": 0,
  "cardStatus": "string"
}
```