### Load a payment card
Load a payment card

```endpoint
POST /api/v1/partner/payments/load
```

#### Example Request
```json
{
  "type": "Linx",
  "paymentCardNumber": "string",
  "amount": 0,
  "creditCard": {
    "number": "string",
    "name": "string",
    "cvv": "string",
    "expirationMonth": "string",
    "expirationYear": "string",
    "zipCode": "string",
    "expiration_month": "string",
    "expiration_year": "string",
    "zip_code": "string"
  },
  "linxCardNumber": "string",
  "transactionId": "string"
}
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`request` | load card request body | body | Object


#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "loadedTotal": 0,
  "feeTotal": 0,
  "chargedTotal": 0
}
```