### Search Product
Search product by dates

```endpoint
GET  /api/v1/partner/products
```
#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/products
```
```javascript
client.getProducts(function(err, products) {
  console.log(products);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`startDate` | start date | query | long
`endDate` | end date | query | long
`start` | start | query | integer
`limit` | limit | query | integer


#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "importId": "string",
      "importSrc": "string",
      "categoryId": "string",
      "sku": "string",
      "vendorId": "string",
      "productSaleType": "Medicinal",
      "name": "string",
      "description": "string",
      "flowerType": "string",
      "unitPrice": 0,
      "weightPerUnit": "EACH",
      "unitValue": 0,
      "thc": 0,
      "cbn": 0,
      "cbd": 0,
      "cbda": 0,
      "thca": 0,
      "active": true,
      "genetics": "string",
      "medicalConditions": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "name": "string"
        }
      ],
      "priceRanges": [
        {
          "id": "string",
          "weightToleranceId": "string",
          "price": 0,
          "priority": 0,
          "weightTolerance": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "startWeight": 0,
            "endWeight": 0,
            "priority": 0,
            "unitValue": 0,
            "weightValue": 0,
            "weightKey": "UNIT",
            "enabled": true
          }
        }
      ],
      "priceBreaks": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "priceBreakType": "None",
          "name": "string",
          "displayName": "string",
          "price": 0,
          "quantity": 0,
          "active": true,
          "priority": 0
        }
      ],
      "assets": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        }
      ],
      "quantities": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "inventoryId": "string",
          "quantity": 0
        }
      ],
      "brandId": "string",
      "category": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "name": "string",
        "cannabis": true,
        "photo": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        },
        "unitType": "units",
        "active": true,
        "priority": 0,
        "lowThreshold": 0,
        "cannabisType": "NONE"
      },
      "notes": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "writerId": "string",
          "writerName": "string",
          "message": "string",
          "enableOnFleet": true
        }
      ],
      "enableMixMatch": true,
      "enableWeedmap": true,
      "showInWidget": true,
      "taxType": "Default",
      "taxOrder": "PostTaxed",
      "customTaxInfo": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "cityTax": 0,
        "stateTax": 0,
        "federalTax": 0
      },
      "discountable": true,
      "vendor": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "importId": "string",
        "active": true,
        "name": "string",
        "phone": "string",
        "email": "string",
        "fax": "string",
        "address": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "address": "string",
          "city": "string",
          "state": "string",
          "zipCode": "string",
          "country": "string"
        },
        "description": "string",
        "website": "string",
        "firstName": "string",
        "lastName": "string",
        "notes": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "writerId": "string",
            "writerName": "string",
            "message": "string",
            "enableOnFleet": true
          }
        ],
        "licenseNumber": "string",
        "assets": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          }
        ],
        "backOrderEnabled": true,
        "licenseExpirationDate": 0,
        "armsLengthType": "ARMS_LENGTH",
        "brands": [
          "string"
        ],
        "qbVendorRef": [
          {}
        ],
        "qbDesktopRef": "string",
        "companyType": "DISTRIBUTOR",
        "additionalAddressList": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "address": "string",
            "city": "string",
            "state": "string",
            "zipCode": "string",
            "country": "string"
          }
        ],
        "credits": 0,
        "mobileNumber": "string",
        "licenceType": "RECREATIONAL",
        "relatedEntity": true,
        "vendorType": "CUSTOMER",
        "dbaName": "string",
        "vendorKey": "string"
      },
      "brand": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "active": true,
        "website": "string",
        "phoneNo": "string",
        "brandLogo": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        },
        "vendorList": [
          "string"
        ]
      },
      "lowThreshold": 0,
      "lowInventoryNotification": true,
      "medicinal": true,
      "byGram": true,
      "byPrepackage": true,
      "enableExciseTax": true,
      "priceIncludesExcise": true,
      "priceIncludesALExcise": true,
      "cannabisType": "NONE",
      "potencyAmount": {
        "thc": 0,
        "cbd": 0,
        "cbn": 0,
        "thca": 0,
        "cbda": 0
      },
      "taxTables": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "name": "string",
          "active": true,
          "taxType": "Default",
          "taxOrder": "PostTaxed",
          "consumerType": "Other",
          "cityTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default",
            "activeExciseTax": true,
            "taxOrder": "PostTaxed"
          },
          "countyTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default",
            "activeExciseTax": true,
            "taxOrder": "PostTaxed"
          },
          "stateTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default",
            "activeExciseTax": true,
            "taxOrder": "PostTaxed"
          },
          "federalTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default",
            "activeExciseTax": true,
            "taxOrder": "PostTaxed"
          }
        }
      ],
      "tags": [
        "string"
      ],
      "qbItemRef": "string",
      "qbDesktopItemRef": "string",
      "customWeight": 0,
      "automaticReOrder": true,
      "reOrderLevel": 0,
      "customGramType": "GRAM",
      "pricingTemplateId": "string",
      "productType": "REGULAR",
      "potency": true,
      "companyLinkId": "string"
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```