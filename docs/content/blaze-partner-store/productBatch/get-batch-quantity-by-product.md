### Get BatchQuantity By Product
Get Batch Quantity by product

```endpoint
GET  /api/v1/partner/store/batches/{productId}/batchQuantityInfo
```
#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/batches/{productId}/batchQuantityInfo
```
```javascript
client.getBatchQuantity(function(err, batchQuantity) {
  console.log(batchQuantity);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`productId` | product id | path | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
[
  {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shopId": "string",
    "dirty": true,
    "productId": "string",
    "inventoryId": "string",
    "batchId": "string",
    "quantity": 0,
    "unitType": "units",
    "batchPurchaseDate": 0
  }
]
```