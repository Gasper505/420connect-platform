### Get Purchase Order by id
Get purchase order by id

```endpoint
GET  /api/v1/partner/purchaseorders/{purchaseOrderId}
```
#### Example Request

```curl
$ curl -X GET {path}/api/v1/purchaseorders/{purchaseOrderId}
```
```javascript
client.getPurchaseOrderById(function(err, purchaseOrder) {
  console.log(purchaseOrder);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`purchaseOrderId` | purchase order id | path | string

#### Response Status

```json
{
"code" : 200,
"message" : "successful operation"
}
```
#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "poNumber": "string",
  "parentPOId": "string",
  "parentPONumber": "string",
  "approvedDate": 0,
  "approvedBy": "string",
  "approvedSignature": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "largeX2URL": "string",
    "origURL": "string",
    "assetType": "Photo"
  },
  "poPaymentTerms": "NET_30",
  "purchaseOrderStatus": "InProgress",
  "poPaymentOptions": "CASH",
  "companyAssetList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string",
      "key": "string",
      "type": "Photo",
      "publicURL": "string",
      "active": true,
      "priority": 0,
      "secured": true,
      "thumbURL": "string",
      "mediumURL": "string",
      "largeURL": "string",
      "largeX2URL": "string",
      "origURL": "string",
      "assetType": "Photo"
    }
  ],
  "poProductRequestList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "productId": "string",
      "productName": "string",
      "requestStatus": "PENDING",
      "requestQuantity": 0,
      "receivedQuantity": 0,
      "trackTraceSystem": "MANUAL",
      "trackingPackagesList": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "quantity": 0,
          "packageLabel": "string",
          "batchSku": "string",
          "referenceNo": "string",
          "dateSent": 0,
          "testingCompanyId": "string",
          "trackHarvestBatchId": "string",
          "trackHarvestBatchDate": "string"
        }
      ],
      "notes": "string",
      "unitPrice": 0,
      "totalCost": 0,
      "exciseTax": 0,
      "totalExciseTax": 0,
      "totalCultivationTax": 0,
      "declineReason": "string",
      "batchQuantityMap": {},
      "receiveBatchStatus": "RECEIVED",
      "batchAddDetails": [
        {
          "batchSku": "string",
          "quantity": 0,
          "testId": "string",
          "testingCompanyId": "string",
          "dateSent": 0,
          "trackHarvestBatchId": "string",
          "trackHarvestBatchDate": "string"
        }
      ]
    }
  ],
  "vendorId": "string",
  "receivedDate": 0,
  "deliveredBy": "string",
  "receivedByEmployeeId": "string",
  "createdByEmployeeId": "string",
  "completedByEmployeeId": "string",
  "poType": "Normal",
  "completedDate": 0,
  "notes": "string",
  "declineReason": "string",
  "declineDate": 0,
  "totalCost": 0,
  "amountPaid": 0,
  "submitForApprovalDate": 0,
  "archive": true,
  "archiveDate": 0,
  "customTermDate": 0,
  "managerReceiveSignature": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "largeX2URL": "string",
    "origURL": "string",
    "assetType": "Photo"
  },
  "reference": "string",
  "qbPurchaseOrderRef": "string",
  "grandTotal": 0,
  "totalTax": 0,
  "totalPreCalcTax": 0,
  "totalCalcTax": 0,
  "taxResult": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "totalPreCalcTax": 0,
    "totalPostCalcTax": 0,
    "totalCityTax": 0,
    "totalCountyTax": 0,
    "totalStateTax": 0,
    "totalFedTax": 0,
    "totalCityPreTax": 0,
    "totalCountyPreTax": 0,
    "totalStatePreTax": 0,
    "totalFedPreTax": 0,
    "totalExciseTax": 0,
    "totalNALPreExciseTax": 0,
    "totalALExciseTax": 0,
    "totalALPostExciseTax": 0,
    "cultivationTaxResult": {
      "totalFlowerTax": 0,
      "totalLeafTax": 0,
      "totalFlowerOz": 0,
      "totalLeafOz": 0,
      "totalCultivationTax": 0,
      "leafTaxOz": 0,
      "flowerTaxOz": 0
    }
  },
  "transactionType": "ARMS_LENGTH",
  "customerType": "VENDOR",
  "deliveryAddress": "string",
  "deliveryTime": 0,
  "deliveryDate": 0,
  "termsAndCondition": "string",
  "deliveryCharge": 0,
  "enableDeliveryCharge": true,
  "poQrCodeAsset": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "largeX2URL": "string",
    "origURL": "string",
    "assetType": "Photo"
  },
  "poQrCodeUrl": "string",
  "flowerSourceType": "CULTIVATOR_DIRECT",
  "shipmentBillId": "string",
  "discount": 0,
  "fees": 0,
  "purchaseOrderDate": 0,
  "dueDate": 0,
  "vendor": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "importId": "string",
    "active": true,
    "name": "string",
    "phone": "string",
    "email": "string",
    "fax": "string",
    "address": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "zipCode": "string",
      "country": "string"
    },
    "description": "string",
    "website": "string",
    "firstName": "string",
    "lastName": "string",
    "notes": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "writerId": "string",
        "writerName": "string",
        "message": "string",
        "enableOnFleet": true
      }
    ],
    "licenseNumber": "string",
    "assets": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "largeX2URL": "string",
        "origURL": "string",
        "assetType": "Photo"
      }
    ],
    "backOrderEnabled": true,
    "licenseExpirationDate": 0,
    "armsLengthType": "ARMS_LENGTH",
    "brands": [
      "string"
    ],
    "qbVendorRef": [
      {}
    ],
    "qbDesktopRef": "string",
    "companyType": "DISTRIBUTOR",
    "additionalAddressList": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "address": "string",
        "city": "string",
        "state": "string",
        "zipCode": "string",
        "country": "string"
      }
    ],
    "credits": 0,
    "mobileNumber": "string",
    "licenceType": "RECREATIONAL",
    "relatedEntity": true,
    "vendorType": "CUSTOMER",
    "dbaName": "string",
    "vendorKey": "string"
  },
  "approvedByMember": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "firstName": "string",
    "lastName": "string",
    "pin": "string",
    "roleId": "string",
    "email": "string",
    "password": "string",
    "driversLicense": "string",
    "dlExpirationDate": "string",
    "vehicleMake": "string",
    "notes": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "writerId": "string",
        "writerName": "string",
        "message": "string",
        "enableOnFleet": true
      }
    ],
    "shops": [
      "string"
    ],
    "disabled": true,
    "phoneNumber": "string",
    "assignedInventoryId": "string",
    "assignedTerminalId": "string",
    "address": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "zipCode": "string",
      "country": "string"
    },
    "timecardId": "string",
    "timeCard": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "employeeId": "string",
      "clockInTime": 0,
      "clockOutTime": 0,
      "clockin": true,
      "sessions": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "terminalId": "string",
          "employeeId": "string",
          "timeCardId": "string",
          "startTime": 0,
          "endTime": 0
        }
      ]
    },
    "role": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "permissions": [
        "None"
      ],
      "name": "string"
    },
    "canApplyCustomDiscount": true,
    "insuranceExpireDate": 0,
    "insuranceCompanyName": "string",
    "policyNumber": "string",
    "registrationExpireDate": 0,
    "vehiclePin": "string",
    "vinNo": "string",
    "vehicleModel": "string",
    "vehicleLicensePlate": "string",
    "recentLocation": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "terminalId": "string",
      "employeeId": "string",
      "timeCardId": "string",
      "deviceId": "string",
      "name": "string",
      "loc": [
        0
      ]
    },
    "employeeOnFleetInfoList": [
      {
        "shopId": "string",
        "onFleetWorkerId": "string",
        "onFleetTeamList": [
          "string"
        ]
      }
    ],
    "appAccessList": [
      "AuthenticationApp"
    ],
    "tookanInfoList": [
      {
        "shopId": "string",
        "tookanAgentId": "string",
        "status": "AVAILABLE",
        "teamId": "string"
      }
    ],
    "lastLoggedInShopId": "string"
  },
  "poProductRequestResultList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "productId": "string",
      "productName": "string",
      "requestStatus": "PENDING",
      "requestQuantity": 0,
      "receivedQuantity": 0,
      "trackTraceSystem": "MANUAL",
      "trackingPackagesList": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "quantity": 0,
          "packageLabel": "string",
          "batchSku": "string",
          "referenceNo": "string",
          "dateSent": 0,
          "testingCompanyId": "string",
          "trackHarvestBatchId": "string",
          "trackHarvestBatchDate": "string"
        }
      ],
      "notes": "string",
      "unitPrice": 0,
      "totalCost": 0,
      "exciseTax": 0,
      "totalExciseTax": 0,
      "totalCultivationTax": 0,
      "declineReason": "string",
      "batchQuantityMap": {},
      "receiveBatchStatus": "RECEIVED",
      "batchAddDetails": [
        {
          "batchSku": "string",
          "quantity": 0,
          "testId": "string",
          "testingCompanyId": "string",
          "dateSent": 0,
          "trackHarvestBatchId": "string",
          "trackHarvestBatchDate": "string"
        }
      ],
      "product": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "importId": "string",
        "importSrc": "string",
        "categoryId": "string",
        "sku": "string",
        "vendorId": "string",
        "productSaleType": "Medicinal",
        "name": "string",
        "description": "string",
        "flowerType": "string",
        "unitPrice": 0,
        "weightPerUnit": "EACH",
        "unitValue": 0,
        "thc": 0,
        "cbn": 0,
        "cbd": 0,
        "cbda": 0,
        "thca": 0,
        "active": true,
        "genetics": "string",
        "medicalConditions": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "name": "string"
          }
        ],
        "priceRanges": [
          {
            "id": "string",
            "weightToleranceId": "string",
            "price": 0,
            "priority": 0,
            "weightTolerance": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "name": "string",
              "startWeight": 0,
              "endWeight": 0,
              "priority": 0,
              "unitValue": 0,
              "weightValue": 0,
              "weightKey": "UNIT",
              "enabled": true
            }
          }
        ],
        "priceBreaks": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "priceBreakType": "None",
            "name": "string",
            "displayName": "string",
            "price": 0,
            "quantity": 0,
            "active": true,
            "priority": 0
          }
        ],
        "assets": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          }
        ],
        "quantities": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "inventoryId": "string",
            "quantity": 0
          }
        ],
        "brandId": "string",
        "category": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "name": "string",
          "cannabis": true,
          "photo": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          },
          "unitType": "units",
          "active": true,
          "priority": 0,
          "lowThreshold": 0,
          "cannabisType": "NONE"
        },
        "notes": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "writerId": "string",
            "writerName": "string",
            "message": "string",
            "enableOnFleet": true
          }
        ],
        "enableMixMatch": true,
        "enableWeedmap": true,
        "showInWidget": true,
        "taxType": "Default",
        "taxOrder": "PostTaxed",
        "customTaxInfo": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "cityTax": 0,
          "stateTax": 0,
          "federalTax": 0
        },
        "discountable": true,
        "vendor": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "importId": "string",
          "active": true,
          "name": "string",
          "phone": "string",
          "email": "string",
          "fax": "string",
          "address": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "address": "string",
            "city": "string",
            "state": "string",
            "zipCode": "string",
            "country": "string"
          },
          "description": "string",
          "website": "string",
          "firstName": "string",
          "lastName": "string",
          "notes": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "writerId": "string",
              "writerName": "string",
              "message": "string",
              "enableOnFleet": true
            }
          ],
          "licenseNumber": "string",
          "assets": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "name": "string",
              "key": "string",
              "type": "Photo",
              "publicURL": "string",
              "active": true,
              "priority": 0,
              "secured": true,
              "thumbURL": "string",
              "mediumURL": "string",
              "largeURL": "string",
              "largeX2URL": "string",
              "origURL": "string",
              "assetType": "Photo"
            }
          ],
          "backOrderEnabled": true,
          "licenseExpirationDate": 0,
          "armsLengthType": "ARMS_LENGTH",
          "brands": [
            "string"
          ],
          "qbVendorRef": [
            {}
          ],
          "qbDesktopRef": "string",
          "companyType": "DISTRIBUTOR",
          "additionalAddressList": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "address": "string",
              "city": "string",
              "state": "string",
              "zipCode": "string",
              "country": "string"
            }
          ],
          "credits": 0,
          "mobileNumber": "string",
          "licenceType": "RECREATIONAL",
          "relatedEntity": true,
          "vendorType": "CUSTOMER",
          "dbaName": "string",
          "vendorKey": "string"
        },
        "brand": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "active": true,
          "website": "string",
          "phoneNo": "string",
          "brandLogo": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          },
          "vendorList": [
            "string"
          ]
        },
        "lowThreshold": 0,
        "lowInventoryNotification": true,
        "medicinal": true,
        "byGram": true,
        "byPrepackage": true,
        "enableExciseTax": true,
        "priceIncludesExcise": true,
        "priceIncludesALExcise": true,
        "cannabisType": "NONE",
        "potencyAmount": {
          "thc": 0,
          "cbd": 0,
          "cbn": 0,
          "thca": 0,
          "cbda": 0
        },
        "taxTables": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "name": "string",
            "active": true,
            "taxType": "Default",
            "taxOrder": "PostTaxed",
            "consumerType": "Other",
            "cityTax": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "shopId": "string",
              "dirty": true,
              "taxRate": 0,
              "compound": true,
              "active": true,
              "territory": "Default",
              "activeExciseTax": true,
              "taxOrder": "PostTaxed"
            },
            "countyTax": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "shopId": "string",
              "dirty": true,
              "taxRate": 0,
              "compound": true,
              "active": true,
              "territory": "Default",
              "activeExciseTax": true,
              "taxOrder": "PostTaxed"
            },
            "stateTax": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "shopId": "string",
              "dirty": true,
              "taxRate": 0,
              "compound": true,
              "active": true,
              "territory": "Default",
              "activeExciseTax": true,
              "taxOrder": "PostTaxed"
            },
            "federalTax": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "shopId": "string",
              "dirty": true,
              "taxRate": 0,
              "compound": true,
              "active": true,
              "territory": "Default",
              "activeExciseTax": true,
              "taxOrder": "PostTaxed"
            }
          }
        ],
        "tags": [
          "string"
        ],
        "qbItemRef": "string",
        "qbDesktopItemRef": "string",
        "customWeight": 0,
        "automaticReOrder": true,
        "reOrderLevel": 0,
        "customGramType": "GRAM",
        "pricingTemplateId": "string",
        "productType": "REGULAR",
        "potency": true,
        "companyLinkId": "string"
      },
      "cannabis": true
    }
  ],
  "poActivityLog": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "purchaseOrderId": "string",
      "employeeId": "string",
      "log": "string",
      "activityType": "NORMAL_LOG"
    }
  ],
  "companyLogo": "string",
  "podueDate": 0
}
```