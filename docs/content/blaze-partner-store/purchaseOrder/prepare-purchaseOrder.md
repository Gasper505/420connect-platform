### Prepare Purchase Order
Prepare Purchase Order

```endpoint
POST  /api/v1/partner/purchaseorders/prepare
```
#### Example Request

```curl
$ curl -X POST {path}/api/v1/partner/purchaseorders/prepare
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`request` | request | body | Object

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Request
```json
{
  "vendorId": "string",
  "notes": "string",
  "poPaymentTerms": "string",
  "paymentType": "string",
  "poProductAddRequestList": [
    {
      "productId": "string",
      "requestQuantity": 0,
      "notes": "string",
      "totalCost": 0
    }
  ],
  "customTermDate": 0,
  "reference": "string",
  "transactionType": "ARMS_LENGTH",
  "discount": 0,
  "fees": 0,
  "customerType": "VENDOR",
  "deliveryAddress": "string",
  "deliveryTime": 0,
  "deliveryDate": 0,
  "termsAndCondition": "string",
  "flowerSourceType": "CULTIVATOR_DIRECT",
  "purchaseOrderDate": 0,
  "currentEmployeeId": "string"
}
```

#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "poNumber": "string",
  "parentPOId": "string",
  "parentPONumber": "string",
  "approvedDate": 0,
  "approvedBy": "string",
  "approvedSignature": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "largeX2URL": "string",
    "origURL": "string",
    "assetType": "Photo"
  },
  "poPaymentTerms": "NET_30",
  "purchaseOrderStatus": "InProgress",
  "poPaymentOptions": "CASH",
  "companyAssetList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string",
      "key": "string",
      "type": "Photo",
      "publicURL": "string",
      "active": true,
      "priority": 0,
      "secured": true,
      "thumbURL": "string",
      "mediumURL": "string",
      "largeURL": "string",
      "largeX2URL": "string",
      "origURL": "string",
      "assetType": "Photo"
    }
  ],
  "poProductRequestList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "productId": "string",
      "productName": "string",
      "requestStatus": "PENDING",
      "requestQuantity": 0,
      "receivedQuantity": 0,
      "trackTraceSystem": "MANUAL",
      "trackingPackagesList": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "quantity": 0,
          "packageLabel": "string",
          "batchSku": "string",
          "referenceNo": "string",
          "dateSent": 0,
          "testingCompanyId": "string",
          "trackHarvestBatchId": "string",
          "trackHarvestBatchDate": "string"
        }
      ],
      "notes": "string",
      "unitPrice": 0,
      "totalCost": 0,
      "exciseTax": 0,
      "totalExciseTax": 0,
      "totalCultivationTax": 0,
      "declineReason": "string",
      "batchQuantityMap": {},
      "receiveBatchStatus": "RECEIVED",
      "batchAddDetails": [
        {
          "batchSku": "string",
          "quantity": 0,
          "testId": "string",
          "testingCompanyId": "string",
          "dateSent": 0,
          "trackHarvestBatchId": "string",
          "trackHarvestBatchDate": "string"
        }
      ]
    }
  ],
  "vendorId": "string",
  "receivedDate": 0,
  "deliveredBy": "string",
  "receivedByEmployeeId": "string",
  "createdByEmployeeId": "string",
  "completedByEmployeeId": "string",
  "poType": "Normal",
  "completedDate": 0,
  "notes": "string",
  "declineReason": "string",
  "declineDate": 0,
  "totalCost": 0,
  "amountPaid": 0,
  "submitForApprovalDate": 0,
  "archive": true,
  "archiveDate": 0,
  "customTermDate": 0,
  "managerReceiveSignature": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "largeX2URL": "string",
    "origURL": "string",
    "assetType": "Photo"
  },
  "reference": "string",
  "qbPurchaseOrderRef": "string",
  "grandTotal": 0,
  "totalTax": 0,
  "totalPreCalcTax": 0,
  "totalCalcTax": 0,
  "taxResult": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "totalPreCalcTax": 0,
    "totalPostCalcTax": 0,
    "totalCityTax": 0,
    "totalCountyTax": 0,
    "totalStateTax": 0,
    "totalFedTax": 0,
    "totalCityPreTax": 0,
    "totalCountyPreTax": 0,
    "totalStatePreTax": 0,
    "totalFedPreTax": 0,
    "totalExciseTax": 0,
    "totalNALPreExciseTax": 0,
    "totalALExciseTax": 0,
    "totalALPostExciseTax": 0,
    "cultivationTaxResult": {
      "totalFlowerTax": 0,
      "totalLeafTax": 0,
      "totalFlowerOz": 0,
      "totalLeafOz": 0,
      "totalCultivationTax": 0,
      "leafTaxOz": 0,
      "flowerTaxOz": 0
    }
  },
  "transactionType": "ARMS_LENGTH",
  "customerType": "VENDOR",
  "deliveryAddress": "string",
  "deliveryTime": 0,
  "deliveryDate": 0,
  "termsAndCondition": "string",
  "deliveryCharge": 0,
  "enableDeliveryCharge": true,
  "poQrCodeAsset": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "largeX2URL": "string",
    "origURL": "string",
    "assetType": "Photo"
  },
  "poQrCodeUrl": "string",
  "flowerSourceType": "CULTIVATOR_DIRECT",
  "shipmentBillId": "string",
  "discount": 0,
  "fees": 0,
  "purchaseOrderDate": 0,
  "dueDate": 0,
  "podueDate": 0
}
```