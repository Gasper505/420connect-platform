### Find Cost
find product cost.

```endpoint
POST /api/v1/partner/store/cart/product/findCost
```

#### Request 
```json
{
  "productId": "string",
  "quantity": 0
}
```
#### Example Request

```curl
$ curl {path}/api/v1/partner/store/cart/product/findCost
```
```javascript
client.findCost(function(err, data) {
  console.log(data);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`productId` | Product ID of product | body | string
`quantity` | Product Quantity | body | integer
`cost` | Product Cost | body | integer 
`cuid` | consumer user ID | query | string

#### Example Response
```json
{
  "productId": "string",
  "quantity": 0,
  "cost": 0
}
```

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```