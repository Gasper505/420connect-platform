### Submit Cart 
 
Submit cart to be completed.

```endpoint
POST /api/v1/partner/store/cart/submitCart/{consumerCartId}
```

#### Example Request

```curl
$ curl {path}/api/v1/partner/store/cart/submitCart/{consumerCartId}
```

```javascript
client.submitCart(function(err, cart) {
  console.log(cart);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`consumerCartId` | (required) cart ID | path | string 
`cart` | (required) valid cart object | body | Cart
`cuid` | consumer user ID | query | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "consumerId": "string",
  "memberId": "string",
  "cart": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "discount": 0,
    "discountType": "Cash",
    "promoCode": "string",
    "cashReceived": 0,
    "changeDue": 0,
    "paymentOption": "Cash",
    "subTotal": 0,
    "totalDiscount": 0,
    "calcCartDiscount": 0,
    "subTotalDiscount": 0,
    "total": 0,
    "taxTable": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "name": "string",
      "active": true,
      "taxType": "Default",
      "taxOrder": "PostTaxed",
      "consumerType": "AdultUse",
      "cityTax": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "taxRate": 0,
        "compound": true,
        "active": true,
        "territory": "Default"
      },
      "countyTax": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "taxRate": 0,
        "compound": true,
        "active": true,
        "territory": "Default"
      },
      "stateTax": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "taxRate": 0,
        "compound": true,
        "active": true,
        "territory": "Default"
      },
      "federalTax": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "taxRate": 0,
        "compound": true,
        "active": true,
        "territory": "Default"
      }
    },
    "tax": 0,
    "taxResult": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "totalPreCalcTax": 0,
      "totalPostCalcTax": 0,
      "totalCityTax": 0,
      "totalCountyTax": 0,
      "totalStateTax": 0,
      "totalFedTax": 0
    },
    "totalCalcTax": 0,
    "totalPreCalcTax": 0,
    "refundAmount": 0,
    "enableDeliveryFee": true,
    "deliveryFee": 0,
    "deliveryDiscount": 0,
    "deliveryDiscountType": "Cash",
    "deliveryPromotionId": "string",
    "roundAmt": 0,
    "pointSpent": 0,
    "items": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "orderItemId": "string",
        "quantity": 0,
        "origQuantity": 0,
        "discountedQty": 0,
        "remarks": "string",
        "productId": "string",
        "cost": 0,
        "unitPrice": 0,
        "discount": 0,
        "discountType": "Cash",
        "calcDiscount": 0,
        "finalPrice": 0,
        "calcTax": 0,
        "taxOrder": "PostTaxed",
        "taxInfo": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "cityTax": 0,
          "stateTax": 0,
          "federalTax": 0
        },
        "taxType": "Default",
        "taxTable": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "name": "string",
          "active": true,
          "taxType": "Default",
          "taxOrder": "PostTaxed",
          "consumerType": "AdultUse",
          "cityTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          },
          "countyTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          },
          "stateTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          },
          "federalTax": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "taxRate": 0,
            "compound": true,
            "active": true,
            "territory": "Default"
          }
        },
        "calcPreTax": 0,
        "status": "Active",
        "quantityLogs": [
          {
            "id": "string",
            "inventoryId": "string",
            "quantity": 0,
            "prepackageItemId": "string",
            "batchId": "string"
          }
        ],
        "mixMatched": true,
        "ignoreMixMatch": true,
        "promotionReqs": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "promotionId": "string",
            "rewardId": "string"
          }
        ],
        "prepackageItemId": "string",
        "batchId": "string",
        "prepackage": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "customWeight": true,
          "toleranceId": "string",
          "productId": "string",
          "active": true,
          "unitValue": 0,
          "price": 0,
          "name": "string"
        },
        "prepackageProductItem": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "productId": "string",
          "batchId": "string",
          "batchSKU": "string",
          "prepackageId": "string",
          "sku": "string",
          "active": true
        },
        "product": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "importId": "string",
          "categoryId": "string",
          "sku": "string",
          "vendorId": "string",
          "productSaleType": "Medicinal",
          "name": "string",
          "description": "string",
          "flowerType": "string",
          "unitPrice": 0,
          "weightPerUnit": "EACH",
          "unitValue": 0,
          "thc": 0,
          "cbn": 0,
          "cbd": 0,
          "cbda": 0,
          "thca": 0,
          "active": true,
          "genetics": "string",
          "medicalConditions": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "name": "string"
            }
          ],
          "priceRanges": [
            {
              "id": "string",
              "weightToleranceId": "string",
              "price": 0,
              "priority": 0,
              "weightTolerance": {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "companyId": "string",
                "name": "string",
                "startWeight": 0,
                "endWeight": 0,
                "priority": 0,
                "unitValue": 0,
                "weightValue": 0,
                "weightKey": "UNIT",
                "enabled": true
              }
            }
          ],
          "priceBreaks": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "priceBreakType": "None",
              "name": "string",
              "price": 0,
              "quantity": 0,
              "active": true,
              "priority": 0
            }
          ],
          "assets": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "name": "string",
              "key": "string",
              "type": "Photo",
              "publicURL": "string",
              "active": true,
              "priority": 0,
              "secured": true,
              "thumbURL": "string",
              "mediumURL": "string",
              "largeURL": "string",
              "assetType": "Photo"
            }
          ],
          "quantities": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "shopId": "string",
              "dirty": true,
              "inventoryId": "string",
              "quantity": 0
            }
          ],
          "category": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "shopId": "string",
            "dirty": true,
            "name": "string",
            "cannabis": true,
            "photo": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "name": "string",
              "key": "string",
              "type": "Photo",
              "publicURL": "string",
              "active": true,
              "priority": 0,
              "secured": true,
              "thumbURL": "string",
              "mediumURL": "string",
              "largeURL": "string",
              "assetType": "Photo"
            },
            "unitType": "units",
            "active": true,
            "priority": 0,
            "lowThreshold": 0
          },
          "notes": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "writerId": "string",
              "writerName": "string",
              "message": "string"
            }
          ],
          "enableMixMatch": true,
          "enableWeedmap": true,
          "showInWidget": true,
          "taxType": "Default",
          "taxOrder": "PostTaxed",
          "customTaxInfo": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "cityTax": 0,
            "stateTax": 0,
            "federalTax": 0
          },
          "discountable": true,
          "vendor": {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "importId": "string",
            "active": true,
            "name": "string",
            "phone": "string",
            "email": "string",
            "fax": "string",
            "address": {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "address": "string",
              "city": "string",
              "state": "string",
              "zipCode": "string",
              "country": "string"
            },
            "description": "string",
            "website": "string",
            "firstName": "string",
            "lastName": "string",
            "notes": [
              {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "writerId": "string",
                "writerName": "string",
                "message": "string"
              }
            ],
            "licenseNumber": "string",
            "assets": [
              {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "companyId": "string",
                "name": "string",
                "key": "string",
                "type": "Photo",
                "publicURL": "string",
                "active": true,
                "priority": 0,
                "secured": true,
                "thumbURL": "string",
                "mediumURL": "string",
                "largeURL": "string",
                "assetType": "Photo"
              }
            ],
            "backOrderEnabled": true,
            "vendorKey": "string"
          },
          "lowThreshold": 0,
          "lowInventoryNotification": true,
          "medicinal": true,
          "byGram": true,
          "byPrepackage": true,
          "taxTables": [
            {
              "id": "string",
              "created": 0,
              "modified": 0,
              "deleted": true,
              "updated": true,
              "companyId": "string",
              "shopId": "string",
              "dirty": true,
              "name": "string",
              "active": true,
              "taxType": "Default",
              "taxOrder": "PostTaxed",
              "consumerType": "AdultUse",
              "cityTax": {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "companyId": "string",
                "shopId": "string",
                "dirty": true,
                "taxRate": 0,
                "compound": true,
                "active": true,
                "territory": "Default"
              },
              "countyTax": {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "companyId": "string",
                "shopId": "string",
                "dirty": true,
                "taxRate": 0,
                "compound": true,
                "active": true,
                "territory": "Default"
              },
              "stateTax": {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "companyId": "string",
                "shopId": "string",
                "dirty": true,
                "taxRate": 0,
                "compound": true,
                "active": true,
                "territory": "Default"
              },
              "federalTax": {
                "id": "string",
                "created": 0,
                "modified": 0,
                "deleted": true,
                "updated": true,
                "companyId": "string",
                "shopId": "string",
                "dirty": true,
                "taxRate": 0,
                "compound": true,
                "active": true,
                "territory": "Default"
              }
            }
          ],
          "tags": [
            "string"
          ],
          "companyLinkId": "string"
        },
        "preparedQty": 0,
        "fulfilled": true
      }
    ],
    "promotionReqs": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "promotionId": "string",
        "rewardId": "string"
      }
    ],
    "promotionReqLogs": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "promotionId": "string",
        "rewardId": "string",
        "amount": 0
      }
    ],
    "splitPayment": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "cashAmt": 0,
      "creditDebitAmt": 0,
      "checkAmt": 0
    },
    "paymentType": "Full",
    "storageLocation": "CashVault"
  },
  "orderPlacedTime": 0,
  "acceptedTime": 0,
  "packagedTime": 0,
  "onTheWayTime": 0,
  "completedTime": 0,
  "accepted": true,
  "completed": true,
  "packaged": true,
  "onTheWay": true,
  "eta": 0,
  "reason": "string",
  "sessionId": "string",
  "publicKey": "string",
  "cartStatus": "InProgress",
  "trackingStatus": "NotStarted",
  "pickupType": "Pickup",
  "source": "string",
  "transactionId": "string",
  "transNo": "string",
  "orderNo": "string",
  "employeeName": "string"
}
```