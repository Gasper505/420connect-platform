### Create Webhook For PlugIn
Create webhook for plugin

```endpoint
POST /api/v1/partner/store/webHook
```

#### Example Request

**Data**

Property | Description | Data Type 
---|---|---
`url` | url | string
`webHookType` | web hook type | string
`shopId` | shop id | string
`webhookUrls` | web hook urls | list

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
[
  {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shopId": "string",
    "dirty": true,
    "url": "string",
    "webHookType": "NEW_CONSUMER_ORDER",
    "webHookUrls": [
      "string"
    ]
  }
]
```