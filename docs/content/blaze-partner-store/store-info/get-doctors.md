### Get Doctors
Get all doctors

```endpoint
GET /api/v1/partner/store/doctors
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/doctors
```
```javascript
client.getDoctors(function(err, doctors) {
  console.log(doctors);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`term` |provide term _if any_| query | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "importId": "string",
      "firstName": "string",
      "lastName": "string",
      "license": "string",
      "website": "string",
      "phoneNumber": "string",
      "email": "string",
      "fax": "string",
      "degree": "string",
      "state": "string",
      "searchText": "string",
      "address": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "address": "string",
        "city": "string",
        "state": "string",
        "zipCode": "string",
        "country": "string"
      },
      "active": true,
      "notes": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "writerId": "string",
          "writerName": "string",
          "message": "string"
        }
      ],
      "assets": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "assetType": "Photo"
        }
      ]
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```