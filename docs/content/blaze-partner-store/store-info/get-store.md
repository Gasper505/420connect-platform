### Get Store
Get store information.

```endpoint
GET /api/v1/partner/store
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store
```
```javascript
client.getStore(function(err, store) {
  console.log(store);
});
```

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "shop": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shortIdentifier": "string",
    "name": "string",
    "shopType": "Medicinal",
    "address": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "zipCode": "string",
      "country": "string"
    },
    "phoneNumber": "string",
    "emailAdress": "string",
    "license": "string",
    "enableDeliveryFee": true,
    "deliveryFee": 0,
    "taxOrder": "PostTaxed",
    "taxInfo": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "cityTax": 0,
      "stateTax": 0,
      "federalTax": 0
    },
    "showWalkInQueue": true,
    "showDeliveryQueue": true,
    "showOnlineQueue": true,
    "enableCashInOut": true,
    "timeZone": "string",
    "latitude": 0,
    "longitude": 0,
    "active": true,
    "snapshopTime": 0,
    "defaultCountry": "string",
    "onlineStoreInfo": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "enableStorePickup": true,
      "enableDelivery": true,
      "enableOnlineShipment": true,
      "enableProductReviews": true,
      "colorTheme": "Light",
      "defaultETA": 0,
      "pageOneMessageTitle": "string",
      "pageOneMessageBody": "string",
      "submissionMessage": "string",
      "cartMinimum": 0,
      "enabled": true,
      "websiteOrigins": "string",
      "supportEmail": "string",
      "enableOnlinePOS": true,
      "enableDeliveryAreaRestrictions": true,
      "restrictedZipCodes": [
        "string"
      ],
      "enableETA": true,
      "customMessageETA": "string"
    },
    "deliveryFees": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "enabled": true,
        "subTotalThreshold": 0,
        "fee": 0
      }
    ],
    "enableSaleLogout": true,
    "assets": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "name": "string",
        "key": "string",
        "type": "Photo",
        "publicURL": "string",
        "active": true,
        "priority": 0,
        "secured": true,
        "thumbURL": "string",
        "mediumURL": "string",
        "largeURL": "string",
        "assetType": "Photo"
      }
    ],
    "enableBCCReceipt": true,
    "bccEmailAddress": "string",
    "enableGPSTracking": true,
    "receivingInventoryId": "string",
    "defaultPinTimeout": 0,
    "showSpecialQueue": true,
    "emailList": [
      "string"
    ],
    "enableLowInventoryEmail": true,
    "restrictedViews": true,
    "emailMessage": "string",
    "taxRoundOffType": "ONE_CENT",
    "enforceCashDrawers": true,
    "useAssignedEmployee": true,
    "showProductByAvailableQuantity": true,
    "autoCashDrawer": true,
    "numAllowActiveTrans": 0,
    "requireValidRecDate": true,
    "enableDeliverySignature": true,
    "restrictIncomingOrderNotifications": true,
    "restrictedNotificationTerminals": [
      "string"
    ],
    "roundOffType": "NONE",
    "roundUpMessage": "string",
    "useComplexTax": true,
    "taxTables": [
      {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "name": "string",
        "active": true,
        "taxType": "Default",
        "taxOrder": "PostTaxed",
        "consumerType": "AdultUse",
        "cityTax": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "taxRate": 0,
          "compound": true,
          "active": true,
          "territory": "Default"
        },
        "countyTax": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "taxRate": 0,
          "compound": true,
          "active": true,
          "territory": "Default"
        },
        "stateTax": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "taxRate": 0,
          "compound": true,
          "active": true,
          "territory": "Default"
        },
        "federalTax": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "taxRate": 0,
          "compound": true,
          "active": true,
          "territory": "Default"
        }
      }
    ],
    "marketingSources": [
      "string"
    ],
    "productsTag": [
      "string"
    ],
    "checkoutType": "Direct",
    "timezoneOffsetInMinutes": 0,
    "defaultPinTimeoutDuration": 0
  },
  "contract": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "shopId": "string",
    "dirty": true,
    "name": "string",
    "active": true,
    "text": "string",
    "version": 0,
    "required": true,
    "pdfFile": {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string",
      "key": "string",
      "type": "Photo",
      "publicURL": "string",
      "active": true,
      "priority": 0,
      "secured": true,
      "thumbURL": "string",
      "mediumURL": "string",
      "largeURL": "string",
      "assetType": "Photo"
    },
    "enableWitnessSignature": true,
    "enableEmployeeSignature": true,
    "contentType": "TEXT"
  },
  "companyLogoURL": "string"
}
```