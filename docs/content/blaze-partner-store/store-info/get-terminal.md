### Get Terminal
Get all terminal

```endpoint
GET /api/v1/partner/store/terminals
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/partner/store/terminals
```
```javascript
client.getAllTerminals(function(err, terminal) {
  console.log(terminal);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`start` |start index of terminal array| query | string
`limit` |number of terminal desired | query | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "values": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "shopId": "string",
      "dirty": true,
      "active": true,
      "deviceId": "string",
      "name": "string",
      "deviceModel": "string",
      "deviceVersion": "string",
      "deviceName": "string",
      "appVersion": "string",
      "deviceToken": "string",
      "deviceType": "string",
      "assignedInventoryId": "string",
      "cvAccountId": "string",
      "currentEmployeeId": "string",
      "terminalLocations": [
        {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "shopId": "string",
          "dirty": true,
          "terminalId": "string",
          "employeeId": "string",
          "timeCardId": "string",
          "deviceId": "string",
          "name": "string",
          "loc": [
            0
          ]
        }
      ]
    }
  ],
  "skip": 0,
  "limit": 0,
  "total": 0
}
```