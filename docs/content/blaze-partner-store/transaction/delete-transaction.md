### Delete Transaction
Delete transaction from queue

```endpoint
DELETE /api/v1/partner/transactions/{transactionId}
```

**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`transactionId` | transaction id | path | string
`deleteRequest` | delete transaction request | body | Object

#### Example Request
```json
{
  "reason": "string",
  "terminalId": "string",
  "employeeId": "string"
}
```
#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```