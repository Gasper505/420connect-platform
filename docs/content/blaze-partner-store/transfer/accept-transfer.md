### Accept Transfer
Accept Inventory Transfer

```endpoint
POST  /api/v1/partner/store/batches/transferInventory/{historyId}/accept
```
#### Example Request

```curl
$ curl -X POST {path}/api/v1/partner/store/batches/transferInventory/{historyId}/accept
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`historyId` | history id | path | String
`request` | request | body | Object

#### Example Request
```json
{
  "currentEmployeeId": "string"
}
```
#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```

#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "status": "ACCEPTED",
  "createByEmployeeId": "string",
  "acceptByEmployeeId": "string",
  "declineByEmployeeId": "string",
  "fromShopId": "string",
  "toShopId": "string",
  "fromInventoryId": "string",
  "toInventoryId": "string",
  "transferLogs": [
    {
      "productId": "string",
      "prepackageItemId": "string",
      "transferAmount": 0,
      "finalInventory": 0,
      "origFromQty": 0,
      "finalFromQty": 0,
      "origToQty": 0,
      "prevTransferAmt": 0,
      "fromBatchId": "string",
      "fromBatchInfo": "string",
      "prepackageName": "string",
      "toBatchId": "string",
      "fromProductBatch": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "productId": "string",
        "vendorId": "string",
        "batchNo": 0,
        "cost": 0,
        "costPerUnit": 0,
        "quantity": 0,
        "sku": "string",
        "thc": 0,
        "cbn": 0,
        "cbd": 0,
        "cbda": 0,
        "thca": 0,
        "purchasedDate": 0,
        "sellBy": 0,
        "archived": true,
        "archivedDate": 0,
        "published": true,
        "publishedQuantity": 0,
        "publishedDate": 0,
        "trackTraceVerified": true,
        "trackPackageId": 0,
        "trackPackageLabel": "string",
        "trackHarvestBatch": "string",
        "trackHarvestDate": "string",
        "trackTraceSystem": "MANUAL",
        "trackWeight": "UNIT",
        "purchaseOrderId": "string",
        "perUnitExciseTax": 0,
        "totalExciseTax": 0,
        "customerCompanyId": "string",
        "brandId": "string",
        "receiveDate": 0,
        "poNumber": "string",
        "status": "RECEIVED",
        "metrcCategory": "string",
        "voidStatus": true,
        "active": true,
        "batchQRAsset": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        },
        "productBatchLabel": {
          "labelType": "TEST_RESULT",
          "enabledProductName": true,
          "enablesBatchId": true,
          "enablesLotId": true,
          "enabledPackageId": true,
          "enablesNetWeight": true,
          "enabledBarCode": true,
          "enabledQRCode": true,
          "enableCultivationName": true,
          "enableTestResults": true,
          "enableTestDate": true
        },
        "lotId": "string",
        "metrcTagId": "string",
        "totalCultivationTax": 0,
        "potencyAmount": {
          "thc": 0,
          "cbd": 0,
          "cbn": 0,
          "thca": 0,
          "cbda": 0
        },
        "referenceNote": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "writerId": "string",
          "writerName": "string",
          "message": "string",
          "enableOnFleet": true
        },
        "flowerSourceType": "CULTIVATOR_DIRECT",
        "attachments": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          }
        ],
        "productName": "string",
        "vendorName": "string",
        "brandName": "string",
        "liveQuantity": 0,
        "derivedLogId": "string",
        "expirationDate": 0,
        "licenseId": "string",
        "labelInfo": "string",
        "notes": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "writerId": "string",
            "writerName": "string",
            "message": "string",
            "enableOnFleet": true
          }
        ],
        "prepaidTax": true
      },
      "toProductBatch": {
        "id": "string",
        "created": 0,
        "modified": 0,
        "deleted": true,
        "updated": true,
        "companyId": "string",
        "shopId": "string",
        "dirty": true,
        "productId": "string",
        "vendorId": "string",
        "batchNo": 0,
        "cost": 0,
        "costPerUnit": 0,
        "quantity": 0,
        "sku": "string",
        "thc": 0,
        "cbn": 0,
        "cbd": 0,
        "cbda": 0,
        "thca": 0,
        "purchasedDate": 0,
        "sellBy": 0,
        "archived": true,
        "archivedDate": 0,
        "published": true,
        "publishedQuantity": 0,
        "publishedDate": 0,
        "trackTraceVerified": true,
        "trackPackageId": 0,
        "trackPackageLabel": "string",
        "trackHarvestBatch": "string",
        "trackHarvestDate": "string",
        "trackTraceSystem": "MANUAL",
        "trackWeight": "UNIT",
        "purchaseOrderId": "string",
        "perUnitExciseTax": 0,
        "totalExciseTax": 0,
        "customerCompanyId": "string",
        "brandId": "string",
        "receiveDate": 0,
        "poNumber": "string",
        "status": "RECEIVED",
        "metrcCategory": "string",
        "voidStatus": true,
        "active": true,
        "batchQRAsset": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "companyId": "string",
          "name": "string",
          "key": "string",
          "type": "Photo",
          "publicURL": "string",
          "active": true,
          "priority": 0,
          "secured": true,
          "thumbURL": "string",
          "mediumURL": "string",
          "largeURL": "string",
          "largeX2URL": "string",
          "origURL": "string",
          "assetType": "Photo"
        },
        "productBatchLabel": {
          "labelType": "TEST_RESULT",
          "enabledProductName": true,
          "enablesBatchId": true,
          "enablesLotId": true,
          "enabledPackageId": true,
          "enablesNetWeight": true,
          "enabledBarCode": true,
          "enabledQRCode": true,
          "enableCultivationName": true,
          "enableTestResults": true,
          "enableTestDate": true
        },
        "lotId": "string",
        "metrcTagId": "string",
        "totalCultivationTax": 0,
        "potencyAmount": {
          "thc": 0,
          "cbd": 0,
          "cbn": 0,
          "thca": 0,
          "cbda": 0
        },
        "referenceNote": {
          "id": "string",
          "created": 0,
          "modified": 0,
          "deleted": true,
          "updated": true,
          "writerId": "string",
          "writerName": "string",
          "message": "string",
          "enableOnFleet": true
        },
        "flowerSourceType": "CULTIVATOR_DIRECT",
        "attachments": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "companyId": "string",
            "name": "string",
            "key": "string",
            "type": "Photo",
            "publicURL": "string",
            "active": true,
            "priority": 0,
            "secured": true,
            "thumbURL": "string",
            "mediumURL": "string",
            "largeURL": "string",
            "largeX2URL": "string",
            "origURL": "string",
            "assetType": "Photo"
          }
        ],
        "productName": "string",
        "vendorName": "string",
        "brandName": "string",
        "liveQuantity": 0,
        "derivedLogId": "string",
        "expirationDate": 0,
        "licenseId": "string",
        "labelInfo": "string",
        "notes": [
          {
            "id": "string",
            "created": 0,
            "modified": 0,
            "deleted": true,
            "updated": true,
            "writerId": "string",
            "writerName": "string",
            "message": "string",
            "enableOnFleet": true
          }
        ],
        "prepaidTax": true
      }
    }
  ],
  "batchQuantityInfoMap": {},
  "declinedDate": 0,
  "acceptedDate": 0,
  "transferNo": "string",
  "completeTransfer": true,
  "transferByBatch": true,
  "processedTime": 0
}
```