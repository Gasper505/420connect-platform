### Add Vendor
Add Vendor

```endpoint
POST  /api/v1/partner/vendors
```
#### Example Request

```curl
$ curl -X POST {path}/api/v1/partner/vendors
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`request` | request | body | Object

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Request
```json
{
  "importId": "string",
  "active": true,
  "name": "string",
  "medical": true,
  "phone": "string",
  "email": "string",
  "website": "string",
  "fax": "string",
  "address": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "address": "string",
    "city": "string",
    "state": "string",
    "zipCode": "string",
    "country": "string"
  },
  "description": "string",
  "contactFirstName": "string",
  "contactLastName": "string",
  "licenseNumber": "string",
  "firstName": "string",
  "lastName": "string",
  "assets": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string",
      "key": "string",
      "type": "Photo",
      "publicURL": "string",
      "active": true,
      "priority": 0,
      "secured": true,
      "thumbURL": "string",
      "mediumURL": "string",
      "largeURL": "string",
      "largeX2URL": "string",
      "origURL": "string",
      "assetType": "Photo"
    }
  ],
  "licenseExpirationDate": 0,
  "armsLengthType": "ARMS_LENGTH",
  "brands": [
    "string"
  ],
  "companyType": "DISTRIBUTOR",
  "additionalAddressList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "zipCode": "string",
      "country": "string"
    }
  ],
  "licenceType": "RECREATIONAL",
  "relatedEntity": true,
  "mobileNumber": "string",
  "notes": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "writerId": "string",
      "writerName": "string",
      "message": "string",
      "enableOnFleet": true
    }
  ],
  "vendorType": "CUSTOMER",
  "dbaName": "string"
}
```

#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "importId": "string",
  "active": true,
  "name": "string",
  "phone": "string",
  "email": "string",
  "fax": "string",
  "address": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "address": "string",
    "city": "string",
    "state": "string",
    "zipCode": "string",
    "country": "string"
  },
  "description": "string",
  "website": "string",
  "firstName": "string",
  "lastName": "string",
  "notes": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "writerId": "string",
      "writerName": "string",
      "message": "string",
      "enableOnFleet": true
    }
  ],
  "licenseNumber": "string",
  "assets": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "name": "string",
      "key": "string",
      "type": "Photo",
      "publicURL": "string",
      "active": true,
      "priority": 0,
      "secured": true,
      "thumbURL": "string",
      "mediumURL": "string",
      "largeURL": "string",
      "largeX2URL": "string",
      "origURL": "string",
      "assetType": "Photo"
    }
  ],
  "backOrderEnabled": true,
  "licenseExpirationDate": 0,
  "armsLengthType": "ARMS_LENGTH",
  "brands": [
    "string"
  ],
  "qbVendorRef": [
    {}
  ],
  "qbDesktopRef": "string",
  "companyType": "DISTRIBUTOR",
  "additionalAddressList": [
    {
      "id": "string",
      "created": 0,
      "modified": 0,
      "deleted": true,
      "updated": true,
      "companyId": "string",
      "address": "string",
      "city": "string",
      "state": "string",
      "zipCode": "string",
      "country": "string"
    }
  ],
  "credits": 0,
  "mobileNumber": "string",
  "licenceType": "RECREATIONAL",
  "relatedEntity": true,
  "vendorType": "CUSTOMER",
  "dbaName": "string",
  "vendorKey": "string"
}
```