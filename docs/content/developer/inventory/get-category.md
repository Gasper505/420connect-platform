### Get Category
Get product category by product Id.

```endpoint
GET /api/v1/public/inventory/categories
```

#### Example Request

```curl
$ curl -X GET {path}/api/v1/public/inventory/categories
```
```javascript
client.getCategory(function(err, category) {
  console.log(category);
});
```
**Parameters**

Property | Description | Parameter Type | Data Type 
---|---|---|---
`categoryId` |(required) product category ID| path | string

#### Response Status

```json
{
code : 200,
message : 'successful operation'
}
```
#### Example Response
```json
{
  "id": "string",
  "created": 0,
  "modified": 0,
  "deleted": true,
  "updated": true,
  "companyId": "string",
  "shopId": "string",
  "dirty": true,
  "name": "string",
  "cannabis": true,
  "photo": {
    "id": "string",
    "created": 0,
    "modified": 0,
    "deleted": true,
    "updated": true,
    "companyId": "string",
    "name": "string",
    "key": "string",
    "type": "Photo",
    "publicURL": "string",
    "active": true,
    "priority": 0,
    "secured": true,
    "thumbURL": "string",
    "mediumURL": "string",
    "largeURL": "string",
    "assetType": "Photo"
  },
  "unitType": "units",
  "active": true,
  "priority": 0,
  "lowThreshold": 0
}
```