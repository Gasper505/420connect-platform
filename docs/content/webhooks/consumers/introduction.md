## Consumer User

These are webhooks which will be called by Blaze Platform whenever a new consumer user is signed up
Blaze Platform will execute a POST request with a `Content-Type: application/json`