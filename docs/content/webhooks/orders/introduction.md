## Consumer Order

These are webhooks which will be called by Blaze Platform whenever a new order is placed or updated.
Blaze Platform will execute a POST request with a `Content-Type: application/json`