### Update Consumer Order
Whenever an order is updated this webhook will be executed. e.g. If tracking status gets changed etc.

```endpoint
POST URL_SAVED_WITHIN_BLAZE
```

**Data**

Property | Description | Data Type 
---|---|---
`consumerOrderId` | consumer order id | string
`consumerOrderNo` | consumer order number | string
`consumerName` | consumer name | string
`memberName` | member name | string
`orderTime` | order time | integer
`orderStatus` | order status | string

**Valid order status**
 - InProgress
 - Placed
 - Accepted
 - Completed
 - Declined
 - CanceledByConsumer
 - CanceledByDispensary

#### Example Data
```json
{
  "consumerOrderId": "string",
  "consumerOrderNo": "string",
  "consumerName": "string",
  "phoneNumber": "string",
  "memberName": "string",
  "orderTime": 0,
  "orderStatus": "string"
}
```