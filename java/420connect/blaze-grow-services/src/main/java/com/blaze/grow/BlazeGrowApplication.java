package com.blaze.grow;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fourtwenty.core.CoreModule;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.google.common.collect.ImmutableMap;
import io.dropwizard.Application;
import io.dropwizard.server.AbstractServerFactory;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.LifeCycleInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.ManagedInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.TaskInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.eager.EagerSingletonInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.HealthCheckInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.JerseyFeatureInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.ResourceInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.provider.JerseyProviderInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.plugin.PluginInstaller;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Map;

/**
 * Created by mdo on 6/8/17.
 */
public class BlazeGrowApplication extends Application<ConnectConfiguration> {
    static final Log LOG = LogFactory.getLog(BlazeGrowApplication.class);
    GuiceBundle<ConnectConfiguration> guiceBundle = null;


    public static void main(String[] args) throws Exception {
        new BlazeGrowApplication().run(args);
        //new QuartzProperties();
    }

    @Override
    public void initialize(Bootstrap<ConnectConfiguration> bootstrap) {
        super.initialize(bootstrap);

        bootstrap.getObjectMapper().registerSubtypes(DefaultServerFactory.class);
        //bootstrap.getObjectMapper().registerModule(new FCSerializerModule());
        bootstrap.getObjectMapper().enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);

        BlazeGrowModule coreModule = new BlazeGrowModule();
        GuiceBundle.Builder builder = GuiceBundle.<ConnectConfiguration>builder()
                .noDefaultInstallers()
                .installers(new Class[]{LifeCycleInstaller.class,
                        ManagedInstaller.class,
                        JerseyFeatureInstaller.class,
                        JerseyProviderInstaller.class,
                        ResourceInstaller.class,
                        EagerSingletonInstaller.class,
                        HealthCheckInstaller.class,
                        TaskInstaller.class,
                        PluginInstaller.class
                })
                .modules(new CoreModule(), coreModule)
                .enableAutoConfig(BlazeGrowApplication.class.getPackage().getName());

        bootstrap.addBundle(new GrowSwaggerBundle());
        postInitialize(bootstrap, builder);
        guiceBundle = builder.build();
        bootstrap.addBundle(guiceBundle);
    }

    @Override
    public void run(ConnectConfiguration configuration, Environment environment) throws Exception {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        AbstractServerFactory sf = (AbstractServerFactory) configuration.getServerFactory();
        environment.jersey().register(MultiPartFeature.class);


        String corsURL = "*";
        Map<String, String> corsInitParams = ImmutableMap.of(
                CrossOriginFilter.ALLOWED_ORIGINS_PARAM, corsURL,
                CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS",
                CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true",
                CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin",
                CrossOriginFilter.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true"
        );

        String appContext = environment.getApplicationContext().getContextPath();
        FilterRegistration.Dynamic dFilter = environment.servlets()
                .addFilter("CORSFilter", CrossOriginFilter.class);
        dFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, environment.getApplicationContext().getContextPath() + "*");
        dFilter.setInitParameters(corsInitParams);


        LOG.info("App context " + appContext);


        // environment.getObjectMapper().registerModule()
        LOG.debug("in config");
        postRun(configuration, environment);

        LOG.info("****************STARTING  BLAZE GROW APPLICATION ENVIRONMENT MODE: " + configuration.getEnv() + "*******************");

    }

    protected void postRun(final ConnectConfiguration configuration, final Environment environment) throws Exception {
        // Sub-classes should
    }

    protected void postInitialize(Bootstrap<ConnectConfiguration> bootstrapm, GuiceBundle.Builder guiceBuilder) {
        // Sub-classes should
    }
}
