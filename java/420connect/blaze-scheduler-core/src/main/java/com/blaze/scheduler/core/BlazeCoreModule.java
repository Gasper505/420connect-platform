package com.blaze.scheduler.core;

import com.blaze.scheduler.core.services.scheduler.HeadsetQuartzService;
import com.blaze.scheduler.core.services.scheduler.QuartzService;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.blaze.scheduler.core.services.scheduler.impl.AutoCashDrawerStartupTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.BackUpLogTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.CalculateReportDataJobImpl;
import com.blaze.scheduler.core.services.scheduler.impl.CloverStartUpTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.CompanyLicenseExpirationTask;
import com.blaze.scheduler.core.services.scheduler.impl.ConsumerOrderStartUpTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.DailySummaryStartUpTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.HeadsetQuartzServiceImpl;
import com.blaze.scheduler.core.services.scheduler.impl.HeadsetStartupTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.InventoryNotificationStartupTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.InventorySnapshotStartUpTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.OnPremSyncStartupTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.ProductBatchExpirationTask;
import com.blaze.scheduler.core.services.scheduler.impl.QuartzServiceImpl;
import com.blaze.scheduler.core.services.scheduler.impl.QuickAccesstokenJobImpl;
import com.blaze.scheduler.core.services.scheduler.impl.QuickBooksSyncTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.WeedmapAuthStartUpTaskImpl;
import com.blaze.scheduler.core.services.scheduler.impl.LeaflyQuartzJobStartupTaskImpl;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

/**
 * Created by mdo on 6/14/17.
 */
public class BlazeCoreModule extends AbstractModule {
    @Override
    protected void configure() {

        bind(QuartzService.class).to(QuartzServiceImpl.class);
        bind(HeadsetQuartzService.class).to(HeadsetQuartzServiceImpl.class);

        // bind quartz startup task
        Multibinder<QuartzStartupTask> mb = Multibinder.newSetBinder(binder(), QuartzStartupTask.class);
        mb.addBinding().to(OnPremSyncStartupTaskImpl.class);
        mb.addBinding().to(HeadsetStartupTaskImpl.class);
        mb.addBinding().to(InventoryNotificationStartupTaskImpl.class);
        //mb.addBinding().to( MemberBirthdayStartupTaskImpl.class);
        mb.addBinding().to(AutoCashDrawerStartupTaskImpl.class);
        mb.addBinding().to(BackUpLogTaskImpl.class);
        mb.addBinding().to(QuickBooksSyncTaskImpl.class);
        mb.addBinding().to(QuickAccesstokenJobImpl.class);
        mb.addBinding().to(DailySummaryStartUpTaskImpl.class);
        mb.addBinding().to(InventorySnapshotStartUpTaskImpl.class);
        mb.addBinding().to(ConsumerOrderStartUpTaskImpl.class);
        mb.addBinding().to(CompanyLicenseExpirationTask.class);

        mb.addBinding().to(WeedmapAuthStartUpTaskImpl.class);

        mb.addBinding().to(CalculateReportDataJobImpl.class);
        mb.addBinding().to(CloverStartUpTaskImpl.class);
        mb.addBinding().to(ProductBatchExpirationTask.class);
        mb.addBinding().to(LeaflyQuartzJobStartupTaskImpl.class);
    }
}
