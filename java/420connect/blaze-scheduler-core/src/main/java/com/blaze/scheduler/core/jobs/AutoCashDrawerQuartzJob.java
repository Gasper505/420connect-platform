package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionResult;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by mdo on 12/7/17.
 */
@DisallowConcurrentExecution
public class AutoCashDrawerQuartzJob implements Job {
    static final int MIN_UTC_HOUR = 10;
    static final Log LOGGER = LogFactory.getLog(AutoCashDrawerQuartzJob.class);

    @Inject
    ShopRepository shopRepository;
    @Inject
    CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    PaidInOutItemRepository paidInOutItemRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    CashDrawerActivityLogRepository cashDrawerActivityLogRepository;
    @Inject
    CashDrawerProcessorService cashDrawerProcessorService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Executing AutoCashDrawerQuartzJob job");

        // this is in UTC
        DateTime nowUTC = DateUtil.nowUTC();
        int hourOfDay = nowUTC.getHourOfDay();
        if (hourOfDay < MIN_UTC_HOUR) {
            LOGGER.info(String.format("CASH DRAWER: UTC DATE '%d' is lower than %d", hourOfDay, MIN_UTC_HOUR));
            return;
        }

        DateTime now = DateTime.now();
        long nowMillis = now.getMillis();

        DateTime yesterday = DateTime.now().minusDays(1);
        long yesterdayMillis = yesterday.getMillis();


        HashMap<String, Company> companyHashMap = companyRepository.listAsMap();
        List<Shop> shops = shopRepository.list();

        // there aren't that many terminals so it's better to get all terminals instead of doing multiple terminal calls
        Iterable<Terminal> terminals = terminalRepository.listNonDeleted();


        int newCashdrawersCount = 0;
        int newPaidsOutsCount = 0;
        int updatedDrawersCount = 0;
        for (Shop shop : shops) {
            Company company = companyHashMap.get(shop.getCompanyId());

            // need to do this per shop since we need the shop's timezone
            List<CashDrawerSession> newCashDrawers = new ArrayList<>();
            List<PaidInOutItem> newPaidsOuts = new ArrayList<>();
            List<CashDrawerSession> needUpdatedDrawers = new ArrayList<>();
            List<CashDrawerSessionActivityLog> newActivityLogs = new ArrayList<>();


            if (company.isActive()) {
                if (shop.isAutoCashDrawer()) {

                    // Current date
                    String todayDate = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone());
                    String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyyMMdd");


                    String yesterdayDate = DateUtil.toDateFormatted(yesterdayMillis, shop.getTimeZone());
                    String yesterdayDateFormmated = DateUtil.toDateFormatted(yesterdayMillis, shop.getTimeZone(), "yyyyMMdd");

                    LOGGER.info(String.format("%s: Cash Drawer Date: %s,  formatted: %s,  yesterday: %s, yesterdayFormatted: %s",
                            shop.getName(), todayDate, todayFormatted, yesterdayDate, yesterdayDateFormmated));


                    HashMap<String, CashDrawerSession> yesterdayDrawers = cashDrawerSessionRepository.getCashDrawersForDateAsMapTerminal(shop.getCompanyId(),
                            shop.getId(), yesterdayDateFormmated);


                    HashMap<String, CashDrawerSession> todaysDrawers = cashDrawerSessionRepository.getCashDrawersForDateAsMapTerminal(shop.getCompanyId(),
                            shop.getId(), todayFormatted);

                    for (Terminal terminal : terminals) {
                        if (terminal.isActive() && terminal.getShopId().equalsIgnoreCase(shop.getId())) {
                            // must be the same shop
                            CashDrawerSession todayDrawer = todaysDrawers.get(terminal.getId());


                            // get expected
                            BigDecimal expected = new BigDecimal(0);
                            CashDrawerSession yesterDrawer = yesterdayDrawers.get(terminal.getId());
                            if (yesterDrawer != null) {
                                // recalculate
                                //processCurrentCashDrawer(yesterDrawer, false, shop);
                                cashDrawerProcessorService.processCurrentCashDrawer(shop, yesterDrawer, false);
                                expected = yesterDrawer.getExpectedCash();
                            }
                            //
                            boolean carriedOver = false;
                            if (todayDrawer == null) {
                                // Cash drawer doesn't exist, let's create a new one
                                // Create a new Cash Drawer
                                CashDrawerSessionResult logResult = new CashDrawerSessionResult();
                                logResult.prepare(terminal.getCompanyId());
                                logResult.setTerminalId(terminal.getId());
                                logResult.setShopId(terminal.getShopId());
                                logResult.setStartEmployeeId(terminal.getCurrentEmployeeId());
                                logResult.setStartTime(DateTime.now().getMillis());
                                logResult.setStartCash(expected);
                                logResult.setDate(todayFormatted);


                                DateTime cdDate = DateUtil.parseDateKey(logResult.getDate(),shop.getTimeZone());
                                logResult.setDateTimestamp(cdDate.plusMinutes(1).getMillis());


                                newCashDrawers.add(logResult);




                                carriedOver = true;
                                // new cash drawers
                                //processCurrentCashDrawer(logResult, true, shop);
                                cashDrawerProcessorService.processCurrentCashDrawer(shop, logResult, true);
                            } else {
                                // today cash drawer exist, now let's just transfer over if starting cash is 0
                                if (todayDrawer.getStartCash().doubleValue() <= 0 && expected.doubleValue() > 0) {
                                    todayDrawer.setStartCash(expected);
                                    //Update starting cash
                                    carriedOver = true;
                                    //processCurrentCashDrawer(todayDrawer, true, shop);
                                    cashDrawerProcessorService.processCurrentCashDrawer(shop, todayDrawer, true);
                                    needUpdatedDrawers.add(todayDrawer);
                                }
                            }

                            if (carriedOver && yesterday != null && expected.doubleValue() > 0) {
                                // now create a paid in for the cash drawer
                                PaidInOutItem log = new PaidInOutItem();
                                log.prepare(yesterDrawer.getCompanyId());
                                log.setShopId(yesterDrawer.getShopId());
                                log.setCdSessionId(yesterDrawer.getId());
                                log.setTerminalId(yesterDrawer.getTerminalId());
                                log.setEmployeeId(terminal.getCurrentEmployeeId());
                                log.setAmount(expected);
                                log.setReason("Auto carry over to next day.");
                                log.setCashVault(false);
                                log.setType(PaidInOutItem.PaidInOutType.PaidOut);
                                log.setToTerminalId(null);
                                log.setCarryOver(true);

                                newPaidsOuts.add(log);

                                if (yesterDrawer.getStatus() == CashDrawerSession.CashDrawerLogStatus.Open) {
                                    yesterDrawer.setStatus(CashDrawerSession.CashDrawerLogStatus.Closed);
                                    yesterDrawer.setEndEmployeeId(terminal.getCurrentEmployeeId());
                                    yesterDrawer.setEndTime(DateTime.now().getMillis());
                                    yesterDrawer.setAmtBeforeCarryOver(expected);
                                }
                                needUpdatedDrawers.add(yesterDrawer);

                                CashDrawerSessionActivityLog activityLog = createCashDrawerActivityType(log, yesterDrawer, CashDrawerSessionActivityLog.CashDrawerType.AddPaidOut, expected);
                                newActivityLogs.add(activityLog);
                            }
                        }
                    }

                }
            }


            // create paidIns
            if (newPaidsOuts.size() > 0) {
                paidInOutItemRepository.save(newPaidsOuts);
            }

            // update old cash drawers
            for (CashDrawerSession yesterdayDrawer : needUpdatedDrawers) {
                //processCurrentCashDrawer(yesterdayDrawer,false,shop);
                cashDrawerProcessorService.processCurrentCashDrawer(shop, yesterdayDrawer, false);
                cashDrawerSessionRepository.update(yesterdayDrawer.getCompanyId(), yesterdayDrawer.getId(), yesterdayDrawer);
            }

            // add new cash drawers
            if (newCashDrawers.size() > 0) {
                cashDrawerSessionRepository.save(newCashDrawers);
            }

            if (newActivityLogs.size() > 0) {
                cashDrawerActivityLogRepository.save(newActivityLogs);
            }

            newCashdrawersCount += newCashDrawers.size();
            newPaidsOutsCount += newPaidsOuts.size();
            updatedDrawersCount += needUpdatedDrawers.size();
        }


        LOGGER.info(String.format("Created Paidouts: %d", newPaidsOutsCount));
        LOGGER.info(String.format("Created New Cash Drawers: %d", newCashdrawersCount));
        LOGGER.info(String.format("Updated Yesterday's Drawers: %d", updatedDrawersCount));

    }


    public CashDrawerSessionActivityLog createCashDrawerActivityType(PaidInOutItem paidInOutItem, CashDrawerSession cashDrawerSession,
                                                                     CashDrawerSessionActivityLog.CashDrawerType cashDrawerType,
                                                                     BigDecimal amount) {
        CashDrawerSessionActivityLog cashDrawerSessionActivityLog = new CashDrawerSessionActivityLog();
        cashDrawerSessionActivityLog.prepare(cashDrawerSession.getCompanyId());
        cashDrawerSessionActivityLog.setShopId(cashDrawerSession.getShopId());

        if (!Objects.isNull(paidInOutItem)) {
            cashDrawerSessionActivityLog.setPadIoItemId(paidInOutItem.getId());
            cashDrawerSessionActivityLog.setCashDrawerSessionId(cashDrawerSession.getId());
        }

        cashDrawerSessionActivityLog.setCashDrawerSessionId(cashDrawerSession.getId());
        cashDrawerSessionActivityLog.setCashDrawerType(cashDrawerType);
        cashDrawerSessionActivityLog.setCreatedByEmployeeId(null);

        cashDrawerSessionActivityLog.setSaleAmount(amount);
        cashDrawerSessionActivityLog.setMessage("(System) " + cashDrawerType.getMessage());

        return cashDrawerSessionActivityLog;
    }
}
