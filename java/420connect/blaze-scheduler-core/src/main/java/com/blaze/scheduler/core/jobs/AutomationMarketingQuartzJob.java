package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyBounceNumber;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

@DisallowConcurrentExecution
public class AutomationMarketingQuartzJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(MarketingQuartzJob.class);

    @Inject
    private CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    private AppRepository appRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private MarketingJobRepository marketingJobRepository;
    @Inject
    private CompanyBounceNumberRepository companyBounceNumberRepository;
    @Inject
    private MarketingJobLogRepository marketingJobLogRepository;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOG.info("Executing Automation Marketing Job");

        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        boolean fakeSMS = app.isFakeSMS();
        boolean fakeEmail = app.isFakeEmail();
        if (app == null || !app.isEnableSMS()) {
            // do not execute if sms is not enabled
            LOG.info("SMS is not enabled for app.");
            return;
        }
        LOG.info("Fake SMS: " + fakeSMS + ", fake Email: " + fakeEmail);

        Iterable<MarketingJob> jobs = marketingJobRepository.getMarketingJobsByType(MarketingJob.MarketingType.Automation);

        HashMap<String, Shop> shopHashSet = new HashMap<>();
        HashMap<String, Company> companyHashMap = new HashMap<>();
        if (jobs.iterator().hasNext()) {
            shopHashSet = shopRepository.listAsMap();
            companyHashMap = companyRepository.listAsMap();
        }

        int marketingJobSuccess = 0;
        int marketingJobFailed = 0;
        for (MarketingJob marketingJob : jobs) {
            // these should only be pending jobs

            Company company = companyHashMap.get(marketingJob.getCompanyId());
            if (company == null || !company.isActive()) {
                updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.Failed);
                marketingJobFailed++;
                continue;
            }

            Shop shop = shopHashSet.get(marketingJob.getShopId());
            if (shop == null || !shop.isActive()) {
                updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.Failed);
                marketingJobFailed++;
                continue;
            }

            final String fromNumber = amazonServiceManager.getFromNumber(app, shop, true);
            String cleanFromNumber = AmazonServiceManager.cleanPhoneNumber(fromNumber, shop);

            updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.InProcess);

            String address = String.format("%s, %s, %s %s", shop.getAddress().getAddress(),
                    shop.getAddress().getCity(), shop.getAddress().getState(), shop.getAddress().getZipCode());

            String message = marketingJob.getMessage();
            message = message.replaceAll(Pattern.quote("{{shop_name}}"), shop.getName());
            message = message.replaceAll(Pattern.quote("{{shop_website}}"), company.getWebsite());
            message = message.replaceAll(Pattern.quote("{{shop_address}}"), address);
            // Set this to In Progress


            Iterable<Member> members = getMembersTextOptIn(marketingJob);
            List<Member> membersList = Lists.newArrayList(members);

            int smsSent = 0;
            int emailSent = 0;
            if (membersList.size() > 0) {
                CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(company.getId());
                if (companyFeatures == null) {
                    continue;
                }
                int smsAvailable = companyFeatures.getSmsMarketingCredits();
                int emailAvailable = companyFeatures.getEmailMarketingCredits();


                HashMap<String, CompanyBounceNumber> companyBounceNumberMap = companyBounceNumberRepository.listAllByCompany(company.getId());

                List<MarketingJobLog> jobLogs = new ArrayList<>();


                for (Member member : membersList) {

                    String newMessage = message + "";
                    String lastVisited = "";
                    if (member.getLastVisitDate() != null) {
                        lastVisited = DateUtil.toDateFormatted(member.getLastVisitDate(), shop.getTimeZone());
                    }

                    newMessage = newMessage.replaceAll(Pattern.quote("{{member_firstname}}"), member.getFirstName());
                    newMessage = newMessage.replaceAll(Pattern.quote("{{member_lastname}}"), member.getLastName());
                    newMessage = newMessage.replaceAll(Pattern.quote("{{member_lastvisited}}"), lastVisited);


                    if (marketingJob.getJobType() == MarketingJob.MarketingJobType.SMS
                            && StringUtils.isNotBlank(member.getPrimaryPhone())
                            && member.isTextOptIn()) {
                        if (smsAvailable > smsSent) {

                            String phoneNumber = AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
                            CompanyBounceNumber bounceNumber = companyBounceNumberMap.get(phoneNumber);

                            if (fakeSMS == false && (bounceNumber == null || (bounceNumber.getStatus() == CompanyBounceNumber.BounceNumberStatus.OPT_IN))) {

                                // send sms message
                                boolean sentRequested = amazonServiceManager.sendMarketingSMSMessage(Lists.newArrayList(member.getPrimaryPhone()), newMessage, shop, fromNumber, app);

                                if (sentRequested) {
                                    smsSent++;

                                    MarketingJobLog jobLog = new MarketingJobLog();
                                    jobLog.prepare(marketingJob.getCompanyId());
                                    jobLog.setMarketingJobId(marketingJob.getId());
                                    jobLog.setStatus(MarketingJobLog.MSentStatus.SENT);
                                    jobLog.setFromNumber(cleanFromNumber);
                                    jobLog.setToNumber(phoneNumber);

                                    // add to log
                                    jobLogs.add(jobLog);
                                }
                            }
                        }
                    }
                }


                smsAvailable -= smsSent;
                emailAvailable -= emailSent;

                if (smsAvailable != companyFeatures.getSmsMarketingCredits()
                        || emailAvailable != companyFeatures.getEmailMarketingCredits()) {
                    // there were changes to available credits, let's update
                    companyFeaturesRepository.subtractSMSEmailCredits(marketingJob.getCompanyId(), smsSent, emailSent);
                }


                // Persist Job Logs
                marketingJobLogRepository.save(jobLogs);
            }

            if (marketingJob.getJobType() == MarketingJob.MarketingJobType.SMS) {
                double cost = smsSent * app.getSmsCost().doubleValue() / 100;
                marketingJob.setSentCost(new BigDecimal(cost));
                marketingJob.setSentCount(smsSent);
            } else {
                double cost = smsSent * app.getEmailCost().doubleValue() / 100;
                marketingJob.setSentCost(new BigDecimal(cost));
                marketingJob.setSentCount(emailSent);
            }
            // Update Job status
//            updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.Completed);
            marketingJobSuccess++;
        }

        LOG.info("Automation Marketing Job Success: " + marketingJobSuccess + ",  fails: " + marketingJobFailed);
    }

    private void updateMarketingJob(MarketingJob marketingJob, MarketingJob.MarketingJobStatus status) {
        marketingJob.setStatus(status);
        if (status == MarketingJob.MarketingJobStatus.Completed
                || status == MarketingJob.MarketingJobStatus.Failed) {
            marketingJob.setCompletedDate(DateTime.now().getMillis());
            marketingJob.setActive(false);
        }
        marketingJobRepository.update(marketingJob.getCompanyId(), marketingJob.getId(), marketingJob);
    }

    private Iterable<Member> getMembersTextOptIn(final MarketingJob marketingJob) {
        if (marketingJob.isEnable() && MarketingJob.MarketingRecipientType.AutomatedBirthdayText == marketingJob.getRecipientType()) {

            List<Member> memberList = new ArrayList<>();
            DateTime now = DateTime.now();
            Iterable<Member> members = memberRepository.listWithOptionsTextOptIn(marketingJob.getCompanyId(),
                    Member.MembershipStatus.Active,
                    "{firstName:1,lastName:1,primaryPhone:1,email:1,textOptIn:1,emailOptIn:1,dob:1}");

            for (Member member : members) {
                if (member.getDob() != null) {
                    int days = DateUtil.getDaysBetweenTwoDates(now.getMillis(), member.getDob());
                    if (days == marketingJob.getDayBefore()) {
                        memberList.add(member);
                    }
                }
            }

            return memberList;
        } else if (marketingJob.isEnable() && MarketingJob.MarketingRecipientType.AutomatedRecommendationExpireText == marketingJob.getRecipientType()) {

            List<Member> memberList = new ArrayList<>();
            DateTime now = DateTime.now();
            Iterable<Member> members = memberRepository.listWithOptionsTextOptIn(marketingJob.getCompanyId(),
                    Member.MembershipStatus.Active,
                    "{firstName:1,lastName:1,primaryPhone:1,email:1,textOptIn:1,emailOptIn:1,recommendations:1}");

            for (Member member : members) {
                if (!member.getRecommendations().isEmpty() && member.getRecommendations().get(0).getExpirationDate() != null) {
                    int days = DateUtil.getDaysBetweenTwoDates(now.getMillis(), member.getRecommendations().get(0).getExpirationDate());
                    if (days == marketingJob.getDayBefore()) {
                        memberList.add(member);
                    }
                }
            }
            return memberList;
        }

        return null;
    }
}
