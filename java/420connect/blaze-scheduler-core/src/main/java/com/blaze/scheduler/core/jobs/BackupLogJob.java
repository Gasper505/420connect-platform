package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.repositories.dispensary.AuditLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductChangeLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.QueuedTransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class BackupLogJob implements Job {

    static final Logger LOGGER = LoggerFactory.getLogger(BackupLogJob.class);

    @Inject
    private AuditLogRepository auditLogRepository;
    @Inject
    private ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private AmazonS3Service amazonS3Service;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Disabling backup log for now...");
/*
        long date = new DateTime().minusMonths(4).getMillis();

        Iterable<AuditLog> auditLogs = auditLogRepository.listBefore(date);
        Set<ObjectId> shopIds = new HashSet<>();

        Map<String, List<AuditLog>> auditLogByShop = new HashMap<>();
        for (AuditLog auditLog : auditLogs) {
            if(ObjectId.isValid(auditLog.getShopId())) {
                shopIds.add(new ObjectId(auditLog.getShopId()));

                auditLogByShop.putIfAbsent(auditLog.getShopId(), new ArrayList<>());
                auditLogByShop.get(auditLog.getShopId()).add(auditLog);
            }
        }

        Iterable<ProductChangeLog> productLogs = productChangeLogRepository.listBefore(date);

        Map<String, List<ProductChangeLog>> productLogByShop = new HashMap<>();
        for (ProductChangeLog productLog : productLogs) {

            if(ObjectId.isValid(productLog.getShopId())) {
                shopIds.add(new ObjectId(productLog.getShopId()));

                productLogByShop.putIfAbsent(productLog.getShopId(), new ArrayList<>());
                productLogByShop.get(productLog.getShopId()).add(productLog);
            }
        }


        Iterable<QueuedTransaction> queuedTransactions = queuedTransactionRepository.listBefore(date);


        HashMap<String, Shop> shopMap = shopRepository.findItemsInAsMap(new ArrayList<ObjectId>(shopIds));

        try {

            for (Map.Entry<String, Shop> shopEntry : shopMap.entrySet()) {

                boolean shouldDeleteAuditLogs = false;
                boolean shouldDeleteProductLogs = false;

                String shopName = shopEntry.getValue().getId();

                amazonS3Service.createFolder(shopName);

                List<AuditLog> shopAuditLogs = auditLogByShop.get(shopEntry.getKey());

                if(shopAuditLogs != null && !shopAuditLogs.isEmpty()) {
                    amazonS3Service.createFolder(shopName + "/" + "audit_log");

                    AuditLog firstLog = shopAuditLogs.get(0);
                    AuditLog lastLog = shopAuditLogs.get(shopAuditLogs.size() - 1);
                    String fileName = this.createLogFileName(firstLog.getCreated(), lastLog.getCreated());

                    String logs = JsonSerializer.toJson(shopAuditLogs);
                    InputStream stream = new ByteArrayInputStream(logs.getBytes());

                    shouldDeleteAuditLogs = amazonS3Service.uploadFile(stream, fileName, ".json", shopName + "/" + "Audit Log");
                }

                if(shouldDeleteAuditLogs) {
                    deleteAuditLogs(date);
                }


                List<ProductChangeLog> productChangeLogs = productLogByShop.get(shopEntry.getKey());

                if(productChangeLogs != null && !productChangeLogs.isEmpty()) {
                    amazonS3Service.createFolder(shopName + "/" + "product_log");

                    ProductChangeLog firstLog = productChangeLogs.get(0);
                    ProductChangeLog lastLog = productChangeLogs.get(productChangeLogs.size() - 1);

                    String fileName = this.createLogFileName(firstLog.getCreated(), lastLog.getCreated());

                    String logs = JsonSerializer.toJson(productChangeLogs);
                    InputStream stream = new ByteArrayInputStream(logs.getBytes());

                    shouldDeleteProductLogs = amazonS3Service.uploadFile(stream, fileName, ".json", shopName + "/" + "Product Log");
                }

                if(shouldDeleteProductLogs) {
                    deleteProductLogs(date);
                }

            }

        } catch (Exception e) {
            LOGGER.error("Error while copying logs ", e.getMessage(), e);
        }
*/
    }

    private String createLogFileName(Long created, Long lastCreated) {
        return (DateUtil.toUTCDateTimeFormat(lastCreated) + " -- " + DateUtil.toUTCDateTimeFormat(created)).replaceAll("/", "-");
    }

    private void deleteProductLogs(long date) {
        productChangeLogRepository.hardRemoveByBeforeDate(date);
    }

    private void deleteAuditLogs(long date) {
        auditLogRepository.hardRemoveByBeforeDate(date);
    }

}
