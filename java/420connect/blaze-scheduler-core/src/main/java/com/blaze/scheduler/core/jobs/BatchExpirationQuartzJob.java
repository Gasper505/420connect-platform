package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BatchExpirationQuartzJob implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchExpirationQuartzJob.class);
    private static final String DELIMITER = ",";
    private static final String fileName = "Product Batch Expiration.csv";
    private static final String FILE_HEADERS = "S.No., Product Name, Vendor Name, Batch Id, Expiration Date, Expiring In Days, Purchased Quantity, Live Quantity";
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private NotificationInfoRepository notificationInfoRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private VendorRepository vendorRepository;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Product Batch Expiration notification job has been started");

        HashMap<String, Company> companyHashMap = companyRepository.listNonDeletedActiveAsMap();
        Iterable<Shop> shops = shopRepository.iterator();

        for (Shop shop : shops) {
            if (shop.isDeleted() || !shop.isActive()) {
                continue;
            }
            Company company = companyHashMap.get(shop.getCompanyId());
            if (company == null || company.isDeleted() || !company.isActive()) {
                continue;
            }
            LOGGER.info("Batch Expiration notification for shop: " + shop.getName());
            dataForBatchExpirationNotification(shop, company, true, null);
            LOGGER.info("Product Batch Expiration notification job has been ended");
        }

    }

    /**
     * @param shop
     * @param nightlyJob
     * @param email
     */
    public void dataForBatchExpirationNotification(Shop shop, Company company, boolean nightlyJob, List<String> email) {

        // For last sent email
        HashMap<String, NotificationInfo> notificationInfoHashMap = notificationInfoRepository.listByShopAndTypeAsMap(NotificationInfo.NotificationType.Product_Batch_Expiration);
        /* If for only one shop*/

        if (shop == null && company == null) {
            return;
        }

        NotificationInfo notificationInfo = notificationInfoHashMap.get(shop.getId());

        List<String> toEmails = new ArrayList<>();

        /* If email provided in request use those else use from notification info else use shop's email*/
        toEmails = email;
        if ((toEmails == null || toEmails.isEmpty()) && notificationInfo != null && StringUtils.isNotBlank(notificationInfo.getDefaultEmail())) {
            toEmails = Arrays.asList(notificationInfo.getDefaultEmail().split("\\s*,\\s*"));
        }

        DateTime newNotificationTime = null;
        if (nightlyJob && notificationInfo != null) {
            if (!notificationInfo.isActive()) {
                return;
            }
            /*
             run only when call from quartz job
            */
            DateTime dateTime = new DateTime(notificationInfo.getLastSent());
            if (dateTime.plusDays(1).isAfterNow()) {
                return;
            }
            // last snap shot taken time
            long lastNotificationTime = notificationInfo.getLastSent();
            long nextNotificationTime = notificationInfo.getNextNotificationTime();

            String lastNotificationStr = DateUtil.toDateFormatted(lastNotificationTime, shop.getTimeZone());
            String nextNotificationTimeStr = DateUtil.toDateFormatted(nextNotificationTime, shop.getTimeZone());

            // Job takes snapshot at 2:00AM else continue
            newNotificationTime = DateUtil.nowWithTimeZone(shop.getTimeZone()).withTimeAtStartOfDay().plusDays(1).plusHours(3);
            DateTime currentTime = DateUtil.nowWithTimeZone(shop.getTimeZone());
            String newRequiredTimeStr = DateUtil.toDateFormatted(newNotificationTime.getMillis(), shop.getTimeZone());


            /* run only when call from quartz job or previously send */
            if (currentTime.getMillis() < nextNotificationTime) {
                // Not yet time
                LOGGER.info("Not yet time, skipping...");
                return;
            }

            if (StringUtils.isNotBlank(notificationInfo.getDefaultEmail())) {
                toEmails = Arrays.asList(notificationInfo.getDefaultEmail().split("\\s*,\\s*"));
            }
        }


        DateTime currentTime = DateUtil.nowUTC().withTimeAtStartOfDay();
        String timeZone = DateTimeZone.UTC.toString();

        if (StringUtils.isNotBlank(shop.getTimeZone())) {
            timeZone = shop.getTimeZone();
            currentTime = DateUtil.nowWithTimeZone(timeZone).withTimeAtStartOfDay();
        }

        DateTime sevenDaysTime = currentTime.plusDays(7).withTimeAtStartOfDay();

        Iterable<ProductBatch> productBatches = productBatchRepository.listAllByShop(shop.getCompanyId(), shop.getId());

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> vendorIds = new ArrayList<>();
        for (ProductBatch batch : productBatches) {
            if (StringUtils.isNotBlank(batch.getProductId()) && ObjectId.isValid(batch.getProductId())) {
                productIds.add(new ObjectId(batch.getProductId()));
            }

            if (StringUtils.isNotBlank(batch.getVendorId()) && ObjectId.isValid(batch.getVendorId())) {
                vendorIds.add(new ObjectId(batch.getVendorId()));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(company.getId(), productIds);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(company.getId(), vendorIds);

        Map<String, Object> productBatchMap = this.prepareBatchList(productBatches, productHashMap, vendorHashMap, currentTime.getMillis(), sevenDaysTime, notificationInfo, timeZone);

        String emailBody = this.prepareEmailBody(shop, company);
        Boolean dataAvailable = (Boolean) productBatchMap.get("dataAvailable");

        //If product batch information  is available then only send email
        if (dataAvailable) {
            StringBuilder data = (StringBuilder) productBatchMap.get("data");
            if (StringUtils.isNotBlank(data)) {
                StringBuilder str = new StringBuilder(FILE_HEADERS);
                data = str.append("\n").append(data);
                this.sendMail(shop, data, emailBody, toEmails);
            }
        }
    }


    /**
     * @param productBatches
     * @param sevenDaysTime
     * @param notificationInfo
     * @param timeZone
     * @return
     */
    private Map<String, Object> prepareBatchList(Iterable<ProductBatch> productBatches, HashMap<String, Product> productHashMap, HashMap<String, Vendor> vendorHashMap, long currentTime, DateTime sevenDaysTime, NotificationInfo notificationInfo, String timeZone) {
        Map<String, Object> productBatchMap = new HashMap<>();

        StringBuilder builder = new StringBuilder();

        DateTime expirationDate;
        int sNo = 0;
        for (ProductBatch batch : productBatches) {

            if (batch.getExpirationDate() <= 0) {
                continue;
            }
            if (batch.isArchived()) {
                continue;
            }
            Product product = productHashMap.get(batch.getProductId());
            if (product == null || product.isDeleted() || !product.isActive()) {
                continue;
            }
            Vendor vendor = vendorHashMap.get(batch.getVendorId());

            if (batch.getExpirationDate() > 0) {
                expirationDate = getBatchExpirationDate(batch.getExpirationDate(), timeZone);
                sNo = prepareBatchData(sNo, builder, batch, product, vendor, expirationDate, sevenDaysTime, currentTime, notificationInfo);
            }
        }


        productBatchMap.put("dataAvailable", Boolean.FALSE);
        if (sNo > 0) {
            productBatchMap.put("dataAvailable", Boolean.TRUE);
            productBatchMap.put("data", builder);
        }

        return productBatchMap;
    }

    /**
     * @param shop
     * @param company
     * @return
     */
    private String prepareEmailBody(Shop shop, Company company) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/batchExpirationNotification.html");

        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        String emailBody = writer.toString();

        String logoURL = company != null && company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";

        emailBody = emailBody.replaceAll("==logo==", logoURL);
        emailBody = emailBody.replaceAll("==preferredEmailColor==", company != null && StringUtils.isNotBlank(company.getPreferredEmailColor()) ? company.getPreferredEmailColor() : "#1cc4e8 !important");
        emailBody = emailBody.replaceAll("==shopName==", shop.getName().replaceAll("," ," "));

        return emailBody;
    }

    /**
     * @param expirationDate
     * @param timeZone
     * @return
     */
    private DateTime getBatchExpirationDate(long expirationDate, String timeZone) {
        return DateUtil.toDateTime(expirationDate, timeZone).withTimeAtStartOfDay();
    }

    /**
     * @param shop
     * @param data
     * @param emailBody
     */

    private void sendMail(Shop shop, StringBuilder data, String emailBody, List<String> requestToEmail) {

        byte[] bytes = data.toString().getBytes();
        InputStream stream = new ByteArrayInputStream(bytes);

        HashMap<String, InputStream> attachmentData = new HashMap<>();
        attachmentData.put(fileName, stream);

        HashMap<String, HashMap<String, InputStream>> attachmentMap = new HashMap<>();
        attachmentMap.put("attachment", attachmentData);

        if (requestToEmail != null && requestToEmail.size() > 0) {
            for (String email : requestToEmail) {
                amazonServiceManager.sendMultiPartEmail("support@blaze.me", email, "Product Batch Expiration Notification", emailBody, null, null, attachmentMap, "Content-Disposition","","Blaze Support");
            }
        } else {
            List<String> emailList = shop.getEmailList();
            for (String email : emailList) {
                amazonServiceManager.sendMultiPartEmail("support@blaze.me", email, "Product Batch Expiration Notification", emailBody, null, null, attachmentMap, "Content-Disposition", "", "Blaze Support");
            }
            amazonServiceManager.sendMultiPartEmail("support@blaze.me", shop.getEmailAdress(), "Product Batch Expiration Notification", emailBody, null, null, attachmentMap, "Content-Disposition", "", "Blaze Support");
        }
    }

    /**
     * @param builder
     * @param batch
     * @param expirationDate
     * @param sevenDaysTime
     * @param currentTime
     * @param notificationInfo
     * @param serialNumber
     * @return
     */
    private int prepareBatchData(int serialNumber, StringBuilder builder, ProductBatch batch, Product product, Vendor vendor, DateTime expirationDate, DateTime sevenDaysTime, long currentTime, NotificationInfo notificationInfo) {

        if ((DateUtil.getDaysBetweenTwoDates(currentTime, expirationDate.getMillis()) >= 0) && (expirationDate.getMillis() <= sevenDaysTime.getMillis())) {
            if (batch != null) {
                builder.append(++serialNumber).append(DELIMITER);
                builder.append(StringUtils.isNotBlank(product.getName()) ? product.getName().replaceAll(",", " ") : "N/A").append(DELIMITER);
                builder.append("\"" + (StringUtils.isNotBlank(vendor.getName()) ? vendor.getName().replaceAll(",", " ") : "N/A") + "\"").append(DELIMITER);
                builder.append(TextUtil.textOrEmpty(StringUtils.isNotBlank(batch.getSku()) ? batch.getSku().replaceAll(",", " ") : "N/A")).append(DELIMITER);
                builder.append(TextUtil.toDate(expirationDate.getMillis())).append(DELIMITER);
                builder.append(TextUtil.textOrEmpty((DateUtil.getDaysBetweenTwoDates(currentTime, expirationDate.getMillis()) == 0) ? "Today" : DateUtil.getDaysBetweenTwoDates(currentTime, expirationDate.getMillis()) + " Day(s)")).append(DELIMITER);
                builder.append(TextUtil.formatToTwoDecimalPoints((batch.getQuantity() != null && batch.getQuantity().doubleValue() > 0) ? batch.getQuantity() : BigDecimal.ZERO)).append(DELIMITER);
                builder.append(TextUtil.formatToTwoDecimalPoints((batch.getLiveQuantity() != null && batch.getLiveQuantity().doubleValue() > 0) ? batch.getLiveQuantity() : BigDecimal.ZERO)).append(DELIMITER);

                builder.append("\n");
            }
        }
        return serialNumber;

    }

}
