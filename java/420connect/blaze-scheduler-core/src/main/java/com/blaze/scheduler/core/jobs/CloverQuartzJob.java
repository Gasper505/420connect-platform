package com.blaze.scheduler.core.jobs;


import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.clover.CloverJobEvent;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class CloverQuartzJob implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(CloverQuartzJob.class);
    @Inject
    private BlazeEventBus blazeEventBus;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Send Payment to Clover");
        CloverJobEvent cloverJobEvent = new CloverJobEvent();
        blazeEventBus.post(cloverJobEvent);
    }
}
