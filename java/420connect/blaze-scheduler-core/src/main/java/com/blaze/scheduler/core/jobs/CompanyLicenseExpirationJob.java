package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.jobs.CompanyLicenseExpirationNotification;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class CompanyLicenseExpirationJob implements Job {

    static final Logger LOGGER = LoggerFactory.getLogger(CompanyLicenseExpirationJob.class);

    @Inject
    private CompanyLicenseExpirationNotification expirationNotification;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        LOGGER.info("Company license expiration job has been started");
        expirationNotification.validateCompanyLicence();
        LOGGER.info("Company license expiration job has been ended");
    }
}
