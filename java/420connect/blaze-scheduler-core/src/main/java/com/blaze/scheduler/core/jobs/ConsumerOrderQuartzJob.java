package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.jobs.ConsumerOrderNotification;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class ConsumerOrderQuartzJob implements Job {
    static final Logger LOGGER = LoggerFactory.getLogger(ConsumerOrderQuartzJob.class);

    @Inject
    private ConsumerOrderNotification consumerOrderNotification;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Starting consumer order notification quartz job");
        //consumerOrderNotification.processConsumerOrderNotification(true, null, null);
    }
}