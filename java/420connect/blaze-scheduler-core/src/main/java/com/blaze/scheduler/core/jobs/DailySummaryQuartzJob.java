package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.jobs.DailySummaryEmail;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class DailySummaryQuartzJob implements Job {
    static final Logger LOGGER = LoggerFactory.getLogger(DailySummaryQuartzJob.class);

    @Inject
    private DailySummaryEmail dailySummaryEmail;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Sending daily summary");
        dailySummaryEmail.dataForDailySummary(true, null, null, null, 0);
    }
}