package com.blaze.scheduler.core.jobs;

import com.blaze.scheduler.core.managed.HeadsetAPIManager;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.common.HeadsetConfig;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.HeadsetLocationRepository;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.HeadsetProductAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.RestockAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.TicketAddRequest;
import com.fourtwenty.core.thirdparty.headset.HeadsetAPIService;
import com.fourtwenty.core.thirdparty.headset.models.HeadsetMappingResult;
import com.fourtwenty.core.thirdparty.headset.models.HeadsetTicketItem;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.assertj.core.util.Lists;
import org.joda.time.DateTime;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;


/**
 * Created by Gaurav Saini on 10/7/17.
 */
@DisallowConcurrentExecution
public class HeadsetTransferDataQuartzJob implements Job {
    private static final int INITIAL_DAYS_SYNC_SALES = 7; // 7 days ago
    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(HeadsetTransferDataQuartzJob.class);
    @Inject
    HeadsetAPIService headsetAPIService;
    @Inject
    HeadsetLocationRepository headsetLocationRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    HeadsetAPIManager headsetAPIManager;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    AppRepository appRepository;
    @Inject
    IntegrationSettingRepository integrationSettingRepository;

    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOG.info("Executing Headset job");
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app == null || !app.isEnableHeadset()) {
            // do not execute if sms is not enabled
            LOG.info("Headset Data Transfer is not enabled for app.");
            return;
        }


        Iterable<HeadsetLocation> headsetAccounts = headsetLocationRepository.getAcceptedHeadsetLocations();
        List<HeadsetLocation> headsetLocations = Lists.newArrayList(headsetAccounts);
        HashMap<String, Company> companyHashMap = companyRepository.listAsMap();
        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap();
        // locations in this list has accepted the terms
        int accountSynced = 0; // https://trello.com/c/aeed5xEk/441-p0-update-wordpress-plugin-with-the-latest-javascript-widget-code
        for (HeadsetLocation headsetLocation : headsetLocations) {
            LOG.info("-- Syncing company: {} shop: {} datasource: {}", headsetLocation.getCompanyId(), headsetLocation.getShopId(), headsetLocation.getDataSourceId());

            HeadsetConfig headsetConfig = integrationSettingRepository.getHeadsetConfig(headsetLocation.getEnvironment());
            int secondsToCheck = headsetConfig.getSyncInterval();

            // check last connection status if more than 10 mins sync data
            long lastSyncDate = headsetLocation.getLastSyncDate() == null ? 0 : headsetLocation.getLastSyncDate();
            DateTime nowTime = DateTime.now();
            boolean initialSync = headsetLocation.getLastSyncDate() == null;

            DateTime lastSync = new DateTime(lastSyncDate);
            // We only want to this if headset account was sync'ed greater than 10 minutes ago
            if (lastSync.plusSeconds(secondsToCheck).isAfter(nowTime)) {
                // continue
                continue;
            }

            //Update last sync so we can check next 10 minutes
            headsetLocation.setLastSyncDate(nowTime.getMillis());
            headsetLocation.setProductLastSyncDate(nowTime.getMillis());
            headsetLocationRepository.update(headsetLocation.getCompanyId(), headsetLocation.getId(), headsetLocation);


            // check for active company
            Company company = companyHashMap.get(headsetLocation.getCompanyId());
            if (company == null || !company.isActive()) {
                // skip this headset account
                continue;
            }

            // check shop
            Shop shop = shopHashMap.get(headsetLocation.getShopId());
            if (shop == null || !shop.isActive()) {
                continue;
            }

            if (!headsetLocation.isEnableSync()) {
                LOG.info(String.format("Headset sync is not enabled for '%s. Skipping..", shop.getName()));
                continue;
            }
            LOG.info("Headset transfer for shop: " + shop.getName());


            String companyId = headsetLocation.getCompanyId();
            String shopId = headsetLocation.getShopId();
            String dataSourceId = headsetLocation.getDataSourceId();

            if (dataSourceId == null) {
                try {
                    HeadsetMappingResult result = headsetAPIService.mapping(headsetConfig, headsetLocation);

                    if (result != null && result.size() > 0) {
                        dataSourceId = result.get(0).getDataSourceId() + "";
                        headsetLocation.setDataSourceId(dataSourceId);
                    }
                    System.out.println(result);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            int employeesSuccess = 0;
            int employeesErr = 0;
            int membersSuccess = 0;
            int membersErr = 0;
            int productsSucccess = 0;
            int productsErr = 0;
            int batchSuccess = 0;
            int batchErr = 0;
            int transSuccess = 0;
            int transErr = 0;


            long now = DateTime.now().getMillis();

            //NOTE: WE ARE NOT SYNCING MEMBERS OR EMPLOYEES FOR NOW!!!
            // Emplyee with Budtender.
            //mdo: Only retrieve employees that are greater than lastSync
            /*
            Iterable<Employee> employeeList = employeeRepository.listWithDate(companyId, lastSyncDate,now);
            for (Employee employee : employeeList) {
                employeesSuccess++;
                BudTenderAddRequest request = new BudTenderAddRequest();
                request.setBudtenderId(employee.getId());
                request.setEmail(employee.getEmail());
                request.setFirstName(employee.getFirstName());
                request.setLastName(employee.getLastName());

                headsetAPIManager.addBudTender(account.getApiKey(), request, dataSourceId);
            }*/

            // Member with HeadsetCustomer

            /*Iterable<Member> memberList = memberRepository.listWithDate(companyId, lastSyncDate, now);
            //mdo: Only retrieve members that are greater than lastSync
            for (Member member : memberList) {
                membersSuccess++;
                HeadsetCustomerAddRequest request = new HeadsetCustomerAddRequest();
                request.setCustomerId(member.getId());
                request.setDob(DateUtil.toDateFormatted(member.getDob(),shop.getTimeZone()));
                request.setEmail(member.getEmail());
                request.setFirstName(member.getFirstName());
                request.setPhoneNumber(member.getPrimaryPhone());

                request.setPostalCode("");
                if (member.getAddress() != null) {
                    request.setPostalCode(member.getAddress().getZipCode());
                }
//            request.setReferralSource();      // referralSource is related to which field for Member

                headsetAPIManager.addCustomer(account.getApiKey(), request, dataSourceId);
            }
            */

            long productLastSyncDate = headsetLocation.getProductLastSyncDate() == null ? 0 : headsetLocation.getProductLastSyncDate();
            // Product with Product
            Iterable<Product> productList = productRepository.listByShopWithDate(companyId, shopId, productLastSyncDate, now);
            LOG.info("-- Fetched product list of {} records.", Iterables.size(productList));


            HashMap<String, Vendor> vendorHashMap = vendorRepository.listAllAsMap(companyId);
            HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAllAsMap(companyId, shopId);
            HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAllAsMap(companyId, shopId);
            HashMap<String, Product> productHashMap = productRepository.listAllAsMap(companyId, shopId);
            HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(companyId, shopId);
            HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(companyId, shopId);


            //mdo: Only retrieve products that are greater than lastSync
            for (Product product : productList) {
                productsSucccess++;
                HeadsetProductAddRequest request = new HeadsetProductAddRequest();
                request.setProductId(product.getId());
                request.setName(product.getName());
                request.setPrice((int) (product.getUnitPrice().doubleValue()));
                request.setUnit(String.valueOf(product.getUnitValue()));


                Vendor vendor = vendorHashMap.get(product.getVendorId());
                request.setVendor("Unknown Vendor");
                if (vendor != null) {
                    request.setVendor(vendor.getName());
                }
                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                if (category != null) {
                    request.setCategory(category.getName());
                } else {
                    request.setCategory("Unknown");
                }
                request.setActive(product.isActive());
                request.setBulkItem(false);
                request.setProductType(product.getFlowerType());            // is this correct ?
                int quantity = 0;
                for (ProductQuantity pq : product.getQuantities()) {
                    quantity += pq.getQuantity().intValue();


                    // If this is the initial sync, then let's send initial stock over
                    if (initialSync) {

                        if (pq.getQuantity().doubleValue() > 0) {
                            RestockAddRequest restockAddRequest = new RestockAddRequest();
                            restockAddRequest.setParentId("");
                            restockAddRequest.setId(UUID.randomUUID().toString());
                            restockAddRequest.setUtcDate(DateUtil.toUTCDateTime(DateTime.now().getMillis()).toString());      // is this correct ?
                            restockAddRequest.setQuantity(pq.getQuantity().intValue());
                            restockAddRequest.setCostPerUnit(0);
                            restockAddRequest.setProductId(product.getId());
                            Inventory inventory = inventoryHashMap.get(pq.getInventoryId());
                            if (inventory != null) {
                                restockAddRequest.setInventoryId(inventory.getName());
                            } else {
                                restockAddRequest.setInventoryId(Inventory.SAFE);
                            }

                            headsetAPIManager.restock(headsetConfig, headsetLocation, restockAddRequest);
                        }
                    }
                }
                request.setQuantityInStock(quantity);

                headsetAPIManager.addProduct(headsetConfig, headsetLocation, request);
                LOG.info("-- Added product {}.", request.getName());
            }

            // Product Batches with Restock. Only do this when after initial sync
            if (!initialSync) {
                // Safe Inventory
                Inventory safeInventory = null;
                for (Inventory inventory : inventoryHashMap.values()) {
                    if (inventory.getName().equalsIgnoreCase(Inventory.SAFE)) {
                        safeInventory = inventory;
                        break;
                    }
                }

                Iterable<ProductBatch> productBatches = productBatchRepository.listByShopWithDate(companyId, shopId, lastSyncDate, now);  //getProductBatches(companyId, shopId);
                LOG.info("-- Fetched product batch of {} records.", Iterables.size(productBatches));
                for (ProductBatch productBatch : productBatches) {
                    batchSuccess++;
                    RestockAddRequest restockAddRequest = new RestockAddRequest();
                    restockAddRequest.setParentId("");
                    restockAddRequest.setId(productBatch.getId());
                    restockAddRequest.setUtcDate(DateUtil.toUTCDateTime(productBatch.getPurchasedDate()).toString());      // is this correct ?
                    restockAddRequest.setQuantity(productBatch.getQuantity().intValue());
                    double costPerUnit = productBatch.getFinalUnitCost().doubleValue();
                    restockAddRequest.setCostPerUnit((int) (costPerUnit));
                    restockAddRequest.setProductId(productBatch.getProductId());
                    if (safeInventory != null) {
                        restockAddRequest.setInventoryId(safeInventory.getName());
                    } else {
                        restockAddRequest.setInventoryId(Inventory.SAFE);
                    }

                    headsetAPIManager.restock(headsetConfig, headsetLocation, restockAddRequest);

                    LOG.info("-- Restocked from product batch {}[{}] with {} quantity.",
                            restockAddRequest.getProductId(),
                            restockAddRequest.getInventoryId(),
                            restockAddRequest.getQuantity());
                }
            }

            //mdo: Only retrieve transactions that are greater than lastSync
            long salesLastSync = lastSyncDate;
            if (initialSync) {
                salesLastSync = DateTime.now().withTimeAtStartOfDay().minusDays(INITIAL_DAYS_SYNC_SALES).getMillis();
            }
            // Only do this for new sync's
            Iterable<Transaction> transactionList = transactionRepository.getCompletedTransactionsDatesList(headsetLocation.getCompanyId(),
                    headsetLocation.getShopId(),
                    salesLastSync, now);
            LOG.info("-- Fetched transaction list of {} records.", Iterables.size(transactionList));

            for (Transaction transaction : transactionList) {
                transSuccess++;
                if (transaction.getTransType() == Transaction.TransactionType.Transfer) {
                    // this is a transfer
                    for (OrderItem orderItem : transaction.getCart().getItems()) {
                        RestockAddRequest restockAddRequest = new RestockAddRequest();
                        restockAddRequest.setParentId("");
                        restockAddRequest.setId(orderItem.getId());
                        restockAddRequest.setUtcDate(DateUtil.toUTCDateTime(transaction.getProcessedTime()).toString());      // is this correct ?
                        restockAddRequest.setQuantity(orderItem.getQuantity().intValue());
                        double costPerUnit = 0;
                        if (orderItem.getQuantity().doubleValue() > 0
                                && orderItem.getCost().doubleValue() > 0) {
                            double cost = orderItem.getCost().doubleValue();
                            double quantity = orderItem.getQuantity().doubleValue();
                            costPerUnit = cost / quantity;
                        }
                        restockAddRequest.setCostPerUnit((int) (costPerUnit));
                        restockAddRequest.setProductId(orderItem.getProductId());

                        restockAddRequest.setInventoryId(Inventory.SAFE);
                        // override from logs
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            Inventory inventory = inventoryHashMap.get(quantityLog.getInventoryId());
                            if (inventory != null) {
                                restockAddRequest.setInventoryId(inventory.getName());
                                break;
                            }
                        }

                        headsetAPIManager.restock(headsetConfig, headsetLocation, restockAddRequest);

                        LOG.info("-- Restocked from transaction {}[{}] with {} quantity.",
                                restockAddRequest.getProductId(),
                                restockAddRequest.getInventoryId(),
                                restockAddRequest.getQuantity());
                    }
                } else {
                    // continue with sales
                    if (transaction.getStatus() == Transaction.TransactionStatus.Void) {
                        continue;
                    }
                    // ALL SALE PRICES ARE IN 'CENTS' PER HEADSET
                    TicketAddRequest request = new TicketAddRequest();
                    request.setId(transaction.getId());
                    request.setUtcDate(DateUtil.toUTCDateTime(transaction.getProcessedTime()).toString());
                    request.setLocalTime(DateUtil.stripTimezoneOffset(
                            DateUtil.toDateTime(transaction.getProcessedTime(), shop.getTimeZone()).toString()));

                    request.setTicketType(transaction.getTransType().name());
                    request.setCustomerReference("");
                    request.setEmployeeReference("");
                    request.setPaymentType(transaction.getCart().getPaymentOption().name());
                    request.setTotal((int) (transaction.getCart().getTotal()).doubleValue() * 100);
                    request.setTax((int) (transaction.getCart().getTotalCalcTax().doubleValue() * 100));
                    request.setSubTotal((int) (transaction.getCart().getSubTotal().doubleValue() * 100));
                    request.setDiscount((int) (transaction.getCart().getDiscount().doubleValue() * 100));

                    List<HeadsetTicketItem> ticketItems = new ArrayList<>();
                    List<OrderItem> cartItems = transaction.getCart().getItems();
                    for (OrderItem orderItem : cartItems) {
                        Product product = productHashMap.get(orderItem.getProductId());
                        HeadsetTicketItem ticketItem = new HeadsetTicketItem();
                        ticketItem.setProductId(orderItem.getProductId());

                        ticketItem.setInventoryId(Inventory.SAFE);
                        // Override if necessary
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            Inventory inventory = inventoryHashMap.get(quantityLog.getInventoryId());
                            if (inventory != null) {
                                ticketItem.setInventoryId(inventory.getName());
                                break;
                            }
                        }
                        ProductCategory category = null;
                        if (product != null) {
                            ticketItem.setName(product.getName());
                            category = categoryHashMap.get(product.getCategoryId());
                        }
                        ticketItem.setSubTotal((int) (orderItem.getCost().doubleValue() * 100));
                        double amountToTax = orderItem.getCost().doubleValue();
                        amountToTax -= orderItem.getDiscount().doubleValue();
                        double tax = amountToTax * transaction.getCart().getTax().doubleValue();
                        ticketItem.setTax((int) tax * 100);

                        //ticketItem.setTax((int)(orderItem.getTax().doubleValue()*100));
                        ticketItem.setDiscount((int) (orderItem.getDiscount().doubleValue() * 100));

                        ticketItem.setTotal((int) (orderItem.getFinalPrice().doubleValue() * 100));
                        ticketItem.setCost((int) (orderItem.getCost().doubleValue() * 100));


                        // Set quantity and weight accordingly
                        if (orderItem.getPrepackageItemId() == null) {
                            if (category != null && category.getUnitType() == ProductCategory.UnitType.grams) {
                                // grams type
                                ticketItem.setQuantity(1);
                                ticketItem.setWeight(orderItem.getQuantity());
                            } else {
                                // unit type
                                ticketItem.setQuantity(orderItem.getQuantity().intValue());
                                ticketItem.setWeight(new BigDecimal(1.0));
                            }
                        } else {
                            // this is a prepackage so look up the prepackage
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                            boolean found = false;
                            if (prepackageProductItem != null) {
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null) {
                                    ticketItem.setQuantity(orderItem.getQuantity().intValue());
                                    ticketItem.setWeight(prepackage.getUnitValue());
                                    found = true;
                                }
                            }
                            if (!found) {
                                ticketItem.setQuantity(1);
                                ticketItem.setWeight(orderItem.getQuantity());
                            }
                        }
                        ticketItems.add(ticketItem);
                    }
                    request.setItems(ticketItems);


                    headsetAPIManager.addSaleItem(headsetConfig, headsetLocation, request);

                    LOG.info("-- Add sale  {} [{}]  ${} ({})",
                            request.getUtcDate(),
                            request.getId(),
                            request.getTotal(),
                            request.getPaymentType());
                }
            }

            //NOW: UPDATE THE LAST SYNC
            try {
                headsetLocation.setLastSyncDate(nowTime.getMillis());
                headsetLocation.setProductLastSyncDate(nowTime.getMillis());
                headsetLocationRepository.update(headsetLocation.getCompanyId(), headsetLocation.getId(), headsetLocation);

                String updateLastConnection = headsetAPIService.updateLastConnection(headsetConfig, headsetLocation, nowTime.toString());

                LOG.info("Last update connection: " + updateLastConnection);
            } catch (Exception e) {
                LOG.error("Error updating last connection", e);
            }
            LOG.info("Employees transfer success: " + employeesSuccess);
            LOG.info("Employees transfer err: " + employeesErr);
            LOG.info("Members transfer success: " + membersSuccess);
            LOG.info("Members transfer err: " + membersErr);
            LOG.info("Products transfer success: " + productsSucccess);
            LOG.info("Products transfer err: " + productsErr);
            LOG.info("Batches transfer success: " + batchSuccess);
            LOG.info("Batches transfer err: " + batchErr);
            LOG.info("Transactions transfer success: " + transSuccess);
            LOG.info("Transactions transfer err: " + transErr);
            accountSynced++;
        }
        LOG.info("Account synced: " + accountSynced);
    }

}