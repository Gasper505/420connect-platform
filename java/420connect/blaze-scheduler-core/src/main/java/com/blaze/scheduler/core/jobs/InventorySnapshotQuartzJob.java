package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.util.DateUtil;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashMap;

public class InventorySnapshotQuartzJob implements Job {
    static final Logger LOGGER = LoggerFactory.getLogger(InventorySnapshotQuartzJob.class);

    @Inject
    private ShopRepository shopRepository;
    @Inject
    BackgroundTaskManager taskManager;
    @Inject
    CompanyRepository companyRepository;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Iterable<Shop> shops = shopRepository.listNonDeleted();

        HashMap<String, Company> companyHashMap = companyRepository.listAsMap();

        for (Shop shop : shops) {
            if (!shop.isActive()) {
                continue;
            }

            Company company = companyHashMap.get(shop.getCompanyId());
            if (company == null || !company.isActive()) {
                // company not active
                continue;
            }

            // last snap shot taken time
            long lastSnapShot = (shop.getSnapshopTime() == null) ? 0 : shop.getSnapshopTime();
            long nextSnapshotTime = (shop.getNextSnapshotTime() == null) ? 0 : shop.getNextSnapshotTime();

            String lastSnapShotStr = DateUtil.toDateFormatted(lastSnapShot, shop.getTimeZone());
            String nextSnapshotTimeStr = DateUtil.toDateFormatted(nextSnapshotTime, shop.getTimeZone());

            // Job takes snapshot at 11:50 else continue
            DateTime requiredDate = DateUtil.nowWithTimeZone(shop.getTimeZone()).withTimeAtStartOfDay().plusDays(1).plusMinutes(5);
            DateTime currentTime = DateUtil.nowWithTimeZone(shop.getTimeZone());
            String requiredDateStr = DateUtil.toDateFormatted(requiredDate.getMillis(), shop.getTimeZone());

            LOGGER.info("Time: " + DateUtil.nowUTC() + " " + DateUtil.nowUTC().getMillis());
            LOGGER.info("Next Snapshot Time: " + nextSnapshotTimeStr + " - " + nextSnapshotTime);
            LOGGER.info("Shop: " + shop.getName() + " - " + lastSnapShot);
            LOGGER.info("Last Sync Date: " + lastSnapShotStr + ", current date: " + requiredDateStr + " snapshotted: " + lastSnapShotStr.equalsIgnoreCase(requiredDateStr));
            LOGGER.info("Required Time " + requiredDate + " - " + requiredDate.getMillis());
            LOGGER.info("Current Time " + currentTime + " - " + currentTime.getMillis());


            LOGGER.info("Time: " + DateUtil.nowUTC() + " " + DateUtil.nowUTC().getMillis());
            if (DateUtil.getMinutesBetweenTwoDates(DateUtil.nowUTC().getMillis(), lastSnapShot) >= 15) {
                // has not taken a snapshot in the past 15 minutes
                if (currentTime.getMillis() < nextSnapshotTime) {
                    // not yet time to execute snapshot, continue
                    LOGGER.info("Not yet time, skipping...");
                    continue;
                }

                taskManager.takeInventorySnapshot(shop.getCompanyId(), shop.getId(), "", "Inventory Snapshot Job", true);
                shopRepository.updateNextSnapshoptTime(shop.getCompanyId(),shop.getId(),requiredDate.getMillis());
            } else {
                LOGGER.info("Interval too short, skipping...");
            }
        }
    }

}
