package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.services.thirdparty.LeaflyService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class LeaflyQuartzJob implements Job {
    static final Logger LOGGER = LoggerFactory.getLogger(LeaflyQuartzJob.class);

    @Inject
    private LeaflyService leaflyService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Starting Leafly quartz job");
        leaflyService.syncAllMenu();
    }
}
