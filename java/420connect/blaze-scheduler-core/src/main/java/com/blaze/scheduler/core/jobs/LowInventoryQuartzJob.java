package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.jobs.LowInventoryNotification;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashMap;


@SuppressWarnings("unchecked")
public class LowInventoryQuartzJob implements Job {

    static final Logger LOGGER = LoggerFactory.getLogger(LowInventoryQuartzJob.class);
    @Inject
    private LowInventoryNotification lowInventoryNotification;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ShopRepository shopRepository;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Sending email for low inventory notification");


        HashMap<String, Company> companyHashMap = companyRepository.listNonDeletedActiveAsMap();
        Iterable<Shop> shops = shopRepository.iterator();

        for (Shop shop : shops) {
            if (shop.isDeleted() || shop.isActive() == false) {
                continue;
            }
            Company company = companyHashMap.get(shop.getCompanyId());
            if (company == null || company.isDeleted() || company.isActive() == false) {
                continue;
            }
            LOGGER.info("Low-inventory for shop: " + shop.getName());
            lowInventoryNotification.dataForLowInventoryNotification(shop.getId(), true, null);
        }


    }
}
