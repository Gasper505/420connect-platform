package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyBounceNumber;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyBounceNumberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MarketingJobLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MarketingJobRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by mdo on 7/17/17.
 */
@DisallowConcurrentExecution
public class MarketingQuartzJob implements Job {
    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(MarketingQuartzJob.class);
    private static final String DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private static final String FILE_HEADERS = "MarketingJob Id,Company Id,Shop Id,Member Id,Date Sent,from Number,To Number,Status";

    @Inject
    private MarketingJobRepository marketingJobRepository;
    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    AppRepository appRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private CompanyBounceNumberRepository companyBounceNumberRepository;
    @Inject
    private MarketingJobLogRepository marketingJobLogRepository;
    @Inject
    private AmazonS3Service amazonS3Service;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOG.info("Executing Marketing Job");

        // TODO: Put this inside of a QueuedMarketingJob

        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        boolean fakeSMS = app.isFakeSMS();
        boolean fakeEmail = app.isFakeEmail();
        if (app == null || !app.isEnableSMS()) {
            // do not execute if sms is not enabled
            LOG.info("SMS is not enabled for app.");
            return;
        }
        LOG.info("Fake SMS: " + fakeSMS + ", fake Email: " + fakeEmail);

        DateTime now = DateTime.now();
        Iterable<MarketingJob> jobs = marketingJobRepository.listMarketingJobsPending(now.getMillis(), MarketingJob.MarketingType.Blasts);

        HashMap<String, Shop> shopHashSet = new HashMap<>();
        HashMap<String, Company> companyHashMap = new HashMap<>();
        if (jobs.iterator().hasNext()) {
            shopHashSet = shopRepository.listAsMap();
            companyHashMap = companyRepository.listAsMap();
        }

        int marketingJobSuccess = 0;
        int marketingJobFailed = 0;

        HashMap<String, StringBuilder> logBuilderMap = new HashMap<>();
        for (MarketingJob marketingJob : jobs) {
            // these should only be pending jobs

            StringBuilder builder = logBuilderMap.getOrDefault(marketingJob.getId(), new StringBuilder(FILE_HEADERS));
            boolean isLogCreated = false;

            Company company = companyHashMap.get(marketingJob.getCompanyId());
            if (company == null || !company.isActive()) {
                updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.Failed);
                marketingJobFailed++;
                continue;
            }

            Shop shop = shopHashSet.get(marketingJob.getShopId());
            if (shop == null || !shop.isActive()) {
                updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.Failed);
                marketingJobFailed++;
                continue;
            }

            final String fromNumber = amazonServiceManager.getFromNumber(app, shop, true);

            String cleanFromNumber = AmazonServiceManager.cleanPhoneNumber(fromNumber, shop);

            updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.InProcess);

            String address = String.format("%s, %s, %s %s", shop.getAddress().getAddress(),
                    shop.getAddress().getCity(), shop.getAddress().getState(), shop.getAddress().getZipCode());

            String message = marketingJob.getMessage();
            message = message.replaceAll(Pattern.quote("{{shop_name}}"), shop.getName());
            message = message.replaceAll(Pattern.quote("{{shop_website}}"), company.getWebsite());
            message = message.replaceAll(Pattern.quote("{{shop_address}}"), address);
            // Set this to In Progress

            Iterable<Member> members = getMembersTextOptIn(marketingJob);
            int smsSent = 0;
            int emailSent = 0;
            List<Member> membersList = Lists.newArrayList(members);

            if (membersList.size() > 0) {
                CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(company.getId());
                if (companyFeatures == null) {
                    continue;
                }


                List<MarketingJobLog> jobLogs = new ArrayList<>();


                HashMap<String, CompanyBounceNumber> companyBounceNumberMap = companyBounceNumberRepository.listAllByCompany(company.getId());


                int smsAailable = companyFeatures.getSmsMarketingCredits();
                int emailAvailable = companyFeatures.getEmailMarketingCredits();

                for (Member member : membersList) {
                    // Send sms

                    String newMessage = message + "";
                    String lastVisited = "";
                    if (member.getLastVisitDate() != null) {
                        lastVisited = DateUtil.toDateFormatted(member.getLastVisitDate(), shop.getTimeZone());
                    }

                    newMessage = newMessage.replaceAll(Pattern.quote("{{member_firstname}}"), member.getFirstName());
                    newMessage = newMessage.replaceAll(Pattern.quote("{{member_lastname}}"), member.getLastName());
                    newMessage = newMessage.replaceAll(Pattern.quote("{{member_lastvisited}}"), lastVisited);


                    if (marketingJob.getJobType() == MarketingJob.MarketingJobType.SMS
                            && StringUtils.isNotBlank(member.getPrimaryPhone())
                            && member.isTextOptIn()) {
                        if (smsAailable > smsSent) {
                            String phoneNumber = AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
                            CompanyBounceNumber bounceNumber = companyBounceNumberMap.get(phoneNumber);
                            if (fakeSMS == false && (bounceNumber == null || (bounceNumber.getStatus() == CompanyBounceNumber.BounceNumberStatus.OPT_IN))) {
                                // send sms message
                                boolean sentRequested = amazonServiceManager.sendMarketingSMSMessage(Lists.newArrayList(member.getPrimaryPhone()), newMessage, shop, fromNumber, app);

                                if (sentRequested) {
                                    smsSent++;
                                    if (marketingJob.getRecipientType() != MarketingJob.MarketingRecipientType.SpecificMembers) {
                                        MarketingJobLog jobLog = new MarketingJobLog();
                                        jobLog.prepare(marketingJob.getCompanyId());
                                        jobLog.setMarketingJobId(marketingJob.getId());
                                        jobLog.setStatus(MarketingJobLog.MSentStatus.SENT);
                                        jobLog.setFromNumber(cleanFromNumber);
                                        jobLog.setToNumber(phoneNumber);
                                        jobLog.setMemberId(member.getId());


                                        // add to log
                                        jobLogs.add(jobLog);
                                    } else {
                                        isLogCreated = true;
                                        builder.append(NEW_LINE_SEPARATOR);
                                        builder.append(marketingJob.getId()).append(DELIMITER);
                                        builder.append(marketingJob.getCompanyId()).append(DELIMITER);
                                        builder.append(marketingJob.getShopId()).append(DELIMITER);
                                        builder.append(member.getId()).append(DELIMITER);
                                        builder.append(DateTime.now().getMillis()).append(DELIMITER);
                                        builder.append(cleanFromNumber).append(DELIMITER);
                                        builder.append(phoneNumber).append(DELIMITER);
                                        builder.append(MarketingJobLog.MSentStatus.SENT).append(DELIMITER);
                                    }
                                }
                            }
                            if (bounceNumber != null && bounceNumber.getStatus() == CompanyBounceNumber.BounceNumberStatus.OPT_OUT) {
                                LOG.info("Member text optIn status: " + bounceNumber.getStatus());
                            }
                        }
                    }
                    /*else if (marketingJob.getJobType() == MarketingJob.MarketingJobType.Email
                            && BLStringUtils.isNotBlank(member.getEmail())
                            && member.isEmailOptIn()) {
                        if (emailAvailable > emailSent) {
                            if (fakeEmail == false) {
                                amazonServiceManager.sendEmail("support@blaze.me", member.getEmail(), "Hello from " + shop.getName(), newMessage);
                            }
                            emailSent++;
                        }
                    }*/
                }
                LOG.info("SMS Sent: " + smsSent);
                LOG.info("Email Sent: " + emailSent);
                smsAailable -= smsSent;
                emailAvailable -= emailSent;

                if (smsAailable != companyFeatures.getSmsMarketingCredits()
                        || emailAvailable != companyFeatures.getEmailMarketingCredits()) {
                    // there were changes to available credits, let's update
                    companyFeaturesRepository.subtractSMSEmailCredits(marketingJob.getCompanyId(), smsSent, emailSent);
                }

                if (!jobLogs.isEmpty()) {
                    // Persist Job Logs
                    marketingJobLogRepository.save(jobLogs);
                }
            }
            if (isLogCreated) {
                logBuilderMap.put(marketingJob.getId(), builder);
            }
            if (marketingJob.getJobType() == MarketingJob.MarketingJobType.SMS) {
                double cost = smsSent * app.getSmsCost().doubleValue() / 100;
                marketingJob.setSentCost(new BigDecimal(cost));
                marketingJob.setSentCount(smsSent);
            } else {
                double cost = smsSent * app.getEmailCost().doubleValue() / 100;
                marketingJob.setSentCost(new BigDecimal(cost));
                marketingJob.setSentCount(emailSent);
            }
            // Update Job status
            updateMarketingJob(marketingJob, MarketingJob.MarketingJobStatus.Completed);
            marketingJobSuccess++;
        }

        if (!logBuilderMap.isEmpty()) {
            for (String marketingJobId : logBuilderMap.keySet()) {
                uploadMarketingJobLogs(marketingJobId, logBuilderMap.get(marketingJobId));
            }
        }

        LOG.info("Marketing Job Success: " + marketingJobSuccess + ",  fails: " + marketingJobFailed);
    }


    private void updateMarketingJob(MarketingJob marketingJob, MarketingJob.MarketingJobStatus status) {
        marketingJob.setStatus(status);
        if (status == MarketingJob.MarketingJobStatus.Completed
                || status == MarketingJob.MarketingJobStatus.Failed) {
            marketingJob.setCompletedDate(DateTime.now().getMillis());
            marketingJob.setActive(false);
        }
        marketingJobRepository.update(marketingJob.getCompanyId(), marketingJob.getId(), marketingJob);
    }

    private Iterable<Member> getMembersTextOptIn(final MarketingJob marketingJob) {

        if (MarketingJob.MarketingRecipientType.AllMembers.equals(marketingJob.getRecipientType())) {
            // find all member for this company
            return memberRepository.listWithOptionsTextOptIn(marketingJob.getCompanyId(),
                    Member.MembershipStatus.Active,
                    "{firstName:1,lastName:1,primaryPhone:1,email:1,textOptIn:1,emailOptIn:1}");

        } else if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.InactiveMembers) {

            DateTime daysOld = DateTime.now().withTimeAtStartOfDay().minusDays(marketingJob.getInactiveDays());
            return memberRepository.getInactiveMembersWithDaysTextOptIn(marketingJob.getCompanyId(), daysOld.getMillis());

        } else if (MarketingJob.MarketingRecipientType.MemberGroup.equals(marketingJob.getRecipientType())) {

            return memberRepository.getMemberByMemberGroupsTextOptIn(marketingJob.getCompanyId(),
                    marketingJob.getMemberGroupIds());

        } else if (MarketingJob.MarketingRecipientType.MembersSpentXDollarsInYDateRange.equals(marketingJob.getRecipientType())) {

            //Get transaction in date range (start date and end date)

            Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(marketingJob.getCompanyId(), marketingJob.getShopId(), marketingJob.getStartDate(), marketingJob.getEndDate());

            Map<String, BigDecimal> membersTotalAmountMap = new HashMap<>();

            Set<ObjectId> memberIds = new HashSet<>();
            double amt = NumberUtils.round(marketingJob.getAmountDollars(), 2);
            for (Transaction transaction : transactions) {

                BigDecimal total = membersTotalAmountMap.getOrDefault(transaction.getMemberId(), new BigDecimal(0));
                total = total.add(transaction.getCart().getTotal());
                membersTotalAmountMap.put(transaction.getMemberId(), total);

                if (NumberUtils.round(total, 2) >= amt) {
                    memberIds.add(new ObjectId(transaction.getMemberId()));
                }
            }


            return memberRepository.listByTextOptIn(marketingJob.getCompanyId(), Lists.newArrayList(memberIds));
        } else if (MarketingJob.MarketingRecipientType.MembersHadTransactionsBetweenDateRange.equals(marketingJob.getRecipientType())) {

            Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(marketingJob.getCompanyId(), marketingJob.getShopId(), marketingJob.getStartDate(), marketingJob.getEndDate());

            Set<ObjectId> memberIds = new HashSet<>();
            for (Transaction transaction : transactions) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }

            //Get members who are text opt in
            return memberRepository.listByTextOptIn(marketingJob.getCompanyId(), Lists.newArrayList(memberIds));
        } else if (MarketingJob.MarketingRecipientType.MembersBoughtProductsFromVendorXInYDateRange.equals(marketingJob.getRecipientType())) {

            Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(marketingJob.getCompanyId(), marketingJob.getShopId(), marketingJob.getStartDate(), marketingJob.getEndDate());

            Set<ObjectId> memberIds = new HashSet<>();
            HashMap<String, Product> productHashMap = productRepository.listAllAsMap(marketingJob.getCompanyId(), marketingJob.getShopId());

            for (Transaction transaction : transactions) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    Product product = productHashMap.get(orderItem.getProductId());

                    if (product != null && product.getVendorId().equals(marketingJob.getVendorId())) {
                        memberIds.add(new ObjectId(transaction.getMemberId()));
                    }
                }
            }

            return memberRepository.listByTextOptIn(marketingJob.getCompanyId(), Lists.newArrayList(memberIds));
        } else if (MarketingJob.MarketingRecipientType.MembersBoughtXCategoryInLastYDateRange.equals(marketingJob.getRecipientType())) {
            final String categoryId = marketingJob.getCategoryId();

            System.out.println("1: " + DateTime.now());
            Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(marketingJob.getCompanyId(), marketingJob.getShopId(), marketingJob.getStartDate(), marketingJob.getEndDate());

            System.out.println("2: " + DateTime.now());
            HashMap<String, Product> productHashMap = productRepository.listAllAsMap(marketingJob.getCompanyId(), marketingJob.getShopId());

            System.out.println("3: " + DateTime.now());
            Set<ObjectId> memberIds = new HashSet<>();
            for (Transaction transaction : transactions) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product != null && product.getCategoryId().equals(categoryId)) {
                        memberIds.add(new ObjectId(transaction.getMemberId()));
                    }
                }
            }
            System.out.println("4: " + DateTime.now());

            return memberRepository.listByTextOptIn(marketingJob.getCompanyId(), Lists.newArrayList(memberIds));
        } else if (MarketingJob.MarketingRecipientType.MembersVisitedXTimesInYDateRange.equals(marketingJob.getRecipientType())) {

            Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(marketingJob.getCompanyId(), marketingJob.getShopId(), marketingJob.getStartDate(), marketingJob.getEndDate());

            Map<String, Integer> memberVisits = new HashMap<>();

            Set<ObjectId> memberIds = new HashSet<>();
            int noOfVisits = marketingJob.getNoOfVisits();
            for (Transaction transaction : transactions) {

                Integer visits = memberVisits.getOrDefault(transaction.getMemberId(), 0);
                visits++;
                memberVisits.put(transaction.getMemberId(), visits);

                if (visits >= noOfVisits) {
                    memberIds.add(new ObjectId(transaction.getMemberId()));
                }

            }

            return memberRepository.listByTextOptIn(marketingJob.getCompanyId(), Lists.newArrayList(memberIds));
        } else if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.SpecificMembers) {
            String keyName = marketingJob.getMemberAsset().getKey();
            AssetStreamResult result = amazonS3Service.downloadFile(keyName, true);
            Set<ObjectId> memberIds = new HashSet<>();
            if (result != null && result.getStream() != null) {
                CSVParser parser = null;
                try {
                    Reader in = new InputStreamReader(result.getStream());
                    CSVFormat format = CSVFormat.EXCEL.withHeader().withSkipHeaderRecord(true);
                    parser = new CSVParser(in, format);
                    for (CSVRecord r : parser) {
                        String entity = r.get("Member Id");
                        if (StringUtils.isNotBlank(entity) && ObjectId.isValid(entity)) {
                            memberIds.add(new ObjectId(entity));
                        }
                    }
                } catch (IOException e) {
                    //add log Exception in member csv read
                    return null;
                }
            }
            return memberRepository.listByTextOptIn(marketingJob.getCompanyId(), Lists.newArrayList(memberIds));
        } else if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.MembersWithSpecificTags) {
            if (!CollectionUtils.isEmpty(marketingJob.getMemberTags())) {
                return memberRepository.getMemberTextOptInByTags(marketingJob.getCompanyId(), Lists.newArrayList(marketingJob.getMemberTags()));
            }
        }

        return null;
    }

    private void uploadMarketingJobLogs(String keyName, StringBuilder builder) {
        try {
            keyName = keyName + "_" + DateTime.now().getMillis();
            File file = stringToFile(builder, keyName);
            String key = amazonS3Service.uploadFile(file, keyName, ".csv");
            LOG.info("Marketing Job Log file uploaded, key : " + key);
        } catch (Exception e) {
            LOG.error("Unable to upload log file", e);
        }

    }

    private File stringToFile(StringBuilder builder, String keyName) throws IOException {
        final File tempFile = File.createTempFile(keyName, ".csv");
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(IOUtils.toInputStream(builder.toString()), out);
        }
        return tempFile;

    }
}
