package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by decipher on 18/11/17 4:53 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@DisallowConcurrentExecution
public class MemberBirthdayQuartzJob implements Job {
    static final Log LOGGER = LogFactory.getLog(MemberBirthdayQuartzJob.class);
    @Inject
    MemberRepository memberRepository;
    @Inject
    AmazonServiceManager amazonServiceManager;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap();
        Set<String> memberKeys = memberHashMap.keySet();
        List<String> phoneNumbers = new ArrayList<>();

        /*String message;
        String memberName;
        LocalDate localDate = new LocalDate();
        for (String memberKey : memberKeys){
            Member member = memberHashMap.get(memberKey);
            if (!member.isDeleted() && member.getDob() != null && member.getPrimaryPhone() != null){
                LocalDate memberBirthday = new LocalDate(member.getDob());

                if (memberBirthday.getMonthOfYear() == localDate.getMonthOfYear() && memberBirthday.getDayOfMonth() == localDate.getDayOfMonth()){

                    memberName = member.getFirstName() + " " + (member.getLastName() != null ? member.getLastName() : "");
                    message = "Happy Birthday "+ memberName+ " from team of Blaze";
                    phoneNumbers.add(member.getPrimaryPhone());
                    amazonServiceManager.sendMarketingSMSMessage(phoneNumbers, message);
                }
            }
        }
        */
    }
}
