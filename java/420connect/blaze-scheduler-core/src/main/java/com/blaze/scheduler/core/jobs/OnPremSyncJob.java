package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.services.onprem.impl.OnPremSyncServiceImpl;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

@DisallowConcurrentExecution
public class OnPremSyncJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnPremSyncJob.class);
    @Inject
    private OnPremSyncServiceImpl onCloudAPIService;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        onCloudAPIService.syncWithCloud();
    }
}
