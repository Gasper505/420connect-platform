package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.report.PreCalculateReportEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.inject.Inject;

public class PreCalculateReportingQuartzJob implements Job {

    private static final Log LOG = LogFactory.getLog(PreCalculateReportingQuartzJob.class);

    @Inject
    private BlazeEventBus blazeEventBus;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOG.info("PreCalculateReportingQuartzJob started");

        PreCalculateReportEvent reportEvent = new PreCalculateReportEvent();
        reportEvent.setTriggerTime(DateTime.now().getMillis());

        blazeEventBus.post(reportEvent);

        LOG.info("PreCalculateReportingQuartzJob ended");
    }


}
