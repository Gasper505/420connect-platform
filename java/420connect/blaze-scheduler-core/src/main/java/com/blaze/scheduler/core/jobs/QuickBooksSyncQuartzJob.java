package com.blaze.scheduler.core.jobs;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.inject.Inject;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import javax.inject.Inject;

/**
 * Created by mdo on 7/17/17.
 */
@DisallowConcurrentExecution
public class QuickBooksSyncQuartzJob implements Job {


    @Inject
    private QuickbookSync quickbookSync;
    @Inject
    private AppRepository appRepository;

    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(QuickBooksSyncQuartzJob.class);


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("QuickBooksSyncQuartzJob");
        LOG.info("QuickBooksSyncQuartzJob");
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app.isEnableQBDesktop() == false) {
            LOG.info("Quickbook Desktop is not enabled.");
            return;
        }

        quickbookSync.syncCustomQuickbookEntities();


    }

}