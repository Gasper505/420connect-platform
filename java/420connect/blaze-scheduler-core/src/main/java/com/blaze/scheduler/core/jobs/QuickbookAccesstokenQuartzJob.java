package com.blaze.scheduler.core.jobs;

import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.inject.Inject;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mdo on 7/17/17.
 */
@DisallowConcurrentExecution
public class QuickbookAccesstokenQuartzJob implements Job {


    @Inject
    private QuickbookSync quickbookSync;

    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(QuickbookAccesstokenQuartzJob.class);


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {


        System.out.println("QuickbookAccesstokenQuartzJob");
        LOG.info("QuickbookAccesstokenQuartzJob");
        //quickbookSync.getAccestoken();


    }

}