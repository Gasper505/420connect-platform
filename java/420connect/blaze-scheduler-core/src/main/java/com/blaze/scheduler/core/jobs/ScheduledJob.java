package com.blaze.scheduler.core.jobs;

import com.blaze.scheduler.core.services.scheduler.QuartzService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by mdo on 6/20/17.
 */
public class ScheduledJob implements Runnable {
    @Inject
    QuartzService quartzService;

    // Properties
    private String companyId;
    private String shopId;
    private String message;
    private List<String> phoneNumbers;

    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(ScheduledJob.class);


    @Override
    public void run() {

        LOG.info("Received job");

        // Execute the message
        // call sms to schedule the job with quartz
        quartzService.scheduleSendToAllMembers(DateTime.now().getMillis(), "myjob", "mytrigger", "companyId", "this is the message", "topicName");
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public QuartzService getQuartzService() {
        return quartzService;
    }

    public void setQuartzService(QuartzService quartzService) {
        this.quartzService = quartzService;
    }
}
