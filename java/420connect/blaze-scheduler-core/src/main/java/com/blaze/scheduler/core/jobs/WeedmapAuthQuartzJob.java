package com.blaze.scheduler.core.jobs;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapAccountRepository;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class WeedmapAuthQuartzJob implements Job {
    static final Logger LOGGER = LoggerFactory.getLogger(WeedmapAuthQuartzJob.class);

    @Inject
    private WeedmapAccountRepository weedmapAccountRepository;
    @Inject
    private BackgroundTaskManager taskManager;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Starting weed map authentication quartz job");

        Iterable<WeedmapAccount> weedmapAccounts = weedmapAccountRepository.list();
        WeedmapConfig prodEnvironment = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);

        for (WeedmapAccount account : weedmapAccounts) {
            if (!account.isActive() || account.isDeleted()) {
                LOGGER.info(String.format("Account not active/delete/not for version 2 for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
                continue;
            }

            if (CollectionUtils.isNullOrEmpty(account.getApiKeyList())) {
                LOGGER.info(String.format("Api key list is empty for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
                continue;
            }

            boolean isUpdated = false;

            for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
                if (apiKeyMap.getVersion() != WeedmapAccount.Version.V2) {
                    LOGGER.info(String.format("Api key map not for V2 companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
                    continue;
                }

                if (StringUtils.isBlank(apiKeyMap.getToken())) {
                    LOGGER.info(String.format("Account not authorized for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
                    continue;
                }
                WeedmapConfig config =  prodEnvironment;
                DateTime expiredTime = new DateTime(apiKeyMap.getExpiresIn());


                if (expiredTime.isBeforeNow()) {
                    LOGGER.info(String.format("Current time: %s, Expired Time: %s", DateTime.now(), expiredTime));

                    LOGGER.info(String.format("Token expired for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));

                    LOGGER.info(String.format("Renew token for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));

                    WeedmapTokenType type = new WeedmapTokenType();
                    type.setType(WeedmapTokenType.TokenType.REFRESHTOKEN);
                    taskManager.authenticateWeedmapToken(account.getCompanyId(), account.getShopId(), account, type, true, config);

                    isUpdated = true;
                }
            }

            if (isUpdated) {
                weedmapAccountRepository.update(account.getId(), account);
            }
        }
    }
}
