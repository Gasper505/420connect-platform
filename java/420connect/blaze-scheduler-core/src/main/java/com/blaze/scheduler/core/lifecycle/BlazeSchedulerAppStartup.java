package com.blaze.scheduler.core.lifecycle;

import com.blaze.scheduler.core.jobs.AutomationMarketingQuartzJob;
import com.blaze.scheduler.core.jobs.MarketingQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import com.fourtwenty.core.lifecycle.AppStartup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;
import java.util.Set;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by mdo on 7/14/17.
 */
public class BlazeSchedulerAppStartup implements AppStartup {
    private static final int MARKETING_JOB_INTERVAL_SEC = 10; // 10 SECONDS
    @Inject
    private SchedulerQuartzManager quartzManager;
    @Inject
    Set<QuartzStartupTask> quartzStartupTaskSet;

    @Inject
    private ConnectConfiguration connectConfiguration;


    private static final Log LOG = LogFactory.getLog(BlazeSchedulerAppStartup.class);

    @Override
    public void run() {


        LOG.info("BlazeSchedulerAppStartup is starting up..");
        for (QuartzStartupTask task : quartzStartupTaskSet) {
            LOG.info(String.format("Starting up '%s'", task.getName()));
            task.initiateStartup();
        }

        startSMSJob();
        startAutomatedJob();
    }

    private void startSMSJob() {
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }
        final String jobName = "MarketingNotificationJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "MarketingNotificationTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "MarketingNotification";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(MarketingQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(MARKETING_JOB_INTERVAL_SEC)).build();
        quartzManager.scheduleJob(job, trigger);
    }

    private void startAutomatedJob() {
        final String jobName = "AutomatedMarketingNotificationJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "AutomatedMarketingNotificationTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "AutomatedMarketingNotification";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(AutomationMarketingQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        //It will fire at 12pm (noon) every day
        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(cronSchedule("0 0 12 * * ?")).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
