package com.blaze.scheduler.core.managed;

import com.fourtwenty.core.domain.models.common.HeadsetConfig;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.*;
import com.fourtwenty.core.thirdparty.headset.HeadsetAPIService;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mdo on 7/24/17.
 */
public class HeadsetAPIManager implements Managed {
    private static final Log LOG = LogFactory.getLog(HeadsetAPIManager.class);
    @Inject
    HeadsetAPIService headsetAPIService;

    ExecutorService executorService;

    @Override
    public void start() throws Exception {
        LOG.debug("Starting HeadsetAPIManager");
        initialize();
    }

    private void initialize() {
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(8);
        }
    }

    @Override
    public void stop() throws Exception {
        if (executorService != null) {
            executorService.shutdown();
        }
    }


    public void addBudTender(final HeadsetConfig config, final HeadsetLocation location, final BudTenderAddRequest request) {
        initialize();

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    headsetAPIService.addBudTender(config, location, request);
                } catch (Exception e) {
                    String json = JsonSerializer.toJson(request);
                    LOG.info("Object: " + json);
                    LOG.error(String.format("Error transferring employee: %s %s", request.getFirstName(), request.getLastName()), e);
                }
            }
        });
    }

    public void addCustomer(final HeadsetConfig config, final HeadsetLocation location, final HeadsetCustomerAddRequest request) {
        initialize();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    headsetAPIService.addCustomer(config, location, request);
                } catch (Exception e) {
                    String json = JsonSerializer.toJson(request);
                    LOG.info("Object: " + json);
                    LOG.error(String.format("Error transferring member: %s", request.getFirstName()), e);
                }
            }
        });
    }

    public void addProduct(final HeadsetConfig config, final HeadsetLocation location, final HeadsetProductAddRequest request) {
        initialize();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    headsetAPIService.addProduct(config, location, request);
                } catch (Exception e) {
                    String json = JsonSerializer.toJson(request);
                    LOG.info("Object: " + json);
                    LOG.error(String.format("Error transferring product: %s", request.getName()), e);
                }
            }
        });
    }

    public void addSaleItem(final HeadsetConfig config, final HeadsetLocation location, final TicketAddRequest request) {
        initialize();
        String json = JsonSerializer.toJson(request);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String json = JsonSerializer.toJson(request);
                    headsetAPIService.addSaleItem(config, location, request);
                } catch (Exception e) {
                    String json = JsonSerializer.toJson(request);
                    LOG.info("Object: " + json);
                    LOG.error(String.format("Error transferring transaction: %s", request.getId()), e);
                }
            }
        });
    }

    public void restock(final HeadsetConfig config, final HeadsetLocation location, final RestockAddRequest request) {
        String json = JsonSerializer.toJson(request);
        initialize();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String json = JsonSerializer.toJson(request);
                    //mdo: Only retrieve employees that are greater than lastSync
                    headsetAPIService.restock(config, location, request);
                } catch (Exception e) {
                    String json = JsonSerializer.toJson(request);
                    LOG.info("Object: " + json);
                    LOG.error(String.format("Error transferring batch: %s", request.getUtcDate()), e);
                }
            }
        });
    }


}
