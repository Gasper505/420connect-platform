package com.blaze.scheduler.core.managed;

import com.blaze.scheduler.core.jobs.GuiceJobFactory;
import com.blaze.scheduler.core.services.scheduler.QuartzProperties;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by mdo on 6/22/17.
 */
@Singleton
public class SchedulerQuartzManager implements Managed {
    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(SchedulerQuartzManager.class);
    @Inject
    Injector injector;
    @Inject
    ConnectConfiguration configuration;
    Scheduler scheduler;

    QuartzProperties quartzProperties = new QuartzProperties();

    @Override
    public void start() throws Exception {
        if (configuration.getQuartzConfiguration().isEnable()) {
            LOG.info("Starting quartz scheduler..");
            if (scheduler == null) {
                try {

                    SchedulerFactory stdSchedulerFactory = new StdSchedulerFactory(quartzProperties.getProperties(configuration));
                    scheduler = stdSchedulerFactory.getScheduler();

                    scheduler.setJobFactory(injector.getInstance(GuiceJobFactory.class));

                    scheduler.clear();
                    scheduler.start();
                } catch (Exception e) {
                    LOG.error("Error starting quartz scheduler", e);
                }
            }
        } else {
            LOG.info("Quartz is not enabled.");
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.info("Stopping quartz scheduler..");
        if (scheduler != null) {
            scheduler.shutdown();
            scheduler = null;
        }
    }

    public void scheduleJob(JobDetail jobDetail, Trigger trigger) {
        if (scheduler == null) {
            LOG.info("Quartz Scheduler is not enabled.");
            return;
        }
        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            LOG.error("Error scheduling job", e);
        }
    }
}
