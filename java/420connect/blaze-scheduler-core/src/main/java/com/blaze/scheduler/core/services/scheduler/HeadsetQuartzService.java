package com.blaze.scheduler.core.services.scheduler;

/**
 * Created by Gaurav Saini on 10/7/17.
 */
public interface HeadsetQuartzService {

    void getHeadsetAccount(String jobName, String triggerName);

}
