package com.blaze.scheduler.core.services.scheduler;

import com.fourtwenty.core.config.ConnectConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Properties;

/**
 * Created by Gaurav Saini on 16/6/17.
 */
public final class QuartzProperties {
    private static final Log LOG = LogFactory.getLog(QuartzProperties.class);


    public Properties getProperties(ConnectConfiguration connectConfiguration) {
        Properties prop = new Properties();
        System.out.println("=========<<<<<<<<<<<<==================Need to read all value from yml. Will Do this later====================================================");
        //RMI configuration to make the client to connect to the Quartz server
        prop.put("org.quartz.scheduler.rmi.export", "true");
        prop.put("org.quartz.scheduler.rmi.createRegistry", "true");
        prop.put("org.quartz.scheduler.rmi.registryHost", "localhost");
        prop.put("org.quartz.scheduler.rmi.registryPort", "1099");
        prop.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        prop.put("org.quartz.threadPool.threadCount", "10");

        //Quartz Server Properties
        prop.put("quartz.scheduler.instanceName", "ServerScheduler");
        prop.put("org.quartz.scheduler.instanceId", "AUTO");
        prop.put("org.quartz.scheduler.skipUpdateCheck", "true");
        prop.put("org.quartz.scheduler.jobFactory.class", "org.quartz.simpl.SimpleJobFactory");
        prop.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
        prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
        prop.put("org.quartz.jobStore.dataSource", "quartzDataSource");
        prop.put("org.quartz.jobStore.tablePrefix", connectConfiguration.getQuartzConfiguration().getTablePrefix());
        prop.put("org.quartz.jobStore.isClustered", "true");

        //MYSQL DATABASE CONFIGURATION
        //If we do not specify this configuration, QUARTZ will use RAM(in-memory) to store jobs
        //Once we restart QUARTZ, the jobs will not be persisted
        prop.put("org.quartz.dataSource.quartzDataSource.driver", connectConfiguration.getQuartzConfiguration().getDriver());
        prop.put("org.quartz.dataSource.quartzDataSource.URL", connectConfiguration.getQuartzConfiguration().getHost());
        prop.put("org.quartz.dataSource.quartzDataSource.user", connectConfiguration.getQuartzConfiguration().getUsername());
        prop.put("org.quartz.dataSource.quartzDataSource.password", connectConfiguration.getQuartzConfiguration().getPassword());
        prop.put("org.quartz.dataSource.quartzDataSource.maxConnections", connectConfiguration.getQuartzConfiguration().getMaxConnections());


        LOG.info("Quartz DB Driver: " + connectConfiguration.getQuartzConfiguration().getDriver());
        LOG.info("Quartz Scheduler DB: " + connectConfiguration.getQuartzConfiguration().getHost());

        return prop;
    }
}
