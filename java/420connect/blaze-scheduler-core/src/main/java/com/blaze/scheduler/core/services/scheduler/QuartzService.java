package com.blaze.scheduler.core.services.scheduler;


/**
 * Created by Gaurav Saini on 14/6/17.
 */
public interface QuartzService {
    void scheduleSendToSpecificMemberGroups(Long startDate, String jobName, String triggerName, String companyId, String message, String memberGroupId, String topicName);

    void scheduleSendToAllMembers(Long startDate, String jobName, String triggerName, String companyId, String message, String topicName);

    void scheduleSendToInActiveMembers(Long startDate, String jobName, String triggerName, String companyId, String message, Long noOfDays, String topicName);
}
