package com.blaze.scheduler.core.services.scheduler;

/**
 * Created by mdo on 12/7/17.
 */
public interface QuartzStartupTask {
    String getName();

    void initiateStartup();
}
