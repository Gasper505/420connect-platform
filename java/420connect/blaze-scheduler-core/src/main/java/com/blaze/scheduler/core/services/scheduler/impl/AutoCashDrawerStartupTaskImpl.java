package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.AutoCashDrawerQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by mdo on 12/7/17.
 */
public class AutoCashDrawerStartupTaskImpl implements QuartzStartupTask {
    private static final int LOW_INVENTORY_JOB_INTERVAL_SEC = 300; //3600; // 3 HRS(3600 SECONDS)

    @Inject
    private SchedulerQuartzManager quartzManager;

    @Override
    public String getName() {
        return "Auto CashDrawer Startup Task";
    }

    @Inject
    private ConnectConfiguration configuration;

    @Override
    public void initiateStartup() {
        // Only do if it's not on-prem
        if (configuration.getPlatformModeConfiguration() != null
                && configuration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "AutoCashDrawerJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "AutoCashDrawerTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "AutoCashDrawer";

        JobDataMap jobData = new JobDataMap();

        //Create Job
        JobDetail job = newJob(AutoCashDrawerQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(LOW_INVENTORY_JOB_INTERVAL_SEC)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
