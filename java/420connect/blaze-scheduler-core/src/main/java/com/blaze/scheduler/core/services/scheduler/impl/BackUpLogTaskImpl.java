package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.BackupLogJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

public class BackUpLogTaskImpl implements QuartzStartupTask {

    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration configuration;

    @Override
    public String getName() {
        return "Backup audit and product log at amazon s3 Task";
    }

    @Override
    public void initiateStartup() {

        // Only do if it's not on-prem
        if (configuration.getPlatformModeConfiguration() != null
                && configuration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "BackupLogNotificationJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "BackupLogNotificationTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "BackupLogNotification";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(BackupLogJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(0, 0)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
