package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.PreCalculateReportingQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class CalculateReportDataJobImpl implements QuartzStartupTask {

    private static final int JOB_INTERVAL_SEC = 3600;

    @Inject
    private SchedulerQuartzManager quartzManager;
    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "Calculate report data";
    }

    @Override
    public void initiateStartup() {

        if (!connectConfiguration.isEnableReportingCache())
            return;


        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "CalculateReportData".concat(String.valueOf(DateTime.now()));
        final String triggerName = "CalculateReportDataTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "CalculateReportData";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(PreCalculateReportingQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(JOB_INTERVAL_SEC)).build();
        quartzManager.scheduleJob(job, trigger);

    }
}
