package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.CloverQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class CloverStartUpTaskImpl implements QuartzStartupTask {
    private static final int CLOVER_INTERVAL = 300; // 5 minutes
    @Inject
    private SchedulerQuartzManager quartzManager;
    @Override
    public String getName() {
        return "Clover Startup Task";
    }
    @Inject
    private ConnectConfiguration configuration;

    @Override
    public void initiateStartup() {

        // Only do this on-prem
        if (configuration.getPlatformModeConfiguration() != null
                && configuration.getPlatformModeConfiguration().getMode() != PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }


        final String jobName = "CloverPaymentJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "CloverPaymentTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "CloverPayment";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(CloverQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();
        // Check every 5 minutes
        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(CLOVER_INTERVAL)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
