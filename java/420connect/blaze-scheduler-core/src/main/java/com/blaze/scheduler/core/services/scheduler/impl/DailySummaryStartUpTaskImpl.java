package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.DailySummaryQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class DailySummaryStartUpTaskImpl implements QuartzStartupTask {
    private static final String TIME_ZONE = "America/Los_Angeles";
    private static final int DAILY_SUMMARY_INTERVAL = 300; // 5 minutes
    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "Daily Summary Startup Task";
    }

    @Override
    public void initiateStartup() {
        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "DailySummaryDetailsJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "DailySummaryDetailsTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "DailySummaryDetails";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(DailySummaryQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();
        //TimeZone timeZone = TimeZone.getTimeZone(TIME_ZONE);

        /*CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.dailyAtHourAndMinute(02, 00).inTimeZone(timeZone);
        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup).withSchedule(cronScheduleBuilder).build();
        quartzManager.scheduleJob(job, trigger);*/


        // Check every 15 minutes
        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(DAILY_SUMMARY_INTERVAL)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
