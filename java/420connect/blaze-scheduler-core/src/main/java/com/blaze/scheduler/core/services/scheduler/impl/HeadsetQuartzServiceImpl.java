package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.HeadsetTransferDataQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.HeadsetQuartzService;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;
import java.util.List;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by Gaurav Sani on 10/7/17.
 */
public class HeadsetQuartzServiceImpl implements HeadsetQuartzService {
    private static final String JOB_GROUP = "HeadsetTransfer";
    private static final int JOB_INTERVAL = 600; // 10 minutes
    @Inject
    SchedulerQuartzManager quartzManager;
    @Inject
    IntegrationSettingRepository integrationSettingRepository;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public void getHeadsetAccount(String jobName, String triggerName) {
        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(HeadsetTransferDataQuartzJob.class)
                .withIdentity(jobName, JOB_GROUP)
                .setJobData(jobData)
                .build();

        // Get the lowest SYNC_INTERVAL
        List<IntegrationSetting> settings = integrationSettingRepository.getIntegrationSettings(
                IntegrationSettingConstants.Integrations.Headset.NAME);

        int syncInterval = settings
                .stream()
                .filter(s -> IntegrationSettingConstants.Integrations.Headset.SYNC_INTERVAL.equals(s.getKey()))
                .mapToInt(s -> s.getValueInt())
                .sorted()
                .findFirst()
                .orElse(300);

        Trigger trigger = newTrigger().withIdentity(triggerName, JOB_GROUP)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(syncInterval)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
