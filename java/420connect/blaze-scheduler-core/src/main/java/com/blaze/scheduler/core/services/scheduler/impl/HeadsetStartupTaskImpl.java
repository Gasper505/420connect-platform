package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.services.scheduler.HeadsetQuartzService;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import com.google.inject.Inject;
import org.joda.time.DateTime;

/**
 * Created by Gaurav Saini on 17/7/17.
 */
public class HeadsetStartupTaskImpl implements QuartzStartupTask {

    @Inject
    HeadsetQuartzService headsetQuartzService;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "Headset Startup Task";
    }

    @Override
    public void initiateStartup() {
        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        String jobName = "HeadsetJob".concat(String.valueOf(DateTime.now()));
        String triggerName = "HeadsetTrigger".concat(String.valueOf(DateTime.now()));

        headsetQuartzService.getHeadsetAccount(jobName, triggerName);
    }
}
