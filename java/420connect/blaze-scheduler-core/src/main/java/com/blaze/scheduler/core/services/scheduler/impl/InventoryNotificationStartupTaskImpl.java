package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.LowInventoryQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class InventoryNotificationStartupTaskImpl implements QuartzStartupTask {
    private static final int LOW_INVENTORY_JOB_INTERVAL_SEC = 86400; // 24 HRS(86400 SECONDS)
    @Inject
    private SchedulerQuartzManager quartzManager;

    @Override
    public String getName() {
        return "Low Inventory Startup Task";
    }

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public void initiateStartup() {
        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "LowInventoryNotificationJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "LowInventoryNotificationTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "LowInventoryNotification";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(LowInventoryQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(LOW_INVENTORY_JOB_INTERVAL_SEC)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
