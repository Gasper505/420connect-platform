package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.InventorySnapshotQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class InventorySnapshotStartUpTaskImpl implements QuartzStartupTask {
    private static final int Inventory_Snapshot_Interval = 300; // 5 minutes
    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "Inventory Snapshot Startup Task";
    }

    @Override
    public void initiateStartup() {
        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "InventorySnapshotJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "InventorySnapshotTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "InventorySnapshotTask";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(InventorySnapshotQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        // Check every 15 minutes
        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(Inventory_Snapshot_Interval)).build();
        quartzManager.scheduleJob(job, trigger);

    }
}
