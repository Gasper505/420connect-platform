package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.LeaflyQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import com.fourtwenty.core.services.thirdparty.impl.LeaflyConstants;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import java.util.TimeZone;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class LeaflyQuartzJobStartupTaskImpl implements QuartzStartupTask {
    //private static final int JOB_INTERVAL_SECONDS = 86400; //24 HRS
    private static final int JOB_INTERVAL_SECONDS = 60; //3 minute
    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "LeaflySync Quartz Job Startup Task";
    }

    @Override
    public void initiateStartup() {
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = LeaflyConstants.LEAFLY_SYNC_JOB.concat(String.valueOf(DateTime.now()));
        final String triggerName = LeaflyConstants.LEAFLY_SYNC_QUARTZ_TRIGGER.concat(String.valueOf(DateTime.now()));
        final String jobGroup = LeaflyConstants.LEAFLY_SYNC;
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(LeaflyQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();
        TimeZone timeZone = TimeZone.getTimeZone(LeaflyConstants.TIME_ZONE);
        /*Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup).withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(0, 0).inTimeZone(timeZone)).build();
        quartzManager.scheduleJob(job, trigger);*/

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(JOB_INTERVAL_SECONDS)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
