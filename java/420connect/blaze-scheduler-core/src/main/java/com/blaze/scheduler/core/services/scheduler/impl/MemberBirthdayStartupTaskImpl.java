package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.MemberBirthdayQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by decipher on 18/11/17 5:10 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class MemberBirthdayStartupTaskImpl implements QuartzStartupTask {
    private static final int MEMBER_BIRTHDAY_JOB_INTERVAL_SEC = 86400;
    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "Birthday Startup Task";
    }

    @Override
    public void initiateStartup() {
        // Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "MemberBirthdayJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "MemberBirthdayTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "MemberBirthday";

        JobDataMap jobData = new JobDataMap();

        //Create Job
        JobDetail job = newJob(MemberBirthdayQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(MEMBER_BIRTHDAY_JOB_INTERVAL_SEC)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
