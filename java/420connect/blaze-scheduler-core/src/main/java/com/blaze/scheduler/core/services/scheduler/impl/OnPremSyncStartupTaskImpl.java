package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.OnPremSyncJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class OnPremSyncStartupTaskImpl implements QuartzStartupTask {
    private static final int JOB_INTERVAL_SEC = 30; // 1 HR(3600 SECONDS)
    private static final String JOB_NAME = "On-Prem Sync";

    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration configuration;

    @Override
    public String getName() {
        return JOB_NAME;
    }

    @Override
    public void initiateStartup() {
// Only do this on-prem
        if (configuration.getPlatformModeConfiguration() != null
                && configuration.getPlatformModeConfiguration().getMode() != PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "OnPremSyncJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "OnPremSyncJob".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "OnPremSyncJob";

        JobDataMap jobData = new JobDataMap();

        //Create Job
        JobDetail job = newJob(OnPremSyncJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();


        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(JOB_INTERVAL_SEC)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
