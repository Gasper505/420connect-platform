package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.BatchExpirationQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import org.joda.time.DateTime;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;
import java.util.TimeZone;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

public class ProductBatchExpirationTask implements QuartzStartupTask {

    private static final String TIME_ZONE = "America/Los_Angeles";

    @Inject
    private SchedulerQuartzManager quartzManager;

    @Override
    public String getName() {
        return "Product Batch Expiration Notification Task";
    }

    @Override
    public void initiateStartup() {

        final String jobName = "BatchExpirationQuartzJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "BatchExpirationTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "BatchExpiration";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(BatchExpirationQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();
        TimeZone timeZone = TimeZone.getTimeZone(TIME_ZONE);

        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.dailyAtHourAndMinute(23, 55).inTimeZone(timeZone);

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup).withSchedule(cronScheduleBuilder).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
