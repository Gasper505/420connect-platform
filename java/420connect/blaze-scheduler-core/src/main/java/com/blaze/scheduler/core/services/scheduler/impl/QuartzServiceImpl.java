package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.services.scheduler.QuartzService;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;
import java.util.Properties;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by Gaurav Saini on 20/6/17.
 */
public class QuartzServiceImpl implements QuartzService {

    @Override
    public void scheduleSendToSpecificMemberGroups(Long startDate, String jobName, String triggerName, String companyId,
                                                   String message, String memberGroupId, String topicName) {
        /*JobDataMap jobData = new JobDataMap();
        jobData.put("companyId", companyId);
        jobData.put("message", message);
        jobData.put("memberGroupId", memberGroupId);
        jobData.put("topicName", topicName);
        // Create Job
        JobDetail job = newJob(SendToSpecificMemberGroupsJob.class)
                .withIdentity(jobName, "group5")
                .setJobData(jobData)
                .build();
        startScheduler(job,startDate,triggerName);*/
    }

    @Override
    public void scheduleSendToAllMembers(Long startDate, String jobName, String triggerName, String companyId, String message, String topicName) {
        /*JobDataMap jobData = new JobDataMap();
        jobData.put("companyId", companyId);
        jobData.put("message", message);
        jobData.put("topicName", topicName);
        // Create Job
        JobDetail job = newJob(SendToAllMembersJob.class)
                .withIdentity(jobName, "group5")
                .setJobData(jobData)
                .build();

        startScheduler(job,startDate,triggerName);*/
    }

    @Override
    public void scheduleSendToInActiveMembers(Long startDate, String jobName, String triggerName, String companyId, String message, Long noOfDays, String topicName) {
        /*JobDataMap jobData = new JobDataMap();
        jobData.put("companyId", companyId);
        jobData.put("message", message);
        jobData.put("noOfDays", noOfDays);
        jobData.put("topicName", topicName);
        // Create Job
        JobDetail job = newJob(SendToInActiveMembersJob.class)
                .withIdentity(jobName, "group5")
                .setJobData(jobData)
                .build();

        startScheduler(job,startDate,triggerName);*/
    }

    private void startScheduler(JobDetail job, Long startDate, String triggerName) {
        try {
            Date start = new Date(startDate);
            // Create Trigger
            Trigger trigger = newTrigger().withIdentity(triggerName, "group5")
                    .startAt(start)
                    .withSchedule(simpleSchedule()
                            .withIntervalInSeconds(10)
                            .withRepeatCount(1)).build();

            // TODO : Need to read all value from yml. Will Do this later
            //Quartz Server Properties
            Properties prop = new Properties();
            prop.put("org.quartz.scheduler.rmi.proxy", "true");
            prop.put("org.quartz.scheduler.rmi.registryHost", "localhost");
            prop.put("org.quartz.scheduler.rmi.registryPort", "1099");
            prop.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
            prop.put("org.quartz.threadPool.threadCount", "1");

            Scheduler scheduler = new StdSchedulerFactory(prop).getScheduler();
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
