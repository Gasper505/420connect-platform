package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.QuickBooksSyncQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.*;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;

/**
 * Created by mdo on 12/7/17.
 */
public class QuickBooksSyncTaskImpl implements QuartzStartupTask {
    //  private static final String SYNC_INTERVAL = "0 12 * * * ?";

    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public String getName() {
        return "Auto QuickBooks Sync Task";
    }

    @Override
    public void initiateStartup() {// Only do if it's not on-prem
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "AutoQuickBooksSyncJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "AutoQuickBooksSyncTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "AutoQuickBooksSync";

        JobDataMap jobData = new JobDataMap();

        //Create Job
        JobDetail job = newJob(QuickBooksSyncQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();


        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity(triggerName, jobGroup)
                .withSchedule(
                        CronScheduleBuilder.cronSchedule("0 0 0 * * ?"))
                .build();


        quartzManager.scheduleJob(job, trigger);
    }
}