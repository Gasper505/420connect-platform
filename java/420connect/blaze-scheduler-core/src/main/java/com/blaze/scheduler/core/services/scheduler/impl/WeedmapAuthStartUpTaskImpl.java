package com.blaze.scheduler.core.services.scheduler.impl;

import com.blaze.scheduler.core.jobs.WeedmapAuthQuartzJob;
import com.blaze.scheduler.core.managed.SchedulerQuartzManager;
import com.blaze.scheduler.core.services.scheduler.QuartzStartupTask;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import org.joda.time.DateTime;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import javax.inject.Inject;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class WeedmapAuthStartUpTaskImpl implements QuartzStartupTask {
    private static final int JOB_INTERVAL_SECONDS = 86400; //24 HRS
    @Inject
    private SchedulerQuartzManager quartzManager;

    @Inject
    private ConnectConfiguration connectConfiguration;
    
    @Override
    public String getName() {
        return "Weedmap Authentication Startup Task";
    }

    @Override
    public void initiateStartup() {
        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            return;
        }

        final String jobName = "WeedmapAuthJob".concat(String.valueOf(DateTime.now()));
        final String triggerName = "WeedmapAuthQuartzTrigger".concat(String.valueOf(DateTime.now()));
        final String jobGroup = "WeedmapAuthentication";
        JobDataMap jobData = new JobDataMap();

        // Create Job
        JobDetail job = newJob(WeedmapAuthQuartzJob.class)
                .withIdentity(jobName, jobGroup)
                .setJobData(jobData)
                .build();

        Trigger trigger = newTrigger().withIdentity(triggerName, jobGroup)
                .withSchedule(simpleSchedule()
                        .repeatForever().withIntervalInSeconds(JOB_INTERVAL_SECONDS)).build();
        quartzManager.scheduleJob(job, trigger);
    }
}
