package com.blaze.scheduler;

import com.fourtwenty.core.config.ConnectConfiguration;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * Created by Gaurav Saini on 20/6/17.
 */
public class SchedulerSwaggerBundle extends SwaggerBundle<ConnectConfiguration> {

    @Override
    protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ConnectConfiguration configuration) {
        return configuration.swaggerBundleConfiguration;
    }
}
