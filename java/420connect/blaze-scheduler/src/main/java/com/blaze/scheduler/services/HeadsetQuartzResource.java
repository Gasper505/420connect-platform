package com.blaze.scheduler.services;

import com.blaze.scheduler.core.services.scheduler.HeadsetQuartzService;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Secured;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gaurav Saini on 10/7/17.
 */
@Api("Headset")
@Path("/api/v1/headset")
@Produces(MediaType.APPLICATION_JSON)
public class HeadsetQuartzResource {

    @Inject
    HeadsetQuartzService headsetQuartzService;

    /**
     * Retrive all Headset Accounts. This job will run after every 10 minutes
     *
     * @param jobName
     * @param triggerName
     */
    @POST
    @Path("/headsetAccounts")
    @Secured
    @Timed(name = "headsetAccounts")
    @ApiOperation(value = "Retrive All Headset Accounts")
    public void getHeadsetAccount(@FormParam("jobName") String jobName,
                                  @FormParam("triggerName") String triggerName) {
        headsetQuartzService.getHeadsetAccount(jobName, triggerName);
    }
}
