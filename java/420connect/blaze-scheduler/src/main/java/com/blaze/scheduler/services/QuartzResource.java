package com.blaze.scheduler.services;

import com.blaze.scheduler.core.services.scheduler.QuartzService;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Secured;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gaurav Saini on 20/6/17.
 */
@Api("Scheduler - SMS")
@Path("/api/v1/scheduler/sms")
@Produces(MediaType.APPLICATION_JSON)
public class QuartzResource {
    @Inject
    QuartzService quartzService;

    /**
     * @param startDate
     * @param jobName
     * @param triggerName
     * @param message
     * @param memberGroupId
     */
    @POST
    @Path("/scheduleSendToSpecificMemberGroups")
    @Secured
    @Timed(name = "scheduleSendToSpecificMemberGroups")
    @ApiOperation(value = "Schedule To Send Message To Specific Member Groups")
    public void scheduleSendToSpecificMemberGroups(@FormParam("startDate") Long startDate,
                                                   @FormParam("jobName") String jobName,
                                                   @FormParam("triggerName") String triggerName,
                                                   @FormParam("companyId") String companyId,
                                                   @FormParam("message") String message,
                                                   @FormParam("memberGroupId") String memberGroupId,
                                                   @FormParam("topicName") String topicName) {
        quartzService.scheduleSendToSpecificMemberGroups(startDate, jobName, triggerName, companyId, message, memberGroupId, topicName);
    }

    /**
     * @param startDate
     * @param jobName
     * @param triggerName
     * @param message
     * @return
     */
    @POST
    @Path("/scheduleSendToAllMembers")
    @Secured
    @Timed(name = "scheduleSendToAllMembers")
    @ApiOperation(value = "Schedule To Send To All Members")
    public void scheduleSendToAllMembers(@FormParam("startDate") Long startDate,
                                         @FormParam("jobName") String jobName,
                                         @FormParam("triggerName") String triggerName,
                                         @FormParam("companyId") String companyId,
                                         @FormParam("message") String message,
                                         @FormParam("topicName") String topicName) {
        quartzService.scheduleSendToAllMembers(startDate, jobName, triggerName, companyId, message, topicName);
    }

    /**
     * @param startDate
     * @param jobName
     * @param triggerName
     * @param message
     * @param noOfDays
     */
    @POST
    @Path("/scheduleSendToInActiveMembers")
    @Secured
    @Timed(name = "scheduleSendToAllMembers")
    @ApiOperation(value = "Schedule To Send To All Members")
    public void scheduleSendToInActiveMembers(@FormParam("startDate") Long startDate,
                                              @FormParam("jobName") String jobName,
                                              @FormParam("triggerName") String triggerName,
                                              @FormParam("companyId") String companyId,
                                              @FormParam("message") String message,
                                              @FormParam("noOfDays") Long noOfDays,
                                              @FormParam("topicName") String topicName) {
        quartzService.scheduleSendToInActiveMembers(startDate, jobName, triggerName, companyId, message, noOfDays, topicName);
    }
}