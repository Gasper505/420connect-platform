package com.fourtwenty;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.vendor.VendorUpdateEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VendorSubscriber implements BlazeSubscriber {

    private static final Log LOG = LogFactory.getLog(VendorSubscriber.class);
    private static final String VENDOR = "Vendor";

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;

    @Subscribe
    public void vendorUpdateSubscriber(VendorUpdateEvent event) {
        if (StringUtils.isBlank(event.getCompanyId())) {
            LOG.info("Company cannot be blank.");
            event.setResponse(false);
            return;
        }
        if (StringUtils.isBlank(event.getVendorId())) {
            LOG.info("Vendor cannot be blank.");
            event.setResponse(null);
            return;
        }

        Vendor.VendorType dbVendorType = event.getDbVendorType();
        Vendor.VendorType reqVendorType = event.getReqVendorType();

        boolean isUpdate = (dbVendorType != reqVendorType && Vendor.VendorType.BOTH != reqVendorType);

        long count = 0;
        String msg = "";
        if (isUpdate && dbVendorType == Vendor.VendorType.CUSTOMER) {
            // invoice
            count = invoiceRepository.countActiveInvoicesByCustomer(event.getCompanyId(), event.getVendorId());
            msg = "Please complete pending invoices before updating vendor type.";
        } else if (isUpdate && dbVendorType == Vendor.VendorType.VENDOR) {
            // purchase order
            count = purchaseOrderRepository.countActivePurchaseOrderByVendor(event.getCompanyId(), event.getVendorId());
            msg = "Please complete pending purchase orders before updating vendor type.";
        } else if (isUpdate && dbVendorType == Vendor.VendorType.BOTH) {
            count = invoiceRepository.countActiveInvoicesByCustomer(event.getCompanyId(), event.getVendorId());
            count += purchaseOrderRepository.countActivePurchaseOrderByVendor(event.getCompanyId(), event.getVendorId());

            msg = "Please complete pending purchase orders or invoices before updating vendor type.";
        }

        if (count > 0) {
            event.throwException(new BlazeInvalidArgException(VENDOR, msg));
        }

        event.setResponse((count > 0));
    }


}
