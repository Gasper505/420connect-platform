package com.warehouse.core;

import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.domain.repositories.testsample.impl.TestSampleRepositoryImpl;
import com.fourtwenty.core.security.developer.DeveloperSecured;
import com.fourtwenty.core.security.developer.DeveloperSecurityInterceptor;
import com.fourtwenty.core.security.developer.DeveloperSecurityProvider;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.dispensary.SecuredInterceptor;
import com.fourtwenty.core.security.dispensary.SecurityProvider;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.store.StoreSecurityInterceptor;
import com.fourtwenty.core.security.store.StoreSecurityProvider;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.DeveloperAuthToken;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.warehouse.core.domain.repositories.batch.IncomingBatchRepository;
import com.warehouse.core.domain.repositories.batch.impl.IncomingBatchRepositoryImpl;
import com.warehouse.core.domain.repositories.dashboard.DashboardWidgetRepository;
import com.warehouse.core.domain.repositories.dashboard.impl.DashboardWidgetRepositoryImpl;
import com.warehouse.core.domain.repositories.expenses.ExpenseActivityLogRepository;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import com.warehouse.core.domain.repositories.expenses.impl.ExpenseActivityLogRepositoryImpl;
import com.warehouse.core.domain.repositories.expenses.impl.ExpensesRepositoryImpl;
import com.warehouse.core.domain.repositories.inventory.DeliveryInventoryRepository;
import com.warehouse.core.domain.repositories.inventory.impl.DeliveryInventoryRepositoryImpl;
import com.warehouse.core.domain.repositories.invoice.*;
import com.warehouse.core.domain.repositories.invoice.impl.*;
import com.warehouse.core.domain.repositories.transfer.TransferTemplateRepository;
import com.warehouse.core.domain.repositories.transfer.impl.TransferTemplateRepositoryImpl;
import com.warehouse.core.services.batch.IncomingBatchService;
import com.warehouse.core.services.batch.TestSampleService;
import com.warehouse.core.services.batch.impl.IncomingBatchServiceImpl;
import com.warehouse.core.services.batch.impl.TestSampleServiceImpl;
import com.warehouse.core.services.dashboard.DashboardService;
import com.warehouse.core.services.dashboard.impl.DashboardServiceImpl;
import com.warehouse.core.services.expenses.ExpenseActivityLogService;
import com.warehouse.core.services.expenses.ExpensesService;
import com.warehouse.core.services.expenses.impl.ExpenseActivityLogServiceImpl;
import com.warehouse.core.services.expenses.impl.ExpensesServiceImpl;
import com.warehouse.core.services.inventory.DeliveryInventoryService;
import com.warehouse.core.services.inventory.ProductInventoryService;
import com.warehouse.core.services.inventory.impl.DeliveryInventoryServiceImpl;
import com.warehouse.core.services.inventory.impl.ProductInventoryServiceImpl;
import com.warehouse.core.services.invoice.*;
import com.warehouse.core.services.invoice.impl.*;
import com.warehouse.core.services.search.DistributionSearchService;
import com.warehouse.core.services.search.impl.DistributionSearchServiceImpl;
import com.warehouse.core.services.settings.DistributionSettingService;
import com.warehouse.core.services.settings.impl.DistributionSettingServiceImpl;
import com.warehouse.core.services.store.PartnerInvoiceService;
import com.warehouse.core.services.store.common.CommonInvoiceService;
import com.warehouse.core.services.store.common.impl.CommonInvoiceServiceImpl;
import com.warehouse.core.services.store.impl.PartnerInvoiceServiceImpl;
import com.warehouse.core.services.transfer.TransferTemplateService;
import com.warehouse.core.services.transfer.impl.TransferTemplateServiceImpl;

import javax.ws.rs.Path;


public class BlazeWareHouseCoreModule extends AbstractModule {

    @Override
    protected void configure() {
        //Repositories
        bind(InvoiceRepository.class).to(InvoiceRepositoryImpl.class);
        bind(InvoicePaymentsRepository.class).to(InvoicePaymentRepositoryImpl.class);
        bind(InvoiceHistoryRepository.class).to(InvoiceHistoryRepositoryImpl.class);
        bind(InvoiceActivityLogRepository.class).to(InvoiceActivityLogRepositoryImpl.class);
        bind(DeliveryInventoryRepository.class).to(DeliveryInventoryRepositoryImpl.class);
        bind(ExpensesRepository.class).to(ExpensesRepositoryImpl.class);
        bind(InvoiceAttachmentRepository.class).to(InvoiceAttachmentRepositoryImpl.class);
        bind(ShippingManifestRepository.class).to(ShippingManifestRepositoryImpl.class);
        bind(IncomingBatchRepository.class).to(IncomingBatchRepositoryImpl.class);
        bind(TestSampleRepository.class).to(TestSampleRepositoryImpl.class);
        bind(ExpenseActivityLogRepository.class).to(ExpenseActivityLogRepositoryImpl.class);
        bind(DashboardWidgetRepository.class).to(DashboardWidgetRepositoryImpl.class);
        bind(TransferTemplateRepository.class).to(TransferTemplateRepositoryImpl.class);

        //Services
        bind(InvoiceService.class).to(InvoiceServiceImpl.class);
        bind(DeliveryInventoryService.class).to(DeliveryInventoryServiceImpl.class);
        bind(ExpensesService.class).to(ExpensesServiceImpl.class);
        bind(InvoiceActivityLogService.class).to(InvoiceActivityLogServiceImpl.class);
        bind(InvoiceHistoryService.class).to(InvoiceHistoryServiceImpl.class);
        bind(ShippingManifestService.class).to(ShippingManifestServiceImpl.class);
        bind(IncomingBatchService.class).to(IncomingBatchServiceImpl.class);
        bind(TestSampleService.class).to(TestSampleServiceImpl.class);
        bind(ExpenseActivityLogService.class).to(ExpenseActivityLogServiceImpl.class);
        bind(DashboardService.class).to(DashboardServiceImpl.class);
        bind(DistributionSearchService.class).to(DistributionSearchServiceImpl.class);
        bind(TransferTemplateService.class).to(TransferTemplateServiceImpl.class);
        bind(InvoiceInventoryService.class).to(InvoiceInventoryServiceImpl.class);
        bind(ProductInventoryService.class).to(ProductInventoryServiceImpl.class);
        bind(DistributionSettingService.class).to(DistributionSettingServiceImpl.class);

        bind(PartnerInvoiceService.class).to(PartnerInvoiceServiceImpl.class);
        bind(CommonInvoiceService.class).to(CommonInvoiceServiceImpl.class);

        // secured interceptors
        SecuredInterceptor interceptor = new SecuredInterceptor();
        requestInjection(interceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(Secured.class), interceptor);

        // Developer secured interceptor
        DeveloperSecurityInterceptor devInterceptor = new DeveloperSecurityInterceptor();
        requestInjection(devInterceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(DeveloperSecured.class), devInterceptor);

        // Store Secured Interceptor
        StoreSecurityInterceptor storeInterceptor = new StoreSecurityInterceptor();
        requestInjection(storeInterceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(StoreSecured.class), storeInterceptor);

        bind(ConnectAuthToken.class).toProvider(SecurityProvider.class);
        bind(DeveloperAuthToken.class).toProvider(DeveloperSecurityProvider.class);
        bind(StoreAuthToken.class).toProvider(StoreSecurityProvider.class);

    }

}