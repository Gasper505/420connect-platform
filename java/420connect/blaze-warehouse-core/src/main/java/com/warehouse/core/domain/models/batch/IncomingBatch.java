package com.warehouse.core.domain.models.batch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@Deprecated
@CollectionName(name = "incoming_batches", indexes = {"{batchId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class IncomingBatch extends ShopBaseModel {
    public enum BatchStatus {
        RECEIVED,
        IN_TESTING,
        READY_FOR_SALE
    }

    @NotEmpty
    private String metrcTagId;
    @NotEmpty
    private String productId;
    @NotEmpty
    private String vendorId;
    @NotEmpty
    private String brandId;
    private String productBatchId;
    private Long receiveDate;
    private String poNumber;
    private BatchStatus status;
    private String externalBatchId;
    private String metrcCategory;
    private Boolean voidStatus = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantityReceived = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal costPerUnit = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal batchPurchasePrice = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal batchCultivationTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal thc = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cbd = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cbn = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal thca = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cbda = new BigDecimal(0);
    private boolean active;
    private boolean archive = false;
    private Long archiveDate;

    public String getMetrcTagId() {
        return metrcTagId;
    }

    public void setMetrcTagId(String metrcTagId) {
        this.metrcTagId = metrcTagId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Long getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Long receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public BatchStatus getStatus() {
        return status;
    }

    public void setStatus(BatchStatus status) {
        this.status = status;
    }

    public String getExternalBatchId() {
        return externalBatchId;
    }

    public void setExternalBatchId(String externalBatchId) {
        this.externalBatchId = externalBatchId;
    }

    public String getMetrcCategory() {
        return metrcCategory;
    }

    public void setMetrcCategory(String metrcCategory) {
        this.metrcCategory = metrcCategory;
    }

    public BigDecimal getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(BigDecimal quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public BigDecimal getBatchPurchasePrice() {
        return batchPurchasePrice;
    }

    public void setBatchPurchasePrice(BigDecimal batchPurchasePrice) {
        this.batchPurchasePrice = batchPurchasePrice;
    }

    public BigDecimal getBatchCultivationTax() {
        return batchCultivationTax;
    }

    public void setBatchCultivationTax(BigDecimal batchCultivationTax) {
        this.batchCultivationTax = batchCultivationTax;
    }

    public BigDecimal getThc() {
        return thc;
    }

    public void setThc(BigDecimal thc) {
        this.thc = thc;
    }

    public BigDecimal getCbd() {
        return cbd;
    }

    public void setCbd(BigDecimal cbd) {
        this.cbd = cbd;
    }

    public BigDecimal getCbn() {
        return cbn;
    }

    public void setCbn(BigDecimal cbn) {
        this.cbn = cbn;
    }

    public BigDecimal getThca() {
        return thca;
    }

    public void setThca(BigDecimal thca) {
        this.thca = thca;
    }

    public BigDecimal getCbda() {
        return cbda;
    }

    public void setCbda(BigDecimal cbda) {
        this.cbda = cbda;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public Long getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(Long archiveDate) {
        this.archiveDate = archiveDate;
    }

    public String getProductBatchId() {
        return productBatchId;
    }

    public void setProductBatchId(String productBatchId) {
        this.productBatchId = productBatchId;
    }

    public Boolean getVoidStatus() {
        return voidStatus;
    }

    public void setVoidStatus(Boolean voidStatus) {
        this.voidStatus = voidStatus;
    }
}
