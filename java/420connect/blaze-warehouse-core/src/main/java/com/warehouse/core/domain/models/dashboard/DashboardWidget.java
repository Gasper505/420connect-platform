package com.warehouse.core.domain.models.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "dashboard_widgets", uniqueIndexes = {"{companyId:1,shopId:1,employeeId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardWidget extends ShopBaseModel {

    private List<String> widgets = new ArrayList<>();

    private String employeeId;

    public List<String> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<String> widgets) {
        this.widgets = widgets;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
