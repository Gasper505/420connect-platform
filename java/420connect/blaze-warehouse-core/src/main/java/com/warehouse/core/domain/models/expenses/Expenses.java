package com.warehouse.core.domain.models.expenses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "expenses")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Expenses extends ShopBaseModel {
    public enum ExpenseCategory {
        INVENTORY,
        LUNCH,
        MILEAGE,
        ADVERTISING,
        ASSETS,
        COMMISSIONS,
        BUSINESS_FEES,
        MEALS_AND_ENTERTAINMENT,
        OFFICE,
        RENT_AND_LEASE,
        TRAVEL,
        PAYROLL,
        OTHER
    }

    public enum ExpenseSort {
        DATE,
        AMOUNT,
        CREATED
    }

    private String expenseName;
    private ExpenseCategory expenseCategory;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    private String reference;
    private String customerId; //Vendor
    private List<CompanyAsset> attachments = new ArrayList<>();
    private List<Note> notes = new ArrayList<>();
    private boolean archive = false;
    private String expenseNumber;
    private boolean active = true;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal balanceDue = new BigDecimal(0);
    private String purchaseOrderId;
    private CompanyAsset expenseQRCodeAsset;
    private String expenseQRCodeUrl;
    private String createdByEmployeeId;
    private String updateByEmployeeId;
    private long expenseDate;

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public String getExpenseNumber() {
        return expenseNumber;
    }

    public void setExpenseNumber(String expenseNumber) {
        this.expenseNumber = expenseNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ExpenseCategory getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public BigDecimal getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public CompanyAsset getExpenseQRCodeAsset() {
        return expenseQRCodeAsset;
    }

    public void setExpenseQRCodeAsset(CompanyAsset expenseQRCodeAsset) {
        this.expenseQRCodeAsset = expenseQRCodeAsset;
    }

    public String getExpenseQRCodeUrl() {
        return expenseQRCodeUrl;
    }

    public void setExpenseQRCodeUrl(String expenseQRCodeUrl) {
        this.expenseQRCodeUrl = expenseQRCodeUrl;
    }

    public String getCreatedByEmployeeId() {
        return createdByEmployeeId;
    }

    public void setCreatedByEmployeeId(String createdByEmployeeId) {
        this.createdByEmployeeId = createdByEmployeeId;
    }

    public String getUpdateByEmployeeId() {
        return updateByEmployeeId;
    }

    public void setUpdateByEmployeeId(String updateByEmployeeId) {
        this.updateByEmployeeId = updateByEmployeeId;
    }

    public long getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(long expenseDate) {
        this.expenseDate = expenseDate;
    }
}
