package com.warehouse.core.domain.models.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class DistributorInformation {
  private String licenseId;
  private String shopId;
  private String companyContactId;
  private String customerCompanyId;


  public String getLicenseId() {
    return licenseId;
  }

  public void setLicenseId(String licenseId) {
    this.licenseId = licenseId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getCompanyContactId() {
    return companyContactId;
  }

  public void setCompanyContactId(String companyContactId) {
    this.companyContactId = companyContactId;
  }

  public String getCustomerCompanyId() {
    return customerCompanyId;
  }

  public void setCustomerCompanyId(String customerCompanyId) {
    this.customerCompanyId = customerCompanyId;
  }
}
