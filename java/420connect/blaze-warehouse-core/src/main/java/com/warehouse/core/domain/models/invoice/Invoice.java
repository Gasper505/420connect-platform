package com.warehouse.core.domain.models.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "invoices", indexes = {"{invoiceId:1}", "{companyId:1,shopId:1,customerId:1,deleted:1}", "{companyId:1,shopId:1,deleted:1,active:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Invoice extends ShopBaseModel implements InternalAllowable {
    public enum InvoiceTerms {
        NET_30,
        COD,
        NET_45,
        NET_60,
        NET_15,
        NET_7,
        CUSTOM_DATE
    }

    public enum InvoiceStatus {
        IN_PROGRESS,
        DRAFT,
        SENT,
        CANCELLED,
        COMPLETED
    }

    public enum PaymentStatus {
        UNPAID,
        PARTIAL,
        PAID
    }

    public enum InvoiceSort {
        DATE,
        OVER_DUE,
        AMOUNT,
        CREATED_DATE,
        DELIVERY_DATE,
        MODIFIED
    }


    private String customerId;
    @Deprecated
    private String license;
    private String invoiceNumber;
    private String orderNumber;
    private InvoiceTerms invoiceTerms;
    @NotEmpty
    private String salesPersonId;
    private List<CompanyAsset> attachments = new ArrayList<>();
    private Long dueDate;
    private String termsAndconditions;
    private List<Note> notes;
    private Long invoiceDate;
    private Long estimatedDeliveryDate;
    private Long estimatedDeliveryTime;

    private InvoiceStatus invoiceStatus;
    private PaymentStatus invoicePaymentStatus;
    private boolean inventoryProcessed;
    private Long inventoryProcessedDate;
    private boolean active;
    @Deprecated
    private boolean relatedEntity = true;
    private Cart cart;
    private CompanyAsset invoiceQrCodeAsset;
    private String invoiceQrCodeUrl;
    private String companyContactId;
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryCharges = new BigDecimal(0);
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal total = new BigDecimal(0);
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal wholeSaleCostOfCannabiesItems = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal markup = new BigDecimal(0);
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal exciseTax = new BigDecimal(0);
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal totalInvoiceAmount = new BigDecimal(0);
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalTax = new BigDecimal(0);
    private Vendor.ArmsLengthType transactionType = Vendor.ArmsLengthType.ARMS_LENGTH;
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal wholeSaleCostOfNonCannabisItems = new BigDecimal(0);
    private String licenseId;

    private String qbDesktopRef; //TxnId
    private String editSequence;
    private String qbListId;
    private boolean qbError;
    private long errorTime;
    private boolean qbPaymentReceived;
    private boolean qbPaymentReceivedError;
    private long qbPaymentReceivedErrorTime;
    private InvoiceStatus invoicePreviousStatus;

    private  String externalId;
    private PurchaseOrder.FlowerSourceType flowerSourceType = PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public InvoiceTerms getInvoiceTerms() {
        return invoiceTerms;
    }

    public void setInvoiceTerms(InvoiceTerms invoiceTerms) {
        this.invoiceTerms = invoiceTerms;
    }

    public String getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(String salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public BigDecimal getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(BigDecimal deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public String getTermsAndconditions() {
        return termsAndconditions;
    }

    public void setTermsAndconditions(String termsAndconditions) {
        this.termsAndconditions = termsAndconditions;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Long getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Long invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Long getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(Long estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    public Long getEstimatedDeliveryTime() {
        return estimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(Long estimatedDeliveryTime) {
        this.estimatedDeliveryTime = estimatedDeliveryTime;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getWholeSaleCostOfCannabiesItems() {
        return wholeSaleCostOfCannabiesItems;
    }

    public void setWholeSaleCostOfCannabiesItems(BigDecimal wholeSaleCostOfCannabiesItems) {
        this.wholeSaleCostOfCannabiesItems = wholeSaleCostOfCannabiesItems;
    }

    public BigDecimal getMarkup() {
        return markup;
    }

    public void setMarkup(BigDecimal markup) {
        this.markup = markup;
    }

    public BigDecimal getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(BigDecimal exciseTax) {
        this.exciseTax = exciseTax;
    }

    public BigDecimal getTotalInvoiceAmount() {
        return totalInvoiceAmount;
    }

    public void setTotalInvoiceAmount(BigDecimal totalInvoiceAmount) {
        this.totalInvoiceAmount = totalInvoiceAmount;
    }

    public InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public PaymentStatus getInvoicePaymentStatus() {
        return invoicePaymentStatus;
    }

    public void setInvoicePaymentStatus(PaymentStatus invoicePaymentStatus) {
        this.invoicePaymentStatus = invoicePaymentStatus;
    }


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isInventoryProcessed() {
        return inventoryProcessed;
    }

    public void setInventoryProcessed(boolean inventoryProcessed) {
        this.inventoryProcessed = inventoryProcessed;
    }

    public Long getInventoryProcessedDate() {
        return inventoryProcessedDate;
    }

    public void setInventoryProcessedDate(Long inventoryProcessedDate) {
        this.inventoryProcessedDate = inventoryProcessedDate;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public BigDecimal getWholeSaleCostOfNonCannabisItems() {
        return wholeSaleCostOfNonCannabisItems;
    }

    public void setWholeSaleCostOfNonCannabisItems(BigDecimal wholeSaleCostOfNonCannabisItems) {
        this.wholeSaleCostOfNonCannabisItems = wholeSaleCostOfNonCannabisItems;
    }

    public Vendor.ArmsLengthType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Vendor.ArmsLengthType transactionType) {
        this.transactionType = transactionType;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public String getInvoiceQrCodeUrl() {
        return invoiceQrCodeUrl;
    }

    public void setInvoiceQrCodeUrl(String invoiceQrCodeUrl) {
        this.invoiceQrCodeUrl = invoiceQrCodeUrl;
    }

    public CompanyAsset getInvoiceQrCodeAsset() {
        return invoiceQrCodeAsset;
    }

    public void setInvoiceQrCodeAsset(CompanyAsset invoiceQrCodeAsset) {
        this.invoiceQrCodeAsset = invoiceQrCodeAsset;
    }

    public String getCompanyContactId() {
        return companyContactId;
    }

    public void setCompanyContactId(String companyContactId) {
        this.companyContactId = companyContactId;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getQbDesktopRef() {
        return qbDesktopRef;
    }

    public void setQbDesktopRef(String qbDesktopRef) {
        this.qbDesktopRef = qbDesktopRef;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getVendorQbListId() {
        return qbListId;
    }

    public void setVendorQbListId(String vendorQbListId) {
        this.qbListId = vendorQbListId;
    }

    public boolean isQbError() {
        return qbError;
    }

    public void setQbError(boolean qbError) {
        this.qbError = qbError;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }

    public boolean isQbPaymentReceived() {
        return qbPaymentReceived;
    }

    public void setQbPaymentReceived(boolean qbPaymentReceived) {
        this.qbPaymentReceived = qbPaymentReceived;
    }

    public boolean isQbPaymentReceivedError() {
        return qbPaymentReceivedError;
    }

    public void setQbPaymentReceivedError(boolean qbPaymentReceivedError) {
        this.qbPaymentReceivedError = qbPaymentReceivedError;
    }

    public long getQbPaymentReceivedErrorTime() {
        return qbPaymentReceivedErrorTime;
    }

    public void setQbPaymentReceivedErrorTime(long qbPaymentReceivedErrorTime) {
        this.qbPaymentReceivedErrorTime = qbPaymentReceivedErrorTime;
    }

    public InvoiceStatus getInvoicePreviousStatus() {
        return invoicePreviousStatus;
    }

    public void setInvoicePreviousStatus(InvoiceStatus invoicePreviousStatus) {
        this.invoicePreviousStatus = invoicePreviousStatus;
    }
    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public PurchaseOrder.FlowerSourceType getFlowerSourceType() {
        return flowerSourceType;
    }

    public void setFlowerSourceType(PurchaseOrder.FlowerSourceType flowerSourceType) {
        this.flowerSourceType = flowerSourceType;
    }
}
