package com.warehouse.core.domain.models.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import javax.validation.constraints.NotNull;

@Deprecated
@CollectionName(name = "invoice_attachment")
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceAttachment extends ShopBaseModel {
    @NotNull
    private String invoiceId;
    private CompanyAsset companyAsset;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public CompanyAsset getCompanyAsset() {
        return companyAsset;
    }

    public void setCompanyAsset(CompanyAsset companyAsset) {
        this.companyAsset = companyAsset;
    }
}
