package com.warehouse.core.domain.models.invoice;

import com.fourtwenty.core.domain.models.generic.BaseModel;

public class ManifestTransactionLog extends BaseModel {
    private ShippingManifest.ShippingManifestStatus status;
    private String transactionId;

    public ShippingManifest.ShippingManifestStatus getStatus() {
        return status;
    }

    public void setStatus(ShippingManifest.ShippingManifestStatus status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
