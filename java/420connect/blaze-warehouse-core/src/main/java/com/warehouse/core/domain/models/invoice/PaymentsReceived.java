package com.warehouse.core.domain.models.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@CollectionName(name = "invoice_payments", indexes = "{invoiceId:1}")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentsReceived extends ShopBaseModel {
    private PaymentType paymentType;
    private String invoiceId;
    private Long paidDate;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amountPaid = new BigDecimal(0);
    private Note notes;
    private String referenceNo;
    private String paymentNo;

    private String qbDesktopPaymentRef;
    private boolean qbErrored;
    private long errorTime;

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Long getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Long paidDate) {
        this.paidDate = paidDate;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Note getNotes() {
        return notes;
    }

    public void setNotes(Note notes) {
        this.notes = notes;
    }

    public enum PaymentType {
        ACH_TRANSFER,
        DEBIT,
        CREDIT,
        CASH,
        CHEQUE,
        OTHER
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getQbDesktopPaymentRef() {
        return qbDesktopPaymentRef;
    }

    public void setQbDesktopPaymentRef(String qbDesktopPaymentRef) {
        this.qbDesktopPaymentRef = qbDesktopPaymentRef;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }
}
