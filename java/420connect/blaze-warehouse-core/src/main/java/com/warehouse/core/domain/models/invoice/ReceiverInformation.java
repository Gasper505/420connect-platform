package com.warehouse.core.domain.models.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.Address;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceiverInformation {
    private String customerCompanyId;
    private String companyContactId;
    private Address companyAddress;

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getCompanyContactId() {
        return companyContactId;
    }

    public void setCompanyContactId(String companyContactId) {
        this.companyContactId = companyContactId;
    }

    public Address getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(Address companyAddress) {
        this.companyAddress = companyAddress;
    }
}
