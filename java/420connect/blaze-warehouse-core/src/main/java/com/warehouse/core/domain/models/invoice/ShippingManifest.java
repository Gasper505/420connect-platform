package com.warehouse.core.domain.models.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CollectionName(name = "shipping_manifest", indexes = {"{invoiceId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShippingManifest extends ShopBaseModel {
    public enum ShippingManifestStatus {
        InProgress,
        Completed,
        Rejected
    }

    private String invoiceId;
    private String driverId;
    private Long shippedDateTime;
    private Long estimatedArrival;
    @Deprecated
    private List<String> productIds = new ArrayList<>();
    private ShipperInformation shipperInformation;
    private ReceiverInformation receiverInformation;
    private String PONumber;
    private Long invoiceCreatedDate;
    private String shippingManifestNo;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal invoiceAmount = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal invoiceBalanceDue = new BigDecimal(0);
    private Invoice.InvoiceStatus invoiceStatus;
    private CompanyAsset signaturePhoto;
    private Long signatureDateTime;

    private List<ProductMetrcInfo> productMetrcInfo = new ArrayList<>();
    @Deprecated
    private HashMap<String, String> metrcMap = new HashMap<>();

    private long deliveryDate;
    private long deliveryTime;
    private List<Note> notes;
    @Deprecated
    private String batchId;
    @Deprecated
    private String metrcLabel;

    private String declineReason;
    private long approvalDate;
    private CompanyAsset managerSignature;
    private String approverEmployeeId;
    private ShippingManifestStatus status = ShippingManifestStatus.InProgress;
    private String transactionId;
    private long declineDate;
    private String rejectInventoryId;
    private List<ManifestTransactionLog> manifestTransactionLog = new ArrayList<>();
    private String externalLicense;
    private DistributorInformation distributorInformation;
    private boolean sentToConnectedShop = false;

    private boolean metrc;
    private long metrcSentTime;
    private String metrcTransferTemplateId;
    private String metrcManifestNumber;
    private String metrcTransferType;

    public String getMetrcTransferType() {
        return metrcTransferType;
    }

    public void setMetrcTransferType(String metrcTransferType) {
        this.metrcTransferType = metrcTransferType;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public ShipperInformation getShipperInformation() {
        return shipperInformation;
    }

    public void setShipperInformation(ShipperInformation shipperInformation) {
        this.shipperInformation = shipperInformation;
    }

    public ReceiverInformation getReceiverInformation() {
        return receiverInformation;
    }

    public void setReceiverInformation(ReceiverInformation receiverInformation) {
        this.receiverInformation = receiverInformation;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Long getShippedDateTime() {
        return shippedDateTime;
    }

    public void setShippedDateTime(Long shippedDateTime) {
        this.shippedDateTime = shippedDateTime;
    }

    public Long getEstimatedArrival() {
        return estimatedArrival;
    }

    public void setEstimatedArrival(Long estimatedArrival) {
        this.estimatedArrival = estimatedArrival;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public String getPONumber() {
        return PONumber;
    }

    public void setPONumber(String PONumber) {
        this.PONumber = PONumber;
    }

    public Long getInvoiceCreatedDate() {
        return invoiceCreatedDate;
    }

    public void setInvoiceCreatedDate(Long invoiceCreatedDate) {
        this.invoiceCreatedDate = invoiceCreatedDate;
    }

    public String getShippingManifestNo() {
        return shippingManifestNo;
    }

    public void setShippingManifestNo(String shippingManifestNo) {
        this.shippingManifestNo = shippingManifestNo;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public BigDecimal getInvoiceBalanceDue() {
        return invoiceBalanceDue;
    }

    public void setInvoiceBalanceDue(BigDecimal invoiceBalanceDue) {
        this.invoiceBalanceDue = invoiceBalanceDue;
    }

    public Invoice.InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(Invoice.InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public List<ProductMetrcInfo> getProductMetrcInfo() {
        return productMetrcInfo;
    }

    public void setProductMetrcInfo(List<ProductMetrcInfo> productMetrcInfo) {
        this.productMetrcInfo = productMetrcInfo;
    }

    public CompanyAsset getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(CompanyAsset signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public Long getSignatureDateTime() {
        return signatureDateTime;
    }

    public void setSignatureDateTime(Long signatureDateTime) {
        this.signatureDateTime = signatureDateTime;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getMetrcLabel() {
        return metrcLabel;
    }

    public void setMetrcLabel(String metrcLabel) {
        this.metrcLabel = metrcLabel;
    }

    public HashMap<String, String> getMetrcMap() {
        return metrcMap;
    }

    public void setMetrcMap(HashMap<String, String> metrcMap) {
        this.metrcMap = metrcMap;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public long getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(long approvalDate) {
        this.approvalDate = approvalDate;
    }

    public CompanyAsset getManagerSignature() {
        return managerSignature;
    }

    public void setManagerSignature(CompanyAsset managerSignature) {
        this.managerSignature = managerSignature;
    }

    public ShippingManifestStatus getStatus() {
        return status;
    }

    public void setStatus(ShippingManifestStatus status) {
        this.status = status;
    }

    public String getApproverEmployeeId() {
        return approverEmployeeId;
    }

    public void setApproverEmployeeId(String approverEmployeeId) {
        this.approverEmployeeId = approverEmployeeId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public long getDeclineDate() {
        return declineDate;
    }

    public void setDeclineDate(long declineDate) {
        this.declineDate = declineDate;
    }

    public String getRejectInventoryId() {
        return rejectInventoryId;
    }

    public void setRejectInventoryId(String rejectInventoryId) {
        this.rejectInventoryId = rejectInventoryId;
    }

    public List<ManifestTransactionLog> getManifestTransactionLog() {
        return manifestTransactionLog;
    }

    public void setManifestTransactionLog(List<ManifestTransactionLog> manifestTransactionLog) {
        this.manifestTransactionLog = manifestTransactionLog;
    }

    public String getExternalLicense() {
        return externalLicense;
    }

    public void setExternalLicense(String externalLicense) {
        this.externalLicense = externalLicense;
    }

    public DistributorInformation getDistributorInformation() {
        return distributorInformation;
    }

    public void setDistributorInformation(DistributorInformation distributorInformation) {
        this.distributorInformation = distributorInformation;
    }

    public boolean isMetrc() {
        return metrc;
    }

    public void setMetrc(boolean metrc) {
        this.metrc = metrc;
    }

    public long getMetrcSentTime() {
        return metrcSentTime;
    }

    public void setMetrcSentTime(long metrcSentTime) {
        this.metrcSentTime = metrcSentTime;
    }

    public String getMetrcTransferTemplateId() {
        return metrcTransferTemplateId;
    }

    public void setMetrcTransferTemplateId(String metrcTransferTemplateId) {
        this.metrcTransferTemplateId = metrcTransferTemplateId;
    }

    public String getMetrcManifestNumber() {
        return metrcManifestNumber;
    }

    public void setMetrcManifestNumber(String metrcManifestNumber) {
        this.metrcManifestNumber = metrcManifestNumber;
    }
    
    public boolean isSentToConnectedShop() {
        return sentToConnectedShop;
    }

    public void setSentToConnectedShop(boolean sentToConnectedShop) {
        this.sentToConnectedShop = sentToConnectedShop;
    }
}
