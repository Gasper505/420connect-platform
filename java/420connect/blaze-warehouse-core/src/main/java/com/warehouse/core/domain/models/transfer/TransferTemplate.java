package com.warehouse.core.domain.models.transfer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.warehouse.core.rest.transfer.request.Destination;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "transfer_templates", indexes = {"{transferTemplateId:1}", "{invoiceId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTemplate extends ShopBaseModel {
    private String templateManifestNumber;
    private String templateShipperLicenseNumber;
    private String templateName;
    private String employeeId;
    private String invoiceId;
    private Boolean templateStatus = true;
    private List<Destination> destinations = new ArrayList<>();

    public String getTemplateManifestNumber() {
        return templateManifestNumber;
    }

    public void setTemplateManifestNumber(String templateManifestNumber) {
        this.templateManifestNumber = templateManifestNumber;
    }

    public String getTemplateShipperLicenseNumber() {
        return templateShipperLicenseNumber;
    }

    public void setTemplateShipperLicenseNumber(String templateShipperLicenseNumber) {
        this.templateShipperLicenseNumber = templateShipperLicenseNumber;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Boolean getTemplateStatus() {
        return templateStatus;
    }

    public void setTemplateStatus(Boolean templateStatus) {
        this.templateStatus = templateStatus;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
}
