package com.warehouse.core.domain.repositories.batch;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.batch.IncomingBatch;
import com.warehouse.core.rest.batch.result.IncomingBatchResult;
import org.bson.types.ObjectId;

import java.util.List;

@Deprecated
public interface IncomingBatchRepository extends MongoShopBaseRepository<IncomingBatch> {
    SearchResult<IncomingBatchResult> getAllIncomingBatches(final String companyId, final String shopId, final String sortOption, final int start, final int limit);

    /*SearchResult<IncomingBatchResult> searchAllIncomingBatches(final String companyId, final String shopId, final String term, final int start, final int limit);*/
    SearchResult<IncomingBatchResult> getAllIncomingBatchesByState(final String companyId, final String shopId, final boolean state, final String sortOption, final int start, final int limit);

    SearchResult<IncomingBatchResult> getAllIncomingBatchesByStatus(final String companyId, final String shopId, final IncomingBatch.BatchStatus status, final String sortOption, final int start, final int limit);

    SearchResult<IncomingBatchResult> getAllArchivedIncomingBatches(final String companyId, final String shopId, final String sortOption, final int start, final int limit);

    SearchResult<IncomingBatchResult> getAllScannedIncomingBatch(final String companyId, final String shopId, final String metrcTagId, final String productId, int start, final int limit, final String sortOption);

    SearchResult<IncomingBatchResult> getAllDeletedIncomingBatch(final String companyId, final String shopId, final String sortOption, final int start, final int limit);

    void bulkBatchDelete(final String companyId, final String shopId, final List<ObjectId> batchIds);

    void bulkBatchStatusUpdate(final String companyId, final String shopId, final List<ObjectId> batchIds, final IncomingBatch.BatchStatus status);

    void bulkBatchArchive(final String companyId, final String shopId, final List<ObjectId> batchIds, final boolean archive);

    void bulkBatchVoidStatusUpdate(final String companyId, final String shopId, final List<ObjectId> batchIds, final Boolean voidStatus);

    SearchResult<IncomingBatchResult> getAllIncomingBatchesByVoidStatus(final String companyId, final String shopId, final boolean voidStatus, final String sortOption, final int start, final int limit);
}
