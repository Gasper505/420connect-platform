package com.warehouse.core.domain.repositories.batch.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.batch.IncomingBatch;
import com.warehouse.core.domain.repositories.batch.IncomingBatchRepository;
import com.warehouse.core.rest.batch.result.IncomingBatchResult;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;

@Deprecated
public class IncomingBatchRepositoryImpl extends ShopBaseRepositoryImpl<IncomingBatch> implements IncomingBatchRepository {
    @Inject
    public IncomingBatchRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(IncomingBatch.class, mongoDb);
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatches(String companyId, String shopId, String sortOption, int start, int limit) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,archive:false}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archive:false}", companyId, shopId);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    /*@Override
    public SearchResult<IncomingBatchResult> searchAllIncomingBatches(String companyId, String shopId, String term, int start, int limit) {
        Pattern pattern = TextUtil.createPattern ( term );
        Iterable<IncomingBatchResult>  items = coll.find ("{$and:[{companyId:#,shopId:#,deleted:false},{$or:[{",companyId,shopId).skip ( start ).limit ( limit ).as ( IncomingBatchResult.class );
        long count = coll.count ("{companyId:#,shopId:#,deleted:false}",companyId,shopId);
        SearchResult<IncomingBatchResult> results = new SearchResult<> ();
        results.setValues ( Lists.newArrayList (items) );
        results.setSkip ( start );
        results.setLimit ( limit );
        results.setTotal ( count );
        return results;
    }*/

    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByState(String companyId, String shopId, boolean state, String sortOption, int start, int limit) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,active:#,archive:false}", companyId, shopId, state).sort(sortOption).skip(start).limit(limit).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:#,archive:false}", companyId, shopId, state);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByStatus(String companyId, String shopId, IncomingBatch.BatchStatus status, String sortOption, int start, int limit) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,status:#,archive:false}", companyId, shopId, status).sort(sortOption).skip(start).limit(limit).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,status:#,archive:false}", companyId, shopId, status);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<IncomingBatchResult> getAllArchivedIncomingBatches(String companyId, String shopId, String sortOption, int start, int limit) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,archive:true,deleted:false}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archive:true}", companyId, shopId);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllScannedIncomingBatch(String companyId, String shopId, String metrcTagId, String productId, int start, int limit, String sortOption) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,metrcTagId:#,productId:#}", companyId, shopId, metrcTagId, productId).sort(sortOption).skip(start).limit(limit).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,metrcTagId:#,productId:#}", companyId, shopId, metrcTagId, productId);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllDeletedIncomingBatch(String companyId, String shopId, String sortOption, int start, int limit) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:true}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:true}", companyId, shopId);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkBatchDelete(String companyId, String shopId, List<ObjectId> batchIds) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, batchIds).multi().with("{$set: {deleted:true, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public void bulkBatchStatusUpdate(String companyId, String shopId, List<ObjectId> batchIds, IncomingBatch.BatchStatus status) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, batchIds).multi().with("{$set: {status:#}}", status);
    }

    @Override
    public void bulkBatchArchive(String companyId, String shopId, List<ObjectId> batchIds, boolean archive) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, batchIds).multi().with("{$set:{archive:#}}", archive);
    }

    @Override
    public void bulkBatchVoidStatusUpdate(String companyId, String shopId, List<ObjectId> batchIds, Boolean voidStatus) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, batchIds).multi().with("{$set:{voidStatus:#}}", voidStatus);
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByVoidStatus(String companyId, String shopId, boolean voidStatus, String sortOption, int start, int limit) {
        Iterable<IncomingBatchResult> items = coll.find("{companyId:#,shopId:#,voidStatus:#,deleted:false,archive:false}", companyId, shopId, voidStatus).limit(limit).skip(start).sort(sortOption).as(IncomingBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,voidStatus:#}", companyId, shopId, voidStatus);
        SearchResult<IncomingBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
