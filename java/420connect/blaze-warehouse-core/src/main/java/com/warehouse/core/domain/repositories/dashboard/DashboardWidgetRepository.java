package com.warehouse.core.domain.repositories.dashboard;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.warehouse.core.domain.models.dashboard.DashboardWidget;

public interface DashboardWidgetRepository extends MongoShopBaseRepository<DashboardWidget> {
    void saveDashboardWidget(DashboardWidget dashboardWidget);
    DashboardWidget findByIds(String employeeId, String companyId, String shopId);
}
