package com.warehouse.core.domain.repositories.dashboard.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.warehouse.core.domain.models.dashboard.DashboardWidget;
import com.warehouse.core.domain.repositories.dashboard.DashboardWidgetRepository;

import javax.inject.Inject;

public class DashboardWidgetRepositoryImpl extends ShopBaseRepositoryImpl<DashboardWidget> implements DashboardWidgetRepository {
    @Inject
    public DashboardWidgetRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(DashboardWidget.class, mongoDb);
    }

    @Override
    public void saveDashboardWidget(DashboardWidget dashboardWidget) {
        save(dashboardWidget);
    }

    @Override
    public DashboardWidget findByIds(String employeeId, String companyId, String shopId) {
        return coll.findOne("{employeeId: #, companyId: #, shopId: #}", employeeId, companyId, shopId).as(entityClazz);
    }
}
