package com.warehouse.core.domain.repositories.expenses;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;

public interface ExpenseActivityLogRepository extends MongoShopBaseRepository<ExpensesActivityLog> {
    SearchResult<ExpensesActivityLog> getAllExpenseActivityLogByExpenseId(final String companyId, final String shopId, final String expenseId, final String sortOption, final int start, final int limit);
}
