package com.warehouse.core.domain.repositories.expenses;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.expenses.Expenses;
import org.bson.types.ObjectId;

import java.util.List;

public interface ExpensesRepository extends MongoShopBaseRepository<Expenses> {
    <E extends Expenses> DateSearchResult<E> findExpensesWithDate(final String companyId, final String shopId, final long afterDate, final long beforeDate, final String sortOption, Class<E> clazz);

    <E extends Expenses> SearchResult<E> getAllExpensesByCustomerId(final String companyId, final String shopId, final String customerId, final int start, final int limit, final String sortOption, Class<E> clazz);

    <E extends Expenses> SearchResult<E> getAllArchivedExpenses(final String companyId, final String shopId, final String sortOption, final int start, final int limit, Class<E> clazz);

    <E extends Expenses> SearchResult<E> getAllExpensesByState(final String companyId, final String shopId, final String sortOption, final boolean state, final int start, final int limit, Class<E> clazz);

    void bulkUpdateExpenseCategory(final String companyId, final String shopId, final List<ObjectId> expenseObjectId, final Expenses.ExpenseCategory expenseCategory);

    void bulkDeleteExpense(final String companyId, final String shopId, final List<ObjectId> expenseObjectId);

    void bulkArchiveExpense(final String companyId, final String shopId, final List<ObjectId> expenseObjectId, final boolean isArchive);

    void bulkChangeState(final String companyId, final String shopId, final List<ObjectId> expenseObjectId, final boolean state);

    <E extends ShopBaseModel> SearchResult<E> getAllExpensesByTerm(final String companyId, final String shopId, final boolean state, final String searchTerm, final String sortOption, final int start, final int limit, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> getAllArchivedExpensesByTerm(final String companyId, final String shopId, final String searchTerm, final String sortOption, final int start, final int limit, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> getAllExpensesWithoutStateByTerm(final String companyId, final String shopId, final String searchTerm, final String sortOption, final int start, final int limit, Class<E> clazz);
}
