package com.warehouse.core.domain.repositories.expenses.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;
import com.warehouse.core.domain.repositories.expenses.ExpenseActivityLogRepository;

import javax.inject.Inject;

public class ExpenseActivityLogRepositoryImpl extends ShopBaseRepositoryImpl<ExpensesActivityLog> implements ExpenseActivityLogRepository {

    @Inject
    public ExpenseActivityLogRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(ExpensesActivityLog.class, mongoDb);
    }

    @Override
    public SearchResult<ExpensesActivityLog> getAllExpenseActivityLogByExpenseId(String companyId, String shopId, String expenseId, String sortOption, int start, int limit) {
        Iterable<ExpensesActivityLog> items = coll.find("{companyId:#,shopId:#,deleted:false,targetId:#}", companyId, shopId, expenseId).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,targetId:#}", companyId, shopId, expenseId);
        SearchResult<ExpensesActivityLog> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
