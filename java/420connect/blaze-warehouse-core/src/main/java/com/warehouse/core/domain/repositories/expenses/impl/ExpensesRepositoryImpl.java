package com.warehouse.core.domain.repositories.expenses.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;
import java.util.regex.Pattern;

public class ExpensesRepositoryImpl extends ShopBaseRepositoryImpl<Expenses> implements ExpensesRepository {

    @Inject
    public ExpensesRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(Expenses.class, mongoDb);
    }

    @Override
    public <E extends Expenses> DateSearchResult<E> findExpensesWithDate(String companyId, String shopId, long afterDate, long beforeDate, String sortOption, Class<E> clazz) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,archive:false, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort(sortOption).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archive:false,modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);
        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Expenses> SearchResult<E> getAllExpensesByCustomerId(String companyId, String shopId, String customerId, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,customerId:#,deleted:false,archive:false}", companyId, shopId, customerId).skip(start).limit(limit).sort(sortOption).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,customerId:#,deleted:false,archive:false}", companyId, shopId, customerId);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Expenses> SearchResult<E> getAllArchivedExpenses(String companyId, String shopId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,archive:true}", companyId, shopId).skip(start).limit(limit).sort(sortOption).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archive:true}", companyId, shopId);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Expenses> SearchResult<E> getAllExpensesByState(String companyId, String shopId, String sortOption, boolean state, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,archive:false,active:#}", companyId, shopId, state).skip(start).limit(limit).sort(sortOption).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archive:false,active:#}", companyId, shopId, state);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkUpdateExpenseCategory(String companyId, String shopId, List<ObjectId> expenseObjectId, Expenses.ExpenseCategory expenseCategory) {
        coll.update("{companyId : #,shopId : #,_id:{$in:#}}", companyId, shopId, expenseObjectId).multi().with("{$set :{expenseCategory:#,modified:#}} ", expenseCategory, DateTime.now().getMillis());
    }

    @Override
    public void bulkDeleteExpense(String companyId, String shopId, List<ObjectId> expenseObjectId) {
        coll.update("{companyId : #,shopId : #,_id:{$in:#}}", companyId, shopId, expenseObjectId).multi().with("{$set :{deleted:true,modified:#}} ", DateTime.now().getMillis());
    }

    @Override
    public void bulkArchiveExpense(String companyId, String shopId, List<ObjectId> expenseObjectId, boolean isArchive) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, expenseObjectId).multi().with("{$set : {archive: #,modified:#} }", isArchive, DateTime.now().getMillis());
    }

    @Override
    public void bulkChangeState(String companyId, String shopId, List<ObjectId> expenseObjectId, boolean state) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, expenseObjectId).multi().with("{$set : {active:#,modified:#} }", state, DateTime.now().getMillis());
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllExpensesByTerm(String companyId, String shopId, boolean state, String searchTerm, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,archive:false,active:#},{$or:[{expenseName:#},{expenseCategory:#},{expenseNumber:#}]}]}", companyId, shopId, state, pattern, pattern, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,archive:false,active:#},{$or:[{expenseName:#},{expenseCategory:#},{expenseNumber:#}]}]}", companyId, shopId, state, pattern, pattern, pattern);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllArchivedExpensesByTerm(String companyId, String shopId, String searchTerm, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,archive:true},{$or:[{expenseName:#},{expenseCategory:#},{expenseNumber:#}]}]}", companyId, shopId, pattern, pattern, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,archive:true},{$or:[{expenseName:#},{expenseCategory:#},{expenseNumber:#}]}]}", companyId, shopId, pattern, pattern, pattern);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllExpensesWithoutStateByTerm(String companyId, String shopId, String searchTerm, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,archive:false},{$or:[{expenseName:#},{expenseCategory:#},{expenseNumber:#}]}]}", companyId, shopId, pattern, pattern, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,archive:false},{$or:[{expenseName:#},{expenseCategory:#},{expenseNumber:#}]}]}", companyId, shopId, pattern, pattern, pattern);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
