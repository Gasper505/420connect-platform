package com.warehouse.core.domain.repositories.inventory;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.warehouse.core.domain.models.inventory.DeliveryInventory;

public interface DeliveryInventoryRepository extends MongoShopBaseRepository<DeliveryInventory> {
}
