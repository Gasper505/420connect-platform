package com.warehouse.core.domain.repositories.inventory.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.warehouse.core.domain.models.inventory.DeliveryInventory;
import com.warehouse.core.domain.repositories.inventory.DeliveryInventoryRepository;

import javax.inject.Inject;

public class DeliveryInventoryRepositoryImpl extends ShopBaseRepositoryImpl<DeliveryInventory> implements DeliveryInventoryRepository {

    @Inject
    public DeliveryInventoryRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(DeliveryInventory.class, mongoDb);
    }
}
