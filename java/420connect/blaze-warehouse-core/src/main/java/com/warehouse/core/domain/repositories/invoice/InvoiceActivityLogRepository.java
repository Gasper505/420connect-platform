package com.warehouse.core.domain.repositories.invoice;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;

import java.util.HashMap;
import java.util.List;


public interface InvoiceActivityLogRepository extends MongoShopBaseRepository<InvoiceActivityLog> {
    SearchResult<InvoiceActivityLog> getInvoiceActivityLogByInvoiceId(final String companyId, final String shopId, final String invoiceId, final String sortOption, final int start, final int limit);

    HashMap<String, List<InvoiceActivityLog>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId);
}
