package com.warehouse.core.domain.repositories.invoice;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.warehouse.core.domain.models.invoice.InvoiceAttachment;

@Deprecated
public interface InvoiceAttachmentRepository extends MongoShopBaseRepository<InvoiceAttachment> {
    InvoiceAttachment getInvoiceAttachmentByInvoiceIdAndAttachmentId(final String invoiceId, final String attachmentId);

    void deleteInvoiceAttachment(final String invoiceId, final String attachmentId);


}

