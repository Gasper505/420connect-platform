package com.warehouse.core.domain.repositories.invoice;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.warehouse.core.domain.models.invoice.InvoiceHistory;

import java.util.HashMap;
import java.util.List;

public interface InvoiceHistoryRepository extends MongoShopBaseRepository<InvoiceHistory> {
    List<InvoiceHistory> getAllInvociesByInvoiceId(final String invoiceId, final String companyId, final String shopId);

    HashMap<String, List<InvoiceHistory>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId);
}
