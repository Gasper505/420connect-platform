package com.warehouse.core.domain.repositories.invoice;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.rest.invoice.result.InvoicePaymentResult;

import java.util.HashMap;
import java.util.List;

public interface InvoicePaymentsRepository extends MongoShopBaseRepository<PaymentsReceived> {
    SearchResult<PaymentsReceived> getAllInvoicePaymentsByInvoiceId(final String companyId, final String invoiceId, final int start, final int limit, final String sortOption);

    InvoicePaymentResult getInvoicePaymentById(final String companyId, final String invoiceId, final String invoicePaymentId);

    Iterable<PaymentsReceived> getAllInvoicePaymentsByInvoiceIdList(final String companyId, final String shopId, final List<String> invoiceIdList, final int start, final int limit, final String sortOption);

    HashMap<String, List<PaymentsReceived>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId, String projection);

    long countAllInvoicePayments(final String companyId, final String shopId, final String invoiceId);

    DateSearchResult<PaymentsReceived> getAllInvoicePaymentsByDate(final String companyId, final String shopId, final long beforeDate, final long afterDate, final String sortOption);

    <E extends PaymentsReceived> SearchResult<E> getPaymentReceivedByInvoiceWithoutQbRef(String companyId, String shopId, long syncTime, int start, int limit, Class<E> clazz);

    <E extends PaymentsReceived> SearchResult<E> getPaymentReceivedByInvoiceWithQBError(String companyId, String shopId, long syncTime, long lastSyncTime, int start, int limit, Class<E> clazz);

    void updateDesktopPaymentReceivedQbErrorAndTime(String companyId, String shopId, String id, boolean qbErrored, long errorTime);

    void updateQbPaymentReceivedRef(String companyId, String shopId, String id, String paymentReceivedRef);

    void hardRemoveQuickBookDataInInvoicePayments(String companyId, String shopId);
}
