package com.warehouse.core.domain.repositories.invoice;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.rest.invoice.request.ProductRequest;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;

public interface InvoiceRepository extends MongoShopBaseRepository<Invoice> {
    SearchResult<Invoice> getInvoicesByProductRequestStatus(final String companyId, final String shopId, final String sortOptions, final int start, final int limit, final List<ProductRequest.RequestStatus> statusList);

    DateSearchResult<Invoice> findInvoicesWithDate(final String companyId, final String shopId, final long afterDate, final long beforeDate, final String sortOption);

    SearchResult<InvoiceResult> getAllShops(final String companyId, final String shopId, final int start, final int limit, List<Invoice.InvoiceStatus> statusList, final String sortOption);

    SearchResult<InvoiceResult> getAllInvoicesByCustomer(final String companyId, final String shopId, final String customerId, final List<Invoice.InvoiceStatus> statusList, final int start, final int limit, final String sortOption);

    SearchResult<InvoiceResult> getAllInvoicesByCustomerWithoutStatus(final String companyId, final String shopId, final String customerId, final int start, final int limit, final String sortOption);

    void bulkUpdateTermType(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, Invoice.InvoiceTerms invoiceTerms);

    void bulkUpdateRelatedEntity(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, final boolean relatedEntity);

    void bulkUpdateInvoiceStatus(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, Invoice.InvoiceStatus invoiceStatus);

    void bulkUpdatePaymentStatus(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, Invoice.PaymentStatus invoicePaymentStatus);

    void bulkDeleteInvoice(String companyId, String shopId, List<ObjectId> invoiceObjectIdList);

    void bulkUpdateStatus(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, boolean active);

    <E extends Invoice> SearchResult<E> getAllInvoices(final String companyId, final String shopId, final int start, final int limit, final String sortOption, Class<E> clazz);

    <E extends Invoice> SearchResult<E> getAllInvoicesByState(final String companyId, final String shopId, final boolean state, final int start, final int limit, final String sortOption, Class<E> clazz);

    Invoice getInvoiceByInvoiceNumber(final String companyId, final String invoiceNumber);

    Iterable<Invoice> getInvoicesByStatus(String companyId, String shopId, List<Invoice.InvoiceStatus> invoiceStatusList, List<Invoice.PaymentStatus> invoicePaymentList, String sortOptions, long startDatemillies, long endDateMillis);

    Iterable<Invoice> getInvoicesByStatusInvoiceDate(String companyId, String shopId, List<Invoice.InvoiceStatus> invoiceStatusList,
                                          List<Invoice.PaymentStatus> invoicePaymentList,
                                          long startDatemillies, long endDateMillis);


    Iterable<Invoice> getInvoicesNonDraft(String companyId, String shopId, String sortOptions, long startDatemillies, long endDateMillis);


    Iterable<InvoiceResult> getBracketSalesByShopId(String companyId, String shopId, int size, List<Invoice.InvoiceStatus> invoiceStatusList, List<Invoice.PaymentStatus> paymentStatus, long beforeDate, long afterDate);

    <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTerm(final String companyId, final String shopId, final int start, final int limit, final String sortOptionStr, final String searchTerm, final List<String> vendorIds, Class<E> clazz);

    <E extends ShopBaseModel> DateSearchResult<E> getAllInvoicesByDates(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz, String projections);

    <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByVendorList(final String companyId, final String shopId, boolean state, final int start, final int limit, final String sortOptionStr, final List<String> vendorIds, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTermAndState(final String companyId, final String shopId, final boolean state, final int start, final int limit, final String sortOptionStr, final String searchTerm, final List<String> vendorIds, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTermAndStatus(final String companyId, final String shopId, final int start, final int limit, final String sortOptionStr, final String searchTerm, final List<String> vendorIds, Class<E> clazz, final List<Invoice.InvoiceStatus> statusList);

    <E extends Invoice> SearchResult<E> getAllInvoicesByPaymentStatus(String companyId, String shopId, boolean state, List<Invoice.PaymentStatus> paymentStatus, List<Invoice.InvoiceStatus> invoiceStatus, int start, int limit, String sortOption, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTermAndStatus(final String companyId, final String shopId, final boolean state, List<Invoice.PaymentStatus> paymentStatus, List<Invoice.InvoiceStatus> invoiceStatus, final int start, final int limit, final String sortOptionStr, final String searchTerm, final List<String> vendorIds, Class<E> clazz);

    DateSearchResult<Invoice> getAllInvoicesByCustomerWithDate(final String companyId, final String shopId, final String customerId, final long afterDate, final long beforeDate, final String sortOption);

    DateSearchResult<Invoice> getAllInvoicesByEmployeeWithDate(final String companyId, final String shopId, final String salesPersonId, final long afterDate, final long beforeDate, final String sortOption);

    DateSearchResult<Invoice> getAllInvoicesByEmployeeAndCustomer(final String companyId, final String shopId, final String salesPersonId, final String customerId, final long afterDate, final long beforeDate, final String sortOption);

    <E extends Invoice> DateSearchResult<E> findItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit, Class<E> clazz);

    Iterable<Invoice> getInvoicesNonDraftByEmployee(String companyId, String shopId, String sortOptions, long startDate, long endDate, String employeeId);

    long countInvoicesByAdjustmentId(String companyId, String shopId, String adjustmentId);
    <E  extends Invoice> List<E> getLimitedInvoiceWithoutQbRef(String companyId, String shopId, long startTime, long endTime, List<Invoice.InvoiceStatus> invoiceStatuses, int start, int limit, Class<E> clazz);

    <E extends Invoice> List<E> getInvoiceByLimitsWithQBError(String companyId, String shopId, Long startTime, List<Invoice.InvoiceStatus> invoiceStatuses, int start, int limit, Class<E> clazz);

    <E extends Invoice> List<E> getLimitedInvoiceWithoutQbRef(String companyId, String shopId, long syncTime, List<Invoice.InvoiceStatus> invoiceStatuses, int start, int limit, Class<E> clazz);

    void updateTransactionQbErrorAndTime(String companyId, String shopId, String invoiceId, Boolean errored, long errorTime);

    <E extends Invoice> List<E> getQBExistInvoice(String companyId, String shopId, int start, int limit, long startTime, List<Invoice.InvoiceStatus> invoiceStatuses, Class<E> clazz);

    void updateQbDesktopRef(String companyId, String shopId, String txnID, String editSequence, String invoiceNumber);

    void updateQbErrorAndTime(String companyId, String shopId, String invoiceNumber, boolean errored, long errorTime);

    void updateInvoiceEditSequence(String companyId, String shopId, String invoiceRetRefNumber, String txnID, String editSequence);

    Map<String, Invoice> listAsMapByInvoiceNumber(String companyId, List<String> invoiceNumbers);

    Invoice updateInvoice(String companyId, String id, Invoice invoice);

    <E extends Invoice> SearchResult<E> getInvoiceByLimitsWithInvoiceIds(String companyId, String shopId, List<ObjectId> invoiceIds, Class<E> clazz);

    <E extends Invoice> SearchResult<E> getInvoiceByLimitsWithQBPaymentReceived(String companyId, String shopId, List<Invoice.PaymentStatus> paymentStatuses, long syncTime, int start, int limit, Class<E> clazz);

    <E extends Invoice> SearchResult<E> getInvoiceByLimitsWithQBPaymentReceived(String companyId, String shopId, long syncTime, long lastSyncTime, Class<E> clazz);

    void updateQbDesktopPaymentReceivedRef(String companyId, String shopId, String id, boolean paymentReceivedRef, boolean qbPaymentReceivedError, long qbPaymentReceivedErroredTime);

    Iterable<Invoice> getInvoicesByStatus(String companyId, String shopId, List<Invoice.InvoiceStatus> invoiceStatusList, List<Invoice.PaymentStatus> invoicePaymentList, String sortOptions, long startDatemillies, long endDateMillis, String projections);

    long countInvoicesByProductId(String companyId, String shopId,  List<String> productIds);

    void hardRemoveQuickBookDataInInvoices(String companyId, String shopId);

    void updateInvoiceStatus(String companyId, String invoiceId, boolean active, Invoice.InvoiceStatus status, Invoice.InvoiceStatus previousStatus);

    long countActiveInvoicesByCustomer(String companyId, String customerId);

}
