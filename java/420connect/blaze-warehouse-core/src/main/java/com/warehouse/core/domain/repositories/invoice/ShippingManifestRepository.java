package com.warehouse.core.domain.repositories.invoice;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface ShippingManifestRepository extends MongoShopBaseRepository<ShippingManifest> {
    SearchResult<ShippingManifestResult> getAllShippingManifest(final String companyId, final String sortOption, final int start, final int limit);

    SearchResult<ShippingManifestResult> getShippingManifestByInvoiceId(final String companyId, final String invoiceId, final String sortOption, final int start, final int limit);

    Long countShippingManifestForInvoiceId(final String companyId, final String invoiceId);

    void updateShippingManifestDetailsByInvoice(final String companyId, final String shopId, final String invoiceId, final BigDecimal totalInvoiceAmount, final BigDecimal balanceDue, final Invoice.InvoiceStatus invoiceStatus);

    HashMap<String, List<ShippingManifestResult>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId, String projections);

    SearchResult<ShippingManifestResult> getShippingManifestByInvoiceIdAndStatus(final String companyId, final String shopId, final String invoiceId, final List<ShippingManifest.ShippingManifestStatus> status, final String sortOption, final int start, final int limit);

    long countShippingManifestByStatus(String companyId, String shopId, String invoiceId, ShippingManifest.ShippingManifestStatus status);

    List<ShippingManifest> getAllShippingManifestByProductId(String companyId, String shopId, String sortOptions, List<String> productIds);

    Long countShippingManifestInProgress(final String companyId, final String invoiceId , ShippingManifest.ShippingManifestStatus status);

    void updateMetrcStatus(String shippingManifestId, boolean metrcStatus, String templateId, String metrcManifestNumber, long metrcSentTime);
    
    ShippingManifestResult getShippingManifest(String shippingManifestId);
}
