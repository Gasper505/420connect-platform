package com.warehouse.core.domain.repositories.invoice.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;
import com.warehouse.core.domain.repositories.invoice.InvoiceActivityLogRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InvoiceActivityLogRepositoryImpl extends ShopBaseRepositoryImpl<InvoiceActivityLog> implements InvoiceActivityLogRepository {

    @Inject
    public InvoiceActivityLogRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(InvoiceActivityLog.class, mongoDb);
    }

    @Override
    public SearchResult<InvoiceActivityLog> getInvoiceActivityLogByInvoiceId(String companyId, String shopId, String invoiceId, String sortOption, int start, int limit) {
        Iterable<InvoiceActivityLog> items = coll.find("{companyId:#,shopId:#,deleted:false,targetId:#}", companyId, shopId, invoiceId).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,targetId:#}", companyId, shopId, invoiceId);
        SearchResult<InvoiceActivityLog> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public HashMap<String, List<InvoiceActivityLog>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId) {
        Iterable<InvoiceActivityLog> items = coll.find("{companyId:#,shopId:#,deleted:false,targetId:{$in:#}}", companyId, shopId, invoiceId).sort("{modified:-1}").as(entityClazz);

        HashMap<String, List<InvoiceActivityLog>> itemsMap = new HashMap<>();
        if (items != null) {
            for (InvoiceActivityLog invoiceActivityLog : items) {
                List<InvoiceActivityLog> invoiceActivityLogList = new ArrayList<>();
                if (itemsMap.containsKey(invoiceActivityLog.getTargetId())) {
                    invoiceActivityLogList = itemsMap.get(invoiceActivityLog.getTargetId());
                }
                invoiceActivityLogList.add(invoiceActivityLog);
                itemsMap.put(invoiceActivityLog.getTargetId(), invoiceActivityLogList);
            }
        }
        return itemsMap;

    }
}
