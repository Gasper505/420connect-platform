package com.warehouse.core.domain.repositories.invoice.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.warehouse.core.domain.models.invoice.InvoiceAttachment;
import com.warehouse.core.domain.repositories.invoice.InvoiceAttachmentRepository;

import javax.inject.Inject;

@Deprecated
public class InvoiceAttachmentRepositoryImpl extends ShopBaseRepositoryImpl<InvoiceAttachment> implements InvoiceAttachmentRepository {

    @Inject
    public InvoiceAttachmentRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(InvoiceAttachment.class, mongoDb);
    }

    @Override
    public InvoiceAttachment getInvoiceAttachmentByInvoiceIdAndAttachmentId(String invoiceId, String attachmentId) {
        return coll.findOne("{invoiceId:#,companyAsset._id:#}", invoiceId, attachmentId).as(entityClazz);
    }

    @Override
    public void deleteInvoiceAttachment(String invoiceId, String attachmentId) {
        coll.remove("{invoiceId:#,companyAsset._id:#}", invoiceId, attachmentId);
    }
}
