package com.warehouse.core.domain.repositories.invoice.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.InvoiceHistory;
import com.warehouse.core.domain.repositories.invoice.InvoiceHistoryRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InvoiceHistoryRepositoryImpl extends ShopBaseRepositoryImpl<InvoiceHistory> implements InvoiceHistoryRepository {
    @Inject
    public InvoiceHistoryRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(InvoiceHistory.class, mongoDb);
    }

    @Override
    public List<InvoiceHistory> getAllInvociesByInvoiceId(String invoiceId, String companyId, String shopId) {
        Iterable<InvoiceHistory> items = coll.find("{companyId:#,shopId:#,deleted:false,invoiceId:#}", companyId, shopId, invoiceId).as(entityClazz);
        if (items != null) {
            return Lists.newArrayList(items);
        }
        return null;
    }

    @Override
    public HashMap<String, List<InvoiceHistory>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId) {
        Iterable<InvoiceHistory> items = coll.find("{companyId:#,shopId:#,deleted:false,invoiceId:{$in:#}}", companyId, shopId, invoiceId).sort("{modified:-1}").as(entityClazz);

        HashMap<String, List<InvoiceHistory>> itemsMap = new HashMap<>();
        if (items != null) {
            for (InvoiceHistory invoiceHistory : items) {
                List<InvoiceHistory> invoiceHistoryList = new ArrayList<>();
                if (itemsMap.containsKey(invoiceHistory.getInvoiceId())) {
                    invoiceHistoryList = itemsMap.get(invoiceHistory.getInvoiceId());
                }
                invoiceHistoryList.add(invoiceHistory);
                itemsMap.put(invoiceHistory.getInvoiceId(), invoiceHistoryList);
            }
        }
        return itemsMap;

    }
}
