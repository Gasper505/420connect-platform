package com.warehouse.core.domain.repositories.invoice.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.rest.invoice.result.InvoicePaymentResult;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InvoicePaymentRepositoryImpl extends ShopBaseRepositoryImpl<PaymentsReceived> implements InvoicePaymentsRepository {

    @Inject
    public InvoicePaymentRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(PaymentsReceived.class, mongoDb);
    }

    @Override
    public SearchResult<PaymentsReceived> getAllInvoicePaymentsByInvoiceId(String companyId, String invoiceId, int start, int limit, String sortOption) {
        Iterable<PaymentsReceived> items = coll.find("{companyId:#,invoiceId:#,deleted:false}", companyId, invoiceId).sort(sortOption).skip(start).limit(limit).as(PaymentsReceived.class);
        long count = coll.count("{companyId:#,deleted:false,invoiceId:#}", companyId, invoiceId);
        SearchResult<PaymentsReceived> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public InvoicePaymentResult getInvoicePaymentById(String companyId, String invoiceId, String invoicePaymentId) {
        return coll.findOne("{companyId:#,invoiceId:#,_id:#}", companyId, invoiceId, invoicePaymentId).as(InvoicePaymentResult.class);
    }

    @Override
    public Iterable<PaymentsReceived> getAllInvoicePaymentsByInvoiceIdList(String companyId, String shopId, List<String> invoiceId, int start, int limit, String sortOption) {
        Iterable<PaymentsReceived> items = coll.find("{companyId:#,shopId:#,invoiceId:{$in :#},deleted:false}", companyId, shopId, invoiceId).sort(sortOption).skip(start).limit(limit).as(PaymentsReceived.class);
        return items;
    }

    @Override
    public HashMap<String, List<PaymentsReceived>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId, String projection) {
        Iterable<PaymentsReceived> items = coll.find("{companyId:#,shopId:#,invoiceId:{$in :#},deleted:false}", companyId, shopId, invoiceId).projection(projection).sort("{modified:-1}").as(PaymentsReceived.class);

        HashMap<String, List<PaymentsReceived>> itemsMap = new HashMap<>();
        if (items != null) {
            for (PaymentsReceived paymentsReceived : items) {
                List<PaymentsReceived> paymentsReceivedList = new ArrayList<>();
                if (itemsMap.containsKey(paymentsReceived.getInvoiceId())) {
                    paymentsReceivedList = itemsMap.get(paymentsReceived.getInvoiceId());
                }
                paymentsReceivedList.add(paymentsReceived);
                itemsMap.put(paymentsReceived.getInvoiceId(), paymentsReceivedList);
            }
        }
        return itemsMap;
    }

    @Override
    public long countAllInvoicePayments(String companyId, String shopId, String invoiceId) {
        return coll.count("{ companyId:#,shopId:#,deleted:false,invoiceId:#}", companyId, shopId, invoiceId);
    }

    @Override
    public DateSearchResult<PaymentsReceived> getAllInvoicePaymentsByDate(final String companyId, final String shopId, final long beforeDate, final long afterDate, final String sortOption) {
        Iterable<PaymentsReceived> items = coll.find("{companyId:#,shopId:#,deleted:false,modified:{$lt:#, $gt:#}}", companyId, shopId, afterDate, beforeDate).sort(sortOption).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$lt:#, $gt:#}}", companyId, shopId, afterDate, beforeDate);
        DateSearchResult<PaymentsReceived> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends PaymentsReceived> SearchResult<E> getPaymentReceivedByInvoiceWithoutQbRef(String companyId, String shopId, long syncTime, int start, int limit, Class<E> clazz) {
        Iterable<E> payments = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopPaymentRef:{$exists: false}, qbErrored:false, created:{$gt:#}}", companyId, shopId, syncTime).skip(start).limit(limit).as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(payments));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public <E extends PaymentsReceived> SearchResult<E> getPaymentReceivedByInvoiceWithQBError(String companyId, String shopId, long syncTime, long syncLastTime, int start, int limit, Class<E> clazz) {
        Iterable<E> payments = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopPaymentRef:{$exists: false}, qbErrored:true, created:{$gt:#}, modified:{$gt:#}}", companyId, shopId, syncTime, syncLastTime).skip(start).limit(limit).as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(payments));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public void updateDesktopPaymentReceivedQbErrorAndTime(String companyId, String shopId, String id, boolean qbErrored, long errorTime) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(id)).with("{$set: {qbErrored:#, errorTime:#}}", qbErrored, errorTime);
    }

    @Override
    public void updateQbPaymentReceivedRef(String companyId, String shopId, String id, String paymentReceivedRef) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(id)).with("{$set: {qbDesktopPaymentRef:#}}", paymentReceivedRef);
    }

    @Override
    public void hardRemoveQuickBookDataInInvoicePayments(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {qbDesktopPaymentRef:1,qbErrored:1, errorTime:1}}");
    }
}
