package com.warehouse.core.domain.repositories.invoice.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.rest.invoice.request.ProductRequest;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class InvoiceRepositoryImpl extends ShopBaseRepositoryImpl<Invoice> implements InvoiceRepository {

    @Inject
    public InvoiceRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(Invoice.class, mongoDb);
    }

    @Override
    public SearchResult<Invoice> getInvoicesByProductRequestStatus(String companyId, String shopId, String sortOptions, int start, int limit, List<ProductRequest.RequestStatus> statusList) {
        Iterable<Invoice> items = coll.find("{companyId:#,shopId:#,productRequests.status:{$in:#}}", companyId, shopId, statusList).sort(sortOptions).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,productRequests.status:{$in:#}}", companyId, shopId, statusList);
        SearchResult<Invoice> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Invoice> findInvoicesWithDate(String companyId, String shopId, long afterDate, long beforeDate, String sortOption) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<Invoice> items = coll.find("{companyId:#,shopId:#,deleted:false, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort(sortOption).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<Invoice> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<InvoiceResult> getAllShops(String companyId, String shopId, int start, int limit, List<Invoice.InvoiceStatus> statusList, String sortOption) {
        Iterable<InvoiceResult> items = coll.find("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},active:true}", companyId, shopId, statusList).sort(sortOption).skip(start).limit(limit).as(InvoiceResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},active:true}", companyId, shopId, statusList);
        SearchResult<InvoiceResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<InvoiceResult> getAllInvoicesByCustomer(String companyId, String shopId, String customerId, List<Invoice.InvoiceStatus> statusList, int start, int limit, String sortOption) {
        Iterable<InvoiceResult> items = coll.find("{companyId:#,shopId:#,customerId:#,invoiceStatus:{$in:#},deleted:false}", companyId, shopId, customerId, statusList).sort(sortOption).skip(start).limit(limit).as(InvoiceResult.class);
        long count = coll.count("{companyId:#,shopId:#,customerId:#,invoiceStatus:{$in:#},deleted:false}", companyId, shopId, customerId, statusList);
        SearchResult<InvoiceResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<InvoiceResult> getAllInvoicesByCustomerWithoutStatus(String companyId, String shopId, String customerId, int start, int limit, String sortOption) {
        Iterable<InvoiceResult> items = coll.find("{companyId:#,shopId:#,customerId:#,deleted:false}", companyId, shopId, customerId).sort(sortOption).skip(start).limit(limit).as(InvoiceResult.class);
        long count = coll.count("{companyId:#,shopId:#,customerId:#,deleted:false}", companyId, shopId, customerId);
        SearchResult<InvoiceResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Invoice> SearchResult<E> getAllInvoices(String companyId, String shopId, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Invoice> SearchResult<E> getAllInvoicesByState(String companyId, String shopId, boolean state, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,active:#}", companyId, shopId, state).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:#}", companyId, shopId, state);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkUpdateTermType(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, Invoice.InvoiceTerms invoiceTerms) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, invoiceObjectIdList).multi().with("{$set:{invoiceTerms:#,modified:#}}", invoiceTerms, DateTime.now().getMillis());

    }

    @Override
    public void bulkUpdateRelatedEntity(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, boolean relatedEntity) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, invoiceObjectIdList).multi().with("{$set:{relatedEntity:#,modified:#}}", relatedEntity, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateInvoiceStatus(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, Invoice.InvoiceStatus invoiceStatus) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, invoiceObjectIdList).multi().with("{$set:{invoiceStatus:#,modified:#}}", invoiceStatus, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdatePaymentStatus(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, Invoice.PaymentStatus invoicePaymentStatus) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, invoiceObjectIdList).multi().with("{$set:{invoicePaymentStatus:#,modified:#}}", invoicePaymentStatus, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateStatus(String companyId, String shopId, List<ObjectId> invoiceObjectIdList, boolean active) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, invoiceObjectIdList).multi().with("{$set:{active:#}}", active);
    }


    @Override
    public void bulkDeleteInvoice(String companyId, String shopId, List<ObjectId> invoiceObjectIdList) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, invoiceObjectIdList).multi().with("{$set:{deleted:true,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public Invoice getInvoiceByInvoiceNumber(String companyId, String invoiceNumber) {
        return coll.findOne("{companyId:#,invoiceNumber:#,deleted:false}", companyId, invoiceNumber).as(entityClazz);
    }

    @Override
    public Iterable<Invoice> getInvoicesByStatus(String companyId, String shopId, List<Invoice.InvoiceStatus> invoiceStatusList, List<Invoice.PaymentStatus> invoicePaymentList, String sortOptions, long startDateMillis, long endDateMillis) {
        return coll.find("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},invoicePaymentStatus:{$in:#},modified : {$gt:#,$lt:#}}", companyId, shopId, invoiceStatusList, invoicePaymentList, startDateMillis, endDateMillis).sort(sortOptions).as(entityClazz);
    }

    @Override
    public Iterable<Invoice> getInvoicesByStatusInvoiceDate(String companyId, String shopId, List<Invoice.InvoiceStatus> invoiceStatusList,
                                                     List<Invoice.PaymentStatus> invoicePaymentList,
                                                     long startDateMillies, long endDateMillis) {
        return coll.find("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},invoicePaymentStatus:{$in:#},invoiceDate : {$gt:#,$lt:#}}",
                companyId, shopId, invoiceStatusList, invoicePaymentList, startDateMillies, endDateMillis).sort("{invoiceDate:1}").as(entityClazz);
    }

    @Override
    public Iterable<Invoice> getInvoicesNonDraft(String companyId, String shopId, String sortOptions, long startDateMillis, long endDateMillis) {
        List<Invoice.InvoiceStatus> invoiceStatusList = new ArrayList<>();
        invoiceStatusList.add(Invoice.InvoiceStatus.COMPLETED);
        invoiceStatusList.add(Invoice.InvoiceStatus.IN_PROGRESS);
        invoiceStatusList.add(Invoice.InvoiceStatus.SENT);
        return coll.find("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},modified : {$gt:#,$lt:#}}", companyId, shopId, invoiceStatusList, startDateMillis, endDateMillis).sort(sortOptions).as(entityClazz);
    }

    @Override
    public Iterable<InvoiceResult> getBracketSalesByShopId(String companyId, String shopId, int size, List<Invoice.InvoiceStatus> invoiceStatusList, List<Invoice.PaymentStatus> paymentStatus, long afterDate, long beforeDate) {
        return coll.find("{companyId:#, shopId: #, deleted:false,invoiceStatus:{$in:#},invoicePaymentStatus:{$in:#},invoiceDate:{$gt:#,$lt:#}}",
                companyId, shopId, invoiceStatusList, paymentStatus, afterDate,beforeDate)
                .sort("{invoiceDate: -1}")
                .limit(size)
                .as(InvoiceResult.class);
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTerm(String companyId, String shopId, int skip, int limit, String sortOptionStr, String searchTerm, List<String> vendorIds, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> invoices = coll.find("{$and: [{companyId:#,shopId:#,deleted:false},{$or:[{invoiceNumber:#},{invoiceStatus:#},{invoicePaymentStatus:#},{customerId:{$in:#}}]}]}", companyId, shopId, pattern, pattern, pattern, vendorIds).sort(sortOptionStr).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false},{$or:[{invoiceNumber:#},{invoiceStatus:#},{invoicePaymentStatus:#},{customerId:{$in:#}}]}]}", companyId, shopId, pattern, pattern, pattern, vendorIds);
        searchResult.setValues(Lists.newArrayList(invoices));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends ShopBaseModel> DateSearchResult<E> getAllInvoicesByDates(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz, String projections) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,modified:{$lt:#,$gt:#}}", companyId, shopId, beforeDate, afterDate).projection(projections).sort("{modified:-1}").as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$lt:#,$gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<E> searchResult = new DateSearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setAfterDate(afterDate);
        searchResult.setBeforeDate(beforeDate);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByVendorList(String companyId, String shopId, boolean state, int start, int limit, String sortOptionStr, List<String> vendorIds, Class<E> clazz) {
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> invoices = coll.find("{companyId:#,shopId:#,deleted:false,active:#,customerId:{$in:#}}", companyId, shopId, state, vendorIds).sort(sortOptionStr).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:#,customerId:{$in:#}}", companyId, shopId, state, vendorIds);
        searchResult.setValues(Lists.newArrayList(invoices));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTermAndState(String companyId, String shopId, boolean state, int skip, int limit, String sortOptionStr, String searchTerm, List<String> vendorIds, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> invoices = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,active:#},{$or:[{invoiceNumber:#},{invoiceStatus:#},{invoicePaymentStatus:#},{customerId:{$in:#}}]}]}", companyId, shopId, state, pattern, pattern, pattern, vendorIds).sort(sortOptionStr).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,active:#},{$or:[{invoiceNumber:#},{invoiceStatus:#},{invoicePaymentStatus:#},{customerId:{$in:#}}]}]}", companyId, shopId, state, pattern, pattern, pattern, vendorIds);
        searchResult.setValues(Lists.newArrayList(invoices));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTermAndStatus(String companyId, String shopId, int skip, int limit, String sortOptionStr, String searchTerm, List<String> vendorIds, Class<E> clazz, List<Invoice.InvoiceStatus> statusList) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> invoices = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#}},{$or:[{invoiceNumber:#},{invoiceStatus:#},{invoicePaymentStatus:#},{customerId:{$in:#}}]}]}", companyId, shopId, statusList, pattern, pattern, pattern, vendorIds).sort(sortOptionStr).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#}},{$or:[{invoiceNumber:#},{invoiceStatus:#},{invoicePaymentStatus:#},{customerId:{$in:#}}]}]}", companyId, shopId, statusList, pattern, pattern, pattern, vendorIds);
        searchResult.setValues(Lists.newArrayList(invoices));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends Invoice> SearchResult<E> getAllInvoicesByPaymentStatus(String companyId, String shopId, boolean state, List<Invoice.PaymentStatus> paymentStatus, List<Invoice.InvoiceStatus> invoiceStatus, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,active:#,invoicePaymentStatus:{$in:#}, invoiceStatus:{$in:#}}", companyId, shopId, state, paymentStatus, invoiceStatus).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:#,invoicePaymentStatus:{$in:#},invoiceStatus:{$in:#}}", companyId, shopId, state, paymentStatus, invoiceStatus);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllInvoicesByTermAndStatus(String companyId, String shopId, boolean state, List<Invoice.PaymentStatus> paymentStatus, List<Invoice.InvoiceStatus> invoiceStatus, int start, int limit, String sortOptionStr, String searchTerm, List<String> vendorIds, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> invoices = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,active:#,invoicePaymentStatus:{$in:#}, invoiceStatus:{$in:#}},{$or:[{invoiceNumber:#},{customerId:{$in:#}}]}]}", companyId, shopId, state, paymentStatus, invoiceStatus, pattern, vendorIds).sort(sortOptionStr).skip(start).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,active:#,invoicePaymentStatus:{$in:#}, invoiceStatus:{$in:#}},{$or:[{invoiceNumber:#},{customerId:{$in:#}}]}]}", companyId, shopId, state, paymentStatus, invoiceStatus, pattern, vendorIds);
        searchResult.setValues(Lists.newArrayList(invoices));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;

    }

    @Override
    public DateSearchResult<Invoice> getAllInvoicesByCustomerWithDate(String companyId, String shopId, String customerId, long afterDate, long beforeDate, String sortOption) {
        Iterable<Invoice> items = coll.find("{companyId:#,shopId:#,customerId:#,deleted:false,active:true,modified:{$lt:#,$gt:#}}", companyId, shopId, customerId, beforeDate, afterDate).sort(sortOption).as(Invoice.class);
        long count = coll.count("{companyId:#,shopId:#,customerId:#,deleted:false,active:true,modified:{$lt:#,$gt:#}}", companyId, shopId, customerId, beforeDate, afterDate);
        DateSearchResult<Invoice> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Invoice> getAllInvoicesByEmployeeWithDate(String companyId, String shopId, String salesPersonId, long afterDate, long beforeDate, String sortOption) {
        Iterable<Invoice> items = coll.find("{companyId:#,shopId:#,salesPersonId:#,deleted:false,active:true,modified:{$lt:#,$gt:#}}", companyId, shopId, salesPersonId, beforeDate, afterDate).sort(sortOption).as(Invoice.class);
        long count = coll.count("{companyId:#,shopId:#,salesPersonId:#,deleted:false,active:true,modified:{$lt:#,$gt:#}}", companyId, shopId, salesPersonId, beforeDate, afterDate);
        DateSearchResult<Invoice> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Invoice> getAllInvoicesByEmployeeAndCustomer(String companyId, String shopId, String salesPersonId, String customerId, long afterDate, long beforeDate, String sortOption) {
        Iterable<Invoice> items = coll.find("{companyId:#,shopId:#,salesPersonId:#,customerId:#,deleted:false,active:true,modified:{$lt:#,$gt:#}}", companyId, shopId, salesPersonId, customerId, beforeDate, afterDate).sort(sortOption).as(Invoice.class);
        long count = coll.count("{companyId:#,shopId:#,salesPersonId:#,customerId:#,deleted:false,active:true,modified:{$lt:#,$gt:#}}", companyId, shopId, salesPersonId, customerId, beforeDate, afterDate);
        DateSearchResult<Invoice> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Invoice> DateSearchResult<E> findItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort("{modified:-1}").skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<Invoice> getInvoicesNonDraftByEmployee(String companyId, String shopId, String sortOptions, long startDate, long endDate, String employeeId) {
        List<Invoice.InvoiceStatus> invoiceStatusList = new ArrayList<>();
        invoiceStatusList.add(Invoice.InvoiceStatus.COMPLETED);
        invoiceStatusList.add(Invoice.InvoiceStatus.IN_PROGRESS);
        invoiceStatusList.add(Invoice.InvoiceStatus.SENT);
        return coll.find("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},modified : {$gt:#,$lt:#}, salesPersonId:#}",
                companyId, shopId, invoiceStatusList, startDate, endDate, employeeId)
                .sort(sortOptions)
                .as(entityClazz);
    }

    @Override
    public <E extends Invoice> List<E> getLimitedInvoiceWithoutQbRef(String companyId, String shopId, long startTime, long endTime, List<Invoice.InvoiceStatus> invoiceStatuses, int start, int limit, Class<E> clazz) {
        Iterable<E> invoices = coll.find("{companyId:#, shopId:#, deleted:false, modified: {$gt:#, $lt:#}, qbDesktopRef:{$exists: false}, invoiceStatus:{$in:#}}", companyId, shopId, startTime, endTime, invoiceStatuses)
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(invoices);
    }

    @Override
    public <E extends Invoice> List<E> getInvoiceByLimitsWithQBError(String companyId, String shopId, Long errorTime, List<Invoice.InvoiceStatus> invoiceStatuses, int start, int limit, Class<E> clazz) {
        Iterable<E> invoices =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: false}, qbError:true, modified:{$gt:#}, invoiceStatus:{$in:#}}", companyId, shopId, errorTime, invoiceStatuses)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(invoices);
    }

    @Override
    public <E extends Invoice> List<E> getLimitedInvoiceWithoutQbRef(String companyId, String shopId, long syncTime, List<Invoice.InvoiceStatus> invoiceStatuses, int start, int limit, Class<E> clazz) {
        Iterable<E> invoices = coll.find("{companyId:#, shopId:#, deleted:false, modified: {$gte:#}, qbDesktopRef:{$exists: false}, invoiceStatus:{$in:#}, qbError:false}", companyId, shopId, syncTime, invoiceStatuses)
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(invoices);
    }

    @Override
    public void updateTransactionQbErrorAndTime(String companyId, String shopId, String invoiceId, Boolean errored, long errorTime) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, invoiceId).with("{$set: {qbError:#, errorTime:#}}", errored, errorTime);
    }

    @Override
    public <E extends Invoice> List<E> getQBExistInvoice(String companyId, String shopId, int start, int limit, long startTime, List<Invoice.InvoiceStatus> invoiceStatuses, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: true}, editSequence:{$exists: true}, modified:{$gt:#}, invoiceStatus:{$in:#}}", companyId, shopId, startTime, invoiceStatuses)
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void updateQbDesktopRef(String companyId, String shopId, String txnID, String editSequence, String invoiceNumber) {
        coll.update("{companyId:#, shopId:#, invoiceNumber:#}", companyId, shopId, invoiceNumber).with("{$set:{qbDesktopRef:#,editSequence:#}}", txnID, editSequence);
    }

    @Override
    public void updateQbErrorAndTime(String companyId, String shopId, String invoiceNumber, boolean errored, long errorTime) {
        coll.update("{companyId:#, shopId:#, invoiceNumber:#}", companyId, shopId, invoiceNumber).with("{$set: {qbError:#, errorTime:#}}", errored, errorTime);
    }

    @Override
    public void updateInvoiceEditSequence(String companyId, String shopId, String invoiceNumber, String txnID, String editSequence) {
        coll.update("{companyId:#, shopId:#, invoiceNumber:#}", companyId, shopId, invoiceNumber).with("{$set: {qbDesktopRef:#, editSequence:#}}", txnID, editSequence);
    }

    @Override
    public Map<String, Invoice> listAsMapByInvoiceNumber(String companyId, List<String> invoiceNumbers) {
        Iterable<Invoice> invoices = coll.find("{companyId:#,invoiceNumber : {$in: #}}", companyId, invoiceNumbers).as(entityClazz);
        return asMap(invoices);
    }

    @Override
    public Invoice updateInvoice(String companyId, String id, Invoice invoice) {
        coll.update("{companyId:#,_id: #}", companyId, new ObjectId(id)).with(invoice);
        return invoice;
    }

    @Override
    public <E extends Invoice> SearchResult<E> getInvoiceByLimitsWithInvoiceIds(String companyId, String shopId, List<ObjectId> invoiceIds, Class<E> clazz) {
        Iterable<E> items  = coll.find("{companyId:#, shopId:#, deleted:false, _id:{$in:#}, qbDesktopRef:{$exists: true}}", companyId, shopId, invoiceIds)
                .sort("{modified:-1}")
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        return results;
    }

    @Override
    public <E extends Invoice> SearchResult<E> getInvoiceByLimitsWithQBPaymentReceived(String companyId, String shopId, List<Invoice.PaymentStatus> paymentStatuses, long syncTime, int start, int limit, Class<E> clazz) {
        Iterable<E> items  = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: true}, qbPaymentReceived : true, invoicePaymentStatus : {$in:#}, created:{$gt:#}}", companyId, shopId, paymentStatuses, syncTime)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public <E extends Invoice> SearchResult<E> getInvoiceByLimitsWithQBPaymentReceived(String companyId, String shopId, long syncTime, long lastSyncTime, Class<E> clazz) {
        Iterable<E> items  = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: true}, qbPaymentReceived : false, qbPaymentReceivedError: true, created:{$gt:#}, modified:{$gt:#}}", companyId, shopId, syncTime, lastSyncTime)
                .sort("{modified:-1}")
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        return results;
    }

    @Override
    public void updateQbDesktopPaymentReceivedRef(String companyId, String shopId, String id, boolean paymentReceivedRef, boolean qbPaymentReceivedError, long qbPaymentReceivedErroredTime) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(id)).with("{$set: {qbPaymentReceived:#, qbPaymentReceivedError:#, qbPaymentReceivedErrorTime:#}}",paymentReceivedRef, qbPaymentReceivedError, qbPaymentReceivedErroredTime);

    }

    @Override
    public long countInvoicesByAdjustmentId(String companyId, String shopId, String adjustmentId) {
        return coll.count("{companyId:#, shopId:#, deleted:false, cart.adjustmentInfoList.adjustmentId:#}", companyId, shopId, adjustmentId);
    }

    @Override
    public Iterable<Invoice> getInvoicesByStatus(String companyId, String shopId, List<Invoice.InvoiceStatus> invoiceStatusList, List<Invoice.PaymentStatus> invoicePaymentList, String sortOptions, long startDateMillis, long endDateMillis, String projections) {
        return coll.find("{companyId:#,shopId:#,deleted:false,invoiceStatus:{$in:#},invoicePaymentStatus:{$in:#},modified : {$gt:#,$lt:#}}", companyId, shopId, invoiceStatusList, invoicePaymentList, startDateMillis, endDateMillis).sort(sortOptions).projection(projections).as(entityClazz);
    }

    @Override
    public long countInvoicesByProductId(String companyId, String shopId, List<String> productIds) {
     return coll.count("{companyId: #, shopId: #, deleted : false, invoiceStatus :{ $nin : ['CANCELLED','COMPLETED']}, cart.items.productId:{$in:#}}", companyId, shopId, productIds);
    }


    @Override
    public void hardRemoveQuickBookDataInInvoices(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {qbDesktopRef:1,editSequence:1, qbListId:1, qbErrored:1, errorTime:1, qbPaymentReceived:1,qbPaymentReceivedError:1,qbPaymentReceivedErrorTime:1}}");
    }

    @Override
    public void updateInvoiceStatus(String companyId, String invoiceId, boolean active, Invoice.InvoiceStatus status, Invoice.InvoiceStatus previousStatus) {
          coll.update("{companyId:#, _id:#}", companyId, new ObjectId(invoiceId)).with("{$set: {active:#, invoiceStatus:#, invoicePreviousStatus:#, modified:#}}", active, status, previousStatus, DateTime.now().getMillis());

    }

    @Override
    public long countActiveInvoicesByCustomer(String companyId, String customerId) {
        return coll.count("{companyId:#, deleted:false, customerId:#, invoiceStatus:{ $nin:['CANCELLED','COMPLETED']}}", companyId, customerId);
    }
}
