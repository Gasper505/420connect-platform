package com.warehouse.core.domain.repositories.invoice.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.domain.repositories.invoice.ShippingManifestRepository;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShippingManifestRepositoryImpl extends ShopBaseRepositoryImpl<ShippingManifest> implements ShippingManifestRepository {

    @Inject
    public ShippingManifestRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ShippingManifest.class, mongoManager);
    }

    @Override
    public SearchResult<ShippingManifestResult> getAllShippingManifest(String companyId, String sortOption, int start, int limit) {
        Iterable<ShippingManifestResult> items = coll.find("{companyId:#,deleted:false}", companyId).sort(sortOption).skip(start).limit(limit).as(ShippingManifestResult.class);
        long count = coll.count("{companyId:#,deleted:false}", companyId);
        SearchResult<ShippingManifestResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<ShippingManifestResult> getShippingManifestByInvoiceId(String companyId, String invoiceId, String sortOption, int start, int limit) {
        Iterable<ShippingManifestResult> items = coll.find("{companyId:#,invoiceId:#,deleted:false}", companyId, invoiceId).sort(sortOption).skip(start).limit(limit).as(ShippingManifestResult.class);
        long count = coll.count("{companyId:#,invoiceId:#,deleted:false}", companyId, invoiceId);
        SearchResult<ShippingManifestResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Long countShippingManifestForInvoiceId(String companyId, String invoiceId) {
        return coll.count("{companyId:#,invoiceId:#}", companyId, invoiceId);
    }

    @Override
    public void updateShippingManifestDetailsByInvoice(String companyId, String shopId, String invoiceId, BigDecimal totalInvoiceAmount, BigDecimal balanceDue, Invoice.InvoiceStatus invoiceStatus) {
        coll.update("{companyId:#,shopId:#,deleted:false,invoiceId:#}", companyId, shopId, invoiceId).multi().with("{$set:{invoiceBalanceDue:#,invoiceAmount:#,invoiceStatus:#,modified:#}}", balanceDue.doubleValue(), totalInvoiceAmount.doubleValue(), invoiceStatus, DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, List<ShippingManifestResult>> findItemsInAsMapByInvoice(String companyId, String shopId, List<String> invoiceId, String projections) {
        Iterable<ShippingManifestResult> items = coll.find("{companyId:#,shopId:#,deleted:false,invoiceId:{$in:#}}", companyId, shopId, invoiceId).projection(projections).sort("{modified:-1}").as(ShippingManifestResult.class);

        HashMap<String, List<ShippingManifestResult>> shippingManifestInvoiceMap = new HashMap<>();
        if (items != null) {
            for (ShippingManifestResult shippingManifest : items) {
                List<ShippingManifestResult> shippingManifestList = new ArrayList<>();
                if (shippingManifestInvoiceMap.containsKey(shippingManifest.getInvoiceId())) {
                    shippingManifestList = shippingManifestInvoiceMap.get(shippingManifest.getInvoiceId());
                }
                shippingManifestList.add(shippingManifest);
                shippingManifestInvoiceMap.put(shippingManifest.getInvoiceId(), shippingManifestList);
            }
        }
        return shippingManifestInvoiceMap;

    }

    @Override
    public SearchResult<ShippingManifestResult> getShippingManifestByInvoiceIdAndStatus(String companyId, String shopId, String invoiceId, List<ShippingManifest.ShippingManifestStatus> status, String sortOption, int start, int limit) {
        Iterable<ShippingManifestResult> items = coll.find("{companyId:#,shopId:#,deleted:false,invoiceId:#,status:{$in:#}}", companyId, shopId, invoiceId, status).sort(sortOption).skip(start).limit(limit).as(ShippingManifestResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,invoiceId:#,status:{$in:#}}", companyId, shopId, invoiceId, status);

        SearchResult<ShippingManifestResult> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setLimit(limit);
        searchResult.setSkip(start);

        return searchResult;
    }

    @Override
    public long countShippingManifestByStatus(String companyId, String shopId, String invoiceId, ShippingManifest.ShippingManifestStatus status) {
        return coll.count("{companyId:#,shopId:#,deleted:false,invoiceId:#,status:#}", companyId, shopId, invoiceId, status);

    }

    @Override
    public List<ShippingManifest> getAllShippingManifestByProductId(String companyId, String shopId, String sortOptions, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<ShippingManifest> items = coll.aggregate("{$match:{companyId:#,shopId:#,status:{ $nin: ['Completed'] },deleted:false}}", companyId, shopId)
                .and("{$match: {productMetrcInfo : { $elemMatch: {productId: {$in:#}}}}}", productIds)
                .options(aggregationOptions)
                .as(entityClazz);
        return Lists.newArrayList((Iterable<ShippingManifest>) items);
    }

    @Override
    public Long countShippingManifestInProgress(String companyId, String invoiceId, ShippingManifest.ShippingManifestStatus status) {
        return coll.count("{companyId:#,invoiceId:#,deleted:false,status:#}", companyId, invoiceId, status);
    }

    @Override
    public void updateMetrcStatus(String shippingManifestId, boolean metrcStatus, String templateId, String metrcManifestNumber, long metrcSentTime) {
        coll.update("{ _id : # }", new ObjectId(shippingManifestId)).with("{ $set: {metrc : #, metrcSentTime: #, metrcTransferTemplateId:#, metrcManifestNumber:#} }", metrcStatus, metrcSentTime, templateId, metrcManifestNumber);
    }
    
    @Override
    public ShippingManifestResult getShippingManifest(String shippingManifestId){
        ShippingManifestResult result = null;
        if(shippingManifestId != null && ObjectId.isValid(shippingManifestId)) {
            result = coll.findOne("{_id:#}", new ObjectId(shippingManifestId)).as(ShippingManifestResult.class);
        }

        return result;
    }
}