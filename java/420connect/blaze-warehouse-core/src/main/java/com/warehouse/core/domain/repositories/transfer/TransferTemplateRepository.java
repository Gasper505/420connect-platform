package com.warehouse.core.domain.repositories.transfer;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.transfer.TransferTemplate;

public interface TransferTemplateRepository extends MongoShopBaseRepository<TransferTemplate> {
    <E extends TransferTemplate> SearchResult<E> getAllTransferTemplate(final String companyId, final String shopId, final int start, final int limit, final String sortOption, Class<E> clazz);
}
