package com.warehouse.core.domain.repositories.transfer.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.transfer.TransferTemplate;
import com.warehouse.core.domain.repositories.transfer.TransferTemplateRepository;

import javax.inject.Inject;

public class TransferTemplateRepositoryImpl extends ShopBaseRepositoryImpl<TransferTemplate> implements TransferTemplateRepository {
    @Inject
    public TransferTemplateRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(TransferTemplate.class, mongoDb);
    }

    @Override
    public <E extends TransferTemplate> SearchResult<E> getAllTransferTemplate(String companyId, String shopId, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);

        return results;
    }


}
