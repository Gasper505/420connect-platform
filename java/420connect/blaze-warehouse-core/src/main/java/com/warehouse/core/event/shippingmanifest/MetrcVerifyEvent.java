package com.warehouse.core.event.shippingmanifest;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MetrcVerifyEvent extends BiDirectionalBlazeEvent<List<String>> {
    private String companyId;
    private String shopId;
    private List<String> reqMetrcLabel = new ArrayList<>();
    private ShippingManifestResult shippingManifest;
    private boolean createPackage;
    private Set<String> requestMetrcInfoKeys;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public List<String> getReqMetrcLabel() {
        return reqMetrcLabel;
    }

    public void setReqMetrcLabel(List<String> reqMetrcLabel) {
        this.reqMetrcLabel = reqMetrcLabel;
    }

    public ShippingManifestResult getShippingManifest() {
        return shippingManifest;
    }

    public void setShippingManifest(ShippingManifestResult shippingManifest) {
        this.shippingManifest = shippingManifest;
    }

    public boolean isCreatePackage() {
        return createPackage;
    }

    public void setCreatePackage(boolean createPackage) {
        this.createPackage = createPackage;
    }

    public Set<String> getRequestMetrcInfoKeys() {
        return requestMetrcInfoKeys;
    }

    public void setRequestMetrcInfoKeys(Set<String> requestMetrcInfoKeys) {
        this.requestMetrcInfoKeys = requestMetrcInfoKeys;
    }
}
