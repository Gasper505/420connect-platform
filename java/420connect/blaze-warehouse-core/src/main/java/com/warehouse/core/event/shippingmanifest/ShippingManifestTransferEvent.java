package com.warehouse.core.event.shippingmanifest;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;

public class ShippingManifestTransferEvent extends BiDirectionalBlazeEvent {
    private ShippingManifestResult shippingManifest;
    private Invoice invoice;

    public ShippingManifestResult getShippingManifest() {
        return shippingManifest;
    }

    public void setShippingManifest(ShippingManifestResult shippingManifest) {
        this.shippingManifest = shippingManifest;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
