package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AccountsReceivableGatherer implements Gatherer {


    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;
    @Inject
    private VendorRepository vendorRepository;

    private String[] attrs = new String[]{"Company", "Invoice #", "Amount", "Paid", "Outstanding"};

    private List reportHeaders = new ArrayList(attrs.length);
    private Map fieldTypes = new HashMap();

    public AccountsReceivableGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Account Receivable Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(),  "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<String> invoiceIds = new ArrayList();
        Set<ObjectId> vendorIds = new HashSet<>();

        for (Invoice invoice : invoiceResults) {
            invoiceIds.add(invoice.getId());
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }
        }

        HashMap<String, List<PaymentsReceived>> invoicePaymentMap = invoicePaymentsRepository.findItemsInAsMapByInvoice(filter.getCompanyId(), filter.getShopId(), invoiceIds, "");
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));

        Vendor vendor = null;
        String nAvail = "N/A";
        List<PaymentsReceived> invoicePayments;
        double amountPaid;
        double balanceDue;

        for (Invoice invoice : invoiceResults) {
            HashMap<String, Object> data = new HashMap<>();

            vendor = vendorHashMap.get(invoice.getCustomerId());

            invoicePayments = invoicePaymentMap.getOrDefault(invoice.getId(), new ArrayList<>());

            amountPaid = 0;

            for (PaymentsReceived invoicePayment : invoicePayments) {
                if (invoicePayment.isDeleted()) {
                    continue;
                }
                amountPaid += invoicePayment.getAmountPaid().doubleValue();
            }

            balanceDue = invoice.getCart().getTotal().doubleValue() - amountPaid;
            balanceDue = (balanceDue < 0) ? 0 : balanceDue;

            int i = 0;
            data.put(attrs[i++], vendor != null && StringUtils.isNotBlank(vendor.getName()) ? vendor.getName() : nAvail);
            data.put(attrs[i++], invoice.getInvoiceNumber());
            data.put(attrs[i++], invoice.getCart().getTotal().doubleValue());
            data.put(attrs[i++], amountPaid);
            data.put(attrs[i++], balanceDue);

            report.add(data);
        }

        return report;
    }
}
