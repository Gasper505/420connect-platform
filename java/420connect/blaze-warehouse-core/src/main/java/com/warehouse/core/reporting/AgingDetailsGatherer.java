package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class AgingDetailsGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentRepository;

    private String[] attrs = new String[]{"Invoice No", "Date", "Due Date", "Overdue By", "Customer Name", "Sales Person Name", "Status", "Total Sales", "Total AL Tax", "Total Tax", "Total", "Payment Received", "Payment Remaining"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public AgingDetailsGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Aging Details Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);


        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(filter.getCompanyId());
        List<String> invoiceIdList = new ArrayList<>();

        if (invoiceResults != null) {
            for (Invoice invoiceResult : invoiceResults) {
                if (!Objects.isNull(invoiceResult)) {
                    invoiceIdList.add(invoiceResult.getId());
                }
            }
        }

        HashMap<String, List<PaymentsReceived>> paymentsMap = invoicePaymentRepository.findItemsInAsMapByInvoice(filter.getCompanyId(), filter.getShopId(), invoiceIdList, "");


        if (invoiceResults != null) {
            for (Invoice invoiceResult : invoiceResults) {
                if (Objects.isNull(invoiceResult)) {
                    continue;
                }
                Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
                if (shop == null) {
                    throw new BlazeInvalidArgException("Shop", "Shop is not found.");
                }
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], invoiceResult.getInvoiceNumber());
                data.put(attrs[1], ProcessorUtil.dateString(invoiceResult.getInvoiceDate()));
                data.put(attrs[2], ProcessorUtil.dateString(invoiceResult.getDueDate()));

                Long dueDate = invoiceResult.getCreated();
                DateTime startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                DateTime endDate = DateUtil.nowWithTimeZone(shop.getTimeZone());
                long daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());

                switch (invoiceResult.getInvoiceTerms()) {
                    case NET_7:
                        daysBetweenTwoDates = daysBetweenTwoDates - 7;
                        break;
                    case NET_15:
                        daysBetweenTwoDates = daysBetweenTwoDates - 15;
                        break;
                    case NET_30:
                        daysBetweenTwoDates = daysBetweenTwoDates - 30;
                        break;
                    case NET_45:
                        daysBetweenTwoDates = daysBetweenTwoDates - 45;
                        break;
                    case NET_60:
                        daysBetweenTwoDates = daysBetweenTwoDates - 60;
                        break;
                    case CUSTOM_DATE:
                        dueDate = invoiceResult.getDueDate();
                        startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                        daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                        break;
                    default:
                        break;
                }
                daysBetweenTwoDates = Math.abs(daysBetweenTwoDates);
                String days = daysBetweenTwoDates + " days";
                if (daysBetweenTwoDates == 0) {
                    days = "Today";
                } else if (daysBetweenTwoDates == 1) {
                    days = daysBetweenTwoDates + " day";
                }
                data.put(attrs[3], days);

                if (vendorHashMap != null) {
                    Vendor vendor = vendorHashMap.get(invoiceResult.getCustomerId());
                    if (!Objects.isNull(vendor)) {
                        data.put(attrs[4], vendor.getName());
                    }
                }

                if (employeeHashMap != null) {
                    Employee employee = employeeHashMap.get(invoiceResult.getSalesPersonId());
                    if (!Objects.isNull(employee)) {
                        StringBuilder name = new StringBuilder("");
                        name = name.append(StringUtils.isNotEmpty(employee.getFirstName()) ? employee.getFirstName() : "");
                        name = name.append(" ");
                        name = name.append(StringUtils.isNotEmpty(employee.getLastName()) ? employee.getLastName() : "");
                        data.put(attrs[5], name);
                    }
                }

                data.put(attrs[6], invoiceResult.getInvoiceStatus());
                if (!Objects.isNull(invoiceResult.getCart()) && !Objects.isNull(invoiceResult.getCart().getTaxResult())) {
                    data.put(attrs[7], invoiceResult.getCart().getSubTotal());
                    data.put(attrs[8], invoiceResult.getCart().getTaxResult().getTotalALExciseTax());
                    data.put(attrs[9], invoiceResult.getCart().getTaxResult().getTotalPostCalcTax());
                    data.put(attrs[10], invoiceResult.getCart().getTotal());

                    if (paymentsMap != null) {
                        List<PaymentsReceived> receivedList = paymentsMap.get(invoiceResult.getId());
                        if (CollectionUtils.isNotEmpty(receivedList)) {
                            BigDecimal totalPaidAmount = BigDecimal.ZERO;
                            for (PaymentsReceived paymentsReceived : receivedList) {
                                totalPaidAmount = totalPaidAmount.add(paymentsReceived.getAmountPaid());
                            }
                            data.put(attrs[11], totalPaidAmount);
                            BigDecimal remainingAmount = invoiceResult.getCart().getTotal().subtract(totalPaidAmount);
                            data.put(attrs[12], remainingAmount);
                            report.add(data);
                        }
                    }
                }
            }
        }
        return report;
    }
}
