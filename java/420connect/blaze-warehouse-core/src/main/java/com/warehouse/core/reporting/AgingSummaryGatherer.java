package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class AgingSummaryGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ShopRepository shopRepository;

    private String[] attrs = new String[]{"Date", "Count", "Total"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public AgingSummaryGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Aging Summary Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);


        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(),  "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }

        if (!Objects.isNull(invoiceResults)) {
            HashMap<Long, AgingSummary> summaryMap = new HashMap<>();
            for (Invoice invoiceResult : invoiceResults) {
                if (Objects.isNull(invoiceResult) && Objects.isNull(invoiceResult.getCart())) {
                    continue;
                }
                Long dueDate = getKey(invoiceResult, filter.getTimezoneOffset());
                DateTime startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                DateTime endDate = DateUtil.nowWithTimeZone(shop.getTimeZone());
                long daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());

                switch (invoiceResult.getInvoiceTerms()) {
                    case NET_7:
                        daysBetweenTwoDates = daysBetweenTwoDates - 7;
                        break;
                    case NET_15:
                        daysBetweenTwoDates = daysBetweenTwoDates - 15;
                        break;
                    case NET_30:
                        daysBetweenTwoDates = daysBetweenTwoDates - 30;
                        break;
                    case NET_45:
                        daysBetweenTwoDates = daysBetweenTwoDates - 45;
                        break;
                    case NET_60:
                        daysBetweenTwoDates = daysBetweenTwoDates - 60;
                        break;
                    case CUSTOM_DATE:
                        dueDate = getKey(invoiceResult, filter.getTimezoneOffset());
                        startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                        daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                        break;
                    default:
                        break;
                }
                daysBetweenTwoDates = Math.abs(daysBetweenTwoDates);

                AgingSummary agingSummary = new AgingSummary();
                agingSummary.setDueDate(dueDate);
                BigDecimal total = BigDecimal.ZERO;
                long count = 1;
                if (summaryMap.containsKey(daysBetweenTwoDates)) {
                    AgingSummary summary = summaryMap.get(daysBetweenTwoDates);
                    total = total.add(summary.getTotalAmount());
                    count += summary.getCount() + 1;
                }

                agingSummary.setTotalAmount(total.add(invoiceResult.getCart().getTotal()));
                agingSummary.setCount(count);
                summaryMap.put(daysBetweenTwoDates, agingSummary);
            }

            if (summaryMap != null) {
                for (long key : summaryMap.keySet()) {
                    AgingSummary agingSummary = summaryMap.get(key);
                    if (!Objects.isNull(agingSummary)) {
                        HashMap<String, Object> data = new HashMap<>();
                        data.put(attrs[0], ProcessorUtil.dateString(agingSummary.getDueDate()));
                        data.put(attrs[1], agingSummary.getCount());
                        data.put(attrs[2], agingSummary.getTotalAmount());
                        report.add(data);
                    }
                }
            }
        }
        return report;
    }

    private Long getKey(Invoice i, int offSet) {
        return new DateTime(i.getDueDate()).minusMinutes(offSet).withTimeAtStartOfDay().getMillis();
    }
}


class AgingSummary {

    long dueDate;
    BigDecimal totalAmount;
    long count;

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}