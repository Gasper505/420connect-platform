package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.BestSellingBrands;
import com.fourtwenty.core.util.NumberUtils;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

public class BestSellingBrandsGather implements Gatherer {
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;

    private String[] attrs = new String[]{"Brand Name", "Sales"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public BestSellingBrandsGather() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Best Selling Brand Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        int limit = filter.getLimit() == 0 ? 10 : filter.getLimit();


        Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(),  "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> brandIds = new ArrayList<>();

        if (invoiceResult != null) {
            for (Invoice invoice : invoiceResult) {
                if (invoice.getCart() != null && invoice.getCart().getItems() != null && !invoice.getCart().getItems().isEmpty()) {
                    for (OrderItem productRequest : invoice.getCart().getItems()) {
                        if (StringUtils.isNotBlank(productRequest.getProductId()) && ObjectId.isValid(productRequest.getProductId())) {
                            productIds.add(new ObjectId(productRequest.getProductId()));
                        }
                    }
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), productIds);

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandMap = brandRepository.listAsMap(filter.getCompanyId(), brandIds);

        HashMap<String, BestSellingBrands> brandSellsMap = new HashMap<>();

        for (Invoice invoice : invoiceResult) {
            if (Objects.isNull(invoice.getCart()) || CollectionUtils.isEmpty(invoice.getCart().getItems())) {
                continue;
            }

            double total = 0.0;
            HashMap<String, Double> propRatioMap = new HashMap<>();

            // Sum up the total
            for (OrderItem orderItem : invoice.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += orderItem.getFinalPrice().doubleValue();
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : invoice.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product != null && product.isDiscountable() && total > 0) {
                    double finalCost = orderItem.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }
            }

            for (OrderItem item : invoice.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }

                Brand brand = brandMap.get(product.getBrandId());

                if (brand == null) {
                    continue;
                }

                Double ratio = propRatioMap.get(item.getId());
                if (ratio == null) {
                    ratio = 1d;
                }

                Double propCartDiscount = invoice.getCart().getCalcCartDiscount() != null ? invoice.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propDeliveryFee = invoice.getCart().getDeliveryFee() != null ? invoice.getCart().getDeliveryFee().doubleValue() * ratio : 0;


                double totalTax = 0;
                if (item.getTaxResult() != null) {
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                    }
                }

                if (totalTax == 0) {
                    totalTax = item.getCalcTax().doubleValue();
                }

                double finalAfterTax = item.getFinalPrice().doubleValue() - propCartDiscount
                        + propDeliveryFee + totalTax;




                BestSellingBrands brandSales = brandSellsMap.getOrDefault(brand.getId(), new BestSellingBrands());
                brandSales.setSales(NumberUtils.round(brandSales.getSales() + finalAfterTax, 2));
                brandSales.setCount(brandSales.getCount() + 1);
                brandSales.setBrandId(brand.getId());
                brandSales.setBrandName(brand.getName());
                brandSellsMap.put(brand.getId(), brandSales);

            }
        }

        for (Map.Entry<String, BestSellingBrands> entry : brandSellsMap.entrySet()) {
            BestSellingBrands sellingBrands = entry.getValue();
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], sellingBrands.getBrandName());
            data.put(attrs[1], sellingBrands.getSales());
            report.add(data);
        }

        return report;
    }

}
