package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class BestSellingProductGatherer implements Gatherer {
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ProductRepository productRepository;

    private String[] attrs = new String[]{"Product Name", "Sales"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public BestSellingProductGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Best Selling Product Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        List<ObjectId> productIds = new ArrayList<>();

        Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, BigDecimal> bestProduct = new HashMap<>();

        if (invoiceResult != null) {
            for (Invoice invoice : invoiceResult) {
                if (invoice.getCart() != null && invoice.getCart().getItems() != null && !invoice.getCart().getItems().isEmpty()) {
                    for (OrderItem orderItem : invoice.getCart().getItems()) {
                        if (StringUtils.isNotBlank(orderItem.getProductId()) && ObjectId.isValid(orderItem.getProductId())) {
                            productIds.add(new ObjectId(orderItem.getProductId()));
                        }
                    }
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), productIds);
        for (Invoice invoice : invoiceResult) {
            if (Objects.isNull(invoice.getCart()) || CollectionUtils.isEmpty(invoice.getCart().getItems())) {
                continue;
            }

            double total = 0.0;
            HashMap<String, Double> propRatioMap = new HashMap<>();

            // Sum up the total
            for (OrderItem orderItem : invoice.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += orderItem.getFinalPrice().doubleValue();
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : invoice.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product != null && product.isDiscountable() && total > 0) {
                    double finalCost = orderItem.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }
            }

            for (OrderItem item : invoice.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                Double ratio = propRatioMap.get(item.getId());
                if (ratio == null) {
                    ratio = 1d;
                }

                Double propCartDiscount = invoice.getCart().getCalcCartDiscount() != null ? invoice.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propDeliveryFee = invoice.getCart().getDeliveryFee() != null ? invoice.getCart().getDeliveryFee().doubleValue() * ratio : 0;


                double totalTax = 0;
                if (item.getTaxResult() != null) {
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                    }
                }

                if (totalTax == 0) {
                    totalTax = item.getCalcTax().doubleValue();
                }

                double finalAfterTax = item.getFinalPrice().doubleValue() - propCartDiscount
                        + propDeliveryFee + totalTax;


                bestProduct.putIfAbsent(product.getName(), BigDecimal.ZERO);
                BigDecimal productSales = bestProduct.get(product.getName());
                productSales = productSales.add(new BigDecimal(finalAfterTax));
                bestProduct.put(product.getName(), productSales);
            }
        }

        for (Map.Entry<String, BigDecimal> entry : bestProduct.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], entry.getKey());
            data.put(attrs[1], NumberUtils.round(entry.getValue(), 2));
            report.add(data);
        }

        return report;
    }

}
