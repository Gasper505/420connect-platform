package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class CustomerBalanceGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentRepository;

    private String[] attrs = new String[]{"Customer Name", "Total Amount", "Received Amount", "Remaining Amount", "Due Date"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public CustomerBalanceGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Customer Balance Report", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        List<Invoice.InvoiceStatus> invoiceStatusList = new ArrayList<>();
        List<Invoice.PaymentStatus> invoicePaymentList = new ArrayList<>();


        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(),  "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        HashMap<String, CustomerBalance> customerBalanceHashMap = new HashMap<>();
        List<String> invoiceIdList = new ArrayList<>();

        if (invoiceResults != null) {
            for (Invoice invoiceResult : invoiceResults) {
                if (!Objects.isNull(invoiceResult)) {
                    invoiceIdList.add(invoiceResult.getId());
                }
            }
        }

        HashMap<String, List<PaymentsReceived>> paymentsMap = invoicePaymentRepository.findItemsInAsMapByInvoice(filter.getCompanyId(), filter.getShopId(), invoiceIdList, "");

        if (invoiceResults != null) {
            for (Invoice invoice : invoiceResults) {
                Vendor vendor = vendorHashMap.get(invoice.getCustomerId());
                if (vendor == null || invoice.getCart() == null) {
                    continue;
                }
                BigDecimal totalAmount = BigDecimal.ZERO;
                BigDecimal totalReceivedAmount = BigDecimal.ZERO;
                if (customerBalanceHashMap.containsKey(invoice.getCustomerId())) {
                    CustomerBalance balance = customerBalanceHashMap.get(invoice.getCustomerId());
                    totalAmount = balance.getTotalAmount();
                    totalReceivedAmount = balance.getTotalReceivedAmount();
                }
                CustomerBalance customerBalance = new CustomerBalance();
                customerBalance.setCustomerName(vendor.getName());
                customerBalance.setTotalAmount(totalAmount.add(invoice.getCart().getTotal()));
                customerBalance.setTotalReceivedAmount(totalReceivedAmount.add(invoice.getCart().getCashReceived()));
                BigDecimal totalPaidAmount = BigDecimal.ZERO;
                if (paymentsMap != null) {
                    List<PaymentsReceived> receivedList = paymentsMap.get(invoice.getId());
                    if (CollectionUtils.isNotEmpty(receivedList)) {
                        for (PaymentsReceived paymentsReceived : receivedList) {
                            totalPaidAmount = totalPaidAmount.add(paymentsReceived.getAmountPaid());
                        }
                    }
                }
                customerBalance.setTotalDueAmount(invoice.getCart().getTotal().subtract(totalPaidAmount));
                customerBalance.setDueDate(invoice.getDueDate());
                customerBalanceHashMap.put(invoice.getCustomerId(), customerBalance);
            }
        }

        if (customerBalanceHashMap != null) {
            for (String key : customerBalanceHashMap.keySet()) {
                CustomerBalance customerBalance = customerBalanceHashMap.get(key);
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], customerBalance.getCustomerName());
                data.put(attrs[1], customerBalance.getTotalAmount());
                data.put(attrs[2], customerBalance.getTotalReceivedAmount());
                data.put(attrs[3], customerBalance.getTotalDueAmount());
                data.put(attrs[4], ProcessorUtil.dateString(customerBalance.getDueDate()));
                report.add(data);
            }
        }
        return report;
    }
}

class CustomerBalance {
    private String customerName;
    private BigDecimal totalAmount;
    private BigDecimal totalReceivedAmount;
    private BigDecimal totalDueAmount;
    private long dueDate;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalReceivedAmount() {
        return totalReceivedAmount;
    }

    public void setTotalReceivedAmount(BigDecimal totalReceivedAmount) {
        this.totalReceivedAmount = totalReceivedAmount;
    }

    public BigDecimal getTotalDueAmount() {
        return totalDueAmount;
    }

    public void setTotalDueAmount(BigDecimal totalDueAmount) {
        this.totalDueAmount = totalDueAmount;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }
}
