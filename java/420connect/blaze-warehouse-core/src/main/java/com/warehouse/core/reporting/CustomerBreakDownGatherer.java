package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class CustomerBreakDownGatherer implements Gatherer {

    @Inject
    InvoiceRepository invoiceRepository;
    @Inject
    VendorRepository vendorRepository;

    private String[] attrs = new String[]{"Company Type", "Sales"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public CustomerBreakDownGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.BIGDECIMAL};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Customer Breakdown", reportHeaders);
        report.setReportFieldTypes(fieldTypes);


        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<ObjectId> vendorsId = new ArrayList<>();
        if (invoiceResults != null) {
            for (Invoice invoiceResult : invoiceResults) {
                if (StringUtils.isNotBlank(invoiceResult.getCustomerId()) && ObjectId.isValid(invoiceResult.getCustomerId())) {
                    vendorsId.add(new ObjectId(invoiceResult.getCustomerId()));
                }
            }
            HashMap<String, Vendor> vendorHashMap = new HashMap<>();
            if (CollectionUtils.isNotEmpty(vendorsId)) {
                vendorHashMap = vendorRepository.findItemsInAsMap(filter.getCompanyId(), vendorsId);
            }
            Vendor vendor;
            HashMap<String, BigDecimal> customerMap = new HashMap<>();

            for (Invoice invoiceResult : invoiceResults) {
                BigDecimal total = BigDecimal.ZERO;
                vendor = vendorHashMap.get(invoiceResult.getCustomerId());
                if (Objects.isNull(vendor) || invoiceResult.getCart() == null)
                    continue;

                // Get license from invoice license
                CompanyLicense companyLicense = vendor.getCompanyLicense(invoiceResult.getLicenseId());

                if (customerMap.containsKey(companyLicense.getCompanyType().toString())) {
                    total = customerMap.get(companyLicense.getCompanyType().toString());
                }
                total = total.add(invoiceResult.getCart().getTotal());
                customerMap.put(companyLicense.getCompanyType().toString(), total);
            }
            for (Map.Entry<String, BigDecimal> entry : customerMap.entrySet()) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], entry.getKey());
                data.put(attrs[1], entry.getValue());
                report.add(data);
            }

        }
        return report;
    }
}
