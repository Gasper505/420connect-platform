package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import java.util.*;

public class DistributionPurchaseOrderGatherer implements Gatherer {
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private VendorRepository vendorRepository;

    private String[] attrs = new String[]{"PO#", "Vendor", "Terms", "PO Date", "Delivery Date", "Sub Total", "Discount", "Fees", "Payment Made", "Total Tax", "Total", "Balance Due", "Over Due", "Status", "Due Date"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    public DistributionPurchaseOrderGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.DATE };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Purchase Orders Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        List<PurchaseOrder.PurchaseOrderStatus> statusList = new ArrayList<>();
        statusList.add(PurchaseOrder.PurchaseOrderStatus.Approved);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.Closed);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.WaitingShipment);

        Iterable<PurchaseOrder> poByStatus = purchaseOrderRepository.getPurchaseOrderByStatus(filter.getCompanyId(), filter.getShopId(), statusList, PurchaseOrder.CustomerType.VENDOR, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        ArrayList<PurchaseOrder> purchaseOrders = Lists.newArrayList(poByStatus);

        List<ObjectId> vendorIds = new ArrayList<>();
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            vendorIds.add(new ObjectId(purchaseOrder.getVendorId()));
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), vendorIds);

        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[0], purchaseOrder.getPoNumber());
            Vendor vendor = vendorHashMap.get(purchaseOrder.getVendorId());
            if (vendor == null) {
                data.put(attrs[1], "N/A");
            } else {
                data.put(attrs[1], vendor.getName() != null ? vendor.getName() : TextUtil.textOrEmpty(""));
            }

            data.put(attrs[2], purchaseOrder.getPoPaymentTerms() != null ? purchaseOrder.getPoPaymentTerms() : TextUtil.textOrEmpty(""));
            data.put(attrs[3], ProcessorUtil.dateStringWithOffset(purchaseOrder.getPurchaseOrderDate() == 0 ? purchaseOrder.getCreated() : purchaseOrder.getPurchaseOrderDate(), filter.getTimezoneOffset()));
            data.put(attrs[4], ProcessorUtil.dateStringWithOffset(purchaseOrder.getDeliveryDate(), filter.getTimezoneOffset()));
            data.put(attrs[5], purchaseOrder.getTotalCost() != null ? new DollarAmount(purchaseOrder.getTotalCost().doubleValue()) : new DollarAmount(0.0));
            data.put(attrs[6], purchaseOrder.getDiscount() != null ? new DollarAmount(purchaseOrder.getDiscount().doubleValue()) : new DollarAmount(0.0));
            data.put(attrs[7], purchaseOrder.getFees() != null ? new DollarAmount(purchaseOrder.getFees().doubleValue()) : new DollarAmount(0.0));
            double amountPaid = 0.0;
            if (purchaseOrder.getAmountPaid() != null && purchaseOrder.getAmountPaid().doubleValue() > 0) {
                amountPaid = purchaseOrder.getAmountPaid().doubleValue();
            }
            data.put(attrs[8], new DollarAmount(amountPaid));
            data.put(attrs[9], purchaseOrder.getTotalTax() != null ? new DollarAmount(purchaseOrder.getTotalTax().doubleValue()) : new DollarAmount(0.0));
            data.put(attrs[10], purchaseOrder.getGrandTotal() != null ? new DollarAmount(purchaseOrder.getGrandTotal().doubleValue()) : new DollarAmount(0.0));
            double balanceDue = 0.0;
            balanceDue = purchaseOrder.getGrandTotal().doubleValue() - amountPaid;
            if (balanceDue < 0) {
                data.put(attrs[11], new DollarAmount(0.0));
                data.put(attrs[12], new DollarAmount((balanceDue * -1)));
            } else {
                data.put(attrs[11], new DollarAmount(balanceDue));
                data.put(attrs[12], new DollarAmount(0.0));
            }
            data.put(attrs[13], purchaseOrder.getPurchaseOrderStatus());
            data.put(attrs[14], ProcessorUtil.dateStringWithOffset(purchaseOrder.getDueDate() == 0 ? purchaseOrder.getPODueDate() : purchaseOrder.getDueDate(), filter.getTimezoneOffset()));


            report.add(data);
        }
        return report;
    }
}
