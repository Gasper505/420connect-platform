package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TimeCardRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DistributionTimeClockGatherer implements Gatherer {

    @Inject
    private TimeCardRepository timeCardRepository;
    @Inject
    private EmployeeRepository employeeRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{
            "Date",
            "Employee",
            "Clock In",
            "Clock Out",
            "Time Clocked In",
            "iPad Sessions"
    };

    public DistributionTimeClockGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, //Date 0
                GathererReport.FieldType.STRING, //Employee 1
                GathererReport.FieldType.STRING, //Clock In 2
                GathererReport.FieldType.STRING, //Clock out 3
                GathererReport.FieldType.STRING, //Time clocked in 4
                GathererReport.FieldType.NUMBER //iPad Sessions
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Time clock report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());

        Iterable<TimeCard> timeCards = timeCardRepository.getAllTimeCards(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        for (TimeCard timeCard : timeCards) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.dateStringWithOffset(timeCard.getCreated(), filter.getTimezoneOffset()));

            Employee emp = employeeMap.get(timeCard.getEmployeeId());
            if (emp == null) {
                continue;
            }

            data.put(attrs[1], emp.getFirstName() + " " + emp.getLastName());
            data.put(attrs[2], ProcessorUtil.timeStampWithOffset(timeCard.getClockInTime(), filter.getTimezoneOffset()));
            data.put(attrs[3], ProcessorUtil.timeStampWithOffset(timeCard.getClockOutTime(), filter.getTimezoneOffset()));

            //compute time worked
            final Period period = new Period(new DateTime(timeCard.getClockInTime()), new DateTime(timeCard.getClockOutTime()));
            int sessionCount = timeCard.getSessions() != null ? timeCard.getSessions().size() : 0;

            StringBuilder diff = new StringBuilder();
            if (period.getYears() > 0) {
                diff.append(period.getYears() + "y");
            }
            if (period.getMonths() > 0) {
                diff.append(period.getMonths() + "m");
            }
            if (period.getDays() > 0) {
                diff.append(period.getDays() + "d");
            }
            diff.append(period.getHours() + "h");
            diff.append(period.getMinutes() + "m");

            data.put(attrs[4], diff.toString());

            data.put(attrs[5], sessionCount);
            report.add(data);
        }

        return report;
    }
}
