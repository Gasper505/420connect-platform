package com.warehouse.core.reporting;

import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class ExpenseByCategoryGatherer implements Gatherer {

    @Inject
    private ExpensesRepository expensesRepository;

    private String[] attrs = new String[]{"Category", "Count", "Amount"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ExpenseByCategoryGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.BIGDECIMAL};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Expenses By Category Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        DateSearchResult<Expenses> expensesResult = expensesRepository.findExpensesWithDate(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}", Expenses.class);
        HashMap<Expenses.ExpenseCategory, ExpensesCategory> expensesCategoryHashMap = new HashMap<>();

        if (expensesResult != null) {
            for (Expenses expenses : expensesResult.getValues()) {
                int count = 0;
                BigDecimal amount = BigDecimal.ZERO;
                ExpensesCategory expensesCategory = new ExpensesCategory();
                if (expensesCategoryHashMap.containsKey(expenses.getExpenseCategory())) {
                    expensesCategory = expensesCategoryHashMap.get(expenses.getExpenseCategory());
                    count = expensesCategory.getCount();
                    amount = expensesCategory.getAmount();
                }
                expensesCategory.setExpensesCategory(expenses.getExpenseCategory());
                expensesCategory.setCount(++count);
                amount = amount.add(expenses.getAmount());
                expensesCategory.setAmount(amount);
                expensesCategoryHashMap.put(expenses.getExpenseCategory(), expensesCategory);
            }
        }

        for (Expenses.ExpenseCategory key : expensesCategoryHashMap.keySet()) {
            ExpensesCategory expenseCategory = expensesCategoryHashMap.get(key);
            if (expenseCategory != null) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], expenseCategory.getExpensesCategory());
                data.put(attrs[1], expenseCategory.getCount());
                data.put(attrs[2], expenseCategory.getAmount());
                report.add(data);
            }
        }
        return report;
    }
}

class ExpensesCategory {
    private Expenses.ExpenseCategory expensesCategory;
    private int count;
    private BigDecimal amount;

    public Expenses.ExpenseCategory getExpensesCategory() {
        return expensesCategory;
    }

    public void setExpensesCategory(Expenses.ExpenseCategory expensesCategory) {
        this.expensesCategory = expensesCategory;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
