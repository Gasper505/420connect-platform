package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class ExpenseByCustomerGatherer implements Gatherer {

    @Inject
    private ExpensesRepository expensesRepository;
    @Inject
    private VendorRepository vendorRepository;

    private String[] attrs = new String[]{"Customer", "Count", "Amount"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ExpenseByCustomerGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.BIGDECIMAL};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Expenses By Customer Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        DateSearchResult<Expenses> expensesResult = expensesRepository.findExpensesWithDate(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}", Expenses.class);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap();
        HashMap<String, ExpensesByCustomer> expensesCustomerHashMap = new HashMap<>();

        if (expensesResult != null) {
            for (Expenses expenses : expensesResult.getValues()) {
                Vendor vendor = vendorHashMap.get(expenses.getCustomerId());
                if (vendor == null) {
                    continue;
                }
                int count = 0;
                BigDecimal amount = BigDecimal.ZERO;
                ExpensesByCustomer expensesByCustomer = new ExpensesByCustomer();
                if (expensesCustomerHashMap.containsKey(expenses.getCustomerId())) {
                    expensesByCustomer = expensesCustomerHashMap.get(expenses.getCustomerId());
                    count = expensesByCustomer.getCount();
                    amount = expensesByCustomer.getAmount();
                }

                expensesByCustomer.setCustomerName(vendor.getName());
                expensesByCustomer.setCount(++count);
                amount = amount.add(expenses.getAmount());
                expensesByCustomer.setAmount(amount);
                expensesCustomerHashMap.put(expenses.getCustomerId(), expensesByCustomer);
            }
        }

        for (String key : expensesCustomerHashMap.keySet()) {
            ExpensesByCustomer expensesByCustomer = expensesCustomerHashMap.get(key);
            if (expensesByCustomer != null) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], expensesByCustomer.getCustomerName());
                data.put(attrs[1], expensesByCustomer.getCount());
                data.put(attrs[2], expensesByCustomer.getAmount());
                report.add(data);
            }
        }
        return report;
    }
}

class ExpensesByCustomer {
    private String customerName;
    private int count;
    private BigDecimal amount;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}