package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

public class ExpensesDetailsGatherer implements Gatherer {

    @Inject
    private ExpensesRepository expensesRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;

    private String[] attrs = new String[]{"Expenses No", "Expenses Name", "Category", "Amount", "Reference No", "Customer Name", "PO Number"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ExpensesDetailsGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.BIGDECIMAL,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Expenses Details Report", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        SearchResult<Expenses> expensesSearchResult = expensesRepository.findExpensesWithDate(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}", Expenses.class);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        HashMap<String, PurchaseOrder> purchaseOrderHashMap = purchaseOrderRepository.listAsMap(filter.getCompanyId());

        if (expensesSearchResult != null) {
            for (Expenses expenses : expensesSearchResult.getValues()) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], expenses.getExpenseNumber());
                data.put(attrs[1], expenses.getExpenseName());
                data.put(attrs[2], expenses.getExpenseCategory());
                data.put(attrs[3], expenses.getAmount());
                data.put(attrs[4], expenses.getReference());
                String customerName = "";
                String poNumber = "";
                if (vendorHashMap != null && purchaseOrderHashMap != null) {
                    Vendor vendor = vendorHashMap.get(expenses.getCustomerId());
                    PurchaseOrder purchaseOrder = purchaseOrderHashMap.get(expenses.getPurchaseOrderId());
                    if (vendor != null) {
                        customerName = vendor.getName();
                    }
                    if (purchaseOrder != null) {
                        poNumber = purchaseOrder.getPoNumber();
                    }
                }
                data.put(attrs[5], customerName);
                data.put(attrs[6], poNumber);
                report.add(data);
            }
        }
        return report;
    }
}