package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class InventoryDistributionGatherer implements Gatherer {

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;

    @Override
    public GathererReport gather(ReportFilter filter) {
        reportHeaders.add("Category");
        fieldTypes.put("Category", GathererReport.FieldType.STRING);
        reportHeaders.add("Total");
        fieldTypes.put("Total", GathererReport.FieldType.NUMBER);

        Iterable<Inventory> inventoryIterable = inventoryRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        List<Inventory> inventories = Lists.newArrayList(inventoryIterable);
        inventories.sort(Comparator.comparing(Inventory::getName));
        for (Inventory inventory : inventories) {
            if (inventory.isActive()) {
                reportHeaders.add(inventory.getName());
                fieldTypes.put(inventory.getName(), GathererReport.FieldType.NUMBER);
            }
        }

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Product> products;
        if (ReportFilter.ALL_CATEGORIES.equals(filter.getCategoryId()) || StringUtils.isBlank(filter.getCategoryId())) {
            products = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        } else {
            products = productRepository.getProductsForCategory(filter.getCompanyId(), filter.getCategoryId());
        }

        List<String> productIds = new ArrayList<>();

        for (Product product : products) {
            productIds.add(product.getId());
        }

        Map<String, ProductBatch> batchMap = batchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), productIds);
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Prepackage> prepackageList = prepackageRepository.findItems(filter.getCompanyId(), filter.getShopId(), 0, Integer.MAX_VALUE).getValues();
        Iterable<PrepackageProductItem> prepackageProductItems = prepackageProductItemRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getProductPrepackageQuantities(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        List<Product> orderedProducts = Lists.newArrayList(products);
        orderedProducts.sort(Comparator.comparing(Product::getName));

        GathererReport report = new GathererReport(filter, "Inventory Distribution Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.DATE_BRACKET);

        HashMap<String, HashMap<String, BigDecimal>> productCostMap = new HashMap<>();

        for (Product product : products) {
            if (!Objects.isNull(product) && StringUtils.isNotBlank(product.getCategoryId())) {
                ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
                if (category != null) {
                    List<ProductQuantity> productQuantityList = product.getQuantities();
                    HashMap<String, BigDecimal> inventoryIdMap = new HashMap<>();

                    double unitCost = 0;

                    ProductBatch batch = batchMap.get(product.getId());

                    if (batch != null) {
                        unitCost = batch.getFinalUnitCost().doubleValue();
                    }
                    if (productCostMap.containsKey(category.getId())) {
                        inventoryIdMap = productCostMap.get(category.getId());
                    }
                    for (ProductQuantity productQuantity : productQuantityList) {
                        if (!Objects.isNull(productQuantity)) {
                            BigDecimal productCost = BigDecimal.ZERO;
                            if (inventoryIdMap.containsKey(productQuantity.getInventoryId())) {
                                productCost = inventoryIdMap.get(productQuantity.getInventoryId());
                            }
                            productCost = BigDecimal.valueOf(productCost.doubleValue() + (productQuantity.getQuantity().doubleValue() * unitCost));
                            inventoryIdMap.put(productQuantity.getInventoryId(), productCost);
                        }
                    }

                    for (Prepackage prepackage : prepackageList) {
                        if (!prepackage.getProductId().equals(product.getId())) {
                            continue;
                        }

                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = (weightTolerance != null) ? weightTolerance.getUnitValue() : BigDecimal.ZERO;
                        }

                        for (PrepackageProductItem prepackageProductItem : prepackageProductItems) {
                            if (!prepackageProductItem.getPrepackageId().equals(prepackage.getId())) {
                                continue;
                            }

                            ProductBatch productBatch = allBatchMap.get(prepackageProductItem.getBatchId());
                            BigDecimal batchUnitCost = (productBatch == null) ? BigDecimal.ZERO : productBatch.getFinalUnitCost();

                            for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
                                double cogs = 0;
                                if (prepackageQuantity.getPrepackageItemId().equals(prepackageProductItem.getId())) {

                                    double unitsSold = prepackageQuantity.getQuantity() * unitValue.doubleValue();
                                    cogs += inventoryIdMap.getOrDefault(prepackageQuantity.getInventoryId(), BigDecimal.ZERO).doubleValue() + (batchUnitCost.doubleValue() * unitsSold);
                                    inventoryIdMap.put(prepackageQuantity.getInventoryId(), BigDecimal.valueOf(cogs));
                                }
                            }
                        }

                    }

                    productCostMap.put(product.getCategoryId(), inventoryIdMap);

                }
            }
        }
        for (String key : productCostMap.keySet()) {
            HashMap<String, BigDecimal> quantityTotalMap = productCostMap.get(key);
            ProductCategory category = productCategoryHashMap.get(key);
            if (category == null || category.isDeleted() || !category.isActive()) {
                continue;
            }

            String categoryName = category.getName();

            HashMap<String, Object> mainRow = getInventoryQuantity(categoryName, key, quantityTotalMap, inventories);
            report.add(mainRow);
        }

        report.getData().sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
                Double d1 = (Double)map1.getOrDefault("Total", 0D);
                Double d2 = (Double)map2.getOrDefault("Total", 0D);
                return d2.compareTo(d1);
            }
        });
        List<HashMap<String, Object>> reportData = report.getData().subList(0, report.getData().size() > 15 ? 15 : report.getData().size());
        report.setData(reportData);
        return report;
    }

    private HashMap<String, Object> getInventoryQuantity(String categoryName, String categoryId,
                                                         HashMap<String, BigDecimal> productQuantityMap,
                                                         List<Inventory> inventories) {
        HashMap<String, Object> row = new HashMap<>();
        row.put("Category", categoryName);

        BigDecimal quantities = BigDecimal.ZERO;

        for (Inventory inventory : inventories) {
            if (inventory.isActive() && !inventory.isDeleted()) {
                String inventoryName = inventory.getName();
                BigDecimal total = productQuantityMap.get(inventory.getId());
                if (total == null) {
                    total = new BigDecimal(0);
                }
                quantities = quantities.add(total);
                row.put("Total", NumberUtils.round(quantities, 2));
                row.put(inventoryName, NumberUtils.round(total, 2));
            }
        }
        return row;
    }
}
