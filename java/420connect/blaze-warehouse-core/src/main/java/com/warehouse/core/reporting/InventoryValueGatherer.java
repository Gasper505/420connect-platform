package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InventoryValueGatherer implements Gatherer {

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductWeightToleranceRepository toleranceRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private String[] attrs = new String[]{
            "Category Name",
            "Total Units",
            "Total Value"};

    public InventoryValueGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory Value Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<Product> products;

        if (ReportFilter.ALL_CATEGORIES.equals(filter.getCategoryId())) {
            products = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        } else {
            products = productRepository.getProductsForCategory(filter.getCompanyId(), filter.getCategoryId());
        }

        Set<ObjectId> categoryIds = new HashSet<>();
        List<String> productIds = new ArrayList<>();

        for (Product product : products) {
            productIds.add(product.getId());
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }


        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds));
        Iterable<Prepackage> prepackages = prepackageRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        Iterable<PrepackageProductItem> prepackageProductItems = prepackageProductItemRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        Map<String, ProductBatch> recentBatchHashMap = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), productIds);
        HashMap<String, ProductBatch> allBatchHashMap = productBatchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = toleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, InventoryValueByCategory> inventoryValueByCategoryMap = new HashMap<>();
        for (Product product : products) {
            if (StringUtils.isBlank(product.getCategoryId())) {
                continue;
            }

            ProductCategory category = categoryMap.get(product.getCategoryId());
            if (category == null) {
                continue;
            }

            InventoryValueByCategory valueByCategory = inventoryValueByCategoryMap.getOrDefault(product.getCategoryId(), new InventoryValueByCategory());

            ProductBatch batch = recentBatchHashMap.get(product.getId());

            double unitCost = (batch == null) ? 0 : batch.getFinalUnitCost().doubleValue();

            double prdQuantity = 0;
            double productCogs;
            if (!CollectionUtils.isEmpty(product.getQuantities())) {
                for (ProductQuantity productQuantity : product.getQuantities()) {
                    prdQuantity += productQuantity.getQuantity().doubleValue();
                }
            }

            productCogs = unitCost * prdQuantity;

            double prepackageCogs = 0;
            double prePkgQuantity = 0;
            for (Prepackage prepackage : prepackages) {
                if (prepackage.getProductId().equals(product.getId())) {

                    double unitValue = (prepackage.getUnitValue() == null) ? 0 : prepackage.getUnitValue().doubleValue();
                    if (unitValue == 0) {
                        ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                        if (weightTolerance != null) {
                            unitValue = weightTolerance.getUnitValue().doubleValue();
                        }
                    }

                    for (PrepackageProductItem prepackageProductItem : prepackageProductItems) {
                        if (prepackageProductItem.getPrepackageId().equals(prepackage.getId())) {
                            batch = allBatchHashMap.get(prepackageProductItem.getBatchId());
                            unitCost = (batch == null) ? 0 : batch.getFinalUnitCost().doubleValue();
                            double quantity = 0;
                            for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
                                if (prepackageQuantity.getPrepackageItemId().equals(prepackageProductItem.getId())) {
                                    quantity += prepackageQuantity.getQuantity();
                                }
                            }

                            prepackageCogs += unitCost * (unitValue * quantity);
                            prePkgQuantity += unitValue * quantity;
                        }
                    }

                }
            }

            valueByCategory.categoryName = category.getName();
            valueByCategory.quantity += (prdQuantity + prePkgQuantity);
            valueByCategory.cogs += (prepackageCogs + productCogs);

            inventoryValueByCategoryMap.put(product.getCategoryId(), valueByCategory);
        }

        for (Map.Entry<String, InventoryValueByCategory> entry : inventoryValueByCategoryMap.entrySet()) {
            InventoryValueByCategory valueByCategory = entry.getValue();
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], valueByCategory.categoryName);
            data.put(attrs[1], valueByCategory.quantity);
            data.put(attrs[2], valueByCategory.cogs);
            report.add(data);
        }
        return report;
    }

    public class InventoryValueByCategory {
        String categoryName;
        double quantity;
        double cogs;

        InventoryValueByCategory() {
            categoryName = "";
        }
    }
}
