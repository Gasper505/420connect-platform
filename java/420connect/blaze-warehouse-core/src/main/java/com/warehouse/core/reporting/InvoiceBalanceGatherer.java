package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InvoiceBalanceGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;

    String[] attrs = new String[]{"Customer", "Total Sales", "Total Payments", "Outstanding", "1 - 30 Days", "31 - 60 Days", "61 - 90 Days"};
    private List<String> reportHeaders;
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    @Inject
    public InvoiceBalanceGatherer() {
        reportHeaders = new ArrayList<>();
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Invoice Balance Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        String projection = "{customerId:1, invoiceDate:1, cart.total:1}";
        Iterable<Invoice> invoices = invoiceRepository.getInvoicesByStatus(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(Invoice.InvoiceStatus.COMPLETED), Lists.newArrayList(Invoice.PaymentStatus.values()), "{modified:1}", 0, filter.getTimeZoneEndDateMillis(), projection);

        Set<ObjectId> vendorIds = new HashSet<>();
        Set<String> invoiceIds = new HashSet<>();

        HashMap<String, InvoiceBalance> invoiceBalanceMap = new HashMap<>();

        for (Invoice invoice : invoices) {
            invoiceIds.add(invoice.getId());
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }

            invoiceBalanceMap.putIfAbsent(invoice.getCustomerId(), new InvoiceBalance());
            InvoiceBalance balance = invoiceBalanceMap.get(invoice.getCustomerId());

            balance.sales = balance.sales.add(invoice.getCart() != null ? invoice.getCart().getTotal() : BigDecimal.ZERO);
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, List<PaymentsReceived>> paymentReceivedMap = invoicePaymentsRepository.findItemsInAsMapByInvoice(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(invoiceIds), "{invoiceId:1, paidDate:1, amountPaid:1}");

        for (Invoice invoice : invoices) {
            List<PaymentsReceived> paymentsReceiveds = paymentReceivedMap.getOrDefault(invoice.getId(), new ArrayList<>());
            InvoiceBalance balance = invoiceBalanceMap.get(invoice.getCustomerId());

            for (PaymentsReceived paymentsReceived : paymentsReceiveds) {
                if (paymentsReceived.isDeleted()) {
                    continue;
                }
                balance.payment = balance.payment.add(paymentsReceived.getAmountPaid());

            }
        }
        prepareInvoiceDetails(invoices, invoiceBalanceMap);

        for (Map.Entry<String, InvoiceBalance> entry : invoiceBalanceMap.entrySet()) {
            Vendor vendor = vendorHashMap.get(entry.getKey());
            InvoiceBalance balance = entry.getValue();

            HashMap<String, Object> data = new HashMap<>();
            int i = 0;

            data.put(attrs[i++], vendor != null ? vendor.getName() : "N/A");
            data.put(attrs[i++], NumberUtils.round(balance.sales, 2));
            data.put(attrs[i++], NumberUtils.round(balance.payment, 2));
            data.put(attrs[i++], NumberUtils.round(balance.getOutstandingSales(), 2));
            data.put(attrs[i++], NumberUtils.round(balance.thirtyDaySales, 2));
            data.put(attrs[i++], NumberUtils.round(balance.sixtyDaySales, 2));
            data.put(attrs[i], NumberUtils.round(balance.ninetyDaySales, 2));

            report.add(data);
        }

        report.getData().sort((o1, o2) -> {
            String vendorName1 = String.valueOf(o1.get(attrs[0]));
            String vendorName2 = String.valueOf(o2.get(attrs[0]));

            return vendorName1.compareTo(vendorName2);
        });
        return report;
    }

    private void prepareInvoiceDetails(Iterable<Invoice> invoices, HashMap<String, InvoiceBalance> invoiceBalanceMap) {
        DateTime currentDate = DateTime.now();
        long thirtyDays = currentDate.minusMonths(1).getMillis();
        long sixtyDays = currentDate.minusMonths(2).getMillis();
        long ninetyDays = currentDate.minusMonths(3).getMillis();
        long endTime = currentDate.getMillis();

        for (Invoice invoice : invoices) {
            InvoiceBalance balance = invoiceBalanceMap.get(invoice.getCustomerId());
            if (thirtyDays <= invoice.getInvoiceDate() && invoice.getInvoiceDate() <= endTime) {
                balance.thirtyDaySales = balance.thirtyDaySales.add(invoice.getCart() != null ? invoice.getCart().getTotal() : BigDecimal.ZERO);
            } else if (sixtyDays <= invoice.getInvoiceDate() && invoice.getInvoiceDate() < thirtyDays) {
                balance.sixtyDaySales = balance.sixtyDaySales.add(invoice.getCart() != null ? invoice.getCart().getTotal() : BigDecimal.ZERO);
            } else if (ninetyDays <= invoice.getInvoiceDate() && invoice.getInvoiceDate() < sixtyDays) {
                balance.ninetyDaySales = balance.ninetyDaySales.add(invoice.getCart() != null ? invoice.getCart().getTotal() : BigDecimal.ZERO);
            }
        }
    }

    public static class InvoiceBalance {
        BigDecimal sales = BigDecimal.ZERO;
        BigDecimal payment = BigDecimal.ZERO;
        BigDecimal thirtyDaySales = BigDecimal.ZERO;
        BigDecimal sixtyDaySales = BigDecimal.ZERO;
        BigDecimal ninetyDaySales = BigDecimal.ZERO;

        BigDecimal getOutstandingSales() {
            return sales.subtract(payment);
        }


    }
}
