package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

public class InvoiceDetailGatherer implements Gatherer {

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private InvoiceGatherer invoiceGatherer;

    private String[] attrs = new String[]{"Invoice No", "Customer Name", "Company Type", "Entity Tax Type", "Invoice Status", "Payment Status", "Shipped Status", "Reference No", "Invoice Date", "Due Date", "Sales Person", "Sub Total", "Discount", "Shipping Charges", "Total Tax", "Total"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InvoiceDetailGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Invoice Details Report", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(filter.getCompanyId());
        String employeeId = filter.getEmployeeId();
        if (employeeId.equalsIgnoreCase("ALL")) {
            employeeId = "";
        }
        SearchResult<Invoice> invoiceSearchResult = null;
        if (StringUtils.isBlank(filter.getCustomerId()) && StringUtils.isBlank(employeeId)) {
            invoiceSearchResult = invoiceRepository.getAllInvoicesByDates(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), Invoice.class, "");
        }
        if (StringUtils.isNotBlank(employeeId) && StringUtils.isBlank(filter.getCustomerId())) {
            invoiceSearchResult = invoiceRepository.getAllInvoicesByEmployeeWithDate(filter.getCompanyId(), filter.getShopId(), employeeId, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}");
        }
        if (StringUtils.isNotBlank(filter.getCustomerId()) && StringUtils.isBlank(employeeId)) {
            invoiceSearchResult = invoiceRepository.getAllInvoicesByCustomerWithDate(filter.getCompanyId(), filter.getShopId(), filter.getCustomerId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}");
        }
        if (StringUtils.isNotBlank(filter.getCustomerId()) && StringUtils.isNotBlank(employeeId)) {
            invoiceSearchResult = invoiceRepository.getAllInvoicesByEmployeeAndCustomer(filter.getCompanyId(), filter.getShopId(), employeeId, filter.getCustomerId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}");
        }


        if (invoiceSearchResult == null) {
            return report;
        }
        List<Invoice> invoices = invoiceSearchResult.getValues();

        Set<String> invoiceIds = new HashSet<>();
        invoices.forEach(invoice -> invoiceIds.add(invoice.getId()));

        HashMap<String, List<String>> invoiceTransactionMap = new HashMap<>();
        Set<String> invoiceManifestList = new HashSet<>();

        invoiceGatherer.getTransactionsForInvoice(filter, Lists.newArrayList(invoiceIds), invoiceTransactionMap, invoiceManifestList);

        for (Invoice invoice : invoices) {
            if (invoice == null) {
                continue;
            }
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], StringUtils.isNotBlank(invoice.getInvoiceNumber()) ? invoice.getInvoiceNumber() : "");
            Vendor vendor = vendorHashMap.get(invoice.getCustomerId());
            if (vendor != null) {
                // Get license from invoice license
                CompanyLicense companyLicense = vendor.getCompanyLicense(invoice.getLicenseId());
                data.put(attrs[1], (StringUtils.isNotBlank(vendor.getName())) ? vendor.getName() : "");
                data.put(attrs[2], companyLicense.getCompanyType());
                data.put(attrs[3], vendor.getArmsLengthType());
            } else {
                data.put(attrs[1], "");
                data.put(attrs[2], "");
                data.put(attrs[3], "");
            }

            String shippingStatus = "Not Started";
            if (invoice.getInvoiceStatus() == Invoice.InvoiceStatus.COMPLETED) {
                shippingStatus = "Shipped";
            } else if (invoiceTransactionMap.containsKey(invoice.getId())) {
                List<String> manifestTransactionList = invoiceTransactionMap.getOrDefault(invoice.getId(), new ArrayList<>());
                if (CollectionUtils.isNotEmpty(manifestTransactionList)) {
                    shippingStatus = "Partial Shipped";
                }
            } else if (invoiceManifestList.contains(invoice.getId())) {
                shippingStatus = "In Progress";
            }

            data.put(attrs[4], (invoice.getInvoiceStatus() != null) ? invoice.getInvoiceStatus() : "");
            data.put(attrs[5], (invoice.getInvoicePaymentStatus() != null) ? invoice.getInvoicePaymentStatus() : "");
            data.put(attrs[6], shippingStatus);
            data.put(attrs[7], (StringUtils.isNotBlank(invoice.getOrderNumber())) ? invoice.getOrderNumber() : "");
            data.put(attrs[8], ProcessorUtil.dateString(invoice.getInvoiceDate()));
            data.put(attrs[9], ProcessorUtil.dateString(invoice.getDueDate()));
            Employee employee = employeeHashMap.get(invoice.getSalesPersonId());
            StringBuilder name = new StringBuilder();
            if (employee != null) {
                name.append(StringUtils.isNotBlank(employee.getFirstName()) ? employee.getFirstName() : "");
                name.append(" ");
                name.append(StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : "");
            }
            data.put(attrs[10], name);
            Cart cart = invoice.getCart();
            if (cart != null) {
                data.put(attrs[11], cart.getSubTotal());
                data.put(attrs[12], cart.getTotalDiscount());
                data.put(attrs[13], cart.getDeliveryFee());
                data.put(attrs[14], (cart.getTaxResult() == null) ? 0 : cart.getTaxResult().getTotalPostCalcTax());
                data.put(attrs[15], cart.getTotal());
            } else {
                data.put(attrs[11], 0);
                data.put(attrs[12], 0);
                data.put(attrs[13], 0);
                data.put(attrs[14], 0);
                data.put(attrs[15], 0);
            }
            report.add(data);
        }
        return report;
    }
}
