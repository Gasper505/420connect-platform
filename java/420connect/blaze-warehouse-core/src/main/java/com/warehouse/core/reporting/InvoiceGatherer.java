package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.domain.repositories.invoice.ShippingManifestRepository;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InvoiceGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;
    @Inject
    private ShippingManifestRepository shippingManifestRepository;
    @Inject
    private TransactionRepository transactionRepository;

    private String[] attrs = new String[]{
            "Date",
            "Customer",
            "Invoice No.",
            "Invoice Status",
            "Tax Type",
            "Company Type",
            "Payment Terms",
            "Payment Status",
            "Shipped Status",
            "COGs",
            "Retail Value",
            "Discounts",
            "Post AL Excise Tax",
            "Post NAL Excise Tax",
            "Total Tax",
            "Shipping Charges",
            "Gross Receipt",
            "Employee",
            "Due Date",
            "Due Amount",
            "OverDue Amount"
    };
    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InvoiceGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        //List<Invoice.InvoiceStatus> invoiceStatuses = Lists.newArrayList(Invoice.InvoiceStatus.COMPLETED);

        List<Invoice.InvoiceStatus> invoiceStatuses = Lists.newArrayList(Invoice.InvoiceStatus.IN_PROGRESS, Invoice.InvoiceStatus.SENT, Invoice.InvoiceStatus.COMPLETED);
        List<Invoice.PaymentStatus> paymentStatuses = Lists.newArrayList(Invoice.PaymentStatus.PAID, Invoice.PaymentStatus.PARTIAL, Invoice.PaymentStatus.UNPAID);

        Iterable<Invoice> results = invoiceRepository.getInvoicesByStatusInvoiceDate(filter.getCompanyId(), filter.getShopId(), invoiceStatuses, paymentStatuses,
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<ObjectId> employeeIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> prepackageIds = new HashSet<>();
        Set<ObjectId> vendorIds = new HashSet<>();
        List<String> invoiceIds = new ArrayList<>();

        for (Invoice invoice : results) {
            invoiceIds.add(invoice.getId());
            if (StringUtils.isNotBlank(invoice.getSalesPersonId()) && ObjectId.isValid(invoice.getSalesPersonId())) {
                employeeIds.add(new ObjectId(invoice.getSalesPersonId()));
            }
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }
            if (invoice.getCart() == null || invoice.getCart().getItems() == null || invoice.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : invoice.getCart().getItems()) {
                productIds.add(new ObjectId(item.getProductId()));
                if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                    prepackageIds.add(new ObjectId(item.getPrepackageId()));
                }
            }
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        Map<String, ProductBatch> recentBatchMap = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));
        HashMap<String, ProductBatch> allBatchMap = productBatchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(prepackageIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, List<PaymentsReceived>> invoicePaymentMap = invoicePaymentsRepository.findItemsInAsMapByInvoice(filter.getCompanyId(), filter.getShopId(), invoiceIds, "");
        HashMap<String, List<String>> invoiceTransactionMap = new HashMap<>();
        Set<String> inProgressManifest = new HashSet<>();

        HashMap<String, Transaction> transactionHashMap = getTransactionsForInvoice(filter, invoiceIds, invoiceTransactionMap, inProgressManifest);


        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();

        for (Product p : productHashMap.values()) {
            List<Product> items = productsByCompanyLinkId.getOrDefault(p.getCompanyLinkId(), new ArrayList<>());
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        GathererReport report = new GathererReport(filter, "All Invoices Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        String nAvail = "N/A";

        Vendor vendor;
        Employee employee;
        StringBuilder employeeName;
        StringBuilder vendorName;

        for (Invoice invoice : results) {

            employee = employeeMap.get(invoice.getSalesPersonId());
            vendor = vendorHashMap.get(invoice.getCustomerId());
            employeeName = new StringBuilder();
            vendorName = new StringBuilder();

            if (employee != null) {
                employeeName.append(employee.getFirstName()).append(StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() + " " : "");
            }

            if (vendor != null) {
                vendorName.append(vendor.getName());
            }

            String consumerTaxType = nAvail;

            if (vendor != null) {
                // Get license from invoice license
                CompanyLicense companyLicense = vendor.getCompanyLicense(invoice.getLicenseId());
                consumerTaxType = String.valueOf(companyLicense.getCompanyType());
            }

            double totalPostTax = invoice.getCart().getTotalCalcTax().doubleValue() - invoice.getCart().getTotalPreCalcTax().doubleValue();
            if (totalPostTax < 0) {
                totalPostTax = 0;
            }

            double postALExciseTax = 0d;
            double postNALExciseTax = 0d;
            if (invoice.getCart().getTaxResult() != null) {
                postALExciseTax = invoice.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = invoice.getCart().getTaxResult().getTotalExciseTax().doubleValue();
            }

            double deliveryFees = 0;
            if (invoice.getCart().getDeliveryFee().doubleValue() > 0) {
                deliveryFees = invoice.getCart().getDeliveryFee().doubleValue();
            }

            double totalCogs = calculateTotalCogs(invoice, transactionHashMap, invoiceTransactionMap, allBatchMap, prepackageHashMap, prepackageProductItemHashMap, toleranceHashMap, recentBatchMap);
            double total = invoice.getCart().getTotal().doubleValue();
            double amountPaid = getAmountPaid(invoicePaymentMap.get(invoice.getId()));
            double balanceDue = total - amountPaid;

            String shippingStatus = "Not Started";
            if (invoice.getInvoiceStatus() == Invoice.InvoiceStatus.COMPLETED) {
                shippingStatus = "Shipped";
            } else if (invoiceTransactionMap.containsKey(invoice.getId())) {
                List<String> manifestTransactionList = invoiceTransactionMap.getOrDefault(invoice.getId(), new ArrayList<>());
                if (!manifestTransactionList.isEmpty()) {
                    shippingStatus = "Partial Shipped";
                }
            } else if (inProgressManifest.contains(invoice.getId())) {
                shippingStatus = "In Progress";
            }
            HashMap<String, Object> data = new HashMap<>();
            int i = 0;
            data.put(attrs[i++], ProcessorUtil.timeStampWithOffset(invoice.getInvoiceDate(), filter.getTimezoneOffset()));
            data.put(attrs[i++], vendorName);
            data.put(attrs[i++], invoice.getInvoiceNumber());
            data.put(attrs[i++], invoice.getInvoiceStatus());
            data.put(attrs[i++], invoice.getTransactionType().toString());
            data.put(attrs[i++], consumerTaxType);
            data.put(attrs[i++], invoice.getInvoiceTerms());
            data.put(attrs[i++], invoice.getInvoicePaymentStatus());
            data.put(attrs[i++], shippingStatus);
            data.put(attrs[i++], new DollarAmount(totalCogs)); //cogs
            data.put(attrs[i++], new DollarAmount(invoice.getCart().getSubTotal().doubleValue()));
            data.put(attrs[i++], new DollarAmount(invoice.getCart().getTotalDiscount().doubleValue()));

            data.put(attrs[i++], new DollarAmount(postALExciseTax));
            data.put(attrs[i++], new DollarAmount(postNALExciseTax));
            data.put(attrs[i++], new DollarAmount(totalPostTax));
            data.put(attrs[i++], new DollarAmount(deliveryFees));
            data.put(attrs[i++], new DollarAmount(total));
            data.put(attrs[i++], employeeName);
            data.put(attrs[i++], ProcessorUtil.timeStampWithOffset(invoice.getDueDate(), filter.getTimezoneOffset()));
            data.put(attrs[i++], (balanceDue >= 0) ? NumberUtils.round(balanceDue, 2) : 0);
            data.put(attrs[i++], (balanceDue < 0) ? NumberUtils.round(Math.abs(balanceDue), 2) : 0);

            report.add(data);


        }
        return report;
    }

    /**
     * Public method to calculate cogs for invoices and transaction
     *
     * @param invoice                  : invoice
     * @param transactionHashMap       : transaction hash map
     * @param invoiceTransactionMap    : invoice transaction map
     * @param productBatchMap          : product batch map
     * @param prepackageHashMap        : prepackage hash map
     * @param prepackageProductItemMap : prepackage product item map
     * @param toleranceHashMap         : tolerance hashmap
     * @param recentBatchMap           : recent batch map
     * @return : return total cogs
     */
    public double calculateTotalCogs(Invoice invoice, HashMap<String, Transaction> transactionHashMap, HashMap<String, List<String>> invoiceTransactionMap, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, Map<String, ProductBatch> recentBatchMap) {
        List<String> transactionIdList = invoiceTransactionMap.get(invoice.getId());
        double cogs = 0;
        boolean calculate = false;
        if (!CollectionUtils.isEmpty(transactionIdList)) {
            for (String transactionId : transactionIdList) {
                Transaction transaction = transactionHashMap.get(transactionId);
                if (transaction == null) {
                    continue;
                }
                cogs += calculateCogs(transaction.getCart(), productBatchMap, prepackageHashMap, prepackageProductItemMap, toleranceHashMap, recentBatchMap);
                calculate = true;
            }
        }

        if (!calculate && invoice.getCart() != null && !CollectionUtils.isEmpty(invoice.getCart().getItems())) {
            cogs += calculateCogs(invoice.getCart(), productBatchMap, prepackageHashMap, prepackageProductItemMap, toleranceHashMap, recentBatchMap);
        }
        return cogs;
    }

    /**
     * Public method to calculate cogs for invoices and transaction
     *
     * @param productBatchMap          : product batch map
     * @param prepackageHashMap        : prepackage hash map
     * @param prepackageProductItemMap : prepackage product item map
     * @param toleranceHashMap         : tolerance hashmap
     * @param recentBatchMap           : recent batch map
     * @return : return total cogs
     */

    /**
     * Public method to calculate cogs for cart
     *
     * @param cart                     : cart
     * @param productBatchMap          : product batch map
     * @param prepackageHashMap        : prepackage hashmap
     * @param prepackageProductItemMap : prepackage product item map
     * @param toleranceHashMap         : tolerance map
     * @param recentBatchMap           : recent batch map
     * @return : return cogs
     */
    public double calculateCogs(Cart cart, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, Map<String, ProductBatch> recentBatchMap) {
        double totalCogs = 0.0;

        for (OrderItem item : cart.getItems()) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(item.getPrepackageItemId());
            boolean calculate = false;
            if (prepackageProductItem != null) {
                ProductBatch batch = productBatchMap.get(prepackageProductItem.getBatchId());
                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                if (batch != null && prepackage != null) {
                    BigDecimal unitValue = prepackage.getUnitValue();
                    if (unitValue == null || unitValue.doubleValue() == 0) {
                        ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                        unitValue = weightTolerance.getUnitValue();
                    }
                    totalCogs += batch.getFinalUnitCost().doubleValue() * (item.getQuantity().doubleValue() * unitValue.doubleValue());
                    calculate = true;
                }
            } else if (item.getQuantityLogs() != null && !item.getQuantityLogs().isEmpty()) {
                for (QuantityLog log : item.getQuantityLogs()) {
                    ProductBatch batch = productBatchMap.get(log.getBatchId());
                    if (batch != null) {
                        totalCogs += batch.getFinalUnitCost().doubleValue() * log.getQuantity().doubleValue();
                        calculate = true;
                    }
                }
            }
            if (!calculate) {
                ProductBatch batch = recentBatchMap.get(item.getProductId());
                BigDecimal unitPrice = (batch == null) ? BigDecimal.ZERO : batch.getFinalUnitCost();
                totalCogs += item.getQuantity().multiply(unitPrice).doubleValue();
            }
        }
        return totalCogs;
    }

    private double getAmountPaid(List<PaymentsReceived> paymentsReceiveds) {
        BigDecimal totalPaidAmount = BigDecimal.ZERO;
        if (paymentsReceiveds != null && !paymentsReceiveds.isEmpty()) {
            for (PaymentsReceived paymentsReceived : paymentsReceiveds) {
                totalPaidAmount = totalPaidAmount.add(paymentsReceived.getAmountPaid());
            }
        }
        return totalPaidAmount.doubleValue();
    }

    /**
     * Public method to get transactions for invoices
     *
     * @param filter                : filter
     * @param invoiceIds            : invoiceids
     * @param invoiceTransactionMap : invoice transaction map
     * @return
     */
    public HashMap<String, Transaction> getTransactionsForInvoice(ReportFilter filter, List<String> invoiceIds, HashMap<String, List<String>> invoiceTransactionMap) {
        return getTransactionsForInvoice(filter, invoiceIds, invoiceTransactionMap, new HashSet<>());
    }

    public HashMap<String, Transaction> getTransactionsForInvoice(ReportFilter filter, List<String> invoiceIds, HashMap<String, List<String>> invoiceTransactionMap, Set<String> inProgressManifest) {
        List<ObjectId> transactionIds = new ArrayList<>();

        HashMap<String, List<ShippingManifestResult>> invoiceShippingManifest = shippingManifestRepository.findItemsInAsMapByInvoice(filter.getCompanyId(), filter.getShopId(), invoiceIds, "{status:1, transactionId:1, invoiceId:1, productMetrcInfo:1}");

        for (Map.Entry<String, List<ShippingManifestResult>> entry : invoiceShippingManifest.entrySet()) {
            for (ShippingManifestResult shippingManifestResult : entry.getValue()) {
                List<String> transactionList = invoiceTransactionMap.getOrDefault(shippingManifestResult.getInvoiceId(), new ArrayList<>());
                if (shippingManifestResult.getStatus() == ShippingManifest.ShippingManifestStatus.Completed && StringUtils.isNotBlank(shippingManifestResult.getTransactionId()) && ObjectId.isValid(shippingManifestResult.getTransactionId())) {
                    transactionIds.add(new ObjectId(shippingManifestResult.getTransactionId()));
                    transactionList.add(shippingManifestResult.getTransactionId());
                }
                if (shippingManifestResult.getStatus() == ShippingManifest.ShippingManifestStatus.InProgress) {
                    inProgressManifest.add(shippingManifestResult.getInvoiceId());
                }
                if (!transactionList.isEmpty()) {
                    invoiceTransactionMap.put(shippingManifestResult.getInvoiceId(), transactionList);
                }
            }
        }

        return transactionRepository.listAsMap(filter.getCompanyId(), transactionIds);
    }
}
