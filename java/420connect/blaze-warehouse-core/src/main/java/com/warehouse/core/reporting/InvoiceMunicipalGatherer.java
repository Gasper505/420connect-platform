package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class InvoiceMunicipalGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ShopRepository shopRepository;

    private String[] attrs = {
            "Date of Transaction",
            "Amount of Sale or Service Fee",
            "Customer",
            "Customer Licensed Address",
            "Customer License Type",
            "Pick-Up Address",
            "Pick- Up Address Municipality",
            "Delivery Address",
            "Delivery Address Municipality",
            "Transport Provided by STATE Distribution"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    @Inject
    public InvoiceMunicipalGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = {
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Invoice Municipal Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<Invoice> invoices = invoiceRepository.getInvoicesByStatusInvoiceDate(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(Invoice.InvoiceStatus.COMPLETED), Lists.newArrayList(Invoice.PaymentStatus.PAID), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<ObjectId> vendorIds = new HashSet<>();
        for (Invoice invoice : invoices) {
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }

        }

        Shop shop = shopRepository.getById(filter.getShopId());

        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));

        Vendor vendor;
        CompanyLicense companyLicense = null;

        String nAvail = "N/A";

        for (Invoice invoice : invoices) {
            HashMap<String, Object> data = new HashMap<>();

            vendor = vendorMap.get(invoice.getCustomerId());

            if (vendor != null) {
                companyLicense = vendor.getCompanyLicense(invoice.getLicenseId());
            }

            long invoiceDate = (invoice.getInvoiceDate() == null || invoice.getInvoiceDate() == 0) ? invoice.getCreated() : invoice.getInvoiceDate();
            int i = 0;

            String stateTransport, pickupAddress, pickupAddressMunicipal, deliveryAddress, deliveryAddressMunicipal;

            stateTransport = shop != null && shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getState()) ? "Yes" : "No";

            pickupAddress = pickupAddressMunicipal = (shop != null && shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getAddressString())) ? shop.getAddress().getAddressString() : nAvail;
            deliveryAddress = deliveryAddressMunicipal = vendor != null && vendor.getAddress() != null && StringUtils.isNotBlank(vendor.getAddress().getAddressString()) ? vendor.getAddress().getAddressString() : nAvail;

            data.put(attrs[i++], ProcessorUtil.dateStringWithOffset(invoiceDate, filter.getTimezoneOffset()));
            data.put(attrs[i++], NumberUtils.truncateDecimal(invoice.getCart().getTotal().doubleValue(), 2));
            data.put(attrs[i++], vendor != null && StringUtils.isNotBlank(vendor.getName()) ? vendor.getName() : nAvail);
            data.put(attrs[i++], vendor != null && vendor.getAddress() != null && StringUtils.isNotBlank(vendor.getAddress().getAddressString()) ? vendor.getAddress().getAddressString() : nAvail);
            data.put(attrs[i++], companyLicense != null ? companyLicense.getCompanyType() : nAvail);
            data.put(attrs[i++], pickupAddress);
            data.put(attrs[i++], pickupAddressMunicipal);
            data.put(attrs[i++], deliveryAddress);
            data.put(attrs[i++], deliveryAddressMunicipal);
            data.put(attrs[i], stateTransport);

            report.add(data);

        }
        return report;
    }
}
