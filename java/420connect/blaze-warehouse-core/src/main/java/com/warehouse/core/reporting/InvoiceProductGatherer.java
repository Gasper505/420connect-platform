package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.*;

public class InvoiceProductGatherer implements Gatherer {

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private InvoiceGatherer invoiceGatherer;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;

    private String[] attrs = new String[]{
            "Date", //0
            "Invoice No.", //1
            "Trans Type", //2
            "Status", //3
            "Product Name", //4
            "Product Category", //5
            "Brand Name", //6
            "Vendor", //7
            "Tax Type", //8
            "Cannabis", //9
            "Payment Terms", //10
            "Payment Status", //11
            "Shipping Status", //12
            "Quantity Sold", //13
            "Batch", //14
            "COGs", //15
            "Retail Value", //16
            "Product Discounts", //17
            "Subtotal", //18
            "Cart Discounts", //19
            "Post ALExcise Tax", //20
            "Post NALExcise Tax", //21
            "Total Tax", //22
            "Delivery Fees", //23
            "Gross Receipt", //24
            "Employee", //25
            "Due Date", //26
            "Paid Date" //27

    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InvoiceProductGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Date 0
                GathererReport.FieldType.STRING, // invoiceNo 1
                GathererReport.FieldType.STRING, // transaction type 2
                GathererReport.FieldType.STRING, // status 3
                GathererReport.FieldType.STRING, // Product Name 4
                GathererReport.FieldType.STRING, // Product Category 5
                GathererReport.FieldType.STRING, // Brand Name 6
                GathererReport.FieldType.STRING, // Vendor 7
                GathererReport.FieldType.STRING, // tax type 8
                GathererReport.FieldType.STRING, // cannabis 9
                GathererReport.FieldType.STRING, // payment term 10
                GathererReport.FieldType.STRING, // payment status 11
                GathererReport.FieldType.STRING, // shipping status 12
                GathererReport.FieldType.STRING, // quantity sold 13
                GathererReport.FieldType.STRING, // batch 14
                GathererReport.FieldType.CURRENCY, // cogs 15
                GathererReport.FieldType.CURRENCY, // Cost 16
                GathererReport.FieldType.CURRENCY, // product discount 17
                GathererReport.FieldType.CURRENCY, // Subtotal 18
                GathererReport.FieldType.CURRENCY, // cart discounts 19
                GathererReport.FieldType.CURRENCY, // post AL excise tax 20
                GathererReport.FieldType.CURRENCY, // post NAL excise tax 21
                GathererReport.FieldType.CURRENCY, // post taxes 22
                GathererReport.FieldType.CURRENCY, // delivery fees 23
                GathererReport.FieldType.CURRENCY, // total sale 24
                GathererReport.FieldType.STRING, // employee 25
                GathererReport.FieldType.STRING, //Due Date 26
                GathererReport.FieldType.STRING, //Paid Date 27
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "All Invoice Detail Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);


        List<Invoice.InvoiceStatus> invoiceStatuses = Lists.newArrayList(Invoice.InvoiceStatus.IN_PROGRESS, Invoice.InvoiceStatus.SENT, Invoice.InvoiceStatus.COMPLETED);
        List<Invoice.PaymentStatus> paymentStatuses = Lists.newArrayList(Invoice.PaymentStatus.PAID, Invoice.PaymentStatus.PARTIAL, Invoice.PaymentStatus.UNPAID);

        Iterable<Invoice> results = invoiceRepository.getInvoicesByStatusInvoiceDate(filter.getCompanyId(), filter.getShopId(), invoiceStatuses, paymentStatuses,
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<ObjectId> employeeIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> prepackageIds = new HashSet<>();
        Set<ObjectId> vendorIds = new HashSet<>();
        List<String> invoiceIds = new ArrayList<>();
        HashMap<String, List<PaymentsReceived>> listOfPaymentReceived = null;

        for (Invoice invoice : results) {
            invoiceIds.add(invoice.getId());
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }
            if (StringUtils.isNotBlank(invoice.getSalesPersonId()) && ObjectId.isValid(invoice.getSalesPersonId())) {
                employeeIds.add(new ObjectId(invoice.getSalesPersonId()));
            }
            if (invoice.getCart() == null || invoice.getCart().getItems() == null || invoice.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : invoice.getCart().getItems()) {
                productIds.add(new ObjectId(item.getProductId()));
                if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                    prepackageIds.add(new ObjectId(item.getPrepackageId()));
                }
            }
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        Map<String, ProductBatch> recentBatchMap = batchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(prepackageIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(filter.getCompanyId());
        Set<String> inProgressManifest = new HashSet<>();
        HashMap<String, List<String>> invoiceTransactionMap = new HashMap<>();

        HashMap<String, Transaction> transactionHashMap = invoiceGatherer.getTransactionsForInvoice(filter, invoiceIds, invoiceTransactionMap, inProgressManifest);

        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : productHashMap.values()) {
            List<Product> items = productsByCompanyLinkId.getOrDefault(p.getCompanyLinkId(), new ArrayList<>());
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        listOfPaymentReceived = invoicePaymentsRepository.findItemsInAsMapByInvoice(filter.getCompanyId(),filter.getShopId(),invoiceIds, "");

        for (Invoice invoice : results) {

            if (invoice.getCart() == null) {
                continue;
            }

            Cart cart = invoice.getCart();

            // Calculate discounts proportionally so we can apply taxes

            List<String> transactionList = invoiceTransactionMap.get(invoice.getId());
            if (!CollectionUtils.isEmpty(transactionList)) {
                for (String transactionId : transactionList) {
                    Transaction transaction = transactionHashMap.get(transactionId);
                   if(Objects.nonNull(transaction)) {
                       cart = transaction.getCart();
                       prepareReportDetails(filter, report, invoice, cart, productHashMap, employeeMap, vendorHashMap, categoryHashMap, brandHashMap, allBatchMap, recentBatchMap, prepackageHashMap, prepackageProductItemHashMap, toleranceHashMap, productsByCompanyLinkId, listOfPaymentReceived, true, inProgressManifest);
                   }
                }
            } else {
                prepareReportDetails(filter, report, invoice, cart, productHashMap, employeeMap, vendorHashMap, categoryHashMap, brandHashMap, allBatchMap, recentBatchMap, prepackageHashMap, prepackageProductItemHashMap, toleranceHashMap, productsByCompanyLinkId, listOfPaymentReceived, false, inProgressManifest);
            }

        }
        return report;
    }

    private void prepareReportDetails(ReportFilter filter,
                                      GathererReport report,
                                      Invoice invoice,
                                      Cart cart,
                                      HashMap<String, Product> productHashMap,
                                      HashMap<String, Employee> employeeMap,
                                      HashMap<String, Vendor> vendorHashMap,
                                      HashMap<String, ProductCategory> categoryHashMap,
                                      HashMap<String, Brand> brandHashMap,
                                      HashMap<String, ProductBatch> allBatchMap,
                                      Map<String, ProductBatch> recentBatchMap,
                                      HashMap<String, Prepackage> prepackageHashMap,
                                      HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                      HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                      HashMap<String, List<Product>> productsByCompanyLinkId,
                                      HashMap<String, List<PaymentsReceived>> listOfPaymentReceived,
                                      boolean shipped,
                                      Set<String> invoiceManifestList) {

        Employee employee = employeeMap.get(invoice.getSalesPersonId());
        Vendor vendor = vendorHashMap.get(invoice.getCustomerId());
        StringBuilder employeeName = new StringBuilder();
        StringBuilder vendorName = new StringBuilder();
        String nAvail = "N/A";
        List<PaymentsReceived> receivedPayments = null;
        long paidDate = 0;

        if (employee != null) {
            employeeName.append(employee.getFirstName()).append(StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() + " " : "");
        }

        if (vendor != null) {
            vendorName.append(vendor.getName());
        }


        String consumerTaxType = nAvail;

        if (vendor != null) {
            CompanyLicense companyLicense = vendor.getCompanyLicense(invoice.getLicenseId());
            consumerTaxType = String.valueOf(companyLicense.getCompanyType());
        }

        String processedDate = ProcessorUtil.timeStampWithOffset(invoice.getInvoiceDate(), filter.getTimezoneOffset());

        HashMap<String, Double> propRatioMap = new HashMap<>();

        if (!cart.getItems().isEmpty()) {

            double total = 0.0;

            // Sum up the total
            for (OrderItem orderItem : invoice.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += orderItem.getFinalPrice().doubleValue();
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : invoice.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product == null) {
                    continue;
                }
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product.isDiscountable() && total > 0) {
                    double finalCost = orderItem.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }
            }
        }
        // Now do real calculation
        for (OrderItem item : cart.getItems()) {

            String productId = item.getProductId();
            Product product = productHashMap.get(productId);
            if (product == null) {
                continue;
            }

            ProductCategory category = categoryHashMap.get(product.getCategoryId());
            Brand brand = brandHashMap.get(product.getBrandId());
            ProductCategory.UnitType unitType = (category != null) ? category.getUnitType() : ProductCategory.UnitType.units;


            Double ratio = propRatioMap.get(item.getId());
            if (ratio == null) {
                ratio = 1d;
            }

            Double propCartDiscount = invoice.getCart().getCalcCartDiscount() != null ? invoice.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
            Double propDeliveryFee = invoice.getCart().getDeliveryFee() != null ? invoice.getCart().getDeliveryFee().doubleValue() * ratio : 0;

            double postALExciseTax = 0;
            double postNALExciseTax = 0;
            boolean cannabis = false;

            double totalTax = 0;
            if (item.getTaxResult() != null) {
                postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();
                if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                    totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                }
            }

            if (totalTax == 0) {
                totalTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
            }

            if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                    || (product.getCannabisType() != Product.CannabisType.CBD
                    && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                    && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                cannabis = true;
            }

            double finalAfterTax = item.getFinalPrice().doubleValue() - propCartDiscount
                    + propDeliveryFee + totalTax;

            StringBuilder batchLogs = new StringBuilder();
            double cogs = 0d;
            boolean calculated = false;
            String productName = product.getName();

            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());

            if (prepackageProductItem != null) {
                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                if (!Objects.isNull(prepackage) && !Objects.isNull(targetBatch)) {
                    BigDecimal unitValue = prepackage.getUnitValue();
                    if (unitValue == null || unitValue.doubleValue() == 0) {
                        ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                        unitValue = weightTolerance.getUnitValue();
                    }
                    // calculate the total quantity based on the prepackage value
                    double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                    cogs += calculateCogs(unitsSold, targetBatch);

                    batchLogs.append(targetBatch.getSku());

                    productName = String.format("%s (%s)", product.getName(), prepackage.getName());
                    calculated = true;
                }
            } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                // otherwise, use quantity logs
                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                    if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                        ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                        if (targetBatch != null) {
                            calculated = true;
                            cogs += calculateCogs(quantityLog.getQuantity().doubleValue(), targetBatch);
                        }
                    }
                    ProductBatch batch = allBatchMap.get(quantityLog.getBatchId());
                    if (batch == null) {
                        batch = recentBatchMap.get(product.getId());
                    }
                    batchLogs.append(StringUtils.isNotBlank(batchLogs) ? "," : "").append((batch != null) ? batch.getSku() : "");
                }
            }

            if (!calculated) {
                double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                cogs = unitCost * item.getQuantity().doubleValue();
            }


            String unitTypeStr = (category != null && category.getUnitType() == ProductCategory.UnitType.grams) ? "g" : "ea";

            double itemCogs = cogs;
            double itemCost = item.getCost().doubleValue();
            double itemDiscount = item.getCalcDiscount().doubleValue();
            double itemFinalPrice = item.getFinalPrice().doubleValue();
            double itemCartDiscount = propCartDiscount;

            double itemPOSTALExciseTax = postALExciseTax;
            double itemPOSTNALExciseTax = postNALExciseTax;
            double itemTotalTAx = totalTax;
            double itemDeliveryFee = propDeliveryFee;

            if (Objects.nonNull(listOfPaymentReceived) && listOfPaymentReceived.containsKey(invoice.getId())) {
                receivedPayments = listOfPaymentReceived.get(invoice.getId());
                receivedPayments.sort(Comparator.comparing(PaymentsReceived::getCreated));
                paidDate = receivedPayments.get(receivedPayments.size() - 1).getPaidDate();
            }

            String shippedStatus = "Not Started";
            if (invoice.getInvoiceStatus() == Invoice.InvoiceStatus.COMPLETED) {
                shippedStatus = "Shipped";
            } else if (shipped) {
                shippedStatus = "Partial Shipped";
            } else if (invoiceManifestList.contains(invoice.getId())) {
                shippedStatus = "In Progress";
            }

            HashMap<String, Object> data = new HashMap<>();
            int i = 0;
            data.put(attrs[i++], processedDate);
            data.put(attrs[i++], invoice.getInvoiceNumber());
            data.put(attrs[i++], vendor.getArmsLengthType().toString());
            data.put(attrs[i++], invoice.getInvoiceStatus());
            data.put(attrs[i++], productName);
            data.put(attrs[i++], (category == null || StringUtils.isBlank(category.getName())) ? nAvail : category.getName());
            data.put(attrs[i++], (brand == null || StringUtils.isBlank(brand.getName())) ? nAvail : brand.getName());
            data.put(attrs[i++], (vendor == null || StringUtils.isBlank(vendor.getName())) ? nAvail : vendor.getName()); // vendor
            data.put(attrs[i++], consumerTaxType);
            data.put(attrs[i++], cannabis ? "Yes" : "No");
            data.put(attrs[i++], invoice.getInvoiceTerms());
            data.put(attrs[i++], invoice.getInvoicePaymentStatus());
            data.put(attrs[i++], shippedStatus);


            data.put(attrs[i++], item.getQuantity().doubleValue() + " " + unitTypeStr);
            data.put(attrs[i++], batchLogs.toString());
            data.put(attrs[i++], new DollarAmount(itemCogs)); // cogs
            data.put(attrs[i++], new DollarAmount(itemCost));
            data.put(attrs[i++], new DollarAmount(itemDiscount));
            data.put(attrs[i++], new DollarAmount(itemFinalPrice));
            data.put(attrs[i++], new DollarAmount(itemCartDiscount));

            data.put(attrs[i++], new DollarAmount(itemPOSTALExciseTax));
            data.put(attrs[i++], new DollarAmount(itemPOSTNALExciseTax));

            data.put(attrs[i++], new DollarAmount(itemTotalTAx));
            data.put(attrs[i++], new DollarAmount(itemDeliveryFee));
            data.put(attrs[i++], new DollarAmount(finalAfterTax));

            data.put(attrs[i++], employeeName);
            data.put(attrs[i++], ProcessorUtil.timeStampWithOffset(invoice.getDueDate(), filter.getTimezoneOffset()));
            data.put(attrs[i++], paidDate != 0 ? ProcessorUtil.timeStampWithOffset(paidDate, filter.getTimezoneOffset()) : "N/A");
            report.add(data);
        }
    }

    private double calculateCogs(final double quantity, final ProductBatch batch) {
        double unitCost = (batch == null) ? 0 : batch.getFinalUnitCost().doubleValue();
        return (quantity * unitCost);
    }

    private double getUnitCost(Product product, Map<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(product.getId());
        if (batch == null) {
            List<Product> products = linkToProductMap.get(product.getCompanyLinkId());
            if (!CollectionUtils.isEmpty(products)) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}
