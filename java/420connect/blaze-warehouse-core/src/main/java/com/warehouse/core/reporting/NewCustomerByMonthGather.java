package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.CustomerByCurrentMonth;

import javax.inject.Inject;
import java.util.*;

public class NewCustomerByMonthGather implements Gatherer {
    @Inject
    private VendorRepository vendorRepository;
    private String[] attrs = new String[]{"New Customer Count"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public NewCustomerByMonthGather() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.NUMBER};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "New Customers Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        CustomerByCurrentMonth customerByCurrentMonth = vendorRepository.getCustomerByCurrentMonth(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, Object> data = new HashMap<>();
        data.put(attrs[0], customerByCurrentMonth.getCount());
        report.add(data);

        return report;
    }
}
