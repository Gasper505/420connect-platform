package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.DateUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.services.invoice.InvoiceService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class OutStandingInvoiceGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private InvoiceService invoiceService;
    @Inject
    private VendorRepository vendorRepository;

    private String[] attrs = new String[]{"Invoice #", "Company", "Past due", "Overdue by", "Id"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public OutStandingInvoiceGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "OutStanding Invoices", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        Map<String, String> filterMap = filter.getMap();
        Integer size = 5;
        if (filterMap.get("size") != null) {
            size = Integer.parseInt(filterMap.get("size"));
        }
        List<Invoice.InvoiceStatus> invoiceStatus = new ArrayList<>();
        List<Invoice.PaymentStatus> paymentStatus = new ArrayList<>();
        invoiceStatus.add(Invoice.InvoiceStatus.IN_PROGRESS);
        invoiceStatus.add(Invoice.InvoiceStatus.SENT);
        invoiceStatus.add(Invoice.InvoiceStatus.COMPLETED);
        paymentStatus.add(Invoice.PaymentStatus.UNPAID);
        paymentStatus.add(Invoice.PaymentStatus.PARTIAL);

        Iterable<InvoiceResult> invoiceResults = invoiceRepository.getBracketSalesByShopId(filter.getCompanyId(), filter.getShopId(), size, invoiceStatus, paymentStatus, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Company company = companyRepository.getById(filter.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company is not found.");
        }

        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }

        if (invoiceResults != null) {
            List<ObjectId> customerCompanyIds = new ArrayList<>();

            for (InvoiceResult invoice : invoiceResults) {
                if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                    customerCompanyIds.add(new ObjectId(invoice.getCustomerId()));
                }
            }
            if (!customerCompanyIds.isEmpty()) {
                HashMap<String, Vendor> customerCompanyMap = vendorRepository.listAsMap(filter.getCompanyId(), customerCompanyIds);

                if (customerCompanyMap != null && !customerCompanyIds.isEmpty()) {
                    for (InvoiceResult invoiceResult : invoiceResults) {

                        HashMap<String, Object> data = new HashMap<>();

                        if (invoiceResult == null) {
                            continue;
                        }

                        data.put(attrs[0], invoiceResult.getInvoiceNumber());
                        Vendor customerCompany = customerCompanyMap.get(invoiceResult.getCustomerId());
                        if (customerCompany != null) {
                            data.put(attrs[1], customerCompany.getName());
                        } else {
                            data.put(attrs[1], "");
                        }
                        data.put(attrs[4], invoiceResult.getId().toString());

                        BigDecimal amountPaid = new BigDecimal(0);
                        BigDecimal balanceDue = new BigDecimal(0);
                        BigDecimal totalInvoiceAmount = new BigDecimal(0);
                        if (invoiceResult.getCart() != null && invoiceResult.getCart().getTotal() != null)
                            totalInvoiceAmount = invoiceResult.getCart().getTotal();
                        Map<String, Object> returnMap = invoiceService.calculateBalanceDue(invoiceResult.getId(), totalInvoiceAmount, amountPaid);
                        balanceDue = (BigDecimal) returnMap.get("balanceDue");

                        data.put(attrs[2], balanceDue);

                        Long dueDate = invoiceResult.getCreated();
                        DateTime startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                        DateTime endDate = DateUtil.nowWithTimeZone(shop.getTimeZone());
                        long daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());

                        switch (invoiceResult.getInvoiceTerms()) {
                            case NET_7:
                                daysBetweenTwoDates = daysBetweenTwoDates - 7;
                                break;
                            case NET_15:
                                daysBetweenTwoDates = daysBetweenTwoDates - 15;
                                break;
                            case NET_30:
                                daysBetweenTwoDates = daysBetweenTwoDates - 30;
                                break;
                            case NET_45:
                                daysBetweenTwoDates = daysBetweenTwoDates - 45;
                                break;
                            case NET_60:
                                daysBetweenTwoDates = daysBetweenTwoDates - 60;
                                break;
                            case CUSTOM_DATE:
                                dueDate = invoiceResult.getDueDate();
                                startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                                daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                                break;
                            default:
                                break;
                        }
                        daysBetweenTwoDates = Math.abs(daysBetweenTwoDates);
                        String days = daysBetweenTwoDates + " days";
                        if (daysBetweenTwoDates == 0) {
                            days = "Today";
                        } else if (daysBetweenTwoDates == 1) {
                            days = daysBetweenTwoDates + " day";
                        }
                        data.put(attrs[3], days);

                        report.add(data);
                    }
                }
            }
        }
        return report;
    }
}
