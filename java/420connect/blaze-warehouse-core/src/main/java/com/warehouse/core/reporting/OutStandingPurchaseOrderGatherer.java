package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class OutStandingPurchaseOrderGatherer implements Gatherer {
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private VendorRepository vendorRepository;

    private String[] attrs = new String[]{"PO #", "Company", "Past due", "Overdue by", "Id"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public OutStandingPurchaseOrderGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "OutStanding Purchase Order", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        Map<String, String> filterMap = filter.getMap();
        Integer size = 5;
        if (filterMap.get("size") != null) {
            size = Integer.parseInt(filterMap.get("size"));
        }

        PurchaseOrder.CustomerType customerType = PurchaseOrder.CustomerType.VENDOR;

        SearchResult<PurchaseOrderItemResult> purchaseOrderResults = purchaseOrderRepository.getAllPurchaseOrderByDate(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", 0, size, customerType, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Company company = companyRepository.getById(filter.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company is not found.");
        }
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }

        if (purchaseOrderResults != null && purchaseOrderResults.getValues() != null) {

            List<ObjectId> customerCompanyIds = new ArrayList<>();

            for (PurchaseOrderItemResult poItem : purchaseOrderResults.getValues()) {
                if (StringUtils.isNotBlank(poItem.getVendorId()) && ObjectId.isValid(poItem.getVendorId())) {
                    customerCompanyIds.add(new ObjectId(poItem.getVendorId()));
                }
            }

            if (!customerCompanyIds.isEmpty()) {
                HashMap<String, Vendor> customerCompanyMap = vendorRepository.listAsMap(filter.getCompanyId(), customerCompanyIds);

                if (customerCompanyMap != null && !customerCompanyIds.isEmpty()) {
                    for (PurchaseOrderItemResult poItem : purchaseOrderResults.getValues()) {

                        HashMap<String, Object> data = new HashMap<>();
                        data.put(attrs[0], poItem.getPoNumber());

                        data.put(attrs[4], poItem.getId());

                        Vendor customerCompany = customerCompanyMap.get(poItem.getVendorId());
                        if (customerCompany != null) {
                            data.put(attrs[1], customerCompany.getName());
                        } else {
                            data.put(attrs[1], "");
                        }

                        BigDecimal amountPaid = new BigDecimal(0);
                        if (poItem.getAmountPaid() != null) {
                            amountPaid = poItem.getAmountPaid();
                        }
                        BigDecimal balanceDue = poItem.getGrandTotal().subtract(amountPaid);
                        balanceDue = (balanceDue.doubleValue() < 0) ? new BigDecimal(0.0) : balanceDue;
                        data.put(attrs[2], balanceDue);

                        Long dueDate = poItem.getCreated();
                        DateTime startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                        DateTime endDate = DateUtil.nowWithTimeZone(shop.getTimeZone());
                        long daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());

                        switch (poItem.getPoPaymentTerms()) {
                            case NET_7:
                                daysBetweenTwoDates = daysBetweenTwoDates - 7;
                                break;
                            case NET_15:
                                daysBetweenTwoDates = daysBetweenTwoDates - 15;
                                break;
                            case NET_30:
                                daysBetweenTwoDates = daysBetweenTwoDates - 30;
                                break;
                            case NET_45:
                                daysBetweenTwoDates = daysBetweenTwoDates - 45;
                                break;
                            case NET_60:
                                daysBetweenTwoDates = daysBetweenTwoDates - 60;
                                break;
                            case CUSTOM_DATE:
                                dueDate = poItem.getCustomTermDate();
                                startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                                daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                                break;
                            default:
                                break;
                        }
                        daysBetweenTwoDates = Math.abs(daysBetweenTwoDates);
                        String days = daysBetweenTwoDates + " days";
                        if (daysBetweenTwoDates == 0) {
                            days = "Today";
                        } else if (daysBetweenTwoDates == 1) {
                            days = daysBetweenTwoDates + " day";
                        }
                        data.put(attrs[3], days);

                        report.add(data);
                    }
                }
            }
        }
        return report;
    }
}
