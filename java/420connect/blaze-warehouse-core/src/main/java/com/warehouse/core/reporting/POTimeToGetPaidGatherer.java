package com.warehouse.core.reporting;


import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;


import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class POTimeToGetPaidGatherer implements Gatherer {
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ShipmentBillRepository shipmentBillRepository;


    private String[] attrs = new String[]{"PO #", "Vendor", "PO Date", "Due Date", "Total", "Overdue By", "PO Status", "Paid Status"};

    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public POTimeToGetPaidGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Time to Get Paid - Purchase Orders", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());

        List<PurchaseOrder.PurchaseOrderStatus> statusList = new ArrayList<>();
        statusList.add(PurchaseOrder.PurchaseOrderStatus.Approved);

        statusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.WaitingShipment);

        Iterable<PurchaseOrder> purchaseOrders = purchaseOrderRepository.getPurchaseOrderByStatus(filter.getCompanyId(), filter.getShopId(), statusList, PurchaseOrder.CustomerType.VENDOR, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<ObjectId> vendorIds = new ArrayList<>();
        List<ObjectId> shipmentBillIds = new ArrayList<>();
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            if (StringUtils.isNotBlank(purchaseOrder.getVendorId()) && ObjectId.isValid(purchaseOrder.getVendorId())) {
                vendorIds.add(new ObjectId(purchaseOrder.getVendorId()));
            }
            if (StringUtils.isNotBlank(purchaseOrder.getShipmentBillId()) && ObjectId.isValid(purchaseOrder.getShipmentBillId())) {
                shipmentBillIds.add(new ObjectId(purchaseOrder.getShipmentBillId()));
            }
        }


        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), vendorIds);
        HashMap<String, ShipmentBill> shipmentHashMap = shipmentBillRepository.listAsMap(filter.getCompanyId(), shipmentBillIds);

        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            HashMap<String, Object> data = new HashMap<>();
            int i = 0;
            data.put(attrs[i], purchaseOrder.getPoNumber());
            Vendor vendor = vendorHashMap.get(purchaseOrder.getVendorId());
            if (vendor == null) {
                data.put(attrs[++i], "N/A");
            } else {
                data.put(attrs[++i], vendor.getName() != null ? vendor.getName() : TextUtil.textOrEmpty(""));
            }
            data.put(attrs[++i], ProcessorUtil.dateStringWithOffset(purchaseOrder.getPurchaseOrderDate() == 0 ? purchaseOrder.getCreated() : purchaseOrder.getPurchaseOrderDate(), filter.getTimezoneOffset()));
            data.put(attrs[++i], ProcessorUtil.dateStringWithOffset(purchaseOrder.getDueDate() == 0 ? purchaseOrder.getPODueDate() : purchaseOrder.getDueDate(), filter.getTimezoneOffset()));
            data.put(attrs[++i], purchaseOrder.getGrandTotal() != null ? new DollarAmount(purchaseOrder.getGrandTotal().doubleValue()) : new DollarAmount(0.0));

            Long dueDate = purchaseOrder.getCreated();
            DateTime startDate = DateUtil.toDateTime(dueDate, StringUtils.isNotBlank(shop.getTimeZone()) ? shop.getTimeZone() :  ConnectAuthToken.DEFAULT_REQ_TIMEZONE);
            DateTime endDate = DateUtil.nowWithTimeZone(StringUtils.isNotBlank(shop.getTimeZone()) ? shop.getTimeZone() :  ConnectAuthToken.DEFAULT_REQ_TIMEZONE);

            long daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
            switch (purchaseOrder.getPoPaymentTerms()) {
                case NET_7:
                    daysBetweenTwoDates = daysBetweenTwoDates - 7;
                    break;
                case NET_15:
                    daysBetweenTwoDates = daysBetweenTwoDates - 15;
                    break;
                case NET_30:
                    daysBetweenTwoDates = daysBetweenTwoDates - 30;
                    break;
                case NET_45:
                    daysBetweenTwoDates = daysBetweenTwoDates - 45;
                    break;
                case NET_60:
                    daysBetweenTwoDates = daysBetweenTwoDates - 60;
                    break;
                case CUSTOM_DATE:
                    dueDate = purchaseOrder.getCustomTermDate();
                    startDate = DateUtil.toDateTime(dueDate, StringUtils.isNotBlank(shop.getTimeZone()) ? shop.getTimeZone() :  ConnectAuthToken.DEFAULT_REQ_TIMEZONE);
                    daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                    break;
                default:
                    break;
            }
            daysBetweenTwoDates = Math.abs(daysBetweenTwoDates);
            String days = daysBetweenTwoDates + " days";
            if (daysBetweenTwoDates == 0) {
                days = "Today";
            } else if (daysBetweenTwoDates == 1) {
                days = daysBetweenTwoDates + " day";
            }
            data.put(attrs[++i], days);
            data.put(attrs[++i], purchaseOrder.getPurchaseOrderStatus());

            ShipmentBill shipmentBill = shipmentHashMap.get(purchaseOrder.getShipmentBillId());
            if (shipmentBill == null) {
                data.put(attrs[++i], "UNPAID");
            } else {
                if (shipmentBill.getPaymentStatus().equals(ShipmentBill.PaymentStatus.UNPAID) && shipmentBill.getAmountPaid().doubleValue() > 0) {
                    data.put(attrs[++i], TextUtil.textOrEmpty("PARTIAL"));
                } else {
                    data.put(attrs[++i], shipmentBill.getPaymentStatus() != null ? shipmentBill.getPaymentStatus() : TextUtil.textOrEmpty(" "));
                }
            }
            report.add(data);
        }

        return report;
    }

}


