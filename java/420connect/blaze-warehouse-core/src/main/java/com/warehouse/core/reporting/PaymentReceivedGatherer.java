package com.warehouse.core.reporting;

import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

public class PaymentReceivedGatherer implements Gatherer {

    @Inject
    private InvoicePaymentsRepository invoicePaymentRepository;
    @Inject
    private InvoiceRepository invoiceRepository;

    private String[] attrs = new String[]{"Invoice No", "Reference No", "Payment No", "Invoice Date", "Invoice Due Date", "Paid Date", "Payment Type", "Total", "Received Amount"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public PaymentReceivedGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Payment Received Report", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        DateSearchResult<PaymentsReceived> paymentsReceiveds = invoicePaymentRepository.getAllInvoicePaymentsByDate(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{modified:-1}");
        HashMap<String, Invoice> invoiceHashMap = invoiceRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        if (paymentsReceiveds != null) {
            List<PaymentsReceived> paymentsReceivedList = paymentsReceiveds.getValues();
            if (CollectionUtils.isNotEmpty(paymentsReceivedList)) {
                for (PaymentsReceived paymentsReceived : paymentsReceivedList) {
                    if (!Objects.isNull(paymentsReceived) && MapUtils.isNotEmpty(invoiceHashMap)) {
                        Invoice invoice = invoiceHashMap.get(paymentsReceived.getInvoiceId());
                        if (!Objects.isNull(invoice)) {
                            HashMap<String, Object> data = new HashMap<>();
                            data.put(attrs[0], invoice.getInvoiceNumber());
                            data.put(attrs[1], paymentsReceived.getReferenceNo());
                            data.put(attrs[2], paymentsReceived.getPaymentNo());
                            data.put(attrs[3], ProcessorUtil.dateString(invoice.getInvoiceDate()));
                            data.put(attrs[4], ProcessorUtil.dateString(invoice.getDueDate()));
                            data.put(attrs[5], ProcessorUtil.dateString(paymentsReceived.getPaidDate()));
                            data.put(attrs[6], paymentsReceived.getPaymentType());
                            data.put(attrs[7], invoice.getTotal());
                            data.put(attrs[8], paymentsReceived.getAmountPaid());
                            report.add(data);
                        }
                    }
                }
            }
        }
        return report;
    }
}