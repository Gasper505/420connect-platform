package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PurchaseOrderMunicipalGatherer implements Gatherer {

    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ShopRepository shopRepository;

    private String[] attrs = {
            "Date of Transaction",
            "Amount of Purchase",
            "Seller",
            "Seller Licensed Address",
            "Seller License Type",
            "Pick-Up Address",
            "Pick- Up Address Municipality",
            "Delivery Address",
            "Delivery Address Municipality",
            "Transport Provided by STATE Distribution"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    @Inject
    public PurchaseOrderMunicipalGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = {
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Purchase Order Municipal Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<PurchaseOrder> purchaseOrders = purchaseOrderRepository.getPurchaseOrderByStatusPurchaseDate(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(PurchaseOrder.PurchaseOrderStatus.Closed), PurchaseOrder.CustomerType.VENDOR, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<ObjectId> vendorIds = new HashSet<>();
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            if (StringUtils.isNotBlank(purchaseOrder.getVendorId()) && ObjectId.isValid(purchaseOrder.getVendorId())) {
                vendorIds.add(new ObjectId(purchaseOrder.getVendorId()));
            }

        }

        Shop shop = shopRepository.getById(filter.getShopId());

        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));

        Vendor vendor;
        CompanyLicense companyLicense = null;

        String nAvail = "N/A";

        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            HashMap<String, Object> data = new HashMap<>();

            vendor = vendorMap.get(purchaseOrder.getVendorId());

            if (vendor != null) {
                companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());
            }

            long purchaseOrderDate = (purchaseOrder.getPurchaseOrderDate() == 0) ? purchaseOrder.getCreated() : purchaseOrder.getPurchaseOrderDate();
            int i = 0;

            String stateTransport, pickupAddress, pickupAddressMunicipal, deliveryAddress, deliveryAddressMunicipal;

            stateTransport = shop != null && shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getState()) ? "Yes" : "No";

            pickupAddress = pickupAddressMunicipal = (vendor != null && vendor.getAddress() != null && StringUtils.isNotBlank(vendor.getAddress().getAddressString())) ? vendor.getAddress().getAddressString() : nAvail;

            deliveryAddress = deliveryAddressMunicipal = nAvail;

            if (StringUtils.isNotBlank(purchaseOrder.getDeliveryAddress())) {
                deliveryAddress = deliveryAddressMunicipal = purchaseOrder.getDeliveryAddress();
            } else if (shop != null && shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getAddressString())) {
                deliveryAddress = deliveryAddressMunicipal = shop.getAddress().getAddressString();
            }

            data.put(attrs[i++], ProcessorUtil.dateStringWithOffset(purchaseOrderDate, filter.getTimezoneOffset()));
            data.put(attrs[i++], NumberUtils.truncateDecimal(purchaseOrder.getGrandTotal().doubleValue(), 2));
            data.put(attrs[i++], vendor != null && StringUtils.isNotBlank(vendor.getName()) ? vendor.getName() : nAvail);
            data.put(attrs[i++], vendor != null && vendor.getAddress() != null && StringUtils.isNotBlank(vendor.getAddress().getAddressString()) ? vendor.getAddress().getAddressString() : nAvail);
            data.put(attrs[i++], companyLicense != null ? companyLicense.getCompanyType() : nAvail);
            data.put(attrs[i++], pickupAddress);
            data.put(attrs[i++], pickupAddressMunicipal);
            data.put(attrs[i++], deliveryAddress);
            data.put(attrs[i++], deliveryAddressMunicipal);
            data.put(attrs[i], stateTransport);

            report.add(data);

        }
        return report;
    }
}
