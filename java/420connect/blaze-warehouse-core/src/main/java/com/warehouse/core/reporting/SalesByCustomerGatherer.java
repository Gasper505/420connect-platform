package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class SalesByCustomerGatherer implements Gatherer {
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private InvoiceGatherer invoiceGatherer;
    @Inject
    private ShopRepository shopRepository;


    private String[] attrs = new String[]{"Customer","Total Sales", "Tax", "Sales Before Tax", "Discounts", "COGS", "Profit", "Percentage"};
    private List<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    @Inject
    public SalesByCustomerGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Customer Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);


        Iterable<Invoice> invoices = invoiceRepository.getInvoicesByStatusInvoiceDate(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(Invoice.InvoiceStatus.COMPLETED), Lists.newArrayList(Invoice.PaymentStatus.values()), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        List<String> invoiceIds = new ArrayList<>();
        Set<ObjectId> vendorIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> prepackageIds = new HashSet<>();

        for (Invoice invoice : invoices) {
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }
            if (invoice.getCart() == null || invoice.getCart().getItems() == null || invoice.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : invoice.getCart().getItems()) {
                productIds.add(new ObjectId(item.getProductId()));
                if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                    prepackageIds.add(new ObjectId(item.getPrepackageId()));
                }
            }
        }
        HashMap<String, List<String>> invoiceTransactionMap = new HashMap<>();

        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        Map<String, ProductBatch> recentBatchMap = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));
        HashMap<String, ProductBatch> allBatchMap = productBatchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(prepackageIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Transaction> transactionHashMap = invoiceGatherer.getTransactionsForInvoice(filter, invoiceIds, invoiceTransactionMap);


        HashMap<String, SalesByCustomer> salesMap = new HashMap<>();
        BigDecimal totalSaleOfInvoices = BigDecimal.ZERO;

        for (Invoice invoice : invoices) {
            totalSaleOfInvoices = invoice.getCart() == null ? BigDecimal.ZERO : totalSaleOfInvoices.add(invoice.getCart().getTotal());
            salesMap.putIfAbsent(invoice.getCustomerId(), new SalesByCustomer());
            SalesByCustomer salesByCustomer = salesMap.get(invoice.getCustomerId());
            salesByCustomer.sales = salesByCustomer.sales.add(invoice.getCart() == null ? BigDecimal.ZERO : invoice.getCart().getTotal());
            salesByCustomer.salesBeforeTax = salesByCustomer.salesBeforeTax.add(invoice.getCart() == null ? BigDecimal.ZERO : invoice.getCart().getSubTotal());
            salesByCustomer.discount = salesByCustomer.discount.add(invoice.getCart() == null ? BigDecimal.ZERO : invoice.getCart().getCalcCartDiscount());
            salesByCustomer.tax = salesByCustomer.tax.add(invoice.getCart() == null || invoice.getCart().getTaxResult() == null ? BigDecimal.ZERO : invoice.getCart().getTaxResult().getTotalPostCalcTax());
            salesByCustomer.cogs = salesByCustomer.cogs.add(BigDecimal.valueOf(invoiceGatherer.calculateTotalCogs(invoice, transactionHashMap, invoiceTransactionMap, allBatchMap, prepackageHashMap, prepackageProductItemHashMap, toleranceHashMap, recentBatchMap)));
            Vendor vendor = vendorMap.get(invoice.getCustomerId());
            salesByCustomer.vendor = vendor != null ? vendor.getName() : "N/A";
        }



        Shop shop = shopRepository.get(filter.getCompanyId(),filter.getShopId());
        List<HashMap<String, Object>> dataList = new ArrayList<>();
        for (Map.Entry<String, SalesByCustomer> entry : salesMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            SalesByCustomer value = entry.getValue();
            int i  = 0;
            BigDecimal percent = (totalSaleOfInvoices == BigDecimal.ZERO) ? BigDecimal.ZERO : value.sales.divide(totalSaleOfInvoices,6,
                RoundingMode.CEILING).multiply(new BigDecimal(100));
            data.put(attrs[i++], value.vendor);
            data.put(attrs[i++], NumberUtils.round(value.sales, 2));
            data.put(attrs[i++], NumberUtils.round(value.tax, 2));
            data.put(attrs[i++], NumberUtils.round(value.salesBeforeTax, 2));
            data.put(attrs[i++], NumberUtils.round(value.discount, 2));
            data.put(attrs[i++], NumberUtils.round(value.cogs, 2));
            data.put(attrs[i++], NumberUtils.round(value.getProfit(), 2));
            data.put(attrs[i], NumberUtils.round(percent, 2));

          dataList.add(data);

        }

        if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
            dataList.sort((o1, o2) -> {
                Double sale1 = (double) o1.get(attrs[1]);
                Double sale2 = (double) o2.get(attrs[1]);

                return sale2.compareTo(sale1);

            });
            dataList = dataList.subList(0, dataList.size() < 5 ? dataList.size() : 5);
        } else {
            dataList.sort((o1, o2) -> {
                String vendorName1 = String.valueOf(o1.get(attrs[0]));
                String vendorName2 = String.valueOf(o2.get(attrs[0]));

                return vendorName1.compareTo(vendorName2);
            });
        }

        report.setData(dataList);

        return report;
    }

    public static class SalesByCustomer {
        BigDecimal sales = BigDecimal.ZERO;
        BigDecimal discount = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        BigDecimal salesBeforeTax = BigDecimal.ZERO;
        BigDecimal cogs = BigDecimal.ZERO;
        String vendor = "N/A";

        BigDecimal getProfit() {
            return salesBeforeTax.subtract(cogs);
        }
    }
}
