package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.*;

import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByProductCategory;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

public class SalesByProductCategoryForInvoiceGatherer implements Gatherer {
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private String[] attrs = new String[]{"Category Name", "Count", "Sold Quantity", "Total Cost"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByProductCategoryForInvoiceGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Sales by Product Category Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));

        int limit = filter.getLimit() == 0 ? 10 : filter.getLimit();

        Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<ObjectId> productIds = new ArrayList<>();
        Set<ObjectId> productCategoryIds = new HashSet<>();

        if (invoiceResult != null) {
            List<Invoice> invoiceList = Lists.newArrayList(invoiceResult);
            for (Invoice invoice : invoiceList) {
                if (invoice.getCart() == null || CollectionUtils.isEmpty(invoice.getCart().getItems())) {
                    continue;
                }
                for (OrderItem productRequest : invoice.getCart().getItems()) {
                    if (StringUtils.isNotBlank(productRequest.getProductId()) && ObjectId.isValid(productRequest.getProductId())) {
                        productIds.add(new ObjectId(productRequest.getProductId()));
                    }
                }
            }
        }

        if (productIds.isEmpty()) {
            return report;
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), productIds);

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productCategoryIds));

        HashMap<String, SalesByProductCategory> salesByCategoryMap = new HashMap<>();

        for (Invoice invoice : invoiceResult) {
            if (invoice.getCart() == null || CollectionUtils.isEmpty(invoice.getCart().getItems())) {
                continue;
            }
            for (OrderItem item : invoice.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());
                if (product == null || StringUtils.isBlank(product.getCategoryId())) {
                    continue;
                }

                ProductCategory category = categoryHashMap.get(product.getCategoryId());

                if (category == null) {
                    continue;
                }

                HashMap<String, Double> propRatioMap = new HashMap<>();

                     double total = 0.0;

                    // Sum up the total
                    for (OrderItem orderItem : invoice.getCart().getItems()) {
                        product = productHashMap.get(orderItem.getProductId());
                        if (product != null && product.isDiscountable()) {
                            total += orderItem.getFinalPrice().doubleValue();
                        }
                    }

                    // Calculate the ratio to be applied
                    for (OrderItem orderItem : invoice.getCart().getItems()) {
                        product = productHashMap.get(orderItem.getProductId());
                        if (product == null) {
                            continue;
                        }
                        propRatioMap.put(orderItem.getId(), 1d); // 100 %
                        if (product.isDiscountable() && total > 0) {
                            double finalCost = orderItem.getFinalPrice().doubleValue();
                            double ratio = finalCost / total;

                            propRatioMap.put(orderItem.getId(), ratio);
                        }
                    }


                Double ratio = propRatioMap.get(item.getId());
                if (ratio == null) {
                    ratio = 1d;
                }

                Double propCartDiscount = invoice.getCart().getCalcCartDiscount() != null ? invoice.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propDeliveryFee = invoice.getCart().getDeliveryFee() != null ? invoice.getCart().getDeliveryFee().doubleValue() * ratio : 0;

                double totalTax = 0;
                if (item.getTaxResult() != null) {
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                    }
                }

                if (totalTax == 0) {
                    totalTax = item.getCalcTax().doubleValue();
                }

                if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                        || (product.getCannabisType() != Product.CannabisType.CBD
                        && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                        && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                }

                double finalAfterTax = item.getFinalPrice().doubleValue() - propCartDiscount
                        + propDeliveryFee + totalTax;

                SalesByProductCategory salesByProductCategory = salesByCategoryMap.getOrDefault(category.getId(), new SalesByProductCategory());

                salesByProductCategory.setCategoryId(category.getId());

                double totalSoldQuantity = item.getQuantity().doubleValue();

                salesByProductCategory.setTotalSoldCost(salesByProductCategory.getTotalSoldCost() + finalAfterTax);

                salesByProductCategory.setSoldQuantity(salesByProductCategory.getSoldQuantity() + totalSoldQuantity);
                salesByProductCategory.setCount(salesByProductCategory.getCount() + 1);
                salesByCategoryMap.put(category.getId(), salesByProductCategory);
            }
        }
        if (!salesByCategoryMap.isEmpty()) {
            for (SalesByProductCategory salesByCat : salesByCategoryMap.values()) {
                HashMap<String, Object> data = new HashMap<>();
                ProductCategory productCategory = categoryHashMap.get(salesByCat.getCategoryId());
                if (productCategory != null) {
                    data.put(attrs[0], productCategory.getName());
                    data.put(attrs[1], salesByCat.getCount());
                    data.put(attrs[2], salesByCat.getSoldQuantity());
                    data.put(attrs[3],  new DollarAmount(salesByCat.getTotalSoldCost()));

                    report.add(data);
                }
            }
        }

        return report;
    }

}
