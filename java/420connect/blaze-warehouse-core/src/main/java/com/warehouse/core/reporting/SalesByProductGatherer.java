package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class SalesByProductGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private String[] attrs = new String[]{"Product Name", "Category", "Vendor", "Units Sold", "Total Cost", "Tax", "Final Price", "Total Discount"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByProductGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Sales By Product", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);


        Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId());
        HashMap<String, SalesByProduct> sales = new HashMap<>();

        if (invoiceResult != null) {
            for (Invoice invoice : invoiceResult) {
                if (invoice.getCart() == null || invoice.getCart().getItems() == null) {
                    continue;
                }
                if (vendorHashMap == null || productHashMap == null || productCategoryHashMap == null) {
                    continue;
                }
                List<OrderItem> items = invoice.getCart().getItems();
                for (OrderItem item : items) {
                    SalesByProduct salesByProduct = new SalesByProduct();
                    Product product = productHashMap.get(item.getProductId());
                    if (product == null) {
                        continue;
                    }
                    Vendor vendor = vendorHashMap.get(product.getVendorId());
                    ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                    if (vendor == null || productCategory == null) {
                        continue;
                    }
                    BigDecimal unitsSold = BigDecimal.ZERO;
                    BigDecimal totalCost = BigDecimal.ZERO;
                    BigDecimal tax = BigDecimal.ZERO;
                    BigDecimal finalPrice = BigDecimal.ZERO;
                    BigDecimal totalDiscount = BigDecimal.ZERO;
                    if (sales.containsKey(item.getProductId())) {
                        SalesByProduct salesByProducts = sales.get(item.getProductId());
                        unitsSold = salesByProducts.getSoldUnits();
                        totalCost = salesByProducts.getTotalCost();
                        tax = salesByProducts.getTax();
                        finalPrice = salesByProducts.getFinalPrice();
                        totalDiscount = salesByProducts.getTotalDiscount();
                    }
                    salesByProduct.setProductName(product.getName());
                    salesByProduct.setVendor((vendor != null) ? vendor.getName() : "");
                    salesByProduct.setCategory((productCategory != null) ? productCategory.getName() : "");
                    salesByProduct.setSoldUnits(unitsSold.add(item.getQuantity()));
                    salesByProduct.setTotalCost(totalCost.add(item.getCost()));
                    salesByProduct.setTax(tax.add(item.getTaxResult().getTotalPostCalcTax()));
                    salesByProduct.setFinalPrice(finalPrice.add(item.getFinalPrice()));
                    salesByProduct.setTotalDiscount(totalDiscount.add(item.getDiscount()));
                    sales.put(item.getProductId(), salesByProduct);
                }
            }
        }

        for (String key : sales.keySet()) {
            SalesByProduct salesByProduct = sales.get(key);
            if (salesByProduct != null) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], salesByProduct.getProductName());
                data.put(attrs[1], salesByProduct.getCategory());
                data.put(attrs[2], salesByProduct.getVendor());
                data.put(attrs[3], salesByProduct.getSoldUnits());
                data.put(attrs[4], salesByProduct.getTotalCost());
                data.put(attrs[5], salesByProduct.getTax());
                data.put(attrs[6], salesByProduct.getFinalPrice());
                data.put(attrs[7], salesByProduct.getTotalDiscount());
                report.add(data);
            }
        }
        return report;
    }
}

class SalesByProduct {

    private String productName;
    private String category;
    private String vendor;
    private BigDecimal soldUnits;
    private BigDecimal totalCost;
    private BigDecimal tax;
    private BigDecimal finalPrice;
    private BigDecimal totalDiscount;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public BigDecimal getSoldUnits() {
        return soldUnits;
    }

    public void setSoldUnits(BigDecimal soldUnits) {
        this.soldUnits = soldUnits;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }
}
