package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SalesBySalesPersonGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private VendorRepository vendorRepository;

    private String[] attrs = new String[]{
            "Sales Person Name",
            "Invoice #",
            "Date",
            "Company",
            "Discount",
            "Shipping Charges",
            "Total Tax",
            "Sub Total",
            "Total",
            "Status"
    };
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesBySalesPersonGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Sales By Sales Person Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Invoice> invoiceResults;
        if (ReportFilter.ALL_EMPLOYEES.equalsIgnoreCase(filter.getEmployeeId())) {
            invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        } else {
            invoiceResults = invoiceRepository.getInvoicesNonDraftByEmployee(filter.getCompanyId(), filter.getShopId(), "{modified:-1}",
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getEmployeeId());
        }

        List<ObjectId> employeeIds = new ArrayList<>();
        List<ObjectId> vendorIds = new ArrayList<>();
        Map<String, List<Invoice>> employeeInvoiceMap = new HashMap<>();
        if (invoiceResults != null) {
            for (Invoice invoice : invoiceResults) {
                employeeIds.add(new ObjectId(invoice.getSalesPersonId()));
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
                employeeInvoiceMap.putIfAbsent(invoice.getSalesPersonId(), new ArrayList<>());
                List<Invoice> invoices = employeeInvoiceMap.get(invoice.getSalesPersonId());
                invoices.add(invoice);
                employeeInvoiceMap.put(invoice.getSalesPersonId(), invoices);
            }
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(filter.getCompanyId(), employeeIds);
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(filter.getCompanyId(), vendorIds);
        for (String employeeId : employeeInvoiceMap.keySet()) {
            Employee employee = employeeMap.get(employeeId);
            if (employee == null) {
                continue;
            }

            List<Invoice> invoiceList = employeeInvoiceMap.get(employeeId);
            if (invoiceList.size() == 0) {
                continue;
            }

            for (Invoice invoice : invoiceList) {
                Vendor vendor = vendorMap.get(invoice.getCustomerId());
                if (vendor == null) {
                    continue;
                }
                HashMap<String, Object> data = new HashMap<>();
                Cart cart = invoice.getCart();

                data.put(attrs[0], employee.getFirstName() + " " + employee.getLastName());
                data.put(attrs[1], invoice.getInvoiceNumber());
                data.put(attrs[2], ProcessorUtil.timeStampWithOffset(invoice.getInvoiceDate(), filter.getTimezoneOffset()));
                data.put(attrs[3], vendor.getName());
                data.put(attrs[4], cart.getTotalDiscount());
                data.put(attrs[5], cart.getDeliveryFee());
                data.put(attrs[6], (cart.getTaxResult() == null) ? 0 : cart.getTaxResult().getTotalPostCalcTax());
                data.put(attrs[7], cart.getSubTotal());
                data.put(attrs[8], cart.getTotal());
                data.put(attrs[9], invoice.getInvoicePaymentStatus());

                report.add(data);
            }

        }


        return report;
    }
}
