package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SalesCogsGatherer implements Gatherer {

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductWeightToleranceRepository toleranceRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private InvoiceGatherer invoiceGatherer;
    private String[] attrs = new String[]{
            "Month",
            "Total Sales",
            "COGS",
            "Gross Profit"};

    public SalesCogsGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales Cogs Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        List<Invoice.InvoiceStatus> invoiceStatusList = Lists.newArrayList(Invoice.InvoiceStatus.COMPLETED);
        List<Invoice.PaymentStatus> paymentList = Lists.newArrayList(Invoice.PaymentStatus.values());

        Iterable<Invoice> invoices = invoiceRepository.getInvoicesByStatusInvoiceDate(filter.getCompanyId(), filter.getShopId(), invoiceStatusList, paymentList, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<String> productIds = new HashSet<>();
        Set<ObjectId> prepackageIds = new HashSet<>();
        List<String> invoiceIds = new ArrayList<>();
        HashMap<String, List<String>> invoiceTransactionMap = new HashMap<>();

        for (Invoice invoice : invoices) {
            invoiceIds.add(invoice.getId());
            if (invoice.getCart() == null || invoice.getCart().getItems() == null || invoice.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : invoice.getCart().getItems()) {
                productIds.add(item.getProductId());
                if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                    prepackageIds.add(new ObjectId(item.getPrepackageId()));
                }
            }
        }

        Map<String, ProductBatch> recentBatchMap = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, ProductBatch> allBatchMap = productBatchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(prepackageIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = toleranceRepository.listAsMap(filter.getCompanyId());

        //Get transactions for quantity logs from shipping manifest in map
        HashMap<String, Transaction> transactionHashMap = invoiceGatherer.getTransactionsForInvoice(filter, invoiceIds, invoiceTransactionMap);

        HashMap<Long, SalesCogs> salesCogsMap = new HashMap<>();

        for (Invoice invoice : invoices) {
            DateTime dt = new DateTime(invoice.getModified()).plusMinutes(filter.getTimezoneOffset());
            dt = dt.minusDays(dt.getDayOfMonth() - 1).withTimeAtStartOfDay();

            SalesCogs salesCogs = salesCogsMap.getOrDefault(dt.getMillis(), new SalesCogs());
            salesCogs.totalSales += invoice.getCart().getTotal().doubleValue();
            salesCogs.cogs += invoiceGatherer.calculateTotalCogs(invoice, transactionHashMap, invoiceTransactionMap, allBatchMap, prepackageHashMap, prepackageProductItemHashMap, toleranceHashMap, recentBatchMap);
            salesCogsMap.put(dt.getMillis(), salesCogs);
        }

        for (Map.Entry<Long, SalesCogs> entry : salesCogsMap.entrySet()) {
            SalesCogs salesCogs = entry.getValue();
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.dateString(entry.getKey()));
            data.put(attrs[1], NumberUtils.truncateDecimal(salesCogs.totalSales));
            data.put(attrs[2], NumberUtils.truncateDecimal(salesCogs.cogs));
            data.put(attrs[3], salesCogs.getGrossProfit());

            report.add(data);
        }

        report.getData().sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
                DateTime dt1 = DateUtil.parseDate(map1.get(attrs[0]).toString());
                DateTime dt2 = DateUtil.parseDate(map2.get(attrs[0]).toString());
                return dt1.compareTo(dt2);
            }
        });
        return report;

    }

    public class SalesCogs {
        double totalSales;
        double cogs;

        public double getGrossProfit() {
            return NumberUtils.truncateDecimal((totalSales - cogs));
        }
    }
}
