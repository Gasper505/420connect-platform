package com.warehouse.core.reporting;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class SalesExpenseReportGatherer implements Gatherer {
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ExpensesRepository expensesRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;

    private String[] attrs = new String[]{"Day", "Expense", "Sales", "Purchase Order", "Expense Total", "Invoice Total", "Purchase Order Total"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesExpenseReportGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Sales Expense", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);


        List<Invoice.InvoiceStatus> invoiceStatusList = new ArrayList<>();
        List<Invoice.PaymentStatus> invoicePaymentList = new ArrayList<>();
        List<PurchaseOrder.PurchaseOrderStatus> purchaseOrderStatusList = new ArrayList<>();

        invoiceStatusList.add(Invoice.InvoiceStatus.IN_PROGRESS);
        invoiceStatusList.add(Invoice.InvoiceStatus.SENT);

        invoicePaymentList.add(Invoice.PaymentStatus.UNPAID);
        invoicePaymentList.add(Invoice.PaymentStatus.PARTIAL);

        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.Approved);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.WaitingShipment);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.Closed);

        PurchaseOrder.CustomerType customerType = PurchaseOrder.CustomerType.VENDOR;

        Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(),  "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Iterable<Expenses> expensesResult = expensesRepository.listByShopWithDates(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Iterable<PurchaseOrder> purchaseOrderResult = purchaseOrderRepository.getPurchaseOrderByStatus(filter.getCompanyId(), filter.getShopId(), purchaseOrderStatusList, customerType, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<Long, SalesAndExpenses> salesExpensesHashMap = new HashMap<>();

        for (Invoice invoice : invoiceResult) {
            long dateLong = invoice.getInvoiceDate();
            long key = getMapKey(dateLong, filter);
            int count = 0;
            double total = invoice.getCart() != null ? invoice.getCart().getTotal().doubleValue() : 0D;
            SalesAndExpenses salesAndExpenses = new SalesAndExpenses();
            if (salesExpensesHashMap.containsKey(key)) {
                salesAndExpenses = salesExpensesHashMap.get(key);
                count = salesAndExpenses.getInvoicesCount();
                total += salesAndExpenses.getInvoiceTotal().doubleValue();
            }
            salesAndExpenses.setInvoicesCount(++count);
            salesAndExpenses.setInvoiceTotal(BigDecimal.valueOf(total));
            salesExpensesHashMap.put(key, salesAndExpenses);
        }
        for (Expenses expenses : expensesResult) {
            long dateLong = expenses.getCreated();
            long key = getMapKey(dateLong, filter);
            int count = 0;
            double total = expenses.getAmount() != null ? expenses.getAmount().doubleValue() : 0D;
            SalesAndExpenses salesAndExpenses = new SalesAndExpenses();
            if (salesExpensesHashMap.containsKey(key)) {
                salesAndExpenses = salesExpensesHashMap.get(key);
                count = salesAndExpenses.getExpenseCount();
                total += salesAndExpenses.getExpenseTotal().doubleValue();
            }
            salesAndExpenses.setExpenseCount(++count);
            salesAndExpenses.setExpenseTotal(BigDecimal.valueOf(total));
            salesExpensesHashMap.put(key, salesAndExpenses);
        }
        for (PurchaseOrder purchaseOrder : purchaseOrderResult) {
            long dateLong = purchaseOrder.getCreated();
            long month = getMapKey(dateLong, filter);
            int count = 0;
            double total = purchaseOrder.getGrandTotal() != null ? purchaseOrder.getGrandTotal().doubleValue() : 0D;
            SalesAndExpenses salesAndExpenses = new SalesAndExpenses();
            if (salesExpensesHashMap.containsKey(month)) {
                salesAndExpenses = salesExpensesHashMap.get(month);
                count = salesAndExpenses.getPoCount();
                total += salesAndExpenses.getPoTotal().doubleValue();
            }
            salesAndExpenses.setPoCount(++count);
            salesAndExpenses.setPoTotal(BigDecimal.valueOf(total));
            salesExpensesHashMap.put(month, salesAndExpenses);
        }
        for (Long key : salesExpensesHashMap.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.dateString(key));
            SalesAndExpenses salesAndExpenses = salesExpensesHashMap.get(key);
            data.put(attrs[1], salesAndExpenses.getExpenseCount());
            data.put(attrs[2], salesAndExpenses.getInvoicesCount());
            data.put(attrs[3], salesAndExpenses.getPoCount());
            data.put(attrs[4], NumberUtils.round(salesAndExpenses.getExpenseTotal(), 2));
            data.put(attrs[5], NumberUtils.round(salesAndExpenses.getInvoiceTotal(), 2));
            data.put(attrs[6], NumberUtils.round(salesAndExpenses.getPoTotal(), 2));
            report.add(data);
        }
        List<HashMap<String, Object>> data = report.getData();

        data.sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
                Long month1 = DateUtil.parseDate(o1.get("Day").toString()).getMillis();
                Long month2 = DateUtil.parseDate(o2.get("Day").toString()).getMillis();
                return month1.compareTo(month2);
            }
        });
        report.setData(data);


        return report;
    }

    private long getMapKey(long dateLong, ReportFilter filter) {
        DateTime dateTime = new DateTime(dateLong).plusMinutes(filter.getTimezoneOffset());
        long key;
        if (DateUtil.getMonthsBetweenTwoDates(filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis()) >= 2) {
            int dayOfWeek = dateTime.getDayOfWeek();
            key = dateTime.minusDays(dayOfWeek).withTimeAtStartOfDay().plusDays(1).getMillis();
        } else {
            DateTime dt = new DateTime(dateLong).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset());
            key = dt.getMillis();
        }
        return key;
    }

}

class SalesAndExpenses {

    private int invoicesCount;
    private int expenseCount;
    private int poCount;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal invoiceTotal = BigDecimal.ZERO;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal expenseTotal = BigDecimal.ZERO;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal poTotal = BigDecimal.ZERO;

    public int getInvoicesCount() {
        return invoicesCount;
    }

    public void setInvoicesCount(int invoicesCount) {
        this.invoicesCount = invoicesCount;
    }

    public int getExpenseCount() {
        return expenseCount;
    }

    public void setExpenseCount(int expenseCount) {
        this.expenseCount = expenseCount;
    }

    public int getPoCount() {
        return poCount;
    }

    public void setPoCount(int poCount) {
        this.poCount = poCount;
    }

    public BigDecimal getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(BigDecimal invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public BigDecimal getExpenseTotal() {
        return expenseTotal;
    }

    public void setExpenseTotal(BigDecimal expenseTotal) {
        this.expenseTotal = expenseTotal;
    }

    public BigDecimal getPoTotal() {
        return poTotal;
    }

    public void setPoTotal(BigDecimal poTotal) {
        this.poTotal = poTotal;
    }
}
