package com.warehouse.core.reporting;

import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class SalesRevenueByMonthGatherer implements Gatherer {

    @Inject
    private InvoiceRepository invoiceRepository;

    private String[] attrs = new String[]{"Sales Revenue"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesRevenueByMonthGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.BIGDECIMAL};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Sales Revenue by Month Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        List<Invoice.InvoiceStatus> invoiceStatusList = new ArrayList<>();
        List<Invoice.PaymentStatus> invoicePaymentList = new ArrayList<>();

        invoiceStatusList.add(Invoice.InvoiceStatus.COMPLETED);
        invoicePaymentList.add(Invoice.PaymentStatus.PAID);

        Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(), "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        BigDecimal totalInvoiceAmount = new BigDecimal(0.0);
        if (invoiceResult != null) {
            for (Invoice invoice : invoiceResult) {
                if (invoice.getCart() != null && invoice.getCart().getTotal() != null) {
                    totalInvoiceAmount = totalInvoiceAmount.add(invoice.getCart().getTotal());
                }
            }
        }
        HashMap<String, Object> data = new HashMap<>();
        data.put(attrs[0], totalInvoiceAmount);
        report.add(data);

        return report;
    }
}
