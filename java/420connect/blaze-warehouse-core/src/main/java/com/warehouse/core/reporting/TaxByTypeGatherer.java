package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class TaxByTypeGatherer implements Gatherer {
    @Inject
    PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    InvoiceRepository invoiceRepository;

    private String[] attrs = new String[]{"Excise", "Harvest"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TaxByTypeGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.BIGDECIMAL,
                GathererReport.FieldType.BIGDECIMAL};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Tax By Type", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        // Calculate Excise Tax
        List<Invoice.InvoiceStatus> invoiceStatus = new ArrayList<>();
        List<Invoice.PaymentStatus> paymentStatus = new ArrayList<>();
        invoiceStatus.add(Invoice.InvoiceStatus.COMPLETED);
        invoiceStatus.add(Invoice.InvoiceStatus.SENT);
        invoiceStatus.add(Invoice.InvoiceStatus.IN_PROGRESS);
        paymentStatus.add(Invoice.PaymentStatus.PAID);
        paymentStatus.add(Invoice.PaymentStatus.PARTIAL);

        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesNonDraft(filter.getCompanyId(), filter.getShopId(),  "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Object> data = new HashMap<>();

        if (invoiceResults != null) {
            BigDecimal totalExciseTax = BigDecimal.ZERO;
            for (Invoice invoiceResult : invoiceResults) {
                if (invoiceResult.getCart() == null)
                    continue;
                totalExciseTax = totalExciseTax.add(invoiceResult.getCart().getTotalCalcTax());
            }
            data.put(attrs[0], NumberUtils.round(totalExciseTax, 2));
        }

        //Calculate Harvest Tax
        List<PurchaseOrder.PurchaseOrderStatus> purchaseOrderStatusList = new ArrayList<>();
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.Approved);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.InProgress);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.Decline);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.WaitingShipment);
        purchaseOrderStatusList.add(PurchaseOrder.PurchaseOrderStatus.RequiredApproval);

        PurchaseOrder.CustomerType customerType = PurchaseOrder.CustomerType.VENDOR;
        Iterable<PurchaseOrder> purchaseOrders = purchaseOrderRepository.getPurchaseOrderByStatus(filter.getCompanyId(), filter.getShopId(), purchaseOrderStatusList, customerType, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        if (purchaseOrders != null) {
            BigDecimal totalHarvestTax = BigDecimal.ZERO;
            for (PurchaseOrder purchaseOrder : purchaseOrders) {
                totalHarvestTax = totalHarvestTax.add(purchaseOrder.getTotalTax());
            }
            data.put(attrs[1], NumberUtils.round(totalHarvestTax, 2));
            report.add(data);
        }
        return report;
    }
}
