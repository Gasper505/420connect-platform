package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

public class TimeToGetPaidGatherer implements Gatherer {
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ShopRepository shopRepository;

    private String[] attrs = new String[]{"Invoice No", "Customer Name", "Date", "Due Date", "Overdue By", "Total"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TimeToGetPaidGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Time to Get Paid - Invoices", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        List<Invoice.InvoiceStatus> invoiceStatus = new ArrayList<>();
        invoiceStatus.add(Invoice.InvoiceStatus.COMPLETED);
        invoiceStatus.add(Invoice.InvoiceStatus.IN_PROGRESS);
        invoiceStatus.add(Invoice.InvoiceStatus.SENT);

        List<Invoice.PaymentStatus> paymentStatus = new ArrayList<>();
        paymentStatus.add(Invoice.PaymentStatus.UNPAID);
        paymentStatus.add(Invoice.PaymentStatus.PARTIAL);

        Iterable<Invoice> invoiceResults = invoiceRepository.getInvoicesByStatus(filter.getCompanyId(), filter.getShopId(), invoiceStatus, paymentStatus, "{modified:-1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }
        if (invoiceResults != null) {
            for (Invoice invoiceResult : invoiceResults) {
                if (Objects.isNull(invoiceResult)) {
                    continue;
                }

                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], invoiceResult.getInvoiceNumber());
                if (vendorHashMap != null) {
                    Vendor vendor = vendorHashMap.get(invoiceResult.getCustomerId());
                    if (!Objects.isNull(vendor)) {
                        data.put(attrs[1], vendor.getName());
                    }
                }
                data.put(attrs[2], ProcessorUtil.dateStringWithOffset(invoiceResult.getInvoiceDate(), filter.getTimezoneOffset()));
                data.put(attrs[3], ProcessorUtil.dateStringWithOffset(invoiceResult.getDueDate(), filter.getTimezoneOffset()));
                Long dueDate = invoiceResult.getCreated();
                DateTime startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                DateTime endDate = DateUtil.nowWithTimeZone(shop.getTimeZone());
                long daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());

                switch (invoiceResult.getInvoiceTerms()) {
                    case NET_7:
                        daysBetweenTwoDates = daysBetweenTwoDates - 7;
                        break;
                    case NET_15:
                        daysBetweenTwoDates = daysBetweenTwoDates - 15;
                        break;
                    case NET_30:
                        daysBetweenTwoDates = daysBetweenTwoDates - 30;
                        break;
                    case NET_45:
                        daysBetweenTwoDates = daysBetweenTwoDates - 45;
                        break;
                    case NET_60:
                        daysBetweenTwoDates = daysBetweenTwoDates - 60;
                        break;
                    case CUSTOM_DATE:
                        dueDate = invoiceResult.getDueDate();
                        startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                        daysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                        break;
                    default:
                        break;
                }
                daysBetweenTwoDates = Math.abs(daysBetweenTwoDates);
                String days = daysBetweenTwoDates + " days";
                if (daysBetweenTwoDates == 0) {
                    days = "Today";
                } else if (daysBetweenTwoDates == 1) {
                    days = daysBetweenTwoDates + " day";
                }
                data.put(attrs[4], days);

                if (!Objects.isNull(invoiceResult.getCart())) {
                    data.put(attrs[5], invoiceResult.getCart().getTotal());
                }
                report.add(data);
            }
        }
        return report;
    }
}
