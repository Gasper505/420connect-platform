package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

public class TotalAvailableInventory implements Gatherer {

    @Inject
    private ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;

    private String[] attrs = new String[]{"Product Name", "Category", "Brand", "Status", "Batch SKU", "Sell By", "Total Available Raw Quantity", "Total Prepackage Quantity"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TotalAvailableInventory() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Total Inventory Available Report", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);


        Iterable<Product> products = null;
        if (StringUtils.isBlank(filter.getProductId()) && (StringUtils.isBlank(filter.getCategoryId()) || filter.getCategoryId().equalsIgnoreCase("all"))) {
            products = productRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        } else if (StringUtils.isNotBlank(filter.getProductId())) {
            Product product = productRepository.getById(filter.getCompanyId(), filter.getProductId());
            products = Lists.newArrayList(product);
        } else if (StringUtils.isNotBlank(filter.getCategoryId())) {
            SearchResult<Product> productSearchResult = productRepository.findProductsByCategoryId(filter.getCompanyId(), filter.getShopId(), filter.getCategoryId(), "{name:-1}", 0, Integer.MAX_VALUE, Product.class);
            products = productSearchResult.getValues();
        }

        if (products == null) {
            return report;
        }

        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> categoryIds = new HashSet<>();
        List<String> productIds = new ArrayList<>();

        for (Product product : products) {
            productIds.add(product.getId());
            if (StringUtils.isNotBlank(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
            if (StringUtils.isNotBlank(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }

        }
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds));
        HashMap<String, List<BatchQuantity>> quantityResultMap = batchQuantityRepository.getBatchQuantityForProduct(filter.getCompanyId(), filter.getShopId(), productIds);
        HashMap<String, ProductBatch> productBatchHashMap = batchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Prepackage> prepackageList = prepackageRepository.getPrepackagesForProducts(filter.getCompanyId(), filter.getShopId(), productIds, Prepackage.class);
        Iterable<PrepackageProductItem> prepackageProductItems = prepackageProductItemRepository.getPrepackagesForProducts(filter.getCompanyId(), filter.getShopId(), productIds);
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(filter.getCompanyId(), filter.getShopId(), productIds);

        ProductCategory category;
        Brand brand;
        List<BatchQuantity> quantityList;
        ProductBatch batch;
        for (Product product : products) {
            brand = brandHashMap.get(product.getBrandId());
            category = productCategoryHashMap.get(product.getCategoryId());
            quantityList = quantityResultMap.getOrDefault(product.getId(), new ArrayList<>());

            if (quantityList != null && !quantityList.isEmpty()) {
                for (BatchQuantity batchQuantity : quantityList) {
                    HashMap<String, Object> data = new HashMap<>(fieldTypes.size());

                    batch = productBatchHashMap.get(batchQuantity.getBatchId());

                    data.put(attrs[0], product.getName());
                    data.put(attrs[1], category == null || StringUtils.isBlank(category.getName()) ? "N/A" : category.getName());
                    data.put(attrs[2], brand == null || StringUtils.isBlank(brand.getName()) ? "N/A" : brand.getName());
                    data.put(attrs[3], product.isActive() ? "Active" : "InActive");
                    data.put(attrs[4], (batch == null) ? "No Batch" : batch.getSku());
                    data.put(attrs[5], (batch == null || batch.getSellBy() == 0) ? "N/A" :
                            ProcessorUtil.dateString(batch.getSellBy()));
                    data.put(attrs[6], batchQuantity.getQuantity());
                    data.put(attrs[7], getPrepackageInfo(prepackageList, prepackageProductItems, productPrepackageQuantities, product, batch));

                    report.add(data);
                }
            } else {
                HashMap<String, Object> data = new HashMap<>(fieldTypes.size());

                data.put(attrs[0], product.getName());
                data.put(attrs[1], category == null || StringUtils.isBlank(category.getName()) ? "N/A" : category.getName());
                data.put(attrs[2], brand == null || StringUtils.isBlank(brand.getName()) ? "N/A" : brand.getName());
                data.put(attrs[3], product.isActive() ? "Active" : "InActive");
                data.put(attrs[4], "No Batch");
                data.put(attrs[5], "N/A");
                data.put(attrs[6], 0);
                data.put(attrs[7], getPrepackageInfo(prepackageList, prepackageProductItems, productPrepackageQuantities, product, null));

                report.add(data);
            }

        }
        return report;
    }

    private StringBuilder getPrepackageInfo(Iterable<Prepackage> prepackageList, Iterable<PrepackageProductItem> prepackageProductItems, Iterable<ProductPrepackageQuantity> productPrepackageQuantities, Product product, ProductBatch batch) {
        StringBuilder info = new StringBuilder();

        for (Prepackage prepackage : prepackageList) {
            if (!product.getId().equals(prepackage.getProductId())) {
                continue;
            }
            for (PrepackageProductItem prepackageProductItem : prepackageProductItems) {
                if (!prepackageProductItem.getPrepackageId().equals(prepackage.getId())) {
                    continue;
                }
                double quantity = 0;
                for (ProductPrepackageQuantity prepackageQuantityItem : productPrepackageQuantities) {
                    if (!prepackageQuantityItem.getPrepackageItemId().equals(prepackageProductItem.getId())) {
                        continue;
                    }
                    if (batch != null && prepackageProductItem.getBatchId().equals(batch.getId())) {
                        quantity += prepackageQuantityItem.getQuantity();
                    } else if (batch == null) {
                        quantity += prepackageQuantityItem.getQuantity();
                    }
                }
                info.append(StringUtils.isNotBlank(info) ? ", " : "");
                info.append(String.format("%s : %s", prepackage.getName(), quantity));
            }
        }
        return info;
    }
}
