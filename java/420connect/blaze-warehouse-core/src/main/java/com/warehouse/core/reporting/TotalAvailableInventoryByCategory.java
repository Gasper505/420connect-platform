package com.warehouse.core.reporting;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TotalAvailableInventoryByCategory implements Gatherer {

    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;

    private String[] attrs = new String[]{"Category", "Unit Type", "Total Available Raw Quantity", "Total Prepackage Quantity"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TotalAvailableInventoryByCategory() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory Available By Category Report", reportHeaders);

        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();

        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<Product> products = null;

        if (StringUtils.isBlank(filter.getCategoryId()) || filter.getCategoryId().equals(ReportFilter.ALL_CATEGORIES)) {
            products = productRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        } else if (StringUtils.isNotBlank(filter.getCategoryId())) {
            products = productRepository.getProductsForCategory(filter.getCompanyId(), filter.getCategoryId());
        }

        if (products == null) {
            return report;
        }

        Set<ObjectId> categoryIds = new HashSet<>();
        List<String> productIds = new ArrayList<>();

        for (Product product : products) {
            productIds.add(product.getId());
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds));
        HashMap<String, List<BatchQuantity>> quantityResultMap = batchQuantityRepository.getBatchQuantityForProduct(filter.getCompanyId(), filter.getShopId(), productIds);
        Iterable<Prepackage> prepackages = prepackageRepository.getPrepackagesForProducts(filter.getCompanyId(), filter.getShopId(), productIds, Prepackage.class);
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(filter.getCompanyId(), filter.getShopId(), productIds);

        HashMap<String, StringBuilder> prepackageInfo = getPrepackageInfoByProduct(prepackages, productPrepackageQuantities);
        HashMap<String, InventoryByCategory> inventoryByCategoryMap = new HashMap<>();

        List<BatchQuantity> quantityList;
        String nAvail = "N/A";
        for (Product product : products) {
            inventoryByCategoryMap.putIfAbsent(product.getCategoryId(), new InventoryByCategory());
            InventoryByCategory inventoryByCategory = inventoryByCategoryMap.get(product.getCategoryId());

            ProductCategory category = categoryHashMap.get(product.getCategoryId());

            quantityList = quantityResultMap.getOrDefault(product.getId(), new ArrayList<>());
            double rawQuantity = 0;
            if (quantityList != null && !quantityList.isEmpty()) {
                for (BatchQuantity batchQuantity : quantityList) {
                    rawQuantity += batchQuantity.getQuantity().doubleValue();
                }
            }


            inventoryByCategory.categoryName = (category != null) ? category.getName() : nAvail;
            inventoryByCategory.categoryType = (category != null) ? category.getUnitType().getType() : "Units";
            inventoryByCategory.totalRawQuantity += rawQuantity;
            if (StringUtils.isBlank(inventoryByCategory.prepackageInfo)) {
                inventoryByCategory.prepackageInfo.append(prepackageInfo.getOrDefault(product.getId(), new StringBuilder(nAvail)));
            }
        }

        for (InventoryByCategory inventoryByCategory : inventoryByCategoryMap.values()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], inventoryByCategory.categoryName);
            data.put(attrs[1], inventoryByCategory.categoryType);
            data.put(attrs[2], inventoryByCategory.totalRawQuantity);
            data.put(attrs[3], inventoryByCategory.prepackageInfo);
            report.add(data);
        }
        return report;
    }

    private HashMap<String, StringBuilder> getPrepackageInfoByProduct(Iterable<Prepackage> prepackages, Iterable<ProductPrepackageQuantity> productPrepackageQuantities) {
        HashMap<String, StringBuilder> prepackageInfo = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            String prepackageName = prepackage.getName();
            StringBuilder info = prepackageInfo.getOrDefault(prepackage.getProductId(), new StringBuilder());
            int quantity = 0;
            for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
                if (!prepackageQuantity.getPrepackageId().equals(prepackage.getId())) {
                    continue;
                }
                quantity += prepackageQuantity.getQuantity();
            }

            info.append(StringUtils.isNotBlank(info) ? ", " : "").append(String.format("%s : %s", prepackageName, quantity));
            prepackageInfo.put(prepackage.getProductId(), info);
        }
        return prepackageInfo;
    }

    public class InventoryByCategory {
        double totalRawQuantity = 0;
        StringBuilder prepackageInfo = new StringBuilder();
        String categoryName = "", categoryType = "";
    }
}