package com.warehouse.core.rest.batch.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.domain.models.batch.IncomingBatch;

import java.util.ArrayList;
import java.util.List;

@Deprecated
@JsonIgnoreProperties(ignoreUnknown = true)
public class BulkBatchUpdateRequest {

    public enum BatchUpdateOperationType {
        STATUS, DELETE_BATCHES, UPDATE_BATCHES_TO_ARCHIVE, VOID_STATUS
    }

    private BatchUpdateOperationType operationType;
    private List<String> incomingBatchIds = new ArrayList<>();
    private IncomingBatch.BatchStatus batchStatus;
    private boolean archive = false;
    private boolean voidStatus = false;

    public List<String> getIncomingBatchIds() {
        return incomingBatchIds;
    }

    public void setIncomingBatchIds(List<String> incomingBatchIds) {
        this.incomingBatchIds = incomingBatchIds;
    }

    public IncomingBatch.BatchStatus getBatchStatus() {
        return batchStatus;
    }

    public void setBatchStatus(IncomingBatch.BatchStatus batchStatus) {
        this.batchStatus = batchStatus;
    }

    public BatchUpdateOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(BatchUpdateOperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public boolean isVoidStatus() {
        return voidStatus;
    }

    public void setVoidStatus(boolean voidStatus) {
        this.voidStatus = voidStatus;
    }
}
