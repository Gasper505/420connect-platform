package com.warehouse.core.rest.batch.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.domain.models.batch.IncomingBatch;

@Deprecated
@JsonIgnoreProperties(ignoreUnknown = true)
public class IncomingBatchStatusRequest {
    private IncomingBatch.BatchStatus updatedStatus;

    public IncomingBatch.BatchStatus getUpdatedStatus() {
        return updatedStatus;
    }

    public void setUpdatedStatus(IncomingBatch.BatchStatus updatedStatus) {
        this.updatedStatus = updatedStatus;
    }
}
