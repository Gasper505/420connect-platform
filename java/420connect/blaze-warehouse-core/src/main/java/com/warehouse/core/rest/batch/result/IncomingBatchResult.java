package com.warehouse.core.rest.batch.result;

import com.warehouse.core.domain.models.batch.IncomingBatch;

public class IncomingBatchResult extends IncomingBatch {
    private String brandName;
    private String vendorName;
    private String productName;
    private String productType;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }
}
