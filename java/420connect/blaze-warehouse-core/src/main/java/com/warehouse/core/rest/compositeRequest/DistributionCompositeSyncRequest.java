package com.warehouse.core.rest.compositerequest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.warehouse.core.rest.invoice.request.CompositeInvoiceUpdateRequest;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DistributionCompositeSyncRequest {
    private List<CompositeInvoiceUpdateRequest> invoice;
    private List<PurchaseOrder> purchaseOrder;
    private List<InventoryTransferHistory> inventoryTransfer;

    public List<CompositeInvoiceUpdateRequest> getInvoice() {
        return invoice;
    }

    public void setInvoice(List<CompositeInvoiceUpdateRequest> invoice) {
        this.invoice = invoice;
    }

    public List<PurchaseOrder> getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(List<PurchaseOrder> purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public List<InventoryTransferHistory> getInventoryTransfer() {
        return inventoryTransfer;
    }

    public void setInventoryTransfer(List<InventoryTransferHistory> inventoryTransfer) {
        this.inventoryTransfer = inventoryTransfer;
    }
}
