package com.warehouse.core.rest.dashboard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardWidgetAddRequest {

    private List<String> dashboardWidget = new ArrayList<>();
    private String employeeId;

    public List<String> getDashboardWidget() {
        return dashboardWidget;
    }

    public void setDashboardWidget(List<String> dashboardWidget) {
        this.dashboardWidget = dashboardWidget;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
