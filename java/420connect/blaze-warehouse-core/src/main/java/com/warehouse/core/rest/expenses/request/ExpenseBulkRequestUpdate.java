package com.warehouse.core.rest.expenses.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.domain.models.expenses.Expenses;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExpenseBulkRequestUpdate {
    public enum ExpenseOperationType {
        EXPENSE_CATEGORY, EXPENSE_DELETE, EXPENSE_ARCHIVE, EXPENSE_STATE
    }

    private ExpenseOperationType operationType;
    private List<String> expenseIds = new ArrayList<>();
    private Expenses.ExpenseCategory expenseCategory;
    private boolean isArchived;
    private boolean active;

    public ExpenseOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ExpenseOperationType operationType) {
        this.operationType = operationType;
    }

    public List<String> getExpenseIds() {
        return expenseIds;
    }

    public void setExpenseIds(List<String> expenseIds) {
        this.expenseIds = expenseIds;
    }

    public Expenses.ExpenseCategory getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(Expenses.ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
