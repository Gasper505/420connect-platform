package com.warehouse.core.rest.expenses.result;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;

public class ExpenseResultComplete extends ExpensesResult {
    private PurchaseOrder purchaseOrder;

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }
}
