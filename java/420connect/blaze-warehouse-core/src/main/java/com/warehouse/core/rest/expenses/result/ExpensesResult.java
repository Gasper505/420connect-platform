package com.warehouse.core.rest.expenses.result;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;

import java.util.List;

public class ExpensesResult extends Expenses {
    Vendor customerCompany;
    private List<ExpensesActivityLog> expensesActivityLogs;
    private String companyLogo;
    private String createdByEmployeeName;
    private PurchaseOrder purchaseOrder;

    public Vendor getCustomerCompany() {
        return customerCompany;
    }

    public void setCustomerCompany(Vendor customerCompany) {
        this.customerCompany = customerCompany;
    }

    public List<ExpensesActivityLog> getExpensesActivityLogs() {
        return expensesActivityLogs;
    }

    public void setExpensesActivityLogs(List<ExpensesActivityLog> expensesActivityLogs) {
        this.expensesActivityLogs = expensesActivityLogs;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getCreatedByEmployeeName() {
        return createdByEmployeeName;
    }

    public void setCreatedByEmployeeName(String createdByEmployeeName) {
        this.createdByEmployeeName = createdByEmployeeName;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }
}
