package com.warehouse.core.rest.inventory.request;

import com.fourtwenty.core.domain.models.generic.Address;

public class DeliveryInventoryRequest {
    private String stateLicense;
    private String licenseType;
    private String businessName;
    private Address address;
    private String phoneNumber;
    private String contactName;

    public String getStateLicense() {
        return stateLicense;
    }

    public void setStateLicense(String stateLicense) {
        this.stateLicense = stateLicense;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
}
