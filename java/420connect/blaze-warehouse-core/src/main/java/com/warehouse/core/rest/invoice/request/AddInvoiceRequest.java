package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import com.warehouse.core.domain.models.invoice.Invoice;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddInvoiceRequest implements InternalAllowable {


    private String customerId;
    @Deprecated
    private String license;
    private String invoiceNumber;
    private String orderNumber;
    private Invoice.InvoiceTerms invoiceTerms;
    private String salesPersonId;
    private List<CompanyAsset> attachments = new ArrayList<>();
    private Long dueDate;
    private String termsAndconditions;
    private List<Note> notes;
    private Long invoiceDate;
    private Long estimatedDeliveryDate;
    private Long estimatedDeliveryTime;
    private Invoice.InvoiceStatus invoiceStatus;
    private Invoice.PaymentStatus invoicePaymentStatus;
    private boolean inventoryProcessed;
    private Long inventoryProcessedDate;
    private boolean active;
    @Deprecated
    private boolean relatedEntity;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryCharges = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal total = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal wholeSaleCostOfCannabiesItems = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal markup = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal exciseTax = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal totalInvoiceAmount = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal wholeSaleCostOfNonCannabisItems = new BigDecimal(0);
    private Vendor.ArmsLengthType transactionType = Vendor.ArmsLengthType.ARMS_LENGTH;
    private Cart cart;
    private String companyContactId;
    private String licenseId;
    private String externalId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Invoice.InvoiceTerms getInvoiceTerms() {
        return invoiceTerms;
    }

    public void setInvoiceTerms(Invoice.InvoiceTerms invoiceTerms) {
        this.invoiceTerms = invoiceTerms;
    }

    public String getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(String salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public String getTermsAndconditions() {
        return termsAndconditions;
    }

    public void setTermsAndconditions(String termsAndconditions) {
        this.termsAndconditions = termsAndconditions;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Long getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Long invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Long getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(Long estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    public Long getEstimatedDeliveryTime() {
        return estimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(Long estimatedDeliveryTime) {
        this.estimatedDeliveryTime = estimatedDeliveryTime;
    }

    public BigDecimal getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(BigDecimal deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getWholeSaleCostOfCannabiesItems() {
        return wholeSaleCostOfCannabiesItems;
    }

    public void setWholeSaleCostOfCannabiesItems(BigDecimal wholeSaleCostOfCannabiesItems) {
        this.wholeSaleCostOfCannabiesItems = wholeSaleCostOfCannabiesItems;
    }

    public BigDecimal getMarkup() {
        return markup;
    }

    public void setMarkup(BigDecimal markup) {
        this.markup = markup;
    }

    public BigDecimal getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(BigDecimal exciseTax) {
        this.exciseTax = exciseTax;
    }

    public BigDecimal getTotalInvoiceAmount() {
        return totalInvoiceAmount;
    }

    public void setTotalInvoiceAmount(BigDecimal totalInvoiceAmount) {
        this.totalInvoiceAmount = totalInvoiceAmount;
    }

    public Invoice.InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(Invoice.InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public Invoice.PaymentStatus getInvoicePaymentStatus() {
        return invoicePaymentStatus;
    }

    public void setInvoicePaymentStatus(Invoice.PaymentStatus invoicePaymentStatus) {
        this.invoicePaymentStatus = invoicePaymentStatus;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isInventoryProcessed() {
        return inventoryProcessed;
    }

    public void setInventoryProcessed(boolean inventoryProcessed) {
        this.inventoryProcessed = inventoryProcessed;
    }

    public Long getInventoryProcessedDate() {
        return inventoryProcessedDate;
    }

    public void setInventoryProcessedDate(Long inventoryProcessedDate) {
        this.inventoryProcessedDate = inventoryProcessedDate;
    }

    public Vendor.ArmsLengthType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Vendor.ArmsLengthType transactionType) {
        this.transactionType = transactionType;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public BigDecimal getWholeSaleCostOfNonCannabisItems() {
        return wholeSaleCostOfNonCannabisItems;
    }

    public void setWholeSaleCostOfNonCannabisItems(BigDecimal wholeSaleCostOfNonCannabisItems) {
        this.wholeSaleCostOfNonCannabisItems = wholeSaleCostOfNonCannabisItems;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public String getCompanyContactId() {
        return companyContactId;
    }

    public void setCompanyContactId(String companyContactId) {
        this.companyContactId = companyContactId;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
