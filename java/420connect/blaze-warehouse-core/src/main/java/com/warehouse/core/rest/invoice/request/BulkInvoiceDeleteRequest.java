package com.warehouse.core.rest.invoice.request;

import java.util.ArrayList;
import java.util.List;

public class BulkInvoiceDeleteRequest {
    private List<String> invoiceIds = new ArrayList<>();

    public List<String> getInvoiceIds() {
        return invoiceIds;
    }

    public void setInvoiceIds(List<String> invoiceIds) {
        this.invoiceIds = invoiceIds;
    }
}
