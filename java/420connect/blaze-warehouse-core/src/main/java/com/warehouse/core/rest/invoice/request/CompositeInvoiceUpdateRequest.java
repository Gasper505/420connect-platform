package com.warehouse.core.rest.invoice.request;

import com.warehouse.core.domain.models.invoice.Invoice;

import java.util.List;

public class CompositeInvoiceUpdateRequest extends Invoice {
    List<PaymentsReceivedAddRequest> addPaymentRequest;
    List<ShippingManifestAddRequest> addShippingManifestRequest;

    public List<PaymentsReceivedAddRequest> getAddPaymentRequest() {
        return addPaymentRequest;
    }

    public void setAddPaymentRequest(List<PaymentsReceivedAddRequest> addPaymentRequest) {
        this.addPaymentRequest = addPaymentRequest;
    }

    public List<ShippingManifestAddRequest> getAddShippingManifestRequest() {
        return addShippingManifestRequest;
    }

    public void setAddShippingManifestRequest(List<ShippingManifestAddRequest> addShippingManifestRequest) {
        this.addShippingManifestRequest = addShippingManifestRequest;
    }
}

