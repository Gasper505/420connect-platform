package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceAttachmentRequest {
    private List<CompanyAsset> companyAsset;

    public List<CompanyAsset> getCompanyAsset() {
        return companyAsset;
    }

    public void setCompanyAsset(List<CompanyAsset> companyAsset) {
        this.companyAsset = companyAsset;
    }
}
