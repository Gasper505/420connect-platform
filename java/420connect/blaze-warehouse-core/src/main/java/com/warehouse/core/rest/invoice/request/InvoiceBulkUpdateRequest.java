package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.domain.models.invoice.Invoice;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceBulkUpdateRequest {
    public enum InvoiceUpdateOperationType {
        INVOICE_TERM_TYPE,
        DELETE_INVOICE,
        /*INVOICE_ENTITY,*/
        INVOICE_STATUS,
        PAYMENT_STATUS,
        STATUS,
        RELATED_ENTITY
    }

    private InvoiceUpdateOperationType operationType;
    private Invoice.InvoiceTerms invoiceTerms;
    /*private Invoice.EntityType entity;*/
    private Invoice.InvoiceStatus invoiceStatus;
    private Invoice.PaymentStatus invoicePaymentStatus;
    private boolean active = true;
    private boolean relatedEntity = true;

    private List<String> invoiceIds = new ArrayList<>();

    public InvoiceUpdateOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(InvoiceUpdateOperationType operationType) {
        this.operationType = operationType;
    }

    public Invoice.InvoiceTerms getInvoiceTerms() {
        return invoiceTerms;
    }

    public void setInvoiceTerms(Invoice.InvoiceTerms invoiceTerms) {
        this.invoiceTerms = invoiceTerms;
    }

    /*public Invoice.EntityType getEntity() {
        return entity;
    }

    public void setEntity(Invoice.EntityType entity) {
        this.entity = entity;
    }*/

    public Invoice.InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(Invoice.InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public Invoice.PaymentStatus getInvoicePaymentStatus() {
        return invoicePaymentStatus;
    }

    public void setInvoicePaymentStatus(Invoice.PaymentStatus invoicePaymentStatus) {
        this.invoicePaymentStatus = invoicePaymentStatus;
    }

    public List<String> getInvoiceIds() {
        return invoiceIds;
    }

    public void setInvoiceIds(List<String> invoiceIds) {
        this.invoiceIds = invoiceIds;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }
}
