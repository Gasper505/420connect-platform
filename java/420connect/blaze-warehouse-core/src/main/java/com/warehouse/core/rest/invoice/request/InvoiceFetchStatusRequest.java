package com.warehouse.core.rest.invoice.request;

public class InvoiceFetchStatusRequest {
    public enum FetchStatus {
        ALL,
        ACTIVE,
        INACTIVE
    }

    private FetchStatus fetchStatus = FetchStatus.ACTIVE;

    public FetchStatus getFetchStatus() {
        return fetchStatus;
    }

    public void setFetchStatus(FetchStatus fetchStatus) {
        this.fetchStatus = fetchStatus;
    }
}
