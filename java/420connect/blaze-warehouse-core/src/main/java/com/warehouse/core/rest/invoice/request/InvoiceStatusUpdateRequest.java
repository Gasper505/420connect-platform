package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.domain.models.invoice.Invoice;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceStatusUpdateRequest {
    private Invoice.InvoiceStatus invoiceupdateStatus;

    public Invoice.InvoiceStatus getInvoiceupdateStatus() {
        return invoiceupdateStatus;
    }

    public void setInvoiceupdateStatus(Invoice.InvoiceStatus invoiceupdateStatus) {
        this.invoiceupdateStatus = invoiceupdateStatus;
    }
}
