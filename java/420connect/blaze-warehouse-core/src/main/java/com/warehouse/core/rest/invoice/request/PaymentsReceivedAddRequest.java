package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentsReceivedAddRequest {
    private PaymentsReceived.PaymentType paymentType;
    private Long paidDate;
    private String invoiceId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amountPaid = new BigDecimal(0);
    private Note notes;

    private String referenceNo;

    public PaymentsReceived.PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentsReceived.PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Long getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Long paidDate) {
        this.paidDate = paidDate;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Note getNotes() {
        return notes;
    }

    public void setNotes(Note notes) {
        this.notes = notes;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
