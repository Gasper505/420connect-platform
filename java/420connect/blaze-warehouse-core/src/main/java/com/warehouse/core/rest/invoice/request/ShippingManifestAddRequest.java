package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.warehouse.core.domain.models.invoice.DistributorInformation;
import com.warehouse.core.domain.models.invoice.ReceiverInformation;
import com.warehouse.core.domain.models.invoice.ShipperInformation;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShippingManifestAddRequest {
    private String invoiceId;
    private String driverId;
    private String PONumber;
    private Long shippedDateTime;
    private Long estimatedArrival;
    @Deprecated
    private List<String> productIds = new ArrayList<>();
    private ShipperInformation shipperInformation;
    private ReceiverInformation receiverInformation;
    private CompanyAsset signaturePhoto;
    private Long signatureDateTime;

    private List<ProductMetrcInfo> productMetrcInfo = new ArrayList<>();

    private long deliveryTime;
    private long deliveryDate;
    private List<Note> notes;
    @Deprecated
    private String batchId;
    @Deprecated
    private String metrcLabel;
    private DistributorInformation distributorInformation;


    private String externalLicense;

    public void parse(ShippingManifest shippingManifest) {
        this.setInvoiceId(shippingManifest.getInvoiceId());
        this.setShipperInformation(shippingManifest.getShipperInformation());
        this.setReceiverInformation(shippingManifest.getReceiverInformation());
        this.setDriverId(shippingManifest.getDriverId());
        this.setShippedDateTime(shippingManifest.getShippedDateTime());
        this.setEstimatedArrival(shippingManifest.getEstimatedArrival());
        this.setProductIds(shippingManifest.getProductIds());
        this.setPONumber(shippingManifest.getPONumber());
        this.setProductMetrcInfo(shippingManifest.getProductMetrcInfo());
        this.setSignaturePhoto(shippingManifest.getSignaturePhoto());
        this.setSignatureDateTime(shippingManifest.getSignatureDateTime());
        this.setDeliveryTime(shippingManifest.getDeliveryTime());
        this.setDeliveryDate(shippingManifest.getDeliveryDate());
        this.setNotes(shippingManifest.getNotes());
        this.setBatchId(shippingManifest.getBatchId());
        this.setMetrcLabel(shippingManifest.getMetrcLabel());
        this.setExternalLicense(shippingManifest.getExternalLicense());

    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public ShipperInformation getShipperInformation() {
        return shipperInformation;
    }

    public void setShipperInformation(ShipperInformation shipperInformation) {
        this.shipperInformation = shipperInformation;
    }

    public ReceiverInformation getReceiverInformation() {
        return receiverInformation;
    }

    public void setReceiverInformation(ReceiverInformation receiverInformation) {
        this.receiverInformation = receiverInformation;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Long getShippedDateTime() {
        return shippedDateTime;
    }

    public void setShippedDateTime(Long shippedDateTime) {
        this.shippedDateTime = shippedDateTime;
    }

    public Long getEstimatedArrival() {
        return estimatedArrival;
    }

    public void setEstimatedArrival(Long estimatedArrival) {
        this.estimatedArrival = estimatedArrival;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public String getPONumber() {
        return PONumber;
    }

    public void setPONumber(String PONumber) {
        this.PONumber = PONumber;
    }

    public List<ProductMetrcInfo> getProductMetrcInfo() {
        return productMetrcInfo;
    }

    public void setProductMetrcInfo(List<ProductMetrcInfo> productMetrcInfo) {
        this.productMetrcInfo = productMetrcInfo;
    }

    public CompanyAsset getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(CompanyAsset signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public Long getSignatureDateTime() {
        return signatureDateTime;
    }

    public void setSignatureDateTime(Long signatureDateTime) {
        this.signatureDateTime = signatureDateTime;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getMetrcLabel() {
        return metrcLabel;
    }

    public void setMetrcLabel(String metrcLabel) {
        this.metrcLabel = metrcLabel;
    }

    public String getExternalLicense() {
        return externalLicense;
    }

    public void setExternalLicense(String externalLicense) {
        this.externalLicense = externalLicense;
    }

    public DistributorInformation getDistributorInformation() {
        return distributorInformation;
    }

    public void setDistributorInformation(DistributorInformation distributorInformation) {
        this.distributorInformation = distributorInformation;
    }
}
