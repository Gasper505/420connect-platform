package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.warehouse.core.domain.models.invoice.ShippingManifest;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ShippingManifestStatusRequest {
    private String declineReason;

    private ShippingManifest.ShippingManifestStatus status;
    private CompanyAsset managerSignature;

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public ShippingManifest.ShippingManifestStatus getStatus() {
        return status;
    }

    public void setStatus(ShippingManifest.ShippingManifestStatus status) {
        this.status = status;
    }

    public CompanyAsset getManagerSignature() {
        return managerSignature;
    }

    public void setManagerSignature(CompanyAsset managerSignature) {
        this.managerSignature = managerSignature;
    }
}
