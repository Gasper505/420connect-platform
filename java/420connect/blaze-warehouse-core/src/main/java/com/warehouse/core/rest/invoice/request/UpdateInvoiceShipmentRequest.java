package com.warehouse.core.rest.invoice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateInvoiceShipmentRequest {
    private List<ProductMetrcInfo> productMetrcInfos = new ArrayList<>();
    private String transferType;

    public List<ProductMetrcInfo> getProductMetrcInfos() {
        return productMetrcInfos;
    }

    public void setProductMetrcInfos(List<ProductMetrcInfo> productMetrcInfos) {
        this.productMetrcInfos = productMetrcInfos;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }
}
