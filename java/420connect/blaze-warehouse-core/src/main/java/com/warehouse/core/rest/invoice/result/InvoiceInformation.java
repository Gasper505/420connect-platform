package com.warehouse.core.rest.invoice.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;
import com.warehouse.core.domain.models.invoice.InvoiceHistory;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceInformation extends Invoice {
    private List<InvoiceHistory> invoiceHistoryList;
    private List<InvoiceActivityLog> invoiceActivityLogs;
    private BigDecimal balanceDue = new BigDecimal(0);
    private Vendor vendor;
    private CompanyContact companyContact;
    private List<ShippingManifestResult> shippingManifests = new ArrayList<>();
    private List<RemainingProductInformation> remainingProductInformations = new ArrayList<>();
    private String companyLogo;
    private List<PaymentsReceived> paymentsReceived;
    private String salesPerson;
    private BigDecimal amountPaid = new BigDecimal(0);
    private List<ProductRequestResult> items;

    public List<InvoiceHistory> getInvoiceHistoryList() {
        return invoiceHistoryList;
    }

    public void setInvoiceHistoryList(List<InvoiceHistory> invoiceHistoryList) {
        this.invoiceHistoryList = invoiceHistoryList;
    }

    public List<InvoiceActivityLog> getInvoiceActivityLogs() {
        return invoiceActivityLogs;
    }

    public void setInvoiceActivityLogs(List<InvoiceActivityLog> invoiceActivityLogs) {
        this.invoiceActivityLogs = invoiceActivityLogs;
    }

    public BigDecimal getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public CompanyContact getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(CompanyContact companyContact) {
        this.companyContact = companyContact;
    }

    public List<ShippingManifestResult> getShippingManifests() {
        return shippingManifests;
    }

    public void setShippingManifests(List<ShippingManifestResult> shippingManifests) {
        this.shippingManifests = shippingManifests;
    }

    public List<RemainingProductInformation> getRemainingProductInformations() {
        return remainingProductInformations;
    }

    public void setRemainingProductInformations(List<RemainingProductInformation> remainingProductInformations) {
        this.remainingProductInformations = remainingProductInformations;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public List<PaymentsReceived> getPaymentsReceived() {
        return paymentsReceived;
    }

    public void setPaymentsReceived(List<PaymentsReceived> paymentsReceived) {
        this.paymentsReceived = paymentsReceived;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public List<ProductRequestResult> getItems() {
        return items;
    }

    public void setItems(List<ProductRequestResult> items) {
        this.items = items;
    }
}
