package com.warehouse.core.rest.invoice.result;

import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;

public class InvoicePaymentResult extends PaymentsReceived {
    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
