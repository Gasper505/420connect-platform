package com.warehouse.core.rest.invoice.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;
import com.warehouse.core.domain.models.invoice.InvoiceHistory;
import org.assertj.core.util.Lists;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceResult extends Invoice {
    private String employeeName;
    private String customerName;
    @Deprecated
    private List<ProductRequestResult> productRequestResultList;
    private List<InvoiceHistory> invoiceHistoryList;
    private List<InvoiceActivityLog> invoiceActivityLogs;
    @Deprecated
    private BigDecimal balanceDue = new BigDecimal(0);

    private Vendor vendor;
    private CompanyContact companyContact;

    private List<ShippingManifestResult> shippingManifests = new ArrayList<>();
    private HashMap<String, BigDecimal> remainingProductMap = new HashMap<>();

    private String companyLogo;
    private String overDueDays;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public List<ProductRequestResult> getProductRequestResultList() {
        return productRequestResultList;
    }

    public void setProductRequestResultList(List<ProductRequestResult> productRequestResultList) {
        this.productRequestResultList = productRequestResultList;
    }

    public List<InvoiceHistory> getInvoiceHistoryList() {
        return invoiceHistoryList;
    }

    public void setInvoiceHistoryList(List<InvoiceHistory> invoiceHistoryList) {
        this.invoiceHistoryList = invoiceHistoryList;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<InvoiceActivityLog> getInvoiceActivityLogs() {
        return invoiceActivityLogs;
    }

    public void setInvoiceActivityLogs(List<InvoiceActivityLog> invoiceActivityLogs) {
        this.invoiceActivityLogs = invoiceActivityLogs;
    }

    public BigDecimal getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public List<ShippingManifestResult> getShippingManifests() {
        return shippingManifests;
    }

    public void setShippingManifests(List<ShippingManifestResult> shippingManifests) {
        this.shippingManifests = shippingManifests;
    }

    public HashMap<String, BigDecimal> getRemainingProductMap() {
        return remainingProductMap;
    }

    public void setRemainingProductMap(HashMap<String, BigDecimal> remainingProductMap) {
        this.remainingProductMap = remainingProductMap;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public CompanyContact getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(CompanyContact companyContact) {
        this.companyContact = companyContact;
    }

    public String getOverDueDays() {
        return overDueDays;
    }

    public void setOverDueDays(String overDueDays) {
        this.overDueDays = overDueDays;
    }

    @JsonIgnore
    public static int compare(InvoiceResult o1, InvoiceResult o2) {
        List<ShippingManifestResult> l1 = Lists.newArrayList(o1.getShippingManifests());
        List<ShippingManifestResult> l2 = Lists.newArrayList(o2.getShippingManifests());
        l1.sort(Comparator.comparing(ShippingManifestResult::getDeliveryDate));
        l2.sort(Comparator.comparing(ShippingManifestResult::getDeliveryDate));


        long d1 = (l1 != null && !l1.isEmpty()) ? l1.stream().min(Comparator.comparing(ShippingManifestResult::getDeliveryDate)).get().getDeliveryDate() : 0;
        long d2 = (l2 != null && !l2.isEmpty()) ? l2.stream().min(Comparator.comparing(ShippingManifestResult::getDeliveryDate)).get().getDeliveryDate() : 0;
        if (d1 == 0) {
            return 1;
        }
        if (d2 == 0) {
            return -1;
        }

        if (d1 < d2) {
            return -1;
        } else if (d1 > d2) {
            return 0;

        }
        return 0;
    }

}
