package com.warehouse.core.rest.invoice.result;

import com.fourtwenty.core.domain.models.product.ProductCategory;

import java.math.BigDecimal;

public class ManifestProductResult {
    private String productName;
    private String brandName;
    private String category;
    private String type;
    private BigDecimal rate = new BigDecimal(0);
    private BigDecimal quantity = new BigDecimal(0);
    private BigDecimal exciseTax = new BigDecimal(0);
    private BigDecimal amount = new BigDecimal(0);
    private ProductCategory.UnitType unitType;
    private String prepackageId;
    private String productId;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(BigDecimal exciseTax) {
        this.exciseTax = exciseTax;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public ProductCategory.UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(ProductCategory.UnitType unitType) {
        this.unitType = unitType;
    }

    public String getPrepackageId() {
        return prepackageId;
    }

    public void setPrepackageId(String prepackageId) {
        this.prepackageId = prepackageId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
