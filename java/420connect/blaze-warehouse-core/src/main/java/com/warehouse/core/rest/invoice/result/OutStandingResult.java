package com.warehouse.core.rest.invoice.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OutStandingResult {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalOutStanding = new BigDecimal(0);

    public BigDecimal getTotalOutStanding() {
        return totalOutStanding;
    }

    public void setTotalOutStanding(BigDecimal totalOutStanding) {
        this.totalOutStanding = totalOutStanding;
    }
}
