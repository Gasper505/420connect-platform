package com.warehouse.core.rest.invoice.result;

import com.fourtwenty.core.domain.models.transaction.OrderItem;

public class ProductRequestResult extends OrderItem {
    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
