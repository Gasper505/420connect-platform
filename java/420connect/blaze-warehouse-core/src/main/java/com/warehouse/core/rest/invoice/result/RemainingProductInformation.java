package com.warehouse.core.rest.invoice.result;

import java.math.BigDecimal;

public class RemainingProductInformation {
    String productId;
    String productName;
    BigDecimal remainingQuantity = new BigDecimal(0);
    BigDecimal requestQuantity = new BigDecimal(0);

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(BigDecimal remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public BigDecimal getRequestQuantity() {
        return requestQuantity;
    }

    public void setRequestQuantity(BigDecimal requestQuantity) {
        this.requestQuantity = requestQuantity;
    }
}
