package com.warehouse.core.rest.invoice.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShippingBatchDetails {
    public enum MetrcStatus {
        Created,
        Error,
        NotCreated
    }
    private String batchId;
    private String batchSku;
    private String metrcLabel;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantity = new BigDecimal(0);
    private String prepackageItemId;
    private String trackHarvestBatchId;
    private long batchPurchaseDate;
    private String overrideInventoryId;
    private String reqMetrcLabel;
    private MetrcStatus metrcStatus = MetrcStatus.NotCreated;
    private String errorMessage;
    private List<MetrcPackageDetails> metrcPackageDetails = new ArrayList<>();

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }

    public String getMetrcLabel() {
        return metrcLabel;
    }

    public void setMetrcLabel(String metrcLabel) {
        this.metrcLabel = metrcLabel;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getTrackHarvestBatchId() {
        return trackHarvestBatchId;
    }

    public void setTrackHarvestBatchId(String trackHarvestBatchId) {
        this.trackHarvestBatchId = trackHarvestBatchId;
    }

    public long getBatchPurchaseDate() {
        return batchPurchaseDate;
    }

    public void setBatchPurchaseDate(long batchPurchaseDate) {
        this.batchPurchaseDate = batchPurchaseDate;
    }

    public String getOverrideInventoryId() {
        return overrideInventoryId;
    }

    public void setOverrideInventoryId(String overrideInventoryId) {
        this.overrideInventoryId = overrideInventoryId;
    }

    public String getReqMetrcLabel() {
        return reqMetrcLabel;
    }

    public void setReqMetrcLabel(String reqMetrcLabel) {
        this.reqMetrcLabel = reqMetrcLabel;
    }

    public MetrcStatus getMetrcStatus() {
        return metrcStatus;
    }

    public void setMetrcStatus(MetrcStatus metrcStatus) {
        this.metrcStatus = metrcStatus;
    }

    public List<MetrcPackageDetails> getMetrcPackageDetails() {
        return metrcPackageDetails;
    }

    public void setMetrcPackageDetails(List<MetrcPackageDetails> metrcPackageDetails) {
        this.metrcPackageDetails = metrcPackageDetails;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static class MetrcPackageDetails {
        private String batchId;
        private String reqMetrcLabel;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal quantity = new BigDecimal(0);
        private MetrcStatus metrcStatus = MetrcStatus.NotCreated;
        private transient String productName;
        private transient String productId;
        private transient String batchSku;
        private transient String metrcLabel;
        private String errorMessage;

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public String getReqMetrcLabel() {
            return reqMetrcLabel;
        }

        public void setReqMetrcLabel(String reqMetrcLabel) {
            this.reqMetrcLabel = reqMetrcLabel;
        }

        public MetrcStatus getMetrcStatus() {
            return metrcStatus;
        }

        public void setMetrcStatus(MetrcStatus metrcStatus) {
            this.metrcStatus = metrcStatus;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void setQuantity(BigDecimal quantity) {
            this.quantity = quantity;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getBatchSku() {
            return batchSku;
        }

        public void setBatchSku(String batchSku) {
            this.batchSku = batchSku;
        }

        public String getMetrcLabel() {
            return metrcLabel;
        }

        public void setMetrcLabel(String metrcLabel) {
            this.metrcLabel = metrcLabel;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }
    }
}
