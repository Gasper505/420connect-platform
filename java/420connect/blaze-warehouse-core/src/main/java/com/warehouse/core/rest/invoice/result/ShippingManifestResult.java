package com.warehouse.core.rest.invoice.result;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.ShopLimitedView;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.warehouse.core.domain.models.invoice.ShippingManifest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShippingManifestResult extends ShippingManifest {
    private transient Vendor shipperCustomerCompany;
    private transient CompanyContact shipperCompanyContact;
    private transient Vendor receiverCustomerCompany;
    private transient CompanyContact receiverCompanyContact;
    private transient ShopLimitedView shipperCompany;
    private transient String invoiceNumber;
    private transient String driverName;
    private transient String driverLicenceNumber;
    private transient String carNumber;
    private transient String vehicleMake;
    private transient String vehicleModel;
    private transient String vehicleLicensePlate;
    private transient String driverVinNo;
    private transient Employee shipperEmployee;
    private transient ShopLimitedView distributorShop;
    private transient String driverPhone;
    private transient String driverEmail;

    @Deprecated
    private transient List<ManifestProductResult> productList = new ArrayList<>();

    private transient HashMap<String, BigDecimal> remainingProductMap = new HashMap<>();

    private transient Vendor distributorCustomerCompany;
    private transient CompanyContact distributorCompanyContact;

    public Vendor getShipperCustomerCompany() {
        return shipperCustomerCompany;
    }

    public void setShipperCustomerCompany(Vendor shipperCustomerCompany) {
        this.shipperCustomerCompany = shipperCustomerCompany;
    }

    public CompanyContact getShipperCompanyContact() {
        return shipperCompanyContact;
    }

    public void setShipperCompanyContact(CompanyContact shipperCompanyContact) {
        this.shipperCompanyContact = shipperCompanyContact;
    }

    public Vendor getReceiverCustomerCompany() {
        return receiverCustomerCompany;
    }

    public void setReceiverCustomerCompany(Vendor receiverCustomerCompany) {
        this.receiverCustomerCompany = receiverCustomerCompany;
    }

    public CompanyContact getReceiverCompanyContact() {
        return receiverCompanyContact;
    }

    public void setReceiverCompanyContact(CompanyContact receiverCompanyContact) {
        this.receiverCompanyContact = receiverCompanyContact;
    }

    public ShopLimitedView getShipperCompany() {
        return shipperCompany;
    }

    public void setShipperCompany(ShopLimitedView shipperCompany) {
        this.shipperCompany = shipperCompany;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenceNumber() {
        return driverLicenceNumber;
    }

    public void setDriverLicenceNumber(String driverLicenceNumber) {
        this.driverLicenceNumber = driverLicenceNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public List<ManifestProductResult> getProductList() {
        return productList;
    }

    public void setProductList(List<ManifestProductResult> productList) {
        this.productList = productList;
    }

    public HashMap<String, BigDecimal> getRemainingProductMap() {
        return remainingProductMap;
    }

    public void setRemainingProductMap(HashMap<String, BigDecimal> remainingProductMap) {
        this.remainingProductMap = remainingProductMap;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlate() {
        return vehicleLicensePlate;
    }

    public void setVehicleLicensePlate(String vehicleLicensePlate) {
        this.vehicleLicensePlate = vehicleLicensePlate;
    }

    public String getDriverVinNo() {
        return driverVinNo;
    }

    public void setDriverVinNo(String driverVinNo) {
        this.driverVinNo = driverVinNo;
    }

    public Employee getShipperEmployee() {
        return shipperEmployee;
    }

    public void setShipperEmployee(Employee shipperEmployee) {
        this.shipperEmployee = shipperEmployee;
    }

    public Vendor getDistributorCustomerCompany() {
        return distributorCustomerCompany;
    }

    public void setDistributorCustomerCompany(Vendor distributorCustomerCompany) {
        this.distributorCustomerCompany = distributorCustomerCompany;
    }

    public CompanyContact getDistributorCompanyContact() {
        return distributorCompanyContact;
    }

    public void setDistributorCompanyContact(CompanyContact distributorCompanyContact) {
        this.distributorCompanyContact = distributorCompanyContact;
    }

    public ShopLimitedView getDistributorShop() {
        return distributorShop;
    }

    public void setDistributorShop(ShopLimitedView distributorShop) {
        this.distributorShop = distributorShop;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getDriverEmail() {
        return driverEmail;
    }

    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }
}
