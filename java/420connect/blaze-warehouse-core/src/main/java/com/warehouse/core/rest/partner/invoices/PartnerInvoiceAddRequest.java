package com.warehouse.core.rest.partner.invoices;

import com.warehouse.core.rest.invoice.request.AddInvoiceRequest;
import org.hibernate.validator.constraints.NotEmpty;

public class PartnerInvoiceAddRequest extends AddInvoiceRequest {
    @NotEmpty
    private String currentEmployeeId;

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }
}
