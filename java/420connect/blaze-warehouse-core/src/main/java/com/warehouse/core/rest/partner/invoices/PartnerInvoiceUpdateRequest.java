package com.warehouse.core.rest.partner.invoices;

import com.warehouse.core.domain.models.invoice.Invoice;
import org.hibernate.validator.constraints.NotEmpty;

public class PartnerInvoiceUpdateRequest extends Invoice {
    @NotEmpty
    private String currentEmployeeId;

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }
}
