package com.warehouse.core.rest.transfer.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Destination {
    private String transferDestinationId;
    private String recipientLicenseNumber;
    private String transferTypeName;
    private String plannedRoute;
    private long estimatedDepartureDateTime;
    private long estimatedArrivalDateTime;
    private List<Transporter> transporters = new ArrayList<>();
    //This id list is used to get list of metrc verified packages.
    private List<TransferTemplatePackages> packages = new ArrayList<>();

    public String getTransferDestinationId() {
        return transferDestinationId;
    }

    public void setTransferDestinationId(String transferDestinationId) {
        this.transferDestinationId = transferDestinationId;
    }

    public String getRecipientLicenseNumber() {
        return recipientLicenseNumber;
    }

    public void setRecipientLicenseNumber(String recipientLicenseNumber) {
        this.recipientLicenseNumber = recipientLicenseNumber;
    }

    public String getTransferTypeName() {
        return transferTypeName;
    }

    public void setTransferTypeName(String transferTypeName) {
        this.transferTypeName = transferTypeName;
    }

    public String getPlannedRoute() {
        return plannedRoute;
    }

    public void setPlannedRoute(String plannedRoute) {
        this.plannedRoute = plannedRoute;
    }

    public long getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(long estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public long getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(long estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public List<Transporter> getTransporters() {
        return transporters;
    }

    public void setTransporters(List<Transporter> transporters) {
        this.transporters = transporters;
    }

    public List<TransferTemplatePackages> getPackages() {
        return packages;
    }

    public void setPackages(List<TransferTemplatePackages> packages) {
        this.packages = packages;
    }
}
