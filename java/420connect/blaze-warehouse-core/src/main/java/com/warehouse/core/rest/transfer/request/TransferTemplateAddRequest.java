package com.warehouse.core.rest.transfer.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTemplateAddRequest {
    @NotEmpty
    private String invoiceId;
    private String templateManifestNumber;
    private String templateShipperLicenseNumber;
    private String templateName;
    private String employeeId;
    private Boolean templateStatus = true;
    private List<Destination> destinations = new ArrayList<>();

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getTemplateManifestNumber() {
        return templateManifestNumber;
    }

    public void setTemplateManifestNumber(String templateManifestNumber) {
        this.templateManifestNumber = templateManifestNumber;
    }

    public String getTemplateShipperLicenseNumber() {
        return templateShipperLicenseNumber;
    }

    public void setTemplateShipperLicenseNumber(String templateShipperLicenseNumber) {
        this.templateShipperLicenseNumber = templateShipperLicenseNumber;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Boolean getTemplateStatus() {
        return templateStatus;
    }

    public void setTemplateStatus(Boolean templateStatus) {
        this.templateStatus = templateStatus;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
}
