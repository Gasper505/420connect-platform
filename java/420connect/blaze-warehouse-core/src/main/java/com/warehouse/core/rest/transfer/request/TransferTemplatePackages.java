package com.warehouse.core.rest.transfer.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTemplatePackages {
    private String packageLabel;
    private String itemName;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantity = new BigDecimal(0);
    private String unitOfMeasureName;
    private long packageDate;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal grossWeight = new BigDecimal(0);
    private String grossUnitOfWeightName;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal wholesalePrice = new BigDecimal(0);

    public String getPackageLabel() {
        return packageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        this.packageLabel = packageLabel;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        this.unitOfMeasureName = unitOfMeasureName;
    }

    public long getPackageDate() {
        return packageDate;
    }

    public void setPackageDate(long packageDate) {
        this.packageDate = packageDate;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getGrossUnitOfWeightName() {
        return grossUnitOfWeightName;
    }

    public void setGrossUnitOfWeightName(String grossUnitOfWeightName) {
        this.grossUnitOfWeightName = grossUnitOfWeightName;
    }

    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }
}
