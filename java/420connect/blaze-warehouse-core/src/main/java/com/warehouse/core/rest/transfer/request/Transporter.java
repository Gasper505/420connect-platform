package com.warehouse.core.rest.transfer.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transporter {
    private String transporterFacilityLicenseNumber;
    private String driverOccupationalLicenseNumber;
    private String phoneNumberForQuestions;
    private boolean isLayover = false;
    private String transporterEmployeeId;
    private String driverName;
    private String driverLicenseNumber;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleLicensePlateNumber;
    private long estimatedDepartureDateTime;
    private long estimatedArrivalDateTime;

    public String getTransporterFacilityLicenseNumber() {
        return transporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        this.transporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getDriverOccupationalLicenseNumber() {
        return driverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        this.driverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getPhoneNumberForQuestions() {
        return phoneNumberForQuestions;
    }

    public void setPhoneNumberForQuestions(String phoneNumberForQuestions) {
        this.phoneNumberForQuestions = phoneNumberForQuestions;
    }

    public boolean isLayover() {
        return isLayover;
    }

    public void setLayover(boolean layover) {
        isLayover = layover;
    }

    public String getTransporterEmployeeId() {
        return transporterEmployeeId;
    }

    public void setTransporterEmployeeId(String transporterEmployeeId) {
        this.transporterEmployeeId = transporterEmployeeId;
    }

    public long getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(long estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public long getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(long estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return vehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        this.vehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }
}
