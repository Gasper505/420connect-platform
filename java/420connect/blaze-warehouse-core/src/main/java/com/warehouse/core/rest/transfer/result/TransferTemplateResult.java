package com.warehouse.core.rest.transfer.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.transfer.TransferTemplate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTemplateResult extends TransferTemplate {

    private String driverName;
    private String driverLicenseNumber;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleLicensePlateNumber;
    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return vehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        this.vehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }
}
