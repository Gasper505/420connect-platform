package com.warehouse.core.result;

import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;

import java.util.ArrayList;
import java.util.List;

public class CompositeErrorResult {
    private List<BulkPostResponse> inventoryTransferError = new ArrayList<>();
    private List<BulkPostResponse> invoiceError = new ArrayList<>();
    private List<BulkPostResponse> poError = new ArrayList<>();

    public List<BulkPostResponse> getInventoryTransferError() {
        return inventoryTransferError;
    }

    public void setInventoryTransferError(List<BulkPostResponse> inventoryTransferError) {
        this.inventoryTransferError = inventoryTransferError;
    }

    public List<BulkPostResponse> getInvoiceError() {
        return invoiceError;
    }

    public void setInvoiceError(List<BulkPostResponse> invoiceError) {
        this.invoiceError = invoiceError;
    }

    public List<BulkPostResponse> getPoError() {
        return poError;
    }

    public void setPoError(List<BulkPostResponse> poError) {
        this.poError = poError;
    }
}
