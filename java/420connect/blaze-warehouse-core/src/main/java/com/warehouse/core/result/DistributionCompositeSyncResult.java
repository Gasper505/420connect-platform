package com.warehouse.core.result;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryShopBaseResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderResult;
import com.warehouse.core.rest.invoice.result.InvoiceInformation;

import java.util.List;

public class DistributionCompositeSyncResult {

    private DateSearchResult<InvoiceInformation> invoice;
    private DateSearchResult<PurchaseOrderResult> purchaseOrder;
    private DateSearchResult<InventoryTransferHistoryResult> inventoryTransfers;
    private DateSearchResult<Product> product;
    private List<InventoryShopBaseResult> inventories;
    private DateSearchResult<ProductBatch> productBatch;
    private SearchResult<EmployeeResult> employees;

    public DateSearchResult<InvoiceInformation> getInvoice() {
        return invoice;
    }

    public void setInvoice(DateSearchResult<InvoiceInformation> invoice) {
        this.invoice = invoice;
    }

    public DateSearchResult<PurchaseOrderResult> getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(DateSearchResult<PurchaseOrderResult> purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public DateSearchResult<Product> getProduct() {
        return product;
    }

    public void setProduct(DateSearchResult<Product> product) {
        this.product = product;
    }

    public List<InventoryShopBaseResult> getInventories() {
        return inventories;
    }

    public void setInventories(List<InventoryShopBaseResult> inventories) {
        this.inventories = inventories;
    }

    public DateSearchResult<InventoryTransferHistoryResult> getInventoryTransfers() {
        return inventoryTransfers;
    }

    public void setInventoryTransfers(DateSearchResult<InventoryTransferHistoryResult> inventoryTransfers) {
        this.inventoryTransfers = inventoryTransfers;
    }

    public DateSearchResult<ProductBatch> getProductBatch() {
        return productBatch;
    }

    public void setProductBatch(DateSearchResult<ProductBatch> productBatch) {
        this.productBatch = productBatch;
    }

    public SearchResult<EmployeeResult> getEmployees() {
        return employees;
    }

    public void setEmployees(SearchResult<EmployeeResult> employees) {
        this.employees = employees;
    }
}
