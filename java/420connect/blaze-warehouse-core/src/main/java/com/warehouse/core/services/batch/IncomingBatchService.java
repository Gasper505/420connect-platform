package com.warehouse.core.services.batch;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.batch.IncomingBatch;
import com.warehouse.core.rest.batch.request.BulkBatchUpdateRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchAddRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchScannedRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchStatusRequest;
import com.warehouse.core.rest.batch.result.IncomingBatchResult;

public interface IncomingBatchService {
    IncomingBatch addInComingBatch(final IncomingBatchAddRequest request);

    IncomingBatchResult getIncomingBatch(final String incomingBatchId);

    SearchResult<IncomingBatchResult> getAllIncomingBatches(final int start, final int limit, final String term);

    SearchResult<IncomingBatchResult> getAllIncomingBatchesByState(final int start, final int limit, final boolean state);

    SearchResult<IncomingBatchResult> getAllIncomingBatchesByStatus(final int start, final int limit, final IncomingBatch.BatchStatus status);

    IncomingBatch archiveIncomingBatch(final String incomingBatchId);

    SearchResult<IncomingBatchResult> getAllArchivedIncomingBatches(final int start, final int limit);

    IncomingBatch updateIncomingBatchStatus(final String incomingBatchId, final IncomingBatchStatusRequest request);

    IncomingBatch updateIncomingBatch(final String incomingBatchId, final IncomingBatch incomingBatch);

    void deleteIncomingBatch(final String incomingBatchId);

    SearchResult<IncomingBatchResult> getAllScannedIncomingBatch(final IncomingBatchScannedRequest request, final int start, final int limit);

    SearchResult<IncomingBatchResult> getAllDeletedIncomingBatch(final int start, final int limit);

    void bulkIncomingBatchUpdate(final BulkBatchUpdateRequest request);

    IncomingBatch updateIncomingBatchVoidStatus(final String incomingBatchId, final boolean voidStatus);

    SearchResult<IncomingBatchResult> getAllIncomingBatchesByVoidStatus(final int start, final int limit, final boolean voidStatus);

}
