package com.warehouse.core.services.batch;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleAttachmentRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleStatusRequest;

import java.util.List;

public interface TestSampleService {
    TestSample addTestSample(TestSampleAddRequest testSampleAddRequest);

    TestSample updateTestSample(final String testSampleId, final TestSample testSample);

    TestSample updateTestSampleStatus(final String testSampleId, final TestSampleStatusRequest request);

    SearchResult<TestSample> getAllTestSamplesByBatchId(final String incomingBatchId, final int start, final int limit);

    void deleteTestSampleById(final String testSampleId);

    TestSample addAttachments(final TestSampleAttachmentRequest request);

    List<CompanyAsset> getAllTestSampleAttachment(final String testSampleId);

    CompanyAsset getTestSampleAttachmentById(final String testSampleId, final String attachmentId);

    void deleteTestSampleAttachmentById(final String testSampleId, final String attachmentId);
}
