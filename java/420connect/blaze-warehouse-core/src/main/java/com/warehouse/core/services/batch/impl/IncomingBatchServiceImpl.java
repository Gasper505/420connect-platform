package com.warehouse.core.services.batch.impl;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.batch.IncomingBatch;
import com.warehouse.core.domain.repositories.batch.IncomingBatchRepository;
import com.warehouse.core.rest.batch.request.BulkBatchUpdateRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchAddRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchScannedRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchStatusRequest;
import com.warehouse.core.rest.batch.result.IncomingBatchResult;
import com.warehouse.core.services.batch.IncomingBatchService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class IncomingBatchServiceImpl extends AbstractAuthServiceImpl implements IncomingBatchService {


    private static final Logger LOGGER = LoggerFactory.getLogger(IncomingBatchServiceImpl.class);
    private static final String INCOMING_BATCH_NOT_FOUND = "No data found for this Incoming Batch";
    private static final String INCOMING_BATCH = "Incoming Batch";
    private static final String EMPTY_LIST = "Empty list";
    private static final String BULK_BATCH_UPDATE = "Bulk Batch Update";
    private IncomingBatchRepository incomingBatchRepository;
    private BrandRepository brandRepository;
    private ProductRepository productRepository;
    private VendorRepository vendorRepository;
    private BarcodeService barcodeService;
    private InventoryService inventoryService;

    @Inject
    public IncomingBatchServiceImpl(Provider<ConnectAuthToken> token,
                                    IncomingBatchRepository incomingBatchRepository,
                                    BrandRepository brandRepository,
                                    ProductRepository productRepository,
                                    VendorRepository vendorRepository,
                                    BarcodeService barcodeService,
                                    InventoryService inventoryService) {
        super(token);
        this.incomingBatchRepository = incomingBatchRepository;
        this.brandRepository = brandRepository;
        this.productRepository = productRepository;
        this.vendorRepository = vendorRepository;
        this.barcodeService = barcodeService;
        this.inventoryService = inventoryService;
    }

    /**
     * This method is used to create Incoming Batch.
     *
     * @param request
     * @return
     */
    @Override
    public IncomingBatch addInComingBatch(IncomingBatchAddRequest request) {
        if (StringUtils.isBlank(request.getMetrcTagId())) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Metrc Tag can't be empty.");
        }
        if (StringUtils.isBlank(request.getProductId())) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Product can't be empty.");
        }
        if (StringUtils.isBlank(request.getVendorId())) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Vendor can't be empty.");
        }
        if (StringUtils.isBlank(request.getBrandId())) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Brand can't be empty.");
        }
        if (StringUtils.isBlank(request.getPoNumber())) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Purchase Order can't be empty.");
        }
        IncomingBatch incomingBatch = new IncomingBatch();
        incomingBatch.prepare(token.getCompanyId());
        incomingBatch.setShopId(token.getShopId());
        incomingBatch.setMetrcTagId(request.getMetrcTagId());
        incomingBatch.setProductId(request.getProductId());
        incomingBatch.setVendorId(request.getVendorId());
        incomingBatch.setBrandId(request.getBrandId());
        incomingBatch.setReceiveDate(request.getReceiveDate());
        incomingBatch.setPoNumber(request.getPoNumber());
        incomingBatch.setStatus(request.getStatus());
        incomingBatch.setExternalBatchId(request.getExternalBatchId());
        incomingBatch.setMetrcCategory(request.getMetrcCategory());
        incomingBatch.setQuantityReceived(request.getQuantityReceived());
        incomingBatch.setCostPerUnit(request.getCostPerUnit());
        if (request.getQuantityReceived().compareTo(BigDecimal.ZERO) > 0 && request.getCostPerUnit().compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal purchasePrice = request.getQuantityReceived().multiply(request.getCostPerUnit());
            incomingBatch.setBatchPurchasePrice(purchasePrice);
        } else {
            incomingBatch.setBatchPurchasePrice(request.getBatchPurchasePrice());
        }
        incomingBatch.setBatchCultivationTax(request.getBatchCultivationTax());
        incomingBatch.setThc(request.getThc());
        incomingBatch.setCbd(request.getCbd());
        incomingBatch.setCbn(request.getCbn());
        incomingBatch.setThca(request.getThca());
        incomingBatch.setCbda(request.getCbda());
        incomingBatch.setActive(request.isActive());
        incomingBatch.setArchive(false);
        incomingBatch.setProductBatchId(request.getProductBatchId());
        incomingBatch.setVoidStatus(request.isVoidStatus());
        return incomingBatchRepository.save(incomingBatch);


    }

    /**
     * This method is used to get incoming batch with incomingBatchId.
     *
     * @param incomingBatchId
     * @return
     */
    @Override
    public IncomingBatchResult getIncomingBatch(String incomingBatchId) {
        IncomingBatchResult incomingBatchResult = incomingBatchRepository.get(token.getCompanyId(), incomingBatchId, IncomingBatchResult.class);
        if (incomingBatchResult == null) {
            LOGGER.debug("Exception in getIncomingBatch", INCOMING_BATCH_NOT_FOUND);
            throw new BlazeInvalidArgException(INCOMING_BATCH, INCOMING_BATCH_NOT_FOUND);
        }
        prepareIncomingBatchResult(incomingBatchResult);
        return incomingBatchResult;
    }

    /**
     * This private method is used to prepare incoming batch for result.
     *
     * @param incomingBatchResult
     */
    private void prepareIncomingBatchResult(IncomingBatchResult incomingBatchResult) {
        try {
            Brand brand;
            Vendor vendor;
            Product product;
            if (!StringUtils.isBlank(incomingBatchResult.getBrandId())) {
                brand = brandRepository.get(token.getCompanyId(), incomingBatchResult.getBrandId());
                if (brand != null) {
                    incomingBatchResult.setBrandName(brand.getName());
                }
            }
            if (!StringUtils.isBlank(incomingBatchResult.getProductId())) {
                product = productRepository.get(token.getCompanyId(), incomingBatchResult.getProductId());
                if (product != null) {
                    incomingBatchResult.setProductName(product.getName());
                    incomingBatchResult.setProductType(product.getProductSaleType().toString());
                }
            }
            if (!StringUtils.isBlank(incomingBatchResult.getVendorId())) {
                vendor = vendorRepository.get(token.getCompanyId(), incomingBatchResult.getVendorId());
                if (vendor != null) {
                    incomingBatchResult.setVendorName(vendor.getFirstName() + " " + vendor.getLastName());
                }
            }
        } catch (Exception ex) {

        }
    }

    /**
     * This method is used to get all incoming batches of shop.
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatches(int start, int limit, String term) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<IncomingBatchResult> searchResult = incomingBatchRepository.getAllIncomingBatches(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);

        if (searchResult.getValues().size() > 0) {
            for (IncomingBatchResult batchResult : searchResult.getValues()) {
                prepareIncomingBatchResult(batchResult);
            }
        }

        return searchResult;
    }

    /**
     * This method is used to archived incoming batch.
     *
     * @param incomingBatchId
     * @return
     */
    @Override
    public IncomingBatch archiveIncomingBatch(String incomingBatchId) {
        if (StringUtils.isBlank(incomingBatchId)) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "incomingBatchId can't be blank");
        }
        IncomingBatch dbIncomingBatch = incomingBatchRepository.get(token.getCompanyId(), incomingBatchId);
        if (dbIncomingBatch == null) {
            LOGGER.debug("Exception in archiveIncomingBatch", INCOMING_BATCH_NOT_FOUND);
            throw new BlazeInvalidArgException(INCOMING_BATCH, INCOMING_BATCH_NOT_FOUND);
        }
        dbIncomingBatch.setArchive(true);
        dbIncomingBatch.setArchiveDate(DateTime.now().getMillis());

        IncomingBatch updatedIncomingBatch = incomingBatchRepository.update(token.getCompanyId(), dbIncomingBatch.getId(), dbIncomingBatch);
        return updatedIncomingBatch;
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllArchivedIncomingBatches(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return incomingBatchRepository.getAllArchivedIncomingBatches(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);
    }

    /**
     * This method is used to update incoming batch status
     *
     * @param incomingBatchId
     * @param request
     * @return
     */
    @Override
    public IncomingBatch updateIncomingBatchStatus(String incomingBatchId, IncomingBatchStatusRequest request) {
        IncomingBatch dbIncomingBatch = null;
        if (StringUtils.isBlank(incomingBatchId)) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "incomingBatchId can't be blank");
        }
        IncomingBatch incomingBatch = incomingBatchRepository.get(token.getCompanyId(), incomingBatchId);
        if (incomingBatch != null) {
            boolean statusChange = false;
            if (request.getUpdatedStatus().equals(IncomingBatch.BatchStatus.IN_TESTING)) {
                incomingBatch.setStatus(request.getUpdatedStatus());
                statusChange = true;
            } else if (request.getUpdatedStatus().equals(IncomingBatch.BatchStatus.READY_FOR_SALE)) {
                BatchAddRequest batchAddRequest = new BatchAddRequest();
                batchAddRequest.setActive(incomingBatch.isActive());
                batchAddRequest.setCbd(incomingBatch.getCbd().doubleValue());
                batchAddRequest.setCbda(incomingBatch.getCbda().doubleValue());
                batchAddRequest.setCbn(incomingBatch.getCbn().doubleValue());
                batchAddRequest.setThc(incomingBatch.getThc().doubleValue());
                batchAddRequest.setThca(incomingBatch.getThca().doubleValue());
                batchAddRequest.setCostPerUnit(incomingBatch.getCostPerUnit());
                batchAddRequest.setProductId(incomingBatch.getProductId());
                batchAddRequest.setTrackTraceSystem(batchAddRequest.getTrackTraceSystem());
                batchAddRequest.setQuantity(incomingBatch.getQuantityReceived());
                ProductBatch dbProductBatch = inventoryService.addBatch(batchAddRequest);
                if (dbProductBatch != null) {
                    incomingBatch.setProductBatchId(dbProductBatch.getId());
                }
                incomingBatch.setStatus(request.getUpdatedStatus());
                statusChange = true;

            } else if (request.getUpdatedStatus().equals(IncomingBatch.BatchStatus.RECEIVED)) {
                incomingBatch.setStatus(request.getUpdatedStatus());
                statusChange = true;
            }
            if (statusChange) {
                dbIncomingBatch = incomingBatchRepository.update(token.getCompanyId(), incomingBatchId, incomingBatch);
            }
        } else {
            throw new BlazeInvalidArgException(INCOMING_BATCH, INCOMING_BATCH_NOT_FOUND);
        }
        return dbIncomingBatch;
    }

    /**
     * This method is used to update incoming batch by its id.
     *
     * @param incomingBatchId
     * @param incomingBatch
     * @return
     */
    @Override
    public IncomingBatch updateIncomingBatch(String incomingBatchId, IncomingBatch incomingBatch) {
        if (StringUtils.isBlank(incomingBatchId)) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "incomingBatchId can't be blank.");
        }
        IncomingBatch dbIncomingBatch = incomingBatchRepository.get(token.getCompanyId(), incomingBatchId);
        if (dbIncomingBatch == null) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, INCOMING_BATCH_NOT_FOUND);
        }
        if (incomingBatch.getQuantityReceived().compareTo(BigDecimal.ZERO) > 0 && incomingBatch.getCostPerUnit().compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal purchasePrice = incomingBatch.getQuantityReceived().multiply(incomingBatch.getCostPerUnit());
            incomingBatch.setBatchPurchasePrice(purchasePrice);
        } else {
            incomingBatch.setBatchPurchasePrice(incomingBatch.getBatchPurchasePrice());
        }
        if (incomingBatch.getStatus().equals(IncomingBatch.BatchStatus.READY_FOR_SALE)) {
            BatchAddRequest batchAddRequest = new BatchAddRequest();
            batchAddRequest.setActive(incomingBatch.isActive());
            batchAddRequest.setCbd(incomingBatch.getCbd().doubleValue());
            batchAddRequest.setCbda(incomingBatch.getCbda().doubleValue());
            batchAddRequest.setCbn(incomingBatch.getCbn().doubleValue());
            batchAddRequest.setThc(incomingBatch.getThc().doubleValue());
            batchAddRequest.setThca(incomingBatch.getThca().doubleValue());
            batchAddRequest.setCostPerUnit(incomingBatch.getCostPerUnit());
            batchAddRequest.setProductId(incomingBatch.getProductId());
            batchAddRequest.setTrackTraceSystem(batchAddRequest.getTrackTraceSystem());
            batchAddRequest.setQuantity(incomingBatch.getQuantityReceived());
            batchAddRequest.setVoidStatus(incomingBatch.getVoidStatus());
            ProductBatch dbProductBatch = inventoryService.addBatch(batchAddRequest);
            if (dbProductBatch != null) {
                incomingBatch.setProductBatchId(dbProductBatch.getId());
            }
            incomingBatch.setStatus(incomingBatch.getStatus());
        }
        return incomingBatchRepository.update(token.getCompanyId(), incomingBatchId, incomingBatch);
    }

    /**
     * This method is used to delete incoming batch by its id.
     *
     * @param incomingBatchId
     */
    @Override
    public void deleteIncomingBatch(String incomingBatchId) {
        if (StringUtils.isBlank(incomingBatchId)) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "incomingBatchId can't be blank");
        }
        IncomingBatch dbIncomingBatch = incomingBatchRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), incomingBatchId);
        if (dbIncomingBatch == null) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, INCOMING_BATCH_NOT_FOUND);
        } else if (dbIncomingBatch.isDeleted()) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Incoming batch is already deleted.");
        } else {
            incomingBatchRepository.removeByIdSetState(token.getCompanyId(), incomingBatchId);
        }

    }

    /**
     * This method is used to get all incoming batches according to active state.
     *
     * @param start
     * @param limit
     * @param state
     * @return
     */
    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByState(int start, int limit, boolean state) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<IncomingBatchResult> searchResult = incomingBatchRepository.getAllIncomingBatchesByState(token.getCompanyId(), token.getShopId(), state, "{modified:-1}", start, limit);

        if (searchResult.getValues().size() > 0) {
            for (IncomingBatchResult batchResult : searchResult.getValues()) {
                prepareIncomingBatchResult(batchResult);
            }
        }

        return searchResult;
    }

    /**
     * This method is used to gat all incoming batches according to its status.
     *
     * @param start
     * @param limit
     * @param status
     * @return
     */
    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByStatus(int start, int limit, IncomingBatch.BatchStatus status) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<IncomingBatchResult> searchResult = incomingBatchRepository.getAllIncomingBatchesByStatus(token.getCompanyId(), token.getShopId(), status, "{modified:-1}", start, limit);

        if (searchResult.getValues().size() > 0) {
            for (IncomingBatchResult batchResult : searchResult.getValues()) {
                prepareIncomingBatchResult(batchResult);
            }
        }

        return searchResult;
    }


    /**
     * This method is used to get all scanned incoming batches.
     *
     * @param request
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<IncomingBatchResult> getAllScannedIncomingBatch(IncomingBatchScannedRequest request, int start, int limit) {
        if (request == null) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Barcode can't be empty.");
        }
        BarcodeItem barcodeItem = barcodeService.getBarcodeItemByBarcode(token.getCompanyId(), token.getShopId(), request.getEntityType(), request.getBarcode());
        if (barcodeItem == null) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, "Barcode Item not found");
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<IncomingBatchResult> searchResult = incomingBatchRepository.getAllScannedIncomingBatch(token.getCompanyId(), token.getShopId(), request.getMertcId(), barcodeItem.getProductId(), start, limit, "{modified:-1}");

        if (searchResult.getValues().size() > 0) {
            for (IncomingBatchResult batchResult : searchResult.getValues()) {
                prepareIncomingBatchResult(batchResult);
            }
        }

        return searchResult;
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllDeletedIncomingBatch(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<IncomingBatchResult> searchResult = incomingBatchRepository.getAllDeletedIncomingBatch(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);
        if (searchResult.getValues().size() > 0) {
            for (IncomingBatchResult batchResult : searchResult.getValues()) {
                prepareIncomingBatchResult(batchResult);
            }
        }


        return searchResult;
    }

    @Override
    public void bulkIncomingBatchUpdate(BulkBatchUpdateRequest request) {
        List<String> batchIds = request.getIncomingBatchIds();
        if (batchIds == null || batchIds.isEmpty()) {
            throw new BlazeInvalidArgException(BULK_BATCH_UPDATE, "Please select Batches");
        }
        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(BULK_BATCH_UPDATE, "Please select a action type");
        }
        List<ObjectId> objectIds = new ArrayList<>();
        for (String id : request.getIncomingBatchIds()) {
            objectIds.add(new ObjectId(id));
        }
        switch (request.getOperationType()) {
            case STATUS:

                if (request.getBatchStatus() == null) {
                    throw new BlazeInvalidArgException(BULK_BATCH_UPDATE, "Please select a status");
                }
                if (request.getBatchStatus().equals(IncomingBatch.BatchStatus.READY_FOR_SALE)) {
                    for (String batchId : request.getIncomingBatchIds()) {
                        IncomingBatch incomingBatch = incomingBatchRepository.get(token.getCompanyId(), batchId);
                        BatchAddRequest batchAddRequest = new BatchAddRequest();
                        batchAddRequest.setActive(incomingBatch.isActive());
                        batchAddRequest.setCbd(incomingBatch.getCbd().doubleValue());
                        batchAddRequest.setCbda(incomingBatch.getCbda().doubleValue());
                        batchAddRequest.setCbn(incomingBatch.getCbn().doubleValue());
                        batchAddRequest.setThc(incomingBatch.getThc().doubleValue());
                        batchAddRequest.setThca(incomingBatch.getThca().doubleValue());
                        batchAddRequest.setCostPerUnit(incomingBatch.getCostPerUnit());
                        batchAddRequest.setProductId(incomingBatch.getProductId());
                        batchAddRequest.setTrackTraceSystem(batchAddRequest.getTrackTraceSystem());
                        batchAddRequest.setQuantity(incomingBatch.getQuantityReceived());
                        ProductBatch dbProductBatch = inventoryService.addBatch(batchAddRequest);
                        if (dbProductBatch != null) {
                            incomingBatch.setProductBatchId(dbProductBatch.getId());
                        }
                        incomingBatch.setStatus(request.getBatchStatus());
                        incomingBatchRepository.update(token.getCompanyId(), batchId, incomingBatch);
                    }

                } else {
                    incomingBatchRepository.bulkBatchStatusUpdate(token.getCompanyId(), token.getShopId(), objectIds, request.getBatchStatus());
                }
                break;
            case DELETE_BATCHES:

                incomingBatchRepository.bulkBatchDelete(token.getCompanyId(), token.getShopId(), objectIds);
                break;
            case UPDATE_BATCHES_TO_ARCHIVE:

                incomingBatchRepository.bulkBatchArchive(token.getCompanyId(), token.getShopId(), objectIds, request.isArchive());
                break;
            case VOID_STATUS:
                incomingBatchRepository.bulkBatchVoidStatusUpdate(token.getCompanyId(), token.getShopId(), objectIds, request.isVoidStatus());
        }


    }

    @Override
    public IncomingBatch updateIncomingBatchVoidStatus(String incomingBatchId, boolean voidStatus) {
        IncomingBatch incomingBatch = null;
        if (StringUtils.isBlank(incomingBatchId)) {
            throw new BlazeInvalidArgException(INCOMING_BATCH, INCOMING_BATCH_NOT_FOUND);
        }
        IncomingBatch dbIncomingBatch = incomingBatchRepository.get(token.getCompanyId(), incomingBatchId);
        if (dbIncomingBatch != null) {
            dbIncomingBatch.setVoidStatus(voidStatus);
            incomingBatch = incomingBatchRepository.update(token.getCompanyId(), incomingBatchId, dbIncomingBatch);
        }
        return incomingBatch;
    }

    @Override
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByVoidStatus(int start, int limit, boolean voidStatus) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<IncomingBatchResult> searchResult = incomingBatchRepository.getAllIncomingBatchesByVoidStatus(token.getCompanyId(), token.getShopId(), voidStatus, "{modified:-1}", start, limit);
        if (searchResult.getValues().size() > 0) {
            for (IncomingBatchResult batchResult : searchResult.getValues()) {
                prepareIncomingBatchResult(batchResult);
            }
        }

        return searchResult;
    }
}
