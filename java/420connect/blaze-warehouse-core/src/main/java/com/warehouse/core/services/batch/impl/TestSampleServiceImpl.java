package com.warehouse.core.services.batch.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.testsample.TestResult;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.models.transaction.ProductBatchQueuedTransaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductBatchStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.BatchActivityLogService;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.ProductBatchService;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleAttachmentRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleStatusRequest;
import com.google.inject.Provider;
import com.warehouse.core.services.batch.TestSampleService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestSampleServiceImpl extends AbstractAuthServiceImpl implements TestSampleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestSampleServiceImpl.class);
    private static final String TEST_SAMPLE = "Test Sample";
    private static final String TEST_SAMPLE_NOT_FOUND = "Error! Test Sample does not exist.";
    private static final String TEST_SAMPLE_NOT_BLANK = "Test sample can't be blank.";
    private static final String BATCH_NOT_EMPTY = "ProductBatch can't be blank.";
    private static final String BATCH_NOT_FOUND = "Error! Product batch does not exist.";
    private static final String TESTING_COMPANY_NOT_BLANK = "Testing Facility company cannot be blank";
    private static final String TESTING_COMPANY_NOT_FOUND = "Testing Facility company does not found";
    private static final String ATTACHMENT_NOT_FOUND = "Attachment does not found";
    private static final String TEST_SAMPLE_QUANTITY_ZERO = "Test Sample quantity cannot be zero or less than zero";

    private TestSampleRepository testSampleRepository;
    private ProductBatchRepository productBatchRepository;
    private VendorRepository vendorRepository;
    private ProductBatchService productBatchService;
    private InventoryRepository inventoryRepository;
    private BatchQuantityService batchQuantityService;
    private BatchQuantityRepository batchQuantityRepository;
    private BatchActivityLogService batchActivityLogService;
    @Inject
    private InventoryService inventoryService;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;

    @Inject
    public TestSampleServiceImpl(Provider<ConnectAuthToken> token,
                                 TestSampleRepository testSampleRepository,
                                 ProductBatchRepository productBatchRepository,
                                 VendorRepository vendorRepository,
                                 ProductBatchService productBatchService, InventoryRepository inventoryRepository,
                                 BatchQuantityService batchQuantityService,
                                 BatchQuantityRepository batchQuantityRepository,
                                 BatchActivityLogService batchActivityLogService) {
        super(token);
        this.testSampleRepository = testSampleRepository;
        this.productBatchRepository = productBatchRepository;
        this.vendorRepository = vendorRepository;
        this.productBatchService = productBatchService;
        this.inventoryRepository = inventoryRepository;
        this.batchQuantityService = batchQuantityService;
        this.batchQuantityRepository = batchQuantityRepository;
        this.batchActivityLogService = batchActivityLogService;
    }

    /**
     * This method is used to create a test sample .
     *
     * @param request
     * @return
     */

    @Override
    public TestSample addTestSample(TestSampleAddRequest request) {
        if (StringUtils.isBlank(request.getBatchId())) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, BATCH_NOT_EMPTY);
        }
        ProductBatch dbProductBatch = productBatchRepository.get(token.getCompanyId(), request.getBatchId());
        if (dbProductBatch == null) {
            throw new BlazeInvalidArgException("Product Batch", BATCH_NOT_FOUND);
        }
        if (dbProductBatch.getStatus().equals(ProductBatch.BatchStatus.READY_FOR_SALE)) {
            throw new BlazeInvalidArgException("Product Batch", "Product Batch already added to ready for sale.");
        }

        if (StringUtils.isBlank(request.getTestingCompanyId())) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TESTING_COMPANY_NOT_BLANK);
        }
        Vendor vendor = vendorRepository.get(token.getCompanyId(), request.getTestingCompanyId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TESTING_COMPANY_NOT_FOUND);
        }
        CompanyLicense companyLicense = vendor.getCompanyLicense(request.getLicenseId());
        if (!Vendor.CompanyType.TESTING_FACILITY.equals(companyLicense.getCompanyType())) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Company is not testing facility provider");
        }
        if (getTotalBatchQuantitiesByInventory(dbProductBatch).doubleValue() < request.getSample().doubleValue()) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Test Sample quantity can't be greater then batch quantity");
        }
        TestSample testSample = new TestSample();
        testSample.prepare(token.getCompanyId());
        testSample.setShopId(token.getShopId());
        testSample.setBatchId(request.getBatchId());
        testSample.setSample(request.getSample());
        long sampleNo = getNextSequence();
        testSample.setSampleNumber(sampleNo);
        testSample.setDateSent((request.getDateSent() == null) ? DateTime.now().getMillis() : request.getDateSent());
        testSample.setDateTested(request.getDateTested());
        testSample.setStatus((request.getStatus() == TestSample.SampleStatus.DRAFT) ? request.getStatus() : TestSample.SampleStatus.IN_TESTING);
        testSample.setTestResult(request.getTestResult());
        updateTestResult(testSample);
        testSample.setTestingCompanyId(request.getTestingCompanyId());
        testSample.setAttachments(request.getAttachments());
        testSample.setTestId(getTestSampleNo(dbProductBatch));
        testSample.setReferenceNo(request.getReferenceNo());
        testSample.setLicenseId(request.getLicenseId());

        if ((testSample.getStatus() != TestSample.SampleStatus.DRAFT)) {
            //update batch quantities
            inventoryService.createQueuedTransactionJobForBatchQuantity(dbProductBatch, Inventory.InventoryType.Quarantine, ProductBatchQueuedTransaction.OperationType.Update, request.getSample().negate(), testSample.getId(), InventoryOperation.SubSourceAction.AddTestSample);
            //update batch status when test sample is adding.
            dbProductBatch.setStatus(ProductBatch.BatchStatus.IN_TESTING);
            productBatchRepository.update(token.getCompanyId(), dbProductBatch.getId(), dbProductBatch);
        }
        batchActivityLogService.addBatchActivityLog(dbProductBatch.getId(), token.getActiveTopUser().getUserId(), String.format("Test sample %s added ", StringUtils.isNotBlank(testSample.getReferenceNo()) ? testSample.getReferenceNo() : testSample.getTestId()));
        return testSampleRepository.save(testSample);

    }

    /**
     * Private method to get total quantity for batch.
     *
     * @param productBatch
     * @return
     */
    private BigDecimal getTotalBatchQuantitiesByInventory(ProductBatch productBatch) {
        BigDecimal totalBatchQuantities = new BigDecimal(0);
        Inventory dbInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.QUARANTINE);
        List<ProductBatchByInventory> quantities = batchQuantityRepository.getBatchQuantitiesByInventory(token.getCompanyId(), token.getShopId(), productBatch.getId(), productBatch.getProductId());
        if (dbInventory != null) {
            for (ProductBatchByInventory productBatchByInventory : quantities) {
                if (productBatchByInventory.getInventoryId().equals(dbInventory.getId()) && !productBatchByInventory.getBatchQuantity().isEmpty()) {
                    for (BatchQuantity batchQuantity : productBatchByInventory.getBatchQuantity()) {
                        totalBatchQuantities = totalBatchQuantities.add(batchQuantity.getQuantity());
                    }
                }
            }
        }


        return totalBatchQuantities;
    }

    /**
     * This method is used to update test sample
     *
     * @param testSampleId
     * @param testSample
     * @return
     */
    @Override
    public TestSample updateTestSample(String testSampleId, TestSample testSample) {
        TestSample updateTestSample = null;
        if (StringUtils.isBlank(testSampleId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_BLANK);
        }
        TestSample dbTestSample = testSampleRepository.get(token.getCompanyId(), testSampleId);
        if (dbTestSample == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_FOUND);
        }

        if (StringUtils.isBlank(testSample.getBatchId())) {
            throw new BlazeInvalidArgException("Product Batch", "Product Batch not found.");
        }
        ProductBatch dbProductBatch = productBatchRepository.get(token.getCompanyId(), testSample.getBatchId());
        if (dbProductBatch == null) {
            throw new BlazeInvalidArgException("Product Batch", "Product Batch not found.");
        }

        if (StringUtils.isBlank(testSample.getTestingCompanyId())) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TESTING_COMPANY_NOT_BLANK);
        }
        Vendor vendor = vendorRepository.get(token.getCompanyId(), testSample.getTestingCompanyId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TESTING_COMPANY_NOT_FOUND);
        }
        CompanyLicense companyLicense = vendor.getCompanyLicense(testSample.getLicenseId());
        if (!Vendor.CompanyType.TESTING_FACILITY.equals(companyLicense.getCompanyType())) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Company is not testing facility provider");
        }
        double addQuantity = dbTestSample.getSample().doubleValue() - testSample.getSample().doubleValue();

        TestSample.SampleStatus oldStatus = dbTestSample.getStatus();

        dbTestSample.setBatchId(testSample.getBatchId());
        dbTestSample.setSample(testSample.getSample());
        dbTestSample.setDateSent(testSample.getDateSent());
        dbTestSample.setDateTested(testSample.getDateTested());
        dbTestSample.setTestResult(testSample.getTestResult());
        updateTestResult(dbTestSample);
        dbTestSample.setTestingCompanyId(testSample.getTestingCompanyId());
        dbTestSample.setAttachments(testSample.getAttachments());
        dbTestSample.setReferenceNo(testSample.getReferenceNo());
        dbTestSample.setStatus(testSample.getStatus());
        dbTestSample.setLicenseId(testSample.getLicenseId());

        updateTestSample = testSampleRepository.update(testSampleId, dbTestSample);

        if ((dbTestSample.getStatus() != TestSample.SampleStatus.DRAFT)) {
            //update batch quantities
            if (addQuantity != 0) {
                ProductBatchQueuedTransaction.OperationType operationType = (addQuantity > 0) ? ProductBatchQueuedTransaction.OperationType.Add : ProductBatchQueuedTransaction.OperationType.Update;
                BigDecimal updateQuantity = new BigDecimal(addQuantity);
                inventoryService.createQueuedTransactionJobForBatchQuantity(dbProductBatch, Inventory.InventoryType.Quarantine, operationType, updateQuantity, dbTestSample.getId(), InventoryOperation.SubSourceAction.UpdateTestSample);
            } else if (oldStatus == TestSample.SampleStatus.DRAFT) {
                inventoryService.createQueuedTransactionJobForBatchQuantity(dbProductBatch, Inventory.InventoryType.Quarantine, ProductBatchQueuedTransaction.OperationType.Update, testSample.getSample().negate(), dbTestSample.getId(), InventoryOperation.SubSourceAction.UpdateTestSample);
                dbProductBatch.setStatus(ProductBatch.BatchStatus.IN_TESTING);
                productBatchRepository.update(token.getCompanyId(), dbProductBatch.getId(), dbProductBatch);

            }
        }
        batchActivityLogService.addBatchActivityLog(dbProductBatch.getId(), token.getActiveTopUser().getUserId(), String.format("Test sample %s updated ", StringUtils.isNotBlank(testSample.getReferenceNo()) ? testSample.getReferenceNo() : testSample.getTestId()));
        return updateTestSample;
    }

    /**
     * This method is used to update test sample according to status.
     *
     * @param testSampleId
     * @param request
     * @return
     */
    @Override
    public TestSample updateTestSampleStatus(String testSampleId, TestSampleStatusRequest request) {
        TestSample dbTestSample = null;
        if (StringUtils.isBlank(testSampleId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_BLANK);
        }

        TestSample testSample = testSampleRepository.get(token.getCompanyId(), testSampleId);
        if (testSample != null) {
            boolean statusChange = true;
            testSample.setStatus(request.getUpdatedStatus());

            long passedDate = (TestSample.SampleStatus.PASSED.equals(request.getUpdatedStatus())) ? DateTime.now().getMillis() : 0;
            testSample.setDatePassed(passedDate);
            testSample.setDateTested(DateTime.now().getMillis());
            dbTestSample = testSampleRepository.update(token.getCompanyId(), testSampleId, testSample);
            if (TestSample.SampleStatus.PASSED.equals(request.getUpdatedStatus())) {
                SearchResult<TestSample> testSampleSearchResult = getAllTestSamplesByBatchId(testSample.getBatchId(), 0, Integer.MAX_VALUE);
                if (testSampleSearchResult != null && testSampleSearchResult.getValues() != null && !testSampleSearchResult.getValues().isEmpty()) {
                    for (TestSample testSampleResult : testSampleSearchResult.getValues()) {
                        if (!TestSample.SampleStatus.PASSED.equals(testSampleResult.getStatus())) {
                            statusChange = false;
                            break;
                        }
                    }
                }
                if (statusChange) {
                    ProductBatch dbProductBatch = productBatchRepository.get(token.getCompanyId(), dbTestSample.getBatchId());
                    if (dbProductBatch != null) {
                        ProductBatchStatusRequest updateRequest = new ProductBatchStatusRequest();
                        updateRequest.setStatus(ProductBatch.BatchStatus.READY_FOR_SALE);
                        batchActivityLogService.addBatchActivityLog(dbProductBatch.getId(), token.getActiveTopUser().getUserId(), String.format("Test sample %s status changed to %s ", StringUtils.isNotBlank(testSample.getReferenceNo()) ? testSample.getReferenceNo() : testSample.getTestId(), dbProductBatch.getStatus()));
                        productBatchService.updateProductBatchStatus(dbProductBatch.getId(), updateRequest);
                    }
                }
            }
        } else {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_FOUND);
        }
        return dbTestSample;
    }

    @Override
    public SearchResult<TestSample> getAllTestSamplesByBatchId(String productBatchId, int start, int limit) {
        if (StringUtils.isBlank(productBatchId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, BATCH_NOT_EMPTY);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return testSampleRepository.getAllTestSamplesByBatchId(token.getCompanyId(), productBatchId, "{modified:-1}", start, limit);
    }

    /**
     * This private method is used to get a sequence for sample number.
     *
     * @return
     */
    private long getNextSequence() {
        return companyUniqueSequenceService.getNewIdentifier(token.getCompanyId(), TEST_SAMPLE, testSampleRepository.count(token.getCompanyId()));
    }

    /**
     * This private method is used to update test result.
     *
     * @param testSample
     */
    private void updateTestResult(TestSample testSample) {
        if (testSample.getTestResult() != null) {
            TestResult testResult = testSample.getTestResult();
            if (testResult.getNote() != null) {
                for (Note note : testResult.getNote()) {
                    note.prepare();
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }
    }

    /**
     * This method use to delete test sample by id
     *
     * @param testSampleId
     */
    @Override
    public void deleteTestSampleById(String testSampleId) {
        if (StringUtils.isBlank(testSampleId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Test Sample can not be blank");
        }

        TestSample dbTestSample = testSampleRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), testSampleId);

        if (dbTestSample == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Test sample is not found");
        }

        if (dbTestSample.isDeleted()) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Test Sample is already deleted.");
        }

        ProductBatch dbBatch = productBatchRepository.get(token.getCompanyId(), dbTestSample.getBatchId());
        if (dbBatch == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Product batch does not exists for this test sample.");
        }
        if (dbBatch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Cannot delete test sample batch is already in ready for sale.");
        }

        if (dbTestSample.getStatus() != TestSample.SampleStatus.FAILED && dbTestSample.getStatus() != TestSample.SampleStatus.PASSED) {
            //check if status is testing or pending then add quantity back to batch.
            BigDecimal updateQuantity = dbTestSample.getSample();
            inventoryService.createQueuedTransactionJobForBatchQuantity(dbBatch, Inventory.InventoryType.Quarantine, ProductBatchQueuedTransaction.OperationType.Add, updateQuantity, dbTestSample.getId(), InventoryOperation.SubSourceAction.DeleteTestSample);
        }
        batchActivityLogService.addBatchActivityLog(dbBatch.getId(), token.getActiveTopUser().getUserId(), String.format("Test sample %s deleted ", StringUtils.isNotBlank(dbTestSample.getReferenceNo()) ? dbTestSample.getReferenceNo() : dbTestSample.getTestId()));
        testSampleRepository.removeByIdSetState(token.getCompanyId(), testSampleId);
    }

    /**
     * Override method to add attachment for test sample
     *
     * @param request
     * @return
     */
    @Override
    public TestSample addAttachments(TestSampleAttachmentRequest request) {
        if (request == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Request cannot be blank");
        }
        if (StringUtils.isBlank(request.getTestSampleId())) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_BLANK);
        }
        if (request.getCompanyAsset() == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Attachment not found");
        }
        TestSample dbTestSample = testSampleRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getTestSampleId());
        if (dbTestSample == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TESTING_COMPANY_NOT_FOUND);
        }
        CompanyAsset asset = request.getCompanyAsset();
        asset.prepare(token.getCompanyId());
        List<CompanyAsset> attachments = new ArrayList<>();
        if (dbTestSample.getAttachments() != null) {
            attachments = dbTestSample.getAttachments();
        }
        attachments.add(asset);
        dbTestSample.setAttachments(attachments);
        dbTestSample = testSampleRepository.update(token.getCompanyId(), dbTestSample.getId(), dbTestSample);
        return dbTestSample;

    }

    /**
     * Override method to get all attachments of test sample by test sample id
     *
     * @param testSampleId
     * @return
     */
    @Override
    public List<CompanyAsset> getAllTestSampleAttachment(String testSampleId) {
        TestSample dbTestSample = testSampleRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), testSampleId);
        if (dbTestSample == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_FOUND);
        }
        if (dbTestSample.getAttachments() != null) {
            return dbTestSample.getAttachments();
        }
        return new ArrayList<>();
    }

    /**
     * Override method to get attachment by test sample and attachment id
     *
     * @param testSampleId
     * @param attachmentId
     * @return
     */
    @Override
    public CompanyAsset getTestSampleAttachmentById(String testSampleId, String attachmentId) {
        if (StringUtils.isBlank(testSampleId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_BLANK);
        }
        if (StringUtils.isBlank(attachmentId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Attachment Id cannot be blank");
        }
        List<CompanyAsset> companyAssets = getAllTestSampleAttachment(testSampleId);
        CompanyAsset dbCompanyAsset = null;
        for (CompanyAsset asset : companyAssets) {
            if (asset.getId().equalsIgnoreCase(attachmentId)) {
                dbCompanyAsset = asset;
                break;
            }
        }
        if (dbCompanyAsset == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, ATTACHMENT_NOT_FOUND);
        }
        return dbCompanyAsset;
    }

    /**
     * Override method for delete attachment by id
     *
     * @param testSampleId
     * @param attachmentId
     */
    @Override
    public void deleteTestSampleAttachmentById(String testSampleId, String attachmentId) {
        if (StringUtils.isBlank(testSampleId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_BLANK);
        }
        TestSample dbTestSample = testSampleRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), testSampleId);
        if (dbTestSample == null) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, TEST_SAMPLE_NOT_FOUND);
        }
        if (StringUtils.isBlank(attachmentId)) {
            throw new BlazeInvalidArgException(TEST_SAMPLE, "Attachment Id cannot be blank");
        }
        List<CompanyAsset> companyAssets = new ArrayList<>();
        if (dbTestSample.getAttachments() != null && !dbTestSample.getAttachments().isEmpty()) {
            for (CompanyAsset asset : dbTestSample.getAttachments()) {
                if (!asset.getId().equalsIgnoreCase(attachmentId)) {
                    companyAssets.add(asset);
                }
            }
            dbTestSample.setAttachments(companyAssets);
            testSampleRepository.update(token.getCompanyId(), testSampleId, dbTestSample);
        }
    }

    private String getTestSampleNo(ProductBatch productBatch) {
        long count = testSampleRepository.countItemsByProductBatch(token.getCompanyId(), token.getShopId(), productBatch.getId());
        return productBatch.getSku() + "-" + (++count) + "-TS";
    }
}

