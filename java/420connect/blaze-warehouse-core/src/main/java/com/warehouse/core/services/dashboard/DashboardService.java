package com.warehouse.core.services.dashboard;

import com.warehouse.core.domain.models.dashboard.DashboardWidget;
import com.warehouse.core.rest.dashboard.DashboardWidgetAddRequest;

public interface DashboardService {
    DashboardWidget addDashboardWidget(DashboardWidgetAddRequest request);

    DashboardWidget dashboardWidgetById(String dashboardWidgetId);

    DashboardWidget updateDashboardWidget(String dashboardWidgetId, DashboardWidget dashboardWidget);
    DashboardWidget findByIds(String employeeId, String companyId, String shopId);

    DashboardWidget addDashboardWidget(DashboardWidgetAddRequest addRequest, boolean isRequired);
}
