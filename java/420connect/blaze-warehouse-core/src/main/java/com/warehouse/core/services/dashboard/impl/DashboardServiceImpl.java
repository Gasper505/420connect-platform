package com.warehouse.core.services.dashboard.impl;


import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.dashboard.DashboardWidget;
import com.warehouse.core.domain.repositories.dashboard.DashboardWidgetRepository;
import com.warehouse.core.rest.dashboard.DashboardWidgetAddRequest;
import com.warehouse.core.services.dashboard.DashboardService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class DashboardServiceImpl extends AbstractAuthServiceImpl implements DashboardService {

    private static final String DASHBOARD_WIDGET = "Dashboard Widget";

    private DashboardWidgetRepository dashboardWidgetRepository;
    private EmployeeRepository employeeRepository;

    @Inject
    public DashboardServiceImpl(Provider<ConnectAuthToken> token, DashboardWidgetRepository dashboardWidgetRepository,
                                EmployeeRepository employeeRepository) {
        super(token);
        this.dashboardWidgetRepository = dashboardWidgetRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public DashboardWidget addDashboardWidget(DashboardWidgetAddRequest request, boolean isRequired) {

        if (isRequired) {
            if (request.getEmployeeId() == null || StringUtils.isBlank(request.getEmployeeId())) {
                throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee ID Not Found");
            }
            if (!ObjectId.isValid(request.getEmployeeId())) {
                throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee ID is not valid");
            }
            if (request.getDashboardWidget().isEmpty()) {
                throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Dashboard Widgets not found");
            }
            Employee employee = employeeRepository.get(token.getCompanyId(), request.getEmployeeId());
            if (employee == null) {
                throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee Not Found");
            }
        }
        List<String> dashboardWidgetSet = new ArrayList<>();
        for (String dashboardName : request.getDashboardWidget()) {
            if (StringUtils.isBlank(dashboardName) && dashboardWidgetSet.contains(dashboardName))
                continue;
            dashboardWidgetSet.add(dashboardName);
        }

        DashboardWidget dashboardWidget = dashboardWidgetRepository.findByIds(request.getEmployeeId(), token.getCompanyId(), token.getShopId());
        if (dashboardWidget == null) {
            dashboardWidget = new DashboardWidget();
            dashboardWidget.prepare(token.getCompanyId());
            dashboardWidget.setShopId(token.getShopId());
            dashboardWidget.setEmployeeId(request.getEmployeeId());
            dashboardWidget.setWidgets(dashboardWidgetSet);
            dashboardWidgetRepository.saveDashboardWidget(dashboardWidget);

        } else {
            dashboardWidget.setWidgets(dashboardWidgetSet);
            dashboardWidgetRepository.update(token.getCompanyId(), dashboardWidget.getId(), dashboardWidget);
        }
        return dashboardWidget;
    }

    @Override
    public DashboardWidget dashboardWidgetById(String dashboardWidgetId) {
        if (dashboardWidgetId == null || StringUtils.isBlank(dashboardWidgetId)) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "DashboardWidget ID not blank");
        }
        if (!ObjectId.isValid(dashboardWidgetId)) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "DashboardWidget ID is not valid");
        }
        DashboardWidget dashboardWidget = dashboardWidgetRepository.getById(dashboardWidgetId);
        if (dashboardWidget == null) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Dashboard Widget not found");
        }
        return dashboardWidget;
    }

    @Override
    public DashboardWidget updateDashboardWidget(String dashboardWidgetId, DashboardWidget dashboardWidget) {
        DashboardWidget dbWidget = dashboardWidgetRepository.getById(dashboardWidgetId);
        if (dbWidget == null) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Dashboard Widget not found");
        }
        if (dashboardWidget.getEmployeeId() == null || StringUtils.isBlank(dashboardWidget.getEmployeeId())) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee ID Not Found");
        }
        if (!ObjectId.isValid(dashboardWidget.getEmployeeId())) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee ID is not valid");
        }
        if (dashboardWidget.getWidgets().isEmpty()) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Dashboard Widgets not found");
        }
        Employee employee = employeeRepository.get(token.getCompanyId(), dashboardWidget.getEmployeeId());
        if (employee == null) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee Not Found");
        }
        List<String> dashboardWidgetSet = new ArrayList<>();
        for (String dashboardName : dashboardWidget.getWidgets()) {
            if (StringUtils.isBlank(dashboardName) && dashboardWidgetSet.contains(dashboardName))
                continue;
            dashboardWidgetSet.add(dashboardName);
        }
        dbWidget.prepare(token.getCompanyId());
        dbWidget.setShopId(token.getShopId());
        dbWidget.setWidgets(dashboardWidgetSet);
        dbWidget.setEmployeeId(dashboardWidget.getEmployeeId());

        dashboardWidgetRepository.update(token.getCompanyId(), dashboardWidgetId, dashboardWidget);

        return dashboardWidget;
    }

    @Override
    public DashboardWidget findByIds(String employeeId, String companyId, String shopId) {
        if (employeeId == null || StringUtils.isBlank(employeeId)) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Employee ID Not Found");
        }
        if (shopId == null || StringUtils.isBlank(shopId)) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Shop ID Not Found");
        }
        if (companyId == null || StringUtils.isBlank(companyId)) {
            throw new BlazeInvalidArgException(DASHBOARD_WIDGET, "Company ID Not Found");
        }
        return dashboardWidgetRepository.findByIds(employeeId, companyId, shopId);
    }

    @Override
    public DashboardWidget addDashboardWidget(DashboardWidgetAddRequest addRequest) {
        return addDashboardWidget(addRequest, true);
    }
}
