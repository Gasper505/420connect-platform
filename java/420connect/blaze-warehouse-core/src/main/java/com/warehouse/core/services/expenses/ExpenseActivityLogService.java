package com.warehouse.core.services.expenses;

import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;

public interface ExpenseActivityLogService {
    ExpensesActivityLog addExpenseActivityLog(final String expenseId, final String employeeId, final String log);

    SearchResult<ExpensesActivityLog> getAllActivityLogsForExpenses(final String expenseId, final int start, final int limit);

    ExpensesActivityLog addActivityComment(ActivityCommentRequest request);
}
