package com.warehouse.core.services.expenses;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.rest.expenses.request.ExpenseBulkRequestUpdate;
import com.warehouse.core.rest.expenses.request.ExpenseEmailRequest;
import com.warehouse.core.rest.expenses.request.ExpensesRequest;
import com.warehouse.core.rest.expenses.result.ExpenseResultComplete;
import com.warehouse.core.rest.expenses.result.ExpensesResult;
import com.warehouse.core.rest.invoice.request.InvoiceAttachmentRequest;

import java.io.InputStream;

public interface ExpensesService {
    ExpensesResult getExpensesById(final String expensesId);

    Expenses createExpenses(final ExpensesRequest expensesRequest);

    Expenses updateExpenses(final String expensesId, final Expenses expenses);

    void deleteExpensesById(final String expensesId);

    SearchResult<ExpensesResult> getAllExpensesByShop(final int start, final int limit, final Expenses.ExpenseSort sortParam, final String searchTerm);

    DateSearchResult<ExpensesResult> getAllExpensesByDates(final long afterDate, final long beforeDate);

    SearchResult<ExpensesResult> getAllExpensesByCustomerId(final String customerId, final int start, final int limit);

    void sendExpenseEmailToCustomer(final String expenseId, final ExpenseEmailRequest request);

    Expenses addExpenseAttachment(final String expenseId, final InvoiceAttachmentRequest attachmentRequest);

    Expenses updateExpenseAttachment(final String expenseId, final String attachmentId, final CompanyAsset request);

    void deleteExpenseAttachment(final String expenseId, final String attachmentId);

    Expenses markExpenseAsArchived(final String expenseId);

    SearchResult<ExpensesResult> getAllArchivedExpenses(final int start, final int limit, final String searchTerm, final Expenses.ExpenseSort sortParam);

    SearchResult<ExpensesResult> getAllExpensesByState(final boolean state, final int start, final int limit, final Expenses.ExpenseSort sortParam, final String searchTerm);

    void bulkExpenseUpdate(ExpenseBulkRequestUpdate request);

    InputStream createPdfForExpense(final String expenseId);

    Expenses updateExpenseState(final String expenseId, final boolean state);

    DateSearchResult<ExpenseResultComplete> getExpenseCompleteResultByDates(long startDate, long endDate);
}
