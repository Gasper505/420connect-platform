package com.warehouse.core.services.expenses.impl;

import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;
import com.warehouse.core.domain.repositories.expenses.ExpenseActivityLogRepository;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import com.warehouse.core.services.expenses.ExpenseActivityLogService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;

public class ExpenseActivityLogServiceImpl extends AbstractAuthServiceImpl implements ExpenseActivityLogService {

    private static final String EXPENSE_ACTIVITY_LOG = "Expense activity log";
    private static final String EXPENSE_NOT_EMPTY = "Expense can't be empty";
    private static final String EXPENSE_ACTIVITY_NOT_FOUND = "Expense activity log does not found";
    private static final String EXPENSE_NOT_FOUND = "Expense not found";

    private ExpenseActivityLogRepository expenseActivityLogRepository;
    private ExpensesRepository expensesRepository;

    @Inject
    public ExpenseActivityLogServiceImpl(Provider<ConnectAuthToken> token,
                                         ExpenseActivityLogRepository expenseActivityLogRepository,
                                         ExpensesRepository expensesRepository) {
        super(token);
        this.expenseActivityLogRepository = expenseActivityLogRepository;
        this.expensesRepository = expensesRepository;
    }

    @Override
    public ExpensesActivityLog addExpenseActivityLog(String expenseId, String employeeId, String log) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE_ACTIVITY_LOG, EXPENSE_NOT_EMPTY);
        }
        ExpensesActivityLog expensesActivityLog = new ExpensesActivityLog();
        expensesActivityLog.prepare(token.getCompanyId());
        expensesActivityLog.setShopId(token.getShopId());
        expensesActivityLog.setTargetId(expenseId);
        expensesActivityLog.setEmployeeId(token.getActiveTopUser().getUserId());
        expensesActivityLog.setLog(log);
        expensesActivityLog.setActivityType(ExpensesActivityLog.ActivityType.NORMAL_LOG);
        return expenseActivityLogRepository.save(expensesActivityLog);
    }

    /**
     * This method is used to get all expenses activity list
     *
     * @param expenseId
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ExpensesActivityLog> getAllActivityLogsForExpenses(String expenseId, int start, int limit) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE_ACTIVITY_LOG, EXPENSE_NOT_EMPTY);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return expenseActivityLogRepository.getAllExpenseActivityLogByExpenseId(token.getCompanyId(), token.getShopId(), expenseId, "{modified:-1}", start, limit);
    }

    /**
     * This method is to add expense activity log
     *
     * @param request
     * @return
     */
    @Override
    public ExpensesActivityLog addActivityComment(ActivityCommentRequest request) {

        if (request == null || StringUtils.isBlank(request.getComment())) {
            throw new BlazeInvalidArgException(EXPENSE_ACTIVITY_LOG, "Comment cannot be empty");
        }
        if (StringUtils.isBlank(request.getTargetId()) || !ObjectId.isValid(request.getTargetId())) {
            throw new BlazeInvalidArgException(EXPENSE_ACTIVITY_LOG, EXPENSE_NOT_FOUND);
        }

        Expenses expense = expensesRepository.getById(request.getTargetId());
        if (expense == null) {
            throw new BlazeInvalidArgException(EXPENSE_ACTIVITY_LOG, EXPENSE_NOT_FOUND);
        }

        ExpensesActivityLog expensesActivityLog = new ExpensesActivityLog();
        expensesActivityLog.prepare(token.getCompanyId());
        expensesActivityLog.setShopId(token.getShopId());
        expensesActivityLog.setEmployeeId(token.getActiveTopUser().getUserId());
        expensesActivityLog.setTargetId(request.getTargetId());
        expensesActivityLog.setLog(request.getComment());
        expensesActivityLog.setActivityType(ExpensesActivityLog.ActivityType.COMMENT);
        return expenseActivityLogRepository.save(expensesActivityLog);
    }
}
