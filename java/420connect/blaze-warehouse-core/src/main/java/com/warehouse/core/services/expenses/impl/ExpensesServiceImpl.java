package com.warehouse.core.services.expenses.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.QrCodeUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import com.warehouse.core.rest.expenses.request.ExpenseBulkRequestUpdate;
import com.warehouse.core.rest.expenses.request.ExpenseEmailRequest;
import com.warehouse.core.rest.expenses.request.ExpensesRequest;
import com.warehouse.core.rest.expenses.result.ExpenseResultComplete;
import com.warehouse.core.rest.expenses.result.ExpensesResult;
import com.warehouse.core.rest.invoice.request.InvoiceAttachmentRequest;
import com.warehouse.core.services.expenses.ExpenseActivityLogService;
import com.warehouse.core.services.expenses.ExpensesService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;


public class ExpensesServiceImpl extends AbstractAuthServiceImpl implements ExpensesService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExpensesServiceImpl.class);
    private static final String EXPENSE_NOT_FOUND = "Expense does not found";
    private static final String EXPENSE = "Expense";
    private static final String CAN_NOT_BLANK = "Expense can't be blank";
    private static final String CUSTOMER_COMPANY = "Customer company";
    private static final String CUSTOMER_COMPANY_NOT_FOUND = "Customer company does not exist";
    private static final String ATTACHMENT = "Attachment";
    private static final String ATTACHMENT_NOT_FOUND = "Attachment does not found";
    private static final String ATTACHMENT_NOT_EMPTY = "Attachment can't be empty";
    private static final String BULK_EXPENSE_UPDATE = "Bulk Expense Update";
    private static final String QR_CODE_ERROR = "Error while creating QR code";
    private static final String EXPENSE_HTML_RESOURCE = "/expense.html";
    private static final String EXPENSE_CATEGORY_NOT_FOUND = "Expense category not found.";
    private static final String EXPENSE_AMOUNT_CANNOT_BE_ZERO = "Expense amount cannot be zero.";
    private static final String EXPENSE_NAME_NOT_BLANK = "Expense name cannot be blank.";

    private ExpensesRepository expensesRepository;
    private ExpenseActivityLogService expenseActivityLogService;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    private PurchaseOrderRepository purchaseOrderRepository;
    private AmazonS3Service amazonS3Service;
    private VendorRepository vendorRepository;
    private UserActivityService userActivityService;
    private CompanyRepository companyRepository;
    private CompanyAssetRepository companyAssetRepository;
    private ElasticSearchManager elasticSearchManager;
    private EmployeeRepository employeeRepository;
    private ShopRepository shopRepository;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;

    @Inject
    public ExpensesServiceImpl(Provider<ConnectAuthToken> token,
                               ExpensesRepository expensesRepository,
                               ExpenseActivityLogService expenseActivityLogService,
                               AmazonServiceManager amazonServiceManager,
                               ConnectConfiguration connectConfiguration,
                               CompanyUniqueSequenceRepository companyUniqueSequenceRepository,
                               PurchaseOrderRepository purchaseOrderRepository,
                               AmazonS3Service amazonS3Service, VendorRepository vendorRepository,
                               UserActivityService userActivityService,
                               CompanyRepository companyRepository, ElasticSearchManager elasticSearchManager,
                               CompanyAssetRepository companyAssetRepository,
                               EmployeeRepository employeeRepository,
                               ShopRepository shopRepository) {
        super(token);
        this.expensesRepository = expensesRepository;
        this.expenseActivityLogService = expenseActivityLogService;
        this.amazonServiceManager = amazonServiceManager;
        this.connectConfiguration = connectConfiguration;
        this.companyUniqueSequenceRepository = companyUniqueSequenceRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.amazonS3Service = amazonS3Service;
        this.vendorRepository = vendorRepository;
        this.userActivityService = userActivityService;
        this.companyRepository = companyRepository;
        this.elasticSearchManager = elasticSearchManager;
        this.companyAssetRepository = companyAssetRepository;
        this.employeeRepository = employeeRepository;
        this.shopRepository = shopRepository;
    }

    /**
     * This method is used to get Expenses by its id.
     *
     * @param expensesId
     * @return
     */

    @Override
    public ExpensesResult getExpensesById(String expensesId) {
        if (StringUtils.isBlank(expensesId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        ExpensesResult expensesResult = expensesRepository.get(token.getCompanyId(), expensesId, ExpensesResult.class);
        if (expensesResult == null) {
            LOGGER.debug(EXPENSE, EXPENSE_NOT_FOUND);
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }
        prepareExpensesResult(expensesResult);
        return expensesResult;
    }

    /**
     * This private method is used to prepare a result for expenses.
     *
     * @param expensesResult
     */
    private void prepareExpensesResult(ExpensesResult expensesResult) {

        if (StringUtils.isNotBlank(expensesResult.getCustomerId())) {
            Vendor customerCompany = vendorRepository.get(token.getCompanyId(), expensesResult.getCustomerId());
            if (customerCompany != null)
                expensesResult.setCustomerCompany(customerCompany);
        }
        if (StringUtils.isNotBlank(expensesResult.getPurchaseOrderId())) {
            PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), expensesResult.getPurchaseOrderId());
            if (purchaseOrder != null) {
                BigDecimal totalPoAmount = purchaseOrder.getGrandTotal();
                expensesResult.setBalanceDue(totalPoAmount);
            }
            expensesResult.setPurchaseOrder(purchaseOrder);
        }
        SearchResult<ExpensesActivityLog> expensesActivityLogSearchResult = expenseActivityLogService.getAllActivityLogsForExpenses(expensesResult.getId(), 0, Integer.MAX_VALUE);
        if (expensesActivityLogSearchResult != null && expensesActivityLogSearchResult.getValues() != null && !expensesActivityLogSearchResult.getValues().isEmpty()) {
            expensesResult.setExpensesActivityLogs(expensesActivityLogSearchResult.getValues());
        }
        Company company = companyRepository.getById(expensesResult.getCompanyId());
        String logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        expensesResult.setCompanyLogo(logoURL);

        if (StringUtils.isNotBlank(expensesResult.getCreatedByEmployeeId())) {
            Employee employee = employeeRepository.get(token.getCompanyId(), expensesResult.getCreatedByEmployeeId());
            if (employee != null) {
                String name = employee.getFirstName() + " " + (StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : "");
                expensesResult.setCreatedByEmployeeName(name);
            }
        }


    }

    /**
     * This method is used to create a new expense.
     *
     * @param expensesRequest
     * @return
     */
    @Override
    public Expenses createExpenses(ExpensesRequest expensesRequest) {

        Vendor vendor;
        if (StringUtils.isBlank(expensesRequest.getExpenseName())) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NAME_NOT_BLANK);
        }
        if (expensesRequest.getExpenseCategory() == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_CATEGORY_NOT_FOUND);
        }
        if (expensesRequest.getAmount() == null || expensesRequest.getAmount().doubleValue() == 0) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_AMOUNT_CANNOT_BE_ZERO);
        }

        PurchaseOrder purchaseOrder = null;
        if (StringUtils.isNotBlank(expensesRequest.getPurchaseOrderId())) {
            purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), expensesRequest.getPurchaseOrderId());
            if (purchaseOrder == null) {
                throw new BlazeInvalidArgException(EXPENSE, "Purchase Order not found");
            }
        }
        Expenses expenses = new Expenses();
        expenses.prepare(token.getCompanyId());
        expenses.setShopId(token.getShopId());
        expenses.setExpenseName(expensesRequest.getExpenseName());
        expenses.setExpenseCategory(expensesRequest.getExpenseCategory());
        expenses.setAmount(expensesRequest.getAmount());
        expenses.setReference(expensesRequest.getReference());
        expenses.setCustomerId(expensesRequest.getCustomerId());
        if (expensesRequest.getAttachments() != null && expensesRequest.getAttachments().size() > 0) {
            for (CompanyAsset companyAsset : expensesRequest.getAttachments()) {
                companyAsset.prepare(token.getCompanyId());
            }
            expenses.setAttachments(expensesRequest.getAttachments());
        }

        if (expensesRequest.getNotes() != null && expensesRequest.getNotes().size() > 0) {
            for (Note note : expensesRequest.getNotes()) {
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            }
            expenses.setNotes(expensesRequest.getNotes());
        }
        expenses.setArchive(false);
        long ordinal = this.getNextSequence();
        String expenseNo = NumberUtils.uniqueNumber("EXP", ordinal);
        expenses.setExpenseNumber(expenseNo);
        expenses.setActive(true);
        expenses.setPurchaseOrderId(expensesRequest.getPurchaseOrderId());
        if (StringUtils.isNotBlank(expensesRequest.getPurchaseOrderId())) {
            BigDecimal totalPoAmount = purchaseOrder.getGrandTotal();
            expenses.setBalanceDue((totalPoAmount.doubleValue() < 0) ? new BigDecimal(0) : totalPoAmount);
        } else {
            expenses.setBalanceDue(expensesRequest.getBalanceDue());
        }

        expenses.setCreatedByEmployeeId(token.getActiveTopUser().getUserId());
        expenses.setExpenseDate(expensesRequest.getExpenseDate());

        Expenses dbExpenses = expensesRepository.save(expenses);

        if (dbExpenses != null) {
            // TODO: Fix
            // elasticSearchManager.createOrUpdateIndexedDocument(dbExpenses, Expenses.class, this);

            expenseActivityLogService.addExpenseActivityLog(dbExpenses.getId(), token.getActiveTopUser().getUserId(), dbExpenses.getExpenseName() + " Expense created");
            UploadFileResult result = null;
            File file = null;
            try {
                InputStream inputStream = QrCodeUtil.getQrCode(dbExpenses.getExpenseNumber());
                file = QrCodeUtil.stream2file(inputStream, ".png");
                String keyName = dbExpenses.getId() + "-" + dbExpenses.getExpenseName().trim();

                keyName = keyName.replaceAll("[&#$%^]", "");

                result = amazonS3Service.uploadFilePublic(file, keyName, ".png");
                if (!Objects.isNull(result) && StringUtils.isNotBlank(result.getUrl())) {
                    CompanyAsset asset = new CompanyAsset();
                    asset.prepare(token.getCompanyId());
                    asset.setName("Expense");
                    asset.setKey(result.getKey());
                    asset.setActive(true);
                    asset.setType(Asset.AssetType.Photo);
                    asset.setPublicURL(result.getUrl());
                    asset.setSecured(false);
                    companyAssetRepository.save(asset);
                    dbExpenses.setExpenseQRCodeAsset(asset);
                    dbExpenses.setExpenseQRCodeUrl(result.getUrl());
                    expensesRepository.update(dbExpenses.getId(), dbExpenses);
                }
            } catch (IOException ex) {
                LOGGER.error(QR_CODE_ERROR);
            }
        }

        return dbExpenses;

    }

    /**
     * This method is used to update an expense by its id and model
     *
     * @param expensesId
     * @param expenses
     * @return
     */
    @Override
    public Expenses updateExpenses(String expensesId, Expenses expenses) {
        Expenses dbExpenses = expensesRepository.get(token.getCompanyId(), expensesId);
        if (dbExpenses == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }
        if (StringUtils.isBlank(expenses.getExpenseName())) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NAME_NOT_BLANK);
        }

        if (expenses.getExpenseCategory() == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_CATEGORY_NOT_FOUND);
        }

        if (expenses.getAmount() == null || expenses.getAmount().doubleValue() == 0) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_AMOUNT_CANNOT_BE_ZERO);
        }

        PurchaseOrder purchaseOrder = null;
        Vendor vendor;
        if (StringUtils.isNotBlank(expenses.getPurchaseOrderId()) && ObjectId.isValid(expenses.getPurchaseOrderId())) {
            purchaseOrder = purchaseOrderRepository.getPOById(token.getCompanyId(), expenses.getPurchaseOrderId());
            if (purchaseOrder == null) {
                throw new BlazeInvalidArgException(EXPENSE, "Purchase Order not found");
            }
        }

        updateAttachments(expenses);
        updateNotes(expenses);
        dbExpenses.setExpenseName(expenses.getExpenseName());
        dbExpenses.setExpenseCategory(expenses.getExpenseCategory());
        dbExpenses.setAmount(expenses.getAmount());
        dbExpenses.setReference((StringUtils.isBlank(expenses.getReference()) ? "" : expenses.getReference()));
        dbExpenses.setCustomerId(expenses.getCustomerId());
        dbExpenses.setActive(expenses.isActive());
        dbExpenses.setArchive(expenses.isArchive());
        dbExpenses.setBalanceDue(expenses.getBalanceDue());
        dbExpenses.setPurchaseOrderId(expenses.getPurchaseOrderId());
        if (purchaseOrder != null) {
            BigDecimal totalPoAmount = purchaseOrder.getGrandTotal();
            expenses.setBalanceDue((totalPoAmount.doubleValue() < 0) ? new BigDecimal(0) : totalPoAmount);
        }
        dbExpenses.setAttachments(expenses.getAttachments());
        dbExpenses.setNotes(expenses.getNotes());

        dbExpenses.setUpdateByEmployeeId(token.getActiveTopUser().getUserId());
        dbExpenses.setExpenseDate(expenses.getExpenseDate());

        Expenses updateExpense = expensesRepository.update(expensesId, dbExpenses);
        if (updateExpense != null) {
            // TODO: Fix
            // elasticSearchManager.createOrUpdateIndexedDocument(updateExpense, Expenses.class, this);

            expenseActivityLogService.addExpenseActivityLog(updateExpense.getId(), token.getActiveTopUser().getUserId(), updateExpense.getExpenseName() + " Expense updated");
        }
        return updateExpense;
    }

    /**
     * This private method is used to update Expense attachments.
     *
     * @param expenses
     */
    private void updateAttachments(Expenses expenses) {
        if (expenses.getAttachments().size() > 0) {
            List<CompanyAsset> assetList = expenses.getAttachments();
            List<CompanyAsset> companyAssetList = new ArrayList<>();
            for (CompanyAsset asset : assetList) {
                if (asset.getId() == null || asset.getId().isEmpty()) {
                    asset.prepare(token.getCompanyId());
                    companyAssetList.add(asset);
                } else {
                    companyAssetList.add(asset);
                }

            }

            expenses.setAttachments(companyAssetList);

        }


    }

    /**
     * This method is used to update note for expenses.
     *
     * @param expenses
     */
    private void updateNotes(Expenses expenses) {
        if (expenses.getNotes().size() > 0) {
            List<Note> notesList = expenses.getNotes();
            List<Note> newNotesList = new ArrayList<>();
            for (Note note : notesList) {
                if (note.getId() == null || note.getId().isEmpty()) {
                    note.prepare();
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                    newNotesList.add(note);
                } else {
                    newNotesList.add(note);
                }

            }

            expenses.setNotes(newNotesList);

        }
    }

    /**
     * This method is used to set status deleted for expenses.
     *
     * @param expensesId
     */
    @Override
    public void deleteExpensesById(String expensesId) {
        if (StringUtils.isBlank(expensesId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        Expenses dbExpenses = expensesRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), expensesId);
        if (dbExpenses == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        } else if (dbExpenses.isDeleted()) {
            throw new BlazeInvalidArgException(EXPENSE, "Expense is already deleted");
        } else {
            expensesRepository.removeByIdSetState(token.getCompanyId(), expensesId);
            expenseActivityLogService.addExpenseActivityLog(dbExpenses.getId(), token.getActiveTopUser().getUserId(), dbExpenses.getExpenseName() + " Expense deleted");

            elasticSearchManager.deleteIndexedDocument(expensesId, Expenses.class);
        }

    }

    /**
     * This method is used to get all expenses for a shop by shopId.
     *
     * @param start
     * @return
     * @Param limit
     */
    @Override
    public SearchResult<ExpensesResult> getAllExpensesByShop(int start, int limit, Expenses.ExpenseSort sortOption, String searchTerm) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        sortOption = (sortOption == null) ? Expenses.ExpenseSort.DATE : sortOption;

        String sortOptionStr = "";
        if (Expenses.ExpenseSort.AMOUNT.toString().equals(sortOption.toString())) {
            sortOptionStr = "{ amount :-1 }";
        } else {
            //default sorting will be on modified date
            sortOptionStr = "{ modified :-1 }";
        }

        SearchResult<ExpensesResult> searchResult;
        if (StringUtils.isNotBlank(searchTerm)) {
            searchResult = expensesRepository.getAllExpensesWithoutStateByTerm(token.getCompanyId(), token.getShopId(), searchTerm, sortOptionStr, start, limit, ExpensesResult.class);
        } else {
            searchResult = expensesRepository.findItems(token.getCompanyId(), token.getShopId(), sortOptionStr, start, limit, ExpensesResult.class);
        }
        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (ExpensesResult result : searchResult.getValues()) {
                prepareExpensesResult(result);
            }
        }
        return searchResult;


    }

    /**
     * This method is used to get all expenses for a company by companyId and dates.
     *
     * @param afterDate
     * @param beforeDate
     * @return
     */
    @Override
    public DateSearchResult<ExpensesResult> getAllExpensesByDates(long afterDate, long beforeDate) {

        DateSearchResult<ExpensesResult> searchResult = expensesRepository.findExpensesWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate, "{modified:-1}", ExpensesResult.class);
        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (ExpensesResult result : searchResult.getValues()) {
                prepareExpensesResult(result);
            }
        }
        return searchResult;
    }

    /**
     * This method is used to get all expenses for a company by companyId and customerId
     *
     * @param customerId
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ExpensesResult> getAllExpensesByCustomerId(String customerId, int start, int limit) {
        if (StringUtils.isBlank(customerId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ExpensesResult> searchResult = expensesRepository.getAllExpensesByCustomerId(token.getCompanyId(), token.getShopId(), customerId, start, limit, "{modified:-1}", ExpensesResult.class);
        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (ExpensesResult result : searchResult.getValues()) {
                prepareExpensesResult(result);
            }
        }
        return searchResult;
    }

    @Override
    public void sendExpenseEmailToCustomer(String expenseId, ExpenseEmailRequest request) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        final Expenses dbExpense = expensesRepository.get(token.getCompanyId(), expenseId);
        if (dbExpense == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }
        final Vendor customerCompany = vendorRepository.get(token.getCompanyId(), dbExpense.getCustomerId());

        String body = emailBody(customerCompany, dbExpense);

        HashMap<String, InputStream> attachmentStreamList = new HashMap<>();
        HashMap<String, HashMap<String, InputStream>> attachmentMap = new HashMap<>();
        if (dbExpense.getAttachments() != null && !dbExpense.getAttachments().isEmpty()) {
            attachmentStreamList = getExpenseAttachmentList(dbExpense.getAttachments());
            attachmentMap.put("attachment", attachmentStreamList);
        }

        amazonServiceManager.sendMultiPartEmail("support@blaze.me", request.getEmail(), connectConfiguration.getAppName(), body, null, null, attachmentMap, "Content-Disposition", "", "");
    }

    private String emailBody(Vendor customerCompany, Expenses expenses) {
        Shop shop = shopRepository.getById(expenses.getShopId());
        Company company = companyRepository.getById(expenses.getCompanyId());

        Employee employee = employeeRepository.getById(expenses.getCreatedByEmployeeId());

        InputStream inputStream = ExpensesServiceImpl.class.getResourceAsStream(EXPENSE_HTML_RESOURCE);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in expense email", ex);
        }
        String body = writer.toString();
        body = body.replaceAll("==expenseName==", expenses.getExpenseCategory().toString().replaceAll("_", " ").toLowerCase());
        if (customerCompany != null) {
            body = body.replaceAll("==customerCompanyName==", customerCompany.getName().toLowerCase());
        } else {
            body = body.replaceAll("==customerCompanyName==", TextUtil.textOrEmpty(""));
        }
        if (customerCompany != null && customerCompany.getAddress() != null) {
            Address address = customerCompany.getAddress();
            StringBuilder addressStr = new StringBuilder();
            addressStr.append(StringUtils.isNotBlank(address.getAddress()) ? address.getAddress() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getCity()) ? address.getCity() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getState()) ? address.getState() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getCountry()) ? address.getCountry() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getZipCode()) ? address.getZipCode() : "");

            body = body.replaceAll("==address==", addressStr.toString().toLowerCase());

        } else {
            body = body.replaceAll("==showAddress==", "display:none");
        }

        String timeZone = (StringUtils.isBlank(token.getRequestTimeZone()) ? shop.getTimeZone() : token.getRequestTimeZone());

        body = body.replaceAll("==showCompany==", TextUtil.textOrEmpty((customerCompany == null) ? "display:none;" : ""));
        body = body.replaceAll("==expenseReference==", (StringUtils.isBlank(expenses.getReference())) ? "" : expenses.getReference().replaceAll("&", "&amp;"));
        body = body.replaceAll("==expenseDate==", TextUtil.toDate(DateUtil.toDateTime((expenses.getExpenseDate() == 0) ? expenses.getCreated() : expenses.getExpenseDate(), timeZone)));
        body = body.replaceAll("==balanceDue==", expenses.getAmount().toString());
        if (CollectionUtils.isNotEmpty(expenses.getNotes())) {
            String notes = getNoteSection(expenses.getNotes());
            body = body.replaceAll("==notes==", notes.replaceAll("&", "&amp;"));
            body = body.replaceAll("==showNotes==", TextUtil.textOrEmpty(StringUtils.isNotBlank(notes) ? "" : "display:none"));
        } else {
            body = body.replaceAll("==showNotes==", TextUtil.textOrEmpty("display:none;"));
        }
        String poNumber = "";
        if (StringUtils.isNotBlank(expenses.getPurchaseOrderId())) {
            PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), expenses.getPurchaseOrderId());
            poNumber = purchaseOrder != null ? purchaseOrder.getPoNumber() : "";
        }

        body = body.replaceAll("==showPoNumber==", TextUtil.textOrEmpty(StringUtils.isNotBlank(poNumber) ? "" : "display:none;"));
        body = body.replaceAll("==poNumber==", TextUtil.textOrEmpty(poNumber));

        /*if(expenses.getExpenseQRCodeAsset () != null)
        body = body.replaceAll ( "==QRCodeImage==",(expenses.getExpenseQRCodeAsset ().getPublicURL () == null)?"":expenses.getExpenseQRCodeAsset ().getPublicURL ());*/
        body = body.replaceAll("==QRCodeImage==", (expenses.getExpenseQRCodeUrl() == null) ? "" : expenses.getExpenseQRCodeUrl());

        body = body.replaceAll("==shopName==", ((shop != null && (StringUtils.isNotBlank(shop.getName().toLowerCase())) ? shop.getName() : TextUtil.textOrEmpty(""))));

        if (shop != null && shop.getAddress() != null) {
            Address address = shop.getAddress();
            StringBuilder addressStr = new StringBuilder();
            addressStr.append(StringUtils.isNotBlank(address.getAddress()) ? address.getAddress() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getCity()) ? address.getCity() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getState()) ? address.getState() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getCountry()) ? address.getCountry() + "<BR/>" : "");
            addressStr.append(StringUtils.isNotBlank(address.getZipCode()) ? address.getZipCode() : "");

            body = body.replaceAll("==shopAddress==", addressStr.toString().toLowerCase());
        } else {
            body = body.replaceAll("==showShopAddress==", TextUtil.textOrEmpty("display:none;"));
        }
        body = body.replaceAll("==showShop==", TextUtil.textOrEmpty((shop == null) ? "display:none;" : ""));
        StringBuilder employeeName = new StringBuilder("");
        if (employee != null) {
            employeeName =  employee.getName();
        }
        body = body.replaceAll("==createdBy==", TextUtil.textOrEmpty(employeeName.toString()));
        body = body.replaceAll("==showCreatedBy==", (TextUtil.textOrEmpty((StringUtils.isBlank(employeeName) ? "display:none;" : ""))));
        String logoURL = (shop.getLogo() != null && StringUtils.isNotBlank(shop.getLogo().getLargeURL())) ? shop.getLogo().getLargeURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        body = body.replaceAll("==companyLogo==", logoURL);

        List<CompanyAsset> companyAssetList = expenses.getAttachments();
        if (CollectionUtils.isNotEmpty(companyAssetList)) {
            CompanyAsset asset = expenses.getAttachments().get(0);
            if (asset != null) {
                body = body.replaceAll("==attachmentUrl==", TextUtil.textOrEmpty(asset.getPublicURL()));
            } else {
                body = body.replaceAll("==showAttachmentUrl==", TextUtil.textOrEmpty("display:none;"));
            }
        }
        body = body.replaceAll("==showAttachmentUrl==", TextUtil.textOrEmpty(CollectionUtils.isEmpty(companyAssetList) ? "display:none;" : ""));

        writer.write(body);
        return body;
    }


    private String getNoteSection(List<Note> notes) {
        String noteSection = "";
        for (Note note : notes) {
            if (StringUtils.isNotBlank(note.getMessage())) {
                noteSection += "<p style=\"font-size:14px;color:#9E9E9E;margin:2px 0px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\">" + note.getMessage() + "</p>";
            }
        }
        return noteSection;
    }

    @Override
    public Expenses addExpenseAttachment(String expenseId, InvoiceAttachmentRequest attachmentRequest) {
        Expenses dbExpense = null;
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        if (attachmentRequest == null || attachmentRequest.getCompanyAsset() == null || attachmentRequest.getCompanyAsset().isEmpty()) {
            throw new BlazeInvalidArgException(EXPENSE, ATTACHMENT_NOT_FOUND);
        }
        Expenses expenses = expensesRepository.get(token.getCompanyId(), expenseId);
        if (expenses == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }

        List<CompanyAsset> assets = attachmentRequest.getCompanyAsset();
        List<CompanyAsset> attachments = (expenses.getAttachments() == null) ? new ArrayList<>() : expenses.getAttachments();
        StringBuilder attachmentName = new StringBuilder();
        for (CompanyAsset asset : assets) {
            asset.prepare(token.getCompanyId());
            attachments.add(asset);
            if (StringUtils.isNotBlank(attachmentName)) {
                attachmentName.append(",");
            }
            attachmentName.append(asset.getName());

        }

        expenses.setAttachments(attachments);

        dbExpense = expensesRepository.update(token.getCompanyId(), expenses.getId(), expenses);

        if (dbExpense != null) {
            expenseActivityLogService.addExpenseActivityLog(dbExpense.getId(), token.getActiveTopUser().getUserId(), attachmentName + "added");
        }


        return dbExpense;
    }

    @Override
    public Expenses updateExpenseAttachment(String expenseId, String attachmentId, CompanyAsset asset) {
        Expenses dbExpense = null;
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        if (StringUtils.isBlank(attachmentId)) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_EMPTY);
        }
        if (asset == null) {
            throw new BlazeInvalidArgException(EXPENSE, ATTACHMENT_NOT_FOUND);
        }
        Expenses expenses = expensesRepository.get(token.getCompanyId(), expenseId);
        if (expenses == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }

        List<CompanyAsset> companyAssetList = expenses.getAttachments();

        CompanyAsset companyAsset = getInvoiceAttachmentById(companyAssetList, attachmentId);

        if (companyAsset == null) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }


        companyAsset.setName(asset.getName());
        companyAsset.setKey(asset.getKey());
        companyAsset.setType(asset.getType());
        companyAsset.setPublicURL(asset.getPublicURL());
        companyAsset.setActive(asset.isActive());
        companyAsset.setPriority(asset.getPriority());
        companyAsset.setSecured(asset.isSecured());

        dbExpense = expensesRepository.update(token.getCompanyId(), expenses.getId(), expenses);

        if (dbExpense != null) {
            expenseActivityLogService.addExpenseActivityLog(dbExpense.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment updated");
        }


        return dbExpense;
    }

    private CompanyAsset getInvoiceAttachmentById(List<CompanyAsset> companyAssetList, String attachmentId) {
        CompanyAsset asset = null;
        if (companyAssetList != null) {
            for (CompanyAsset dbAsset : companyAssetList) {
                if (dbAsset.getId().equals(attachmentId)) {
                    asset = dbAsset;
                    break;
                }
            }
        }

        return asset;
    }

    @Override
    public void deleteExpenseAttachment(String expenseId, String attachmentId) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        Expenses expense = expensesRepository.get(token.getCompanyId(), expenseId);
        if (expense == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }

        List<CompanyAsset> companyAssetList = expense.getAttachments();
        CompanyAsset asset = getInvoiceAttachmentById(companyAssetList, attachmentId);
        if (asset != null) {
            companyAssetList.remove(asset);
            Expenses dbExpense = expensesRepository.update(token.getCompanyId(), expense.getId(), expense);

            if (dbExpense != null) {
                expenseActivityLogService.addExpenseActivityLog(dbExpense.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment deleted");
            }
        } else {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }

    }


    @Override
    public Expenses markExpenseAsArchived(String expenseId) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        Expenses dbExpense = expensesRepository.get(token.getCompanyId(), expenseId);
        if (dbExpense == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }
        dbExpense.setArchive(true);
        Expenses updatedExpense = expensesRepository.update(token.getCompanyId(), expenseId, dbExpense);

        expenseActivityLogService.addExpenseActivityLog(dbExpense.getId(), token.getActiveTopUser().getUserId(), dbExpense.getExpenseName() + " Expense archived");

        // TODO: Fix
        // elasticSearchManager.createOrUpdateIndexedDocument(updatedExpense, Expenses.class, this);

        return updatedExpense;
    }

    @Override
    public SearchResult<ExpensesResult> getAllArchivedExpenses(int start, int limit, String searchTerm, Expenses.ExpenseSort sortOption) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }


        sortOption = (sortOption == null) ? Expenses.ExpenseSort.DATE : sortOption;

        String sortOptionStr = "";
        if (Expenses.ExpenseSort.AMOUNT.toString().equals(sortOption.toString())) {
            sortOptionStr = "{ amount :-1 }";
        } else {
            //default sorting will be on modified date
            sortOptionStr = "{ modified :-1 }";
        }

        SearchResult<ExpensesResult> searchResult;
        if (StringUtils.isBlank(searchTerm)) {
            searchResult = expensesRepository.getAllArchivedExpenses(token.getCompanyId(), token.getShopId(), sortOptionStr, start, limit, ExpensesResult.class);
        } else {
            searchResult = expensesRepository.getAllArchivedExpensesByTerm(token.getCompanyId(), token.getShopId(), searchTerm, sortOptionStr, start, limit, ExpensesResult.class);
        }
        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (ExpensesResult result : searchResult.getValues()) {
                prepareExpensesResult(result);
            }
        }
        return searchResult;
    }

    /**
     * This private method is used to get a sequence for Expense number.
     *
     * @return
     */
    private long getNextSequence() {
        return companyUniqueSequenceService.getNewIdentifier(token.getCompanyId(), EXPENSE, expensesRepository.count(token.getCompanyId()));
    }

    /**
     * This is override method to get all expenses by state or given search term (term may be expense name, number,category )
     *
     * @param state
     * @param start
     * @param limit
     * @param sortOption
     * @param searchTerm
     * @return
     */
    @Override
    public SearchResult<ExpensesResult> getAllExpensesByState(boolean state, int start, int limit, Expenses.ExpenseSort sortOption, String searchTerm) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        sortOption = (sortOption == null) ? Expenses.ExpenseSort.DATE : sortOption;

        String sortOptionStr = "";
        if (Expenses.ExpenseSort.AMOUNT.toString().equals(sortOption.toString())) {
            sortOptionStr = "{ amount :-1 }";
        } else if (Expenses.ExpenseSort.CREATED.toString().equals(sortOption.toString())) {
            sortOptionStr = "{ created :-1 }";
        } else {
            //default sorting will be on modified date
            sortOptionStr = "{ modified :-1 }";
        }
        SearchResult<ExpensesResult> searchResult;
        if (StringUtils.isNotBlank(searchTerm)) {
            searchResult = expensesRepository.getAllExpensesByTerm(token.getCompanyId(), token.getShopId(), state, searchTerm, sortOptionStr, start, limit, ExpensesResult.class);
        } else {
            searchResult = expensesRepository.getAllExpensesByState(token.getCompanyId(), token.getShopId(), sortOptionStr, state, start, limit, ExpensesResult.class);
        }

        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (ExpensesResult result : searchResult.getValues()) {
                prepareExpensesResult(result);
            }
        }

        return searchResult;
    }


    @Override
    public void bulkExpenseUpdate(ExpenseBulkRequestUpdate request) {
        List<String> expenseIds = request.getExpenseIds();
        if (expenseIds == null || expenseIds.isEmpty()) {
            throw new BlazeInvalidArgException(BULK_EXPENSE_UPDATE, "Please select expenses");
        }
        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(BULK_EXPENSE_UPDATE, "Please select operation type");
        }
        List<ObjectId> expenseObjectId = new ArrayList<>();
        for (String id : expenseIds) {
            if (StringUtils.isNotBlank(id) && ObjectId.isValid(id)) {
                expenseObjectId.add(new ObjectId(id));
            }
        }

        switch (request.getOperationType()) {
            case EXPENSE_CATEGORY:
                if (request.getExpenseCategory() == null)
                    throw new BlazeInvalidArgException(BULK_EXPENSE_UPDATE, "Expense category not found");
                expensesRepository.bulkUpdateExpenseCategory(token.getCompanyId(), token.getShopId(), expenseObjectId, request.getExpenseCategory());
                break;
            case EXPENSE_DELETE:
                expensesRepository.bulkDeleteExpense(token.getCompanyId(), token.getShopId(), expenseObjectId);
                break;
            case EXPENSE_ARCHIVE:
                expensesRepository.bulkArchiveExpense(token.getCompanyId(), token.getShopId(), expenseObjectId, request.isArchived());
                break;
            case EXPENSE_STATE:
                expensesRepository.bulkChangeState(token.getCompanyId(), token.getShopId(), expenseObjectId, request.isActive());
        }
    }

    @Override
    public InputStream createPdfForExpense(String expenseId) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        final Expenses dbExpense = expensesRepository.get(token.getCompanyId(), expenseId);
        if (dbExpense == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }
        Vendor customerCompany = null;
        if (StringUtils.isNotBlank(dbExpense.getCustomerId())) {
            customerCompany = vendorRepository.get(token.getCompanyId(), dbExpense.getCustomerId());
        }

        String body = emailBody(customerCompany, dbExpense);
        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, EXPENSE_HTML_RESOURCE));
    }


    /**
     * This private method used to get top 5 expense attachment
     *
     * @return
     */
    private HashMap<String, InputStream> getExpenseAttachmentList(List<CompanyAsset> attachment) {

        attachment.sort(new Comparator<CompanyAsset>() {
            @Override
            public int compare(CompanyAsset o1, CompanyAsset o2) {
                DateTime d1 = DateUtil.parseLongToDate(o1.getModified());
                DateTime d2 = DateUtil.parseLongToDate(o2.getModified());
                return d2.compareTo(d1);
            }
        });

        HashMap<String, InputStream> attachmentInputStream = new HashMap<>();

        for (CompanyAsset asset : attachment) {
            if (asset != null && StringUtils.isNotBlank(asset.getKey())) {
                AssetStreamResult assetStreamResult = amazonS3Service.downloadFile(asset.getKey(), true);
                if (assetStreamResult != null) {
                    InputStream stream = assetStreamResult.getStream();
                    if (stream != null) {
                        attachmentInputStream.put(StringUtils.isBlank(asset.getName()) ? asset.getKey() : asset.getName(), stream);
                    }
                }
            }
            if (attachmentInputStream.size() == 3) {
                break;
            }
        }
        return attachmentInputStream;
    }

    /**
     * This method is used to update en expense state that is active or in-active.
     *
     * @param expenseId
     * @param state
     * @return
     */
    @Override
    public Expenses updateExpenseState(String expenseId, boolean state) {
        if (StringUtils.isBlank(expenseId)) {
            throw new BlazeInvalidArgException(EXPENSE, CAN_NOT_BLANK);
        }
        Expenses dbExpense = expensesRepository.get(token.getCompanyId(), expenseId);
        if (dbExpense == null) {
            throw new BlazeInvalidArgException(EXPENSE, EXPENSE_NOT_FOUND);
        }
        if (!state) {
            dbExpense.setActive(false);
        } else {
            dbExpense.setActive(true);
        }

        Expenses updatedExpense = expensesRepository.update(token.getCompanyId(), expenseId, dbExpense);

        // TODO: Fix
        // elasticSearchManager.createOrUpdateIndexedDocument(updatedExpense, Expenses.class, this);

        return updatedExpense;

    }

    // TODO: Fix
//    @Override
//    public JSONObject toElasticSearchObject(Object obj) {
//        final JSONObject jsonObject = new JSONObject();
//
//        Expenses expenses = obj instanceof Expenses ? (Expenses) obj : null;
//
//        if(expenses == null) {
//            throw new BlazeInvalidArgException(EXPENSE, "Object is not instance of Expenses");
//        }
//
//        Vendor vendor = vendorRepository.get(token.getCompanyId(), expenses.getCustomerId());
//
//        jsonObject.put("expenseName", expenses.getExpenseName());
//        jsonObject.put("expenseCategory", expenses.getExpenseCategory());
//        jsonObject.put("expenseNumber", expenses.getExpenseNumber());
//        jsonObject.put("archive", expenses.isArchive());
//        jsonObject.put("active", expenses.isActive());
//        jsonObject.put("customerId", vendor != null ? vendor.getName() : "");
//
//        return jsonObject;
//    }

    @Override
    public DateSearchResult<ExpenseResultComplete> getExpenseCompleteResultByDates(long startDate, long endDate) {
        if (endDate <= 0) endDate = DateTime.now().getMillis();
        DateSearchResult<ExpenseResultComplete> expenseResult = expensesRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), startDate, endDate, ExpenseResultComplete.class);
        if (expenseResult != null && expenseResult.getValues() != null && !expenseResult.getValues().isEmpty()) {
            List<ObjectId> poIds = new ArrayList<>();
            for (ExpenseResultComplete expense : expenseResult.getValues()) {
                prepareExpensesResult(expense);
                if (StringUtils.isNotBlank(expense.getPurchaseOrderId()) && ObjectId.isValid(expense.getPurchaseOrderId())) {
                    poIds.add(new ObjectId(expense.getPurchaseOrderId()));
                }

            }
            if (!poIds.isEmpty()) {
                HashMap<String, PurchaseOrder> purchaseOrderHashMap = purchaseOrderRepository.listAsMap(token.getCompanyId(), poIds);
                for (ExpenseResultComplete expense : expenseResult.getValues()) {
                    PurchaseOrder purchaseOrder = purchaseOrderHashMap.get(expense.getPurchaseOrderId());
                    expense.setPurchaseOrder(purchaseOrder);
                }
            }

        }
        return expenseResult;
    }

}
