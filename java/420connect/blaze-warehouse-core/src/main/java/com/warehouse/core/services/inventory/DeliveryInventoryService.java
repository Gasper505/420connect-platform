package com.warehouse.core.services.inventory;

import com.warehouse.core.domain.models.inventory.DeliveryInventory;
import com.warehouse.core.rest.inventory.request.DeliveryInventoryRequest;

public interface DeliveryInventoryService {
    DeliveryInventory getDeliveryInventoryById(final String deliveryInventoryId);

    DeliveryInventory updateDeliveryInventory(final String deliveryInventoryId, final DeliveryInventory deliveryInventory);

    DeliveryInventory createDeliveryInventory(final DeliveryInventoryRequest deliveryInventoryRequest);

    void deleteDeliveryInventoryById(final String deliveryInventoryId);
}
