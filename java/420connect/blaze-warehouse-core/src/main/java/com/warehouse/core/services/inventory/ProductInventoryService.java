package com.warehouse.core.services.inventory;


import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductByVendorResult;

import java.util.List;

public interface ProductInventoryService {

    SearchResult<ProductByVendorResult> getProductsByVendorId(String vendorId, boolean active, boolean countQuantity);
    void updateProductStatus(List<String> productIds, ProductStatus.Status status);
}
