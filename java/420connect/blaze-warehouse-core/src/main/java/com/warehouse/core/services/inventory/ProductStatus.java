package com.warehouse.core.services.inventory;

public class ProductStatus {
    public enum Status {
        DELETE,
        ACTIVE,
        INACTIVE
    }
}
