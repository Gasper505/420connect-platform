package com.warehouse.core.services.inventory.impl;

import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.inventory.DeliveryInventory;
import com.warehouse.core.domain.repositories.inventory.DeliveryInventoryRepository;
import com.warehouse.core.rest.inventory.request.DeliveryInventoryRequest;
import com.warehouse.core.services.inventory.DeliveryInventoryService;

import javax.inject.Inject;

public class DeliveryInventoryServiceImpl extends AbstractAuthServiceImpl implements DeliveryInventoryService {

    private DeliveryInventoryRepository deliveryInventoryRepository;

    @Inject
    public DeliveryInventoryServiceImpl(Provider<ConnectAuthToken> token,
                                        DeliveryInventoryRepository deliveryInventoryRepository) {
        super(token);
        this.deliveryInventoryRepository = deliveryInventoryRepository;
    }

    /**
     * This method is used to get delivery inventory by id
     *
     * @param deliveryInventoryId
     * @return
     */
    @Override
    public DeliveryInventory getDeliveryInventoryById(String deliveryInventoryId) {
        return deliveryInventoryRepository.get(token.getCompanyId(), deliveryInventoryId);
    }

    /**
     * This method is used to delete delivery inventory by its id with company and shop.
     *
     * @param deliveryInventoryId
     */
    @Override
    public void deleteDeliveryInventoryById(String deliveryInventoryId) {
        DeliveryInventory dbDeliveryInventory = deliveryInventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), deliveryInventoryId);
        if (dbDeliveryInventory != null) {
            deliveryInventoryRepository.removeByIdSetState(token.getCompanyId(), deliveryInventoryId);
        } else {
            throw new BlazeInvalidArgException("Delivery Inventory", "There is some problem in deletion");
        }

    }

    /**
     * This method is used to create new delivery inventory.
     *
     * @param deliveryInventoryRequest
     * @return
     */
    @Override
    public DeliveryInventory createDeliveryInventory(DeliveryInventoryRequest deliveryInventoryRequest) {
        DeliveryInventory deliveryInventory = new DeliveryInventory();
        deliveryInventory.prepare(token.getCompanyId());
        deliveryInventory.setShopId(token.getShopId());
        deliveryInventory.setBusinessName(deliveryInventoryRequest.getBusinessName());
        deliveryInventory.setContactName(deliveryInventoryRequest.getContactName());
        deliveryInventory.setLicenseType(deliveryInventoryRequest.getLicenseType());
        deliveryInventory.setPhoneNumber(deliveryInventoryRequest.getPhoneNumber());
        deliveryInventory.setStateLicense(deliveryInventoryRequest.getStateLicense());
        if (deliveryInventoryRequest.getAddress() != null) {
            Address address = deliveryInventoryRequest.getAddress();
            address.prepare(token.getCompanyId());
            deliveryInventory.setAddress(address);
        }

        return deliveryInventoryRepository.save(deliveryInventory);
    }

    /**
     * This method is used to update delivery inventory by its id and model with company.
     *
     * @param deliveryInventoryId
     * @param deliveryInventory
     * @return
     */
    @Override
    public DeliveryInventory updateDeliveryInventory(String deliveryInventoryId, DeliveryInventory deliveryInventory) {
        DeliveryInventory dbDeliveryInventory = deliveryInventoryRepository.get(token.getCompanyId(), deliveryInventoryId);
        if (dbDeliveryInventory == null) {
            throw new BlazeInvalidArgException("Delivery Inventory", "Delivery Inventory does not exist with this id");
        }
        return deliveryInventoryRepository.update(deliveryInventoryId, deliveryInventory);
    }
}
