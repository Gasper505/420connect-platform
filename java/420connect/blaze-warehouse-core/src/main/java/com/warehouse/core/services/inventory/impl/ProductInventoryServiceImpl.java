package com.warehouse.core.services.inventory.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductByVendorResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.domain.repositories.invoice.ShippingManifestRepository;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;
import com.warehouse.core.rest.invoice.result.ShippingBatchDetails;
import com.warehouse.core.services.inventory.ProductInventoryService;
import com.warehouse.core.services.inventory.ProductStatus;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductInventoryServiceImpl extends AbstractAuthServiceImpl implements ProductInventoryService {

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ShippingManifestRepository shippingManifestRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    BarcodeService barcodeService;
    @Inject
    RealtimeService realtimeService;

    @Inject
    public ProductInventoryServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public SearchResult<ProductByVendorResult> getProductsByVendorId(String vendorId, boolean active, boolean countQuantity) {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (countQuantity && !shop.isRetail()) {
            SearchResult<ProductByVendorResult> products = productRepository.getProductsByVendorId(token.getCompanyId(), token.getShopId(), vendorId, active, ProductByVendorResult.class);

            this.calculateAvailableQuantity(products);

            return products;
        } else {
            return productRepository.getProductsByVendorId(token.getCompanyId(), token.getShopId(), vendorId, active, ProductByVendorResult.class);
        }
    }

    /**
     * This method calculates :
     * quantity available (inventory that is added in the shipping manifest will be counted as committed inventory for calculating `Quantity Available`) &
     * Quantity in PO (It is the total number of inventory of product in POs (Not already present in product's inventory) except already received)
     * @param products : list of products
     */
    private void calculateAvailableQuantity(SearchResult<ProductByVendorResult> products) {

        List<String> productIds = new ArrayList<>();
        for (ProductByVendorResult product : products.getValues()) {
            productIds.add(product.getId());
        }

        List<ShippingManifest> shippingManifests = shippingManifestRepository.getAllShippingManifestByProductId(token.getCompanyId(), token.getShopId(), "{modified:-1}", productIds);
        SearchResult<PurchaseOrder> purchaseOrders = purchaseOrderRepository.getAllPurchaseOrderByProductId(token.getCompanyId(), token.getShopId(), "{modified:-1}", productIds);
        Map<String, BigDecimal> poProductQuantityMap = new HashMap<>();
        Map<String, BigDecimal> quantityAvailableMap = new HashMap<>();

        for (ProductByVendorResult product : products.getValues()) {
            for (PurchaseOrder purchaseOrder : purchaseOrders.getValues()) {
                if (purchaseOrder.getPoProductRequestList() != null && purchaseOrder.getPoProductRequestList().size() > 0) {
                    for (POProductRequest productRequest : purchaseOrder.getPoProductRequestList()) {
                        if (productRequest.getProductId().equalsIgnoreCase(product.getId())) {
                            addProductAvailableQuantity(poProductQuantityMap, product.getId(), productRequest.getRequestQuantity().doubleValue());
                            break;
                        }
                    }
                }
            }

            for (ShippingManifest manifest : shippingManifests) {
                if (manifest.getProductMetrcInfo() != null && manifest.getProductMetrcInfo().size() > 0) {
                    for (ProductMetrcInfo productMetrcInfo : manifest.getProductMetrcInfo()) {
                        if (productMetrcInfo.getProductId().equals(product.getId()) && productMetrcInfo.getBatchDetails() != null && productMetrcInfo.getBatchDetails().size() > 0) {
                            for (ShippingBatchDetails detail : productMetrcInfo.getBatchDetails()) {
                                addProductAvailableQuantity(quantityAvailableMap, product.getId(), detail.getQuantity().doubleValue());
                            }
                        }
                    }
                }
            }

            BigDecimal totalQuantity = new BigDecimal(0);
            if (product.getQuantities() != null && product.getQuantities().size() > 0) {
                for (ProductQuantity quantity : product.getQuantities()) {
                    totalQuantity = totalQuantity.add(quantity.getQuantity());
                }
            }

            BigDecimal totalAvailableQuantity = new BigDecimal(0);
            if (quantityAvailableMap.get(product.getId()) != null) {
                totalAvailableQuantity = totalQuantity.subtract(quantityAvailableMap.get(product.getId()));
            } else {
                totalAvailableQuantity = totalQuantity;
            }

            product.setQuantityInPO(poProductQuantityMap.get(product.getId()));
            product.setQuantityAvailable(totalAvailableQuantity);
        }

    }

    private void addProductAvailableQuantity(Map<String, BigDecimal> poProductQuantityMap, String productId, double requestQuantity) {
        poProductQuantityMap.putIfAbsent(productId, BigDecimal.ZERO);
        double quantity = poProductQuantityMap.get(productId).doubleValue() + requestQuantity;
        poProductQuantityMap.put(productId, new BigDecimal(quantity));
    }

    /**
     * This method update product status:
     * invoices with status IN_PROGRESS , DRAFT, SENT  &
     * purchase orders with status InProgress, ReceivingShipment, RequiredApproval, Approved, WaitingShipment, ReceivingShipment &
     * transactions with status   Queued, Hold,  InProgress
     * @param productIds : list of products id
     * @param status : DELETE, ACTIVE, INACTIVE,
     */
    @Override
    public void updateProductStatus(List<String> productIds, ProductStatus.Status status) {
        List<ObjectId> ids = new ArrayList<>();

        for (String pid : productIds) {
            if (StringUtils.isNotBlank(pid) && ObjectId.isValid(pid)) {
                ids.add(new ObjectId(pid));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(ids));

        if (productHashMap.size() == 0) {
            throw new BlazeInvalidArgException("Product", "Product not found");
        }

        if (status.equals(ProductStatus.Status.DELETE) || status.equals(ProductStatus.Status.INACTIVE)) {
                long invoiceCount = invoiceRepository.countInvoicesByProductId(token.getCompanyId(), token.getShopId(), productIds);
                long purchaseorderCount = purchaseOrderRepository.countPurchaseOrderByProductId(token.getCompanyId(), token.getShopId(), productIds);
                long transactionsCount = transactionRepository.countTransactionsByProductId(token.getCompanyId(), token.getShopId(), productIds);
                if (invoiceCount > 0 || purchaseorderCount > 0 || transactionsCount > 0) {
                    throw new BlazeInvalidArgException("Product", "This product is being used in invoices and(or) purchase orders and(or) transactions. So can not deactivate it.");
                }
            }

        if (status == (ProductStatus.Status.DELETE)) {
                for (Product product : productHashMap.values()) {
                    //boolean hasQuantity = false;
                    for (ProductQuantity quantity : product.getQuantities()) {
                        if (quantity.getQuantity().doubleValue() > 0) {
                            throw new BlazeInvalidArgException("Product", "Cannot delete " + product.getName() + " products with available inventory. You can clear inventory by reporting a loss.");
                        }
                    }
                }
                for (Product product : productHashMap.values()) {
                    elasticSearchManager.deleteIndexedDocument(product.getId(), Product.class);
                }
                productRepository.bulkDeleteProduct(token.getCompanyId(), token.getShopId(), ids);
                barcodeService.deleteBarCodesForProducts(token.getCompanyId(), token.getShopId(), productIds);
                realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                        RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
            } else {
            productRepository.bulkUpdateStatus(token.getCompanyId(), token.getShopId(), ids, status == ProductStatus.Status.ACTIVE);
        }

    }

}
