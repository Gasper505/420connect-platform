package com.warehouse.core.services.invoice;

import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;

public interface InvoiceActivityLogService {
    InvoiceActivityLog addInvoiceActivityLog(final String invoiceId, final String employeeId, final String log);

    SearchResult<InvoiceActivityLog> getAllInvoiceActivityLog(final String companyId, final String invoiceId, final int start, final int limit);

    InvoiceActivityLog addActivityComment(final ActivityCommentRequest request);
}
