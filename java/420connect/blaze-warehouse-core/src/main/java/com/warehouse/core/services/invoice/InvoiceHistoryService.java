package com.warehouse.core.services.invoice;

import com.warehouse.core.domain.models.invoice.InvoiceHistory;

import java.util.List;

public interface InvoiceHistoryService {
    InvoiceHistory addInvoiceHistory(final String invoiceId, final String employeeId);

    InvoiceHistory updateInvoiceHistory(final String invoiceId, final String employeeId);

    List<InvoiceHistory> getAllInvoicesByInvoiceId(final String invoiceId);
}
