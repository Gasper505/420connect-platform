package com.warehouse.core.services.invoice;

import com.fourtwenty.core.domain.models.company.Shop;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.ShippingManifest;

public interface InvoiceInventoryService {
    void processInvoiceForInventory(Shop shop, Invoice invoice, ShippingManifest shippingManifest);

    void calculateTaxes(Shop shop, Invoice invoice);

    void processInvoiceForRevertManifest(Shop shop, Invoice invoice, ShippingManifest dbShippingManifest);

    void calculateActualTaxes(Invoice invoice);
}
