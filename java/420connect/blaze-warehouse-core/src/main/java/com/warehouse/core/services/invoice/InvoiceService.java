package com.warehouse.core.services.invoice;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.rest.invoice.request.*;
import com.warehouse.core.rest.invoice.result.InvoiceInformation;
import com.warehouse.core.rest.invoice.result.InvoicePaymentResult;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.invoice.result.OutStandingResult;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface InvoiceService {
    class WholeSaleCost {
        public BigDecimal costOfCannabis = new BigDecimal(0d);
        public BigDecimal costOfNonCannabis = new BigDecimal(0d);
    }

    InvoiceResult getInvoiceById(final String invoiceId);

    Invoice createInvoice(final AddInvoiceRequest invoiceRequest);

    Invoice updateInvoice(final String invoiceId, final Invoice invoice);

    void deleteInvoiceById(final String invoiceId);

    SearchResult<InvoiceActivityLog> getInvoiceActivityLogByInvoiceId(final String invoiceId, final int start, final int limit);

    Invoice addInvoiceAttachment(final String invoiceId, final InvoiceAttachmentRequest request);

    CompanyAsset getInvoiceAttachment(final String invoiceId, final String attachmentId);

    void deleteInvoiceAttachment(final String invoiceId, final String attachmentId);

    SearchResult<InvoiceResult> getShopInvoices(final String shopId, final List<Invoice.InvoiceStatus> status, final int start, final int limit, final Invoice.InvoiceSort sortOption, final String searchTerm);

    DateSearchResult<Invoice> getCompanyInvoices(final long afterDate, final long beforeDate, final String status);

    SearchResult<Invoice> getInvoicesByProductRequestStatus(final int start, final int limit, final String productRequestStatus);

    Invoice updateAttachment(final String invoiceId, final String attachmentId, final CompanyAsset request);

    PaymentsReceived addInvoicePayment(final String invoiceId, final PaymentsReceivedAddRequest invoicePaymentRequest);

    InvoicePaymentResult getInvoicePaymentById(final String invoicePaymentId);

    SearchResult<PaymentsReceived> getAllInvoicePayments(final String invoiceId, final int start, final int limit);

    Invoice updateInvoiceStatus(final String invoiceId, final InvoiceStatusUpdateRequest updateRequest);

    void sendEmailToCustomer(final String invoiceID, final InvoiceEmailRequest emailRequest);

    SearchResult<InvoiceResult> getAllInvoiceByCustomer(final String customerId, final List<Invoice.InvoiceStatus> statusList, final int start, final int limit);

    void bulkInvoiceUpdates(InvoiceBulkUpdateRequest request);

    OutStandingResult getTotalOutStanding(final String customerId);

    SearchResult<InvoiceResult> getALLInvoicesByFetchStatus(final InvoiceFetchStatusRequest request, final int start, final int limit, final Invoice.InvoiceSort sortOption, final String searchTerm);

    Invoice getInvoiceByInvoiceNumber(final String invoiceNumber);

    Map<String, Object> calculateBalanceDue(String invoiceId, BigDecimal totalInvoiceAmount, BigDecimal amountPaid);

    Invoice prepareInvoice(AddInvoiceRequest invoiceRequest);

    InputStream createPdfForInvoice(final String invoiceId);

    DateSearchResult<InvoiceInformation> getInvoiceInformationByDate(long startDate, long endDate);

    List<BulkPostResponse> compositeInvoiceRequest(List<CompositeInvoiceUpdateRequest> request);

    PaymentsReceived updatePayment(String invoiceId, String paymentId, PaymentsReceived request);

    void deletePayment(String invoiceId, String paymentId);

    void completeInvoiceStatus(final String invoiceId, boolean active);
}
