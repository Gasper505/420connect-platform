package com.warehouse.core.services.invoice;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.rest.invoice.request.ManifestRejectRequest;
import com.warehouse.core.rest.invoice.request.ShippingManifestAddRequest;
import com.warehouse.core.rest.invoice.request.UpdateInvoiceShipmentRequest;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface ShippingManifestService {
    ShippingManifestResult addShippingManifest(final ShippingManifestAddRequest shippingManifestAddRequest);

    ShippingManifestResult getShippingManifestById(final String shippingManifestId);

    SearchResult<ShippingManifestResult> getShippingManifestByInvoiceId(final String invoiceId, final int start, final int limit);

    SearchResult<ShippingManifestResult> getAllShippingManifest(final int start, final int limit);

    ShippingManifestResult updateShippingManifest(String shippingManifestId, ShippingManifest shippingManifest);

    void deleteShippingManifest(String shippingManifestId);

    HashMap<String, Object> getInvoiceStatusByProducts(HashMap<String, BigDecimal> requestMap, Invoice dbInvoice, String shippingManifestId, boolean checkRequired);

    InputStream createPdfForShippingManifest(String shippingManifestId, final InputStream stream, final boolean printRoute, final boolean isNew);

    void sendShippingManifestEmail(final String shippingManifestId, final String email, final InputStream stream, final boolean printRoute);

    ShippingManifestResult finalizeShippingManifest(final String shippingManifestId);

    SearchResult<ShippingManifestResult> getAllShippingManifestByStatus(final String invoiceId, final ShippingManifest.ShippingManifestStatus status, final int start, final int limit);

    List<ShippingManifestResult> addShippingManifestForSales(final ShippingManifestAddRequest shippingManifestAddRequest);

    List<ShippingManifestResult> updateShippingManifestForSales(String shippingManifestId, ShippingManifest shippingManifest, ShippingManifestAddRequest shippingManifestAddRequest);

    Long countShippingManifestInProgress(String companyId, String invoiceId, ShippingManifest.ShippingManifestStatus status);

    ShippingManifestResult unfinalizeShippingManifest(final String shippingManifestId);

    ShippingManifestResult rejectShippingManifest(final String shippingManifestId, final ManifestRejectRequest request);

    ShippingManifestResult sendToMetrc(String shippingManifestId);

    ShippingManifestResult saveMetrcTemplate(String shippingManifestId, UpdateInvoiceShipmentRequest updateMetrcRequest, boolean submitPackage);
    
    void sendShippingManifestToConnectedStore(final String shippingManifestId);
    
    ShippingManifestResult getShippingManifest(String shippingManifestId);

    Boolean isAcceptedConnectedShipment(String shippingManifestId);

    Boolean isAssignedConnectedShipment(String shippingManifestId);
}
