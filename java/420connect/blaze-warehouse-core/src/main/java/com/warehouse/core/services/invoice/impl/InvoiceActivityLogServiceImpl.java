package com.warehouse.core.services.invoice.impl;

import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.impl.AuthenticationServiceImpl;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;
import com.warehouse.core.domain.repositories.invoice.InvoiceActivityLogRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.services.invoice.InvoiceActivityLogService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

public class InvoiceActivityLogServiceImpl extends AuthenticationServiceImpl implements InvoiceActivityLogService {

    private static final String ACTIVITY_LOG = "Invoice Activity Log";
    private static final String ACTIVITY_LOG_NOT_FOUND = "Invoice activity log does not found";
    private static final String INVOICE_NOT_FOUND = "Invoice not found";


    private InvoiceActivityLogRepository invoiceActivityLogRepository;
    private InvoiceRepository invoiceRepository;

    @Inject
    public InvoiceActivityLogServiceImpl(Provider<ConnectAuthToken> token,
                                         InvoiceActivityLogRepository invoiceActivityLogRepository, InvoiceRepository invoiceRepository) {
        super(token);
        this.invoiceActivityLogRepository = invoiceActivityLogRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public InvoiceActivityLog addInvoiceActivityLog(String invoiceId, String employeeId, String log) {
        if (StringUtils.isNotBlank(invoiceId)) {
            InvoiceActivityLog invoiceActivityLog = new InvoiceActivityLog();
            invoiceActivityLog.prepare(token.getCompanyId());
            invoiceActivityLog.setShopId(token.getShopId());
            invoiceActivityLog.setEmployeeId(token.getActiveTopUser().getUserId());
            invoiceActivityLog.setTargetId(invoiceId);
            invoiceActivityLog.setLog(log);
            invoiceActivityLog.setActivityType(InvoiceActivityLog.ActivityType.NORMAL_LOG);
            invoiceActivityLogRepository.save(invoiceActivityLog);
        }
        return null;
    }

    /**
     * This method is used to get all invoice activity list for comment and normal log
     *
     * @param companyId
     * @param invoiceId
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<InvoiceActivityLog> getAllInvoiceActivityLog(String companyId, String invoiceId, int start, int limit) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, INVOICE_NOT_FOUND);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return invoiceActivityLogRepository.getInvoiceActivityLogByInvoiceId(token.getCompanyId(), token.getShopId(), invoiceId, "{modified:-1}", start, limit);
    }

    /**
     * Overridden method is use for adding comments for invoice as activity log.
     *
     * @param request : request to add activity
     * @return InvoiceActivityLog
     */

    @Override
    public InvoiceActivityLog addActivityComment(ActivityCommentRequest request) {
        if (request == null || StringUtils.isBlank(request.getComment())) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, "Comment cannot be empty");
        }
        if (StringUtils.isBlank(request.getTargetId()) || !ObjectId.isValid(request.getTargetId())) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, INVOICE_NOT_FOUND);
        }

        Invoice invoice = invoiceRepository.getById(request.getTargetId());
        if (invoice == null) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, INVOICE_NOT_FOUND);
        }
        InvoiceActivityLog invoiceActivityLog = new InvoiceActivityLog();
        invoiceActivityLog.prepare(token.getCompanyId());
        invoiceActivityLog.setShopId(token.getShopId());
        invoiceActivityLog.setEmployeeId(token.getActiveTopUser().getUserId());
        invoiceActivityLog.setTargetId(request.getTargetId());
        invoiceActivityLog.setLog(request.getComment());
        invoiceActivityLog.setActivityType(InvoiceActivityLog.ActivityType.COMMENT);
        return invoiceActivityLogRepository.save(invoiceActivityLog);
    }


}
