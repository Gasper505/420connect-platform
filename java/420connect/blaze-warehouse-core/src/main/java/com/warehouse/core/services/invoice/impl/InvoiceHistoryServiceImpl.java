package com.warehouse.core.services.invoice.impl;

import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.impl.AuthenticationServiceImpl;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.InvoiceHistory;
import com.warehouse.core.domain.repositories.invoice.InvoiceHistoryRepository;
import com.warehouse.core.services.invoice.InvoiceHistoryService;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class InvoiceHistoryServiceImpl extends AuthenticationServiceImpl implements InvoiceHistoryService {

    private static final String INVOICE_HISTORY = "Invoice history";
    private static final String INVOICE_NOT_FOUND = "Invoice not found";

    private InvoiceHistoryRepository invoiceHistoryRepository;

    @Inject
    public InvoiceHistoryServiceImpl(Provider<ConnectAuthToken> token,
                                     InvoiceHistoryRepository invoiceHistoryRepository) {
        super(token);
        this.invoiceHistoryRepository = invoiceHistoryRepository;
    }

    /**
     * This method is used to add invoice history.
     *
     * @param invoiceId
     * @param employeeId
     * @return
     */
    @Override
    public InvoiceHistory addInvoiceHistory(String invoiceId, String employeeId) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE_HISTORY, INVOICE_NOT_FOUND);
        }

        InvoiceHistory invoiceHistory = new InvoiceHistory();
        invoiceHistory.prepare(token.getCompanyId());
        invoiceHistory.setShopId(token.getShopId());
        invoiceHistory.setCreatedBy(employeeId);
        invoiceHistory.setInvoiceId(invoiceId);
        return invoiceHistoryRepository.save(invoiceHistory);
    }

    /**
     * This method is used to update invoice history.
     *
     * @param invoiceId
     * @param employeeId
     * @return
     */
    @Override
    public InvoiceHistory updateInvoiceHistory(String invoiceId, String employeeId) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE_HISTORY, INVOICE_NOT_FOUND);
        }

        InvoiceHistory invoiceHistory = new InvoiceHistory();
        invoiceHistory.prepare(token.getCompanyId());
        invoiceHistory.setShopId(token.getShopId());
        invoiceHistory.setUpdatedBy(employeeId);
        invoiceHistory.setInvoiceId(invoiceId);
        return invoiceHistoryRepository.save(invoiceHistory);

    }

    /**
     * This method is used to get all invoices by invoiceId.
     *
     * @param invoiceId
     * @return
     */
    @Override
    public List<InvoiceHistory> getAllInvoicesByInvoiceId(String invoiceId) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE_HISTORY, INVOICE_NOT_FOUND);
        }

        return invoiceHistoryRepository.getAllInvociesByInvoiceId(invoiceId, token.getCompanyId(), token.getShopId());
    }
}
