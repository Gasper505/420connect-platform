package com.warehouse.core.services.invoice.impl;

import com.amazonaws.util.CollectionUtils;
import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.CustomerInfo;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.QueuedTransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.UniqueSequenceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.mgmt.CartService;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.ManifestTransactionLog;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.domain.repositories.invoice.ShippingManifestRepository;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;
import com.warehouse.core.rest.invoice.result.ShippingBatchDetails;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import com.warehouse.core.services.invoice.InvoiceInventoryService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class InvoiceInventoryServiceImpl implements InvoiceInventoryService {

    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private CartService cartService;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ShippingManifestRepository shippingManifestRepository;

    /**
     * This method process inventory for shipping manifest
     *
     * @param shop             : shop
     * @param requestInvoice   : invoice
     * @param shippingManifest : shipping manifest
     * @implNote : It follow below listed process :
     * 1. Check weather terminal exists for shop if not then create and use 'Safe' inventory for assigned inventory of terminal
     * 2. Create transaction for requested shipping manifest with transaction type 'ShippingManifest' in mehtod 'createTransactionForShippingManifest'
     * 3. Afterwords it send created transaction to queue for process inventory
     */
    @Override
    public void processInvoiceForInventory(Shop shop, Invoice requestInvoice, ShippingManifest shippingManifest) {

        Kryo kryo = new Kryo();
        Invoice invoice = kryo.copy(requestInvoice);

        //Get terminal of shop for getting inventory from which we need to deduct quantity
        SearchResult<Terminal> terminalList = terminalRepository.getAllTerminalsByState(invoice.getCompanyId(), invoice.getShopId(), true, "{create:-1}", 0, Integer.MAX_VALUE);
        Terminal terminal = null;
        if (terminalList != null && !CollectionUtils.isNullOrEmpty(terminalList.getValues())) {
            terminal = terminalList.getValues().get(0);
        }

        //If terminal not found then create a default terminal
        if (terminal == null) {
            terminal = createTerminal(invoice.getCompanyId(), invoice.getShopId(), terminal);
        }

        //Create transaction for queue
        Transaction transaction = this.createTransactionForShippingManifest(invoice, terminal, shop, shippingManifest);
        shippingManifest.setTransactionId(transaction.getId());
        shippingManifest.getManifestTransactionLog().add(prepareTransactionLog(shippingManifest.getStatus(), transaction.getId()));
        //Create queued transaction for processing inventory
        this.queueTransactionJob(invoice, invoice.getSalesPersonId(), terminal, Transaction.TransactionStatus.Completed,
                Boolean.FALSE, StringUtils.EMPTY, transaction, shippingManifest, "", QueuedTransaction.QueuedTransactionType.ShippingManifest);
    }

    /**
     * This method created queued transaction job for invoice for processing inventory
     *
     * @param invoice             : invoice
     * @param salesPersonId       : sales person's id
     * @param terminal            : terminal
     * @param pendingStatus       : pending status of invoice
     * @param transferItems       : need to transfer items or not
     * @param transferInventoryId : inventory id in which quantity need to transfer
     * @param transaction         : transaction
     * @param shippingManifest    : shipping manifest
     * @implNote : This method creates a queued transaction job that will be assigned to background job
     */
    private void queueTransactionJob(Invoice invoice, String salesPersonId, Terminal terminal, Transaction.TransactionStatus pendingStatus,
                                     boolean transferItems, String transferInventoryId, Transaction transaction, ShippingManifest shippingManifest, String requestInventoryId, QueuedTransaction.QueuedTransactionType status) {

        //If transaction is not created then don't process it
        if (transaction == null) {
            return;
        }

        QueuedTransaction queuedTransaction = new QueuedTransaction();

        queuedTransaction.prepare(invoice.getCompanyId());
        queuedTransaction.setShopId(invoice.getShopId());
        queuedTransaction.setCart(transaction.getCart());
        queuedTransaction.setPendingStatus(pendingStatus);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(status);
        queuedTransaction.setSellerId(salesPersonId);
        queuedTransaction.setTerminalId(terminal.getId());
        queuedTransaction.setInvoiceId(invoice.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setTransactionId(transaction.getId());
        queuedTransaction.setReassignTransfer(transferItems);
        queuedTransaction.setNewTransferInventoryId(transferInventoryId);
        queuedTransaction.setShippingManifestId(shippingManifest.getId());
        queuedTransaction.setInventoryId(requestInventoryId);
        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(invoice.getCompanyId(), invoice.getShopId(), queuedTransaction.getId());
    }

    /**
     * This method create transaction for invoice to process inventory updates
     *
     * @param invoice          : invoice object
     * @param terminal         : terminal object
     * @param shop             : shop
     * @param shippingManifest : shipping manifest
     */
    private Transaction createTransactionForShippingManifest(Invoice invoice, Terminal terminal, Shop shop, ShippingManifest shippingManifest) {

        if (shippingManifest == null || CollectionUtils.isNullOrEmpty(shippingManifest.getProductMetrcInfo())) {
            return null;
        }

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(invoice.getCompanyId(), invoice.getShopId(), "TransactionPriority");

        Transaction transaction = new Transaction();

        Cart cart = new Cart();
        cart.prepare(invoice.getCompanyId());

        List<OrderItem> orderItems = new ArrayList<>();
        //Create order item from shipping manifest's product
        List<ProductMetrcInfo> productMetrcInfo = shippingManifest.getProductMetrcInfo();
        if (!CollectionUtils.isNullOrEmpty(productMetrcInfo)) {
            for (ProductMetrcInfo info : productMetrcInfo) {
                OrderItem invoiceOrderItem = null;
                for (OrderItem orderItem : invoice.getCart().getItems()) {
                    if (orderItem.getId().equals(info.getOrderItemId())) {
                        invoiceOrderItem = orderItem;
                        break;
                    }
                }

                //If order item does not found then continue
                if (invoiceOrderItem == null) {
                    continue;
                }

                if (!CollectionUtils.isNullOrEmpty(info.getBatchDetails())) {
                    for (ShippingBatchDetails batchDetails : info.getBatchDetails()) {
                        OrderItem item = this.createShippingManifestOrderItem(invoiceOrderItem, batchDetails.getQuantity(), batchDetails.getBatchId(), batchDetails.getPrepackageItemId(), batchDetails.getOverrideInventoryId());
                        orderItems.add(item);
                    }
                } else {
                    OrderItem item = this.createShippingManifestOrderItem(invoiceOrderItem, info.getQuantity(), null, null, null);
                    orderItems.add(item);
                }
            }
        }

        cart.setItems(orderItems);

        transaction.prepare(invoice.getCompanyId());
        transaction.setShopId(invoice.getShopId());
        transaction.setActive(Boolean.TRUE);
        transaction.setTransNo("" + sequence.getCount());
        transaction.setCart(cart);
        transaction.setQueueType(Transaction.QueueType.Delivery);
        transaction.setTransType(Transaction.TransactionType.ShippingManifest);
        transaction.setCheckinTime(invoice.getCreated());
        transaction.setSellerId(invoice.getSalesPersonId());
        transaction.setCreatedById(invoice.getSalesPersonId());
        transaction.setPriority(sequence.getCount());
        transaction.setFulfillmentStep(Transaction.FulfillmentStep.Prepare);
        transaction.setTerminalId(terminal.getId());
        transaction.setPreparedBy(invoice.getSalesPersonId());

        invoice.setCart(cart);

        this.calculateTaxes(shop, invoice);
        transaction.setCart(invoice.getCart());
        return transactionRepository.save(transaction);
    }

    /**
     * This method creates order item for shipping manifest
     *
     * @param invoiceOrderItem    : invoice order item (Original order item)
     * @param quantity            : shipping manifest quantity
     * @param batchId             : Batch id
     * @param prepackageItemId    : Prepackage product item id
     * @param overrideInventoryId : override inventoryId
     */
    private OrderItem createShippingManifestOrderItem(OrderItem invoiceOrderItem, BigDecimal quantity, String batchId, String prepackageItemId, String overrideInventoryId) {

        OrderItem item = new OrderItem();

        double finalPrice = invoiceOrderItem.getOverridePrice().doubleValue() * quantity.doubleValue();

        item.prepare(invoiceOrderItem.getCompanyId());
        item.setQuantity(quantity);
        item.setOverridePrice(invoiceOrderItem.getOverridePrice());
        item.setFinalPrice(new BigDecimal(finalPrice));
        item.setDiscount(invoiceOrderItem.getDiscount());
        item.setDiscountType(invoiceOrderItem.getDiscountType());
        item.setUnitQty(invoiceOrderItem.getUnitQty());
        item.setUseUnitQty(invoiceOrderItem.isUseUnitQty());
        item.setWeightKey(invoiceOrderItem.getWeightKey());
        item.setOrigQuantity(quantity);
        item.setDiscountedQty(invoiceOrderItem.getDiscountedQty());
        item.setProductId(invoiceOrderItem.getProductId());
        item.setStatus(OrderItem.OrderItemStatus.Active);
        item.setBatchId(batchId);
        item.setPrepackageItemId(prepackageItemId);
        item.setInventoryId(overrideInventoryId);
        item.setOrigOrderItemId(invoiceOrderItem.getId()); //use orderItemId for invoice's order item
        return item;
    }

    /**
     * This method is used to calculate excise taxes for invoices according to vendor arms length type
     *
     * @param shop    : shop object
     * @param invoice : invoice object
     */
    @Override
    public void calculateTaxes(Shop shop, Invoice invoice) {
        Vendor vendor = vendorRepository.get(invoice.getCompanyId(), invoice.getCustomerId());
        Transaction transaction = new Transaction();
        transaction.setCart(invoice.getCart());
        Transaction.QueueType queueType = Transaction.QueueType.Delivery;
        transaction.setQueueType(queueType);
//        resetDiscounts(invoice.getCart());

        // get company license from selected invoice license id
        CompanyLicense dbCompanyLicense = vendor.getCompanyLicense(invoice.getLicenseId());

        // update customer info and company type
        CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.setArmsLengthType(vendor.getArmsLengthType());
        customerInfo.setCompanyType(dbCompanyLicense.getCompanyType());
        customerInfo.setEnableCultivationTax((shop.isGrow()));
        cartService.prepareCart(shop, transaction, true, Transaction.TransactionStatus.InProgress, true, customerInfo, false);
        Cart cart = transaction.getCart();

        // Update the Invoice cart
        resetTaxes(cart, customerInfo, shop);

        invoice.setCart(cart);
        invoice.setTotal(cart.getSubTotalDiscount());
        invoice.setDeliveryCharges(cart.getDeliveryFee());
        invoice.setTotalInvoiceAmount(cart.getTotal());
        invoice.getCart().setCashReceived(BigDecimal.ZERO);
        if (invoice.getCart().getTotal().doubleValue() <= 0) {
            invoice.setInvoicePaymentStatus(Invoice.PaymentStatus.PAID);
        }
        prepareOrderItem(invoice);
    }

    /**
     * Private method to update calculated invoice taxes according to vendor's selected license.
     *  @param cart         : cart
     * @param customerInfo : customer info
     * @param shop
     */
    private void resetTaxes(Cart cart, CustomerInfo customerInfo, Shop shop) {
        TaxResult taxResult = new TaxResult();
        taxResult.prepare();
        TaxResult cartTaxResult = cart.getTaxResult();

        taxResult.setCultivationTaxResult((shop.isGrow() ? cartTaxResult.getCultivationTaxResult() : new CultivationTaxResult()));

        BigDecimal totalTax = BigDecimal.ZERO;
        if (cart.getItems() != null) {
            for (OrderItem orderItem : cart.getItems()) {
                if (orderItem != null) {
                    this.resetOrderItemTaxResult(orderItem, customerInfo);
                    totalTax = totalTax.add(orderItem.getCalcTax());
                }
            }
        }
        BigDecimal taxes = BigDecimal.ZERO;

        if ((Vendor.CompanyType.RETAILER == customerInfo.getCompanyType() && Vendor.ArmsLengthType.ARMS_LENGTH == customerInfo.getArmsLengthType())
            || customerInfo.isEnableCultivationTax()) {
            taxes = taxes.add(cartTaxResult.getTotalCountyTax());
            taxes = taxes.add(cartTaxResult.getTotalCityTax());
            taxes = taxes.add(cartTaxResult.getTotalFedTax());
            taxes = taxes.add(cartTaxResult.getTotalStateTax());
            taxes = taxes.add(cartTaxResult.getTotalExciseTax());
            taxResult.setTotalPostCalcTax(totalTax);
            taxResult.setTotalALExciseTax(cartTaxResult.getTotalALExciseTax());
            taxResult.setTotalALPostExciseTax(cartTaxResult.getTotalALPostExciseTax());
            cart.setTotalALExciseTax(cartTaxResult.getTotalALPostExciseTax());
            cart.setTotalExciseTax(BigDecimal.ZERO);
        } else {
            taxes = cart.getTotalCalcTax().subtract(cart.getTotalPreCalcTax());
            cart.setTotalALExciseTax(BigDecimal.ZERO);
            cart.setTotalExciseTax(BigDecimal.ZERO);
        }

        BigDecimal total = cart.getTotal().subtract(taxes);
        cart.setTotal(total);
        cart.setTaxResult(taxResult);
        cart.setTax(BigDecimal.ZERO);
        cart.setTotalCalcTax(taxResult.getTotalPostCalcTax());
        cart.setTotalPreCalcTax(BigDecimal.ZERO);
    }

    private void resetOrderItemTaxResult(OrderItem orderItem, CustomerInfo customerInfo) {
        TaxResult orderItemTaxResult = orderItem.getTaxResult();
        if (orderItemTaxResult != null) {
            TaxResult taxResult = new TaxResult();
            taxResult.prepare();
            taxResult.setCultivationTaxResult(orderItemTaxResult.getCultivationTaxResult());

            BigDecimal itemCultivationTax = taxResult.getCultivationTaxResult() != null ? taxResult.getCultivationTaxResult().getTotalCultivationTax() : BigDecimal.ZERO;
            BigDecimal totalExciseTax = BigDecimal.ZERO;
            if ((Vendor.CompanyType.RETAILER == customerInfo.getCompanyType() && Vendor.ArmsLengthType.ARMS_LENGTH == customerInfo.getArmsLengthType())
             || customerInfo.isEnableCultivationTax()) {
                totalExciseTax = orderItemTaxResult.getTotalALPostExciseTax();
                taxResult.setTotalPostCalcTax(totalExciseTax.add(itemCultivationTax));
                taxResult.setTotalALExciseTax(orderItemTaxResult.getTotalALExciseTax());
                taxResult.setTotalALPostExciseTax(orderItemTaxResult.getTotalALPostExciseTax());
            }
            orderItem.setCalcTax(taxResult.getTotalPostCalcTax());
            orderItem.setExciseTax(totalExciseTax);
            orderItem.setCalcCultivationTax(itemCultivationTax);
            orderItem.setTaxResult(taxResult);
        }
    }

    private void resetDiscounts(Cart cart) {
        if (cart != null) {
            cart.setDiscount(new BigDecimal(0));
            cart.setDiscountType(OrderItem.DiscountType.Cash);
        }
    }

    private void prepareOrderItem(Invoice invoice) {
        List<ObjectId> prepackageIds = new ArrayList<>();
        if (invoice != null && invoice.getCart() != null && invoice.getCart().getItems() != null && !invoice.getCart().getItems().isEmpty()) {
            for (OrderItem item : invoice.getCart().getItems()) {
                if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                    prepackageIds.add(new ObjectId(item.getPrepackageId()));
                }
            }
            HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(invoice.getCompanyId(), prepackageIds);
            for (OrderItem item : invoice.getCart().getItems()) {
                item.setPrepackage(prepackageHashMap.get(item.getPrepackageId()));
            }

        }
    }

    /**
     * Override method to process invoice for revert of manifest
     * @param shop             : shop
     * @param invoice          : invoice
     * @param shippingManifest : manifest
     */
    @Override
    public void processInvoiceForRevertManifest(Shop shop, Invoice invoice, ShippingManifest shippingManifest) {


        //Get terminal of shop for getting inventory from which we need to deduct quantity
        SearchResult<Terminal> terminalList = terminalRepository.getAllTerminalsByState(invoice.getCompanyId(), invoice.getShopId(), true, "{create:-1}", 0, Integer.MAX_VALUE);
        Terminal terminal = null;
        if (terminalList != null && !CollectionUtils.isNullOrEmpty(terminalList.getValues())) {
            terminal = terminalList.getValues().get(0);
        }

        //If terminal not found then create a default terminal
        if (terminal == null) {
            terminal = createTerminal(invoice.getCompanyId(), invoice.getShopId(), terminal);
        }

        Transaction transaction = transactionRepository.getByShopAndId(shop.getCompanyId(), shop.getId(), shippingManifest.getTransactionId());
        if (transaction == null) {
            throw new BlazeInvalidArgException("Shipping Manifest", "Transaction not found for manifest.");
        }

        //Create transaction for queue
        transaction = this.createTransactionForRevertShippingManifest(transaction, terminal, shippingManifest.getRejectInventoryId());
        shippingManifest.setTransactionId(transaction.getId());
        shippingManifest.getManifestTransactionLog().add(prepareTransactionLog(shippingManifest.getStatus(), transaction.getId()));
        //Create queued transaction for processing inventory
        QueuedTransaction.QueuedTransactionType status = shippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.InProgress ? QueuedTransaction.QueuedTransactionType.RevertShippingManifest : QueuedTransaction.QueuedTransactionType.RejectShippingManifest;
        this.queueTransactionJob(invoice, invoice.getSalesPersonId(), terminal, Transaction.TransactionStatus.RefundWithInventory,
                Boolean.FALSE, StringUtils.EMPTY, transaction, shippingManifest, shippingManifest.getRejectInventoryId(), status);
    }

    /**
     * Private method to create terminal and inventory if not found
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param terminal  : terminal
     * @return : terminal
     */
    private Terminal createTerminal(String companyId, String shopId, Terminal terminal) {
        Inventory safeInventory = inventoryRepository.getInventory(companyId, shopId, Inventory.SAFE);

        if (safeInventory == null) {
            safeInventory = new Inventory();

            safeInventory.prepare(companyId);
            safeInventory.setShopId(shopId);
            safeInventory.setActive(Boolean.TRUE);
            safeInventory.setName(Inventory.SAFE);
            safeInventory.setType(Inventory.InventoryType.Storage);

            inventoryRepository.save(safeInventory);
        }

        terminal = new Terminal();
        terminal.setShopId(shopId);
        terminal.setName("Default Terminal");
        terminal.setActive(true);
        terminal.prepare(companyId);
        terminal.setDeviceType("iOS");
        terminal.setDeviceId(UUID.randomUUID().toString());
        terminal.setAssignedInventoryId(safeInventory.getId());

        return terminalRepository.save(terminal);
    }

    /**
     * Private method to create transaction from original manifest transaction
     *
     * @param manifestTransaction : manifest transaction
     * @param terminal            : terminal
     * @param requestInventoryId  : request transaction
     * @return
     */
    private Transaction createTransactionForRevertShippingManifest(Transaction manifestTransaction, Terminal terminal, String requestInventoryId) {
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(manifestTransaction.getCompanyId(), manifestTransaction.getShopId(), "TransactionPriority");

        Transaction transaction = new Kryo().copy(manifestTransaction);

        transaction.resetPrepare(manifestTransaction.getCompanyId());
        transaction.setActive(Boolean.TRUE);
        transaction.setTransNo("" + sequence.getCount());
        transaction.setQueueType(Transaction.QueueType.Delivery);
        transaction.setTransType(Transaction.TransactionType.ShippingManifest);
        transaction.setPriority(sequence.getCount());
        transaction.setFulfillmentStep(Transaction.FulfillmentStep.Prepare);
        transaction.setTerminalId(terminal.getId());
        transaction.setStatus(Transaction.TransactionStatus.RefundWithInventory);
        transaction.setOverrideInventoryId(requestInventoryId);
        transaction.getCart().getItems().forEach(orderItem -> {
            orderItem.resetPrepare(transaction.getCompanyId());
            orderItem.setOrderItemId(UUID.randomUUID().toString());
        });
        return transactionRepository.save(transaction);
    }

    /**
     * Private method to prepare manifest transaction log
     * @param status         : status
     * @param transactionId: transactionId
     * @return : manifest log
     */

    private ManifestTransactionLog prepareTransactionLog(ShippingManifest.ShippingManifestStatus status, String transactionId) {
        ManifestTransactionLog log = new ManifestTransactionLog();
        log.prepare();
        log.setStatus(status);
        log.setTransactionId(transactionId);
        return log;
    }

    public void calculateActualTaxes(Invoice invoice) {
        HashMap<String, OrderItem> invoiceItemMap = new HashMap<>();
        HashMap<String, TaxResult> itemActualTaxMap = new HashMap<>();
        invoice.getCart().getItems().forEach(orderItem -> {
            invoiceItemMap.put(orderItem.getId(), orderItem);
        });

        SearchResult<ShippingManifestResult> shippingManifests = shippingManifestRepository.getShippingManifestByInvoiceId(invoice.getCompanyId(), invoice.getId(), "", 0, Integer.MAX_VALUE);
        List<ObjectId> objectIds = new ArrayList<>();
        shippingManifests.getValues().forEach(shippingManifestResult -> {
            if (StringUtils.isNotBlank(shippingManifestResult.getTransactionId()) && ObjectId.isValid(shippingManifestResult.getTransactionId())) {
                objectIds.add(new ObjectId(shippingManifestResult.getTransactionId()));
            }
        });
        HashMap<String, Transaction> transactionHashMap = transactionRepository.listAsMap(invoice.getCompanyId(), objectIds);
        TaxResult invoiceTaxResult = new TaxResult();

        for (Transaction transaction : transactionHashMap.values()) {
            transaction.getCart().getItems().forEach(orderItem -> {
                OrderItem invoiceItem = invoiceItemMap.get(orderItem.getOrigOrderItemId());
                if (invoiceItem == null) {
                    return;
                }
                itemActualTaxMap.putIfAbsent(orderItem.getOrigOrderItemId(), new TaxResult());
                TaxResult actualItemtax = itemActualTaxMap.get(orderItem.getOrigOrderItemId());
                if (actualItemtax.getCultivationTaxResult() == null) {
                    actualItemtax.setCultivationTaxResult(new CultivationTaxResult());
                }

                actualItemtax.prepare();

                if (orderItem.getTaxResult() != null) {
                    TaxResult taxResult = orderItem.getTaxResult();
                    if (taxResult.getCultivationTaxResult() == null) {
                        taxResult.setCultivationTaxResult(new CultivationTaxResult());
                    }
                    actualItemtax.addTaxes(taxResult);
                    invoiceItem.setCalcCultivationTax(orderItem.getCalcCultivationTax().add(invoiceItem.getCalcCultivationTax()));
                    invoiceItem.setCalcTax(orderItem.getCalcTax().add(invoiceItem.getCalcTax()));
                    if (invoiceTaxResult.getCultivationTaxResult() == null) {
                        invoiceTaxResult.setCultivationTaxResult(new CultivationTaxResult());
                    }
                    invoiceTaxResult.addTaxes(taxResult);
                }

            });
        }

        invoice.getCart().getItems().forEach(orderItem -> {
            OrderItem item = invoiceItemMap.get(orderItem.getId());
            if (item == null) {
                return;
            }
            TaxResult taxResult = itemActualTaxMap.get(orderItem.getId());
            if (taxResult == null) {
                return;
            }
            item.setTaxResult(taxResult);
        });
        BigDecimal newTotal = invoice.getCart().getTotal().subtract(invoice.getCart().getTaxResult().getTotalPostCalcTax()).add(invoiceTaxResult.getTotalPostCalcTax());
        invoice.getCart().setTaxResult(invoiceTaxResult);
        invoice.getCart().setTotalCalcTax(invoiceTaxResult.getTotalPostCalcTax());
        invoice.getCart().setTotal(newTotal);
        invoice.setTotal(newTotal);
    }

}
