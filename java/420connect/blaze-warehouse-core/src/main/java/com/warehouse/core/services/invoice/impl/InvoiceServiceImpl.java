package com.warehouse.core.services.invoice.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.AdjustmentInfo;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.services.mgmt.VendorService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.QrCodeUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.*;
import com.warehouse.core.domain.repositories.invoice.*;
import com.warehouse.core.rest.invoice.request.*;
import com.warehouse.core.rest.invoice.result.*;
import com.warehouse.core.services.invoice.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;

public class InvoiceServiceImpl extends AbstractAuthServiceImpl implements InvoiceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceServiceImpl.class);
    private static final String INVOICE_NOT_FOUND = "Invoice is not found.";
    private static final String ATTACHMENT_NOT_FOUND = "Error! Attachment does not exist.";
    private static final String INVOICE_PAYMENT_NOT_FOUND = "Invoice Payment is not found.";
    private static final String QR_CODE_ERROR = "Error while creating QR code";
    private static final String INVOICE = "Invoice";
    private static final String ATTACHMENT = "Attachment";
    private static final String BULK_INVOICE_UPDATE = "Bulk Invoice Update";
    private static final String CUSTOMER_COMPANY = "Customer company";
    private static final String CART = "Cart";
    private static final String INVOICE_HTML_RESOURCE = "/invoice.html";
    private static final String NOT_CUSTOM_DATE = "Please provide custom date.";
    private static final String INVOICE_NOT_ALLOWED_FOR_VENDOR = "Invoice not allowed for Vendors";
    private static final String PAYMENT_NOT_ALLOWED_FOR_DRAFT = "Payment not allowed for draft or cancelled invoices";
    private static final String PAYMENT_TYPE_NOT_FOUND = "Invoice payment type does not exist.";
    private static final String ITEM_NOT_ALLOWED_FOR_REMOVE = "Invoice Item not allowed for remove";
    private static final String PAYMENT = "Invoice Payment";
    private static final String PAYMENT_ALREADY_DELETED = "Payment is already deleted";
    private static final String NOT_VALID_ADJUSTMENT_AMOUNT = "Please enter valid adjustment amount";
    private static final String CUSTOMER_LICENSE_EXPIRED = "Company license has been expired";

    private InvoiceRepository invoiceRepository;
    private InvoiceActivityLogRepository invoiceActivityLogRepository;
    private EmployeeService employeeService;
    private ProductRepository productRepository;
    private InvoiceActivityLogService invoiceActivityLogService;
    private InvoiceHistoryService invoiceHistoryService;
    private InvoicePaymentsRepository invoicePaymentsRepository;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private CompanyRepository companyRepository;
    private ShopRepository shopRepository;
    private CartService cartService;
    private VendorRepository vendorRepository;
    private AmazonS3Service amazonS3Service;
    private ShippingManifestService shippingManifestService;
    private CompanyContactRepository companyContactRepository;
    private ElasticSearchManager elasticSearchManager;
    private ShippingManifestRepository shippingManifestRepository;
    private CompanyAssetRepository companyAssetRepository;
    private EmployeeRepository employeeRepository;
    private InvoiceHistoryRepository invoiceHistoryRepository;
    private InvoiceInventoryService invoiceInventoryService;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private AdjustmentRepository adjustmentRepository;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;
    @Inject
    private VendorService vendorService;

    @Inject
    public InvoiceServiceImpl(Provider<ConnectAuthToken> token, InvoiceRepository invoiceRepository,
                              InvoiceActivityLogRepository invoiceActivityLogRepository,
                              EmployeeService employeeService,
                              ProductRepository productRepository,
                              InvoiceActivityLogService invoiceActivityLogService,
                              InvoiceHistoryService invoiceHistoryService,
                              InvoicePaymentsRepository invoicePaymentsRepository,
                              AmazonServiceManager amazonServiceManager,
                              ConnectConfiguration connectConfiguration,
                              ShopRepository shopRepository,
                              CompanyRepository companyRepository,
                              VendorRepository vendorRepository,
                              CartService cartService,
                              AmazonS3Service amazonS3Service,
                              ShippingManifestService shippingManifestService,
                              CompanyContactRepository companyContactRepository,
                              ShippingManifestRepository shippingManifestRepository,
                              ElasticSearchManager elasticSearchManager,
                              CompanyAssetRepository companyAssetRepository,
                              EmployeeRepository employeeRepository,
                              InvoiceHistoryRepository invoiceHistoryRepository,
                              InvoiceInventoryService invoiceInventoryService) {

        super(token);
        this.invoiceRepository = invoiceRepository;
        this.invoiceActivityLogRepository = invoiceActivityLogRepository;
        this.employeeService = employeeService;
        this.productRepository = productRepository;
        this.invoiceActivityLogService = invoiceActivityLogService;
        this.invoiceHistoryService = invoiceHistoryService;
        this.invoicePaymentsRepository = invoicePaymentsRepository;
        this.amazonServiceManager = amazonServiceManager;
        this.connectConfiguration = connectConfiguration;
        this.shopRepository = shopRepository;
        this.companyRepository = companyRepository;
        this.vendorRepository = vendorRepository;
        this.cartService = cartService;
        this.amazonS3Service = amazonS3Service;
        this.shippingManifestService = shippingManifestService;
        this.companyContactRepository = companyContactRepository;
        this.shippingManifestRepository = shippingManifestRepository;
        this.companyAssetRepository = companyAssetRepository;
        this.elasticSearchManager = elasticSearchManager;
        this.employeeRepository = employeeRepository;
        this.invoiceHistoryRepository = invoiceHistoryRepository;
        this.invoiceInventoryService = invoiceInventoryService;
    }

    /**
     * This method is used to get an invoice according to its id.
     *
     * @param invoiceId
     * @return
     */
    @Override
    public InvoiceResult getInvoiceById(String invoiceId) {

        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        InvoiceResult invoiceResult = invoiceRepository.get(token.getCompanyId(), invoiceId, InvoiceResult.class);
        if (invoiceResult == null) {
            LOGGER.debug("Exception in Invoice", INVOICE_NOT_FOUND);
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        prepareInvoiceResult(invoiceResult);
        return invoiceResult;

    }

    private void prepareInvoiceResult(InvoiceResult invoiceResult) {

        Employee employee;
        Vendor customerCompany;
        try {

            Company company = companyRepository.getById(invoiceResult.getCompanyId());
            String logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
            invoiceResult.setCompanyLogo(logoURL);

            if (StringUtils.isNotBlank(invoiceResult.getSalesPersonId()) && ObjectId.isValid(invoiceResult.getSalesPersonId())) {
                employee = employeeRepository.getEmployeeById(invoiceResult.getSalesPersonId(), token.getCompanyId(), "", Employee.class);
                if (employee != null) {
                    invoiceResult.setEmployeeName(employee.getFirstName() + " " + employee.getLastName());
                }
            }
            if (StringUtils.isNotBlank(invoiceResult.getCustomerId())) {
                customerCompany = vendorRepository.getById(invoiceResult.getCustomerId());
                if (customerCompany != null) {
                    invoiceResult.setCustomerName(customerCompany.getName());
                    invoiceResult.setVendor(customerCompany);
                }
            }
            if (StringUtils.isNotBlank(invoiceResult.getCompanyContactId())) {
                CompanyContact companyContact = companyContactRepository.getById(invoiceResult.getCompanyContactId());
                if (companyContact != null) {
                    invoiceResult.setCompanyContact(companyContact);
                }
            }

            List<InvoiceHistory> invoiceHistoryList = invoiceHistoryService.getAllInvoicesByInvoiceId(invoiceResult.getId());
            if (!invoiceHistoryList.isEmpty()) {
                invoiceResult.setInvoiceHistoryList(invoiceHistoryList);
            }
            SearchResult<InvoiceActivityLog> activityLogs = invoiceActivityLogService.getAllInvoiceActivityLog(token.getCompanyId(), invoiceResult.getId(), 0, Integer.MAX_VALUE);
            if (activityLogs != null && activityLogs.getValues() != null && !activityLogs.getValues().isEmpty()) {
                invoiceResult.setInvoiceActivityLogs(activityLogs.getValues());
            }

            BigDecimal amountPaid = new BigDecimal(0);
            BigDecimal balanceDue = new BigDecimal(0);
            BigDecimal changeDue = new BigDecimal(0);
            BigDecimal totalInvoiceAmount = new BigDecimal(0);
            if (invoiceResult.getCart() != null && invoiceResult.getCart().getTotal() != null) {
                totalInvoiceAmount = invoiceResult.getCart().getTotal();
            }
            Map<String, Object> returnMapInvoice = calculateBalanceDue(invoiceResult.getId(), totalInvoiceAmount, amountPaid);
            balanceDue = (BigDecimal) returnMapInvoice.get("balanceDue");
            amountPaid = (BigDecimal) returnMapInvoice.get("amountPaid");
            changeDue = (BigDecimal) returnMapInvoice.get("changeDue");
            if (invoiceResult.getCart() != null) {
                invoiceResult.getCart().setBalanceDue(balanceDue);
                invoiceResult.getCart().setChangeDue(changeDue);
                invoiceResult.getCart().setCashReceived(amountPaid);
            }
            SearchResult<ShippingManifestResult> shippingManifestSearchResults = shippingManifestService.getShippingManifestByInvoiceId(invoiceResult.getId(), 0, Integer.MAX_VALUE);
            if (shippingManifestSearchResults != null && shippingManifestSearchResults.getValues() != null) {
                invoiceResult.setShippingManifests(shippingManifestSearchResults.getValues());
                HashMap<String, Object> returnMap = shippingManifestService.getInvoiceStatusByProducts(new HashMap<>(), invoiceResult, "", false);
                HashMap<String, BigDecimal> remainingProductMap = (HashMap<String, BigDecimal>) returnMap.get("remainingProduct");
                invoiceResult.setRemainingProductMap(remainingProductMap);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.debug("Exception in prepareInvoiceResult", ex.getCause());
        }
    }

    private void prepareProductRequest(List<ProductRequestResult> productRequestResults, List<OrderItem> productRequests) {

        Product product;
        ProductRequestResult productRequestResult;
        for (OrderItem productRequest : productRequests) {
            if (!productRequest.getProductId().isEmpty()) {
                product = productRepository.get(token.getCompanyId(), productRequest.getProductId());
                if (product != null) {
                    productRequestResult = new ProductRequestResult();
                    productRequestResult.setProductName(product.getName());
                    if (!productRequest.getStatus().toString().isEmpty()) {
                        if ("Active".equalsIgnoreCase(productRequest.getStatus().toString())) {
                            productRequestResult.setStatus(OrderItem.OrderItemStatus.Active);
                        }
                    }
                    productRequestResult.setProductId(product.getId());
                    productRequestResult.setQuantity(productRequest.getQuantity());
                    productRequestResult.setOrigQuantity(productRequest.getOrigQuantity());
                    BigDecimal totalCost = productRequest.getQuantity().multiply(productRequest.getUnitPrice());
                    productRequestResult.setUnitPrice(productRequest.getUnitPrice());
                    productRequestResult.setCost(totalCost);
                    productRequestResult.setDiscount(productRequest.getDiscount());
                    productRequestResults.add(productRequestResult);
                }
            }
        }

    }

    private WholeSaleCost preparePRForWholeSaleCost(List<OrderItem> requestProductRequest) {

        Product product;
        WholeSaleCost wholeSaleCost = new WholeSaleCost();
        BigDecimal amount;
        for (OrderItem productRequest : requestProductRequest) {
            if (!productRequest.getProductId().isEmpty()) {
                product = productRepository.get(token.getCompanyId(), productRequest.getProductId());
                if (product != null) {
                    if (Product.CannabisType.NON_CANNABIS.equals(product.getCannabisType())) {
                        amount = productRequest.getQuantity().multiply(product.getUnitPrice());
                        if (productRequest.getDiscount().compareTo(BigDecimal.ZERO) > 0 && amount.compareTo(BigDecimal.ZERO) > 0) {
                            amount = amount.subtract(productRequest.getDiscount());
                        }

                        wholeSaleCost.costOfNonCannabis = wholeSaleCost.costOfNonCannabis.add(amount);
                    } else {
                        amount = productRequest.getQuantity().multiply(product.getUnitPrice());
                        if (productRequest.getDiscount().compareTo(BigDecimal.ZERO) > 0 && amount.compareTo(BigDecimal.ZERO) > 0) {
                            amount = amount.subtract(productRequest.getDiscount());
                        }
                        wholeSaleCost.costOfCannabis = wholeSaleCost.costOfCannabis.add(amount);
                    }
                }
            }
        }
        return wholeSaleCost;
    }


    /**
     * This method is used to update an complete invoice.
     *
     * @param invoice
     * @return
     */
    @Override
    public Invoice updateInvoice(String invoiceId, Invoice invoice) {
        Invoice dbInvoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (dbInvoice == null) {
            LOGGER.debug("Exception in Invoice", INVOICE_NOT_FOUND);
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        if (StringUtils.isNotBlank(invoice.getSalesPersonId())) {
            Employee employee = employeeService.getEmployeeById(invoice.getSalesPersonId());
            if (employee == null) {
                throw new BlazeInvalidArgException("Sales Person", "Sales person not found");
            }
        }
        Vendor vendor;
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && StringUtils.isBlank(invoice.getCustomerId())) {
            vendor = vendorService.getDefaultVendor(token.getCompanyId());
            invoice.setCustomerId(vendor.getId());
        } else {
            if (StringUtils.isBlank(invoice.getCustomerId())) {
                throw new BlazeInvalidArgException(INVOICE, "Customer can't be blank.");
            }
            // Check if vendor exist
            vendor = vendorRepository.get(token.getCompanyId(), invoice.getCustomerId());
            if (vendor == null) {
                throw new BlazeInvalidArgException("Vendor", "Vendor  not exist");
            }
        }
        Vendor customerCompany = vendorRepository.get(token.getCompanyId(), invoice.getCustomerId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "Please choose a valid customer");
        }
        if (Vendor.VendorType.VENDOR.equals(customerCompany.getVendorType())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, INVOICE_NOT_ALLOWED_FOR_VENDOR);
        }


        if (invoice.getCart() == null) {
            throw new BlazeInvalidArgException(CART, "Cart can't be empty");
        }
        if (invoice.getCart().getItems() == null && invoice.getCart().getItems().isEmpty()) {
            throw new BlazeInvalidArgException(CART, "Products can't be empty in Cart");
        }
        if (Invoice.InvoiceStatus.COMPLETED.equals(invoice.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice status can't be completed, until Shipping Manifest is not done");
        }
        if (invoice.getInvoiceTerms() == null) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice term cannot be blank");
        }
        if (Invoice.InvoiceTerms.CUSTOM_DATE.equals(invoice.getInvoiceTerms()) && (invoice.getDueDate() == null || invoice.getDueDate() == 0)) {
            throw new BlazeInvalidArgException(INVOICE, NOT_CUSTOM_DATE);
        }

        CompanyLicense dbCompanyLicense = customerCompany.getCompanyLicense(invoice.getLicenseId());

        if (dbCompanyLicense == null) {
            throw new BlazeInvalidArgException(INVOICE, "Company license not found");
        }

        dbInvoice.setCustomerId(invoice.getCustomerId());
        dbInvoice.setLicenseId(invoice.getLicenseId());
        dbInvoice.setOrderNumber(invoice.getOrderNumber());
        dbInvoice.setInvoiceTerms(invoice.getInvoiceTerms());
        dbInvoice.setSalesPersonId(invoice.getSalesPersonId());
        if (StringUtils.isBlank(invoice.getSalesPersonId())) {
            dbInvoice.setSalesPersonId(token.getActiveTopUser().getUserId());
        }
        dbInvoice.setActive(true);
        if (invoice.getAttachments() != null && !invoice.getAttachments().isEmpty()) {
            List<CompanyAsset> companyAssetList = invoice.getAttachments();
            List<CompanyAsset> newCompanyAssetList = new ArrayList<>();
            for (CompanyAsset companyAsset : companyAssetList) {
                companyAsset.prepare(token.getCompanyId());
                newCompanyAssetList.add(companyAsset);
            }
            dbInvoice.setAttachments(newCompanyAssetList);
        } else {
            dbInvoice.setAttachments(new ArrayList<>());
        }

        dbInvoice.setDueDate(invoice.getDueDate());
        dbInvoice.setInvoiceDate(invoice.getInvoiceDate());
        updateInvoiceDueDate(dbInvoice);

        dbInvoice.setTermsAndconditions(invoice.getTermsAndconditions());
        dbInvoice.setNotes(invoice.getNotes());

        dbInvoice.setCompanyContactId(invoice.getCompanyContactId());

        dbInvoice.setEstimatedDeliveryDate(invoice.getEstimatedDeliveryDate());
        dbInvoice.setEstimatedDeliveryTime(invoice.getEstimatedDeliveryTime());
        dbInvoice.setDeliveryCharges(invoice.getDeliveryCharges());
        dbInvoice.setMarkup(invoice.getMarkup());
        dbInvoice.setTotalInvoiceAmount(invoice.getTotalInvoiceAmount());
        if (Invoice.InvoiceStatus.SENT.equals(invoice.getInvoiceStatus())) {
            dbInvoice.setInvoiceStatus(Invoice.InvoiceStatus.SENT);
        } else if (Invoice.InvoiceStatus.DRAFT.equals(invoice.getInvoiceStatus())) {
            dbInvoice.setInvoiceStatus(Invoice.InvoiceStatus.DRAFT);
        } else {
            dbInvoice.setInvoiceStatus(Invoice.InvoiceStatus.IN_PROGRESS);
        }
        dbInvoice.setInvoicePaymentStatus(invoice.getInvoicePaymentStatus());
        dbInvoice.setInventoryProcessed(invoice.isInventoryProcessed());
        dbInvoice.setInventoryProcessedDate(invoice.getInventoryProcessedDate());
        dbInvoice.setTransactionType(invoice.getTransactionType());
        if (customerCompany.getArmsLengthType() != null) {
            dbInvoice.setTransactionType(customerCompany.getArmsLengthType());
        }

        Cart oldCart = dbInvoice.getCart();
        dbInvoice.setCart(invoice.getCart());
        updateOrderItems(dbInvoice.getCart());
        updateNotes(dbInvoice);
        invoiceInventoryService.calculateTaxes(shop, dbInvoice);

        prepareCartForItems(oldCart, dbInvoice.getCart(), dbInvoice.getId());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(invoice.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            dbInvoice.setExternalId(invoice.getExternalId());
        }

        Invoice updatedInvoice = invoiceRepository.update(invoiceId, dbInvoice);

        // TODO: Fix
        // elasticSearchManager.createOrUpdateIndexedDocument(updatedInvoice, Invoice.class, this);

        try {
            invoiceHistoryService.updateInvoiceHistory(dbInvoice.getId(), token.getActiveTopUser().getUserId());
            invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), dbInvoice.getInvoiceNumber() + " invoice updated");
        } catch (Exception ex) {

        }

        return updatedInvoice;
    }

    /**
     * This method is used to delete an invoice by id.
     *
     * @param invoiceId
     */
    @Override
    public void deleteInvoiceById(String invoiceId) {

        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Invoice dbInvoice = invoiceRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), invoiceId);
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        } else if (dbInvoice.isDeleted()) {
            throw new BlazeInvalidArgException(INVOICE, "This invoice is already deleted");
        } else {
            try {
                invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), dbInvoice.getInvoiceNumber() + " invoice deleted");
                invoiceRepository.removeByIdSetState(token.getCompanyId(), invoiceId);

                elasticSearchManager.deleteIndexedDocument(invoiceId, Invoice.class);
            } catch (Exception ex) {

            }

        }

    }

    /**
     * This method is used to create an new invoice.
     *
     * @param invoiceRequest
     * @return
     */
    @Override
    public Invoice createInvoice(AddInvoiceRequest invoiceRequest) {
        Invoice invoice = this.prepareInvoice(invoiceRequest);

        if (invoice != null) {
            invoice.prepare(token.getCompanyId());
            invoice.setShopId(token.getShopId());
            if (StringUtils.isBlank(invoice.getSalesPersonId())) {
                invoice.setSalesPersonId(token.getActiveTopUser().getUserId());
            }

            if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
                if (StringUtils.isBlank(invoiceRequest.getExternalId())) {
                    throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
                }
                invoice.setExternalId(invoiceRequest.getExternalId());
            }

            Invoice dbInvoice = invoiceRepository.save(invoice);

            if (dbInvoice != null) {
                invoiceHistoryService.addInvoiceHistory(dbInvoice.getId(), token.getActiveTopUser().getUserId());
                invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), dbInvoice.getInvoiceNumber() + " invoice created");
                UploadFileResult result = null;
                File file = null;
                try {
                    InputStream inputStream = QrCodeUtil.getQrCode(dbInvoice.getInvoiceNumber());
                    file = QrCodeUtil.stream2file(inputStream, ".png");
                    String keyName = dbInvoice.getId() + "-" + dbInvoice.getInvoiceNumber().replaceAll(" ", "");
                    keyName = keyName.replaceAll("[&#$%^]", "");

                    result = amazonS3Service.uploadFilePublic(file, keyName, ".png");
                    if (Objects.nonNull(result) && StringUtils.isNotBlank(result.getUrl())) {
                        CompanyAsset asset = new CompanyAsset();
                        asset.prepare(token.getCompanyId());
                        asset.setName("Invoice");
                        asset.setKey(result.getKey());
                        asset.setActive(true);
                        asset.setType(Asset.AssetType.Photo);
                        asset.setPublicURL(result.getUrl());
                        asset.setSecured(false);
                        companyAssetRepository.save(asset);
                        dbInvoice.setInvoiceQrCodeAsset(asset);
                        dbInvoice.setInvoiceQrCodeUrl(result.getUrl());
                        invoiceRepository.update(dbInvoice.getId(), dbInvoice);
                    }
                } catch (IOException ex) {
                    LOGGER.error(QR_CODE_ERROR);
                }
            }

            // TODO: Fix
            // elasticSearchManager.createOrUpdateIndexedDocument(dbInvoice, Invoice.class, this);

            return dbInvoice;
        }
        return invoice;

    }

    /**
     * This method is used to update note for Invoice.
     *
     * @param invoice
     */
    private void updateNotes(Invoice invoice) {
        if (invoice.getNotes() != null && invoice.getNotes().size() > 0) {
            List<Note> notesList = invoice.getNotes();
            List<Note> newNotesList = new ArrayList<>();
            for (Note note : notesList) {
                if (note.getId() == null || note.getId().isEmpty()) {
                    note.prepare();
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                    newNotesList.add(note);
                } else {
                    newNotesList.add(note);
                }

            }

            invoice.setNotes(newNotesList);

        }
    }

    /**
     * This method is used to update order items for cart if invoice in updating
     *
     * @param cart
     */
    private void updateOrderItems(Cart cart) {

        List<OrderItem> orderItems = cart.getItems();
        List<OrderItem> newOrderItems = new ArrayList<>();
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getId() == null) {
                orderItem.prepare(token.getCompanyId());
                newOrderItems.add(orderItem);
            } else {
                newOrderItems.add(orderItem);
            }
        }
        cart.setItems(newOrderItems);
    }

    /**
     * This method is used to get invoice activity logs by invoice id.
     *
     * @param invoiceId
     * @return
     */
    @Override
    public SearchResult<InvoiceActivityLog> getInvoiceActivityLogByInvoiceId(String invoiceId, int start, int limit) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        return invoiceActivityLogRepository.getInvoiceActivityLogByInvoiceId(token.getCompanyId(), token.getShopId(), invoiceId, "{modified:-1}", start, limit);
    }

    /**
     * This method is used to get invoice attachment by invoiceId and attachmentId.
     *
     * @param invoiceId
     * @param attachmentId
     * @return
     */
    @Override
    public CompanyAsset getInvoiceAttachment(String invoiceId, String attachmentId) {

        CompanyAsset asset;
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        asset = getInvoiceAttachmentById(invoice.getAttachments(), attachmentId);
        if (asset == null) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }
        return asset;
    }

    /**
     * This method is used to get assets of a company by attachmentId and its list;
     *
     * @param companyAssetList
     * @param attachmentId
     * @return
     */
    private CompanyAsset getInvoiceAttachmentById(List<CompanyAsset> companyAssetList, String attachmentId) {

        CompanyAsset asset = null;
        if (companyAssetList != null) {
            for (CompanyAsset dbAsset : companyAssetList) {
                if (dbAsset.getId().equals(attachmentId)) {
                    asset = dbAsset;
                    break;
                }
            }
        }

        return asset;
    }

    /**
     * This method is used to add invoice attachment using invoiceId and company assets.
     *
     * @param invoiceId
     * @param request
     * @return
     */
    @Override
    public Invoice addInvoiceAttachment(String invoiceId, InvoiceAttachmentRequest request) {

        Invoice dbInvoice = null;
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        if (request == null || request.getCompanyAsset() == null || request.getCompanyAsset().isEmpty()) {
            throw new BlazeInvalidArgException(INVOICE, ATTACHMENT_NOT_FOUND);
        }
        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        List<CompanyAsset> assets = request.getCompanyAsset();
        List<CompanyAsset> attachments = (invoice.getAttachments() == null) ? new ArrayList<>() : invoice.getAttachments();
        StringBuilder attachmentName = new StringBuilder();
        for (CompanyAsset asset : assets) {
            asset.prepare(token.getCompanyId());
            attachments.add(asset);
            if (StringUtils.isNotBlank(attachmentName)) {
                attachmentName.append(",");
            }
            attachmentName.append(asset.getName());

        }
        invoice.setAttachments(attachments);

        dbInvoice = invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);

        if (dbInvoice != null) {
            invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), attachmentName + " added");
        }


        return dbInvoice;
    }

    @Override
    public Invoice updateAttachment(String invoiceId, String attachmentId, CompanyAsset asset) {

        Invoice dbInvoice = null;
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        if (asset == null) {
            throw new BlazeInvalidArgException(INVOICE, ATTACHMENT_NOT_FOUND);
        }
        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        List<CompanyAsset> companyAssetList = invoice.getAttachments();

        CompanyAsset companyAsset = getInvoiceAttachmentById(companyAssetList, attachmentId);

        if (companyAsset == null) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }


        companyAsset.setName(asset.getName());
        companyAsset.setKey(asset.getKey());
        companyAsset.setType(asset.getType());
        companyAsset.setPublicURL(asset.getPublicURL());
        companyAsset.setActive(asset.isActive());
        companyAsset.setPriority(asset.getPriority());
        companyAsset.setSecured(asset.isSecured());

        dbInvoice = invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);

        if (dbInvoice != null) {
            invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment updated");
        }


        return dbInvoice;

    }

    /**
     * This method is used to delete invoice attachment using invoiceId and attachmentId.
     *
     * @param invoiceId
     * @param attachmentId
     */
    @Override
    public void deleteInvoiceAttachment(String invoiceId, String attachmentId) {

        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        List<CompanyAsset> companyAssetList = invoice.getAttachments();
        CompanyAsset asset = getInvoiceAttachmentById(companyAssetList, attachmentId);
        if (asset != null) {
            companyAssetList.remove(asset);
            Invoice dbInvoice = invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);

            if (dbInvoice != null) {
                invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment deleted");
            }
        } else {
            throw new BlazeOperationException(ATTACHMENT_NOT_FOUND);
        }


    }


    /**
     * This method is used to get all shops with limits.
     *
     * @param shopId
     * @param statusList
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<InvoiceResult> getShopInvoices(String shopId, List<Invoice.InvoiceStatus> statusList, int start, int limit, Invoice.InvoiceSort sortOption, String searchTerm) {

        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }
        boolean isDeliveryDate = false;
        sortOption = (sortOption == null) ? Invoice.InvoiceSort.DATE : sortOption;

        String sortOptionStr = "";

        if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
            sortOptionStr = "{ dueDate : -1 }";
        } else if (Invoice.InvoiceSort.AMOUNT == sortOption) {
            sortOptionStr = "{ totalInvoiceAmount : -1 }";
        } else if (Invoice.InvoiceSort.CREATED_DATE == sortOption) {
            sortOptionStr = "{invoiceDate: -1}";
        } else if (Invoice.InvoiceSort.DELIVERY_DATE == sortOption) {
            sortOptionStr = "{ modified : -1 }";
            isDeliveryDate = true;
        } else {
            //default sorting option is modified date
            sortOptionStr = "{ modified : -1 }";
        }


        SearchResult<InvoiceResult> items;

        if (StringUtils.isBlank(searchTerm)) {
            if (statusList.size() == 0) {
                items = invoiceRepository.findItems(token.getCompanyId(), shopId, sortOptionStr, start, limit, InvoiceResult.class);
                if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                    items = filterInvoicesWithOverDue(items);
                }
            } else {
                items = invoiceRepository.getAllShops(token.getCompanyId(), shopId, start, limit, statusList, sortOptionStr);
                if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                    items = filterInvoicesWithOverDue(items);
                }
            }
        } else {
            SearchResult<VendorResult> vendorSearchResult = vendorRepository.findVendorsByTerm(token.getCompanyId(), token.getShopId(), searchTerm, VendorResult.class);
            List<VendorResult> vendorList = vendorSearchResult.getValues();
            List<String> vendorIds = new ArrayList<>();
            if (!vendorList.isEmpty()) {

                for (VendorResult vendorResult : vendorList) {
                    vendorIds.add(vendorResult.getId());
                }
            }
            boolean state = true;
            if (!statusList.isEmpty() && statusList.contains(Invoice.InvoiceStatus.COMPLETED)) {
                items = invoiceRepository.getAllInvoicesByTermAndStatus(token.getCompanyId(), shopId, start, limit, sortOptionStr, searchTerm, vendorIds, InvoiceResult.class, statusList);
            } else {
                items = invoiceRepository.getAllInvoicesByTermAndState(token.getCompanyId(), shopId, state, start, limit, sortOptionStr, searchTerm, vendorIds, InvoiceResult.class);
            }

            if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                items = filterInvoicesWithOverDue(items);
            }


        }
        if (items != null && !items.getValues().isEmpty()) {
            for (InvoiceResult invoiceResult : items.getValues()) {
                prepareInvoiceResult(invoiceResult);
            }
        }
        if (isDeliveryDate && Objects.nonNull(items.getValues())) {
            items.getValues().sort(InvoiceResult::compare);
            return items;
        } else {
            return items;
        }
    }

    /**
     * This method is used to get company invoices by dates
     *
     * @param afterDate
     * @param beforeDate
     * @param status
     * @return
     */
    @Override
    public DateSearchResult<Invoice> getCompanyInvoices(long afterDate, long beforeDate, String status) {

        DateSearchResult<Invoice> invoiceDateSearchResult = invoiceRepository.findInvoicesWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate, "{modified:-1}");
        if (status == null) {
            return invoiceDateSearchResult;
        } else {
            if (invoiceDateSearchResult.getValues().size() > 0) {
                List<Invoice> newInvoices = new ArrayList<>();
                List<Invoice> invoices = invoiceDateSearchResult.getValues();
                for (Invoice invoice : invoices) {
                    if (status.equalsIgnoreCase(invoice.getInvoiceStatus().toString())) {
                        newInvoices.add(invoice);
                    }
                }
                invoiceDateSearchResult.setValues(newInvoices);
            }
            return invoiceDateSearchResult;
        }


    }

    /**
     * This method is used to get invoices by product request statuses
     *
     * @param start
     * @param limit
     * @param productRequestStatus
     * @return
     */
    @Override
    public SearchResult<Invoice> getInvoicesByProductRequestStatus(int start, int limit, String productRequestStatus) {

        try {
            if (limit == 0) {
                limit = Integer.MAX_VALUE;
            }
            if (productRequestStatus == null) {
                productRequestStatus = "PENDING";
            }
            List<String> statusList = Arrays.asList(productRequestStatus.split("\\s*,\\s*"));
            if (statusList.isEmpty()) {
                statusList.add(ProductRequest.RequestStatus.PENDING.toString());
            }
            List<ProductRequest.RequestStatus> requestStatusList = new ArrayList<>();
            for (String s : statusList) {
                requestStatusList.add(ProductRequest.RequestStatus.valueOf(s));
            }
            return invoiceRepository.getInvoicesByProductRequestStatus(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit, requestStatusList);
        } catch (Exception ex) {
            LOGGER.debug("Exception in Invoice Search Result By Status", ex.getCause());
        }
        return null;

    }

    /**
     * This method is used to add invoice payment.
     *
     * @param invoiceId
     * @param invoicePaymentRequest
     * @return
     */
    @Override
    public PaymentsReceived addInvoicePayment(String invoiceId, PaymentsReceivedAddRequest invoicePaymentRequest) {

        if (invoicePaymentRequest.getAmountPaid().doubleValue() <= 0) {
            throw new BlazeInvalidArgException(INVOICE, "Payment amount cannot be zero.");
        }
        if (invoicePaymentRequest.getPaymentType() == null) {
            throw new BlazeInvalidArgException(INVOICE, PAYMENT_TYPE_NOT_FOUND);
        }
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        if (Invoice.PaymentStatus.PAID.equals(invoice.getInvoicePaymentStatus())) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice is already paid.");
        }
        if (Invoice.InvoiceStatus.DRAFT.equals(invoice.getInvoiceStatus()) || Invoice.InvoiceStatus.CANCELLED.equals(invoice.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(INVOICE, PAYMENT_NOT_ALLOWED_FOR_DRAFT);
        }
        PaymentsReceived paymentsReceived = new PaymentsReceived();
        paymentsReceived.prepare(token.getCompanyId());
        paymentsReceived.setShopId(token.getShopId());
        paymentsReceived.setInvoiceId(invoiceId);
        paymentsReceived.setAmountPaid(invoicePaymentRequest.getAmountPaid());
        long paidDate = (invoicePaymentRequest.getPaidDate() == null) ? 0 : invoicePaymentRequest.getPaidDate();
        paidDate = (paidDate == 0) ? DateTime.now().getMillis() : invoicePaymentRequest.getPaidDate();
        paymentsReceived.setPaidDate(paidDate);
        if (invoicePaymentRequest.getNotes() != null) {
            Note note = invoicePaymentRequest.getNotes();
            note.prepare();
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            note.setWriterId(token.getActiveTopUser().getUserId());


            paymentsReceived.setNotes(note);
        }

        paymentsReceived.setPaymentType(invoicePaymentRequest.getPaymentType());
        BigDecimal balanceDue = new BigDecimal(0);
        BigDecimal totalInvoiceAmount = new BigDecimal(0);
        if (invoice.getCart() != null && invoice.getCart().getTotal() != null) {
            totalInvoiceAmount = invoice.getCart().getTotal();
        }
        Map<String, Object> returnMap = this.calculateBalanceDue(invoiceId, totalInvoiceAmount, invoicePaymentRequest.getAmountPaid());
        balanceDue = (BigDecimal) returnMap.get("balanceDue");
        balanceDue = (balanceDue == null || balanceDue.doubleValue() < 0) ? new BigDecimal(0) : balanceDue;

        paymentsReceived.setReferenceNo(StringUtils.isBlank(invoicePaymentRequest.getReferenceNo()) ? "" : invoicePaymentRequest.getReferenceNo());

        paymentsReceived.setPaymentNo(generateInvoicePaymentNumber(invoiceId, invoice.getInvoiceNumber()));

        PaymentsReceived dbInvoicePayment = invoicePaymentsRepository.save(paymentsReceived);
        if (dbInvoicePayment != null) {
            if (returnMap.containsKey("invoice")) {
                invoice = (Invoice) returnMap.get("invoice");
                if (invoice != null) {
                    invoiceRepository.update(invoice.getCompanyId(), invoice.getId(), invoice);
                    shippingManifestRepository.updateShippingManifestDetailsByInvoice(token.getCompanyId(), token.getShopId(), invoice.getId(), totalInvoiceAmount, balanceDue, invoice.getInvoiceStatus());
                }
            }
            invoiceActivityLogService.addInvoiceActivityLog(invoiceId, token.getActiveTopUser().getUserId(), invoice.getInvoiceNumber() + " invoice Payment created");
        }
        return dbInvoicePayment;


    }

    /**
     * This method is used to get an invoice payment according to invoicePaymentId.
     *
     * @param invoicePaymentId
     * @return
     */
    @Override
    public InvoicePaymentResult getInvoicePaymentById(String invoicePaymentId) {

        InvoicePaymentResult invoicePaymentResult = invoicePaymentsRepository.get(token.getCompanyId(), invoicePaymentId, InvoicePaymentResult.class);
        if (invoicePaymentResult == null) {
            LOGGER.debug("Exception in getInvoicePaymentById", INVOICE_PAYMENT_NOT_FOUND);
            throw new BlazeOperationException(INVOICE_PAYMENT_NOT_FOUND);
        }
        prepareInvoicePaymentResult(invoicePaymentResult);
        return invoicePaymentResult;

    }

    /**
     * this private method is used to prepare result for invoice payment .
     *
     * @param invoicePaymentResult
     */
    private void prepareInvoicePaymentResult(InvoicePaymentResult invoicePaymentResult) {
        Invoice invoice = null;
        if (invoicePaymentResult.getInvoiceId() != null) {
            invoice = invoiceRepository.get(token.getCompanyId(), invoicePaymentResult.getInvoiceId());
        }
        invoicePaymentResult.setInvoice(invoice);
    }

    /**
     * This method is used to get all invoicePayments for an invoice.
     *
     * @param invoiceId
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<PaymentsReceived> getAllInvoicePayments(String invoiceId, int start, int limit) {

        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        Invoice dbInvoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (dbInvoice == null) {
            LOGGER.debug("Exception in getAllInvoicePayments", INVOICE_NOT_FOUND);
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
       /* for(InvoicePaymentResult invoicePaymentResult : allInvoicePaymentsByInvoiceId.getValues ()){
            prepareInvoicePaymentResult ( invoicePaymentResult );
        }*/
        return invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceId(token.getCompanyId(), invoiceId, start, limit, "{created:1}");

    }

    /**
     * This private method is used to get a sequence for invoice number.
     *
     * @return
     */
    private long getNextSequence() {
        return companyUniqueSequenceService.getNewIdentifier(token.getCompanyId(), INVOICE, invoiceRepository.count(token.getCompanyId()));
    }

    /**
     * This method is used to update Invoice status.
     *
     * @param invoiceId
     * @param updateRequest
     * @return
     */
    @Override
    public Invoice updateInvoiceStatus(String invoiceId, InvoiceStatusUpdateRequest updateRequest) {
        Invoice dbInvoice = null;
        if (updateRequest.getInvoiceupdateStatus() == null) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice status cannot be blank");
        }
        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice != null) {
            invoice.setInvoiceStatus(updateRequest.getInvoiceupdateStatus());
            dbInvoice = invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);

            if (dbInvoice != null) {
                invoiceActivityLogService.addInvoiceActivityLog(dbInvoice.getId(), token.getActiveTopUser().getUserId(), "Invoice Status update " + updateRequest.getInvoiceupdateStatus());
            }

        } else {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        return dbInvoice;
    }

    /**
     * This method is used to get all invoices according to customer company.
     *
     * @param customerId
     * @param statusList
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<InvoiceResult> getAllInvoiceByCustomer(String customerId, List<Invoice.InvoiceStatus> statusList, int start, int limit) {
        if (StringUtils.isBlank(customerId)) {
            throw new BlazeInvalidArgException(INVOICE, "Customer can't be blank");
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        if (statusList == null || statusList.isEmpty()) {
            SearchResult<InvoiceResult> result = invoiceRepository.getAllInvoicesByCustomerWithoutStatus(token.getCompanyId(), token.getShopId(), customerId, start, limit, "{modified:-1}");
            if (result.getValues().size() > 0) {
                for (InvoiceResult invoiceResult : result.getValues()) {
                    prepareInvoiceResult(invoiceResult);
                }
            }
            return result;
        } else {
            SearchResult<InvoiceResult> result = invoiceRepository.getAllInvoicesByCustomer(token.getCompanyId(), token.getShopId(), customerId, statusList, start, limit, "{modified:-1}");
            if (result.getValues().size() > 0) {
                for (InvoiceResult invoiceResult : result.getValues()) {
                    prepareInvoiceResult(invoiceResult);
                }
            }
            return result;
        }

    }

    /**
     * This method is used to send email to customer.
     *
     * @param invoiceId
     * @param emailRequest
     */
    @Override
    public void sendEmailToCustomer(String invoiceId, InvoiceEmailRequest emailRequest) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice can't be empty");
        }
        Invoice dbInvoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        final Vendor customerCompany = vendorRepository.getById(dbInvoice.getCustomerId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(INVOICE, "Customer Company can't be empty");
        }
        String body = emailBody(customerCompany, dbInvoice);
        amazonServiceManager.sendEmail("support@blaze.me", emailRequest.getEmail(), connectConfiguration.getAppName(), body, null, null, "");


    }

    /**
     * This private method is used to create email body for invoice.
     *
     * @param customerCompany
     * @param invoice
     * @return
     */
    private String emailBody(Vendor customerCompany, Invoice invoice) {
        try {
            Company company = companyRepository.getById(token.getCompanyId());
            Shop shop = shopRepository.getById(token.getShopId());
            Employee employee = employeeRepository.getById(invoice.getSalesPersonId());

            BigDecimal amountPaid = new BigDecimal(0);
            BigDecimal discount = new BigDecimal(0);
            BigDecimal balanceDue = new BigDecimal(0);
            BigDecimal subTotal = new BigDecimal(0);
            BigDecimal totalInvoiceAmount = new BigDecimal(0);
            BigDecimal shippingCharges = new BigDecimal(0);
            BigDecimal totalTax = new BigDecimal(0);
            BigDecimal exciseTax = new BigDecimal(0);
            BigDecimal nalExciseTax = new BigDecimal(0);
            BigDecimal alExciseTax = new BigDecimal(0);
            BigDecimal cityTax = new BigDecimal(0);
            BigDecimal stateTax = new BigDecimal(0);
            BigDecimal fedTax = new BigDecimal(0);
            BigDecimal countyTax = new BigDecimal(0);
            BigDecimal tax = new BigDecimal(0);
            BigDecimal changeReturn = new BigDecimal(0);
            BigDecimal cultivationTax = new BigDecimal(0);


            InputStream inputStream = InvoiceServiceImpl.class.getResourceAsStream(INVOICE_HTML_RESOURCE);
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException ex) {
                LOGGER.error("Error! in invoice email", ex);
            }
            String invoiceStatus = invoice.getInvoiceStatus().toString();
            if (invoiceStatus.equalsIgnoreCase(Invoice.InvoiceStatus.IN_PROGRESS.toString())) {
                invoiceStatus = "In Progress";
            }
            String body = writer.toString();
            body = body.replaceAll("==blankSpace==", TextUtil.textOrEmpty("  "));
            body = body.replaceAll("==paymentStatus==", invoice.getInvoicePaymentStatus().toString().toLowerCase() + " / " + invoiceStatus.toLowerCase());
            if (shop != null) {
                body = body.replaceAll("==companyName==", shop.getName());
            } else {
                body = body.replaceAll("==companyName==", TextUtil.textOrEmpty(""));
            }
            if (shop.getAddress() != null) {
                Address address = shop.getAddress();
                body = body.replaceAll("==address==", TextUtil.textOrEmpty(address.getAddress()));
                body = body.replaceAll("==city==", TextUtil.textOrEmpty(address.getCity()));
                body = body.replaceAll("==state==", TextUtil.textOrEmpty(address.getState()));
                body = body.replaceAll("==zipCode==", TextUtil.textOrEmpty(address.getZipCode()));
                body = body.replaceAll("==country==", TextUtil.textOrEmpty(address.getCountry()));
                body = body.replaceAll("==showCompanyAddress==", TextUtil.textOrEmpty(""));
            } else {
                body = body.replaceAll("==showCompanyAddress==", TextUtil.textOrEmpty("display:none;"));
            }

            if (shop != null && StringUtils.isNotBlank(shop.getLicense())) {
                body = body.replaceAll("==licenseNumber==", shop.getLicense());
            } else {
                body = body.replaceAll("==licenseNumber==", TextUtil.textOrEmpty("N/A"));
            }
            if (invoice.getCart() != null && invoice.getCart().getItems() != null && !invoice.getCart().getItems().isEmpty()) {
                int index = 0;
                StringBuilder section = new StringBuilder();
                List<ObjectId> productId = new ArrayList<>();
                totalInvoiceAmount = (invoice.getCart().getTotal() == null) ? new BigDecimal(0) : invoice.getCart().getTotal();
                subTotal = (invoice.getCart().getSubTotal() == null) ? new BigDecimal(0) : invoice.getCart().getSubTotal();
                discount = (invoice.getCart().getTotalDiscount() == null) ? new BigDecimal(0) : invoice.getCart().getTotalDiscount();
                balanceDue = invoice.getCart().getBalanceDue();
                balanceDue = (balanceDue == null || balanceDue.doubleValue() < 0) ? new BigDecimal(0) : balanceDue;
                changeReturn = invoice.getCart().getChangeDue();


                totalTax = invoice.getCart().getTotalCalcTax();
                nalExciseTax = invoice.getCart().getTotalExciseTax();
                alExciseTax = invoice.getCart().getTotalALExciseTax();
                cultivationTax = BigDecimal.ZERO;
                
                if (invoice.getCart().getTaxResult() != null && invoice.getCart().getTaxResult().getCultivationTaxResult() != null) {
                    cultivationTax = invoice.getCart().getTaxResult().getCultivationTaxResult().getTotalCultivationTax();
                }


                tax = invoice.getCart().getTax();
                tax = (tax == null) ? new BigDecimal(0) : tax;

                exciseTax = nalExciseTax.add(alExciseTax);

                if (invoice.getCart().getTaxResult() != null) {
                    TaxResult taxResult = invoice.getCart().getTaxResult();
                    cityTax = (taxResult.getTotalCityTax() == null) ? new BigDecimal(0) : taxResult.getTotalCityTax();
                    countyTax = (taxResult.getTotalCountyTax() == null) ? new BigDecimal(0) : taxResult.getTotalCountyTax();
                    stateTax = (taxResult.getTotalStateTax() == null) ? new BigDecimal(0) : taxResult.getTotalStateTax();
                    fedTax = (taxResult.getTotalFedTax() == null) ? new BigDecimal(0) : taxResult.getTotalFedTax();
                }

                if (invoice.getCart().isEnableDeliveryFee()) {
                    shippingCharges = (invoice.getCart().getDeliveryFee() == null) ? new BigDecimal(0) : invoice.getCart().getDeliveryFee();
                }
                List<ObjectId> prepackageIds = new ArrayList<>();
                for (OrderItem orderItem : invoice.getCart().getItems()) {
                    if (StringUtils.isNotBlank(orderItem.getProductId()) && ObjectId.isValid(orderItem.getProductId())) {
                        productId.add(new ObjectId(orderItem.getProductId()));

                        if (StringUtils.isNotBlank(orderItem.getPrepackageId())) {
                            prepackageIds.add(new ObjectId(orderItem.getPrepackageId()));
                        }
                    }
                }

                HashMap<String, Brand> brandHashMap = brandRepository.listAsMap();

                if (!productId.isEmpty()) {
                    HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), productId);

                    HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(token.getCompanyId(), prepackageIds);

                    for (OrderItem orderItem : invoice.getCart().getItems()) {
                        Product product = productHashMap.get(orderItem.getProductId());
                        if (product == null)
                            continue;

                        String brandName = null;
                        if (StringUtils.isNotBlank(product.getBrandId())) {
                            Brand brand = brandHashMap.get(product.getBrandId());
                            brandName = brand.getName();
                        } else {
                            brandName = "N/A";
                        }

                        Prepackage prepackage = prepackageMap.get(orderItem.getPrepackageId());

                        section.append(getProductInformationSection(++index, product, orderItem, prepackage, brandName));
                    }
                }
                String sectionData = section.toString();
                sectionData = sectionData.replaceAll("&", "&amp;");
                body = body.replaceAll("==productInformation==", Matcher.quoteReplacement(sectionData));

            } else {
                body = body.replaceAll("==productInformation==", TextUtil.textOrEmpty(""));
            }

            Map<String, Object> returnMap = new HashMap<>();

            returnMap = calculateBalanceDue(invoice.getId(), totalInvoiceAmount, amountPaid);

            amountPaid = (BigDecimal) returnMap.get("amountPaid");
            amountPaid = (amountPaid == null) ? new BigDecimal(0) : amountPaid;
            balanceDue = (BigDecimal) returnMap.get("balanceDue");
            changeReturn = (BigDecimal) returnMap.get("changeDue");

            body = body.replaceAll("==invoiceNumber==", invoice.getInvoiceNumber());

            if (customerCompany != null) {
                String companyName = customerCompany.getName();
                if (StringUtils.isNotBlank(customerCompany.getDbaName())) {
                    companyName += " (" + customerCompany.getDbaName() + ")";
                }
                body = body.replaceAll("==billTo==", companyName.replaceAll("&", "&amp;"));
            } else {
                body = body.replaceAll("==billTo==", TextUtil.textOrEmpty(""));
            }
            if (customerCompany != null && customerCompany.getAddress() != null) {
                Address address = customerCompany.getAddress();
                body = body.replaceAll("==billToAddress==", TextUtil.textOrEmpty(address.getAddress()));
                body = body.replaceAll("==billToCity==", TextUtil.textOrEmpty(address.getCity()));
                body = body.replaceAll("==billToState==", TextUtil.textOrEmpty(address.getState()));
                body = body.replaceAll("==billToZipCode==", TextUtil.textOrEmpty(address.getZipCode()));
                body = body.replaceAll("==billToCountry==", TextUtil.textOrEmpty(address.getCountry()));
                body = body.replaceAll("==showBillToAddress==", TextUtil.textOrEmpty(""));
            } else {
                body = body.replaceAll("==showBillToAddress==", TextUtil.textOrEmpty("display:none;"));
            }

            CompanyLicense companyLicense = customerCompany.getCompanyLicense(invoice.getLicenseId());

            StringBuilder billToLicenseNo = new StringBuilder();
            if (companyLicense != null) {
                billToLicenseNo.append(StringUtils.isNotBlank(companyLicense.getLicenseNumber()) ? companyLicense.getLicenseNumber() : "N/A").append(" (").append(companyLicense.getCompanyType()).append(")");
            }

            body = body.replaceAll("==billToLicense==", Matcher.quoteReplacement(billToLicenseNo.toString()));
            String personName="";

            if (employee != null && StringUtils.isNotBlank(employee.getFirstName())) {
                personName = employee.getFirstName() + " ";
            }
            if (employee != null && StringUtils.isNotBlank(employee.getLastName())) {
                personName += employee.getLastName();
            }
            body = body.replaceAll("==personName==", personName);

            long invoiceDate = (invoice.getInvoiceDate() == null || invoice.getInvoiceDate() == 0) ? invoice.getCreated() : invoice.getInvoiceDate();
            String timeZone = StringUtils.isBlank(token.getRequestTimeZone()) ? shop.getTimeZone() : token.getRequestTimeZone();

            body = body.replaceAll("==invoiceDate==", TextUtil.toDate(DateUtil.toDateTime(invoiceDate, timeZone)));
            if (invoice.getDueDate() != null) {
                body = body.replaceAll("==dueDate==", TextUtil.toDate(DateUtil.toDateTime(invoice.getDueDate(), timeZone)));
            } else {
                body = body.replaceAll("==dueDate==", TextUtil.textOrEmpty(""));
            }
            if (invoice.getInvoiceTerms() != null) {
                body = body.replaceAll("==term==", invoice.getInvoiceTerms().toString().replaceAll("&", "&amp;"));
            } else {
                body = body.replaceAll("==term==", TextUtil.textOrEmpty(""));
            }
            if (invoice.getEstimatedDeliveryDate() != null && invoice.getEstimatedDeliveryDate() > 0) {
                body = body.replaceAll("==deliveryDate==", TextUtil.toDate(DateUtil.toDateTime(invoice.getEstimatedDeliveryDate(), timeZone)));
            } else {
                body = body.replaceAll("==deliveryDate==", "");
            }
            if (invoice.getEstimatedDeliveryTime() != null && invoice.getEstimatedDeliveryTime() > 0) {
                body = body.replaceAll("==deliveryTime==", TextUtil.toTime(DateUtil.toDateTime(invoice.getEstimatedDeliveryTime(), timeZone)));
            } else {
                body = body.replaceAll("==deliveryTime==", "");
            }

            body = body.replaceAll("==subTotal==", TextUtil.formatToTwoDecimalPoints(subTotal));
            body = body.replaceAll("==discount==", TextUtil.formatToTwoDecimalPoints(discount));
            //TODO find out shipping charges
            body = body.replaceAll("==shippingCharge==", TextUtil.formatToTwoDecimalPoints(shippingCharges));
            body = body.replaceAll("==alExciseTax==", TextUtil.formatToTwoDecimalPoints(alExciseTax));
            body = body.replaceAll("==nalExciseTax==", TextUtil.formatToTwoDecimalPoints(nalExciseTax));
            body = body.replaceAll("==totalTax==", TextUtil.formatToTwoDecimalPoints(totalTax));
            body = body.replaceAll("==paymentMade==", TextUtil.formatToTwoDecimalPoints(amountPaid));
            body = body.replaceAll("==total==", TextUtil.formatToTwoDecimalPoints(totalInvoiceAmount));
            body = body.replaceAll("==balanceDue==", TextUtil.formatToTwoDecimalPoints(balanceDue));
            body = body.replaceAll("==countyTax==", TextUtil.formatToTwoDecimalPoints(countyTax));
            body = body.replaceAll("==stateTax==", TextUtil.formatToTwoDecimalPoints(stateTax));
            body = body.replaceAll("==cityTax==", TextUtil.formatToTwoDecimalPoints(cityTax));
            body = body.replaceAll("==fedTax==", TextUtil.formatToTwoDecimalPoints(fedTax));
            body = body.replaceAll("==returnAmount==", TextUtil.formatToTwoDecimalPoints(changeReturn));
            body = body.replaceAll("==cultivationTax==", TextUtil.formatToTwoDecimalPoints(cultivationTax));

            body = body.replaceAll("==showExciseTax==", ((exciseTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showAlExciseTax==", ((alExciseTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showNalExciseTax==", ((nalExciseTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showCountyTax==", ((countyTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showStateTax==", ((stateTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showFedTax==", ((fedTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showCityTax==", ((cityTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showReturnAmount==", ((changeReturn.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            body = body.replaceAll("==showCultivationTax==", ((cultivationTax.doubleValue() <= 0) ? TextUtil.textOrEmpty("display:none") : TextUtil.textOrEmpty("")));
            if (exciseTax.doubleValue() <= 0) {
                body = body.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none;"));
            } else {
                body = body.replaceAll("==showExciseTax==", TextUtil.textOrEmpty(""));
            }
            if (totalTax.doubleValue() <= 0) {
                body = body.replaceAll("==showTotalTax==", TextUtil.textOrEmpty("display:none;"));
            } else {
                body = body.replaceAll("==showTotalTax==", TextUtil.textOrEmpty(""));
            }
            if (cultivationTax.doubleValue() <= 0) {
                body = body.replaceAll("==showCultivationTax==", TextUtil.textOrEmpty("display:none;"));
            } else {
                body = body.replaceAll("==showCultivationTax==", TextUtil.textOrEmpty(""));
            }
            if(invoice.getInvoiceStatus()== Invoice.InvoiceStatus.IN_PROGRESS) {
                body = body.replaceAll("==cultivation==", " Estimated Cultivation Tax");
            }else if(invoice.getInvoiceStatus()== Invoice.InvoiceStatus.COMPLETED) {
                body = body.replaceAll("==cultivation==", "Cultivation Tax");
            }

            if (invoice.getNotes() != null && !invoice.getNotes().isEmpty()) {
                StringBuilder notes = new StringBuilder();
                for (Note note : invoice.getNotes()) {
                    notes.append(note.getMessage());
                    notes.append("\n");
                }
                body = body.replaceAll("==notes==", Matcher.quoteReplacement(TextUtil.escapeXMLText(notes.toString())));
            } else {
                body = body.replaceAll("==notes==", TextUtil.textOrEmpty(""));
            }
            body = body.replaceAll("==termsConditions==", Matcher.quoteReplacement(TextUtil.escapeXMLText(invoice.getTermsAndconditions())));

            String logoURL = (shop.getLogo() != null && StringUtils.isNotBlank(shop.getLogo().getLargeURL())) ? shop.getLogo().getLargeURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
            body = body.replaceAll("==logo==", logoURL);
            /*if(invoice.getInvoiceQrCodeAsset () != null)
            body = body.replaceAll ( "==QRCodeImage==",(invoice.getInvoiceQrCodeAsset ().getPublicURL () == null)?"":invoice.getInvoiceQrCodeAsset ().getPublicURL ());*/
            body = body.replaceAll("==QRCodeImage==", (invoice.getInvoiceQrCodeUrl() == null) ? "" : invoice.getInvoiceQrCodeUrl());
            body = body.replaceAll("==referenceNo==", (StringUtils.isNotBlank(invoice.getOrderNumber()) ? invoice.getOrderNumber() : ""));

            if (invoice.getCart().getAdjustmentInfoList() == null || invoice.getCart().getAdjustmentInfoList().size() == 0) {
                body = body.replaceAll("==showAdjustments==" , "display:none");
            } else {
                List<ObjectId> adjustmentIds = new ArrayList<>();
                for (AdjustmentInfo info : invoice.getCart().getAdjustmentInfoList()) {
                    adjustmentIds.add(new ObjectId(info.getAdjustmentId()));
                }

                HashMap<String, Adjustment> adjustmentMap = adjustmentRepository.listAsMap(token.getCompanyId(), adjustmentIds);

                StringBuilder adjustmentData = new StringBuilder();
                for (AdjustmentInfo info : invoice.getCart().getAdjustmentInfoList()) {
                    Adjustment adjustment = adjustmentMap.get(info.getAdjustmentId());
                    if (adjustment == null) {
                        continue;
                    }
                    adjustmentData.append(prepareAdjustmentsInfo(adjustment, info.getAmount().doubleValue(), info.isNegative()));
                }

                String adjustments = adjustmentData.toString();
                adjustments = adjustments.replaceAll("&", "&amp;");
                body = body.replaceAll("==adjustmentInfo==", Matcher.quoteReplacement(adjustments));
                body = body.replaceAll("==showAdjustments==" , " ");
            }

            return body;
        } catch (Exception ex) {
            LOGGER.error(INVOICE, ex.getCause());
            return "Information are not completed";
        }

    }

    private String prepareAdjustmentsInfo(Adjustment adjustment, double amount, boolean negative) {
        StringBuilder adjustmentData = new StringBuilder();

        adjustmentData.append("<div style=\"width:100%;float:left;background-color: rgb(238, 238, 238);\">");
        adjustmentData.append("<div style=\"width:50%;float:left;color: #5a5a5a;text-align: right;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\">" + adjustment.getName() + "</div>");

        if (negative) {
            adjustmentData.append("<div style=\"width:35%;float:right;text-align: right;color: red;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\"> -$" + TextUtil.formatToTwoDecimalPoints(amount) + "</div>");
        } else {
            adjustmentData.append("<div style=\"width:35%;float:right;text-align: right;color: #5a5a5a;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\"> $" + TextUtil.formatToTwoDecimalPoints(amount) + "</div>");
        }
        adjustmentData.append("</div>");

        return adjustmentData.toString();

    }

    /**
     * This private method is used to get product information for email under these parameters.
     *
     * @param index
     * @param product
     * @param orderItem
     * @param prepackage
     * @param brandName
     * @return
     */
    private String getProductInformationSection(final int index, final Product product, final OrderItem orderItem, Prepackage prepackage, final String brandName) {
        String productDescription = StringUtils.EMPTY;

        String quantityExt = "gram";
        if (prepackage != null && StringUtils.isNotBlank(prepackage.getName())) {
            productDescription = (StringUtils.isBlank(product.getName())) ? "" : (product.getName() + " (" + prepackage.getName() + ")");
            quantityExt = "unit";
            if (orderItem.getQuantity().doubleValue() > 1) {
                quantityExt = "units";
            }
        } else {
            productDescription = (StringUtils.isBlank(product.getName())) ? "" : product.getName();

            if (product.getCategory() != null) {
                if (ProductCategory.UnitType.units.equals(product.getCategory().getUnitType())) {
                    quantityExt = "unit";
                    if (orderItem.getQuantity().doubleValue() > 1) {
                        quantityExt = "units";
                    }
                } else {
                    quantityExt = "grams";
                    if (orderItem.getQuantity().doubleValue() > 1) {
                        quantityExt = "grams";
                    }
                }
            }
        }

        return "<tr style=\"border-bottom: 1px solid rgba(235, 235, 235, 1);page-break-inside:avoid; page-break-after:auto;\">\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + index + "</td>\n" +
                "<td style=\"width:60%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:left;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + productDescription + "</td>\n" +
                "<td style=\"width:60%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:left;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + brandName + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + TextUtil.formatToTwoDecimalPoints(orderItem.getQuantity()) + " " + quantityExt + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + TextUtil.formatToTwoDecimalPoints(orderItem.getUnitPrice()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:right;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + TextUtil.formatToTwoDecimalPoints(orderItem.getCalcDiscount()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:right;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:rgba(0, 0, 0, 0.87);page-break-inside:avoid; page-break-after:auto;\">" + TextUtil.formatToTwoDecimalPoints(orderItem.getFinalPrice()) + "</td>\n" +
                "</tr>";
    }

    /**
     * This method is used to update all invoices those are requested by customer in different manner.
     *
     * @param request
     */
    @Override
    public void bulkInvoiceUpdates(InvoiceBulkUpdateRequest request) {
        List<String> invoiceIds = request.getInvoiceIds();
        if (invoiceIds == null || invoiceIds.isEmpty()) {
            throw new BlazeInvalidArgException(BULK_INVOICE_UPDATE, "Please select Invoices");
        }
        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(BULK_INVOICE_UPDATE, "Please selection type of action");
        }
        List<ObjectId> invoiceObjectIdList = new ArrayList<>();
        for (String invoiceId : invoiceIds) {
            invoiceObjectIdList.add(new ObjectId(invoiceId));
        }
        switch (request.getOperationType()) {
            case INVOICE_TERM_TYPE:
                if (request.getInvoiceTerms() == null)
                    throw new BlazeInvalidArgException("Invoice Term Type", "Invoice Term type is not found");
                invoiceRepository.bulkUpdateTermType(token.getCompanyId(), token.getShopId(), invoiceObjectIdList, request.getInvoiceTerms());
                break;
            case RELATED_ENTITY:
                invoiceRepository.bulkUpdateRelatedEntity(token.getCompanyId(), token.getShopId(), invoiceObjectIdList, request.isRelatedEntity());
                break;
            case INVOICE_STATUS:
                if (request.getInvoiceStatus() == null)
                    throw new BlazeInvalidArgException("Invoice Invoice Status Type", "Invoice Status Type not found");
                invoiceRepository.bulkUpdateInvoiceStatus(token.getCompanyId(), token.getShopId(), invoiceObjectIdList, request.getInvoiceStatus());
                break;
            case PAYMENT_STATUS:
                if (request.getInvoicePaymentStatus() == null)
                    throw new BlazeInvalidArgException("Invoice Payment Status Type", "Invoice PaymentStatus Type not found");
                invoiceRepository.bulkUpdatePaymentStatus(token.getCompanyId(), token.getShopId(), invoiceObjectIdList, request.getInvoicePaymentStatus());
                break;
            case DELETE_INVOICE:
                if (request.getInvoiceIds().isEmpty())
                    throw new BlazeInvalidArgException(BULK_INVOICE_UPDATE, "Please select invoices to delete");
                invoiceRepository.bulkDeleteInvoice(token.getCompanyId(), token.getShopId(), invoiceObjectIdList);
                break;
            case STATUS:
                if (request.getInvoiceIds().isEmpty())
                    throw new BlazeInvalidArgException(BULK_INVOICE_UPDATE, "Please select invoices to Deactivate");
                invoiceRepository.bulkUpdateStatus(token.getCompanyId(), token.getShopId(), invoiceObjectIdList, request.isActive());
                break;
        }
    }

    /**
     * This method is used to get total out standing amount of a customer company
     *
     * @param customerId
     * @return
     */
    @Override
    public OutStandingResult getTotalOutStanding(String customerId) {
        BigDecimal totalInvoicesAmount = new BigDecimal(0);
        if (StringUtils.isBlank(customerId)) {
            throw new BlazeInvalidArgException(INVOICE, "Customer can't be blank");
        }
        SearchResult<InvoiceResult> result = invoiceRepository.getAllInvoicesByCustomerWithoutStatus(token.getCompanyId(), token.getShopId(), customerId, 0, Integer.MAX_VALUE, "{modified:-1}");
        if (result.getValues() != null && !result.getValues().isEmpty()) {
            List<InvoiceResult> resultList = result.getValues();
            List<String> invoiceIdList = new ArrayList<>();
            for (InvoiceResult invoiceResult : resultList) {
                invoiceIdList.add(invoiceResult.getId());
            }
            if (!invoiceIdList.isEmpty()) {
                Iterable<PaymentsReceived> paymentsReceivedList = invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceIdList(token.getCompanyId(), token.getShopId(), invoiceIdList, 0, Integer.MAX_VALUE, "{modified:-1}");
                if (paymentsReceivedList != null) {
                    for (PaymentsReceived paymentsReceived : paymentsReceivedList) {
                        totalInvoicesAmount = totalInvoicesAmount.add(paymentsReceived.getAmountPaid());
                    }
                }

            }
        }
        OutStandingResult outStandingResult = new OutStandingResult();
        outStandingResult.setTotalOutStanding(totalInvoicesAmount);
        return outStandingResult;
    }

    /**
     * This method is used to fet all invoices using some status.
     *
     * @param request
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<InvoiceResult> getALLInvoicesByFetchStatus(InvoiceFetchStatusRequest request, int start, int limit, Invoice.InvoiceSort sortOption, String searchTerm) {

        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        sortOption = (sortOption == null) ? Invoice.InvoiceSort.DATE : sortOption;

        String sortOptionStr = "";
        boolean isDeliveryDate = false;

        if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
            sortOptionStr = "{ dueDate : 1 }";
        } else if (Invoice.InvoiceSort.AMOUNT == sortOption) {
            sortOptionStr = "{ totalInvoiceAmount : -1 }";
        } else if (Invoice.InvoiceSort.CREATED_DATE == sortOption) {
            sortOptionStr = "{created: -1}";
        } else if (Invoice.InvoiceSort.DELIVERY_DATE == sortOption) {
            sortOptionStr = "{ modified : -1 }";
            isDeliveryDate = true;
        } else if (Invoice.InvoiceSort.MODIFIED == sortOption) {
            sortOptionStr = "{ modified : -1 }";
        } else {
            //default sorting option is modified date
            sortOptionStr = "{ invoiceDate : -1 }";
        }


        SearchResult<InvoiceResult> searchResult = null;
        if (StringUtils.isBlank(searchTerm) && request != null) {
            if (request.getFetchStatus() != null) {
                if (InvoiceFetchStatusRequest.FetchStatus.ALL.equals(request.getFetchStatus())) {
                    searchResult = invoiceRepository.getAllInvoices(token.getCompanyId(), token.getShopId(), start, limit, sortOptionStr, InvoiceResult.class);
                    if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                        searchResult = filterInvoicesWithOverDue(searchResult);
                    }
                } else {
                    boolean state = InvoiceFetchStatusRequest.FetchStatus.ACTIVE.equals(request.getFetchStatus()) ? true : false;
                    List<Invoice.PaymentStatus> invoicePaymentList = new ArrayList<>();
                    List<Invoice.InvoiceStatus> invoiceStatus = new ArrayList<>();
                    if (InvoiceFetchStatusRequest.FetchStatus.ACTIVE == request.getFetchStatus()) {
                        invoicePaymentList.add(Invoice.PaymentStatus.UNPAID);
                        invoicePaymentList.add(Invoice.PaymentStatus.PARTIAL);
                        invoicePaymentList.add(Invoice.PaymentStatus.PAID);
                        invoiceStatus.add(Invoice.InvoiceStatus.IN_PROGRESS);
                        invoiceStatus.add(Invoice.InvoiceStatus.SENT);
                    } else {
                        invoicePaymentList.add(Invoice.PaymentStatus.PAID);
                        invoiceStatus.add(Invoice.InvoiceStatus.COMPLETED);
                    }
                    searchResult = invoiceRepository.getAllInvoicesByPaymentStatus(token.getCompanyId(), token.getShopId(), state, invoicePaymentList, invoiceStatus, start, limit, sortOptionStr, InvoiceResult.class);
                    if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                        searchResult = filterInvoicesWithOverDue(searchResult);
                    }
                }
            }
        } else if (StringUtils.isNotBlank(searchTerm)) {
            SearchResult<VendorResult> vendorSearchResult = vendorRepository.findVendorsByTerm(token.getCompanyId(), token.getShopId(), searchTerm, VendorResult.class);
            List<VendorResult> vendorList = vendorSearchResult.getValues();
            List<String> vendorIds = new ArrayList<>();
            if (!vendorList.isEmpty()) {

                for (VendorResult vendorResult : vendorList) {
                    vendorIds.add(vendorResult.getId());
                }
            }
            if (request != null && request.getFetchStatus() != null && !InvoiceFetchStatusRequest.FetchStatus.ALL.equals(request.getFetchStatus())) {
                boolean state = InvoiceFetchStatusRequest.FetchStatus.ACTIVE.equals(request.getFetchStatus()) ? true : false;
                List<Invoice.PaymentStatus> invoicePaymentList = new ArrayList<>();
                List<Invoice.InvoiceStatus> invoiceStatus = new ArrayList<>();
                if (InvoiceFetchStatusRequest.FetchStatus.ACTIVE == request.getFetchStatus()) {
                    invoicePaymentList.add(Invoice.PaymentStatus.UNPAID);
                    invoicePaymentList.add(Invoice.PaymentStatus.PARTIAL);
                    invoicePaymentList.add(Invoice.PaymentStatus.PAID);
                    invoiceStatus.add(Invoice.InvoiceStatus.IN_PROGRESS);
                    invoiceStatus.add(Invoice.InvoiceStatus.SENT);
                } else {
                    invoicePaymentList.add(Invoice.PaymentStatus.PAID);
                    invoiceStatus.add(Invoice.InvoiceStatus.COMPLETED);
                }
                searchResult = invoiceRepository.getAllInvoicesByTermAndStatus(token.getCompanyId(), token.getShopId(), state, invoicePaymentList, invoiceStatus, start, limit, sortOptionStr, searchTerm, vendorIds, InvoiceResult.class);
                if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                    searchResult = filterInvoicesWithOverDue(searchResult);
                }
            } else {
                searchResult = invoiceRepository.getAllInvoicesByTerm(token.getCompanyId(), token.getShopId(), start, limit, sortOptionStr, searchTerm, vendorIds, InvoiceResult.class);
                if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                    searchResult = filterInvoicesWithOverDue(searchResult);
                }
            }
        } else {
            boolean state = true;
            searchResult = invoiceRepository.getAllInvoicesByState(token.getCompanyId(), token.getShopId(), state, start, limit, sortOptionStr, InvoiceResult.class);
            if (Invoice.InvoiceSort.OVER_DUE == sortOption) {
                searchResult = filterInvoicesWithOverDue(searchResult);
            }
        }
        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (InvoiceResult invoiceResult : searchResult.getValues()) {
                prepareInvoiceResult(invoiceResult);
            }
        }

        if (isDeliveryDate && Objects.nonNull(searchResult.getValues())) {
            searchResult.getValues().sort(InvoiceResult::compare);
            return searchResult;
        } else {
            return searchResult;
        }
    }

    private SearchResult<InvoiceResult> filterInvoicesWithOverDue(SearchResult<InvoiceResult> searchResult) {
        List<InvoiceResult> invoiceResults = new ArrayList<>();
        if (!searchResult.getValues().isEmpty()) {
            Shop shop = shopRepository.getById(token.getShopId());
            for (InvoiceResult invoiceResult : searchResult.getValues()) {
                if (Invoice.PaymentStatus.PAID == invoiceResult.getInvoicePaymentStatus()) {
                    continue;
                }
                Long dueDate = invoiceResult.getCreated();
                DateTime startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                DateTime endDate = DateUtil.nowWithTimeZone(shop.getTimeZone());
                long standardDaysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                switch (invoiceResult.getInvoiceTerms()) {
                    case NET_7:
                        standardDaysBetweenTwoDates = standardDaysBetweenTwoDates - 7;
                        break;
                    case NET_15:
                        standardDaysBetweenTwoDates = standardDaysBetweenTwoDates - 15;
                        break;
                    case NET_30:
                        standardDaysBetweenTwoDates = standardDaysBetweenTwoDates - 30;
                        break;
                    case NET_60:
                        standardDaysBetweenTwoDates = standardDaysBetweenTwoDates - 60;
                        break;
                    case NET_45:
                        standardDaysBetweenTwoDates = standardDaysBetweenTwoDates - 45;
                        break;
                    case CUSTOM_DATE:
                        dueDate = invoiceResult.getDueDate();
                        startDate = DateUtil.toDateTime(dueDate, shop.getTimeZone());
                        standardDaysBetweenTwoDates = DateUtil.getStandardDaysBetweenTwoDates(startDate.getMillis(), endDate.getMillis());
                        break;
                    default:
                        break;
                }
                String days;
                if (standardDaysBetweenTwoDates > 0) {
                    standardDaysBetweenTwoDates = Math.abs(standardDaysBetweenTwoDates);
                    if (standardDaysBetweenTwoDates == 1) {
                        days = "OverDue by " + standardDaysBetweenTwoDates + " day";
                    } else {
                        days = "OverDue by " + standardDaysBetweenTwoDates + " days";
                    }
                } else if (standardDaysBetweenTwoDates == 0) {
                    days = "Due date is Today";
                } else {
                    standardDaysBetweenTwoDates = Math.abs(standardDaysBetweenTwoDates);
                    if (standardDaysBetweenTwoDates == 1) {
                        days = "Due by " + standardDaysBetweenTwoDates + " day";
                    } else {
                        days = "Due by " + standardDaysBetweenTwoDates + " days";
                    }
                }
                invoiceResult.setOverDueDays(days);
                invoiceResults.add(invoiceResult);
            }

        }
        searchResult.setValues(invoiceResults);
        return searchResult;

    }

    @Override
    public Map<String, Object> calculateBalanceDue(String invoiceId, BigDecimal totalInvoiceAmount, BigDecimal amountPaid) {

        BigDecimal balanceDue = new BigDecimal(0);
        BigDecimal changeDue = new BigDecimal(0);
        Map<String, Object> returnMap = new HashMap<>();
        Invoice dbInvoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (dbInvoice != null && dbInvoice.getCart() != null && dbInvoice.getCart().getTotal().doubleValue() != 0) {
            totalInvoiceAmount = dbInvoice.getCart().getTotal();
        }
        SearchResult<PaymentsReceived> paymentsReceivedSearchResult = invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceId(token.getCompanyId(), invoiceId, 0, Integer.MAX_VALUE, "{modified:-1}");
        if (paymentsReceivedSearchResult != null) {
            List<PaymentsReceived> paymentsReceiveds = paymentsReceivedSearchResult.getValues();
            for (PaymentsReceived paymentsReceived : paymentsReceiveds) {
                amountPaid = amountPaid.add(paymentsReceived.getAmountPaid());
            }
            if (dbInvoice != null && dbInvoice.getCart() != null) {
                dbInvoice.getCart().setCashReceived(amountPaid);
                balanceDue = totalInvoiceAmount.doubleValue() <= 0 ? BigDecimal.ZERO : totalInvoiceAmount.subtract(amountPaid);
                dbInvoice.getCart().setCashReceived(amountPaid);
                if (balanceDue.doubleValue() > 0) {
                    dbInvoice.getCart().setBalanceDue(balanceDue);
                    dbInvoice.getCart().setChangeDue(BigDecimal.ZERO);

                } else {
                    dbInvoice.getCart().setChangeDue(balanceDue.abs());
                    dbInvoice.getCart().setBalanceDue(BigDecimal.ZERO);
                    changeDue = balanceDue.abs();
                }
            }
            Invoice.PaymentStatus invoicePaymentStatus;
            if (amountPaid.compareTo(BigDecimal.ZERO) == 0 && totalInvoiceAmount.doubleValue() > 0) {
                invoicePaymentStatus = Invoice.PaymentStatus.UNPAID;
            } else if (balanceDue.compareTo(BigDecimal.ZERO) <= 0) {
                balanceDue = new BigDecimal(0);
                invoicePaymentStatus = Invoice.PaymentStatus.PAID;
                if (Invoice.InvoiceStatus.COMPLETED == dbInvoice.getInvoiceStatus()) {
                    dbInvoice.setActive(false);
                }
            } else {
                invoicePaymentStatus = Invoice.PaymentStatus.PARTIAL;
            }
            if (dbInvoice != null) {
                dbInvoice.setInvoicePaymentStatus(invoicePaymentStatus);
            }
        }
        returnMap.put("balanceDue", balanceDue);
        returnMap.put("amountPaid", amountPaid);
        returnMap.put("invoice", dbInvoice);
        returnMap.put("changeDue", changeDue);
        return returnMap;
    }

    @Override
    public Invoice getInvoiceByInvoiceNumber(String invoiceNumber) {
        return invoiceRepository.getInvoiceByInvoiceNumber(token.getCompanyId(), invoiceNumber);
    }

    @Override
    public Invoice prepareInvoice(AddInvoiceRequest invoiceRequest) {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        if (StringUtils.isNotBlank(invoiceRequest.getSalesPersonId())) {
            Employee employee = employeeService.getEmployeeById(invoiceRequest.getSalesPersonId());
            if (employee == null) {
                throw new BlazeInvalidArgException("Sales Person", "Sales person not found");
            }
        }
        Vendor vendor;
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && StringUtils.isBlank(invoiceRequest.getCustomerId())) {
            vendor = vendorService.getDefaultVendor(token.getCompanyId());
            invoiceRequest.setCustomerId(vendor.getId());
        } else {
            if (StringUtils.isBlank(invoiceRequest.getCustomerId())) {
                throw new BlazeInvalidArgException(INVOICE, "Customer can't be blank.");
            }
            // Check if vendor exist
            vendor = vendorRepository.get(token.getCompanyId(), invoiceRequest.getCustomerId());
            if (vendor == null) {
                throw new BlazeInvalidArgException("Vendor", "vendor does not exist");
            }
        }
        Vendor customerCompany = vendorRepository.get(token.getCompanyId(), invoiceRequest.getCustomerId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "Please choose a valid customer");
        }
        if (Vendor.VendorType.VENDOR.equals(customerCompany.getVendorType())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, INVOICE_NOT_ALLOWED_FOR_VENDOR);
        }


        if (invoiceRequest.getCart() == null) {
            throw new BlazeInvalidArgException(CART, "Cart can't be empty");
        }
        if (invoiceRequest.getCart().getItems() == null && invoiceRequest.getCart().getItems().isEmpty()) {
            throw new BlazeInvalidArgException(CART, "Products can't be empty in Cart");
        }
        if (Invoice.InvoiceStatus.COMPLETED.equals(invoiceRequest.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice status can't be completed, until Shipping Manifest is not done");
        }
        if (invoiceRequest.getInvoiceTerms() == null) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice term cannot be blank");
        }
        if (Invoice.InvoiceStatus.DRAFT == invoiceRequest.getInvoiceStatus()) {

        } else if (Invoice.InvoiceTerms.CUSTOM_DATE.equals(invoiceRequest.getInvoiceTerms()) && (invoiceRequest.getDueDate() == null || invoiceRequest.getDueDate() == 0)) {
            throw new BlazeInvalidArgException(INVOICE, NOT_CUSTOM_DATE);
        }

        CompanyLicense dbCompanyLicense = customerCompany.getCompanyLicense(invoiceRequest.getLicenseId());
        if (dbCompanyLicense == null) {
            throw new BlazeInvalidArgException(INVOICE, "Company license not found");
        }
        if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
            DateTime currentTime = DateTime.now().withTimeAtStartOfDay();
            String timeZone = shop.getTimeZone();
            if (org.apache.commons.lang.StringUtils.isBlank(timeZone)) {
                timeZone = DateTimeZone.UTC.toString();
            }
            DateTime expirationDate = DateUtil.toDateTime(dbCompanyLicense.getLicenseExpirationDate(), timeZone).withTimeAtStartOfDay();

            if (Objects.nonNull(dbCompanyLicense.getLicenseExpirationDate()) && dbCompanyLicense.getLicenseExpirationDate() != 0 && expirationDate.isBefore(currentTime)) {
                throw new BlazeInvalidArgException(INVOICE, CUSTOMER_LICENSE_EXPIRED);
            }
        }

        Invoice invoice = new Invoice();
        invoice.setShopId(token.getShopId());
        invoice.setCompanyId(token.getCompanyId());
        invoice.setCustomerId(invoiceRequest.getCustomerId());
        invoice.setLicenseId(invoiceRequest.getLicenseId());
        invoice.setOrderNumber(invoiceRequest.getOrderNumber());
        invoice.setInvoiceTerms(invoiceRequest.getInvoiceTerms());
        invoice.setSalesPersonId(invoiceRequest.getSalesPersonId());
        invoice.setActive(true);
        if (invoiceRequest.getAttachments() != null && !invoiceRequest.getAttachments().isEmpty()) {
            List<CompanyAsset> companyAssetList = invoiceRequest.getAttachments();
            List<CompanyAsset> newCompanyAssetList = new ArrayList<>();
            for (CompanyAsset companyAsset : companyAssetList) {
                companyAsset.prepare(token.getCompanyId());
                newCompanyAssetList.add(companyAsset);
            }
            invoice.setAttachments(newCompanyAssetList);
        }

        invoice.setDueDate(invoiceRequest.getDueDate());
        invoice.setInvoiceDate(invoiceRequest.getInvoiceDate());
        updateInvoiceDueDate(invoice);

        invoice.setTermsAndconditions(invoiceRequest.getTermsAndconditions());
        if (invoiceRequest.getNotes() != null && !invoiceRequest.getNotes().isEmpty()) {
            for (Note note : invoiceRequest.getNotes()) {
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());

            }
            invoice.setNotes(invoiceRequest.getNotes());
        }
        invoice.setCompanyContactId(invoiceRequest.getCompanyContactId());

        invoice.setEstimatedDeliveryDate(invoiceRequest.getEstimatedDeliveryDate());
        invoice.setEstimatedDeliveryTime(invoiceRequest.getEstimatedDeliveryTime());
        invoice.setDeliveryCharges(invoiceRequest.getDeliveryCharges());
        invoice.setTotal(invoiceRequest.getTotal());
        invoice.setCart(invoiceRequest.getCart());
        invoice.setMarkup(invoiceRequest.getMarkup());
        invoice.setExciseTax(invoiceRequest.getExciseTax());
        invoice.setTotalInvoiceAmount(invoiceRequest.getTotalInvoiceAmount());
        if (Invoice.InvoiceStatus.SENT.equals(invoiceRequest.getInvoiceStatus())) {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.SENT);
        } else if (Invoice.InvoiceStatus.DRAFT.equals(invoiceRequest.getInvoiceStatus())) {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.DRAFT);
        } else {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.IN_PROGRESS);
        }
        invoice.setInvoicePaymentStatus(Invoice.PaymentStatus.UNPAID);
        if (StringUtils.isBlank(invoiceRequest.getInvoiceNumber())) {
            long ordinal = getNextSequence();
            String invoiceNo = NumberUtils.uniqueNumber("INV", ordinal);
            invoice.setInvoiceNumber(invoiceNo);
        } else {
            invoice.setInvoiceNumber(invoiceRequest.getInvoiceNumber());
        }
        invoice.setInventoryProcessed(invoiceRequest.isInventoryProcessed());
        invoice.setInventoryProcessedDate(invoiceRequest.getInventoryProcessedDate());
        invoice.setTransactionType(invoiceRequest.getTransactionType());
        if (customerCompany.getArmsLengthType() != null) {
            invoice.setTransactionType(customerCompany.getArmsLengthType());
        }

        invoiceInventoryService.calculateTaxes(shop, invoice);


        return invoice;
    }


    /**
     * This method is used to generated pdf for invoice
     *
     * @param invoiceId
     * @return
     */
    @Override
    public InputStream createPdfForInvoice(String invoiceId) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        final Invoice dbInvoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Vendor customerCompany = null;
        if (StringUtils.isNotBlank(dbInvoice.getCustomerId())) {
            customerCompany = vendorRepository.get(token.getCompanyId(), dbInvoice.getCustomerId());
        }

        String body = emailBody(customerCompany, dbInvoice);
        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, INVOICE_HTML_RESOURCE));
    }

    // TODO: Fix
//    @Override
//    public JSONObject toElasticSearchObject(Object obj) {
//        final JSONObject jsonObject = new JSONObject();
//
//        Invoice invoice = obj instanceof Invoice ? (Invoice) obj : null;
//
//        if(invoice == null) {
//            throw new BlazeInvalidArgException(INVOICE, "Object is not instance of Invoice");
//        }
//
//        Vendor vendor = vendorRepository.get(token.getCompanyId(), invoice.getCustomerId());
//
//        jsonObject.put("customerId", vendor != null ? vendor.getName() : "");
//        jsonObject.put("invoiceNumber", invoice.getInvoiceNumber());
//        jsonObject.put("invoiceStatus", invoice.getInvoiceStatus() != null ? invoice.getInvoiceStatus() : "");
//        jsonObject.put("invoicePaymentStatus", invoice.getInvoicePaymentStatus() != null ? invoice.getInvoicePaymentStatus() : "");
//        jsonObject.put("active", invoice.isActive());
//
//        return jsonObject;
//    }


    private void prepareInvoiceInformation(List<InvoiceInformation> invoiceList) {

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> companyContactIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        LinkedHashSet<String> invoiceIds = new LinkedHashSet<>();
        HashMap<String, HashMap<String, BigDecimal>> invoiceProductQty = new HashMap<>();

        ShopLimitedView shop = shopRepository.getById(token.getShopId(),ShopLimitedView.class);
        String logoURL = shop.getLogo() != null ? shop.getLogo().getPublicURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";

        for (InvoiceInformation invoice : invoiceList) {
            invoice.setCompanyLogo(logoURL);
            if (StringUtils.isNotBlank(invoice.getSalesPersonId()) && ObjectId.isValid(invoice.getSalesPersonId())) {
                employeeIds.add(new ObjectId(invoice.getSalesPersonId()));
            }
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }

            if (StringUtils.isNotBlank(invoice.getCompanyContactId()) && ObjectId.isValid(invoice.getCompanyContactId())) {
                companyContactIds.add(new ObjectId(invoice.getCompanyContactId()));
            }
            if (invoice.getCart() != null && invoice.getCart().getItems() != null) {
                HashMap<String, BigDecimal> productQty = new HashMap<String, BigDecimal>();
                for (OrderItem item : invoice.getCart().getItems()) {
                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));
                    }
                    productQty.put(item.getProductId(), item.getQuantity());
                }
                invoiceProductQty.put(invoice.getId(), productQty);
            }
            invoiceIds.add(invoice.getId());
        }

        HashMap<String, List<ShippingManifestResult>> shippingManifestHashMap = shippingManifestRepository.findItemsInAsMapByInvoice(token.getCompanyId(), token.getShopId(), Lists.newArrayList(invoiceIds), "{driverId: 1, invoiceId: 1, shipperInformation: 1, receiverInformation: 1, shippingManifestNo: 1, productMetrcInfo: 1,deliveryDate:1,deliveryTime:1,signaturePhoto:1,signatureDateTime:1}");
        HashMap<String, List<PaymentsReceived>> paymentsReceivedHashMap = invoicePaymentsRepository.findItemsInAsMapByInvoice(token.getCompanyId(), token.getShopId(), Lists.newArrayList(invoiceIds), "{amountPaid : 1, paidDate : 1, invoiceId : 1, referenceNo : 1, paymentType : 1}");

        if (shippingManifestHashMap != null && !shippingManifestHashMap.isEmpty()) {
            for (String invoiceId : shippingManifestHashMap.keySet()) {
                List<ShippingManifestResult> shippingManifestList = shippingManifestHashMap.get(invoiceId);
                if (shippingManifestList != null && !shippingManifestList.isEmpty()) {
                    for (ShippingManifestResult shippingManifest : shippingManifestList) {
                        ReceiverInformation receiverInformation = shippingManifest.getReceiverInformation();
                        ShipperInformation shipperInformation = shippingManifest.getShipperInformation();

                        if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                            vendorIds.add(new ObjectId(receiverInformation.getCustomerCompanyId()));
                        }

                        if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCompanyContactId())) {
                            companyContactIds.add(new ObjectId(receiverInformation.getCompanyContactId()));
                        }

                        if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                            vendorIds.add(new ObjectId(shipperInformation.getCustomerCompanyId()));
                        }

                        if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCompanyContactId())) {
                            companyContactIds.add(new ObjectId(shipperInformation.getCompanyContactId()));
                        }

                        if (StringUtils.isNotBlank(shippingManifest.getDriverId())) {
                            employeeIds.add(new ObjectId(shippingManifest.getDriverId()));
                        }

                    }
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, CompanyContact> companyContactHashMap = companyContactRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(companyContactIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));

        for (InvoiceInformation invoice : invoiceList) {
            String invoiceId = invoice.getId();

            List<PaymentsReceived> paymentsReceivedList = paymentsReceivedHashMap.get(invoiceId);
            List<ShippingManifestResult> shippingManifestList = shippingManifestHashMap.get(invoiceId);

            HashMap<String, Vendor> receiverInformationMap = new HashMap<>();
            List<ProductRequestResult> productList = new ArrayList<>();

            BigDecimal amountPaid = new BigDecimal(0);
            BigDecimal balanceDue = new BigDecimal(0);
            BigDecimal changeDue = new BigDecimal(0);
            BigDecimal total = new BigDecimal(0);
            if (invoice.getCart() != null) {
                total = invoice.getCart().getTotal();
            }
            if (paymentsReceivedList != null && !paymentsReceivedList.isEmpty()) {
                for (PaymentsReceived paymentsReceived : paymentsReceivedList) {
                    amountPaid = amountPaid.add(paymentsReceived.getAmountPaid());
                }
                balanceDue = total.subtract(amountPaid);
                if (balanceDue.doubleValue() <= 0) {
                    changeDue = balanceDue.abs();
                    balanceDue = BigDecimal.ZERO;
                }
                invoice.setBalanceDue(balanceDue);
                invoice.setTotal(total);
                invoice.setPaymentsReceived(paymentsReceivedList);
                invoice.setAmountPaid(amountPaid);
            }

            HashMap<String, BigDecimal> shippedQuantity = new HashMap<>();
            if (shippingManifestList != null && !shippingManifestList.isEmpty()) {
                for (ShippingManifestResult shippingManifest : shippingManifestList) {
                    ReceiverInformation receiverInformation = shippingManifest.getReceiverInformation();
                    ShipperInformation shipperInformation = shippingManifest.getShipperInformation();
                    Vendor receiver = null;
                    Vendor shipper = null;
                    CompanyContact shipperContact = null;
                    CompanyContact receiverContact = null;
                    if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                        receiver = vendorHashMap.get(receiverInformation.getCustomerCompanyId());
                        resetVendorForResult(receiver);
                    }
                    if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCompanyContactId())) {
                        receiverContact = companyContactHashMap.get(receiverInformation.getCompanyContactId());
                    }
                    if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCompanyContactId())) {
                        shipperContact = companyContactHashMap.get(shipperInformation.getCompanyContactId());
                    }
                    if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                        shipper = vendorHashMap.get(shipperInformation.getCustomerCompanyId());
                        resetVendorForResult(receiver);
                    }
                    if (StringUtils.isNotBlank(shippingManifest.getDriverId()) && employeeHashMap.containsKey(shippingManifest.getDriverId())) {
                        Employee driverDetails = employeeHashMap.get(shippingManifest.getDriverId());
                        shippingManifest.setDriverLicenceNumber(driverDetails.getDriversLicense());
                        shippingManifest.setDriverName(driverDetails.getFirstName() + " " + driverDetails.getLastName());
                        shippingManifest.setVehicleLicensePlate(driverDetails.getVehicleLicensePlate());
                        shippingManifest.setDriverVinNo(driverDetails.getVinNo());
                        shippingManifest.setVehicleMake(driverDetails.getVehicleMake());
                        shippingManifest.setVehicleModel(driverDetails.getVehicleModel());

                    }

                    shippingManifest.setReceiverCustomerCompany(receiver);
                    shippingManifest.setShipperCompany(shop);
                    shippingManifest.setShipperCustomerCompany(shipper);
                    shippingManifest.setReceiverCompanyContact(receiverContact);
                    shippingManifest.setShipperCompanyContact(shipperContact);
                    shippingManifest.setInvoiceNumber(invoice.getInvoiceNumber());

                    HashMap<String, BigDecimal> productMap = getProductMap(invoice, shippingManifest.getProductMetrcInfo());
                    for (String key : productMap.keySet()) {
                        BigDecimal qty = productMap.get(key);
                        if (shippedQuantity.containsKey(key)) {
                            BigDecimal mapQty = shippedQuantity.get(key);
                            qty = qty.add(mapQty);
                        }
                        shippedQuantity.put(key, qty);
                    }


                }
            }
            invoice.setShippingManifests(shippingManifestList);

            HashMap<String, BigDecimal> invoiceProducts = invoiceProductQty.get(invoice.getId());
            List<RemainingProductInformation> remainingProductInformations = new ArrayList<>();
            if (invoiceProducts != null && shippedQuantity != null) {
                for (String key : invoiceProducts.keySet()) {
                    BigDecimal shippedQty = shippedQuantity.get(key);
                    shippedQty = (shippedQty == null) ? BigDecimal.ZERO : shippedQty;
                    BigDecimal qty = invoiceProducts.get(key);
                    BigDecimal remainingQty = qty.subtract(shippedQty);
                    if (remainingQty.doubleValue() > 0) {


                        Product product = productHashMap.get(key);
                        if (product != null) {
                            RemainingProductInformation remainingProductInformation = new RemainingProductInformation();
                            remainingProductInformation.setProductId(product.getId());
                            remainingProductInformation.setProductName(product.getName());
                            remainingProductInformation.setRemainingQuantity(remainingQty);
                            remainingProductInformation.setRequestQuantity(qty);
                            remainingProductInformations.add(remainingProductInformation);
                        }


                    }
                }
                invoice.setRemainingProductInformations(remainingProductInformations);
            }
            List<ProductRequestResult> productRequestResults = new ArrayList<>();
            if (invoice.getCart() != null && invoice.getCart().getItems() != null) {
                for (OrderItem item : invoice.getCart().getItems()) {
                    ProductRequestResult productRequestResult = new ProductRequestResult();
                    Product product = productHashMap.get(item.getProductId());
                    if (product != null) {
                        productRequestResult = new ProductRequestResult();
                        productRequestResult.setProductName(product.getName());
                        productRequestResult.setProductId(product.getId());
                        productRequestResult.setQuantity(item.getQuantity());
                        productRequestResult.setOrigQuantity(item.getOrigQuantity());
                        BigDecimal totalCost = item.getCost();
                        productRequestResult.setUnitPrice(item.getUnitPrice());
                        productRequestResult.setCost(totalCost);
                        productRequestResult.setDiscount(item.getDiscount());
                        productRequestResults.add(productRequestResult);
                    }
                }
                invoice.setCart(null);
                invoice.setItems(productRequestResults);
            }

            invoice.setVendor(vendorHashMap.get(invoice.getCustomerId()));
            resetVendorForResult(invoice.getVendor());
            invoice.setCompanyContact(companyContactHashMap.get(invoice.getCompanyContactId()));
            Employee employee = employeeHashMap.get(invoice.getSalesPersonId());
            if (employee != null) {
                StringBuilder employeeName = new StringBuilder();
                employeeName.append(employee.getFirstName()).append(" ").append((StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : ""));
                invoice.setSalesPerson(employeeName.toString());
            }

        }

    }

    @Override
    public DateSearchResult<InvoiceInformation> getInvoiceInformationByDate(long startDate, long endDate) {

        if (endDate <= 0) endDate = DateTime.now().getMillis();

        DateSearchResult<InvoiceInformation> invoiceInformationResult = invoiceRepository.getAllInvoicesByDates(token.getCompanyId(), token.getShopId(), startDate, endDate, InvoiceInformation.class, "{invoiceNumber: 1, dueDate: 1, companyId: 1, customerId: 1, companyContactId: 1, cart.items: 1, cart.total: 1,salesPersonId: 1,invoicePaymentStatus:1,invoiceStatus:1,invoiceTerms:1}");
        if (invoiceInformationResult != null && invoiceInformationResult.getValues() != null && !invoiceInformationResult.getValues().isEmpty()) {
            prepareInvoiceInformation(Lists.newArrayList(invoiceInformationResult.getValues()));
        }
        return invoiceInformationResult;
    }

    private void resetVendorForResult(Vendor vendor) {
        if (vendor != null) {
            vendor.setNotes(null);
            vendor.setAssets(null);
            vendor.setBackOrderEnabled(null);
            vendor.setBrands(null);
            vendor.setCredits(null);
        }

    }

    private void updateInvoiceDueDate(Invoice invoice) {
        long dueDate = invoice.getDueDate();
        DateTime invoiceDate = new DateTime(invoice.getInvoiceDate());

        switch (invoice.getInvoiceTerms()) {
            case NET_7:
                dueDate = invoiceDate.plusDays(7).getMillis();
                break;
            case NET_15:
                dueDate = invoiceDate.plusDays(15).getMillis();
                break;
            case NET_30:
                dueDate = invoiceDate.plusDays(30).getMillis();
                break;
            case NET_45:
                dueDate = invoiceDate.plusDays(45).getMillis();
                break;
            case NET_60:
                dueDate = invoiceDate.plusDays(60).getMillis();
                break;
            case CUSTOM_DATE:
                dueDate = invoice.getDueDate();
                break;
            case COD:
                dueDate = (invoice.getDueDate() == 0) ? invoiceDate.getMillis() : invoice.getInvoiceDate();
                break;

        }
        invoice.setDueDate(dueDate);
    }

    @Override
    public List<BulkPostResponse> compositeInvoiceRequest(List<CompositeInvoiceUpdateRequest> request) {
        List<BulkPostResponse> invoiceErrorList = new ArrayList<>();

        if (request != null && !request.isEmpty()) {
            for (CompositeInvoiceUpdateRequest updateRequest : request) {
                try {
                    prepareInvoiceForBulk(updateRequest);
                } catch (Exception e) {
                    BulkPostResponse response = new BulkPostResponse(updateRequest, e.getMessage());
                    invoiceErrorList.add(response);
                }
            }
        }

        return invoiceErrorList;
    }

    private void prepareInvoiceForBulk(CompositeInvoiceUpdateRequest updateRequest) {
        if (StringUtils.isNotBlank(updateRequest.getId())) {
            Invoice dbInvoice = invoiceRepository.getById(updateRequest.getId());
            if (dbInvoice == null) {
                throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
            }
            if (StringUtils.isNotBlank(updateRequest.getCustomerId())) {
                dbInvoice.setCustomerId(updateRequest.getCustomerId());
            }
            if (StringUtils.isNotBlank(updateRequest.getCompanyContactId())) {
                dbInvoice.setCompanyContactId(updateRequest.getCompanyContactId());
            }
            if (StringUtils.isNotBlank(updateRequest.getOrderNumber())) {
                dbInvoice.setOrderNumber(updateRequest.getOrderNumber());
            }
            if (updateRequest.getInvoiceDate() != null) {
                dbInvoice.setInvoiceDate(updateRequest.getInvoiceDate());
            }
            if (updateRequest.getInvoiceTerms() != null) {
                dbInvoice.setInvoiceTerms(updateRequest.getInvoiceTerms());
                updateInvoiceDueDate(dbInvoice);
            }
            if (StringUtils.isNotBlank(updateRequest.getSalesPersonId())) {
                dbInvoice.setSalesPersonId(updateRequest.getSalesPersonId());
            }
            if (updateRequest.getAttachments() != null && !updateRequest.getAttachments().isEmpty()) {
                List<CompanyAsset> attachments = updateRequest.getAttachments();
                List<CompanyAsset> newAttachments = new ArrayList<>();
                for (CompanyAsset asset : attachments) {
                    if (asset.getId() == null) {
                        asset.prepare();
                        newAttachments.add(asset);
                    } else {
                        newAttachments.add(asset);
                    }
                }
                dbInvoice.setAttachments(newAttachments);
            }
            if (StringUtils.isNotBlank(updateRequest.getTermsAndconditions())) {
                dbInvoice.setTermsAndconditions(updateRequest.getTermsAndconditions());
            }
            if (updateRequest.getNotes() != null && !updateRequest.getNotes().isEmpty()) {
                List<Note> notesList = updateRequest.getNotes();
                List<Note> newNotesList = new ArrayList<>();
                for (Note note : notesList) {
                    if (note.getId() == null || note.getId().isEmpty()) {
                        note.prepare();
                        note.setWriterId(token.getActiveTopUser().getUserId());
                        note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                        newNotesList.add(note);
                    } else {
                        newNotesList.add(note);
                    }
                }
                dbInvoice.setNotes(newNotesList);
            }
            if (updateRequest.getCart() != null) {
                //process adjustments
                updateOrderItems(updateRequest.getCart());
                Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
                invoiceInventoryService.calculateTaxes(shop, dbInvoice);
            }
            invoiceRepository.update(updateRequest.getId(), dbInvoice);

            if (updateRequest.getAddPaymentRequest() != null && !updateRequest.getAddPaymentRequest().isEmpty()) {
                List<PaymentsReceivedAddRequest> payments = updateRequest.getAddPaymentRequest();
                for (PaymentsReceivedAddRequest paymentRequest : payments) {
                    paymentRequest.setInvoiceId(dbInvoice.getId());
                    addInvoicePayment(updateRequest.getId(), paymentRequest);
                }
            }
            if (updateRequest.getAddShippingManifestRequest() != null && !updateRequest.getAddShippingManifestRequest().isEmpty()) {
                List<ShippingManifestAddRequest> manifestAddRequests = updateRequest.getAddShippingManifestRequest();
                for (ShippingManifestAddRequest addRequest : manifestAddRequests) {
                    addRequest.setInvoiceId(dbInvoice.getId());
                    shippingManifestService.addShippingManifest(addRequest);
                }
            }
        } else {
            throw new BlazeInvalidArgException(INVOICE, "Invoice id cannot be blank.");
        }
    }

    private HashMap<String, BigDecimal> getProductMap(Invoice invoice, List<ProductMetrcInfo> productMetrcInfos) {
        HashMap<String, BigDecimal> productMap = new HashMap<>();
        HashMap<String, String> orderItemMap = new HashMap<>();

        if (invoice.getCart() == null || invoice.getCart().getItems() == null) {
            return productMap;
        }
        for (OrderItem item : invoice.getCart().getItems()) {
            orderItemMap.put(item.getId(), item.getProductId());
        }

        if (productMetrcInfos != null && !productMetrcInfos.isEmpty()) {
            for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
                String productId = orderItemMap.get(productMetrcInfo.getOrderItemId());

                List<ShippingBatchDetails> shippingBatchDetails = productMetrcInfo.getBatchDetails();
                BigDecimal quantity = BigDecimal.ZERO;
                if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails)) {
                    for (ShippingBatchDetails batchDetails : shippingBatchDetails) {
                        if (!Objects.isNull(shippingBatchDetails)) {
                            quantity = quantity.add(batchDetails.getQuantity());
                        }
                    }
                }
                productMetrcInfo.setQuantity((quantity.doubleValue() > 0) ? quantity : productMetrcInfo.getQuantity());
                productMap.put(productId, productMetrcInfo.getQuantity());

            }
        }
        return productMap;
    }

    private String generateInvoicePaymentNumber(String invoiceId, String invoiceNo) {
        long paymentCount = invoicePaymentsRepository.countAllInvoicePayments(token.getCompanyId(), token.getShopId(), invoiceId);

        return invoiceNo + "-" + (++paymentCount) + "-Pay";
    }

    /**
     * Private method for updating order item id of new prepare cart from old cart items.
     *
     * @param oldCart
     * @param newCart
     */
    private void prepareCartForItems(Cart oldCart, Cart newCart, String invoiceId) {
        if (newCart == null || newCart.getItems() == null || oldCart.getItems() == null) {
            return;
        }
        HashMap<String, String> completedManifestMap = new HashMap<>();
        HashMap<String, String> inProgressManifestMap = new HashMap<>();
        HashMap<String, String> inProgressManifestIdsMap = new HashMap<>();
        SearchResult<ShippingManifestResult> searchResult = shippingManifestService.getShippingManifestByInvoiceId(invoiceId, 0, Integer.MAX_VALUE);
        if (!CollectionUtils.isNullOrEmpty(searchResult.getValues())) {
            for (ShippingManifest manifestResult : searchResult.getValues()) {
                for (ProductMetrcInfo metrcInfo : manifestResult.getProductMetrcInfo()) {
                    if (ShippingManifest.ShippingManifestStatus.Completed == manifestResult.getStatus()) {
                        completedManifestMap.put(metrcInfo.getProductId(), metrcInfo.getOrderItemId());
                    }
                    if (ShippingManifest.ShippingManifestStatus.InProgress == manifestResult.getStatus()) {
                        inProgressManifestMap.put(metrcInfo.getProductId(), metrcInfo.getOrderItemId());
                        inProgressManifestIdsMap.put(metrcInfo.getProductId(), manifestResult.getId());
                    }
                }
            }
        }
        HashMap<String, String> orderItemMap = new HashMap<>();
        List<ObjectId> productList = new ArrayList<>();
        for (OrderItem item : oldCart.getItems()) {
            orderItemMap.put(item.getProductId(), item.getId());
            productList.add(new ObjectId(item.getProductId()));
        }
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productList);
        for (OrderItem item : newCart.getItems()) {
            if (orderItemMap.containsKey(item.getProductId())) {
                String itemId = orderItemMap.get(item.getProductId());
                if (!itemId.equals(item.getId())) {
                    item.setId(itemId);
                }
                orderItemMap.remove(item.getProductId());
            }

        }
        for (String productId : orderItemMap.keySet()) {
            if (completedManifestMap.containsKey(productId)) {
                Product product = productMap.get(productId);
                throw new BlazeInvalidArgException("ShippingManifest", "Shipping Manifest is already Completed for this ".concat(product.getName()).concat(".So you can't delete this."));
            }
            if (inProgressManifestMap.containsKey(productId)) {
                String manifestId = inProgressManifestIdsMap.get(productId);
                shippingManifestRepository.removeByIdSetState(token.getCompanyId(), manifestId);
            }
        }


    }

    @Override
    public PaymentsReceived updatePayment(String invoiceId, String paymentId, PaymentsReceived request) {
        PaymentsReceived dbPaymentsReceived = invoicePaymentsRepository.get(token.getCompanyId(), paymentId);
        if (dbPaymentsReceived == null) {
            throw new BlazeInvalidArgException(PAYMENT, INVOICE_PAYMENT_NOT_FOUND);
        }

        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        if (!dbPaymentsReceived.getInvoiceId().equalsIgnoreCase(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        dbPaymentsReceived.prepare(token.getCompanyId());
        this.preparePaymentNote(dbPaymentsReceived, request.getNotes());
        dbPaymentsReceived.setAmountPaid(request.getAmountPaid());
        long paidDate  = (request.getPaidDate() == null || request.getPaidDate() == 0) ? DateTime.now().getMillis() : request.getPaidDate();
        dbPaymentsReceived.setPaidDate(paidDate);
        dbPaymentsReceived.setPaymentType(request.getPaymentType());
        dbPaymentsReceived.setReferenceNo(StringUtils.isBlank(request.getReferenceNo()) ? "" : request.getReferenceNo());

        double amountPaid = 0;
        SearchResult<PaymentsReceived> paymentsReceivedList = invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceId(token.getCompanyId(), invoiceId, 0, Integer.MAX_VALUE, "{modified:-1}");
        if (paymentsReceivedList != null && paymentsReceivedList.getValues() != null && paymentsReceivedList.getValues().size() > 0) {
            for (PaymentsReceived payments : paymentsReceivedList.getValues()) {
                if (payments.getId().equalsIgnoreCase(paymentId)) {
                    amountPaid += request.getAmountPaid().doubleValue();
                } else {
                    amountPaid += payments.getAmountPaid().doubleValue();
                }
            }
        }

        BigDecimal balanceDue = this.preparePayment(amountPaid, invoice.getCart().getTotal().doubleValue(), invoice);
        shippingManifestRepository.updateShippingManifestDetailsByInvoice(token.getCompanyId(), token.getShopId(), invoice.getId(), invoice.getCart().getTotal(), balanceDue, invoice.getInvoiceStatus());
        invoiceActivityLogService.addInvoiceActivityLog(invoiceId, token.getActiveTopUser().getUserId(), invoice.getInvoiceNumber() + " invoice payment updated");
        invoicePaymentsRepository.update(token.getCompanyId(), paymentId, dbPaymentsReceived);
        invoiceRepository.update(token.getCompanyId(), invoiceId, invoice);

        return dbPaymentsReceived;
    }

    private void preparePaymentNote(PaymentsReceived dbPaymentsReceived, Note note) {
        if (note != null) {
            note.prepare();
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            note.setWriterId(token.getActiveTopUser().getUserId());

            dbPaymentsReceived.setNotes(note);
        }
    }

    @Override
    public void deletePayment(String invoiceId, String paymentId) {

        PaymentsReceived paymentsReceived = invoicePaymentsRepository.get(token.getCompanyId(), paymentId);
        if (paymentsReceived == null) {
            throw new BlazeInvalidArgException(PAYMENT, INVOICE_PAYMENT_NOT_FOUND);
        }
        if (paymentsReceived.isDeleted()) {
            throw new BlazeInvalidArgException(PAYMENT, PAYMENT_ALREADY_DELETED);
        }

        Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        if (!paymentsReceived.getInvoiceId().equalsIgnoreCase(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        double totalInvoiceAmount = invoice.getCart().getTotal().doubleValue();
        double amountPaid = 0;
        SearchResult<PaymentsReceived> paymentsReceivedList = invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceId(token.getCompanyId(), invoiceId, 0, Integer.MAX_VALUE, "{modified:-1}");
        if (paymentsReceivedList != null && paymentsReceivedList.getValues() != null && paymentsReceivedList.getValues().size() > 0) {
            for (PaymentsReceived payments : paymentsReceivedList.getValues()) {
                amountPaid = amountPaid + payments.getAmountPaid().doubleValue();
            }
        }

        amountPaid = amountPaid - paymentsReceived.getAmountPaid().doubleValue();

        BigDecimal balanceDue = this.preparePayment(amountPaid, totalInvoiceAmount, invoice);

        invoicePaymentsRepository.removeByIdSetState(token.getCompanyId(), paymentId);
        shippingManifestRepository.updateShippingManifestDetailsByInvoice(token.getCompanyId(), token.getShopId(), invoice.getId(), new BigDecimal(totalInvoiceAmount), balanceDue, invoice.getInvoiceStatus());
        invoiceActivityLogService.addInvoiceActivityLog(invoiceId, token.getActiveTopUser().getUserId(), invoice.getInvoiceNumber() + " invoice payment deleted");
        invoiceRepository.update(token.getCompanyId(), invoiceId, invoice);
    }

    private BigDecimal preparePayment(double amountPaid, double totalInvoiceAmount, Invoice invoice) {
        BigDecimal balanceDue = new BigDecimal(totalInvoiceAmount - amountPaid);

        if (balanceDue.doubleValue() > 0) {
            invoice.getCart().setBalanceDue(balanceDue);
        } else {
            invoice.getCart().setBalanceDue(BigDecimal.ZERO);
        }

        Invoice.PaymentStatus paymentStatus;
        if (amountPaid == 0) {
            paymentStatus = Invoice.PaymentStatus.UNPAID;
        } else if (balanceDue.doubleValue() <= 0) {
            paymentStatus = Invoice.PaymentStatus.PAID;
            if (Invoice.InvoiceStatus.COMPLETED == invoice.getInvoiceStatus()) {
                invoice.setActive(false);
            }
        } else {
            paymentStatus = Invoice.PaymentStatus.PARTIAL;
        }
        invoice.setInvoicePaymentStatus(paymentStatus);

        return balanceDue;
    }

    @Override
    public void completeInvoiceStatus(String invoiceId , boolean active) {
        Invoice.InvoiceStatus status;
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, "InvoiceId can't be blank");
        }
        final Invoice invoice = invoiceRepository.get(token.getCompanyId(), invoiceId);
        if (invoice == null) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        Invoice.InvoiceStatus currentStatus = invoice.getInvoiceStatus();

        if (active) { // active == True then incomplete the Invoice
            status = Objects.nonNull(invoice.getInvoicePreviousStatus()) ? invoice.getInvoicePreviousStatus() : Invoice.InvoiceStatus.IN_PROGRESS;
            currentStatus = null;
        } else {
            status = Invoice.InvoiceStatus.COMPLETED;
            long count = shippingManifestService.countShippingManifestInProgress(token.getCompanyId(), invoiceId, ShippingManifest.ShippingManifestStatus.InProgress);
            if (count > 0) {
                throw new BlazeInvalidArgException(INVOICE, "Shipping manifest is still in progress for invoice.");
            }
        }
        invoiceRepository.updateInvoiceStatus(token.getCompanyId(), invoiceId, active, status, currentStatus);
    }
}
