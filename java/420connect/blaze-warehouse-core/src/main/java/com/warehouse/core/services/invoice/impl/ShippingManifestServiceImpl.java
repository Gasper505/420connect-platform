package com.warehouse.core.services.invoice.impl;

import com.amazonaws.util.CollectionUtils;
import com.esotericsoftware.kryo.Kryo;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchBundleItems;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CompanyContactService;
import com.fourtwenty.core.services.mgmt.ShipmentService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.mdo.pusher.SimpleRestUtil;
import com.warehouse.core.domain.models.invoice.*;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.domain.repositories.invoice.ShippingManifestRepository;
import com.warehouse.core.event.shippingmanifest.MetrcVerifyEvent;
import com.warehouse.core.event.shippingmanifest.ShippingManifestTransferEvent;
import com.warehouse.core.rest.invoice.request.ManifestRejectRequest;
import com.warehouse.core.rest.invoice.request.ShippingManifestAddRequest;
import com.warehouse.core.rest.invoice.request.UpdateInvoiceShipmentRequest;
import com.warehouse.core.rest.invoice.result.ManifestProductResult;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;
import com.warehouse.core.rest.invoice.result.ShippingBatchDetails;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import com.warehouse.core.services.invoice.InvoiceActivityLogService;
import com.warehouse.core.services.invoice.InvoiceInventoryService;
import com.warehouse.core.services.invoice.InvoiceService;
import com.warehouse.core.services.invoice.ShippingManifestService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class ShippingManifestServiceImpl extends AbstractAuthServiceImpl implements ShippingManifestService {

    private ShippingManifestRepository shippingManifestRepository;
    private CompanyContactService companyContactService;
    private InvoiceRepository invoiceRepository;
    private ProductRepository productRepository;
    private BrandRepository brandRepository;
    private EmployeeRepository employeeRepository;
    private CompanyRepository companyRepository;
    private VendorRepository vendorRepository;
    private InvoiceService invoiceService;
    private ShopRepository shopRepository;
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ShipmentRepository shipmentRepository;
    private AmazonS3Service amazonS3Service;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private InvoiceActivityLogService invoiceActivityLogService;
    @Inject
    private InvoiceInventoryService invoiceInventoryService;
    private BatchQuantityRepository batchQuantityRepository;
    private InventoryRepository inventoryRepository;
    private TerminalRepository terminalRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    private BlazeEventBus eventBus;
    @Inject
    private ShipmentService shipmentService;

    private static final String SHIPPING_MANIFEST = "Shipping manifest";
    private static final String RECEIVER_INFORMATION_NOT_EMPTY = "Receiver Information can't be empty";
    private static final String SHIPPER_INFORMATION_NOT_EMPTY = "Shipper Information can't be empty";
    private static final String SHIPPING_MANIFEST_NOT_EMPTY = "Shipping manifest can't be empty";
    private static final String INVOICE_NOT_EMPTY = "Invoice can't be empty";
    private static final String DRIVER_NOT_EMPTY = "Driver can't be empty";
    private static final String PRODUCT_NOT_EMPTY = "Product can't be empty";
    private static final String INVOICE_NOT_FOUND = "Invoice not found";
    private static final String SHIPPING_MANIFEST_NOT_FOUND = "Shipping manifest not found";
    private static final String INVOICE_ALREADY_COMPLETED = "This invoice has been previously completed/canceled.";
    private static final String PRODUCT_QUANTITY_EXCEEDS = "Shipping product's quantity can not exceeds from respective quantity of invoice in request";
    private static final String RECEIVER_COMPANY_NOT_FOUND = "Receiver company does not found";
    private static final String SHIPPER_COMPANY_NOT_FOUND = "Shipper company does not found";
    private static final String SHIPPING_MANIFEST_RESOURCE = "/shipping_manifest.html";
    private static final String INVOICE_IS_DRAFTED = "Shipping manifest not allowed for drafted invoices.";
    private static final String BATCH_NOT_FOUND = "Product Batch does not exist.";
    private static final String SHIPPING_EMAIL_NOT_EMPTY = "Shipping manifest email can't be empty";
    private static final String PREPACKAGE = "Prepackage";
    private static final String PREPACKAGE_NOT_FOUND = "Prepackage does not found";
    private static final String PREPACKAGE_NOT_BELONG_TO_PRODUCT = "Prepackage item does not belong to the product";
    private static final String PREPACKAGE_NOT_ENOUGH_QUANTITY = "Prepackage does not have enough quantity";
    private static final String SHIPPER_COMPANY_LICENSE_NOT_FOUND = "Shipper company license not found.";
    private static final String SHIPPING_MANIFEST_FORM_RESOURCE = "/shipping_manifest_form.html";
    private static final String INVENTORY_NOT_FOUND = "Inventory does not found or might not be active.";
    private static final String MANIFEST_ALREADY_INPROGRESS = "Shipping manifest not finalized yet.";
    private static final String MANIFEST_ALREADY_REJECTED = "Shipping manifest already rejected.";
    private static final String DECLINE_REASON_NOT_EMPTY = "Decline reason cannot be blank.";
    private static final String DISTRIBUTOR_COMPANY_NOT_FOUND = "Distributor company does not found";
    private static final String DISTRIBUTOR_COMPANY_LICENSE_NOT_FOUND = "Distributor company license not found.";
    private static final String EXTERNAL_LICENSE_NOT_FOUND = "External license not found for batch ";
    private static final String EMPLOYEE_DO_NOT_ALLOWED = "Current employee doesn’t have permission to %s manifest.";
    private static final String PRODUCT_NOT_FOUND = "Shipping product's does not exist.";
    private static final String SAME_METRC_TAG = "Same metrc tag cannot be use multiple times.";
    private static final String  METRC_TRANSFER_TYPE_EMPTY= "Metrc Transfer Type can't be empty.";
    private static final String  CONNECTED_SHIPMENT_ASSIGNED= "Shipping manifest connected to an assigned shipment can't be %s.";
    private static final String  CONNECTED_SHIPMENT_ACCEPTED= "Shipping manifest connected to an accepted shipment can't be %s.";

    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingManifestServiceImpl.class);


    @Inject
    public ShippingManifestServiceImpl(Provider<ConnectAuthToken> token,
                                       ShippingManifestRepository shippingManifestRepository,
                                       CompanyContactService companyContactService,
                                       InvoiceRepository invoiceRepository,
                                       ProductRepository productRepository,
                                       BrandRepository brandRepository,
                                       EmployeeRepository employeeRepository,
                                       CompanyRepository companyRepository, VendorRepository vendorRepository,
                                       InvoiceService invoiceService,
                                       ShopRepository shopRepository,
                                       ProductBatchRepository productBatchRepository,
                                       AmazonS3Service amazonS3Service,
                                       AmazonServiceManager amazonServiceManager,
                                       ConnectConfiguration connectConfiguration,
                                       InvoiceActivityLogService invoiceActivityLogService,
                                       BatchQuantityRepository batchQuantityRepository,
                                       InventoryRepository inventoryRepository, TerminalRepository terminalRepository) {
        super(token);
        this.shippingManifestRepository = shippingManifestRepository;
        this.companyContactService = companyContactService;
        this.invoiceRepository = invoiceRepository;
        this.productRepository = productRepository;
        this.brandRepository = brandRepository;
        this.employeeRepository = employeeRepository;
        this.companyRepository = companyRepository;
        this.vendorRepository = vendorRepository;
        this.invoiceService = invoiceService;
        this.shopRepository = shopRepository;
        this.productBatchRepository = productBatchRepository;
        this.amazonS3Service = amazonS3Service;
        this.amazonServiceManager = amazonServiceManager;
        this.connectConfiguration = connectConfiguration;
        this.invoiceActivityLogService = invoiceActivityLogService;
        this.batchQuantityRepository = batchQuantityRepository;
        this.inventoryRepository = inventoryRepository;
        this.terminalRepository = terminalRepository;
    }

    /**
     * This method is used to add shipping manifest.
     *
     * @param request
     * @return
     */
    @Override
    public ShippingManifestResult addShippingManifest(ShippingManifestAddRequest request) {
        return addShippingManifest(request, false);
    }

    /**
     * Private method to add shipping manifest for invoice when isPrepare is true else only prepare details
     *
     * @param request   : request
     * @param isPrepare : prepare flag
     * @return : manifest
     */
    private ShippingManifestResult addShippingManifest(ShippingManifestAddRequest request, boolean isPrepare) {
            if (StringUtils.isBlank(request.getInvoiceId())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_EMPTY);
        }
        if (StringUtils.isBlank(request.getDriverId())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, DRIVER_NOT_EMPTY);
        }
        List<ProductMetrcInfo> productMetrcInfos = request.getProductMetrcInfo();
        if (productMetrcInfos == null || productMetrcInfos.isEmpty()) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_NOT_EMPTY);
        }

        Invoice invoice = invoiceRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }

        if (invoice.getInvoiceStatus() == Invoice.InvoiceStatus.COMPLETED || invoice.getInvoiceStatus() == Invoice.InvoiceStatus.CANCELLED) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_ALREADY_COMPLETED);
        }
        if (Invoice.InvoiceStatus.DRAFT.equals(invoice.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_IS_DRAFTED);
        }

        ReceiverInformation receiverInformation = request.getReceiverInformation();
        ShipperInformation shipperInformation = request.getShipperInformation();
        DistributorInformation distributorInformation = request.getDistributorInformation();


        verifyShipperAndReceiverInformation(receiverInformation, shipperInformation, distributorInformation);

        receiverInformation = (receiverInformation == null) ? new ReceiverInformation() : receiverInformation;
        shipperInformation = (shipperInformation == null) ? new ShipperInformation() : shipperInformation;
        distributorInformation = (distributorInformation == null) ? new DistributorInformation() : distributorInformation;

        if (StringUtils.isBlank(receiverInformation.getCustomerCompanyId())) {
            receiverInformation.setCustomerCompanyId(invoice.getCustomerId());
        }
        if (StringUtils.isBlank(shipperInformation.getCompanyId()) && StringUtils.isBlank(shipperInformation.getCustomerCompanyId())) {
            shipperInformation.setCompanyId(token.getShopId());
        }
        if (StringUtils.isBlank(distributorInformation.getShopId()) && StringUtils
            .isBlank(distributorInformation.getCustomerCompanyId())) {
            distributorInformation.setShopId(token.getShopId());
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), request.getDriverId());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee not found");
        }

        ShippingManifest shippingManifest = new ShippingManifestResult();
        shippingManifest.prepare(token.getCompanyId());
        shippingManifest.setShopId(token.getShopId());
        shippingManifest.setInvoiceId(request.getInvoiceId());
        shippingManifest.setDriverId(request.getDriverId());
        shippingManifest.setPONumber(request.getPONumber());
        shippingManifest.setShippedDateTime(request.getShippedDateTime());
        shippingManifest.setEstimatedArrival(request.getEstimatedArrival());
        shippingManifest.setReceiverInformation(receiverInformation);
        shippingManifest.setShipperInformation(shipperInformation);
        shippingManifest.setDistributorInformation(distributorInformation);
        shippingManifest.setExternalLicense(request.getExternalLicense());
        BigDecimal amountPaid = new BigDecimal(0);
        BigDecimal totalInvoiceAmount = new BigDecimal(0);
        if (invoice.getCart() != null) {
            totalInvoiceAmount = invoice.getCart().getTotal();
        }
        totalInvoiceAmount = (totalInvoiceAmount == null) ? new BigDecimal(0) : totalInvoiceAmount;
        Map<String, Object> amountMap = invoiceService.calculateBalanceDue(invoice.getId(), totalInvoiceAmount, amountPaid);
        BigDecimal balanceDue = (BigDecimal) amountMap.get("balanceDue");

        String shippingManifestOrdinal = createShipmentNumber(invoice.getInvoiceNumber(), invoice.getId());

        HashMap<String, BigDecimal> productMap = getProductMapByItem(invoice, productMetrcInfos, true);

        List<ObjectId> batchIds = new ArrayList<>();
        List<ObjectId> prepackageProductItemIds = new ArrayList<>();
        for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
            BigDecimal totalShippedQuantity = productMetrcInfo.getQuantity();
            totalShippedQuantity = (totalShippedQuantity == null) ? BigDecimal.ZERO : totalShippedQuantity;
            if ((productMetrcInfo.getBatchDetails() == null || productMetrcInfo.getBatchDetails().isEmpty())
                    && totalShippedQuantity.doubleValue() == 0) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping quantity cannot be zero.");
            }
            for (ShippingBatchDetails result : productMetrcInfo.getBatchDetails()) {
                if (StringUtils.isNotBlank(result.getBatchId()) && ObjectId.isValid(result.getBatchId())) {
                    batchIds.add(new ObjectId(result.getBatchId()));
                }
                if (StringUtils.isNotBlank(result.getPrepackageItemId()) && ObjectId.isValid(result.getPrepackageItemId())) {
                    prepackageProductItemIds.add(new ObjectId(result.getPrepackageItemId()));
                }
            }
        }

        productMetrcInfos = getProductMetrcInfo(invoice, batchIds, request.getProductMetrcInfo(), prepackageProductItemIds);

        HashMap<String, Object> returnMap = getInvoiceStatusByProducts(productMap, invoice, "", true);
        HashMap<String, BigDecimal> remainingProductMap = (HashMap<String, BigDecimal>) returnMap.get("remainingProduct");

        shippingManifest.setShippingManifestNo(shippingManifestOrdinal);
        shippingManifest.setInvoiceCreatedDate(invoice.getCreated());
        shippingManifest.setInvoiceAmount(totalInvoiceAmount);
        shippingManifest.setInvoiceStatus(invoice.getInvoiceStatus());
        shippingManifest.setInvoiceBalanceDue(balanceDue);
        shippingManifest.setSignaturePhoto(request.getSignaturePhoto());
        if (request.getSignaturePhoto() != null) {
            shippingManifest.setSignatureDateTime(DateTime.now().getMillis());
        }

        shippingManifest.setDeliveryDate(request.getDeliveryDate());
        shippingManifest.setDeliveryTime(request.getDeliveryTime());
        updateNotes(request.getNotes());
        shippingManifest.setNotes(request.getNotes());

        shippingManifest.setProductMetrcInfo(productMetrcInfos);

        if (isPrepare) {
            return (ShippingManifestResult) shippingManifest;
        }
        ShippingManifest dbShippingManifest = shippingManifestRepository.save(shippingManifest);
        invoiceActivityLogService.addInvoiceActivityLog(invoice.getId(), token.getActiveTopUser().getUserId(), String.format("Shipping manifest %s created", shippingManifest.getShippingManifestNo()));
        ShippingManifestResult result = (ShippingManifestResult) dbShippingManifest;

        if (result != null) {
            result.setRemainingProductMap(remainingProductMap);
            prepareShippingManifest(result);
        }

        return result;
    }

    /**
     * This method is used to get all shipping manifest according invoice.
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ShippingManifestResult> getAllShippingManifest(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ShippingManifestResult> searchResult = shippingManifestRepository.getAllShippingManifest(token.getCompanyId(), "{modified:-1}", start, limit);
        if (searchResult.getValues().size() > 0) {
            for (ShippingManifestResult shippingManifestResult : searchResult.getValues()) {
                prepareShippingManifest(shippingManifestResult);
            }

        }
        return searchResult;
    }

    @Override
    public ShippingManifestResult getShippingManifestById(String shippingManifestId) {
        if (StringUtils.isBlank(shippingManifestId)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_EMPTY);
        }
        ShippingManifestResult shippingManifestResult = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (shippingManifestResult == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        prepareShippingManifest(shippingManifestResult, true);
        return shippingManifestResult;

    }

    @Override
    public SearchResult<ShippingManifestResult> getShippingManifestByInvoiceId(String invoiceId, int start, int limit) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_EMPTY);
        }
        SearchResult<ShippingManifestResult> searchResult = shippingManifestRepository.getShippingManifestByInvoiceId(token.getCompanyId(), invoiceId, "{modified:-1}", start, limit);
        if (searchResult.getValues().size() > 0) {
            for (ShippingManifestResult shippingManifestResult : searchResult.getValues()) {
                prepareShippingManifest(shippingManifestResult);
            }

        }
        return searchResult;
    }

    private void prepareShippingManifest(final ShippingManifestResult shippingManifestResult) {
        prepareShippingManifest(shippingManifestResult, false);
    }

    private void prepareShippingManifest(final ShippingManifestResult shippingManifestResult, boolean attachChild) {
        BigDecimal exciseTax = new BigDecimal(0);
        BigDecimal productQuantity = new BigDecimal(0);
        BigDecimal totalAmount;
        prepareShippingManifestResultForReceiver(shippingManifestResult);
        prepareShippingManifestResultForShipper(shippingManifestResult);
        prepareShippingManifestResultForDistributor(shippingManifestResult);

        Invoice dbInvoice = invoiceRepository.get(token.getCompanyId(), shippingManifestResult.getInvoiceId());
        if (dbInvoice != null) {
            shippingManifestResult.setInvoiceNumber(dbInvoice.getInvoiceNumber());
        }
        List<ObjectId> productObjectList = new ArrayList<>();
        HashMap<String, String> itemProductMap = new HashMap<>();
        HashMap<String, String> itemPrepackageMap = new HashMap<>();
        HashMap<String, BigDecimal> itemPriceMap = new HashMap<>();
        if (dbInvoice.getCart() != null && dbInvoice.getCart().getItems() != null) {
            for (OrderItem item : dbInvoice.getCart().getItems()) {
                productObjectList.add(new ObjectId(item.getProductId()));
                itemProductMap.put(item.getId(), item.getProductId());
                itemPriceMap.put(item.getId(), item.getUnitPrice());
                itemPrepackageMap.put(item.getId(), item.getPrepackageId());
            }
        }
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), productObjectList);
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId());

        if (shippingManifestResult.getProductMetrcInfo() != null) {

            List<ObjectId> productBatchIds = new ArrayList<>();

            for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
                if (!CollectionUtils.isNullOrEmpty(productMetrcInfo.getBatchDetails())) {
                    for (ShippingBatchDetails shippingBatchDetails : productMetrcInfo.getBatchDetails()) {
                        if (StringUtils.isNotBlank(shippingBatchDetails.getBatchId()) && ObjectId.isValid(shippingBatchDetails.getBatchId())) {
                            productBatchIds.add(new ObjectId(shippingBatchDetails.getBatchId()));
                        }
                    }
                }
            }


            HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), productBatchIds);
            if (attachChild) {
                prepareBundleBatches(shippingManifestResult, productBatchHashMap, productHashMap);
            }
            List<ManifestProductResult> productResults = new ArrayList<>();

            for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
                String productId = itemProductMap.get(productMetrcInfo.getOrderItemId());
                Product dbProduct = productHashMap.get(productId);
                if (dbProduct == null) {
                    continue;
                }
                BigDecimal unitPrice = BigDecimal.ZERO;
                if (itemPriceMap.containsKey(productMetrcInfo.getOrderItemId())) {
                    unitPrice = itemPriceMap.get(productMetrcInfo.getOrderItemId());
                }
                ManifestProductResult productResult = new ManifestProductResult();
                productResult.setProductName(dbProduct.getName());
                productResult.setProductId(dbProduct.getId());
                if (dbProduct.getCategory() != null) {
                    productResult.setCategory(dbProduct.getCategory().getName());
                    productResult.setUnitType(dbProduct.getCategory().getUnitType());
                } else {
                    productResult.setCategory("");
                    productResult.setUnitType(ProductCategory.UnitType.units);
                }

                productResult.setType((dbProduct.getProductSaleType() == null) ? "" : dbProduct.getProductSaleType().toString());
                productResult.setRate(unitPrice);
                productResult.setQuantity(productMetrcInfo.getQuantity());
                //TODO Calculate Excise Tax and add it to total amount
                totalAmount = unitPrice.multiply(productMetrcInfo.getQuantity());
                totalAmount = totalAmount.add(exciseTax);
                productResult.setAmount(totalAmount);
                if (dbProduct.getBrandId() != null && brandHashMap.containsKey(dbProduct.getBrandId())) {
                    Brand dbBrand = brandHashMap.get(dbProduct.getBrandId());
                    productResult.setBrandName(dbBrand.getName());
                }
                productResult.setPrepackageId(itemPrepackageMap.get(productMetrcInfo.getOrderItemId()));
                productResults.add(productResult);

                if (!CollectionUtils.isNullOrEmpty(productMetrcInfo.getBatchDetails())) {
                    for (ShippingBatchDetails shippingBatchDetails : productMetrcInfo.getBatchDetails()) {
                        ProductBatch batch = productBatchHashMap.get(shippingBatchDetails.getBatchId());
                        if (batch != null) {
                            shippingBatchDetails.setBatchSku(batch.getSku());
                            shippingBatchDetails.setTrackHarvestBatchId(batch.getTrackHarvestBatch());
                            shippingBatchDetails.setBatchPurchaseDate(batch.getPurchasedDate());
                            shippingBatchDetails.setMetrcLabel(batch.getTrackPackageLabel());
                        }
                    }
                }
            }
            shippingManifestResult.setProductList(productResults);

        }

        Employee employee = employeeRepository.get(token.getCompanyId(), shippingManifestResult.getDriverId());
        if (employee != null) {
            shippingManifestResult.setDriverName(employee.getFirstName() + " " + employee.getLastName());
            shippingManifestResult.setDriverLicenceNumber(employee.getDriversLicense());
            shippingManifestResult.setVehicleMake(employee.getVehicleMake());
            shippingManifestResult.setVehicleModel(employee.getVehicleModel());
            shippingManifestResult.setVehicleLicensePlate(employee.getVehicleLicensePlate());
            shippingManifestResult.setDriverVinNo(employee.getVinNo());
            shippingManifestResult.setDriverPhone(employee.getPhoneNumber());
            shippingManifestResult.setDriverEmail(employee.getEmail());
        }

        if (shippingManifestResult.getRemainingProductMap() == null || shippingManifestResult.getRemainingProductMap().isEmpty()) {
            HashMap<String, Object> returnMap = getInvoiceStatusByProducts(new HashMap<>(), dbInvoice, "", false);
            HashMap<String, BigDecimal> remainingMap = (HashMap<String, BigDecimal>) returnMap.get("remainingProduct");
            shippingManifestResult.setRemainingProductMap(remainingMap);
        }

    }

    private void prepareBundleBatches(ShippingManifestResult result, HashMap<String, ProductBatch> batchHashMap, HashMap<String, Product> productHashMap) {

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> batchIds = new ArrayList<>();

        for (Map.Entry<String, ProductBatch> batches : batchHashMap.entrySet()) {
            ProductBatch productBatch = batches.getValue();
            if (productBatch == null) {
                continue;
            }
            Product product = productHashMap.get(productBatch.getProductId());
            if (product == null) {
                continue;
            }
            if (product.getProductType() == Product.ProductType.BUNDLE) {
                for (BatchBundleItems bundleItem : productBatch.getBundleItems()) {
                    if (CollectionUtils.isNullOrEmpty(bundleItem.getBatchItems())) {
                        continue;
                    }
                    if (StringUtils.isNotBlank(bundleItem.getProductId()) || !ObjectId.isValid(bundleItem.getProductId())) {
                        productIds.add(new ObjectId(bundleItem.getProductId()));
                    }
                    for (BatchBundleItems.BatchItems batchBundleItems : bundleItem.getBatchItems()) {
                        if (StringUtils.isBlank(batchBundleItems.getBatchId()) || !ObjectId.isValid(batchBundleItems.getBatchId())) {
                            continue;
                        }
                        batchIds.add(new ObjectId(batchBundleItems.getBatchId()));
                    }
                }
            }
        }

        productHashMap.putAll(productRepository.listAsMap(token.getCompanyId(), productIds));
        batchHashMap.putAll(productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds)));

        for (ProductMetrcInfo productMetrcInfo : result.getProductMetrcInfo()) {
            Product product = productHashMap.get(productMetrcInfo.getProductId());
            if (product == null) {
                continue;
            }

            for (ShippingBatchDetails shippingBatchDetails : productMetrcInfo.getBatchDetails()) {
                if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails.getMetrcPackageDetails())) {
                    shippingBatchDetails.getMetrcPackageDetails().forEach(metrcPackageDetails -> {
                        ProductBatch productBatch = batchHashMap.get(metrcPackageDetails.getBatchId());
                        if (productBatch != null) {
                            metrcPackageDetails.setBatchSku(productBatch.getSku());
                            metrcPackageDetails.setMetrcLabel(productBatch.getTrackPackageLabel());
                            Product dbProduct = productHashMap.get(productBatch.getProductId());
                            if (dbProduct != null) {
                                metrcPackageDetails.setProductId(dbProduct.getId());
                                metrcPackageDetails.setProductName(dbProduct.getName());

                            }
                        }

                    });
                    continue;
                }

                if (product.getProductType() == Product.ProductType.REGULAR) {
                    continue;
                }
                ProductBatch productBatch = batchHashMap.get(shippingBatchDetails.getBatchId());
                if (productBatch == null) {
                    continue;
                }

                if (product.getProductType() == Product.ProductType.BUNDLE && !CollectionUtils.isNullOrEmpty(productBatch.getBundleItems())) {
                    for (BatchBundleItems bundleItem : productBatch.getBundleItems()) {
                        for (BatchBundleItems.BatchItems batchItem : bundleItem.getBatchItems()) {
                            ProductBatch dbProductBatch = batchHashMap.get(batchItem.getBatchId());
                            if (dbProductBatch == null) {
                                continue;
                            }

                            ShippingBatchDetails.MetrcPackageDetails packageDetails = new ShippingBatchDetails.MetrcPackageDetails();
                            packageDetails.setBatchId(dbProductBatch.getId());
                            packageDetails.setQuantity(shippingBatchDetails.getQuantity().multiply(batchItem.getQuantity()));
                            packageDetails.setBatchSku(dbProductBatch.getSku());
                            packageDetails.setMetrcLabel(dbProductBatch.getTrackPackageLabel());
                            Product dbProduct = productHashMap.get(dbProductBatch.getProductId());
                            if (dbProduct != null) {
                                packageDetails.setProductId(dbProduct.getId());
                                packageDetails.setProductName(dbProduct.getName());
                            }
                            shippingBatchDetails.getMetrcPackageDetails().add(packageDetails);

                        }
                    }
                }
            }
        }

    }

    private void prepareShippingManifestResultForReceiver(final ShippingManifestResult shippingManifestResult) {
        if (StringUtils.isNotBlank(shippingManifestResult.getReceiverInformation().getCustomerCompanyId())) {
            Vendor receiverCustomerCompany = vendorRepository.getById(shippingManifestResult.getReceiverInformation().getCustomerCompanyId());
            if (receiverCustomerCompany != null) {
                shippingManifestResult.setReceiverCustomerCompany(receiverCustomerCompany);
            }
        }
        if (StringUtils.isNotBlank(shippingManifestResult.getReceiverInformation().getCompanyContactId())) {
            CompanyContact receiverCompanyContact = companyContactService.getCompanyContactById(shippingManifestResult.getReceiverInformation().getCompanyContactId(), false);
            if (receiverCompanyContact != null) {
                shippingManifestResult.setReceiverCompanyContact(receiverCompanyContact);
            }

        }
    }

    private void prepareShippingManifestResultForShipper(final ShippingManifestResult shippingManifestResult) {
        if (StringUtils.isNotBlank(shippingManifestResult.getShipperInformation().getCustomerCompanyId())) {
            Vendor shipperCustomerCompany = vendorRepository.getById(shippingManifestResult.getShipperInformation().getCustomerCompanyId());
            if (shipperCustomerCompany != null) {
                shippingManifestResult.setShipperCustomerCompany(shipperCustomerCompany);
            }
        } else {
            String companyId = token.getShopId();
            if (StringUtils.isNotBlank(shippingManifestResult.getShipperInformation().getCompanyId())) {
                companyId = shippingManifestResult.getShipperInformation().getCompanyId();
            }
            ShopLimitedView shipperShop = shopRepository.getById(companyId, ShopLimitedView.class);
            if (shipperShop != null) {
                shippingManifestResult.setShipperCompany(shipperShop);
            }

        }
        if (StringUtils.isNotBlank(shippingManifestResult.getShipperInformation().getCompanyContactId())) {
            CompanyContact shipperCompanyContact = companyContactService.getCompanyContactById(shippingManifestResult.getShipperInformation().getCompanyContactId(), false);
            if (shipperCompanyContact != null) {
                shippingManifestResult.setShipperCompanyContact(shipperCompanyContact);
            }
        } else if (StringUtils.isNoneBlank(shippingManifestResult.getShipperInformation().getEmployeeId())) {
             Employee shipperEmployee = employeeRepository.get(token.getCompanyId(), shippingManifestResult.getShipperInformation().getEmployeeId());
             if (shipperEmployee != null) {
                 shippingManifestResult.setShipperEmployee(shipperEmployee);
             }
        }


    }

    /**
     * Create shipping manifest number from invoice number
     *
     * @param invoiceNo : invoice number
     * @param invoiceId : invoice ID
     */
    private String createShipmentNumber(String invoiceNo, String invoiceId) {
        long shipmentManifestNo = 0;
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_EMPTY);
        }
        Long shippingManifestCount = shippingManifestRepository.countShippingManifestForInvoiceId(token.getCompanyId(), invoiceId);

        shipmentManifestNo = (shippingManifestCount == null) ? 0 : shippingManifestCount;
        shipmentManifestNo++;
        return invoiceNo + "-" + shipmentManifestNo + "-SM";
    }

    @Override
    public ShippingManifestResult updateShippingManifest(String shippingManifestId, ShippingManifest shippingManifest) {
        return updateShippingManifest(shippingManifestId, shippingManifest, false);
    }

    /**
     * Private method to update manifest when isPrepare is false
     * @param shippingManifestId : manifest Id
     * @param shippingManifest   : manifest request
     * @param isPrepare          : is prepare flag
     * @return : manifest
     */
    private ShippingManifestResult updateShippingManifest(String shippingManifestId, ShippingManifest shippingManifest, boolean isPrepare) {
            ShippingManifest dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Completed) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping manifest already completed");
        }
        if (StringUtils.isBlank(shippingManifest.getDriverId())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, DRIVER_NOT_EMPTY);
        }
        if (StringUtils.isBlank(shippingManifest.getInvoiceId())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_EMPTY);
        }


        Invoice invoice = invoiceRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), shippingManifest.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }

        if (invoice.getInvoiceStatus() == Invoice.InvoiceStatus.COMPLETED || invoice.getInvoiceStatus() == Invoice.InvoiceStatus.CANCELLED) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_ALREADY_COMPLETED);
        }
        if (Invoice.InvoiceStatus.DRAFT.equals(invoice.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_IS_DRAFTED);
        }
        HashMap<String, BigDecimal> productMap = getProductMapByItem(invoice, shippingManifest.getProductMetrcInfo(), true);
        if (productMap == null || productMap.isEmpty()) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_NOT_EMPTY);
        }

        ReceiverInformation receiverInformation = shippingManifest.getReceiverInformation();
        ShipperInformation shipperInformation = shippingManifest.getShipperInformation();
        DistributorInformation distributorInformation = shippingManifest.getDistributorInformation();

        verifyShipperAndReceiverInformation(receiverInformation, shipperInformation,distributorInformation);

        receiverInformation = (receiverInformation == null) ? new ReceiverInformation() : receiverInformation;
        shipperInformation = (shipperInformation == null) ? new ShipperInformation() : shipperInformation;

        if (StringUtils.isBlank(receiverInformation.getCustomerCompanyId())) {
            receiverInformation.setCustomerCompanyId(invoice.getCustomerId());
        }
        if (StringUtils.isBlank(shipperInformation.getCompanyId()) && StringUtils.isBlank(shipperInformation.getCustomerCompanyId())) {
            shipperInformation.setCompanyId(token.getShopId());
        }

        Employee driverEmployee = employeeRepository.get(token.getCompanyId(), shippingManifest.getDriverId());
        if (driverEmployee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee not found");
        }

        dbShippingManifest.setInvoiceId(shippingManifest.getInvoiceId());
        dbShippingManifest.setDriverId(shippingManifest.getDriverId());
        dbShippingManifest.setPONumber(shippingManifest.getPONumber());
        dbShippingManifest.setShippedDateTime(shippingManifest.getShippedDateTime());
        dbShippingManifest.setEstimatedArrival(shippingManifest.getEstimatedArrival());
        dbShippingManifest.setReceiverInformation(receiverInformation);
        dbShippingManifest.setShipperInformation(shipperInformation);
        dbShippingManifest.setSignaturePhoto(shippingManifest.getSignaturePhoto());
        dbShippingManifest.setExternalLicense(shippingManifest.getExternalLicense());
        dbShippingManifest.setDistributorInformation(shippingManifest.getDistributorInformation());
        if (shippingManifest.getSignaturePhoto() != null) {
            dbShippingManifest.setSignatureDateTime(shippingManifest.getSignaturePhoto().getCreated());
        }

        dbShippingManifest.setShipperInformation(shippingManifest.getShipperInformation());
        BigDecimal totalInvoiceAmount = new BigDecimal(0);
        BigDecimal amountPaid = new BigDecimal(0);
        if (invoice.getCart() != null) {
            totalInvoiceAmount = invoice.getCart().getTotal();
        }
        totalInvoiceAmount = (totalInvoiceAmount == null) ? new BigDecimal(0) : totalInvoiceAmount;
        Map<String, Object> amountMap = invoiceService.calculateBalanceDue(invoice.getId(), totalInvoiceAmount, amountPaid);
        BigDecimal balanceDue = (BigDecimal) amountMap.get("balanceDue");

        HashMap<String, Object> returnMap = getInvoiceStatusByProducts(productMap, invoice, shippingManifestId, true);

        HashMap<String, BigDecimal> remainingProductMap = (HashMap<String, BigDecimal>) returnMap.get("remainingProduct");

        dbShippingManifest.setInvoiceCreatedDate(invoice.getCreated());
        dbShippingManifest.setInvoiceAmount(totalInvoiceAmount);
        dbShippingManifest.setInvoiceStatus(invoice.getInvoiceStatus());
        dbShippingManifest.setInvoiceBalanceDue(balanceDue);

        dbShippingManifest.setDeliveryTime(shippingManifest.getDeliveryTime());
        dbShippingManifest.setDeliveryDate(shippingManifest.getDeliveryDate());
        updateNotes(shippingManifest.getNotes());
        dbShippingManifest.setNotes(shippingManifest.getNotes());

        List<ObjectId> batchIds = new ArrayList<>();
        List<ObjectId> prepackageProductItemIds = new ArrayList<>();
        for (ProductMetrcInfo productMetrcInfo : shippingManifest.getProductMetrcInfo()) {
            BigDecimal totalShippedQuantity = productMetrcInfo.getQuantity();
            totalShippedQuantity = (totalShippedQuantity == null) ? BigDecimal.ZERO : totalShippedQuantity;
            if ((productMetrcInfo.getBatchDetails() == null || productMetrcInfo.getBatchDetails().isEmpty())
                    && totalShippedQuantity.doubleValue() == 0) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping quantity cannot be zero");
            }
            for (ShippingBatchDetails result : productMetrcInfo.getBatchDetails()) {
                if (StringUtils.isNotBlank(result.getBatchId()) && ObjectId.isValid(result.getBatchId())) {
                    batchIds.add(new ObjectId(result.getBatchId()));
                }
                if (StringUtils.isNotBlank(result.getPrepackageItemId()) && ObjectId.isValid(result.getPrepackageItemId())) {
                    prepackageProductItemIds.add(new ObjectId(result.getPrepackageItemId()));
                }
            }

        }
        List<ProductMetrcInfo> productMetrcInfos = getProductMetrcInfo(invoice, batchIds, shippingManifest.getProductMetrcInfo(), prepackageProductItemIds);
        dbShippingManifest.setProductMetrcInfo(productMetrcInfos);


        dbShippingManifest.setStatus(shippingManifest.getStatus());

        dbShippingManifest.setApprovalDate(DateTime.now().getMillis());
        dbShippingManifest.setApproverEmployeeId(token.getActiveTopUser().getUserId());

        dbShippingManifest.setDeclineReason(shippingManifest.getDeclineReason());

        if (shippingManifest.getManagerSignature() != null) {
            CompanyAsset asset = shippingManifest.getManagerSignature();
            asset.prepare(token.getCompanyId());
            dbShippingManifest.setManagerSignature(asset);
        }

        if (isPrepare) {
            return (ShippingManifestResult) dbShippingManifest;
        }
        dbShippingManifest = shippingManifestRepository.update(shippingManifestId, dbShippingManifest);
        invoiceActivityLogService.addInvoiceActivityLog(invoice.getId(), token.getActiveTopUser().getUserId(), String.format("Shipping manifest %s updated", shippingManifest.getShippingManifestNo()));
        ShippingManifestResult result = (ShippingManifestResult) dbShippingManifest;

        if (result != null) {
            result.setRemainingProductMap(remainingProductMap);
            prepareShippingManifest(result);
        }
        return result;

    }

    /**
     * This method is used to delete shipping manifest by given Id
     *
     * @param shippingManifestId
     */
    @Override
    public void deleteShippingManifest(String shippingManifestId) {
        if (StringUtils.isBlank(shippingManifestId) || !ObjectId.isValid(shippingManifestId)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping Manifest Id must be valid");
        }
        ShippingManifest dbShippingManifest = shippingManifestRepository.getById(shippingManifestId);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Completed) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping manifest already completed");
        }

        if (isAssignedConnectedShipment(shippingManifestId)){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(CONNECTED_SHIPMENT_ASSIGNED, "deleted"));
        }else if (isAcceptedConnectedShipment(shippingManifestId)){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(CONNECTED_SHIPMENT_ACCEPTED, "deleted"));
        }else{
            rejectConnectedShipment(shippingManifestId);
        }

        String invoiceId = dbShippingManifest.getInvoiceId();
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Invoice not found");
        }
        Invoice dbInvoice = invoiceRepository.getById(invoiceId);
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Invoice not found");
        }
        if (dbInvoice.getInvoiceStatus() == Invoice.InvoiceStatus.COMPLETED || dbInvoice.getInvoiceStatus() == Invoice.InvoiceStatus.CANCELLED) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_ALREADY_COMPLETED);
        }
        clearMetrcFields(dbShippingManifest);
        dbShippingManifest = shippingManifestRepository.update(token.getCompanyId(), shippingManifestId, dbShippingManifest);
        shippingManifestRepository.removeById(shippingManifestId);
        invoiceActivityLogService.addInvoiceActivityLog(invoiceId, token.getActiveTopUser().getUserId(), String.format("Shipping manifest %s deleted", dbShippingManifest.getShippingManifestNo()));
        HashMap<String, Object> returnMap = getInvoiceStatusByProducts(new HashMap<>(), dbInvoice, shippingManifestId, false);

        HashMap<String, BigDecimal> remainingProductMap = (HashMap<String, BigDecimal>) returnMap.get("remainingProduct");

    }

    /**
     * This is public method used to get invoice status from all shipping manifest product quantities
     *
     * @param requestMap
     * @param dbInvoice
     * @param shippingManifestId
     * @return
     */
    public HashMap<String, Object> getInvoiceStatusByProducts(HashMap<String, BigDecimal> requestMap, Invoice dbInvoice, String shippingManifestId, boolean checkRequired) {
        HashMap<String, BigDecimal> shippedProductMap = new HashMap<>();
        HashMap<String, BigDecimal> invoiceProductMap = new HashMap<>();
        HashMap<String, String> orderItemMap = new HashMap<>();
        HashMap<String, BigDecimal> remainingProductMap = new HashMap<>();

        /* Check if product in request map contains in invoice product request*/
        if (dbInvoice.getCart() != null && dbInvoice.getCart().getItems() != null && !dbInvoice.getCart().getItems().isEmpty()) {
            for (OrderItem productRequest : dbInvoice.getCart().getItems()) {
                BigDecimal requestQuantity = productRequest.getQuantity();
                String itemId = productRequest.getId();
                invoiceProductMap.put(itemId, requestQuantity.add(invoiceProductMap.getOrDefault(itemId, BigDecimal.ZERO)));
                orderItemMap.put(itemId, productRequest.getProductId());
            }
            if (checkRequired) {
                for (String key : requestMap.keySet()) {
                    if (StringUtils.isBlank(key)) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_NOT_EMPTY);
                    }
                    if (!invoiceProductMap.containsKey(key)) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Product does not found in invoice");
                    }
                    if (requestMap.get(key) == null || requestMap.get(key).doubleValue() == 0) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Quantity for product cannot be zero");
                    }
                    if (requestMap.get(key).doubleValue() > invoiceProductMap.get(key).doubleValue()) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_QUANTITY_EXCEEDS);
                    }
                }
            }
        } else if (checkRequired) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Product does not found in invoice");
        }


        /* get all shipping manifest for given invoice id*/

        SearchResult<ShippingManifestResult> searchResult = shippingManifestRepository.getShippingManifestByInvoiceId(token.getCompanyId(), dbInvoice.getId(), "{modified:-1}", 0, Integer.MAX_VALUE);
        List<ShippingManifest.ShippingManifestStatus> manifestStatusList = new ArrayList<>();
        for (ShippingManifest shippingManifest : searchResult.getValues()) {
            if (shippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Rejected) {
                continue;
            }
            /* In case of update not include shipping manifest for given id */
            manifestStatusList.add(shippingManifest.getStatus());
            if (shippingManifest.getId().equalsIgnoreCase(shippingManifestId))
                continue;

            HashMap<String, BigDecimal> dbShippedProductMap = getProductMapByItem(dbInvoice, shippingManifest.getProductMetrcInfo(), checkRequired);

            for (String key : dbShippedProductMap.keySet()) {
                if (shippedProductMap.containsKey(key)) {
                    BigDecimal quantity = dbShippedProductMap.get(key);
                    quantity = quantity.add(shippedProductMap.get(key));
                    shippedProductMap.put(key, quantity);
                } else {
                    shippedProductMap.put(key, dbShippedProductMap.get(key));
                }
            }
        }

        boolean isInvoiceCompleted = false;

        for (String invoiceProductKey : invoiceProductMap.keySet()) {

            String productId = orderItemMap.get(invoiceProductKey);

            BigDecimal invoiceQuantity = invoiceProductMap.get(invoiceProductKey);

            BigDecimal shippedQuantity = shippedProductMap.get(invoiceProductKey);
            shippedQuantity = (shippedQuantity == null) ? new BigDecimal(0) : shippedQuantity;

            BigDecimal requestQuantity = requestMap.get(invoiceProductKey);
            requestQuantity = (requestQuantity == null) ? new BigDecimal(0) : requestQuantity;

            requestQuantity = requestQuantity.add(shippedQuantity);

            if (requestQuantity.doubleValue() > invoiceQuantity.doubleValue()) {
                if (checkRequired) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_QUANTITY_EXCEEDS);
                }
                remainingProductMap.put(invoiceProductKey, new BigDecimal(0));
            } else if (requestQuantity.doubleValue() == invoiceQuantity.doubleValue()) {
                remainingProductMap.put(invoiceProductKey, new BigDecimal(0));
            } else {
                BigDecimal remainingQty = invoiceQuantity.subtract(requestQuantity);
                remainingQty = remainingQty.add(remainingProductMap.getOrDefault(invoiceProductKey, BigDecimal.ZERO));
                remainingProductMap.put(invoiceProductKey, remainingQty);
            }
        }

        for (String productKey : remainingProductMap.keySet()) {
            BigDecimal quantity = remainingProductMap.get(productKey);
            if (quantity.doubleValue() > 0 || manifestStatusList.contains(ShippingManifest.ShippingManifestStatus.InProgress)) {
                isInvoiceCompleted = false;
                break;
            } else {
                isInvoiceCompleted = true;
            }
        }
        HashMap<String, Object> returnMap = new HashMap<>();
        returnMap.put("status", isInvoiceCompleted);
        returnMap.put("remainingProduct", remainingProductMap);
        return returnMap;

    }

    private void verifyShipperAndReceiverInformation(ReceiverInformation receiverInformation, ShipperInformation shipperInformation, DistributorInformation distributorInformation) {

        if (receiverInformation != null) {
            if (StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                Vendor receiverCustomerCompany = vendorRepository.getById(receiverInformation.getCustomerCompanyId());
                if (receiverCustomerCompany == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, RECEIVER_COMPANY_NOT_FOUND);
                }
            }

        }
        if (shipperInformation != null) {
            if (StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                Vendor shipperCompany = vendorRepository.getById(shipperInformation.getCustomerCompanyId());
                if (shipperCompany == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPER_COMPANY_NOT_FOUND);
                }

                CompanyLicense companyLicense = shipperCompany.getCompanyLicense(shipperInformation.getLicenseId());
                if (companyLicense == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPER_COMPANY_LICENSE_NOT_FOUND);
                }

                if (StringUtils.isNotBlank(shipperInformation.getCompanyContactId())) {
                    companyContactService.getCompanyContactById(shipperInformation.getCompanyContactId());
                }
            } else if (StringUtils.isNotBlank(shipperInformation.getCompanyId())) {
                Shop shipperShop = shopRepository.getById(shipperInformation.getCompanyId());
                if (shipperShop == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPER_COMPANY_NOT_FOUND);
                }
            }

        }
        if (distributorInformation != null) {
            if (StringUtils.isNotBlank(distributorInformation.getCustomerCompanyId())) {
                Vendor distributorCompany = vendorRepository.getById(distributorInformation.getCustomerCompanyId());
                if (distributorCompany == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, DISTRIBUTOR_COMPANY_NOT_FOUND);
                }
                if (StringUtils.isNotBlank(distributorInformation.getLicenseId())) {
                    CompanyLicense companyLicense = distributorCompany.getCompanyLicense(distributorInformation.getLicenseId());
                    if (companyLicense == null) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, DISTRIBUTOR_COMPANY_LICENSE_NOT_FOUND);
                    }
                }
            }


        }

    }

    /**
     * This is override method to create pdf for shipping manifest
     *
     * @param shippingManifestId
     * @return
     */
    @Override
    public InputStream createPdfForShippingManifest(String shippingManifestId, InputStream stream, boolean printRoute, boolean isNew) {
        ShippingManifestResult shippingManifestResult = getShippingManifestById(shippingManifestId);
        if (shippingManifestResult == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        String body;
        if (isNew) {
            body = generateShippingManifestForm(shippingManifestResult , printRoute);
        } else {
            body = emailBody(shippingManifestResult, true, stream, printRoute);
        }
        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, isNew ? SHIPPING_MANIFEST_FORM_RESOURCE : SHIPPING_MANIFEST_RESOURCE));
    }

    private String emailBody(ShippingManifestResult shippingManifestResult, boolean addSignature, InputStream mapStream, boolean printRoute) {
        InputStream inputStream = InvoiceServiceImpl.class.getResourceAsStream("/shipping_manifest.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in shipping manifest", ex);
        }
        String theString = writer.toString();
        Company company = companyRepository.getById(token.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company not found");
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }

        String originAddress = "";
        String receiverAddress = "";
        theString = theString.replaceAll("==companyName==", shop.getName() != null ? shop.getName() : TextUtil.textOrEmpty(""));
        if (shop.getAddress() != null) {
            Address address = shop.getAddress();
            theString = theString.replaceAll("==address==", address.getAddress() != null ? address.getAddress() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==city==", address.getCity() != null ? address.getCity() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==state==", address.getState() != null ? address.getState() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==zipCode==", address.getZipCode() != null ? address.getZipCode() : TextUtil.textOrEmpty(""));
        } else {
            theString = theString.replaceAll("==address==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==city==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==state==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==zipCode==", TextUtil.textOrEmpty(""));
        }

        if (StringUtils.isNotBlank(shop.getLicense())) {
            theString = theString.replaceAll("==licenseNumber==", shop.getLicense());
        } else {
            theString = theString.replaceAll("==licenseNumber==", TextUtil.textOrEmpty(""));
        }

        theString = theString.replaceAll("==balanceDue==", shippingManifestResult.getInvoiceBalanceDue() != null ? TextUtil.toEscapeCurrency(shippingManifestResult.getInvoiceBalanceDue().doubleValue(), shop.getDefaultCountry()) : TextUtil.textOrEmpty(""));
        theString = theString.replaceAll("==invoiceNumber==", shippingManifestResult.getInvoiceNumber() != null ? shippingManifestResult.getInvoiceNumber() : TextUtil.textOrEmpty(""));

        Invoice invoice = invoiceRepository.get(token.getCompanyId(), shippingManifestResult.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException("Invoice", INVOICE_NOT_FOUND);
        }

        long invoiceDate = (invoice.getInvoiceDate() == null || invoice.getInvoiceDate() == 0) ? invoice.getCreated() : invoice.getInvoiceDate();
        String timeZone = StringUtils.isBlank(token.getRequestTimeZone()) ? shop.getTimeZone() : token.getRequestTimeZone();


        theString = theString.replaceAll("==term==", invoice.getInvoiceTerms() != null ? invoice.getInvoiceTerms().toString() : TextUtil.textOrEmpty(""));
        theString = theString.replaceAll("==invoiceDate==", TextUtil.toDate(DateUtil.toDateTime(invoiceDate, timeZone)));
        theString = theString.replaceAll("==dueDate==", invoice.getDueDate() != null ? TextUtil.toDate(DateUtil.toDateTime(invoice.getDueDate(), timeZone)) : TextUtil.textOrEmpty(""));
        theString = theString.replaceAll("==referenceNo==", (StringUtils.isNotBlank(invoice.getOrderNumber()) ? invoice.getOrderNumber() : ""));

        String personName = "";
        String  salesPersonContactName = null;
        String  salesPersonContactPhoneNumber = null;
        if (StringUtils.isNoneBlank(invoice.getSalesPersonId())) {
            Employee salesPerson = employeeRepository.get(token.getCompanyId(), invoice.getSalesPersonId());
            if (salesPerson != null) {
                salesPersonContactName = salesPerson.getFirstName().concat(" ").concat(salesPerson.getLastName());
                salesPersonContactPhoneNumber = salesPerson.getPhoneNumber();
                if (StringUtils.isNotBlank(salesPerson.getFirstName())) {
                    personName = salesPerson.getFirstName() + " ";
                }
                if (StringUtils.isNotBlank(salesPerson.getLastName())) {
                    personName += salesPerson.getLastName();
                }
            }
        }

        //Shipper Information
        if (shippingManifestResult.getShipperInformation() != null) {
            ShipperInformation shipperInformation = shippingManifestResult.getShipperInformation();

            StringBuilder shipperContact = new StringBuilder();
            StringBuilder shipperPhone = new StringBuilder();
            if (shippingManifestResult.getShipperCompanyContact() != null) {
                CompanyContact contact = shippingManifestResult.getShipperCompanyContact();
                shipperContact = contact.getName();
                shipperPhone.append(StringUtils.isNotBlank(contact.getPhoneNumber()) ? contact.getPhoneNumber() : StringUtils.isNotBlank(contact.getOfficeNumber()) ? contact.getOfficeNumber()  : "");
            }

            if (StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                Vendor vendor = vendorRepository.get(token.getCompanyId(), shipperInformation.getCustomerCompanyId());
                if (vendor != null) {
                    CompanyLicense companyLicense = vendor.getCompanyLicense(shipperInformation.getLicenseId());
                    originAddress = vendor.getAddress() != null ? vendor.getAddress().getAddressString() : "";
                    String vendorName = vendor.getName();
                    if (StringUtils.isNotBlank(vendor.getDbaName())) {
                        vendorName += " (" + vendor.getDbaName() + ")";

                    }

                    if (StringUtils.isBlank(shipperPhone) && StringUtils.isNotBlank(vendor.getPhone())) {
                        shipperPhone = new StringBuilder(vendor.getPhone());
                    }
                    theString = theString.replaceAll("==shipperCompanyName==", vendorName);
                    theString = theString.replaceAll("==shipperContactName==", TextUtil.textOrEmpty(shipperContact.toString()));
                    theString = theString.replaceAll("==shipperCompanyType==", TextUtil.textOrEmpty((companyLicense == null) ? "" : companyLicense.getCompanyType().toString()));
                    theString = theString.replaceAll("==shipperStateLicense==", TextUtil.textOrEmpty((companyLicense == null) ? "" : companyLicense.getLicenseNumber()));
                    theString = theString.replaceAll("==shipperAddress==", vendor.getAddress() != null ? vendor.getAddress().getAddressString() : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperPhoneNumber==", TextUtil.textOrEmpty(shipperPhone.toString()));
                    theString = theString.replaceAll("==Salesperson==", personName != null ? personName : TextUtil.textOrEmpty(""));
                } else {
                    theString = theString.replaceAll("==shipperCompanyName==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperContactName==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperCompanyType==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperStateLicense==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperAddress==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperPhoneNumber==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==Salesperson==", personName != null ? personName : TextUtil.textOrEmpty(""));
                }
            } else {
                Shop shipperShop = shopRepository.getById(shipperInformation.getCompanyId());
                String shipperCompanyType = "";
                if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
                    shipperCompanyType = Objects.isNull(shipperShop) ? "Distro Company" : "Distro Shop";
                } else if (CompanyFeatures.AppTarget.Retail == shop.getAppTarget()) {
                    shipperCompanyType = Objects.isNull(shipperShop) ? "Retail Company" : "Retail Shop";
                } else if (CompanyFeatures.AppTarget.Grow == shop.getAppTarget()) {
                    shipperCompanyType = Objects.isNull(shipperShop) ? "Grow Company" : "Grow Shop";
                }
                if (shipperShop == null) {
                    /* For older shipping manifests*/
                    Company shipperCompany = companyRepository.getById(shipperInformation.getCompanyId());
                    theString = theString.replaceAll("==shipperCompanyName==", shipperCompany.getName());
                    theString = theString.replaceAll("==shipperContactName==", shipperCompany.getPrimaryContact() != null ? shipperCompany.getPrimaryContact().getName() : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperCompanyType==", TextUtil.textOrEmpty(shipperCompanyType));
                    theString = theString.replaceAll("==shipperStateLicense==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperAddress==", shipperCompany.getAddress() != null ? shipperCompany.getAddress().getAddressString() : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperPhoneNumber==", shipperCompany.getPhoneNumber() != null ? shipperCompany.getPhoneNumber() : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==Salesperson==", personName != null ? personName : TextUtil.textOrEmpty(""));
                    originAddress = shipperCompany.getAddress() != null ? shipperCompany.getAddress().getAddressString() : "";
                } else {
                    theString = theString.replaceAll("==shipperCompanyName==", shipperShop.getName());
                    theString = theString.replaceAll("==shipperContactName==", salesPersonContactName != null ? salesPersonContactName : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperCompanyType==", TextUtil.textOrEmpty(shipperCompanyType));
                    theString = theString.replaceAll("==shipperStateLicense==", TextUtil.textOrEmpty(shipperShop.getLicense()));
                    theString = theString.replaceAll("==shipperAddress==", shipperShop.getAddress() != null ? shipperShop.getAddress().getAddressString() : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==shipperPhoneNumber==", StringUtils.isNotBlank(salesPersonContactPhoneNumber) ? salesPersonContactPhoneNumber : StringUtils.isNotBlank(shipperPhone) ? shipperPhone.toString() : TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==Salesperson==", personName != null ? personName : TextUtil.textOrEmpty(""));
                    originAddress = shipperShop.getAddress() != null ? shipperShop.getAddress().getAddressString() : "";
                }
            }
        }

        //Receiver Information
        Vendor receiverVendor = null;
        if (shippingManifestResult.getReceiverInformation() != null) {
            ReceiverInformation receiverInformation = shippingManifestResult.getReceiverInformation();

            if (StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                receiverVendor = vendorRepository.get(token.getCompanyId(), receiverInformation.getCustomerCompanyId());
            } else {
                receiverVendor = vendorRepository.get(token.getCompanyId(), invoice.getCustomerId());
            }
            StringBuilder receiverContactName = new StringBuilder();
            StringBuilder receiverContactPhone = new StringBuilder();
            if (shippingManifestResult.getReceiverCompanyContact() != null) {
                CompanyContact contact = shippingManifestResult.getReceiverCompanyContact();
                receiverContactName = contact.getName();
                receiverContactPhone.append(StringUtils.isNotBlank(contact.getPhoneNumber()) ? contact.getPhoneNumber() : StringUtils.isNotBlank(contact.getOfficeNumber()) ? contact.getOfficeNumber() : "");
            }
            receiverAddress = receiverInformation.getCompanyAddress() != null ? receiverInformation.getCompanyAddress().getAddressString() : "";

            if (receiverVendor != null) {
                String vendorName = receiverVendor.getName();
                CompanyLicense companyLicense = receiverVendor.getCompanyLicense(invoice.getLicenseId());

                if (StringUtils.isNotBlank(receiverVendor.getDbaName())) {
                    vendorName += " (" + receiverVendor.getDbaName() + ")";
                }
                if (StringUtils.isBlank(receiverContactPhone) && StringUtils.isNotBlank(receiverVendor.getPhone())) {
                    receiverContactPhone = new StringBuilder(receiverVendor.getPhone());
                }
                theString = theString.replaceAll("==receiverCompanyName==", vendorName);
                theString = theString.replaceAll("==receiverContactName==", TextUtil.textOrEmpty(receiverContactName.toString()));
                theString = theString.replaceAll("==receiverCompanyType==", TextUtil.textOrEmpty((companyLicense == null) ? "" : companyLicense.getCompanyType().toString()));
                theString = theString.replaceAll("==receiverStateLicense==", TextUtil.textOrEmpty((companyLicense == null) ? "" : companyLicense.getLicenseNumber()));
                theString = theString.replaceAll("==receiverAddress==", receiverVendor.getAddress() != null ? receiverVendor.getAddress().getAddressString() : TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==receiverPhoneNumber==", TextUtil.textOrEmpty(receiverContactPhone.toString()));

                receiverAddress = (StringUtils.isBlank(receiverAddress) && receiverVendor.getAddress() != null) ? receiverVendor.getAddress().getAddressString() : receiverAddress;

            } else {
                theString = theString.replaceAll("==receiverCompanyName==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==receiverContactName==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==receiverCompanyType==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==receiverStateLicense==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==receiverAddress==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==receiverPhoneNumber==", TextUtil.textOrEmpty(""));
            }

            theString = theString.replaceAll("==shippingAddress==", TextUtil.textOrEmpty(receiverAddress));
        }

        //Driver Information
        Employee employee = employeeRepository.get(token.getCompanyId(), shippingManifestResult.getDriverId());
        if (employee != null) {
            theString = theString.replaceAll("==driverName==", employee.getFirstName().concat(" ").concat(employee.getLastName()));
            theString = theString.replaceAll("==driverPhone==", TextUtil.textOrEmpty(employee.getPhoneNumber()));
            theString = theString.replaceAll("==driverEmail==", TextUtil.textOrEmpty(employee.getEmail()));
            theString = theString.replaceAll("==license==", employee.getDriversLicense() != null ? employee.getDriversLicense() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==vehicleMake==", employee.getVehicleMake() != null ? employee.getVehicleMake() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==vehicleModel==", employee.getVehicleModel() != null ? employee.getVehicleModel() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==vehicleLicensePlate==", employee.getVehicleLicensePlate() != null ? employee.getVehicleLicensePlate() : TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==driverVinNo==", (StringUtils.isBlank(employee.getVinNo()) ? "" : employee.getVinNo()));
        } else {
            theString = theString.replaceAll("==driverName==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==driverPhone==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==driverEmail==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==license==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==vehicleMake==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==vehicleModel==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==vehicleLicensePlate==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==driverVinNo==", TextUtil.textOrEmpty(""));
        }

        String logoURL = (shop.getLogo() != null && StringUtils.isNotBlank(shop.getLogo().getLargeURL())) ? shop.getLogo().getLargeURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";

        theString = theString.replaceAll("==logo==", logoURL);
        /*theString = theString.replaceAll("==QRCodeImage==", (invoice.getInvoiceQrCodeAsset ().getPublicURL () == null) ? TextUtil.textOrEmpty("") : invoice.getInvoiceQrCodeAsset ().getPublicURL ());*/
        theString = theString.replaceAll("==QRCodeImage==", (invoice.getInvoiceQrCodeUrl() == null) ? TextUtil.textOrEmpty("") : invoice.getInvoiceQrCodeUrl());
        HashMap<String, BigDecimal> productMap = getProductMap(invoice, shippingManifestResult.getProductMetrcInfo(), true);
        HashMap<String, List<ShippingBatchDetails>> shippedProductBatchMap = new HashMap<>();
        if (!productMap.isEmpty()) {
            for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
                if (!productMetrcInfo.getBatchDetails().isEmpty()) {
                    shippedProductBatchMap.put(productMetrcInfo.getOrderItemId(), productMetrcInfo.getBatchDetails());
                }
            }
        }
        if ((invoice.getCart() != null && invoice.getCart().getItems() != null)
                && (productMap != null && !productMap.isEmpty())) {
            List<OrderItem> items = invoice.getCart().getItems();
            HashMap<String, BigDecimal> shippingProductMap = productMap;
            List<ObjectId> productBatchIds = new ArrayList<>();
            List<ObjectId> productIds = new ArrayList<>();
            List<ObjectId> prepackageIds = new ArrayList<>();
            List<ObjectId> prepackageItemsIds = new ArrayList<>();

            for (OrderItem item : items) {
                if (shippingProductMap.containsKey(item.getProductId())) {
                    List<ShippingBatchDetails> shippingBatchDetails = shippedProductBatchMap.get(item.getId());
                    if (shippingBatchDetails != null && !shippingBatchDetails.isEmpty()) {
                        for (ShippingBatchDetails result : shippingBatchDetails) {
                            if (result.getBatchId() != null && ObjectId.isValid(result.getBatchId())) {
                                productBatchIds.add(new ObjectId(result.getBatchId()));
                            }
                            if (StringUtils.isNotBlank(item.getPrepackageId())) {
                                prepackageIds.add(new ObjectId(item.getPrepackageId()));
                            }
                            if (StringUtils.isNotBlank(result.getPrepackageItemId()) && ObjectId.isValid(result.getPrepackageItemId())) {
                                prepackageItemsIds.add(new ObjectId(result.getPrepackageItemId()));
                            }
                        }
                    }
                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));
                    }

                }
            }

            HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(token.getCompanyId(), prepackageIds);

            HashMap<String, PrepackageProductItem> prepackageProductItemMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), prepackageItemsIds);

            for (Map.Entry<String, PrepackageProductItem> productItem : prepackageProductItemMap.entrySet()) {

                if (StringUtils.isNotBlank(productItem.getValue().getBatchId())) {
                    productBatchIds.add(new ObjectId(productItem.getValue().getBatchId()));
                }
            }


            HashMap<String, ProductBatch> productBatchMap = new HashMap<>();
            HashMap<String, Product> productHashMap = new HashMap<>();
            if (!productBatchIds.isEmpty()) {
                productBatchMap = productBatchRepository.listAsMap(token.getCompanyId(), productBatchIds);
            }
            if (!productIds.isEmpty()) {
                productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);
            }
            List<ObjectId> categoryIds = new ArrayList<>();
            for (Product product : productHashMap.values()) {
                if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                    categoryIds.add(new ObjectId(product.getCategoryId()));
                }
            }

            HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), categoryIds);
            StringBuilder productInfo = new StringBuilder();
            for (OrderItem item : items) {
                if (!shippingProductMap.containsKey(item.getProductId())) {
                    continue;
                }

                Product product = productHashMap.get(item.getProductId());
                List<ShippingBatchDetails> batchDetailsList = shippedProductBatchMap.get(item.getId());
                BigDecimal shipQuantity = shippingProductMap.get(item.getProductId());
                if (batchDetailsList != null && !batchDetailsList.isEmpty()) {
                    for (ShippingBatchDetails batchDetail : batchDetailsList) {
                        productInfo.append(getProductInformation(product, shipQuantity, batchDetail, productBatchMap, prepackageMap, prepackageProductItemMap, item.getPrepackageId(), productCategoryHashMap, timeZone));
                    }
                } else {
                    productInfo.append(getProductInformation(product, shipQuantity, null, productBatchMap, prepackageMap, prepackageProductItemMap, item.getPrepackageId(), productCategoryHashMap, timeZone));
                }
            }
            if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                theString = theString.replaceAll("==batchId==", TextUtil.textOrEmpty("Package Id"));
            } else {
                theString = theString.replaceAll("==batchId==", TextUtil.textOrEmpty("Batch Id"));
            }

            theString = theString.replaceAll("==productInformation==", productInfo.toString());
        } else {
            theString = theString.replaceAll("==productInformation==", TextUtil.textOrEmpty(""));
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yy");

        theString = theString.replaceAll("==deliveryDate==", formatter.print(DateUtil.toDateTime(shippingManifestResult.getDeliveryDate(), timeZone)));
        theString = theString.replaceAll("==deliveryTime==", TextUtil.toTime(DateUtil.toDateTime(shippingManifestResult.getDeliveryTime(), timeZone)));

        String notes = "";
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(shippingManifestResult.getNotes())) {
            notes = getNoteSection(shippingManifestResult.getNotes());
            theString = theString.replaceAll("==notes==", notes.replaceAll("&", "&amp;"));
            theString = theString.replaceAll("==showNotes==", TextUtil.textOrEmpty(StringUtils.isNotBlank(notes) ? "" : "display:none"));
        } else {
            theString = theString.replaceAll("==showNotes==", TextUtil.textOrEmpty("display:none;"));
        }

        if (printRoute) {
            theString = generateRouteBody(originAddress, receiverAddress, theString, notes);
        } else {
            theString = theString.replaceAll("==showRouteDetail==", "display:none;");
            theString = theString.replaceAll("==showPageBreak==", "display:none;");
        }

        if (addSignature && shippingManifestResult.getSignaturePhoto() != null && StringUtils.isNotBlank(shippingManifestResult.getSignaturePhoto().getKey())) {
            try {
                AssetStreamResult assetStreamResult = amazonS3Service.downloadFile(shippingManifestResult.getSignaturePhoto().getKey(), true);
                InputStream stream = assetStreamResult.getStream();
                String imageStr = Base64.encodeBase64String(IOUtils.toByteArray(stream));
                theString = theString.replaceAll("==signatureImage==", imageStr);
                theString = theString.replaceAll("==showSignature==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==showPageBreak==", TextUtil.textOrEmpty("display:none;"));
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else if (shippingManifestResult.getSignaturePhoto() == null || StringUtils.isBlank(shippingManifestResult.getSignaturePhoto().getKey())) {
            theString = theString.replaceAll("data:image/jpeg;base64,==signatureImage==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==showSignature==", TextUtil.textOrEmpty("display:none;"));
            theString = theString.replaceAll("==showPageBreak==", "");
        }
        if (shippingManifestResult.getSignaturePhoto() != null) {
            long signDateTime = shippingManifestResult.getSignaturePhoto().getCreated();
            DateTime dateTime = DateUtil.toDateTime(signDateTime, timeZone);
            theString = theString.replaceAll("==signatureDate==", TextUtil.toDate(dateTime));
            theString = theString.replaceAll("==signatureTime==", TextUtil.toTime(dateTime));
        } else {
            theString = theString.replaceAll("==signatureDate==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==signatureTime==", TextUtil.textOrEmpty(""));
        }
        if (addSignature && mapStream != null) {
            try {
                byte[] imageBytes = IOUtils.toByteArray(mapStream);
                mapStream.read(imageBytes, 0, imageBytes.length);
                mapStream.close();
                String imageStr = Base64.encodeBase64String(imageBytes);
                theString = theString.replaceAll("==mapImage==", imageStr);
                theString = theString.replaceAll("==showMap==", TextUtil.textOrEmpty(""));
            } catch (IOException ex) {

            }

        } else if (mapStream == null) {
            theString = theString.replaceAll("data:image/jpeg;base64,==mapImage==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==showMap==", TextUtil.textOrEmpty("display:none;"));
        }

        theString = theString.replaceAll("&", "&amp;");
        if(shop.getAppTarget()== CompanyFeatures.AppTarget.Grow) {
            theString = theString.replaceAll("==CultivatorCompanyName==", "Cultivator Name");
        }else {
            theString = theString.replaceAll("==CultivatorCompanyName==", "Company Name");
        }
        return theString;
    }
    private String getNoteSection(List<Note> notes) {
        String noteSection = "";
        for (Note note : notes) {
            if (StringUtils.isNotBlank(note.getMessage())) {
                noteSection += "<p style=\"font-size:14px;color:#9E9E9E;margin:2px 0px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\">" + note.getMessage() + "</p>";
            }
        }
        return noteSection;
    }

    @Override
    public void sendShippingManifestEmail(final String shippingManifestId, final String email, final InputStream mapStream, final boolean printRoute) {
        ShippingManifestResult shippingManifestResult = getShippingManifestById(shippingManifestId);
        if (shippingManifestResult == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (StringUtils.isBlank(email)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_EMAIL_NOT_EMPTY);
        }
        String body = emailBody(shippingManifestResult, false, mapStream, printRoute);
        InputStream stream = null;
        HashMap<String, InputStream> attachmentStreamList = new HashMap<>();
        HashMap<String, HashMap<String, InputStream>> attachmentMap = new HashMap<>();
        if (shippingManifestResult.getSignaturePhoto() != null && StringUtils.isNotBlank(shippingManifestResult.getSignaturePhoto().getKey())) {
            try {
                AssetStreamResult assetStreamResult = amazonS3Service.downloadFile(shippingManifestResult.getSignaturePhoto().getKey(), true);
                stream = assetStreamResult.getStream();
                attachmentStreamList.put(assetStreamResult.getKey(), stream);
                body = body.replaceAll("data:image/jpeg;base64,==signatureImage==", TextUtil.textOrEmpty("cid:" + assetStreamResult.getKey()));
                attachmentMap.put("inlineAttachment", attachmentStreamList);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                body = body.replaceAll("data:image/jpeg;base64,==signatureImage==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==showSignature==", TextUtil.textOrEmpty("display:none;"));

            }
        }

        if (mapStream != null) {
            try {
                byte[] bytes = IOUtils.toByteArray(mapStream);
                stream = new ByteArrayInputStream(bytes);
                body = body.replaceAll("data:image/jpeg;base64,==mapImage==", TextUtil.textOrEmpty("cid:mapImage.jpeg"));
                attachmentStreamList.put("mapImage.jpeg", stream);
                attachmentMap.put("inlineAttachment", attachmentStreamList);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                body = body.replaceAll("data:image/jpeg;base64,==mapImage==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==showMap==", TextUtil.textOrEmpty("display:none;"));
            }
        }
        amazonServiceManager.sendMultiPartEmail("support@blaze.me", email, connectConfiguration.getAppName(), body, null, null, attachmentMap, "Content-Disposition", "", "");
    }

    private void updateNotes(List<Note> notes) {
        if (notes != null) {
            for (Note note : notes) {
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            }
        }
    }

    private HashMap<String, BigDecimal> getProductMap(Invoice invoice, List<ProductMetrcInfo> productMetrcInfos, boolean checkRequired) {
        HashMap<String, BigDecimal> productMap = new HashMap<>();
        HashMap<String, String> orderItemMap = new HashMap<>();

        if (invoice.getCart() == null || invoice.getCart().getItems() == null) {
            return productMap;
        }
        for (OrderItem item : invoice.getCart().getItems()) {
            orderItemMap.put(item.getId(), item.getProductId());

        }

        if (productMetrcInfos != null && !productMetrcInfos.isEmpty()) {
            for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
                if (checkRequired && StringUtils.isBlank(productMetrcInfo.getOrderItemId())) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Order item cannot be blank.");
                }
                if (checkRequired && !orderItemMap.containsKey(productMetrcInfo.getOrderItemId())) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Product does not found for order item");
                }
                String productId = orderItemMap.get(productMetrcInfo.getOrderItemId());
                if (StringUtils.isBlank(productId) && !checkRequired) {
                    continue;
                }
                List<ShippingBatchDetails> shippingBatchDetails = productMetrcInfo.getBatchDetails();
                BigDecimal quantity = BigDecimal.ZERO;
                if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails)) {
                    for (ShippingBatchDetails batchDetails : shippingBatchDetails) {
                        if (!Objects.isNull(shippingBatchDetails)) {
                            quantity = quantity.add(batchDetails.getQuantity());
                        }
                    }
                }
                productMetrcInfo.setQuantity((quantity.doubleValue() > 0) ? quantity : productMetrcInfo.getQuantity());
                productMetrcInfo.setProductId(productId);
                productMap.put(productId, productMetrcInfo.getQuantity());

            }
        }
        return productMap;
    }

    //Calculate batch quantity to check that product has quantities in any related inventory or not.
    private BigDecimal createBatchQuantity(String batchId, String productId, String inventoryId) {
        BigDecimal totalQuantity;
        List<ProductBatchByInventory> quantities = batchQuantityRepository.getBatchQuantitiesByInventory(token.getCompanyId(), token.getShopId(), batchId, productId);
        Map<String, List<BatchQuantity>> quantityByInventoryMap = new HashMap<>();
        if (!CollectionUtils.isNullOrEmpty(quantities)) {
            for (ProductBatchByInventory batchQuantity : quantities) {
                quantityByInventoryMap.putIfAbsent(batchQuantity.getInventoryId(), new ArrayList<>());
                quantityByInventoryMap.get(batchQuantity.getInventoryId()).addAll(batchQuantity.getBatchQuantity());
            }
        }

        List<BatchQuantity> batchQuantities = quantityByInventoryMap.get(inventoryId);

        double quantityPerBatch = 0d;
        //Calculate batch quantity
        if (!CollectionUtils.isNullOrEmpty(batchQuantities)) {
            for (BatchQuantity batchQuantity : batchQuantities) {
                quantityPerBatch += batchQuantity.getQuantity().doubleValue();
            }
        }
        totalQuantity = new BigDecimal(quantityPerBatch);
        return totalQuantity;
    }

    /**
     * This is override method to update status (Finalize) of shipping manifest
     *
     * @param shippingManifestId : Shipping manifest id
     */
    @Override
    public ShippingManifestResult finalizeShippingManifest(String shippingManifestId) {
        ShippingManifest dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Completed) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping manifest already completed");
        }
        Invoice invoice = invoiceRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), dbShippingManifest.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }

        SearchResult<Terminal> terminalList = terminalRepository.getAllTerminalsByState(invoice.getCompanyId(), invoice.getShopId(), true, "{create:-1}", 0, Integer.MAX_VALUE);
        Terminal terminal = null;
        if (terminalList != null && !CollectionUtils.isNullOrEmpty(terminalList.getValues())) {
            terminal = terminalList.getValues().get(0);
        }
        Inventory inventory = null;
        if (terminal == null) {
            inventory = inventoryRepository.getInventory(invoice.getCompanyId(), invoice.getShopId(), Inventory.SAFE);
        } else {
            inventory = inventoryRepository.get(invoice.getCompanyId(), terminal.getAssignedInventoryId());
        }

        List<ProductMetrcInfo> productMetrcInfos = dbShippingManifest.getProductMetrcInfo();
        Set<String> productIds = new HashSet<>();
        Set<ObjectId> prepackageItemIds = new HashSet<>();
        Set<ObjectId> inventoryIds = new HashSet<>();
        if (!CollectionUtils.isNullOrEmpty(productMetrcInfos)) {
            for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
                if (StringUtils.isNotBlank(productMetrcInfo.getProductId()) && ObjectId.isValid(productMetrcInfo.getProductId())) {
                    productIds.add(productMetrcInfo.getProductId());
                }
                List<ShippingBatchDetails> shippingManifestDetails = productMetrcInfo.getBatchDetails();
                if (!CollectionUtils.isNullOrEmpty(shippingManifestDetails)) {
                    for (ShippingBatchDetails manifestDetail : shippingManifestDetails) {
                        if (StringUtils.isNotBlank(manifestDetail.getPrepackageItemId()) && ObjectId.isValid(manifestDetail.getPrepackageItemId())) {
                            prepackageItemIds.add(new ObjectId(manifestDetail.getPrepackageItemId()));
                        }
                        if (StringUtils.isNotBlank(manifestDetail.getOverrideInventoryId()) && ObjectId.isValid(manifestDetail.getOverrideInventoryId())) {
                            inventoryIds.add(new ObjectId(manifestDetail.getOverrideInventoryId()));
                        }
                    }
                }
            }
        }

        HashMap<String, PrepackageProductItem> prepackageMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(prepackageItemIds));
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(inventoryIds));

        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));

        if (!CollectionUtils.isNullOrEmpty(productMetrcInfos)) {
            for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
                List<ShippingBatchDetails> shippingBatchDetails = productMetrcInfo.getBatchDetails();
                if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails)) {
                    for (ShippingBatchDetails batchDetails : shippingBatchDetails) {
                        Inventory assignInventory = inventoryMap.get(batchDetails.getOverrideInventoryId());
                        if (assignInventory == null) {
                            assignInventory = inventory;
                        }

                        if (StringUtils.isNotBlank(batchDetails.getPrepackageItemId())) {
                            PrepackageProductItem prepackageProductItem = prepackageMap.get(batchDetails.getPrepackageItemId());
                            if (prepackageProductItem == null) {
                                throw new BlazeInvalidArgException(PREPACKAGE, PREPACKAGE_NOT_FOUND);
                            }
                            if (!prepackageProductItem.getProductId().equalsIgnoreCase(productMetrcInfo.getProductId())) {
                                throw new BlazeInvalidArgException(PREPACKAGE, PREPACKAGE_NOT_BELONG_TO_PRODUCT);
                            }

                            double availablePrepackageQuantity = 0d;
                            for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
                                if (prepackageQuantity.getProductId().equals(productMetrcInfo.getProductId()) &&
                                        prepackageQuantity.getPrepackageItemId().equals(batchDetails.getPrepackageItemId()) &&
                                        prepackageQuantity.getInventoryId().equals(assignInventory.getId())) {

                                    availablePrepackageQuantity += prepackageQuantity.getQuantity();
                                }
                            }

                            if (availablePrepackageQuantity < batchDetails.getQuantity().doubleValue()) {
                                throw new BlazeInvalidArgException(PREPACKAGE, PREPACKAGE_NOT_ENOUGH_QUANTITY);
                            }

                        } else {
                            if (!Objects.isNull(batchDetails) && StringUtils.isNotBlank(batchDetails.getBatchId())) {
                                ProductBatch productBatch = productBatchRepository.get(token.getCompanyId(), batchDetails.getBatchId());
                                if (Objects.isNull(productBatch)) {
                                    throw new BlazeInvalidArgException("Product Batch", "Product Batch not available for this product.");
                                }
                                if (productBatch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE) {
                                    throw new BlazeInvalidArgException("Product Batch", "Product Batch is not Ready For Sale.");
                                }
                                double batchQuantity = createBatchQuantity(batchDetails.getBatchId(), productMetrcInfo.getProductId(), assignInventory.getId()).doubleValue();
                                if (batchQuantity < batchDetails.getQuantity().doubleValue()) {
                                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Product batch does not have requested quantity.");
                                }
                            } else {
                                throw new BlazeInvalidArgException("Shipping manifest", "Please select a prepackage or product batch.");
                            }
                        }
                    }
                }
            }
        }

        dbShippingManifest.setStatus(ShippingManifest.ShippingManifestStatus.Completed);

        dbShippingManifest.setApprovalDate(DateTime.now().getMillis());
        dbShippingManifest.setApproverEmployeeId(token.getActiveTopUser().getUserId());

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        //Process shipping manifest inventory
        invoiceInventoryService.processInvoiceForInventory(shop, invoice, dbShippingManifest);

        dbShippingManifest = shippingManifestRepository.update(token.getCompanyId(), shippingManifestId, dbShippingManifest);

        HashMap<String, Object> returnMap = getInvoiceStatusByProducts(new HashMap<>(), invoice, "", false);
        boolean status = (boolean) returnMap.get("status");

        long countInProgressManifest = shippingManifestRepository.countShippingManifestByStatus(token.getCompanyId(), token.getShopId(), invoice.getId(), ShippingManifest.ShippingManifestStatus.InProgress);
        if (countInProgressManifest == 0 && status) {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.COMPLETED);
            invoice.setActive(!(invoice.getInvoicePaymentStatus() == Invoice.PaymentStatus.PAID));
            invoiceInventoryService.calculateActualTaxes(invoice);
            Map<String, Object> map = invoiceService.calculateBalanceDue(invoice.getId(), invoice.getCart().getTotal(), BigDecimal.ZERO);
            BigDecimal balanceDue = (BigDecimal) map.get("balanceDue");
            BigDecimal amountPaid = (BigDecimal) map.get("amountPaid");
            BigDecimal changeDue = (BigDecimal) map.get("changeDue");
            if (invoice.getCart() != null) {
                invoice.getCart().setBalanceDue(balanceDue);
                invoice.getCart().setChangeDue(changeDue);
                invoice.getCart().setCashReceived(amountPaid);
            }
            invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);
        }
        ShippingManifestResult shippingManifestResult = (ShippingManifestResult) dbShippingManifest;
        prepareShippingManifest(shippingManifestResult);
        invoiceActivityLogService.addInvoiceActivityLog(invoice.getId(), token.getActiveTopUser().getUserId(), String.format("Shipping manifest %s finalized.", shippingManifestResult.getShippingManifestNo()));

        return shippingManifestResult;
    }

    @Override
    public SearchResult<ShippingManifestResult> getAllShippingManifestByStatus(String invoiceId, ShippingManifest.ShippingManifestStatus status, int start, int limit) {
        SearchResult<ShippingManifestResult> searchResult = new SearchResult<>();
        limit = (limit == 0) ? Integer.MAX_VALUE : limit;
        if (status == null) {
            searchResult = shippingManifestRepository.getShippingManifestByInvoiceId(token.getCompanyId(), invoiceId, "{modified:-1}", start, limit);
        } else {
            List<ShippingManifest.ShippingManifestStatus> statusList = new ArrayList<>();
            statusList.add(status);
            searchResult = shippingManifestRepository.getShippingManifestByInvoiceIdAndStatus(token.getCompanyId(), token.getShopId(), invoiceId, statusList, "{modified:-1}", start, limit);
        }
        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            for (ShippingManifestResult result : searchResult.getValues()) {
                prepareShippingManifest(result);
            }
        }
        return searchResult;
    }

    /**
     * Private method for get product metrc info details for save and update.
     *
     * @param batchIds
     * @param productMetrcInfos
     * @param prepackageProductItemIds
     * @return
     */
    private List<ProductMetrcInfo> getProductMetrcInfo(Invoice invoice, List<ObjectId> batchIds, List<ProductMetrcInfo> productMetrcInfos, List<ObjectId> prepackageProductItemIds) {
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), batchIds);

        HashMap<String, PrepackageProductItem> prepackageItemMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), prepackageProductItemIds);

        HashMap<String, String> invoiceProductMap = new HashMap<>(invoice.getCart().getItems().size());
        HashMap<String, BigDecimal> invoiceQtyMap = new HashMap<>(invoice.getCart().getItems().size());
        HashMap<String, String> invoicePrepackageMap = new HashMap<>(invoice.getCart().getItems().size());

        for (OrderItem item : invoice.getCart().getItems()) {
            invoiceProductMap.put(item.getId(), item.getProductId());
            invoiceQtyMap.put(item.getId(), item.getQuantity());
            invoicePrepackageMap.put(item.getId(), item.getPrepackageId());
        }

        for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {

            List<ShippingBatchDetails> shippingBatchDetails = new ArrayList<>();

            if (productMetrcInfo.getBatchDetails() != null && !productMetrcInfo.getBatchDetails().isEmpty()) {

                for (ShippingBatchDetails shippingProductDetail : productMetrcInfo.getBatchDetails()) {

                    ProductBatch batch = batchHashMap.get(shippingProductDetail.getBatchId());
                    PrepackageProductItem prepackageProductItem = prepackageItemMap.get(shippingProductDetail.getPrepackageItemId());
                    BigDecimal shippingQuantity = (shippingProductDetail.getQuantity() == null) ? BigDecimal.ZERO : shippingProductDetail.getQuantity();

                    if (StringUtils.isBlank(shippingProductDetail.getBatchId()) && shippingQuantity.doubleValue() == 0 && StringUtils.isBlank(shippingProductDetail.getPrepackageItemId())) {
                        continue;
                    }
                    if (StringUtils.isNotBlank(shippingProductDetail.getBatchId()) && batch == null) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Batch does not found");
                    }

                    if (StringUtils.isNotBlank(shippingProductDetail.getPrepackageItemId()) && prepackageProductItem == null) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PREPACKAGE_NOT_FOUND);
                    }

                    if ((StringUtils.isNotBlank(shippingProductDetail.getBatchId()) || StringUtils.isNotBlank(shippingProductDetail.getPrepackageItemId())) &&
                            shippingQuantity.doubleValue() == 0) {
                        throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Shipping quantity cannot be zero.");
                    }
                    if (StringUtils.isNotBlank(shippingProductDetail.getPrepackageItemId())) {
                        String prepackageId = invoicePrepackageMap.get(productMetrcInfo.getOrderItemId());
                        if (StringUtils.isBlank(prepackageId)) {
                            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Prepackage does not found in invoice.");
                        }
                        if (!prepackageProductItem.getPrepackageId().equalsIgnoreCase(prepackageId)) {
                            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Selected prepackage item does not belongs to invoice prepackage");
                        }
                    }
                    ShippingBatchDetails batchDetails = new ShippingBatchDetails();
                    batchDetails.setBatchId(shippingProductDetail.getBatchId());
                    batchDetails.setPrepackageItemId(shippingProductDetail.getPrepackageItemId());
                    if (batch != null) {
                        batchDetails.setBatchSku(batch.getSku());
                        batchDetails.setMetrcLabel(batch.getMetrcTagId());
                    }
                    batchDetails.setQuantity(shippingProductDetail.getQuantity());
                    batchDetails.setOverrideInventoryId(shippingProductDetail.getOverrideInventoryId());
                    batchDetails.setReqMetrcLabel(shippingProductDetail.getReqMetrcLabel());

                    shippingBatchDetails.add(batchDetails);
                }
                productMetrcInfo.setBatchDetails(shippingBatchDetails);
            }
        }
        return productMetrcInfos;
    }

    /**
     * Private method for getting shipped product information for pdf and email
     *
     * @param product                  : product
     * @param shipQuantity             : shipQuantity
     * @param batchDetails             : batchDetails
     * @param productBatchMap          : productBatchMap
     * @param prepackageMap            : prepackageMap
     * @param prepackageProductItemMap : prepackageProductItemMap
     * @param prepackageId             : prepackageId
     * @param productCategoryHashMap   : productCategoryHashMap
     * @return
     */
    private StringBuilder getProductInformation(Product product, BigDecimal shipQuantity, ShippingBatchDetails batchDetails, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, String prepackageId, HashMap<String, ProductCategory> productCategoryHashMap, String timeZone) {
        StringBuilder productInfo = new StringBuilder();
        String batchLink = "";
        ProductBatch productBatch = null;
        Inventory inventory;
        String quantityExt = "gram";
        Prepackage prepackage = prepackageMap.get(prepackageId);
        PrepackageProductItem prepackageProductItem = null;
        if (batchDetails != null) {
            prepackageProductItem = prepackageProductItemMap.get(batchDetails.getPrepackageItemId());
            if (StringUtils.isNotBlank(batchDetails.getBatchId())) {
                productBatch = productBatchMap.get(batchDetails.getBatchId());
                shipQuantity = batchDetails.getQuantity();
            }
        }

        productInfo.append("<tr>");
        productInfo.append("<td style='width:20%;'>");
        if (productBatch != null) {
            String tag = StringUtils.isNotBlank(productBatch.getTrackPackageLabel()) ? productBatch.getTrackPackageLabel() : "N/A";
            if (StringUtils.isNotBlank(batchDetails.getReqMetrcLabel()) && batchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                tag = batchDetails.getReqMetrcLabel();
            }
            productInfo.append(tag);
        }
        productInfo.append("</td>");
        if (product != null) {
            ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
            String productName = product.getName() != null ? product.getName() : "-";
            String harvestBatchId = "-";
            if (batchDetails != null && StringUtils.isNotBlank(batchDetails.getOverrideInventoryId())) {
                inventory = inventoryRepository.get(token.getCompanyId(), batchDetails.getOverrideInventoryId());
            } else {
                inventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
            }

            /* If prepackage is selected for shipping manifest*/
            if (prepackage != null && prepackageProductItem != null) {

                ProductBatch batch = productBatchMap.get(prepackageProductItem.getBatchId());

                if (batch != null) {
                    productName = productName + " (" + prepackage.getName() + " - " + batch.getSku() + ")";

                } else {
                    productName = productName + " (" + prepackage.getName() + ")";
                }

                if (batch != null) {
                    harvestBatchId = String.format("%s - (%s)", StringUtils.isNotBlank(batch.getTrackHarvestBatch()) ? batch.getTrackHarvestBatch() : batch.getSku(), DateUtil.toDateFormatted(batch.getPurchasedDate(), timeZone));
                    batchLink = getBatchAttachmentLink(batch);
                }
            } else if (productBatch != null) {
                /* If product batch is selected for shipping manifest*/
                productName = productName + " (" + productBatch.getSku() + ")";
                Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
                if (shop.isGrow()) {
                    harvestBatchId = String.format("%s - (%s)", productBatch.getSku(), DateUtil.toDateFormatted(productBatch.getPurchasedDate(), timeZone));
                } else {
                    harvestBatchId = String.format("%s - (%s)", StringUtils.isNotBlank(productBatch.getTrackHarvestBatch()) ? productBatch.getTrackHarvestBatch() : productBatch.getSku(), DateUtil.toDateFormatted(productBatch.getPurchasedDate(), timeZone));
                }
                batchLink = getBatchAttachmentLink(productBatch);
            }

            if (productCategory != null && ProductCategory.UnitType.grams == productCategory.getUnitType()) {
                if (shipQuantity.doubleValue() > 1) {
                    quantityExt = "grams";
                }
            } else {
                quantityExt = "unit";
                if (shipQuantity.doubleValue() > 1) {
                    quantityExt = "units";
                }
            }

            productInfo.append("<td style='width:30%;'>").append(productName).append("</td>");
            productInfo.append("<td style='width:20%;'>").append(!Objects.isNull(inventory) ? inventory.getName() : "-").append("</td>");

            productInfo.append("<td style='width:15%;'>" + (StringUtils.isNotBlank(batchLink) ? "<a href='" + batchLink + "'>" : "")).append(harvestBatchId).append((StringUtils.isNotBlank(batchLink) ? "</a>" : "") + "</td>");

            if (product.isActive()) {
                productInfo.append("<td style='width:15%;'>").append("Active").append("</td>");
            } else {
                productInfo.append("<td style='width:15%;'>").append("InActive").append("</td>");
            }

            productInfo.append("<td style='width:15%;'>").append((product.getProductSaleType() == null) ? " " : product.getProductSaleType()).append("</td>");
            productInfo.append("<td style='width:10%;'>").append(TextUtil.formatToTwoDecimalPoints(shipQuantity) + " " + quantityExt).append("</td>");
        } else {
            productInfo.append("<td style='width:45%;'>").append(" ").append("</td>");
            productInfo.append("<td style='width:15%;'>").append(" ").append("</td>");
            productInfo.append("<td style='width:10%;'>").append(" ").append("</td>");
            productInfo.append("<td style='width:10%;'>").append(" ").append("</td>");
        }
        productInfo.append("</tr>");

        return productInfo;
    }

    private HashMap<String, BigDecimal> getProductMapByItem(Invoice invoice, List<ProductMetrcInfo> productMetrcInfos, boolean checkRequired) {
        HashMap<String, BigDecimal> productMap = new HashMap<>();
        HashMap<String, String> orderItemMap = new HashMap<>();

        if (invoice.getCart() == null || invoice.getCart().getItems() == null) {
            return productMap;
        }
        for (OrderItem item : invoice.getCart().getItems()) {
            orderItemMap.put(item.getId(), item.getProductId());

        }

        if (productMetrcInfos != null && !productMetrcInfos.isEmpty()) {
            Set<ObjectId> inventoryIds = new HashSet<>();
            for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
                if (CollectionUtils.isNullOrEmpty(productMetrcInfo.getBatchDetails())) {
                    continue;
                }

                for (ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                    if (StringUtils.isNotBlank(batchDetails.getOverrideInventoryId()) && ObjectId.isValid(batchDetails.getOverrideInventoryId())) {
                        inventoryIds.add(new ObjectId(batchDetails.getOverrideInventoryId()));
                    }
                }
            }

            HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(inventoryIds));

            for (ProductMetrcInfo productMetrcInfo : productMetrcInfos) {
                    if (checkRequired && StringUtils.isBlank(productMetrcInfo.getOrderItemId())) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Order item cannot be blank.");
                }
                if (checkRequired && !orderItemMap.containsKey(productMetrcInfo.getOrderItemId())) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Product does not found for order item");
                }
                String productId = orderItemMap.get(productMetrcInfo.getOrderItemId());
                if (StringUtils.isBlank(productId) && !checkRequired) {
                    continue;
                }
                List<ShippingBatchDetails> shippingBatchDetails = productMetrcInfo.getBatchDetails();
                BigDecimal quantity = BigDecimal.ZERO;
                if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails)) {
                    for (ShippingBatchDetails batchDetails : shippingBatchDetails) {
                        if (!Objects.isNull(batchDetails)) {
                            quantity = quantity.add(batchDetails.getQuantity());
                            if (StringUtils.isNotBlank(batchDetails.getOverrideInventoryId())) {
                                Inventory inventory = inventoryHashMap.get(batchDetails.getOverrideInventoryId());
                                if (checkRequired && (inventory == null || !inventory.isActive() || inventory.isDeleted())) {
                                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVENTORY_NOT_FOUND);
                                }
                            }
                        }
                    }
                }
                productMetrcInfo.setQuantity((quantity.doubleValue() > 0) ? quantity : productMetrcInfo.getQuantity());
                productMetrcInfo.setProductId(productId);
                productMap.put(productMetrcInfo.getOrderItemId(), productMetrcInfo.getQuantity());

            }
        }
        return productMap;
    }


    private String generateRouteBody(String origin, String receiver, String emailBody, String notes) {
        final String key = connectConfiguration.getGoogleAPIKey();
        final String apiPath = "https://maps.googleapis.com/maps/api/directions/json";

        final String urlPath = String.format("%s?origin=%s&destination=%s&mode=%s&key=%s",
                apiPath,
                origin,
                receiver,
                "driving",
                key);

        StringBuilder routeDetail = new StringBuilder();
        String showRoute = "display: none;";
        try {

            Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(apiPath)
                    .queryParam("origin", origin)
                    .queryParam("destination", receiver)
                    .queryParam("mode", "driving")
                    .queryParam("key", key);
            String result = SimpleRestUtil.get(String.class, webTarget);

            if (StringUtils.isBlank(result)) {
                emailBody = emailBody.replaceAll("==showRouteDetail==", "display:none;");
                if (StringUtils.isBlank(notes)) {
                    emailBody = emailBody.replace("==showDetails==", "display:none");
                }
                return emailBody;
            }

            JSONObject routeObject = new JSONObject(result);

            if (routeObject.has("routes") && routeObject.getJSONArray("routes").length() >= 1) {
                showRoute = "";
                JSONObject routes = routeObject.getJSONArray("routes").getJSONObject(0);
                routes = routes.getJSONArray("legs").getJSONObject(0);

                JSONObject distance = routes.getJSONObject("distance");
                JSONObject duration = routes.getJSONObject("duration");
                String startAddress = routes.getString("start_address");
                String endAddress = routes.getString("end_address");

                emailBody = emailBody.replaceAll("==expectedMinute==", (distance != null && distance.has("text")) ? distance.getString("text") : "");
                emailBody = emailBody.replaceAll("==expectedTime==", (duration != null && duration.has("text")) ? duration.getString("text") : "");
                emailBody = emailBody.replaceAll("==originAddress==", (StringUtils.isNotBlank(startAddress) ? startAddress : ""));
                emailBody = emailBody.replaceAll("==destinationAddress==", (StringUtils.isNotBlank(endAddress) ? endAddress : ""));


                if (routes.has("steps") && routes.getJSONArray("steps").length() >= 1) {
                    JSONArray steps = routes.getJSONArray("steps");

                    for (int i = 0; i < steps.length(); i++) {
                        String html = steps.getJSONObject(i).getString("html_instructions");
                        JSONObject jsonObject =  steps.getJSONObject(i);
                        JSONObject subDistance = jsonObject.getJSONObject("distance");
                        routeDetail.append("<tr>")
                                .append("<td>" + (i + 1) + ".</td>")
                                .append("<td>" + html.replaceAll("&nbsp;", " ") + "</td>")
                                .append("<td>" + subDistance.getString("text") + "</td>")
                                .append("</tr>");
                    }
                }
                emailBody = emailBody.replaceAll(" ==routeBody==", routeDetail.toString());

            }

        } catch (Exception e) {
            LOGGER.error("Error retrieving distance", e);
            showRoute = "display:none;";
            if (StringUtils.isBlank(notes)) {
                emailBody = emailBody.replace("==showDetails==", "display:none");
            }
        }

        emailBody = emailBody.replaceAll("==showRouteDetail==", showRoute);
        if (StringUtils.isNotBlank(showRoute)) {
            emailBody = emailBody.replaceAll("==showPageBreak==", showRoute);
        }

        return emailBody;
    }


    private String generateShippingManifestForm(ShippingManifestResult shippingManifestResult,  boolean printRoute) {
        InputStream inputStream = InvoiceServiceImpl.class.getResourceAsStream(SHIPPING_MANIFEST_FORM_RESOURCE);
        String originAddress = "";
        String receiverAddress = "";
        String batchLink = "";
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in shipping manifest form", ex);
        }

        String body = writer.toString();
        Company company = companyRepository.getById(token.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company not found");
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }

        Invoice invoice = invoiceRepository.get(token.getCompanyId(), shippingManifestResult.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException("Invoice", INVOICE_NOT_FOUND);
        }

        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MM/dd/yy");
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("hh:mm");
        DateTimeFormatter timeSuffixFormatter = DateTimeFormat.forPattern("a");

        Vendor shipperInfo = null;
        CompanyLicense shipperLicense = null;
        Vendor receiverInfo = null;
        CompanyLicense receiverLicense = null;
        Employee driverInfo = null;
        CompanyContact shipperContact = null;
        CompanyContact receiverContact = null;
        Employee shipperEmployee = null;
        Vendor distributorInfo =null;
        CompanyLicense distributorLicense =null;

        String timeZone = token.getRequestTimeZone();

        String emptyString = "N/A";
        if (StringUtils.isBlank(timeZone)) {
            timeZone = shop.getTimeZone();
        }

        boolean isShipperVendor = Boolean.TRUE;
        if (shippingManifestResult.getShipperInformation() != null) {
            ShipperInformation shipperInformation = shippingManifestResult.getShipperInformation();
            if (StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                shipperInfo = vendorRepository.get(token.getCompanyId(), shipperInformation.getCustomerCompanyId());
                if (shipperInfo != null) {
                    shipperLicense = shipperInfo.getCompanyLicense(shipperInformation.getLicenseId());
                }
            } else {
                isShipperVendor = Boolean.FALSE;
            }
            if (StringUtils.isNotBlank(shipperInformation.getCompanyContactId())) {
                shipperContact = companyContactService.getCompanyContactById(shipperInformation.getCompanyContactId(), false);
            } else {
                shipperEmployee = employeeRepository.getById(shipperInformation.getEmployeeId());
            }
        }

        if (shippingManifestResult.getReceiverInformation() != null) {
            ReceiverInformation receiverInformation = shippingManifestResult.getReceiverInformation();
            if (StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                receiverInfo = vendorRepository.get(token.getCompanyId(), receiverInformation.getCustomerCompanyId());
                if (receiverInfo != null) {
                    receiverLicense = receiverInfo.getCompanyLicense(invoice.getLicenseId());
                }
                receiverAddress = receiverInformation.getCompanyAddress() != null ? receiverInformation.getCompanyAddress().getAddressString() : "";
            }
            if (StringUtils.isNotBlank(receiverInformation.getCompanyContactId())) {
                receiverContact = companyContactService.getCompanyContactById(receiverInformation.getCompanyContactId(), false);
            }
        }
        boolean isDistributorVendor = Boolean.FALSE;
        if (shippingManifestResult.getDistributorInformation() !=null) {
            DistributorInformation distributorInformation = shippingManifestResult.getDistributorInformation();
            if (StringUtils.isNotBlank(distributorInformation.getCustomerCompanyId())) {
                distributorInfo =vendorRepository.get(token.getCompanyId(), distributorInformation.getCustomerCompanyId());
                if (distributorInfo!= null) {
                    isDistributorVendor = true;
                    distributorLicense = distributorInfo.getCompanyLicense(invoice.getLicenseId());
                }

            }

        }


        driverInfo = employeeRepository.getById(shippingManifestResult.getDriverId());

        long shippingDate = DateUtil.toDateTime(shippingManifestResult.getDeliveryDate(), timeZone).getMillis();
        long shippingTime = DateUtil.toDateTime(shippingManifestResult.getDeliveryTime(), timeZone).getMillis();
        String shippingSuffix = timeSuffixFormatter.print(shippingTime);

        String distributorContactName = (Objects.isNull(shippingManifestResult.getDistributorCompanyContact())  ? "N/A"
                : shippingManifestResult.getDistributorCompanyContact().getFirstName() +
                " " + shippingManifestResult.getDistributorCompanyContact().getLastName());

        String distributorPhoneNumber = (Objects.isNull(shippingManifestResult.getDistributorCompanyContact())  ? "N/A"
                : shippingManifestResult.getDistributorCompanyContact().getPhoneNumber());

        StringBuilder address = null;
        StringBuilder contactName = new StringBuilder();
        StringBuilder contactNo = new StringBuilder();
        StringBuilder driverName = new StringBuilder();

        if (shipperInfo != null && shipperInfo.getAddress() != null) {
            address = this.prepareAddress(shipperInfo.getAddress());
        }

        if (shipperContact != null) {
            contactName.append(shipperContact.getFirstName()).append(" ").append(shipperContact.getLastName());
            contactNo.append(StringUtils.isNotBlank(shipperContact.getPhoneNumber()) ? shipperContact.getPhoneNumber() : StringUtils.isNotBlank(shipperContact.getOfficeNumber()) ? shipperContact.getOfficeNumber() : "");
        } else if (shipperEmployee != null) {
            contactName.append(shipperEmployee.getFirstName()).append(" ").append(shipperEmployee.getLastName());
            contactNo.append(StringUtils.isNotBlank(shipperEmployee.getPhoneNumber()) ? shipperEmployee.getPhoneNumber() : "");
        } else {
            contactName.append(emptyString);
        }

        StringBuilder shopAddress = null;
        if (shop.getAddress() != null) {
            shopAddress = this.prepareAddress(shop.getAddress());
        }

        body = body.replaceAll("==shippingManifestNo==", StringUtils.isNotBlank(shippingManifestResult.getShippingManifestNo()) ? shippingManifestResult.getShippingManifestNo() : emptyString);
        body = body.replaceAll("==invoiceNo==", StringUtils.isNotBlank(invoice.getInvoiceNumber()) ? invoice.getInvoiceNumber() : emptyString);
        body = body.replaceAll("==totalPageNo==", "2");
        body = body.replaceAll("==pageNo==", "2");

        body = body.replaceAll("==shippingDate==", dateFormatter.print(shippingDate) + " " + timeFormatter.print(shippingTime));
        body = body.replaceAll("==estimatedDate==", TextUtil.textOrEmpty(" "));

        if (shippingSuffix.equals("AM")) {
            body = body.replaceAll("==shippingAM==", "");
            body = body.replaceAll("==shippingPM==", "display:none;");
        } else {
            body = body.replaceAll("==shippingAM==", "display:none;");
            body = body.replaceAll("==shippingPM==", "");
        }

        if (isShipperVendor) {

            String shipperName = "";
            if (shipperInfo != null) {
                shipperName = shipperInfo.getName();

                if (StringUtils.isNotBlank(shipperInfo.getDbaName())) {
                    shipperName = shipperName + " (" + shipperInfo.getDbaName() + ")";
                }
            }

            contactNo.append(StringUtils.isBlank(contactNo) && StringUtils.isNotBlank(shipperInfo.getPhone()) ? shipperInfo.getPhone() : "");

            body = body.replaceAll("==shipperLicenseNo==", TextUtil.textOrEmpty(shipperLicense != null ? shipperLicense.getLicenseNumber() : emptyString));
            body = body.replaceAll("==shipperLicenseType==", TextUtil.textOrEmpty(shipperLicense != null && shipperLicense.getLicenseType() != null ? shipperLicense.getLicenseType().name() : emptyString));
            body = body.replaceAll("==shipperBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperName) ? shipperName : emptyString));
            body = body.replaceAll("==shipperAddress1==", TextUtil.textOrEmpty(shipperInfo != null && shipperInfo.getAddress() != null ? shipperInfo.getAddress().getAddress() : emptyString));
            body = body.replaceAll("==shipperAddress2==", TextUtil.textOrEmpty(address != null ? address.toString() : emptyString));
            body = body.replaceAll("==shipperPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
            body = body.replaceAll("==shipperContactName==", TextUtil.textOrEmpty(contactName.toString()));
            originAddress = shipperInfo.getAddress() != null ? shipperInfo.getAddress().getAddressString() : "";

        } else {
            contactNo.append(StringUtils.isBlank(contactNo) && StringUtils.isNotBlank(shop.getPhoneNumber()) ? shop.getPhoneNumber() : "");
            body = body.replaceAll("==shipperLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getLicense()) ? shop.getLicense() : emptyString));
            body = body.replaceAll("==shipperLicenseType==", TextUtil.textOrEmpty(shop.getShopType().name()));
            body = body.replaceAll("==shipperBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getName()) ? shop.getName() : emptyString));
            body = body.replaceAll("==shipperAddress1==", TextUtil.textOrEmpty((shop.getAddress() != null && shop.getAddress().getAddress() != null) ? shop.getAddress().getAddress() : emptyString));
            body = body.replaceAll("==shipperAddress2==", TextUtil.textOrEmpty(shopAddress != null ? shopAddress.toString() : emptyString));
            body = body.replaceAll("==shipperPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
            body = body.replaceAll("==shipperPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
            body = body.replaceAll("==shipperContactName==", TextUtil.textOrEmpty(contactName.toString()));

            originAddress = shop.getAddress() != null ? shop.getAddress().getAddressString() : "";
        }

        if (isDistributorVendor) {
            address = new StringBuilder();
            if (distributorInfo != null && distributorInfo.getAddress() != null) {
                address = this.prepareAddress(distributorInfo.getAddress());
            }

            contactName = new StringBuilder();
            contactNo = new StringBuilder();
            String distributorName = "";
            if (distributorInfo != null) {
                distributorName = distributorInfo.getName();

                if (StringUtils.isNotBlank(distributorInfo.getDbaName())) {
                    distributorName = distributorName + " (" + distributorInfo.getDbaName() + ")";
                }

                contactName.append(distributorInfo.getFirstName()).append(" ").append(distributorInfo.getLastName());
                contactNo.append(StringUtils.isNotBlank(distributorInfo.getPhone()) ? distributorInfo.getPhone(): "");
            }

            contactNo.append(StringUtils.isBlank(contactNo) && distributorInfo != null && StringUtils.isNotBlank(distributorInfo.getPhone()) ? distributorInfo.getPhone() : "");

            body = body.replaceAll("==distributorLicenseNo==", TextUtil.textOrEmpty(distributorLicense != null ? distributorLicense.getLicenseNumber() : emptyString));
            body = body.replaceAll("==distributorLicenseType==", TextUtil.textOrEmpty(distributorLicense != null && distributorLicense.getLicenseType() != null ? distributorLicense.getLicenseType().name() : emptyString));
            body = body.replaceAll("==distributorBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(distributorName) ? distributorName : emptyString));
            body = body.replaceAll("==distributorAddress1==", TextUtil.textOrEmpty(distributorInfo != null && distributorInfo.getAddress() != null ? distributorInfo.getAddress().getAddress() : emptyString));
            body = body.replaceAll("==distributorAddress2==", TextUtil.textOrEmpty(address != null ? address.toString() : emptyString));
            body = body.replaceAll("==distributorPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(distributorPhoneNumber) ? distributorPhoneNumber : emptyString));
            body = body.replaceAll("==distributorContactName==", TextUtil.textOrEmpty(distributorContactName));


        } else {
            contactNo.append(StringUtils.isBlank(contactNo) && StringUtils.isNotBlank(shop.getPhoneNumber()) ? shop.getPhoneNumber() : "");
            body = body.replaceAll("==distributorLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getLicense()) ? shop.getLicense() : emptyString));
            body = body.replaceAll("==distributorLicenseType==", TextUtil.textOrEmpty(shop.getShopType().name()));
            body = body.replaceAll("==distributorBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getName()) ? shop.getName() : emptyString));
            body = body.replaceAll("==distributorAddress1==", TextUtil.textOrEmpty((shop.getAddress() != null && shop.getAddress().getAddress() != null) ? shop.getAddress().getAddress() : emptyString));
            body = body.replaceAll("==distributorAddress2==", TextUtil.textOrEmpty(shopAddress != null ? shopAddress.toString() : emptyString));
            body = body.replaceAll("==distributorPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
            body = body.replaceAll("==distributorPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
            body = body.replaceAll("==distributorContactName==", TextUtil.textOrEmpty(contactName.toString()));

        }


        address = new StringBuilder();
        if (receiverInfo != null && receiverInfo.getAddress() != null) {
            address = this.prepareAddress(receiverInfo.getAddress());
        }

        contactName = new StringBuilder();
        contactNo = new StringBuilder();

        if (receiverContact != null) {
            contactName.append(receiverContact.getFirstName()).append(" ").append(receiverContact.getLastName());
            contactNo.append(StringUtils.isNotBlank(receiverContact.getPhoneNumber()) ? receiverContact.getPhoneNumber() : StringUtils.isNotBlank(receiverContact.getOfficeNumber()) ? receiverContact.getOfficeNumber() : "");
        } else {
            contactName.append(emptyString);
        }

        if (driverInfo != null) {
            driverName.append(driverInfo.getFirstName()).append(" ").append(driverInfo.getLastName());
        } else {
            driverName.append(emptyString);
        }

        Employee approvedByEmployee = employeeRepository.get(token.getCompanyId(), shippingManifestResult.getApproverEmployeeId());
        String approvedByEmployeeName = null;
        if (approvedByEmployee != null) {
            approvedByEmployeeName = approvedByEmployee.getFirstName() + " " + approvedByEmployee.getLastName();
        }

        contactNo.append(StringUtils.isBlank(contactNo) && (receiverInfo != null && StringUtils.isNotBlank(receiverInfo.getPhone())) ? receiverInfo.getPhone() : "");

        String receiverName = "";
        if (receiverInfo != null) {
            receiverName = receiverInfo.getName();

            if (StringUtils.isNotBlank(receiverInfo.getDbaName())) {
                receiverName = receiverName + " (" + receiverInfo.getDbaName() + ")";
            }
        }

        body = body.replaceAll("==receiverLicenseNo==", TextUtil.textOrEmpty(receiverLicense != null ? receiverLicense.getLicenseNumber() : emptyString));
        body = body.replaceAll("==receiverLicenseType==", TextUtil.textOrEmpty(receiverLicense != null && receiverLicense.getLicenseType() != null ? receiverLicense.getLicenseType().name() : emptyString));
        body = body.replaceAll("==receiverBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(receiverName) ? receiverName : emptyString));
        body = body.replaceAll("==receiverAddress1==", TextUtil.textOrEmpty(receiverInfo != null && receiverInfo.getAddress() != null ? receiverInfo.getAddress().getAddress() : emptyString));
        body = body.replaceAll("==receiverAddress2==", TextUtil.textOrEmpty(address != null ? address.toString() : emptyString));
        body = body.replaceAll("==receiverPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
        body = body.replaceAll("==receiverContactName==", TextUtil.textOrEmpty(contactName.toString()));

        receiverAddress = (StringUtils.isBlank(receiverAddress) && receiverInfo.getAddress() != null) ? receiverInfo.getAddress().getAddressString() : receiverAddress;

        body = body.replaceAll("==distributorLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getLicense()) ? shop.getLicense() : emptyString));
        body = body.replaceAll("==distributorLicenseType==", TextUtil.textOrEmpty(shop.getShopType().name()));
        body = body.replaceAll("==distributorBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getName()) ? shop.getName() : emptyString));
        body = body.replaceAll("==distributorAddress1==", TextUtil.textOrEmpty((shop.getAddress() != null && shop.getAddress().getAddress() != null) ? shop.getAddress().getAddress() : emptyString));
        body = body.replaceAll("==distributorAddress2==", TextUtil.textOrEmpty(shopAddress != null ? shopAddress.toString() : emptyString));
        body = body.replaceAll("==distributorPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getPhoneNumber()) ? shop.getPhoneNumber() : emptyString));
        body = body.replaceAll("==distributorContactName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(approvedByEmployeeName) ? approvedByEmployeeName : emptyString));

        body = body.replaceAll("==driverName==", TextUtil.textOrEmpty(driverName.toString()));
        body = body.replaceAll("==driverLicenseNo==", TextUtil.textOrEmpty(driverInfo != null && StringUtils.isNotBlank(driverInfo.getDriversLicense()) ? driverInfo.getDriversLicense() : emptyString));
        body = body.replaceAll("==driverVehicleMake==", TextUtil.textOrEmpty(driverInfo != null && StringUtils.isNotBlank(driverInfo.getVehicleMake()) ? driverInfo.getVehicleMake() : emptyString));
        body = body.replaceAll("==driverVehicleModel==", TextUtil.textOrEmpty(driverInfo != null && StringUtils.isNotBlank(driverInfo.getVehicleModel()) ? driverInfo.getVehicleModel() : emptyString));
        body = body.replaceAll("==driverVehiclePlate==", TextUtil.textOrEmpty(driverInfo != null && StringUtils.isNotBlank(driverInfo.getVehicleLicensePlate()) ? driverInfo.getVehicleLicensePlate() : emptyString));
        body = body.replaceAll("==arrivalTime==", TextUtil.textOrEmpty(" "));

        body = body.replaceAll("==rejectionReason==", StringUtils.isNotBlank(shippingManifestResult.getDeclineReason()) ? shippingManifestResult.getDeclineReason() : "");


        String notes = "";
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(shippingManifestResult.getNotes())) {
            notes = getNoteSection(shippingManifestResult.getNotes());
            body = body.replaceAll("==notes==", notes.replaceAll("&", "&amp;"));
            body = body.replaceAll("==showNotes==", TextUtil.textOrEmpty(StringUtils.isNotBlank(notes) ? "" : "display:none"));
        } else {
            body = body.replaceAll("==showNotes==", TextUtil.textOrEmpty("display:none;"));
        }

        if (printRoute) {
            body = generateRouteBody(originAddress, receiverAddress, body, notes);
        } else {
            body = body.replaceAll("==showRouteDetail==", "display:none;");
            body = body.replaceAll("==showPageBreak==", "display:none;");
        }

        HashMap<String, List<ShippingBatchDetails>> shippedProductBatchMap = new HashMap<>();
        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> categoryIds = new ArrayList<>();
        List<ObjectId> productBatchIds = new ArrayList<>();
        List<ObjectId> prepackageIds = new ArrayList<>();
        List<ObjectId> prepackageItemsIds = new ArrayList<>();
        HashMap<String, Product> productHashMap = new HashMap<>();

        for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
            if (!productMetrcInfo.getBatchDetails().isEmpty()) {
                shippedProductBatchMap.put(productMetrcInfo.getOrderItemId(), productMetrcInfo.getBatchDetails());
                for (ShippingBatchDetails result : productMetrcInfo.getBatchDetails()) {
                    if (result.getBatchId() != null && ObjectId.isValid(result.getBatchId())) {
                        productBatchIds.add(new ObjectId(result.getBatchId()));
                    }
                    if (StringUtils.isNotBlank(result.getPrepackageItemId()) && ObjectId.isValid(result.getPrepackageItemId())) {
                        prepackageItemsIds.add(new ObjectId(result.getPrepackageItemId()));
                    }
                }
            }
            if (StringUtils.isNotBlank(productMetrcInfo.getProductId()) && ObjectId.isValid(productMetrcInfo.getProductId())) {
                productIds.add(new ObjectId(productMetrcInfo.getProductId()));
            }
        }

        for (OrderItem item : invoice.getCart().getItems()) {
            if (StringUtils.isNotBlank(item.getPrepackageId())) {
                prepackageIds.add(new ObjectId(item.getPrepackageId()));
            }
        }

        if (!productIds.isEmpty()) {
            productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        }

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(token.getCompanyId(), prepackageIds);

        HashMap<String, PrepackageProductItem> prepackageProductItemMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), prepackageItemsIds);
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(token.getCompanyId());

        for (Map.Entry<String, PrepackageProductItem> productItem : prepackageProductItemMap.entrySet()) {

            if (StringUtils.isNotBlank(productItem.getValue().getBatchId())) {
                productBatchIds.add(new ObjectId(productItem.getValue().getBatchId()));
            }
        }


        HashMap<String, ProductBatch> productBatchMap = productBatchRepository.listAsMap(token.getCompanyId(), productBatchIds);
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), categoryIds);

        productBatchIds.clear();
        productIds.clear();
        productBatchMap.values().forEach(productBatch -> {
            if (CollectionUtils.isNullOrEmpty(productBatch.getBundleItems())) {
                return;
            }

            productBatch.getBundleItems().forEach(productBundleItems -> {
                if (StringUtils.isNotBlank(productBundleItems.getProductId()) && ObjectId.isValid(productBundleItems.getProductId())) {
                    productIds.add(new ObjectId(productBundleItems.getProductId()));
                }

                if (CollectionUtils.isNullOrEmpty(productBundleItems.getBatchItems())) {
                    return;
                }

                productBundleItems.getBatchItems().forEach(batchItems -> {
                    if (StringUtils.isNotBlank(batchItems.getBatchId()) && ObjectId.isValid(batchItems.getBatchId())) {
                        productBatchIds.add(new ObjectId(batchItems.getBatchId()));
                    }
                });

            });

        });
        productBatchMap.putAll(productBatchRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), productBatchIds));
        productHashMap.putAll(productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), productIds));

        StringBuilder productInfo = new StringBuilder();
        int productCount = 0;
        for (OrderItem item : invoice.getCart().getItems()) {
            if (!shippedProductBatchMap.containsKey(item.getId())) {
                continue;
            }
            Product product = productHashMap.get(item.getProductId());

            if (product == null) {
                continue;
            }

            String categoryStr = "units";
            ProductCategory category = categoryHashMap.get(product.getCategoryId());
            if (category != null) {
                categoryStr = category.getUnitType().toString();
            }

            List<ShippingBatchDetails> batchDetails = shippedProductBatchMap.getOrDefault(item.getId(), new ArrayList<>());

            Brand brand = brandHashMap.get(product.getBrandId());
            MathContext precision = new MathContext(3);
            boolean iterated = Boolean.FALSE;
            for (ShippingBatchDetails shippingBatchDetails : batchDetails) {
                PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(shippingBatchDetails.getPrepackageItemId());
                Prepackage prepackage = prepackageMap.get(item.getPrepackageId());
                ProductBatch batch = productBatchMap.get(shippingBatchDetails.getBatchId());

                BigDecimal  shipQty = new BigDecimal(shippingBatchDetails.getQuantity().doubleValue());
                StringBuilder productName = new StringBuilder(product.getName());
                String uidTag = "-";
                String itemQty = "-";
                BigDecimal productBatchDiscount = new BigDecimal(0);
                BigDecimal batchFinalCost = new BigDecimal(0);

                if (prepackage != null && prepackageProductItem != null) {
                    batch = productBatchMap.get(prepackageProductItem.getBatchId());
                    productName.append(" ( ").append(prepackage.getName());
                    if (brand != null) {
                        productName.append(" - " + brand.getName());
                    }
                    productName.append(" )");
                } else if (brand != null) {
                    productName.append(" ( " + brand.getName() + " )");
                }
                StringBuilder bundleUidTags = new StringBuilder();
                if (batch != null) {
                    if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                        uidTag = StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "";
                    } else {
                        uidTag = StringUtils.isNotBlank(batch.getTrackHarvestBatch()) ? batch.getTrackHarvestBatch() : batch.getSku();
                    }

                    uidTag +=  " (" + DateUtil.toDateFormatted(batch.getPurchasedDate(), timeZone) + ")";
                    if (StringUtils.isNotBlank(shippingBatchDetails.getReqMetrcLabel()) && shippingBatchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                        uidTag += shippingBatchDetails.getReqMetrcLabel();
                    } else {
                        uidTag += StringUtils.isNotBlank(batch.getTrackPackageLabel()) ? " - " + batch.getTrackPackageLabel() : "";
                    }

                    batchLink = getBatchAttachmentLink(batch);
                    if (product.getProductType() == Product.ProductType.BUNDLE && !CollectionUtils.isNullOrEmpty(batch.getBundleItems())) {
                        int i = 0;
                        for (BatchBundleItems batchBundleItems : batch.getBundleItems()) {
                            Product itemProduct = productHashMap.get(batchBundleItems.getProductId());

                            if (!CollectionUtils.isNullOrEmpty(batchBundleItems.getBatchItems())) {
                                for (BatchBundleItems.BatchItems batchItems : batchBundleItems.getBatchItems()) {
                                    ProductBatch itemBatch = productBatchMap.get(batchItems.getBatchId());
                                    if (itemBatch == null) {
                                        continue;
                                    }
                                    String outcomingMetrcLabel = StringUtils.isNotBlank(itemBatch.getTrackPackageLabel()) ? " - " + itemBatch.getTrackPackageLabel() : "";

                                    if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails.getMetrcPackageDetails())) {
                                        ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails = shippingBatchDetails.getMetrcPackageDetails().stream().filter(metrcPackageDetail -> metrcPackageDetail.getBatchId().equalsIgnoreCase(itemBatch.getId())).findFirst().orElse(null);
                                        if (metrcPackageDetails != null && StringUtils.isNotBlank(metrcPackageDetails.getReqMetrcLabel()) && metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                                            outcomingMetrcLabel = metrcPackageDetails.getReqMetrcLabel();
                                        }

                                    }
                                    String itemBatchLink = getBatchAttachmentLink(itemBatch);
                                    String itemUidTag = "";
                                    if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                                        itemUidTag = StringUtils.isNotBlank(itemBatch.getSku()) ? itemBatch.getSku() : "";
                                    } else {
                                        itemUidTag = StringUtils.isNotBlank(itemBatch.getTrackHarvestBatch()) ? itemBatch.getTrackHarvestBatch() : itemBatch.getSku();
                                    }
                                    itemUidTag += " (" + DateUtil.toDateFormatted(itemBatch.getPurchasedDate(), timeZone) + ")";
                                    itemUidTag += outcomingMetrcLabel;
                                    bundleUidTags.append(StringUtils.isNotBlank(bundleUidTags) ? " <br/> " : "");
                                    bundleUidTags.append((++i) + ".").append(product.getName()).append("/").append((itemProduct != null) ? itemProduct.getName() : "").append(" - ");
                                    bundleUidTags.append(StringUtils.isNotBlank(itemBatchLink) ? String.format("<a href='%s'>%s</a>", itemBatchLink, itemUidTag) : itemUidTag);
                                }
                            }
                        }
                    }
                    if (item.getDiscount() != null && item.getDiscount().doubleValue() > 0) {
                        productBatchDiscount = item.getDiscount().divide(item.getQuantity(), precision).multiply(shipQty);
                    }
                    batchFinalCost = shipQty.multiply(item.getUnitPrice());
                    batchFinalCost = batchFinalCost.subtract(productBatchDiscount);
                    if (!iterated) {
                        itemQty = TextUtil.formatToTwoDecimalPoints(item.getQuantity()) + " " + categoryStr;
                    }

                    uidTag = StringUtils.isNotBlank(batchLink) ? String.format("<a href='%s'>%s</a>", batchLink, uidTag) : uidTag;
                    if (StringUtils.isNotBlank(bundleUidTags)) {
                        uidTag += StringUtils.isNotBlank(uidTag) ? "" : "";
                     //   uidTag += bundleUidTags.toString();
                    }
                }

                String productNameStr = productName.toString();
                if (StringUtils.isNotBlank(bundleUidTags)) {
                    uidTag += String.format("<br/> <div style=\"text-align:left\"><br/>%s</div>", StringUtils.isNotBlank(bundleUidTags) ? bundleUidTags.toString() : "");
                }
                productNameStr = String.format("%s", productName);
                productInfo.append("<tr><td class=\"content text-center\" style=\"width: 15%\">" + uidTag + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 25%; word-wrap: break-word;\">" + productNameStr + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 9%\">" + itemQty + "</td>")
                        .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 10%\">" + TextUtil.formatToTwoDecimalPoints(shipQty) + categoryStr + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toEscapeCurrency(item.getOverridePrice().doubleValue(), shop.getDefaultCountry()) + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toEscapeCurrency(productBatchDiscount.doubleValue() , shop.getDefaultCountry()) + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toEscapeCurrency(batchFinalCost.doubleValue(), shop.getDefaultCountry()) + "</td>")
                        .append("<td style=\"width: 11%\" class=\"bg-dark-grey content text-center\"> </td>")
                        .append("<td class=\"bg-dark-grey content text-center\"> </td>")
                        .append("</tr>");
                iterated = Boolean.TRUE;
                productCount++;
            }
        }
        for (int i = 0; i < 3; i++) {
            this.addEmptyProductRaw(productInfo);
            productCount++;
        }

        body = body.replaceAll("==productInformation==", productInfo.toString());
        body = body.replaceAll("&", "&amp;");

        body = body.replaceAll("==pageBreakPage1==", productCount == 6 ?  "page-break-before: always" : (productCount > 5 ? "" : "page-break-before: always"));
        return body;
    }

    private void addEmptyProductRaw(StringBuilder productInfo) {
        productInfo.append("<tr style=\"height: 30px;\"><td class=\"content text-center\" style=\"width: 15%\">  </td>")
                .append("<td class=\"content text-center\" style=\"width: 25%; word-wrap: break-word;\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 9%\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 10%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\"> </td>")
                .append("<td style=\"width: 11%\" class=\"bg-dark-grey content text-center\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\"> </td>")
                .append("</tr>");
    }

    private StringBuilder prepareAddress(Address address) {
        StringBuilder addressData = new StringBuilder();
        if (StringUtils.isNotBlank(address.getCity())) {
            addressData.append(address.getCity());
        }
        if (StringUtils.isNotBlank(address.getState())) {
            addressData.append(", ");
            addressData.append(address.getState());
        }
        if (StringUtils.isNotBlank(address.getZipCode())) {
            addressData.append(", ");
            addressData.append(address.getZipCode());
        }

        return addressData;
    }

    /**
     * Private method to prepare distrbutor info for manifest result
     *
     * @param shippingManifestResult : manifestResult
     */
    private void prepareShippingManifestResultForDistributor(final ShippingManifestResult shippingManifestResult) {
        if (shippingManifestResult.getDistributorInformation() != null) {
            if (StringUtils.isNotBlank(shippingManifestResult.getDistributorInformation().getCustomerCompanyId())) {
                Vendor distributorCustomerCompany = vendorRepository.getById(shippingManifestResult.getDistributorInformation().getCustomerCompanyId());
                if (distributorCustomerCompany != null) {
                    shippingManifestResult.setDistributorCustomerCompany(distributorCustomerCompany);
                }
            } else {
                String companyId = token.getShopId();
                if (StringUtils.isNotBlank(shippingManifestResult.getDistributorInformation().getShopId())) {
                    companyId = shippingManifestResult.getDistributorInformation().getShopId();
                }
                ShopLimitedView distributorShop = shopRepository.getById(companyId, ShopLimitedView.class);
                if (distributorShop != null) {
                    shippingManifestResult.setDistributorShop(distributorShop);
                }

            }
            if (StringUtils.isNotBlank(shippingManifestResult.getDistributorInformation().getCompanyContactId())) {
                CompanyContact distributorCompanyContact = companyContactService.getCompanyContactById(shippingManifestResult.getDistributorInformation().getCompanyContactId(), false);
                if (distributorCompanyContact != null) {
                    shippingManifestResult.setDistributorCompanyContact(distributorCompanyContact);
                }

            }
        }

    }

    /**
     * Override method to add shipping manifest
     *
     * @param shippingManifestAddRequest : addRequest
     * @return : list of manifest
     */
    @Override
    public List<ShippingManifestResult> addShippingManifestForSales(ShippingManifestAddRequest shippingManifestAddRequest) {

        return updateShippingManifestForSales("", null, shippingManifestAddRequest);
    }

    /**
     * Override method to update manifest for grow sales
     *
     * @param shippingManifestId : shippingManifestId
     * @param shippingManifest   : shippingManifest
     * @param addRequest         : addRequest
     * @return : list of manifest
     */
    @Override
    public List<ShippingManifestResult> updateShippingManifestForSales(String shippingManifestId, ShippingManifest shippingManifest, ShippingManifestAddRequest addRequest) {
        List<ShippingManifestResult> shippingManifestResults = new ArrayList<>();
        List<ShippingManifestAddRequest> addManifests = new ArrayList<>();
        List<ShippingManifestResult> addManifestResults = new ArrayList<>();
        List<ShippingManifest> updateManifests = new ArrayList<>();

        String invoiceId = (shippingManifest == null) ? addRequest.getInvoiceId() : shippingManifest.getInvoiceId();

        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_EMPTY);
        }
        Invoice dbInvoice = invoiceRepository.getById(invoiceId);
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }
        ShippingManifest dbManifest = null;
        if (StringUtils.isNotBlank(shippingManifestId)) {
            dbManifest = shippingManifestRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), shippingManifestId);
            if (dbManifest == null) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
            }
        }

        List<ProductMetrcInfo> metrcInfoList = (shippingManifest == null) ? addRequest.getProductMetrcInfo() : shippingManifest.getProductMetrcInfo();
        if (CollectionUtils.isNullOrEmpty(metrcInfoList)) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_NOT_EMPTY);
        }
        SearchResult<ShippingManifestResult> shippingManifestsByInvoiceId = shippingManifestRepository.getShippingManifestByInvoiceId(token.getCompanyId(), invoiceId, StringUtils.EMPTY, 0, Integer.MAX_VALUE);

        Map<String, ShippingManifestResult> manifestResultMap = new HashMap<>();
        shippingManifestsByInvoiceId.getValues().forEach(shippingManifestResult -> {
            if (shippingManifestResult.getStatus() != ShippingManifest.ShippingManifestStatus.InProgress) {
                return;
            }
            if (StringUtils.isNotBlank(shippingManifestId) && shippingManifestId.equalsIgnoreCase(shippingManifestResult.getId())) {
                return;
            }
            String license = StringUtils.isNotBlank(shippingManifestResult.getExternalLicense()) ? shippingManifestResult.getExternalLicense() : "";
            manifestResultMap.put(license, shippingManifestResult);
        });

        HashMap<String, List<ProductMetrcInfo>> addMetrcInfoMap = this.prepareShippingManifest(metrcInfoList);
        if (dbManifest != null && !addMetrcInfoMap.containsKey(dbManifest.getExternalLicense()) && !manifestResultMap.isEmpty()) {
            deleteShippingManifest(dbManifest.getId());
        }

        for (Map.Entry<String, List<ProductMetrcInfo>> entry : addMetrcInfoMap.entrySet()) {
            String license = entry.getKey();
            license = StringUtils.isNotBlank(license) ? license : "";

            HashMap<String, ProductMetrcInfo> addMetrcInfoByIdMap = new HashMap<>();
            entry.getValue().forEach(info -> {
                addMetrcInfoByIdMap.put(info.getOrderItemId(), info);
            });

            if (shippingManifest != null && (license.equalsIgnoreCase(shippingManifest.getExternalLicense()) || manifestResultMap.isEmpty())) {

                shippingManifest.setProductMetrcInfo(entry.getValue());
                shippingManifest.setExternalLicense(license);
                updateManifests.add(this.updateShippingManifest(shippingManifestId, shippingManifest, true));

            } else if (manifestResultMap.containsKey(license)) {

                ShippingManifest updateManifest = manifestResultMap.get(license);
                updateManifest.setExternalLicense(license);
                List<ProductMetrcInfo> updateInfo = new ArrayList<>();
                for (ProductMetrcInfo dbInfo: updateManifest.getProductMetrcInfo()) {
                    if (addMetrcInfoByIdMap.containsKey(dbInfo.getOrderItemId())) {
                        List<ShippingBatchDetails> addInfo = addMetrcInfoByIdMap.get(dbInfo.getOrderItemId()).getBatchDetails();
                        boolean isFound = false;
                        for (ShippingBatchDetails addDetail : addInfo) {
                            for (ShippingBatchDetails dbDetail: dbInfo.getBatchDetails()) {
                                if (StringUtils.isNotBlank(dbDetail.getBatchId())
                                        && dbDetail.getBatchId().equalsIgnoreCase(addDetail.getBatchId())) {
                                    dbDetail.setQuantity(addDetail.getQuantity().add(dbDetail.getQuantity()));
                                    dbDetail.setOverrideInventoryId(addDetail.getOverrideInventoryId());
                                    dbDetail.setReqMetrcLabel(addDetail.getReqMetrcLabel().trim());
                                    isFound = true;
                                    break;
                                }
                            }
                            if (!isFound) {
                                dbInfo.getBatchDetails().add(addDetail);
                            }
                        }
                        addMetrcInfoByIdMap.remove(dbInfo.getOrderItemId());
                    }
                    updateInfo.add(dbInfo);
                }
                updateInfo.addAll(addMetrcInfoByIdMap.values());
                updateManifest.setProductMetrcInfo(updateInfo);
                updateManifests.add(this.updateShippingManifest(updateManifest.getId(), updateManifest, true));

            } else if (!manifestResultMap.containsKey(license)) {
                ShippingManifestAddRequest manifestAddRequest = null;
                if (addRequest == null && shippingManifest != null) {
                    manifestAddRequest = new ShippingManifestAddRequest();
                    manifestAddRequest.parse(shippingManifest);
                    manifestAddRequest.getProductMetrcInfo().addAll(entry.getValue());
                    manifestAddRequest.setExternalLicense(license);
                } else if (addRequest != null){
                    manifestAddRequest = new Kryo().copy(addRequest);
                    manifestAddRequest.setProductMetrcInfo(entry.getValue());
                    manifestAddRequest.setExternalLicense(license);
                }
                if (manifestAddRequest != null) {
                    addManifestResults.add(this.addShippingManifest(manifestAddRequest, true));
                    addManifests.add(manifestAddRequest);
                }
            }
        }
        verifyShippingQuantities(dbInvoice, addManifestResults, updateManifests);

        for (ShippingManifestAddRequest addManifest : addManifests) {
            shippingManifestResults.add(addShippingManifest(addManifest));
        }
        for (ShippingManifest updateManifest : updateManifests) {
            shippingManifestResults.add(updateShippingManifest(updateManifest.getId(), updateManifest));
        }

        return shippingManifestResults;
    }

    private void verifyShippingQuantities(Invoice dbInvoice, List<ShippingManifestResult> addManifests, List<ShippingManifest> updateManifests) {
        HashMap<String, BigDecimal> invoiceProductMap = new HashMap<>();
        HashMap<String, String> orderItemMap = new HashMap<>();
        HashMap<String, BigDecimal> shippedProductMap = new HashMap<>();

        if (dbInvoice.getCart() != null && dbInvoice.getCart().getItems() != null && !dbInvoice.getCart().getItems().isEmpty()) {
            for (OrderItem productRequest : dbInvoice.getCart().getItems()) {
                BigDecimal requestQuantity = productRequest.getQuantity();
                String itemId = productRequest.getId();
                invoiceProductMap.put(itemId, requestQuantity.add(invoiceProductMap.getOrDefault(itemId, BigDecimal.ZERO)));
                orderItemMap.put(itemId, productRequest.getProductId());
            }
        }
        SearchResult<ShippingManifestResult> searchResult = shippingManifestRepository.getShippingManifestByInvoiceId(token.getCompanyId(), dbInvoice.getId(), "{modified:-1}", 0, Integer.MAX_VALUE);
        for (ShippingManifest shippingManifest : searchResult.getValues()) {
            if (shippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Rejected) {
                continue;
            }
            /* In case of update not include shipping manifest for given id */
            ShippingManifest dbManifest = updateManifests.stream().filter(shippingManifest1 -> shippingManifest1.getId().equalsIgnoreCase(shippingManifest.getId())).findFirst().orElse(shippingManifest);
            HashMap<String, BigDecimal> dbShippedProductMap = getProductMapByItem(dbInvoice, dbManifest.getProductMetrcInfo(), false);

            for (String key : dbShippedProductMap.keySet()) {
                if (shippedProductMap.containsKey(key)) {
                    BigDecimal quantity = dbShippedProductMap.get(key);
                    quantity = quantity.add(shippedProductMap.get(key));
                    shippedProductMap.put(key, quantity);
                } else {
                    shippedProductMap.put(key, dbShippedProductMap.get(key));
                }
            }
        }

        HashMap<String, BigDecimal> requestMap = new HashMap<>();
        for (ShippingManifest shippingManifest : addManifests) {
            if (shippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Rejected) {
                continue;
            }
            /* In case of update not include shipping manifest for given id */
            HashMap<String, BigDecimal> requestProductMap = getProductMapByItem(dbInvoice, shippingManifest.getProductMetrcInfo(), false);

            for (String key : requestProductMap.keySet()) {
                if (requestMap.containsKey(key)) {
                    BigDecimal quantity = requestProductMap.get(key);
                    quantity = quantity.add(requestMap.get(key));
                    requestMap.put(key, quantity);
                } else {
                    requestMap.put(key, requestProductMap.get(key));
                }
            }
        }
        for (String invoiceProductKey : invoiceProductMap.keySet()) {

            BigDecimal invoiceQuantity = invoiceProductMap.get(invoiceProductKey);

            BigDecimal shippedQuantity = shippedProductMap.get(invoiceProductKey);
            shippedQuantity = (shippedQuantity == null) ? new BigDecimal(0) : shippedQuantity;

            BigDecimal requestQuantity = requestMap.get(invoiceProductKey);
            requestQuantity = (requestQuantity == null) ? new BigDecimal(0) : requestQuantity;

            requestQuantity = requestQuantity.add(shippedQuantity);

            if (requestQuantity.doubleValue() > invoiceQuantity.doubleValue()) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_QUANTITY_EXCEEDS);
            }
        }
    }

    /**
     * Private method to prepare update product/batch details.
     *
     * @param productMetrcInfos : productMetrcInfo
     * @return : return
     */
    private HashMap<String, List<ProductMetrcInfo>> prepareShippingManifest(List<ProductMetrcInfo> productMetrcInfos) {
        HashMap<String, List<ProductMetrcInfo>> productMetrcInfoHashMap = new HashMap<>();

        Set<ObjectId> batchIds = new HashSet<>();
        for (ProductMetrcInfo metrcInfo : productMetrcInfos) {
            for (ShippingBatchDetails batchDetail : metrcInfo.getBatchDetails()) {
                if (StringUtils.isNotBlank(batchDetail.getBatchId()) && ObjectId.isValid(batchDetail.getBatchId())) {
                    batchIds.add(new ObjectId(batchDetail.getBatchId()));
                }
            }
        }
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));

        for (ProductMetrcInfo metrcInfo : productMetrcInfos) {
            for (ShippingBatchDetails batchDetail : metrcInfo.getBatchDetails()) {
                ProductBatch productBatchResult = batchHashMap.get(batchDetail.getBatchId());
                if (productBatchResult == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, BATCH_NOT_FOUND);
                }

                boolean isFound = false;
                if (productMetrcInfoHashMap.containsKey(productBatchResult.getExternalLicense())) {
                    for (ProductMetrcInfo info : productMetrcInfoHashMap.get(productBatchResult.getExternalLicense())) {
                        if (info.getOrderItemId().equalsIgnoreCase(metrcInfo.getOrderItemId())) {
                            info.getBatchDetails().add(batchDetail);
                            isFound = true;
                            break;
                        }
                    }
                }
                if (!isFound){
                    ProductMetrcInfo productMetrcInfo = new ProductMetrcInfo();
                    productMetrcInfo.setOrderItemId(metrcInfo.getOrderItemId());
                    productMetrcInfo.setProductId(metrcInfo.getProductId());
                    productMetrcInfo.setQuantity(metrcInfo.getQuantity());
                    productMetrcInfo.getBatchDetails().add(batchDetail);
                    productMetrcInfoHashMap.putIfAbsent(productBatchResult.getExternalLicense(), Lists.newArrayList());
                    productMetrcInfoHashMap.get(productBatchResult.getExternalLicense()).add(productMetrcInfo);
                }
            }
        }

        return productMetrcInfoHashMap;
    }

    @Override
    public Long countShippingManifestInProgress(String companyId, String invoiceId, ShippingManifest.ShippingManifestStatus status) {
        return shippingManifestRepository.countShippingManifestInProgress(companyId, invoiceId, status);
    }

    /**
     * Override method to un finalize shipping manifest
     * @param shippingManifestId : shippingManifestId
     * @return
     */
    @Override
    public ShippingManifestResult unfinalizeShippingManifest(final String shippingManifestId) {

        ShippingManifest dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.InProgress) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, MANIFEST_ALREADY_INPROGRESS);
        }
        boolean isAllowed = checkEmployeePermission(token.getActiveTopUser().getUserId());

        if (!isAllowed) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(EMPLOYEE_DO_NOT_ALLOWED, "un-finalize"));
        }

        if (isAssignedConnectedShipment(shippingManifestId)){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(CONNECTED_SHIPMENT_ASSIGNED, "unfinalized"));
        }else if (isAcceptedConnectedShipment(shippingManifestId)){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(CONNECTED_SHIPMENT_ACCEPTED, "unfinalized"));
        }else{
            rejectConnectedShipment(shippingManifestId);
        }

        Invoice invoice = invoiceRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), dbShippingManifest.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }

        clearMetrcFields(dbShippingManifest);
        dbShippingManifest.setStatus(ShippingManifest.ShippingManifestStatus.InProgress);
        dbShippingManifest.setApprovalDate(0);
        dbShippingManifest.setApproverEmployeeId(null);

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        //Process shipping manifest inventory
        invoiceInventoryService.processInvoiceForRevertManifest(shop, invoice, dbShippingManifest);

        invoice.setInvoiceStatus(Invoice.InvoiceStatus.IN_PROGRESS);
        invoice.setActive(!(invoice.getInvoicePaymentStatus() == Invoice.PaymentStatus.PAID));
        invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);

        dbShippingManifest = shippingManifestRepository.update(token.getCompanyId(), shippingManifestId, dbShippingManifest);

        ShippingManifestResult shippingManifestResult = (ShippingManifestResult) dbShippingManifest;
        prepareShippingManifest(shippingManifestResult);

        invoiceActivityLogService.addInvoiceActivityLog(invoice.getId(), token.getActiveTopUser().getUserId(), String.format("Shipping manifest %s un-finalized.", shippingManifestResult.getShippingManifestNo()));
        return shippingManifestResult;
    }

    private void clearMetrcFields(ShippingManifest dbShippingManifest){
        dbShippingManifest.setMetrcManifestNumber("");
        dbShippingManifest.setMetrcSentTime(0L);
        dbShippingManifest.setMetrcTransferTemplateId("");
        dbShippingManifest.setMetrc(false);
        dbShippingManifest.setMetrcTransferType("");

        for(ProductMetrcInfo productMetrcInfo : dbShippingManifest.getProductMetrcInfo()) {
            for(ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                batchDetails.setReqMetrcLabel("");
                batchDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.NotCreated);
                batchDetails.setErrorMessage("");
                
                for(ShippingBatchDetails.MetrcPackageDetails packageDetails : batchDetails.getMetrcPackageDetails()) {
                    packageDetails.setReqMetrcLabel("");
                    packageDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.NotCreated);
                    packageDetails.setErrorMessage("");
                }
            }
        }
    }

    /**
     * Override method to reject shipping manifest from customer
     * @param shippingManifestId : shippingManifestId
     * @return
     */
    @Override
    public ShippingManifestResult rejectShippingManifest(final String shippingManifestId, final ManifestRejectRequest request) {
        if (request == null || StringUtils.isBlank(request.getDeclineReason())) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, DECLINE_REASON_NOT_EMPTY);
        }
        ShippingManifest dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.InProgress) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, MANIFEST_ALREADY_INPROGRESS);
        }

        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.Rejected) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, MANIFEST_ALREADY_REJECTED);
        }
        boolean isAllowed = checkEmployeePermission(token.getActiveTopUser().getUserId());

        if (!isAllowed) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(EMPLOYEE_DO_NOT_ALLOWED, "reject"));
        }

        if (isAssignedConnectedShipment(shippingManifestId)){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(CONNECTED_SHIPMENT_ASSIGNED, "rejected"));
        }else if (isAcceptedConnectedShipment(shippingManifestId)){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format(CONNECTED_SHIPMENT_ACCEPTED, "rejected"));
        }else{
            rejectConnectedShipment(shippingManifestId);
        }

        Invoice invoice = invoiceRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), dbShippingManifest.getInvoiceId());
        if (invoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }
        if (StringUtils.isNotBlank(request.getInventoryId())) {
            Inventory inventory = inventoryRepository.get(token.getCompanyId(), request.getInventoryId());
            if (inventory == null) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Requested inventory does not exist.");
            }
        }
        clearMetrcFields(dbShippingManifest);
        dbShippingManifest.setStatus(ShippingManifest.ShippingManifestStatus.Rejected);
        dbShippingManifest.setDeclineDate(DateTime.now().getMillis());
        dbShippingManifest.setRejectInventoryId(request.getInventoryId());
        dbShippingManifest.setDeclineReason(request.getDeclineReason());

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        //Process shipping manifest inventory
        invoiceInventoryService.processInvoiceForRevertManifest(shop, invoice, dbShippingManifest);

        invoice.setInvoiceStatus(Invoice.InvoiceStatus.IN_PROGRESS);
        invoice.setActive((invoice.getInvoicePaymentStatus() != Invoice.PaymentStatus.PAID));
        invoiceRepository.update(token.getCompanyId(), invoice.getId(), invoice);

        dbShippingManifest = shippingManifestRepository.update(token.getCompanyId(), shippingManifestId, dbShippingManifest);

        ShippingManifestResult shippingManifestResult = (ShippingManifestResult) dbShippingManifest;
        prepareShippingManifest(shippingManifestResult);

        invoiceActivityLogService.addInvoiceActivityLog(dbShippingManifest.getInvoiceId(), token.getActiveTopUser().getUserId(), String.format("Shipping manifest %s rejected..", shippingManifestResult.getShippingManifestNo()));
        return shippingManifestResult;
    }

    private String getBatchAttachmentLink(ProductBatch productBatch) {
        if (!CollectionUtils.isNullOrEmpty(productBatch.getAttachments())) {
            CompanyAsset asset = productBatch.getAttachments().stream().filter(b -> StringUtils.isNotBlank(b.getPublicURL())).findFirst().orElse(null);
            if (asset != null) {
                return asset.getPublicURL();
            }
        }
        return "";
    }

    private boolean checkEmployeePermission(String employeeId) {
        Employee employee = employeeRepository.getById(employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, "Employee doesn't found.");
        }

        Role role = roleRepository.get(token.getCompanyId(), employee.getRoleId());
        return !(role == null || CollectionUtils.isNullOrEmpty(role.getPermissions()) || !role.getPermissions().contains(Role.Permission.WebInvoiceManifestChange));
    }


    @Override
    public ShippingManifestResult sendToMetrc(String shippingManifestId) {
        ShippingManifestResult dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.InProgress) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, MANIFEST_ALREADY_INPROGRESS);
        }
        Invoice dbInvoice = invoiceRepository.getById(dbShippingManifest.getInvoiceId());
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }
        if (StringUtils.isBlank(dbShippingManifest.getMetrcTransferType())){
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, METRC_TRANSFER_TYPE_EMPTY);
        }
        prepareShippingManifest(dbShippingManifest);
        ShippingManifestTransferEvent event = new ShippingManifestTransferEvent();
        event.setShippingManifest(dbShippingManifest);
        event.setInvoice(dbInvoice);
        eventBus.post(event);
        return (ShippingManifestResult) event.getResponse();
    }

    @Override
    public ShippingManifestResult saveMetrcTemplate(String shippingManifestId, UpdateInvoiceShipmentRequest updateInvoiceShipmentRequest, boolean submitPackage) {
        ShippingManifestResult dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }
        if (dbShippingManifest.getStatus() == ShippingManifest.ShippingManifestStatus.InProgress) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, MANIFEST_ALREADY_INPROGRESS);
        }
        Invoice dbInvoice = invoiceRepository.getById(dbShippingManifest.getInvoiceId());
        if (dbInvoice == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, INVOICE_NOT_FOUND);
        }
        prepareShippingManifest(dbShippingManifest, true);

        Set<String> metrcReqLabel = new HashSet<>();
        Set<String> metrcSuccessLabel = new HashSet<>();
        Set<ObjectId> batchIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();

        HashMap<String, ShippingBatchDetails> metrcInfoHashMap = new HashMap<>();

        for (ProductMetrcInfo productMetrcInfo : dbShippingManifest.getProductMetrcInfo()) {
            if (StringUtils.isNotBlank(productMetrcInfo.getProductId()) && ObjectId.isValid(productMetrcInfo.getProductId())) {
                productIds.add(new ObjectId(productMetrcInfo.getProductId()));
            }
            for (ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                if (batchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                    metrcSuccessLabel.add(batchDetails.getReqMetrcLabel());
                }
                if (StringUtils.isNotBlank(batchDetails.getBatchId()) && ObjectId.isValid(batchDetails.getBatchId())) {
                    batchIds.add(new ObjectId(batchDetails.getBatchId()));
                }
                if (!CollectionUtils.isNullOrEmpty(batchDetails.getMetrcPackageDetails())) {
                    for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails : batchDetails.getMetrcPackageDetails()) {
                        if (StringUtils.isNotBlank(metrcPackageDetails.getBatchId()) && ObjectId.isValid(metrcPackageDetails.getBatchId())) {
                            batchIds.add(new ObjectId(metrcPackageDetails.getBatchId()));
                        }
                        if (metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                            metrcSuccessLabel.add(metrcPackageDetails.getReqMetrcLabel());
                        }
                    }
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(batchIds));
        boolean errorSameMetrcTag = false; //If the same metrc tag is used multiple times
        for (ProductMetrcInfo metrcInfo : updateInvoiceShipmentRequest.getProductMetrcInfos()) {
            Product product = productHashMap.get(metrcInfo.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_NOT_FOUND);
            }
            for (ShippingBatchDetails batchDetails : metrcInfo.getBatchDetails()) {
                List<String> keyList = Lists.newArrayList(metrcInfo.getOrderItemId(), batchDetails.getBatchId(), batchDetails.getOverrideInventoryId(), batchDetails.getPrepackageItemId());
                keyList.removeAll(Lists.newArrayList("", null));
                String key = StringUtils.join(keyList.toArray(), "_");
                metrcInfoHashMap.putIfAbsent(key, batchDetails);

                if (StringUtils.isNotBlank(batchDetails.getReqMetrcLabel()) && batchDetails.getMetrcStatus() != ShippingBatchDetails.MetrcStatus.Created && !batchDetails.getReqMetrcLabel().trim().equals(batchDetails.getMetrcLabel().trim())) {
                    if (metrcReqLabel.contains(batchDetails.getReqMetrcLabel().trim()) || metrcSuccessLabel.contains(batchDetails.getReqMetrcLabel().trim())) {
                        errorSameMetrcTag = true;
                    }
                    metrcReqLabel.add(batchDetails.getReqMetrcLabel());
                }

                if (!CollectionUtils.isNullOrEmpty(batchDetails.getMetrcPackageDetails()) && product.getProductType() == Product.ProductType.BUNDLE) {
                    for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails : batchDetails.getMetrcPackageDetails()) {
                        if (metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                            continue;
                        }
                        if (StringUtils.isNotBlank(metrcPackageDetails.getBatchId()) && ObjectId.isValid(metrcPackageDetails.getBatchId())) {
                            batchIds.add(new ObjectId(metrcPackageDetails.getBatchId()));
                        }
                        if (StringUtils.isNotBlank(metrcPackageDetails.getReqMetrcLabel()) && !metrcPackageDetails.getReqMetrcLabel().trim().equals(metrcPackageDetails.getMetrcLabel().trim())) {
                            if (metrcReqLabel.contains(metrcPackageDetails.getReqMetrcLabel().trim()) || metrcSuccessLabel.contains(metrcPackageDetails.getReqMetrcLabel().trim())) {
                                errorSameMetrcTag = true;
                            }
                            metrcReqLabel.add(metrcPackageDetails.getReqMetrcLabel().trim());
                        }
                    }
                }
            }
        }


        for (ProductMetrcInfo productMetrcInfo : dbShippingManifest.getProductMetrcInfo()) {
            Product product = productHashMap.get(productMetrcInfo.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, PRODUCT_NOT_FOUND);
            }
            for (ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                ProductBatch batch = batchHashMap.get(batchDetails.getBatchId());
                if (batch == null) {
                    throw new BlazeInvalidArgException(SHIPPING_MANIFEST, BATCH_NOT_FOUND);
                }
                if (batchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                    continue;
                }

                List<String> keyList = Lists.newArrayList(productMetrcInfo.getOrderItemId(), batchDetails.getBatchId(), batchDetails.getOverrideInventoryId(), batchDetails.getPrepackageItemId());
                keyList.removeAll(Lists.newArrayList("", null));
                String key = StringUtils.join(keyList.toArray(), "_");
                ShippingBatchDetails shippingBatchDetails = metrcInfoHashMap.get(key);

                if (shippingBatchDetails == null) {
                    continue;
                }
                if (errorSameMetrcTag) {
                    batchDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Error);
                    batchDetails.setErrorMessage(SAME_METRC_TAG);
                }

                if (StringUtils.isNotBlank(shippingBatchDetails.getReqMetrcLabel())) {
                    batchDetails.setReqMetrcLabel(shippingBatchDetails.getReqMetrcLabel().trim());
                }

                if(StringUtils.isNotBlank(shippingBatchDetails.getReqMetrcLabel()) && shippingBatchDetails.getReqMetrcLabel().trim().equals(shippingBatchDetails.getMetrcLabel().trim())){
                    batchDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Created);
                    batchDetails.setErrorMessage("");
                }

                if(StringUtils.isNotBlank(shippingBatchDetails.getReqMetrcLabel()) && shippingBatchDetails.getReqMetrcLabel().trim().equals(shippingBatchDetails.getMetrcLabel().trim())){
                    batchDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Created);
                    batchDetails.setErrorMessage("");
                }

                if (!CollectionUtils.isNullOrEmpty(shippingBatchDetails.getMetrcPackageDetails()) && product.getProductType() == Product.ProductType.BUNDLE) {
                    for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails : shippingBatchDetails.getMetrcPackageDetails()) {
                        if (metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                            continue;
                        }
                        if (errorSameMetrcTag) {
                            ShippingBatchDetails.MetrcPackageDetails details = batchDetails.getMetrcPackageDetails().stream().filter(metrcPackageDetails1 -> metrcPackageDetails1.getBatchId().equalsIgnoreCase(metrcPackageDetails.getBatchId())).findFirst().orElse(null);
                            details.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Error);
                            details.setErrorMessage(SAME_METRC_TAG);
                        }
                        if (StringUtils.isNotBlank(metrcPackageDetails.getReqMetrcLabel())) {
                            ShippingBatchDetails.MetrcPackageDetails details = batchDetails.getMetrcPackageDetails().stream().filter(metrcPackageDetails1 -> metrcPackageDetails1.getBatchId().equalsIgnoreCase(metrcPackageDetails.getBatchId())).findFirst().orElse(null);
                            if (details != null && details.getMetrcStatus() != ShippingBatchDetails.MetrcStatus.Created) {
                                details.setReqMetrcLabel(metrcPackageDetails.getReqMetrcLabel().trim());
                            }
                        }
                        if(StringUtils.isNotBlank(metrcPackageDetails.getReqMetrcLabel()) && metrcPackageDetails.getReqMetrcLabel().trim().equals(metrcPackageDetails.getMetrcLabel().trim())) {
                            ShippingBatchDetails.MetrcPackageDetails details = batchDetails.getMetrcPackageDetails().stream().filter(metrcPackageDetails1 -> metrcPackageDetails1.getBatchId().equalsIgnoreCase(metrcPackageDetails.getBatchId())).findFirst().orElse(null);
                            if (details != null && details.getMetrcStatus() != ShippingBatchDetails.MetrcStatus.Created) {
                                details.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Created);
                                details.setErrorMessage("");
                            }
                        }
                        if(StringUtils.isNotBlank(metrcPackageDetails.getReqMetrcLabel()) && metrcPackageDetails.getReqMetrcLabel().trim().equals(metrcPackageDetails.getMetrcLabel().trim())) {
                            ShippingBatchDetails.MetrcPackageDetails details = batchDetails.getMetrcPackageDetails().stream().filter(metrcPackageDetails1 -> metrcPackageDetails1.getBatchId().equalsIgnoreCase(metrcPackageDetails.getBatchId())).findFirst().orElse(null);
                            if (details != null && details.getMetrcStatus() != ShippingBatchDetails.MetrcStatus.Created) {
                                details.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Created);
                                details.setErrorMessage("");
                            }
                        }
                    }
                }
            }
        }

        if(!errorSameMetrcTag && !CollectionUtils.isNullOrEmpty(metrcReqLabel)) {
            MetrcVerifyEvent event = new MetrcVerifyEvent();
            event.setCompanyId(token.getCompanyId());
            event.setShopId(token.getShopId());
            event.setReqMetrcLabel(Lists.newArrayList(metrcReqLabel));
            event.setCreatePackage(submitPackage);
            event.setShippingManifest(dbShippingManifest);
            event.setRequestMetrcInfoKeys(metrcInfoHashMap.keySet());
            eventBus.post(event);

            List<String> metrcList = event.getResponse();

            if (!CollectionUtils.isNullOrEmpty(metrcList)) {
                throw new BlazeInvalidArgException(SHIPPING_MANIFEST, String.format("Metrc package label are already in use: %s", StringUtils.join(metrcList.toArray(), ",")));
            }
        }

        dbShippingManifest.setMetrcTransferType(updateInvoiceShipmentRequest.getTransferType());
        shippingManifestRepository.update(shippingManifestId, (ShippingManifest) dbShippingManifest);
        return dbShippingManifest;
    }

    public void sendShippingManifestToConnectedStore(final String shippingManifestId){
        ShippingManifestResult shippingManifestResult = getShippingManifestById(shippingManifestId);
        if (shippingManifestResult == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }

        Vendor receiverCustomerCompany = shippingManifestResult.getReceiverCustomerCompany();
        String connectedShopId = receiverCustomerCompany.getConnectedShop();
        Shop connectedShop = shopRepository.getById(connectedShopId);

        String vendorName = shippingManifestResult.getShipperCompany().getName();

        com.fourtwenty.core.domain.models.purchaseorder.ShipperInformation shipperInformation = new com.fourtwenty.core.domain.models.purchaseorder.ShipperInformation();
        shipperInformation.setVendorName(vendorName);

        List<ProductMetrcInfo> productMetrcInfoList = shippingManifestResult.getProductMetrcInfo();
        List<com.fourtwenty.core.domain.models.purchaseorder.ProductMetrcInfo> shipmentProductMetrcList = new ArrayList<>();
        if (productMetrcInfoList != null) {
            for (ProductMetrcInfo productMetrcInfo : productMetrcInfoList) {
                com.fourtwenty.core.domain.models.purchaseorder.ProductMetrcInfo productInfo = new com.fourtwenty.core.domain.models.purchaseorder.ProductMetrcInfo();

                Product product = productRepository.get(token.getCompanyId(), productMetrcInfo.getProductId());
                String productName = "";
                if(product !=null) {
                    productName = product.getName();
                }
                productInfo.setProductName(productName);
                productInfo.setQuantity(productMetrcInfo.getQuantity());

                List<com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails> batchDetails = new ArrayList<>();
                for(ShippingBatchDetails shippingBatchDetail : productMetrcInfo.getBatchDetails()){
                    ProductBatch productBatch = productBatchRepository.get(token.getCompanyId(), shippingBatchDetail.getBatchId());

                    com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails batchDetail = new com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails();
                    batchDetail.setBatchId(shippingBatchDetail.getBatchId());
                    batchDetail.setBatchPurchaseDate(shippingBatchDetail.getBatchPurchaseDate());
                    batchDetail.setBatchSku(shippingBatchDetail.getBatchSku());
                    batchDetail.setMetrcLabel(shippingBatchDetail.getMetrcLabel());
                    batchDetail.setPrepackageItemId(shippingBatchDetail.getPrepackageItemId());
                    batchDetail.setQuantity(shippingBatchDetail.getQuantity());
                    batchDetail.setTrackHarvestBatchId(shippingBatchDetail.getTrackHarvestBatchId());

                    List<com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails.MetrcPackageDetails>
                        metrcPackageDetails = new ArrayList<>();
                    if (!CollectionUtils.isNullOrEmpty(shippingBatchDetail.getMetrcPackageDetails())){
                        for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetail : shippingBatchDetail.getMetrcPackageDetails()){
                            com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails.MetrcPackageDetails packageDetail = new
                                com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails.MetrcPackageDetails();
                            packageDetail.setBatchId(metrcPackageDetail.getBatchId());
                            packageDetail.setBatchSku(metrcPackageDetail.getBatchSku());
                            packageDetail.setMetrcLabel(metrcPackageDetail.getMetrcLabel());
                            switch(metrcPackageDetail.getMetrcStatus()){
                                case Created:
                                    packageDetail.setMetrcStatus(com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails.MetrcStatus.Created);
                                    break;
                                case Error:
                                    packageDetail.setMetrcStatus(com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails.MetrcStatus.Error);
                                    break;
                                case NotCreated:
                                    packageDetail.setMetrcStatus(com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails.MetrcStatus.NotCreated);
                                    break;
                            }
                            //packageDetail.setMetrcStatus(MetrcPackageDetail.getMetrcStatus());
                            packageDetail.setProductId(metrcPackageDetail.getProductId());
                            packageDetail.setProductName(metrcPackageDetail.getProductName());
                            packageDetail.setQuantity(metrcPackageDetail.getQuantity());
                            packageDetail.setReqMetrcLabel(metrcPackageDetail.getReqMetrcLabel());
                            metrcPackageDetails.add(packageDetail);
                        }
                    }
                    batchDetail.setMetrcPackageDetails(metrcPackageDetails);

                    /* batchDetail.setTrackHarvestBatch(productBatch.getTrackHarvestBatch());
                    batchDetail.setBatchExpirationDate(productBatch.getExpirationDate());
                    batchDetail.setBatchHarvestDate(productBatch.getTrackHarvestDate());
                    batchDetail.setBatchSellByDate(productBatch.getSellBy());
                    batchDetail.setCostPerUnit(productBatch.getCostPerUnit()); */

                    batchDetails.add(batchDetail);
                }
                productInfo.setBatchDetails(batchDetails);

                shipmentProductMetrcList.add(productInfo);
            }
        }

        Shipment shipment = new Shipment();
        shipment.setCompanyId(connectedShop.getCompanyId());
        shipment.setShopId(connectedShopId);
        shipment.setShippingManifestId(shippingManifestResult.getId());
        shipment.setShippingManifestNo(shippingManifestResult.getShippingManifestNo());
        shipment.setShippedDateTime(DateUtil.nowUTC().getMillis());
        shipment.setDeliveryDate(shippingManifestResult.getDeliveryDate());
        shipment.setDeliveryTime(shippingManifestResult.getDeliveryTime());
        shipment.setEstimatedArrival(shippingManifestResult.getEstimatedArrival());
        shipment.setProductMetrcInfo(shipmentProductMetrcList);
        shipment.setShipperInformation(shipperInformation);
        shipmentService.addShipment(shipment);

        //Update sent to connected shop flag of shipping manifest
        ShippingManifest dbShippingManifest = shippingManifestRepository.get(token.getCompanyId(), shippingManifestId, ShippingManifestResult.class);
        if (dbShippingManifest == null) {
            throw new BlazeInvalidArgException(SHIPPING_MANIFEST, SHIPPING_MANIFEST_NOT_FOUND);
        }

        dbShippingManifest.setSentToConnectedShop(true);
        shippingManifestRepository.update(shippingManifestId, dbShippingManifest);
    }

    @Override
    public ShippingManifestResult getShippingManifest(String shippingManifestId) {
        ShippingManifestResult result = shippingManifestRepository.getShippingManifest(shippingManifestId);
    prepareShippingManifest(result,true);
        return result;
    }

    @Override
    public Boolean isAcceptedConnectedShipment(String shippingManifestId){
        Shipment shipment = shipmentRepository.getShipmentByShippingManifestId(shippingManifestId);
        if (shipment!=null){
            return shipment.getShipmentStatus()==Shipment.ShipmentStatus.Accepted;
        }
        return false;
    }

    @Override
    public Boolean isAssignedConnectedShipment(String shippingManifestId){
        Shipment shipment = shipmentRepository.getShipmentByShippingManifestId(shippingManifestId);
        if (shipment!=null){
            return shipment.isAssigned();
        }
        return false;
    }

    private void rejectConnectedShipment(String shippingManifestId) {
        Shipment shipment = shipmentRepository.getShipmentByShippingManifestId(shippingManifestId);
        if (shipment!=null){
            shipmentService.rejectShipment(shipment.getId());
        }
    }

}