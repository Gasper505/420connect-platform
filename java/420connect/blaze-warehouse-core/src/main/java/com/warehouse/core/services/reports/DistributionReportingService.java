package com.warehouse.core.services.reports;

import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.FormatProcessor;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.google.inject.Provider;
import com.warehouse.core.services.reports.gatherer.DistributionGathererManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class DistributionReportingService extends AbstractAuthServiceImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(DistributionReportingService.class);

    @Inject
    private DistributionGathererManager gathererManager;
    @Inject
    ShopRepository shopRepository;

    @Inject
    FormatProcessor formatProcessor;

    @Inject
    public DistributionReportingService(Provider<ConnectAuthToken> token) {
        super(token);
    }

    public Report getReport(String typeString, String startDate, String endDate, String filterString, String formatString, ReportTrack.ReportSectionType section) {
        //Get the Enum type for this specific report
        if (typeString == null) {
            throw new BlazeInvalidArgException("ReportService", "Argument Not Found - Report Type");
        }

        ReportType type = ReportType.getType(typeString);
        if (type == ReportType.DEFAULT) {
            throw new BlazeInvalidArgException("ReportService", "Invalid Argument - Report Type");
        }
        FormatProcessor.ReportFormat format = FormatProcessor.ReportFormat.getType(formatString);
        if (format == FormatProcessor.ReportFormat.DEFAULT) {
            throw new BlazeInvalidArgException("ReportService", "Invalid Argument - Report Format");
        }

        Shop shop = shopRepository.get(token.getCompanyId(),token.getShopId());
        //Build the filter object to be used to sort the data we want
        ReportFilter filter = new ReportFilter(shop,type, token, filterString, startDate, endDate);

        Gatherer gatherer = gathererManager.getGathererForReportType(type);
        if (gatherer == null) {
            throw new BlazeInvalidArgException("ReportService", "Gatherer not available.");
        }
        GathererReport report = gatherer.gather(filter);
        if (report == null) {
            throw new BlazeInvalidArgException("Report", "Report not available.");
        }

        return formatProcessor.process(format, report);
    }
}
