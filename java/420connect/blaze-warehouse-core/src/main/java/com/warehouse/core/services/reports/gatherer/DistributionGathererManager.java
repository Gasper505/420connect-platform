package com.warehouse.core.services.reports.gatherer;

import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.GathererManager;
import com.fourtwenty.core.reporting.gather.impl.inventory.InventoryDistributionReport;
import com.warehouse.core.reporting.*;

public class DistributionGathererManager extends GathererManager {

    public DistributionGathererManager() {
    }

    @Override
    public Gatherer getGathererForReportType(ReportType type) {
        Gatherer g = null;

        switch (type) {
            case DISTRIBUTION_OUTSTANDING_INVOICE_REPORT_BY_CUSTOMER_COMPANY_ID:
                g = injector.getInstance(OutStandingInvoiceGatherer.class);
                break;
            case DISTRIBUTION_OUTSTANDING_PURCHASE_ORDER_REPORT_BY_CUSTOMER_TYPE:
                g = injector.getInstance(OutStandingPurchaseOrderGatherer.class);
                break;
            case DISTRIBUTION_BEST_SELLING_BRAND_REPORT:
                g = injector.getInstance(BestSellingBrandsGather.class);
                break;
            case DISTRIBUTION_NEW_CUSTOMER_BY_MONTH_REPORT:
                g = injector.getInstance(NewCustomerByMonthGather.class);
                break;
            case DISTRIBUTION_SALES_BY_PRODUCT_CATEGORY_FOR_INVOICE_REPORT:
                g = injector.getInstance(SalesByProductCategoryForInvoiceGatherer.class);
                break;
            case DISTRIBUTION_SALES_REVENUE_BY_MONTH_GATHERER_REPORT:
                g = injector.getInstance(SalesRevenueByMonthGatherer.class);
                break;
            case DISTRIBUTION_TAX_BY_TYPE_REPORT:
                g = injector.getInstance(TaxByTypeGatherer.class);
                break;
            case DISTRIBUTION_CUSTOMER_BREAKDOWN_REPORT:
                g = injector.getInstance(CustomerBreakDownGatherer.class);
                break;
            case DISTRIBUTION_SALES_EXPENSES:
                g = injector.getInstance(SalesExpenseReportGatherer.class);
                break;
            case DISTRIBUTION_SALES_BY_SALES_PERSON_REPORT:
                g = injector.getInstance(SalesBySalesPersonGatherer.class);
                break;
            case DISTRIBUTION_EXPENSE_BY_CATEGORY_REPORT:
                g = injector.getInstance(ExpenseByCategoryGatherer.class);
                break;
            case DISTRIBUTION_EXPENSE_BY_CUSTOMER_REPORT:
                g = injector.getInstance(ExpenseByCustomerGatherer.class);
                break;
            case DISTRIBUTION_SALES_BY_CUSTOMER:
                g = injector.getInstance(SalesByCustomerGatherer.class);
                break;
            case DISTRIBUTION_SALES_BY_PRODUCT:
                g = injector.getInstance(SalesByProductGatherer.class);
                break;
            case DISTRIBUTION_EXPENSES_DETAILS:
                g = injector.getInstance(ExpensesDetailsGatherer.class);
                break;
            case DISTRIBUTION_CUSTOMER_BALANCE_REPORT:
                g = injector.getInstance(CustomerBalanceGatherer.class);
                break;
            case DISTRIBUTION_INVENTORY_DISTRIBUTION_REPORT:
                g = injector.getInstance(InventoryDistributionGatherer.class);
                break;
            case DISTRIBUTION_INVOICE_DETAIL_REPORT:
                g = injector.getInstance(InvoiceDetailGatherer.class);
                break;
            case PAYMENT_RECEIVED_REPORT:
                g = injector.getInstance(PaymentReceivedGatherer.class);
                break;
            case AGING_DETAILS_REPORT:
                g = injector.getInstance(AgingDetailsGatherer.class);
                break;
            case TIME_TO_GET_PAID_REPORT:
                g = injector.getInstance(TimeToGetPaidGatherer.class);
                break;
            case AGING_SUMMARY_REPORT:
                g = injector.getInstance(AgingSummaryGatherer.class);
                break;
            case TOTAL_INVENTORY_AVAILABLE:
                g = injector.getInstance(TotalAvailableInventory.class);
                break;
            case INVENTORY_DISTRIBUTION:
                g = injector.getInstance(InventoryDistributionReport.class);
                break;
            case DISTRIBUTION_PURCHASE_ORDER_REPORT:
                g = injector.getInstance(DistributionPurchaseOrderGatherer.class);
                break;
            //Admin reports
            case DISTRIBUTION_TIME_CLOCK_REPORT:
                g = injector.getInstance(DistributionTimeClockGatherer.class);
                break;
            case ALL_INVOICES_REPORT:
                g = injector.getInstance(InvoiceGatherer.class);
                break;
            case ALL_INVOICE_DETAIL_REPORT:
                g = injector.getInstance(InvoiceProductGatherer.class);
                break;
            case SALES_COGS:
                g = injector.getInstance(SalesCogsGatherer.class);
                break;
            case INVENTORY_VALUE:
                g = injector.getInstance(InventoryValueGatherer.class);
                break;
            case TOTAL_INVENTORY_AVAILABLE_CATEGORY:
                g = injector.getInstance(TotalAvailableInventoryByCategory.class);
                break;
            case ACCOUNT_RECEIVABLE:
                g = injector.getInstance(AccountsReceivableGatherer.class);
                break;
            case INVOICE_MUNICIPAL:
                g = injector.getInstance(InvoiceMunicipalGatherer.class);
                break;
            case PURCHASE_ORDER_MUNICIPAL:
                g = injector.getInstance(PurchaseOrderMunicipalGatherer.class);
                break;
            case INVOICE_BALANCE:
                g = injector.getInstance(InvoiceBalanceGatherer.class);
                break;
            case PO_TIME_TO_GET_PAID:
                g = injector.getInstance(POTimeToGetPaidGatherer.class);
                break;
            case BEST_SELLING_PRODUCT_REPORT:
                g = injector.getInstance(BestSellingProductGatherer.class);
                break;
            default:
                g = super.getGathererForReportType(type);
                break;
        }

        return g;

    }
}
