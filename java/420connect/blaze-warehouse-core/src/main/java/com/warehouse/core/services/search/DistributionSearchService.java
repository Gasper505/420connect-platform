package com.warehouse.core.services.search;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.invoice.Invoice;

public interface DistributionSearchService {
    SearchResult<Expenses> searchExpenses(String term, int start, int limit, Boolean active, Boolean archive);

    SearchResult<Invoice> searchInvoices(String term, int start, int limit, Boolean active);
}
