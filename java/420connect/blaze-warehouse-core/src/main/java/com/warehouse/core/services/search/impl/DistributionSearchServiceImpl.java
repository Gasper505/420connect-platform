package com.warehouse.core.services.search.impl;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.thirdparty.elasticsearch.models.request.SearchRequestBuilder;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponse;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchCommunicatorService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchIndexingService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.services.search.DistributionSearchService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class DistributionSearchServiceImpl extends AbstractAuthServiceImpl implements DistributionSearchService {

    private ElasticSearchIndexingService elasticSearchIndexingService;
    private ElasticSearchCommunicatorService communicatorService;
    private ExpensesRepository expensesRepository;
    private InvoiceRepository invoiceRepository;

    @Inject
    public DistributionSearchServiceImpl(Provider<ConnectAuthToken> token, ElasticSearchIndexingService elasticSearchIndexingService, ElasticSearchCommunicatorService communicatorService, ExpensesRepository expensesRepository, InvoiceRepository invoiceRepository) {
        super(token);
        this.elasticSearchIndexingService = elasticSearchIndexingService;
        this.communicatorService = communicatorService;
        this.expensesRepository = expensesRepository;
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public SearchResult<Expenses> searchExpenses(String term, int start, int limit, Boolean active, Boolean archive) {
        final List<String> indexAndType = elasticSearchIndexingService.getIndexAndType(Expenses.class);

        final SearchResult<Expenses> searchResult = new SearchResult<>();
        searchResult.setLimit(limit);
        searchResult.setSkip(start);

        SearchRequestBuilder searchRequestBuilder = null;
        if (active != null || archive != null) {
            searchRequestBuilder = SearchRequestBuilder.get();

            if (active != null) {
                searchRequestBuilder.add("active", active, SearchRequestBuilder.SearchOperator.AND);
            }

            if (archive != null) {
                searchRequestBuilder.add("archive", archive, SearchRequestBuilder.SearchOperator.AND);
            }
        }

        final AWSSearchResponse awsSearchResponse = communicatorService.searchIndexedDocument(indexAndType.get(0), indexAndType.get(1), term, searchRequestBuilder, start, limit);
        if (awsSearchResponse.getDocumentIds().isEmpty()) {
            searchResult.setTotal(0L);
            searchResult.setValues(Lists.newArrayList());
            return searchResult;
        }

        final ArrayList<Expenses> expenses = Lists.newArrayList(expensesRepository.findItemsIn(token.getCompanyId(), token.getShopId(), awsSearchResponse.getDocumentIds()));
        searchResult.setValues(expenses);

        // TODO: Fix
//        final AWSSearchResponse countAwsSearchResponse = communicatorService.searchIndexedDocument(indexAndType.get(0), indexAndType.get(1), term);
//        searchResult.setTotal(expensesRepository.countItemsIn(token.getCompanyId(), token.getShopId(), countAwsSearchResponse.getDocumentIds()));

        return searchResult;
    }

    @Override
    public SearchResult<Invoice> searchInvoices(String term, int start, int limit, Boolean active) {
        final List<String> indexAndType = elasticSearchIndexingService.getIndexAndType(Invoice.class);

        final SearchResult<Invoice> searchResult = new SearchResult<>();
        searchResult.setLimit(limit);
        searchResult.setSkip(start);

        SearchRequestBuilder searchRequestBuilder = null;
        if (active != null) {
            searchRequestBuilder = SearchRequestBuilder.get();
            searchRequestBuilder.add("active", String.valueOf(active), SearchRequestBuilder.SearchOperator.AND);
        }

        final AWSSearchResponse awsSearchResponse = communicatorService.searchIndexedDocument(indexAndType.get(0), indexAndType.get(1), term, searchRequestBuilder, start, limit);
        if (awsSearchResponse.getDocumentIds().isEmpty()) {
            searchResult.setTotal(0L);
            searchResult.setValues(Lists.newArrayList());
            return searchResult;
        }

        final ArrayList<Invoice> invoices = Lists.newArrayList(invoiceRepository.findItemsIn(token.getCompanyId(), token.getShopId(), awsSearchResponse.getDocumentIds()));
        searchResult.setValues(invoices);

        // TODO: Fix
//        final AWSSearchResponse countAwsSearchResponse = communicatorService.searchIndexedDocument(indexAndType.get(0), indexAndType.get(1), term);
//        searchResult.setTotal(invoiceRepository.countItemsIn(token.getCompanyId(), token.getShopId(), countAwsSearchResponse.getDocumentIds()));

        return searchResult;
    }
}
