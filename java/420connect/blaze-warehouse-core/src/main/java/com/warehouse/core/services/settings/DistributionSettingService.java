package com.warehouse.core.services.settings;

public interface DistributionSettingService {
    void deleteAdjustment(String adjustmentId);
}
