package com.warehouse.core.services.settings.impl;

import com.fourtwenty.core.domain.models.company.Adjustment;
import com.fourtwenty.core.domain.repositories.dispensary.AdjustmentRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.services.settings.DistributionSettingService;


public class DistributionSettingServiceImpl extends AbstractAuthServiceImpl implements DistributionSettingService {

    private static final String ADJUSTMENT = "Adjustment";
    private static final String ADJUSTMENT_NOT_EXIST = "Adjustment is not exist";

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private AdjustmentRepository adjustmentRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;

    @Inject
    public DistributionSettingServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public void deleteAdjustment(String adjustmentId) {
        Adjustment dbAdjustment = adjustmentRepository.get(token.getCompanyId(), adjustmentId);
        if (dbAdjustment == null) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ADJUSTMENT_NOT_EXIST);
        }

        long invoiceCount = invoiceRepository.countInvoicesByAdjustmentId(token.getCompanyId(), token.getShopId(), adjustmentId);
        long poCount = purchaseOrderRepository.countPOByAdjustmentId(token.getCompanyId(), token.getShopId(), adjustmentId);

        if (invoiceCount > 0 || poCount > 0) {
            throw new BlazeInvalidArgException(ADJUSTMENT, "Adjustment can't be deleted as it's currently being used in invoices and(or) purchase orders.");
        }

        adjustmentRepository.removeById(token.getCompanyId(), adjustmentId);
    }
}
