package com.warehouse.core.services.store;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.partner.invoices.PartnerInvoiceAddRequest;
import com.warehouse.core.rest.partner.invoices.PartnerInvoiceUpdateRequest;

public interface PartnerInvoiceService {

    InvoiceResult getInvoiceById(String invoiceId);

    Invoice createInvoice(PartnerInvoiceAddRequest request);

    Invoice prepareInvoice(PartnerInvoiceAddRequest request);

    Invoice updateInvoice(String invoiceId, PartnerInvoiceUpdateRequest request);

    SearchResult<InvoiceResult> getInvoicesByDates(String startDate, String endDate, int start, int limit);
}
