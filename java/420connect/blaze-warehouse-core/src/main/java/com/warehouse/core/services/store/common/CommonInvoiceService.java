package com.warehouse.core.services.store.common;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.rest.invoice.request.AddInvoiceRequest;
import com.warehouse.core.rest.invoice.result.InvoiceResult;

public interface CommonInvoiceService {
    InvoiceResult getInvoiceById(String companyId, String shopId, String invoiceId);

    Invoice createInvoice(String companyId, String shopId, String currentEmployeeId, AddInvoiceRequest request);

    Invoice prepareInvoice(String companyId, String shopId, String currentEmployeeId, AddInvoiceRequest request);

    Invoice updateInvoice(String companyId, String shopId, String currentEmployeeId, String invoiceId, Invoice invoice);

    SearchResult<InvoiceResult> getInvoicesByDates(String companyId, String shopId, String startDate, String endDate, int start, int limit);
}
