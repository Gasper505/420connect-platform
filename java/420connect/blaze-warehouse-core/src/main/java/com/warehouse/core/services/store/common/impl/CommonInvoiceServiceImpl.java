package com.warehouse.core.services.store.common.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.QrCodeUtil;
import com.warehouse.core.domain.models.invoice.*;
import com.warehouse.core.domain.repositories.invoice.*;
import com.warehouse.core.rest.invoice.request.AddInvoiceRequest;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import com.warehouse.core.services.invoice.InvoiceInventoryService;
import com.warehouse.core.services.store.common.CommonInvoiceService;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

public class CommonInvoiceServiceImpl implements CommonInvoiceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonInvoiceServiceImpl.class);
    private static final String INVOICE_NOT_FOUND = "Error! Invoice does not exist.";
    private static final String QR_CODE_ERROR = "Error while creating QR code";
    private static final String INVOICE = "Invoice";
    private static final String CUSTOMER_COMPANY = "Customer company";
    private static final String CART = "Cart";
    private static final String NOT_CUSTOM_DATE = "Please provide custom date.";
    private static final String INVOICE_NOT_ALLOWED_FOR_VENDOR = "Invoice not allowed for Vendors";
    private static final String ACTIVITY_LOG = "Invoice Activity Log";

    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private InvoiceHistoryRepository invoiceHistoryRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private ShippingManifestRepository shippingManifestRepository;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private InvoiceInventoryService invoiceInventoryService;
    @Inject
    private CompanyContactRepository companyContactRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;
    @Inject
    private InvoiceActivityLogRepository invoiceActivityLogRepository;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;

    /**
     * Override method to prepare invoice
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param currentEmployeeId : currentEmployeeId
     * @param invoiceRequest    : invoice request
     * @return : invoice
     */
    @Override
    public Invoice prepareInvoice(String companyId, String shopId, String currentEmployeeId, AddInvoiceRequest invoiceRequest) {

        Employee currentEmployee = employeeRepository.getById(currentEmployeeId);
        if (currentEmployee == null) {
            throw new BlazeInvalidArgException(INVOICE, "Logged In Employee not found.");
        }

        if (StringUtils.isBlank(invoiceRequest.getCustomerId())) {
            throw new BlazeInvalidArgException(INVOICE, "Customer can't be blank.");
        }
        Vendor customerCompany = vendorRepository.get(companyId, invoiceRequest.getCustomerId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "Please choose a valid customer");
        }
        if (Vendor.VendorType.VENDOR.equals(customerCompany.getVendorType())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, INVOICE_NOT_ALLOWED_FOR_VENDOR);
        }
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        if (invoiceRequest.getCart() == null) {
            throw new BlazeInvalidArgException(CART, "Cart can't be empty");
        }
        if (invoiceRequest.getCart().getItems() == null && invoiceRequest.getCart().getItems().isEmpty()) {
            throw new BlazeInvalidArgException(CART, "Products can't be empty in Cart");
        }
        if (Invoice.InvoiceStatus.COMPLETED.equals(invoiceRequest.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice status can't be completed, until Shipping Manifest is not done");
        }
        if (invoiceRequest.getInvoiceTerms() == null) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice term cannot be blank");
        }
        if (Invoice.InvoiceTerms.CUSTOM_DATE.equals(invoiceRequest.getInvoiceTerms()) && (invoiceRequest.getDueDate() == null || invoiceRequest.getDueDate() == 0)) {
            throw new BlazeInvalidArgException(INVOICE, NOT_CUSTOM_DATE);
        }

        CompanyLicense dbCompanyLicense = customerCompany.getCompanyLicense(invoiceRequest.getLicenseId());
        if (dbCompanyLicense == null) {
            throw new BlazeInvalidArgException(INVOICE, "Company license not found");
        }

        Invoice invoice = new Invoice();
        invoice.setShopId(shopId);
        invoice.setCompanyId(companyId);
        invoice.setCustomerId(invoiceRequest.getCustomerId());
        invoice.setLicenseId(invoiceRequest.getLicenseId());
        invoice.setOrderNumber(invoiceRequest.getOrderNumber());
        invoice.setInvoiceTerms(invoiceRequest.getInvoiceTerms());
        invoice.setSalesPersonId(invoiceRequest.getSalesPersonId());
        invoice.setActive(true);
        if (invoiceRequest.getAttachments() != null && !invoiceRequest.getAttachments().isEmpty()) {
            List<CompanyAsset> companyAssetList = invoiceRequest.getAttachments();
            List<CompanyAsset> newCompanyAssetList = new ArrayList<>();
            for (CompanyAsset companyAsset : companyAssetList) {
                companyAsset.prepare(companyId);
                newCompanyAssetList.add(companyAsset);
            }
            invoice.setAttachments(newCompanyAssetList);
        }

        invoice.setDueDate(invoiceRequest.getDueDate());
        invoice.setInvoiceDate(invoiceRequest.getInvoiceDate());
        updateInvoiceDueDate(invoice);

        invoice.setTermsAndconditions(invoiceRequest.getTermsAndconditions());
        if (invoiceRequest.getNotes() != null && !invoiceRequest.getNotes().isEmpty()) {
            String employeeName = currentEmployee.getFirstName() + " " + currentEmployee.getLastName();
            for (Note note : invoiceRequest.getNotes()) {
                note.prepare();
                note.setWriterId(currentEmployeeId);
                note.setWriterName(employeeName);

            }
            invoice.setNotes(invoiceRequest.getNotes());
        }
        invoice.setCompanyContactId(invoiceRequest.getCompanyContactId());

        invoice.setEstimatedDeliveryDate(invoiceRequest.getEstimatedDeliveryDate());
        invoice.setEstimatedDeliveryTime(invoiceRequest.getEstimatedDeliveryTime());
        invoice.setDeliveryCharges(invoiceRequest.getDeliveryCharges());
        invoice.setTotal(invoiceRequest.getTotal());
        invoice.setCart(invoiceRequest.getCart());
        invoice.setMarkup(invoiceRequest.getMarkup());
        invoice.setExciseTax(invoiceRequest.getExciseTax());
        invoice.setTotalInvoiceAmount(invoiceRequest.getTotalInvoiceAmount());
        if (Invoice.InvoiceStatus.SENT.equals(invoiceRequest.getInvoiceStatus())) {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.SENT);
        } else if (Invoice.InvoiceStatus.DRAFT.equals(invoiceRequest.getInvoiceStatus())) {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.DRAFT);
        } else {
            invoice.setInvoiceStatus(Invoice.InvoiceStatus.IN_PROGRESS);
        }
        invoice.setInvoicePaymentStatus(Invoice.PaymentStatus.UNPAID);

        if (StringUtils.isBlank(invoiceRequest.getInvoiceNumber())) {
            long ordinal = getNextSequence(companyId);
            String invoiceNo = NumberUtils.uniqueNumber("INV", ordinal);
            invoice.setInvoiceNumber(invoiceNo);
        } else {
            invoice.setInvoiceNumber(invoiceRequest.getInvoiceNumber());
        }

        invoice.setInventoryProcessed(invoiceRequest.isInventoryProcessed());
        invoice.setInventoryProcessedDate(invoiceRequest.getInventoryProcessedDate());
        invoice.setTransactionType(invoiceRequest.getTransactionType());
        if (customerCompany.getArmsLengthType() != null) {
            invoice.setTransactionType(customerCompany.getArmsLengthType());
        }
        invoiceInventoryService.calculateTaxes(shop, invoice);


        return invoice;
    }

    /**
     * Override method to create invoice
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param currentEmployeeId : currentEmployeeId
     * @param request           : request
     * @return: invoice
     */
    @Override
    public Invoice createInvoice(String companyId, String shopId, String currentEmployeeId, AddInvoiceRequest request) {
        Invoice invoice = this.prepareInvoice(companyId, shopId, currentEmployeeId, request);

        if (invoice != null) {
            invoice.prepare(companyId);
            invoice.setShopId(shopId);
            if (StringUtils.isBlank(invoice.getSalesPersonId())) {
                invoice.setSalesPersonId(currentEmployeeId);
            }
            Invoice dbInvoice = invoiceRepository.save(invoice);

            if (dbInvoice != null) {
                addInvoiceHistory(companyId, shopId, dbInvoice.getId(), currentEmployeeId);
                addInvoiceActivityLog(companyId, shopId, dbInvoice.getId(), currentEmployeeId, dbInvoice.getInvoiceNumber() + " invoice created");
                UploadFileResult result;
                File file;
                try {
                    InputStream inputStream = QrCodeUtil.getQrCode(dbInvoice.getInvoiceNumber());
                    file = QrCodeUtil.stream2file(inputStream, ".png");
                    String keyName = dbInvoice.getId() + "-" + dbInvoice.getInvoiceNumber().replaceAll(" ", "");
                    result = amazonS3Service.uploadFilePublic(file, keyName, ".png");
                    if (Objects.nonNull(result) && StringUtils.isNotBlank(result.getUrl())) {
                        CompanyAsset asset = new CompanyAsset();
                        asset.prepare(companyId);
                        asset.setName("Invoice");
                        asset.setKey(result.getKey());
                        asset.setActive(true);
                        asset.setType(Asset.AssetType.Photo);
                        asset.setPublicURL(result.getUrl());
                        asset.setSecured(false);
                        companyAssetRepository.save(asset);
                        dbInvoice.setInvoiceQrCodeAsset(asset);
                        dbInvoice.setInvoiceQrCodeUrl(result.getUrl());
                        invoiceRepository.update(dbInvoice.getId(), dbInvoice);
                    }
                } catch (IOException ex) {
                    LOGGER.error(QR_CODE_ERROR);
                }
            }

            return dbInvoice;
        }
        return invoice;

    }


    /**
     * Override method to update invoice
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param currentEmployeeId : currentEmployeeId
     * @param invoiceId         : invoice id
     * @param invoice           : request
     * @return: invoice
     */
    @Override
    public Invoice updateInvoice(String companyId, String shopId, String currentEmployeeId, String invoiceId, Invoice invoice) {
        Invoice dbInvoice = invoiceRepository.get(companyId, invoiceId);
        if (dbInvoice == null) {
            LOGGER.debug("Exception in Invoice", INVOICE_NOT_FOUND);
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }

        Employee currentEmployee = employeeRepository.getById(currentEmployeeId);
        if (currentEmployee == null) {
            throw new BlazeInvalidArgException(INVOICE, "Logged In Employee not found.");
        }

        if (StringUtils.isBlank(invoice.getCustomerId())) {
            throw new BlazeInvalidArgException(INVOICE, "Customer can't be blank.");
        }
        Vendor customerCompany = vendorRepository.get(companyId, invoice.getCustomerId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "Please choose a valid customer");
        }
        if (Vendor.VendorType.VENDOR.equals(customerCompany.getVendorType())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, INVOICE_NOT_ALLOWED_FOR_VENDOR);
        }
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        if (invoice.getCart() == null) {
            throw new BlazeInvalidArgException(CART, "Cart can't be empty");
        }
        if (invoice.getCart().getItems() == null && invoice.getCart().getItems().isEmpty()) {
            throw new BlazeInvalidArgException(CART, "Products can't be empty in Cart");
        }
        if (Invoice.InvoiceStatus.COMPLETED.equals(invoice.getInvoiceStatus())) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice status can't be completed, until Shipping Manifest is not done");
        }
        if (invoice.getInvoiceTerms() == null) {
            throw new BlazeInvalidArgException(INVOICE, "Invoice term cannot be blank");
        }
        if (Invoice.InvoiceTerms.CUSTOM_DATE.equals(invoice.getInvoiceTerms()) && (invoice.getDueDate() == null || invoice.getDueDate() == 0)) {
            throw new BlazeInvalidArgException(INVOICE, NOT_CUSTOM_DATE);
        }

        CompanyLicense dbCompanyLicense = customerCompany.getCompanyLicense(invoice.getLicenseId());

        if (dbCompanyLicense == null) {
            throw new BlazeInvalidArgException(INVOICE, "Company license not found");
        }

        dbInvoice.setCustomerId(invoice.getCustomerId());
        dbInvoice.setLicenseId(invoice.getLicenseId());
        dbInvoice.setOrderNumber(invoice.getOrderNumber());
        dbInvoice.setInvoiceTerms(invoice.getInvoiceTerms());
        dbInvoice.setSalesPersonId(invoice.getSalesPersonId());
        if (StringUtils.isBlank(invoice.getSalesPersonId())) {
            dbInvoice.setSalesPersonId(currentEmployeeId);
        }
        dbInvoice.setActive(true);
        if (invoice.getAttachments() != null && !invoice.getAttachments().isEmpty()) {
            List<CompanyAsset> companyAssetList = invoice.getAttachments();
            List<CompanyAsset> newCompanyAssetList = new ArrayList<>();
            for (CompanyAsset companyAsset : companyAssetList) {
                companyAsset.prepare(companyId);
                newCompanyAssetList.add(companyAsset);
            }
            dbInvoice.setAttachments(newCompanyAssetList);
        } else {
            dbInvoice.setAttachments(new ArrayList<>());
        }

        dbInvoice.setDueDate(invoice.getDueDate());
        dbInvoice.setInvoiceDate(invoice.getInvoiceDate());
        updateInvoiceDueDate(dbInvoice);

        dbInvoice.setTermsAndconditions(invoice.getTermsAndconditions());
        dbInvoice.setNotes(invoice.getNotes());

        dbInvoice.setCompanyContactId(invoice.getCompanyContactId());

        dbInvoice.setEstimatedDeliveryDate(invoice.getEstimatedDeliveryDate());
        dbInvoice.setEstimatedDeliveryTime(invoice.getEstimatedDeliveryTime());
        dbInvoice.setDeliveryCharges(invoice.getDeliveryCharges());
        dbInvoice.setMarkup(invoice.getMarkup());
        dbInvoice.setTotalInvoiceAmount(invoice.getTotalInvoiceAmount());
        if (Invoice.InvoiceStatus.DRAFT == invoice.getInvoiceStatus()) {
            dbInvoice.setInvoiceStatus(Invoice.InvoiceStatus.DRAFT);
        } else if (Invoice.InvoiceStatus.SENT == invoice.getInvoiceStatus()) {
            dbInvoice.setInvoiceStatus(Invoice.InvoiceStatus.SENT);
        } else {
            dbInvoice.setInvoiceStatus(Invoice.InvoiceStatus.IN_PROGRESS);
        }
        dbInvoice.setInvoicePaymentStatus(invoice.getInvoicePaymentStatus());
        dbInvoice.setInventoryProcessed(invoice.isInventoryProcessed());
        dbInvoice.setInventoryProcessedDate(invoice.getInventoryProcessedDate());
        dbInvoice.setTransactionType(invoice.getTransactionType());
        if (customerCompany.getArmsLengthType() != null) {
            dbInvoice.setTransactionType(customerCompany.getArmsLengthType());
        }
        Cart oldCart = dbInvoice.getCart();
        dbInvoice.setCart(invoice.getCart());
        updateOrderItems(companyId, dbInvoice.getCart());
        updateNotes(currentEmployee, dbInvoice);
        invoiceInventoryService.calculateTaxes(shop, dbInvoice);

        prepareCartForItems(companyId, oldCart, dbInvoice.getCart(), dbInvoice.getId());
        Invoice updatedInvoice = invoiceRepository.update(invoiceId, dbInvoice);

        try {
            addInvoiceHistory(companyId, shopId, dbInvoice.getId(), currentEmployeeId);
            addInvoiceActivityLog(companyId, shopId, dbInvoice.getId(), currentEmployeeId, dbInvoice.getInvoiceNumber() + " invoice updated");
        } catch (Exception ex) {
            LOGGER.info("Error in invoice history and activity save");
        }

        return updatedInvoice;
    }

    /**
     * Override method to get invoice by id
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param invoiceId : invoiceId
     * @return: invoice result
     */
    @Override
    public InvoiceResult getInvoiceById(String companyId, String shopId, String invoiceId) {

        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        InvoiceResult invoiceResult = invoiceRepository.get(companyId, invoiceId, InvoiceResult.class);
        if (invoiceResult == null) {
            LOGGER.debug("Exception in Invoice", INVOICE_NOT_FOUND);
            throw new BlazeInvalidArgException(INVOICE, INVOICE_NOT_FOUND);
        }
        prepareInvoiceResult(companyId, shopId, invoiceResult);
        return invoiceResult;
    }

    /**
     * Override method to get invoice by dates
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param startDate : startDate
     * @param endDate   : endDate
     * @param start     : start
     * @param limit     : end
     * @return: invoiceresult
     */
    @Override
    public SearchResult<InvoiceResult> getInvoicesByDates(String companyId, String shopId, String startDate, String endDate, int start, int limit) {
        SearchResult<InvoiceResult> result;

        Shop shop = shopRepository.getById(shopId);

        if (shop == null) {
            throw new BlazeInvalidArgException(INVOICE, "Shop does not exists.");
        }

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        if (start < 0) {
            start = 0;
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");


        if (StringUtils.isBlank(endDate) && StringUtils.isBlank(startDate)) {
            result = invoiceRepository.getAllInvoices(companyId, shopId, start, limit, "{modified:-1}", InvoiceResult.class);
        } else {
            DateTime endDateTime;
            if (StringUtils.isNotBlank(endDate)) {
                endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
            } else {
                endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
            }

            DateTime startDateTime;
            if (StringUtils.isNotBlank(startDate)) {
                startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
            } else {
                startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
            }
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

            result = invoiceRepository.findItemsWithDateAndLimit(companyId, shopId, timeZoneStartDateMillis, timeZoneEndDateMillis, start, limit, InvoiceResult.class);

        }

        if (result != null && !result.getValues().isEmpty()) {
            prepareInvoiceResult(companyId, shopId, result.getValues());
        }


        return result;
    }

    /**
     * Private method to update invoice due date
     *
     * @param invoice : invoice
     */
    private void updateInvoiceDueDate(Invoice invoice) {
        long dueDate = invoice.getDueDate();
        DateTime invoiceDate = new DateTime(invoice.getInvoiceDate());

        switch (invoice.getInvoiceTerms()) {
            case CUSTOM_DATE:
                dueDate = invoice.getDueDate();
                break;
            case NET_7:
                dueDate = invoiceDate.plusDays(7).getMillis();
                break;
            case NET_15:
                dueDate = invoiceDate.plusDays(15).getMillis();
                break;
            case NET_30:
                dueDate = invoiceDate.plusDays(30).getMillis();
                break;
            case NET_45:
                dueDate = invoiceDate.plusDays(45).getMillis();
                break;
            case NET_60:
                dueDate = invoiceDate.plusDays(60).getMillis();
                break;
            case COD:
                dueDate = (invoice.getDueDate() == 0) ? invoiceDate.getMillis() : invoice.getInvoiceDate();
                break;

        }
        invoice.setDueDate(dueDate);
    }

    /**
     * This private method is used to get a sequence for invoice number.
     *
     * @param companyId : companyId
     * @return: invoice number
     */
    private long getNextSequence(String companyId) {
        return companyUniqueSequenceService.getNewIdentifier(companyId, INVOICE, invoiceRepository.count(companyId));
    }

    /**
     * This method is used to update order items for cart if invoice in updating
     *
     * @param companyId : companyId
     * @param cart      : cart
     */
    private void updateOrderItems(String companyId, Cart cart) {

        List<OrderItem> orderItems = cart.getItems();
        List<OrderItem> newOrderItems = new ArrayList<>();
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getId() == null) {
                orderItem.prepare(companyId);
                newOrderItems.add(orderItem);
            } else {
                newOrderItems.add(orderItem);
            }
        }
        cart.setItems(newOrderItems);
    }

    /**
     * This method is used to update note for Invoice.
     *
     * @param currentEmployee : currentEmployee
     * @param invoice         : invoice
     */
    private void updateNotes(Employee currentEmployee, Invoice invoice) {
        if (invoice.getNotes() != null && invoice.getNotes().size() > 0) {
            List<Note> notesList = invoice.getNotes();
            List<Note> newNotesList = new ArrayList<>();
            for (Note note : notesList) {
                if (note.getId() == null || note.getId().isEmpty()) {
                    note.prepare();
                    note.setWriterId(currentEmployee.getId());
                    note.setWriterName(currentEmployee.getFirstName() + " " + currentEmployee.getLastName());
                    newNotesList.add(note);
                } else {
                    newNotesList.add(note);
                }

            }

            invoice.setNotes(newNotesList);

        }
    }

    /**
     * Private method for updating order item id of new prepare cart from old cart items.
     *
     * @param companyId : companyId
     * @param oldCart   : oldcart
     * @param newCart   : newCart
     * @param invoiceId : invoiceId
     */
    private void prepareCartForItems(String companyId, Cart oldCart, Cart newCart, String invoiceId) {
        if (newCart == null || newCart.getItems() == null || oldCart.getItems() == null) {
            return;
        }
        HashMap<String, String> completedManifestMap = new HashMap<>();
        HashMap<String, String> inProgressManifestMap = new HashMap<>();
        HashMap<String, String> inProgressManifestIdsMap = new HashMap<>();
        SearchResult<ShippingManifestResult> searchResult = getShippingManifestByInvoiceId(companyId, invoiceId, 0, Integer.MAX_VALUE);
        if (!CollectionUtils.isNullOrEmpty(searchResult.getValues())) {
            for (ShippingManifest manifestResult : searchResult.getValues()) {
                for (ProductMetrcInfo metrcInfo : manifestResult.getProductMetrcInfo()) {
                    if (ShippingManifest.ShippingManifestStatus.InProgress == manifestResult.getStatus()) {
                        inProgressManifestMap.put(metrcInfo.getProductId(), metrcInfo.getOrderItemId());
                        inProgressManifestIdsMap.put(metrcInfo.getProductId(), manifestResult.getId());
                    }
                    if (ShippingManifest.ShippingManifestStatus.Completed == manifestResult.getStatus()) {
                        completedManifestMap.put(metrcInfo.getProductId(), metrcInfo.getOrderItemId());
                    }
                }
            }
        }
        HashMap<String, String> orderItemMap = new HashMap<>();
        List<ObjectId> productList = new ArrayList<>();
        for (OrderItem item : oldCart.getItems()) {
            orderItemMap.put(item.getProductId(), item.getId());
            productList.add(new ObjectId(item.getProductId()));
        }
        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, productList);
        for (OrderItem item : newCart.getItems()) {
            if (orderItemMap.containsKey(item.getProductId())) {
                String itemId = orderItemMap.get(item.getProductId());
                if (!itemId.equals(item.getId())) {
                    item.setId(itemId);
                }
                orderItemMap.remove(item.getProductId());
            }

        }
        for (String productId : orderItemMap.keySet()) {
            if (completedManifestMap.containsKey(productId)) {
                Product product = productMap.get(productId);
                throw new BlazeInvalidArgException("ShippingManifest", "Shipping Manifest is already Completed for this ".concat(product.getName()).concat(".So you can't delete this."));
            }
            if (inProgressManifestMap.containsKey(productId)) {
                String manifestId = inProgressManifestIdsMap.get(productId);
                shippingManifestRepository.removeByIdSetState(companyId, manifestId);
            }
        }
    }

    /**
     * Private method to update invoice result
     *
     * @param companyId     : companyId
     * @param shopId        : shopId
     * @param invoiceResult : invoiceResult
     */
    private void prepareInvoiceResult(String companyId, String shopId, InvoiceResult invoiceResult) {

        Employee employee;
        Vendor customerCompany;
        Shop shop = shopRepository.getById(invoiceResult.getShopId());
        String logoURL = shop.getLogo() != null ? shop.getLogo().getPublicURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        invoiceResult.setCompanyLogo(logoURL);

        if (StringUtils.isNotBlank(invoiceResult.getSalesPersonId()) && ObjectId.isValid(invoiceResult.getSalesPersonId())) {
            employee = employeeRepository.getById(invoiceResult.getSalesPersonId());
            if (employee != null) {
                invoiceResult.setEmployeeName(employee.getFirstName() + " " + employee.getLastName());
            }
        }
        if (StringUtils.isNotBlank(invoiceResult.getCustomerId())) {
            customerCompany = vendorRepository.getById(invoiceResult.getCustomerId());
            if (customerCompany != null) {
                invoiceResult.setVendor(customerCompany);
                invoiceResult.setCustomerName(customerCompany.getName());
            }
        }
        if (StringUtils.isNotBlank(invoiceResult.getCompanyContactId())) {
            CompanyContact companyContact = companyContactRepository.getById(invoiceResult.getCompanyContactId());
            if (companyContact != null) {
                invoiceResult.setCompanyContact(companyContact);
            }
        }

        List<InvoiceHistory> invoiceHistoryList = invoiceHistoryRepository.getAllInvociesByInvoiceId(invoiceResult.getId(), companyId, shopId);
        if (!invoiceHistoryList.isEmpty()) {
            invoiceResult.setInvoiceHistoryList(invoiceHistoryList);
        }
        SearchResult<InvoiceActivityLog> activityLogs = getAllInvoiceActivityLog(companyId, shopId, invoiceResult.getId(), 0, Integer.MAX_VALUE);
        if (activityLogs != null && activityLogs.getValues() != null && !activityLogs.getValues().isEmpty()) {
            invoiceResult.setInvoiceActivityLogs(activityLogs.getValues());
        }

        BigDecimal amountPaid = new BigDecimal(0);
        BigDecimal balanceDue;
        BigDecimal changeDue;
        BigDecimal totalInvoiceAmount = new BigDecimal(0);
        if (invoiceResult.getCart() != null && invoiceResult.getCart().getTotal() != null) {
            totalInvoiceAmount = invoiceResult.getCart().getTotal();
        }
        Map<String, BigDecimal> returnMapInvoice = calculateBalanceDue(companyId, invoiceResult, totalInvoiceAmount, amountPaid);
        balanceDue = returnMapInvoice.getOrDefault("balanceDue", BigDecimal.ZERO);
        amountPaid = returnMapInvoice.getOrDefault("amountPaid", BigDecimal.ZERO);
        changeDue = returnMapInvoice.getOrDefault("changeDue", BigDecimal.ZERO);
        if (invoiceResult.getCart() != null) {
            invoiceResult.getCart().setBalanceDue(balanceDue);
            invoiceResult.getCart().setChangeDue(changeDue);
            invoiceResult.getCart().setCashReceived(amountPaid);
        }
        invoiceResult.setBalanceDue(balanceDue);
    }

    /**
     * Private method to calculate balance due
     *
     * @param companyId          : companyId
     * @param dbInvoice          : invoice
     * @param totalInvoiceAmount : totalInvoiceAmount
     * @param amountPaid         : amountPaid
     * @return: return map
     */
    private Map<String, BigDecimal> calculateBalanceDue(String companyId, Invoice dbInvoice, BigDecimal totalInvoiceAmount, BigDecimal amountPaid) {

        BigDecimal balanceDue = new BigDecimal(0);
        BigDecimal changeDue = new BigDecimal(0);
        Map<String, BigDecimal> returnMap = new HashMap<>();

        if (dbInvoice != null && dbInvoice.getCart() != null && dbInvoice.getCart().getTotal().doubleValue() != 0) {
            totalInvoiceAmount = dbInvoice.getCart().getTotal();
        }
        SearchResult<PaymentsReceived> paymentsReceivedSearchResult = invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceId(companyId, dbInvoice.getId(), 0, Integer.MAX_VALUE, "{modified:-1}");
        if (paymentsReceivedSearchResult != null) {

            List<PaymentsReceived> paymentsReceiveds = paymentsReceivedSearchResult.getValues();

            for (PaymentsReceived paymentsReceived : paymentsReceiveds) {
                amountPaid = amountPaid.add(paymentsReceived.getAmountPaid());
            }

            if (dbInvoice != null && dbInvoice.getCart() != null) {

                balanceDue = totalInvoiceAmount.subtract(amountPaid);
                dbInvoice.getCart().setCashReceived(amountPaid);

                if (balanceDue.doubleValue() > 0) {
                    dbInvoice.getCart().setChangeDue(BigDecimal.ZERO);
                    dbInvoice.getCart().setBalanceDue(balanceDue);

                } else {
                    dbInvoice.getCart().setChangeDue(balanceDue.abs());
                    dbInvoice.getCart().setBalanceDue(BigDecimal.ZERO);
                    changeDue = balanceDue.abs();
                }
            }
            Invoice.PaymentStatus invoicePaymentStatus;
            if (amountPaid.compareTo(BigDecimal.ZERO) == 0) {
                invoicePaymentStatus = Invoice.PaymentStatus.UNPAID;
            } else if (balanceDue.compareTo(BigDecimal.ZERO) <= 0) {
                invoicePaymentStatus = Invoice.PaymentStatus.PAID;
                balanceDue = new BigDecimal(0);
                if (Invoice.InvoiceStatus.COMPLETED == dbInvoice.getInvoiceStatus()) {
                    dbInvoice.setActive(false);
                }
            } else {
                invoicePaymentStatus = Invoice.PaymentStatus.PARTIAL;
            }
            if (dbInvoice != null) {
                dbInvoice.setInvoicePaymentStatus(invoicePaymentStatus);
            }
        }
        returnMap.put("balanceDue", balanceDue);
        returnMap.put("amountPaid", amountPaid);
        returnMap.put("changeDue", changeDue);
        return returnMap;
    }

    /**
     * Private method to prepare invoice result for list
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param invoiceList : invoiceList
     */
    private void prepareInvoiceResult(String companyId, String shopId, List<InvoiceResult> invoiceList) {

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> companyContactIds = new LinkedHashSet<>();
        LinkedHashSet<String> invoiceIds = new LinkedHashSet<>();

        ShopLimitedView shop = shopRepository.getById(shopId, ShopLimitedView.class);
        String logoURL = shop.getLogo() != null ? shop.getLogo().getPublicURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";

        for (InvoiceResult invoice : invoiceList) {
            invoice.setCompanyLogo(logoURL);

            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }
            if (StringUtils.isNotBlank(invoice.getSalesPersonId()) && ObjectId.isValid(invoice.getSalesPersonId())) {
                employeeIds.add(new ObjectId(invoice.getSalesPersonId()));
            }
            if (StringUtils.isNotBlank(invoice.getCompanyContactId()) && ObjectId.isValid(invoice.getCompanyContactId())) {
                companyContactIds.add(new ObjectId(invoice.getCompanyContactId()));
            }
            invoiceIds.add(invoice.getId());
        }

        HashMap<String, List<ShippingManifestResult>> shippingManifestHashMap = shippingManifestRepository.findItemsInAsMapByInvoice(companyId, shopId, Lists.newArrayList(invoiceIds), "{driverId: 1, invoiceId: 1, shipperInformation: 1, receiverInformation: 1, shippingManifestNo: 1, productMetrcInfo: 1,deliveryDate:1,deliveryTime:1,signaturePhoto:1,signatureDateTime:1}");
        HashMap<String, List<PaymentsReceived>> paymentsReceivedHashMap = invoicePaymentsRepository.findItemsInAsMapByInvoice(companyId, shopId, Lists.newArrayList(invoiceIds), "{amountPaid : 1, paidDate : 1, invoiceId : 1, referenceNo : 1, paymentType : 1}");

        if (shippingManifestHashMap != null && !shippingManifestHashMap.isEmpty()) {
            for (String invoiceId : shippingManifestHashMap.keySet()) {
                List<ShippingManifestResult> shippingManifestList = shippingManifestHashMap.get(invoiceId);
                if (shippingManifestList != null && !shippingManifestList.isEmpty()) {
                    for (ShippingManifestResult shippingManifest : shippingManifestList) {
                        ShipperInformation shipperInformation = shippingManifest.getShipperInformation();
                        ReceiverInformation receiverInformation = shippingManifest.getReceiverInformation();

                        if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                            vendorIds.add(new ObjectId(receiverInformation.getCustomerCompanyId()));
                        }

                        if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCompanyContactId())) {
                            companyContactIds.add(new ObjectId(receiverInformation.getCompanyContactId()));
                        }

                        if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                            vendorIds.add(new ObjectId(shipperInformation.getCustomerCompanyId()));
                        }

                        if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCompanyContactId())) {
                            companyContactIds.add(new ObjectId(shipperInformation.getCompanyContactId()));
                        }

                        if (StringUtils.isNotBlank(shippingManifest.getDriverId())) {
                            employeeIds.add(new ObjectId(shippingManifest.getDriverId()));
                        }

                    }
                }
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(companyId, Lists.newArrayList(vendorIds));
        HashMap<String, CompanyContact> companyContactHashMap = companyContactRepository.findItemsInAsMap(companyId, Lists.newArrayList(companyContactIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(companyId, Lists.newArrayList(employeeIds));

        for (InvoiceResult invoice : invoiceList) {
            String invoiceId = invoice.getId();

            List<ShippingManifestResult> shippingManifestList = shippingManifestHashMap.get(invoiceId);
            List<PaymentsReceived> paymentsReceivedList = paymentsReceivedHashMap.get(invoiceId);


            BigDecimal amountPaid = new BigDecimal(0);
            BigDecimal balanceDue = BigDecimal.ZERO;
            BigDecimal total = new BigDecimal(0);
            if (invoice.getCart() != null) {
                total = invoice.getCart().getTotal();
            }
            if (paymentsReceivedList != null && !paymentsReceivedList.isEmpty()) {
                for (PaymentsReceived paymentsReceived : paymentsReceivedList) {
                    amountPaid = amountPaid.add(paymentsReceived.getAmountPaid());
                }
            }

            if (invoice.getCart() != null) {
                balanceDue = total.subtract(amountPaid);
                if (balanceDue.doubleValue() <= 0) {
                    balanceDue = BigDecimal.ZERO;
                }
                invoice.getCart().setCashReceived(amountPaid);

                if (balanceDue.doubleValue() > 0) {
                    invoice.getCart().setChangeDue(BigDecimal.ZERO);
                    invoice.getCart().setBalanceDue(balanceDue);

                } else {
                    invoice.getCart().setChangeDue(balanceDue.abs());
                    invoice.getCart().setBalanceDue(BigDecimal.ZERO);
                }
            }
            invoice.setBalanceDue(balanceDue);
            invoice.setTotal(total);

            if (shippingManifestList != null && !shippingManifestList.isEmpty()) {
                for (ShippingManifestResult shippingManifest : shippingManifestList) {
                    ReceiverInformation receiverInformation = shippingManifest.getReceiverInformation();
                    ShipperInformation shipperInformation = shippingManifest.getShipperInformation();
                    Vendor receiver = null;
                    Vendor shipper = null;
                    CompanyContact shipperContact = null;
                    CompanyContact receiverContact = null;
                    if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCustomerCompanyId())) {
                        receiver = vendorHashMap.get(receiverInformation.getCustomerCompanyId());
                    }
                    if (receiverInformation != null && StringUtils.isNotBlank(receiverInformation.getCompanyContactId())) {
                        receiverContact = companyContactHashMap.get(receiverInformation.getCompanyContactId());
                    }
                    if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCompanyContactId())) {
                        shipperContact = companyContactHashMap.get(shipperInformation.getCompanyContactId());
                    }
                    if (shipperInformation != null && StringUtils.isNotBlank(shipperInformation.getCustomerCompanyId())) {
                        shipper = vendorHashMap.get(shipperInformation.getCustomerCompanyId());
                    }
                    if (StringUtils.isNotBlank(shippingManifest.getDriverId()) && employeeHashMap.containsKey(shippingManifest.getDriverId())) {
                        Employee driverDetails = employeeHashMap.get(shippingManifest.getDriverId());
                        shippingManifest.setDriverLicenceNumber(driverDetails.getDriversLicense());
                        shippingManifest.setDriverName(driverDetails.getFirstName() + " " + driverDetails.getLastName());
                        shippingManifest.setDriverVinNo(driverDetails.getVinNo());
                        shippingManifest.setVehicleLicensePlate(driverDetails.getVehicleLicensePlate());
                        shippingManifest.setVehicleMake(driverDetails.getVehicleMake());
                        shippingManifest.setVehicleModel(driverDetails.getVehicleModel());

                    }

                    shippingManifest.setReceiverCustomerCompany(receiver);
                    shippingManifest.setShipperCompany(shop);
                    shippingManifest.setShipperCustomerCompany(shipper);
                    shippingManifest.setReceiverCompanyContact(receiverContact);
                    shippingManifest.setShipperCompanyContact(shipperContact);
                    shippingManifest.setInvoiceNumber(invoice.getInvoiceNumber());

                }
            }
            invoice.setShippingManifests(shippingManifestList);

            invoice.setVendor(vendorHashMap.get(invoice.getCustomerId()));
            invoice.setCompanyContact(companyContactHashMap.get(invoice.getCompanyContactId()));
            Employee employee = employeeHashMap.get(invoice.getSalesPersonId());
            if (employee != null) {
                StringBuilder employeeName = new StringBuilder();
                employeeName.append(employee.getFirstName()).append(" ").append((StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : ""));
            }
        }
    }

    /**
     * Private method to add invoice activity log
     *
     * @param companyId  : companyId
     * @param shopId     : shopId
     * @param invoiceId  : invoiceId
     * @param employeeId : employeeid
     * @param log        : log message
     */
    private void addInvoiceActivityLog(String companyId, String shopId, String invoiceId, String employeeId, String log) {
        if (StringUtils.isNotBlank(invoiceId)) {
            InvoiceActivityLog invoiceActivityLog = new InvoiceActivityLog();
            invoiceActivityLog.prepare(companyId);
            invoiceActivityLog.setShopId(shopId);
            invoiceActivityLog.setEmployeeId(employeeId);
            invoiceActivityLog.setTargetId(invoiceId);
            invoiceActivityLog.setLog(log);
            invoiceActivityLog.setActivityType(InvoiceActivityLog.ActivityType.NORMAL_LOG);
            invoiceActivityLogRepository.save(invoiceActivityLog);
        }
    }

    /**
     * Private method to get invoice activity logs
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param invoiceId : invoiceId
     * @param start     : start
     * @param limit     : limit
     * @return : invoice activity logs.
     */
    private SearchResult<InvoiceActivityLog> getAllInvoiceActivityLog(String companyId, String shopId, String invoiceId, int start, int limit) {
        if (StringUtils.isBlank(invoiceId)) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, INVOICE_NOT_FOUND);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return invoiceActivityLogRepository.getInvoiceActivityLogByInvoiceId(companyId, shopId, invoiceId, "{modified:-1}", start, limit);
    }

    /**
     * Private method to get shipping manifest by invoiceId
     *
     * @param invoiceId : invoiceId
     * @param start     : start
     * @param limit     : ilmit
     * @return : searchResult
     */
    private SearchResult<ShippingManifestResult> getShippingManifestByInvoiceId(String companyId, String invoiceId, int start, int limit) {
        return shippingManifestRepository.getShippingManifestByInvoiceId(companyId, invoiceId, "{modified:-1}", start, limit);
    }

    /**
     * Private method to add invoice history
     *
     * @param companyId  : companyId
     * @param shopId     : shopId
     * @param invoiceId  : invoiceId
     * @param employeeId : employeeId
     */
    private void addInvoiceHistory(String companyId, String shopId, String invoiceId, String employeeId) {
        InvoiceHistory invoiceHistory = new InvoiceHistory();
        invoiceHistory.prepare(companyId);
        invoiceHistory.setShopId(shopId);
        invoiceHistory.setCreatedBy(employeeId);
        invoiceHistory.setInvoiceId(invoiceId);
        invoiceHistoryRepository.save(invoiceHistory);
    }

}
