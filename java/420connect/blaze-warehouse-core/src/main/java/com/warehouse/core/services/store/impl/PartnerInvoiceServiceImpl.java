package com.warehouse.core.services.store.impl;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.partner.invoices.PartnerInvoiceAddRequest;
import com.warehouse.core.rest.partner.invoices.PartnerInvoiceUpdateRequest;
import com.warehouse.core.services.store.PartnerInvoiceService;
import com.warehouse.core.services.store.common.CommonInvoiceService;

public class PartnerInvoiceServiceImpl extends AbstractStoreServiceImpl implements PartnerInvoiceService {

    @Inject
    private CommonInvoiceService commonInvoiceService;

    @Inject
    public PartnerInvoiceServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @Override
    public InvoiceResult getInvoiceById(String invoiceId) {
        return commonInvoiceService.getInvoiceById(storeToken.getCompanyId(), storeToken.getShopId(), invoiceId);
    }

    @Override
    public Invoice createInvoice(PartnerInvoiceAddRequest request) {
        return commonInvoiceService.createInvoice(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), request);
    }

    @Override
    public Invoice prepareInvoice(PartnerInvoiceAddRequest request) {
        return commonInvoiceService.prepareInvoice(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), request);
    }

    @Override
    public Invoice updateInvoice(String invoiceId, PartnerInvoiceUpdateRequest request) {
        return commonInvoiceService.updateInvoice(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), invoiceId, request);
    }

    @Override
    public SearchResult<InvoiceResult> getInvoicesByDates(String startDate, String endDate, int start, int limit) {
        return commonInvoiceService.getInvoicesByDates(storeToken.getCompanyId(), storeToken.getShopId(), startDate, endDate, start, limit);
    }
}
