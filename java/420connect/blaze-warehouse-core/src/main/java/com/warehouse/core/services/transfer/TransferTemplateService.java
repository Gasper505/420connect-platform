package com.warehouse.core.services.transfer;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.warehouse.core.domain.models.transfer.TransferTemplate;
import com.warehouse.core.rest.transfer.request.TransferTemplateAddRequest;
import com.warehouse.core.rest.transfer.result.TransferTemplateResult;

public interface TransferTemplateService {
    TransferTemplateResult getTransferTemplateById(final String transferTemplateId);

    TransferTemplate createTransferTemplate(final TransferTemplateAddRequest request);

    TransferTemplate updateTransferTemplate(final String transferTemplateId, TransferTemplate transferTemplate);

    SearchResult<TransferTemplateResult> getAllTransferTemplates(final int start, final int limit);

    void deleteTransferTemplateById(final String transferTemplateId);

}
