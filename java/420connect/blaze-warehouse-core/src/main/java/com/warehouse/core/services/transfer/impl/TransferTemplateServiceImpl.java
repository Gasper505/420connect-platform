package com.warehouse.core.services.transfer.impl;

import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.transfer.TransferTemplate;
import com.warehouse.core.domain.repositories.transfer.TransferTemplateRepository;
import com.warehouse.core.rest.transfer.request.Destination;
import com.warehouse.core.rest.transfer.request.TransferTemplateAddRequest;
import com.warehouse.core.rest.transfer.request.TransferTemplatePackages;
import com.warehouse.core.rest.transfer.request.Transporter;
import com.warehouse.core.rest.transfer.result.TransferTemplateResult;
import com.warehouse.core.services.invoice.InvoiceService;
import com.warehouse.core.services.transfer.TransferTemplateService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TransferTemplateServiceImpl extends AbstractAuthServiceImpl implements TransferTemplateService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransferTemplateServiceImpl.class);
    private static final String TRANSFER_TEMPLATE = "Transfer Template";
    private static final String TEMPALTE_INVOICE_NOT_EMPTY = "Template Invoice can not be empty..!";
    private static final String TEMPLATE_NOT_FOUND = "Error! Transfer Template does not exist.";
    private static final String TEMPLATE_ID_NOT_FOUND = "Error! Transfer Template ID does not exist.";
    private static final String TEMPLATE_DELETED = "This Transfer Template already deleted";
    private InvoiceService invoiceService;
    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;
    private TransferTemplateRepository templateRepository;
    private MetrcAccountService metrcAccountService;

    @Inject
    public TransferTemplateServiceImpl(Provider<ConnectAuthToken> token, EmployeeService employeeService,
                                       TransferTemplateRepository templateRepository, InvoiceService invoiceService,
                                       EmployeeRepository employeeRepository, MetrcAccountService metrcAccountService) {
        super(token);

        this.employeeService = employeeService;
        this.templateRepository = templateRepository;
        this.invoiceService = invoiceService;
        this.employeeRepository = employeeRepository;
        this.metrcAccountService = metrcAccountService;
    }

    @Override
    public TransferTemplate createTransferTemplate(TransferTemplateAddRequest transferTemplateAddRequest) {


        if (transferTemplateAddRequest.getInvoiceId() == null) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPALTE_INVOICE_NOT_EMPTY);
        }

        TransferTemplate transferTemplate = new TransferTemplate();
        transferTemplate.prepare();
        transferTemplate.setCompanyId(token.getCompanyId());
        transferTemplate.setShopId(token.getShopId());
        transferTemplate.setEmployeeId(token.getActiveTopUser().getUserId());
        transferTemplate.setInvoiceId(transferTemplateAddRequest.getInvoiceId());
        transferTemplate.setTemplateManifestNumber(transferTemplateAddRequest.getTemplateManifestNumber());
        transferTemplate.setTemplateShipperLicenseNumber(transferTemplateAddRequest.getTemplateShipperLicenseNumber());
        transferTemplate.setTemplateName(transferTemplateAddRequest.getTemplateName());
        transferTemplate.setTemplateStatus(transferTemplateAddRequest.getTemplateStatus());
        if (transferTemplateAddRequest.getDestinations() != null && !transferTemplateAddRequest.getDestinations().isEmpty()) {
            prepareTransferDestination(transferTemplateAddRequest.getDestinations());
            transferTemplate.setDestinations(transferTemplateAddRequest.getDestinations());
        }
        return templateRepository.save(transferTemplate);

    }

    @Override
    public TransferTemplate updateTransferTemplate(String transferTemplateId, TransferTemplate transferTemplate) {
        if (StringUtils.isBlank(transferTemplateId)) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_ID_NOT_FOUND);
        }
        TransferTemplate dbTemplate = templateRepository.get(token.getCompanyId(), transferTemplateId);
        if (dbTemplate == null) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_NOT_FOUND);
        }
        if (transferTemplate.getDestinations() != null && !transferTemplate.getDestinations().isEmpty()) {
            prepareTransferDestination(transferTemplate.getDestinations());
            dbTemplate.setDestinations(transferTemplate.getDestinations());
        }
        TransferTemplate update = templateRepository.update(transferTemplateId, dbTemplate);
        return update;
    }

    public void prepareTransferDestination(List<Destination> destinations) {
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, MetricsPackages> metrcMap = metrcAccountService.getMetrcPackagesMap();
        for (Destination destination : destinations) {
            if (destination.getTransporters() != null && !destination.getTransporters().isEmpty()) {

                List<Transporter> transporters = destination.getTransporters();
                List<Transporter> newTransportes = new ArrayList<>();
                for (Transporter transporter : transporters) {
                    Employee employee = employeeMap.get(transporter.getTransporterEmployeeId());
                    if (employee != null) {
                        transporter.setDriverName(employee.getFirstName().concat(" ").concat(employee.getLastName()));
                        transporter.setDriverLicenseNumber(employee.getDriversLicense());
                        transporter.setVehicleModel(employee.getVehicleModel());
                        transporter.setVehicleLicensePlateNumber(employee.getVehicleLicensePlate());
                        transporter.setVehicleMake(employee.getVehicleMake());
                        newTransportes.add(transporter);
                    }
                }
                destination.setTransporters(newTransportes);
                List<TransferTemplatePackages> transferPackages = destination.getPackages();
                List<TransferTemplatePackages> newTransferPackages = new ArrayList<>();
                if (!metrcMap.isEmpty() && !transferPackages.isEmpty()) {
                    for (TransferTemplatePackages transferPackage : transferPackages) {
                        MetricsPackages metricsPackages = metrcMap.get(transferPackage.getPackageLabel() == null ? null : transferPackage.getPackageLabel());
                        if (metricsPackages != null) {
                            transferPackage.setPackageLabel(metricsPackages.getLabel());
                            transferPackage.setItemName(metricsPackages.getProductName());
                            transferPackage.setQuantity(metricsPackages.getQuantity());
                            transferPackage.setUnitOfMeasureName(metricsPackages.getUnitOfMeasureName());
                            transferPackage.setGrossWeight(transferPackage.getGrossWeight());
                            transferPackage.setGrossUnitOfWeightName(transferPackage.getUnitOfMeasureName());
                            transferPackage.setWholesalePrice(transferPackage.getWholesalePrice());
                            newTransferPackages.add(transferPackage);
                        }
                        destination.setPackages(newTransferPackages);
                    }
                }
            }
        }

    }

    @Override
    public TransferTemplateResult getTransferTemplateById(String transferTemplateId) {
        if (StringUtils.isBlank(transferTemplateId)) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_ID_NOT_FOUND);
        }

        TransferTemplateResult templateRequest = templateRepository.get(token.getCompanyId(), transferTemplateId, TransferTemplateResult.class);
        if (templateRequest == null) {
            LOGGER.debug("Exception in Transfer Template", TEMPLATE_NOT_FOUND);
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_NOT_FOUND);
        }
        prepareTransferTemplate(templateRequest);
        return templateRequest;
    }

    private void prepareTransferTemplate(TransferTemplateResult templateResult) {

        if (StringUtils.isNotBlank(templateResult.getEmployeeId())) {
            Employee transferBy = employeeService.getEmployeeById(templateResult.getEmployeeId());
            templateResult.setDriverLicenseNumber(transferBy.getDriversLicense());
            templateResult.setDriverName(transferBy.getFirstName().concat("").concat(transferBy.getLastName()));
            templateResult.setVehicleLicensePlateNumber(transferBy.getVehicleLicensePlate());
            templateResult.setVehicleMake(transferBy.getVehicleMake());
            templateResult.setVehicleModel(transferBy.getVehicleModel());
        }
        if (StringUtils.isNotBlank(templateResult.getInvoiceId())) {
            Invoice invoice = invoiceService.getInvoiceById(templateResult.getInvoiceId());
            templateResult.setInvoice(invoice);
        }


    }

    @Override
    public SearchResult<TransferTemplateResult> getAllTransferTemplates(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<TransferTemplateResult> searchResult = templateRepository.getAllTransferTemplate(token.getCompanyId(), token.getShopId(), start, limit, "{modified:-1}", TransferTemplateResult.class);
        if (searchResult.getValues().size() > 0) {
            for (TransferTemplateResult templateResult : searchResult.getValues()) {
                prepareTransferTemplate(templateResult);
            }
        }
        return searchResult;
    }

    @Override
    public void deleteTransferTemplateById(String transferTemplateId) {
        if (StringUtils.isBlank(transferTemplateId)) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_ID_NOT_FOUND);
        }
        TransferTemplate dbTransferTemplate = templateRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), transferTemplateId);
        if (dbTransferTemplate == null) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_NOT_FOUND);
        } else if (dbTransferTemplate.isDeleted()) {
            throw new BlazeInvalidArgException(TRANSFER_TEMPLATE, TEMPLATE_DELETED);
        } else {
            try {
                templateRepository.removeByIdSetState(token.getCompanyId(), transferTemplateId);
            } catch (Exception ex) {

            }
        }
    }
}
