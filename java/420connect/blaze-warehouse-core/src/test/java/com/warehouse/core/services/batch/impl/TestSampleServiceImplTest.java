package com.warehouse.core.services.batch.impl;

import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.ProductBatchService;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleStatusRequest;
import com.google.inject.Provider;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class TestSampleServiceImplTest {
    private ConnectAuthToken connectAuthToken;
    private TestSampleRepository testSampleRepository;
    private CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    private Provider<ConnectAuthToken> token;
    private TestSampleServiceImpl testSampleService;
    private ProductBatchRepository productBatchRepository;
    private TestSample testSample;
    private VendorRepository vendorRepository;
    private ProductBatchService productBatchService;
    private InventoryRepository inventoryRepository;
    private BatchQuantityService batchQuantityService;
    private BatchQuantityRepository batchQuantityRepository;
    private ProductBatch productBatch;

    @Before
    public void setUp() throws Exception {
        connectAuthToken = mock(ConnectAuthToken.class);
        testSampleRepository = mock(TestSampleRepository.class);
        companyUniqueSequenceRepository = mock(CompanyUniqueSequenceRepository.class);
        testSample = mock(TestSample.class);
        productBatchRepository = mock(ProductBatchRepository.class);
        vendorRepository = mock(VendorRepository.class);
        productBatchService = mock(ProductBatchService.class);
        inventoryRepository = mock(InventoryRepository.class);
        batchQuantityRepository = mock(BatchQuantityRepository.class);
        batchQuantityService = mock(BatchQuantityService.class);
        productBatch = mock(ProductBatch.class);

        token = () -> connectAuthToken;
        testSampleService = spy(new TestSampleServiceImpl(token,
                testSampleRepository,
                productBatchRepository, vendorRepository,
                productBatchService, inventoryRepository,
                batchQuantityService, batchQuantityRepository));
    }

    @Ignore
    @Test
    public void addTestSample() throws Exception {
        TestSampleAddRequest testSampleAddRequest = mock(TestSampleAddRequest.class);
        CompanyUniqueSequence companyUniqueSequence = mock(CompanyUniqueSequence.class);
        Vendor vendor = mock(Vendor.class);
        when(testSampleAddRequest.getBatchId()).thenReturn("12345");

        Inventory inventory = mock(Inventory.class);
        when(inventory.getId()).thenReturn("11");
        when(inventoryRepository.getInventory(token.get().getCompanyId(), token.get().getShopId(), Inventory.QUARANTINE)).thenReturn(inventory);

        BatchQuantity batchQuantity = mock(BatchQuantity.class);
        List<BatchQuantity> batchQuantities = new ArrayList<>();
        batchQuantities.add(batchQuantity);

        ProductBatchByInventory productBatchByInventory = mock(ProductBatchByInventory.class);
        when(productBatchByInventory.getInventoryId()).thenReturn("11");
        List<ProductBatchByInventory> quantities = new ArrayList<>();
        quantities.add(productBatchByInventory);
        when(batchQuantityRepository.getBatchQuantitiesByInventory(token.get().getCompanyId(), token.get().getShopId(), productBatch.getId(), productBatch.getProductId())).thenReturn(quantities);
        when(productBatchByInventory.getBatchQuantity()).thenReturn(batchQuantities);
        when(batchQuantity.getQuantity()).thenReturn(BigDecimal.TEN);


        //when ( productBatchByInventory.getBatchQuantity () ).thenReturn ( batchQuantities );

        when(testSampleAddRequest.getSample()).thenReturn(BigDecimal.ONE);
        when(testSampleAddRequest.getTestingCompanyId()).thenReturn("vendorId");
        when(productBatchRepository.get(token.get().getCompanyId(), testSampleAddRequest.getBatchId())).thenReturn(productBatch);
        when(companyUniqueSequenceRepository.getNewIdentifier(anyString(), anyString())).thenReturn(companyUniqueSequence);
        when(productBatch.getStatus()).thenReturn(ProductBatch.BatchStatus.IN_TESTING);
        when(vendorRepository.get(token.get().getCompanyId(), testSampleAddRequest.getTestingCompanyId())).thenReturn(vendor);
        when(vendor.getCompanyType()).thenReturn(Vendor.CompanyType.TESTING_FACILITY);

        testSampleService.addTestSample(testSampleAddRequest);

        verify(testSampleRepository).save(any(TestSample.class));
        verify(testSampleService).addTestSample(any(TestSampleAddRequest.class));
    }

    @Ignore
    @Test
    public void updateTestSample() throws Exception {
        CompanyUniqueSequence companyUniqueSequence = mock(CompanyUniqueSequence.class);
        Vendor vendor = mock(Vendor.class);
        TestSample dbTestSample = mock(TestSample.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(testSample.getBatchId()).thenReturn("12345");
        when(testSampleRepository.get("12345", "sampleId")).thenReturn(dbTestSample);
        when(dbTestSample.getSample()).thenReturn(BigDecimal.TEN);
        when(testSample.getSample()).thenReturn(BigDecimal.ONE);
        when(testSample.getBatchId()).thenReturn("12345");
        //when(testsample.getSample()).thenReturn(BigDecimal.TEN);
        when(testSample.getTestingCompanyId()).thenReturn("vendorId");
        when(productBatchRepository.get(token.get().getCompanyId(), testSample.getBatchId())).thenReturn(productBatch);
        when(companyUniqueSequenceRepository.getNewIdentifier(anyString(), anyString())).thenReturn(companyUniqueSequence);
        when(productBatch.getStatus()).thenReturn(ProductBatch.BatchStatus.IN_TESTING);
        when(vendorRepository.get(token.get().getCompanyId(), testSample.getTestingCompanyId())).thenReturn(vendor);
        when(vendor.getCompanyType()).thenReturn(Vendor.CompanyType.TESTING_FACILITY);

        testSampleService.updateTestSample("sampleId", testSample);
        verify(testSampleRepository).get("12345", "sampleId");
        verify(testSampleService).updateTestSample("sampleId", testSample);
    }

    @Ignore
    @Test
    public void updateTestSampleStatus() throws Exception {
        TestSampleStatusRequest testSampleStatusRequest = mock(TestSampleStatusRequest.class);
        when(testSampleStatusRequest.getUpdatedStatus()).thenReturn(TestSample.SampleStatus.PENDING);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(testSampleRepository.get("12345", "sampleId")).thenReturn(testSample);
        testSampleService.updateTestSampleStatus("sampleId", testSampleStatusRequest);
        verify(testSampleRepository).get("12345", "sampleId");
        verify(testSampleService).updateTestSampleStatus("sampleId", testSampleStatusRequest);
    }

    @Ignore
    @Test
    public void getAllTestSamplesByBatchId() throws Exception {
        ArrayList<TestSample> testSamples = new ArrayList<>();
        testSamples.add(testSample);
        SearchResult<TestSample> testSampleSearchResult = new SearchResult<>();
        testSampleSearchResult.setValues(testSamples);
        testSampleSearchResult.setTotal(1L);
        testSampleSearchResult.setSkip(0);
        testSampleSearchResult.setLimit(200);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(testSampleRepository.getAllTestSamplesByBatchId(anyString(), anyString(), anyString(), anyInt(), anyInt())).thenReturn(testSampleSearchResult);
        testSampleService.getAllTestSamplesByBatchId("batchId", 0, 200);
        verify(testSampleRepository).getAllTestSamplesByBatchId("12345", "batchId", "{modified:-1}", 0, 200);
        verify(testSampleService).getAllTestSamplesByBatchId("batchId", 0, 200);
    }

    @Ignore
    @Test
    public void deleteTestSampleById() throws Exception {
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(testSampleRepository.getByShopAndId("12345", "1234", "sampleId")).thenReturn(testSample);
        when(productBatchRepository.get(token.get().getCompanyId(), testSample.getId())).thenReturn(productBatch);
        when(testSample.getSample()).thenReturn(BigDecimal.ONE);
        testSampleService.deleteTestSampleById("sampleId");
        verify(testSampleRepository).getByShopAndId("12345", "1234", "sampleId");
        verify(testSampleService).deleteTestSampleById("sampleId");
    }

}