package com.warehouse.core.services.expenses.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.TerminalUser;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;
import com.warehouse.core.domain.repositories.expenses.ExpenseActivityLogRepository;
import com.warehouse.core.domain.repositories.expenses.ExpensesRepository;
import com.warehouse.core.rest.expenses.request.ExpenseEmailRequest;
import com.warehouse.core.rest.expenses.request.ExpensesRequest;
import com.warehouse.core.rest.expenses.result.ExpensesResult;
import com.warehouse.core.services.expenses.ExpenseActivityLogService;
import com.warehouse.core.services.invoice.InvoiceService;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ExpensesServiceImplTest {
    private ConnectAuthToken connectAuthToken;
    private ExpensesRepository expensesRepository;
    private MemberRepository memberRepository;
    private Provider<ConnectAuthToken> token;
    private ExpensesServiceImpl expenseServiceImpl;
    private Expenses expenses;
    private ExpensesResult expensesResult;
    private CustomerCompanyRepository customerCompanyRepository;
    private ExpenseActivityLogService expenseActivityLogService;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private InvoiceService invoiceService;
    private CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    private PurchaseOrderRepository purchaseOrderRepository;
    private AmazonS3Service amazonS3Service;
    private VendorRepository vendorRepository;
    private UserActivityService userActivityService;
    private TerminalUser terminalUser;
    private Vendor vendor;
    private CompanyRepository companyRepository;
    private ElasticSearchManager elasticSearchManager;
    private CompanyAssetRepository companyAssetRepository;
    private EmployeeRepository employeeRepository;
    private ShopRepository shopRepository;

    @Before
    public void setUp() throws Exception {
        connectAuthToken = mock(ConnectAuthToken.class);
        expensesRepository = mock(ExpensesRepository.class);
        memberRepository = mock(MemberRepository.class);
        expenses = mock(Expenses.class);
        expensesResult = mock(ExpensesResult.class);
        customerCompanyRepository = mock(CustomerCompanyRepository.class);
        expenseActivityLogService = mock(ExpenseActivityLogService.class);
        amazonServiceManager = mock(AmazonServiceManager.class);
        connectConfiguration = mock(ConnectConfiguration.class);
        invoiceService = mock(InvoiceService.class);
        companyUniqueSequenceRepository = mock(CompanyUniqueSequenceRepository.class);
        purchaseOrderRepository = mock(PurchaseOrderRepository.class);
        amazonS3Service = mock(AmazonS3Service.class);
        vendorRepository = mock(VendorRepository.class);
        userActivityService = mock(UserActivityService.class);
        terminalUser = mock(TerminalUser.class);
        vendor = mock(Vendor.class);
        companyRepository = mock(CompanyRepository.class);
        elasticSearchManager = mock(ElasticSearchManager.class);
        companyAssetRepository = mock(CompanyAssetRepository.class);
        employeeRepository = mock(EmployeeRepository.class);
        shopRepository = mock(ShopRepository.class);

        token = () -> connectAuthToken;
        expenseServiceImpl = spy(new ExpensesServiceImpl(token,
                expensesRepository, expenseActivityLogService,
                amazonServiceManager,
                connectConfiguration,
                companyUniqueSequenceRepository,
                purchaseOrderRepository,
                amazonS3Service,
                vendorRepository,
                userActivityService,
                companyRepository,
                elasticSearchManager, companyAssetRepository, employeeRepository, shopRepository));
    }

    @Test
    public void getExpensesById() throws Exception {
        Company company = mock(Company.class);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(expensesRepository.get("12345", "expenseId", ExpensesResult.class)).thenReturn(expensesResult);
        when(expensesResult.getCompanyId()).thenReturn("12345");
        when(companyRepository.getById("12345")).thenReturn(company);
        company = companyRepository.getById("12345");
        expenseServiceImpl.getExpensesById("expenseId");
        verify(expensesRepository).get("12345", "expenseId", ExpensesResult.class);
        when(expensesResult.getCompanyId()).thenReturn("12345");
        when(companyRepository.getById("12345")).thenReturn(company);
        company = companyRepository.getById("12345");
        when(company.getLogoURL()).thenReturn("url");
        verify(expenseServiceImpl).getExpensesById("expenseId");
    }

    @Test
    public void createExpenses() throws Exception {
        ExpensesRequest expensesRequest = mock(ExpensesRequest.class);
        CompanyUniqueSequence companyUniqueSequence = mock(CompanyUniqueSequence.class);
        Expenses dbExpenses = mock(Expenses.class);
        TerminalUser topUser = mock(TerminalUser.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getActiveTopUser()).thenReturn(topUser);
        when(topUser.getUserId()).thenReturn("6789");
        when(expensesRequest.getCustomerId()).thenReturn("vendorId");
        when(expensesRequest.getExpenseName()).thenReturn("expense name");
        when(expensesRequest.getExpenseCategory()).thenReturn(Expenses.ExpenseCategory.LUNCH);
        BigDecimal amount = new BigDecimal(10);
        when(expensesRequest.getAmount()).thenReturn(amount);

        when(vendorRepository.get("12345", "vendorId")).thenReturn(vendor);
        when(companyUniqueSequenceRepository.getNewIdentifier(token.get().getCompanyId(), "Expense")).thenReturn(companyUniqueSequence);

        when(expensesRepository.save(expenses)).thenReturn(dbExpenses);

        expenseServiceImpl.createExpenses(expensesRequest);
        verify(expenseServiceImpl).createExpenses(any(ExpensesRequest.class));
        verify(expensesRepository).save(any(Expenses.class));

    }

    @Test
    public void updateExpenses() throws Exception {
        TerminalUser topUser = mock(TerminalUser.class);
        when(token.get().getActiveTopUser()).thenReturn(topUser);
        when(topUser.getUserId()).thenReturn("6789");
        when(token.get().getCompanyId()).thenReturn("12345");
        when(expensesRepository.get("12345", "expenseId")).thenReturn(expenses);
        when(expenses.getCustomerId()).thenReturn("vendorId");
        when(expenses.getExpenseName()).thenReturn("expense name");
        when(expenses.getExpenseCategory()).thenReturn(Expenses.ExpenseCategory.LUNCH);
        BigDecimal amount = new BigDecimal(10);
        when(expenses.getAmount()).thenReturn(amount);
        when(vendorRepository.get("12345", "vendorId")).thenReturn(vendor);

        expenseServiceImpl.updateExpenses("expenseId", expenses);
        verify(expensesRepository).get("12345", "expenseId");
        verify(expenseServiceImpl).updateExpenses("expenseId", expenses);
    }

    @Test
    public void deleteExpensesById() throws Exception {
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        when(token.get().getActiveTopUser()).thenReturn(terminalUser);
        when(terminalUser.getUserId()).thenReturn("12345");
        when(expensesRepository.getByShopAndId("12345", "123", "expenseId")).thenReturn(expenses);
        expenseServiceImpl.deleteExpensesById("expenseId");
        verify(expenseServiceImpl).deleteExpensesById("expenseId");
        verify(expensesRepository).getByShopAndId("12345", "123", "expenseId");
    }

    @Test
    public void getAllExpensesByShop() throws Exception {
        Company company = mock(Company.class);
        ArrayList<ExpensesResult> expensesResults = new ArrayList<>();
        expensesResults.add(expensesResult);
        SearchResult<ExpensesResult> expensesSearchResult = new SearchResult<>();
        expensesSearchResult.setValues(expensesResults);
        expensesSearchResult.setTotal(1L);
        expensesSearchResult.setSkip(0);
        expensesSearchResult.setLimit(200);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        when(expensesResult.getCompanyId()).thenReturn("12345");
        when(companyRepository.getById("12345")).thenReturn(company);
        company = companyRepository.getById("12345");
        when(company.getLogoURL()).thenReturn("url");
        when(expensesRepository.findItems("12345", "123", "{ amount :-1 }", 0, 200, ExpensesResult.class)).thenReturn(expensesSearchResult);
        expenseServiceImpl.getAllExpensesByShop(0, 200, Expenses.ExpenseSort.AMOUNT, "");
        verify(expensesRepository).findItems("12345", "123", "{ amount :-1 }", 0, 200, ExpensesResult.class);
        verify(expenseServiceImpl).getAllExpensesByShop(0, 200, Expenses.ExpenseSort.AMOUNT, "");

    }

    @Test
    public void getAllExpensesByDates() throws Exception {
        Company company = mock(Company.class);
        ArrayList<ExpensesResult> expensesDetails = new ArrayList<>();
        expensesDetails.add(expensesResult);
        DateSearchResult<ExpensesResult> expensesSearchResult = new DateSearchResult<>();
        expensesSearchResult.setValues(expensesDetails);
        expensesSearchResult.setTotal(1L);
        expensesSearchResult.setSkip(0);
        expensesSearchResult.setLimit(200);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(expensesResult.getCompanyId()).thenReturn("12345");
        when(companyRepository.getById("12345")).thenReturn(company);
        company = companyRepository.getById("12345");
        when(company.getLogoURL()).thenReturn("url");
        when(expensesRepository.findExpensesWithDate("12345", "1234", Long.parseLong("1502450831756"), Long.parseLong("1502450831756"), "{modified:-1}", ExpensesResult.class)).thenReturn(expensesSearchResult);
        expenseServiceImpl.getAllExpensesByDates(Long.parseLong("1502450831756"), Long.parseLong("1502450831756"));
        verify(expensesRepository).findExpensesWithDate("12345", "1234", Long.parseLong("1502450831756"), Long.parseLong("1502450831756"), "{modified:-1}", ExpensesResult.class);
        verify(expenseServiceImpl).getAllExpensesByDates(Long.parseLong("1502450831756"), Long.parseLong("1502450831756"));

    }

    @Test
    public void markExpenseAsArchived() throws Exception {
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        when(expensesRepository.get("12345", "expenseId")).thenReturn(expenses);
        expensesRepository.update("expenseId", expenses);
        expenses.setArchive(true);
        verify(expensesRepository).update("expenseId", expenses);
    }

    @Test
    public void addActivityComment() throws Exception {
        ActivityCommentRequest request = mock(ActivityCommentRequest.class);
        ExpensesActivityLog activityLog = mock(ExpensesActivityLog.class);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        when(request.getComment()).thenReturn("test comment");
        when(request.getTargetId()).thenReturn("expenseId");
        when(expensesRepository.getById("expenseId")).thenReturn(expenses);
        expenseActivityLogService.addActivityComment(request);
        verify(expenseActivityLogService).addActivityComment(request);
    }

    @Test
    public void getAllExpenseActivityLogsByExpenseId() throws Exception {
        ExpenseActivityLogRepository expenseActivityLogRepository = mock(ExpenseActivityLogRepository.class);
        ExpensesActivityLog expenseActivityLog = mock(ExpensesActivityLog.class);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        ArrayList<ExpensesActivityLog> expensesLogs = new ArrayList<>();
        expensesLogs.add(expenseActivityLog);
        SearchResult<ExpensesActivityLog> expensesSearchResult = new SearchResult<>();
        expensesSearchResult.setValues(expensesLogs);
        expensesSearchResult.setTotal(1L);
        expensesSearchResult.setSkip(0);
        expensesSearchResult.setLimit(200);
        when(expenseActivityLogRepository.getAllExpenseActivityLogByExpenseId("12345", "123", "expenseId", "{ modified:-1 }", 0, 200)).thenReturn(expensesSearchResult);
        expenseActivityLogRepository.getAllExpenseActivityLogByExpenseId("12345", "123", "expenseId", "{ modified:-1 }", 0, 200);
        expenseActivityLogService.getAllActivityLogsForExpenses("expenseId", 0, 200);
        verify(expenseActivityLogRepository).getAllExpenseActivityLogByExpenseId("12345", "123", "expenseId", "{ modified:-1 }", 0, 200);
        verify(expenseActivityLogService).getAllActivityLogsForExpenses("expenseId", 0, 200);
    }

    @Test
    public void createExpensePdf() throws Exception {
        Company company = mock(Company.class);
        Address address = mock(Address.class);
        Note notes = mock(Note.class);
        List<Note> noteList = new ArrayList<>();
        noteList.add(notes);
        BigDecimal balance = mock(BigDecimal.class);

        //InputStream inputStream = mock(InputStream.class);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        when(expensesRepository.get("12345", "expenseId")).thenReturn(expenses);
        when(expenses.getReference()).thenReturn("reference");
        when(expenses.getCustomerId()).thenReturn("vendorId");
        when(vendorRepository.get("12345", "vendorId")).thenReturn(vendor);
        when(expenses.getExpenseName()).thenReturn("ExpenseName");
        when(vendor.getName()).thenReturn("VendorName");
        when(vendor.getAddress()).thenReturn(address);
        when(address.getAddress()).thenReturn("Address");
        when(address.getCity()).thenReturn("City");
        when(address.getState()).thenReturn("State");
        when(address.getZipCode()).thenReturn("ZipCode");
        when(expenses.getBalanceDue()).thenReturn(balance);
        BigDecimal amount = new BigDecimal(10);
        when(expenses.getAmount()).thenReturn(amount);
        when(expenses.getModified()).thenReturn(Long.parseLong("123456789"));
        when(expenses.getNotes()).thenReturn(noteList);
        when(expenses.getExpenseQRCodeUrl()).thenReturn("ExpenseCodeUrl");
        when(expenses.getExpenseCategory()).thenReturn(Expenses.ExpenseCategory.BUSINESS_FEES);
        when(token.get().getRequestTimeZone()).thenReturn("America/Los_Angeles");
        when(companyRepository.getById(expenses.getCompanyId())).thenReturn(company);
        company = companyRepository.getById(expenses.getCompanyId());
        when(company.getLogoURL()).thenReturn("url");
        expenseServiceImpl.createPdfForExpense("expenseId");
        verify(expenseServiceImpl).createPdfForExpense("expenseId");
    }

    @Test
    public void sendExpenseEmailToCustomer() throws Exception {
        Company company = mock(Company.class);
        Address address = mock(Address.class);
        Note notes = mock(Note.class);
        List<Note> noteList = new ArrayList<>();
        noteList.add(notes);
        BigDecimal balance = mock(BigDecimal.class);
        ExpenseEmailRequest expenseEmailRequest = mock(ExpenseEmailRequest.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        when(expensesRepository.get("12345", "expenseId")).thenReturn(expenses);
        when(expenses.getReference()).thenReturn("reference");
        when(expenses.getCustomerId()).thenReturn("vendorId");
        when(vendorRepository.get("12345", "vendorId")).thenReturn(vendor);
        when(expenses.getExpenseName()).thenReturn("ExpenseName");
        when(vendor.getName()).thenReturn("VendorName");
        when(vendor.getAddress()).thenReturn(address);
        when(address.getAddress()).thenReturn("Address");
        when(address.getCity()).thenReturn("City");
        when(address.getState()).thenReturn("State");
        when(address.getZipCode()).thenReturn("ZipCode");
        when(expenses.getBalanceDue()).thenReturn(balance);
        BigDecimal amount = new BigDecimal(10);
        when(expenses.getAmount()).thenReturn(amount);

        when(expenses.getModified()).thenReturn(Long.parseLong("123456789"));
        when(expenses.getNotes()).thenReturn(noteList);
        when(expenses.getExpenseQRCodeUrl()).thenReturn("ExpenseCodeUrl");
        when(expenses.getExpenseCategory()).thenReturn(Expenses.ExpenseCategory.BUSINESS_FEES);
        when(token.get().getRequestTimeZone()).thenReturn("America/Los_Angeles");
        when(companyRepository.getById(expenses.getCompanyId())).thenReturn(company);
        company = companyRepository.getById(expenses.getCompanyId());
        when(company.getLogoURL()).thenReturn("url");
        expenseServiceImpl.sendExpenseEmailToCustomer("expenseId", expenseEmailRequest);
        verify(expenseServiceImpl).sendExpenseEmailToCustomer("expenseId", expenseEmailRequest);
    }

}