package com.warehouse.core.services.invoice.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.TerminalUser;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.*;
import com.warehouse.core.domain.repositories.invoice.*;
import com.warehouse.core.rest.invoice.request.*;
import com.warehouse.core.rest.invoice.result.InvoicePaymentResult;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import com.warehouse.core.services.invoice.InvoiceActivityLogService;
import com.warehouse.core.services.invoice.InvoiceHistoryService;
import com.warehouse.core.services.invoice.InvoiceInventoryService;
import com.warehouse.core.services.invoice.ShippingManifestService;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class InvoiceServiceImplTest {

    private ConnectAuthToken connectAuthToken;
    private InvoiceServiceImpl invoiceService;
    private InvoiceRepository invoiceRepository;
    private InvoiceActivityLogRepository invoiceActivityLogRepository;
    private InvoiceAttachmentRepository invoiceAttachmentRepository;
    private Invoice invoice;
    private Provider<ConnectAuthToken> token;
    private InvoiceActivityLog invoiceActivityLog;
    private InvoiceAttachment invoiceAttachment;
    private InvoiceActivityLogService invoiceActivityLogService;
    private EmployeeService employeeService;
    private ProductRepository productRepository;
    private InvoiceHistoryService invoiceHistoryService;
    private InvoicePaymentsRepository invoicePaymentsRepository;
    private CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    private CustomerCompanyRepository customerCompanyRepository;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private InvoiceResult invoiceResult;
    private CompanyRepository companyRepository;
    private VendorRepository vendorRepository;
    private UserActivityService userActivityService;
    private AmazonS3Service amazonS3Service;
    private ShopRepository shopRepository;
    private CartService cartService;
    private ShippingManifestService shippingManifestService;
    private CompanyContactRepository companyContactRepository;
    private ElasticSearchManager elasticSearchManager;
    private CompanyAssetRepository companyAssetRepository;
    private ShippingManifestRepository shippingManifestRepository;
    private EmployeeRepository employeeRepository;
    private InvoiceHistoryRepository invoiceHistoryRepository;
    private InvoiceInventoryService invoiceInventoryService;

    @Before
    public void setUp() throws Exception {
        connectAuthToken = mock(ConnectAuthToken.class);
        invoiceRepository = mock(InvoiceRepository.class);
        invoiceActivityLogRepository = mock(InvoiceActivityLogRepository.class);
        invoiceAttachmentRepository = mock(InvoiceAttachmentRepository.class);
        invoiceActivityLog = mock(InvoiceActivityLog.class);
        invoiceAttachment = mock(InvoiceAttachment.class);
        invoiceActivityLogService = mock(InvoiceActivityLogService.class);
        employeeService = mock(EmployeeService.class);
        productRepository = mock(ProductRepository.class);
        invoice = mock(Invoice.class);
        invoiceHistoryService = mock(InvoiceHistoryService.class);
        invoicePaymentsRepository = mock(InvoicePaymentsRepository.class);
        companyUniqueSequenceRepository = mock(CompanyUniqueSequenceRepository.class);
        customerCompanyRepository = mock(CustomerCompanyRepository.class);
        amazonServiceManager = mock(AmazonServiceManager.class);
        connectConfiguration = mock(ConnectConfiguration.class);
        invoiceResult = mock(InvoiceResult.class);
        companyRepository = mock(CompanyRepository.class);
        vendorRepository = mock(VendorRepository.class);
        userActivityService = mock(UserActivityService.class);
        amazonS3Service = mock(AmazonS3Service.class);
        shopRepository = mock(ShopRepository.class);
        cartService = mock(CartService.class);
        shippingManifestService = mock(ShippingManifestService.class);
        companyContactRepository = mock(CompanyContactRepository.class);
        elasticSearchManager = mock(ElasticSearchManager.class);
        companyAssetRepository = mock(CompanyAssetRepository.class);
        shippingManifestRepository = mock(ShippingManifestRepository.class);
        employeeRepository = mock(EmployeeRepository.class);
        invoiceHistoryRepository = mock(InvoiceHistoryRepository.class);

        invoiceInventoryService = mock(InvoiceInventoryService.class);

        token = () -> connectAuthToken;
        invoiceService = spy(new InvoiceServiceImpl(token,
                invoiceRepository,
                invoiceActivityLogRepository,
                employeeService,
                productRepository,
                invoiceActivityLogService,
                invoiceHistoryService,
                invoicePaymentsRepository,
                amazonServiceManager,
                connectConfiguration,
                shopRepository,
                companyRepository,
                vendorRepository,
                cartService,
                amazonS3Service,
                shippingManifestService,
                companyContactRepository,
                shippingManifestRepository,
                elasticSearchManager,
                companyAssetRepository,
                employeeRepository,
                invoiceHistoryRepository,
                invoiceInventoryService
        ));
    }

    @Test
    public void getInvoiceById() throws Exception {
        Company company = mock(Company.class);
        when(token.get().getCompanyId()).thenReturn("1234");
        when(invoiceRepository.get("1234", "invoiceId", InvoiceResult.class)).thenReturn(invoiceResult);
        when(invoiceResult.getCompanyId()).thenReturn("12345");
        when(companyRepository.getById("12345")).thenReturn(company);
        when(company.getLogoURL()).thenReturn("url");
        when(invoiceResult.getSalesPersonId()).thenReturn("");
        invoiceService.getInvoiceById("invoiceId");
        verify(invoiceRepository).get("1234", "invoiceId", InvoiceResult.class);
        verify(invoiceService).getInvoiceById("invoiceId");
    }

    @Ignore
    @Test
    public void updateInvoice() throws Exception {
        Shop shop = mock(Shop.class);
        Cart cart = mock(Cart.class);
        Vendor vendor = mock(Vendor.class);
        TerminalUser terminalUser = mock(TerminalUser.class);
        BigDecimal value = new BigDecimal(0);
        ArrayList<OrderItem> orderItemList = new ArrayList<>();
        when(token.get().getCompanyId()).thenReturn("1234");
        when(token.get().getShopId()).thenReturn("1234");
        when(shopRepository.get(token.get().getCompanyId(), token.get().getShopId())).thenReturn(shop);
        shop = shopRepository.get(token.get().getCompanyId(), token.get().getShopId());
        when(invoiceRepository.get("1234", "invoiceId")).thenReturn(invoice);
        when(invoice.getCart()).thenReturn(cart);
        when(invoice.getCustomerId()).thenReturn("customerId");
        when(vendorRepository.get(token.get().getCompanyId(), invoice.getCustomerId())).thenReturn(vendor);
        when(invoice.getInvoiceTerms()).thenReturn(Invoice.InvoiceTerms.NET_60);
        when(token.get().getActiveTopUser()).thenReturn(terminalUser);
        when(token.get().getActiveTopUser().getUserId()).thenReturn("12345");
        when(cart.getItems()).thenReturn(orderItemList);
        when(cart.getTotalALExciseTax()).thenReturn(value);
        when(cart.getDeliveryFee()).thenReturn(value);
        when(cart.getTotal()).thenReturn(value);

        invoiceService.updateInvoice("invoiceId", invoice);
        verify(invoiceRepository).get("1234", "invoiceId");
        verify(invoiceService).updateInvoice("invoiceId", invoice);
    }

    @Test
    public void deleteInvoiceById() throws Exception {
        when(token.get().getCompanyId()).thenReturn("1234");
        when(token.get().getShopId()).thenReturn("12345");
        when(invoiceRepository.getByShopAndId("1234", "12345", "mockString")).thenReturn(invoice);
        invoiceService.deleteInvoiceById("mockString");
        verify(invoiceService).deleteInvoiceById("mockString");
        verify(invoiceRepository).getByShopAndId("1234", "12345", "mockString");

    }

    @Test
    public void createInvoice() throws Exception {
        AddInvoiceRequest request = createInvoiceAddRequest();
        invoiceService.createInvoice(request);
        verify(invoiceRepository).save(any(Invoice.class));
        verify(invoiceService).createInvoice(any(AddInvoiceRequest.class));
    }

    @Test
    public void getInvoiceActivityLogByInvoiceId() throws Exception {
        ArrayList<InvoiceActivityLog> logList = new ArrayList<>();
        logList.add(invoiceActivityLog);
        SearchResult<InvoiceActivityLog> invoiceActivityLogSearchResult = new SearchResult<>();
        invoiceActivityLogSearchResult.setValues(logList);
        invoiceActivityLogSearchResult.setTotal(1L);
        invoiceActivityLogSearchResult.setSkip(0);
        invoiceActivityLogSearchResult.setLimit(200);
        when(token.get().getCompanyId()).thenReturn("123");
        when(token.get().getShopId()).thenReturn("123");
        when(invoiceActivityLogRepository.getInvoiceActivityLogByInvoiceId("123", "123", "invoiceId", "{modified:-1}", 0, 200)).thenReturn(invoiceActivityLogSearchResult);
        invoiceService.getInvoiceActivityLogByInvoiceId("invoiceId", 0, 200);
        verify(invoiceActivityLogRepository).getInvoiceActivityLogByInvoiceId("123", "123", "invoiceId", "{modified:-1}", 0, 200);
        verify(invoiceService).getInvoiceActivityLogByInvoiceId("invoiceId", 0, 200);
    }

    @Test
    public void getInvoiceAttachmentTest() throws Exception {
        CompanyAsset asset = mock(CompanyAsset.class);
        ArrayList<CompanyAsset> attachmentList = new ArrayList<>();
        attachmentList.add(asset);
        Invoice invoice = mock(Invoice.class);
        doReturn(attachmentList).when(invoice).getAttachments();
        doReturn("attachmentId").when(asset).getId();
        when(token.get().getCompanyId()).thenReturn("123");
        when(invoiceRepository.get(anyString(), anyString())).thenReturn(invoice);
        invoiceService.getInvoiceAttachment("mockString", "attachmentId");
        verify(invoiceService).getInvoiceAttachment("mockString", "attachmentId");
    }

    @Test
    public void addInvoiceAttachment() throws Exception {
        InvoiceAttachmentRequest request = mock(InvoiceAttachmentRequest.class);
        CompanyAsset companyAsset = mock(CompanyAsset.class);
        List<CompanyAsset> assets = new ArrayList<>();
        assets.add(companyAsset);
        when(token.get().getCompanyId()).thenReturn("123");
        when(invoiceRepository.get("123", "invoiceId")).thenReturn(invoice);
        when(request.getCompanyAsset()).thenReturn(assets);
        invoiceService.addInvoiceAttachment("invoiceId", request);
        verify(invoiceRepository).update("123", invoice.getId(), invoice);
        verify(invoiceService).addInvoiceAttachment("invoiceId", request);
    }

    @Test
    public void deleteInvoiceAttachment() throws Exception {
        CompanyAsset asset = mock(CompanyAsset.class);
        TerminalUser terminalUser = mock(TerminalUser.class);
        List<CompanyAsset> companyAssetList = new ArrayList<>();
        companyAssetList.add(asset);
        //when(invoiceAttachmentRepository.getInvoiceAttachmentByInvoiceIdAndAttachmentId ( "invoiceId","attachmentId")).thenReturn ( invoiceAttachment );
        when(token.get().getCompanyId()).thenReturn("123");
        when(invoiceRepository.get("123", "invoiceId")).thenReturn(invoice);
        when(invoice.getAttachments()).thenReturn(companyAssetList);
        when(invoice.getId()).thenReturn("invoiceId");
        when(asset.getId()).thenReturn("attachmentId");
        when(invoiceRepository.update(token.get().getCompanyId(), invoice.getId(), invoice)).thenReturn(invoice);
        when(token.get().getActiveTopUser()).thenReturn(terminalUser);
        when(token.get().getActiveTopUser().getUserId()).thenReturn("");

        invoiceService.deleteInvoiceAttachment("invoiceId", "attachmentId");
        // verify ( invoiceAttachmentRepository).getInvoiceAttachmentByInvoiceIdAndAttachmentId ( "invoiceId","attachmentId" );
        verify(invoiceService).deleteInvoiceAttachment("invoiceId", "attachmentId");
    }

    @Test
    public void getShopInvoices() throws Exception {
        InvoiceResult invoiceResult = mock(InvoiceResult.class);
        ArrayList<InvoiceResult> invoiceResults = new ArrayList<>();
        invoiceResults.add(invoiceResult);
        SearchResult<InvoiceResult> invoiceResultSearchResult = new SearchResult<>();
        invoiceResultSearchResult.setValues(invoiceResults);
        invoiceResultSearchResult.setTotal(1L);
        invoiceResultSearchResult.setSkip(0);
        invoiceResultSearchResult.setLimit(200);
        when(token.get().getCompanyId()).thenReturn("123");
        when(token.get().getShopId()).thenReturn("123");
        when(invoiceRepository.findItems(token.get().getCompanyId(), token.get().getShopId(), "{modified:-1}", 0, 100, InvoiceResult.class)).thenReturn(invoiceResultSearchResult);
        invoiceRepository.findItems(token.get().getCompanyId(), token.get().getShopId(), "{modified:-1}", 0, 100, InvoiceResult.class);
        verify(invoiceRepository).findItems(token.get().getCompanyId(), token.get().getShopId(), "{modified:-1}", 0, 100, InvoiceResult.class);
    }

    @Test
    public void getCompanyInvoices() throws Exception {
        Invoice invoice = mock(Invoice.class);
        ArrayList<Invoice> invoiceResults = new ArrayList<>();
        invoiceResults.add(invoice);

        DateSearchResult<Invoice> invoiceResultSearchResult = new DateSearchResult<>();
        invoiceResultSearchResult.setValues(invoiceResults);
        invoiceResultSearchResult.setTotal(1L);
        invoiceResultSearchResult.setSkip(0);
        invoiceResultSearchResult.setLimit(200);

        when(token.get().getCompanyId()).thenReturn("123");
        when(token.get().getShopId()).thenReturn("123");
        when(invoiceRepository.findInvoicesWithDate(token.get().getCompanyId(), token.get().getShopId(), 14356564534L, 44545454545L, "{modified:-1}")).thenReturn(invoiceResultSearchResult);

        invoiceRepository.findInvoicesWithDate(token.get().getCompanyId(), token.get().getShopId(), 14356564534L, 44545454545L, "{modified:-1}");
        verify(invoiceRepository).findInvoicesWithDate(token.get().getCompanyId(), token.get().getShopId(), 14356564534L, 44545454545L, "{modified:-1}");
    }

    @Test
    public void getInvoicesByProductRequestStatus() throws Exception {
        String status = "pending";
        List<String> statusList = Arrays.asList(status.split("\\s*,\\s*"));
        List<ProductRequest.RequestStatus> requestStatusList = new ArrayList<>();
        Invoice invoiceResult = mock(Invoice.class);
        ArrayList<Invoice> invoiceResults = new ArrayList<>();
        invoiceResults.add(invoiceResult);
        SearchResult<Invoice> invoiceResultSearchResult = new SearchResult<>();
        invoiceResultSearchResult.setValues(invoiceResults);
        invoiceResultSearchResult.setTotal(1L);
        invoiceResultSearchResult.setSkip(0);
        invoiceResultSearchResult.setLimit(200);

        when(token.get().getCompanyId()).thenReturn("123");
        when(token.get().getShopId()).thenReturn("123");
        when(invoiceRepository.getInvoicesByProductRequestStatus(token.get().getCompanyId(), token.get().getShopId(), "{modified:-1}", 0, 200, requestStatusList)).thenReturn(invoiceResultSearchResult);
        invoiceRepository.getInvoicesByProductRequestStatus(token.get().getCompanyId(), token.get().getShopId(), "{modified:-1}", 0, 200, requestStatusList);
        verify(invoiceRepository).getInvoicesByProductRequestStatus(token.get().getCompanyId(), token.get().getShopId(), "{modified:-1}", 0, 200, requestStatusList);

    }

    @Test
    public void addInvoicePayment() throws Exception {
        String invoiceId = "invoiceId";
        PaymentsReceivedAddRequest request = mock(PaymentsReceivedAddRequest.class);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("123");
        BigDecimal amount = BigDecimal.TEN;

        when(request.getAmountPaid()).thenReturn(amount);
        when(request.getPaymentType()).thenReturn(PaymentsReceived.PaymentType.CASH);
        when(request.getReferenceNo()).thenReturn("referenceNo");

        when(invoiceRepository.get(token.get().getCompanyId(), "invoiceId")).thenReturn(invoice);
        when(invoice.getInvoicePaymentStatus()).thenReturn(Invoice.PaymentStatus.UNPAID);
        when(invoice.getInvoiceStatus()).thenReturn(Invoice.InvoiceStatus.IN_PROGRESS);

        invoiceService.addInvoicePayment(invoiceId, request);
        verify(invoicePaymentsRepository).save(any(PaymentsReceived.class));
        verify(invoiceService).addInvoicePayment(invoiceId, request);
    }

    @Test
    public void getInvoicePayment() throws Exception {
        InvoicePaymentResult payment = mock(InvoicePaymentResult.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");

        when(invoicePaymentsRepository.get(token.get().getCompanyId(), "paymentId", InvoicePaymentResult.class)).thenReturn(payment);
        when(payment.getInvoiceId()).thenReturn("invoiceId");
        when(invoiceRepository.get(token.get().getCompanyId(), "invoiceId")).thenReturn(invoice);

        invoiceService.getInvoicePaymentById("paymentId");

        verify(invoiceService).getInvoicePaymentById("paymentId");

    }

    @Test
    public void getAllInvoicePayment() throws Exception {
        PaymentsReceived payment = mock(PaymentsReceived.class);

        SearchResult<PaymentsReceived> searchResult = new SearchResult<>();
        searchResult.setLimit(Integer.MAX_VALUE);
        searchResult.setSkip(0);
        searchResult.setValues(Lists.newArrayList(payment));

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(invoiceRepository.get(token.get().getCompanyId(), "invoiceId")).thenReturn(invoice);

        when(invoicePaymentsRepository.getAllInvoicePaymentsByInvoiceId(token.get().getCompanyId(), "invoiceId", 0, Integer.MAX_VALUE, "{modified:-1}")).thenReturn(searchResult);

        invoiceService.getAllInvoicePayments("invoiceId", 0, Integer.MAX_VALUE);

        verify(invoiceService).getAllInvoicePayments("invoiceId", 0, Integer.MAX_VALUE);

    }

    @Test
    public void updateInvoiceStatus() throws Exception {
        InvoiceStatusUpdateRequest request = mock(InvoiceStatusUpdateRequest.class);
        TerminalUser user = mock(TerminalUser.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(request.getInvoiceupdateStatus()).thenReturn(Invoice.InvoiceStatus.DRAFT);
        when(invoiceRepository.get(token.get().getCompanyId(), "invoiceId")).thenReturn(invoice);
        when(invoice.getId()).thenReturn("id");
        when(invoiceRepository.update(token.get().getCompanyId(), invoice.getId(), invoice)).thenReturn(invoice);
        when(token.get().getActiveTopUser()).thenReturn(user);
        when(user.getUserId()).thenReturn("userId");

        invoiceService.updateInvoiceStatus("invoiceId", request);

        verify(invoiceService).updateInvoiceStatus("invoiceId", request);
        verify(invoiceRepository).update(token.get().getCompanyId(), invoice.getId(), invoice);
    }

    @Test
    public void addInvoiceComments() throws Exception {
        ActivityCommentRequest request = mock(ActivityCommentRequest.class);
        TerminalUser user = mock(TerminalUser.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(request.getTargetId()).thenReturn("targetId");
        when(request.getComment()).thenReturn("comment");
        when(invoiceRepository.getById("targetId")).thenReturn(invoice);
        when(token.get().getActiveTopUser()).thenReturn(user);
        when(user.getUserId()).thenReturn("userId");

        invoiceActivityLogService.addActivityComment(request);

        verify(invoiceActivityLogService).addActivityComment(request);
    }

    @Test
    public void addShippingManifest() throws Exception {
        ShippingManifestAddRequest request = mock(ShippingManifestAddRequest.class);
        ReceiverInformation receiverInformation = mock(ReceiverInformation.class);
        ShipperInformation shipperInformation = mock(ShipperInformation.class);
        Vendor vendor = mock(Vendor.class);
        Cart cart = mock(Cart.class);
        Employee employee = mock(Employee.class);
        Product product = mock(Product.class);
        OrderItem orderItem = mock(OrderItem.class);
        BigDecimal value = new BigDecimal(0);
        ArrayList<OrderItem> orderItemList = new ArrayList<>();
        orderItemList.add(orderItem);
        ProductMetrcInfo productMetrcInfo = mock(ProductMetrcInfo.class);

        List<ProductMetrcInfo> productMetrcInfos = new ArrayList<>();
        productMetrcInfos.add(productMetrcInfo);


        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(request.getProductMetrcInfo()).thenReturn(productMetrcInfos);
        when(request.getReceiverInformation()).thenReturn(receiverInformation);
        when(request.getShipperInformation()).thenReturn(shipperInformation);
        when(invoiceRepository.getByShopAndId(token.get().getCompanyId(), token.get().getShopId(), request.getInvoiceId())).thenReturn(invoice);
        when(invoice.getInvoiceStatus()).thenReturn(Invoice.InvoiceStatus.IN_PROGRESS);
        when(receiverInformation.getCustomerCompanyId()).thenReturn("receiverVendor");
        when(shipperInformation.getCustomerCompanyId()).thenReturn("shipperVendor");
        when(vendorRepository.getById(receiverInformation.getCustomerCompanyId())).thenReturn(vendor);
        when(vendorRepository.getById(shipperInformation.getCustomerCompanyId())).thenReturn(vendor);
        when(employeeRepository.get(token.get().getCompanyId(), request.getDriverId())).thenReturn(employee);
        when(invoice.getCart()).thenReturn(cart);
        when(cart.getItems()).thenReturn(orderItemList);
        when(orderItem.getProductId()).thenReturn("productId");
        when(orderItem.getQuantity()).thenReturn(value);

        shippingManifestService.addShippingManifest(request);

        verify(shippingManifestService).addShippingManifest(request);

    }

    @Test
    public void getAllShippingManifest() throws Exception {
        ShippingManifestResult result = mock(ShippingManifestResult.class);

        SearchResult<ShippingManifestResult> searchResult = new SearchResult<>();
        searchResult.setLimit(Integer.MAX_VALUE);
        searchResult.setSkip(0);
        searchResult.setValues(Lists.newArrayList(result));

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(shippingManifestRepository.getAllShippingManifest(token.get().getCompanyId(), "{modified:-1}", 0, Integer.MAX_VALUE)).thenReturn(searchResult);

        shippingManifestService.getAllShippingManifest(0, Integer.MAX_VALUE);

        verify(shippingManifestService).getAllShippingManifest(0, Integer.MAX_VALUE);
    }

    @Test
    public void bulkInvoiceUpdates() throws Exception {
        InvoiceBulkUpdateRequest request = mock(InvoiceBulkUpdateRequest.class);
        List<String> invoiceIds = new ArrayList<>();
        invoiceIds.add(ObjectId.get().toString());
        List<ObjectId> invoiceObjectList = new ArrayList<>();

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(request.getInvoiceIds()).thenReturn(invoiceIds);
        when(request.getOperationType()).thenReturn(InvoiceBulkUpdateRequest.InvoiceUpdateOperationType.INVOICE_STATUS);
        when(request.getInvoiceStatus()).thenReturn(Invoice.InvoiceStatus.COMPLETED);

        invoiceService.bulkInvoiceUpdates(request);
        verify(invoiceService).bulkInvoiceUpdates(request);
    }

    @Test
    public void getTotalOutStanding() throws Exception {
        SearchResult<InvoiceResult> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(invoiceResult));
        searchResult.setLimit(Integer.MAX_VALUE);
        searchResult.setSkip(0);

        when(invoiceRepository.getAllInvoicesByCustomerWithoutStatus(token.get().getCompanyId(), token.get().getShopId(), "customerId", 0, Integer.MAX_VALUE, "{modified:-1}")).thenReturn(searchResult);

        invoiceService.getTotalOutStanding("customerId");

        verify(invoiceService).getTotalOutStanding("customerId");
    }

    @Test
    public void getAllInvoicesByFetchStatus() throws Exception {
        InvoiceFetchStatusRequest request = mock(InvoiceFetchStatusRequest.class);
        when(request.getFetchStatus()).thenReturn(InvoiceFetchStatusRequest.FetchStatus.ACTIVE);
        invoiceService.getALLInvoicesByFetchStatus(request, 0, Integer.MAX_VALUE, Invoice.InvoiceSort.DATE, "");
        verify(invoiceService).getALLInvoicesByFetchStatus(request, 0, Integer.MAX_VALUE, Invoice.InvoiceSort.DATE, "");
    }

    @Test
    public void getShippingManifestById() throws Exception {
        ShippingManifestResult shippingManifestResult = mock(ShippingManifestResult.class);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(shippingManifestRepository.get(token.get().getCompanyId(), "shippingManifestId", ShippingManifestResult.class)).thenReturn(shippingManifestResult);
        shippingManifestService.getShippingManifestById("shippingManifestId");
        verify(shippingManifestService).getShippingManifestById("shippingManifestId");


    }

    @Test
    public void getShippingManifestByInvoiceId() throws Exception {
        ShippingManifestResult result = mock(ShippingManifestResult.class);

        SearchResult<ShippingManifestResult> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(result));
        searchResult.setLimit(Integer.MAX_VALUE);
        searchResult.setSkip(0);
        when(shippingManifestRepository.getShippingManifestByInvoiceId(token.get().getCompanyId(), "invoiceId", "{modified:-1}", 0, Integer.MAX_VALUE)).thenReturn(searchResult);

        shippingManifestService.getShippingManifestByInvoiceId("invoiceId", 0, Integer.MAX_VALUE);

        verify(shippingManifestService).getShippingManifestByInvoiceId("invoiceId", 0, Integer.MAX_VALUE);


    }

    @Test
    public void updateShippingManifest() throws Exception {
        ShippingManifestResult shippingManifest = mock(ShippingManifestResult.class);

        ReceiverInformation receiverInformation = mock(ReceiverInformation.class);
        ShipperInformation shipperInformation = mock(ShipperInformation.class);
        Vendor vendor = mock(Vendor.class);
        Cart cart = mock(Cart.class);
        Employee employee = mock(Employee.class);
        Product product = mock(Product.class);
        OrderItem orderItem = mock(OrderItem.class);
        BigDecimal value = new BigDecimal(0);
        ArrayList<OrderItem> orderItemList = new ArrayList<>();
        orderItemList.add(orderItem);

        ProductMetrcInfo productMetrcInfo = mock(ProductMetrcInfo.class);

        List<ProductMetrcInfo> productMetrcInfos = new ArrayList<>();
        productMetrcInfos.add(productMetrcInfo);


        when(shippingManifestRepository.get(token.get().getCompanyId(), "shippingManifestId", ShippingManifestResult.class)).thenReturn(shippingManifest);

        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("1234");
        when(shippingManifest.getProductMetrcInfo()).thenReturn(productMetrcInfos);
        when(shippingManifest.getReceiverInformation()).thenReturn(receiverInformation);
        when(shippingManifest.getShipperInformation()).thenReturn(shipperInformation);
        when(invoiceRepository.getByShopAndId(token.get().getCompanyId(), token.get().getShopId(), shippingManifest.getInvoiceId())).thenReturn(invoice);
        when(invoice.getInvoiceStatus()).thenReturn(Invoice.InvoiceStatus.IN_PROGRESS);
        when(receiverInformation.getCustomerCompanyId()).thenReturn("receiverVendor");
        when(shipperInformation.getCustomerCompanyId()).thenReturn("shipperVendor");
        when(vendorRepository.getById(receiverInformation.getCustomerCompanyId())).thenReturn(vendor);
        when(vendorRepository.getById(shipperInformation.getCustomerCompanyId())).thenReturn(vendor);
        when(employeeRepository.get(token.get().getCompanyId(), shippingManifest.getDriverId())).thenReturn(employee);
        when(invoice.getCart()).thenReturn(cart);
        when(cart.getItems()).thenReturn(orderItemList);
        when(orderItem.getProductId()).thenReturn("productId");
        when(orderItem.getQuantity()).thenReturn(value);

        shippingManifestService.updateShippingManifest("shippingManifestId", shippingManifest);

        verify(shippingManifestService).updateShippingManifest("shippingManifestId", shippingManifest);
    }

    @Test
    public void addActivityComment() throws Exception {
        ActivityCommentRequest request = mock(ActivityCommentRequest.class);

        when(request.getComment()).thenReturn("comment");
        when(request.getTargetId()).thenReturn("targetId");
        when(invoiceRepository.getById(request.getTargetId())).thenReturn(invoice);

        invoiceActivityLogService.addActivityComment(request);

        verify(invoiceActivityLogService).addActivityComment(request);
    }

    @Test
    public void deleteShippingManifest() throws Exception {
        ShippingManifest shippingManifest = mock(ShippingManifest.class);

        when(shippingManifestRepository.getById("shippingManifestId")).thenReturn(shippingManifest);
        when(shippingManifest.getInvoiceId()).thenReturn("invoiceId");

        shippingManifestService.deleteShippingManifest("shippingManifestId");

        verify(shippingManifestService).deleteShippingManifest("shippingManifestId");

    }

    @Test
    public void prepareInvoice() throws Exception {
        AddInvoiceRequest request = createInvoiceAddRequest();
        invoiceService.prepareInvoice(request);
        verify(invoiceService).prepareInvoice(request);

    }

    private AddInvoiceRequest createInvoiceAddRequest() {
        Vendor vendor = mock(Vendor.class);
        Shop shop = mock(Shop.class);
        Cart cart = mock(Cart.class);
        Product product = mock(Product.class);
        OrderItem orderItem = mock(OrderItem.class);
        BigDecimal value = new BigDecimal(0);
        ArrayList<OrderItem> orderItemList = new ArrayList<>();
        orderItemList.add(orderItem);
        AddInvoiceRequest addInvoiceRequest = mock(AddInvoiceRequest.class);
        when(addInvoiceRequest.getCustomerId()).thenReturn("123");
        TerminalUser terminalUser = mock(TerminalUser.class);
        when(token.get().getCompanyId()).thenReturn("12345");
        when(token.get().getShopId()).thenReturn("12345");
        when(addInvoiceRequest.getCustomerId()).thenReturn("vendorId");
        when(vendorRepository.get("12345", "vendorId")).thenReturn(vendor);
        when(shopRepository.get(token.get().getCompanyId(), token.get().getShopId())).thenReturn(shop);
        shop = shopRepository.get(token.get().getCompanyId(), token.get().getShopId());
        when(token.get().getActiveTopUser()).thenReturn(terminalUser);
        when(terminalUser.getUserId()).thenReturn("1234");
        CompanyUniqueSequence sequence = mock(CompanyUniqueSequence.class);
        when(companyUniqueSequenceRepository.getNewIdentifier(anyString(), anyString())).thenReturn(sequence);
        when(addInvoiceRequest.getCart()).thenReturn(cart);
        when(cart.getItems()).thenReturn(orderItemList);
        when(cart.getTotalALExciseTax()).thenReturn(value);
        when(cart.getDeliveryFee()).thenReturn(value);
        when(cart.getTotal()).thenReturn(value);
        when(addInvoiceRequest.getInvoiceTerms()).thenReturn(Invoice.InvoiceTerms.NET_60);
        when(orderItem.getProductId()).thenReturn("productId");
        when(productRepository.get(token.get().getCompanyId(), orderItem.getProductId())).thenReturn(product);
        when(product.getUnitPrice()).thenReturn(value);
        when(orderItem.getQuantity()).thenReturn(value);
        when(orderItem.getDiscount()).thenReturn(value);

        return addInvoiceRequest;
    }
}