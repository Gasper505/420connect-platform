package com.blaze.warehouse.constants;

public class ResourceConstants extends BaseConstants {
    public static final String INVOICE_URL = APP_BASE_URL + "/invoice";
    public static final String EXPENSES_URL = APP_BASE_URL + "/expenses";
    public static final String INVENTORY_TRANSFER_URL = APP_BASE_URL + "/inventoryTransfer";
    public static final String BATCH_URL = APP_BASE_URL + "/batch";
    public static final String REPORTING_URL = APP_BASE_URL + "/reports";
    public static final String DASHBOARD_URL = APP_BASE_URL + "/dashboard";
    public static final String SEARCH_URL = APP_BASE_URL + "/search";
    public static final String SYNC_URL = APP_BASE_URL + "/dataSync";
    public static final String TRANSFER_TEMPLATE_URL = APP_BASE_URL + "/transferTemplate";
    public static final String PRODUCT_URL = APP_BASE_URL + "/product/inventory";
    public static final String SETTING_URL = APP_BASE_URL + "/settings";

}
