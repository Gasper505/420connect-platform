package com.blaze.warehouse.services;

import com.blaze.warehouse.migrations.WareHouseMigrations;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.core.lifecycle.CoreAppStartup;
import com.fourtwenty.core.lifecycle.Migration;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class BlazeWareHouseModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Migration.class).to(WareHouseMigrations.class);

        Multibinder<AppStartup> mb = Multibinder.newSetBinder(binder(), AppStartup.class);
        mb.addBinding().to(CoreAppStartup.class);

    }
}
