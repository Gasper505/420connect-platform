package com.blaze.warehouse.services;

import com.fourtwenty.core.config.ConnectConfiguration;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class WareHouseSwaggerBundle extends SwaggerBundle<ConnectConfiguration> {

    @Override
    protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ConnectConfiguration configuration) {
        return configuration.swaggerBundleConfiguration;
    }
}
