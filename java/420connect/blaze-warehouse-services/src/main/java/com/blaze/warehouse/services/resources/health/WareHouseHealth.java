package com.blaze.warehouse.services.resources.health;

import com.codahale.metrics.health.HealthCheck;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.NamedHealthCheck;

public class WareHouseHealth extends NamedHealthCheck {

    @Override
    public String getName() {
        return "WareHouseHealth";
    }

    @Override
    protected HealthCheck.Result check() throws Exception {
        return HealthCheck.Result.healthy();
    }
}
