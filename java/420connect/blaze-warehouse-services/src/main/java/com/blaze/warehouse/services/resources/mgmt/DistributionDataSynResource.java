package com.blaze.warehouse.services.resources.mgmt;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.*;
import com.warehouse.core.rest.compositerequest.DistributionCompositeSyncRequest;
import com.warehouse.core.rest.invoice.request.CompositeInvoiceUpdateRequest;
import com.warehouse.core.rest.invoice.result.InvoiceInformation;
import com.warehouse.core.result.CompositeErrorResult;
import com.warehouse.core.result.DistributionCompositeSyncResult;
import com.warehouse.core.services.invoice.InvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api("Management - Distribution - Data Sync")
@Path(ResourceConstants.SYNC_URL)
@Produces(MediaType.APPLICATION_JSON)

public class DistributionDataSynResource extends BaseResource {

    @Inject
    private InvoiceService invoiceService;
    @Inject
    private PurchaseOrderService purchaseOrderService;
    @Inject
    private InventoryTransferHistoryService inventoryTransferHistoryService;
    @Inject
    private ProductService productService;
    @Inject
    private InventoryService inventoryService;
    @Inject
    private EmployeeService employeeService;

    @GET
    @Path("/")
    @Timed(name = "bulkGetRequest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get all purchase order,inventory,invoices by date")
    public DistributionCompositeSyncResult bulkGetRequest(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        if (beforeDate <= 0)
            beforeDate = DateTime.now().getMillis();

        DistributionCompositeSyncResult distributionCompositeSyncResult = new DistributionCompositeSyncResult();
        DateSearchResult<PurchaseOrderResult> purchaseOrder = purchaseOrderService.getAllPOByDates(afterDate, beforeDate);
        DateSearchResult<InvoiceInformation> invoices = invoiceService.getInvoiceInformationByDate(afterDate, beforeDate);
        DateSearchResult<InventoryTransferHistoryResult> inventoryTransfers = inventoryTransferHistoryService.getInventoryTransferByDate(afterDate, beforeDate);
        DateSearchResult<Product> productDateSearchResult = productService.getProductsByDates(afterDate, beforeDate);
        DateSearchResult<ProductBatch> productBatchResult = inventoryService.getBatchesWithDates(afterDate, beforeDate);
        SearchResult<EmployeeResult> employeesResult = employeeService.getEmployeeByRole(getToken().getCompanyId());
        distributionCompositeSyncResult.setInventories(inventoryService.getInventoriesPerShop());
        distributionCompositeSyncResult.setInvoice(invoices);
        distributionCompositeSyncResult.setPurchaseOrder(purchaseOrder);
        distributionCompositeSyncResult.setInventoryTransfers(inventoryTransfers);
        distributionCompositeSyncResult.setProduct(productDateSearchResult);
        distributionCompositeSyncResult.setProductBatch(productBatchResult);
        distributionCompositeSyncResult.setEmployees(employeesResult);
        return distributionCompositeSyncResult;

    }

    @PUT
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "compositeBulkUpdate")
    @ApiOperation(value = "Composite bulk updates")
    public CompositeErrorResult compositeBulkUpdate(@Valid DistributionCompositeSyncRequest request) {
        List<InventoryTransferHistory> inventoryTransferRequest = request.getInventoryTransfer();
        List<CompositeInvoiceUpdateRequest> invoiceRequest = request.getInvoice();
        List<PurchaseOrder> purchaseOrder = request.getPurchaseOrder();
        CompositeErrorResult result = new CompositeErrorResult();

        if (inventoryTransferRequest != null && !inventoryTransferRequest.isEmpty()) {
            result.setInventoryTransferError(inventoryTransferHistoryService.compositeAddInventoryTransfer(inventoryTransferRequest));
        }

        if (invoiceRequest != null) {
            result.setInvoiceError(invoiceService.compositeInvoiceRequest(invoiceRequest));
        }

        if (purchaseOrder != null) {
            result.setPoError(purchaseOrderService.compositePOUpdate(purchaseOrder));
        }


        return result;
    }


}
