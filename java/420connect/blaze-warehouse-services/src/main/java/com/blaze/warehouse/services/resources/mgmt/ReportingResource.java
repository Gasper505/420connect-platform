package com.blaze.warehouse.services.resources.mgmt;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.google.common.io.ByteStreams;
import com.warehouse.core.services.reports.DistributionReportingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Api("Management - Distribution - Reporting")
@Path(ResourceConstants.REPORTING_URL)
@Produces(MediaType.APPLICATION_JSON)
public class ReportingResource extends BaseResource {
    @Inject
    private DistributionReportingService reportingService;

    @GET
    @Path("/")
    @Timed(name = "reporting")
    @Secured
    @ApiOperation("Get Reports")
    public Response getReport(@QueryParam("type") String reportType,
                              @QueryParam("startDate") String startDate,
                              @QueryParam("endDate") String endDate,
                              @QueryParam("filter") String filter,
                              @QueryParam("format") String format,
                              @QueryParam("section") ReportTrack.ReportSectionType section) {
        if (StringUtils.isBlank(format)) {
            format = "JSON";
        }

        final Report report = reportingService.getReport(reportType, startDate, endDate, filter, format, section);

        if (format.equalsIgnoreCase("CSV")) {
            StreamingOutput streamingOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    ByteStreams.copy((InputStream) report.getData(), output);
                }
            };
            return Response.ok(streamingOutput, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=\"" + report.getReportName() + ".csv\"").type(report.getContentType()).build();
        } else if (format.equalsIgnoreCase("JSON")) {
            return Response.ok(report.getData(), MediaType.APPLICATION_JSON).build();
        } else if (format.equalsIgnoreCase("PDF")) {
            return null;
        } else {
            return null;
        }
    }

}
