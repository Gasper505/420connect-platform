package com.blaze.warehouse.services.resources.mgmt.batch;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleStatusRequest;
import com.warehouse.core.domain.models.batch.IncomingBatch;
import com.warehouse.core.rest.batch.request.BulkBatchUpdateRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchAddRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchScannedRequest;
import com.warehouse.core.rest.batch.request.IncomingBatchStatusRequest;
import com.warehouse.core.rest.batch.result.IncomingBatchResult;
import com.warehouse.core.services.batch.IncomingBatchService;
import com.warehouse.core.services.batch.TestSampleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Deprecated
@Api("Management - Distribution - Batch")
@Path(ResourceConstants.BATCH_URL)
@Produces(MediaType.APPLICATION_JSON)
public class BatchResource extends BaseResource {
    private IncomingBatchService incomingBatchService;
    private TestSampleService testSampleService;


    @Inject
    public BatchResource(IncomingBatchService incomingBatchService,
                         TestSampleService testSampleService
    ) {
        this.incomingBatchService = incomingBatchService;
        this.testSampleService = testSampleService;

    }

    @Deprecated
    @POST
    @Path("/")
    @Timed(name = "addIncomingBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Incoming Batch")
    public IncomingBatch addIncomingBatch(@Valid IncomingBatchAddRequest request) {
        return incomingBatchService.addInComingBatch(request);
    }

    @Deprecated
    @GET
    @Path("/{incomingBatchId}")
    @Timed(name = "getIncomingBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Incoming Batch")
    public IncomingBatchResult getIncomingBatch(@PathParam("incomingBatchId") String incomingBatchId) {
        return incomingBatchService.getIncomingBatch(incomingBatchId);
    }

    @Deprecated
    @GET
    @Path("/getAllIncomingBatches")
    @Timed(name = "getAllIncomingBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Incoming Batches")
    public SearchResult<IncomingBatchResult> getAllIncomingBatches(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String term) {
        return incomingBatchService.getAllIncomingBatches(start, limit, term);
    }

    @Deprecated
    @POST
    @Path("/{incomingBatchId}/markAsArchived")
    @Timed(name = "markIncomingBatchArchived")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Mark Incoming Batch As Archived")
    public IncomingBatch archiveIncomingBatch(@PathParam("incomingBatchId") String incomingBatchId) {
        return incomingBatchService.archiveIncomingBatch(incomingBatchId);
    }

    @Deprecated
    @GET
    @Path("/list/archived")
    @Timed(name = "getAllArchivedIncomingBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Archived Incoming Batches")
    public SearchResult<IncomingBatchResult> getAllArchivedIncomingBatches(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return incomingBatchService.getAllArchivedIncomingBatches(start, limit);
    }

    @Deprecated
    @PUT
    @Path("/{incomingBatchId}/status")
    @Timed(name = "updateIncomingBatchStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Incoming Batch Status")
    public IncomingBatch updateIncomingBatchStatus(@PathParam("incomingBatchId") String incomingBatchId, @Valid IncomingBatchStatusRequest request) {
        return incomingBatchService.updateIncomingBatchStatus(incomingBatchId, request);
    }

    @Deprecated
    @POST
    @Path("/testsample/")
    @Timed(name = "addTestSample")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Test Sample")
    public TestSample addTestSample(@Valid TestSampleAddRequest request) {
        return testSampleService.addTestSample(request);
    }

    @Deprecated
    @PUT
    @Path("/testsample/{testSampleId}")
    @Timed(name = "updateTestSample")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Test Sample")
    public TestSample updateTestSample(@PathParam("testSampleId") String testSampleId, @Valid TestSample testSample) {
        return testSampleService.updateTestSample(testSampleId, testSample);
    }

    @Deprecated
    @PUT
    @Path("/testsample/{testSampleId}/status")
    @Timed(name = "updateTestSampleStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Test Sample Status")
    public TestSample updateTestSampleStatus(@PathParam("testSampleId") String testSampleId, @Valid TestSampleStatusRequest request) {
        return testSampleService.updateTestSampleStatus(testSampleId, request);
    }

    @Deprecated
    @GET
    @Path("/{incomingBatchId}/testsample/list")
    @Timed(name = "getAllTestSamplesByBatchId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Test Samples By Batch")
    public SearchResult<TestSample> getAllTestSampleByBatchId(@PathParam("incomingBatchId") String incomingBatchId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return testSampleService.getAllTestSamplesByBatchId(incomingBatchId, start, limit);
    }

    @Deprecated
    @PUT
    @Path("/{incomingBatchId}/updateBatch")
    @Timed(name = "updateIncomingBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Incoming Batch")
    public IncomingBatch updateIncomingBatch(@PathParam("incomingBatchId") String incomingBatchId, @Valid IncomingBatch incomingBatch) {
        return incomingBatchService.updateIncomingBatch(incomingBatchId, incomingBatch);
    }

    @Deprecated
    @DELETE
    @Path("/{incomingBatchId}/deleteBatch")
    @Timed(name = "deleteIncomingBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete Incoming Batch")
    public Response deleteIncomingBatch(@PathParam("incomingBatchId") String incomingBatchId) {
        incomingBatchService.deleteIncomingBatch(incomingBatchId);
        return ok();
    }

    @Deprecated
    @GET
    @Path("/incomingBatchesByState")
    @Timed(name = "getAllIncomingBatchesByState")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Incoming Batches By State")
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByState(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("state") boolean state) {
        return incomingBatchService.getAllIncomingBatchesByState(start, limit, state);
    }

    @Deprecated
    @GET
    @Path("/incomingBatchesByStatus")
    @Timed(name = "getAllIncomingBatchesByStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Incoming Batches By Status")
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByStatus(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("status") IncomingBatch.BatchStatus status) {
        return incomingBatchService.getAllIncomingBatchesByStatus(start, limit, status);
    }

    @Deprecated
    @POST
    @Path("/scanningIncomingBatch")
    @Timed(name = "getScannedIncomingBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Scanned Incoming Batch")
    public SearchResult<IncomingBatchResult> getAllScannedIncomingBatch(@Valid IncomingBatchScannedRequest request, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return incomingBatchService.getAllScannedIncomingBatch(request, start, limit);
    }

    @Deprecated
    @GET
    @Path("/list/deletedIncomingBatch")
    @Timed(name = "getAllDeletedIncomingBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Deleted Incoming Batches")
    public SearchResult<IncomingBatchResult> getAllDeletedIncomingBatches(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return incomingBatchService.getAllDeletedIncomingBatch(start, limit);
    }

    @Deprecated
    @PUT
    @Path("/bulkBatchUpdate")
    @Timed(name = "bulkIncomingBatchUpdate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Bulk Incoming Batch Update")
    public Response bulkIncomingBatchUpdate(@Valid BulkBatchUpdateRequest request) {
        incomingBatchService.bulkIncomingBatchUpdate(request);
        return Response.ok().build();
    }

    @Deprecated
    @DELETE
    @Path("/{testSampleId}/deleteTestSample")
    @Timed(name = "deleteTestSampleById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete TestSample By Id")
    public Response deleteTestSample(@PathParam("testSampleId") String testSampleId) {
        testSampleService.deleteTestSampleById(testSampleId);
        return ok();
    }

    @Deprecated
    @PUT
    @Path("/{incomingBatchId}/voidStatus")
    @Timed(name = "updateIncomingBatchVoidStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Incoming Batch Void Status")
    public IncomingBatch updateIncomingBatchVoidStatus(@PathParam("incomingBatchId") String incomingBatchId, @QueryParam("voidStatus") Boolean voidStatus) {
        return incomingBatchService.updateIncomingBatchVoidStatus(incomingBatchId, voidStatus);

    }

    @Deprecated
    @GET
    @Path("/list/voidStatus")
    @Timed(name = "getAllIncomingBatchesByVoidStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Incoming Batches By Void Status")
    public SearchResult<IncomingBatchResult> getAllIncomingBatchesByVoidStatus(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("voidStatus") Boolean voidStatus) {
        return incomingBatchService.getAllIncomingBatchesByVoidStatus(start, limit, voidStatus);
    }
}
