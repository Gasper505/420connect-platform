package com.blaze.warehouse.services.resources.mgmt.dashboard;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.warehouse.core.domain.models.dashboard.DashboardWidget;
import com.warehouse.core.rest.dashboard.DashboardWidgetAddRequest;
import com.warehouse.core.services.dashboard.DashboardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Api("Management - Distribution - Dashboard")
@Path(ResourceConstants.DASHBOARD_URL)
@Produces(MediaType.APPLICATION_JSON)
public class DashboardResource extends BaseResource {

    private DashboardService dashboardService;

    @Inject
    public DashboardResource(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @POST
    @Path("/")
    @Timed(name = "createDashboardWidget")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create New DashboardWidget")
    public DashboardWidget createDashboardWidget(@Valid DashboardWidgetAddRequest request) {
        return dashboardService.addDashboardWidget(request);
    }

    @GET
    @Path("/{dashboardWidgetId}")
    @Timed(name = "getDashboardWidget")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Dashboard Widget By Id")
    public DashboardWidget getDashboardWidgetById(@PathParam("dashboardWidgetId") String dashboardWidgetId) {
        return dashboardService.dashboardWidgetById(dashboardWidgetId);
    }

    @PUT
    @Path("/{dashboardWidgetId}")
    @Timed(name = "updateDashboardWidgetId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update An Dashboard Widget By Id")
    public DashboardWidget updateDashboardWidgetId(@PathParam("dashboardWidgetId") String dashboardWidgetId, @Valid DashboardWidget dashboardWidget) {
        return dashboardService.updateDashboardWidget(dashboardWidgetId, dashboardWidget);
    }

}
