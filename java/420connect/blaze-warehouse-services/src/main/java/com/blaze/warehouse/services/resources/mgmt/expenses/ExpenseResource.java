package com.blaze.warehouse.services.resources.mgmt.expenses;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.comments.UserActivityAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.google.common.io.ByteStreams;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.expenses.ExpensesActivityLog;
import com.warehouse.core.rest.expenses.request.ExpenseBulkRequestUpdate;
import com.warehouse.core.rest.expenses.request.ExpenseEmailRequest;
import com.warehouse.core.rest.expenses.request.ExpensesRequest;
import com.warehouse.core.rest.expenses.result.ExpensesResult;
import com.warehouse.core.rest.invoice.request.InvoiceAttachmentRequest;
import com.warehouse.core.services.expenses.ExpenseActivityLogService;
import com.warehouse.core.services.expenses.ExpensesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Api("Management - Distribution - Expenses")
@Path(ResourceConstants.EXPENSES_URL)
@Produces(MediaType.APPLICATION_JSON)
public class ExpenseResource extends BaseResource {
    private ExpensesService expensesService;
    private ExpenseActivityLogService expenseActivityLogService;
    private UserActivityService userActivityService;

    @Inject
    public ExpenseResource(ExpensesService expensesService,
                           ExpenseActivityLogService expenseActivityLogService,
                           UserActivityService userActivityService) {
        this.expensesService = expensesService;
        this.expenseActivityLogService = expenseActivityLogService;
        this.userActivityService = userActivityService;
    }

    @POST
    @Path("/")
    @Timed(name = "createExpenses")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create New Expense")
    public Expenses createExpenses(@Valid ExpensesRequest expensesRequest) {
        return expensesService.createExpenses(expensesRequest);
    }

    @GET
    @Path("/{expenseId}")
    @Timed(name = "getExpense")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Expense By Id")
    public ExpensesResult getExpenses(@PathParam("expenseId") String expenseId) {
        return expensesService.getExpensesById(expenseId);
    }

    @PUT
    @Path("/{expenseId}")
    @Timed(name = "updateExpense")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Expense By Id")
    public Expenses updateExpenses(@PathParam("expenseId") String expenseId, @Valid Expenses expenses) {
        return expensesService.updateExpenses(expenseId, expenses);
    }

    @DELETE
    @Path("/{expenseId}")
    @Timed(name = "deleteExpense")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete Expense By Id")
    public Response deleteExpenses(@PathParam("expenseId") String expenseId) {
        expensesService.deleteExpensesById(expenseId);
        return ok();
    }

    @GET
    @Path("/")
    @Timed(name = "getAllExpensesByShop")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Expenses By Shop")
    public SearchResult<ExpensesResult> getAllExpensesByShop(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortOption") Expenses.ExpenseSort sortOption, @QueryParam("term") String searchTerm) {
        return expensesService.getAllExpensesByShop(start, limit, sortOption, searchTerm);
    }

    @GET
    @Path("/expensesList")
    @Timed(name = "getAllExpensesByDates")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Expenses By Dates")
    public SearchResult<ExpensesResult> getAllExpensesByDates(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return expensesService.getAllExpensesByDates(afterDate, beforeDate);
    }

    @GET
    @Path("/{customerId}/expensesList")
    @Timed(name = "getAllExpensesByCustomer")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Expenses By Customer Id")
    public SearchResult<ExpensesResult> getAllExpensesByCustomerId(@PathParam("customerId") String customerId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return expensesService.getAllExpensesByCustomerId(customerId, start, limit);
    }

    @GET
    @Path("/{expenseId}/logsList")
    @Timed(name = "getAllExpensesActivityLogs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Expense Activity Logs")
    public SearchResult<ExpensesActivityLog> getAllExpenseActivityLogsByExpenseId(@PathParam("expenseId") String expenseId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return expenseActivityLogService.getAllActivityLogsForExpenses(expenseId, start, limit);
    }

    @POST
    @Path("/{expenseId}/emailToCustomer")
    @Timed(name = "sendExpenseEmailToCustomer")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send Expense Email To Customer ")
    public Response sendExpenseEmailToCustomer(@PathParam("expenseId") String expenseId, @Valid ExpenseEmailRequest request) {
        expensesService.sendExpenseEmailToCustomer(expenseId, request);
        return Response.ok().build();
    }

    @Deprecated
    @POST
    @Path("/{expenseId}/expenseComment")
    @Timed(name = "addExpenseComment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Expense Comment")
    public UserActivity addExpenseComment(@Valid UserActivityAddRequest activityAddRequest) {
        return userActivityService.addUserActivity(activityAddRequest);
    }

    @Deprecated
    @GET
    @Path("/{expenseId}/commentList")
    @Timed(name = "getAllCommentsOfExpense")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Comments Of Expense")
    public SearchResult<UserActivity> getAllCommentsOfExpense(@PathParam("expenseId") String expenseId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return userActivityService.getAllCommentsByReferenceId(expenseId, start, limit);
    }

    @POST
    @Path("/{expenseId}/addAttachment")
    @Timed(name = "addExpenseAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Expense's Attachment")
    public Expenses addExpenseAttachment(@PathParam("expenseId") String expenseId, @Valid InvoiceAttachmentRequest attachmentRequest) {
        return expensesService.addExpenseAttachment(expenseId, attachmentRequest);
    }

    @PUT
    @Path("/{expenseId}/attachment/{attachmentId}")
    @Timed(name = "updateExpenseAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Expense Attachment")
    public Expenses updateExpenseAttachment(@PathParam("expenseId") String expenseId, @PathParam("attachmentId") String attachmentId, @Valid CompanyAsset request) {
        return expensesService.updateExpenseAttachment(expenseId, attachmentId, request);
    }

    @DELETE
    @Path("/{expenseId}/attachment/{attachmentId}")
    @Timed(name = "deleteExpenseAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete Expense Attachment")
    public Response deleteExpenseAttachment(@PathParam("expenseId") String expenseId, @PathParam("attachmentId") String attachmentId) {
        expensesService.deleteExpenseAttachment(expenseId, attachmentId);
        return Response.ok().build();
    }

    @PUT
    @Path("/{expenseId}/markArchive")
    @Timed(name = "markExpenseAsArchived")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Mark Expense As Archive")
    public Expenses markExpenseAsArchived(@PathParam("expenseId") String expenseId) {
        return expensesService.markExpenseAsArchived(expenseId);
    }

    @GET
    @Path("/list/archivedExpenses")
    @Timed(name = "getAllArchivedExpenses")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Archived Expenses")
    public SearchResult<ExpensesResult> getAllArchivedExpenses(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String searchTerm, @QueryParam("sortOption") Expenses.ExpenseSort sortOption) {
        return expensesService.getAllArchivedExpenses(start, limit, searchTerm, sortOption);
    }

    @GET
    @Path("/list/expensesByState")
    @Timed(name = "getAllExpensesByState")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Expenses By State")
    public SearchResult<ExpensesResult> getAllExpensesByState(@QueryParam("state") boolean state, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortOption") Expenses.ExpenseSort sortParam, @QueryParam("term") String searchTerm) {
        return expensesService.getAllExpensesByState(state, start, limit, sortParam, searchTerm);
    }

    @PUT
    @Path("/bulkExpenseUpdate")
    @Secured(requiredShop = true)
    @Timed(name = "bulkExpenseUpdate")
    @ApiOperation(value = "Bulk Expense Update")
    public Response bulkExpenseUpdates(@Valid ExpenseBulkRequestUpdate request) {
        expensesService.bulkExpenseUpdate(request);
        return Response.ok().build();
    }

    @GET
    @Path("/{expenseId}/generatePDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForExpense")
    @ApiOperation(value = "Create PDF For Expense")
    public Response createPdfForExpense(@PathParam("expenseId") String expenseId) {
        InputStream inputStream = expensesService.createPdfForExpense(expenseId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };

            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/activity/comment")
    @Timed(name = "addActivityComment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add comment for log")
    public ExpensesActivityLog addActivityComment(@Valid ActivityCommentRequest request) {
        return expenseActivityLogService.addActivityComment(request);
    }

    @PUT
    @Path("/{expenseId}/updateState")
    @Timed(name = "updateExpenseState")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Expense Active Status")
    public Expenses updateExpenseState(@PathParam("expenseId") String expenseId, @QueryParam("state") boolean state) {
        return expensesService.updateExpenseState(expenseId, state);
    }

}
