package com.blaze.warehouse.services.resources.mgmt.inventory;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductByVendorResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.google.inject.Inject;
import com.warehouse.core.services.inventory.ProductInventoryService;
import com.warehouse.core.services.inventory.ProductStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Api("Management - Distribution - Product")
@Path(ResourceConstants.PRODUCT_URL)
@Produces(MediaType.APPLICATION_JSON)
public class ProductInventoryResource extends BaseResource {

    @Inject
    private ProductInventoryService productInventoryService;

    @GET
    @Path("/list/vendors/{vendorId}")
    @Timed(name = "getProductsByVendorId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Products by vendor id")
    public SearchResult<ProductByVendorResult> getProductsByVendorId(@PathParam("vendorId") String vendorId,
                                                                     @QueryParam("active") boolean active, @QueryParam("countQuantity") boolean countQuantity) {

        return productInventoryService.getProductsByVendorId(vendorId, active, countQuantity);
    }

    @GET
    @Path("/list")
    @Timed(name = "updateProductStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Product Status")
    public Response updateProductStatus(@QueryParam("productIds") List<String> productIds, @QueryParam("status") ProductStatus.Status status) {
        productInventoryService.updateProductStatus(productIds, status);
        return ok();
    }

}
