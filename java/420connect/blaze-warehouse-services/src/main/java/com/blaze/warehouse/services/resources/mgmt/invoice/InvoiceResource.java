package com.blaze.warehouse.services.resources.mgmt.invoice;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.comments.UserActivityAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.google.common.io.ByteStreams;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.InvoiceActivityLog;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.rest.invoice.request.*;
import com.warehouse.core.rest.invoice.result.InvoicePaymentResult;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.invoice.result.OutStandingResult;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import com.warehouse.core.services.invoice.InvoiceActivityLogService;
import com.warehouse.core.services.invoice.InvoiceService;
import com.warehouse.core.services.invoice.ShippingManifestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

@Api("Management - Distribution - Invoices")
@Path(ResourceConstants.INVOICE_URL)
@Produces(MediaType.APPLICATION_JSON)
public class InvoiceResource extends BaseResource {

    private InvoiceService invoiceService;
    private UserActivityService userActivityService;
    private ShippingManifestService shippingManifestService;
    private InvoiceActivityLogService invoiceActivityLogService;

    @Inject
    public InvoiceResource(InvoiceService invoiceService,
                           UserActivityService userActivityService,
                           ShippingManifestService shippingManifestService, InvoiceActivityLogService invoiceActivityLogService) {
        this.invoiceService = invoiceService;
        this.userActivityService = userActivityService;
        this.shippingManifestService = shippingManifestService;
        this.invoiceActivityLogService = invoiceActivityLogService;
    }

    @POST
    @Path("/")
    @Timed(name = "createInvoice")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create New Invoice")
    public Invoice createInvoice(@Valid AddInvoiceRequest request) {
        return invoiceService.createInvoice(request);
    }

    @GET
    @Path("/{invoiceId}")
    @Timed(name = "getInvoice")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get An Invoice By Id")
    public InvoiceResult getInvoiceById(@PathParam("invoiceId") String invoiceId) {
        return invoiceService.getInvoiceById(invoiceId);
    }

    @PUT
    @Path("/{invoiceId}")
    @Timed(name = "updateInvoice")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update An Invoice By Id")
    public Invoice updateInvoiceById(@PathParam("invoiceId") String invoiceId, @Valid Invoice invoice) {
        return invoiceService.updateInvoice(invoiceId, invoice);
    }

    @DELETE
    @Path("/{invoiceId}")
    @Timed(name = "deleteInvoice")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete An Invoice By Id")
    public Response deleteInvoiceById(@PathParam("invoiceId") String invoiceId) {
        invoiceService.deleteInvoiceById(invoiceId);
        return Response.ok().build();
    }

    @GET
    @Path("/{invoiceId}/logsList")
    @Timed(name = "getInvoiceActivityLog")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Invoice Activity Log By invoiceId")
    public SearchResult<InvoiceActivityLog> getInvoiceActivityLog(@PathParam("invoiceId") String invoiceId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return invoiceService.getInvoiceActivityLogByInvoiceId(invoiceId, start, limit);
    }

    @POST
    @Path("/{invoiceId}/addAttachment")
    @Timed(name = "addInvoiceAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Invoice Attachment")
    public Invoice addInvoiceAttachment(@PathParam("invoiceId") String invoiceId, @Valid InvoiceAttachmentRequest request) {
        return invoiceService.addInvoiceAttachment(invoiceId, request);
    }

    @GET
    @Path("/{invoiceId}/attachments/{attachmentId}")
    @Timed(name = "getInvoiceAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Invoice Attachment By Invoice Id And Attachment Id ")
    public CompanyAsset getInvoiceAttachment(@PathParam("invoiceId") String invoiceId, @PathParam("attachmentId") String attachmentId) {
        return invoiceService.getInvoiceAttachment(invoiceId, attachmentId);
    }

    @PUT
    @Path("/{invoiceId}/attachments/{attachmentId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateAttachment")
    @ApiOperation(value = "Update Invoice Attachment")
    public Invoice updateInvoiceAttachment(@PathParam("invoiceId") String invoiceId,
                                           @PathParam("attachmentId") String attachmentId,
                                           @Valid CompanyAsset invoiceAttachmentRequest) {
        return invoiceService.updateAttachment(invoiceId, attachmentId, invoiceAttachmentRequest);
    }

    @DELETE
    @Path("/{invoiceId}/attachments/{attachmentId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteAttachment")
    @ApiOperation(value = "Delete Invoice Attachment")
    public Response deleteAttachment(@PathParam("invoiceId") String invoiceId, @PathParam("attachmentId") String attachmentId) {
        invoiceService.deleteInvoiceAttachment(invoiceId, attachmentId);
        return Response.ok().build();
    }

    @GET
    @Path("/")
    @Timed(name = "getInvoices")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Invoices of Shop")
    public SearchResult<InvoiceResult> getInvoices(@QueryParam("shopId") String shopId, @QueryParam("status") List<Invoice.InvoiceStatus> status,
                                                   @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortOption") Invoice.InvoiceSort sortOption, @QueryParam("term") String searchTerm) {
        return invoiceService.getShopInvoices(shopId, status, start, limit, sortOption, searchTerm);
    }

    @GET
    @Path("/{customerId}/customerInvoices")
    @Timed(name = "getAllCustomerInvoices")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Customer Invoices")
    public SearchResult<InvoiceResult> getAllInvoicesByCustomer(@PathParam("customerId") String customerId, @QueryParam("status") List<Invoice.InvoiceStatus> status,
                                                                @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return invoiceService.getAllInvoiceByCustomer(customerId, status, start, limit);

    }


    @GET
    @Path("/invoices")
    @Timed(name = "syncInvoices")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Sync Invoices")
    public SearchResult<Invoice> syncInvoices(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate, @QueryParam("status") String status) {
        return invoiceService.getCompanyInvoices(afterDate, DateTime.now().getMillis(), status);
    }

    @GET
    @Path("/invoiceHistory")
    @Timed(name = "getInvoiceHistoryByStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Invoice History By Status")
    public SearchResult<Invoice> getInvoiceHistoryByStatus(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("status") String status) {
        return invoiceService.getInvoicesByProductRequestStatus(start, limit, status);
    }

    @POST
    @Path("/{invoiceId}/addInvoicePayment")
    @Timed(name = "addInvoicePayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Invoice Payment")
    public PaymentsReceived addInvoicePayment(@PathParam("invoiceId") String invoiceId, @Valid PaymentsReceivedAddRequest request) {
        return invoiceService.addInvoicePayment(invoiceId, request);
    }

    @GET
    @Path("/getInvoicePayment/{invoicePaymentId}")
    @Timed(name = "getInvoicePayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Invoice Payment")
    public InvoicePaymentResult getInvoicePayment(@PathParam("invoicePaymentId") String invoicePaymentId) {
        return invoiceService.getInvoicePaymentById(invoicePaymentId);
    }

    @GET
    @Path("/{invoiceId}/getAllInvoicePayments")
    @Timed(name = "getAllInvoicePayments")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Invoice Payments")
    public SearchResult<PaymentsReceived> getAllInvoicePayment(@PathParam("invoiceId") String invoiceId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return invoiceService.getAllInvoicePayments(invoiceId, start, limit);
    }

    @PUT
    @Path("/{invoiceId}/status")
    @Timed(name = "updateInvoiceStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Invoice Status")
    public Invoice updateInvoiceStatus(@PathParam("invoiceId") String invoiceId, @Valid InvoiceStatusUpdateRequest updateRequest) {
        return invoiceService.updateInvoiceStatus(invoiceId, updateRequest);
    }

    @Deprecated
    @POST
    @Path("/invoiceComment/comments")
    @Timed(name = "addInvoiceComments")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Invoice Comments")
    public UserActivity addInvoiceComments(@Valid UserActivityAddRequest activityAddRequest) {
        return userActivityService.addUserActivity(activityAddRequest);
    }

    @Deprecated
    @GET
    @Path("/{invoiceId}/getAllCommentsOfInvoice")
    @Timed(name = "getAllCommentsOfInvoice")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Comments Of Invoice")
    public SearchResult<UserActivity> getAllCommentsOfInvoice(@PathParam("invoiceId") String invoiceId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return userActivityService.getAllCommentsByReferenceId(invoiceId, start, limit);
    }

    @POST
    @Path("/{invoiceId}/sendInvoiceToCustomer")
    @Timed(name = "sendEmailToCustomer")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send Invoice To Customer")
    public Response sendEmailToCustomer(@PathParam("invoiceId") String invoiceId, @Valid InvoiceEmailRequest invoiceEmailRequest) {
        invoiceService.sendEmailToCustomer(invoiceId, invoiceEmailRequest);
        return Response.ok().build();
    }

    @POST
    @Path("/invoiceShipping/addShippingManifest")
    @Timed(name = "addShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Shipping Manifest")
    public ShippingManifestResult addShippingManifest(@Valid ShippingManifestAddRequest request) {

        return shippingManifestService.addShippingManifest(request);
    }

    @GET
    @Path("/{invoiceId}/shipping/getAllShippingManifest")
    @Timed(name = "getAllShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Shipping Manifest")
    public SearchResult<ShippingManifestResult> getAllShippingManifest(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return shippingManifestService.getAllShippingManifest(start, limit);
    }

    @PUT
    @Path("bulkinvoiceupdate")
    @Secured(requiredShop = true)
    @Timed(name = "bulkInvoiceUpdate")
    @ApiOperation(value = "Bulk Invoice Update")
    public Response bulkInvoiceUpdates(@Valid InvoiceBulkUpdateRequest request) {
        invoiceService.bulkInvoiceUpdates(request);
        return ok();
    }

    @GET
    @Path("/{customerId}/outStanding")
    @Timed(name = "getTotalOutStanding")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Total Out Standing")
    public OutStandingResult getTotalOutStanding(@PathParam("customerId") String customerId) {
        return invoiceService.getTotalOutStanding(customerId);
    }

    @POST
    @Path("/allInvoices/fetchStatus")
    @Timed(name = "getAllInvoicesByFetchStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Invoices By Fetch Status")
    public SearchResult<InvoiceResult> getAllInvoicesByFetchStatus(@Valid InvoiceFetchStatusRequest request, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortOption") Invoice.InvoiceSort sortOption, @QueryParam("term") String searchTerm) {
        return invoiceService.getALLInvoicesByFetchStatus(request, start, limit, sortOption, searchTerm);
    }

    @GET
    @Path("/{shippingManifestId}/shipping/getShippingManifestById")
    @Timed(name = "getShippingManifestById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Shipping Manifest By Id")
    public ShippingManifestResult getShippingManifestById(@PathParam("shippingManifestId") String shippingManifestId) {
        return shippingManifestService.getShippingManifestById(shippingManifestId);
    }

    @GET
    @Path("/{invoiceId}/shipping/getShippingManifestByInvoiceId")
    @Timed(name = "getShippingManifestByInvoiceId")
    @Secured(requiredShop = true)
    @ApiOperation(value = " Get Shipping Manifest By InvoiceId")
    public SearchResult<ShippingManifestResult> getShippingManifestByInvoiceId(@PathParam("invoiceId") String invoiceId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return shippingManifestService.getShippingManifestByInvoiceId(invoiceId, start, limit);
    }

    @PUT
    @Path("/{shippingManifestId}/updateShippingManifest")
    @Timed(name = "updateShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Shipping Manifest By Id")
    public ShippingManifestResult updateShippingManifest(@PathParam("shippingManifestId") String shippingManifestId, @Valid ShippingManifest shippingManifest) {
        return shippingManifestService.updateShippingManifest(shippingManifestId, shippingManifest);
    }

    @POST
    @Path("/activity/comment")
    @Timed(name = "addActivityComment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add comment for log")
    public InvoiceActivityLog addActivityComment(@Valid ActivityCommentRequest request) {
        return invoiceActivityLogService.addActivityComment(request);
    }

    @DELETE
    @Path("/{shippingManifestId}/deleteShippingManifest")
    @Timed(name = "deleteShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete shipping manifest")
    public Response deleteShippingManifest(@PathParam("shippingManifestId") String shippingManifestId) {
        shippingManifestService.deleteShippingManifest(shippingManifestId);
        return ok();
    }

    @POST
    @Path("/prepareInvoice")
    @Timed(name = "prepareInvoice")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Prepare invoice request")
    public Invoice prepareInvoice(@Valid AddInvoiceRequest invoiceRequest) {
        return invoiceService.prepareInvoice(invoiceRequest);
    }

    @GET
    @Path("/{invoiceId}/generatePDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForInvoice")
    @ApiOperation(value = "Create PDF For Invoice")
    public Response createPdfForInvoice(@PathParam("invoiceId") String invoiceId) {
        InputStream inputStream = invoiceService.createPdfForInvoice(invoiceId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };

            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/shippingManifest/{shippingManifestId}/generatePDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForShippingManifest")
    @ApiOperation(value = "Create PDF For Shipping Manifest")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createPdfForShippingManifest(@PathParam("shippingManifestId") String shippingManifestId,
                                                 @FormDataParam("file") InputStream stream, @FormDataParam("printRoute") boolean printRoute,
                                                 @FormDataParam("isNew") boolean isNew) {
        InputStream inputStream = shippingManifestService.createPdfForShippingManifest(shippingManifestId, stream, printRoute, isNew);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };

            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/shippingManifest/{shippingManifestId}/sendShippingManifestEmail")
    @Timed(name = "sendShippingManifestEmail")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send shipping manifest to customer")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response sendShippingManifestEmail(@PathParam("shippingManifestId") String shippingManifestId,
                                              @FormDataParam("email") String email,
                                              @FormDataParam("file") InputStream stream,
                                              @FormDataParam("printRoute") boolean printRoute) {
        shippingManifestService.sendShippingManifestEmail(shippingManifestId, email, stream, printRoute);
        return Response.ok().build();
    }

    @PUT
    @Path("/shippingManifest/{shippingManifestId}/finalize")
    @Timed(name = "finalizeShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Finalize shipping manifest")
    public ShippingManifestResult finalizeShippingManifest(@PathParam("shippingManifestId") String shippingManifestId) {
        return shippingManifestService.finalizeShippingManifest(shippingManifestId);
    }

    @GET
    @Path("/shippingManifest/{invoiceId}/status")
    @Timed(name = "getShippingManifestByStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get shipping manifest by invoice id and status")
    public SearchResult<ShippingManifestResult> getShippingManifestByStatus(@PathParam("invoiceId") String invoiceId, @QueryParam("status") ShippingManifest.ShippingManifestStatus status, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return shippingManifestService.getAllShippingManifestByStatus(invoiceId, status, start, limit);
    }

    @PUT
    @Path("/{invoiceId}/updatePayment/{paymentId}")
    @Timed(name = "updatePayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update invoice payment")
    public PaymentsReceived updatePayment(@PathParam("invoiceId") String invoiceId, @PathParam("paymentId") String paymentId, @Valid PaymentsReceived request) {
        return invoiceService.updatePayment(invoiceId, paymentId, request);
    }

    @DELETE
    @Path("/{invoiceId}/deletePayment/{paymentId}")
    @Timed(name = "deletePayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete invoice payment")
    public Response deletePayment(@PathParam("invoiceId")String invoiceId, @PathParam("paymentId") String paymentId) {
        invoiceService.deletePayment(invoiceId, paymentId);
        return Response.ok().build();
    }

    @POST
    @Path("/{invoiceId}/complete")
    @Timed(name = "mark")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Complete the invoice")
    public Response completeInvoiceStatus(@PathParam("invoiceId") String invoiceId) {
        invoiceService.completeInvoiceStatus(invoiceId, Boolean.FALSE);
        return ok();
    }

    @POST
    @Path("/{invoiceId}/incomplete")
    @Timed(name = "unmark")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Mark Invoice incomplete")
    public Response inCompleteInvoiceStatus(@PathParam("invoiceId") String invoiceId) {
        invoiceService.completeInvoiceStatus(invoiceId, Boolean.TRUE);
        return ok();
    }

    @PUT
    @Path("/shippingManifest/{shippingManifestId}/unfinalize")
    @Timed(name = "unfinalizeShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Unfinalize shipping manifest")
    public ShippingManifestResult unfinalizeShippingManifest(@PathParam("shippingManifestId") String shippingManifestId) {
        return shippingManifestService.unfinalizeShippingManifest(shippingManifestId);
    }

    @PUT
    @Path("/shippingManifest/{shippingManifestId}/reject")
    @Timed(name = "rejectShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Reject shipping manifest")
    public ShippingManifestResult rejectShippingManifest(@PathParam("shippingManifestId") String shippingManifestId, @Valid ManifestRejectRequest request) {
        return shippingManifestService.rejectShippingManifest(shippingManifestId, request);
    }

    @GET
    @Path("/shippingManifest/{shippingManifestId}/sendToMetrc")
    @Timed(name = "sendManifestToMetrc")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send shipping manifest to metrc")
    public ShippingManifestResult sendToMetrc(@PathParam("shippingManifestId") String shippingManifestId) {
        return shippingManifestService.sendToMetrc(shippingManifestId);
    }
    
    @POST
    @Path("/shippingManifest/{shippingManifestId}/sendToConnectedShop")
    @Timed(name = "sendShippingManifestToConnectedShop")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send Shipping Manifest to connected shop")
    public Response sendShippingManifestToConnectedShop(@PathParam("shippingManifestId") String shippingManifestId){
        shippingManifestService.sendShippingManifestToConnectedStore(shippingManifestId);
        return ok();
    }

    @PUT
    @Path("/shippingManifest/{shippingManifestId}/saveMetrcTemplate")
    @Timed(name = "saveMetrcTemplate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Save shipping manifest to metrc template")
    public ShippingManifestResult saveMetrcTemplate(@PathParam("shippingManifestId") String shippingManifestId, @Valid UpdateInvoiceShipmentRequest request) {
        return shippingManifestService.saveMetrcTemplate(shippingManifestId, request, false);
    }

    @POST
    @Path("/shippingManifest/{shippingManifestId}/submitMetrcTemplate")
    @Timed(name = "submitMetrcTemplate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Submit shipping manifest to metrc template")
    public ShippingManifestResult submitMetrcTemplate(@PathParam("shippingManifestId") String shippingManifestId, @Valid UpdateInvoiceShipmentRequest request) {
        return shippingManifestService.saveMetrcTemplate(shippingManifestId, request, true);
    }

    @GET
    @Path("/shippingManifest/{shippingManifestId}")
    @Timed(name = "getExternalShippingManifestById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Shipping Manifest By Id")
    public ShippingManifestResult getExternalShippingManifestById(@PathParam("shippingManifestId") String shippingManifestId) {
        return shippingManifestService.getShippingManifest(shippingManifestId);
    }
}
