package com.blaze.warehouse.services.resources.mgmt.search;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.warehouse.core.domain.models.expenses.Expenses;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.services.search.DistributionSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Management - Distribution - Search")
@Path(ResourceConstants.SEARCH_URL)
@Produces(MediaType.APPLICATION_JSON)
public class DistributionSearchResource {

    private DistributionSearchService searchService;

    @Inject
    public DistributionSearchResource(DistributionSearchService searchService) {
        this.searchService = searchService;
    }

    @GET
    @Path("/expense")
    @Timed(name = "searchExpenses")
    @Secured
    @ApiOperation("Search Expenses")
    public SearchResult<Expenses> searchExpenses(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("active") Boolean active, @QueryParam("archive") Boolean archive) {
        return searchService.searchExpenses(term, start, limit, active, archive);
    }

    @GET
    @Path("/invoice")
    @Timed(name = "searchInvoices")
    @Secured
    @ApiOperation("Search Invoice")
    public SearchResult<Invoice> searchInvoices(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("active") Boolean active) {
        return searchService.searchInvoices(term, start, limit, active);
    }

}
