package com.blaze.warehouse.services.resources.mgmt.setting;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.google.inject.Inject;
import com.warehouse.core.services.settings.DistributionSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Distribution - Settings")
@Path(ResourceConstants.SETTING_URL)
@Produces(MediaType.APPLICATION_JSON)
public class SettingResource extends BaseResource {

    @Inject
    private DistributionSettingService distributionSettingService;

    @DELETE
    @Path("/adjustment/{adjustmentId}")
    @Timed(name = "deleteAdjustment")
    @Secured(requiredShop = true)
    @Audit(action = "Delete adjustment")
    @ApiOperation(value = "Delete adjustment")
    public Response deleteAdjustment(@PathParam("adjustmentId") String adjustmentId) {
        distributionSettingService.deleteAdjustment(adjustmentId);
        return Response.ok().build();
    }

}
