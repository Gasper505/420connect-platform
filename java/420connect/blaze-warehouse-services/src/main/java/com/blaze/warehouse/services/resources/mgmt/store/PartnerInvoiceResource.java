package com.blaze.warehouse.services.resources.mgmt.store;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.rest.invoice.result.InvoiceResult;
import com.warehouse.core.rest.partner.invoices.PartnerInvoiceAddRequest;
import com.warehouse.core.rest.partner.invoices.PartnerInvoiceUpdateRequest;
import com.warehouse.core.services.store.PartnerInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Store - Invoice")
@Path("/api/v1/partner/invoices")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerInvoiceResource extends BasePartnerDevResource {

    @Inject
    private PartnerInvoiceService storeInvoiceService;

    @Inject
    public PartnerInvoiceResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/")
    @Timed(name = "createInvoice")
    @ApiOperation(value = "Create Invoice")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Invoice addInvoice(@Valid PartnerInvoiceAddRequest request) {
        return storeInvoiceService.createInvoice(request);
    }

    @POST
    @Path("/prepare")
    @Timed(name = "prepareInvoice")
    @ApiOperation(value = "Prepare Invoice")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Invoice prepareInvoice(@Valid PartnerInvoiceAddRequest request) {
        return storeInvoiceService.prepareInvoice(request);
    }

    @PUT
    @Path("/{invoiceId}")
    @Timed(name = "updateInvoice")
    @ApiOperation(value = "Update Invoice")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Invoice updateInvoice(@PathParam("invoiceId") String invoiceId, @Valid PartnerInvoiceUpdateRequest request) {
        return storeInvoiceService.updateInvoice(invoiceId, request);
    }

    @GET
    @Path("/{invoiceId}")
    @Timed(name = "getInvoiceById")
    @ApiOperation(value = "Get Invoice by id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public InvoiceResult getInvoiceById(@PathParam("invoiceId") String invoiceId) {
        return storeInvoiceService.getInvoiceById(invoiceId);
    }


    @GET
    @Path("/list")
    @Timed(name = "getInvoiceList")
    @ApiOperation(value = "Get Invoice list")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<InvoiceResult> getAllInvoices(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storeInvoiceService.getInvoicesByDates(startDate, endDate, start, limit);
    }


}
