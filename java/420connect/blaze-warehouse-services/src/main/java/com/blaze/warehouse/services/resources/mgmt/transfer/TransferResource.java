package com.blaze.warehouse.services.resources.mgmt.transfer;

import com.blaze.warehouse.constants.ResourceConstants;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.warehouse.core.domain.models.transfer.TransferTemplate;
import com.warehouse.core.rest.transfer.request.TransferTemplateAddRequest;
import com.warehouse.core.rest.transfer.result.TransferTemplateResult;
import com.warehouse.core.services.transfer.TransferTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Api("Management-Transfer-Template")
@Path(ResourceConstants.TRANSFER_TEMPLATE_URL)
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource extends BaseResource {
    private TransferTemplateService transferTemplateService;

    @Inject
    public TransferResource(TransferTemplateService transferTemplateService) {
        this.transferTemplateService = transferTemplateService;
    }

    @POST
    @Path("/")
    @Timed(name = "createTransferTemplate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create New Transfer Template")
    public TransferTemplate createTransferTemplate(@Valid TransferTemplateAddRequest request) {
        return transferTemplateService.createTransferTemplate(request);
    }

    @GET
    @Path("/{transferTemplateId}")
    @Timed(name = "getTransferTemplate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get An Transfer Template By Id")
    public TransferTemplateResult getTransferTemplateById(@PathParam("transferTemplateId") String transferTemplateId) {
        return transferTemplateService.getTransferTemplateById(transferTemplateId);
    }

    @PUT
    @Path("/{transferTemplateId}")
    @Timed(name = "updateTransferTemplate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Transfer Template By Id")
    public TransferTemplate updateTransferTemplateById(@PathParam("transferTemplateId") String transferTemplateId, @Valid TransferTemplate transferTemplate) {
        return transferTemplateService.updateTransferTemplate(transferTemplateId, transferTemplate);
    }

    @GET
    @Path("/")
    @Timed(name = "getAllTransferTemplates")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Transfer Templates")
    public SearchResult<TransferTemplateResult> getAllTransferTemplates(@QueryParam("companyId") String companyId, @QueryParam("shopId") String shopId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return transferTemplateService.getAllTransferTemplates(start, limit);
    }

    @DELETE
    @Path("/{transferTemplateId}")
    @Timed(name = "deleteTransferTemplate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete An Transfer Template By Id")
    public Response deleteTransferTemplateById(@PathParam("transferTemplateId") String transferTemplateId) {
        transferTemplateService.deleteTransferTemplateById(transferTemplateId);
        return Response.ok().build();
    }
}
