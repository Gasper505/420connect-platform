package com.fourtwenty.consumer.transactions.subscriber;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.transaction.ConsumerCartEvent;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.store.webhooks.ConsumerOrderData;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

public class PosSubscriber implements BlazeSubscriber {
    private static final Log LOG = LogFactory.getLog(PosSubscriber.class);

    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;

    @Subscribe
    public void subscribe(ConsumerCartEvent event) {
        Transaction transaction = event.getTransaction();

        if (StringUtils.isNotBlank(transaction.getConsumerCartId())) {
            ConsumerCart consumerCart = consumerCartRepository.get(event.getCompanyId(), transaction.getConsumerCartId());
            if (transaction.getCart() != null) {
                consumerCart.setCart(transaction.getCart());
                consumerCartRepository.update(event.getCompanyId(), consumerCart.getId(), consumerCart);
            }
            updateConsumerOrderWebHook(consumerCart, event.getCompanyId(), event.getShopId());
        }

    }

    /**
     * This is private method for update consumer order web hook
     *
     * @param dbConsumerCart
     */
    private void updateConsumerOrderWebHook(ConsumerCart dbConsumerCart, String companyId, String shopId) {
        final ConsumerOrderData consumerOrderData = new ConsumerOrderData();
        StringBuilder memberName = new StringBuilder();
        StringBuilder consumerName = new StringBuilder();

        if (StringUtils.isNotBlank(dbConsumerCart.getConsumerId())) {
            ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getId());
            if (consumerUser != null) {
                consumerName.append(consumerUser.getFirstName())
                        .append((consumerUser.getMiddleName() == null) ? " " : consumerUser.getMiddleName())
                        .append((consumerUser.getLastName() == null) ? "" : consumerUser.getLastName());
            }
        }

        if (StringUtils.isNotBlank(dbConsumerCart.getMemberId())) {
            Member member = memberRepository.getById(dbConsumerCart.getMemberId());
            if (member == null) {
                member = memberRepository.getMemberWithConsumerId(companyId, dbConsumerCart.getConsumerId());
            }
            if (member != null) {
                memberName.append(member.getFirstName())
                        .append((member.getMiddleName() == null) ? " " : member.getMiddleName())
                        .append((member.getLastName() == null) ? "" : member.getLastName());
            }
        }
        consumerOrderData.setConsumerName(consumerName.toString());
        consumerOrderData.setConsumerOrderId(dbConsumerCart.getId());
        consumerOrderData.setConsumerOrderNo(dbConsumerCart.getOrderNo());
        consumerOrderData.setOrderStatus(dbConsumerCart.getCartStatus());
        consumerOrderData.setOrderTime(dbConsumerCart.getOrderPlacedTime());
        consumerOrderData.setMemberName(memberName.toString());
        consumerOrderData.setCart(dbConsumerCart.getCart());
        try {
            partnerWebHookManager.updateConsumerOrderHook(companyId, shopId, consumerOrderData);
        } catch (Exception e) {
            LOG.warn("Update consumer web hook failed for shop id :" + shopId + " and company id:" + companyId, e);
        }
    }
}
