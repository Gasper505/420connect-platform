package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AverageDiscountGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;

    private String[] attrs = new String[]{
            "Average Discount",
            "Total"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public AverageDiscountGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        return prepareAverageDiscount(filter, transactions);
    }

    public GathererReport prepareAverageDiscount(ReportFilter filter, Iterable<Transaction> transactions) {
        GathererReport report = new GathererReport(filter, "Average Discount Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        BigDecimal discount = BigDecimal.ZERO;
        int count = 0;
        BigDecimal factor;
        for (Transaction transaction : transactions) {
            if (transaction.getCart() == null) {
                continue;
            }

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }
            discount = discount.add(transaction.getCart().getTotalDiscount().multiply(factor));

            count++;
        }

        double averageDiscount = (count == 0) ? 0 : discount.doubleValue() / count;

        HashMap<String, Object> data = new HashMap<>();
        data.put(attrs[0], NumberUtils.truncateDecimal(averageDiscount, 2));
        data.put(attrs[1], count);

        report.add(data);
        return report;
    }
}
