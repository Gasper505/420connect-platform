package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class BestSellingProductsGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private String[] attrs = {"Product Name", "Unit Sold", "Percentage"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public BestSellingProductsGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.PERCENTAGE,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        for (Transaction transaction : transactions) {
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));
                    }
                }
            }

        }
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        Set<ObjectId> categoryIds = new HashSet<>();
        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds));

        return prepareBestSellingProducts(filter, transactions, productHashMap, categoryHashMap);

    }

    public GathererReport prepareBestSellingProducts(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> categoryHashMap) {
        GathererReport report = new GathererReport(filter, "Best Selling Products", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Map<String, ProductSale> productQuantityList = new HashMap<>();
        BigDecimal totalQuantity = BigDecimal.ZERO;


        BigDecimal factor;
        for (Transaction transaction : transactions) {

            if (transaction.getCart() != null && transaction.getCart().getItems() == null) {
                continue;
            }

            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }

                factor = BigDecimal.ONE;
                if ((transaction.getTransType() == Transaction.TransactionType.Refund || transaction.getStatus() == Transaction.TransactionStatus.RefundWithInventory) && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                    factor = factor.negate();
                }

                if (transaction.getTransType() == Transaction.TransactionType.Sale
                        || (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded)) {

                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product == null) {
                        continue;
                    }

                    String key = StringUtils.isNotBlank(product.getId()) ? product.getId() : "N/A";
                    ProductSale productDetails = productQuantityList.get(key);

                    if (productDetails == null) {
                        productDetails = new ProductSale();
                        productQuantityList.put(key, productDetails);
                    }

                    BigDecimal productQuantity = (orderItem.getOrigQuantity() == null) ? BigDecimal.ZERO : orderItem.getOrigQuantity().multiply(factor);
                    productDetails.productId = key;
                    productDetails.quantity = productDetails.quantity.add(productQuantity);
                    totalQuantity = totalQuantity.add(productQuantity);
                    productQuantityList.put(key, productDetails);
                }
            }
        }

        if (productQuantityList.size() == 0) {
            return report;
        }

        List<ProductSale> productSales = new ArrayList<>(productQuantityList.values());
        productSales.sort((first, second) -> {
            if (first.quantity.equals(second.quantity)) {
                return 0;
            }
            return second.quantity.compareTo(first.quantity);
        });

        productSales = productSales.subList(0, productSales.size() > 5 ? 5 : productSales.size());

        for (ProductSale sale : productSales) {
            int i = 0;
            HashMap<String, Object> data = new HashMap<>();
            if (productHashMap.get(sale.productId) == null) {
                continue;
            }
            Product product = productHashMap.get(sale.productId);
            ProductCategory category = null;
            if (product != null) {
                category = categoryHashMap.get(product.getId());
            }
            String unitTypeStr = (category != null) ? category.getUnitType().name() : "units";

            data.put(attrs[i++], StringUtils.isNotBlank(product.getName()) ? product.getName() : "N/A");
            data.put(attrs[i++], sale.quantity.setScale(2, RoundingMode.HALF_EVEN) + " " + unitTypeStr);
            data.put(attrs[i], new Percentage(totalQuantity.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : sale.quantity.divide(totalQuantity, 4, RoundingMode.HALF_UP)));

            report.add(data);
        }

        return report;
    }

    public static final class ProductSale {
        public String productId;
        BigDecimal quantity = BigDecimal.ZERO;
    }
}

