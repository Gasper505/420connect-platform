package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CompanySalesByLocationGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ShopRepository shopRepository;

    private String[] attrs = new String[]{
            "Location",
            "Sales",

    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public CompanySalesByLocationGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Iterable<Transaction> transactions = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Set<ObjectId> shopIds = new HashSet<>();
        for (Transaction transaction : transactions) {
            if (StringUtils.isNotBlank(transaction.getShopId()) && ObjectId.isValid(transaction.getShopId())) {
                shopIds.add(new ObjectId(transaction.getShopId()));
            }
        }

        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(shopIds));

        return prepareSalesByLocation(filter, transactions, shopHashMap);

    }

    public GathererReport prepareSalesByLocation(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String, Shop> shopHashMap) {
        GathererReport report = new GathererReport(filter, "Sales by Location", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, BigDecimal> salesByLocationMap = new HashMap<>();
        BigDecimal factor;

        for (Transaction transaction : transactions) {
            Shop shop = shopHashMap.get(transaction.getShopId());
            if (shop == null) {
                continue;
            }

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            if (transaction.getCart() == null || (transaction.getCart() != null && transaction.getCart().getPaymentOption() == Cart.PaymentOption.None)) {
                continue;
            }


            BigDecimal totalSales = transaction.getCart().getTotal() == null ? BigDecimal.ZERO : transaction.getCart().getTotal();
            totalSales = totalSales.multiply(factor);
            String key = StringUtils.isNotBlank(shop.getName()) ? shop.getName() : "N/A";
            salesByLocationMap.put(key, totalSales.add(salesByLocationMap.getOrDefault(key, BigDecimal.ZERO)));
        }

        for (Map.Entry<String, BigDecimal> salesDetails : salesByLocationMap.entrySet()) {
            int i = 0;
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[i++], salesDetails.getKey());
            data.put(attrs[i], NumberUtils.round(salesDetails.getValue(), 2));
            report.add(data);
        }
        return report;
    }
}
