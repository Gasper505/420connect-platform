package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.SalesByProductCategoryGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class DashboardCategorySalesGatherer implements Gatherer {

    @Inject
    private SalesByProductCategoryGatherer salesByProductCategory;

    private String[] attrs = new String[]{
            "Category Name",
            "Gross Sales"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DashboardCategorySalesGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        HashMap<String, SalesByProductCategoryGatherer.CategorySale> categorySaleMap = salesByProductCategory.prepareCategorySaleReport(filter);
        return prepareDashboardCategorySales(filter, categorySaleMap);
    }

    public GathererReport prepareDashboardCategorySales(ReportFilter filter, HashMap<String, SalesByProductCategoryGatherer.CategorySale> categorySaleMap) {
        GathererReport report = new GathererReport(filter, "Sales By Category", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        for (Map.Entry<String, SalesByProductCategoryGatherer.CategorySale> entry : categorySaleMap.entrySet()) {
            SalesByProductCategoryGatherer.CategorySale categorySale = entry.getValue();
            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[0], categorySale.name);
            data.put(attrs[1], new DollarAmount(categorySale.grossReceipt));
            report.add(data);
        }

        report.getData().sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
                DollarAmount d1 = (DollarAmount)map1.getOrDefault("Gross Sales", 0D);
                DollarAmount d2 = (DollarAmount)map2.getOrDefault("Gross Sales", 0D);

                Double amt1=d1.doubleValue(), amt2=d2.doubleValue();
                return amt2.compareTo(amt1);
            }
        });
        return report;
    }
}
