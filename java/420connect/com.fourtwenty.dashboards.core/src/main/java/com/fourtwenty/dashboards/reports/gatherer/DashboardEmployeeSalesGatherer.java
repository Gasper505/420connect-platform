package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.employee.SalesByEmployeeGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardEmployeeSalesGatherer implements Gatherer {

    @Inject
    private SalesByEmployeeGatherer salesByEmployeeGatherer;

    private String[] attrs = new String[]{
            "Employee Name",
            "Visitors",
            "Average Sale",
            "Gross Sales"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DashboardEmployeeSalesGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        List<SalesByEmployeeGatherer.EmployeeSale> budTenderSales = salesByEmployeeGatherer.prepareSalesReport(filter);
        return prepareDashboardEmployeeSales(filter, budTenderSales);
    }

    public GathererReport prepareDashboardEmployeeSales(ReportFilter filter, List<SalesByEmployeeGatherer.EmployeeSale> budTenderSales) {
        GathererReport report = new GathererReport(filter, "Sales By BudTender", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        budTenderSales.sort((o1, o2) -> BigDecimal.valueOf(o2.totalSales).compareTo(BigDecimal.valueOf(o1.totalSales)));

        budTenderSales = budTenderSales.subList(0, budTenderSales.size() < 5 ? budTenderSales.size() : 5);

        for (SalesByEmployeeGatherer.EmployeeSale employeeSale : budTenderSales) {
            StringBuilder name = new StringBuilder();
            name.append(StringUtils.isNotBlank(employeeSale.firstName) ? employeeSale.firstName : "")
                    .append(StringUtils.isNotBlank(employeeSale.lastName) ? " " + employeeSale.lastName : "");

            name = StringUtils.isBlank(name) ? new StringBuilder("N/A") : name;

            HashMap<String, Object> data = new HashMap<>();
            int i = 0;

            double avgAmount = employeeSale.numTransactions == 0 ? 0 : employeeSale.totalSales / employeeSale.numTransactions;

            data.put(attrs[i++], name);
            data.put(attrs[i++], employeeSale.numTransactions);
            data.put(attrs[i++], new DollarAmount(avgAmount));
            data.put(attrs[i], new DollarAmount(employeeSale.totalSales));

            report.add(data);
        }
        return report;
    }
}
