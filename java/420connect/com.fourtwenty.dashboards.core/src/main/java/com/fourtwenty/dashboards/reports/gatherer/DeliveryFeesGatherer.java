package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DeliveryFeesGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;

    private String[] attrs = new String[]{
            "Total Delivery Fees"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DeliveryFeesGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        return prepareDeliveryFees(filter, transactions);
    }

    public GathererReport prepareDeliveryFees(ReportFilter filter, Iterable<Transaction> transactions) {
        GathererReport report = new GathererReport(filter, "Total Delivery Fees Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        BigDecimal deliveryFees = BigDecimal.ZERO;

        for (Transaction transaction : transactions) {
            if (transaction.getCart() != null && transaction.getCart().getDeliveryFee() != null) {
                deliveryFees = deliveryFees.add(transaction.getCart().getDeliveryFee());
            }
        }

        HashMap<String, Object> data = new HashMap<>();
        data.put(attrs[0], NumberUtils.truncateDecimal(deliveryFees.doubleValue(), 2));

        report.add(data);

        return report;
    }
}
