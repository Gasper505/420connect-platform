package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.dashboards.reports.result.EmployeeSalesResult;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EmployeeSalesByCategory implements Gatherer {

    @Inject
    ProductRepository productRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    private String[] attrs = {"Name", "Category Name", "Sales"};

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public EmployeeSalesByCategory() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {


        Set<ObjectId> productCategoryIds = new HashSet<>();

        Iterable<Transaction> transactions;
        if (ReportFilter.ALL_EMPLOYEES.equalsIgnoreCase(filter.getEmployeeId())) {
            transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        } else {
            transactions = transactionRepository.getBracketSalesByEmployee(filter.getCompanyId(), filter.getShopId(), filter.getEmployeeId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        if (transactions != null) {
            for (Iterator<Transaction> iterator = transactions.iterator(); iterator.hasNext(); ) {
                Transaction transaction = iterator.next();
                if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                    continue;
                }
                List<OrderItem> items = transaction.getCart().getItems();
                for (int i = 0; i < items.size(); i++) {
                    OrderItem item = items.get(i);
                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                        continue;
                    }

                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));
                    }
                }
                if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()))
                    employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
                if (StringUtils.isNotBlank(transaction.getSellerId()))
                    employeeIds.add(new ObjectId(transaction.getSellerId()));
            }
        }

        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productCategoryIds));
        return prepareEmployeeSalesByCategory(filter, transactions, employeeHashMap, productHashMap, productCategoryMap);


    }

    private BigDecimal getCalculateTax(OrderItem item, BigDecimal propCartDiscount) {
        BigDecimal postTax, postALExciseTax, postNALExciseTax, totalCityTax, totalStateTax, totalFedTax;

        postTax = BigDecimal.ZERO;

        BigDecimal subTotalAfterDiscount = item.getFinalPrice().subtract(propCartDiscount);
        if (item.getTaxResult() != null) {
            postALExciseTax = item.getTaxResult().getTotalALPostExciseTax();
            postNALExciseTax = item.getTaxResult().getTotalExciseTax();

            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                postTax = postTax.add(item.getTaxResult().getTotalPostCalcTax().add(postALExciseTax).add(postNALExciseTax));
            }
        }
        if (postTax.doubleValue() == 0 && ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null)) {
            TaxInfo taxInfo = item.getTaxInfo();
            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                totalCityTax = subTotalAfterDiscount.multiply(taxInfo.getCityTax());
                totalStateTax = subTotalAfterDiscount.multiply(taxInfo.getStateTax());
                totalFedTax = subTotalAfterDiscount.multiply(taxInfo.getFederalTax());

                postTax = totalCityTax.add(totalStateTax).add(totalFedTax);
            }
        }

        if (postTax.doubleValue() == 0) {
            postTax = item.getCalcTax(); // - item.getCalcPreTax().doubleValue();
        }
        return postTax;
    }

    public GathererReport prepareEmployeeSalesByCategory(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String, Employee> employeeHashMap, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> productCategoryMap) {
        GathererReport report = new GathererReport(filter, "Employee By Category", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, EmployeeSalesResult> resultMap = new HashMap<>();
        HashMap<String, Double> propRatioMap = new HashMap<>();


        BigDecimal factor;
        if (transactions != null) {
            for (Transaction transaction : transactions) {

                Employee employee = employeeHashMap.get(transaction.getSellerId());

                if (employee == null || transaction.getCart() == null) {
                    continue;
                }

                resultMap.putIfAbsent(transaction.getSellerId(), new EmployeeSalesResult());

                EmployeeSalesResult employeeSalesResult = resultMap.get(transaction.getSellerId());

                employeeSalesResult.setEmployeeId(transaction.getSellerId());

                StringBuilder employeeName = new StringBuilder();


                employeeName.append(employee.getFirstName().trim())
                        .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName().trim() : "");

                employeeSalesResult.setEmployeeName(employeeName.toString());


                factor = BigDecimal.ONE;
                if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                    factor = factor.negate();
                }


                double total = 0.0;
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (orderItem.getProduct() != null && orderItem.getProduct().isDiscountable()) {
                        total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                    }
                }

                // Calculate the ratio to be applied
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product == null) {
                        continue;
                    }
                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    propRatioMap.put(orderItem.getId(), 1d); // 100 %
                    if (orderItem.getProduct() != null && product.isDiscountable() && total > 0) {
                        double finalCost = orderItem.getFinalPrice().doubleValue();
                        double ratio = finalCost / total;

                        propRatioMap.put(orderItem.getId(), ratio);
                    }
                }


                for (OrderItem item : transaction.getCart().getItems()) {
                    Product product = productHashMap.get(item.getProductId());

                    if (product == null) {
                        continue;
                    }

                    ProductCategory category = productCategoryMap.get(product.getCategoryId());

                    BigDecimal ratio = BigDecimal.ONE;
                    if (propRatioMap.containsKey(item.getId())) {
                        ratio = BigDecimal.valueOf(propRatioMap.getOrDefault(item.getId(), 1D));
                    }

                    BigDecimal propDeliveryFee = transaction.getCart().getDeliveryFee() != null ? transaction.getCart().getDeliveryFee().multiply(ratio) : BigDecimal.ZERO;
                    BigDecimal propCartDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().multiply(ratio) : BigDecimal.ZERO;
                    BigDecimal propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().multiply(ratio) : BigDecimal.ZERO;
                    BigDecimal propCCFee = transaction.getCart().getCreditCardFee() != null ? transaction.getCart().getCreditCardFee().multiply(ratio) : BigDecimal.ZERO;

                    // calculated postTax

                    BigDecimal totalTax = getCalculateTax(item, propCartDiscount);

                    BigDecimal gross = item.getFinalPrice().subtract(propCartDiscount).add(propDeliveryFee).add(totalTax).add(propCCFee).subtract(propAfterTaxDiscount);
                    gross = gross.multiply(factor);

                    employeeSalesResult.setUnitsSold(employeeSalesResult.getUnitsSold().add(item.getQuantity().multiply(factor)));
                    employeeSalesResult.setSales(employeeSalesResult.getSales().add(gross));

                    if (category != null) {
                        EmployeeSalesResult.SoldProductType soldProductType = null;
                        for (EmployeeSalesResult.SoldProductType productType : employeeSalesResult.getProductTypes()) {
                            if (category.getId().equals(productType.getCategoryId())) {
                                soldProductType = productType;
                            }
                        }
                        if (soldProductType == null) {
                            soldProductType = new EmployeeSalesResult.SoldProductType();
                            employeeSalesResult.getProductTypes().add(soldProductType);
                        }

                        soldProductType.setSales(soldProductType.getSales().add(gross));
                        soldProductType.setCategoryName(category.getName());
                        soldProductType.setCategoryId(product.getCategoryId());
                    }


                }
            }
        }

        for (Map.Entry<String, EmployeeSalesResult> entry : resultMap.entrySet()) {
            if (StringUtils.isBlank(entry.getKey()) && entry.getValue().getProductTypes().size() == 0) {
                continue;
            }
            for (EmployeeSalesResult.SoldProductType productType : entry.getValue().getProductTypes()) {
                HashMap<String, Object> data = new HashMap<>();
                EmployeeSalesResult employeeSales = resultMap.get(entry.getKey());
                int i = 0;
                data.put(attrs[i], employeeSales.getEmployeeName());
                data.put(attrs[++i], productType.getCategoryName());
                data.put(attrs[++i], productType.getSales());
                report.add(data);
            }
        }
        return report;
    }

}