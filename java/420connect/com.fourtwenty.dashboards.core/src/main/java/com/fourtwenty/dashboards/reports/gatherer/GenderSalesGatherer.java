package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;


public class GenderSalesGatherer implements Gatherer {
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    MemberRepository memberRepository;

    private String[] attrs = {"Gender", "Total Sales"};

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public GenderSalesGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        LinkedHashSet<ObjectId> memberIds = new LinkedHashSet<>();

        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(memberIds));
        return prepareGenderSales(filter, results, memberHashMap);
    }

    public GathererReport prepareGenderSales(ReportFilter filter, Iterable<Transaction> results, HashMap<String, Member> memberHashMap) {
        GathererReport report = new GathererReport(filter, "Sales by Gender", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, BigDecimal> memberData = new HashMap<>();
        int factor = 1;
        BigDecimal totalSales = BigDecimal.ZERO;

        for (Transaction transaction : results) {
            Member member = memberHashMap.get(transaction.getMemberId());
            if (member == null || transaction.getCart() == null) {
                continue;
            }

            factor = 1;

            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            BigDecimal memberTotalSales = transaction.getCart().getTotal() == null ? BigDecimal.ZERO : transaction.getCart().getTotal();
            memberTotalSales = BigDecimal.valueOf(memberTotalSales.doubleValue() * factor);

            totalSales = totalSales.add(memberTotalSales);

            memberData.put(member.getSex().toString(), memberTotalSales.add(memberData.getOrDefault(member.getSex().toString(), BigDecimal.ZERO)));

        }

        for (Map.Entry<String, BigDecimal> memberDetails : memberData.entrySet()) {
            int i = 0;
            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[i++], memberDetails.getKey());
            data.put(attrs[i], new Percentage(totalSales.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : memberDetails.getValue().divide(totalSales, 4, RoundingMode.HALF_UP)));
            report.add(data);

        }

        return report;
    }

}
