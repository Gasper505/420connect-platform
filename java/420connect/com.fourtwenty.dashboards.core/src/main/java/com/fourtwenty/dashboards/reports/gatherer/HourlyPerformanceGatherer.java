package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class HourlyPerformanceGatherer implements Gatherer {
    private static final Logger logger = LoggerFactory.getLogger(HourlyPerformanceGatherer.class);

    @Inject
    private TransactionRepository transactionRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public HourlyPerformanceGatherer() {
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> results;
        if(filter.getType() == ReportType.COMPANY_SALES_BY_HOUR_DASHBOARD){
         results = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(),
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        } else {
            results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }

        return prepareSalesByHour(filter, results);

    }

    public GathererReport prepareSalesByHour(ReportFilter filter, Iterable<Transaction> results) {
        HashMap<String, HashMap<Integer, HourPerformanceStats>> hourSaleMap = new HashMap<>();
        List<Integer> hourSet = new ArrayList<>();
        for (int i = 0; i<24; i++) {
            if (i == 0) {
                reportHeaders.add("Detail");
                fieldTypes.put("Detail", GathererReport.FieldType.STRING);
            }
            hourSet.add(i);
            reportHeaders.add(String.valueOf(i));
            fieldTypes.put(String.valueOf(i), GathererReport.FieldType.CURRENCY);
        }
        GathererReport report = new GathererReport(filter, "Hourly Performance Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        BigDecimal factor;
        for (Transaction transaction : results) {
            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            DateTime processedTime;
            String key;
            DateTime dateTime = new DateTime(transaction.getProcessedTime()).minusMinutes(filter.getTimezoneOffset());
            DateTime endDate = new DateTime(filter.getTimeZoneEndDateMillis()).withTimeAtStartOfDay();
            if (dateTime.withTimeAtStartOfDay().isEqual(endDate.getMillis())) {
                processedTime = dateTime;
                key = "Today";
            } else {
                processedTime = dateTime;
                key = "Average Sale";
            }
            logger.info("Key : "+ key +"  Processed Time:"+ processedTime +"  End Time :"+ new DateTime(filter.getTimeZoneEndDateMillis()));
            int hourOfDay = processedTime.getHourOfDay();

            hourSaleMap.putIfAbsent(key, new HashMap<>());

            HashMap<Integer, HourPerformanceStats> hoursSalesStats = hourSaleMap.get(key);
            hoursSalesStats.putIfAbsent(hourOfDay, new HourPerformanceStats());

            HourPerformanceStats hourPerformanceStats = hoursSalesStats.get(hourOfDay);
            if (transaction.getCart() != null) {
                hourPerformanceStats.sales = hourPerformanceStats.sales.add(transaction.getCart().getTotal().multiply(factor));
                hourPerformanceStats.count++;
            }

            hourPerformanceStats.hour = hourOfDay;
        }

        for (String key : Lists.newArrayList("Today", "Average Sale")) {
            HashMap<Integer, HourPerformanceStats> statsHashMap = hourSaleMap.getOrDefault(key, new HashMap<>());
            if(statsHashMap.size() == 0){
                continue;
            }
            HashMap<String ,Object> data = new HashMap<>();
            data.put("Detail", key);
            boolean isToday = key.equalsIgnoreCase("Today");

            for (Integer hour : hourSet) {
                HourPerformanceStats value = statsHashMap.getOrDefault(hour, new HourPerformanceStats());
                BigDecimal sale = value.sales;
                if (!isToday) {
                    sale = value.count != 0 ? sale.divide(BigDecimal.valueOf(value.count), RoundingMode.HALF_EVEN) : BigDecimal.ZERO;
                }
                data.put(String.valueOf(hour), NumberUtils.round(sale));
            }
            report.add(data);
        }
        return report;
    }

    private class HourPerformanceStats {
        int hour;
        BigDecimal sales = BigDecimal.ZERO;
        int count = 0;
    }
}
