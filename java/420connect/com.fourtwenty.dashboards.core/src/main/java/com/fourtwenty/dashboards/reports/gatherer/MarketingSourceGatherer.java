package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;


public class MarketingSourceGatherer implements Gatherer {
    @Inject
    MemberRepository memberRepository;
    @Inject
    private TransactionRepository transactionRepository;
    private ArrayList<String> reportHeaders = new ArrayList<>();

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = {"Name", "Sales", "Percentage"};


    public MarketingSourceGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.PERCENTAGE
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        LinkedHashSet<ObjectId> memberIds = new LinkedHashSet<>();

        for (Transaction transaction : transactions) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
        }
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(memberIds));
        return prepareMarketingSource(filter, transactions, memberHashMap);
    }

    public GathererReport prepareMarketingSource(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String, Member> memberHashMap) {
        GathererReport report = new GathererReport(filter, "Marketing Source", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, BigDecimal> marketingSource = new HashMap<>();


        BigDecimal totalSales = BigDecimal.ZERO;
        BigDecimal marketingSale;
        BigDecimal factor;
        for (Transaction transaction : transactions) {

            Member member = memberHashMap.get(transaction.getMemberId());

            if (member == null || transaction.getCart() == null) {
                continue;
            }

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            marketingSale = transaction.getCart().getTotal() == null ? BigDecimal.ZERO : transaction.getCart().getTotal();

            marketingSale = marketingSale.multiply(factor);
            totalSales = totalSales.add(marketingSale);

            String memberSource = member.getMarketingSource() == null || StringUtils.isEmpty(member.getMarketingSource()) ? "N/A" : member.getMarketingSource();

            marketingSource.put(memberSource, marketingSale.add(marketingSource.getOrDefault(memberSource, BigDecimal.ZERO)));
        }

        for (Map.Entry<String, BigDecimal> sourceDetails : marketingSource.entrySet()) {
            if (sourceDetails.getValue() == BigDecimal.ZERO) {
                continue;
            }
            HashMap<String, Object> data = new HashMap<>();
            int i = 0;
            data.put(attrs[i++], sourceDetails.getKey());
            data.put(attrs[i++], sourceDetails.getValue());
            data.put(attrs[i], new Percentage(totalSales.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : sourceDetails.getValue().divide(totalSales, 4, RoundingMode.HALF_UP)));
            report.add(data);
        }

        report.getData().sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
                BigDecimal d1 = (BigDecimal) map1.getOrDefault("Sales", BigDecimal.ZERO);
                BigDecimal d2 = (BigDecimal) map2.getOrDefault("Sales", BigDecimal.ZERO);
                return d2.compareTo(d1);
            }
        });
        List<HashMap<String, Object>> reportData = report.getData().subList(0, report.getData().size() > 10 ? 10 : report.getData().size());

        report.setData(reportData);
        return report;

    }
}
