package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NewMembersByTypeGatherer implements Gatherer {
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    MemberRepository memberRepository;

    private String[] attrs = {"Type", "Percentage"};

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public NewMembersByTypeGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.PERCENTAGE
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Iterable<Member> newMembers = memberRepository.getMembersWithStartDate(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        return prepareNewMembersByType(filter, newMembers);
    }

    public GathererReport prepareNewMembersByType(ReportFilter filter, Iterable<Member> newMembers) {
        GathererReport report = new GathererReport(filter, "New Members By Type", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, Integer> membersMap = new HashMap<>();
        int total = 0;


        for (Member member : newMembers) {

            if (StringUtils.isNotBlank(member.getConsumerType().getDisplayName())) {
                int countConsumer = membersMap.getOrDefault(member.getConsumerType().getDisplayName(), 0);
                total++;
                membersMap.put(member.getConsumerType().getDisplayName(), ++countConsumer);

            }


        }

        for (Map.Entry<String, Integer> memberDetails : membersMap.entrySet()) {
            int i = 0;
            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[i++], memberDetails.getKey());

            BigDecimal value = BigDecimal.valueOf(total == 0 ? 0 : (memberDetails.getValue() * 100 / total));
            data.put(attrs[i], NumberUtils.round(value, 2));
            report.add(data);
        }
        return report;

    }
}
