package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class OverviewGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;

    private String[] attrs = new String[]{
            "Discounts",
            "Sales",
            "Average Sale",
            "Transaction Count",
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public OverviewGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        return prepareOverview(filter, transactions);
    }

    public GathererReport prepareOverview(ReportFilter filter, Iterable<Transaction> transactions) {
        GathererReport report = new GathererReport(filter, "Overview Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        BigDecimal discount = BigDecimal.ZERO;
        BigDecimal sales = BigDecimal.ZERO;
        int count = 0;
        BigDecimal factor;

        for (Transaction transaction : transactions) {
            if (transaction.getCart() == null) {
                continue;
            }

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }
            BigDecimal totalDiscount = transaction.getCart().getTotalDiscount();
            BigDecimal total = transaction.getCart().getTotal();

            totalDiscount = (totalDiscount == null) ? BigDecimal.ZERO : totalDiscount;

            discount = discount.add(totalDiscount.multiply(factor));
            sales = sales.add(total.multiply(factor));

            count++;
        }

        BigDecimal averageSales = count == 0 ? BigDecimal.ZERO : sales.divide(BigDecimal.valueOf(count), RoundingMode.HALF_EVEN);

        HashMap<String, Object> data = new HashMap<>();
        data.put(attrs[0], NumberUtils.truncateDecimal(discount.doubleValue(), 2));
        data.put(attrs[1], NumberUtils.truncateDecimal(sales.doubleValue(), 2));
        data.put(attrs[2], NumberUtils.truncateDecimal(averageSales.doubleValue(), 2));
        data.put(attrs[3], count);

        report.add(data);

        return report;
    }
}
