package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class SalesByAgeGroupGatherer implements Gatherer {
    @Inject
    MemberRepository memberRepository;
    @Inject
    private TransactionRepository transactionRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = {"Age Group", "Total Sales"};


    public SalesByAgeGroupGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> transactions;

        if (filter.getType() == ReportType.COMPANY_SALES_BY_AGE_DASHBOARD) {
             transactions = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        } else {
            transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }

        LinkedHashSet<ObjectId> memberIds = new LinkedHashSet<>();
        for (Transaction transaction : transactions) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }

        }
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(memberIds));
        return prepareSalesByAgeGroup(filter, transactions, memberHashMap);
    }

    public GathererReport prepareSalesByAgeGroup(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String, Member> memberHashMap) {
        GathererReport report = new GathererReport(filter, "Sales By Age Group", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, BigDecimal> memberData = new HashMap<>();
        BigDecimal factor;

        for (Transaction transaction : transactions) {

            Member member = memberHashMap.get(transaction.getMemberId());

            if (member == null || transaction.getCart() == null || member.getDob() == null) {
                continue;
            }

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            BigDecimal totalSales = transaction.getCart().getTotal() == null ? BigDecimal.ZERO : transaction.getCart().getTotal();

            int memberAge = DateUtil.getYearsBetweenTwoDates(member.getDob(), DateTime.now().getMillis());

            totalSales = totalSales.multiply(factor);

            memberData.put(getAgeGroup(memberAge), totalSales.add(memberData.getOrDefault(getAgeGroup(memberAge), BigDecimal.ZERO)));
        }

        for (Map.Entry<String, BigDecimal> entry : memberData.entrySet()) {
            if (entry.getValue() == BigDecimal.ZERO) {
                continue;
            }
            HashMap<String, Object> data = new HashMap<>();
            int i = 0;

            data.put(attrs[i], entry.getKey());
            data.put(attrs[++i], entry.getValue());

            report.add(data);
        }
        return report;
    }

    private String getAgeGroup(int ageGoup) {
        if (ageGoup >= 1 && ageGoup <= 10) {
            return "1-10";
        } else if (ageGoup >= 11 && ageGoup <= 20) {
            return "12-20";
        } else if (ageGoup >= 21 && ageGoup <= 30) {
            return "21-30";
        } else if (ageGoup >= 31 && ageGoup <= 40) {
            return "31-40";
        } else if (ageGoup >= 41 && ageGoup <= 50) {
            return "41-50";
        } else if (ageGoup >= 51 && ageGoup <= 60) {
            return "51-60";
        } else if (ageGoup >= 61 && ageGoup <= 70) {
            return "70";
        } else if (ageGoup > 70) {
            return "70+";
        }
        return String.valueOf(ageGoup);
    }
}
