package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.SalesByBrand;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SalesByBrandGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;

    private String[] attrs = new String[]{
            "Brand Name",
            "Sales",
            "Sales Percent"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByBrandGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.PERCENTAGE
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Iterable<Transaction> transactions;
        if(filter.getType() == ReportType.COMPANY_SALES_BY_BRAND_DASHBOARD) {
          transactions = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }else {
            transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }

        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        for (Transaction transaction : transactions) {


            if (transaction.getCart() == null || StringUtils.isBlank(transaction.getAssignedEmployeeId()) || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                productIds.add(new ObjectId(item.getProductId()));
            }
        }
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(brandIds));
        return prepareSalesByBrand(filter, transactions, productHashMap, brandHashMap);
    }

    public GathererReport prepareSalesByBrand(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String, Product> productHashMap, HashMap<String, Brand> brandHashMap) {
        GathererReport report = new GathererReport(filter, "Sales by brand", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        BigDecimal factor;
        BigDecimal totalSales = BigDecimal.ZERO;


        for (Transaction transaction : transactions) {
            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            totalSales = totalSales.add(transaction.getCart().getTotal().multiply(factor));

            if (transaction.getCart() == null || StringUtils.isBlank(transaction.getAssignedEmployeeId()) || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }
            }
        }
        HashMap<String, Double> propRatioMap;
        Product product;
        Brand brand;
        BigDecimal propCartDiscount;
        BigDecimal propDeliveryFee;
        BigDecimal propCCFee;
        BigDecimal propAfterTaxDiscount;
        BigDecimal finalCost;
        double ratio;
        BigDecimal gross, totalTax, postTax, postALExciseTax, postNALExciseTax, totalCityTax, totalStateTax, totalFedTax, subTotalAfterDiscount;

        HashMap<String, SalesByBrand> salesByBrandMap = new HashMap<>();

        for (Transaction transaction : transactions) {

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            // Calculate discounts proportionally so we can apply taxes

            propRatioMap = new HashMap<>();

            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            double total = 0.0;
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                product = productHashMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                product = productHashMap.get(orderItem.getProductId());
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product != null && product.isDiscountable() && total > 0) {
                    finalCost = orderItem.getFinalPrice();
                    ratio = finalCost.doubleValue() / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }

            }

            for (OrderItem item : transaction.getCart().getItems()) {

                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                brand = brandHashMap.get(product.getBrandId());

                if (brand == null) {
                    continue;
                }

                ratio = 1D;
                if (propRatioMap.containsKey(item.getId())) {
                    ratio = propRatioMap.get(item.getId());
                }

                propCartDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().multiply(BigDecimal.valueOf(ratio)) : BigDecimal.ZERO;
                propDeliveryFee = transaction.getCart().getDeliveryFee() != null ? transaction.getCart().getDeliveryFee().multiply(BigDecimal.valueOf(ratio)) : BigDecimal.ZERO;
                propCCFee = transaction.getCart().getCreditCardFee() != null ? transaction.getCart().getCreditCardFee().multiply(BigDecimal.valueOf(ratio)) : BigDecimal.ZERO;
                propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().multiply(BigDecimal.valueOf(ratio)) : BigDecimal.ZERO;

                // calculated postTax
                postTax = BigDecimal.ZERO;

                subTotalAfterDiscount = item.getFinalPrice().subtract(propCartDiscount);
                if (item.getTaxResult() != null) {
                    postNALExciseTax = item.getTaxResult().getTotalExciseTax();
                    postALExciseTax = item.getTaxResult().getTotalALPostExciseTax();

                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        postTax = postTax.add(item.getTaxResult().getTotalPostCalcTax().add(postALExciseTax).add(postNALExciseTax));
                    }
                }
                if (postTax.doubleValue() == 0 && ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null)) {
                    TaxInfo taxInfo = item.getTaxInfo();
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        totalStateTax = subTotalAfterDiscount.multiply(taxInfo.getStateTax());
                        totalCityTax = subTotalAfterDiscount.multiply(taxInfo.getCityTax());
                        totalFedTax = subTotalAfterDiscount.multiply(taxInfo.getFederalTax());

                        postTax = totalCityTax.add(totalStateTax).add(totalFedTax);
                    }
                }

                if (postTax.doubleValue() == 0) {
                    postTax = item.getCalcTax();
                }

                totalTax = postTax;

                gross = item.getFinalPrice().subtract(propCartDiscount).add(propDeliveryFee).add(totalTax).add(propCCFee).subtract(propAfterTaxDiscount);

                gross = gross.multiply(factor);

                SalesByBrand salesByBrand = salesByBrandMap.getOrDefault(brand.getId(), new SalesByBrand());
                salesByBrand.setBrandId(brand.getId());
                salesByBrand.setBrandName(brand.getName());
                salesByBrand.setSales(salesByBrand.getSales().add(gross));

                salesByBrandMap.put(brand.getId(), salesByBrand);

            }
        }

        for (Map.Entry<String, SalesByBrand> entry : salesByBrandMap.entrySet()) {
            SalesByBrand value = entry.getValue();
            HashMap<String, Object> data = new HashMap<>();
            int i = 0;
            data.put(attrs[i++], value.getBrandName());
            data.put(attrs[i++], NumberUtils.round(value.getSales(), 2));
            data.put(attrs[i], NumberUtils.round(totalSales.doubleValue() == 0 ? 0 : value.getSales().divide(totalSales, RoundingMode.HALF_UP).doubleValue() * 100, 2));
            report.add(data);
        }

        report.getData().sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
                BigDecimal d1 = BigDecimal.valueOf((Double) map1.getOrDefault(attrs[1], BigDecimal.ZERO));
                BigDecimal d2 = BigDecimal.valueOf((Double) map2.getOrDefault(attrs[1], BigDecimal.ZERO));
                return d2.compareTo(d1);
            }
        });

        return report;
    }
}
