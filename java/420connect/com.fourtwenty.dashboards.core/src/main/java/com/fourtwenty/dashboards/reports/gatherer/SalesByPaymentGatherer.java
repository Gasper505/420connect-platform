package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class SalesByPaymentGatherer implements Gatherer {
    @Inject
    TransactionRepository transactionRepository;

    private String[] attrs = {"Payment Type", "Transactions", "Total Sales"};

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByPaymentGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Iterable<Transaction> results;
        if(filter.getType() == ReportType.COMPANY_SALES_BY_PAYMENT_DASHBOARD){
             results = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        } else {
           results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }

        return prepareSalesByPayment(filter, results);
    }

    public GathererReport prepareSalesByPayment(ReportFilter filter, Iterable<Transaction> results) {
        GathererReport report = new GathererReport(filter, "Sales by Payment Type", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, PaymentDetails> paymentData = new HashMap<>();
        int factor = 1;
        for (Transaction transaction : results) {
            factor = 1;

            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            if (transaction.getCart() == null || (transaction.getCart() != null && transaction.getCart().getPaymentOption() == Cart.PaymentOption.None)) {
                continue;
            }

            BigDecimal paymentTotalSales = transaction.getCart().getTotal() == null ? BigDecimal.ZERO : transaction.getCart().getTotal();

            paymentTotalSales = BigDecimal.valueOf(paymentTotalSales.doubleValue() * factor);

            String key = "N/A";
            if (StringUtils.isNotBlank(transaction.getCart().getPaymentOption().name())) {
                key = transaction.getCart().getPaymentOption().name();
            }

            PaymentDetails paymentDetails = paymentData.get(key);

            if (paymentDetails == null) {
                paymentDetails = new PaymentDetails();
                paymentData.put(key, paymentDetails);
            }

            paymentDetails.paymentType = key;
            paymentDetails.sales = paymentDetails.sales.add(paymentTotalSales);
            paymentDetails.count += (factor > 0) ? 1 : 0;

            paymentData.put(key, paymentDetails);
        }

        for (String key : paymentData.keySet()) {
            HashMap<String, Object> data = new HashMap<>();

            PaymentDetails paymentDetail = paymentData.get(key);
            int i = 0;
            data.put(attrs[i++], paymentDetail.paymentType);
            data.put(attrs[i++], paymentDetail.count);
            data.put(attrs[i], paymentDetail.sales);

            report.add(data);
        }
        return report;
    }

    public static final class PaymentDetails {
        BigDecimal sales = BigDecimal.ZERO;
        int count;
        private String paymentType;
    }

}
