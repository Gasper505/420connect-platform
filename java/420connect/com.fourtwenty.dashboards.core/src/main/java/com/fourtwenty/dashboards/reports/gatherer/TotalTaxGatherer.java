package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TotalTaxGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;

    private String[] attrs = new String[]{
            "Tax"
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TotalTaxGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.CURRENCY,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        return prepareTotalTax(filter, results);
    }

    public GathererReport prepareTotalTax(ReportFilter filter, Iterable<Transaction> results) {
        String reportName = "";
        if (filter.getType() == ReportType.EXCISE_TAX_DASHBOARD) reportName = "Excise Tax";
        else if (filter.getType() == ReportType.CITY_TAX_DASHBOARD) reportName = "City Tax";
        else if (filter.getType() == ReportType.COUNTY_TAX_DASHBOARD) reportName = "County Tax";
        else if (filter.getType() == ReportType.STATE_TAX_DASHBOARD) reportName = "State Tax";
        GathererReport report = new GathererReport(filter, reportName, reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Object> data = new HashMap<>();
        BigDecimal factor;
        BigDecimal totalTax = BigDecimal.ZERO;

        for (Transaction ts : results) {
            factor = BigDecimal.ONE;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart() != null && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }
            if (ts.getCart() != null && ts.getCart().getTaxResult() != null) {
                if (filter.getType() == ReportType.EXCISE_TAX_DASHBOARD) {
                    BigDecimal exciseTax = ts.getCart().getTaxResult().getTotalALPostExciseTax().add(ts.getCart().getTaxResult().getTotalExciseTax()).add(ts.getCart().getTaxResult().getTotalNALPreExciseTax()).add(ts.getCart().getTaxResult().getTotalALExciseTax()).multiply(factor);
                    totalTax = totalTax.add(exciseTax);
                } else if (filter.getType() == ReportType.CITY_TAX_DASHBOARD) {
                    BigDecimal cityTax = ts.getCart().getTaxResult().getTotalCityTax().add(ts.getCart().getTaxResult().getTotalCityPreTax()).multiply(factor);
                    totalTax = totalTax.add(cityTax);
                } else if (filter.getType() == ReportType.COUNTY_TAX_DASHBOARD) {
                    BigDecimal countyTax = ts.getCart().getTaxResult().getTotalCountyTax().add(ts.getCart().getTaxResult().getTotalCountyPreTax()).multiply(factor);
                    totalTax = totalTax.add(countyTax);
                } else if (filter.getType() == ReportType.STATE_TAX_DASHBOARD) {
                    BigDecimal stateTax = ts.getCart().getTaxResult().getTotalStateTax().add(ts.getCart().getTaxResult().getTotalStatePreTax()).multiply(factor);
                    totalTax = totalTax.add(stateTax);
                }

            }

        }
        data.put(attrs[0], new DollarAmount(totalTax));
        report.add(data);
        return report;
    }
}
