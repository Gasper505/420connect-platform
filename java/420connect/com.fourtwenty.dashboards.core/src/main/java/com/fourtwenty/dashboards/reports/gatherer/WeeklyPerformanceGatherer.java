package com.fourtwenty.dashboards.reports.gatherer;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class WeeklyPerformanceGatherer implements Gatherer {

    private static final Logger logger = LoggerFactory.getLogger(WeeklyPerformanceGatherer.class);

    @Inject
    private TransactionRepository transactionRepository;
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = {"Week", "Current Week Sale", "Previous Week Sale"};

    public WeeklyPerformanceGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        long startTimeZoneMillies = new DateTime(filter.getTimezoneOffset()).minusDays(15).getMillis();
        long endTimeZoneMiiles = new DateTime().minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusDays(1).minusSeconds(1).plusMinutes(filter.getTimezoneOffset()).getMillis();
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), startTimeZoneMillies, endTimeZoneMiiles);
        return prepareWeeklyPerformance(filter, transactions);
    }


    class WeekTotalSales {
        BigDecimal currentWeekSale = BigDecimal.ZERO;
        BigDecimal previousWeekSale = BigDecimal.ZERO;
    }

    public GathererReport prepareWeeklyPerformance(ReportFilter filter, Iterable<Transaction> transactions) {
        GathererReport report = new GathererReport(filter, "Weekly Performance", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, WeekTotalSales> weekData = new HashMap<>();


        BigDecimal transactionTotalSales;
        int factor = 1;

        for (Transaction transaction : transactions) {
            factor = 1;

            if (transaction.getCart() == null) {
                continue;
            }

            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            transactionTotalSales = transaction.getCart().getTotal() == null ? BigDecimal.ZERO : transaction.getCart().getTotal();
            transactionTotalSales = BigDecimal.valueOf(transactionTotalSales.doubleValue() * factor);

            long key = new DateTime(transaction.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();
            String day = ProcessorUtil.dayString(key);


            weekData.putIfAbsent(day, new WeekTotalSales());

            WeekTotalSales weekTotalSales = weekData.get(day);
            int currentWeek = isCurrentWeek(transaction.getProcessedTime(), filter);
            if (currentWeek == 0) {
                logger.info(currentWeek+","+transaction.getTransNo()+","+transactionTotalSales+","+ProcessorUtil.dateString(key)+","+ProcessorUtil.dayString(key) +","+","+key);
                weekTotalSales.currentWeekSale = weekTotalSales.currentWeekSale.add(transactionTotalSales);

            } else if (currentWeek == 1) {
                logger.info(currentWeek+","+transaction.getTransNo()+","+transactionTotalSales+","+ProcessorUtil.dateString(key)+","+ProcessorUtil.dayString(key) +","+","+key);
                weekTotalSales.previousWeekSale = weekTotalSales.previousWeekSale.add(transactionTotalSales);
            }
        }
        for (Map.Entry<String, WeekTotalSales> weekSales : weekData.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            WeekTotalSales weekTotalSales = weekSales.getValue();
            int i = 0;
            data.put(attrs[i++], weekSales.getKey());
            data.put(attrs[i++], weekTotalSales.currentWeekSale);
            data.put(attrs[i], weekTotalSales.previousWeekSale);
            report.add(data);
        }
        return report;
    }

    private int isCurrentWeek(long millies, ReportFilter filter) {
        DateTime currentWeekStart = new DateTime().plusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().minusMinutes(filter.getTimezoneOffset()).weekOfWeekyear().roundFloorCopy();
        DateTime lastWeekStart = new DateTime().plusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().minusMinutes(filter.getTimezoneOffset()).weekOfWeekyear().roundFloorCopy().minusDays(7);

        DateTime date = new DateTime(millies).plusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().minusMinutes(filter.getTimezoneOffset());

        if(date.isAfter(currentWeekStart) || DateUtil.getStandardDaysBetweenTwoDates(currentWeekStart.getMillis(), date.getMillis()) == 0) {
            return 0;
        } else if (date.isAfter(lastWeekStart) || DateUtil.getStandardDaysBetweenTwoDates(lastWeekStart.getMillis(), date.getMillis()) == 0) {
            return 1;
        }
        return -1;
    }
}
