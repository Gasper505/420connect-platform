package com.fourtwenty.dashboards.reports.gatherermanager;

import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.GathererManager;
import com.fourtwenty.core.reporting.gather.impl.inventory.LowInventoryGatherer;
import com.fourtwenty.dashboards.reports.gatherer.AverageDiscountGatherer;
import com.fourtwenty.dashboards.reports.gatherer.AverageOrderGatherer;
import com.fourtwenty.dashboards.reports.gatherer.BestSellingProductsGatherer;
import com.fourtwenty.dashboards.reports.gatherer.CompanySalesByLocationGatherer;
import com.fourtwenty.dashboards.reports.gatherer.CompanyTotalSalesGatherer;
import com.fourtwenty.dashboards.reports.gatherer.CreditCardFeesGatherer;
import com.fourtwenty.dashboards.reports.gatherer.DashboardCategorySalesGatherer;
import com.fourtwenty.dashboards.reports.gatherer.DashboardEmployeeSalesGatherer;
import com.fourtwenty.dashboards.reports.gatherer.DeliveryFeesGatherer;
import com.fourtwenty.dashboards.reports.gatherer.EmployeeSalesByCategory;
import com.fourtwenty.dashboards.reports.gatherer.GenderSalesGatherer;
import com.fourtwenty.dashboards.reports.gatherer.HourlyPerformanceGatherer;
import com.fourtwenty.dashboards.reports.gatherer.MarketingSourceGatherer;
import com.fourtwenty.dashboards.reports.gatherer.NewMembersByTypeGatherer;
import com.fourtwenty.dashboards.reports.gatherer.OverviewGatherer;
import com.fourtwenty.dashboards.reports.gatherer.SalesByAgeGroupGatherer;
import com.fourtwenty.dashboards.reports.gatherer.SalesByBrandGatherer;
import com.fourtwenty.dashboards.reports.gatherer.SalesByPaymentGatherer;
import com.fourtwenty.dashboards.reports.gatherer.TotalTaxGatherer;
import com.fourtwenty.dashboards.reports.gatherer.WeeklyPerformanceGatherer;
import com.google.inject.Injector;

import javax.inject.Inject;

public class DashboardGathererManager extends GathererManager {

    @Inject
    protected Injector injector;

    public DashboardGathererManager() {
    }

    @Override
    public Gatherer getGathererForReportType(ReportType type) {
        Gatherer g;

        switch (type) {
            case OVERVIEW_DASHBOARD:
                g = injector.getInstance(OverviewGatherer.class);
                break;
            case AVERAGE_ORDER_DASHBOARD:
                g = injector.getInstance(AverageOrderGatherer.class);
                break;
            case AVERAGE_DISCOUNT_DASHBOARD:
                g = injector.getInstance(AverageDiscountGatherer.class);
                break;
            case CREDIT_CARD_FEE_DASHBOARD:
                g = injector.getInstance(CreditCardFeesGatherer.class);
                break;
            case DELIVERY_FEE_DASHBOARD:
                g = injector.getInstance(DeliveryFeesGatherer.class);
                break;
            case CATEGORY_SALES_DASHBOARD:
                g = injector.getInstance(DashboardCategorySalesGatherer.class);
                break;
            case EMPLOYEE_SALES_DASHBOARD:
                g = injector.getInstance(DashboardEmployeeSalesGatherer.class);
                break;
            case BRAND_SALES_DASHBOARD:
                g = injector.getInstance(SalesByBrandGatherer.class);
                break;
            case EXCISE_TAX_DASHBOARD:
                g = injector.getInstance(TotalTaxGatherer.class);
                break;
            case CITY_TAX_DASHBOARD:
                g = injector.getInstance(TotalTaxGatherer.class);
                break;
            case STATE_TAX_DASHBOARD:
                g = injector.getInstance(TotalTaxGatherer.class);
                break;
            case COUNTY_TAX_DASHBOARD:
                g = injector.getInstance(TotalTaxGatherer.class);
                break;
            case GENDER_SALES_DASHBOARD:
                g = injector.getInstance(GenderSalesGatherer.class);
                break;
            case BEST_SELLING_PRODUCTS_DASHBOARD:
                g = injector.getInstance(BestSellingProductsGatherer.class);
                break;
            case NEW_MEMBERS_BY_TYPE_DASHBOARD:
                g = injector.getInstance(NewMembersByTypeGatherer.class);
                break;
            case SALES_BY_PAYMENT_TYPE_DASHBOARD:
                g = injector.getInstance(SalesByPaymentGatherer.class);
                break;
            case SALES_BY_AGE_GROUP_DASHBOARD:
                g = injector.getInstance(SalesByAgeGroupGatherer.class);
                break;
            case MARKETING_SOURCE_DASHBOARD:
                g = injector.getInstance(MarketingSourceGatherer.class);
                break;
            case WEEKLY_PERFORMANCE_DASHBOARD:
                g = injector.getInstance(WeeklyPerformanceGatherer.class);
                break;
            case EMPLOYEE_SALES_BY_CATEGORY_DASHBOARD:
                g = injector.getInstance(EmployeeSalesByCategory.class);
                break;
            case LOW_INVENTORY_DASHBOARD:
                g = injector.getInstance(LowInventoryGatherer.class);
                break;
            case HOURLY_PERFORMANCE_DASHBOARD:
                g = injector.getInstance(HourlyPerformanceGatherer.class);
                break;
            case COMPANY_SALES_BY_AGE_DASHBOARD:
                g = injector.getInstance(SalesByAgeGroupGatherer.class);
                break;
            case COMPANY_SALES_BY_PAYMENT_DASHBOARD:
                g = injector.getInstance(SalesByPaymentGatherer.class);
                break;
            case COMPANY_SALES_BY_BRAND_DASHBOARD:
                g = injector.getInstance(SalesByBrandGatherer.class);
                break;
            case COMPANY_SALES_BY_HOUR_DASHBOARD:
                g = injector.getInstance(HourlyPerformanceGatherer.class);
                break;
            case COMPANY_TOTAL_SALES_DASHBOARD:
                g = injector.getInstance(CompanyTotalSalesGatherer.class);
                break;
            case COMPANY_SALES_BY_LOCATION_DASHBOARD:
                g = injector.getInstance(CompanySalesByLocationGatherer.class);
                break;
            default:
                g = super.getGathererForReportType(type);
                break;
        }

        return g;

    }
}
