package com.fourtwenty.dashboards.reports.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fourtwenty.core.domain.models.company.ShopLimitedView;
import com.fourtwenty.core.reporting.model.DailySummary;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DailySummaryResult  {
    private DailySummary dailySummary;
    private ShopLimitedView shopLimitedView;

    public DailySummary getDailySummary() {
        return dailySummary;
    }

    public void setDailySummary(DailySummary dailySummary) {
        this.dailySummary = dailySummary;
    }

    public ShopLimitedView getShopLimitedView() {
        return shopLimitedView;
    }

    public void setShopLimitedView(ShopLimitedView shopLimitedView) {
        this.shopLimitedView = shopLimitedView;
    }


}

