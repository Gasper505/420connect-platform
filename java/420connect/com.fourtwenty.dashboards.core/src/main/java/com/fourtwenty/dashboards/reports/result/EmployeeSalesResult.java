package com.fourtwenty.dashboards.reports.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseProduct;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeSalesResult {
    private String employeeId;
    private String employeeName;
    private List<PurchaseProduct> products = new ArrayList<>();
    private List<SoldProductType> productTypes = new ArrayList<>();
    private String avgTransactionLength;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitsSold = BigDecimal.ZERO;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal sales = BigDecimal.ZERO;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal avgSale = BigDecimal.ZERO;
    private long totalVisits;
    @DecimalMin("0")
    private BigDecimal totalDiscount = BigDecimal.ZERO;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public List<SoldProductType> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<SoldProductType> productTypes) {
        this.productTypes = productTypes;
    }

    public String getAvgTransactionLength() {
        return avgTransactionLength;
    }

    public void setAvgTransactionLength(String avgTransactionLength) {
        this.avgTransactionLength = avgTransactionLength;
    }

    public BigDecimal getUnitsSold() {
        return unitsSold;
    }

    public void setUnitsSold(BigDecimal unitsSold) {
        this.unitsSold = unitsSold;
    }

    public BigDecimal getSales() {
        return sales;
    }

    public void setSales(BigDecimal sales) {
        this.sales = sales;
    }

    public BigDecimal getAvgSale() {
        return avgSale;
    }

    public void setAvgSale(BigDecimal avgSale) {
        this.avgSale = avgSale;
    }

    public long getTotalVisits() {
        return totalVisits;
    }

    public void setTotalVisits(long totalVisits) {
        this.totalVisits = totalVisits;
    }

    public List<PurchaseProduct> getProducts() {
        return products;
    }

    public void setProducts(List<PurchaseProduct> products) {
        this.products = products;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public static class SoldProductType {
        private String categoryId;
        private String categoryName;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal percent = BigDecimal.ZERO;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal sales = BigDecimal.ZERO;

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public BigDecimal getPercent() {
            return percent;
        }

        public void setPercent(BigDecimal percent) {
            this.percent = percent;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public BigDecimal getSales() {
            return sales;
        }

        public void setSales(BigDecimal sales) {
            this.sales = sales;
        }
    }
}
