package com.fourtwenty.dashboards.reports.result;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.SalesByBrand;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickGlanceResult {
    private List<SalesByConsumerType> salesByConsumer = new ArrayList<>();
    private List<SalesByBrand> salesByBrands = new ArrayList<>();
    private MemberVisits memberVisits = new MemberVisits();
    private List<SalesByType> salesByType = new ArrayList<>();
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal totalSales = BigDecimal.ZERO;

    public List<SalesByBrand> getSalesByBrands() {
        return salesByBrands;
    }

    public void setSalesByBrands(List<SalesByBrand> salesByBrands) {
        this.salesByBrands = salesByBrands;
    }

    public MemberVisits getMemberVisits() {
        return memberVisits;
    }

    public void setMemberVisits(MemberVisits memberVisits) {
        this.memberVisits = memberVisits;
    }

    public List<SalesByType> getSalesByType() {
        return salesByType;
    }

    public void setSalesByType(List<SalesByType> salesByType) {
        this.salesByType = salesByType;
    }

    public List<SalesByConsumerType> getSalesByConsumer() {
        return salesByConsumer;
    }

    public void setSalesByConsumer(List<SalesByConsumerType> salesByConsumer) {
        this.salesByConsumer = salesByConsumer;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public static class SalesByConsumerType {
        private ConsumerType consumerType;
        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        private BigDecimal sales = BigDecimal.ZERO;
        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        private BigDecimal salesPercent = BigDecimal.ZERO;
        private long count;

        public ConsumerType getConsumerType() {
            return consumerType;
        }

        public void setConsumerType(ConsumerType consumerType) {
            this.consumerType = consumerType;
        }

        public BigDecimal getSales() {
            return sales;
        }

        public void setSales(BigDecimal sales) {
            this.sales = sales;
        }

        public BigDecimal getSalesPercent() {
            return salesPercent;
        }

        public void setSalesPercent(BigDecimal salesPercent) {
            this.salesPercent = salesPercent;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }
    }

    public static class MemberVisits {
        private int newMemberVisits;
        private int totalVisits;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal newMemberSales = BigDecimal.ZERO;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal totalSales = BigDecimal.ZERO;

        private int refundVisits;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal refundSales = BigDecimal.ZERO;

        public int getNewMemberVisits() {
            return newMemberVisits;
        }

        public void setNewMemberVisits(int newMemberVisits) {
            this.newMemberVisits = newMemberVisits;
        }

        public int getTotalVisits() {
            return totalVisits;
        }

        public void setTotalVisits(int totalVisits) {
            this.totalVisits = totalVisits;
        }

        public BigDecimal getNewMemberSales() {
            return newMemberSales;
        }

        public void setNewMemberSales(BigDecimal newMemberSales) {
            this.newMemberSales = newMemberSales;
        }

        public BigDecimal getTotalSales() {
            return totalSales;
        }

        public void setTotalSales(BigDecimal totalSales) {
            this.totalSales = totalSales;
        }

        public int getRefundVisits() {
            return refundVisits;
        }

        public void setRefundVisits(int refundVisits) {
            this.refundVisits = refundVisits;
        }

        public BigDecimal getRefundSales() {
            return refundSales;
        }

        public void setRefundSales(BigDecimal refundSales) {
            this.refundSales = refundSales;
        }

    }

    public static class SalesByType {
        private String salesType;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal grossSales = BigDecimal.ZERO;
        private long count;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal percent = BigDecimal.ZERO;

        public String getSalesType() {
            return salesType;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public BigDecimal getGrossSales() {
            return grossSales;
        }

        public void setGrossSales(BigDecimal grossSales) {
            this.grossSales = grossSales;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public BigDecimal getPercent() {
            return percent;
        }

        public void setPercent(BigDecimal percent) {
            this.percent = percent;
        }
    }

}

