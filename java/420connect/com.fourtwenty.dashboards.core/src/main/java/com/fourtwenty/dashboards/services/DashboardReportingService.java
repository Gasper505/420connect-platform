package com.fourtwenty.dashboards.services;

import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.dashboards.reports.result.DailySummaryResult;
import com.fourtwenty.dashboards.reports.result.EmployeeSalesResult;
import com.fourtwenty.dashboards.reports.result.QuickGlanceResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public interface DashboardReportingService {

    Report getReport(String reportType, String startDate, String endDate, String filter, String format, ReportTrack.ReportSectionType section);

    QuickGlanceResult getQuickGlanceReport(String startDate, String endDate);

    ArrayList<EmployeeSalesResult> getSalesByEmployeeReport(String employeeId, String startDate, String endDate);

    LinkedHashMap<String, Object> getAllReports(String startDate, String endDate, String reportTypes, int timezoneOffset);

    DailySummaryResult getDailySummaryReport(String startDate);

    HashMap<String, Object> getAllCompanyReports(String startDate, String endDate, String reportTypes, int timezoneOffset);
}

