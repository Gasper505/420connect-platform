package com.fourtwenty.dashboards.services.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.ShopLimitedView;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalLocationRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.company.CompanySalesByProductCategoryGatherer;
import com.fourtwenty.core.reporting.gather.impl.dashboard.SalesByCannabisTypeGatherer;
import com.fourtwenty.core.reporting.gather.impl.employee.MileageReportByEmployeeGatherer;
import com.fourtwenty.core.reporting.gather.impl.employee.SalesByEmployeeGatherer;
import com.fourtwenty.core.reporting.gather.impl.inventory.LowInventoryGatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.AverageTransactionTimeGatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.DailySummaryGatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.SalesByProductCategoryGatherer;
import com.fourtwenty.core.reporting.model.DailySummary;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.FormatProcessor;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseProduct;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.SalesByBrand;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.DeliveryService;
import com.fourtwenty.core.services.mgmt.QueuePOSService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.dashboards.reports.gatherer.*;
import com.fourtwenty.dashboards.reports.gatherermanager.DashboardGathererManager;
import com.fourtwenty.dashboards.reports.result.DailySummaryResult;
import com.fourtwenty.dashboards.reports.result.EmployeeSalesResult;
import com.fourtwenty.dashboards.reports.result.QuickGlanceResult;
import com.fourtwenty.dashboards.services.DashboardReportingService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.warehouse.core.domain.models.dashboard.DashboardWidget;
import com.warehouse.core.rest.dashboard.DashboardWidgetAddRequest;
import com.warehouse.core.services.dashboard.DashboardService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class DashboardReportingServiceImpl extends AbstractAuthServiceImpl implements DashboardReportingService {
    private static final String REPORTING = "ReportService";

    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardReportingServiceImpl.class);

    @Inject
    private FormatProcessor formatProcessor;
    @Inject
    private DashboardGathererManager gathererManager;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private AverageDiscountGatherer averageDiscountGatherer;
    @Inject
    private AverageOrderGatherer averageOrderGatherer;
    @Inject
    private CreditCardFeesGatherer creditCardFeesGatherer;
    @Inject
    private DashboardCategorySalesGatherer dashboardCategorySalesGatherer;
    @Inject
    private SalesByProductCategoryGatherer salesByProductCategory;
    @Inject
    private DashboardEmployeeSalesGatherer dashboardEmployeeSalesGatherer;
    @Inject
    private SalesByEmployeeGatherer salesByEmployeeGatherer;
    @Inject
    private DeliveryFeesGatherer deliveryFeesGatherer;
    @Inject
    private TotalTaxGatherer totalTaxGatherer;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private AverageTransactionTimeGatherer averageTransactionTimeGatherer;
    @Inject
    private BestSellingProductsGatherer bestSellingProductsGatherer;
    @Inject
    private GenderSalesGatherer genderSalesGatherer;
    @Inject
    private MarketingSourceGatherer marketingSourceGatherer;
    @Inject
    private SalesByAgeGroupGatherer salesByAgeGroupGatherer;
    @Inject
    private SalesByPaymentGatherer salesByPaymentGatherer;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private SalesByBrandGatherer salesByBrandGatherer;
    @Inject
    private QueuePOSService queuePOSService;
    @Inject
    private TerminalLocationRepository terminalLocationRepository;
    @Inject
    private DeliveryService deliveryService;
    @Inject
    private WeeklyPerformanceGatherer weeklyPerformanceGatherer;
    @Inject
    private NewMembersByTypeGatherer newMembersByTypeGatherer;
    @Inject
    private EmployeeSalesByCategory employeeSalesByCategory;
    @Inject
    private LowInventoryGatherer lowInventoryGatherer;
    @Inject
    private DashboardService dashboardService;
    @Inject
    private DailySummaryGatherer dailySummaryGatherer;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private HourlyPerformanceGatherer hourlyPerformanceGatherer;
    @Inject
    private CompanyTotalSalesGatherer companyTotalSalesGatherer;
    @Inject
    private CompanySalesByLocationGatherer companySalesByLocationGatherer;
    @Inject
    private CompanySalesByProductCategoryGatherer companySalesByProductCategoryGatherer;

    @Inject
    private OverviewGatherer overviewGatherer;

    @Inject
    public DashboardReportingServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public Report getReport(String typeString, String startDate, String endDate, String filterString, String formatString, ReportTrack.ReportSectionType section) {
        if (StringUtils.isBlank(typeString)) {
            throw new BlazeInvalidArgException(REPORTING, "Argument Not Found - Report Type");
        }

        ReportType type = ReportType.getType(typeString);
        if (type == ReportType.DEFAULT) {
            throw new BlazeInvalidArgException(REPORTING, "Invalid Argument - Report Type");
        }

        FormatProcessor.ReportFormat format = FormatProcessor.ReportFormat.getType(formatString);
        if (format == FormatProcessor.ReportFormat.DEFAULT) {
            throw new BlazeInvalidArgException(REPORTING, "Invalid Argument - Report Format");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        //Build the filter object to be used to sort the data we want
        ReportFilter filter = new ReportFilter(shop, type, token, filterString, startDate, endDate);

        Gatherer gatherer = gathererManager.getGathererForReportType(type);
        if (gatherer == null) {
            throw new BlazeInvalidArgException(REPORTING, "Gatherer not available.");
        }
        GathererReport report = gatherer.gather(filter);
        if (report == null) {
            throw new BlazeInvalidArgException("Report", "Report not available.");
        }

        return formatProcessor.process(format, report);
    }

    public QuickGlanceResult getQuickGlanceReport(String startDateStr, String endDateStr) {
        Shop shop = shopRepository.getById(token.getShopId());
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(token.getRequestTimeZone());
        if (shop != null) {
            timeZoneOffset = shop.getTimezoneOffsetInMinutes()*-1;
        }
        long startDate = getDateByTimeZone(startDateStr, false, timeZoneOffset);
        long endDate = getDateByTimeZone(endDateStr, true, timeZoneOffset);

        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Iterable<Transaction> transactions = transactionRepository.getAllBracketSales(token.getCompanyId(), token.getShopId(), startDate, endDate);
        Iterable<Member> newMembers = memberRepository.getMembersWithStartDate(token.getCompanyId(), startDate, endDate);
        for (Transaction transaction : transactions) {
            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        return prepareQuickGalanceResult(transactions, newMembers, productHashMap, brandHashMap);

    }

    @Override
    public ArrayList<EmployeeSalesResult> getSalesByEmployeeReport(String employeeId, String startDateStr, String endDateStr) {
        Iterable<Transaction> transactions;
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> productCategoryIds = new HashSet<>();
        Set<ObjectId> employeeList = new HashSet<>();

        Shop shop = shopRepository.getById(token.getShopId());
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(token.getRequestTimeZone());
        if (shop != null) {
            timeZoneOffset = shop.getTimezoneOffsetInMinutes()*-1;
        }
        long startDate = getDateByTimeZone(startDateStr, false, timeZoneOffset);
        long endDate = getDateByTimeZone(endDateStr, true, timeZoneOffset);

        employeeId = (StringUtils.isBlank(employeeId)) ? token.getActiveTopUser().getUserId() : employeeId;

        transactions = transactionRepository.getBracketSalesByEmployee(token.getCompanyId(), token.getShopId(), employeeId, startDate, endDate);


        for (Transaction transaction : transactions) {
            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
            }
            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()))
                employeeList.add(new ObjectId(transaction.getAssignedEmployeeId()));
        }

        if (employeeList.isEmpty()) {
            return Lists.newArrayList();
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeList));

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productCategoryIds));
        return prepareSalesByEmployeeReport(employeeId, transactions, employeeMap, productHashMap, productCategoryMap, shop);
    }

    @Override
    public LinkedHashMap<String, Object> getAllReports(String startDate, String endDate, String reqReportTypesStr, int timeZoneOffset) {
        Shop shop = shopRepository.getById(token.getShopId());
        List<String> reqReportTypes = new ArrayList<>();
        if (StringUtils.isNotBlank(reqReportTypesStr)) {
            reqReportTypes = (Arrays.asList(StringUtils.split(reqReportTypesStr, "\\s*,\\s*")));

        }
        timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(token.getRequestTimeZone());
        if (shop != null) {
            timeZoneOffset = shop.getTimezoneOffsetInMinutes()*-1;
        }
        long startDateTimeZone = getDateByTimeZone(startDate, false, timeZoneOffset);
        long endDateTimeZone = getDateByTimeZone(endDate, true, timeZoneOffset);

        LOGGER.info(String.format("Start Date: %s EndDAte: %s", startDateTimeZone, endDateTimeZone));
        List<String> reportTypes = Lists.newArrayList(reqReportTypes);
        if (CollectionUtils.isEmpty(reportTypes)) {
            DashboardWidget widget = dashboardService.findByIds(token.getActiveTopUser().getUserId(), token.getCompanyId(), token.getShopId());
            if (widget != null && !CollectionUtils.isEmpty(widget.getWidgets())) {
                reportTypes = Lists.newArrayList(widget.getWidgets());
            }
        }
        if (CollectionUtils.isEmpty(reportTypes)) {
            reportTypes = getDefaultDashboardReports();
        }

        LinkedHashMap<String, Object> reportList = new LinkedHashMap<>();

        // List All bracket sales included canceled, Sales, Refund, Refund With inventory
        Iterable<Transaction> allBracketSales = transactionRepository.getAllBracketSales(token.getCompanyId(), token.getShopId(), startDateTimeZone, endDateTimeZone);

        Set<String> productIds = new HashSet<>();
        Set<ObjectId> productObjIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> memberIds = new HashSet<>();
        Set<ObjectId> productCategoryIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();
        List<Transaction> transactions = new ArrayList<>();

        for (Transaction transaction : allBracketSales) {
            if (transaction.getStatus() == Transaction.TransactionStatus.Canceled) {
                continue;
            }
            transactions.add(transaction);
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && ObjectId.isValid(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
            if (StringUtils.isNotBlank(transaction.getSellerId()) && ObjectId.isValid(transaction.getSellerId())) {
                employeeIds.add(new ObjectId(transaction.getSellerId()));
            }
            if (transaction.getCart() != null && CollectionUtils.isNotEmpty(transaction.getCart().getItems())) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                        continue;
                    }
                    productObjIds.add(new ObjectId(item.getProductId()));
                    productIds.add(item.getProductId());
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productObjIds));
        List<Product> activeProductList = reportTypes.contains(ReportType.LOW_INVENTORY_DASHBOARD.name()) ? productRepository.getActiveProductList(token.getCompanyId(), token.getShopId()) : new ArrayList<>();

        for (Product product : productHashMap.values()) {
            if (!productIds.contains(product.getId())) {
                continue;
            }
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }
        for (Product product : activeProductList) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productCategoryIds));
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(memberIds));

        HashMap<String, Prepackage> prepackagesMap = reportTypes.contains(ReportType.LOW_INVENTORY_DASHBOARD.name()) ?
                prepackageRepository.listAsMap(token.getCompanyId(), token.getShopId()) :
                prepackageRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));

        HashMap<String, Employee> employeeHashMap;

        Iterable<TerminalLocation> terminalLocationList = null;

        if (reportTypes.contains(ReportType.MILEAGE_REPORT_BY_EMPLOYEE.name())) {
            employeeHashMap = employeeRepository.listAsMap(token.getCompanyId());
            terminalLocationList = terminalLocationRepository.getTermLocationsWithDate(token.getCompanyId(), token.getShopId(), startDateTimeZone, endDateTimeZone, "{created:1}");
        } else {
            employeeHashMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        }

        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getProductPrepackageQuantities(token.getCompanyId(), token.getShopId());

        Iterable<Member> newMembers = memberRepository.getMembersWithStartDate(token.getCompanyId(), startDateTimeZone, endDateTimeZone);

        HashMap<String, PrepackageProductItem> prepackageProductItemMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(token.getCompanyId());
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId());

        Map<String, ProductBatch> recentBatchMap = getProductBatchByProductId(productBatchHashMap);

        Object report;
        for (String reportName : reportTypes) {

            ReportType reportType = null;
            try {
                reportType = ReportType.valueOf(reportName);
            } catch (Exception e) {
                LOGGER.info(String.format("Report type not found for : %s", reportName));
            }
            if (reportType == null) {
                continue;
            }

            ReportFilter filter = new ReportFilter(shop, reportType, token, "timezoneOffset:" + timeZoneOffset, startDate, endDate);

            switch (reportType) {
                /* Average Discount Dashboard Report */
                case AVERAGE_DISCOUNT_DASHBOARD:
                    report = averageDiscountGatherer.prepareAverageDiscount(filter, transactions);
                    break;
                /* Sales By Category Dashboard Report */
                case CATEGORY_SALES_DASHBOARD:
                    HashMap<String, SalesByProductCategoryGatherer.CategorySale> categorySaleMap = salesByProductCategory.prepareCategorySaleReport(filter);
                    report = dashboardCategorySalesGatherer.prepareDashboardCategorySales(filter, categorySaleMap);
                    break;
                /* Top 5 Bud Tenders Report */
                case EMPLOYEE_SALES_DASHBOARD:
                    List<SalesByEmployeeGatherer.EmployeeSale> budTenderSales = salesByEmployeeGatherer.prepareSalesReport(filter);
                    report = dashboardEmployeeSalesGatherer.prepareDashboardEmployeeSales(filter, budTenderSales);
                    break;
                /*Best Selling Brand */
                case BRAND_SALES_DASHBOARD:
                    report = salesByBrandGatherer.prepareSalesByBrand(filter, transactions, productHashMap, brandHashMap);
                    break;
                case AVERAGE_ORDER_DASHBOARD:
                    report = averageOrderGatherer.prepareAverageOrder(filter, transactions);
                    break;
                /* Credit Card Fees Dashboard Report */
                case CREDIT_CARD_FEE_DASHBOARD:
                    report = creditCardFeesGatherer.prepareCreditCardFees(filter, transactions);
                    break;
                /* Delivery Fee Dashboard Report */
                case DELIVERY_FEE_DASHBOARD:
                    report = deliveryFeesGatherer.prepareDeliveryFees(filter, transactions);
                    break;
                /* Sales By Age Group Dashboard Report */
                case SALES_BY_AGE_GROUP_DASHBOARD:
                    report = salesByAgeGroupGatherer.prepareSalesByAgeGroup(filter, transactions, memberHashMap);
                    break;
                /* Best Selling Products Dashboard Report */
                case BEST_SELLING_PRODUCTS_DASHBOARD:
                    report = bestSellingProductsGatherer.prepareBestSellingProducts(filter, transactions, productHashMap, productCategoryMap);
                    break;
                /* Excise Tax Dashboard Report */
                /* City Tax Dashboard Report */
                /* County Tax Dashboard Report */
                /* State Tax Dashboard Report */
                case EXCISE_TAX_DASHBOARD:
                case CITY_TAX_DASHBOARD:
                case COUNTY_TAX_DASHBOARD:
                case STATE_TAX_DASHBOARD:
                    report = totalTaxGatherer.prepareTotalTax(filter, transactions);
                    break;
                /* Sales By Payment Type Dashboard Report */
                case SALES_BY_PAYMENT_TYPE_DASHBOARD:
                    report = salesByPaymentGatherer.prepareSalesByPayment(filter, transactions);
                    break;
                /* Marketing Source Dashboard Report */
                case MARKETING_SOURCE_DASHBOARD:
                    report = marketingSourceGatherer.prepareMarketingSource(filter, transactions, memberHashMap);
                    break;
                /* Gender Sales Dashboard Report */
                case GENDER_SALES_DASHBOARD:
                    report = genderSalesGatherer.prepareGenderSales(filter, transactions, memberHashMap);
                    break;
                /* New Members By Type Dashboard Report */
                case NEW_MEMBERS_BY_TYPE_DASHBOARD:
                    report = newMembersByTypeGatherer.prepareNewMembersByType(filter, newMembers);
                    break;
                /* Employee Sales By Category Dashboard Report */
                case EMPLOYEE_SALES_BY_CATEGORY_DASHBOARD:
                    HashMap<String, Employee> currentEmployeeMap = new HashMap<>();
                    currentEmployeeMap.putIfAbsent(token.getActiveTopUser().getUserId(), employeeHashMap.get(token.getActiveTopUser().getUserId()));
                    report = employeeSalesByCategory.prepareEmployeeSalesByCategory(filter, transactions, currentEmployeeMap, productHashMap, productCategoryMap);
                    break;
                /* Low Inventory Dashboard Report */
                case LOW_INVENTORY_DASHBOARD:
                    report = lowInventoryGatherer.prepareLowInventory(filter, productCategoryMap, activeProductList, productPrepackageQuantities, prepackagesMap);
                    break;
                /* Sales By Employee Dashboard Report */
                case SALES_BY_EMPLOYEE:
                    report = prepareSalesByEmployeeReport(token.getActiveTopUser().getUserId(), allBracketSales, employeeHashMap, productHashMap, productCategoryMap, shop);
                    break;
                /* QuickGlance Dashboard Report */
                case QUICK_GALANCE:
                    report = prepareQuickGalanceResult(allBracketSales, newMembers, productHashMap, brandHashMap);
                    break;
                /* Member Queue Dashboard Report */
                case MEMBER_QUEUE:
                    report = queuePOSService.getTransactionsWithDate(filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
                    break;
                /* Sales By Strain Type Report */
                case SALES_BY_CANNABIS_TYPE:
                    report = new SalesByCannabisTypeGatherer(transactionRepository, productRepository).prepareSalesByCannabisType(filter, transactions, productHashMap);
                    break;
                /* Sales By Strain Type Report */
                case MILEAGE_REPORT_BY_EMPLOYEE:
                    report = new MileageReportByEmployeeGatherer(employeeRepository, terminalLocationRepository, deliveryService).prepareMileageReportByEmployee(filter, terminalLocationList, employeeHashMap);
                    break;
                /* Weekly Performance Report */
                case WEEKLY_PERFORMANCE_DASHBOARD:
                    long startTimeZoneMillies = new DateTime(filter.getStartDateMillis()).minusDays(15).getMillis();
                    long endTimeZoneMillies = new DateTime().minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusDays(1).minusSeconds(1).plusMinutes(filter.getTimezoneOffset()).getMillis();
                    Iterable<Transaction> allTransactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), startTimeZoneMillies, endTimeZoneMillies);
                    report = weeklyPerformanceGatherer.prepareWeeklyPerformance(filter, allTransactions);
                    break;
                case DAILY_SUMMARY_DASHBOARD:
                    report = prepareDailySummaryReport(allBracketSales, prepackagesMap, prepackageProductItemMap, toleranceHashMap, productBatchHashMap, recentBatchMap, employeeHashMap);
                    break;
                case HOURLY_PERFORMANCE_DASHBOARD:
                    report = hourlyPerformanceGatherer.prepareSalesByHour(filter, transactions);
                    break;
                case OVERVIEW_DASHBOARD:
                    report = overviewGatherer.prepareOverview(filter, transactions);
                    break;
                default:
                    report = null;
                    break;
            }
            reportList.put(reportType.name(), report);
        }

        DashboardWidgetAddRequest addRequest = new DashboardWidgetAddRequest();
        addRequest.setEmployeeId(token.getActiveTopUser().getUserId());
        addRequest.setDashboardWidget(reportTypes);
        dashboardService.addDashboardWidget(addRequest, false);
        return reportList;
    }

    @Override
    public DailySummaryResult getDailySummaryReport(String startDateStr) {
        Shop shop = shopRepository.getById(token.getShopId());
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(token.getRequestTimeZone());
        if (shop != null) {
            timeZoneOffset = shop.getTimezoneOffsetInMinutes()*-1;
        }
        long timeZoneStartDateMillis = getDateByTimeZone(startDateStr, false, timeZoneOffset);
        long endDateMillis = getDateByTimeZone(startDateStr, true, timeZoneOffset);
        Iterable<Transaction> transactions = transactionRepository.getAllBracketSales(token.getCompanyId(), token.getShopId(), timeZoneStartDateMillis, endDateMillis);
        Set<String> productIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();

        for (Transaction transaction : transactions) {
            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                productIds.add(item.getProductId());
            }

            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && ObjectId.isValid(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
            if (StringUtils.isNotBlank(transaction.getSellerId()) && ObjectId.isValid(transaction.getSellerId())) {
                employeeIds.add(new ObjectId(transaction.getSellerId()));
            }
        }

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(token.getCompanyId());
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId());
        Map<String, ProductBatch> recentBatchMap = productBatchRepository.getLatestBatchForProductList(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));

        return prepareDailySummaryReport(transactions, prepackageHashMap, prepackageProductItemMap, toleranceHashMap, productBatchHashMap, recentBatchMap, employeeHashMap);
    }

    private QuickGlanceResult prepareQuickGalanceResult(Iterable<Transaction> transactions, Iterable<Member> newMembers, HashMap<String, Product> productHashMap, HashMap<String, Brand> brandHashMap) {

        QuickGlanceResult result = new QuickGlanceResult();
        QuickGlanceResult.MemberVisits memberVisits = new QuickGlanceResult.MemberVisits();

        HashMap<String, QuickGlanceResult.SalesByType> orderDetailsMap = new HashMap<>();
        HashMap<String, QuickGlanceResult.SalesByConsumerType> salesByConsumerTypeMap = new HashMap<>();
        HashMap<String, SalesByBrand> salesByBrandMap = new HashMap<>();

        BigDecimal factor;
        BigDecimal totalSales = BigDecimal.ZERO;
        long salesCount = 0;
        Product product;
        BigDecimal refundSales = BigDecimal.ZERO;


        HashMap<String, Member> newMembersMap = new HashMap<>();
        for (Member member : newMembers) {
            newMembersMap.put(member.getId(), member);
        }
        for (Transaction transaction : transactions) {

            if (transaction.getStatus() == Transaction.TransactionStatus.Canceled || transaction.getStatus() == Transaction.TransactionStatus.Completed) {
                memberVisits.setTotalVisits(memberVisits.getTotalVisits() + 1);
            }

            if (transaction.getStatus() == Transaction.TransactionStatus.Canceled || transaction.getCart() == null) {
                continue;
            }

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                memberVisits.setRefundVisits(memberVisits.getRefundVisits() + 1);
                refundSales = transaction.getCart().getTotal();
                factor = factor.negate();
            }

            BigDecimal sales = transaction.getCart().getTotal().multiply(factor);

            totalSales = totalSales.add(sales);
            salesCount++;

            if (newMembersMap.containsKey(transaction.getMemberId())) {
                memberVisits.setNewMemberSales(memberVisits.getNewMemberSales().add(sales));
            }

            memberVisits.setTotalSales(memberVisits.getTotalSales().add(sales));
            memberVisits.setNewMemberVisits(newMembersMap.size());
            memberVisits.setRefundSales(refundSales);

            salesByConsumerTypeMap.putIfAbsent(transaction.getCart().getFinalConsumerTye().toString(), new QuickGlanceResult.SalesByConsumerType());

            QuickGlanceResult.SalesByConsumerType salesByConsumerType = salesByConsumerTypeMap.get(transaction.getCart().getFinalConsumerTye().toString());
            salesByConsumerType.setConsumerType(transaction.getCart().getFinalConsumerTye());
            salesByConsumerType.setSales(salesByConsumerType.getSales().add(sales));
            salesByConsumerType.setCount(salesByConsumerType.getCount() + 1);

            String type = "Sale";
            if (StringUtils.isNotBlank(transaction.getConsumerCartId())) {
                type = "Online";
            }

            if (factor.intValue() == -1) {
                type = "Refund";
            }

            orderDetailsMap.putIfAbsent(type, new QuickGlanceResult.SalesByType());
            QuickGlanceResult.SalesByType salesByType = orderDetailsMap.get(type);

            salesByType.setSalesType(type);
            salesByType.setCount(salesByType.getCount() + 1);
            salesByType.setGrossSales(salesByType.getGrossSales().add(sales));

            HashMap<String, Double> propRatioMap = new HashMap<>();

            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            double total = 0.0;
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                product = productHashMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                product = productHashMap.get(orderItem.getProductId());
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product != null && product.isDiscountable() && total > 0) {
                    double finalCost = orderItem.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }

            }

            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                Brand brand = brandHashMap.get(product.getBrandId());

                BigDecimal ratio = BigDecimal.ONE;
                if (propRatioMap.containsKey(item.getId())) {
                    ratio = BigDecimal.valueOf(propRatioMap.getOrDefault(item.getId(), 1D));
                }

                BigDecimal propCartDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propDeliveryFee = transaction.getCart().getDeliveryFee() != null ? transaction.getCart().getDeliveryFee().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propCCFee = transaction.getCart().getCreditCardFee() != null ? transaction.getCart().getCreditCardFee().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().multiply(ratio) : BigDecimal.ZERO;

                // calculated postTax
                BigDecimal gross, totalTax;

                totalTax = getCalculateTax(item, propCartDiscount);

                gross = item.getFinalPrice().subtract(propCartDiscount).add(propDeliveryFee).add(totalTax).add(propCCFee).subtract(propAfterTaxDiscount);

                gross = gross.multiply(factor);

                if (brand != null) {
                    salesByBrandMap.putIfAbsent(brand.getId(), new SalesByBrand());
                    SalesByBrand salesByBrand = salesByBrandMap.get(brand.getId());

                    salesByBrand.setSales(salesByBrand.getSales().add(gross));
                    salesByBrand.setBrandName(brand.getName());
                    salesByBrand.setBrandId(brand.getId());
                }

            }
        }

        for (Map.Entry<String, QuickGlanceResult.SalesByConsumerType> entry : salesByConsumerTypeMap.entrySet()) {
            QuickGlanceResult.SalesByConsumerType value = entry.getValue();
            value.setSalesPercent(salesCount == 0 ? BigDecimal.ZERO : BigDecimal.valueOf(value.getCount() * 100 / salesCount));
        }

        for (Map.Entry<String, QuickGlanceResult.SalesByType> entry : orderDetailsMap.entrySet()) {
            QuickGlanceResult.SalesByType value = entry.getValue();
            value.setPercent(salesCount == 0 ? BigDecimal.ZERO : BigDecimal.valueOf(value.getCount() * 100 / salesCount));
        }


        result.setTotalSales(totalSales);
        result.setMemberVisits(memberVisits);
        result.setSalesByBrands(Lists.newArrayList(salesByBrandMap.values()));
        result.setSalesByConsumer(Lists.newArrayList(salesByConsumerTypeMap.values()));
        result.setSalesByType(Lists.newArrayList(orderDetailsMap.values()));

        result.getSalesByBrands().removeIf(salesByBrand -> salesByBrand.getSales().doubleValue() <= 0);

        result.getSalesByBrands().sort(Comparator.comparing(SalesByBrand::getSales));
        return result;
    }

    private BigDecimal getCalculateTax(OrderItem item, BigDecimal propCartDiscount) {
        BigDecimal postTax, postALExciseTax, postNALExciseTax, totalCityTax, totalStateTax, totalFedTax;

        postTax = BigDecimal.ZERO;

        BigDecimal subTotalAfterDiscount = item.getFinalPrice().subtract(propCartDiscount);
        if (item.getTaxResult() != null) {
            postALExciseTax = item.getTaxResult().getTotalALPostExciseTax();
            postNALExciseTax = item.getTaxResult().getTotalExciseTax();

            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                postTax = postTax.add(item.getTaxResult().getTotalPostCalcTax().add(postALExciseTax).add(postNALExciseTax));
            }
        }
        if (postTax.doubleValue() == 0 && ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null)) {
            TaxInfo taxInfo = item.getTaxInfo();
            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                totalCityTax = subTotalAfterDiscount.multiply(taxInfo.getCityTax());
                totalStateTax = subTotalAfterDiscount.multiply(taxInfo.getStateTax());
                totalFedTax = subTotalAfterDiscount.multiply(taxInfo.getFederalTax());

                postTax = totalCityTax.add(totalStateTax).add(totalFedTax);
            }
        }

        if (postTax.doubleValue() == 0) {
            postTax = item.getCalcTax(); // - item.getCalcPreTax().doubleValue();
        }
        return postTax;
    }

    private DailySummaryResult prepareDailySummaryReport(Iterable<Transaction> transactions, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, HashMap<String, ProductBatch> productBatchHashMap, Map<String, ProductBatch> recentBatchMap, HashMap<String, Employee> employeeHashMap) {
        DailySummaryResult result = new DailySummaryResult();
        // Build unique set
        HashSet<Cart.PaymentOption> customPaymentOptions = new HashSet<>();
        HashMap<Long, DailySummary> days = new HashMap<>();
        ArrayList<DailySummary> summaries = dailySummaryGatherer.getDailySummaries(days, new ArrayList<>(), transactions, productBatchHashMap,
                prepackageHashMap, prepackageProductItemMap, toleranceHashMap, (HashMap<String, ProductBatch>) recentBatchMap, customPaymentOptions, true);

        for (Transaction transaction : transactions) {
            switch (transaction.getCart().getPaymentOption()) {
                case Cash:
                case Check:
                case Credit:
                case StoreCredit:
                case Split:
                case CashlessATM:
                    // Skip these, they're already explicitly defined.
                    break;
                default:
                    // Everything else is considered custom
                    customPaymentOptions.add(transaction.getCart().getPaymentOption());
            }
        }
        DailySummary today;

        if (!summaries.isEmpty()) {
        today =  summaries.get(0);

            for (Map.Entry<String, DailySummary.EmployeeStats> entry : today.employeeStatsMap.entrySet()) {
                Employee employee = employeeHashMap.get(entry.getKey());
                StringBuilder employeeName = new StringBuilder();
                if (employee != null) {
                    employeeName.append(employee.getFirstName().trim())
                            .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName().trim() : "");
                } else {
                    employeeName.append("N/A");
                }
                entry.getValue().name = employeeName.toString();
                if (entry.getValue() != null) {
                    entry.getValue().sales = NumberUtils.round(entry.getValue().sales, 2);
                    entry.getValue().refunds = NumberUtils.round(entry.getValue().refunds, 2);
                }
            }
            if (today.customPaymentOptions != null) {
                today.customPaymentOptions.remove(Cart.PaymentOption.None);
            }
            if (today.customPaymentTotals != null) {
                today.customPaymentTotals.remove(Cart.PaymentOption.None);
            }
            result.setDailySummary(today);
            result.getDailySummary().transactionCount = today.salesCount - today.refundCount;
            result.getDailySummary().totalrefundCount = today.salesItems - today.refundItems;
            result.getDailySummary().grossProfit = today.gross - today.cogs;
            result.getDailySummary().fees = today.deliveryFees + today.creditFees;
            result.getDailySummary().employeeStatsList = Lists.newArrayList(today.employeeStatsMap.values());
            result.getDailySummary().employeeStatsMap = null;
            today.employeeStatsMap = null;
        }

        result.setShopLimitedView(shopRepository.getById(token.getShopId(), ShopLimitedView.class));
        return result;
    }

    private List<String> getDefaultDashboardReports() {
        return Lists.newArrayList(
                ReportType.OVERVIEW_DASHBOARD.name(),
                ReportType.AVERAGE_DISCOUNT_DASHBOARD.name(),
                ReportType.CATEGORY_SALES_DASHBOARD.name(),
                ReportType.EMPLOYEE_SALES_DASHBOARD.name(),
                ReportType.BRAND_SALES_DASHBOARD.name(),
                ReportType.AVERAGE_ORDER_DASHBOARD.name(),
                ReportType.CREDIT_CARD_FEE_DASHBOARD.name(),
                ReportType.DELIVERY_FEE_DASHBOARD.name(),
                ReportType.SALES_BY_AGE_GROUP_DASHBOARD.name(),
                ReportType.BEST_SELLING_PRODUCTS_DASHBOARD.name(),
                ReportType.EXCISE_TAX_DASHBOARD.name(),
                ReportType.CITY_TAX_DASHBOARD.name(),
                ReportType.COUNTY_TAX_DASHBOARD.name(),
                ReportType.STATE_TAX_DASHBOARD.name(),
                ReportType.SALES_BY_PAYMENT_TYPE_DASHBOARD.name(),
                ReportType.MARKETING_SOURCE_DASHBOARD.name(),
                ReportType.GENDER_SALES_DASHBOARD.name(),
                ReportType.HOURLY_PERFORMANCE_DASHBOARD.name(),
                ReportType.NEW_MEMBERS_BY_TYPE_DASHBOARD.name(),
                ReportType.EMPLOYEE_SALES_BY_CATEGORY_DASHBOARD.name(),
                ReportType.LOW_INVENTORY_DASHBOARD.name(),
                ReportType.SALES_BY_EMPLOYEE.name(),
                ReportType.QUICK_GALANCE.name(),
                ReportType.MEMBER_QUEUE.name(),
                ReportType.SALES_BY_CANNABIS_TYPE.name(),
                ReportType.MILEAGE_REPORT_BY_EMPLOYEE.name(),
                ReportType.WEEKLY_PERFORMANCE_DASHBOARD.name());

    }

    private ArrayList<EmployeeSalesResult> prepareSalesByEmployeeReport(String employeeId, Iterable<Transaction> transactions, HashMap<String, Employee> employeeMap, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> productCategoryMap, Shop shop) {
        BigDecimal totalDiscount = BigDecimal.ZERO;
        HashMap<String, Double> propRatioMap = new HashMap<>();
        int salesCount = 0;
        BigDecimal factor;
        HashMap<String, EmployeeSalesResult> resultMap = new HashMap<>();
        HashMap<String, Long> employeeTransactionWaitTime = new HashMap<>();
        HashMap<String, Integer> employeeTransactionCount = new HashMap<>();

        for (Transaction transaction : transactions) {
            String transEmployeeId = transaction.getSellerId();
            Employee employee = employeeMap.get(transEmployeeId);
            if (employee == null) {
                continue;
            }
            if (StringUtils.isNotBlank(employeeId) && !employee.getId().equalsIgnoreCase(employeeId)) {
                continue;
            }

            resultMap.putIfAbsent(transEmployeeId, new EmployeeSalesResult());
            EmployeeSalesResult employeeSalesResult = resultMap.get(transEmployeeId);

            employeeSalesResult.setEmployeeId(transEmployeeId);

            String employeeName = employee.getFirstName().trim() +
                    (StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName().trim() : "");
            employeeSalesResult.setEmployeeName(employeeName);

            long queueCreateTime = (transaction.getStartTime() == null) ? 0 : transaction.getStartTime();
            long queueEndTime = (transaction.getEndTime() == null) ? 0 : transaction.getEndTime();

            if (queueCreateTime == 0 && transaction.getCreated() != null) {
                queueCreateTime = transaction.getCreated();
            }

            if (queueEndTime == 0 && transaction.getProcessedTime() != null) {
                queueEndTime = transaction.getProcessedTime();
            }

            long queueDuration = queueEndTime - queueCreateTime;
            if (queueDuration < 0) {
                queueDuration = 0;
            }

            employeeTransactionWaitTime.put(transEmployeeId, employeeTransactionWaitTime.getOrDefault(transEmployeeId, 0L) + queueDuration);
            employeeTransactionCount.put(transEmployeeId, employeeTransactionCount.getOrDefault(transEmployeeId, 0) + 1);

            employeeSalesResult.setTotalVisits(employeeSalesResult.getTotalVisits() + 1);

            factor = BigDecimal.ONE;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = factor.negate();
            }

            double total = 0.0;
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product == null) {
                    continue;
                }
                if (product != null && product.isDiscountable()) {
                    total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product == null) {
                    continue;
                }
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product != null && product.isDiscountable() && total > 0) {
                    double finalCost = orderItem.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }
            }

            for (OrderItem item : transaction.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());

                if (product == null) {
                    continue;
                }

                ProductCategory category = productCategoryMap.get(product.getCategoryId());

                BigDecimal ratio = BigDecimal.ONE;
                if (propRatioMap.containsKey(item.getId())) {
                    ratio = BigDecimal.valueOf(propRatioMap.getOrDefault(item.getId(), 1D));
                }

                BigDecimal propDeliveryFee = transaction.getCart().getDeliveryFee() != null ? transaction.getCart().getDeliveryFee().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propCartDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propCCFee = transaction.getCart().getCreditCardFee() != null ? transaction.getCart().getCreditCardFee().multiply(ratio) : BigDecimal.ZERO;

                // calculated postTax

                BigDecimal totalTax = getCalculateTax(item, propCartDiscount);

                BigDecimal gross = item.getFinalPrice().subtract(propCartDiscount).add(propDeliveryFee).add(totalTax).add(propCCFee).subtract(propAfterTaxDiscount);
                double roundedTotal = gross.doubleValue();

                if (Shop.TaxRoundOffType.FIVE_CENT.equals(shop.getTaxRoundOffType()) && transaction.getCart().getPaymentOption() == Cart.PaymentOption.Cash) {
                    gross = BigDecimal.valueOf(NumberUtils.roundToNearestCent(roundedTotal));
                } else if (Shop.TaxRoundOffType.UP_DOLLAR == shop.getTaxRoundOffType()) {
                    gross = BigDecimal.valueOf(NumberUtils.ceil(roundedTotal));
                } else if (Shop.TaxRoundOffType.NEAREST_DOLLAR == shop.getTaxRoundOffType()) {
                    gross = BigDecimal.valueOf(NumberUtils.roundToNearestCent(roundedTotal));
                } else if (Shop.TaxRoundOffType.DOWN_DOLLAR == shop.getTaxRoundOffType()) {
                    gross = BigDecimal.valueOf(NumberUtils.floor(roundedTotal));
                } else {
                    gross = BigDecimal.valueOf(NumberUtils.round(roundedTotal, 2));
                }

                gross = gross.multiply(factor);

                employeeSalesResult.setUnitsSold(employeeSalesResult.getUnitsSold().add(item.getQuantity().multiply(factor)));
                employeeSalesResult.setSales(employeeSalesResult.getSales().add(gross));
                totalDiscount = totalDiscount.add(transaction.getCart().getTotalDiscount().multiply(factor));
                employeeSalesResult.setTotalDiscount(totalDiscount);

                salesCount++;

                employeeSalesResult.setAvgSale(salesCount == 0 ? BigDecimal.ZERO : BigDecimal.valueOf(employeeSalesResult.getSales().doubleValue() / ((double) salesCount)));


                PurchaseProduct purchaseProduct = null;
                for (PurchaseProduct prod : employeeSalesResult.getProducts()) {
                    if (item.getProductId().equals(prod.getProductId())) {
                        purchaseProduct = prod;
                    }
                }

                if (purchaseProduct == null) {
                    purchaseProduct = new PurchaseProduct();
                    employeeSalesResult.getProducts().add(purchaseProduct);
                }

                purchaseProduct.setProductId(product.getId());
                purchaseProduct.setQuantity(purchaseProduct.getQuantity().add(item.getQuantity().multiply(factor)));
                purchaseProduct.setProductName(product.getName());

                if (category != null) {
                    EmployeeSalesResult.SoldProductType soldProductType = null;
                    for (EmployeeSalesResult.SoldProductType productType : employeeSalesResult.getProductTypes()) {
                        if (category.getId().equals(productType.getCategoryId())) {
                            soldProductType = productType;
                        }
                    }
                    if (soldProductType == null) {
                        soldProductType = new EmployeeSalesResult.SoldProductType();
                        employeeSalesResult.getProductTypes().add(soldProductType);
                    }

                    soldProductType.setSales(soldProductType.getSales().add(gross));
                    soldProductType.setCategoryName(category.getName());
                    soldProductType.setCategoryId(product.getCategoryId());
                }
            }
        }


        for (Map.Entry<String, EmployeeSalesResult> entry : resultMap.entrySet()) {
            if (StringUtils.isBlank(entry.getKey())) {
                continue;
            }
            Long waitTime = employeeTransactionWaitTime.getOrDefault(entry.getKey(), 0L);
            Integer count = employeeTransactionCount.getOrDefault(entry.getKey(), 0);

            String avgLength = "N/A";
            if (count != 0 || waitTime != 0) {
               avgLength = averageTransactionTimeGatherer.formatTime(waitTime / count);
            }

            EmployeeSalesResult value = entry.getValue();

            entry.getValue().getProductTypes().stream()
                    .filter(soldProductType -> value.getSales().doubleValue() != 0)
                    .forEach(soldProductType -> soldProductType.setPercent(BigDecimal.valueOf((soldProductType.getSales().doubleValue() / value.getSales().doubleValue()) * 100)));

            value.getProducts().sort((first, second) -> {
                if (first.getQuantity().equals(second.getQuantity())) {
                    return 0;
                }
                return second.getQuantity().compareTo(first.getQuantity());
            });

            value.getProducts().subList(0, value.getProducts().size() > 5 ? 5 : value.getProducts().size());

            value.setAvgTransactionLength(avgLength);
        }

        return Lists.newArrayList(resultMap.values());
    }

    private HashMap<String, ProductBatch> getProductBatchByProductId(HashMap<String, ProductBatch> productBatchHashMap) {
        HashMap<String, ProductBatch> returnMap = new HashMap<>();

        for (String key : productBatchHashMap.keySet()) {
            ProductBatch batch = productBatchHashMap.get(key);
            if (returnMap.containsKey(batch.getProductId())) {
                ProductBatch mapBatch = returnMap.get(batch.getProductId());
                DateTime batchDateTime = new DateTime(batch.getCreated());
                if (batchDateTime.isBefore(mapBatch.getCreated())) {
                    batch = mapBatch;
                }
            }
            returnMap.put(batch.getProductId(), batch);
        }

        return returnMap;
    }


    @Override
    public HashMap<String, Object> getAllCompanyReports(String startDateStr, String endDateStr, String reqReportTypesStr, int timeZoneOffset) {
        Shop shop = shopRepository.getById(token.getShopId());

        Set<String> reqReportTypes = new HashSet<>();
        if (StringUtils.isNotBlank(reqReportTypesStr)) {
            reqReportTypes = new HashSet<>(Arrays.asList(StringUtils.split(reqReportTypesStr, "\\s*,\\s*")));

        }

        timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(token.getRequestTimeZone());
        if (shop != null) {
            timeZoneOffset = shop.getTimezoneOffsetInMinutes()*-1;
        }
        long startDateTimeZone = getDateByTimeZone(startDateStr, false, timeZoneOffset);
        long endDateTimeZone = getDateByTimeZone(endDateStr, true, timeZoneOffset);

        LOGGER.info(String.format("Start Date: %s EndDAte: %s", startDateTimeZone, endDateTimeZone));

        List<String> reportTypes = Lists.newArrayList(reqReportTypes);

        if (CollectionUtils.isEmpty(reportTypes)) {
            reportTypes = getDefaultCompanyDashboardReports();
        }

        HashMap<String, Object> reportList = new HashMap<>();

        // List All bracket sales included canceled, Sales, Refund, Refund With inventory
        Iterable<Transaction> allBracketCompanySales = transactionRepository.getBracketSalesByCompany(token.getCompanyId(), startDateTimeZone, endDateTimeZone);

        Set<String> productIds = new HashSet<>();
        Set<ObjectId> productObjIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> memberIds = new HashSet<>();
        Set<ObjectId> shopIds = new HashSet<>();
        LinkedHashSet<String> productIdList = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();

        for (Transaction transaction : allBracketCompanySales) {
            if (transaction.getStatus() == Transaction.TransactionStatus.Canceled) {
                continue;
            }
            transactions.add(transaction);
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (StringUtils.isNotBlank(transaction.getShopId()) && ObjectId.isValid(transaction.getShopId())) {
                shopIds.add(new ObjectId(transaction.getShopId()));
            }

            if (transaction.getCart() != null && CollectionUtils.isNotEmpty(transaction.getCart().getItems())) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                        continue;
                    }
                    productObjIds.add(new ObjectId(item.getProductId()));
                    productIds.add(item.getProductId());
                    productIdList.add(item.getProductId());
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productObjIds));

        for (Product product : productHashMap.values()) {
            if (!productIds.contains(product.getId())) {
                continue;
            }
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(memberIds));
        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(shopIds));
        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(token.getCompanyId());
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.getBatchesForProductsMap(token.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(token.getCompanyId());
        Iterable<Prepackage> prepackagesForProducts = prepackageRepository.getPrepackagesForProductsByCompany(token.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsByCompany(token.getCompanyId(), Lists.newArrayList(productIdList));

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }
        Object report;

        for (String reportName : reportTypes) {

            ReportType reportType = null;
            try {
                reportType = ReportType.valueOf(reportName);
            } catch (Exception e) {
                LOGGER.info(String.format("Report type not found for : %s", reportName));
            }
            if (reportType == null) {
                continue;
            }

            ReportFilter filter = new ReportFilter(shop, reportType, token, "timezoneOffset:" + timeZoneOffset, startDateStr, endDateStr);

            switch (reportType) {
                /* Company Sales By Age Dashboard Report */
                case COMPANY_SALES_BY_AGE_DASHBOARD:
                    report = salesByAgeGroupGatherer.prepareSalesByAgeGroup(filter, transactions, memberHashMap);
                    break;
                /* Company Sales By Category Dashboard Report */
                case COMPANY_SALES_BY_PAYMENT_DASHBOARD:
                    report = salesByPaymentGatherer.prepareSalesByPayment(filter,transactions);
                    break;
                /* Company Sales By Brand Dashboard Report */
                case COMPANY_SALES_BY_BRAND_DASHBOARD:
                    report = salesByBrandGatherer.prepareSalesByBrand(filter, transactions, productHashMap, brandHashMap);
                    break;
                /* Company Sales By Hour Dashboard Report */
                case COMPANY_SALES_BY_HOUR_DASHBOARD:
                    report = hourlyPerformanceGatherer.prepareSalesByHour(filter, transactions);
                    break;
                /* Company Total Sales Dashboard Report */
                case COMPANY_TOTAL_SALES_DASHBOARD:
                   report = companyTotalSalesGatherer.prepareCompanyTotalSales(filter, transactions);
                    break;
                /* Company Sales By Location Dashboard Report */
                case COMPANY_SALES_BY_LOCATION_DASHBOARD:
                    report = companySalesByLocationGatherer.prepareSalesByLocation(filter, transactions, shopHashMap);
                    break;
                case COMPANY_SALES_BY_PRODUCT_CATEGORY:
                    report = companySalesByProductCategoryGatherer.prepareCompanySalesByCategory(filter,transactions, productHashMap,categoryMap,batchHashMap,weightToleranceHashMap,prepackagesForProducts, prepackageProductItemHashMap);
                break;
                default:
                    report = null;
                    break;
            }
            reportList.put(reportType.name(), report);
        }

        return  reportList;
    }

    private List<String> getDefaultCompanyDashboardReports() {
        return Lists.newArrayList(
                ReportType.COMPANY_SALES_BY_AGE_DASHBOARD.name(),
                ReportType.COMPANY_SALES_BY_PAYMENT_DASHBOARD.name(),
                ReportType.COMPANY_SALES_BY_BRAND_DASHBOARD.name(),
                ReportType.COMPANY_SALES_BY_HOUR_DASHBOARD.name(),
                ReportType.COMPANY_SALES_BY_LOCATION_DASHBOARD.name(),
                ReportType.COMPANY_TOTAL_SALES_DASHBOARD.name(),
                ReportType.COMPANY_SALES_BY_PRODUCT_CATEGORY.name());
    }

    /**
     * Method to return Start Date or End Date of given time zone.
     * @param date
     * @param endDate
     * @param timeZone
     * @return
     */
    private long getDateByTimeZone(String date, boolean endDate, int timeZone) {
        Long dateMillis, resultMillis;
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        if (endDate) {
            DateTime jodaEndDate = null;
            if (date != null) {
                jodaEndDate = formatter.parseDateTime(date);
                jodaEndDate = jodaEndDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
                dateMillis = jodaEndDate.getMillis();
            } else {
                jodaEndDate = DateTime.now();
                dateMillis = jodaEndDate.getMillis();
            }
            DateTime eDate = new DateTime(dateMillis);
            resultMillis = jodaEndDate.plusMinutes(timeZone).getMillis();
        } else {
            if (date != null) {
                dateMillis = formatter.parseDateTime(date).getMillis();
            } else {
                dateMillis = 0l;
            }
            DateTime eDate = new DateTime(dateMillis);
            resultMillis = eDate.withTimeAtStartOfDay().plusMinutes(timeZone).getMillis();
        }
        return resultMillis;
    }
}
