package com.fourtwenty.dashboards.services;

import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.core.lifecycle.CoreAppStartup;
import com.fourtwenty.dashboards.services.impl.DashboardReportingServiceImpl;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class DashboardCoreModule extends AbstractModule {
    @Override
    protected void configure() {

        bind(DashboardReportingService.class).to(DashboardReportingServiceImpl.class);

        Multibinder<AppStartup> mb = Multibinder.newSetBinder(binder(), AppStartup.class);
        mb.addBinding().to(CoreAppStartup.class);

    }
}
