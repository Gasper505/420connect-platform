package com.fourtwenty.dashboards.services;

import com.fourtwenty.core.config.ConnectConfiguration;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class DashboardSwaggerBundle extends SwaggerBundle<ConnectConfiguration> {

    @Override
    protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ConnectConfiguration configuration) {
        return configuration.swaggerBundleConfiguration;
    }
}
