package com.fourtwenty.dashboards.services.health;

import com.codahale.metrics.health.HealthCheck;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.NamedHealthCheck;

public class DashboardHealthCheck extends NamedHealthCheck {

    @Override
    public String getName() {
        return "DashboardHealthCheck";
    }

    @Override
    protected HealthCheck.Result check() throws Exception {
        return HealthCheck.Result.healthy();
    }
}
