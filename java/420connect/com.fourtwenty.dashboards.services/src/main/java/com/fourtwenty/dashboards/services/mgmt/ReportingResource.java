package com.fourtwenty.dashboards.services.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.dashboards.constants.ResourceConstants;
import com.fourtwenty.dashboards.reports.result.DailySummaryResult;
import com.fourtwenty.dashboards.reports.result.EmployeeSalesResult;
import com.fourtwenty.dashboards.reports.result.QuickGlanceResult;
import com.fourtwenty.dashboards.services.DashboardReportingService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;


@Api("Management - Dashboard")
@Path(ResourceConstants.DASHBOARD_URL)
@Produces(MediaType.APPLICATION_JSON)
public class ReportingResource extends BaseResource {
    @Inject
    private DashboardReportingService reportingService;

    @GET
    @Path("/")
    @Timed(name = "reporting")
    @Secured
    @ApiOperation("Get Reports")
    public Response getReport(@QueryParam("type") String reportType,
                              @QueryParam("startDate") String startDate,
                              @QueryParam("endDate") String endDate,
                              @QueryParam("filter") String filter,
                              @QueryParam("format") String format,
                              @QueryParam("section") ReportTrack.ReportSectionType section) {
        if (StringUtils.isBlank(format)) {
            format = "JSON";
        }

        final Report report = reportingService.getReport(reportType, startDate, endDate, filter, format, section);

        if (format.equalsIgnoreCase("JSON")) {
            return Response.ok(report.getData(), MediaType.APPLICATION_JSON).build();
        } else if (format.equalsIgnoreCase("CSV")) {
            StreamingOutput streamingOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    ByteStreams.copy((InputStream) report.getData(), output);
                }
            };
            return Response.ok(streamingOutput, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=\"" + report.getReportName() + ".csv\"").type(report.getContentType()).build();
        } else if (format.equalsIgnoreCase("PDF")) {
            return null;
        } else {
            return null;
        }
    }

    @GET
    @Path("/quickGlance")
    @Timed(name = "quickGlance")
    @Secured
    @ApiOperation("Dashboard Quick Glance Report")
    public QuickGlanceResult getReport(@QueryParam("startDate") String startDate,
                                       @QueryParam("endDate") String endDate) {
        return reportingService.getQuickGlanceReport(startDate, endDate);

    }

    @GET
    @Path("/employeeSales")
    @Timed(name = "employeeSales")
    @Secured
    @ApiOperation("Sales by employee dashboard report")
    public ArrayList<EmployeeSalesResult> getReport(@QueryParam("employeeId") String employeeId,
                                                    @QueryParam("startDate") String startDate,
                                                    @QueryParam("endDate") String endDate) {
        return reportingService.getSalesByEmployeeReport(employeeId, startDate, endDate);

    }

    @GET
    @Path("/employeeReports")
    @Timed(name = "employeeReports")
    @Secured
    @ApiOperation("Get all dashboard reports")
    public LinkedHashMap<String, Object> getAllReports(@QueryParam("startDate") String startDate,
                                                       @QueryParam("endDate") String endDate,
                                                       @QueryParam("reportType") String reportTypes,
                                                       @QueryParam("timezoneOffset")int timezoneOffset) {
        return reportingService.getAllReports(startDate, endDate, reportTypes, timezoneOffset);

    }


    @GET
    @Path("/dailySummaryReport")
    @Timed(name = "dailySummaryReport")
    @Secured
    @ApiOperation("Get Daily Summary Report")
    public DailySummaryResult getDailyReports(@QueryParam("startDate") String startDate) {
        return reportingService.getDailySummaryReport(startDate);

    }

    @GET
    @Path("/companyReports")
    @Timed(name = "companyReports")
    @Secured
    @ApiOperation("Get all company reports")
    public HashMap<String, Object> getAllCompanyReports(@QueryParam("startDate") String startDate,
                                                        @QueryParam("endDate") String endDate,
                                                        @QueryParam("reportType") String reportTypes,
                                                        @QueryParam("timezoneOffset")int timezoneOffset) {
        return reportingService.getAllCompanyReports(startDate, endDate, reportTypes, timezoneOffset);

    }


}
