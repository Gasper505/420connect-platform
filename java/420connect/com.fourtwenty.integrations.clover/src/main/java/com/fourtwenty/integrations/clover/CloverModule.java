package com.fourtwenty.integrations.clover;

import com.fourtwenty.integrations.clover.repository.CloverRepository;
import com.fourtwenty.integrations.clover.repository.impl.CloverRepositoryImpl;
import com.fourtwenty.integrations.clover.service.CloverService;
import com.fourtwenty.integrations.clover.service.impl.CloverServiceImpl;
import com.google.inject.AbstractModule;

public class CloverModule extends AbstractModule {
    @Override
    protected void configure() {
        // Repository
        bind(CloverRepository.class).to(CloverRepositoryImpl.class);
        // Service
        bind(CloverService.class).to(CloverServiceImpl.class);
    }
}
