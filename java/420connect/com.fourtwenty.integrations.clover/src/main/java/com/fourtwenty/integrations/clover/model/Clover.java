package com.fourtwenty.integrations.clover.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@CollectionName(name = "clover_accounts")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Clover extends ShopBaseModel{
    @JsonProperty("expMonth")
    private String expMonth;
    @JsonProperty("cvv")
    private String cvv;
    @JsonProperty("amount")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("last4")
    private String accoutLastFourNumber;
    @JsonProperty("expYear")
    private int expYear;
    @JsonProperty("first6")
    private String accoutFirstSixNumber;
    @JsonProperty
    private String transactionId;
    @JsonProperty("cardNumber")
    private String cardNumber;
    public enum SyncStatus {
        INPROGRESS,
        COMPLETED,
        ERROR
    }
    @JsonProperty("syncStatus")
    private SyncStatus syncStatus;
    @JsonProperty("message")
    private String message;

    @JsonProperty("cardPaymentId")
    private String cardPaymentId;

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccoutLastFourNumber() {
        return accoutLastFourNumber;
    }

    public void setAccoutLastFourNumber(String accoutLastFourNumber) {
        this.accoutLastFourNumber = accoutLastFourNumber;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getAccoutFirstSixNumber() {
        return accoutFirstSixNumber;
    }

    public void setAccoutFirstSixNumber(String accoutFirstSixNumber) {
        this.accoutFirstSixNumber = accoutFirstSixNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public SyncStatus getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(SyncStatus syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCardPaymentId() {
        return cardPaymentId;
    }

    public void setCardPaymentId(String cardPaymentId) {
        this.cardPaymentId = cardPaymentId;
    }
}
