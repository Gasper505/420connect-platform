package com.fourtwenty.integrations.clover.repository;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.integrations.clover.model.Clover;

import java.util.List;

public interface CloverRepository extends MongoShopBaseRepository<Clover> {
    List<Clover> getAllProgressCloverTransaction(Clover.SyncStatus status);

    Iterable<Clover> getAllProgressCloverTransaction(String companyId, String shopId, String transactionId, Clover.SyncStatus status);
}
