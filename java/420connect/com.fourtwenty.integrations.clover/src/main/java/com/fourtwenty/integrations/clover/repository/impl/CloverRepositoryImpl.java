package com.fourtwenty.integrations.clover.repository.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.integrations.clover.model.Clover;
import com.fourtwenty.integrations.clover.repository.CloverRepository;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import java.util.List;

public class CloverRepositoryImpl extends ShopBaseRepositoryImpl<Clover> implements CloverRepository {
    @Inject
    public CloverRepositoryImpl(MongoDb mongo) throws Exception {
        super(Clover.class, mongo);
    }

    /**
     * @param status : Clover.SyncStatus
     * @return : List<Clover>
     * @implNote : This method is used to get all clover records which status is InProgress
     */
    @Override
    public List<Clover> getAllProgressCloverTransaction(Clover.SyncStatus status) {
        Iterable<Clover> results = coll.find("{syncStatus:#, deleted: false}", status)
                .as(entityClazz);
        return Lists.newArrayList(results.iterator());
    }

    @Override
    public Iterable<Clover> getAllProgressCloverTransaction(String companyId, String shopId, String transactionId, Clover.SyncStatus status) {
        return coll.find("{companyId:#, shopId:#, syncStatus:#, deleted: false, transactionId:#}", companyId, shopId, status, transactionId)
                .as(entityClazz);
    }
}

