package com.fourtwenty.integrations.clover.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloverPaymentRequest {
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("expMonth")
    private int expMonth;
    @JsonProperty("cvv")
    private String cvv;

    @JsonProperty("amount")
    private int amount = 0;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("last4")
    private String accoutLastFourNumber;
    @JsonProperty("expYear")
    private int expYear;
    @JsonProperty("first6")
    private String accoutFirstSixNumber;
    @JsonProperty("cardEncrypted")
    private String cardEncrypted;
    @JsonProperty
    private String transactionId;
    @JsonProperty("cardNumber")
    private String cardNumber;

    @JsonIgnore
    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccoutLastFourNumber() {
        return accoutLastFourNumber;
    }

    public void setAccoutLastFourNumber(String accoutLastFourNumber) {
        this.accoutLastFourNumber = accoutLastFourNumber;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getAccoutFirstSixNumber() {
        return accoutFirstSixNumber;
    }

    public void setAccoutFirstSixNumber(String accoutFirstSixNumber) {
        this.accoutFirstSixNumber = accoutFirstSixNumber;
    }

    public String getCardEncrypted() {
        return cardEncrypted;
    }

    public void setCardEncrypted(String cardEncrypted) {
        this.cardEncrypted = cardEncrypted;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
