package com.fourtwenty.integrations.clover.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.integrations.clover.response.BaseOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderRequest extends BaseOrder {

}
