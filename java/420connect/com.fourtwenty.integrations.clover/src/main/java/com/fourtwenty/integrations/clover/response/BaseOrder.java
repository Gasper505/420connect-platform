package com.fourtwenty.integrations.clover.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseOrder {
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("total")
    private int total = 0;
    @JsonProperty("title")
    private String title;
    @JsonProperty("note")
    private String note;
    @JsonProperty("taxRemoved")
    private boolean taxRemoved;


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isTaxRemoved() {
        return taxRemoved;
    }

    public void setTaxRemoved(boolean taxRemoved) {
        this.taxRemoved = taxRemoved;
    }

    class Employee {
        @JsonProperty("id")
        private String id;
    }

}
