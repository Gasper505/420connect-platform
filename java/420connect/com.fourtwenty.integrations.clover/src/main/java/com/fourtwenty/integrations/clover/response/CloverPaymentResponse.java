package com.fourtwenty.integrations.clover.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloverPaymentResponse {
    @JsonProperty("paymentId")
    private String paymentId;
    @JsonProperty("result")
    private String result;
    @JsonProperty("authCode")
    private String authCode;
    @JsonProperty("token")
    private String token;
    @JsonProperty("vaultedCard")
    private VaultedCard vaultedCard;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public VaultedCard getVaultedCard() {
        return vaultedCard;
    }

    public void setVaultedCard(VaultedCard vaultedCard) {
        this.vaultedCard = vaultedCard;
    }
}
