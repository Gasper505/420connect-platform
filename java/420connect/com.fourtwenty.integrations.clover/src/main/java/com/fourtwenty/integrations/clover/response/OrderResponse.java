package com.fourtwenty.integrations.clover.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderResponse extends BaseOrder{
    @JsonProperty("href")
    private String href;
    @JsonProperty("id")
    private String id;
    @JsonProperty("isVat")
    private boolean isVat;
    @JsonProperty("manualTransaction")
    private boolean manualTransaction;
    @JsonProperty("groupLineItems")
    private boolean groupLineItems;
    @JsonProperty("testMode")
    private boolean testMode;
    @JsonProperty("createdTime")
    private Long createdTime;
    @JsonProperty("clientCreatedTime")
    private Long clientCreatedTime;
    @JsonProperty("modifiedTime")
    private Long modifiedTime;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isVat() {
        return isVat;
    }

    public void setVat(boolean vat) {
        isVat = vat;
    }

    public boolean isManualTransaction() {
        return manualTransaction;
    }

    public void setManualTransaction(boolean manualTransaction) {
        this.manualTransaction = manualTransaction;
    }

    public boolean isGroupLineItems() {
        return groupLineItems;
    }

    public void setGroupLineItems(boolean groupLineItems) {
        this.groupLineItems = groupLineItems;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public Long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    public Long getClientCreatedTime() {
        return clientCreatedTime;
    }

    public void setClientCreatedTime(Long clientCreatedTime) {
        this.clientCreatedTime = clientCreatedTime;
    }

    public Long getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Long modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
