package com.fourtwenty.integrations.clover.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VaultedCard {
    @JsonProperty("first6")
    private String accoutFirstSixNumber;
    @JsonProperty("last4")
    private String accoutLastFourNumber;
    @JsonProperty("expirationDate")
    private String expirationDate;
    @JsonProperty("token")
    private String token;

    public String getAccoutFirstSixNumber() {
        return accoutFirstSixNumber;
    }

    public void setAccoutFirstSixNumber(String accoutFirstSixNumber) {
        this.accoutFirstSixNumber = accoutFirstSixNumber;
    }

    public String getAccoutLastFourNumber() {
        return accoutLastFourNumber;
    }

    public void setAccoutLastFourNumber(String accoutLastFourNumber) {
        this.accoutLastFourNumber = accoutLastFourNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
