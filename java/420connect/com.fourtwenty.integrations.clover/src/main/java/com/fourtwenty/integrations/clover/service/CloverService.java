package com.fourtwenty.integrations.clover.service;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.integrations.clover.request.CloverPaymentRequest;
import com.fourtwenty.integrations.clover.request.OrderRequest;
import com.fourtwenty.integrations.clover.response.CloverKeyResponse;
import com.fourtwenty.integrations.clover.response.CloverPaymentResponse;
import com.fourtwenty.integrations.clover.response.OrderResponse;

public interface CloverService {
    CloverKeyResponse getCloverKey(String merchantId, String token);

    OrderResponse addOrder(OrderRequest request, String merchantId, String token);

    CloverPaymentResponse processTransactionPayment(CloverPaymentRequest request, String merchantId, String token);

    void processPendingCloverPayments(Transaction transaction);

    void processCloverReceipts(Transaction dbTransaction);

    void prepareTransactionByJob();
}
