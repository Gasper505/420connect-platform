package com.fourtwenty.integrations.clover.service.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.common.CloverConfig;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.domain.models.receipt.CloverReceipt;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPaymentStatus;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.payment.CloverReceiptRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.paymentcard.clover.CloverRequest;
import com.fourtwenty.integrations.clover.model.Clover;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.SecurityUtil;
import com.fourtwenty.integrations.clover.repository.CloverRepository;
import com.fourtwenty.integrations.clover.request.CloverPaymentRequest;
import com.fourtwenty.integrations.clover.request.OrderRequest;
import com.fourtwenty.integrations.clover.response.CloverKeyResponse;
import com.fourtwenty.integrations.clover.response.CloverPaymentResponse;
import com.fourtwenty.integrations.clover.response.OrderResponse;
import com.fourtwenty.integrations.clover.service.CloverService;
import com.google.common.collect.Lists;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CloverServiceImpl implements CloverService {
    //private static final String BASE_URL = "https://apisandbox.dev.clover.com/";
    //private static final String BASE_URL = "https://api.clover.com/";//https://apisandbox.dev.clover.com/";
    private static final String CLOVER_URL = "https://api.clover.com/";//https://apisandbox.dev.clover.com/";
    private static final Logger LOGGER = LoggerFactory.getLogger(CloverServiceImpl.class);
    private static final String CLOVER = "Clover";

    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    @Inject
    private ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private CloverRepository cloverRepository;
    @Inject
    private SecurityUtil securityUtil;
    @Inject
    private CloverReceiptRepository cloverReceiptRepository;
    private CloverConfig cloverConfig = null;

    private static PublicKey getPublicKey(final BigInteger modulus, final BigInteger exponent) throws Exception {
        final KeyFactory factory = KeyFactory.getInstance("RSA");
        return factory.generatePublic(new RSAPublicKeySpec(modulus, exponent));
    }

    private static String encryptPAN(final String prefix, final String pan, PublicKey publicKey) throws Exception {
        byte[] input = String.format("%s%s", prefix, pan).getBytes();
        Security.addProvider(new BouncyCastleProvider());
        Cipher cipher = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey, new SecureRandom());
        byte[] cipherText = cipher.doFinal(input);
        return DatatypeConverter.printBase64Binary(cipherText);
    }

    @Override
    public CloverKeyResponse getCloverKey(String merchantId, String token) {
        String url = getBaseUrl() + "v2/merchant/" + merchantId.trim() + "/pay/key";
        return doGetToClover(url, CloverKeyResponse.class, token);
    }

    @Override
    public OrderResponse addOrder(OrderRequest request, String merchantId, String token) {
        String url = getBaseUrl() + "v3/merchants/" + merchantId.trim() + "/orders";
        return doPostToClover(url, request, OrderResponse.class, token);
    }

    @Override
    public CloverPaymentResponse processTransactionPayment(CloverPaymentRequest request, String merchantId, String token) {
        String url = getBaseUrl() + "v2/merchant/" + merchantId.trim() + "/pay";
        OrderRequest orderRequest = new OrderRequest();

        orderRequest.setTotal(request.getAmount());
        orderRequest.setCurrency(request.getCurrency());
        orderRequest.setTitle(request.getNote());
        orderRequest.setNote("TransId: " + request.getTransactionId());
        try {
            orderRequest.setCurrency("$");
            OrderResponse orderResponse = addOrder(orderRequest, merchantId, token);
            CloverKeyResponse cloverKeyResponse = getCloverKey(merchantId, token);
            final PublicKey publicKey = getPublicKey(new BigInteger(cloverKeyResponse.getModulus()), new BigInteger(cloverKeyResponse.getExponent()));
            final String ccEncrypted = encryptPAN(cloverKeyResponse.getPrefix(), request.getCardNumber(), publicKey);
            request.setOrderId(orderResponse.getId());
            request.setCardEncrypted(ccEncrypted);

            request.setCurrency("usd");
            //CloverPayRequest payRequest = request.toPayRequest();
            return doPostToClover(url, request, CloverPaymentResponse.class, token);
        } catch (Exception e) {
            LOGGER.info(String.format("Exception while processTransaction payment :%s", e.getMessage()));
            throw new BlazeOperationException(String.format("%s", e.getMessage()), e);
        }
    }

    private <T> T doGetToClover(String url, Class<T> returnClass, String token) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        headers.putSingle("Authorization", "Bearer " + token);
        LOGGER.info("Calling Clover API URL : {}", url);
        try {
            return SimpleRestUtil.get(url, returnClass, Clover.class, headers);
        } catch (RestErrorException ex) {
            LOGGER.info("Exception while get Clover Api:", ex);
            throw new BlazeOperationException(String.format("%s", ((Clover) ex.getErrorResponse()).getMessage()), ex);
        } catch (Exception ex) {
            LOGGER.info("Exception while get Clover Api:", ex);
            throw new BlazeOperationException(String.format("%s", ex.getMessage()), ex);
        }
    }

    private <T> T doPostToClover(String url, Object request, Class<T> returnClass, String token) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        headers.putSingle("Accept", MediaType.APPLICATION_JSON);
        headers.putSingle("Authorization", "Bearer " + token);
        LOGGER.info("Calling Clover API URL : {}", url);
        try {
            return SimpleRestUtil.post(url, request, returnClass, String.class, headers);

        } catch (RestErrorException ex) {
            if (request instanceof CloverPaymentRequest) {
                CloverPaymentRequest orderRequest = (CloverPaymentRequest) request;
                orderRequest.setCardNumber(null);
                String json = JsonSerializer.toJson(orderRequest);
                LOGGER.info("Clover:" + json);
            } else {
                String json = JsonSerializer.toJson(request);
                LOGGER.info("Clover:" + json);
            }
            String headersJson = JsonSerializer.toJson(headers);
            LOGGER.info("CloverHeader:" + headersJson);
            throw new BlazeOperationException(String.format("%s", (ex.getErrorResponse())), ex);
        } catch (Exception ex) {
            LOGGER.info("Exception while get Clover Api:", ex);
            throw new BlazeOperationException(String.format("%s", ex.getMessage()), ex);
        }
    }


    private String getBaseUrl() {
        /*if (cloverConfig == null) {
            cloverConfig = integrationSettingRepository.getCloverConfig(IntegrationSetting.Environment.Production);
        }
        if (cloverConfig != null) {
            return cloverConfig.getHost();
        }*/
        // default to production api
        return "https://api.clover.com/";
    }


    /**
     * @param transaction : transaction
     * @implNote This method is used to make payment using clover request.
     */
    @Override
    public void processPendingCloverPayments(Transaction transaction) {
        if (transaction.getCart().getPaymentOption() != Cart.PaymentOption.Clover) return;
        CloverPaymentResponse response;
        ThirdPartyAccount thirdPartyAccount = thirdPartyAccountRepository.findByAccountTypeByShopId(transaction.getCompanyId(), transaction.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Clover);

        if (Objects.isNull(thirdPartyAccount)) {
            throw new BlazeInvalidArgException("Clover", "Clover account is not configured");
            // return;
        }

        Shop shop = shopRepository.get(transaction.getCompanyId(), transaction.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Terminal", "Shop does not exist.");
            // return;
        }


        List<PaymentCardPayment> pendingPayments = transaction.getPaymentCardPayments()
                .stream()
                .filter(p -> p.getStatus() == PaymentCardPaymentStatus.Pending)
                .collect(Collectors.toList());

        List<Clover> clovers = new ArrayList<>();
        CloverRequest cloverRequest = new CloverRequest();
        List<CloverReceipt> cloverReceiptList = new ArrayList<>();
        // If theres pending payments, try to process them
        if (!pendingPayments.isEmpty()) {
            // Ensure transaction prereqs
            if (transaction.getShop() == null)
                transaction.setShop(shopRepository.get(transaction.getCompanyId(), transaction.getShopId()));
            if (transaction.getSeller() == null)
                transaction.setSeller(employeeRepository.get(transaction.getCompanyId(), transaction.getSellerId()));
            if (transaction.getMember() == null)
                transaction.setMember(memberRepository.get(transaction.getCompanyId(), transaction.getMemberId()));

            LOGGER.info("Looping through payment list...");
            for (PaymentCardPayment payment : pendingPayments) {
                BigDecimal amount = payment.getAmount();

                if (amount == null || (NumberUtils.round(amount, 2)) == 0) {
                    throw new BlazeInvalidArgException("Clover", "Credit Card payment failed. Please try again.");
                }

                int amountCents = (amount.multiply(new BigDecimal(100))).intValue();
                if (amountCents == 0) {
                    throw new BlazeInvalidArgException("Clover", "Credit Card payment failed. Please try again.");
                }
                payment.setType(PaymentCardType.Clover);
                payment.prepare();
                payment.setProcessedTime(org.joda.time.DateTime.now().getMillis());


                //prepare clover payment object
                LOGGER.info("Creating clover payment request");
                CloverPaymentRequest cloverPaymentRequest = new CloverPaymentRequest();
                cloverPaymentRequest.setTransactionId(transaction.getId());
                cloverPaymentRequest.setExpMonth(payment.cloverPaymentRequest().getExpMonth());
                cloverPaymentRequest.setCvv(payment.cloverPaymentRequest().getCvv());
                cloverPaymentRequest.setAmount(amountCents);
                //cloverPaymentRequest.setAmount(35);
                cloverPaymentRequest.setCurrency(payment.cloverPaymentRequest().getCurrency());
                cloverPaymentRequest.setAccoutLastFourNumber(payment.cloverPaymentRequest().getAccoutLastFourNumber());
                cloverPaymentRequest.setExpYear(payment.cloverPaymentRequest().getExpYear());
                cloverPaymentRequest.setAccoutFirstSixNumber(payment.cloverPaymentRequest().getAccoutFirstSixNumber());
                cloverPaymentRequest.setCardNumber(payment.cloverPaymentRequest().getCardNumber());
                cloverPaymentRequest.setNote(String.format("%s: #%s", shop.getName(), transaction.getTransNo()));

                //prepare clover request
                cloverRequest.setExpMonth(payment.cloverPaymentRequest().getExpMonth());
                cloverRequest.setCvv(payment.cloverPaymentRequest().getCvv());
                cloverRequest.setAmount(BigDecimal.valueOf(payment.cloverPaymentRequest().getAmount()));
                cloverRequest.setCurrency(payment.cloverPaymentRequest().getCurrency());
                cloverRequest.setAccoutFirstSixNumber(payment.cloverPaymentRequest().getAccoutFirstSixNumber());
                cloverRequest.setAccoutLastFourNumber(payment.cloverPaymentRequest().getAccoutLastFourNumber());
                cloverRequest.setExpYear(payment.cloverPaymentRequest().getExpYear());
                cloverRequest.setCardNumber(payment.cloverPaymentRequest().getCardNumber());

                try {
                    boolean reachable = true;
                    if (reachable) {
                        LOGGER.info("Processing Clover Payment..: " + transaction.getTransNo());
                        response = processTransactionPayment(cloverPaymentRequest, thirdPartyAccount.getMerchantId(), thirdPartyAccount.getToken());
                        LOGGER.info("Processing Clover Payment..Response: " + transaction.getTransNo() + " Result: " + (response != null ? response.getResult() : "No Response"));
                        if (Objects.nonNull(response) && Objects.nonNull(response.getPaymentId()) && response.getResult().equalsIgnoreCase("APPROVED")) {
                            CloverReceipt cloverReceipt = new CloverReceipt();
                            cloverReceipt.prepare(transaction.getCompanyId());
                            cloverReceipt.setShopId(transaction.getShopId());
                            cloverReceipt.setCloverTxId(response.getPaymentId());
                            cloverReceipt.setTransactionId(transaction.getId());
                            cloverReceipt.setAmount(amount);
                            cloverReceipt.setReceiptType(CloverReceipt.ReceiptType.CLOVER);
                            cloverReceipt.setTimestamp(DateTime.now().getMillis());
                            cloverReceipt.setLastFour(payment.cloverPaymentRequest().getAccoutLastFourNumber());
                            cloverReceipt.setPaymentId(payment.getId());
                            cloverReceiptList.add(cloverReceipt);
                            payment.setPaymentId(response.getPaymentId());
                            payment.setStatus(PaymentCardPaymentStatus.Paid);

                        } else {
                            transaction.setStatus(Transaction.TransactionStatus.Hold);
                            transaction.setActive(true);
                            clovers.add(prepareClover(cloverRequest, transaction.getShopId(), transaction.getCompanyId(), response.getResult(), Clover.SyncStatus.ERROR, payment.getId()));
                        }
                    } else {
                        LOGGER.error("CLOVER NOT REACHABLE..");
                        clovers.add(prepareClover(cloverRequest, transaction.getShopId(), transaction.getCompanyId(), StringUtils.EMPTY, Clover.SyncStatus.INPROGRESS, payment.getId()));
                        response = new CloverPaymentResponse();
                        transaction.setStatus(Transaction.TransactionStatus.Hold);
                        transaction.setActive(true);
                        payment.setStatus(PaymentCardPaymentStatus.Pending);
                        response.setResult("APPROVED");
                    }
                } catch (Exception e) {
                    LOGGER.error("Error processing clover payment: " + e.getMessage(), e);
                    throw new BlazeOperationException(String.format("Exception while processTransaction payment :%s", e.getMessage()), e);
                }
            }
        } else {
            LOGGER.error("Error processing clover payment: Payment is empty");
            // There was no payments, even though this was a Clover transaction. Throw error
            throw new BlazeOperationException("You must include a clover payment when paying with the clover payment type.");
        }
        // Ensure transaction is paid for
        double paymentCardTotalPaid = transaction.getPaymentCardPayments()
                .stream()
                .filter(p -> p.getStatus() == PaymentCardPaymentStatus.Paid)
                .mapToDouble(p -> p.getAmount().doubleValue())
                .sum();

        if (paymentCardTotalPaid < transaction.getCart().getTotal().doubleValue()) {
            transaction.setStatus(Transaction.TransactionStatus.Hold);
            transaction.setActive(true);
            throw new BlazeOperationException("Transaction not paid.");
        }
        cloverRepository.save(clovers);
        cloverReceiptRepository.save(cloverReceiptList);
        transactionRepository.update(transaction.getId(), transaction);

    }


    /**
     * @param targetUrl : Url
     * @return : Booleam
     * @throws IOException : IOException class object
     * @implNote This method is used to check wheather net is connected or not if net is connected then return true otherwise return false.
     */
    public static boolean isReachable(String targetUrl) throws IOException {
        HttpURLConnection httpUrlConnection = (HttpURLConnection) new URL(
                targetUrl).openConnection();
        httpUrlConnection.setRequestMethod("HEAD");
        try {
            int responseCode = httpUrlConnection.getResponseCode();
            return responseCode == HttpURLConnection.HTTP_NO_CONTENT;
        } catch (UnknownHostException noInternetConnection) {
            return false;
        }
    }


    /**
     * @param request   : CloverRequest
     * @param shopId    : String
     * @param companyId : String
     * @param message   ; String
     * @param sync      : Clover.SyncStatus
     * @return : Clover
     * @implNote : This method is use to CloverRequest class get value and set in Clover class object
     */
    public Clover prepareClover(CloverRequest request, String shopId, String companyId, String message, Clover.SyncStatus sync, String cardpaymentId) {
        Clover clover = new Clover();
        clover.prepare(companyId);
        clover.setExpMonth(securityUtil.encryptClover(String.valueOf(request.getExpMonth())));
        clover.setCvv(securityUtil.encryptClover(request.getCvv()));
        clover.setAmount(request.getAmount());
        clover.setCurrency(request.getCurrency());
        clover.setAccoutLastFourNumber(securityUtil.encryptClover(request.getAccoutLastFourNumber()));
        clover.setExpYear(request.getExpYear());
        clover.setAccoutFirstSixNumber(securityUtil.encryptClover(request.getAccoutFirstSixNumber()));
        clover.setCardNumber(securityUtil.encryptClover(request.getCardNumber()));
        clover.setTransactionId(request.getTransactionId());
        clover.setSyncStatus(sync);
        clover.setShopId(shopId);
        clover.setMessage(message);
        clover.setCardPaymentId(cardpaymentId);
        return clover;
    }

    @Override
    public void prepareTransactionByJob() {
        List<Clover> cloverList = cloverRepository.getAllProgressCloverTransaction(Clover.SyncStatus.INPROGRESS);
        List<ObjectId> transactionIds = new ArrayList<>();

        for (Clover clover : cloverList) {
            transactionIds.add(new ObjectId(clover.getTransactionId()));
        }
        HashMap<String, Transaction> transactionHashMap = transactionRepository.findItemsInAsMap(transactionIds);
        prepareTransactionByJob(cloverList, transactionHashMap);

    }

    private void prepareTransactionByJob(List<Clover> cloverList, HashMap<String, Transaction> transactionHashMap) {
        List<CloverReceipt> cloverReceiptList = new ArrayList<>();

        HashMap<String, ThirdPartyAccount> accountsMap = new HashMap<>();
        LOGGER.info("Process clover receipts for pending transacions...");
        for (Clover clover : cloverList) {
            Transaction transaction = transactionHashMap.get(clover.getTransactionId());

            if (transaction == null) {
                LOGGER.info("Transaction does not exist.");
                continue;
            }
            LOGGER.info("Process pending receipts for transaction #:" + transaction.getTransNo());

            ThirdPartyAccount thirdPartyAccount = null;
            if (accountsMap.containsKey(clover.getShopId())) {
                thirdPartyAccount = accountsMap.get(clover.getShopId());
            }
            if (thirdPartyAccount == null) {
                thirdPartyAccount = thirdPartyAccountRepository.findByAccountTypeByShopId(clover.getCompanyId(), clover.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Clover);
            }

            if (Objects.isNull(thirdPartyAccount)) {
                throw new BlazeInvalidArgException(CLOVER, "Clover account is not configured");
            }

            accountsMap.putIfAbsent(thirdPartyAccount.getShopId(), thirdPartyAccount);

            HashMap<String, PaymentCardPayment> pendingPaymentMap = new HashMap<>();
            if (!CollectionUtils.isNullOrEmpty(transaction.getPaymentCardPayments())) {
                transaction.getPaymentCardPayments().forEach(paymentCardPayment -> pendingPaymentMap.put(paymentCardPayment.getId(), paymentCardPayment));
            }

            int amountCents = (clover.getAmount().multiply(new BigDecimal(100))).intValue();

            CloverPaymentRequest cloverRequest = new CloverPaymentRequest();
            cloverRequest.setExpMonth(Integer.parseInt(securityUtil.dencryptClover(clover.getExpMonth())));
            cloverRequest.setCvv(securityUtil.dencryptClover(clover.getCvv()));
            cloverRequest.setAmount(amountCents);
            cloverRequest.setCurrency(clover.getCurrency());
            cloverRequest.setAccoutLastFourNumber(securityUtil.dencryptClover(clover.getAccoutLastFourNumber()));
            cloverRequest.setExpYear(clover.getExpYear());
            cloverRequest.setAccoutFirstSixNumber(securityUtil.dencryptClover(clover.getAccoutFirstSixNumber()));
            cloverRequest.setCardNumber(securityUtil.dencryptClover(clover.getCardNumber()));
            cloverRequest.setTransactionId(clover.getTransactionId());

            CloverPaymentResponse response = processTransactionPayment(cloverRequest, thirdPartyAccount.getMerchantId(), thirdPartyAccount.getToken());
            if (Objects.nonNull(response) && Objects.nonNull(response.getPaymentId()) && response.getResult().equalsIgnoreCase("APPROVED")) {
                transactionRepository.saveCloverTxn(clover.getCompanyId(), clover.getShopId(), cloverRequest.getTransactionId(), response.getPaymentId());
                clover.setSyncStatus(Clover.SyncStatus.COMPLETED);
                cloverRepository.update(clover.getId(), clover);
                CloverReceipt cloverReceipt = new CloverReceipt();
                cloverReceipt.prepare(clover.getCompanyId());
                cloverReceipt.setShopId(clover.getShopId());
                cloverReceipt.setCloverTxId(response.getPaymentId());
                cloverReceipt.setTransactionId(clover.getTransactionId());
                cloverReceipt.setAmount(clover.getAmount());
                cloverReceipt.setReceiptType(CloverReceipt.ReceiptType.CLOVER);
                cloverReceipt.setTimestamp(DateTime.now().getMillis());

                cloverReceipt.setLastFour(clover.getAccoutLastFourNumber());
                cloverReceipt.setPaymentId(clover.getCardPaymentId());
                cloverReceiptList.add(cloverReceipt);

                if (pendingPaymentMap.containsKey(clover.getCardPaymentId())) {
                    PaymentCardPayment payment = pendingPaymentMap.get(clover.getCardPaymentId());
                    payment.setProcessedTime(DateTime.now().getMillis());
                    payment.setPaymentId(response.getPaymentId());
                    payment.setStatus(PaymentCardPaymentStatus.Paid);
                }
            } else {
                clover.setSyncStatus(Clover.SyncStatus.ERROR);
                clover.setMessage(response.getResult());
                cloverRepository.update(clover.getId(), clover);
                if (pendingPaymentMap.containsKey(clover.getCardPaymentId())) {
                    PaymentCardPayment payment = pendingPaymentMap.get(clover.getCardPaymentId());
                    payment.setProcessedTime(DateTime.now().getMillis());
                    payment.setStatus(PaymentCardPaymentStatus.Declined);
                }
            }
            transactionRepository.update(transaction.getId(), transaction);
        }

        cloverReceiptRepository.save(cloverReceiptList);
    }

    @Override
    public void processCloverReceipts(Transaction dbTransaction) {
        // check if any pending pending
        Iterable<Clover> clovers = cloverRepository.getAllProgressCloverTransaction(dbTransaction.getCompanyId(), dbTransaction.getShopId(), dbTransaction.getId(), Clover.SyncStatus.INPROGRESS);
        HashMap<String, Transaction> transactionHashMap = new HashMap<>();
        transactionHashMap.putIfAbsent(dbTransaction.getId(), dbTransaction);
        LOGGER.info("Clover receipt process starts for transaction.." + dbTransaction.getTransNo());
        prepareTransactionByJob(Lists.newArrayList(clovers), transactionHashMap);
        LOGGER.info("Clover receipt process ends for transaction #.." + dbTransaction.getTransNo());

        // Now check if any transaction is paid
        Iterable<CloverReceipt> cloverReceipts = cloverReceiptRepository.getCloverReceiptsTransaction(dbTransaction.getId());

        double receiptTotal = Lists.newArrayList(cloverReceipts).stream().mapToDouble(value -> value.getAmount().doubleValue()).sum();

        LOGGER.info("Clover receipt total amount.." + receiptTotal + " and cart total is.." + dbTransaction.getCart().getTotal());

        BigDecimal cartTotal = dbTransaction.getCart().getTotal();
        if (cartTotal.doubleValue() > receiptTotal) {
            throw new BlazeOperationException("Transaction not paid.");
        }
        LOGGER.info("Transaction # "+ dbTransaction.getTransNo() + " paid successfully...");
    }
}
