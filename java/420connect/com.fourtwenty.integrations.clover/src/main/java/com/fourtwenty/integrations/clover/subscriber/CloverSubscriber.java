package com.fourtwenty.integrations.clover.subscriber;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.receipt.CloverReceipt;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.payment.CloverReceiptRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.clover.CloverEvent;
import com.fourtwenty.core.event.clover.CloverJobEvent;
import com.fourtwenty.core.event.transaction.CloverPaymentCardsForTransactionEvent;
import com.fourtwenty.core.event.transaction.ProcessLoyaltyCardsForTransactionResult;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.paymentcard.clover.CloverRequest;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.integrations.clover.model.Clover;
import com.fourtwenty.integrations.clover.repository.CloverRepository;
import com.fourtwenty.integrations.clover.request.CloverPaymentRequest;
import com.fourtwenty.integrations.clover.response.CloverPaymentResponse;
import com.fourtwenty.integrations.clover.service.CloverService;
import com.fourtwenty.integrations.clover.service.impl.CloverServiceImpl;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Objects;

public class CloverSubscriber implements BlazeSubscriber {
    private static final String CLOVER = "Clover";
    private static final String CLOVER_URL = "https://apisandbox.dev.clover.com/";
    private static final Log LOG = LogFactory.getLog(CloverSubscriber.class);

    @Inject
    private ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private CloverService cloverService;
    @Inject
    private CloverRepository cloverRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private CloverReceiptRepository cloverReceiptRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private CloverServiceImpl cloverServiceImp;


    /**
     *
     * @param event : CloverEvent
     * @implNote This method call CloverResource to use for payment via clover.
     */
    @Subscribe
    public void prepareTransaction(CloverEvent event) {
        if (event == null) {
            new CloverEvent().throwException(new BlazeInvalidArgException(CLOVER, "Credit Card payment failed. Please try again."));
            return;
        }
        ThirdPartyAccount thirdPartyAccount = thirdPartyAccountRepository.findByAccountTypeByShopId(event.getCompanyId(), event.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Clover);
        if (thirdPartyAccount == null) {
            event.throwException(new BlazeInvalidArgException(CLOVER, "Clover account is not configured"));
            return;
        }

        Shop shop = shopRepository.get(event.getCompanyId(),event.getShopId());
        if (shop == null) {
            event.throwException(new BlazeInvalidArgException("Terminal","Shop does not exist."));
            return;
        }

        Transaction dbTrans = transactionRepository.get(event.getCompanyId(), event.getCloverRequest().getTransactionId());
        if (dbTrans == null) {
            event.throwException(new BlazeInvalidArgException("Terminal","Transaction does not exist."));
            return;
        }
/*
        Terminal sellerTerminal = terminalRepository.getTerminalById(event.getCompanyId(), sellerTerminalId);
        if (sellerTerminal == null || sellerTerminal.isDeleted() || sellerTerminal.isActive() == false) {
            event.throwException(new BlazeInvalidArgException("Terminal",
                    String.format("Terminal '%s' is no longer active.",
                            sellerTerminal == null ? "" : sellerTerminal.getName())));
            return;
        }

        if (shop != null) {
            // make sure the current terminal has a cash drawer
            if (shop.isEnforceCashDrawers()) {
                long nowMillis = DateTime.now().getMillis();
                String todayDate = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone());
                String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyyMMdd");

                CashDrawerSession dbLogResult = cashDrawerSessionRepository.getCashDrawerForDate(event.getCompanyId(),
                        shop.getId(), sellerTerminalId, todayFormatted);

                if (dbLogResult == null) {
                    event.throwException(new BlazeInvalidArgException("Terminal",
                            String.format("Please create a cash drawer for today's date, %s, for terminal '%s' before completing your sale.",
                                    todayDate,
                                    sellerTerminal.getName())));
                    return;
                } else if (dbLogResult != null && dbLogResult.getStatus() == CashDrawerSession.CashDrawerLogStatus.Closed) {
                    event.throwException(new BlazeInvalidArgException("Terminal",
                            String.format("Please open a cash drawer for today's date, %s, for terminal '%s' before completing your sale.",
                                    todayDate,
                                    sellerTerminal.getName())));
                    return;
                }
            }
        }*/


        CloverRequest request = event.getCloverRequest();

        BigDecimal amount = null;
        if (request != null) {
            if (StringUtils.isNotBlank(request.getTransactionId())) {
                // Find latest transaction (In-Progress)
                QueuedTransaction queuedTransaction = queuedTransactionRepository.getRecentQueuedTransactionModified(event.getCompanyId(), event.getShopId(), request.getTransactionId());
                if (queuedTransaction != null) {
                    amount = queuedTransaction.getCart().getTotal();
                }
            }
        }
        if (request != null && (amount == null || (NumberUtils.round(amount,2)) == 0)) {
            amount = request.getAmount();
        }

        if (amount == null || (NumberUtils.round(amount,2)) == 0) {
            event.throwException(new BlazeInvalidArgException(CLOVER, "Credit Card payment failed. Please try again."));
            return;
        }

        int amountCents = (amount.multiply(new BigDecimal(100))).intValue();
        if (amountCents == 0) {
            event.throwException(new BlazeInvalidArgException(CLOVER, "Credit Card payment failed. Please try again."));
        }


        CloverPaymentRequest cloverPaymentRequest = new CloverPaymentRequest();
        cloverPaymentRequest.setTransactionId(request.getTransactionId());
        cloverPaymentRequest.setExpMonth(request.getExpMonth());
        cloverPaymentRequest.setCvv(request.getCvv());
        cloverPaymentRequest.setAmount(amountCents);
        //cloverPaymentRequest.setAmount(35);
        cloverPaymentRequest.setCurrency(request.getCurrency());
        cloverPaymentRequest.setAccoutLastFourNumber(request.getAccoutLastFourNumber());
        cloverPaymentRequest.setExpYear(request.getExpYear());
        cloverPaymentRequest.setAccoutFirstSixNumber(request.getAccoutFirstSixNumber());
        cloverPaymentRequest.setCardNumber(request.getCardNumber());
        cloverPaymentRequest.setNote(String.format("%s: #%s",shop.getName(),dbTrans.getTransNo()));

        try {
            if (CloverServiceImpl.isReachable(CLOVER_URL)) {
                CloverPaymentResponse response = cloverService.processTransactionPayment(cloverPaymentRequest, thirdPartyAccount.getMerchantId(), thirdPartyAccount.getToken());
                if (Objects.nonNull(response) && Objects.nonNull(response.getPaymentId()) && response.getResult().equalsIgnoreCase("APPROVED")) {
                    transactionRepository.saveCloverTxn(event.getCompanyId(), event.getShopId(), request.getTransactionId(), response.getPaymentId());
                    event.setResponse(response);

                    CloverReceipt cloverReceipt = new CloverReceipt();
                    cloverReceipt.prepare(event.getCompanyId());
                    cloverReceipt.setShopId(event.getShopId());
                    cloverReceipt.setCloverTxId(response.getPaymentId());
                    cloverReceipt.setTransactionId(request.getTransactionId());
                    cloverReceipt.setAmount(amount);
                    cloverReceipt.setReceiptType(CloverReceipt.ReceiptType.CLOVER);
                    cloverReceipt.setTimestamp(DateTime.now().getMillis());
                    cloverReceipt.setLastFour(request.getAccoutLastFourNumber());
                    cloverReceiptRepository.save(cloverReceipt);
                } else {
                    event.setResponse(response);
                    cloverRepository.save(cloverServiceImp.prepareClover(request, event.getShopId(), event.getCompanyId(), response.getResult(), Clover.SyncStatus.ERROR, ""));
                }

            } else {
                cloverRepository.save(cloverServiceImp.prepareClover(request, event.getShopId(), event.getCompanyId(), StringUtils.EMPTY, Clover.SyncStatus.INPROGRESS, ""));
                CloverPaymentResponse response = new CloverPaymentResponse();
                response.setResult("APPROVED");
                event.setResponse(response);
            }

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    /**
     *
     * @param event : CloverJobEvent
     * @implNote : This method call CloverJob every 5 minutes.Get data from Db and send to Clover
     */

    @Subscribe
    public void prepareTransactionByJob(CloverJobEvent event) {
        cloverService.prepareTransactionByJob();
    }

    /**
     * For transaction CloverPaymentCardsForTransactionEvent
     * @param event : payment event

     */
    @Subscribe
    public void subscribe(CloverPaymentCardsForTransactionEvent event) {
        try {
            Transaction dbTransaction = event.getDbTransaction();
            Transaction requestTransaction = event.getRequestTransaction();

            LOG.info("dbTransaction.cloverPayments: " + dbTransaction.getPaymentCardPayments());
            LOG.info("requestTransaction.cloverPayments: " + requestTransaction.getPaymentCardPayments());

            if (requestTransaction.getCart().getPaymentOption() == Cart.PaymentOption.Clover) {
                if (CollectionUtils.isNullOrEmpty(requestTransaction.getPaymentCardPayments())) {
                    // check if already paid
                    LOG.info(String.format("Payment not found.Check if already paid %s", requestTransaction.getTransNo()));
                    cloverService.processCloverReceipts(dbTransaction);
                    dbTransaction.addPaymentCardPayments(requestTransaction.getPaymentCardPayments());
                    dbTransaction.getCart().setPaymentOption(requestTransaction.getCart().getPaymentOption());
                    dbTransaction.getCart().setTotal(requestTransaction.getCart().getTotal());

                } else {
                    LOG.info(String.format("Payment found.Process pending payments %s", requestTransaction.getTransNo()));
                    for (PaymentCardPayment paymentCard : requestTransaction.getPaymentCardPayments()) {
                        if (paymentCard.getStatus() == null) {
                            paymentCard.setStatus(PaymentCardPaymentStatus.Pending);
                        }
                    }
                    dbTransaction.addPaymentCardPayments(requestTransaction.getPaymentCardPayments());
                    dbTransaction.getCart().setPaymentOption(requestTransaction.getCart().getPaymentOption());
                    dbTransaction.getCart().setTotal(requestTransaction.getCart().getTotal());

                    cloverService.processPendingCloverPayments(dbTransaction);
                    event.setResponse(new ProcessLoyaltyCardsForTransactionResult(dbTransaction));
                }
            }

        } catch (Exception e) {
            String transId = "";
            if (event != null && event.getDbTransaction() != null) {
                transId = event.getDbTransaction().getId();
            }
            LOG.error(String.format("Error Payment found. Error processing payments %s: %s", transId, e.getMessage()),e);
            event.throwException(new BlazeInvalidArgException(CLOVER, e.getMessage()));

        }
    }

}
