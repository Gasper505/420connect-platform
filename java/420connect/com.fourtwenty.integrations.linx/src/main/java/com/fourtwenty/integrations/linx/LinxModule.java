package com.fourtwenty.integrations.linx;

import com.fourtwenty.integrations.linx.repository.LinxAccountRepository;
import com.fourtwenty.integrations.linx.repository.impl.LinxAccountRepositoryImpl;
import com.fourtwenty.integrations.linx.service.LinxCommunicatorService;
import com.fourtwenty.integrations.linx.service.LinxService;
import com.fourtwenty.integrations.linx.service.impl.LinxCommunicatorServiceImpl;
import com.fourtwenty.integrations.linx.service.impl.LinxServiceImpl;
import com.google.inject.AbstractModule;


public class LinxModule extends AbstractModule {
    @Override
    protected void configure() {
        // Repositories
        bind(LinxAccountRepository.class).to(LinxAccountRepositoryImpl.class);

        // Services
        bind(LinxCommunicatorService.class).to(LinxCommunicatorServiceImpl.class);
        bind(LinxService.class).to(LinxServiceImpl.class);
    }
}
