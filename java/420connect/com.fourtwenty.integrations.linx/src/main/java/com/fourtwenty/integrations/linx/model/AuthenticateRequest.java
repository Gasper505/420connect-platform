package com.fourtwenty.integrations.linx.model;

import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;

public class AuthenticateRequest {
    private String accessToken;
    private String secret;
    private String grantType;

    public AuthenticateRequest(LinxAccount linxAccount) {
        accessToken = linxAccount.getAccessToken();
        secret = linxAccount.getSecret();
        accessToken = linxAccount.getAccessToken();
        grantType = "client_credentials";
    }


    public AuthenticateRequest(String accessToken, String secret, String grantType) {
        this.accessToken = accessToken;
        this.secret = secret;
        this.grantType = grantType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public String getGrantType() {
        return grantType;
    }
}
