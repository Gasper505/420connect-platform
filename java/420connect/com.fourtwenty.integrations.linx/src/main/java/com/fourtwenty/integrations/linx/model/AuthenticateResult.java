package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticateResult {
    private static final int MILISECONDS_BEFORE_EXPIRE = 20000;

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("expires_in")
    private int expiresIn;
    @JsonProperty("created_at")
    private long createdAt;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public void validate() {

        int expiresMs = getExpiresIn() * 1000;
        long createdAtMs = getCreatedAt() * 1000;
        Date expires = new Date(createdAtMs + expiresMs - MILISECONDS_BEFORE_EXPIRE);
        Date now = new Date();

        if (StringUtils.isBlank(getAccessToken()))
            throw new BlazeOperationException("Linx Auth Token is empty.");

//        if (now.after(expires))
//            throw new BlazeOperationException("Linx Auth Token has expired.");
    }
}
