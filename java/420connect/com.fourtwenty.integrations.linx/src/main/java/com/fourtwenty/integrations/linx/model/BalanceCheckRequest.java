package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckEvent;
import com.fourtwenty.core.security.IAuthToken;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;

public class BalanceCheckRequest {
    @JsonIgnore
    private IAuthToken token;
    @JsonIgnore
    private String shopId;

    @JsonIgnore
    private AuthenticateResult authenticateResult;

    public String linxCardNumber;

    public BalanceCheckRequest() {
    }

    public BalanceCheckRequest(PaymentCardBalanceCheckEvent event) {
        token = event.getToken();
        linxCardNumber = event.getPaymentCardNumber();
    }

    public IAuthToken getToken() {
        return token;
    }

    public void setToken(ConnectAuthToken token) {
        this.token = token;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public AuthenticateResult getAuthenticateResult() {
        return authenticateResult;
    }

    public void setAuthenticateResult(AuthenticateResult authenticateResult) {
        this.authenticateResult = authenticateResult;
    }

    public String getLinxCardNumber() {
        return linxCardNumber;
    }

    public void setLinxCardNumber(String linxCardNumber) {
        this.linxCardNumber = linxCardNumber;
    }
}
