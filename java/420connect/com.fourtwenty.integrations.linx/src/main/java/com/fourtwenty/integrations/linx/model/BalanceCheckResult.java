package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckResult;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BalanceCheckResult extends LinxResult {
    @JsonProperty("card_status")
    private String cardStatus;

    @JsonProperty("balance")
    private String balance;

    @JsonProperty("expired")
    private boolean expired;


    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public PaymentCardBalanceCheckResult asPaymentCardBalanceCheckResult() {
        return new PaymentCardBalanceCheckResult(getCardStatus(), new BigDecimal(getBalance().substring(1)));
    }
}
