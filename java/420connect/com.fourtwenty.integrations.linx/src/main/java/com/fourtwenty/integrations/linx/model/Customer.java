package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer extends NamedEntity {
    @JsonProperty("type")
    private String type;

    @JsonProperty("id_number")
    private String idNumber;

    @JsonProperty("state")
    private String state;


    public Customer() {
        super();
    }

    public Customer(String name, String type, String idNumber, String state) {
        super(name);
        this.type = type;
        this.idNumber = idNumber;
        this.state = state;
    }



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
