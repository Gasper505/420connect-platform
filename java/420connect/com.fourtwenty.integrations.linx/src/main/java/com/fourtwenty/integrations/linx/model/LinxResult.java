package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinxResult {
    @JsonIgnore
    private long processedDate;

    @JsonProperty("success")
    private boolean success;

    @JsonProperty("message")
    private String message;

    @JsonProperty("error_description")
    private List<String> errorDescription;


    public LinxResult() {
        this.processedDate = new Date().getTime();
    }

    public void setProcessedDate(long processedDate) {
        this.processedDate = processedDate;
    }

    public long getProcessedDate() {
        return processedDate;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message != null ? message :
                errorDescription != null ? String.join(",", errorDescription) :
                        null;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
