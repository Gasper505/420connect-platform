package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardEvent;
import com.fourtwenty.core.security.IAuthToken;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadLinxCardRequest {
    @JsonIgnore
    private IAuthToken token;

    @JsonIgnore
    private AuthenticateResult authenticateResult;

    @JsonProperty("card_number")
    private String cardNumber;

    @JsonProperty("amount")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    private BigDecimal amount;

    @JsonProperty("payment_method")
    private String paymentMethod = "credit_card";

    @JsonProperty("transaction_reference")
    private String transactionReference = "";

    @JsonProperty("credit_card")
    private CreditCard creditCard;

    // Encrypted (DUKPT) support
    @JsonIgnore
    private byte[] ksn;
    @JsonIgnore
    private byte[] encryptedTrack1;
    @JsonIgnore
    private byte[] encryptedTrack2;
    @JsonIgnore
    private byte[] encryptedTrack3;

    public LoadLinxCardRequest() {
    }

    public LoadLinxCardRequest(AuthenticateResult authenticateResult) {
        this.authenticateResult = authenticateResult;
    }

    public LoadLinxCardRequest(LoadPaymentCardEvent event) {
        setToken(event.getToken());
        setCardNumber(event.getPaymentCardNumber());
        setAmount(event.getAmount());

        if (event.getCreditCard() != null) {
            creditCard = new CreditCard();
            creditCard.setName(event.getCreditCard().getName());
            creditCard.setNumber(event.getCreditCard().getNumber());
            creditCard.setCvv(event.getCreditCard().getCvv());
            creditCard.setExpiryMonth(event.getCreditCard().getExpirationMonth());
            creditCard.setExpiryYear(event.getCreditCard().getExpirationYear());
            creditCard.setZipCode(event.getCreditCard().getZipCode());

            if (StringUtils.isNotBlank(event.getCreditCard().getExpiration_month())) {
                creditCard.setExpiryMonth(event.getCreditCard().getExpiration_month());
                creditCard.setExpiryYear(event.getCreditCard().getExpiration_year());
                creditCard.setZipCode(event.getCreditCard().getZip_code());
            }
        }
    }

    public IAuthToken getToken() {
        return token;
    }

    public void setToken(IAuthToken token) {
        this.token = token;
    }

    public AuthenticateResult getAuthenticateResult() {
        return authenticateResult;
    }

    public void setAuthenticateResult(AuthenticateResult authenticateResult) {
        this.authenticateResult = authenticateResult;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public byte[] getKsn() {
        return ksn;
    }

    public void setKsn(byte[] ksn) {
        this.ksn = ksn;
    }

    public byte[] getEncryptedTrack1() {
        return encryptedTrack1;
    }

    public void setEncryptedTrack1(byte[] encryptedTrack1) {
        this.encryptedTrack1 = encryptedTrack1;
    }

    public byte[] getEncryptedTrack2() {
        return encryptedTrack2;
    }

    public void setEncryptedTrack2(byte[] encryptedTrack2) {
        this.encryptedTrack2 = encryptedTrack2;
    }

    public byte[] getEncryptedTrack3() {
        return encryptedTrack3;
    }

    public void setEncryptedTrack3(byte[] encryptedTrack3) {
        this.encryptedTrack3 = encryptedTrack3;
    }
}
