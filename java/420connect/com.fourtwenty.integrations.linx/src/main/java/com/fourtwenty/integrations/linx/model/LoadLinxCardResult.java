package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardResult;

import java.math.BigDecimal;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadLinxCardResult extends LinxResult {
    @JsonProperty("id")
    private String id;

    @JsonProperty("status")
    private LoadStatus status;

    @JsonProperty("total_paid")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    private BigDecimal totalPaid;

    @JsonProperty("total_value")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    private BigDecimal totalValue;

    @JsonProperty("total_fees")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    private BigDecimal totalFees;

    @JsonProperty("created_at")
    private Date createdAt;

    @JsonProperty("transaction_reference")
    private String transactionReference;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LoadStatus getStatus() {
        return status;
    }

    public void setStatus(LoadStatus status) {
        this.status = status;
    }

    public BigDecimal getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(BigDecimal totalPaid) {
        this.totalPaid = totalPaid;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public BigDecimal getTotalFees() {
        return totalFees;
    }

    public void setTotalFees(BigDecimal totalFees) {
        this.totalFees = totalFees;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public LoadPaymentCardResult asLoadLoyaltyCardResult() {
        return new LoadPaymentCardResult(getTotalValue(), getTotalFees(), getTotalPaid());
    }
}
