package com.fourtwenty.integrations.linx.model;

public enum LoadStatus {
    paid,
    failed,
    pending,
    created
}
