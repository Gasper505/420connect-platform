package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NamedEntity {
    @JsonProperty("name")
    private String name;

    public NamedEntity() {
    }

    public NamedEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
