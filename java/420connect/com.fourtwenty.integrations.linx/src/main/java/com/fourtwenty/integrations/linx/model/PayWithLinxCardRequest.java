package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PayWithLinxCardRequest {
    @JsonIgnore
    private AuthenticateResult authenticateResult;

    @JsonProperty("linx_card_number")
    private String linxCardNumber;

    @JsonProperty("customer")
    private Customer customer;

    @JsonProperty("product_type")
    private ProductType productType;

    @JsonProperty("store_location")
    private NamedEntity storeLocation;

    @JsonProperty("budtender")
    private NamedEntity budtender;

    @JsonProperty("amount")
    private String amount;

    public PayWithLinxCardRequest() {
    }

    public PayWithLinxCardRequest(AuthenticateResult authenticateResult) {
        this.authenticateResult = authenticateResult;
    }

    public PayWithLinxCardRequest(Transaction transaction, PaymentCardPayment payment) {
        overrideFieldsFrom(transaction);

        linxCardNumber = payment.getPaymentCardNumber();
    }

    public AuthenticateResult getAuthenticateResult() {
        return authenticateResult;
    }

    public void setAuthenticateResult(AuthenticateResult authenticateResult) {
        this.authenticateResult = authenticateResult;
    }

    public String getLinxCardNumber() {
        return linxCardNumber;
    }

    public void setLinxCardNumber(String linxCardNumber) {
        this.linxCardNumber = linxCardNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public NamedEntity getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(NamedEntity storeLocation) {
        this.storeLocation = storeLocation;
    }

    public NamedEntity getBudtender() {
        return budtender;
    }

    public void setBudtender(NamedEntity budtender) {
        this.budtender = budtender;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    public void validate() {
        if (StringUtils.isBlank(getLinxCardNumber()))
            throw new BlazeOperationException("Linx card not submitted.");


        try {
            Double d = Double.parseDouble(getAmount());
            System.out.println(String.format("amount: %s, d: %.2f",getAmount(),d));
            int amt = (int) (d * 100);
            if (amt <= 0) {
                throw new BlazeOperationException("Linx charge amount not valid.");
            }
        } catch (Exception e) {
            throw new BlazeOperationException("Linx charge amount not valid.");
        }
        if (getBudtender() == null || StringUtils.isBlank(getBudtender().getName()))
            throw new BlazeOperationException("Budtender name is not set.");

        if (getCustomer() == null || StringUtils.isBlank(getCustomer().getName()))
            throw new BlazeOperationException("Customer name is not set.");

        if (getCustomer() == null || StringUtils.isBlank(getCustomer().getIdNumber()))
            throw new BlazeOperationException("Customer ID is not set.");

        if (getCustomer() == null || StringUtils.isBlank(getCustomer().getType()))
            throw new BlazeOperationException("Customer ID Type is not set.");

        if (getCustomer() == null || StringUtils.isBlank(getCustomer().getState()))
            throw new BlazeOperationException("Customer ID State is not set.");

        if (getStoreLocation() == null || StringUtils.isBlank(getStoreLocation().getName()))
            throw new BlazeOperationException("Store location name is not set.");
    }

    public void overrideFieldsFrom(Transaction transaction) {
        if (transaction.getSeller() != null) {
            String employeeName = String.format("%s %s",
                    transaction.getSeller().getFirstName(),
                    transaction.getSeller().getLastName())
                    .trim();
            setBudtender(new NamedEntity(employeeName));
        }

        if (transaction.getMember() != null) {
            String memberName = String.format("%s %s",
                    transaction.getMember().getFirstName(),
                    transaction.getMember().getLastName())
                    .trim();

            String memberIdType = "drivers_license";
            String memberIdNumber = null;
            String memberIdState = transaction.getShop().getAddress() != null ?
                    transaction.getShop().getAddress().getState() : "Unknown";

            if (transaction.getMember().getIdentifications() != null &&
                    !transaction.getMember().getIdentifications().isEmpty()) {
//                memberIdType =  transaction.getMember().getIdentifications().get(0).getType() != null ?
//                        transaction.getMember().getIdentifications().get(0).getType().getTypeName() : null;

                memberIdNumber = transaction.getMember().getIdentifications().get(0).getLicenseNumber();

                if (StringUtils.isNotBlank(transaction.getMember().getIdentifications().get(0).getState()))
                    memberIdState = transaction.getMember().getIdentifications().get(0).getState();
            }

            switch (transaction.getMember().getConsumerType()) {
                case AdultUse:
                case Other:
                    setProductType(ProductType.recreational);
                    break;
                case MedicinalThirdParty:
                case MedicinalState:
                    setProductType(ProductType.medicinal);
                    break;
            }
            setCustomer(new Customer(memberName, memberIdType, memberIdNumber, memberIdState));
        }

        if (transaction.getShop() != null) {
            setStoreLocation(new NamedEntity(transaction.getShop().getName()));
        }

        setAmount(String.valueOf(NumberUtils.round(transaction.getCart().getTotal(), 2)));
    }
}
