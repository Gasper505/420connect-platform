package com.fourtwenty.integrations.linx.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPaymentStatus;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayWithLinxCardResult extends LinxResult {
    @JsonProperty("id")
    private String id;

    @JsonProperty("status")
    private RedemptionStatus status;

    @JsonProperty("closing_balance")
    private BigDecimal closingBalance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RedemptionStatus getStatus() {
        return status;
    }

    public void setStatus(RedemptionStatus status) {
        this.status = status;
    }

    public BigDecimal getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }

    public void copyFieldsInto(PaymentCardPayment payment) {
        switch (getStatus()) {
            case created:
            case pending:
                payment.setStatus(PaymentCardPaymentStatus.Pending);
                break;
            case approved:
                payment.setStatus(PaymentCardPaymentStatus.Paid);
                break;
            case failed:
            case declined:
                payment.setStatus(PaymentCardPaymentStatus.Declined);
                break;
        }

        payment.setStatusMessage(getMessage());
        payment.setBalance(getClosingBalance());
    }
}
