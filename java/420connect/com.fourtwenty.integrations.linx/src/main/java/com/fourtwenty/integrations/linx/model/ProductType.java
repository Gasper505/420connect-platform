package com.fourtwenty.integrations.linx.model;

/*
Linx wants it lowercase
 */
public enum ProductType {
    recreational,
    medicinal
}
