package com.fourtwenty.integrations.linx.model;

public enum RedemptionStatus {
    created,
    pending,
    approved,
    declined,
    failed
}
