package com.fourtwenty.integrations.linx.repository;

import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface LinxAccountRepository extends MongoShopBaseRepository<LinxAccount> {
    LinxAccount getByCompanyAndShop(String companyId, String shopId);
}
