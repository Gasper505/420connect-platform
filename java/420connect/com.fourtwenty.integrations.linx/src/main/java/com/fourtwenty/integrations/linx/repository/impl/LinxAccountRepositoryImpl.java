package com.fourtwenty.integrations.linx.repository.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.integrations.linx.repository.LinxAccountRepository;
import com.google.inject.Inject;

public class LinxAccountRepositoryImpl extends ShopBaseRepositoryImpl<LinxAccount> implements LinxAccountRepository {
    @Inject
    public LinxAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(LinxAccount.class, mongoManager);
    }

    @Override
    public LinxAccount getByCompanyAndShop(String companyId, String shopId) {
        Iterable<LinxAccount> linxAccounts = listByShop(companyId, shopId);
        if (linxAccounts != null) {
            for (LinxAccount linxAccount : linxAccounts) {
                return linxAccount;
            }
        }
        return null;
    }
}
