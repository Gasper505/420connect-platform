package com.fourtwenty.integrations.linx.service;

import com.fourtwenty.core.domain.models.common.LinxConfig;
import com.fourtwenty.integrations.linx.model.*;

public interface LinxCommunicatorService {
    AuthenticateResult authenticate(LinxConfig linxConfig, AuthenticateRequest request);

    LoadLinxCardResult loadLinxCard(LinxConfig linxConfig, LoadLinxCardRequest request);

    PayWithLinxCardResult payWithLinxCard(LinxConfig linxConfig, PayWithLinxCardRequest request);

    BalanceCheckResult balanceCheck(LinxConfig linxConfig, BalanceCheckRequest request);
}
