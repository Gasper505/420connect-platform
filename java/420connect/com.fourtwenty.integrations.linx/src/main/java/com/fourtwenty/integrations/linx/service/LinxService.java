package com.fourtwenty.integrations.linx.service;


import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.integrations.linx.model.BalanceCheckRequest;
import com.fourtwenty.integrations.linx.model.BalanceCheckResult;
import com.fourtwenty.integrations.linx.model.LoadLinxCardRequest;
import com.fourtwenty.integrations.linx.model.LoadLinxCardResult;

import java.util.List;

public interface LinxService {
    // Linx Admin
    List<LinxAccount> getLinxAccounts(String companyId);

    List<LinxAccount> updateLinxAccounts(List<LinxAccount> linxAccounts);

    // Linx proxy API
    void processPendingLinxPayments(Transaction transaction);

    LoadLinxCardResult loadLinxCard(LoadLinxCardRequest request);

    BalanceCheckResult balanceCheck(BalanceCheckRequest request);
}
