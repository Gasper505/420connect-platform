package com.fourtwenty.integrations.linx.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.fourtwenty.core.domain.models.common.LinxConfig;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.integrations.linx.model.*;
import com.fourtwenty.integrations.linx.service.LinxCommunicatorService;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;

public class LinxCommunicatorServiceImpl implements LinxCommunicatorService {
    private static final Log LOG = LogFactory.getLog(LinxCommunicatorServiceImpl.class);
    private ObjectMapper mapper = new ObjectMapper();


    @Override
    public AuthenticateResult authenticate(LinxConfig linxConfig, AuthenticateRequest request) {
        try {
            String url = buildUrl(linxConfig, "/oauth/token");

            HttpAuthenticationFeature feature = HttpAuthenticationFeature
                    .basic(request.getAccessToken(), request.getSecret());

            ClientConfig clientConfig = new ClientConfig();
            clientConfig.register(feature);

            Client client = JerseyClientBuilder.newClient(clientConfig).register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(url);
            Response response = webTarget
                    .request()
                    .accept("*/*")
                    .post(
                            Entity.entity(
                                    String.format("grant_type=%s", request.getGrantType()),
                                    MediaType.APPLICATION_FORM_URLENCODED_TYPE));
            if (response.getStatus() == 200) {
                AuthenticateResult result = response.readEntity(AuthenticateResult.class);
                return result;
            } else {
                // throw error
                throw new BlazeOperationException(response.getEntity().toString());
            }
        } catch (Exception e) {
            throw new BlazeOperationException("Failed to authenticate with linx service.", e);
        }
    }

    @Override
    public LoadLinxCardResult loadLinxCard(LinxConfig linxConfig, LoadLinxCardRequest request) {
        if (request.getAuthenticateResult() == null || StringUtils.isBlank(request.getAuthenticateResult().getAccessToken()))
            throw new BlazeOperationException("No access token defined.");

        try {
            String url = buildUrl(linxConfig, "/api/v1/loads/load"); //"http://localhost:8888/api/v1/loads/load"; //buildUrl("/api/v1/loads/load");

            MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
            headers.putSingle("Authorization", String.format("Bearer %s", request.getAuthenticateResult().getAccessToken()));
            headers.putSingle("Accept", "*/*");

            String json = mapper.writeValueAsString(request);
            LOG.info("load json: " + json);

            LoadLinxCardResult result = SimpleRestUtil.post(url, json, LoadLinxCardResult.class, LoadLinxCardResult.class, headers);
            return result;
        } catch (RestErrorException e) {
            throw new BlazeOperationException(String.format("Failed to load linx card. \n%s", ((LoadLinxCardResult) e.getErrorResponse()).getMessage()), e);
        } catch (Exception e) {
            throw new BlazeOperationException(String.format("Failed to load linx card. \n%s", e.getMessage()), e);
        }
    }

    @Override
    public PayWithLinxCardResult payWithLinxCard(LinxConfig linxConfig, PayWithLinxCardRequest request) {
        if (request.getAuthenticateResult() == null || StringUtils.isBlank(request.getAuthenticateResult().getAccessToken()))
            throw new BlazeOperationException("No access token defined.");

        try {
            String url = buildUrl(linxConfig, "/api/v1/redemptions/redemption");

            MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
            headers.putSingle("Authorization", String.format("Bearer %s", request.getAuthenticateResult().getAccessToken()));

            String json = mapper.writeValueAsString(request);
            LOG.info("load json: " + json);

            PayWithLinxCardResult result = SimpleRestUtil.post(url, json, PayWithLinxCardResult.class, PayWithLinxCardResult.class, headers);
            return result;
        } catch (RestErrorException e) {
            throw new BlazeOperationException(String.format("Failed to pay with linx card.\n%s", ((PayWithLinxCardResult) e.getErrorResponse()).getMessage()), e);
        } catch (Exception e) {
            throw new BlazeOperationException(String.format("Failed to pay with linx card.\n%s", e.getMessage()), e);
        }
    }

    @Override
    public BalanceCheckResult balanceCheck(LinxConfig linxConfig, BalanceCheckRequest request) {
        if (request.getAuthenticateResult() == null || StringUtils.isBlank(request.getAuthenticateResult().getAccessToken()))
            throw new BlazeOperationException("No access token defined.");

        try {
            String url = buildUrl(linxConfig, "/api/v1/balance");
            Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(url);
            Response response = webTarget
                    .request()
                    .accept("*/*")
                    .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s",
                            request.getAuthenticateResult().getAccessToken()))
                    .post(
                            Entity.entity(
                                    String.format("card_number=%s", request.getLinxCardNumber()),
                                    MediaType.APPLICATION_FORM_URLENCODED_TYPE));
            if (response.getStatus() == 200) {
                BalanceCheckResult result = response.readEntity(BalanceCheckResult.class);
                return result;
            } else {
                // throw error
                throw new BlazeOperationException(response.readEntity(BalanceCheckResult.class).getMessage());
            }
        } catch (Exception e) {
            throw new BlazeOperationException(String.format("Failed to check linx card balance.\n%s", e.getMessage()), e);
        }
    }

    private <T> T makeApiRequest(String url, String accessToken, String json, Class<T> returnClass) {
        try {
            Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(url);
            Response response = webTarget
                    .request()
                    .accept("*/*")
                    .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", accessToken))
                    .post(Entity.json(json));
            if (response.getStatus() == 200) {
                T result = response.readEntity(returnClass);
                return result;
            } else {
                // throw error
                LinxResult result = (LinxResult) response.readEntity(returnClass);
                throw new BlazeOperationException(result.getMessage());
            }
        } catch (Exception e) {
            throw new BlazeOperationException(e.getMessage(), e);
        }
    }


    private String buildUrl(LinxConfig linxConfig, String path) {
        return String.format("%s%s",
                linxConfig.getHost(),
                path);
    }
}
