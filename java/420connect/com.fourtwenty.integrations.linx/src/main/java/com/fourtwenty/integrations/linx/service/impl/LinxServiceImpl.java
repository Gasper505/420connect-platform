package com.fourtwenty.integrations.linx.service.impl;

import com.fourtwenty.core.domain.models.common.LinxConfig;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPaymentStatus;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.integrations.linx.model.*;
import com.fourtwenty.integrations.linx.repository.LinxAccountRepository;
import com.fourtwenty.integrations.linx.service.LinxCommunicatorService;
import com.fourtwenty.integrations.linx.service.LinxService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LinxServiceImpl implements LinxService {
    private static final Log LOG = LogFactory.getLog(LinxServiceImpl.class);
    @Inject
    private LinxAccountRepository linxAccountRepository;

    @Inject
    private TerminalRepository terminalRepository;

    @Inject
    private LinxCommunicatorService linxCommunicatorService;

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private MemberRepository memberRepository;

    @Inject
    private IntegrationSettingRepository integrationSettingRepository;

    // Account Admin Methods

    /**
     * Override method to get linx account for all shops.If exists get db account else prepare new account.
     *
     * @param companyId
     * @return
     */
    @Override
    public List<LinxAccount> getLinxAccounts(String companyId) {
        List<LinxAccount> linxAccounts = Lists.newArrayList(linxAccountRepository.list(companyId));
        return linxAccounts;
    }

    /**
     * Override method for creating multiple linx account.
     *
     * @param linxAccounts
     * @return
     */
    @Override
    public List<LinxAccount> updateLinxAccounts(List<LinxAccount> linxAccounts) {
        if (linxAccounts == null || linxAccounts.isEmpty()) {
            return linxAccounts;
        }

        // Get Distinct CompanyIDs (should normally be 1 company in List)
        List<String> companyIds = linxAccounts
                .stream()
                .map(la -> la.getCompanyId())
                .distinct()
                .collect(Collectors.toList());

        // Discover existing Linx accounts in Mongo for companies
        List<LinxAccount> existingLinxAccounts = new ArrayList<>();
        companyIds.forEach(companyId ->
                existingLinxAccounts.addAll(Lists.newArrayList(linxAccountRepository.list(companyId))));

        // Save passed in linxAccounts
        for (LinxAccount linxAccount : linxAccounts) {
            LinxAccount linxAccountToSave = existingLinxAccounts.stream()
                    .filter(la ->
                            la.getCompanyId().equals(linxAccount.getCompanyId()) &&
                                    la.getShopId().equals(linxAccount.getShopId()))
                    .findFirst()
                    .orElse(linxAccount);

            if (linxAccount == linxAccountToSave) {
                linxAccountRepository.save(linxAccountToSave);
            } else {
                linxAccountToSave.setEnabled(linxAccount.isEnabled());
                linxAccountToSave.setAccessToken(linxAccount.getAccessToken());
                linxAccountToSave.setSecret(linxAccount.getSecret());
                linxAccountToSave.setEnvironment(linxAccount.getEnvironment());
                linxAccountRepository.update(linxAccountToSave.getId(), linxAccountToSave);
            }
        }

        return linxAccounts;
    }


    // API Proxy Methods
    @Override
    public void processPendingLinxPayments(Transaction transaction) {
        if (transaction.getCart().getPaymentOption() != Cart.PaymentOption.Linx) return;

        LinxAccount linxAccount = getLinxAccount(transaction.getCompanyId(), transaction.getShopId());
        LinxConfig linxConfig = getLinxConfig(linxAccount);

        LOG.info("LinxConfig: " + linxConfig);
        LOG.info("linxAccount: " + linxAccount);
        List<PaymentCardPayment> pendingPayments = transaction.getPaymentCardPayments()
                .stream()
                .filter(p -> p.getStatus() == PaymentCardPaymentStatus.Pending)
                .collect(Collectors.toList());

        // If theres pending payments, try to process them
        if (!pendingPayments.isEmpty()) {

            // Ensure transaction prereqs
            if (transaction.getShop() == null)
                transaction.setShop(shopRepository.get(transaction.getCompanyId(), transaction.getShopId()));
            if (transaction.getSeller() == null)
                transaction.setSeller(employeeRepository.get(transaction.getCompanyId(), transaction.getSellerId()));
            if (transaction.getMember() == null)
                transaction.setMember(memberRepository.get(transaction.getCompanyId(), transaction.getMemberId()));

            LOG.info(String.format("MemberId: %s, Member: %s",transaction.getMemberId(),transaction.getMember()));
            for (PaymentCardPayment payment : pendingPayments) {
                payment.prepare();
                payment.setProcessedTime(org.joda.time.DateTime.now().getMillis());
                PayWithLinxCardRequest payWithLinxCardRequest = new PayWithLinxCardRequest(transaction, payment);
                payWithLinxCardRequest.validate();

                // Authenticate with Linx
                AuthenticateResult authenticateResult = linxCommunicatorService.authenticate(linxConfig,
                        new AuthenticateRequest(linxAccount));
                authenticateResult.validate();

                // Perform Payment with Linx
                payWithLinxCardRequest.setAuthenticateResult(authenticateResult);
                PayWithLinxCardResult result = linxCommunicatorService.payWithLinxCard(linxConfig, payWithLinxCardRequest);
                result.copyFieldsInto(payment);

                // Update DB state
                transactionRepository.update(transaction.getId(), transaction);

                if (payment.getStatus() != PaymentCardPaymentStatus.Paid)
                    throw new BlazeOperationException("Linx Payment was declined.");
            }
        } else {
            // There was no payments, even though this was a Linx transaction. Throw error
            throw new BlazeOperationException("You must include a linx payment when paying with the linx payment type.");
        }

        // Ensure transaction is paid for
        double paymentCardTotalPaid = transaction.getPaymentCardPayments()
                .stream()
                .filter(p -> p.getStatus() == PaymentCardPaymentStatus.Paid)
                .mapToDouble(p -> p.getAmount().doubleValue())
                .sum();

        if (paymentCardTotalPaid < transaction.getCart().getTotal().doubleValue()) {
            throw new BlazeOperationException("Transaction not paid.");
        }
    }

    @Override
    public LoadLinxCardResult loadLinxCard(LoadLinxCardRequest request) {
        LinxAccount linxAccount = getLinxAccount(request.getToken().getCompanyId(), request.getToken().getShopId());
        LinxConfig linxConfig = getLinxConfig(linxAccount);

        LOG.info("LinxConfig: " + linxConfig);
        LOG.info("linxAccount: " + linxAccount);

        // Optionally translate encrypted data to credit card for Linx request
        transformEncryptedData(request);

        // Authenticate with Linx
        AuthenticateResult authenticateResult = linxCommunicatorService.authenticate(linxConfig,
                new AuthenticateRequest(linxAccount));
        authenticateResult.validate();


        LOG.info("Linx Auth passed, let's loadLinxCard...");
        // Perform Load operation
        request.setAuthenticateResult(authenticateResult);
        LoadLinxCardResult result = linxCommunicatorService.loadLinxCard(linxConfig, request);
        return result;
    }

    @Override
    public BalanceCheckResult balanceCheck(BalanceCheckRequest request) {
        LinxAccount linxAccount = getLinxAccount(request.getToken().getCompanyId(), request.getToken().getShopId());
        LinxConfig linxConfig = getLinxConfig(linxAccount);

        LOG.info("LinxConfig: " + linxConfig);
        LOG.info("linxAccount: " + linxAccount);

        // Authenticate with Linx
        AuthenticateResult authenticateResult = linxCommunicatorService.authenticate(linxConfig,
                new AuthenticateRequest(linxAccount));
        authenticateResult.validate();


        LOG.info("Linx Auth passed, let's balanceCheck...");
        // Perform Balance check
        request.setAuthenticateResult(authenticateResult);
        BalanceCheckResult result = linxCommunicatorService.balanceCheck(linxConfig, request);
        return result;
    }


    private void transformEncryptedData(LoadLinxCardRequest request) {
        if (request.getEncryptedTrack1() != null && request.getEncryptedTrack1().length > 0) {
            // Encryption Support
            Terminal terminal = terminalRepository.getTerminalById(
                    request.getToken().getCompanyId(),
                    request.getToken().getTerminalId());

            if (terminal.getCreditCardReader() == null)
                throw new BlazeOperationException("Terminal is not configured with a credit card reader.");

            // Todo: decrypt track data, parse it, and populate request.setCreditCard()
            switch (terminal.getCreditCardReader().getEncryptionType()) {
                case BlazeSymmetricEncryption:
                    break;
                case DUKPT:
                    break;
            }
        }
    }


    // Helpers
    private LinxAccount getLinxAccount(String companyId, String shopId) {
        // Validate Linx configuration
        LinxAccount linxAccount = linxAccountRepository.getByCompanyAndShop(companyId, shopId);
        if (linxAccount == null ||
                StringUtils.isBlank(linxAccount.getAccessToken()) || StringUtils.isBlank(linxAccount.getSecret()))
            throw new BlazeOperationException("No Linx account configured for this shop.");

        return linxAccount;
    }

    private LinxConfig getLinxConfig(LinxAccount linxAccount) {
        return integrationSettingRepository.getLinxConfig(linxAccount.getEnvironment());
    }
}
