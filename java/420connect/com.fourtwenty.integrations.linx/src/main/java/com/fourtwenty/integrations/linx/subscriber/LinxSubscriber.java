package com.fourtwenty.integrations.linx.subscriber;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPaymentStatus;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.linx.*;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardEvent;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckEvent;
import com.fourtwenty.core.event.transaction.ProcessLoyaltyCardsForTransactionResult;
import com.fourtwenty.core.event.transaction.ProcessPaymentCardsForTransactionEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.integrations.linx.model.BalanceCheckRequest;
import com.fourtwenty.integrations.linx.model.BalanceCheckResult;
import com.fourtwenty.integrations.linx.model.LoadLinxCardRequest;
import com.fourtwenty.integrations.linx.model.LoadLinxCardResult;
import com.fourtwenty.integrations.linx.repository.LinxAccountRepository;
import com.fourtwenty.integrations.linx.service.LinxService;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class LinxSubscriber implements BlazeSubscriber {
    private static final Log LOG = LogFactory.getLog(LinxSubscriber.class);

    @Inject
    private LinxService linxService;
    @Inject
    private LinxAccountRepository linxAccountRepository;
    @Inject
    private ShopRepository shopRepository;

    /////////////////////////////////////////
    // Settings Support

    @Subscribe
    public void subscribe(GetLinxAccountsEvent event) {
        try {
            if (StringUtils.isBlank(event.getCompanyId())) {
                event.throwException(new BlazeInvalidArgException("companyId", "You must pass a company id."));
            }

            List<LinxAccount> linxAccounts = linxService.getLinxAccounts(event.getCompanyId());

            List<String> savedAccountShopIds = linxAccounts
                    .stream()
                    .map(la -> la.getShopId())
                    .distinct()
                    .collect(Collectors.toList());

            List<Shop> shops = Lists.newArrayList(shopRepository.list(event.getCompanyId()));
            linxAccounts.addAll(shops.stream()
                    .filter(s -> !savedAccountShopIds.contains(s.getId()))
                    .map(s -> {
                        LinxAccount account = new LinxAccount();
                        account.setCompanyId(event.getCompanyId());
                        account.setShopId(s.getId());
                        return account;
                    })
                    .collect(Collectors.toList()));

            event.setResponse(new GetLinxAccountsResult(linxAccounts));

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    @Subscribe
    public void subscribe(SaveLinxAccountsEvent event) {
        try {
            List<LinxAccount> linxAccounts = linxService.updateLinxAccounts(event.getLinxAccounts());
            event.setResponse(new SaveLinxAccountsResult(linxAccounts));

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    // Used in Payment Options Support
    @Subscribe
    public void subscribe(IsLinxEnabledEvent event) {
        try {
            LinxAccount linxAccount =
                    linxAccountRepository.getByCompanyAndShop(event.getCompanyId(), event.getShopId());
            boolean isEnabled =
                    linxAccount != null &&
                            StringUtils.isNotEmpty(linxAccount.getAccessToken()) &&
                            StringUtils.isNotEmpty(linxAccount.getSecret()) &&
                            linxAccount.isEnabled();

            event.setResponse(new IsLinxEnabledResult(isEnabled));

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }


    /////////////////////////////////////////
    // Loyalty Card Support

    @Subscribe
    public void subscribe(LoadPaymentCardEvent event) {
        try {

            LOG.info("card Number: " + event.getPaymentCardNumber());
            // Construct Linx Specific balance check request from Event

            // Construct Linx Specific load request from Event
            LoadLinxCardRequest linxLoadRequest = new LoadLinxCardRequest(event);

            // Run Load
            LoadLinxCardResult linxLoadResult = linxService.loadLinxCard(linxLoadRequest);

            LOG.info("Result Total Value: " + linxLoadResult.getTotalValue());
            LOG.info("Result Total Paid: " + linxLoadResult.getTotalPaid());
            LOG.info("Result Message: " + linxLoadResult.getMessage());

            // Return linx-specific result as integration-agnostic result
            event.setResponse(linxLoadResult.asLoadLoyaltyCardResult());

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    @Subscribe
    public void subscribe(PaymentCardBalanceCheckEvent event) {
        try {
            LOG.info("card Number: " + event.getPaymentCardNumber());
            // Construct Linx Specific balance check request from Event
            BalanceCheckRequest linxBalanceCheck = new BalanceCheckRequest(event);

            // Run balance check
            BalanceCheckResult linxBalanceCheckResult = linxService.balanceCheck(linxBalanceCheck);

            LOG.info("Result Balance: " + linxBalanceCheckResult.getBalance());
            LOG.info("Result Status: " + linxBalanceCheckResult.getCardStatus());
            LOG.info("Result Message: " + linxBalanceCheckResult.getMessage());

            // Return linx-specific result as integration-agnostic result
            event.setResponse(linxBalanceCheckResult.asPaymentCardBalanceCheckResult());

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }


    /////////////////////////////////////////
    // Transaction Workflow Support

    @Subscribe
    public void subscribe(ProcessPaymentCardsForTransactionEvent event) {
        try {
            Transaction dbTransaction = event.getDbTransaction();
            Transaction requestTransaction = event.getRequestTransaction();

            LOG.info("dbTransaction.linxPayments: " + dbTransaction.getPaymentCardPayments());
            LOG.info("requestTransaction.linxPayments: " + requestTransaction.getPaymentCardPayments());

            if (requestTransaction.getCart().getPaymentOption() == Cart.PaymentOption.Linx) {
                for (PaymentCardPayment paymentCardPayment : requestTransaction.getPaymentCardPayments()) {
                    if (paymentCardPayment.getStatus() == null) {
                        paymentCardPayment.setStatus(PaymentCardPaymentStatus.Pending);
                    }
                }
                dbTransaction.addPaymentCardPayments(requestTransaction.getPaymentCardPayments());
                dbTransaction.getCart().setPaymentOption(requestTransaction.getCart().getPaymentOption());
                dbTransaction.getCart().setTotal(requestTransaction.getCart().getTotal());
                linxService.processPendingLinxPayments(dbTransaction);
                event.setResponse(new ProcessLoyaltyCardsForTransactionResult(dbTransaction));
            }

        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }
}
