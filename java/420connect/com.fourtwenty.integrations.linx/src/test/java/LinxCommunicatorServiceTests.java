import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.models.common.LinxConfig;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.integrations.linx.model.*;
import com.fourtwenty.integrations.linx.service.LinxCommunicatorService;
import com.fourtwenty.integrations.linx.service.impl.LinxCommunicatorServiceImpl;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class LinxCommunicatorServiceTests {
    private static final String ACCESS_KEY = "09504e73fec88d485db6fabba0787d6ad8aa03727f210bbf521080a810d34bb8";
    private static final String SECRET = "3535bf3956f83792924e7bee145585e1bc65650659b6186e056e967ff1a0903f";
    private static final String GRANT_TYPE = "client_credentials";
    private static final String LINX_CARD = "002Y5AM91";
    private static final String LINX_CARD_NUMBER = "6058690014870212";


    @Test
    public void testAuthentication() {
        LinxCommunicatorService linxCommunicatorService = new LinxCommunicatorServiceImpl();

        LinxConfig linxConfig = getLinxConfig();

        AuthenticateResult authenticateResult =
                linxCommunicatorService.authenticate(linxConfig,
                        new AuthenticateRequest(ACCESS_KEY, SECRET, GRANT_TYPE));

        Assert.assertNotNull(authenticateResult);
        Assert.assertTrue(!authenticateResult.getAccessToken().isEmpty());
        Assert.assertEquals("bearer", authenticateResult.getTokenType());
        Assert.assertTrue(authenticateResult.getCreatedAt() > 0);
        Assert.assertTrue(authenticateResult.getExpiresIn() > 0);
    }

    @Test
    public void testBalanceCheck() {
        LinxCommunicatorService linxCommunicatorService = new LinxCommunicatorServiceImpl();

        LinxConfig linxConfig = getLinxConfig();

        AuthenticateResult authenticateResult =
                linxCommunicatorService.authenticate(linxConfig,
                        new AuthenticateRequest(ACCESS_KEY, SECRET, GRANT_TYPE));
        Assert.assertNotNull(authenticateResult);
        Assert.assertTrue(!authenticateResult.getAccessToken().isEmpty());

        BalanceCheckRequest balanceCheckRequest = new BalanceCheckRequest();
        balanceCheckRequest.setLinxCardNumber(LINX_CARD);
        balanceCheckRequest.setAuthenticateResult(authenticateResult);

        BalanceCheckResult balanceCheckResult = linxCommunicatorService.balanceCheck(linxConfig, balanceCheckRequest);

        Assert.assertNotNull(balanceCheckResult);
        Assert.assertTrue(balanceCheckResult.isSuccess());
        Assert.assertFalse(balanceCheckResult.isExpired());
        Assert.assertTrue(StringUtils.isNotBlank(balanceCheckResult.getBalance()));
        Assert.assertNotNull(new BigDecimal(balanceCheckResult.getBalance().substring(1)));
    }

    @Ignore
    @Test
    public void testPayWithLinxCard() {
        LinxCommunicatorService linxCommunicatorService = new LinxCommunicatorServiceImpl();

        LinxConfig linxConfig = getLinxConfig();

        AuthenticateResult authenticateResult =
                linxCommunicatorService.authenticate(linxConfig,
                        new AuthenticateRequest(ACCESS_KEY, SECRET, GRANT_TYPE));
        Assert.assertNotNull(authenticateResult);
        Assert.assertTrue(!authenticateResult.getAccessToken().isEmpty());

        PayWithLinxCardRequest payWithLinxCardRequest = new PayWithLinxCardRequest(authenticateResult);
        payWithLinxCardRequest.setLinxCardNumber(LINX_CARD_NUMBER);
        payWithLinxCardRequest.setProductType(ProductType.recreational);
        payWithLinxCardRequest.setStoreLocation(new NamedEntity("Hollywood Blvd"));
        payWithLinxCardRequest.setCustomer(new Customer("John Doe", "drivers_license", "1234", "California"));
        payWithLinxCardRequest.setBudtender(new NamedEntity("Mary Jane Doe"));
        payWithLinxCardRequest.setAmount("6VerificationMethod.java0.00");

        PayWithLinxCardResult payWithLinxCardResult =
                linxCommunicatorService.payWithLinxCard(linxConfig, payWithLinxCardRequest);

        Assert.assertNotNull(payWithLinxCardResult);
        Assert.assertTrue(StringUtils.isNotBlank(payWithLinxCardResult.getId()));
        Assert.assertTrue(payWithLinxCardResult.getStatus() == RedemptionStatus.approved);
        Assert.assertTrue(payWithLinxCardResult.getClosingBalance().longValue() > 0);
    }

    @Ignore
    @Test
    public void testLoadLinxCard() {
        LinxCommunicatorService linxCommunicatorService = new LinxCommunicatorServiceImpl();

        LinxConfig linxConfig = getLinxConfig();

        AuthenticateResult authenticateResult =
                linxCommunicatorService.authenticate(linxConfig,
                        new AuthenticateRequest(ACCESS_KEY, SECRET, GRANT_TYPE));
        Assert.assertNotNull(authenticateResult);
        Assert.assertTrue(!authenticateResult.getAccessToken().isEmpty());

        LoadLinxCardRequest loadLinxCardRequest = new LoadLinxCardRequest(authenticateResult);
        double value = (new Random()).nextDouble() + 5;
        loadLinxCardRequest.setAmount(BigDecimal.valueOf(value));
        loadLinxCardRequest.setCardNumber(LINX_CARD);
        loadLinxCardRequest.setPaymentMethod("credit_card");
        loadLinxCardRequest.setCreditCard(new CreditCard(
                "4444333322221111",
                "John Citizen",
                String.format("%02d", ThreadLocalRandom.current().nextInt(8, 13)),
                "2022",
                String.format("%03d", ThreadLocalRandom.current().nextInt(100, 1000)),
                "90210"));
        loadLinxCardRequest.setTransactionReference("123");

        String json = JsonSerializer.toJson(loadLinxCardRequest);
        System.out.println("json: " + json);
        LoadLinxCardResult loadLinxCardResult = linxCommunicatorService.loadLinxCard(linxConfig, loadLinxCardRequest);

        Assert.assertNotNull(loadLinxCardResult);
        Assert.assertTrue(StringUtils.isNotBlank(loadLinxCardResult.getId()));
        Assert.assertTrue(StringUtils.isNotBlank(loadLinxCardResult.getTransactionReference()));
        Assert.assertTrue(loadLinxCardResult.getCreatedAt() != null);
        Assert.assertTrue(loadLinxCardResult.getTotalFees() != null);
        Assert.assertTrue(loadLinxCardResult.getTotalPaid() != null);
        Assert.assertTrue(loadLinxCardResult.getTotalValue() != null);
        Assert.assertTrue(loadLinxCardResult.getStatus() == LoadStatus.paid);
    }

    private LinxConfig getLinxConfig() {
        LinxConfig linxConfig = new LinxConfig(Lists.newArrayList(new IntegrationSetting[]{
                new IntegrationSetting(
                        IntegrationSettingConstants.Integrations.Linx.NAME,
                        IntegrationSetting.Environment.Development,
                        IntegrationSettingConstants.Integrations.Linx.HOST,
                        "https://staging.linxkiosk.com")
        }));
        return linxConfig;
    }
}
