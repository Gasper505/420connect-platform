package com.fourtwenty.integrations.metrc;

import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;
import org.apache.commons.lang3.StringUtils;

public class MetrcMapper {
    public static String map(ReconciliationHistoryList.ReconciliationReason reason, String stateCode) {
        if (reason == null) return "Incorrect Quantity";

        if (StringUtils.isNotBlank(stateCode) && stateCode.equalsIgnoreCase("AK")) {
            return reason.akMetrcReason;
        }
        return reason.metrcReason;
        /*
        switch (reason) {
            case WASTE_DISPOSAL:
                return "Waste (Unusable Product)";
            case WASTE_RETURN:
                return "Waste (Unusable Product)";
            case WASTE_DISPLAY:
                return "Package Display Sample";
            case WASTE_EXPIRED:
                return "Spoilage";
            case STORE_TRANSFER:
                return "Non-Metrc User Transfer";
            case DAMAGED:
                return "Damage (BCC)";
            case OTHER:
            case PO_ERROR:
            case AUDIT:
            default:
                return "Incorrect Quantity";
        }*/
    }
}
