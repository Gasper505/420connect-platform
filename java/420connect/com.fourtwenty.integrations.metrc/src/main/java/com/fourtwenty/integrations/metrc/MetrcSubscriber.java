package com.fourtwenty.integrations.metrc;

import com.amazonaws.util.CollectionUtils;
import com.amazonaws.util.StringUtils;
import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetrcMeasurement;
import com.blaze.clients.metrcs.MetricsItemsAPIService;
import com.blaze.clients.metrcs.MetricsPackagesAPIService;
import com.blaze.clients.metrcs.MetricsTransferAPIService;
import com.blaze.clients.metrcs.models.MetrcError;
import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.items.MetricsItem;
import com.blaze.clients.metrcs.models.items.MetricsItemCategory;
import com.blaze.clients.metrcs.models.packages.MetricsPackage;
import com.blaze.clients.metrcs.models.packages.MetricsPackageAdjustment;
import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.blaze.clients.metrcs.models.packages.PackageIngredients;
import com.blaze.clients.metrcs.models.transfers.ManifestDestination;
import com.blaze.clients.metrcs.models.transfers.ManifestMetricsTransferTemplate;
import com.blaze.clients.metrcs.models.transfers.MetrcsTransferTemplateResult;
import com.blaze.clients.metrcs.models.transfers.MetricsTransferDestination;
import com.blaze.clients.metrcs.models.transfers.MetricsTransferDestinationPackage;
import com.blaze.clients.metrcs.models.transfers.MetricsTransferTemplate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.compliance.ComplianceCategory;
import com.fourtwenty.core.domain.models.compliance.ComplianceItem;
import com.fourtwenty.core.domain.models.compliance.ComplianceStrain;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.TrackingPackages;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceCategoryRepository;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.DerivedProductBatchLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.batches.MetrcVerifyCreatePackage;
import com.fourtwenty.core.event.inventory.ComplianceBatch;
import com.fourtwenty.core.event.inventory.DerivedProductBatchEvent;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesEvent;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesResult;
import com.fourtwenty.core.event.inventory.InventoryReconciliationEvent;
import com.fourtwenty.core.event.inventory.InventoryTransferEvent;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsEvent;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsResult;
import com.fourtwenty.core.event.purchaseorder.ValidateCompleteShipmentArrivalEvent;
import com.fourtwenty.core.event.purchaseorder.ValidateCompleteShipmentArrivalResult;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.util.*;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.ShippingManifestRepository;
import com.warehouse.core.event.shippingmanifest.MetrcVerifyEvent;
import com.warehouse.core.event.shippingmanifest.ShippingManifestTransferEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Injector;
import com.warehouse.core.rest.invoice.result.ProductMetrcInfo;
import com.warehouse.core.rest.invoice.result.ShippingBatchDetails;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;
import java.io.*;


public class MetrcSubscriber implements BlazeSubscriber {

    private static final Log LOG = LogFactory.getLog(MetrcSubscriber.class);

    @Inject
    ConnectConfiguration connectConfiguration;
    @Inject
    private Injector injector;
    @Inject
    private MetrcService metrcService;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductWeightToleranceRepository toleranceRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ShippingManifestRepository shippingManifestRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private DerivedProductBatchLogRepository logRepository;
    @Inject
    private ComplianceItemRepository complianceItemRepository;
    @Inject
    private ComplianceCategoryRepository complianceCategoryRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private MetrcSyncSubscriber metrcSyncSubscriber;

    private static final String PRODUCT_COMPLIANCE_NOT_FOUND = "Product compliance not found.";
    private static final String PRODUCT_NON_CANNABIS = "Product is non cannabis.";
    private static final String PRODUCT_COMPLIANCE_CATEGORY_NOT_FOUND = "Product compliance category not found.";
    private static final String PRODUCT_CATEGORY_NON_CANNABIS = "Product category is non cannabis";
    private static final String METRIC_PACKAGE_NOT_FOUND = "Metrc package not found";
    private static final String METRC_LABEL_NOT_EMPTY = "Metrc label can't be empty";
    private static final String METRC_ERROR_CREATING_PACKAGE = "Error creating metrc package.";
    private static final String METRC = "Metrc";
    public static final String BEGINNING_INVENTORY = " - Beginning Inventory";

    //
    // BATCHES
    //
    @Subscribe
    public void getBatches(GetComplianceBatchesEvent event) {
        try {
            String stateCode = metrcService.getShopStateCode(event.getCompanyId(), event.getShopId());
            // Ignore if not enabled
            MetrcFacilityAccount account = metrcService.getMetrcAccount(event.getCompanyId(), stateCode)
                    .getMetrcFacilityAccount(event.getShopId());
            if (account == null || !account.isEnabled()) return;

            if (StringUtils.isNullOrEmpty(account.getFacLicense()))
                event.throwException(new BlazeOperationException("The facility license is not configured for this shop."));

            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    event.getCompanyId(),
                    event.getShopId());

            System.out.println("----------------------");
            System.out.println("- Getting Batches    -");
            System.out.println("----------------------");

            GetComplianceBatchesResult result = new GetComplianceBatchesResult();

            final MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
            final MetricsPackagesList activePackages =
                    metricsPackagesAPIService.getActivePackages(account.getFacLicense());
            System.out.println(String.format("- Got Active Batches: %s ", activePackages.size()));
            final MetricsPackagesList onHoldPackages =
                    metricsPackagesAPIService.getOnholdPackages(account.getFacLicense());
            System.out.println(String.format("- Got OnHold Batches: %s ", onHoldPackages.size()));
            final MetricsPackagesList inactivePackages =
                    metricsPackagesAPIService.getInActivePackages(account.getFacLicense());
            System.out.println(String.format("- Got Inactive Batches: %s ", inactivePackages.size()));

            result.setActiveBatches(toComplianceBatch(event.getCompanyId(), activePackages));
            result.setOnHoldBatches(toComplianceBatch(event.getCompanyId(), onHoldPackages));
            result.setInactiveBatches(toComplianceBatch(event.getCompanyId(), inactivePackages));

            // Join to BatchIDs
            List<ProductBatch> productBatches =
                    productBatchRepository.getProductBatches(event.getCompanyId(), event.getShopId());
            for (ComplianceBatch complianceBatch : result.getAllBatches()) {
                String productBatchId = productBatches
                        .stream()
                        .filter(pb -> complianceBatch.getLabel().equals(pb.getTrackPackageLabel()))
                        .findFirst()
                        .map(pb -> pb.getId())
                        .orElse(null);
                complianceBatch.setProductBatchId(productBatchId);
            }

            event.setResponse(result);
        } catch (Exception e) {
            event.throwException(new BlazeInvalidArgException("Metrc", "Metrc is not configured correctly."));
        }
    }

    private List<ComplianceBatch> toComplianceBatch(String companyId, MetricsPackagesList list) {
        List<ComplianceBatch> complianceBatches = list
                .stream()
                .map(i -> {
                    ComplianceBatch batch = new ComplianceBatch();
                    batch.setLabel(i.getLabel());
                    batch.setQuantity(i.getQuantity());
                    batch.setMeasurement(i.getUnitOfMeasureName());
                    batch.setProductName(i.getProductName());
                    batch.setCategoryName(i.getProductCategoryName());
                    batch.setPackagedDate(i.getPackagedDate());
                    batch.setTestingState(i.getLabTestingState());
                    batch.setTestingStateDate(i.getLabTestingStateDate());
                    batch.setLastModifiedDate((StringUtils.isNullOrEmpty(i.getLastModified())) ? "N/A" : DateUtil.convertToPatternUTC(i.getLastModified(), "yyyy-MM-dd'T'HH:mm:ssZ", "MM/dd/yyyy hh:mm:ss a"));
                    prepareMetrcPackages(companyId, batch);
                    return batch;
                })
                .collect(Collectors.toList());
        return complianceBatches;
    }


    //
    // PURCHASE ORDER
    //
    @Subscribe
    public void validateCompleteShipmentArrival(ValidateCompleteShipmentArrivalEvent event) {
        try {
            // Get event payload
            POProductRequest purchaseOrder = event.getPoProductRequest();

            String stateCode = metrcService.getShopStateCode(purchaseOrder.getCompanyId(), purchaseOrder.getShopId(), false);

            if (StringUtils.isNullOrEmpty(stateCode)) {
                LOG.info("State code not found for shop");
                event.setResponse(null);
                return;
            }
            // Ignore if not enabled
            MetrcAccount metrcAccount = metrcService.getMetrcAccount(purchaseOrder.getCompanyId(), stateCode);
            MetrcFacilityAccount account = null;

            if (metrcAccount != null) {
                account = metrcAccount.getMetrcFacilityAccount(purchaseOrder.getShopId());
            }

            boolean metrcEnabled = account != null && account.isEnabled();
            if (metrcEnabled && event.getPoProductRequest().getTrackTraceSystem() != null && event.getPoProductRequest().getTrackTraceSystem() == ProductBatch.TrackTraceSystem.METRC && event.getPoProductRequest().getTrackingPackagesList().isEmpty()) {
                event.throwException(new BlazeInvalidArgException("Metrc Enforced", "You have to set packages for this PO"));
            }
            event.setResponse(new ValidateCompleteShipmentArrivalResult());
        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    @Subscribe
    public void assignProductBatchDetails(AssignProductBatchDetailsEvent event) {
        try {
            // Get event payload
            TrackingPackages trackingPackages = event.getTrackingPackages();
            ProductBatch productBatch = event.getProductBatch();

            String stateCode = metrcService.getShopStateCode(productBatch.getCompanyId(), productBatch.getShopId());
            // Ignore if not enabled
            MetrcFacilityAccount account = metrcService.getMetrcAccount(productBatch.getCompanyId(), stateCode)
                    .getMetrcFacilityAccount(productBatch.getShopId());
            if (account == null || !account.isEnabled()) return;

            if (trackingPackages == null) {
                event.throwException(new BlazeInvalidArgException("TrackingPackages", "TrackingPackages is null"));
                return;
            }
            if (productBatch == null) {
                event.throwException(new BlazeInvalidArgException("ProductBatch", "ProductBatch is null"));
                return;
            }

            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    productBatch.getCompanyId(),
                    productBatch.getShopId());

            // Process event
            productBatch.setTrackTraceSystem(ProductBatch.TrackTraceSystem.METRC);
            productBatch.setTrackPackageLabel(trackingPackages.getPackageLabel());

            final MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
            final MetricsPackages metricsPackages = metricsPackagesAPIService.getPackageLabel(trackingPackages.getPackageLabel());
            if (metricsPackages != null) {
                productBatch.setTrackPackageId(metricsPackages.getId());
                event.setResponse(new AssignProductBatchDetailsResult(metricsPackages.getQuantity()));
            } else {
                event.throwException(new BlazeInvalidArgException("packageLabel", "Package Label does not exist in Metrcs."));
            }
        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    //
    // INVENTORY
    //
    @Subscribe
    public void processInventoryTransfer(InventoryTransferEvent event) {
        try {
            // Get event payload
            InventoryTransferHistory inventoryTransferHistory = event.getInventoryTransferHistory();

            if (inventoryTransferHistory == null)
                event.throwException(new BlazeOperationException("Inventory Transfer History is invalid."));

            String stateCode = metrcService.getShopStateCode(inventoryTransferHistory.getCompanyId(), inventoryTransferHistory.getShopId());
            // Ignore if not enabled
            MetrcFacilityAccount account = metrcService.getMetrcAccount(inventoryTransferHistory.getCompanyId(), stateCode)
                    .getMetrcFacilityAccount(inventoryTransferHistory.getShopId());
            if (account == null || !account.isEnabled()) return;


            System.out.println("----------------------");
            System.out.println("- Inventory Transfer -");
            System.out.println("----------------------");

            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    inventoryTransferHistory.getCompanyId(),
                    inventoryTransferHistory.getShopId());

            System.out.println(String.format("Transfer No: %s", inventoryTransferHistory.getTransferNo()));

            MetricsPackagesAPIService packageService = new MetricsPackagesAPIService(metrcAuthorization);

            List<MetricsTransferDestinationPackage> destinationPackages = new ArrayList<>();
            for (InventoryTransferLog transferLog : inventoryTransferHistory.getTransferLogs()) {
                ProductBatch productBatch = productBatchRepository.getById(transferLog.getFromBatchId());
                if (productBatch == null) return;

                String packageLabel = productBatch.getTrackPackageLabel();
                System.out.println(String.format("Package Label: %s", packageLabel));

                MetricsPackages metricsPackages = packageService.getPackageLabel(packageLabel);
                if (metricsPackages == null) return;
                System.out.println(String.format("Metrics Package Qty: %s", metricsPackages.getQuantity()));

                // Build new package for transfer
                MetricsTransferDestinationPackage destinationPackage = new MetricsTransferDestinationPackage();
                destinationPackage.setPackageLabel(metricsPackages.getLabel());
                destinationPackage.setGrossUnitOfWeightName(metricsPackages.getUnitOfMeasureName());
                destinationPackage.setItemName(metricsPackages.getProductName());
                destinationPackage.setPackagedDate(metricsPackages.getPackagedDate());
                destinationPackage.setQuantity(transferLog.getTransferAmount().doubleValue());
                destinationPackage.setWholesalePrice(productBatch.getFinalUnitCost().doubleValue()
                        * transferLog.getTransferAmount().doubleValue());
                destinationPackages.add(destinationPackage);
            }

            MetricsTransferAPIService transferService = new MetricsTransferAPIService(metrcAuthorization);
            MetricsTransferTemplate template = new MetricsTransferTemplate();
            MetricsTransferDestination destination = new MetricsTransferDestination();
            destination.setPackages(destinationPackages);
            template.setDestinations(Arrays.asList(destination));
            ObjectMapper objectMapper = new ObjectMapper();
            System.out.println(String.format("Transfer request: %s", objectMapper.writeValueAsString(template)));
            String transferResult = transferService.createTransferTemplate(Arrays.asList(template));
            System.out.println(String.format("Transfer result: %s", transferResult));

        } catch (Exception e) {
            System.out.println(String.format("MetrcSubscriber.processInventoryReconciliation failed: %s - %s", e.getMessage(), e.getStackTrace()));
            event.throwException(new BlazeOperationException(e));
        }
    }

    @Subscribe
    public void processInventoryReconciliation(InventoryReconciliationEvent event) {
        try {
            // Get event payload
            ReconciliationHistory reconciliationHistory = event.getReconciliationHistory();

            if (reconciliationHistory == null)
                event.throwException(new BlazeOperationException("Reconciliation History is invalid."));

            // Metrc requires batch reconcile
            if (!reconciliationHistory.isBatchReconcile()) return;

            String stateCode = metrcService.getShopStateCode(reconciliationHistory.getCompanyId(), reconciliationHistory.getShopId());
            // Ignore if not enabled
            MetrcFacilityAccount account = metrcService.getMetrcAccount(reconciliationHistory.getCompanyId(), stateCode)
                    .getMetrcFacilityAccount(reconciliationHistory.getShopId());
            if (account == null || !account.isEnabled()) return;


            System.out.println("----------------------------");
            System.out.println("- Inventory Reconciliation -");
            System.out.println("----------------------------");
            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    reconciliationHistory.getCompanyId(),
                    reconciliationHistory.getShopId());

            MetricsPackagesAPIService service = new MetricsPackagesAPIService(metrcAuthorization);

            // For each batch reconcile
            for (ReconciliationHistoryList reconciliationHistoryList : reconciliationHistory.getReconciliations()) {
                System.out.println(String.format("Batch: %s", reconciliationHistoryList.getBatchId()));

                // Get the product batch [to get the label]
                ProductBatch productBatch = productBatchRepository.getById(reconciliationHistoryList.getBatchId());
                if (productBatch != null && productBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.METRC) {
                    System.out.println(String.format("Tracking Label: %s", productBatch.getTrackPackageLabel()));

                    // Lookup the current quantity from metrc using the label
                    MetricsPackages metricsPackages = service.getPackageLabel(productBatch.getTrackPackageLabel());
                    if (metricsPackages != null) {
                        System.out.println(String.format("Quantity: Old (%s) New (%s) Metric (%s)", reconciliationHistoryList.getOldQuantity(),
                                reconciliationHistoryList.getNewQuantity(),
                                metricsPackages.getQuantity()));

                        // Compute the delta
                        // TODO: determine if we need to compute quantity considering weight differrences?  1 lb vs 10 grams on metrc?

                        Product product = productRepository.get(reconciliationHistory.getCompanyId(), reconciliationHistoryList.getProductId());

                        //BigDecimal newQty = reconciliationHistoryList.getNewQuantity();
                        // get total qty
                        BigDecimal newQty = batchQuantityRepository.getBatchQuantityForByBatchIdTotal(reconciliationHistory.getCompanyId(),
                                reconciliationHistory.getShopId(),
                                product.getId(), productBatch.getId());

                        if (product != null) {
                            // now convert if necessary
                            newQty = MeasurementConversionUtil.convertToMetrcQuantity(toleranceRepository,
                                    reconciliationHistory.getCompanyId(),
                                    metricsPackages.getUnitOfMeasureName(),
                                    product,
                                    newQty);
                        }
                        BigDecimal adjustQuantity = newQty.subtract(metricsPackages.getQuantity());
                        System.out.println(String.format("Adjustment: %s", adjustQuantity));

                        if (adjustQuantity.doubleValue() != 0) {
                            // Execute API adjust
                            MetricsPackageAdjustment adjustment = new MetricsPackageAdjustment();
                            adjustment.setLabel(metricsPackages.getLabel());
                            adjustment.setAdjustmentDate(DateTime.now().toLocalDate().toString());
                            adjustment.setQuantity(adjustQuantity);
                            adjustment.setUnitOfMeasure(metricsPackages.getUnitOfMeasureName());
                            adjustment.setAdjustmentReason(MetrcMapper.map(reconciliationHistoryList.getReconciliationReason(),stateCode));
                            adjustment.setReasonNote(reconciliationHistoryList.getNote() != null ?
                                    reconciliationHistoryList.getNote().getMessage() + "" : "");
                            String message = service.adjustPackage(Arrays.asList(adjustment),
                                    metrcAuthorization.getFacilityLicense());

                            System.out.println(String.format("Response: %s", message));
                        }
                    }
                }
            }

            //MetricsTransferList list = service.adjustPackage(metrcAuthorization.getFacilityLicense());
        } catch (Exception e) {
            System.out.println(String.format("MetrcSubscriber.processInventoryReconciliation failed: %s - %s", e.getMessage(), e.getStackTrace()));
            event.throwException(new BlazeOperationException(e));
        }
    }

    private void prepareMetrcPackages(String companyId, ComplianceBatch complianceBatch) {
        if (complianceBatch.getMeasurement().equalsIgnoreCase("Ounces")) {
            // convert to grams
            // Use the defined unit value from the tolerance for ounce
            ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(companyId,
                    ProductWeightTolerance.WeightKey.OUNCE);
            double ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue.doubleValue();
            if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
                ozUnitValue = ozTolerance.getUnitValue().doubleValue();
            }

            double blazeQuantity = ozUnitValue * complianceBatch.getQuantity().doubleValue();
            if (blazeQuantity < 0) {
                blazeQuantity = 0;
            }
            complianceBatch.setBlazeQuantity(BigDecimal.valueOf(blazeQuantity));
            complianceBatch.setBlazeMeasurement(ProductWeightTolerance.WeightKey.GRAM);
        } else if (complianceBatch.getMeasurement().equalsIgnoreCase("Grams")) {
            // just pass it on (grams)
            complianceBatch.setBlazeQuantity(complianceBatch.getQuantity());
            complianceBatch.setBlazeMeasurement(ProductWeightTolerance.WeightKey.GRAM);
        } else {
            // just pass it on
            complianceBatch.setBlazeQuantity(complianceBatch.getQuantity());
            complianceBatch.setBlazeMeasurement(ProductWeightTolerance.WeightKey.UNIT);
        }
    }

    @Subscribe
    public void processInventoryForDerivedBatch(DerivedProductBatchEvent event) {
        //MANH: THIS CODE IS WRONG AND NEEDS TO BE COMMENTED OUT UNTIL FURTHER INVESTIGATION
        // FOR SALES RECEIPT, WE NEED TO SEND THE SALES RETAIL PRICE, NOT THE BATCH PRICE
        /*try {
            // Get event payload
            HashMap<String, Double> batchQuantityMap = event.getBatchQuantityMap();
            if (batchQuantityMap == null || batchQuantityMap.isEmpty())
                event.throwException(new BlazeInvalidArgException("Metrc", "Product batch quantity details are invalid"));

            String stateCode = metrcService.getShopStateCode(event.getCompanyId(), event.getShopId());
            // Ignore if not enabled
            MetrcFacilityAccount account = metrcService.getMetrcAccount(event.getCompanyId(), stateCode)
                    .getMetrcFacilityAccount(event.getShopId());

            if (account == null || !account.isEnabled()) return;


            LOG.info("------------------------------------------------------------------------");
            LOG.info("-  Process inventory for batches used in derived product batch sales   -");
            LOG.info("------------------------------------------------------------------------");

            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    event.getCompanyId(),
                    event.getShopId());


            MetricsPackagesAPIService packageService = new MetricsPackagesAPIService(metrcAuthorization);
            MetricsSalesAPIService salesService = new MetricsSalesAPIService(metrcAuthorization);


            List<ObjectId> batchIds = new ArrayList<>();
            for (String batchId : batchQuantityMap.keySet()) {
                batchIds.add(new ObjectId(batchId));
            }

            HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(event.getCompanyId(), batchIds);

            List<MetricsSaleTransaction> transactionList = new ArrayList<>();

            for (String batchId : batchQuantityMap.keySet()) {

                double batchSoldQuantity = batchQuantityMap.get(batchId);
                ProductBatch productBatch = batchHashMap.get(batchId);
                if (productBatch == null || StringUtils.isNullOrEmpty(productBatch.getTrackPackageLabel()))
                    continue;

                String packageLabel = productBatch.getTrackPackageLabel();
                LOG.info(String.format("Package Label: %s", packageLabel));

                MetricsPackages metricsPackages = packageService.getPackageLabel(packageLabel);
                if (metricsPackages == null) continue;
                LOG.info(String.format("Metrics Package Qty: %s", metricsPackages.getQuantity()));

                double metrcQuantity = batchSoldQuantity;

                if (metricsPackages.getUnitOfMeasureName().equalsIgnoreCase("Ounces")) {
                    // convert to grams
                    // Use the defined unit value from the tolerance for ounce
                    ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(event.getCompanyId(),
                            ProductWeightTolerance.WeightKey.OUNCE);
                    double ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue.doubleValue();
                    if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
                        ozUnitValue = ozTolerance.getUnitValue().doubleValue();
                    }
                    metrcQuantity = batchSoldQuantity / ozUnitValue;
                }

                if (metrcQuantity < 0) {
                    metrcQuantity = 0;
                }

                if (metrcQuantity > metricsPackages.getQuantity()) {
                    metrcQuantity = metricsPackages.getQuantity();
                }
                MetricsSaleTransaction metricsSaleTransaction = new MetricsSaleTransaction();
                metricsSaleTransaction.setPackageLabel(packageLabel);
                metricsSaleTransaction.setQuantity(metrcQuantity);
                metricsSaleTransaction.setUnitOfMeasure(metricsPackages.getUnitOfMeasureName());
                metricsSaleTransaction.setTotalAmount(productBatch.getFinalUnitCost().doubleValue() * metrcQuantity);
                transactionList.add(metricsSaleTransaction);
            }

            MetricsSaleReceipt saleReceipt = new MetricsSaleReceipt();
            saleReceipt.setSalesCustomerType("Consumer");
            saleReceipt.setSalesDateTime(DateUtil.toDateTimeFormatted(event.getSalesDateTime(), DateTimeZone.UTC.getID(), "yyyy-MM-dd'T'HH:mm:ssZ"));
            saleReceipt.setTransactions(transactionList);
            salesService.createSaleReceipts(Arrays.asList(saleReceipt), metrcAuthorization.getFacilityLicense());


        } catch (Exception e) {
            LOG.info(String.format("MetrcSubscriber.processInventoryForDerivedBatch failed: %s - %s", e.getMessage(), e.getStackTrace()));
            event.throwException(new BlazeInvalidArgException("Metrc", e.getMessage()));
        }
        */
    }

    @Subscribe
    public void sendManifestToMetrc(ShippingManifestTransferEvent event) {
        if (event == null || event.getShippingManifest() == null) {
            LOG.info("Shipping manifest cannot be blank.");
            return;
        }

        ShippingManifestResult shippingManifestResult = event.getShippingManifest();
        Invoice invoice = event.getInvoice();

        if (shippingManifestResult == null) {
            LOG.info("Shipping manifest is not found.");
            return;
        }

        if (invoice == null) {
            LOG.info("Invoice is not exists.");
            return;
        }

        String stateCode = metrcService.getShopStateCode(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId());
        // Ignore if not enabled
        MetrcAccount account = metrcService.getMetrcAccount(shippingManifestResult.getCompanyId(), stateCode);

        if (account == null) {
            LOG.info("Metrc Account is not found.");
            return;
        }
        try {
            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    shippingManifestResult.getCompanyId(),
                    shippingManifestResult.getShopId());

            LOG.info(String.format("Shipping Manifest No: %s", shippingManifestResult.getShippingManifestNo()));

            MetricsPackagesAPIService packageService = new MetricsPackagesAPIService(metrcAuthorization);

            Transaction transaction = transactionRepository.getById(shippingManifestResult.getTransactionId());
            if (transaction == null) {
                LOG.info(String.format("Transaction is not found for shipping Manifest No: %s", shippingManifestResult.getShippingManifestNo()));
                return;
            }

            List<ManifestDestination.ManifestPackages> packagesList = new ArrayList<>();
            HashMap<String, Double> orderItemPriceMap = new HashMap<>();

            invoice.getCart().getItems().forEach(orderItem -> {
                orderItemPriceMap.put(orderItem.getId(), orderItem.getUnitPrice().doubleValue());
            });

            for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
               for (ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                    if (!CollectionUtils.isNullOrEmpty(batchDetails.getMetrcPackageDetails())) {
                        // dealing w/ bundles packages
                        Double bundlePrice = orderItemPriceMap.getOrDefault(productMetrcInfo.getOrderItemId(),0D);
                        BigDecimal totalQty = BigDecimal.ZERO;
                        for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails : batchDetails.getMetrcPackageDetails()) {
                            totalQty = totalQty.add(metrcPackageDetails.getQuantity());
                        }
                        if (totalQty.doubleValue() < 0) {
                            totalQty = BigDecimal.ONE;
                        }

                        for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails : batchDetails.getMetrcPackageDetails()) {
                            if (!StringUtils.isNullOrEmpty(metrcPackageDetails.getReqMetrcLabel()) && metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                                String packageLabel = metrcPackageDetails.getReqMetrcLabel();
                                MetricsPackages metricsPackages = null;
                                try {
                                    metricsPackages = packageService.getPackageLabel(packageLabel);
                                } catch (Exception e) {
                                    LOG.info("No metrc package found..: " + packageLabel);
                                }
                                if (metricsPackages != null) {
                                    BigDecimal ratio = metrcPackageDetails.getQuantity().divide(totalQty,4, RoundingMode.HALF_EVEN);
                                    Double bundleItemPrice = bundlePrice * ratio.doubleValue();

                                    prepareManifestPackages(metrcPackageDetails.getQuantity(), packagesList, packageLabel, metricsPackages, bundleItemPrice);
                                }
                            }
                        }
                    } else if (!StringUtils.isNullOrEmpty(batchDetails.getReqMetrcLabel()) && batchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                        String packageLabel = batchDetails.getReqMetrcLabel();
                        MetricsPackages metricsPackages = null;
                        try {
                            metricsPackages = packageService.getPackageLabel(packageLabel);
                        } catch (Exception e) {
                            LOG.info("No metrc package found..: " + packageLabel);
                        }
                        if (metricsPackages == null) {
                            continue;
                        }
                        prepareManifestPackages(batchDetails.getQuantity(), packagesList, packageLabel, metricsPackages, orderItemPriceMap.getOrDefault(productMetrcInfo.getOrderItemId(), 0D));
                    }
                }
            }
            Vendor receiverCompany = shippingManifestResult.getReceiverCustomerCompany();
            String receiverLicense = "";
            if (receiverCompany != null && invoice != null) {
                CompanyLicense companyLicense = receiverCompany.getCompanyLicense(invoice.getLicenseId());
                if (companyLicense != null) {
                    receiverLicense = companyLicense.getLicenseNumber();
                }
            }

           /* HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), productIds);

            for (OrderItem item : transaction.getCart().getItems()) {
                if (CollectionUtils.isNullOrEmpty(item.getQuantityLogs())) {
                    continue;
                }
                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                    if (!StringUtils.isNullOrEmpty(quantityLog.getBatchId()) && ObjectId.isValid(quantityLog.getBatchId())) {
                        batchIds.add(new ObjectId(quantityLog.getBatchId()));
                    }
                }
            }

            HashMap<String, ProductBatch> batchHashMap = productBatchRepository.findItemsInAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), batchIds);
            List<ObjectId> bundleBatchIds = new ArrayList<>();
            HashMap<String, MetricsPackages> metrcPackagesMap = new HashMap<>();
            List<ObjectId> derivedBatchLogIds = new ArrayList<>();

            for (Map.Entry<String, ProductBatch> batches : batchHashMap.entrySet()) {
                ProductBatch productBatch = batches.getValue();
                if (productBatch == null) {
                    continue;
                }
                String packageLabel = productBatch.getTrackPackageLabel();
                LOG.info(String.format("Package Label: %s found for batch: %s", packageLabel, productBatch.getSku()));

                MetricsPackages metricsPackages = new MetricsPackages();
                try {
                    metricsPackages = packageService.getPackageLabel(packageLabel);
                } catch (Exception e) {
                    LOG.info("No metrc package found..: " + packageLabel);
                }
                if (metricsPackages != null && metricsPackages.getId() != 0) {
                    metrcPackagesMap.put(packageLabel, metricsPackages);
                } else {
                    Product product = productHashMap.get(productBatch.getProductId());
                    if (product == null) {
                        continue;
                    }

                    if (product.getProductType() == Product.ProductType.BUNDLE) {
                        for (BatchBundleItems bundleItem : productBatch.getBundleItems()) {
                            if (bundleItem == null && CollectionUtils.isNullOrEmpty(bundleItem.getBatchItems())) {
                                continue;
                            }
                            for (BatchBundleItems.BatchItems batchBundleItems : bundleItem.getBatchItems()) {
                                if (batchBundleItems ==null && StringUtils.isNullOrEmpty(batchBundleItems.getBatchId())) {
                                    continue;
                                }
                                bundleBatchIds.add(new ObjectId(batchBundleItems.getBatchId()));
                            }
                        }
                    }

                    if (product.getProductType() == Product.ProductType.DERIVED) {
                        if (StringUtils.isNullOrEmpty(productBatch.getDerivedLogId())) {
                            continue;
                        }
                        derivedBatchLogIds.add(new ObjectId(productBatch.getDerivedLogId()));
                    }
                }
            }

            HashMap<String, DerivedProductBatchLog> derivedProductBatchLogHashMap = logRepository.findItemsInAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), derivedBatchLogIds);
            List<ObjectId> derivedProductBatchIds = new ArrayList<>();

            for (Map.Entry<String, DerivedProductBatchLog> derivedProductBatchLogEntry : derivedProductBatchLogHashMap.entrySet()) {
                DerivedProductBatchLog log = derivedProductBatchLogEntry.getValue();
                if (log == null || (log.getProductMap() == null || log.getProductMap().isEmpty())) {
                    continue;
                }

                for (List<DerivedProductBatchInfo> derivedProductBatchInfos : log.getProductMap().values()) {
                    for (DerivedProductBatchInfo info : derivedProductBatchInfos) {
                        if (org.apache.commons.lang3.StringUtils.isNotBlank(info.getBatchId()) && ObjectId.isValid(info.getBatchId())) {
                            derivedProductBatchIds.add(new ObjectId(info.getBatchId()));
                        }
                    }
                }
            }

            HashMap<String, ProductBatch> derivedProductBatchMap = productBatchRepository.findItemsInAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), derivedProductBatchIds);
            HashMap<String, ProductBatch> bundleProductAllBatches = productBatchRepository.findItemsInAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), bundleBatchIds);

            Vendor receiverCompany = shippingManifestResult.getReceiverCustomerCompany();
            String receiverLicense = "";
            if (receiverCompany != null && invoice != null) {
                CompanyLicense companyLicense = receiverCompany.getCompanyLicense(invoice.getLicenseId());
                if (companyLicense != null) {
                    receiverLicense = companyLicense.getLicenseNumber();
                }
            }

            ProductBatch productBatch;
            List<ManifestDestination.ManifestPackages> packagesList = new ArrayList<>();

            for (OrderItem item : transaction.getCart().getItems()) {
                if (CollectionUtils.isNullOrEmpty(item.getQuantityLogs())) {
                    continue;
                }
                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                    productBatch = batchHashMap.get(quantityLog.getBatchId());
                    if (productBatch == null) {
                        continue;
                    }
                    String packageLabel = shippingBatchDetailsHashMap.get(productBatch.getId());
                    if (StringUtils.isNullOrEmpty(packageLabel)) {
                        continue;
                    }

                    MetricsPackages metricsPackages = metrcPackagesMap.get(packageLabel);
                    if (metricsPackages != null) {
                        prepareManifestPackages(quantityLog, item, packagesList, packageLabel, metricsPackages);
                    } else {
                        Product product = productHashMap.get(productBatch.getProductId());
                        if (product == null) {
                            continue;
                        }

                        if (product.getProductType() == Product.ProductType.BUNDLE && !CollectionUtils.isNullOrEmpty(productBatch.getBundleItems())) {
                            for (BatchBundleItems bundleItem : productBatch.getBundleItems()) {
                                for (BatchBundleItems.BatchItems batchItem : bundleItem.getBatchItems()) {
                                    ProductBatch dbProductBatch = bundleProductAllBatches.get(batchItem.getBatchId());
                                    if (dbProductBatch == null) {
                                        continue;
                                    }
                                    try {
                                        packageLabel = shippingBatchDetailsHashMap.get(dbProductBatch.getId());
                                        if (StringUtils.isNullOrEmpty(packageLabel)) {
                                            continue;
                                        }
                                        if (checkPackageLabel(packagesList, packageLabel)) {
                                            metricsPackages = packageService.getPackageLabel(packageLabel);
                                            if (metricsPackages == null) {
                                                continue;
                                            }
                                            prepareManifestPackages(quantityLog, item, packagesList, packageLabel, metricsPackages);
                                        }
                                    } catch (Exception e) {
                                        LOG.info("No metrc package found..: " + packageLabel);
                                    }
                                }
                            }
                        }

                        if (product.getProductType() == Product.ProductType.DERIVED) {
                            if (StringUtils.isNullOrEmpty(productBatch.getDerivedLogId())) {
                                continue;
                            }
                            DerivedProductBatchLog derivedProductBatchLog = derivedProductBatchLogHashMap.get(productBatch.getDerivedLogId());
                            if (derivedProductBatchLog == null) {
                                continue;
                            }
                            for (Map.Entry<String, List<DerivedProductBatchInfo>> derivedBatchEntry : derivedProductBatchLog.getProductMap().entrySet()) {
                                for (DerivedProductBatchInfo derivedProductBatchInfo : derivedBatchEntry.getValue()) {
                                    ProductBatch dbProductBatch = derivedProductBatchMap.get(derivedProductBatchInfo.getBatchId());
                                    if (dbProductBatch == null) {
                                        continue;
                                    }
                                    try {
                                        packageLabel = dbProductBatch.getTrackPackageLabel();
                                        if (checkPackageLabel(packagesList, packageLabel)) {
                                            metricsPackages = packageService.getPackageLabel(packageLabel);
                                            if (metricsPackages == null) {
                                                continue;
                                            }
                                            prepareManifestPackages(quantityLog, item, packagesList, packageLabel, metricsPackages);
                                        }
                                    } catch (Exception e) {
                                        LOG.info("No metrc package found..: " + packageLabel);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }*/

            if (packagesList.isEmpty()) {
                LOG.info("No valid outgoing metrc tags found for manifest: " + shippingManifestResult.getShippingManifestNo());
                event.throwException(new BlazeInvalidArgException("Shipping manifest", String.format("No valid outgoing metrc tags found for manifest: %s ", shippingManifestResult.getShippingManifestNo())));
                return;
            }
            /*
            ManifestTransporter transporter = new ManifestTransporter();
            transporter.setDriverLicenseNumber(shippingManifestResult.getDriverLicenceNumber());
            transporter.setDriverName(shippingManifestResult.getDriverName());
            transporter.setDriverOccupationalLicenseNumber(shippingManifestResult.getDriverLicenceNumber());
            transporter.setTransporterFacilityLicenseNumber(metrcAuthorization.getFacilityLicense());
            transporter.setVehicleLicensePlateNumber(shippingManifestResult.getDriverVinNo());
            transporter.setVehicleMake(shippingManifestResult.getVehicleMake());
            transporter.setVehicleModel(shippingManifestResult.getVehicleModel());
            transporter.setEstimatedArrivalDateTime(DateUtil.toDateFormattedURC(shippingManifestResult.getDeliveryDate(), "yyyy-MM-dd'T'HH:mm:ss.000"));
            transporter.isLayover(false);
            transporter.setEstimatedDepartureDateTime(DateUtil.toDateFormattedURC(shippingManifestResult.getDeliveryDate(), "yyyy-MM-dd'T'HH:mm:ss.000"));*/


            Shop shop = shopRepository.getById(shippingManifestResult.getShopId());
            String originAddress = "";
            String destinationAddress = receiverCompany != null && receiverCompany.getAddress() != null ? receiverCompany.getAddress().getAddressString() : "";
            originAddress = shop.getAddress() != null ? shop.getAddress().getAddressString() : "";


            String plannedRoute = generatePlainPlannedRoute(originAddress,destinationAddress,"");

            // Build new package for transfer
            ManifestDestination destinationPackage = new ManifestDestination();
            destinationPackage.setTransferTypeName(shippingManifestResult.getMetrcTransferType());
            destinationPackage.setEstimatedArrivalDateTime(DateUtil.convertToISO8601(shippingManifestResult.getDeliveryDate(), shop.getTimeZone()));
            destinationPackage.setEstimatedDepartureDateTime(DateUtil.convertToISO8601(shippingManifestResult.getDeliveryDate(), shop.getTimeZone()));
            destinationPackage.setPlannedRoute(plannedRoute);
            destinationPackage.setRecipientLicenseNumber(receiverLicense);
            //destinationPackage.getTransporters().add(transporter);
            destinationPackage.setPackages(packagesList);

            ManifestMetricsTransferTemplate manifestTemplate = new ManifestMetricsTransferTemplate();
            manifestTemplate.setName(String.format("Manifest Template %s", shippingManifestResult.getShippingManifestNo()));
            manifestTemplate.setDriverLicenseNumber(shippingManifestResult.getDriverLicenceNumber());
            manifestTemplate.setDriverName(shippingManifestResult.getDriverName());
            manifestTemplate.setDriverOccupationalLicenseNumber(shippingManifestResult.getDriverLicenceNumber());
            manifestTemplate.setTransporterFacilityLicenseNumber(metrcAuthorization.getFacilityLicense());
            manifestTemplate.setVehicleLicensePlateNumber(shippingManifestResult.getDriverVinNo());
            manifestTemplate.setVehicleMake(shippingManifestResult.getVehicleMake());
            manifestTemplate.setVehicleModel(shippingManifestResult.getVehicleModel());
            manifestTemplate.getDestinations().add(destinationPackage);

            MetricsTransferAPIService transferService = new MetricsTransferAPIService(metrcAuthorization);

            Employee driverInfo = employeeRepository.getById(shippingManifestResult.getDriverId());

            if (!Objects.isNull(driverInfo) && !Objects.isNull(shop) && !StringUtils.isNullOrEmpty(driverInfo.getPhoneNumber())) {
                String phoneNumber = AmazonServiceManager.cleanPhoneNumber(driverInfo.getPhoneNumber(), shop);
                manifestTemplate.setPhoneNumberForQuestions(phoneNumber);
                //transporter.setPhoneNumberForQuestions(phoneNumber);
            }

            ObjectMapper objectMapper = new ObjectMapper();
            if (!shippingManifestResult.isMetrc() && StringUtils.isNullOrEmpty(shippingManifestResult.getMetrcTransferTemplateId())) {
                String lastModifiedStart = DateUtil.toDateFormattedURC(DateTime.now().minusMinutes(3).getMillis(), "yyyy-MM-dd'T'HH:mm:ss").concat("Z");
                LOG.info(String.format("Shipping manifest request: %s", objectMapper.writeValueAsString(manifestTemplate)));
                transferService.createManifestTransferTemplate(Lists.newArrayList(manifestTemplate), metrcAuthorization.getFacilityLicense());
                LOG.info("Shipping manifest result: success");

                String lastModifiedEnd = DateUtil.toDateFormattedURC(DateTime.now().plusMinutes(3).getMillis(), "yyyy-MM-dd'T'HH:mm:ss").concat("Z");
                String response = transferService.getManifestTransferTemplatesByDate(lastModifiedStart, lastModifiedEnd);
                JsonNode rootNode = objectMapper.readTree(response);
                MetrcsTransferTemplateResult[] metrcsTranferTemplateResults = objectMapper.treeToValue(rootNode, MetrcsTransferTemplateResult[].class);
                for (MetrcsTransferTemplateResult result : metrcsTranferTemplateResults) {
                    if (manifestTemplate.getName().equals(result.getName())) {
                        long metrcSentTime = DateTime.now().getMillis();
                        shippingManifestRepository.updateMetrcStatus(shippingManifestResult.getId(), true, result.getId(), result.getManifestNumber(), metrcSentTime);
                        shippingManifestResult.setMetrcManifestNumber(result.getManifestNumber());
                        shippingManifestResult.setMetrcSentTime(metrcSentTime);
                        shippingManifestResult.setMetrcTransferTemplateId(result.getId());
                        shippingManifestResult.setMetrc(true);
                        event.setResponse(shippingManifestResult);
                    }
                }
            } else {
                LOG.info(String.format("Shipping manifest request: %s", objectMapper.writeValueAsString(manifestTemplate)));
                manifestTemplate.setTransferTemplateId(shippingManifestResult.getMetrcTransferTemplateId());
                transferService.updateManifestTransferTemplate(Lists.newArrayList(manifestTemplate));
                LOG.info("Shipping manifest update result: success");
                event.setResponse(shippingManifestResult);
            }
        } catch (RestErrorException ex) {
            String errors = "";
            if (ex.getErrorResponse() != null) {
                int errorsCount = 1;
                if (ex.getErrorResponse() instanceof  MetricsErrorList) {
                    for (MetrcError metrcErrors : (MetricsErrorList) ex.getErrorResponse()) {
                        errors += errorsCount + ".- ".concat(metrcErrors.getMessage()).concat(" ");
                        errorsCount++;
                    }
                } else if (ex.getErrorResponse() instanceof String) {
                    HashMap<String,String> jsonObject = JsonSerializer.fromJson(ex.getErrorResponse().toString(),HashMap.class);
                    if (jsonObject!=null){
                        errors += errorsCount + ".- ".concat(jsonObject.getOrDefault("Message", "Unknown error."));
                        errorsCount++;
                    }else{
                        try{
                            String jsonErrorList = ex.getErrorResponse().toString();
                            ObjectMapper mapper = new ObjectMapper();
                            List<MetrcError> metrcErrorList = Arrays.asList(mapper.readValue(jsonErrorList, MetrcError[].class));
                            for (MetrcError metrcError : metrcErrorList){
                                errors += errorsCount + ".- ".concat(metrcError.getMessage()).concat(" ");
                                errorsCount++;
                            }
                        }catch(IOException ioex){
                            errors += "Unknown error.";
                            errorsCount++;
                        }
                    }
                }
                LOG.error(String.format("MetrcSubscriber.sendManifestToMetrc failed: %s", errors, ex));
                event.throwException(new BlazeInvalidArgException("Shipping manifest", errors));
            } else {
                LOG.error(String.format("MetrcSubscriber.sendManifestToMetrc failed: %s", ex.getMessage()));
                event.throwException(new BlazeInvalidArgException("Shipping manifest", "Metrc Authorization is not configured properly."));
            }
        } catch (Exception e) {
            LOG.error(String.format("MetrcSubscriber.sendManifestToMetrc failed: %s", e.getMessage()), e);
            event.throwException(new BlazeInvalidArgException("Shipping manifest", e.getMessage()));
        }
    }

    @Subscribe
    public void verifyMetrcLabel(MetrcVerifyEvent event) {
        if (event == null || CollectionUtils.isNullOrEmpty(event.getReqMetrcLabel())) {
            LOG.info("Requested metrc label is empty.");
            return;
        }

        if (event.isCreatePackage() && event.getShippingManifest() == null) {
            LOG.info("Shipping manifest not found");
            return;
        }
        List<String> reqMetrcLabels = event.getReqMetrcLabel();
        List<String> returnMetrcLabels = new ArrayList<>();
        String stateCode = metrcService.getShopStateCode(event.getCompanyId(), event.getShopId());
        // Ignore if not enabled
        MetrcAccount account = metrcService.getMetrcAccount(event.getCompanyId(), stateCode);

        if (account == null) {
            LOG.info("Metrc Account does not exist.");
            return;
        }
        try {
            MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                    event.getCompanyId(),
                    event.getShopId());


            final MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
            final MetricsPackagesList activePackages =
                    metricsPackagesAPIService.getActivePackages(metrcAuthorization.getFacilityLicense());
            LOG.info(String.format("- Got Active Batches: %s ", activePackages.size()));
            final MetricsPackagesList onHoldPackages =
                    metricsPackagesAPIService.getOnholdPackages(metrcAuthorization.getFacilityLicense());
            LOG.info(String.format("- Got OnHold Batches: %s ", onHoldPackages.size()));
            final MetricsPackagesList inactivePackages =
                    metricsPackagesAPIService.getInActivePackages(metrcAuthorization.getFacilityLicense());
            LOG.info(String.format("- Got Inactive Batches: %s ", inactivePackages.size()));

            activePackages.forEach(packages -> {
                if (reqMetrcLabels.contains(packages.getLabel())) {
                    returnMetrcLabels.add(packages.getLabel());
                }
            });

            onHoldPackages.forEach(packages -> {
                if (reqMetrcLabels.contains(packages.getLabel())) {
                    returnMetrcLabels.add(packages.getLabel());
                }
            });

            inactivePackages.forEach(packages -> {
                if (reqMetrcLabels.contains(packages.getLabel())) {
                    returnMetrcLabels.add(packages.getLabel());
                }
            });

            if (!event.isCreatePackage() || !CollectionUtils.isNullOrEmpty(returnMetrcLabels)) {
                event.setResponse(returnMetrcLabels);
            } else {
                createPackages(event.getShippingManifest(), metricsPackagesAPIService, metrcAuthorization, event.getRequestMetrcInfoKeys());
                event.setResponse(Lists.newArrayList());
            }


        } catch (Exception e) {
            LOG.error(String.format("MetrcSubscriber.verifyMetrcLabel failed: %s", e.getMessage()), e);
            event.throwException(new BlazeInvalidArgException("Shipping manifest", e.getMessage()));
        }
    }

    private void createPackages(ShippingManifestResult shippingManifestResult,
                                MetricsPackagesAPIService metricsPackagesAPIService,
                                MetrcAuthorization metrcAuthorization, Set<String> requestMetrcInfos) {
        if (CollectionUtils.isNullOrEmpty(requestMetrcInfos)) {
            LOG.info(String.format("Requested metrc infos empty for shipping Manifest No: %s", shippingManifestResult.getShippingManifestNo()));
            throw new BlazeInvalidArgException("Shipping manifest", String.format("Requested metrc infos empty for shipping Manifest No: %s", shippingManifestResult.getShippingManifestNo()));
        }

        Transaction transaction = transactionRepository.getById(shippingManifestResult.getTransactionId());
        if (transaction == null) {
            LOG.info(String.format("Transaction does not found for shipping Manifest No: %s", shippingManifestResult.getShippingManifestNo()));
            throw new BlazeInvalidArgException("Shipping manifest", String.format("Transaction does not found for shipping Manifest No: %s", shippingManifestResult.getShippingManifestNo()));
        }

        Shop shop = shopRepository.getById(shippingManifestResult.getShopId());
        HashMap<String, ComplianceCategory> complianceCategoryHashMap = complianceCategoryRepository.listAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId());

        List<ObjectId> productIds = new ArrayList<>();
        Set<ObjectId> batchIds = new HashSet<>();
        Set<ObjectId> categoryIds = new HashSet<>();
        Set<String> complianceIds = new HashSet<>();

        for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
            productIds.add(new ObjectId(productMetrcInfo.getProductId()));
            for (ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                if (!StringUtils.isNullOrEmpty(batchDetails.getBatchId()) && ObjectId.isValid(batchDetails.getBatchId())) {
                    batchIds.add(new ObjectId(batchDetails.getBatchId()));
                }
                if (!CollectionUtils.isNullOrEmpty(batchDetails.getMetrcPackageDetails())) {
                    batchDetails.getMetrcPackageDetails().forEach(metrcPackageDetails -> {
                        if (!StringUtils.isNullOrEmpty(metrcPackageDetails.getBatchId()) && ObjectId.isValid(metrcPackageDetails.getBatchId())) {
                            batchIds.add(new ObjectId(metrcPackageDetails.getBatchId()));
                        }

                    });
                }
            }
        }
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.findItemsInAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), Lists.newArrayList(batchIds));

        batchHashMap.values().forEach(productBatch -> {
            productIds.add(new ObjectId(productBatch.getProductId()));
        });

        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), productIds);

        for (Product product : productHashMap.values()) {
            if (!StringUtils.isNullOrEmpty(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
            if (!StringUtils.isNullOrEmpty(product.getComplianceId())) {
                complianceIds.add(product.getComplianceId());
            }
        }

        HashMap<String, ComplianceItem> complianceItemMap = complianceItemRepository.getComplianceItemByKeyAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), Lists.newArrayList(complianceIds));
        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.findItemsInAsMap(shippingManifestResult.getCompanyId(), shippingManifestResult.getShopId(), Lists.newArrayList(categoryIds));

        for (ProductMetrcInfo productMetrcInfo : shippingManifestResult.getProductMetrcInfo()) {
            for (ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                ProductBatch batch = batchHashMap.get(batchDetails.getBatchId());

                if (batch == null || batchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                    LOG.info(String.format("batch found : %s | request metrc tag : %s | status isCreated : %s", batch == null, StringUtils.isNullOrEmpty(batchDetails.getReqMetrcLabel()), batchDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created));
                    continue;
                }
                List<String> keyList = Lists.newArrayList(productMetrcInfo.getOrderItemId(), batchDetails.getBatchId(), batchDetails.getOverrideInventoryId(), batchDetails.getPrepackageItemId());
                keyList.removeAll(Lists.newArrayList("", null));
                String key = org.apache.commons.lang3.StringUtils.join(keyList.toArray(), "_");

                if (!requestMetrcInfos.contains(key)) {
                    LOG.info(String.format("Skipping package creation as its not requested: %s", key));
                    continue;
                }
                if (!CollectionUtils.isNullOrEmpty(batchDetails.getMetrcPackageDetails())) {
                    for (ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails : batchDetails.getMetrcPackageDetails()) {
                        batch = batchHashMap.get(metrcPackageDetails.getBatchId());
                        if (batch == null || metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created) {
                            LOG.info(String.format("batch found : %s | request metrc tag : %s | status isCreated : %s", batch == null, StringUtils.isNullOrEmpty(batchDetails.getReqMetrcLabel()), metrcPackageDetails.getMetrcStatus() == ShippingBatchDetails.MetrcStatus.Created));
                            continue;
                        }

                        MetricsPackages metricsPackages = null;
                        try {
                            metricsPackages = metricsPackagesAPIService.getPackage(batch.getTrackPackageLabel());
                        } catch (Exception e) {
                            LOG.info("No metrc package found..: " + batch.getTrackPackageLabel());
                        }
                        if (metricsPackages != null) {
                            String errorMessage = prepareIngredients(metrcPackageDetails.getReqMetrcLabel(), shop, metrcPackageDetails.getQuantity(), batch, metricsPackages, productHashMap, complianceCategoryHashMap, complianceItemMap, categoryHashMap, metricsPackagesAPIService, metrcAuthorization);
                            metrcPackageDetails.setMetrcStatus(StringUtils.isNullOrEmpty(errorMessage) ? ShippingBatchDetails.MetrcStatus.Created : ShippingBatchDetails.MetrcStatus.Error);
                            metrcPackageDetails.setErrorMessage(errorMessage);
                        }else if(!StringUtils.isNullOrEmpty(batch.getTrackPackageLabel())) {
                            metrcPackageDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Error);
                            metrcPackageDetails.setErrorMessage(METRIC_PACKAGE_NOT_FOUND);
                        }else{
                            metrcPackageDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Error);
                            metrcPackageDetails.setErrorMessage(METRC_LABEL_NOT_EMPTY);
                        }
                    }
                } else if (!StringUtils.isNullOrEmpty(batchDetails.getReqMetrcLabel())) {
                    MetricsPackages metricsPackages = null;
                    try {
                        metricsPackages = metricsPackagesAPIService.getPackage(batch.getTrackPackageLabel());
                    } catch (Exception e) {
                        LOG.info("No metrc package found..: " + batch.getTrackPackageLabel());
                    }
                    if (metricsPackages != null) {
                        String errorMessage = prepareIngredients(batchDetails.getReqMetrcLabel(), shop, batchDetails.getQuantity(), batch, metricsPackages, productHashMap, complianceCategoryHashMap, complianceItemMap, categoryHashMap, metricsPackagesAPIService, metrcAuthorization);
                        batchDetails.setMetrcStatus(StringUtils.isNullOrEmpty(errorMessage) ? ShippingBatchDetails.MetrcStatus.Created : ShippingBatchDetails.MetrcStatus.Error);
                        batchDetails.setErrorMessage(errorMessage);
                    }else if(!StringUtils.isNullOrEmpty(batch.getTrackPackageLabel())) {
                        batchDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Error);
                        batchDetails.setErrorMessage(METRIC_PACKAGE_NOT_FOUND);
                    }else{
                        batchDetails.setMetrcStatus(ShippingBatchDetails.MetrcStatus.Error);
                        batchDetails.setErrorMessage(METRC_LABEL_NOT_EMPTY);
                    }
                }
            }
        }

        shippingManifestRepository.update(shippingManifestResult.getCompanyId(), shippingManifestResult.getId(), shippingManifestResult);
    }

    private String prepareIngredients(String reqMetrcLabel, Shop shop, BigDecimal quantity, ProductBatch batch, MetricsPackages metricsPackages, HashMap<String, Product> productHashMap, HashMap<String, ComplianceCategory> complianceCategoryHashMap, HashMap<String, ComplianceItem> complianceItemMap, HashMap<String, ProductCategory> categoryHashMap, MetricsPackagesAPIService metricsPackagesAPIService, MetrcAuthorization metrcAuthorization) {
        String trackPackageLabel = batch.getTrackPackageLabel();
        Product product = productHashMap.get(batch.getProductId());
        if (product == null || StringUtils.isNullOrEmpty(product.getComplianceId())) {
            return PRODUCT_COMPLIANCE_NOT_FOUND;
        }

        ProductCategory category = categoryHashMap.get(product.getCategoryId());

        if (category == null || !category.isCannabis()) {
            return PRODUCT_CATEGORY_NON_CANNABIS;
        }
        if (product.getCannabisType() == Product.CannabisType.NON_CANNABIS) {
            return PRODUCT_NON_CANNABIS;
        }

        ComplianceItem complianceItem = complianceItemMap.get(product.getComplianceId());

        if (complianceItem == null) {
            return PRODUCT_COMPLIANCE_NOT_FOUND;
        }

        ComplianceCategory complianceCategory = complianceCategoryHashMap.get(category.getComplianceId());
        if (complianceCategory == null) {
            return PRODUCT_COMPLIANCE_CATEGORY_NOT_FOUND;
        }


        MetricsPackage metricsPackage = new MetricsPackage();
        metricsPackage.setQuantity(quantity.doubleValue());
        metricsPackage.setItem(complianceItem.getData().getName()); // must be a valid item name

        long actualDate = DateTime.now().getMillis();

        metricsPackage.setActualDate(DateUtil.toDateFormatted(actualDate,shop.getTimeZone(),"yyyy-MM-dd"));
        metricsPackage.setProductionBatch(false);
        metricsPackage.setProductionBatchNumber("");
        metricsPackage.setPatientLicenseNumber("");

        // now specify quantity
        metricsPackage.setUnitOfMeasure(product.getWeightPerUnit().toMetrcMeasurement().name());

        ProductWeightTolerance.WeightKey trackWeight = ProductWeightTolerance.WeightKey.UNIT;
        trackWeight = getWeightKey(complianceCategory, metricsPackage, trackWeight);

        BigDecimal newQty = quantity;

        if (trackWeight != ProductWeightTolerance.WeightKey.UNIT && product.getWeightPerUnit() != Product.WeightPerUnit.EACH) {
            newQty = MeasurementConversionUtil.convertToMetrcQuantity(toleranceRepository,
                    product.getCompanyId(),
                    MetrcMeasurement.Grams.measurementName,
                    product,
                    newQty);
        }


        metricsPackage.setQuantity(newQty.doubleValue());
        LOG.info(String.format("Metrics Package Qty: %s", metricsPackages.getQuantity()));
        PackageIngredients packageIngredients = new PackageIngredients();
        packageIngredients.setPackage(trackPackageLabel);
        packageIngredients.setQuantity(quantity.doubleValue());
        packageIngredients.setUnitOfMeasure(metricsPackages.getUnitOfMeasureName());
        metricsPackage.setIngredients(Lists.newArrayList(packageIngredients));
        metricsPackage.setTag(reqMetrcLabel);
        try {

            List<MetricsPackage> createPackage = Lists.newArrayList(metricsPackage);
            metricsPackagesAPIService.createPackages(createPackage, metrcAuthorization.getFacilityLicense());
            LOG.info("Created package: " + reqMetrcLabel);
            return "";
        } catch (RestErrorException e) {
            LOG.error("Error creating packages: " + e.getErrorResponse(), e);
            return METRC_ERROR_CREATING_PACKAGE;
        } catch (Exception e) {
            LOG.error("Error creating packages: " + e.getMessage(), e);
            return METRC_ERROR_CREATING_PACKAGE;
        }
    }

    private ProductWeightTolerance.WeightKey getWeightKey(ComplianceCategory complianceCategory, MetricsPackage metricsPackage, ProductWeightTolerance.WeightKey trackWeight) {
        if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("CountBased")) {
            metricsPackage.setUnitOfMeasure(MetrcMeasurement.Each.measurementName);
        } else {
            trackWeight = ProductWeightTolerance.WeightKey.GRAM;
            if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("VolumedBased")) {
                metricsPackage.setUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
            } else if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("WeightBased")) {
                metricsPackage.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
            }
        }
        return trackWeight;
    }

    private void prepareManifestPackages(BigDecimal quantity, List<ManifestDestination.ManifestPackages> packagesList, String packageLabel, MetricsPackages metricsPackages, Double unitPrice) {
        LOG.info(String.format("Metrics Package (Qty * price): (%s * %s)", metricsPackages.getQuantity(), unitPrice));
        ManifestDestination.ManifestPackages packages = new ManifestDestination.ManifestPackages();
        packages.setPackageLabel(packageLabel);
        packages.setWholesalePrice(NumberUtils.round(quantity.doubleValue() * unitPrice, 2));
        if (checkPackageLabel(packagesList, packageLabel)) {
            packagesList.add(packages);
        }
    }

    private boolean checkPackageLabel(List<ManifestDestination.ManifestPackages> packagesList, String packageLabel) {
        if (CollectionUtils.isNullOrEmpty(packagesList)) {
            return true;
        }
        for (ManifestDestination.ManifestPackages manifestPackages : packagesList) {
            if (manifestPackages.getPackageLabel().equals(packageLabel)) {
                return false;
            }
        }
        return true;
    }



    private String generatePlainPlannedRoute(String origin, String receiver, String emailBody) {
        final String key = connectConfiguration.getGoogleAPIKey();
        final String apiPath = "https://maps.googleapis.com/maps/api/directions/json";
        if (StringUtils.isNullOrEmpty(origin) || StringUtils.isNullOrEmpty(receiver)) {
            return receiver;
        }
        final String urlPath = String.format("%s?origin=%s&destination=%s&mode=%s&key=%s",
                apiPath,
                origin,
                receiver,
                "driving",
                key);

        StringBuilder routeDetail = new StringBuilder();
        routeDetail.append(receiver);
        routeDetail.append("\n\n");
        routeDetail.append("---------------------------");
        routeDetail.append("\n\n");

        try {

            Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(apiPath)
                    .queryParam("origin", origin)
                    .queryParam("destination", receiver)
                    .queryParam("mode", "driving")
                    .queryParam("key", key);
            String result = SimpleRestUtil.get(String.class, webTarget);


            JSONObject routeObject = new JSONObject(result);

            if (routeObject.has("routes") && routeObject.getJSONArray("routes").length() >= 1) {
                JSONObject routes = routeObject.getJSONArray("routes").getJSONObject(0);
                routes = routes.getJSONArray("legs").getJSONObject(0);

                JSONObject distance = routes.getJSONObject("distance");
                JSONObject duration = routes.getJSONObject("duration");

                routeDetail.append("Expected Distance: " +  ((distance != null && distance.has("text")) ? distance.getString("text") : ""));
                routeDetail.append("\n");
                routeDetail.append("Expected Time: " +  ((duration != null && duration.has("text")) ? duration.getString("text") : ""));

                routeDetail.append("\n\n");

                if (routes.has("steps") && routes.getJSONArray("steps").length() >= 1) {
                    JSONArray steps = routes.getJSONArray("steps");

                    for (int i = 0; i < steps.length(); i++) {
                        String html = steps.getJSONObject(i).getString("html_instructions");
                        JSONObject jsonObject =  steps.getJSONObject(i);
                        JSONObject subDistance = jsonObject.getJSONObject("distance");
                        routeDetail.append("\n")
                                .append((i + 1))
                                .append(": " + html.replaceAll("&nbsp;", " ") + "; ")
                                .append(subDistance.getString("text"));
                    }
                }
                String routeInfo = routeDetail.toString();
                routeInfo = routeInfo.replaceAll("\\<[^>]*>","");
                return routeInfo;

            }

        } catch (Exception e) {
            LOG.error("Error retrieving distance", e);
        }

        return routeDetail.toString();
    }




    @Subscribe
    public void verifyAndCreateDerivedPackage(MetrcVerifyCreatePackage event) {
        if (event == null || StringUtils.isNullOrEmpty(event.getMetrcTag())) {
            LOG.info("Requested metrc label is empty.");
            return;
        }

        if (event.getDerivedBatchAddRequest()  == null) {
            LOG.info("Derived product not found");
            return;
        }

        Shop shop = shopRepository.get(event.getCompanyId(), event.getShopId());

        Product targetProduct = event.getProduct();
        if (targetProduct == null) {
            event.throwException(new BlazeInvalidArgException(METRC, "Target product does not exist."));
            return;
        }
        if (targetProduct.isDeleted()) {
            event.throwException(new BlazeInvalidArgException(METRC, "Target product is deleted."));
            return;
        }

        // skip any products that aren't active
        if (!targetProduct.isActive()) {
            event.throwException(new BlazeInvalidArgException(METRC, "Target product is inactive."));
            return;
        }
        ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(event.getCompanyId(),
                ProductWeightTolerance.WeightKey.OUNCE);
        ProductWeightTolerance eighthTolerance = toleranceRepository.getToleranceForWeight(event.getCompanyId(),
                ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
        ProductWeightTolerance fourthTolerance = toleranceRepository.getToleranceForWeight(event.getCompanyId(),
                ProductWeightTolerance.WeightKey.QUARTER);

        double ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue.doubleValue();
        double eighthUnitValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue.doubleValue();
        double fourthUnitValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue.doubleValue();
        if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
            ozUnitValue = ozTolerance.getUnitValue().doubleValue();
        }

        if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
            eighthUnitValue = eighthTolerance.getUnitValue().doubleValue();
        }

        if (fourthTolerance != null && fourthTolerance.getUnitValue().doubleValue() > 0) {
            fourthUnitValue = fourthTolerance.getUnitValue().doubleValue();
        }



        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(event.getCompanyId(), event.getShopId());
        MetricsItemsAPIService metricsItemsAPIService = new MetricsItemsAPIService(metrcAuthorization);
        MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);

        // check if targetProduct's name exists in metrc, if it does, perfect.
        ComplianceItem item = createMetrcItem(targetProduct, metrcAuthorization, metricsItemsAPIService);

        // if target product does not exist, we need to create it in metrc
        // 1. Create metrc items if not exists.
        // 2. Fetch metrc items
        // 3. Associate metrc item to product.complianceId

        if (item == null) {
            event.throwException(new BlazeInvalidArgException(METRC, "Error in creating metrc item"));
            return;
        }

        ProductCategory productCategory = categoryRepository.getById(targetProduct.getCategoryId());

        ComplianceCategory complianceCategory = complianceCategoryRepository.getById(productCategory.getComplianceId());
        if (complianceCategory == null) {
            LOG.info("Compliance category is not found.");
            event.throwException(new BlazeInvalidArgException(METRC, "Compliance category doesn't found."));
            return;
        }

        // create package
        MetricsPackage metricsPackage = new MetricsPackage();
        metricsPackage.setQuantity(event.getQuantity().doubleValue());
        metricsPackage.setItem(item.getData().getName()); // must be a valid item name

        String timeZone = shop != null ? shop.getTimeZone() : "";
        String actualDate = DateUtil.toDateFormatted(DateTime.now().getMillis(), timeZone,"yyyy-MM-dd");

        // Use receive date, otherwise, use purchaseDate
        if (!StringUtils.isNullOrEmpty(event.getActualDate())) {
            actualDate = event.getActualDate();
        }

        metricsPackage.setActualDate(actualDate);
        metricsPackage.setProductionBatch(true);
        metricsPackage.setProductionBatchNumber(event.getSku());
        metricsPackage.setIngredients(new ArrayList<>());
        metricsPackage.setPatientLicenseNumber("");

        // now specify quantity
        metricsPackage.setUnitOfMeasure(targetProduct.getWeightPerUnit().toMetrcMeasurement().name());
        metricsPackage.setQuantity(event.getQuantity().doubleValue());

        ProductWeightTolerance.WeightKey trackWeight = ProductWeightTolerance.WeightKey.UNIT;
        trackWeight = getWeightKey(complianceCategory, metricsPackage, trackWeight);

        BigDecimal newQty = event.getQuantity();

        if (trackWeight != ProductWeightTolerance.WeightKey.UNIT && targetProduct.getWeightPerUnit() != Product.WeightPerUnit.EACH) {
            newQty = MeasurementConversionUtil.convertToMetrcQuantity(toleranceRepository,
                    targetProduct.getCompanyId(),
                    MetrcMeasurement.Grams.measurementName,
                    targetProduct,
                    newQty);
        }

        // supply the ingredients
        List<PackageIngredients> ingredients = new ArrayList<>();
        Set<ObjectId> batchIds = new HashSet<>();

        for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : event.getDerivedBatchAddRequest().getProductMap().entrySet()) {
            for (DerivedProductBatchInfo batchInfo : entry.getValue()) {
                if (!StringUtils.isNullOrEmpty(batchInfo.getBatchId()) && ObjectId.isValid(batchInfo.getBatchId())) {
                    batchIds.add(new ObjectId(batchInfo.getBatchId()));
                }
            }
        }

        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(event.getCompanyId(), Lists.newArrayList(batchIds));

        for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : event.getDerivedBatchAddRequest().getProductMap().entrySet()) {
            for (DerivedProductBatchInfo batchInfo : entry.getValue()) {
                ProductBatch productBatch = batchHashMap.get(batchInfo.getBatchId());
                if (productBatch != null && !StringUtils.isNullOrEmpty(productBatch.getTrackPackageLabel())) {
                    // add to ingredients

                    MetricsPackages ingredientPackage = null;
                    Product product = productRepository.get(event.getCompanyId(),productBatch.getProductId());
                    try {
                        ingredientPackage = metricsPackagesAPIService.getPackageLabel(productBatch.getTrackPackageLabel());


                        PackageIngredients ingredient = new PackageIngredients();
                        ingredient.setPackage(productBatch.getTrackPackageLabel());
                        BigDecimal qty = batchInfo.getQuantity().multiply(event.getQuantity());


                        double quantity = qty.abs().doubleValue();
                        double quantityInGrams = quantity;


                        if ((productBatch.getTrackWeight() == null || productBatch.getTrackWeight() != ProductWeightTolerance.WeightKey.UNIT)
                                || (ingredientPackage != null && "Grams".equalsIgnoreCase(ingredientPackage.getUnitOfMeasureName()))) {
                            // if weight == null or weight not equal to UNIT

                            if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                                double quantityInOunces = quantity;
                                quantityInOunces = NumberUtils.round(quantityInOunces, 6);
                                ingredient.setQuantity(quantityInOunces);
                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                                quantityInGrams = (quantity / 2); // divide by 2 to get half a gram per t
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                                quantityInGrams = quantity * eighthUnitValue; // multiply by eighth to get amount in grams eighth value
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                                quantityInGrams = quantity * fourthUnitValue; // multiply by FOURTH to get amount in grams fourth value
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null
                                    && product.getCustomWeight().doubleValue() > 0) {
                                if (Product.CustomGramType.GRAM == product.getCustomGramType()) {
                                    quantityInGrams = quantity * product.getCustomWeight().doubleValue();
                                    quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                                } else if (Product.CustomGramType.MILLIGRAM == product.getCustomGramType()) {
                                    quantityInGrams = quantity * (product.getCustomWeight().doubleValue() / 1000);
                                    quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                                }

                            } else {
                                // set to EACH
                                ingredient.setUnitOfMeasure(ingredientPackage.getUnitOfMeasureName());
                                quantityInGrams = quantity;
                                ingredient.setQuantity(quantityInGrams);
                            }
                        } else {
                            // metrc track is also in units so send in units
                            ingredient.setUnitOfMeasure("Each");
                            quantityInGrams = quantity;
                            ingredient.setQuantity(quantityInGrams);
                        }


                        // need to use what's metrc is doing
                        ingredients.add(ingredient);

                    } catch (Exception e) {

                    }

                    // add ingredient here
                }
            }
        }

        metricsPackage.setIngredients(ingredients);
        metricsPackage.setQuantity(newQty.doubleValue());
        List<String> errorTags = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(event.getMetrcTag()) && ingredients.size() > 0) {
            metricsPackage.setTag(event.getMetrcTag());
            try {
                List<MetricsPackage> createPackage = Lists.newArrayList(metricsPackage);
                metricsPackagesAPIService.createPackages(createPackage, metrcAuthorization.getFacilityLicense());
                LOG.info("Created package: " + event.getMetrcTag());
                // dbBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());
            } catch (RestErrorException e) {
                LOG.error("Error creating packages: " + e.getErrorResponse(), e);
                errorTags.add(targetProduct.getName() + " - TAG: " + event.getMetrcTag() + " - " + e.getErrorResponse());

                event.throwException(new BlazeInvalidArgException("METRC", targetProduct.getName() + " - TAG: " + event.getMetrcTag() + " - " + e.getErrorResponse()));
            } catch (Exception e) {
                LOG.error("Error creating packages: " + e.getMessage(), e);
                errorTags.add(targetProduct.getName() + " - TAG: " + event.getMetrcTag() + " - " + e.getMessage());
                event.throwException(new BlazeInvalidArgException("METRC", targetProduct.getName() + " - TAG: " + event.getMetrcTag() + " - " + e.getMessage()));
            }
        }

        // FINAL: create package in metrc

        event.setResponse(errorTags);
    }

    private ComplianceItem createMetrcItem(Product product, MetrcAuthorization metrcAuthorization, MetricsItemsAPIService metricsItemsAPIService) {
        if (!StringUtils.isNullOrEmpty(product.getComplianceId())) {
            return complianceItemRepository.getComplianceItemByKey(product.getCompanyId(), product.getShopId(), product.getComplianceId());
        }

        ProductCategory productCategory = categoryRepository.getById(product.getCategoryId());
        if (productCategory == null) {
            LOG.info("Category does not exist.");
            throw new BlazeInvalidArgException(METRC, "Category does not exist.");
        }
        if (!productCategory.isCannabis()) {
            LOG.info(String.format("Category %s is non cannabis.", productCategory.getName()));
            throw new BlazeInvalidArgException(METRC, String.format("Category %s is non cannabis.", productCategory.getName()));
        }

        if (product.getCannabisType() == Product.CannabisType.NON_CANNABIS) {
            LOG.info(String.format("Product %s is non cannabis.", product.getName()));
            throw new BlazeInvalidArgException(METRC, String.format("Category %s is non cannabis.", product.getName()));
        }

        ComplianceCategory complianceCategory = complianceCategoryRepository.getById(productCategory.getComplianceId());
        if (complianceCategory == null) {
            LOG.info("Compliance category doesn't found.");
            throw new BlazeInvalidArgException(METRC, "Compliance category doesn't found.");
        }

        ComplianceStrain defaultStrain = metrcSyncSubscriber.getGeneralFlowerStrain(product.getCompanyId(),product.getShopId(),metrcAuthorization);

        // create a compliance item and assign it
        MetricsItem metricsItem = new MetricsItem();
        metricsItem.setItemCategory(complianceCategory.getKey());

        String name = product.getName().trim();
        String cleanName = org.apache.commons.lang3.StringUtils.normalizeSpace(name).toLowerCase();

        metricsItem.setName(cleanName);
        metricsItem.setUnitOfMeasure(product.getWeightPerUnit().toMetrcMeasurement().name());
        metricsItem.setUnitWeight(new BigDecimal(1));
        metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);

        BigDecimal value = new BigDecimal(1);
        if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
            metricsItem.setUnitWeight(new BigDecimal(1));
            metricsItem.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
        } else {
            if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null) {
                metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                value = product.getCustomWeight();
                if (value.doubleValue() == 0) {
                    value = BigDecimal.valueOf(1);
                }
                metricsItem.setUnitWeight(value);
            }

            metricsItem.setUnitQuantity(new BigDecimal(1));
        }

        if (complianceCategory.getData() != null) {
            switch (complianceCategory.getData().getQuantityType()) {
                case "WeightBased" :
                    metricsItem.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                    break;
                case "VolumedBased" :
                    metricsItem.setUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
                    break;
                default :
                    metricsItem.setUnitOfMeasure(MetrcMeasurement.Each.measurementName);
            }

            MetricsItemCategory metricsItemCategory = complianceCategory.getData();
            if (metricsItemCategory.isRequiresItemBrand()) {
                metricsItem.setItemBrand(product.getBrandId());
            }

            if (metricsItemCategory.isRequiresStrain() && defaultStrain != null) {
                metricsItem.setStrain(defaultStrain.getKey());

            }

            if (metricsItemCategory.isRequiresServingSize()) {
                metricsItem.setServingSize("1");
            }
            if (metricsItemCategory.isRequiresIngredients()) {
                metricsItem.setIngredients("-");
            }
            if (metricsItemCategory.isRequiresUnitThcContent()) {
                if (product.getPotencyAmount() != null) {
                    PotencyMG potencyMG = product.getPotencyAmount();
                    metricsItem.setUnitThcContentUnitOfMeasure(MetrcMeasurement.Milligrams.measurementName);
                    metricsItem.setUnitThcContent(potencyMG.getThc());
                }
            }

            if (metricsItemCategory.isRequiresUnitCbdContent()) {
                if (product.getPotencyAmount() != null) {
                    PotencyMG potencyMG = product.getPotencyAmount();
                    metricsItem.setUnitThcContentUnitOfMeasure(MetrcMeasurement.Milligrams.measurementName);
                    metricsItem.setUnitCbdContent(potencyMG.getCbd());
                }
            }
            if (metricsItemCategory.isRequiresUnitWeight()) {
                metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                metricsItem.setUnitWeight(value);
            }

            if (metricsItemCategory.isRequiresUnitVolume()) {
                metricsItem.setUnitVolume(value);
                metricsItem.setUnitVolumeUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);

                metricsItem.setUnitWeightUnitOfMeasure(null);
                metricsItem.setUnitWeight(null);
            }
        }

        try {
            String newName = String.format("%s%s",metricsItem.getName(),BEGINNING_INVENTORY);
            metricsItem.setName(newName);
            LOG.info("Creating item: " + metricsItem.getName());
            metricsItemsAPIService.createItems(Lists.newArrayList(metricsItem), metrcAuthorization.getFacilityLicense());
            int days = 1;
            DateTime startDate = DateTime.now().minusMinutes(10);
            List<ComplianceItem> compliancePackageList = new ArrayList<>();

            HashMap<String, ComplianceItem> oldItemsMap = complianceItemRepository.getItemsAsMapById(product.getCompanyId(), product.getShopId());
            compliancePackageList.addAll(metrcSyncSubscriber.getItems(oldItemsMap, product.getCompanyId(), product.getShopId(), metricsItemsAPIService, metrcAuthorization, startDate));

            // save these newly created items
            complianceItemRepository.save(compliancePackageList);
            HashMap<String, ComplianceItem> complianceItemHashMap = new HashMap<>();
            for (ComplianceItem complianceItem : compliancePackageList) {
                complianceItemHashMap.put(complianceItem.getData().getName().replaceAll(BEGINNING_INVENTORY,"").trim().toLowerCase(), complianceItem);
            }
            ComplianceItem complianceItem = complianceItemHashMap.get(cleanName);

            if (complianceItem != null) {
                productRepository.updateComplianceId(product.getCompanyId(), product.getId(), complianceItem.getData().getId() + "");
                complianceItemRepository.updateProductId(complianceItem.getCompanyId(), complianceItem.getId(), product.getId());
                product.setComplianceId(complianceItem.getData().getId() + "");
                complianceItem.setProductId(product.getId());
                return complianceItem;
            }

            throw new BlazeInvalidArgException(METRC, String.format("Compliance item for %s not found.", name));
        } catch (RestErrorException e) {
            LOG.error("Error creating items: " + e.getErrorResponse(), e);
            throw new BlazeInvalidArgException(METRC, "Error creating items: " + e.getErrorResponse());
        } catch (Exception e) {
            LOG.error("Error creating items: " + e.getMessage(), e);
            throw new BlazeInvalidArgException(METRC, "Error creating items: " + e.getMessage());
        }
    }
}
