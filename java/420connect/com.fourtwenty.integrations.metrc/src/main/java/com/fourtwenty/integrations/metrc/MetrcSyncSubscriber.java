package com.fourtwenty.integrations.metrc;

import com.blaze.clients.metrcs.*;
import com.blaze.clients.metrcs.models.MetrcError;
import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.items.MetricsItem;
import com.blaze.clients.metrcs.models.items.MetricsItemCategory;
import com.blaze.clients.metrcs.models.items.MetricsItemsList;
import com.blaze.clients.metrcs.models.packages.MetricsPackage;
import com.blaze.clients.metrcs.models.strains.MetrcStrainRequest;
import com.blaze.clients.metrcs.models.strains.MetricsStrain;
import com.blaze.clients.metrcs.models.strains.MetricsStrainList;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.compliance.ComplianceBatchPackagePair;
import com.fourtwenty.core.domain.models.compliance.ComplianceCategory;
import com.fourtwenty.core.domain.models.compliance.ComplianceItem;
import com.fourtwenty.core.domain.models.compliance.ComplianceStrain;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceCategoryRepository;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceItemRepository;
import com.fourtwenty.core.domain.repositories.compliance.CompliancePackageRepository;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceStrainRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.compliance.AssignTagComplianceEvent;
import com.fourtwenty.core.event.compliance.CreateItemComplianceEvent;
import com.fourtwenty.core.event.compliance.StrainComplianceEvent;
import com.fourtwenty.core.event.compliance.SyncComplianceEvent;
import com.fourtwenty.core.event.compliance.SyncComplianceResult;
import com.fourtwenty.core.event.compliance.SyncUpComplianceEvent;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.managed.BGComplianceTaskManager;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.MeasurementConversionUtil;
import com.fourtwenty.integrations.metrc.tasks.MetrcSyncTask;
import com.fourtwenty.integrations.metrc.tasks.MetrcSyncUpTask;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Injector;
import com.mdo.pusher.RestErrorException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class MetrcSyncSubscriber implements BlazeSubscriber {
    private static final Log LOG = LogFactory.getLog(MetrcSyncSubscriber.class);
    public static final String BEGINNING_INVENTORY = " - Beginning Inventory";
    private static final boolean UPDATE_PACKAGES = true;
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    ComplianceCategoryRepository complianceCategoryRepository;
    @Inject
    BrandRepository brandRepository;
    @Inject
    ComplianceItemRepository complianceItemRepository;
    @Inject
    CompliancePackageRepository compliancePackageRepository;
    @Inject
    BGComplianceTaskManager bgComplianceTaskManager;
    @Inject
    ShopRepository shopRepository;
    @Inject
    ComplianceStrainRepository complianceStrainRepository;
    @Inject
    private Injector injector;
    @Inject
    private MetrcService metrcService;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductWeightToleranceRepository toleranceRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private InventoryRepository inventoryRepository;

    @Subscribe
    public void sync(SyncComplianceEvent event) {
        String stateCode = metrcService.getShopStateCode(event.getCompanyId(), event.getShopId());
        // Ignore if not enabled
        MetrcFacilityAccount account = metrcService.getMetrcAccount(event.getCompanyId(), stateCode)
                .getMetrcFacilityAccount(event.getShopId());
        if (account == null || !account.isEnabled()) return;

        if (StringUtils.isBlank(account.getFacLicense()))
            event.throwException(new BlazeOperationException("The facility license is not configured for this shop."));

        MetrcSyncTask syncTask = injector.getInstance(MetrcSyncTask.class);
        syncTask.setCompanyId(event.getCompanyId());
        syncTask.setShopId(event.getShopId());
        try {
            syncTask.execute(null,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //bgComplianceTaskManager.runTaskInBackground(syncTask);
        event.setResponse(new SyncComplianceResult());
    }

    @Subscribe
    public void subscribeSyncUp(SyncUpComplianceEvent event) {
        Shop shop = shopRepository.get(event.getCompanyId(),event.getShopId());
        if (shop == null || shop.isActive() == false || shop.isDeleted()) {
            return;
        }
        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(event.getCompanyId(), event.getShopId());
        MetricsItemsAPIService metricsItemsAPIService = new MetricsItemsAPIService(metrcAuthorization);

        MetrcSyncUpTask syncTask = injector.getInstance(MetrcSyncUpTask.class);
        syncTask.setCompanyId(event.getCompanyId());
        syncTask.setShopId(event.getShopId());
        syncTask.setComplianceEvent(event);
        try {
            syncTask.execute(null,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //bgComplianceTaskManager.runTaskInBackground(syncTask);
        event.setResponse(new SyncComplianceResult());
    }


    @Subscribe
    public void subscribeAssignTags(AssignTagComplianceEvent event) {
        Shop shop = shopRepository.get(event.getCompanyId(), event.getShopId());
        if (shop == null || shop.isActive() == false || shop.isDeleted()) {
            return;
        }
        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(event.getCompanyId(), event.getShopId());
        MetricsItemsAPIService metricsItemsAPIService = new MetricsItemsAPIService(metrcAuthorization);

        HashMap<String, Product> productHashMap = productRepository.listAsMap(event.getCompanyId(), event.getShopId());
        MetricsItemsAPIService itemsAPIService = new MetricsItemsAPIService(metrcAuthorization);
        HashMap<String, ProductCategory> productCategoryHashMap = categoryRepository.listAsMap(event.getCompanyId(), event.getShopId());
        HashMap<String, ComplianceCategory> complianceCategoryHashMap = complianceCategoryRepository.listAsMap(event.getCompanyId(), event.getShopId());

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(event.getCompanyId());

        /*Iterable<ProductBatch> productBatches = productBatchRepository.getBatchesLiveNonMetrcBatches(event.getCompanyId(),
                event.getShopId(),
                0, Integer.MAX_VALUE);*/
        Iterable<ProductBatch> productBatches = productBatchRepository.listAllByShop(event.getCompanyId(),event.getShopId());
        HashMap<String,ProductBatch> batchHashMap = new HashMap<>();
        for (ProductBatch productBatch : productBatches) {
            batchHashMap.put(productBatch.getId(),productBatch);
        }


        List<ComplianceBatchPackagePair> packageTags = event.getPackageTags();
        List<MetricsItem> items = new ArrayList<>();
        DateTime now = DateTime.now();
        if (packageTags != null && packageTags.size() > 0) {
            items = getMetrcItemsToBeCreated(metrcAuthorization,event, packageTags, batchHashMap, productHashMap, productCategoryHashMap);

            // create the metrc items
            for (MetricsItem item: items) {
                try {
                    String newName = String.format("%s%s",item.getName(),BEGINNING_INVENTORY);
                    item.setName(newName);
                    LOG.info("Creating item: " + item.getName());
                    metricsItemsAPIService.createItems(Lists.newArrayList(item), metrcAuthorization.getFacilityLicense());
                } catch (Exception e) {
                    LOG.error("Error creating items: " + e.getMessage(), e);
                }
            }

            // retrieve the list of metrc items and persist them
            int days = 1;
            DateTime startDate = now.minusMinutes(10);
            List<ComplianceItem> compliancePackageList = new ArrayList<>();

            HashMap<String, ComplianceItem> oldItemsMap = complianceItemRepository.getItemsAsMapById(event.getCompanyId(), event.getShopId());
            compliancePackageList.addAll(getItems(oldItemsMap, event.getCompanyId(), event.getShopId(), metricsItemsAPIService, metrcAuthorization, startDate));

            // save these newly created items
            complianceItemRepository.save(compliancePackageList);
            HashMap<String, ComplianceItem> complianceItemHashMap = new HashMap<>();
            for (ComplianceItem complianceItem : compliancePackageList) {
                complianceItemHashMap.put(complianceItem.getData().getName().replaceAll(BEGINNING_INVENTORY,"").trim().toLowerCase(), complianceItem);
            }

            // associate products
            Collection<Product> products = productHashMap.values();
            for (Product product : products) {
                if (StringUtils.isBlank(product.getComplianceId())) {

                    String name = product.getName().trim();
                    String cleanName = StringUtils.normalizeSpace(name).toLowerCase();
                    ComplianceItem complianceItem = complianceItemHashMap.get(cleanName);

                    if (complianceItem != null) {
                        product.setComplianceId(complianceItem.getData().getId() + "");
                        productRepository.updateComplianceId(product.getCompanyId(), product.getId(), complianceItem.getData().getId() + "");
                        complianceItem.setProductId(product.getId());
                        complianceItemRepository.updateProductId(complianceItem.getCompanyId(), complianceItem.getId(), product.getId());
                    }
                }
            }

            // now get products to be created
            createPackagesFromBatches(metrcAuthorization,shop,event,packageTags, batchHashMap,productHashMap,productCategoryHashMap);
        }

        event.setResponse(new SyncComplianceResult());
    }

    private List<MetricsPackage> createPackagesFromBatches(final MetrcAuthorization metrcAuthorization,
                                                           final Shop shop,
                                             final AssignTagComplianceEvent event,
                                                           final List<ComplianceBatchPackagePair> packageTagPairs,
                                                           final HashMap<String,ProductBatch> batchHashMap,
                                         final HashMap<String, Product> productHashMap,
                                         final HashMap<String, ProductCategory> productCategoryHashMap) {



        HashMap<String, ComplianceCategory> complianceCategoryHashMap = complianceCategoryRepository.listAsMap(event.getCompanyId(), event.getShopId());
        MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(shop.getCompanyId(), shop.getId());


        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.listAllByShop(shop.getCompanyId(), shop.getId());

        HashMap<String,BigDecimal> batchQuantityMap = new HashMap<>();
        for (BatchQuantity batchQuantity : batchQuantities) {
            Inventory inventory = inventoryMap.get(batchQuantity.getInventoryId());
            if (inventory == null || inventory.isDeleted() || !inventory.isActive()) {
                continue;
            }
            BigDecimal liveQuantity = batchQuantityMap.getOrDefault(batchQuantity.getBatchId(),BigDecimal.ZERO);
            liveQuantity = liveQuantity.add(batchQuantity.getQuantity());
            batchQuantityMap.put(batchQuantity.getBatchId(),liveQuantity);
        }

        List<MetricsPackage> packages = new ArrayList<>();
        int created = 0;
        List<String> errorTags = new ArrayList<>();
        for (ComplianceBatchPackagePair batchPackagePair : packageTagPairs) {
            if (batchPackagePair.getTag().equalsIgnoreCase("N/A")) {
                continue;
            }

            ProductBatch productBatch = batchHashMap.get(batchPackagePair.getBatchId());
            if (productBatch == null) {
                continue;
            }
            BigDecimal liveQuantity = batchQuantityMap.getOrDefault(productBatch.getId(),BigDecimal.ZERO);

            if (liveQuantity.doubleValue() <= 0) {
                continue;
            }
            productBatch.setLiveQuantity(liveQuantity);

            Product product = productHashMap.get(productBatch.getProductId());
            if (product != null
                    && (StringUtils.isBlank(productBatch.getTrackPackageLabel())
                    || (UPDATE_PACKAGES && !batchPackagePair.getTag().equalsIgnoreCase(productBatch.getTrackPackageLabel())))) {
                if (product.isDeleted()) {
                    continue;
                }

                // skip any products that don't have compliance id
                if (StringUtils.isBlank(product.getComplianceId())) {
                    continue;
                }

                // skip any products that aren't active
                if (product.isActive() == false) {
                    continue;
                }

                ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
                if (category == null) {
                    continue;
                }

                if (!category.isCannabis()) {
                    continue;
                }

                if (product.getCannabisType() == Product.CannabisType.NON_CANNABIS) {
                    continue;
                }
                ComplianceCategory complianceCategory = complianceCategoryHashMap.get(category.getComplianceId());
                if (complianceCategory == null) {
                    continue;
                }


                ComplianceItem complianceItem = complianceItemRepository.getComplianceItemByKey(event.getCompanyId(), product.getShopId(), product.getComplianceId());
                if (complianceItem != null) {

                    MetricsPackage metricsPackage = new MetricsPackage();
                    metricsPackage.setQuantity(liveQuantity.doubleValue()); //TODO: LIVE QTY HERE IS NOT ACCURATE. NEED TO SUM IT UP FROM BATCH QUANTITIES
                    metricsPackage.setItem(complianceItem.getData().getName()); // must be a valid item name

                    long actualDate = DateTime.now().getMillis();
                    // Use receive date, otherwise, use purchaseDate
                    if (productBatch.getReceiveDate() != null && productBatch.getReceiveDate() > 0) {
                        actualDate = productBatch.getReceiveDate();
                    } else if (productBatch.getPurchasedDate() > 0) {
                        actualDate = productBatch.getPurchasedDate();
                    } else {
                        actualDate = productBatch.getCreated().longValue();
                    }
                    if (actualDate < DateTime.now().minusYears(10).getMillis()) {
                        actualDate = productBatch.getCreated().longValue();
                    }

                    metricsPackage.setActualDate(DateUtil.toDateFormatted(actualDate,shop.getTimeZone(),"yyyy-MM-dd"));
                    metricsPackage.setProductionBatch(true);
                    metricsPackage.setProductionBatchNumber(productBatch.getSku());
                    metricsPackage.setIngredients(new ArrayList<>());
                    metricsPackage.setPatientLicenseNumber("");

                    // now specify quantity
                    metricsPackage.setUnitOfMeasure(product.getWeightPerUnit().toMetrcMeasurement().name());
                    metricsPackage.setQuantity(productBatch.getLiveQuantity().doubleValue());

                    ProductWeightTolerance.WeightKey trackWeight = ProductWeightTolerance.WeightKey.UNIT;
                    if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("CountBased")) {
                        metricsPackage.setUnitOfMeasure(MetrcMeasurement.Each.measurementName);
                    } else {
                        trackWeight = ProductWeightTolerance.WeightKey.GRAM;
                        if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("WeightBased")) {
                            metricsPackage.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                        } else if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("VolumedBased")) {
                            metricsPackage.setUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
                        }
                    }

                    BigDecimal newQty = productBatch.getLiveQuantity();

                    if (trackWeight != ProductWeightTolerance.WeightKey.UNIT && product.getWeightPerUnit() != Product.WeightPerUnit.EACH) {
                        newQty = MeasurementConversionUtil.convertToMetrcQuantity(toleranceRepository,
                                product.getCompanyId(),
                                MetrcMeasurement.Grams.measurementName,
                                product,
                                newQty);
                    }


                    metricsPackage.setQuantity(newQty.doubleValue());

                    packages.add(metricsPackage);

                    boolean createSuccess = false;
                    if (batchPackagePair != null && StringUtils.isNotBlank(batchPackagePair.getTag())) {
                        metricsPackage.setTag(batchPackagePair.getTag());
                        try {
                            List<MetricsPackage> createPackage = Lists.newArrayList(metricsPackage);
                            metricsPackagesAPIService.createPackages(createPackage, metrcAuthorization.getFacilityLicense());
                            LOG.info("Created package: " + batchPackagePair.getTag());
                            // dbBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());
                            productBatchRepository.updateMetrcTag(productBatch.getId(), batchPackagePair.getTag(), trackWeight);
                            createSuccess = true;
                            created++;
                        } catch (RestErrorException e) {
                            LOG.error("Error creating packages: " + e.getErrorResponse(), e);
                            errorTags.add(product.getName() + " - TAG: " + batchPackagePair.getTag() + " - " + e.getErrorResponse());
                        } catch (Exception e) {
                            LOG.error("Error creating packages: " + e.getMessage(), e);
                            errorTags.add(product.getName() + " - TAG: " + batchPackagePair.getTag() + " - " + e.getMessage());
                        }
                    }
                }


            }
        }
        LOG.info("Total created: " + created);
        LOG.info("Total Errors: " + errorTags.size());
        errorTags.forEach((tag) -> {
            LOG.info(tag);
        });
        return packages;
    }

    private List<MetricsItem> getMetrcItemsToBeCreated(final MetrcAuthorization metrcAuthorization,
                                                       final AssignTagComplianceEvent event,
                                                         final List<ComplianceBatchPackagePair> packageTagPairs,
                                                       final HashMap<String,ProductBatch> batchHashMap,
                                                         final HashMap<String, Product> productHashMap,
                                                         final HashMap<String, ProductCategory> productCategoryHashMap) {
        HashMap<String, ComplianceCategory> complianceCategoryHashMap = complianceCategoryRepository.listAsMap(event.getCompanyId(), event.getShopId());

        ComplianceStrain defaultStrain = getGeneralFlowerStrain(event.getCompanyId(),event.getShopId(),metrcAuthorization);
        HashMap<String,ComplianceStrain> strainsMap = complianceStrainRepository.getItemsAsMapById(event.getCompanyId(),event.getShopId());

        Set<MetricsItem> uniqueItems = new HashSet<>();
        for (ComplianceBatchPackagePair batchPackagePair : packageTagPairs) {
            if (batchPackagePair.getTag().equalsIgnoreCase("N/A")) {
                continue;
            }

            ProductBatch productBatch = batchHashMap.get(batchPackagePair.getBatchId());
            if (productBatch == null) {
                continue;
            }

            boolean search = true;
            Product product = productHashMap.get(productBatch.getProductId());
            if (product != null && (StringUtils.isBlank(productBatch.getTrackPackageLabel())
                    || (UPDATE_PACKAGES && !batchPackagePair.getTag().equalsIgnoreCase(productBatch.getTrackPackageLabel())))) {
                if (product.isDeleted()) {
                    continue;
                }

                // skip any products that aren't active
                if (product.isActive() == false) {
                    continue;
                }
                // 1. Check if this product is associated to an item
                // If not associated, create an item
                ComplianceItem complianceItem = null;
                if (StringUtils.isNotBlank(product.getComplianceId())) {
                    complianceItem = complianceItemRepository.getComplianceItemByKey(event.getCompanyId(), product.getShopId(), product.getComplianceId());
                }

                if (complianceItem == null) {
                    ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                    if (productCategory == null) {
                        continue;
                    }
                    if (!productCategory.isCannabis()) {
                        continue;
                    }

                    if (product.getCannabisType() == Product.CannabisType.NON_CANNABIS) {
                        continue;
                    }

                    ComplianceCategory complianceCategory = complianceCategoryHashMap.get(productCategory.getComplianceId());
                    if (complianceCategory == null) {
                        continue;
                    }

                    // create a compliance item and assign it
                    MetricsItem metricsItem = new MetricsItem();
                    metricsItem.setItemCategory(complianceCategory.getKey());

                    String name = product.getName().trim();
                    String cleanName = StringUtils.normalizeSpace(name);

                    metricsItem.setName(cleanName);
                    metricsItem.setUnitOfMeasure(product.getWeightPerUnit().toMetrcMeasurement().name());
                    metricsItem.setUnitWeight(new BigDecimal(1));
                    metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);


                    if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("WeightBased")) {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                    } else if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("VolumedBased")) {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
                    } else {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Each.measurementName);
                    }


                    BigDecimal value = new BigDecimal(1);
                    if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                        metricsItem.setUnitWeight(new BigDecimal(1));
                    } else {

                        if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null) {
                            value = product.getCustomWeight();

                            metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                            if (value.doubleValue() == 0) {
                                value = BigDecimal.valueOf(1);
                            }
                            metricsItem.setUnitWeight(value);
                        }

                        metricsItem.setUnitQuantity(new BigDecimal(1));
                    }






                    if (complianceCategory.getData() != null) {
                        MetricsItemCategory metricsItemCategory = complianceCategory.getData();
                        if (metricsItemCategory.isRequiresItemBrand()) {
                            metricsItem.setItemBrand(product.getBrandId());
                        }

                        if (metricsItemCategory.isRequiresStrain()) {
                            // need to set the strain
                            ComplianceStrain strain = strainsMap.get(batchPackagePair.getStrainName().toLowerCase());
                            if (strain != null) {
                                metricsItem.setStrain(strain.getKey());
                            } else {
                                if (defaultStrain != null) {
                                    metricsItem.setStrain(defaultStrain.getKey());
                                }
                            }
                        }

                        if (metricsItemCategory.isRequiresServingSize()) {
                            metricsItem.setServingSize("1");
                        }
                        if (metricsItemCategory.isRequiresIngredients()) {
                            metricsItem.setIngredients("-");
                        }
                        if (metricsItemCategory.isRequiresUnitThcContent()) {
                            if (product.getPotencyAmount() != null) {
                                PotencyMG potencyMG = product.getPotencyAmount();
                                metricsItem.setUnitThcContent(potencyMG.getThc());
                                metricsItem.setUnitThcContentUnitOfMeasure(MetrcMeasurement.Milligrams.measurementName);
                            }
                        }

                        if (metricsItemCategory.isRequiresUnitCbdContent()) {
                            if (product.getPotencyAmount() != null) {
                                PotencyMG potencyMG = product.getPotencyAmount();
                                metricsItem.setUnitCbdContent(potencyMG.getCbd());
                                metricsItem.setUnitThcContentUnitOfMeasure(MetrcMeasurement.Milligrams.measurementName);
                            }
                        }
                        if (metricsItemCategory.isRequiresUnitWeight()) {
                            metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                            metricsItem.setUnitWeight(value);
                        }

                        if (metricsItemCategory.isRequiresUnitVolume()) {
                            metricsItem.setUnitVolumeUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
                            metricsItem.setUnitVolume(value);

                            metricsItem.setUnitWeightUnitOfMeasure(null);
                            metricsItem.setUnitWeight(null);
                        }
                    }
                    uniqueItems.add(metricsItem);
                }
            }
        }

        List<MetricsItem> items = Lists.newArrayList(uniqueItems);
        return items;
    }


    public LinkedHashSet<ComplianceItem> getItems(final HashMap<String, ComplianceItem> itemHashMap,
                                           final String companyId,
                                           final String shopId,
                                           MetricsItemsAPIService itemsAPIService, MetrcAuthorization metrcAuthorization,
                                           DateTime aStartDate) {
        DateTime startDate = aStartDate;
        int interval = 1;
        LinkedHashSet<ComplianceItem> metricsPackages = new LinkedHashSet<>();
        DateTime now = new DateTime();
        while (startDate.isBefore(now)) {
            String startDateStr = startDate.toString();
            String endDate = (startDate.plusHours(24).toString());
            LOG.info("Syncing items.." + startDateStr);
            try {
                MetricsItemsList packages = itemsAPIService.getActiveItems(metrcAuthorization.getFacilityLicense(),
                        startDateStr,
                        endDate);

                List<ComplianceItem> compliancePackageList = new ArrayList<>();

                packages.forEach((pk) -> {
                    if (itemHashMap.get(pk.getId() + "") == null) {
                        ComplianceItem compliancePackage = new ComplianceItem();
                        compliancePackage.prepare(companyId);
                        compliancePackage.setShopId(shopId);
                        compliancePackage.setKey(pk.getId() + "");
                        compliancePackage.setSearchField(pk.getId() + pk.getName() + pk.getStrainName());
                        compliancePackage.setData(pk);
                        compliancePackageList.add(compliancePackage);

                        itemHashMap.put(compliancePackage.getKey(), compliancePackage);
                    }
                });
                metricsPackages.addAll(compliancePackageList);

            } catch (Exception e) {
            }
            startDate = startDate.plusDays(interval);
        }

        return metricsPackages;
    }

    public ComplianceStrain getGeneralFlowerStrain(
            final String companyId,
            final String shopId,
            MetrcAuthorization metrcAuthorization) {
        LOG.info("Syncing strains..");
        try {
            HashMap<String, ComplianceStrain> oldItemsMap = complianceStrainRepository.getItemsAsMapById(companyId, shopId);

            ComplianceStrain defaultStrain = popularStrains(companyId, shopId, oldItemsMap, metrcAuthorization);

            // get it again
            return defaultStrain;

        } catch (Exception e) {
            System.out.println(e);
        }
        LOG.info("Syncing strains...completed.");
        return null;
    }

    private ComplianceStrain popularStrains(
            final String companyId,
            final String shopId,
            HashMap<String, ComplianceStrain> oldItemsMap,
            MetrcAuthorization metrcAuthorization) {
        MetricsStrainsAPIService apiService = new MetricsStrainsAPIService(metrcAuthorization);
        LinkedHashSet<ComplianceStrain> items = new LinkedHashSet<>();

        getCreateStrains(companyId, shopId, apiService, metrcAuthorization, oldItemsMap, items);

        ComplianceStrain defaultStrain = oldItemsMap.get(ComplianceStrain.DefaultStrain.GeneralFlower.strainName);
        if (defaultStrain == null) {
            MetricsStrain metricsStrain = new MetricsStrain();
            metricsStrain.setName(ComplianceStrain.DefaultStrain.GeneralFlower.strainName);
            metricsStrain.setCbdLevel(0.5);
            metricsStrain.setThcLevel(0.5);
            metricsStrain.setIndicaPercentage(50);
            metricsStrain.setSativaPercentage(50);
            metricsStrain.setTestingStatus("None");

            List<MetricsStrain> strains = new ArrayList<>();
            strains.add(metricsStrain);
            try {
                String jsonBody = JsonSerializer.toJson(strains);
                apiService.createStrains(strains,metrcAuthorization.getFacilityLicense());
            } catch (Exception e) {
                LOG.error("Cannot create strain", e);
            }

            getCreateStrains(companyId, shopId, apiService, metrcAuthorization, oldItemsMap, items);
        }


        complianceStrainRepository.save(Lists.newArrayList(items));

        return defaultStrain;
    }

    private void getCreateStrains(
            final String companyId,
            final String shopId,
            final MetricsStrainsAPIService apiService,
            MetrcAuthorization metrcAuthorization,
            HashMap<String, ComplianceStrain> oldItemsMap,
            LinkedHashSet<ComplianceStrain> items) {

        MetricsStrainList strainList = apiService.getActiveStrains(metrcAuthorization.getFacilityLicense());
        strainList.forEach((strain) -> {

            if (oldItemsMap.get(strain.getName()) == null) {

                ComplianceStrain complianceStrain = new ComplianceStrain();
                complianceStrain.prepare(companyId);
                complianceStrain.setShopId(shopId);
                complianceStrain.setKey(strain.getName());
                complianceStrain.setSearchField(strain.getId() + " " + strain.getName());
                complianceStrain.setData(strain);
                items.add(complianceStrain);


                oldItemsMap.put(strain.getName(), complianceStrain);
            }
        });
    }


    @Subscribe
    public void subscribecreateItem(CreateItemComplianceEvent event) {
        Shop shop = shopRepository.get(event.getCompanyId(), event.getShopId());
        if (shop == null || shop.isActive() == false || shop.isDeleted()) {
            return;
        }


        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(event.getCompanyId(), event.getShopId());
        MetricsItemsAPIService metricsItemsAPIService = new MetricsItemsAPIService(metrcAuthorization);

        MetrcSyncUpTask syncTask = injector.getInstance(MetrcSyncUpTask.class);
        syncTask.setCompanyId(event.getCompanyId());
        syncTask.setShopId(event.getShopId());
        syncTask.setCreateItemComplianceEvent(event);

        HashMap<String, Product> productHashMap = productRepository.listAsMap(event.getCompanyId(), event.getShopId());
        HashMap<String, ProductCategory> productCategoryHashMap = categoryRepository.listAsMap(event.getCompanyId(), event.getShopId());

        List<ComplianceBatchPackagePair> packageTags = event.getPackageTags();


        AssignTagComplianceEvent assignTagComplianceEvent = new AssignTagComplianceEvent();
        assignTagComplianceEvent.setCompanyId(event.getCompanyId());
        assignTagComplianceEvent.setShopId(event.getShopId());
        assignTagComplianceEvent.setPackageTags(packageTags);
        assignTagComplianceEvent.setComplianceSyncJob(event.getComplianceSyncJob());



        List<MetricsItem> items = getCreateMetrcItems(metrcAuthorization, assignTagComplianceEvent, packageTags, productHashMap, productCategoryHashMap);
        int created = 0;
       String error = "";
       int errorCount = 0;
        // create the metrc items
        for (MetricsItem item : items) {
            try {
                metricsItemsAPIService.createItems(Lists.newArrayList(item), metrcAuthorization.getFacilityLicense());
                LOG.info("Creating items");
                created++;
            } catch (RestErrorException e) {
                LOG.error("Error creating items: " + e.getErrorResponse(), e);
                if(e.getErrorResponse() != null){
                    for(MetrcError metrcError : (MetricsErrorList) e.getErrorResponse()){
                        error +=  "Compliance Job Id : " + (event.getComplianceSyncJob().getId()) + ": " + errorCount + metrcError.getMessage() + " ,";
                        errorCount++;
                    }
                }

            } catch (Exception e) {
                LOG.error("Error creating items: " + e.getMessage(), e);
            }
        }

        // retrieve the list of metrc items and persist them
        int days = 60;
        DateTime now = DateTime.now();
        DateTime startDate = now.minusMinutes(10);
        List<ComplianceItem> compliancePackageList = new ArrayList<>();

        HashMap<String, ComplianceItem> oldItemsMap = complianceItemRepository.getItemsAsMapById(event.getCompanyId(), event.getShopId());
        compliancePackageList.addAll(getItems(oldItemsMap, event.getCompanyId(), event.getShopId(), metricsItemsAPIService, metrcAuthorization, startDate));


        // save these newly created items
        complianceItemRepository.save(compliancePackageList);
        HashMap<String, ComplianceItem> complianceItemHashMap = new HashMap<>();
        for (ComplianceItem complianceItem : compliancePackageList) {
            String name = complianceItem.getData().getName().trim();
            String cleanName = StringUtils.normalizeSpace(name).toLowerCase();
            complianceItemHashMap.put(cleanName, complianceItem);
        }


        // associate products
        Collection<Product> products = productHashMap.values();
        for (Product product : products) {
                if (StringUtils.isBlank(product.getComplianceId())) {

                    String name = product.getName().trim();
                    String cleanName = StringUtils.normalizeSpace(name);
                    ComplianceItem complianceItem = complianceItemHashMap.get(cleanName);

                    if (complianceItem != null) {
                        product.setComplianceId(complianceItem.getData().getId() + "");
                        productRepository.updateComplianceId(product.getCompanyId(), product.getId(), complianceItem.getData().getId() + "");
                        LOG.info(product.getId());
                        complianceItem.setProductId(product.getId());
                        complianceItemRepository.updateProductId(complianceItem.getCompanyId(), complianceItem.getId(), product.getId());
                    }
            }
        }

        LOG.info("Total created: " + created);
        event.setErrorMsg(error);
        event.setResponse(event);
    }


    /**
     * This method is used  to collect items to be created
     * @param metrcAuthorization
     * @param assignTagComplianceEvent
     * @param packageTags
     * @param productHashMap
     * @param productCategoryHashMap
     * @return
     */
    private List<MetricsItem> getCreateMetrcItems(MetrcAuthorization metrcAuthorization, AssignTagComplianceEvent assignTagComplianceEvent, List<ComplianceBatchPackagePair> packageTags, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> productCategoryHashMap) {


        HashMap<String, ComplianceCategory> complianceCategoryHashMap = complianceCategoryRepository.listAsMap(assignTagComplianceEvent.getCompanyId(), assignTagComplianceEvent.getShopId());
        HashMap<String, ComplianceStrain> strainsMap = complianceStrainRepository.getItemsAsMapById(assignTagComplianceEvent.getCompanyId(), assignTagComplianceEvent.getShopId());
        ComplianceStrain defaultStrain = getGeneralFlowerStrain(assignTagComplianceEvent.getCompanyId(), assignTagComplianceEvent.getShopId(), metrcAuthorization);


        Set<MetricsItem> uniqueItems = new HashSet<>();
        for (ComplianceBatchPackagePair batchPackagePair : packageTags) {
            boolean search = true;
            Product product = productHashMap.get(batchPackagePair.getProductId());
            if (product != null) {
                if (product.isDeleted()) {
                    continue;
                }

                // 1. Check if this product is associated to an item
                // If not associated, create an item
                ComplianceItem complianceItem = null;
                if (StringUtils.isNotBlank(product.getComplianceId())) {
                    complianceItem = complianceItemRepository.getComplianceItemByKey(assignTagComplianceEvent.getCompanyId(), product.getShopId(), product.getComplianceId());
                }
                if (complianceItem != null) {
                    LOG.info("Compliance Item Already exsits" + batchPackagePair.getProductId());
                }

                // skip any products that aren't active
                if (product.isActive() == false) {
                    continue;
                }


                if (complianceItem == null) {

                    ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                    if (productCategory == null) {
                        continue;
                    }


                    if (product.getCannabisType() == Product.CannabisType.NON_CANNABIS) {
                        continue;
                    }

                    if (!productCategory.isCannabis()) {
                        continue;
                    }


                    ComplianceCategory complianceCategory = complianceCategoryHashMap.get(productCategory.getComplianceId());
                    if (complianceCategory == null) {
                        continue;
                    }

                    // create a compliance item and assign it
                    MetricsItem metricsItem = new MetricsItem();
                    metricsItem.setItemCategory(complianceCategory.getKey());

                    String name = product.getName().trim();
                    String cleanName = StringUtils.normalizeSpace(name);

                    metricsItem.setName(cleanName);
                    metricsItem.setUnitOfMeasure(product.getWeightPerUnit().toMetrcMeasurement().name());
                    metricsItem.setUnitWeight(new BigDecimal(1));
                    metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);


                    if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("WeightBased")) {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                    } else if (complianceCategory.getData().getQuantityType().equalsIgnoreCase("VolumedBased")) {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
                    } else {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Each.measurementName);
                    }


                    BigDecimal value = new BigDecimal(1);
                    if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
                        metricsItem.setUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                        metricsItem.setUnitWeight(new BigDecimal(1));
                    } else {

                        if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null) {
                            value = product.getCustomWeight();

                            metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                            if (value.doubleValue() == 0) {
                                value = BigDecimal.valueOf(1);
                            }
                            metricsItem.setUnitWeight(value);
                        }

                        metricsItem.setUnitQuantity(new BigDecimal(1));
                    }


                    if (complianceCategory.getData() != null) {
                        MetricsItemCategory metricsItemCategory = complianceCategory.getData();
                        if (metricsItemCategory.isRequiresItemBrand()) {
                            metricsItem.setItemBrand(product.getBrandId());
                        }

                        if (metricsItemCategory.isRequiresStrain()) {
                            // need to set the strain
                            ComplianceStrain strain = strainsMap.get(batchPackagePair.getStrainName().toLowerCase());
                            if (strain != null) {
                                metricsItem.setStrain(strain.getKey());
                            } else {
                                if (defaultStrain != null) {
                                    metricsItem.setStrain(defaultStrain.getKey());
                                }
                            }
                        }

                        if (metricsItemCategory.isRequiresUnitThcContent()) {
                            if (product.getPotencyAmount() != null) {
                                PotencyMG potencyMG = product.getPotencyAmount();
                                metricsItem.setUnitThcContent(potencyMG.getThc());
                                metricsItem.setUnitThcContentUnitOfMeasure(MetrcMeasurement.Milligrams.measurementName);
                            }
                        }

                        if (metricsItemCategory.isRequiresServingSize()) {
                            metricsItem.setServingSize("1");
                        }
                        if (metricsItemCategory.isRequiresIngredients()) {
                            metricsItem.setIngredients("-");
                        }


                        if (metricsItemCategory.isRequiresUnitCbdContent()) {
                            if (product.getPotencyAmount() != null) {
                                PotencyMG potencyMG = product.getPotencyAmount();
                                metricsItem.setUnitCbdContent(potencyMG.getCbd());
                                metricsItem.setUnitThcContentUnitOfMeasure(MetrcMeasurement.Milligrams.measurementName);
                            }
                        }


                        if (metricsItemCategory.isRequiresUnitWeight()) {
                            metricsItem.setUnitWeightUnitOfMeasure(MetrcMeasurement.Grams.measurementName);
                            metricsItem.setUnitWeight(value);
                        }

                        if (metricsItemCategory.isRequiresUnitVolume()) {
                            metricsItem.setUnitVolumeUnitOfMeasure(MetrcMeasurement.Milliliters.measurementName);
                            metricsItem.setUnitVolume(value);

                            metricsItem.setUnitWeightUnitOfMeasure(null);
                            metricsItem.setUnitWeight(null);
                        }
                    }

                    uniqueItems.add(metricsItem);
                }
            }
        }

        List<MetricsItem> items = Lists.newArrayList(uniqueItems);
        return items;
    }


    @Subscribe
    public void sendStrainComplainceToMetrc(StrainComplianceEvent event) {

        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(event.getCompanyId(), event.getShopId());
        MetricsStrainsAPIService apiService = new MetricsStrainsAPIService(metrcAuthorization);
        HashMap<String, ComplianceStrain> oldItemsMap = complianceStrainRepository.getItemsAsMapById(event.getCompanyId(), event.getShopId());
        LinkedHashSet<ComplianceStrain> items = new LinkedHashSet<>();

        int created = 0;
        getCreateStrains(event.getCompanyId(), event.getShopId(), apiService, metrcAuthorization, oldItemsMap, items);
        List<MetricsStrain> strains = new ArrayList<>();
        String errors = "";
        int errorsCount = 0;
        if (event.getMetrcStr() != null) {

            for (MetrcStrainRequest metricsStrain : event.getMetrcStr()) {
                ComplianceStrain defaultStrain = oldItemsMap.get(metricsStrain.getName());
                if (defaultStrain == null) {
                    MetricsStrain mStrain = new MetricsStrain();
                    mStrain.setName(metricsStrain.getName());
                    mStrain.setCbdLevel(metricsStrain.getCbdLevel());
                    mStrain.setThcLevel(metricsStrain.getThcLevel());
                    mStrain.setIndicaPercentage(metricsStrain.getIndicaPercentage());
                    mStrain.setSativaPercentage(metricsStrain.getSativaPercentage());
                    mStrain.setTestingStatus(metricsStrain.getTestingStatus());
                    strains.add(mStrain);
                } else {
                    errors +=  "Compliance Job Id : " + (event.getComplianceSyncJob().getId()) + ": "+ defaultStrain.getData().getName() + "-" + errorsCount + ".- ".concat("Strain already exsits").concat(", ");
                    errorsCount++;
                }
            }
            try {
                if(strains.size() > 0) {
                    String jsonBody = JsonSerializer.toJson(strains);
                    apiService.createStrains(strains, metrcAuthorization.getFacilityLicense());
                    getCreateStrains(event.getCompanyId(), event.getShopId(), apiService, metrcAuthorization, oldItemsMap, items);
                    created++;
                }
            } catch (RestErrorException e) {
                LOG.error("Error creating starins:" + e.getErrorResponse(), e);
                if (e.getErrorResponse() != null) {
                    for (MetrcError metrcErrors : (MetricsErrorList) e.getErrorResponse()) {
                        errors +=  " " +  errorsCount + ".- ".concat(metrcErrors.getMessage()).concat(" ");
                        errorsCount++;
                    }
                }
            } catch (Exception e) {
                LOG.error("Error creating strains:" + e.getMessage(), e);
            }

            LOG.info("Created : " + created);
            LOG.info("Errors :" + errors);
            event.setMessage(errors);
            complianceStrainRepository.save(Lists.newArrayList(items));
            event.setResponse(event);
        }
    }



}
