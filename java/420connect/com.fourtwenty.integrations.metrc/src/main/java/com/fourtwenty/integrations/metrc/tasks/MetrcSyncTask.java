package com.fourtwenty.integrations.metrc.tasks;

import com.blaze.clients.metrcs.*;
import com.blaze.clients.metrcs.models.items.MetricsItemCategoryList;
import com.blaze.clients.metrcs.models.items.MetricsItemsList;
import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.blaze.clients.metrcs.models.sales.MetricsReceiptList;
import com.blaze.clients.metrcs.models.strains.MetricsStrainList;
import com.fourtwenty.core.domain.models.compliance.*;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.repositories.compliance.*;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import static com.fourtwenty.integrations.metrc.MetrcSyncSubscriber.BEGINNING_INVENTORY;

public class MetrcSyncTask extends Task {

    private static final Logger LOG = LoggerFactory.getLogger(MetrcSyncTask.class);

    private static final int FETCH_LIMIT = 1000;

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private MetrcService metrcService;
    @Inject
    private CompliancePackageRepository compliancePackageRepository;
    @Inject
    private ComplianceTransferRepository complianceTransferRepository;
    @Inject
    private ComplianceItemRepository complianceItemRepository;
    @Inject
    private ComplianceCategoryRepository complianceCategoryRepository;
    @Inject
    private ComplianceStrainRepository complianceStrainRepository;
    @Inject
    private ComplianceSaleReceiptRepository saleReceiptRepository;
    @Inject
    private ProductRepository productRepository;

    private String companyId;
    private String shopId;




    public MetrcSyncTask() {
        super("metrc-sync-task");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        final String companyId = getCompanyId() != null ? getCompanyId() : getParameter(parameters,"companyId");
        final String shopId = getShopId() != null ? getShopId() : getParameter(parameters,"shopId");
        if (companyId == null || shopId == null) {
            return;
        }

        String stateCode = metrcService.getShopStateCode(companyId, shopId);
        // Ignore if not enabled
        MetrcFacilityAccount account = metrcService.getMetrcAccount(companyId, stateCode)
                .getMetrcFacilityAccount(shopId);
        if (account == null || !account.isEnabled()) return;

        if (StringUtils.isBlank(account.getFacLicense())) {
            return;
        }

        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(companyId,shopId);

        syncPackages(metrcAuthorization,account);
        syncItems(metrcAuthorization,account);
        syncCategories(metrcAuthorization,account);
        syncStrains(metrcAuthorization,account);
        syncSaleReceipts(metrcAuthorization,account);

        mappItemsToProducts(account);
    }

    private void mappItemsToProducts(MetrcFacilityAccount account) {
        Iterable<ComplianceItem> allItems = complianceItemRepository.listAllByShop(account.getCompanyId(),account.getShopId());
        HashMap<String,ComplianceItem> complianceItemHashMap = new HashMap<>();
        for (ComplianceItem complianceItem : allItems) {
            if (complianceItem != null) {
                complianceItemHashMap.put(complianceItem.getData().getName().replace(BEGINNING_INVENTORY,"").trim().toLowerCase(), complianceItem);
            }
        }

        Iterable<Product> products = productRepository.listByShop(account.getCompanyId(),account.getShopId());
        int count = 0;
        List<Product> products1 = Lists.newArrayList(products);
        for (Product product : products1) {
            String name = product.getName().trim();
            String cleanName = StringUtils.normalizeSpace(name).toLowerCase();
            ComplianceItem complianceItem = complianceItemHashMap.get(cleanName);
            if (complianceItem != null && StringUtils.isBlank(product.getComplianceId())) {
                try {
                    LOG.info("Linking: " + cleanName);
                    productRepository.updateComplianceId(product.getCompanyId(), product.getId(), complianceItem.getData().getId() + "");
                    count++;
                } catch (Exception e) {
                    LOG.error("Error linking compliance item",e);
                }
            }
        }
        LOG.info("Finish linking compliance item: " + count);
    }

    private void syncSaleReceipts(MetrcAuthorization metrcAuthorization,MetrcFacilityAccount account) {
        MetricsSalesAPIService apiService = new MetricsSalesAPIService(metrcAuthorization);

        LinkedHashSet<ComplianceSaleReceipt> items = new LinkedHashSet<>();
        LOG.info("Syncing sales receipts..");
        try {
            int days = 7;
            DateTime startDate = new DateTime().minusDays(days);

            ComplianceSaleReceipt lastReceipt = saleReceiptRepository.getLastModified(account.getCompanyId(),account.getShopId());
            if (lastReceipt != null) {
                DateTime date = new DateTime(lastReceipt.getModified());
                if (date != null) {
                    startDate = date.minusDays(1);
                }
            }

            HashMap<String,ComplianceSaleReceipt> oldItemsMap = saleReceiptRepository.getItemsAsMapById(account.getCompanyId(),account.getShopId());
            items.addAll(getSales(oldItemsMap,apiService,account,startDate));

            saleReceiptRepository.save(Lists.newArrayList(items));
        } catch (Exception e) {
            System.out.println(e);
        }

        LOG.info("Syncing sales receipts..completed.");
    }

    private LinkedHashSet<ComplianceSaleReceipt> getSales(HashMap<String,ComplianceSaleReceipt> oldItemsMap,
                                                          MetricsSalesAPIService salesAPIService, MetrcFacilityAccount account,
                                                   DateTime aStartDate) {
        DateTime startDate = aStartDate;
        int interval = 1;
        LinkedHashSet<ComplianceSaleReceipt> items = new LinkedHashSet<>();
        DateTime now = new DateTime();

        LOG.info("Syncing sales receipts...");
        while(startDate.isBefore(now)) {
            String startDateStr = startDate.toString();
            LOG.info("Syncing sales receipts.." + startDateStr);
            String endDate = (startDate.plusHours(24).toString());
            try {
                MetricsReceiptList packages = salesAPIService.getSaleReceipts(account.getFacLicense(),
                        startDateStr);

                List<ComplianceSaleReceipt> compliancePackageList = new ArrayList<>();

                packages.forEach((pk) -> {
                    if (oldItemsMap.get(pk.getId()+"") == null){
                        ComplianceSaleReceipt compliancePackage = new ComplianceSaleReceipt();
                        compliancePackage.prepare(account.getCompanyId());
                        compliancePackage.setShopId(account.getShopId());
                        compliancePackage.setKey(pk.getId() + "");
                        compliancePackage.setSearchField(pk.getId() + " " + pk.getPatientLicenseNumber());
                        compliancePackage.setData(pk);
                        compliancePackage.setModified((new DateTime(pk.getSalesDateTime()).getMillis()));
                        compliancePackageList.add(compliancePackage);


                        oldItemsMap.put(pk.getId() + "",compliancePackage);

                    }
                });
                items.addAll(compliancePackageList);

            } catch (Exception e) {
            }
            startDate = startDate.plusDays(interval);
        }

        LOG.info("Syncing sales receipts...completed.");
        return items;
    }

    private void syncStrains(MetrcAuthorization metrcAuthorization,MetrcFacilityAccount account) {
        MetricsStrainsAPIService apiService = new MetricsStrainsAPIService(metrcAuthorization);

        LinkedHashSet<ComplianceStrain> items = new LinkedHashSet<>();
        LOG.info("Syncing strains..");
        try {

            HashMap<String,ComplianceStrain> oldItemsMap = complianceStrainRepository.getItemsAsMapById(account.getCompanyId(),account.getShopId());

            MetricsStrainList strainList = apiService.getActiveStrains(account.getFacLicense());
            strainList.forEach((strain) -> {

                if (oldItemsMap.get(strain.getName()) == null) {

                    ComplianceStrain complianceStrain = new ComplianceStrain();
                    complianceStrain.prepare(account.getCompanyId());
                    complianceStrain.setShopId(account.getShopId());
                    complianceStrain.setKey(strain.getName());
                    complianceStrain.setSearchField(strain.getId() + " " + strain.getName());
                    complianceStrain.setData(strain);
                    items.add(complianceStrain);


                    oldItemsMap.put(strain.getName(),complianceStrain);
                }
            });

            complianceStrainRepository.save(Lists.newArrayList(items));
        } catch (Exception e) {
            System.out.println(e);
        }
        LOG.info("Syncing strains...completed.");

    }


    private void syncCategories(MetrcAuthorization metrcAuthorization,MetrcFacilityAccount account) {
        MetricsItemsAPIService packagesAPIService = new MetricsItemsAPIService(metrcAuthorization);

        LinkedHashSet<ComplianceCategory> items = new LinkedHashSet<>();
        LOG.info("Syncing categories..");
        try {

            HashMap<String,ComplianceCategory> oldItemsMap = complianceCategoryRepository.getItemsAsMapById(account.getCompanyId(),account.getShopId());

            MetricsItemCategoryList categoryList = packagesAPIService.getItemCategories();
            categoryList.forEach((category) -> {
                if (oldItemsMap.get(category.getName()) == null) {
                    ComplianceCategory complianceCategory = new ComplianceCategory();
                    complianceCategory.prepare(account.getCompanyId());
                    complianceCategory.setShopId(account.getShopId());
                    complianceCategory.setKey(category.getName());
                    complianceCategory.setSearchField(category.getName());
                    complianceCategory.setData(category);
                    items.add(complianceCategory);

                    oldItemsMap.put(category.getName(),complianceCategory);
                }
            });

            complianceCategoryRepository.save(Lists.newArrayList(items));
        } catch (Exception e) {
            System.out.println(e);
        }

        LOG.info("Syncing categories...completed.");
    }


    private void syncItems(MetrcAuthorization metrcAuthorization,MetrcFacilityAccount account) {
        MetricsItemsAPIService packagesAPIService = new MetricsItemsAPIService(metrcAuthorization);

        LinkedHashSet<ComplianceItem> metricsPackages = new LinkedHashSet<>();
        LOG.info("Syncing items..");
        HashMap<String,ComplianceItem> oldItemsMap = complianceItemRepository.getItemsAsMapById(account.getCompanyId(),account.getShopId());
        try {
            int days = 60;
            DateTime startDate = new DateTime().minusDays(days);
            ComplianceItem lastItem = complianceItemRepository.getLastModified(account.getCompanyId(),account.getShopId());
            if (lastItem != null) {
                DateTime date = new DateTime(lastItem.getModified());
                if (date != null) {
                    startDate = date.minusDays(2);
                }
            }

            DateTime now = new DateTime();

            metricsPackages.addAll(getItems(oldItemsMap,packagesAPIService,account,startDate,now));

            complianceItemRepository.save(Lists.newArrayList(metricsPackages));
        } catch (Exception e) {
            System.out.println(e);
        }

        LOG.info("Syncing items...completed.");
    }

    private void syncPackages(MetrcAuthorization metrcAuthorization,MetrcFacilityAccount account) {
        MetricsPackagesAPIService packagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);

        LinkedHashSet<CompliancePackage> metricsPackages = new LinkedHashSet<>();
        LOG.info("Syncing packages..");

        HashMap<String,CompliancePackage> oldItemsMap = compliancePackageRepository.getItemsAsMapById(account.getCompanyId(),account.getShopId());

        try {
            int days = 60;
            DateTime startDate = new DateTime().minusDays(days);
            DateTime holdStartDate = new DateTime().minusDays(30);

            CompliancePackage lastItem = compliancePackageRepository.getLastModified(account.getCompanyId(),account.getShopId());
            if (lastItem != null) {
                DateTime date = new DateTime(lastItem.getModified());
                if (date != null) {
                    startDate = date.minusDays(2);
                    holdStartDate = date.minusDays(2);
                }
            }

            metricsPackages.addAll(getPackages(oldItemsMap,packagesAPIService,account,startDate, CompliancePackage.PackageStatus.ACTIVE));
            metricsPackages.addAll(getPackages(oldItemsMap,packagesAPIService,account,holdStartDate, CompliancePackage.PackageStatus.ONHOLD));
            metricsPackages.addAll(getPackages(oldItemsMap,packagesAPIService,account,holdStartDate, CompliancePackage.PackageStatus.INACTIVE));

            compliancePackageRepository.save(Lists.newArrayList(metricsPackages));
        } catch (Exception e) {
            System.out.println(e);
        }
        LOG.info("Syncing packages...completed.");
    }

    private LinkedHashSet<CompliancePackage> getPackages(HashMap<String,CompliancePackage> oldItemsMap,
                                                         MetricsPackagesAPIService packagesAPIService, MetrcFacilityAccount account,
                             DateTime aStartDate, final CompliancePackage.PackageStatus aStatus) {
        DateTime startDate = aStartDate;
        int interval = 1;
        LinkedHashSet<CompliancePackage> metricsPackages = new LinkedHashSet<>();
        DateTime now = new DateTime();
        while(startDate.isBefore(now)) {
            String startDateStr = startDate.toString();
            String endDate = (startDate.plusHours(24).toString());
            LOG.info("Syncing packages.." + startDateStr);
            try {
                MetricsPackagesList packages = packagesAPIService.getPackagesByStatus(account.getFacLicense(),
                        aStatus.name().toLowerCase(),
                        startDateStr,
                        endDate);

                List<CompliancePackage> compliancePackageList = new ArrayList<>();

                packages.forEach((pk) -> {
                    if (oldItemsMap.get(pk.getLabel()) == null) {
                        CompliancePackage compliancePackage = new CompliancePackage();
                        compliancePackage.prepare(account.getCompanyId());
                        compliancePackage.setShopId(account.getShopId());
                        compliancePackage.setKey(pk.getLabel());
                        compliancePackage.setData(pk);
                        compliancePackage.setSearchField(pk.getLabel() + pk.getProductName());
                        compliancePackage.setStatus(aStatus);
                        compliancePackage.setModified((new DateTime(pk.getLastModified()).getMillis()));
                        compliancePackageList.add(compliancePackage);

                        oldItemsMap.put(pk.getLabel(),compliancePackage);
                    }
                });
                metricsPackages.addAll(compliancePackageList);

            } catch (Exception e) {
            }
            startDate = startDate.plusDays(interval);
        }

        return metricsPackages;
    }

    private LinkedHashSet<ComplianceItem> getItems(HashMap<String,ComplianceItem> itemHashMap,
                                                   MetricsItemsAPIService itemsAPIService, MetrcFacilityAccount account,
                                                         DateTime aStartDate, DateTime aEndDate) {
        DateTime startDate = aStartDate;
        int interval = 1;
        LinkedHashSet<ComplianceItem> metricsPackages = new LinkedHashSet<>();
        while(startDate.isBefore(aEndDate)) {
            String startDateStr = startDate.toString();
            String endDate = (startDate.plusHours(24).toString());
            LOG.info("Syncing items.." + startDateStr);
            try {
                MetricsItemsList packages = itemsAPIService.getActiveItems(account.getFacLicense(),
                        startDateStr,
                        endDate);

                List<ComplianceItem> compliancePackageList = new ArrayList<>();

                packages.forEach((pk) -> {
                    if (itemHashMap.get(pk.getId()+"") == null) {
                        ComplianceItem compliancePackage = new ComplianceItem();
                        compliancePackage.prepare(account.getCompanyId());
                        compliancePackage.setShopId(account.getShopId());
                        compliancePackage.setKey(pk.getId() + "");
                        compliancePackage.setSearchField(pk.getId() + pk.getName() + pk.getStrainName());
                        compliancePackage.setData(pk);
                        compliancePackageList.add(compliancePackage);

                        itemHashMap.put(compliancePackage.getKey(),compliancePackage);
                    }
                });
                metricsPackages.addAll(compliancePackageList);

            } catch (Exception e) {
            }
            startDate = startDate.plusDays(interval);
        }

        return metricsPackages;
    }


    private String getParameter(ImmutableMultimap<String, String> parameters, String key) {
        ImmutableCollection<String> values = parameters.get("key");
        return getFirstFromArray(values);
    }

    private String getFirstFromArray(ImmutableCollection<String> array) {
        if (array != null && array.size() > 0) {
            return (String) array.toArray()[0];
        }
        return null;
    }


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
