package com.fourtwenty.integrations.metrc.tasks;

import com.amazonaws.util.StringUtils;
import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsStrainsAPIService;
import com.blaze.clients.metrcs.models.strains.MetricsStrain;
import com.blaze.clients.metrcs.models.strains.MetricsStrainList;
import com.fourtwenty.core.domain.models.compliance.ComplianceBatchPackagePair;
import com.fourtwenty.core.domain.models.compliance.ComplianceStrain;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.repositories.compliance.*;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.event.compliance.CreateItemComplianceEvent;
import com.fourtwenty.core.event.compliance.SyncUpComplianceEvent;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.*;

public class MetrcSyncUpTask extends Task {
    private static final Logger LOG = LoggerFactory.getLogger(MetrcSyncTask.class);

    private static final int FETCH_LIMIT = 1000;

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private MetrcService metrcService;
    @Inject
    private CompliancePackageRepository compliancePackageRepository;
    @Inject
    private ComplianceTransferRepository complianceTransferRepository;
    @Inject
    private ComplianceItemRepository complianceItemRepository;
    @Inject
    private ComplianceCategoryRepository complianceCategoryRepository;
    @Inject
    private ComplianceStrainRepository complianceStrainRepository;
    @Inject
    private ComplianceSaleReceiptRepository saleReceiptRepository;

    private String companyId;
    private String shopId;

    private SyncUpComplianceEvent complianceEvent;
    private CreateItemComplianceEvent createItemComplianceEvent;


    public MetrcSyncUpTask() {
        super("metrc-syncup-task");
    }




    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        final String companyId = getCompanyId() != null ? getCompanyId() : getParameter(parameters,"companyId");
        final String shopId = getShopId() != null ? getShopId() : getParameter(parameters,"shopId");
        if (companyId == null || shopId == null) {
            return;
        }

        String stateCode = metrcService.getShopStateCode(companyId, shopId);
        // Ignore if not enabled
        MetrcFacilityAccount account = metrcService.getMetrcAccount(companyId, stateCode)
                .getMetrcFacilityAccount(shopId);
        if (account == null || !account.isEnabled()) return;

        if (StringUtils.isNullOrEmpty(account.getFacLicense())) {
            return;
        }

        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(companyId,shopId);


        if (complianceEvent != null) {
            // Sync up strain names
            syncStrains(complianceEvent.getPackageTags(),metrcAuthorization);

            syncProducts(complianceEvent.getPackageTags(),metrcAuthorization);
        }
    }


    private void syncStrains(List<ComplianceBatchPackagePair> strainList, MetrcAuthorization metrcAuthorization) {
        HashMap<String,ComplianceStrain> oldItemsMap = complianceStrainRepository.getItemsAsMapById(companyId,shopId);
        LinkedHashSet<ComplianceStrain> items = new LinkedHashSet<>();
        MetricsStrainsAPIService apiService = new MetricsStrainsAPIService(metrcAuthorization);


        Set<String> strainsSet = new HashSet<>();

        for (ComplianceBatchPackagePair batchPackagePair : strainList) {
            if (!StringUtils.isNullOrEmpty(batchPackagePair.getStrainName())) {
                strainsSet.add(batchPackagePair.getStrainName().toLowerCase().trim());
            }
        }

        int created = 0;
        int errors = 0;
        for (String strainName : strainsSet) {

            ComplianceStrain defaultStrain = oldItemsMap.get(strainName.toLowerCase());

            if (defaultStrain == null) {
                MetricsStrain metricsStrain = new MetricsStrain();
                metricsStrain.setName(strainName);
                metricsStrain.setCbdLevel(0.5);
                metricsStrain.setThcLevel(0.5);
                metricsStrain.setIndicaPercentage(50);
                metricsStrain.setSativaPercentage(50);
                metricsStrain.setTestingStatus("None");

                List<MetricsStrain> strains = new ArrayList<>();
                strains.add(metricsStrain);
                try {
                    LOG.info("Creating strain: " + strainName);
                    String jsonBody = JsonSerializer.toJson(strains);
                    apiService.createStrains(strains, metrcAuthorization.getFacilityLicense());
                    created++;
                } catch (Exception e) {
                    LOG.error("Cannot create strain", e);
                    errors++;
                }

            }

        }

        getCreateStrains(companyId,shopId,apiService,metrcAuthorization,oldItemsMap,items);

        complianceStrainRepository.save(Lists.newArrayList(items));

        LOG.info("Created: " + created);
        LOG.info("Errors: " + errors);

    }


    private void syncProducts(List<ComplianceBatchPackagePair> strainList, MetrcAuthorization metrcAuthorization) {
        // sync all products up
        // 1. Fetch all categories
        // 2. Only sync categories with correctly assigned metrc categories
        // 3. Only sync products with no compliance id
        HashMap<String,ComplianceStrain> oldItemsMap = complianceStrainRepository.getItemsAsMapById(companyId,shopId);

    }

    private void syncProductBatches(MetrcAuthorization metrcAuthorization) {
        // sync all product batches up
        // need to sync entire batch inventories
    }

    private void getCreateStrains(
            final String companyId,
            final String shopId,
            final MetricsStrainsAPIService apiService,
            MetrcAuthorization metrcAuthorization,
            HashMap<String,ComplianceStrain> oldItemsMap,
            LinkedHashSet<ComplianceStrain> items) {

        MetricsStrainList strainList = apiService.getActiveStrains(metrcAuthorization.getFacilityLicense());
        strainList.forEach((strain) -> {

            if (oldItemsMap.get(strain.getName()) == null) {

                ComplianceStrain complianceStrain = new ComplianceStrain();
                complianceStrain.prepare(companyId);
                complianceStrain.setShopId(shopId);
                complianceStrain.setKey(strain.getName());
                complianceStrain.setSearchField(strain.getId() + " " + strain.getName());
                complianceStrain.setData(strain);
                items.add(complianceStrain);


                oldItemsMap.put(strain.getName(),complianceStrain);
            }
        });
    }




    /// GETTERS/SETTERS
    private String getParameter(ImmutableMultimap<String, String> parameters, String key) {
        ImmutableCollection<String> values = parameters.get("key");
        return getFirstFromArray(values);
    }

    private String getFirstFromArray(ImmutableCollection<String> array) {
        if (array != null && array.size() > 0) {
            return (String) array.toArray()[0];
        }
        return null;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public SyncUpComplianceEvent getComplianceEvent() {
        return complianceEvent;
    }

    public void setComplianceEvent(SyncUpComplianceEvent complianceEvent) {
        this.complianceEvent = complianceEvent;
    }

    public CreateItemComplianceEvent getCreateItemComplianceEvent() {
        return createItemComplianceEvent;
    }

    public void setCreateItemComplianceEvent(CreateItemComplianceEvent createItemComplianceEvent) {
        this.createItemComplianceEvent = createItemComplianceEvent;
    }
}
