package com.fourtwenty.integrations.mtrac;

import com.fourtwenty.integrations.mtrac.service.MtracService;
import com.fourtwenty.integrations.mtrac.service.impl.MtracServiceImpl;
import com.google.inject.AbstractModule;

public class MtracModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MtracService.class).to(MtracServiceImpl.class);

    }
}
