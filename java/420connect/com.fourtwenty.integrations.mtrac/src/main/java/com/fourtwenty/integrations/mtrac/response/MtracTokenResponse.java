package com.fourtwenty.integrations.mtrac.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MtracTokenResponse {

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("payment_gateway")
    private String paymentGateway;

    @JsonProperty("second_payment_gateway")
    private String secondPaymentGateway;

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecondPaymentGateway() {
        return secondPaymentGateway;
    }

    public void setSecondPaymentGateway(String secondPaymentGateway) {
        this.secondPaymentGateway = secondPaymentGateway;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
}
