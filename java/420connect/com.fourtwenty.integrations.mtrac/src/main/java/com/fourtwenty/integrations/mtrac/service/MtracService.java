package com.fourtwenty.integrations.mtrac.service;

import com.fourtwenty.core.config.MtracConfiguration;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.paymentcard.mtrac.MtracTransactionRequest;
import com.fourtwenty.integrations.mtrac.request.MtracOAuthRequest;
import com.fourtwenty.integrations.mtrac.request.RefundMoneyRequest;
import com.fourtwenty.integrations.mtrac.response.RefundMoneyResponse;
import com.fourtwenty.integrations.mtrac.response.MtracTransactionResponse;

public interface MtracService {
    MtracTransactionResponse processTransactionPayment(MtracOAuthRequest oAuthRequest, MtracTransactionRequest request, MtracConfiguration mtracConfiguration);

    RefundMoneyResponse refundTransaction(MtracOAuthRequest oAuthRequest, RefundMoneyRequest request, MtracConfiguration mtracConfiguration);

    // Mtrac proxy API
    void processPendingMtracPayments(Transaction transaction);
}
