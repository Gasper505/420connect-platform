package com.fourtwenty.integrations.mtrac.service.impl;

import com.fourtwenty.core.config.MtracConfiguration;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPaymentStatus;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.event.mtrac.MtracVirtualEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.paymentcard.mtrac.MtracTransactionRequest;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.integrations.mtrac.request.MtracOAuthRequest;
import com.fourtwenty.integrations.mtrac.request.RefundMoneyRequest;
import com.fourtwenty.integrations.mtrac.response.MtracTokenResponse;
import com.fourtwenty.integrations.mtrac.response.MtracTransactionResponse;
import com.fourtwenty.integrations.mtrac.response.RefundMoneyResponse;
import com.fourtwenty.integrations.mtrac.service.MtracService;
import com.google.inject.Inject;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MtracServiceImpl implements MtracService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MtracServiceImpl.class);
    private static final String ENTITY = "MtracSubscriber";

    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    @Inject
    private ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    private MtracService mtracService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private TransactionRepository transactionRepository;

    private MtracTokenResponse authenticate(MtracOAuthRequest request, MtracConfiguration mtracConfiguration) {
        String url = mtracConfiguration.getHost() + "/oauth/token/retrieve";
        return doPostToMtrac(url, request, MtracTokenResponse.class);
    }

    @Override
    public MtracTransactionResponse processTransactionPayment(MtracOAuthRequest oAuthRequest, MtracTransactionRequest request, MtracConfiguration mtracConfiguration) {
        String url = mtracConfiguration.getHost() + "/api/registrations/virtual_transaction";
        // Authenticate
        MtracTokenResponse tokenResponse = authenticate(oAuthRequest, mtracConfiguration);
        request.setAuthToken(tokenResponse.getAccessToken());

        return doPostToMtrac(url, request, MtracTransactionResponse.class);
    }

    @Override
    public RefundMoneyResponse refundTransaction(MtracOAuthRequest oAuthRequest, RefundMoneyRequest request, MtracConfiguration mtracConfiguration) {
        String url = mtracConfiguration.getHost() + "/api/wallets/refund_money";
        // Authenticate
        MtracTokenResponse tokenResponse = authenticate(oAuthRequest, mtracConfiguration);
        request.setAuthToken(tokenResponse.getAccessToken());

        return doPostToMtrac(url, request, RefundMoneyResponse.class);
    }

    private <T> T doPostToMtrac(String url, Object request, Class<T> returnClass) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        headers.putSingle("accept", "*/*");
        LOGGER.info("Calling Mtrac API URL : {}", url);

        try {
            final String json = JsonSerializer.toJson(request);
            return SimpleRestUtil.post(url, json, returnClass, headers);
        } catch (Exception ex) {
            LOGGER.info("Exception while get Refund Money:", ex);
            throw new BlazeOperationException(String.format("Exception while get Refund Money:.\n%s", ex.getMessage()), ex);

        }
    }


    /**
     * @param transaction
     * @implNote This method is used to make payment using mtrac request api and update transaction payment details
     */

    @Override
    public void processPendingMtracPayments(Transaction transaction) {
        MtracConfiguration mtracConfiguration = integrationSettingRepository.getMtracConfig(IntegrationSetting.Environment.Production);
        if (mtracConfiguration == null) {
            LOGGER.info("Mtrac configuration does not exist.");
            return;
        }
        if (transaction.getCart().getPaymentOption() != Cart.PaymentOption.Mtrac) return;

        if (StringUtils.isNotBlank(transaction.getMtracTxnId())) {
            LOGGER.info("Transaction is already paid");
            return;
        }
        ThirdPartyAccount thirdPartyAccount = thirdPartyAccountRepository.findByAccountTypeByShopId(transaction.getCompanyId(),
                transaction.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Mtrac);

        if (Objects.isNull(thirdPartyAccount)) {
            throw new BlazeInvalidArgException(ENTITY, "Mtrac account is not configured");
        }


        List<PaymentCardPayment> pendingPayments = transaction.getPaymentCardPayments()
                .stream()
                .filter(p -> p.getStatus() == PaymentCardPaymentStatus.Pending)
                .collect(Collectors.toList());

        // If theres pending payments, try to process them
        if (!pendingPayments.isEmpty()) {

            // Ensure transaction prereqs
            if (transaction.getShop() == null)
                transaction.setShop(shopRepository.get(transaction.getCompanyId(), transaction.getShopId()));
            if (transaction.getSeller() == null)
                transaction.setSeller(employeeRepository.get(transaction.getCompanyId(), transaction.getSellerId()));
            if (transaction.getMember() == null)
                transaction.setMember(memberRepository.get(transaction.getCompanyId(), transaction.getMemberId()));

            LOGGER.info(String.format("MemberId: %s, Member: %s", transaction.getMemberId(), transaction.getMember()));

            for (PaymentCardPayment payment : pendingPayments) {

                payment.setType(PaymentCardType.Mtrac);
                payment.prepare();
                payment.setProcessedTime(org.joda.time.DateTime.now().getMillis());

                // Perform Payment with Mtrac
                MtracOAuthRequest mtracOAuthRequest = new MtracOAuthRequest();

                mtracOAuthRequest.setClientId(thirdPartyAccount.getClientId());
                mtracOAuthRequest.setClientSecret(thirdPartyAccount.getClientSecret());

                MtracTransactionRequest request = payment.mtracTransactionRequest();
                request.setLocationId(thirdPartyAccount.getLocationId());
                MtracVirtualEvent event = new MtracVirtualEvent();

                try {
                    MtracTransactionResponse response = mtracService.processTransactionPayment(mtracOAuthRequest, request, mtracConfiguration);
                    if (Objects.nonNull(response) && Objects.nonNull(response.getTransactionId())) {
                        payment.setPaymentId(response.getTransactionId());
                        payment.setStatus(PaymentCardPaymentStatus.Paid);
                        transactionRepository.saveMtracTxn(transaction.getCompanyId(), transaction.getShopId(), transaction.getId(), transaction.getId());
                        event.setResponse(response);
                    } else {
                        transaction.setStatus(Transaction.TransactionStatus.Hold);
                        transaction.setActive(true);
                        if (!response.isSuccess()) {
                            throw new BlazeOperationException("Mtrac Payment was declined.");
                        }
                        event.setResponse(response);
                    }
                } catch (Exception e) {
                    throw new BlazeOperationException(e);
                }
            }
        } else {
            transaction.setStatus(Transaction.TransactionStatus.Hold);
            transaction.setActive(true);
            // There was no payments, even though this was a mtrac transaction. Throw error
            throw new BlazeOperationException("You must include a mtrac payment when paying with the mtrac payment type.");
        }

        // Ensure transaction is paid for
        double paymentCardTotalPaid = transaction.getPaymentCardPayments()
                .stream()
                .filter(p -> p.getStatus() == PaymentCardPaymentStatus.Paid)
                .mapToDouble(p -> p.getAmount().doubleValue())
                .sum();

        if (paymentCardTotalPaid < transaction.getCart().getTotal().doubleValue()) {
            transaction.setStatus(Transaction.TransactionStatus.Hold);
            transaction.setActive(true);
            transactionRepository.update(transaction.getId(), transaction);
            throw new BlazeOperationException("Transaction not paid.");
        }
        // Update DB state
        transactionRepository.update(transaction.getId(), transaction);
    }

}
