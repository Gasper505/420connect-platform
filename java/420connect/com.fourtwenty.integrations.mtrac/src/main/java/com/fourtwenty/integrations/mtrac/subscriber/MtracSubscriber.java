package com.fourtwenty.integrations.mtrac.subscriber;

import com.fourtwenty.core.config.MtracConfiguration;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPayment;
import com.fourtwenty.core.domain.models.transaction.PaymentCardPaymentStatus;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.mtrac.MtracRefundTransactionEvent;
import com.fourtwenty.core.event.mtrac.MtracVirtualEvent;
import com.fourtwenty.core.event.transaction.MtracPaymentCardsForTransactionEvent;
import com.fourtwenty.core.event.transaction.ProcessLoyaltyCardsForTransactionResult;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.paymentcard.mtrac.MtracTransactionRequest;
import com.fourtwenty.integrations.mtrac.request.MtracOAuthRequest;
import com.fourtwenty.integrations.mtrac.request.RefundMoneyRequest;
import com.fourtwenty.integrations.mtrac.response.MtracTransactionResponse;
import com.fourtwenty.integrations.mtrac.response.RefundMoneyResponse;
import com.fourtwenty.integrations.mtrac.service.MtracService;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Objects;

public class MtracSubscriber implements BlazeSubscriber {
    private static final Logger LOGGER = LoggerFactory.getLogger(MtracSubscriber.class);
    private static final String ENTITY = "MtracSubscriber";
    private static final String MTRAC = "MTRAC";
    private static final String CONFIGURATION_NOT_FOUND = "Mtrac is not configured correctly.";

    private static final Log LOG = LogFactory.getLog(MtracSubscriber.class);
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    MtracService mtracService;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;

    @Subscribe
    public void virtualTransaction(MtracVirtualEvent event) {

        if (event == null) {
            new MtracVirtualEvent().throwException(new BlazeInvalidArgException(ENTITY, "Request is empty "));
            return;
        }
        MtracConfiguration mtracConfiguration = integrationSettingRepository.getMtracConfig(IntegrationSetting.Environment.Production);
        if (mtracConfiguration == null) {
            event.throwException(new BlazeInvalidArgException(MTRAC, CONFIGURATION_NOT_FOUND));
        }

        ThirdPartyAccount thirdPartyAccount = thirdPartyAccountRepository.findByAccountTypeByShopId(event.getCompanyId(),
                event.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Mtrac);

        if (thirdPartyAccount == null) {
            event.throwException(new BlazeInvalidArgException(ENTITY, "Mtrac account is not configured"));
            return;
        }
        MtracOAuthRequest mtracOAuthRequest = new MtracOAuthRequest();

        mtracOAuthRequest.setClientId(thirdPartyAccount.getClientId());
        mtracOAuthRequest.setClientSecret(thirdPartyAccount.getClientSecret());

        MtracTransactionRequest request = event.getMtracTransactionRequest();
        request.setLocationId(thirdPartyAccount.getLocationId());

        try {
            MtracTransactionResponse response = mtracService.processTransactionPayment(mtracOAuthRequest, request, mtracConfiguration);
            if (Objects.nonNull(response) && Objects.nonNull(response.getTransactionId())) {
                transactionRepository.saveMtracTxn(event.getCompanyId(), event.getShopId(), request.getTransactionId(), response.getTransactionId());
                event.setResponse(response);
            }else{
                event.setResponse(response);
            }
        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }


    }

    @Subscribe
    public void refundMoneyTransaction(MtracRefundTransactionEvent event) {

        if (event == null) {
            new MtracRefundTransactionEvent().throwException(new BlazeInvalidArgException(ENTITY, "Request is empty "));
            return;
        }
        MtracConfiguration mtracConfiguration = integrationSettingRepository.getMtracConfig(IntegrationSetting.Environment.Production);
        if (mtracConfiguration == null) {
            event.throwException(new BlazeInvalidArgException(MTRAC, CONFIGURATION_NOT_FOUND));
        }

        ThirdPartyAccount thirdPartyAccount = thirdPartyAccountRepository.findByAccountTypeByShopId(event.getCompanyId(),
                event.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Mtrac);

        if (thirdPartyAccount == null) {
            event.throwException(new BlazeInvalidArgException(ENTITY, "Mtrac account is not configured"));
            return;
        }

        Transaction transaction = event.getTransaction();
        if (transaction == null || transaction.getCart().getPaymentOption() != Cart.PaymentOption.Mtrac || !StringUtils.isNotBlank(transaction.getMtracTxnId())) {
            event.throwException(new BlazeInvalidArgException(ENTITY, "Transaction is either null or payment is not mtrac"));
            return;
        }

        MtracOAuthRequest mtracOAuthRequest = new MtracOAuthRequest();
        mtracOAuthRequest.setClientId(thirdPartyAccount.getClientId());
        mtracOAuthRequest.setClientSecret(thirdPartyAccount.getClientSecret());

        RefundMoneyRequest request = new RefundMoneyRequest();
        request.setPartialAmount(event.getPartialAmount());
        request.setTransactId(transaction.getMtracTxnId());
        try {
            RefundMoneyResponse refundMoneyResponse = mtracService.refundTransaction(mtracOAuthRequest, request, mtracConfiguration);
            if (Objects.nonNull(refundMoneyResponse)) {
                event.setResponse(refundMoneyResponse);
            }
        } catch (Exception e) {
            event.throwException(new BlazeOperationException(e));
        }
    }

    private void verifyAccounts() {

    }

    /**
     * @param paymentevent
     * @implNote This even is used for transaction payment using mtrac payment type.
     */

    @Subscribe
    public void subscribe(MtracPaymentCardsForTransactionEvent paymentevent) {
        try {
            Transaction dbTransaction = paymentevent.getDbTransaction();
            Transaction requestTransaction = paymentevent.getRequestTransaction();

            LOG.info("dbTransaction.mtracPayments: " + dbTransaction.getPaymentCardPayments());
            LOG.info("requestTransaction.mtracPayments: " + requestTransaction.getPaymentCardPayments());

            if (requestTransaction.getCart().getPaymentOption() == Cart.PaymentOption.Mtrac) {

                for (PaymentCardPayment cardPayment : requestTransaction.getPaymentCardPayments()) {
                    if (cardPayment.getStatus() == null) {
                        cardPayment.setStatus(PaymentCardPaymentStatus.Pending);
                    }
                }

                dbTransaction.addPaymentCardPayments(requestTransaction.getPaymentCardPayments());
                dbTransaction.getCart().setPaymentOption(requestTransaction.getCart().getPaymentOption());
                dbTransaction.getCart().setTotal(requestTransaction.getCart().getTotal());

                mtracService.processPendingMtracPayments(dbTransaction);
                paymentevent.setResponse(new ProcessLoyaltyCardsForTransactionResult(dbTransaction));
            }

        } catch (Exception e) {
            paymentevent.throwException(new BlazeOperationException(e));
        }
    }
}
