package com.fourtwenty.integrations.weedmap;

import com.fourtwenty.integrations.weedmap.services.WeedMapSyncService;
import com.fourtwenty.integrations.weedmap.services.impl.WeedMapSyncServiceImpl;
import com.google.inject.AbstractModule;

public class WeedMapModule extends AbstractModule {

    @Override
    protected void configure() {
        // Services
        bind(WeedMapSyncService.class).to(WeedMapSyncServiceImpl.class);
    }
}