package com.fourtwenty.integrations.weedmap;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.weedmap.WeedMapDeleteEvent;
import com.fourtwenty.core.event.weedmap.WeedMapSyncEvent;
import com.fourtwenty.core.event.weedmap.WeedmapAuthEvent;
import com.fourtwenty.core.event.weedmap.WeedmapFetchEvent;
import com.fourtwenty.core.event.weedmap.WeedmapSyncResponse;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.integrations.weedmap.services.WeedMapSyncService;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;

public class WeedMapSubscriber implements BlazeSubscriber {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeedMapSubscriber.class);

    private static final String WEEDMAP = "WeedMap";
    @Inject
    private WeedMapSyncService weedMapSyncService;

    @Subscribe
    public void syncWeedmap(WeedMapSyncEvent event) {
        try {
            if (StringUtils.isBlank(event.getCompanyId())) {
                throw new BlazeOperationException("Company cannot be blank.");
            }
            if (StringUtils.isBlank(event.getShopId())) {
                throw new BlazeOperationException("Shop cannot be blank.");
            }
            if (event.getProductIds() == null || event.getProductIds().isEmpty()) {
                throw new BlazeOperationException("Inventory cannot be blank.");
            }

            event.setResponse(weedMapSyncService.syncWeedMap(event.getCompanyId(), event.getShopId(), event.getProductIds()));
        } catch (Exception e) {
            LOGGER.info(e.getMessage(), e);
            WeedmapSyncResponse response = new WeedmapSyncResponse();
            response.setErrorMsg(e.getMessage());
            response.setSyncStatus(false);
            event.setResponse(response);
        }
    }

    @Subscribe
    public void deleteWeedmapAccount(WeedMapDeleteEvent event) {
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        try {
            if (StringUtils.isBlank(event.getCompanyId())) {
                throw new BlazeOperationException("Company cannot be blank.");
            }
            if (StringUtils.isBlank(event.getShopId())) {
                throw new BlazeOperationException("Shop cannot be blank.");
            }
            if (event.getCategoryMappings() == null || event.getCategoryMappings().isEmpty()) {
                throw new BlazeOperationException("Category mapping cannot ba blank");
            }

            if (event.getApiKeyMaps() == null || event.getApiKeyMaps().isEmpty()) {
                throw new BlazeOperationException("Weedmap api key list is blank");
            }
            Set<String> productIds = new HashSet<String>();
            for (Product product : event.getProducts()) {
                productIds.add(product.getId());
            }
            if (productIds.size() > 0) {
                response = weedMapSyncService.syncWeedMap(event.getCompanyId(), event.getShopId(), productIds);
            } else {
                throw new BlazeOperationException("Products list is blank");
            }

        } catch (Exception e) {
            LOGGER.info(e.getMessage(), e);
            response.setErrorMsg(e.getMessage());
            response.setSyncStatus(false);
        }
        event.setResponse(response);
    }

    @Subscribe
    public void authenticate(WeedmapAuthEvent event) {
        try {
            if (StringUtils.isBlank(event.getCompanyId())) {
                LOGGER.info("Company cannot be blank.");
                return;
            }
            if (StringUtils.isBlank(event.getShopId())) {
                LOGGER.info("Shop cannot be blank.");
                return;
            }
            if (event.getAccount() == null) {
                LOGGER.info("Weedmap account is empty.");
                return;
            }

            WeedmapAccount weedmapAccount = weedMapSyncService.authenticate(event.getCompanyId(),event.getShopId(),event.getAccount(),event.getType());
            event.setResponse(weedmapAccount);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            event.setResponse(event.getAccount());
        }

    }

    @Subscribe
    public void fetchMenuItems(WeedmapFetchEvent event) {
        try {
            if (StringUtils.isBlank(event.getCompanyId())) {
                LOGGER.info("Company cannot be blank.");
                return;
            }
            if (StringUtils.isBlank(event.getShopId())) {
                LOGGER.info("Shop cannot be blank.");
                return;
            }
            if (event.getAccount() == null) {
                LOGGER.info("Weedmap account is empty.");
                return;
            }

            weedMapSyncService.fetchWeedmapMenuItems(event.getCompanyId(),event.getShopId(),event.getAccount(),event.getType());
        } catch (Exception e) {
            LOGGER.info(e.getMessage(), e);
        }

    }
}
