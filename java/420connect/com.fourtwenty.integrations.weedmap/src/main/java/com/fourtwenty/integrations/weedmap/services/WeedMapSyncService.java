package com.fourtwenty.integrations.weedmap.services;

import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.event.weedmap.WeedmapSyncResponse;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;

import java.util.Set;

public interface WeedMapSyncService {
    WeedmapSyncResponse syncWeedMap(String companyId, String shopId, Set<String> productIds);


    WeedmapAccount authenticate(String companyId, String shopId, WeedmapAccount weedmapAccount, WeedmapTokenType tokenType);

    void fetchWeedmapMenuItems(String companyId, String shopId, WeedmapAccount account, WeedmapTokenType tokenType);
}
