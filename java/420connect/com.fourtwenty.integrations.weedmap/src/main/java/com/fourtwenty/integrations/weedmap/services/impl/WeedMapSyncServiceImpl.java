package com.fourtwenty.integrations.weedmap.services.impl;

import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapAccountRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapSyncItemRepository;
import com.fourtwenty.core.event.weedmap.WeedmapSyncResponse;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;
import com.fourtwenty.integrations.weedmap.services.WeedMapSyncService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class WeedMapSyncServiceImpl implements WeedMapSyncService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeedMapSyncServiceImpl.class);

    @Inject
    private WeedmapAccountRepository weedmapAccountRepository;
    @Inject
    private BackgroundTaskManager taskManager;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    @Inject
    private WeedmapSyncItemRepository weedmapSyncItemRepository;

    @Override
    public WeedmapSyncResponse syncWeedMap(String companyId, String shopId, Set<String> productIds) {
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        WeedmapAccount account = weedmapAccountRepository.getWeedmapAccount(companyId, shopId);
        if (account == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }

        Set<ObjectId> productIdsSet = new HashSet<>();

        for (String key : productIds) {
            productIdsSet.add(new ObjectId(key));
        }
        Iterable<Product> products = productRepository.list(companyId, Lists.newArrayList(productIdsSet));


        /* Prepare Quantities to sync at weed map according to regions associate inventories*/


        if (CollectionUtils.isNotEmpty(account.getApiKeyList())) {
            response = taskManager.syncWeedmap(companyId, shopId, account.getApiKeyList(), Lists.newArrayList(products));
        } else {
            response.setSyncStatus(false);
            response.setErrorMsg("Api key list is empty");
        }
        return response;
    }

    /**
     * Private method to calculate product and prepackage quantity according to inventory
     *
     * @param product                   : product
     * @param inventory                 : inventory
     * @param prepackageQuantityHashMap : prepackage product quantity hashMap
     * @return : returns product+prepackage quantity for given inventory.
     */
    private double getQuantityByInventory(Product product, Inventory inventory, HashMap<String, ProductPrepackageQuantity> prepackageQuantityHashMap) {

        double quantity = 0;

        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantityHashMap.values()) {
            if (prepackageQuantity.getProductId().equalsIgnoreCase(product.getId()) && prepackageQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                quantity += prepackageQuantity.getQuantity();
            }
        }

        if (product.getQuantities() != null && !product.getQuantities().isEmpty()) {
            for (ProductQuantity productQuantity : product.getQuantities()) {
                if (productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                    quantity += productQuantity.getQuantity().doubleValue();
                }
            }
        }

        return quantity;
    }



    @Override
    public WeedmapAccount authenticate(String companyId, String shopId, WeedmapAccount weedmapAccount, WeedmapTokenType tokenType) {

        WeedmapConfig prodEnvironment = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);

        WeedmapAccount account = taskManager.authenticateWeedmapToken(companyId, shopId, weedmapAccount, tokenType, true, prodEnvironment);

        return account;
    }

    @Override
    public void fetchWeedmapMenuItems(String companyId, String shopId, WeedmapAccount account, WeedmapTokenType tokenType) {
        if (tokenType.getType() == WeedmapTokenType.TokenType.FETCHTOKEN) {
            WeedmapConfig prodEnvironment = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);

            List<WmSyncItems> syncItemsList = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(account.getApiKeyList())) {
                List<String> wmListId = new ArrayList<>();
                for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
                    wmListId.add(apiKeyMap.getListingWmId());
                }
                HashMap<String, WmSyncItems> organizationByListIdMap = weedmapSyncItemRepository.getWmItemsByListingIdAsMap(companyId, shopId, wmListId, WmSyncItems.WMItemType.ORGANIZATIONS);

                for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
                    if (apiKeyMap.getStatus() == WeedmapAccount.AuthStatus.Authorized) {
                        WmSyncItems organizationItem = organizationByListIdMap.get(apiKeyMap.getListingWmId());
                        HashMap<String, WmSyncItems> itemsHashMap = new HashMap<>();
                        itemsHashMap.put(shopId, organizationItem);
                        taskManager.createWmOrganization(companyId, shopId, apiKeyMap, syncItemsList, itemsHashMap, prodEnvironment, true);
                    }
                }

                if (!syncItemsList.isEmpty()) {
                    List<String> itemIds = new ArrayList<>();
                    syncItemsList.forEach(syncItems -> {
                        itemIds.add(syncItems.getItemId());
                    });

                    HashMap<String, HashMap<String, WmSyncItems>> syncItemsByItemIdsAsMap = weedmapSyncItemRepository.getSyncItemsByItemIdsAsMap(companyId, shopId, itemIds);
                    List<WmSyncItems> syncSaveList = new ArrayList<>();
                    List<WmSyncItems> syncUpdateList = new ArrayList<>();

                    syncItemsList.forEach(syncItems -> {
                        if (syncItemsByItemIdsAsMap.containsKey(syncItems.getWmListingId())) {
                            HashMap<String, WmSyncItems> syncItemsMap = syncItemsByItemIdsAsMap.get(syncItems.getWmListingId());
                            WmSyncItems item = syncItemsMap.get(syncItems.getItemId());
                            if (item != null) {
                                syncItems.setWmItemId(item.getWmItemId());
                                syncItems.setWmLinkedItems(item.getWmLinkedItems());
                                syncItems.setVerified(item.isVerified());
                                syncItems.setWmsin(item.getWmsin());
                                syncUpdateList.add(syncItems);
                            } else {
                                syncSaveList.add(syncItems);
                            }
                        } else {
                            syncSaveList.add(syncItems);
                        }
                    });
                    if (!syncSaveList.isEmpty()) {
                        weedmapSyncItemRepository.save(syncSaveList);
                    }
                    if (!syncUpdateList.isEmpty()) {
                        syncUpdateList.forEach(syncItems -> {
                            weedmapSyncItemRepository.update(syncItems.getCompanyId(), syncItems.getId(), syncItems);
                        });
                    }
                }
            }
        }
    }
}
