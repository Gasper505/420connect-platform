package com.fourtwenty.integrations.woocommerce;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.member.MemberEmailAndDobUpdateEvent;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.RandomUtil;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MemberEmailAndDobUpdateSubscriber implements BlazeSubscriber {

    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;

    private static final Log LOG = LogFactory.getLog(MemberEmailAndDobUpdateSubscriber.class);
    private static final int FETCH_LIMIT = 1000;

    @Subscribe
    public void updateMemberWithEmailAndDob(MemberEmailAndDobUpdateEvent event) {

        final String companyId = event.getCompanyId();
        final String shopId = event.getShopId();

        if (StringUtils.isBlank(companyId)) {
            LOG.info("Company cannot be blank.");
            event.setResponse(false);
            return;
        }
        if (StringUtils.isBlank(shopId)) {
            LOG.info("Shop cannot be blank.");
            event.setResponse(false);
            return;
        }

        long count = memberRepository.countEmptyEmailAndDobMember(companyId, shopId);
        if (count == 0) {
            LOG.info("No employee found without email id or DOB");
            return;
        }

        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            LOG.info("Shop not found.");
            return;
        }

        DateTime dobTime = DateUtil.parseDate("01/01/1995");
        long dob = dobTime.getMillis();

        String email;
        Set<String> memberInfoSet = new HashSet<>();
        List<Member> updatedMembers = new ArrayList<>();
        boolean exist;
        do {

            List<Member> members = memberRepository.getEmptyEmailAndDobMember(companyId, shopId, 0, FETCH_LIMIT, Member.class);

            for (Member member : members) {
                exist = checkMemberUniqueness(memberInfoSet, member, shop);
                if (exist) {
                    //If member is already processed then skip it
                    continue;
                }
                if (StringUtils.isBlank(member.getEmail())) {
                    email = this.prepareEmailId(member, shop);
                    member.setEmail(email);
                }
                if (member.getDob() == null || member.getDob() <= 0) {
                    member.setDob(dob);
                }
                updatedMembers.add(member);
                memberRepository.update(companyId, member.getId(), member);
            }

            count = count - (long) members.size();

        } while (count > 0);

        if (updatedMembers.size() > 0) {
            elasticSearchManager.createOrUpdateIndexedDocuments(updatedMembers, 50);
        }
    }

    private boolean checkMemberUniqueness(Set<String> memberInfoSet, Member member, Shop shop) {
        String info = StringUtils.EMPTY;

        if (StringUtils.isNotBlank(member.getFirstName())) {
            info = info.concat(member.getFirstName().trim());
        }
        if (StringUtils.isNotBlank(member.getLastName())) {
            info = info.concat(member.getLastName().trim());
        }
        if (StringUtils.isNotBlank(member.getPrimaryPhone())) {
            String phoneNumber = AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
            if (phoneNumber != null) {
                info = info.concat(phoneNumber);
            }
        }

        boolean contains = memberInfoSet.contains(info);
        if (!contains) {
            memberInfoSet.add(info);
        }

        return contains;
    }

    private String prepareEmailId(Member member, Shop shop) {
        String email = StringUtils.EMPTY;

        if (StringUtils.isNotBlank(member.getEmail())) {
            return member.getEmail();
        }

        if (StringUtils.isNotEmpty(member.getFirstName())) {
            email = email.concat(member.getFirstName().trim());
        }
        if (StringUtils.isNotEmpty(member.getLastName())) {
            email = email.concat(member.getLastName().trim());
        }
        email = email.replaceAll(" ","");

        String uniqueId = "";
        if (StringUtils.isNotEmpty(member.getPrimaryPhone())) {
            String phoneNumber = AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
            uniqueId = phoneNumber;
        }

        if (StringUtils.isEmpty(uniqueId)) {
            uniqueId = RandomUtil.getIdentifier();
        }


        email = email.concat("-");
        email = email.concat(uniqueId.trim());
        email = email.replace("+","");
        email = email.concat("@liftedandchill-temp.com");

        return email.toLowerCase();
    }

}
