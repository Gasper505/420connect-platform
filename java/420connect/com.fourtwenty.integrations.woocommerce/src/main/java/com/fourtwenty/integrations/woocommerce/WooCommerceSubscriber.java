package com.fourtwenty.integrations.woocommerce;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.PasswordReset;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.repositories.dispensary.PasswordResetRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.consumeruser.WooCommerceRegistrationUserEvent;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class WooCommerceSubscriber implements BlazeSubscriber {
    private static final Logger LOGGER = LoggerFactory.getLogger(WooCommerceSubscriber.class);

    @Inject
    private PasswordResetRepository passwordResetRepository;
    @Inject
    private SecurityUtil securityUtil;
    @Inject
    private ConnectConfiguration connectConfiguration;
    @Inject
    private AmazonServiceManager amazonServiceManager;

    @Subscribe
    public void subscribe(WooCommerceRegistrationUserEvent event) {
        List<PasswordReset> passwordResets = passwordResetForConsumerUsers(event);
        for (ConsumerUser consumerUser : event.getConsumerUsers()) {
            for (PasswordReset passwordReset : passwordResets) {
                if (passwordReset.getEmployeeId().equalsIgnoreCase(consumerUser.getId())) {
                    String emailBody = getPasswordEmailBody(consumerUser, passwordReset);
                    LOGGER.info("Email send to : " + consumerUser.getEmail());
                    amazonServiceManager.sendEmail("support@blaze.me", consumerUser.getEmail(), "Password Reset", emailBody, null, null, "Blaze Support");
                }
            }
        }
    }

    /**
     * @param event : event
     * @return this method is responsible for consumer user password reset
     */
    private List<PasswordReset> passwordResetForConsumerUsers(WooCommerceRegistrationUserEvent event) {
        List<String> consumerUserIds = new ArrayList<>();
        List<PasswordReset> passwordResetList = new ArrayList<>();
        for (ConsumerUser consumerUser : event.getConsumerUsers()) {

            PasswordReset passwordReset = new PasswordReset();
            passwordReset.setCompanyId(consumerUser.getSourceCompanyId());
            passwordReset.setEmployeeId(consumerUser.getId());
            passwordReset.setResetCode(securityUtil.getNextResetCode());
            passwordReset.setExpirationDate(DateTime.now().plusDays(2).getMillis());
            passwordReset.setExpired(false);

            consumerUserIds.add(consumerUser.getId());
            passwordResetList.add(passwordReset);
        }
        passwordResetRepository.setPasswordExpiredForEmployees(event.getCompanyId(), consumerUserIds);
        return passwordResetRepository.save(passwordResetList);
    }

    /**
     * @param consumerUser  : consumeruser
     * @param passwordReset : passwordReset
     * @return this method is responsible for create consumer user reset password body
     */
    private String getPasswordEmailBody(ConsumerUser consumerUser, PasswordReset passwordReset) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/passwordreset.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String theString = writer.toString();

        theString = theString.replaceAll(Pattern.quote("{{name}}"), consumerUser.getFirstName().concat(" ").concat(consumerUser.getLastName()));
        theString = theString.replaceAll(Pattern.quote("{{action_url}}"), connectConfiguration.getAppWebsiteURL() + "/password/recovery?code=" + passwordReset.getResetCode());


        return theString;
    }
}
