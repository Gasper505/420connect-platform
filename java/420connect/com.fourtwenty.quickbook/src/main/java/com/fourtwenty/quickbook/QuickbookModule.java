package com.fourtwenty.quickbook;

import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.quickbook.lifecycle.QBDefaultDataLoader;
import com.fourtwenty.quickbook.lifecycle.QuickbookAppStartUp;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookService;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.quickbook.online.quickbookservices.impl.QuickbookSyncImpl;
import com.fourtwenty.quickbook.online.quickbookservices.impl.QuickbookServiceimpl;
import com.fourtwenty.quickbook.repositories.*;
import com.fourtwenty.quickbook.repositories.impl.*;
import com.fourtwenty.quickbook.service.QuickBooksSessionService;
import com.fourtwenty.quickbook.service.impl.QuickBookSessionServiceImpl;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class QuickbookModule extends AbstractModule {
    @Override
    protected void configure() {

        //Quickbook Services
        bind(QuickbookSyncDetailsRepository.class).to(QuickbookSyncDetailsRepositoryImpl.class);
        bind(QuickbookCustomEntitiesRepository.class).to(QuickbookCustomEntitiesRepositoryImpl.class);
        bind(QuickbookAccountRepository.class).to(QuickbookAccountRepositoryImpl.class);
        bind(QuickbookEntityRepository.class).to(QuickbookEntityRepositoryImpl.class);
        bind(QuickbookDesktopAccountRepository.class).to(QuickbookDesktopAccountRepositoryImpl.class);
        bind(QbDesktopOperationRepository.class).to(QbDesktopOperationRepositoryImpl.class);
        bind(QbAccountRepository.class).to(QbDesktopAccountRepositoryImpl.class);
        bind(ErrorLogsRepository.class).to(ErrorLogsRepositoryImpl.class);
        bind(QbDesktopCurrentSyncRepository.class).to(QbDesktopCurrentRepositoryImpl.class);
        bind(QBDesktopSyncReferenceRepository.class).to(QBDesktopSyncReferenceRepositoryImpl.class);
        bind(QBUniqueSequenceRepository.class).to(QBUniqueSequenceRepositoryImpl.class);
        bind(QuickBookAccountDetailsRepository.class).to(QuickBookAccountDetailsRepositoryImpl.class);
        bind(QuickBookSessionDataRepository.class).to(QuickBookSessionDataRepositoryImpl.class);


        bind(QuickbookService.class).to(QuickbookServiceimpl.class);
        bind(QuickbookSync.class).to(QuickbookSyncImpl.class);
        bind(QuickBooksSessionService.class).to(QuickBookSessionServiceImpl.class);

        Multibinder<AppStartup> mb = Multibinder.newSetBinder(binder(), AppStartup.class);
        mb.addBinding().to(QuickbookAppStartUp.class);
        mb.addBinding().to(QBDefaultDataLoader.class);

    }
}
