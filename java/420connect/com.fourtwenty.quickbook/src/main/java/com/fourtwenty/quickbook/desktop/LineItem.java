package com.fourtwenty.quickbook.desktop;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fourtwenty.quickbook.desktop.data.FullNameElement;

public class LineItem {

    @JacksonXmlProperty(localName = "ItemRef")
    private FullNameElement productId;

    @JacksonXmlProperty(localName = "Quantity")
    private double quantity;

    @JacksonXmlProperty(localName = "Cost")
    private double price;

    public FullNameElement getProductId() {
        return productId;
    }

    public void setProductId(FullNameElement productId) {
        this.productId = productId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
