package com.fourtwenty.quickbook.desktop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickbook_session_data")
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickBookSessionData extends ShopBaseModel {
    private String sessionId;
    private String qbRetCode = "";
    private boolean authenticated;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getQbRetCode() {
        return qbRetCode;
    }

    public void setQbRetCode(String qbRetCode) {
        this.qbRetCode = qbRetCode;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
