package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.AccountData;
import com.fourtwenty.quickbook.desktop.data.AccountRequest;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;

import java.util.List;
import java.util.Map;

public class AccountConvertor extends BaseConverter<AccountRequest, QBDesktopAccounts> {
    private DataWrapper<AccountRequest> accountDataWrapper = new DataWrapper<>();

    public AccountConvertor(List<QBDesktopAccounts> qbDesktopAccounts, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super(qbDesktopAccounts, qbDesktopFieldMap);
    }

    @Override
    protected void prepare(List<QBDesktopAccounts> qbDesktopAccounts, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (QBDesktopAccounts qbDesktopAccount : qbDesktopAccounts) {
            AccountRequest request = new AccountRequest();
            AccountData accountData = new AccountData();

            accountData.setAccountType(qbDesktopAccount.getAccountType());
            accountData.setName(qbDesktopAccount.getAccountName());
            request.setAccountData(accountData);
            accountDataWrapper.add(request);
        }


    }

    @Override
    protected String getExtraDataType() {
        return "Account";
    }

    @Override
    protected String getWrapperNode() {
        return "AccountAddReq";
    }

    @Override
    public DataWrapper<AccountRequest> getDataWrapper() {
        return accountDataWrapper;
    }
}
