package com.fourtwenty.quickbook.desktop.converters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.ExtraDataRequest;
import com.fourtwenty.quickbook.desktop.data.FullNameElement;
import com.fourtwenty.quickbook.desktop.data.TxnData;
import org.apache.commons.lang.StringEscapeUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public abstract class BaseConverter<D extends Data, M> implements Converter<M> {

    private XmlMapper objectMapper;
    protected List<M> mList;
    protected Map<String, QBDesktopField> qbDesktopFieldMap;

    public BaseConverter(List<M> mList, Map<String, QBDesktopField> qbDesktopFieldMap) {
        this.mList = mList;
        this.qbDesktopFieldMap = qbDesktopFieldMap;
        init();
    }

    public void init() {
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.setDefaultUseWrapper(false);
        objectMapper = new XmlMapper(xmlModule);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    protected ExtraDataRequest createCustomDataNode(String name, String value, String objectReference) {
        return new ExtraDataRequest(0, name, getExtraDataType(), value, objectReference);
    }

    @Override
    public String convert() {
        prepare(mList, qbDesktopFieldMap);
        DataWrapper<D> wrapper = getDataWrapper();
        StringBuilder result = new StringBuilder();
        try {
            for (D d : wrapper.getdList()) {
                result.append(StringEscapeUtils.unescapeHtml(objectMapper.writeValueAsString(d)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    protected abstract String getExtraDataType();

    protected abstract String getWrapperNode();

    protected abstract void prepare(List<M> mList, Map<String, QBDesktopField> qbDesktopFieldMap);

    protected FullNameElement createFullNameElement(String name, String listId) {
        FullNameElement element = new FullNameElement();
        element.setFields(name);
        element.setListId(listId);
        return element;
    }

    protected TxnData createTxnData(String txnId, BigDecimal amount) {
        TxnData txnData = new TxnData();
        txnData.setTxnId(txnId);
        txnData.setPaymentAmount(amount.setScale(2, BigDecimal.ROUND_HALF_UP));
        return txnData;
    }

    protected String cleanData(String data) {
        return TextUtil.formatSpecialCharacterConvertToHexCode(data);
    }


    public List<M> getmList() {
        return mList;
    }
}