package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.LineItem;
import com.fourtwenty.quickbook.desktop.data.Address;
import com.fourtwenty.quickbook.desktop.data.BillData;
import com.fourtwenty.quickbook.desktop.data.BillRequest;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.BillWrapper;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BillConverter extends BaseConverter<BillRequest, BillWrapper> {
    private DataWrapper<BillRequest> billRequestDataWrapper = new DataWrapper<>();

    public BillConverter(List<BillWrapper> billWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(billWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "Bill";
    }

    @Override
    protected String getWrapperNode() {
        return "BillAddRq";
    }

    @Override
    protected void prepare(List<BillWrapper> billWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (BillWrapper billWrapper : billWrappers) {
            BillRequest billRequest = new BillRequest();
            BillData billData = new BillData();

            if (qbDesktopFieldMap.get("VendorId") != null && qbDesktopFieldMap.get("VendorId").isEnabled()) {
                billData.setVendorRef(createFullNameElement(billWrapper.getVendorQbRef(), billWrapper.getVendorListId()));
            } else {
                System.out.println(billWrapper.getId());
            }

            if (qbDesktopFieldMap.get("Address") != null && qbDesktopFieldMap.get("Address").isEnabled()) {
                if (billWrapper.getAddress() != null) {
                    Address address = new Address();
                    String addr = billWrapper.getAddress().getAddress();
                    if (addr.length() > 25) {
                        addr = billWrapper.getAddress().getAddress().substring(0,25);
                    }
                    if (StringUtils.isNotBlank(addr)) {
                        address.setAddr(cleanData(addr));
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getCity())) {
                        address.setCity(billWrapper.getAddress().getCity());
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getState())) {
                        address.setState(billWrapper.getAddress().getState());
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getCountry())) {
                        address.setCountry(billWrapper.getAddress().getCountry());
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getZipCode())) {
                        address.setZipCode(billWrapper.getAddress().getZipCode());
                    }

                    billData.setAddress(address);
                }
            }

            if (qbDesktopFieldMap.get("Date") != null && qbDesktopFieldMap.get("Date").isEnabled()) {
                billData.setTxnDate(DateUtil.toDateFormattedURC(billWrapper.getCompletedDate(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.get("BillNo") != null && qbDesktopFieldMap.get("BillNo").isEnabled()) {
                billData.setRefNumber(billWrapper.getShipmentBillNumber());
            }

            HashMap<String, Product> productMap = billWrapper.getProductMap();
            List<LineItem> orderItems = billData.getOrderItems();
            if (qbDesktopFieldMap.get("ProductIds") != null && qbDesktopFieldMap.get("ProductIds").isEnabled()) {
                for (POProductRequest poProductRequest : billWrapper.getPoProductRequest()) {
                    Product product = productMap.get(poProductRequest.getProductId());
                    if (product == null) {
                        continue;
                    }
                    LineItem lineItem = createLineItem(product.getQbListId(), cleanData(product.getQbDesktopItemRef()), poProductRequest.getReceivedQuantity(), poProductRequest.getUnitPrice());
                    orderItems.add(lineItem);

                    if (qbDesktopFieldMap.get("ExciseTax") != null && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                            && poProductRequest.getTotalExciseTax() != null && poProductRequest.getTotalExciseTax().doubleValue() > 0) {
                        QBDesktopField qbDesktopField = qbDesktopFieldMap.get("ExciseTax");
                        orderItems.add(createLineItem(null, qbDesktopField.getName(), BigDecimal.ONE, poProductRequest.getTotalExciseTax()));
                    }

                    if (qbDesktopFieldMap.get("CultivationTax") != null && qbDesktopFieldMap.get("CultivationTax").isEnabled()
                            && poProductRequest.getTotalCultivationTax() != null && poProductRequest.getTotalCultivationTax().doubleValue() > 0) {
                        QBDesktopField qbDesktopField = qbDesktopFieldMap.get("CultivationTax");
                        orderItems.add(createLineItem(null, qbDesktopField.getName(), BigDecimal.ONE, poProductRequest.getTotalCultivationTax()));
                    }
                }
            }

            if (qbDesktopFieldMap.get("DeliveryFees") != null && qbDesktopFieldMap.get("DeliveryFees").isEnabled()
                    && billWrapper.getFees() != null && billWrapper.getFees().doubleValue() > 0) {
                QBDesktopField qbDesktopField = qbDesktopFieldMap.get("DeliveryFees");
                LineItem lineItem = createLineItem(null, qbDesktopField.getName(), BigDecimal.ONE, billWrapper.getFees());
                orderItems.add(lineItem);
            }

            if (qbDesktopFieldMap.get("Discount") != null && qbDesktopFieldMap.get("Discount").isEnabled()
                    && billWrapper.getDiscount() != null && billWrapper.getDiscount().doubleValue() > 0) {
                BigDecimal discount = billWrapper.getDiscount().multiply(new BigDecimal(-1));
                QBDesktopField qbDesktopField = qbDesktopFieldMap.get("Discount");
                LineItem lineItem = createLineItem(null, qbDesktopField.getName(), new BigDecimal(1), discount);
                orderItems.add(lineItem);
            }

            billRequest.setBillData(billData);
            billRequestDataWrapper.add(billRequest);
        }
    }

    @Override
    public DataWrapper<BillRequest> getDataWrapper() {
        return billRequestDataWrapper;
    }

    private LineItem createLineItem(String listId, String qbDesktopItemRef, BigDecimal quantity, BigDecimal unitPrice) {
        LineItem lineItem = new LineItem();

        lineItem.setProductId(createFullNameElement(cleanData(qbDesktopItemRef), listId));
        if (quantity != null)
            lineItem.setQuantity(quantity.doubleValue());
        if (unitPrice != null) {
            BigDecimal bigDecimal = unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            lineItem.setPrice(bigDecimal.doubleValue());
        }

        return lineItem;
    }
}



