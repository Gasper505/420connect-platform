package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.BillPaymentCheckData;
import com.fourtwenty.quickbook.desktop.data.BillPaymentCheckRequest;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.BillPaymentReceivedWrapper;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class BillPaymentConverter extends BaseConverter<BillPaymentCheckRequest, BillPaymentReceivedWrapper> {
    private DataWrapper<BillPaymentCheckRequest> receivedRequestDataWrapper = new DataWrapper<>();

    public BillPaymentConverter(List<BillPaymentReceivedWrapper> billPaymentReceivedWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(billPaymentReceivedWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "ReceivePayment";
    }

    @Override
    protected String getWrapperNode() {
        return "ReceivePaymentAddRq";
    }

    @Override
    protected void prepare(List<BillPaymentReceivedWrapper> billPaymentReceivedWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (BillPaymentReceivedWrapper billPaymentReceivedWrapper : billPaymentReceivedWrappers) {
            BillPaymentCheckRequest receivedRequest = new BillPaymentCheckRequest();
            BillPaymentCheckData data = new BillPaymentCheckData();

            if (qbDesktopFieldMap.get("VendorId") != null && qbDesktopFieldMap.get("VendorId").isEnabled()) {
                data.setPaymentEntityRef(createFullNameElement(billPaymentReceivedWrapper.getQbVendorRef(), billPaymentReceivedWrapper.getQbVendorListId()));
            }
            if (qbDesktopFieldMap.containsKey("PayableAccount") && qbDesktopFieldMap.get("PayableAccount").isEnabled()
                    && StringUtils.isNotBlank(billPaymentReceivedWrapper.getPayableAccount())) {
                data.setPayableAccountRef(createFullNameElement(cleanData(billPaymentReceivedWrapper.getPayableAccount()), null));
            }
            if (qbDesktopFieldMap.containsKey("BankAccount") && qbDesktopFieldMap.get("BankAccount").isEnabled()
                    && StringUtils.isNotBlank(billPaymentReceivedWrapper.getPayableAccount())) {
                data.setBankAccountRef(createFullNameElement(cleanData(billPaymentReceivedWrapper.getBankAccount()), null));
            }
            if (qbDesktopFieldMap.get("TxnData") != null && qbDesktopFieldMap.get("TxnData").isEnabled()) {
                data.setTxnData(createTxnData(billPaymentReceivedWrapper.getTxnId(), billPaymentReceivedWrapper.getAmountPaid()));
            }
            if (qbDesktopFieldMap.get("Date") != null && qbDesktopFieldMap.get("Date").isEnabled()) {
                data.setTxnDate(DateUtil.toDateFormattedURC(billPaymentReceivedWrapper.getPaidDate(), "yyyy-MM-dd"));
            }
            if (qbDesktopFieldMap.containsKey("RefNo") && qbDesktopFieldMap.get("RefNo").isEnabled()) {
                data.setRefNumber(cleanData(billPaymentReceivedWrapper.getRefNo()));
            }
            if (qbDesktopFieldMap.get("Memo") != null && qbDesktopFieldMap.get("Memo").isEnabled()) {
                data.setMemo(billPaymentReceivedWrapper.getMemo());
            }

            receivedRequest.setBillPaymentCheckData(data);
            receivedRequestDataWrapper.add(receivedRequest);
        }
    }

    @Override
    public DataWrapper<BillPaymentCheckRequest> getDataWrapper() {
        return receivedRequestDataWrapper;
    }
}
