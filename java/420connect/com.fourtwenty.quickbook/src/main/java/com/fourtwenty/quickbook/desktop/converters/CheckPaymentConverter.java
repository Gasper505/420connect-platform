package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.quickbook.desktop.data.*;
import com.fourtwenty.quickbook.desktop.wrappers.CheckWrapper;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class CheckPaymentConverter extends BaseConverter<CheckRequest, CheckWrapper> {
private DataWrapper<CheckRequest> receivedRequestDataWrapper = new DataWrapper<>();

    public CheckPaymentConverter(List<CheckWrapper> checkWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(checkWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "CheckAdd";
    }

    @Override
    protected String getWrapperNode() {
        return "CheckAddRq";
    }

    @Override
    protected void prepare(List<CheckWrapper> checkWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (CheckWrapper checkWrapper : checkWrappers) {
            CheckRequest checkRequest = new CheckRequest();
            CheckData checkData = new CheckData();

            if (qbDesktopFieldMap.get("BankAccount") != null && qbDesktopFieldMap.get("BankAccount").isEnabled()  && checkWrapper.getAccountRef() != null) {
                checkData.setAccountRef(createFullNameElement(cleanData(checkWrapper.getAccountRef()), null));
            }
            if (qbDesktopFieldMap.get("CustomerId") != null && qbDesktopFieldMap.get("CustomerId").isEnabled() &&checkWrapper.getPayeeEntityRef() != null) {
                checkData.setPayeeEntityRef(createFullNameElement(cleanData(checkWrapper.getPayeeEntityRef()), checkWrapper.getPayeeRefListId()));
            }
            if (qbDesktopFieldMap.get("RefNo") != null && qbDesktopFieldMap.get("RefNo").isEnabled() && checkWrapper.getRefNumber() != null) {
                checkData.setRefNumber(checkWrapper.getRefNumber());
            }
            if (qbDesktopFieldMap.get("Date") != null && qbDesktopFieldMap.get("Date").isEnabled() && checkWrapper.getTxnDate() > 0) {
                checkData.setTxnDate(DateUtil.toDateFormattedURC(checkWrapper.getTxnDate(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.get("TxnData") != null && qbDesktopFieldMap.get("TxnData").isEnabled() &&
                    qbDesktopFieldMap.get("TotalAmount") != null && qbDesktopFieldMap.get("TotalAmount").isEnabled() ) {
                ApplyCheckToTxnAdd toTxnAdd = new ApplyCheckToTxnAdd();
                toTxnAdd.setTxnID(checkWrapper.getTxnId());
                toTxnAdd.setAmount(NumberUtils.round(checkWrapper.getAmount().doubleValue(), 2));
                checkData.setApplyCheckToTxnAdd(toTxnAdd);
            }

            if (qbDesktopFieldMap.get("ReceivableAccount") != null && qbDesktopFieldMap.get("ReceivableAccount").isEnabled() &&
                    qbDesktopFieldMap.get("TotalAmount") != null && qbDesktopFieldMap.get("TotalAmount").isEnabled() ) {
                ExpenseLineAdd expenseLineAdd = new ExpenseLineAdd();
                expenseLineAdd.setAccountRef(createFullNameElement(cleanData(checkWrapper.getAccountReceivable()), null));
                expenseLineAdd.setAmount(NumberUtils.round(checkWrapper.getAmount().doubleValue(), 2));
                checkData.setExpenseLineAdd(expenseLineAdd);
            }

            checkRequest.setCheckData(checkData);
            receivedRequestDataWrapper.add(checkRequest);
        }


    }

    @Override
    public DataWrapper<CheckRequest> getDataWrapper() {
        return receivedRequestDataWrapper;
    }
}
