package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;

public interface Converter<M> {
    String convert();

    <D extends Data> DataWrapper<D> getDataWrapper();
}
