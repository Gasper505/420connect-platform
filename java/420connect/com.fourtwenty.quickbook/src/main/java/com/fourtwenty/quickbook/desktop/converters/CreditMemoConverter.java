package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.Address;
import com.fourtwenty.quickbook.desktop.data.CreditMemoData;
import com.fourtwenty.quickbook.desktop.data.CreditMemoRequest;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.SalesItem;
import com.fourtwenty.quickbook.desktop.wrappers.TransactionWrapper;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreditMemoConverter extends BaseConverter<CreditMemoRequest, TransactionWrapper> {

    private DataWrapper<CreditMemoRequest> itemDataWrapper = new DataWrapper<>();

    public CreditMemoConverter(List<TransactionWrapper> transactionWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(transactionWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<TransactionWrapper> transactionWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (TransactionWrapper transaction : transactionWrappers) {
            CreditMemoRequest request = new CreditMemoRequest();
            CreditMemoData receiptData = new CreditMemoData();

            if (qbDesktopFieldMap.containsKey("CustomerId") && qbDesktopFieldMap.get("CustomerId").isEnabled()
                    && StringUtils.isNotBlank(transaction.getMemberQbRef()) && StringUtils.isNotBlank(transaction.getMemberListId())) {
                receiptData.setCustomerInfo(createFullNameElement(cleanData(transaction.getMemberQbRef()), transaction.getMemberListId()));
            }

            if (qbDesktopFieldMap.containsKey("Date") && qbDesktopFieldMap.get("Date").isEnabled()) {
                receiptData.setTransactionDate(DateUtil.toDateFormattedURC(transaction.getProcessedTime(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.containsKey("TransactionNo") && qbDesktopFieldMap.get("TransactionNo").isEnabled()) {
                receiptData.setTransactionNo(transaction.getTransNo());
            }

            if (qbDesktopFieldMap.containsKey("Address") && qbDesktopFieldMap.get("Address").isEnabled()
                    && transaction.getDeliveryAddress() != null) {
                Address address = new Address();
                address.setAddr(cleanData(transaction.getDeliveryAddress().getAddress()));
                address.setCity(transaction.getDeliveryAddress().getCity());
                address.setState(transaction.getDeliveryAddress().getState());
                address.setCountry(transaction.getDeliveryAddress().getCountry());
                address.setZipCode(transaction.getDeliveryAddress().getZipCode());

                receiptData.setAddress(address);
            }

            HashMap<String, Product> productMap = transaction.getProductMap();
            List<SalesItem> orderItems = receiptData.getOrderItems();
            if (qbDesktopFieldMap.containsKey("ProductIds") && qbDesktopFieldMap.get("ProductIds").isEnabled()) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    Product product = productMap.get(item.getProductId());
                    if (product == null) {
                        continue;
                    }
                    SalesItem salesItem = createSalesItem(product.getQbListId(), cleanData(product.getQbDesktopItemRef()), item.getQuantity(), item.getUnitPrice());;
                    orderItems.add(salesItem);

                    if (item.getDiscount() != null && item.getDiscount().doubleValue() > 0) {
                        BigDecimal discount = item.getDiscount().multiply(new BigDecimal(-1));
                        orderItems.add(createSalesItem(null, "Discount", BigDecimal.ONE, discount));
                    }
                }
            }

            if (qbDesktopFieldMap.containsKey("CityTax") && qbDesktopFieldMap.get("CityTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalCityTax() != null
                    && transaction.getCart().getTaxResult().getTotalCityTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CityTax");
                SalesItem salesItem = createSalesItem( null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalCityTax());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("StateTax") && qbDesktopFieldMap.get("StateTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalStateTax() != null
                    && transaction.getCart().getTaxResult().getTotalStateTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("StateTax");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalStateTax());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("CountyTax") && qbDesktopFieldMap.get("CountyTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalCountyTax() != null
                    && transaction.getCart().getTaxResult().getTotalCountyTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CountyTax");
                SalesItem salesItem = createSalesItem( null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalCountyTax());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalExciseTax() != null
                    && transaction.getCart().getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                SalesItem salesItem = createSalesItem( null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalExciseTax());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("PreTaxDiscount") && qbDesktopFieldMap.get("PreTaxDiscount").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalPreCalcTax() != null
                    && transaction.getCart().getTaxResult().getTotalPreCalcTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("PreTaxDiscount");
                SalesItem salesItem = createSalesItem( null, field.getName(), new BigDecimal(-1), transaction.getCart().getTaxResult().getTotalPreCalcTax());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("AfterTaxDiscount") && qbDesktopFieldMap.get("AfterTaxDiscount").isEnabled()
                    && transaction.getCart().getAppliedAfterTaxDiscount() != null
                    && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("AfterTaxDiscount");
                SalesItem salesItem = createSalesItem( null, field.getName(), new BigDecimal(-1), transaction.getCart().getAppliedAfterTaxDiscount());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("DeliveryFees") && qbDesktopFieldMap.get("DeliveryFees").isEnabled()
                    && transaction.getCart().getDeliveryFee() != null
                    && transaction.getCart().getDeliveryFee().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("DeliveryFees");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getDeliveryFee());
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("CreditCardFees") && qbDesktopFieldMap.get("CreditCardFees").isEnabled()
                    && transaction.getCart().getCreditCardFee() != null && transaction.getCart().getCreditCardFee() != null
                    && transaction.getCart().getCreditCardFee().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CreditCardFees");
                SalesItem salesItem = createSalesItem( null, field.getName(), BigDecimal.ONE, transaction.getCart().getCreditCardFee());
                orderItems.add(salesItem);
            }

            if (transaction.getCart().getDiscount() != null && transaction.getCart().getDiscount().doubleValue() > 0) {
                BigDecimal discount = transaction.getCart().getDiscount().multiply(new BigDecimal(-1));
                SalesItem salesItem = createSalesItem(null, "Discount", new BigDecimal(1), discount);
                orderItems.add(salesItem);
            }

            request.setMemoData(receiptData);
            itemDataWrapper.add(request);
        }
    }

    private SalesItem createSalesItem(String listId, String qbDesktopItemRef, BigDecimal quantity, BigDecimal unitPrice) {
        SalesItem salesItem = new SalesItem();

        salesItem.setProductId(createFullNameElement(qbDesktopItemRef, listId));
        if (quantity != null)
            salesItem.setQuantity(quantity.doubleValue());
        if (unitPrice != null)
            salesItem.setPrice(unitPrice.doubleValue());

        return salesItem;
    }

    @Override
    public DataWrapper<CreditMemoRequest> getDataWrapper() {
        return itemDataWrapper;
    }
}
