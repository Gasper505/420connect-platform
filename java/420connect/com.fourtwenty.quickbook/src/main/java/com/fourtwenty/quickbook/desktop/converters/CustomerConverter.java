package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.quickbook.desktop.data.CustomerData;
import com.fourtwenty.quickbook.desktop.data.CustomerEditRequest;
import com.fourtwenty.quickbook.desktop.data.CustomerRequest;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.MemberWrapper;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class CustomerConverter<D extends Data> extends BaseConverter<D, MemberWrapper> {

    private DataWrapper<CustomerRequest> customerDataWrapper = new DataWrapper<>();

    public CustomerConverter(List<MemberWrapper> members, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(members, qbDesktopFieldMap);
    }

    public void prepare(List<MemberWrapper> members, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (MemberWrapper member : members) {
            CustomerRequest customerRequest = null;
            try {
                customerRequest = getDataClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

            CustomerData customerData = new CustomerData();
            String customerName = member.getQbMemberName();

            if (StringUtils.isNotBlank(customerName) && qbDesktopFieldMap.get("CustomerName") != null && qbDesktopFieldMap.get("CustomerName").isEnabled()) {
                customerData.setName(customerName);
            }

            if (StringUtils.isNotBlank(member.getPrimaryPhone()) && qbDesktopFieldMap.get("Phone") != null && qbDesktopFieldMap.get("Phone").isEnabled()) {
                customerData.setPhone(TextUtil.formatSpecialCharacterConvertToHexCode(member.getPrimaryPhone()));
            }
            if (StringUtils.isNotBlank(member.getEmail()) && qbDesktopFieldMap.get("Email") != null && qbDesktopFieldMap.get("Email").isEnabled()) {
                customerData.setEmail(TextUtil.formatSpecialCharacterConvertToHexCode(member.getEmail()));
            }

            if (customerRequest instanceof CustomerEditRequest && StringUtils.isNotBlank(member.getQbEditSequence())
                    && StringUtils.isNotBlank(member.getListId())) {
                customerData.setListId(member.getListId());
                customerData.setEditSequence(member.getQbEditSequence());
            }

            customerRequest.setCustomerData(customerData);
            customerDataWrapper.add(customerRequest);
        }
    }

    @Override
    public String getExtraDataType() {
        return "Customer";
    }

    @Override
    protected String getWrapperNode() {
        return "CustomerAddRq";
    }

    @Override
    public DataWrapper<CustomerRequest> getDataWrapper() {
        return customerDataWrapper;
    }

    protected Class<? extends CustomerRequest> getDataClass() {
        return CustomerRequest.class;
    }
}
