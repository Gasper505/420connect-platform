package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.CustomerEditRequest;
import com.fourtwenty.quickbook.desktop.data.CustomerRequest;
import com.fourtwenty.quickbook.desktop.wrappers.MemberWrapper;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class CustomerEditConverter extends CustomerConverter<CustomerEditRequest> {

    public CustomerEditConverter(List<MemberWrapper> members, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(members, qbDesktopFieldMap);
    }

    @Override
    public void prepare(List<MemberWrapper> members, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super.prepare(members, qbDesktopFieldMap);
    }

    @Override
    protected Class<? extends CustomerRequest> getDataClass() {
        return CustomerEditRequest.class;
    }
}
