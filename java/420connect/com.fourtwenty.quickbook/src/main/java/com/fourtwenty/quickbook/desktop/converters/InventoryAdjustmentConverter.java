package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.InventoryAdjustmentData;
import com.fourtwenty.quickbook.desktop.data.InventoryAdjustmentItem;
import com.fourtwenty.quickbook.desktop.data.InventoryAdjustmentRequest;
import com.fourtwenty.quickbook.desktop.data.QuantityAdjustment;
import com.fourtwenty.quickbook.desktop.wrappers.InventoryAdjustmentWrapper;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InventoryAdjustmentConverter extends BaseConverter<InventoryAdjustmentRequest, InventoryAdjustmentWrapper> {

    private DataWrapper<InventoryAdjustmentRequest> itemDataWrapper = new DataWrapper<>();

    public InventoryAdjustmentConverter(List<InventoryAdjustmentWrapper> inventoryAdjustmentWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super(inventoryAdjustmentWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "InventoryAdjustmentAdd";
    }

    @Override
    protected String getWrapperNode() {
        return "InventoryAdjustmentAddRq";
    }

    @Override
    protected void prepare(List<InventoryAdjustmentWrapper> adjustmentList, Map<String, QBDesktopField> qbDesktopFieldMap) {

        long currentTime = DateTime.now().getMillis();
        for (InventoryAdjustmentWrapper wrapper : adjustmentList) {
            InventoryAdjustmentRequest request = new InventoryAdjustmentRequest();

            InventoryAdjustmentData data = new InventoryAdjustmentData();

            request.setAdjustmentData(data);
            if (qbDesktopFieldMap.containsKey("ProductQbRef") && qbDesktopFieldMap.get("ProductQbRef").isEnabled()) {
                data.setAccountRef(createFullNameElement(wrapper.getAccountName(), null));
                data.setTransactionDate(DateUtil.toDateFormattedURC(currentTime, "yyyy-MM-dd"));

                List<InventoryAdjustmentItem> adjustmentItems = new ArrayList<>();
                InventoryAdjustmentItem adjustmentItem = new InventoryAdjustmentItem();
                adjustmentItem.setItemRef(createFullNameElement(cleanData(wrapper.getProductQBDesktopRef()), wrapper.getProductListId()));
                adjustmentItems.add(adjustmentItem);

                List<QuantityAdjustment> adjustments = new ArrayList<>();
                QuantityAdjustment adjustment = new QuantityAdjustment();
                if (wrapper.isQuantitySynced()) {
                    adjustment.setQuantityDifference(wrapper.getQuantity());
                } else {
                    adjustment.setNewQuantity(wrapper.getQuantity());
                }
                adjustment.setNewValue(wrapper.getQuantity().setScale(2, BigDecimal.ROUND_HALF_UP));
                adjustments.add(adjustment);

                if (qbDesktopFieldMap.get("ProductQuantity") != null && qbDesktopFieldMap.get("ProductQuantity").isEnabled()) {
                    adjustmentItem.setQuantityAdjustments(adjustments);
                }

                data.setItems(adjustmentItems);
                request.setAdjustmentData(data);
                itemDataWrapper.add(request);
            }

        }
    }

    @Override
    public DataWrapper<InventoryAdjustmentRequest> getDataWrapper() {
        return itemDataWrapper;
    }
}
