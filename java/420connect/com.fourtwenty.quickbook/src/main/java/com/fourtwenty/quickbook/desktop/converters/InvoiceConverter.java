package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.InvoiceData;
import com.fourtwenty.quickbook.desktop.data.InvoiceRequest;
import com.fourtwenty.quickbook.desktop.data.SalesItem;
import com.fourtwenty.quickbook.desktop.wrappers.InvoiceWrapper;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InvoiceConverter<D extends Data> extends BaseConverter<D, InvoiceWrapper> {

    private DataWrapper<InvoiceRequest> itemDataWrapper = new DataWrapper<>();

    public InvoiceConverter(List<InvoiceWrapper> invoiceWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super(invoiceWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<InvoiceWrapper> invoiceWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (InvoiceWrapper invoice : invoiceWrappers) {
            InvoiceRequest request = new InvoiceRequest();
            InvoiceData receiptData = new InvoiceData();

            receiptData.setEditSequence(invoice.getEditSequence());
            if (qbDesktopFieldMap.containsKey("DueDate") && qbDesktopFieldMap.get("DueDate").isEnabled()) {
                receiptData.setDueDate(DateUtil.toDateFormattedURC(invoice.getDueDate(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.containsKey("CustomerId") && qbDesktopFieldMap.get("CustomerId").isEnabled()) {
                receiptData.setCustomerInfo(createFullNameElement(cleanData(invoice.getVendorQbRef()), invoice.getVendorQbListId()));
            }

            if (qbDesktopFieldMap.containsKey("Date") && qbDesktopFieldMap.get("Date").isEnabled()) {
                receiptData.setInvoiceDate(DateUtil.toDateFormattedURC(invoice.getInvoiceDate(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.containsKey("InvoiceNo") && qbDesktopFieldMap.get("InvoiceNo").isEnabled()) {
                receiptData.setInvoiceNo(cleanData(invoice.getInvoiceNumber()));
            }
            if (qbDesktopFieldMap.containsKey("ReceivableAccount") && qbDesktopFieldMap.get("ReceivableAccount").isEnabled()
                    && StringUtils.isNotBlank(invoice.getReceivableAccount())) {
                receiptData.setArAccountRef(createFullNameElement(cleanData(invoice.getReceivableAccount()), null));
            }

            HashMap<String, Product> productMap = invoice.getProductMap();
            List<SalesItem> orderItems = receiptData.getOrderItems();
            if (qbDesktopFieldMap.containsKey("ProductIds") && qbDesktopFieldMap.get("ProductIds").isEnabled()) {
                for (OrderItem item : invoice.getCart().getItems()) {
                    Product product = productMap.get(item.getProductId());
                    if (product == null) {
                        continue;
                    }
                    SalesItem salesItem = createSalesItem(cleanData(product.getQbDesktopItemRef()), product.getQbListId(),
                            item.getQuantity(), item.getUnitPrice(), item.getTxnLineID());
                    orderItems.add(salesItem);
                }
            }

            if (qbDesktopFieldMap.containsKey("AdjustmentAmount") && qbDesktopFieldMap.get("AdjustmentAmount").isEnabled()
                    && invoice.getCart() != null && invoice.getCart().getAdjustmentAmount() != null
                    && invoice.getCart().getAdjustmentAmount().doubleValue() != 0) {
                int quantity = 0;
                if (invoice.getCart().getAdjustmentAmount().doubleValue() > 0) {
                    quantity = 1;
                }
                if (invoice.getCart().getAdjustmentAmount().doubleValue() < 0) {
                    quantity = -1;
                }
                QBDesktopField field = qbDesktopFieldMap.get("AdjustmentAmount");
                SalesItem salesItem = createSalesItem(field.getName(), null, new BigDecimal(quantity), invoice.getCart().getAdjustmentAmount().abs(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && invoice.getCart().getTaxResult() != null && invoice.getCart().getTaxResult().getTotalPostCalcTax() != null
                    && invoice.getCart().getTaxResult().getTotalPostCalcTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                SalesItem salesItem = createSalesItem(field.getName(), null, BigDecimal.ONE, invoice.getCart().getTaxResult().getTotalPostCalcTax(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                    && invoice.getCart().getTaxResult() != null && invoice.getCart().getTotalDiscount() != null
                    && invoice.getCart().getTotalDiscount().doubleValue() != 0) {
                BigDecimal discount = invoice.getCart().getTotalDiscount().multiply(new BigDecimal(-1));
                QBDesktopField field = qbDesktopFieldMap.get("Discount");
                SalesItem salesItem = createSalesItem(field.getName(), null, new BigDecimal(1), discount, null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("DeliveryFees") && qbDesktopFieldMap.get("DeliveryFees").isEnabled()
                    && invoice.getCart().getDeliveryFee() != null
                    && invoice.getCart().getDeliveryFee().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("DeliveryFees");
                SalesItem salesItem = createSalesItem(field.getName(), null, BigDecimal.ONE, invoice.getCart().getDeliveryFee(), null);
                orderItems.add(salesItem);
            }

            request.setData(receiptData);
            itemDataWrapper.add(request);
        }
    }

    private SalesItem createSalesItem(String qbDesktopItemRef, String listId, BigDecimal quantity, BigDecimal unitPrice, String txnLineID) {
        SalesItem salesItem = new SalesItem();

        salesItem.setProductId(createFullNameElement(cleanData(qbDesktopItemRef), listId));
        if (quantity != null)
            salesItem.setQuantity(quantity.doubleValue());
        if (unitPrice != null) {
            BigDecimal scale = unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            salesItem.setPrice(scale.doubleValue());
        }

        return salesItem;
    }

    @Override
    public DataWrapper<InvoiceRequest> getDataWrapper() {
        return itemDataWrapper;
    }

    protected Class<? extends InvoiceRequest> getDataClass() {
        return InvoiceRequest.class;
    }
}
