package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.JournalEntryData;
import com.fourtwenty.quickbook.desktop.data.JournalEntryRequest;
import com.fourtwenty.quickbook.desktop.data.JournalLineData;
import com.fourtwenty.quickbook.desktop.wrappers.JournalEntryWrapper;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class JournalEntryConverter extends BaseConverter<JournalEntryRequest, JournalEntryWrapper> {

    private DataWrapper<JournalEntryRequest> itemDataWrapper = new DataWrapper<>();

    public JournalEntryConverter(List<JournalEntryWrapper> journalEntryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(journalEntryWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<JournalEntryWrapper> journalEntries, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (JournalEntryWrapper journalEntry : journalEntries) {
            JournalEntryRequest entryRequest = new JournalEntryRequest();
            JournalEntryData entryData = new JournalEntryData();

            entryData.setTransactionProcessedDate(DateUtil.toDateFormattedURC(journalEntry.getTransactionProcessedDate(), "yyyy-MM-dd"));
            entryData.setTransNo(journalEntry.getTransNo());
            List<JournalLineData> creditLineDataList = entryData.getCreditLineDataList();
            List<JournalLineData> debitLineDataList = entryData.getDebitLineDataList();
            debitLineDataList.add(createJournalData(journalEntry.getDebitAccountRef(), journalEntry.getDebitAccountAmount()));
            creditLineDataList.add(createJournalData(journalEntry.getCreditAccountRef(), journalEntry.getCreditAccountAmount()));
            entryData.setCreditLineDataList(creditLineDataList);
            entryData.setDebitLineDataList(debitLineDataList);

            entryRequest.setEntryData(entryData);
            itemDataWrapper.add(entryRequest);
        }

    }

    private JournalLineData createJournalData(String debitAccountRef, BigDecimal amount) {
        JournalLineData data = new JournalLineData();
        data.setAccountRef(createFullNameElement(debitAccountRef, null));
        data.setAmount(amount);
        return data;
    }

    @Override
    public DataWrapper<JournalEntryRequest> getDataWrapper() {
        return itemDataWrapper;
    }
}
