package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.PaymentReceivedData;
import com.fourtwenty.quickbook.desktop.data.PaymentReceivedRequest;
import com.fourtwenty.quickbook.desktop.wrappers.PaymentReceivedWrapper;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class PaymentReceivedConverter extends BaseConverter<PaymentReceivedRequest, PaymentReceivedWrapper> {
    private DataWrapper<PaymentReceivedRequest> receivedRequestDataWrapper = new DataWrapper<>();

    public PaymentReceivedConverter(List<PaymentReceivedWrapper> paymentReceivedWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(paymentReceivedWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "ReceivePayment";
    }

    @Override
    protected String getWrapperNode() {
        return "ReceivePaymentAddRq";
    }

    @Override
    protected void prepare(List<PaymentReceivedWrapper> paymentReceivedWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (PaymentReceivedWrapper wrapper : paymentReceivedWrappers) {
            PaymentReceivedRequest receivedRequest = new PaymentReceivedRequest();
            PaymentReceivedData data = new PaymentReceivedData();

            if (qbDesktopFieldMap.get("CustomerQbRef") != null && qbDesktopFieldMap.get("CustomerQbRef").isEnabled()) {
                data.setCustomerRef(createFullNameElement(wrapper.getQbCustomerRef(), wrapper.getQbCustomerListId()));
            }
            if (qbDesktopFieldMap.containsKey("ReceivableAccount") && qbDesktopFieldMap.get("ReceivableAccount").isEnabled()
                    && StringUtils.isNotBlank(wrapper.getReceivableAccount())) {
                data.setReceivableAccountRef(createFullNameElement(cleanData(wrapper.getReceivableAccount()), null));
            }
            if (qbDesktopFieldMap.get("TxnData") != null && qbDesktopFieldMap.get("TxnData").isEnabled()) {
                data.setTxnData(createTxnData(wrapper.getTxnId(), wrapper.getAmountPaid()));
            }
            if (qbDesktopFieldMap.get("Date") != null && qbDesktopFieldMap.get("Date").isEnabled()) {
                data.setDate(DateUtil.toDateFormattedURC(wrapper.getPaidDate(), "yyyy-MM-dd"));
            }
            if (qbDesktopFieldMap.get("PaymentType") != null && qbDesktopFieldMap.get("PaymentType").isEnabled()) {
                data.setPaymentMethodRef(createFullNameElement(wrapper.getPaymentType().toString(),null));
            }
            if (qbDesktopFieldMap.get("PaymentNo") != null && qbDesktopFieldMap.get("PaymentNo").isEnabled()) {
                data.setRefNumber(wrapper.getPaymentNo());
            }
            if (qbDesktopFieldMap.get("TotalAmount") != null && qbDesktopFieldMap.get("TotalAmount").isEnabled()) {
                data.setTotalAmount(wrapper.getTotalAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
            }
            if (qbDesktopFieldMap.get("Memo") != null && qbDesktopFieldMap.get("Memo").isEnabled()) {
                data.setMemo(wrapper.getMemo());
            }
            if (StringUtils.isNotBlank(wrapper.getDepositAccount())) {
                data.setDepositAccountRef(createFullNameElement(cleanData(wrapper.getDepositAccount()), null));
            }
            if (qbDesktopFieldMap.get("TxnData") != null && qbDesktopFieldMap.get("TxnData").isEnabled()) {
                data.setTxnData(createTxnData(wrapper.getTxnId(), wrapper.getAmountPaid()));
            }

            receivedRequest.setPaymentReceivedData(data);
            receivedRequestDataWrapper.add(receivedRequest);
        }
    }

    @Override
    public DataWrapper<PaymentReceivedRequest> getDataWrapper() {
        return receivedRequestDataWrapper;
    }
}

