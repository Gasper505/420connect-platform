package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.PredefinedDataRequest;
import com.fourtwenty.quickbook.desktop.data.PredefinedItem;
import com.fourtwenty.quickbook.desktop.data.PredefinedItemData;
import com.fourtwenty.quickbook.desktop.data.PredefinedItemRequest;
import com.fourtwenty.quickbook.desktop.wrappers.PredefinedItemWrapper;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class PredefinedItemConvertor extends BaseConverter<PredefinedItemRequest, PredefinedItemWrapper>{

    DataWrapper<PredefinedItemRequest> predefinedItemRequestDataWrapper = new DataWrapper<>();
    public PredefinedItemConvertor(List<PredefinedItemWrapper> predefinedItemWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(predefinedItemWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<PredefinedItemWrapper> predefinedItemWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        double price = 1.0;
        for (PredefinedItemWrapper predefinedItemWrapper : predefinedItemWrappers) {
            List<PredefinedDataRequest> requests = predefinedItemWrapper.getRequests();
            for (PredefinedDataRequest request : requests) {
                PredefinedItemRequest itemRequest = new PredefinedItemRequest();
                PredefinedItemData data = new PredefinedItemData();
                data.setName(request.getName());

                PredefinedItem predefinedItem = new PredefinedItem();
                predefinedItem.setPrice(price);
                predefinedItem.setAccountRef(createFullNameElement(request.getAccountName(), null));

                data.setPredefinedItem(predefinedItem);
                itemRequest.setPredefinedItemData(data);
                predefinedItemRequestDataWrapper.add(itemRequest);
            }
        }
    }

    @Override
    public DataWrapper<PredefinedItemRequest> getDataWrapper() {
        return predefinedItemRequestDataWrapper;
    }
}
