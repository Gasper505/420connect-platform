package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.LineItem;
import com.fourtwenty.quickbook.desktop.data.Address;
import com.fourtwenty.quickbook.desktop.data.BillData;
import com.fourtwenty.quickbook.desktop.data.BillRequest;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.POProductItem;
import com.fourtwenty.quickbook.desktop.request.ProductCategoryPORequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryBillWrapper;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class ProductCategoryBillConverter extends BaseConverter<BillRequest, ProductCategoryBillWrapper> {
    private DataWrapper<BillRequest> billRequestDataWrapper = new DataWrapper<>();

    public ProductCategoryBillConverter(List<ProductCategoryBillWrapper> productCategoryBillWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException{
        super(productCategoryBillWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "Bill";
    }

    @Override
    protected String getWrapperNode() {
        return "BillAddRq";
    }

    @Override
    protected void prepare(List<ProductCategoryBillWrapper> productCategoryBillWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (ProductCategoryBillWrapper billWrapper : productCategoryBillWrappers) {
            BillRequest billRequest = new BillRequest();
            BillData billData = new BillData();

            if (qbDesktopFieldMap.get("VendorId") != null && qbDesktopFieldMap.get("VendorId").isEnabled()) {
                billData.setVendorRef(createFullNameElement(billWrapper.getVendorQbRef(), billWrapper.getVendorListId()));
            }

            if (qbDesktopFieldMap.get("Address") != null && qbDesktopFieldMap.get("Address").isEnabled()) {
                if (billWrapper.getAddress() != null) {
                    Address address = new Address();
                    String addr = billWrapper.getAddress().getAddress();
                    if (addr.length() > 25) {
                        addr = billWrapper.getAddress().getAddress().substring(0,25);
                    }
                    if (StringUtils.isNotBlank(addr)) {
                        address.setAddr(cleanData(addr));
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getCity())) {
                        address.setCity(billWrapper.getAddress().getCity());
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getState())) {
                        address.setState(billWrapper.getAddress().getState());
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getCountry())) {
                        address.setCountry(billWrapper.getAddress().getCountry());
                    }
                    if (StringUtils.isNotBlank(billWrapper.getAddress().getZipCode())) {
                        address.setZipCode(billWrapper.getAddress().getZipCode());
                    }

                    billData.setAddress(address);
                }
            }

            if (qbDesktopFieldMap.get("Date") != null && qbDesktopFieldMap.get("Date").isEnabled()) {
                billData.setTxnDate(DateUtil.toDateFormattedURC(billWrapper.getCompletedDate(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.get("BillNo") != null && qbDesktopFieldMap.get("BillNo").isEnabled()) {
                billData.setRefNumber(billWrapper.getShipmentBillNumber());
            }

            List<LineItem> orderItems = billData.getOrderItems();
            for (ProductCategoryPORequest productRequest : billWrapper.getRequests()) {
                LineItem item = new LineItem();
                item.setProductId(createFullNameElement(cleanData(productRequest.getCategoryRef()), productRequest.getCategoryListId()));
                item.setQuantity(productRequest.getQuantity());
                item.setPrice(productRequest.getTotalCost().doubleValue());
                orderItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && billWrapper.getTaxResult() != null && billWrapper.getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                LineItem item = new LineItem();
                item.setProductId(createFullNameElement(field.getName(), null));
                item.setPrice(billWrapper.getTaxResult().getTotalExciseTax().doubleValue());
                item.setQuantity(1);
                orderItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("Fee") && qbDesktopFieldMap.get("Fee").isEnabled()
                    && billWrapper.getFees() != null && billWrapper.getFees().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("Fee");
                LineItem item = new LineItem();
                item.setProductId(createFullNameElement(field.getName(), null));
                item.setPrice(billWrapper.getFees().doubleValue());
                item.setQuantity(1);
                orderItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                    && billWrapper.getDiscount() != null && billWrapper.getDiscount().doubleValue() != 0) {
                double discount = billWrapper.getDiscount().doubleValue() * (-1);
                QBDesktopField field = qbDesktopFieldMap.get("Discount");
                LineItem item = new LineItem();
                item.setProductId(createFullNameElement(field.getName(), null));
                item.setPrice(discount);
                item.setQuantity(1);
                orderItems.add(item);
            }

            billRequest.setBillData(billData);
            billRequestDataWrapper.add(billRequest);
        }
    }

    @Override
    public DataWrapper<BillRequest> getDataWrapper() {
        return billRequestDataWrapper;
    }
}
