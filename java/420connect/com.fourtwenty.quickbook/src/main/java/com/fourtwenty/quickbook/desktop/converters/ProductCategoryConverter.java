package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.ProductCategoryData;
import com.fourtwenty.quickbook.desktop.data.ProductCategoryEditRequest;
import com.fourtwenty.quickbook.desktop.data.ProductCategoryRequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryWrapper;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class ProductCategoryConverter<D extends Data> extends BaseConverter<D, ProductCategoryWrapper> {
    private DataWrapper<ProductCategoryRequest> categoryDataWrapper = new DataWrapper<>();

    public ProductCategoryConverter(List<ProductCategoryWrapper> productCategoryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(productCategoryWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "Item";
    }

    @Override
    protected String getWrapperNode() {
        return "ItemInventoryAddRq";
    }

    @Override
    protected void prepare(List<ProductCategoryWrapper> productCategoryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {


        for (ProductCategoryWrapper productCategoryWrapper : productCategoryWrappers) {
            ProductCategoryRequest request = null;

            try {
                request = getDataClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            ProductCategoryData productCategoryData = new ProductCategoryData();

            if (request instanceof ProductCategoryEditRequest && StringUtils.isNotBlank(productCategoryWrapper.getQbDesktopRef())
                    && StringUtils.isNotBlank(productCategoryWrapper.getQbListId()) && StringUtils.isNotBlank(productCategoryWrapper.getEditSequence())) {
                productCategoryData.setListId(productCategoryWrapper.getQbListId());
                productCategoryData.setEditSequence(productCategoryWrapper.getEditSequence());
            }

            if (qbDesktopFieldMap.containsKey("CategoryName") && qbDesktopFieldMap.get("CategoryName").isEnabled()
                    && StringUtils.isNotBlank(productCategoryWrapper.getQbCategoryName())) {
                productCategoryData.setCategoryName(cleanData(productCategoryWrapper.getQbCategoryName()));
            }

            if (qbDesktopFieldMap.containsKey("Cost") && qbDesktopFieldMap.get("Cost").isEnabled()) {
                productCategoryData.setCost(1.0);
            }
            if (qbDesktopFieldMap.containsKey("IncomeAccount") && qbDesktopFieldMap.get("IncomeAccount").isEnabled()
                    && StringUtils.isNotBlank(productCategoryWrapper.getIncomeAccount())) {
                productCategoryData.setIncomeAccount(createFullNameElement(cleanData(productCategoryWrapper.getIncomeAccount()), null));
            }
            if (qbDesktopFieldMap.containsKey("AssetAccount") && qbDesktopFieldMap.get("AssetAccount").isEnabled()
                    && StringUtils.isNotBlank(productCategoryWrapper.getInventoryAccount())) {
                productCategoryData.setAssetAccount(createFullNameElement(cleanData(productCategoryWrapper.getInventoryAccount()), null));
            }
            if (qbDesktopFieldMap.containsKey("COGSAccount") && qbDesktopFieldMap.get("COGSAccount").isEnabled()
                    && StringUtils.isNotBlank(productCategoryWrapper.getCogsAccount())) {
                productCategoryData.setCogsAccount(createFullNameElement(cleanData(productCategoryWrapper.getCogsAccount()), null));
            }

            request.setProductCategoryData(productCategoryData);
            categoryDataWrapper.add(request);
        }

    }

    @Override
    public DataWrapper<ProductCategoryRequest> getDataWrapper() {
        return categoryDataWrapper;
    }

    protected Class<? extends ProductCategoryRequest> getDataClass() {
        return ProductCategoryRequest.class;
    }
}
