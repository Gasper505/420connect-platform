package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.ProductCategoryEditRequest;
import com.fourtwenty.quickbook.desktop.data.ProductCategoryRequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryWrapper;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class ProductCategoryEditConverter extends ProductCategoryConverter<ProductCategoryEditRequest> {

    public ProductCategoryEditConverter(List<ProductCategoryWrapper> productCategoryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(productCategoryWrappers, qbDesktopFieldMap);
    }

    @Override
    protected void prepare(List<ProductCategoryWrapper> productCategoryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super.prepare(productCategoryWrappers, qbDesktopFieldMap);
    }

    @Override
    protected Class<? extends ProductCategoryRequest> getDataClass() {
        return ProductCategoryEditRequest.class;
    }
}
