package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.JournalEntryData;
import com.fourtwenty.quickbook.desktop.data.JournalEntryRequest;
import com.fourtwenty.quickbook.desktop.data.JournalLineData;
import com.fourtwenty.quickbook.desktop.wrappers.JournalEntryProductCategoryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

public class ProductCategoryJournalEntryConverter<D extends Data> extends BaseConverter<D, JournalEntryProductCategoryWrapper> {

    private DataWrapper<JournalEntryRequest> journalEntryRequestDataWrapper = new DataWrapper<>();

    public ProductCategoryJournalEntryConverter(List<JournalEntryProductCategoryWrapper> journalEntryProductCategoryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(journalEntryProductCategoryWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<JournalEntryProductCategoryWrapper> journalEntryProductCategoryWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (JournalEntryProductCategoryWrapper journalEntry : journalEntryProductCategoryWrappers) {
            JournalEntryRequest entryRequest = new JournalEntryRequest();
            JournalEntryData entryData = new JournalEntryData();

            entryData.setTransactionProcessedDate(DateUtil.toDateFormattedURC(DateTime.now().getMillis(), "yyyy-MM-dd"));
            List<JournalLineData> creditLineDataList = entryData.getCreditLineDataList();
            List<JournalLineData> debitLineDataList = entryData.getDebitLineDataList();
            if (qbDesktopFieldMap.containsKey("AssetAccount") && qbDesktopFieldMap.get("AssetAccount").isEnabled()
                    && StringUtils.isNotBlank(journalEntry.getQbCategoryRef())) {
                creditLineDataList.add(createJournalData(journalEntry.getCreditAccountRef(), journalEntry.getCreditAccountAmount(), journalEntry.getQbCategoryRef()));
            }

            if (qbDesktopFieldMap.containsKey("COGSAccount") && qbDesktopFieldMap.get("COGSAccount").isEnabled()
                    && StringUtils.isNotBlank(journalEntry.getQbCategoryRef())) {
                debitLineDataList.add(createJournalData(journalEntry.getDebitAccountRef(), journalEntry.getDebitAccountAmount(), journalEntry.getQbCategoryRef()));
            }

            if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                    && journalEntry.getDiscounts() > 0) {
                QBDesktopField field = qbDesktopFieldMap.get("Discount");
                JournalLineData journalData = createJournalData(journalEntry.getDebitAccountRef(), new BigDecimal(journalEntry.getDiscounts()), field.getName());
                creditLineDataList.add(journalData);
                debitLineDataList.add(journalData);
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && journalEntry.getExciseTax() > 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                JournalLineData journalData = createJournalData(journalEntry.getDebitAccountRef(), new BigDecimal(journalEntry.getExciseTax()), field.getName());
                creditLineDataList.add(journalData);
                debitLineDataList.add(journalData);
            }

            if (qbDesktopFieldMap.containsKey("DeliveryFees") && qbDesktopFieldMap.get("DeliveryFees").isEnabled()
                    && journalEntry.getDeliveryFees() > 0) {
                QBDesktopField field = qbDesktopFieldMap.get("DeliveryFees");
                JournalLineData journalData = createJournalData(journalEntry.getDebitAccountRef(), new BigDecimal(journalEntry.getDeliveryFees()), field.getName());
                creditLineDataList.add(journalData);
                debitLineDataList.add(journalData);
            }

            if (qbDesktopFieldMap.containsKey("CreditCardFees") && qbDesktopFieldMap.get("CreditCardFees").isEnabled()
                    && journalEntry.getCreditCardFees() > 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CreditCardFees");
                JournalLineData journalData = createJournalData(journalEntry.getDebitAccountRef(), new BigDecimal(journalEntry.getCreditCardFees()), field.getName());
                creditLineDataList.add(journalData);
                debitLineDataList.add(journalData);
            }

            entryData.setCreditLineDataList(creditLineDataList);
            entryData.setDebitLineDataList(debitLineDataList);

            entryRequest.setEntryData(entryData);
            journalEntryRequestDataWrapper.add(entryRequest);
        }
    }

    private JournalLineData createJournalData(String debitAccountRef, BigDecimal amount, String memo) {
        JournalLineData data = new JournalLineData();
        data.setAccountRef(createFullNameElement(debitAccountRef, null));
        data.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
        if (StringUtils.isNotBlank(memo)) {
            data.setMemo(memo);
        }
        return data;
    }

    @Override
    public DataWrapper<JournalEntryRequest> getDataWrapper() {
        return journalEntryRequestDataWrapper;
    }
}
