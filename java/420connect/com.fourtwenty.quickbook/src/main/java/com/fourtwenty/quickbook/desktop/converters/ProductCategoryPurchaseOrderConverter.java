package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.POProductItem;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderData;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderRequest;
import com.fourtwenty.quickbook.desktop.request.ProductCategoryPORequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryPOWrapper;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class ProductCategoryPurchaseOrderConverter extends BaseConverter<PurchaseOrderRequest, ProductCategoryPOWrapper> {
    private DataWrapper<PurchaseOrderRequest> poDataWrapper = new DataWrapper<>();

    public ProductCategoryPurchaseOrderConverter(List<ProductCategoryPOWrapper> purchaseOrders, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(purchaseOrders, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "PurchaseOrder";
    }

    @Override
    protected String getWrapperNode() {
        return "PurchaseOrderAddRq";
    }

    @Override
    protected void prepare(List<ProductCategoryPOWrapper> purchaseOrders, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (ProductCategoryPOWrapper purchaseOrder : purchaseOrders) {
            PurchaseOrderRequest request = new PurchaseOrderRequest();
            PurchaseOrderData orderData = new PurchaseOrderData();

            if (qbDesktopFieldMap.containsKey("VendorId") && qbDesktopFieldMap.get("VendorId").isEnabled() &&
                    StringUtils.isNotBlank(purchaseOrder.getQbVendorRef()) && StringUtils.isNotBlank(purchaseOrder.getQbVendorListId())) {
                orderData.setVendorId(createFullNameElement(cleanData(purchaseOrder.getQbVendorRef()), purchaseOrder.getQbVendorListId()));
            }

            if (qbDesktopFieldMap.containsKey("Date") && qbDesktopFieldMap.get("Date").isEnabled()) {
                orderData.setCreateDate(DateUtil.toDateFormattedURC(purchaseOrder.getCreated(), "yyyy-MM-dd"));
            } else {
                orderData.setCreateDate(DateUtil.toDateFormattedURC(DateTime.now().getMillis(), "yyyy-MM-dd"));
            }

            List<POProductItem> poProductItems = orderData.getPoProductItems();
            for (ProductCategoryPORequest productRequest : purchaseOrder.getRequests()) {
                POProductItem item = new POProductItem();
                item.setProductItem(createFullNameElement(cleanData(productRequest.getCategoryRef()), productRequest.getCategoryListId()));
                item.setQuantity(productRequest.getQuantity());
                item.setCost(productRequest.getTotalCost().doubleValue());
                poProductItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && purchaseOrder.getTaxResult() != null && purchaseOrder.getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                POProductItem item = new POProductItem();
                item.setProductItem(createFullNameElement(field.getName(), null));
                item.setCost(purchaseOrder.getTaxResult().getTotalExciseTax().doubleValue());
                item.setQuantity(1);
                poProductItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("Fee") && qbDesktopFieldMap.get("Fee").isEnabled()
                    && purchaseOrder.getFees() != null && purchaseOrder.getFees().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("Fee");
                POProductItem item = new POProductItem();
                item.setProductItem(createFullNameElement(field.getName(), null));
                item.setCost(purchaseOrder.getFees().doubleValue());
                item.setQuantity(1);
                poProductItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                    && purchaseOrder.getDiscount() != null && purchaseOrder.getDiscount().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("Discount");
                double discount = purchaseOrder.getDiscount().doubleValue() * (-1);
                POProductItem item = new POProductItem();
                item.setProductItem(createFullNameElement(field.getName(), null));
                item.setCost(discount);
                item.setQuantity(1);
                poProductItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("PONo") && qbDesktopFieldMap.get("PONo").isEnabled()) {
                orderData.setPoNumber(cleanData(purchaseOrder.getPoNumber()));
            }

            request.setOrderData(orderData);
            poDataWrapper.add(request);
        }
    }

    @Override
    public DataWrapper<PurchaseOrderRequest> getDataWrapper() {
        return poDataWrapper;
    }
}
