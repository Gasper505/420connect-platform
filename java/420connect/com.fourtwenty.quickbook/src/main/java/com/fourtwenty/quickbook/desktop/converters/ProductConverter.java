package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.ExtraDataRequest;
import com.fourtwenty.quickbook.desktop.data.ProductData;
import com.fourtwenty.quickbook.desktop.data.ProductEditRequest;
import com.fourtwenty.quickbook.desktop.data.ProductRequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductWrapper;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class ProductConverter<D extends Data> extends BaseConverter<D, ProductWrapper> {

    private DataWrapper<ProductRequest> itemDataWrapper = new DataWrapper<>();

    public ProductConverter(List<ProductWrapper> products, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(products, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "Item";
    }

    @Override
    protected String getWrapperNode() {
        return "ItemInventoryAddRq";
    }

    @Override
    protected void prepare(List<ProductWrapper> products, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (ProductWrapper product : products) {
            ProductRequest productRequest = null;
            try {
                productRequest = getDataClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            ProductData productData = new ProductData();

            String productName = product.getQbProductName();

            if (productRequest instanceof ProductEditRequest && StringUtils.isNotBlank(product.getQbDesktopItemRef())
                    && StringUtils.isNotBlank(product.getQbListId()) && StringUtils.isNotBlank(product.getEditSequence())) {
                productData.setListId(product.getQbListId());
                productData.setEditSequence(product.getEditSequence());
            }

            if (qbDesktopFieldMap.containsKey("ProductName") && qbDesktopFieldMap.get("ProductName").isEnabled()
                    && StringUtils.isNotBlank(product.getName())) {
                productData.setProductName(cleanData(productName));
            }
            if (qbDesktopFieldMap.containsKey("VendorId") && qbDesktopFieldMap.get("VendorId").isEnabled()
                    && StringUtils.isNotBlank(product.getVendorId()) && StringUtils.isNotBlank(product.getVendorQbRef())) {
                productData.setVendorId(createFullNameElement(cleanData(product.getVendorQbRef()), product.getVendorQbListId()));
            }
            if (qbDesktopFieldMap.containsKey("Cost") && qbDesktopFieldMap.get("Cost").isEnabled()) {
                if (product.getUnitPrice().doubleValue() > 0) {
                    productData.setCost(product.getUnitPrice().doubleValue());
                } else {
                    productData.setCost(0.0);
                }
            }
            if (qbDesktopFieldMap.containsKey("Description") && qbDesktopFieldMap.get("Description").isEnabled()
                    && StringUtils.isNotBlank(product.getDescription())) {
                productData.setSalesDesc(cleanData(product.getDescription()));
            }
            if (qbDesktopFieldMap.containsKey("IncomeAccount") && qbDesktopFieldMap.get("IncomeAccount").isEnabled()
                    && StringUtils.isNotBlank(product.getIncomeAccount())) {
                productData.setIncomeAccount(createFullNameElement(cleanData(product.getIncomeAccount()), null));
            }
            if (qbDesktopFieldMap.containsKey("AssetAccount") && qbDesktopFieldMap.get("AssetAccount").isEnabled()
                    && StringUtils.isNotBlank(product.getInventoryAccount())) {
                productData.setAssetAccount(createFullNameElement(cleanData(product.getInventoryAccount()), null));
            }
            if (qbDesktopFieldMap.containsKey("COGSAccount") && qbDesktopFieldMap.get("COGSAccount").isEnabled()
                    && StringUtils.isNotBlank(product.getCogsAccount())) {
                productData.setCogsAccount(createFullNameElement(cleanData(product.getCogsAccount()), null));
            }
            if (!(productRequest instanceof ProductEditRequest) && qbDesktopFieldMap.containsKey("Quantity") && qbDesktopFieldMap.get("Quantity").isEnabled()) {
                if (product.getCurrentQuantity() > 0) {
//                    productData.setQuantityOnHand(product.getCurrentQuantity());
                    productData.setQuantityOnHand(0.0);
                } else {
                    productData.setQuantityOnHand(0.0);
                }
            }

//            if (!(productRequest instanceof ProductEditRequest) && qbDesktopFieldMap.containsKey("COGS") && qbDesktopFieldMap.get("COGS").isEnabled()
//                    && product.getCogs() != null && product.getCogs().doubleValue() > 0) {
//                    BigDecimal cogs = product.getCogs().setScale(2, BigDecimal.ROUND_HALF_UP);
//                    productData.setCogs(cogs);
//            }

            if (!(productRequest instanceof ProductEditRequest) && qbDesktopFieldMap.containsKey("Sku")&&
                    qbDesktopFieldMap.get("Sku").isEnabled() && StringUtils.isNotBlank(product.getSku())) {
                QBDesktopField productField = qbDesktopFieldMap.get("Sku");
                ExtraDataRequest customDataNode = createCustomDataNode(productField.getName(), cleanData(product.getSku()), productName);

                itemDataWrapper.add(customDataNode);
            }

            productRequest.setProductData(productData);
            itemDataWrapper.add(productRequest);
        }
    }

    @Override
    public DataWrapper<ProductRequest> getDataWrapper() {
        return itemDataWrapper;
    }

    protected Class<? extends ProductRequest> getDataClass() {
        return ProductRequest.class;
    }
}
