package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.ProductEditRequest;
import com.fourtwenty.quickbook.desktop.data.ProductRequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductWrapper;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class ProductEditConverter extends ProductConverter<ProductEditRequest> {

    public ProductEditConverter(List<ProductWrapper> products, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(products, qbDesktopFieldMap);
    }

    @Override
    protected void prepare(List<ProductWrapper> products, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super.prepare(products, qbDesktopFieldMap);
    }

    @Override
    protected Class<? extends ProductRequest> getDataClass() {
        return ProductEditRequest.class;
    }
}
