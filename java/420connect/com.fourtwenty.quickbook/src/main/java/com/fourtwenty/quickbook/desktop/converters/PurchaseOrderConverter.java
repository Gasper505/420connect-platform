package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.transaction.CultivationTaxResult;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.POProductItem;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderData;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderEditRequest;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderRequest;
import com.fourtwenty.quickbook.desktop.wrappers.PurchaseOrderWrapper;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PurchaseOrderConverter<D extends Data> extends BaseConverter<D, PurchaseOrderWrapper> {

    private DataWrapper<PurchaseOrderRequest> itemDataWrapper = new DataWrapper<>();

    public PurchaseOrderConverter(List<PurchaseOrderWrapper> purchaseOrders, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(purchaseOrders, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return "PurchaseOrder";
    }

    @Override
    protected String getWrapperNode() {
        return "PurchaseOrderAddRq";
    }

    @Override
    protected void prepare(List<PurchaseOrderWrapper> purchaseOrders, Map<String, QBDesktopField> qbDesktopFieldMap) {

        long currentTime = DateTime.now().getMillis();

        for (PurchaseOrderWrapper purchaseOrder : purchaseOrders) {
            PurchaseOrderRequest request = null;
            try {
                request = getDataClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

            PurchaseOrderData orderData = new PurchaseOrderData();

            if (request instanceof PurchaseOrderEditRequest && StringUtils.isNotBlank(purchaseOrder.getQbDesktopPurchaseOrderRef())
                    && StringUtils.isNotBlank(purchaseOrder.getEditSequence())) {
                orderData.setTxnId(purchaseOrder.getQbDesktopPurchaseOrderRef());
                orderData.setEditSequence(purchaseOrder.getEditSequence());
            }

            if (qbDesktopFieldMap.containsKey("VendorId") && qbDesktopFieldMap.get("VendorId").isEnabled() &&
                StringUtils.isNotBlank(purchaseOrder.getVendorQbRef()) && StringUtils.isNotBlank(purchaseOrder.getVendorQbListId())) {
                orderData.setVendorId(createFullNameElement(cleanData(purchaseOrder.getVendorQbRef()), purchaseOrder.getVendorQbListId()));
            }

            if (qbDesktopFieldMap.containsKey("Date") && qbDesktopFieldMap.get("Date").isEnabled()) {
                orderData.setCreateDate(DateUtil.toDateFormattedURC(purchaseOrder.getCreated(), "yyyy-MM-dd"));
            } else {
                orderData.setCreateDate(DateUtil.toDateFormattedURC(currentTime, "yyyy-MM-dd"));
            }

            BigDecimal totalProductDiscount = new BigDecimal(0);
            List<POProductItem> poProductItems = new ArrayList<>();
            for (POProductRequest productRequest : purchaseOrder.getPoProductRequestList()) {
                Product product = purchaseOrder.getProductMap().get(productRequest.getProductId());
                if (product == null) {
                    continue;
                }

                String txnLineId = null;
                if (request instanceof PurchaseOrderEditRequest) {
                    if (StringUtils.isNotBlank(productRequest.getTxnLineID())) {
                        txnLineId = productRequest.getTxnLineID();
                    } else {
                        txnLineId = "-1";
                    }
                }

                if (productRequest.getDiscount().doubleValue() > 0) {
                    totalProductDiscount = totalProductDiscount.add(productRequest.getDiscount());
                }
                POProductItem item = this.createPOProductItem(product.getQbDesktopItemRef(), product.getQbListId(), productRequest.getRequestQuantity(), productRequest.getUnitPrice(), txnLineId);
                poProductItems.add(item);
            }

            String txnLineId = null;
            if (request instanceof PurchaseOrderEditRequest) {
                txnLineId = "-1";
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && purchaseOrder.getTaxResult() != null && purchaseOrder.getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                POProductItem item = this.createPOProductItem(field.getName(), null , new BigDecimal(1), purchaseOrder.getTaxResult().getTotalExciseTax(), txnLineId);
                poProductItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("Fee") && qbDesktopFieldMap.get("Fee").isEnabled()
                    && purchaseOrder.getFees() != null && purchaseOrder.getFees().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("Fee");
                POProductItem item = this.createPOProductItem(field.getName(), null , new BigDecimal(1), purchaseOrder.getFees(), txnLineId);
                poProductItems.add(item);
            }

            if (totalProductDiscount.doubleValue() > 0) {
                BigDecimal discount = totalProductDiscount.multiply(new BigDecimal(-1));
                POProductItem item = this.createPOProductItem("Total Product Discount", null , new BigDecimal(1), discount, txnLineId);
                poProductItems.add(item);
            }

            if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                    && purchaseOrder.getDiscount() != null && purchaseOrder.getDiscount().doubleValue() != 0) {
                BigDecimal discount = purchaseOrder.getDiscount().multiply(new BigDecimal(-1));
                QBDesktopField field = qbDesktopFieldMap.get("Discount");
                POProductItem item = this.createPOProductItem(field.getName(), null , new BigDecimal(1), discount, txnLineId);
                poProductItems.add(item);
            }


            if (purchaseOrder.getTaxResult() != null && purchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                CultivationTaxResult cultivationTaxResult = purchaseOrder.getTaxResult().getCultivationTaxResult();
                if (cultivationTaxResult != null && cultivationTaxResult.getTotalCultivationTax().doubleValue() > 0) {
                    POProductItem item = this.createPOProductItem("Cultivation Tax", null , new BigDecimal(1), cultivationTaxResult.getTotalCultivationTax(), txnLineId);
                    poProductItems.add(item);
                }
            }

            if (request instanceof PurchaseOrderEditRequest) {
                orderData.setPoProductModifiedItems(poProductItems);
            } else {
                orderData.setPoProductItems(poProductItems);
            }

            if (qbDesktopFieldMap.containsKey("PONo") && qbDesktopFieldMap.get("PONo").isEnabled()) {
                orderData.setPoNumber(cleanData(purchaseOrder.getPoNumber()));
            }

            request.setOrderData(orderData);
            itemDataWrapper.add(request);
        }
    }


    private POProductItem createPOProductItem(String qbDesktopItemRef, String listId, BigDecimal quantity, BigDecimal unitPrice, String txnLineID) {
        POProductItem poProductItem = new POProductItem();

        if (StringUtils.isNotBlank(txnLineID)) {
            poProductItem.setTxnLineID(txnLineID);
        }
        poProductItem.setProductItem(createFullNameElement(cleanData(qbDesktopItemRef), listId));
        if (quantity != null)
            poProductItem.setQuantity(quantity.doubleValue());
        if (unitPrice != null) {
            BigDecimal scale = unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            poProductItem.setCost(scale.doubleValue());
        }

        return poProductItem;
    }

    @Override
    public DataWrapper<PurchaseOrderRequest> getDataWrapper() {
        return itemDataWrapper;
    }

    protected Class<? extends PurchaseOrderRequest> getDataClass() {
        return PurchaseOrderRequest.class;
    }
}
