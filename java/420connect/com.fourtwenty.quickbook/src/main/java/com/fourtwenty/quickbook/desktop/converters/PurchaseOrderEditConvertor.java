package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderEditRequest;
import com.fourtwenty.quickbook.desktop.data.PurchaseOrderRequest;
import com.fourtwenty.quickbook.desktop.wrappers.PurchaseOrderWrapper;

import javax.xml.stream.XMLStreamException;
import java.util.List;
import java.util.Map;

public class PurchaseOrderEditConvertor extends PurchaseOrderConverter<PurchaseOrderEditRequest>  {

    public PurchaseOrderEditConvertor(List<PurchaseOrderWrapper> purchaseOrders, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(purchaseOrders, qbDesktopFieldMap);
    }

    @Override
    protected void prepare(List<PurchaseOrderWrapper> purchaseOrders, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super.prepare(purchaseOrders, qbDesktopFieldMap);
    }

    @Override
    protected Class<? extends PurchaseOrderRequest> getDataClass() {
        return PurchaseOrderEditRequest.class;
    }
}
