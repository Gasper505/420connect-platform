package com.fourtwenty.quickbook.desktop.converters;

import org.apache.commons.lang3.StringUtils;

public class QBFinalXMLConverter {
    private static final String VERSION = "13.0";
    private static final ErrorHandlingStrategy ERROR_HANDLING_STRATEGY = ErrorHandlingStrategy.CONTINUE_ON_ERROR;

    public String getFinalXmlString(String data) {
        if (StringUtils.isNotBlank(data)) {
            StringBuilder finalXmlStringBuilder = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<?qbxml version=\"" + VERSION + "\"?>\n" +
                    "<QBXML>\n" +
                    "        <QBXMLMsgsRq onError=\"" + ERROR_HANDLING_STRATEGY.getValue() + "\">\n");
            finalXmlStringBuilder.append(data);
            finalXmlStringBuilder.append("</QBXMLMsgsRq>\n" +
                    "</QBXML>");

            return finalXmlStringBuilder.toString();
        }
        return "";
    }

    public enum ErrorHandlingStrategy {
        STOP_ON_ERROR("stopOnError"),
        CONTINUE_ON_ERROR("continueOnError");

        String v;

        ErrorHandlingStrategy(String v) {
            this.v = v;
        }

        String getValue() {
            return v;
        }
    }
}
