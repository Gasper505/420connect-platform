package com.fourtwenty.quickbook.desktop.converters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.quickbook.desktop.data.ExtraDataRequest;
import org.apache.commons.lang.StringEscapeUtils;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class QBXMLConverter {
    private List<Converter> converters;

    public QBXMLConverter(List<Converter> converters) {
        this.converters = converters;
    }

    public String getXMLString() {
        StringBuilder xmlStringBuilder = new StringBuilder();
        List<ExtraDataRequest> extraDataRequests = new ArrayList<>();
        for (Converter converter : converters) {
            xmlStringBuilder.append(converter.convert());
            extraDataRequests.addAll(converter.getDataWrapper().getExtraDataList());
        }

        // Extra Data
        try {
            xmlStringBuilder.append(generateExtraDataXML(extraDataRequests));
        } catch (Exception e) {
            throw new BlazeOperationException(e);
        }

        return xmlStringBuilder.toString();
    }

    private String generateExtraDataXML(List<ExtraDataRequest> data) throws XMLStreamException, IOException {

        if (data != null && !data.isEmpty()) {
            JacksonXmlModule xmlModule = new JacksonXmlModule();
            xmlModule.setDefaultUseWrapper(false);
            XmlMapper objectMapper = new XmlMapper(xmlModule);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            StringBuilder result = new StringBuilder();

            for (ExtraDataRequest extraData : data) {
                result.append(StringEscapeUtils.unescapeHtml(objectMapper.writeValueAsString(extraData)));
            }

            return result.toString();
        }

        return "";
    }

}
