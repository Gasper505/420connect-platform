package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.QbAccountRequest;

import java.util.List;
import java.util.Map;

public class QbAccountConvertor extends BaseConverter<QbAccountRequest, QBDesktopAccounts>{
    DataWrapper<QbAccountRequest> qbAccountRequestDataWrapper = new DataWrapper<>();

    public QbAccountConvertor(List<QBDesktopAccounts> qbDesktopAccounts, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super(qbDesktopAccounts, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<QBDesktopAccounts> qbDesktopAccounts, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (QBDesktopAccounts.AccountType value : QBDesktopAccounts.AccountType.values()) {
            QbAccountRequest request = new QbAccountRequest();
            request.setAccountType(value);
            qbAccountRequestDataWrapper.add(request);
        }
    }

    @Override
    public DataWrapper<QbAccountRequest> getDataWrapper() {
        return qbAccountRequestDataWrapper;
    }
}

