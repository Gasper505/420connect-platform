package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.data.Address;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.SalesItem;
import com.fourtwenty.quickbook.desktop.data.SalesReceiptData;
import com.fourtwenty.quickbook.desktop.data.SalesReceiptRequest;
import com.fourtwenty.quickbook.desktop.wrappers.TransactionWrapper;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SalesReceiptConverter extends BaseConverter<SalesReceiptRequest, TransactionWrapper> {

    private DataWrapper<SalesReceiptRequest> itemDataWrapper = new DataWrapper<>();

    public SalesReceiptConverter(List<TransactionWrapper> transactionWrappers, Map<String, QBDesktopField> qbDesktopFieldMap) throws XMLStreamException {
        super(transactionWrappers, qbDesktopFieldMap);
    }

    @Override
    protected String getExtraDataType() {
        return null;
    }

    @Override
    protected String getWrapperNode() {
        return null;
    }

    @Override
    protected void prepare(List<TransactionWrapper> salesReceipts, Map<String, QBDesktopField> qbDesktopFieldMap) {
        for (TransactionWrapper transaction : salesReceipts) {
            SalesReceiptRequest request = new SalesReceiptRequest();
            SalesReceiptData receiptData = new SalesReceiptData();

            if (qbDesktopFieldMap.containsKey("CustomerId") && qbDesktopFieldMap.get("CustomerId").isEnabled()
                        && StringUtils.isNotBlank(transaction.getMemberQbRef()) && StringUtils.isNotBlank(transaction.getMemberListId())) {
                receiptData.setCustomerInfo(createFullNameElement(cleanData(transaction.getMemberQbRef()), transaction.getMemberListId()));
            }

            if (qbDesktopFieldMap.containsKey("Date") && qbDesktopFieldMap.get("Date").isEnabled()) {
                receiptData.setTransactionDate(DateUtil.toDateFormattedURC(transaction.getProcessedTime(), "yyyy-MM-dd"));
            }

            if (qbDesktopFieldMap.containsKey("TransactionNo") && qbDesktopFieldMap.get("TransactionNo").isEnabled()) {
                receiptData.setTransactionNo(transaction.getTransNo());
            }

            if (qbDesktopFieldMap.containsKey("DeliveryAddress") && qbDesktopFieldMap.get("DeliveryAddress").isEnabled()
                    && transaction.getDeliveryAddress() != null) {
                Address address = new Address();
                String address1 = transaction.getDeliveryAddress().getAddress();
                if (address1.length() > 25) {
                    address1 = transaction.getDeliveryAddress().getAddress().substring(0,25);
                }
                if (StringUtils.isNotBlank(address1)) {
                    address.setAddr(cleanData(address1));
                }
                if (StringUtils.isNotBlank(transaction.getDeliveryAddress().getCity())) {
                    address.setCity(transaction.getDeliveryAddress().getCity());
                }
                if (StringUtils.isNotBlank(transaction.getDeliveryAddress().getState())) {
                    address.setState(transaction.getDeliveryAddress().getState());
                }
                if (StringUtils.isNotBlank(transaction.getDeliveryAddress().getCountry())) {
                    address.setCountry(transaction.getDeliveryAddress().getCountry());
                }
                if (StringUtils.isNotBlank(transaction.getDeliveryAddress().getZipCode())) {
                    address.setZipCode(transaction.getDeliveryAddress().getZipCode());
                }
                if (transaction.getNote() != null && StringUtils.isNotBlank(transaction.getNote().getMessage())) {
                    address.setNote(cleanData(transaction.getNote().getMessage()));
                }

                receiptData.setAddress(address);
            }

            if (qbDesktopFieldMap.containsKey("PaymentType") && qbDesktopFieldMap.get("PaymentType").isEnabled() &&
                !transaction.getCart().getPaymentOption().equals(Cart.PaymentOption.None)) {
                receiptData.setPaymentType(createFullNameElement(transaction.getCart().getPaymentOption().toString(), null));
            }

            HashMap<String, Product> productMap = transaction.getProductMap();
            List<SalesItem> orderItems = receiptData.getOrderItems();
            if (qbDesktopFieldMap.containsKey("ProductIds") && qbDesktopFieldMap.get("ProductIds").isEnabled()) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    Product product = productMap.get(item.getProductId());
                    if (product == null) {
                        continue;
                    }
                    SalesItem salesItem = createSalesItem(product.getQbListId(), cleanData(product.getQbDesktopItemRef()), item.getQuantity(), item.getUnitPrice(), null);
                    orderItems.add(salesItem);

                    if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                            && item.getDiscount() != null && item.getDiscount().doubleValue() > 0) {
                        BigDecimal discount = item.getDiscount().multiply(new BigDecimal(-1));
                        QBDesktopField field = qbDesktopFieldMap.get("Discount");
                        orderItems.add(createSalesItem(null, field.getName(), new BigDecimal(1), discount, null));
                    }
                }
            }

            if (qbDesktopFieldMap.containsKey("CityTax") && qbDesktopFieldMap.get("CityTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalCityTax() != null
                    && transaction.getCart().getTaxResult().getTotalCityTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CityTax");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalCityTax(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("StateTax") && qbDesktopFieldMap.get("StateTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalStateTax() != null
                    && transaction.getCart().getTaxResult().getTotalStateTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("StateTax");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalStateTax(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("CountyTax") && qbDesktopFieldMap.get("CountyTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalCountyTax() != null
                    && transaction.getCart().getTaxResult().getTotalCountyTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CountyTax");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalCountyTax(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("ExciseTax") && qbDesktopFieldMap.get("ExciseTax").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalExciseTax() != null
                    && transaction.getCart().getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("ExciseTax");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalExciseTax(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("PreTaxDiscount") && qbDesktopFieldMap.get("PreTaxDiscount").isEnabled()
                    && transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalPreCalcTax() != null
                    && transaction.getCart().getTaxResult().getTotalPreCalcTax().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("PreTaxDiscount");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getTaxResult().getTotalPreCalcTax(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("AfterTaxDiscount") && qbDesktopFieldMap.get("AfterTaxDiscount").isEnabled()
                    && transaction.getCart().getAppliedAfterTaxDiscount() != null
                    && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("AfterTaxDiscount");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getAppliedAfterTaxDiscount(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("DeliveryFees") && qbDesktopFieldMap.get("DeliveryFees").isEnabled()
                    && transaction.getCart().getDeliveryFee() != null
                    && transaction.getCart().getDeliveryFee().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("DeliveryFees");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getDeliveryFee(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("CreditCardFees") && qbDesktopFieldMap.get("CreditCardFees").isEnabled()
                    && transaction.getCart().getCreditCardFee() != null && transaction.getCart().getCreditCardFee().doubleValue() != 0) {
                QBDesktopField field = qbDesktopFieldMap.get("CreditCardFees");
                SalesItem salesItem = createSalesItem(null, field.getName(), BigDecimal.ONE, transaction.getCart().getCreditCardFee(), null);
                orderItems.add(salesItem);
            }

            if (qbDesktopFieldMap.containsKey("Discount") && qbDesktopFieldMap.get("Discount").isEnabled()
                    && transaction.getCart().getDiscount() != null && transaction.getCart().getDiscount().doubleValue() > 0) {
                BigDecimal discount = transaction.getCart().getDiscount().multiply(new BigDecimal(-1));

                QBDesktopField field = qbDesktopFieldMap.get("Discount");
                SalesItem salesItem = createSalesItem(null, field.getName(), new BigDecimal(1), discount, null);
                orderItems.add(salesItem);
            }

            request.setReceiptData(receiptData);
            itemDataWrapper.add(request);
        }
    }

    private SalesItem createSalesItem(String listId, String qbDesktopItemRef, BigDecimal quantity, BigDecimal unitPrice, String saleTaxCode) {
        SalesItem salesItem = new SalesItem();

        salesItem.setProductId(createFullNameElement(cleanData(qbDesktopItemRef), listId));
        if (quantity != null)
            salesItem.setQuantity(quantity.doubleValue());
        if (unitPrice != null) {
            BigDecimal bigDecimal = unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            salesItem.setPrice(bigDecimal.doubleValue());
        }
        if (StringUtils.isNotBlank(saleTaxCode))
            salesItem.setSalesTaxCode(createFullNameElement(saleTaxCode, null));
        return salesItem;
    }

    @Override
    public DataWrapper<SalesReceiptRequest> getDataWrapper() {
        return itemDataWrapper;
    }
}
