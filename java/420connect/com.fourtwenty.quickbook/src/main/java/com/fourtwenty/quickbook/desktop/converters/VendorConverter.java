package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.quickbook.desktop.data.Data;
import com.fourtwenty.quickbook.desktop.data.Address;
import com.fourtwenty.quickbook.desktop.data.DataWrapper;
import com.fourtwenty.quickbook.desktop.data.VendorData;
import com.fourtwenty.quickbook.desktop.data.VendorEditRequest;
import com.fourtwenty.quickbook.desktop.data.VendorRequest;
import com.fourtwenty.quickbook.desktop.wrappers.VendorWrapper;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class VendorConverter<D extends Data> extends BaseConverter<D, VendorWrapper> {

    private DataWrapper<VendorRequest> vendorDataWrapper = new DataWrapper<>();

    public VendorConverter(List<VendorWrapper> vendors, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super(vendors, qbDesktopFieldMap);
    }

    @Override
    public void prepare(List<VendorWrapper> vendors, Map<String, QBDesktopField> qbDesktopFieldMap) {

        for (VendorWrapper vendor : vendors) {
            VendorRequest vendorRequest = null;
            try {
                vendorRequest = getDataClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            VendorData vendorData = new VendorData();
            if (qbDesktopFieldMap.containsKey("VendorName") && qbDesktopFieldMap.get("VendorName").isEnabled()) {
                String vendorName = vendor.getQbVendorName();

                if (vendorRequest instanceof VendorEditRequest && StringUtils.isNotBlank(vendor.getVendorQbRef())
                        && StringUtils.isNotBlank(vendor.getQbEditSequence()) && StringUtils.isNotBlank(vendor.getListId())) {
                    vendorData.setQbRef(vendor.getListId());
                    vendorData.setEditSequence(vendor.getQbEditSequence());
                }

                if (StringUtils.isNotBlank(vendorName) && qbDesktopFieldMap.get("VendorName") != null && qbDesktopFieldMap.get("VendorName").isEnabled()) {
                    vendorData.setName(TextUtil.formatSpecialCharacterConvertToHexCode(vendorName));
                }
                if (StringUtils.isNotBlank(vendor.getPhone()) && qbDesktopFieldMap.get("Phone") != null && qbDesktopFieldMap.get("Phone").isEnabled()) {
                    vendorData.setPhone(vendor.getPhone());
                }
                if (StringUtils.isNotBlank(vendor.getEmail()) && qbDesktopFieldMap.get("Email") != null && qbDesktopFieldMap.get("Email").isEnabled()) {
                    vendorData.setEmail(TextUtil.formatSpecialCharacterConvertToHexCode(vendor.getEmail()));
                }

                if (qbDesktopFieldMap.containsKey("Address") && qbDesktopFieldMap.get("Address").isEnabled()
                        && vendor.getAddress() != null) {
                    Address address = new Address();
                    String addr = vendor.getAddress().getAddress();
                    if (addr.length() > 25) {
                        addr = vendor.getAddress().getAddress().substring(0,25);
                    }
                    if (StringUtils.isNotBlank(addr)) {
                        address.setAddr(cleanData(addr));
                    }
                    if (StringUtils.isNotBlank(vendor.getAddress().getCity())) {
                        String city = vendor.getAddress().getCity();
                        if (city.length() > 31) {
                            city = city.substring(0,31);
                        }
                        address.setCity(city);
                    }
                    if (StringUtils.isNotBlank(vendor.getAddress().getState())) {
                        String state = vendor.getAddress().getState();
                        if (state.length() > 21) {
                            state = state.substring(0,21);
                        }
                        address.setState(state);
                    }
                    if (StringUtils.isNotBlank(vendor.getAddress().getCountry())) {
                        String country = vendor.getAddress().getCountry();
                        if (country.length() > 31) {
                            country = country.substring(0,31);
                        }
                        address.setCountry(country);
                    }
                    if (StringUtils.isNotBlank(vendor.getAddress().getZipCode())) {
                        String zipCode = vendor.getAddress().getZipCode();
                        if (zipCode.length() > 13) {
                            zipCode = zipCode.substring(0,13);
                        }
                        address.setZipCode(zipCode);
                    }

                    if (vendor.getNotes() != null && vendor.getNotes().size() > 0) {
                        String noteMessage = "";
                        for (Note note : vendor.getNotes()) {
                            if (note.getMessage().length() > 40) {
                                noteMessage = note.getMessage().substring(0, 39);
                            } else {
                                noteMessage = note.getMessage();
                            }
                            break;
                        }
                        address.setNote(cleanData(noteMessage));
                    }
                    vendorData.setAddress(address);
                }

                if (qbDesktopFieldMap.get("VendorType") != null && qbDesktopFieldMap.get("VendorType").isEnabled()
                        && vendor.getCompanyType() != null) {
                    vendorData.setVendorTypeRef(createFullNameElement(vendor.getCompanyType().toString(), null));
                }
                vendorRequest.setVendorData(vendorData);
                vendorDataWrapper.add(vendorRequest);
            }
        }
    }

    @Override
    public String getExtraDataType() {
        return "Vendor";
    }

    @Override
    protected String getWrapperNode() {
        return "VendorAddRq";
    }

    @Override
    public DataWrapper<VendorRequest> getDataWrapper() {
        return vendorDataWrapper;
    }

    protected Class<? extends VendorRequest> getDataClass() {
        return VendorRequest.class;
    }

}
