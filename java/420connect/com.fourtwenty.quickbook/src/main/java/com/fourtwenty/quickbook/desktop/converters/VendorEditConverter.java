package com.fourtwenty.quickbook.desktop.converters;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.quickbook.desktop.data.VendorEditRequest;
import com.fourtwenty.quickbook.desktop.data.VendorRequest;
import com.fourtwenty.quickbook.desktop.wrappers.VendorWrapper;

import java.util.List;
import java.util.Map;

public class VendorEditConverter extends VendorConverter<VendorEditRequest>{

    public VendorEditConverter(List<VendorWrapper> vendors, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super(vendors, qbDesktopFieldMap);
    }

    @Override
    public void prepare(List<VendorWrapper> vendors, Map<String, QBDesktopField> qbDesktopFieldMap) {
        super.prepare(vendors, qbDesktopFieldMap);
    }

    @Override
    protected Class<? extends VendorRequest> getDataClass() {
        return VendorEditRequest.class;
    }
}
