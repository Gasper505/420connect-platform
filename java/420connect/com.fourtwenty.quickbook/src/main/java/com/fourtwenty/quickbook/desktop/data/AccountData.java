package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;

public class AccountData extends Data {
    @JacksonXmlProperty(localName = "Name")
    private String name;
    @JacksonXmlProperty(localName = "AccountType")
    private QBDesktopAccounts.AccountType accountType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QBDesktopAccounts.AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(QBDesktopAccounts.AccountType accountType) {
        this.accountType = accountType;
    }
}
