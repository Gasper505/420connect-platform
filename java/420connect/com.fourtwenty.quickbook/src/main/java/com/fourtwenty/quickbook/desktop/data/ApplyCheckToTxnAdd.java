package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ApplyCheckToTxnAdd {
    @JacksonXmlProperty(localName = "TxnID")
    private String txnID;

    @JacksonXmlProperty(localName = "Amount")
    private double amount;

    public String getTxnID() {
        return txnID;
    }

    public void setTxnID(String txnID) {
        this.txnID = txnID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
