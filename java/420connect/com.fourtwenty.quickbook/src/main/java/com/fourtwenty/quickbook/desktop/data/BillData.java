package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fourtwenty.quickbook.desktop.LineItem;

import java.util.ArrayList;
import java.util.List;

public class BillData extends Data {

    @JacksonXmlProperty(localName = "VendorRef")
    private FullNameElement vendorRef;

    @JacksonXmlProperty(localName = "VendorAddress")
    private Address address;

    @JacksonXmlProperty(localName = "TxnDate")
    private String txnDate;

    @JacksonXmlProperty(localName = "RefNumber")
    private String refNumber;

    @JacksonXmlProperty(localName = "ItemLineAdd")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<LineItem> orderItems = new ArrayList<>();

    public FullNameElement getVendorRef() {
        return vendorRef;
    }

    public void setVendorRef(FullNameElement vendorRef) {
        this.vendorRef = vendorRef;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public List<LineItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<LineItem> orderItems) {
        this.orderItems = orderItems;
    }
}
