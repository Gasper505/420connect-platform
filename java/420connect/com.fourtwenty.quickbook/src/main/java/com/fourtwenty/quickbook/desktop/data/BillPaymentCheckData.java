package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BillPaymentCheckData extends Data {

    @JacksonXmlProperty(localName = "PayeeEntityRef")
    private FullNameElement paymentEntityRef;

    @JacksonXmlProperty(localName = "APAccountRef")
    private FullNameElement payableAccountRef;

    @JacksonXmlProperty(localName = "TxnDate")
    private String txnDate;

    @JacksonXmlProperty(localName = "BankAccountRef")
    private FullNameElement bankAccountRef;

    @JacksonXmlProperty(localName = "RefNumber")
    private String refNumber;

    @JacksonXmlProperty(localName = "Memo")
    private String memo;

    @JacksonXmlProperty(localName = "AppliedToTxnAdd")
    private TxnData txnData;

    public FullNameElement getPaymentEntityRef() {
        return paymentEntityRef;
    }

    public void setPaymentEntityRef(FullNameElement paymentEntityRef) {
        this.paymentEntityRef = paymentEntityRef;
    }

    public FullNameElement getPayableAccountRef() {
        return payableAccountRef;
    }

    public void setPayableAccountRef(FullNameElement payableAccountRef) {
        this.payableAccountRef = payableAccountRef;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public FullNameElement getBankAccountRef() {
        return bankAccountRef;
    }

    public void setBankAccountRef(FullNameElement bankAccountRef) {
        this.bankAccountRef = bankAccountRef;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public TxnData getTxnData() {
        return txnData;
    }

    public void setTxnData(TxnData txnData) {
        this.txnData = txnData;
    }
}
