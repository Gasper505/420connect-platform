package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "BillPaymentCheckAddRq")
public class BillPaymentCheckRequest extends Data {

    @JacksonXmlProperty(localName = "BillPaymentCheckAdd")
    private BillPaymentCheckData billPaymentCheckData;

    public BillPaymentCheckData getBillPaymentCheckData() {
        return billPaymentCheckData;
    }

    public void setBillPaymentCheckData(BillPaymentCheckData billPaymentCheckData) {
        this.billPaymentCheckData = billPaymentCheckData;
    }
}
