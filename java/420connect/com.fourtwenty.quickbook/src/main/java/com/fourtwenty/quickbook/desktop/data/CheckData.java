package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CheckData extends Data{

    @JacksonXmlProperty(localName = "AccountRef")
    private FullNameElement accountRef;

    @JacksonXmlProperty(localName = "PayeeEntityRef")
    private FullNameElement payeeEntityRef;

    @JacksonXmlProperty(localName = "RefNumber")
    private String refNumber;

    @JacksonXmlProperty(localName = "TxnDate")
    private String txnDate;

    @JacksonXmlProperty(localName = "ApplyCheckToTxnAdd")
    private ApplyCheckToTxnAdd applyCheckToTxnAdd;

    @JacksonXmlProperty(localName = "ExpenseLineAdd")
    private ExpenseLineAdd expenseLineAdd;

    public FullNameElement getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(FullNameElement accountRef) {
        this.accountRef = accountRef;
    }

    public FullNameElement getPayeeEntityRef() {
        return payeeEntityRef;
    }

    public void setPayeeEntityRef(FullNameElement payeeEntityRef) {
        this.payeeEntityRef = payeeEntityRef;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public ApplyCheckToTxnAdd getApplyCheckToTxnAdd() {
        return applyCheckToTxnAdd;
    }

    public void setApplyCheckToTxnAdd(ApplyCheckToTxnAdd applyCheckToTxnAdd) {
        this.applyCheckToTxnAdd = applyCheckToTxnAdd;
    }

    public ExpenseLineAdd getExpenseLineAdd() {
        return expenseLineAdd;
    }

    public void setExpenseLineAdd(ExpenseLineAdd expenseLineAdd) {
        this.expenseLineAdd = expenseLineAdd;
    }
}
