package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "CheckAddRq")
public class CheckRequest extends Data {

    @JacksonXmlProperty(localName = "CheckAdd")
    private CheckData checkData;

    public CheckData getCheckData() {
        return checkData;
    }

    public void setCheckData(CheckData checkData) {
        this.checkData = checkData;
    }
}
