package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "CreditMemoAddRq")
public class CreditMemoRequest extends Data {

    @JacksonXmlProperty(localName = "CreditMemoAdd")
    private CreditMemoData memoData;

    public CreditMemoData getMemoData() {
        return memoData;
    }

    public void setMemoData(CreditMemoData memoData) {
        this.memoData = memoData;
    }
}
