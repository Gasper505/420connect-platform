package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "CustomerModRq")
public class CustomerEditRequest extends CustomerRequest {

    @JacksonXmlProperty(localName = "CustomerMod")
    private CustomerData customerData;

    @Override
    public CustomerData getCustomerData() {
        return customerData;
    }

    @Override
    public void setCustomerData(CustomerData customerData) {
        this.customerData = customerData;
    }
}
