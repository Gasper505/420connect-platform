package com.fourtwenty.quickbook.desktop.data;

import java.util.ArrayList;
import java.util.List;

public class DataWrapper<D extends Data> {
    private List<D> dList;
    private List<ExtraDataRequest> extraDataRequestList;

    public DataWrapper() {
        this.dList = new ArrayList<>();
        this.extraDataRequestList = new ArrayList<>();
    }

    public void add(D d) {
        this.dList.add(d);
    }

    public void add(ExtraDataRequest extraData) {
        this.extraDataRequestList.add(extraData);
    }

    public List<D> getdList() {
        return dList;
    }

    public List<ExtraDataRequest> getExtraDataList() {
        return extraDataRequestList;
    }
}