package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ExpenseLineAdd {

    @JacksonXmlProperty(localName = "AccountRef")
    private FullNameElement accountRef;

    @JacksonXmlProperty(localName = "Amount")
    private double amount;

    public FullNameElement getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(FullNameElement accountRef) {
        this.accountRef = accountRef;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
