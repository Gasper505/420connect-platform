package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fourtwenty.core.util.TextUtil;

public class ExtraData {

    @JacksonXmlProperty(localName = "OwnerID")
    private int ownerId;

    @JacksonXmlProperty(localName = "DataExtName")
    private String name;

    @JacksonXmlProperty(localName = "ListDataExtType")
    private String dataType;

    @JacksonXmlProperty(localName = "ListObjRef")
    private ObjectReference objectReference;

    @JacksonXmlProperty(localName = "DataExtValue")
    private String value;

    public ExtraData(int ownerId, String name, String dataType, String value, String objectReference) {
        this.ownerId = ownerId;
        this.name = name;
        this.dataType = dataType;
        this.value = value;
        this.objectReference = new ObjectReference(objectReference);
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public ObjectReference getObjectReference() {
        return objectReference;
    }

    public void setObjectReference(ObjectReference objectReference) {
        this.objectReference = objectReference;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private static class ObjectReference {
        @JacksonXmlProperty(localName = "FullName")
        private String name;

        public ObjectReference(String name) {
            this.name = TextUtil.formatSpecialCharacterConvertToHexCode(name);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
