package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "DataExtAddRq")
public class ExtraDataRequest {

    public ExtraDataRequest(int ownerId, String name, String dataType, String value, String objectReference) {
        this.extraData = new ExtraData(ownerId, name, dataType, value, objectReference);
    }

    @JacksonXmlProperty(localName = "DataExtAdd")
    private ExtraData extraData;

    public ExtraData getExtraData() {
        return extraData;
    }

    public void setExtraData(ExtraData extraData) {
        this.extraData = extraData;
    }
}
