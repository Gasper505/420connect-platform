package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class FullNameElement {

    @JacksonXmlProperty(localName = "ListID")
    private String listId;
    @JacksonXmlProperty(localName = "FullName")
    private String fields;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}
