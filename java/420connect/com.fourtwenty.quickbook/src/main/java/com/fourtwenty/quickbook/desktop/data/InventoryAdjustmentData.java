package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class InventoryAdjustmentData extends Data {

    @JacksonXmlProperty(localName = "AccountRef")
    private FullNameElement accountRef;

    @JacksonXmlProperty(localName = "TxnDate")
    private String transactionDate;

    @JacksonXmlProperty(localName = "RefNumber")
    private String refNumber;

    @JacksonXmlProperty(localName = "InventoryAdjustmentLineAdd")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<InventoryAdjustmentItem> items = new ArrayList<>();

    public FullNameElement getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(FullNameElement accountRef) {
        this.accountRef = accountRef;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public List<InventoryAdjustmentItem> getItems() {
        return items;
    }

    public void setItems(List<InventoryAdjustmentItem> items) {
        this.items = items;
    }
}
