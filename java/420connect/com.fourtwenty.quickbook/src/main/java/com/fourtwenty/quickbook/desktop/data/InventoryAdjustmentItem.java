package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class InventoryAdjustmentItem {

    @JacksonXmlProperty(localName = "ItemRef")
    private FullNameElement itemRef;

    @JacksonXmlProperty(localName = "ValueAdjustment")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<QuantityAdjustment> quantityAdjustments;

    public FullNameElement getItemRef() {
        return itemRef;
    }

    public void setItemRef(FullNameElement itemRef) {
        this.itemRef = itemRef;
    }

    public List<QuantityAdjustment> getQuantityAdjustments() {
        return quantityAdjustments;
    }

    public void setQuantityAdjustments(List<QuantityAdjustment> quantityAdjustments) {
        this.quantityAdjustments = quantityAdjustments;
    }
}
