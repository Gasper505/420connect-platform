package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "InventoryAdjustmentAddRq")
public class InventoryAdjustmentRequest extends Data {

    @JacksonXmlProperty(localName = "InventoryAdjustmentAdd")
    private InventoryAdjustmentData adjustmentData;

    public InventoryAdjustmentData getAdjustmentData() {
        return adjustmentData;
    }

    public void setAdjustmentData(InventoryAdjustmentData adjustmentData) {
        this.adjustmentData = adjustmentData;
    }
}
