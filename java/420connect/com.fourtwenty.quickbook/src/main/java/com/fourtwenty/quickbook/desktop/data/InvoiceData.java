package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class InvoiceData {

    @JacksonXmlProperty(localName = "TxnID")
    private String txnID;

    @JacksonXmlProperty(localName = "EditSequence")
    private String editSequence;

    @JacksonXmlProperty(localName = "CustomerRef")
    private FullNameElement customerInfo;

    @JacksonXmlProperty(localName = "ARAccountRef")
    private FullNameElement arAccountRef;

    @JacksonXmlProperty(localName = "TxnDate")
    private String invoiceDate;

    @JacksonXmlProperty(localName = "RefNumber")
    private String invoiceNo;

    @JacksonXmlProperty(localName = "BillAddress")
    private Address address;

    @JacksonXmlProperty(localName = "DueDate")
    private String dueDate;

    @JacksonXmlProperty(localName = "InvoiceLineAdd")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<SalesItem> orderItems = new ArrayList<>();

    public String getTxnID() {
        return txnID;
    }

    public void setTxnID(String txnID) {
        this.txnID = txnID;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public FullNameElement getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(FullNameElement customerInfo) {
        this.customerInfo = customerInfo;
    }

    public FullNameElement getArAccountRef() {
        return arAccountRef;
    }

    public void setArAccountRef(FullNameElement arAccountRef) {
        this.arAccountRef = arAccountRef;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public List<SalesItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<SalesItem> orderItems) {
        this.orderItems = orderItems;
    }
}
