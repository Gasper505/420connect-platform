package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "InvoiceModRq")
public class InvoiceEditRequest extends Data {

    @JacksonXmlProperty(localName = "InvoiceMod")
    private InvoiceEditData data;

    public InvoiceEditData getData() {
        return data;
    }

    public void setData(InvoiceEditData data) {
        this.data = data;
    }
}
