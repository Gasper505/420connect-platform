package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;


public class JournalEntryData {

    @JacksonXmlProperty(localName = "TxnDate")
    private String transactionProcessedDate;

    @JacksonXmlProperty(localName = "RefNumber")
    private String transNo;

    @JacksonXmlProperty(localName = "JournalDebitLine")
    @JacksonXmlElementWrapper(useWrapping = false)
    private  List<JournalLineData> debitLineDataList = new ArrayList<>();

    @JacksonXmlProperty(localName = "JournalCreditLine")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<JournalLineData> creditLineDataList = new ArrayList<>();

    public String getTransactionProcessedDate() {
        return transactionProcessedDate;
    }

    public void setTransactionProcessedDate(String transactionProcessedDate) {
        this.transactionProcessedDate = transactionProcessedDate;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public List<JournalLineData> getDebitLineDataList() {
        return debitLineDataList;
    }

    public void setDebitLineDataList(List<JournalLineData> debitLineDataList) {
        this.debitLineDataList = debitLineDataList;
    }

    public List<JournalLineData> getCreditLineDataList() {
        return creditLineDataList;
    }

    public void setCreditLineDataList(List<JournalLineData> creditLineDataList) {
        this.creditLineDataList = creditLineDataList;
    }
}
