package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "JournalEntryAddRq")
public class JournalEntryRequest extends Data {

    @JacksonXmlProperty(localName = "JournalEntryAdd")
    private JournalEntryData entryData;

    public JournalEntryData getEntryData() {
        return entryData;
    }

    public void setEntryData(JournalEntryData entryData) {
        this.entryData = entryData;
    }
}
