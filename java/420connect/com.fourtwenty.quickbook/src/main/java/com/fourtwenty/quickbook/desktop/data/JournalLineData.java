package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.math.BigDecimal;

public class JournalLineData {

    @JacksonXmlProperty(localName = "AccountRef")
    private FullNameElement accountRef;

    @JacksonXmlProperty(localName = "Amount")
    private BigDecimal amount;

    @JacksonXmlProperty(localName = "Memo")
    private String  memo;

    public FullNameElement getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(FullNameElement accountRef) {
        this.accountRef = accountRef;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
