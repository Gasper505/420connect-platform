package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class POProductItem {

    @JacksonXmlProperty(localName = "TxnLineID")
    private String  txnLineID;

    @JacksonXmlProperty(localName = "ItemRef")
    private FullNameElement productItem;

    @JacksonXmlProperty(localName = "Quantity")
    private double quantity;

    @JacksonXmlProperty(localName = "Rate")
    private double cost;

    public String getTxnLineID() {
        return txnLineID;
    }

    public void setTxnLineID(String txnLineID) {
        this.txnLineID = txnLineID;
    }

    public FullNameElement getProductItem() {
        return productItem;
    }

    public void setProductItem(FullNameElement productItem) {
        this.productItem = productItem;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
