package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.math.BigDecimal;

public class PaymentReceivedData extends Data {

    @JacksonXmlProperty(localName = "CustomerRef")
    private FullNameElement customerRef;

    @JacksonXmlProperty(localName = "ARAccountRef")
    private FullNameElement receivableAccountRef;

    @JacksonXmlProperty(localName = "TxnDate")
    private String date;

    @JacksonXmlProperty(localName = "RefNumber")
    private String refNumber;

    @JacksonXmlProperty(localName = "TotalAmount")
    private BigDecimal totalAmount;

    @JacksonXmlProperty(localName = "PaymentMethodRef")
    private FullNameElement paymentMethodRef;

    @JacksonXmlProperty(localName = "Memo")
    private String memo;

    @JacksonXmlProperty(localName = "DepositToAccountRef")
    private FullNameElement depositAccountRef;

    @JacksonXmlProperty(localName = "AppliedToTxnAdd")
    private TxnData txnData;

    public FullNameElement getCustomerRef() {
        return customerRef;
    }

    public void setCustomerRef(FullNameElement customerRef) {
        this.customerRef = customerRef;
    }

    public FullNameElement getReceivableAccountRef() {
        return receivableAccountRef;
    }

    public void setReceivableAccountRef(FullNameElement receivableAccountRef) {
        this.receivableAccountRef = receivableAccountRef;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public FullNameElement getPaymentMethodRef() {
        return paymentMethodRef;
    }

    public void setPaymentMethodRef(FullNameElement paymentMethodRef) {
        this.paymentMethodRef = paymentMethodRef;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public FullNameElement getDepositAccountRef() {
        return depositAccountRef;
    }

    public void setDepositAccountRef(FullNameElement depositAccountRef) {
        this.depositAccountRef = depositAccountRef;
    }

    public TxnData getTxnData() {
        return txnData;
    }

    public void setTxnData(TxnData txnData) {
        this.txnData = txnData;
    }
}
