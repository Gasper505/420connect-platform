package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ReceivePaymentAddRq")
public class PaymentReceivedRequest extends Data {

    @JacksonXmlProperty(localName = "ReceivePaymentAdd")
    private PaymentReceivedData paymentReceivedData;

    public PaymentReceivedData getPaymentReceivedData() {
        return paymentReceivedData;
    }

    public void setPaymentReceivedData(PaymentReceivedData paymentReceivedData) {
        this.paymentReceivedData = paymentReceivedData;
    }
}
