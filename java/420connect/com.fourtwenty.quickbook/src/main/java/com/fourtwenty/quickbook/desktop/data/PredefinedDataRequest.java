package com.fourtwenty.quickbook.desktop.data;

public class PredefinedDataRequest {
    private String name;
    private String accountName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
