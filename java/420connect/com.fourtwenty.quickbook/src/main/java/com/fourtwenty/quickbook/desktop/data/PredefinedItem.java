package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PredefinedItem {
    @JacksonXmlProperty(localName = "Price")
    private double price;

    @JacksonXmlProperty(localName = "AccountRef")
    private FullNameElement accountRef;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public FullNameElement getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(FullNameElement accountRef) {
        this.accountRef = accountRef;
    }
}
