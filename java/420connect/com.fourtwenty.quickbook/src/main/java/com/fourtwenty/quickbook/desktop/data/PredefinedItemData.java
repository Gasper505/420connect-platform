package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PredefinedItemData extends Data {
    @JacksonXmlProperty(localName = "Name")
    private String name;

    @JacksonXmlProperty(localName = "SalesOrPurchase")
    @JacksonXmlElementWrapper(useWrapping = false)
    private PredefinedItem predefinedItem;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PredefinedItem getPredefinedItem() {
        return predefinedItem;
    }

    public void setPredefinedItem(PredefinedItem predefinedItem) {
        this.predefinedItem = predefinedItem;
    }
}
