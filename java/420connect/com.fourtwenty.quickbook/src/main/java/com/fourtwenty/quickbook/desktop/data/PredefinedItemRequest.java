package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ItemServiceAddRq")
public class PredefinedItemRequest extends Data {

    @JacksonXmlProperty(localName = "ItemServiceAdd")
    private PredefinedItemData predefinedItemData;

    public PredefinedItemData getPredefinedItemData() {
        return predefinedItemData;
    }

    public void setPredefinedItemData(PredefinedItemData predefinedItemData) {
        this.predefinedItemData = predefinedItemData;
    }
}
