package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ProductCategoryData extends Data {

    @JacksonXmlProperty(localName = "ListID")
    private String listId;

    @JacksonXmlProperty(localName = "EditSequence")
    private String editSequence;

    @JacksonXmlProperty(localName = "Name")
    private String categoryName;

    @JacksonXmlProperty(localName = "SalesPrice")
    private double cost;

    @JacksonXmlProperty(localName = "IncomeAccountRef")
    private FullNameElement incomeAccount;

    @JacksonXmlProperty(localName = "COGSAccountRef")
    private FullNameElement cogsAccount;

    @JacksonXmlProperty(localName = "AssetAccountRef")
    private FullNameElement assetAccount;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public FullNameElement getIncomeAccount() {
        return incomeAccount;
    }

    public void setIncomeAccount(FullNameElement incomeAccount) {
        this.incomeAccount = incomeAccount;
    }

    public FullNameElement getCogsAccount() {
        return cogsAccount;
    }

    public void setCogsAccount(FullNameElement cogsAccount) {
        this.cogsAccount = cogsAccount;
    }

    public FullNameElement getAssetAccount() {
        return assetAccount;
    }

    public void setAssetAccount(FullNameElement assetAccount) {
        this.assetAccount = assetAccount;
    }
}
