package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ItemInventoryModRq")
public class ProductCategoryEditRequest extends ProductCategoryRequest {
    @JacksonXmlProperty(localName = "ItemInventoryMod")
    private ProductCategoryData productCategoryData;

    public ProductCategoryData getProductCategoryData() {
        return productCategoryData;
    }

    public void setProductCategoryData(ProductCategoryData productCategoryData) {
        this.productCategoryData = productCategoryData;
    }
}
