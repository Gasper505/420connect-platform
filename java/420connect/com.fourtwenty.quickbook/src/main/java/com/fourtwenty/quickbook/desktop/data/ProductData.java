package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.math.BigDecimal;

public class ProductData extends Data {

    @JacksonXmlProperty(localName = "ListID")
    private String listId;

    @JacksonXmlProperty(localName = "EditSequence")
    private String editSequence;

    @JacksonXmlProperty(localName = "Name")
    private String productName;

    @JacksonXmlProperty(localName = "SalesDesc")
    private String salesDesc;

    @JacksonXmlProperty(localName = "SalesPrice")
    private double cost;

    @JacksonXmlProperty(localName = "IncomeAccountRef")
    private FullNameElement incomeAccount;

    @JacksonXmlProperty(localName = "COGSAccountRef")
    private FullNameElement cogsAccount;

    @JacksonXmlProperty(localName = "PrefVendorRef")
    private FullNameElement vendorId;

    @JacksonXmlProperty(localName = "AssetAccountRef")
    private FullNameElement assetAccount;

    @JacksonXmlProperty(localName = "QuantityOnHand")
    private Double quantityOnHand;

    @JacksonXmlProperty(localName = "TotalValue")
    private BigDecimal cogs;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public FullNameElement getVendorId() {
        return vendorId;
    }

    public void setVendorId(FullNameElement vendorId) {
        this.vendorId = vendorId;
    }

    public Double getQuantityOnHand() {
        return quantityOnHand;
    }

    public void setQuantityOnHand(Double quantityOnHand) {
        this.quantityOnHand = quantityOnHand;
    }

    public FullNameElement getAssetAccount() {
        return assetAccount;
    }

    public void setAssetAccount(FullNameElement assetAccount) {
        this.assetAccount = assetAccount;
    }

    public FullNameElement getCogsAccount() {
        return cogsAccount;
    }

    public void setCogsAccount(FullNameElement cogsAccount) {
        this.cogsAccount = cogsAccount;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public FullNameElement getIncomeAccount() {
        return incomeAccount;
    }

    public void setIncomeAccount(FullNameElement incomeAccount) {
        this.incomeAccount = incomeAccount;
    }

    public String getSalesDesc() {
        return salesDesc;
    }

    public void setSalesDesc(String salesDesc) {
        this.salesDesc = salesDesc;
    }

    public BigDecimal getCogs() {
        return cogs;
    }

    public void setCogs(BigDecimal cogs) {
        this.cogs = cogs;
    }
}
