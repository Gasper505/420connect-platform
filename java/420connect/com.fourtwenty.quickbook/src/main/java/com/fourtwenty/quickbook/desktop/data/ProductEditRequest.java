package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ItemInventoryModRq")
public class ProductEditRequest extends ProductRequest {

    @JacksonXmlProperty(localName = "ItemInventoryMod")
    private ProductData productData;

    @Override
    public ProductData getProductData() {
        return productData;
    }

    @Override
    public void setProductData(ProductData productData) {
        this.productData = productData;
    }
}
