package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderData extends Data {
    @JacksonXmlProperty(localName = "TxnID")
    private String txnId;

    @JacksonXmlProperty(localName = "EditSequence")
    private String editSequence;

    @JacksonXmlProperty(localName = "VendorRef")
    private FullNameElement vendorId;

    @JacksonXmlProperty(localName = "TxnDate")
    private String createDate;

    @JacksonXmlProperty(localName = "RefNumber")
    private String poNumber;

    @JacksonXmlProperty(localName = "PurchaseOrderLineAdd")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<POProductItem> poProductItems = new ArrayList<>();

    @JacksonXmlProperty(localName = "PurchaseOrderLineMod")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<POProductItem> poProductModifiedItems = new ArrayList<>();

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public FullNameElement getVendorId() {
        return vendorId;
    }

    public void setVendorId(FullNameElement vendorId) {
        this.vendorId = vendorId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public List<POProductItem> getPoProductItems() {
        return poProductItems;
    }

    public void setPoProductItems(List<POProductItem> poProductItems) {
        this.poProductItems = poProductItems;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public List<POProductItem> getPoProductModifiedItems() {
        return poProductModifiedItems;
    }

    public void setPoProductModifiedItems(List<POProductItem> poProductModifiedItems) {
        this.poProductModifiedItems = poProductModifiedItems;
    }
}
