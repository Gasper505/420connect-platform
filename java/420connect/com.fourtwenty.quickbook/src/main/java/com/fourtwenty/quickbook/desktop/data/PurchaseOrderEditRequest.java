package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "PurchaseOrderModRq")
public class PurchaseOrderEditRequest extends PurchaseOrderRequest {

    @JacksonXmlProperty(localName = "PurchaseOrderMod")
    private PurchaseOrderData orderData;

    public PurchaseOrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(PurchaseOrderData orderData) {
        this.orderData = orderData;
    }
}
