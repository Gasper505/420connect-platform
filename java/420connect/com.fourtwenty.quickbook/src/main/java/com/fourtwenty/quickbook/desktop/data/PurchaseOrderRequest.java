package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "PurchaseOrderAddRq")
public class PurchaseOrderRequest extends Data {

    @JacksonXmlProperty(localName = "PurchaseOrderAdd")
    private PurchaseOrderData orderData;

    public PurchaseOrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(PurchaseOrderData orderData) {
        this.orderData = orderData;
    }
}
