package com.fourtwenty.quickbook.desktop.data;

public class QBResponseData {
    private String qbRef;
    private String editSequence;
    private String listId;
    private String txnId;

    public String getQbRef() {
        return qbRef;
    }

    public void setQbRef(String qbRef) {
        this.qbRef = qbRef;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }
}
