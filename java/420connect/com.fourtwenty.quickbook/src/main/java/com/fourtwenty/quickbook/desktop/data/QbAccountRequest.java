package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;

@JacksonXmlRootElement(localName = "AccountQueryRq")
public class QbAccountRequest extends Data {

    @JacksonXmlProperty(localName = "AccountType")
    private QBDesktopAccounts.AccountType accountType;

    public QBDesktopAccounts.AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(QBDesktopAccounts.AccountType accountType) {
        this.accountType = accountType;
    }
}
