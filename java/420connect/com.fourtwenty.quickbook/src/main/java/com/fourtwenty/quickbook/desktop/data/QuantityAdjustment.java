package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.math.BigDecimal;

public class QuantityAdjustment {

    @JacksonXmlProperty(localName = "NewQuantity")
    private BigDecimal newQuantity;
    @JacksonXmlProperty(localName = "QuantityDifference")
    private BigDecimal quantityDifference;
    @JacksonXmlProperty(localName = "NewValue")
    private BigDecimal newValue = BigDecimal.ZERO;

    public BigDecimal getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(BigDecimal newQuantity) {
        this.newQuantity = newQuantity;
    }

    public BigDecimal getNewValue() {
        return newValue;
    }

    public void setNewValue(BigDecimal newValue) {
        this.newValue = newValue;
    }

    public BigDecimal getQuantityDifference() {
        return quantityDifference;
    }

    public void setQuantityDifference(BigDecimal quantityDifference) {
        this.quantityDifference = quantityDifference;
    }
}
