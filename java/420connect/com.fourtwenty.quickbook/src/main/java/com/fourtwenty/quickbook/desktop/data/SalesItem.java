package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SalesItem {

    @JacksonXmlProperty(localName = "TxnLineID")
    private String txnLineID;

    @JacksonXmlProperty(localName = "ItemRef")
    private FullNameElement productId;

    @JacksonXmlProperty(localName = "Quantity")
    private double quantity;

    @JacksonXmlProperty(localName = "Rate")
    private double price;

    @JacksonXmlProperty(localName = "SalesTaxCodeRef")
    private FullNameElement salesTaxCode;

    public String getTxnLineID() {
        return txnLineID;
    }

    public void setTxnLineID(String txnLineID) {
        this.txnLineID = txnLineID;
    }

    public FullNameElement getProductId() {
        return productId;
    }

    public void setProductId(FullNameElement productId) {
        this.productId = productId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public FullNameElement getSalesTaxCode() {
        return salesTaxCode;
    }

    public void setSalesTaxCode(FullNameElement salesTaxCode) {
        this.salesTaxCode = salesTaxCode;
    }
}
