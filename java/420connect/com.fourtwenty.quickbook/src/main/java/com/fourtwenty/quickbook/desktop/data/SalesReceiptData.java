package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class SalesReceiptData extends Data {

    @JacksonXmlProperty(localName = "CustomerRef")
    private FullNameElement customerInfo;

    @JacksonXmlProperty(localName = "TxnDate")
    private String transactionDate;

    @JacksonXmlProperty(localName = "RefNumber")
    private String transactionNo;

    @JacksonXmlProperty(localName = "BillAddress")
    private Address address;

    @JacksonXmlProperty(localName = "PaymentMethodRef")
    private FullNameElement paymentType;

    @JacksonXmlProperty(localName = "SalesReceiptLineAdd")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<SalesItem> orderItems = new ArrayList<>();

    public FullNameElement getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(FullNameElement customerInfo) {
        this.customerInfo = customerInfo;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public FullNameElement getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(FullNameElement paymentType) {
        this.paymentType = paymentType;
    }

    public List<SalesItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<SalesItem> orderItems) {
        this.orderItems = orderItems;
    }
}
