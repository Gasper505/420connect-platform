package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "SalesReceiptAddRq")
public class SalesReceiptRequest extends Data {

    @JacksonXmlProperty(localName = "SalesReceiptAdd")
    private SalesReceiptData receiptData;

    public SalesReceiptData getReceiptData() {
        return receiptData;
    }

    public void setReceiptData(SalesReceiptData receiptData) {
        this.receiptData = receiptData;
    }
}
