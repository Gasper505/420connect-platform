package com.fourtwenty.quickbook.desktop.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class VendorData extends Data {

    @JacksonXmlProperty(localName = "ListID")
    private String qbRef;
    @JacksonXmlProperty(localName = "EditSequence")
    private String editSequence;
    @JacksonXmlProperty(localName = "Name")
    private String name;
    @JacksonXmlProperty(localName = "VendorAddress")
    private Address address;
    @JacksonXmlProperty(localName = "Phone")
    private String phone;
    @JacksonXmlProperty(localName = "Email")
    private String email;
    @JacksonXmlProperty(localName = "VendorTypeRef")
    private FullNameElement vendorTypeRef;


    public String getQbRef() {
        return qbRef;
    }

    public void setQbRef(String qbRef) {
        this.qbRef = qbRef;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public FullNameElement getVendorTypeRef() {
        return vendorTypeRef;
    }

    public void setVendorTypeRef(FullNameElement vendorTypeRef) {
        this.vendorTypeRef = vendorTypeRef;
    }
}
