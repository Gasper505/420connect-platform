package com.fourtwenty.quickbook.desktop.data;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "VendorModRq")
public class VendorEditRequest extends VendorRequest {


    @JacksonXmlProperty(localName = "VendorMod")
    private VendorData vendorData;

    @Override
    public VendorData getVendorData() {
        return vendorData;
    }

    @Override
    public void setVendorData(VendorData vendorData) {
        this.vendorData = vendorData;
    }
}
