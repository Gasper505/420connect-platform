package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.repositories.QbAccountRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.desktop.converters.AccountConvertor;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.converters.QbAccountConvertor;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class AccountXMLService {

    public String convertIntoAccountXML(String companyID, String shopID, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QbAccountRepository qbAccountRepository, QuickbookCustomEntities qbCustomEntities) {
        String accountQuery = "";

        if (qbCustomEntities.getQbCustomAccount() == QuickbookCustomEntities.QbCustomAccount.Custom) {
            List<QBDesktopAccounts> qbAccountList = qbAccountRepository.getQBAccountListWihoutAccountId(companyID, shopID);

            if (qbAccountList != null && qbAccountList.size() > 0) {
                QuickbookSyncDetails details = null;
                List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
                statuses.add(QuickbookSyncDetails.Status.Pending);

                List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Account, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
                if (syncDetailsByStatus != null && syncDetailsByStatus.size() > 0) {
                    for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
                        details = detailsByStatus;
                        break;
                    }
                }

                AccountConvertor accountConvertor = null;
                accountConvertor = new AccountConvertor(qbAccountList, null);

                if (details != null) {
                    details.setStatus(QuickbookSyncDetails.Status.Inprogress);
                    details.setTotalRecords(qbAccountList.size());
                    quickbookSyncDetailsRepository.update(companyID, details.getId(), details);
                }

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(accountConvertor));
                accountQuery = qbxmlConverter.getXMLString();

            }
        } else {
            QbAccountConvertor qbAccountConvertor = new QbAccountConvertor(null, null);
            quickbookSyncDetailsRepository.saveQuickbookEntity(0,0,0,companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Account, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull);

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(qbAccountConvertor));
            accountQuery = qbxmlConverter.getXMLString();
        }

        return accountQuery;
    }
}
