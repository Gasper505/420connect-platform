package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import org.apache.commons.lang.StringUtils;

public class AddAccount {

    String createAccount() {
        String bankAccount = "<AccountAddRq>" +
                "<AccountAdd>" +
                "<NAME>New Blaze Account</NAME>" +
                "<AccountType ></AccountType>" +
                "</AccountAdd>" +
                "</AccountAddRq>";
        return bankAccount;
    }

    public boolean checkAccountsIsExist(String companyId, String shopId, QuickbookCustomEntities quickbookCustomEntities, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        boolean status = true;
        QuickBookAccountDetails accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, quickbookCustomEntities.getId());
        if (accountDetails == null) {
            status = false;
        } else {
            if (StringUtils.isBlank(accountDetails.getInventory())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getSuppliesAndMaterials())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getSales())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getChecking())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getCc_clearance())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getClearance())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getPayableAccount())) {
                status = false;
            }
            if (StringUtils.isBlank(accountDetails.getReceivable())) {
                status = false;
            }
        }
        return status;
    }
}
