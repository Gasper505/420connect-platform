package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import org.apache.commons.lang3.StringUtils;

public class AddPredefinedData {

    public String addPredefinedData(CompanyFeatures.AppTarget appTarget) {
        String vendorType = vendorType();
        String custom = customData();
        String paymentOption = paymentOptions(appTarget);
        return vendorType + custom + paymentOption;
    }

    String customData() {
        String qbCustomData = "";
        qbCustomData = qbCustomData + "<DataExtDefAddRq>" +
                "   <DataExtDefAdd>" +
                "       <OwnerID >0</OwnerID>" +
                "       <DataExtName >Sku</DataExtName>" +
                "       <DataExtType >STR255TYPE</DataExtType>" +
                "       <AssignToObject >Item</AssignToObject>" +
                "       <DataExtListRequire >true</DataExtListRequire>" +
                "       <DataExtTxnRequire >true</DataExtTxnRequire>" +
                "       <DataExtFormatString >Sku</DataExtFormatString>" +
                "   </DataExtDefAdd>" +
                "</DataExtDefAddRq>";

        /*qbCustomData = qbCustomData + " <DataExtDefAddRq>" +
                "   <DataExtDefAdd>" +
                "       <OwnerID >0</OwnerID>" +
                "       <DataExtName >Customer Id</DataExtName>" +
                "       <DataExtType >STR255TYPE</DataExtType>" +
                "       <AssignToObject >Customer</AssignToObject>" +
                "       <DataExtListRequire >true</DataExtListRequire>" +
                "       <DataExtTxnRequire >true</DataExtTxnRequire>" +
                "       <DataExtFormatString >CustomerId</DataExtFormatString>" +
                "   </DataExtDefAdd>" +
                "</DataExtDefAddRq>";

        qbCustomData = qbCustomData + " <DataExtDefAddRq>" +
                "   <DataExtDefAdd>" +
                "       <OwnerID >0</OwnerID>" +
                "       <DataExtName >Vendor Id</DataExtName>" +
                "       <DataExtType >STR255TYPE</DataExtType>" +
                "       <AssignToObject >Vendor</AssignToObject>" +
                "       <DataExtListRequire >true</DataExtListRequire>" +
                "       <DataExtTxnRequire >true</DataExtTxnRequire>" +
                "       <DataExtFormatString >VendorId</DataExtFormatString>" +
                "   </DataExtDefAdd>" +
                "</DataExtDefAddRq>";*/

        return qbCustomData;
    }

    String vendorType() {
        String vendorData = "";
        for (Vendor.CompanyType value : Vendor.CompanyType.values()) {
            vendorData += "<VendorTypeAddRq>\n" +
                    "   <VendorTypeAdd> \n" +
                    "       <Name >" + value.toString() + "</Name> \n" +
                    "   </VendorTypeAdd>\n" +
                    "</VendorTypeAddRq> \n";
        }
        return vendorData;
    }

    String paymentOptions(CompanyFeatures.AppTarget appTarget) {
        String paymentOptionData = "";
        if (CompanyFeatures.AppTarget.Retail == appTarget) {
            for (Cart.PaymentOption value : Cart.PaymentOption.values()) {
                if (value.toString().equalsIgnoreCase("Cash")) {
                    paymentOptionData += paymentQuery(value.toString(), "Cash");
                }
                if (value.toString().equalsIgnoreCase("Credit")) {
                    paymentOptionData += paymentQuery(value.toString(), "OtherCreditCard");
                }
                if (value.toString().equalsIgnoreCase("Check")) {
                    paymentOptionData += paymentQuery(value.toString(), "Check");
                }
                if (value.toString().equalsIgnoreCase("Hypur")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
                if (value.toString().equalsIgnoreCase("Split")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
                if (value.toString().equalsIgnoreCase("StoreCredit")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
                if (value.toString().equalsIgnoreCase("Linx")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
                if (value.toString().equalsIgnoreCase("CashlessATM")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
            }
        } else if (CompanyFeatures.AppTarget.Distribution == appTarget) {
            for (PaymentsReceived.PaymentType value : PaymentsReceived.PaymentType.values()) {
                if (value.toString().equalsIgnoreCase("CREDIT")) {
                    paymentOptionData += paymentQuery(value.toString(), "OtherCreditCard");
                }
                if (value.toString().equalsIgnoreCase("CASH")) {
                    paymentOptionData += paymentQuery(value.toString(), "Cash");
                }
                if (value.toString().equalsIgnoreCase("CHEQUE")) {
                    paymentOptionData += paymentQuery(value.toString(), "Check");
                }
                if (value.toString().equalsIgnoreCase("DEBIT")) {
                    paymentOptionData += paymentQuery(value.toString(), "DebitCard");
                }
                if (value.toString().equalsIgnoreCase("OTHER")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
                if (value.toString().equalsIgnoreCase("ACH_TRANSFER")) {
                    paymentOptionData += paymentQuery(value.toString(), "Other");
                }
            }
        } else {
            paymentOptionData += StringUtils.EMPTY;
        }
        return paymentOptionData;
    }

    public String paymentQuery(String name, String type) {
        String payment = "<PaymentMethodAddRq> \n" +
                "   <PaymentMethodAdd> \n" +
                "       <Name >" + name + "</Name> \n" +
                "       <PaymentMethodType >" + type +"</PaymentMethodType> \n" +
                "   </PaymentMethodAdd> \n" +
                "</PaymentMethodAddRq> \n";
        return payment;
    }

}
