package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.desktop.converters.BillConverter;
import com.fourtwenty.quickbook.desktop.converters.ProductCategoryBillConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.request.ProductCategoryPORequest;
import com.fourtwenty.quickbook.desktop.wrappers.BillPaymentReceivedWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.BillWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryBillWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;

public class BillXMLService {

    public String billXMLRequest(String companyID, String shopID, ProductRepository productRepository, VendorRepository vendorRepository, PurchaseOrderRepository purchaseOrderRepository, ShipmentBillRepository shipmentBillRepository,
                                 QbDesktopOperationRepository qbDesktopOperationRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ErrorLogsRepository errorLogsRepository, QuickbookCustomEntities quickbookCustomEntities, ProductCategoryRepository productCategoryRepository) {
        String billQuery = "";
        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.Bill);

        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            return billQuery;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Bill, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = syncDetails.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Bill, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        int start = 0;
        int limit = 1000;

        List<String> poIds = new ArrayList<>();
        if (quickbookCustomEntities.getSyncTime() > 0) {
            SearchResult<PurchaseOrder> poByLimitsWithoutBillRef = purchaseOrderRepository.getPOByLimitsWithoutBillRef(companyID, shopID, quickbookCustomEntities.getSyncTime(), start, limit, PurchaseOrder.class);
            for (PurchaseOrder value : poByLimitsWithoutBillRef.getValues()) {
                poIds.add(value.getId());
            }
        }

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(companyID, shopID);
        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyID, shopID);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyID);
        SearchResult<BillWrapper> billWrappers = new SearchResult<>();
        if (quickbookCustomEntities.getSyncTime() > 0 && quickbookCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
            billWrappers = shipmentBillRepository.getShipmentBillByPOWithoutQbRef(companyID, shopID, quickbookCustomEntities.getSyncTime(), poIds, BillWrapper.class);
        }

        SearchResult<ProductCategoryBillWrapper> productCategoryBillWrappers = new SearchResult<>();
        if (quickbookCustomEntities.getSyncTime() > 0 && quickbookCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
            productCategoryBillWrappers = shipmentBillRepository.getShipmentBillByPOWithoutQbRef(companyID, shopID, quickbookCustomEntities.getSyncTime(), poIds, ProductCategoryBillWrapper.class);
        }

        if (billWrappers.getValues() != null && billWrappers.getValues().size() > 0) {
            billQuery = this.prepareShipmentBillForSyncIndividual(companyID, shopID, billWrappers, vendorHashMap, productHashMap, qbDesktopOperationRef, quickbookSyncDetailsRepository, qbDesktopCurrentSyncRepository, shipmentBillRepository, errorLogsRepository);
        }

        if (productCategoryBillWrappers.getValues() != null && productCategoryBillWrappers.getValues().size() > 0) {
            billQuery = this.prepareShipmentBillForSalesByProductCategory(companyID, shopID, productCategoryBillWrappers, vendorHashMap, productHashMap, qbDesktopOperationRef, quickbookSyncDetailsRepository, qbDesktopCurrentSyncRepository, productCategoryHashMap);
        }

        return billQuery;
    }

    private String prepareShipmentBillForSyncIndividual(String companyID, String shopID, SearchResult<BillWrapper> billWrappers, HashMap<String, Vendor> vendorHashMap, HashMap<String, Product> productHashMap, QBDesktopOperation qbDesktopOperationRef,
                                                        QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ShipmentBillRepository shipmentBillRepository, ErrorLogsRepository errorLogsRepository) {
        String billQuery = "";
        List<BillWrapper> successBillWrappers = new ArrayList<>();
        List<BillWrapper> errorBillWrappers = new ArrayList<>();

        if (billWrappers.getValues().size() > 0) {
            for (BillWrapper value : billWrappers.getValues()) {
                boolean addStatus = false;
                if (value != null) {
                    if (StringUtils.isNotBlank(value.getVendorId())) {
                        Vendor vendor = vendorHashMap.get(value.getVendorId());
                        if (vendor != null && vendor.getQbMapping() !=null && vendor.getQbMapping().size() > 0) {
                            for (QBDataMapping dataMapping : vendor.getQbMapping()) {
                                if (dataMapping != null && dataMapping.getShopId().equals(shopID) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                        && StringUtils.isNotBlank(dataMapping.getQbListId())) {

                                    value.setVendorQbRef(dataMapping.getQbDesktopRef());
                                    value.setVendorListId(dataMapping.getQbListId());
                                    value.setAddress(vendor.getAddress());
                                    addStatus = true;
                                }
                            }
                        }
                    }

                    if (value.getPoProductRequest().size() > 0) {
                        HashMap<String, Product> availableProductMap = new HashMap<>();
                        for (POProductRequest poProductRequest : value.getPoProductRequest()) {
                            if (StringUtils.isNotBlank(poProductRequest.getProductId())) {
                                Product product = productHashMap.get(poProductRequest.getProductId());
                                if (product != null && StringUtils.isNotBlank(product.getQbDesktopItemRef()) && StringUtils.isNotBlank(product.getQbListId())) {
                                    if (!availableProductMap.containsKey(product.getId())) {
                                        availableProductMap.put(product.getId(), product);
                                    }
                                }
                            }

                        }
                        value.setProductMap(availableProductMap);
                    }

                    if (addStatus) {
                        if (value.getProductMap().size() != 0) {
                            successBillWrappers.add(value);
                        } else {
                            errorBillWrappers.add(value);
                        }
                    } else {
                        errorBillWrappers.add(value);
                    }
                }
            }
        }

        if (successBillWrappers.size() > 0) {
            Map<String, String> billCurrentSyncDataMap = new HashMap<>();
            for (BillWrapper billWrapper : successBillWrappers) {
                billCurrentSyncDataMap.put(billWrapper.getId(), billWrapper.getShipmentBillNumber());
            }

            BillConverter billConverter = null;
            try {
                billConverter = new BillConverter(successBillWrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(billConverter.getmList().size(), 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Bill, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(billConverter));
            billQuery = qbxmlConverter.getXMLString();

            saveCurrentSync(companyID, shopID, billCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Bill);
        }

        if (errorBillWrappers.size() > 0) {
            long currentTime = DateTime.now().getMillis();
            String errorMsg = "Shipment's bill product are not synchronized";
            for (BillWrapper wrapper : errorBillWrappers) {
                shipmentBillRepository.updateDesktopBillQbErrorAndTime(companyID, shopID, wrapper.getShipmentBillNumber(), Boolean.TRUE, currentTime);
                saveErrorLogs(companyID, shopID, "500", errorMsg, null, QuickbookSyncDetails.QuickbookEntityType.Bill, errorLogsRepository);
            }
        }
        return billQuery;
    }

    private String prepareShipmentBillForSalesByProductCategory(String companyID, String shopID, SearchResult<ProductCategoryBillWrapper> billWrappers, HashMap<String, Vendor> vendorHashMap, HashMap<String, Product> productHashMap, QBDesktopOperation qbDesktopOperationRef,
                                                                QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, HashMap<String, ProductCategory> productCategoryHashMap) {

        String billQuery = "";
        List<ProductCategoryBillWrapper> successBillWrappers = new ArrayList<>();
        for (ProductCategoryBillWrapper billWrapper : billWrappers.getValues()) {
            if (billWrapper != null) {
                if (StringUtils.isNotBlank(billWrapper.getVendorId())) {
                    Vendor vendor = vendorHashMap.get(billWrapper.getVendorId());
                    if (vendor != null && vendor.getQbMapping() !=null && vendor.getQbMapping().size() > 0) {
                        for (QBDataMapping dataMapping : vendor.getQbMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopID) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getQbListId())) {

                                billWrapper.setVendorQbRef(dataMapping.getQbDesktopRef());
                                billWrapper.setVendorListId(dataMapping.getQbListId());
                                billWrapper.setAddress(vendor.getAddress());
                            }
                        }
                    }
                }

                Map<String, ProductCategoryPORequest> categoryPORequestMap = new HashMap<>();
                if (billWrapper.getPoProductRequest().size() > 0) {
                    for (POProductRequest poProductRequest : billWrapper.getPoProductRequest()) {
                        if (StringUtils.isNotBlank(poProductRequest.getProductId())) {
                            Product product = productHashMap.get(poProductRequest.getProductId());
                            ProductCategory productCategory = null;
                            if (StringUtils.isNotBlank(product.getCategoryId())) {
                                productCategory = productCategoryHashMap.get(product.getCategoryId());
                            }

                            if (productCategory != null) {
                                if (categoryPORequestMap.containsKey(productCategory.getId())) {
                                    ProductCategoryPORequest request = categoryPORequestMap.get(productCategory.getId());

                                    double quantity = request.getQuantity();
                                    BigDecimal totalCost = request.getTotalCost();
                                    double newQty = quantity + poProductRequest.getReceivedQuantity().doubleValue();
                                    BigDecimal newCost = totalCost.add(poProductRequest.getUnitPrice());
                                    request.setQuantity(newQty);
                                    request.setTotalCost(newCost);

                                    categoryPORequestMap.put(productCategory.getId(), request);

                                } else {
                                    ProductCategoryPORequest request = new ProductCategoryPORequest();
                                    request.setCategoryRef(productCategory.getQbDesktopRef());
                                    request.setCategoryListId(productCategory.getQbListId());
                                    request.setQuantity(poProductRequest.getReceivedQuantity().doubleValue());
                                    request.setTotalCost(poProductRequest.getUnitPrice());

                                    categoryPORequestMap.put(productCategory.getId(), request);
                                }
                            }
                        }
                    }
                }

                List<ProductCategoryPORequest> requests = new ArrayList<>();
                for (String categoryId : categoryPORequestMap.keySet()) {
                    ProductCategoryPORequest request = categoryPORequestMap.get(categoryId);
                    requests.add(request);
                }

                billWrapper.setRequests(requests);
                successBillWrappers.add(billWrapper);
            }
        }

        int total = 0;
        Map<String, String> billCurrentSyncDataMap = new HashMap<>();
        if (successBillWrappers.size() > 0) {
            for (ProductCategoryBillWrapper billWrapper : successBillWrappers) {
                billCurrentSyncDataMap.put(billWrapper.getId(), billWrapper.getShipmentBillNumber());
            }
            ProductCategoryBillConverter billConverter = null;
            try {
                billConverter = new ProductCategoryBillConverter(successBillWrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = billConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(billConverter));
            billQuery = qbxmlConverter.getXMLString();
        }

        if (total > 0) {
            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Bill, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyID, shopID, billCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Bill);
        }

        return billQuery;
    }
}
