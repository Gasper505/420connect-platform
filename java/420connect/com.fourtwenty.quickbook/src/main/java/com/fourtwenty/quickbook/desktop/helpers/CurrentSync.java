package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;

import java.util.Map;

public class CurrentSync {

    public static void saveCurrentSync(String companyId, String shopId, Map<String, String> dataReferenceIdsMap, String referenceId, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType entityType) {
        QbDesktopCurrentSync qbDesktopCurrentSync = new QbDesktopCurrentSync();
        qbDesktopCurrentSync.prepare(companyId);
        qbDesktopCurrentSync.setShopId(shopId);
        qbDesktopCurrentSync.setReferenceIdsMap(dataReferenceIdsMap);
        qbDesktopCurrentSync.setQuickBookEntityType(entityType);
        qbDesktopCurrentSync.setSyncReference(referenceId);
        qbDesktopCurrentSyncRepository.save(qbDesktopCurrentSync);
    }

    public static void updateCurrentSync(QbDesktopCurrentSync currentSync,  Map<String, String> dataReferenceIdsMap, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        currentSync.setReferenceIdsMap(dataReferenceIdsMap);
        qbDesktopCurrentSyncRepository.update(currentSync.getCompanyId(), currentSync.getId(), currentSync);
    }
}
