package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBUniqueSequence;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.soap.qbxml.QBXMLParser;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.CustomerQueryRqType;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.desktop.converters.CustomerConverter;
import com.fourtwenty.quickbook.desktop.converters.CustomerEditConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.MemberWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;


public class CustomerXMLService implements QBXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerXMLService.class);

    private String companyID;
    private String shopID;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private MemberRepository memberRepository;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private QBUniqueSequenceRepository qbUniqueSequenceRepository;

    public CustomerXMLService(String companyID, String shopID, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, MemberRepository memberRepository, QbDesktopOperationRepository qbDesktopOperationRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, QBUniqueSequenceRepository qbUniqueSequenceRepository) {
        this.companyID = companyID;
        this.shopID = shopID;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.memberRepository = memberRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.qbUniqueSequenceRepository = qbUniqueSequenceRepository;
    }

    private List<MemberWrapper> prepareUniqueSequenceData(String companyId, String shopId, List<MemberWrapper> memberWrapperList, QBUniqueSequenceRepository qbUniqueSequenceRepository, boolean checkStatus) {
        Map<String, Integer> dbUniqueSequenceMap = new HashMap<>();
        List<QBUniqueSequence> qbUniqueSequenceByType = qbUniqueSequenceRepository.getQbUniqueSequenceByType(companyId, shopId);
        for (QBUniqueSequence qbUniqueSequence : qbUniqueSequenceByType) {
            if (qbUniqueSequence != null) {
                dbUniqueSequenceMap.put(qbUniqueSequence.getName(), qbUniqueSequence.getCurrentSequence());
            }
        }

        Map<String, Integer> updateUniqueSequenceMap = new HashMap<>();
        Map<String, Integer> newUniqueSequenceMap = new HashMap<>();
        for (MemberWrapper memberWrapper : memberWrapperList) {
            if (checkStatus) {
                String customerName = memberWrapper.getFirstName().trim();
                if (StringUtils.isNotBlank(memberWrapper.getLastName())) {
                    customerName = customerName.concat(" ").concat(memberWrapper.getLastName().trim());
                }
                if (StringUtils.isBlank(customerName)) {
                    String email = memberWrapper.getEmail();
                    String[] emailSplit = email.split("@");
                    customerName = emailSplit[0];
                }

                customerName = customerName.replaceAll("[^0-9a-zA-Z ]", "").trim();
                if (customerName.length() > 35) {
                    customerName = memberWrapper.getFirstName();
                }
                customerName = customerName.toLowerCase();

                if (dbUniqueSequenceMap.containsKey(customerName)) {
                    Integer integer = dbUniqueSequenceMap.get(customerName);
                    String name = customerName + "_" + (integer + 1);
                    memberWrapper.setQbMemberName(name);
                    dbUniqueSequenceMap.put(customerName, (integer + 1));
                    updateUniqueSequenceMap.put(customerName, (integer + 1));
                } else {
                    if (newUniqueSequenceMap.containsKey(customerName)) {
                        Integer integer = newUniqueSequenceMap.get(customerName);
                        String name = customerName + "_" + (integer + 1);
                        memberWrapper.setQbMemberName(name);
                        newUniqueSequenceMap.put(customerName, (integer + 1));
                    } else {
                        memberWrapper.setQbMemberName(customerName);
                        newUniqueSequenceMap.put(customerName, 0);
                    }
                }
            }

            if (!checkStatus) {
                if (memberWrapper.getQbMapping() != null && memberWrapper.getQbMapping().size() > 0) {
                    for (QBDataMapping dataMapping : memberWrapper.getQbMapping()) {
                        if (dataMapping != null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                && StringUtils.isNotBlank(dataMapping.getQbListId()) && StringUtils.isNotBlank(dataMapping.getEditSequence())) {
                            memberWrapper.setQbMemberName(dataMapping.getQbDesktopRef());
                            memberWrapper.setQbRef(dataMapping.getQbDesktopRef());
                            memberWrapper.setListId(dataMapping.getQbListId());
                            memberWrapper.setQbEditSequence(dataMapping.getEditSequence());
                        }
                    }
                }
            }
        }

        if (newUniqueSequenceMap.size() > 0) {
            List<QBUniqueSequence> qbUniqueSequences = new ArrayList<>();
            for (String name : newUniqueSequenceMap.keySet()) {
                QBUniqueSequence qbUniqueSequence = new QBUniqueSequence();
                qbUniqueSequence.prepare(companyId);
                qbUniqueSequence.setShopId(shopId);
                qbUniqueSequence.setName(name);
                qbUniqueSequence.setCurrentSequence(newUniqueSequenceMap.get(name));

                qbUniqueSequences.add(qbUniqueSequence);
            }
            qbUniqueSequenceRepository.save(qbUniqueSequences);
        }

        if (updateUniqueSequenceMap.size() > 0) {
            for (String name : updateUniqueSequenceMap.keySet()) {
                qbUniqueSequenceRepository.updateQbUniqueSequence(companyId, shopId, name, updateUniqueSequenceMap.get(name));
            }
        }
        return memberWrapperList;
    }

    @Override
    public String generatePushRequest() {
        StringBuilder qbCustomerXMLRequest = new StringBuilder();

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.Customer);
        if(qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Customer sync is not available for shop {}, skipping", shopID);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickbookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        ArrayList<QuickbookSyncDetails> detailsList = Lists.newArrayList(quickbookSyncDetailList);

        int failedCount = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        long latestFailTime = 0;
        for (QuickbookSyncDetails details : detailsList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }


        List<MemberWrapper> memberList = new ArrayList<>();
        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Customer);
        if (latestQbDesktopCurrentSync != null) {
            memberList = memberRepository.getMembersByLimitsWithQBError(companyID, shopID, latestQbDesktopCurrentSync.getCreated(), MemberWrapper.class);
        }

        int start = 0;
        int limit = 1000 - memberList.size();

        List<MemberWrapper> membersByLimits = memberRepository.getMembersByLimitsWithoutQbDesktopRef(companyID, start, limit, MemberWrapper.class);
        memberList.addAll(membersByLimits);

        limit = 1000- memberList.size();
        if (limit > 0) {
            List<MemberWrapper> checkQBMappingVendors = memberRepository.getMembersByLimitsWithoutQbDesktopRef(companyID, shopID, start, limit, MemberWrapper.class);
            memberList.addAll(checkQBMappingVendors);
        }

        int total = 0;
        Map<String, String> memberCurrentSyncDataMap = new HashMap<>();
        if (memberList.size() > 0) {
            memberList = prepareUniqueSequenceData(companyID, shopID, memberList, qbUniqueSequenceRepository, true);
            for (MemberWrapper member : memberList) {
                if (member != null) {
                    memberCurrentSyncDataMap.put(member.getId(), member.getQbMemberName());
                }
            }

            CustomerConverter customerConverter = null;
            try {
                customerConverter = new CustomerConverter(memberList, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = customerConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(customerConverter));
            qbCustomerXMLRequest.append(qbxmlConverter.getXMLString());
        }

        List<MemberWrapper> modifiedMembers = new ArrayList<>();
        int modifiedLimit = 1000 - memberList.size();
        if (modifiedLimit > 0) {
            if (detailsList.size() > 0 && quickbookSyncDetails != null) {
                List<MemberWrapper> qbExistingMember = memberRepository.getQBExistingMember(companyID, shopID, start, modifiedLimit, quickbookSyncDetails.getStartTime(), MemberWrapper.class);
                if (qbExistingMember.size() > 0) {
                    modifiedMembers.addAll(qbExistingMember);
                }

                if (modifiedMembers != null && modifiedMembers.size() > 0) {
                    modifiedMembers = prepareUniqueSequenceData(companyID, shopID, modifiedMembers, qbUniqueSequenceRepository, false);
                    for (MemberWrapper member : modifiedMembers) {
                        if (member != null) {
                            memberCurrentSyncDataMap.put(member.getId(), member.getQbMemberName());
                        }
                    }

                    CustomerEditConverter customerEditConverter = null;
                    try {
                        customerEditConverter = new CustomerEditConverter(modifiedMembers, qbDesktopOperationRef.getQbDesktopFieldMap());
                        total = total + customerEditConverter.getmList().size();
                    } catch (Exception e) {
                        LOGGER.error("Error while initiating Customer edit converter : " + e.getMessage());
                    }

                    QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(customerEditConverter));
                    qbCustomerXMLRequest.append(qbxmlConverter.getXMLString());
                }
            }
        }

        if (total > 0) {
            // Save sync details
            QuickbookSyncDetails details = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Customer, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyID, shopID, memberCurrentSyncDataMap, details.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Customer);

        }

        return qbCustomerXMLRequest.toString();
    }

    @Override
    public String generatePullRequest(String timeZone) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        List<QuickbookSyncDetails> detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 1);

        if (detailsByStatus == null || detailsByStatus.isEmpty()) {
            // Maybe this is first time for pull, so let's use push
            detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            if (detailsByStatus == null || detailsByStatus.isEmpty()) {
                return "";
            }
        }

        CustomerQueryRqType customerQueryRqType = new CustomerQueryRqType();
        customerQueryRqType.setFromModifiedDate(DateUtil.convertToISO8601(detailsByStatus.get(0).getStartTime(), timeZone));
        customerQueryRqType.setToModifiedDate(DateUtil.convertToISO8601(DateTime.now().getMillis(), timeZone));
        // Save sync details
        quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Customer, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull);
        return QBXMLParser.parseRequest(customerQueryRqType, CustomerQueryRqType.class, "CustomerQueryRq");
    }
}
