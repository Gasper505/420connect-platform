package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.converters.ProductCategoryPurchaseOrderConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.request.ProductCategoryPORequest;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryPOWrapper;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;

public class DailyPurchaseOrder implements QBXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DailyPurchaseOrder.class);

    private String companyId;
    private String shopId;
    private QuickbookCustomEntities entities;
    private PurchaseOrderRepository purchaseOrderRepository;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private VendorRepository vendorRepository;
    private ProductRepository productRepository;
    private ProductCategoryRepository productCategoryRepository;
    private ShipmentBillRepository shipmentBillRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;

    public DailyPurchaseOrder(String companyId, String shopId, QuickbookCustomEntities entities, PurchaseOrderRepository purchaseOrderRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QbDesktopOperationRepository qbDesktopOperationRepository, VendorRepository vendorRepository, ProductRepository productRepository, ProductCategoryRepository productCategoryRepository, ShipmentBillRepository shipmentBillRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.entities = entities;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.vendorRepository = vendorRepository;
        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.shipmentBillRepository = shipmentBillRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
    }

    @Override
    public String generatePushRequest() {
        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.PurchaseOrder);
        if(qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Item sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        StringBuilder purchaseOrderXML = new StringBuilder();

        Iterable<ProductCategoryPOWrapper> purchaseOrders = null;
        long startTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        if (entities.getSyncTime() > 0) {
            if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
                if (details != null) {
                    startTime = details.getEndTime();
                } else  {
                    startTime = DateTime.now().withTimeAtStartOfDay().getMillis();
                }
                purchaseOrders = purchaseOrderRepository.getPurchaseOrderByLimitsWithDateTime(companyId, shopId, startTime, endTime, ProductCategoryPOWrapper.class);
            } else {
                if (details != null) {
                    startTime = details.getEndTime();
                } else {
                    startTime = entities.getSyncTime();
                }
                purchaseOrders = purchaseOrderRepository.getPurchaseOrderByLimitsWithDateTime(companyId, shopId, startTime, endTime, ProductCategoryPOWrapper.class);
            }
        }

        int total = 0;
        List<ProductCategoryPOWrapper> purchaseOrdersList = null;
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyId);
        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyId, shopId);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(companyId, shopId);

        if (purchaseOrders != null) {
            purchaseOrdersList = Lists.newArrayList(purchaseOrders);
        }

        Map<String, String> purchaseOrderCurrentSyncDataMap = new HashMap<>();
        List<ProductCategoryPOWrapper> wrappers = null;
        if (purchaseOrdersList != null && purchaseOrdersList.size() > 0) {
            wrappers = this.preparePurchaseOrder(purchaseOrdersList, vendorHashMap, productHashMap, productCategoryHashMap);
        }

        if (wrappers != null && wrappers.size() > 0) {
            for (ProductCategoryPOWrapper orderWrapper : wrappers) {
                purchaseOrderCurrentSyncDataMap.put(orderWrapper.getId(), orderWrapper.getPoNumber());
            }

            ProductCategoryPurchaseOrderConverter productCategoryPurchaseOrderConverter = null;
            try {
                productCategoryPurchaseOrderConverter = new ProductCategoryPurchaseOrderConverter(wrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = productCategoryPurchaseOrderConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(productCategoryPurchaseOrderConverter));
            purchaseOrderXML.append(qbxmlConverter.getXMLString());
        }

        if (total > 0) {
            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyId, shopId, purchaseOrderCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Expenses);
        }

        return purchaseOrderXML.toString();
    }

    private List<ProductCategoryPOWrapper> preparePurchaseOrder(List<ProductCategoryPOWrapper> purchaseOrdersList, HashMap<String, Vendor> vendorHashMap, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> productCategoryHashMap) {
        List<String> poIds = new ArrayList<>();
        for (ProductCategoryPOWrapper wrapper : purchaseOrdersList) {
            poIds.add(wrapper.getId());
        }

        List<ProductCategoryPOWrapper> purchaseOrderWrapperList = new ArrayList<>();
        HashMap<String, ShipmentBill> poAsShipmentBillHashMap = shipmentBillRepository.listShipmentBillByPOAsMap(companyId, shopId, poIds);
        for (ProductCategoryPOWrapper purchaseOrder : purchaseOrdersList) {
            ShipmentBill bill = poAsShipmentBillHashMap.get(purchaseOrder.getId());
            if (bill != null && bill.getPaymentStatus().equals(ShipmentBill.PaymentStatus.PAID)) {
                if (StringUtils.isNotBlank(purchaseOrder.getVendorId())) {
                    Vendor vendor = vendorHashMap.get(purchaseOrder.getVendorId());
                    if (vendor != null && vendor.getQbMapping() != null && vendor.getQbMapping().size() > 0) {
                        for (QBDataMapping dataMapping : vendor.getQbMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getQbListId())) {
                                purchaseOrder.setQbVendorListId(dataMapping.getQbListId());
                                purchaseOrder.setQbVendorRef(dataMapping.getQbDesktopRef());
                            }
                        }
                    }
                }

                Map<String, ProductCategoryPORequest> categoryPORequestMap = new HashMap<>();
                List<POProductRequest> poProductRequestList = purchaseOrder.getPoProductRequestList();
                for (POProductRequest poProductRequest : poProductRequestList) {
                    if (StringUtils.isNotBlank(poProductRequest.getProductId())) {
                        Product product = productHashMap.get(poProductRequest.getProductId());
                        ProductCategory productCategory = null;
                        if (StringUtils.isNotBlank(product.getCategoryId())) {
                            productCategory = productCategoryHashMap.get(product.getCategoryId());
                        }
                        if (productCategory != null) {
                            if (categoryPORequestMap.containsKey(productCategory.getId())) {
                                ProductCategoryPORequest request = categoryPORequestMap.get(productCategory.getId());

                                double quantity = request.getQuantity();
                                BigDecimal totalCost = request.getTotalCost();
                                double newQty = quantity + poProductRequest.getReceivedQuantity().doubleValue();
                                BigDecimal newCost = totalCost.add(poProductRequest.getUnitPrice());
                                request.setQuantity(newQty);
                                request.setTotalCost(newCost);

                                categoryPORequestMap.put(productCategory.getId(), request);

                            } else {
                                ProductCategoryPORequest request = new ProductCategoryPORequest();
                                request.setCategoryRef(productCategory.getQbDesktopRef());
                                request.setCategoryListId(productCategory.getQbListId());
                                request.setQuantity(poProductRequest.getReceivedQuantity().doubleValue());
                                request.setTotalCost(poProductRequest.getUnitPrice());

                                categoryPORequestMap.put(productCategory.getId(), request);
                            }
                        }
                    }
                }

                List<ProductCategoryPORequest> requests = new ArrayList<>();
                for (String categoryId : categoryPORequestMap.keySet()) {
                    ProductCategoryPORequest request = categoryPORequestMap.get(categoryId);
                    requests.add(request);
                }

                purchaseOrder.setRequests(requests);
                purchaseOrderWrapperList.add(purchaseOrder);
            }
        }
        return purchaseOrderWrapperList;
    }

    @Override
    public String generatePullRequest(String timeZone) {
        return null;
    }
}
