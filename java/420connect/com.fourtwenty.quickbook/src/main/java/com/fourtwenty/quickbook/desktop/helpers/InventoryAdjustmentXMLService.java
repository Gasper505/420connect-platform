package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.quickbook.desktop.converters.InventoryAdjustmentConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.InventoryAdjustmentWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.ProductWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;

public class InventoryAdjustmentXMLService {

    private static final Logger LOG = LoggerFactory.getLogger(InventoryAdjustmentXMLService.class);

    public String dbAdjustmentXMLRequest(String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                         QbDesktopOperationRepository qbDesktopOperationRepository,
                                         QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository,
                                         ProductRepository productRepository, InventoryRepository inventoryRepository, BatchQuantityRepository batchQuantityRepository,
                                         InventoryActionRepository inventoryActionRepository, QuickbookCustomEntities qbCustomEntities, ProductBatchRepository batchRepository,
                                         PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {

        return createAdjustmentXML(companyId, shopId, quickbookSyncDetailsRepository, qbDesktopOperationRepository,
                qbDesktopCurrentSyncRepository, productRepository, inventoryRepository, batchQuantityRepository, inventoryActionRepository, qbCustomEntities, batchRepository, prepackageRepository, productPrepackageQuantityRepository, productWeightToleranceRepository, quickBookAccountDetailsRepository);

    }

    private String createAdjustmentXML(String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                       QbDesktopOperationRepository qbDesktopOperationRepository,
                                       QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository,
                                       ProductRepository productRepository, InventoryRepository inventoryRepository,BatchQuantityRepository batchQuantityRepository,
                                       InventoryActionRepository inventoryActionRepository,
                                       QuickbookCustomEntities qbCustomEntities, ProductBatchRepository batchRepository,
                                       PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.InventoryAdjustment);

        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickbookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        ArrayList<QuickbookSyncDetails> detailsList = Lists.newArrayList(quickbookSyncDetailList);

        int failedCount = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        long latestFailTime = 0;
        for (QuickbookSyncDetails details : detailsList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
                failedCount += 1;
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }


        String inventoryAdjustmentQuery = "";

        List<ProductWrapper> updatedWrapperList = new ArrayList<>();
        List<ProductWrapper> newWrapperList = new ArrayList<>();
        List<ProductWrapper> newProductWrapperList = new ArrayList<>();
        List<ProductWrapper> products = new ArrayList<>();
        int start = 0;
        int total = 0;
        long dateTime = 0;

        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment);
        if (latestQbDesktopCurrentSync != null) {
            products = productRepository.getProductsByLimitsWithQbQuantityError(companyId, shopId, latestQbDesktopCurrentSync.getCreated(), ProductWrapper.class);
        }


        int limit = 1000 - products.size();
        if (quickbookSyncDetails == null) {
            newWrapperList = productRepository.getProductsLimitsWithoutQuantitySynced(companyId, shopId, start, limit, ProductWrapper.class);
            products.addAll(newWrapperList);

            limit = limit - products.size();
            newProductWrapperList = productRepository.getNewProductsLimitsWithoutQuantitySynced(companyId, shopId, start, limit, ProductWrapper.class);
            products.addAll(newProductWrapperList);
        } else {
            dateTime = quickbookSyncDetails.getStartTime();

            Iterable<InventoryOperation> updatedOperations = inventoryActionRepository.getActionsByTime(companyId, shopId, dateTime);
            List<ObjectId> updatedProducts = new ArrayList<>();
            for (InventoryOperation operation : updatedOperations) {
                if (StringUtils.isNotBlank(operation.getProductId()) && ObjectId.isValid(operation.getProductId())) {
                    updatedProducts.add(new ObjectId(operation.getProductId()));
                }
            }

            if (updatedProducts.size() > 0) {
                updatedWrapperList = productRepository.getQBExistProducts(companyId, updatedProducts, ProductWrapper.class);
                products.addAll(updatedWrapperList);
            }

            limit = limit - updatedWrapperList.size();
            if (limit > 0) {
                newWrapperList = productRepository.getProductsLimitsWithoutQuantitySynced(companyId, shopId, start, limit, ProductWrapper.class);
                products.addAll(newWrapperList);
            }

            limit = limit - products.size();
            if (limit > 0) {
                newProductWrapperList = productRepository.getNewProductsLimitsWithoutQuantitySynced(companyId, shopId, start, limit, ProductWrapper.class);
                products.addAll(newProductWrapperList);
            }
        }

        if (products.size() > 0) {
            Set<String> newProductIds = new HashSet<>();
            Set<String> updatedProductIds = new HashSet<>();
            if (newWrapperList.size() > 0) {
                for (ProductWrapper product : newWrapperList) {
                    newProductIds.add(product.getId());
                }
            }
            if (updatedWrapperList.size() > 0) {
                for (ProductWrapper product : updatedWrapperList) {
                    updatedProductIds.add(product.getId());
                }
            }

            List<String> productIds = new ArrayList<>();
            for (ProductWrapper product : products) {
                productIds.add(product.getId());
            }

            List<InventoryOperation.SourceType> sourceTypes = new ArrayList<>();
            sourceTypes.add(InventoryOperation.SourceType.None);
            sourceTypes.add(InventoryOperation.SourceType.Transfer);
            sourceTypes.add(InventoryOperation.SourceType.Reconciliation);
            sourceTypes.add(InventoryOperation.SourceType.ProductBatch);
            sourceTypes.add(InventoryOperation.SourceType.StockReset);
            sourceTypes.add(InventoryOperation.SourceType.Product);
            sourceTypes.add(InventoryOperation.SourceType.System);
            sourceTypes.add(InventoryOperation.SourceType.DerivedProduct);
            sourceTypes.add(InventoryOperation.SourceType.BundleProduct);


            List<InventoryOperation> inventoryOperations = new ArrayList<>();
            HashMap<String, List<BatchQuantity>> batchQuantityHashMap = new HashMap<>();
            Map<String, BigDecimal> prepackageQuantityMap = new HashMap<>();
            if (newProductIds.size() > 0) {
                List<String> inventoryIds = new ArrayList<>();
                HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(companyId, shopId);
                for (String inventoryId : inventoryHashMap.keySet()) {
                    Inventory inventory = inventoryHashMap.get(inventoryId);
                    if (inventory != null) {
                     inventoryIds.add(inventoryId);
                    }
                }

                batchQuantityHashMap = batchQuantityRepository.getBatchQuantityByInventoryForProduct(companyId, shopId, inventoryIds, productIds);
                prepackageQuantityMap = prepackageQuantityForProducts(companyId, shopId, productIds, prepackageRepository, productPrepackageQuantityRepository, productWeightToleranceRepository);
            }

            if (updatedProductIds.size() > 0) {
                Iterable<InventoryOperation> updatedInventoryOperations = inventoryActionRepository.getOperationsBySourceTypeForProducts(companyId, shopId, dateTime, sourceTypes, Lists.newArrayList(updatedProductIds));
                inventoryOperations.addAll(Lists.newArrayList(updatedInventoryOperations));
            }

            if (products.size() == updatedProductIds.size() && inventoryOperations.size() == 0) {
                return StringUtils.EMPTY;
            }

            HashMap<String, ProductBatch> allBatchMap = batchRepository.getBatchesForProductsMap(companyId, new ArrayList<>(productIds));
            HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
            for (ProductBatch batch : allBatchMap.values()) {
                ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
                if (oldBatch == null) {
                    recentBatchMap.put(batch.getProductId(), batch);
                } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                    recentBatchMap.put(batch.getProductId(), batch);
                }
            }

            List<InventoryAdjustmentWrapper> wrappers = new ArrayList<>();
            this.prepareInventoryAdjustment(companyId, shopId, products, inventoryOperations, wrappers, qbCustomEntities, recentBatchMap, batchQuantityHashMap, prepackageQuantityMap, quickBookAccountDetailsRepository);

            Map<String, String> productCurrentSyncDataMap = new HashMap<>();
            for (InventoryAdjustmentWrapper wrapper : wrappers) {
                productCurrentSyncDataMap.put(wrapper.getProductId(), wrapper.getProductQBDesktopRef());
            }

            InventoryAdjustmentConverter converter = null;
            try {
                converter = new InventoryAdjustmentConverter(wrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
                total += converter.getmList().size();
            } catch (Exception e) {
                LOG.error("Error while creating adjustment converter " + e.getMessage());
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(converter));
            inventoryAdjustmentQuery = qbxmlConverter.getXMLString();

            if (total > 0) {
                QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(converter.getmList().size(), 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
                saveCurrentSync(companyId, shopId, productCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment);
            }
        }
        return inventoryAdjustmentQuery;
    }

    private void prepareInventoryAdjustment(String companyId, String shopId, List<ProductWrapper> products,
                                            Iterable<InventoryOperation> newInventoryOperations,
                                            List<InventoryAdjustmentWrapper> wrappers,
                                            QuickbookCustomEntities qbCustomEntities, HashMap<String, ProductBatch> recentBatchMap, HashMap<String, List<BatchQuantity>> batchQuantityHashMap, Map<String, BigDecimal> prepackageQuantityMap, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        // Required accounts for quickBook
        List<String> referenceIds = new ArrayList<>();
        referenceIds.add(qbCustomEntities.getId());
        for (ProductWrapper wrapper : products) {
            referenceIds.add(wrapper.getId());
        }

        List<QuickBookAccountDetails> accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceIds(companyId, shopId, referenceIds);
        Map<String, QuickBookAccountDetails> accountDetailsMap = new HashMap<>();
        if (accountDetails.size() > 0) {
            for (QuickBookAccountDetails accountDetail : accountDetails) {
                accountDetailsMap.put(accountDetail.getReferenceId(), accountDetail);
            }
        }

        Map<String, List<InventoryOperation>> invOperationMap = new HashMap<>();
        for (InventoryOperation operation : newInventoryOperations) {
            invOperationMap.putIfAbsent(operation.getProductId(), new ArrayList<>());
            List<InventoryOperation> operations = invOperationMap.get(operation.getProductId());
            operations.add(operation);
            invOperationMap.put(operation.getProductId(), operations);
        }


        for (ProductWrapper product : products) {
            BigDecimal quantity = new BigDecimal(0);
            if (batchQuantityHashMap.containsKey(product.getId())) {
                List<BatchQuantity> batchQuantities = batchQuantityHashMap.get(product.getId());
                if (batchQuantities.size() > 0) {
                    for (BatchQuantity batchQuantity : batchQuantities) {
                        quantity = quantity.add(batchQuantity.getQuantity());
                    }
                }
                if (prepackageQuantityMap.containsKey(product.getId())) {
                    BigDecimal prepackageQuantity = prepackageQuantityMap.get(product.getId());
                    quantity = quantity.add(prepackageQuantity);
                }
            } else {
                List<InventoryOperation> operations = invOperationMap.get(product.getId());
                if (operations != null && operations.size() > 0) {
                    quantity = this.prepareQuantity(operations);
                }
            }

            BigDecimal costPerUnit = BigDecimal.ZERO;
            ProductBatch newestBatch = recentBatchMap.get(product.getId());
            if (newestBatch != null) {
                costPerUnit = newestBatch.getFinalUnitCost();
            }
            BigDecimal cogs = costPerUnit.multiply(quantity);

            InventoryAdjustmentWrapper adjustment = new InventoryAdjustmentWrapper();
            adjustment.setProductId(product.getId());
            adjustment.setProductQBDesktopRef(product.getQbDesktopItemRef());
            adjustment.setProductListId(product.getQbListId());

            QuickBookAccountDetails entitiesAccountDetails = accountDetailsMap.get(qbCustomEntities.getId());
            QuickBookAccountDetails details = accountDetailsMap.get(product.getId());
            if (details != null && details.isStatus()) {
                if (StringUtils.isNotBlank(details.getInventory())) {
                    adjustment.setAccountName(details.getInventory());
                } else {
                    adjustment.setAccountName(entitiesAccountDetails.getInventory());
                }
            } else {
                adjustment.setAccountName(entitiesAccountDetails.getInventory());
            }

            adjustment.setQuantity(quantity);
            adjustment.setValue(cogs.abs());
            adjustment.setQuantitySynced(product.isQbQuantitySynced());

            wrappers.add(adjustment);
        }

    }

    private BigDecimal prepareQuantity(List<InventoryOperation> operations) {
        BigDecimal quantity = BigDecimal.ZERO;
        for (InventoryOperation operation : operations) {
            quantity = quantity.add(operation.getQuantity());
        }
        return quantity;
    }

    private Map<String, BigDecimal> prepackageQuantityForProducts(String companyId, String shopId, List<String> productIds, PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository){
        Map<String, BigDecimal> productQuantityMap = new HashMap<>();
        Set<ObjectId> toleranceIds = new HashSet<>();
        Iterable<Prepackage> prepackagesForProducts = prepackageRepository.getPrepackagesForProducts(companyId, shopId, productIds, Prepackage.class);
        Map<String, Prepackage> prepackageMap = new HashMap<>();
        for (Prepackage prepackage : prepackagesForProducts) {
            if (!prepackageMap.containsKey(prepackage.getId())) {
                prepackageMap.put(prepackage.getId(), prepackage);
                if (StringUtils.isNotBlank(prepackage.getToleranceId())) {
                    toleranceIds.add(new ObjectId(prepackage.getToleranceId()));
                }
            }
        }

        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = productWeightToleranceRepository.listAsMap(companyId, Lists.newArrayList(toleranceIds));

        Iterable<ProductPrepackageQuantity> quantitiesForProducts = productPrepackageQuantityRepository.getQuantitiesForProducts(companyId, shopId, productIds);
        for (ProductPrepackageQuantity quantitiesForProduct : quantitiesForProducts) {
            if (quantitiesForProduct != null) {
                int quantity = quantitiesForProduct.getQuantity();
                BigDecimal unitValue = new BigDecimal(0);
                Prepackage prepackage = prepackageMap.get(quantitiesForProduct.getPrepackageId());
                if (prepackage != null && StringUtils.isNotBlank(prepackage.getToleranceId())) {
                    ProductWeightTolerance productWeightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                    if (productWeightTolerance != null) {
                        unitValue = productWeightTolerance.getUnitValue();
                    }
                }

                BigDecimal totalQuantity = unitValue.multiply(new BigDecimal(quantity));
                if (productQuantityMap.containsKey(quantitiesForProduct.getProductId())) {
                    BigDecimal oldQuantity = productQuantityMap.get(quantitiesForProduct.getProductId());
                    BigDecimal newQuantity = oldQuantity.add(totalQuantity);
                    productQuantityMap.put(quantitiesForProduct.getProductId(), newQuantity);
                } else {
                    productQuantityMap.put(quantitiesForProduct.getProductId(), totalQuantity);
                }
            }
        }
        return productQuantityMap;
    }
}
