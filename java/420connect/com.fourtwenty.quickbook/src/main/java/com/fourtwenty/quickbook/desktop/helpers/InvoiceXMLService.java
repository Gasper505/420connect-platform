package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.converters.InvoiceConverter;
import com.fourtwenty.quickbook.desktop.converters.InvoiceEditConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.InvoiceWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;

public class InvoiceXMLService {

    private static final int TOTAL_LIMIT = 1000;

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceXMLService.class);

    private String companyId;
    private String shopId;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private QuickbookCustomEntities customEntities;
    private InvoiceRepository invoiceRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private VendorRepository vendorRepository;
    private ProductRepository productRepository;
    private PrepackageRepository prepackageRepository;
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private ProductWeightToleranceRepository weightToleranceRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    public InvoiceXMLService(String companyId, String shopId, QbDesktopOperationRepository qbDesktopOperationRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntities customEntities, InvoiceRepository invoiceRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, VendorRepository vendorRepository, ProductRepository productRepository, PrepackageRepository prepackageRepository, PrepackageProductItemRepository prepackageProductItemRepository, ProductWeightToleranceRepository weightToleranceRepository, ErrorLogsRepository errorLogsRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.customEntities = customEntities;
        this.invoiceRepository = invoiceRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.vendorRepository = vendorRepository;
        this.productRepository = productRepository;
        this.prepackageRepository = prepackageRepository;
        this.prepackageProductItemRepository = prepackageProductItemRepository;
        this.weightToleranceRepository = weightToleranceRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
    }

    public String generatePushRequest() {

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.Invoice);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOG.warn("Invoice sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Invoice, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Invoice, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        List<Invoice.InvoiceStatus> invoiceStatuses = new ArrayList<>();
        invoiceStatuses.add(Invoice.InvoiceStatus.IN_PROGRESS);
        invoiceStatuses.add(Invoice.InvoiceStatus.SENT);
        invoiceStatuses.add(Invoice.InvoiceStatus.COMPLETED);

        int start = 0;
        int limit = TOTAL_LIMIT;
        List<InvoiceWrapper> invoiceList = new ArrayList<>();
        if (customEntities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
            long startTime;
            long endTime = DateTime.now().getMillis();

            if (details != null) {
                startTime = details.getEndTime();
            } else {
                startTime = DateTime.now().withTimeAtStartOfDay().getMillis();
            }

            invoiceList = invoiceRepository.getLimitedInvoiceWithoutQbRef(companyId, shopId, startTime, endTime, invoiceStatuses, start, limit, InvoiceWrapper.class);
        } else {
            QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Invoice);
            if (latestQbDesktopCurrentSync != null) {
                invoiceList = invoiceRepository.getInvoiceByLimitsWithQBError(companyId, shopId, latestQbDesktopCurrentSync.getCreated(), invoiceStatuses, start, limit, InvoiceWrapper.class);
            }

            limit = TOTAL_LIMIT - invoiceList.size();

            List<InvoiceWrapper> invoices = invoiceRepository.getLimitedInvoiceWithoutQbRef(companyId, shopId, customEntities.getSyncTime(), invoiceStatuses, start, limit, InvoiceWrapper.class);
            invoiceList.addAll(invoices);
        }

        Map<String, String> currentSyncDataMap = new HashMap<>();
        List<InvoiceWrapper> finalInvoices = new ArrayList<>();
        this.prepareInvoices(companyId, shopId, invoiceList, finalInvoices, vendorRepository, productRepository,
                prepackageRepository, prepackageProductItemRepository, weightToleranceRepository, invoiceRepository,
                errorLogsRepository, currentSyncDataMap, Boolean.FALSE, quickBookAccountDetailsRepository);

        StringBuilder request = new StringBuilder();

        int total = 0;
        if (finalInvoices.size() > 0) {
            InvoiceConverter converter = null;
            try {
                converter = new InvoiceConverter(finalInvoices, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = total + converter.getmList().size();
            } catch (Exception e) {
                LOG.error("Error while create QB request for invoice : " + e.getMessage());
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(converter));
            request.append(qbxmlConverter.getXMLString());
        }

        int modifiedLimit = TOTAL_LIMIT - finalInvoices.size();
        if (modifiedLimit > 0) {
            if (details != null) {
                List<InvoiceWrapper> modifiedInvoice = invoiceRepository.getQBExistInvoice(companyId, shopId, start, modifiedLimit, details.getStartTime(), invoiceStatuses, InvoiceWrapper.class);

                if (modifiedInvoice.size() > 0) {
                    List<InvoiceWrapper> finalModifiedInvoices = new ArrayList<>();

                    this.prepareInvoices(companyId, shopId, modifiedInvoice, finalModifiedInvoices, vendorRepository, productRepository,
                            prepackageRepository, prepackageProductItemRepository, weightToleranceRepository, invoiceRepository,
                            errorLogsRepository, currentSyncDataMap, Boolean.TRUE, quickBookAccountDetailsRepository);

                    InvoiceEditConverter editConverter = null;
                    try {
                        editConverter = new InvoiceEditConverter(finalModifiedInvoices, qbDesktopOperationRef.getQbDesktopFieldMap());
                        total = total + editConverter.getmList().size();
                    } catch (Exception e) {
                        LOG.error("Error while create QB request for invoice : " + e.getMessage());
                    }

                    QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(editConverter));
                    request.append(qbxmlConverter.getXMLString());
                }
            }
        }

        if (total > 0) {
            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId,
                    DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.Invoice,
                    QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyId, shopId, currentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository,
                    QuickbookSyncDetails.QuickbookEntityType.Invoice);
        }

        return request.toString();
    }

    private void prepareInvoices(String companyId, String shopId, List<InvoiceWrapper> invoiceList, List<InvoiceWrapper> finalInvoices,
                                 VendorRepository vendorRepository, ProductRepository productRepository, PrepackageRepository prepackageRepository,
                                 PrepackageProductItemRepository prepackageProductItemRepository, ProductWeightToleranceRepository weightToleranceRepository,
                                 InvoiceRepository invoiceRepository, ErrorLogsRepository errorLogsRepository, Map<String, String> currentSyncDataMap, Boolean isModified, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        List<ObjectId> vendorIds = new ArrayList<>();
        List<ObjectId> productIds = new ArrayList<>();
        Set<ObjectId> cartPrepackageIds = new HashSet<>();
        Set<ObjectId> prepackageItemIds = new HashSet<>();
        for (InvoiceWrapper invoice : invoiceList) {
            if (StringUtils.isNotBlank(invoice.getCustomerId()) && ObjectId.isValid(invoice.getCustomerId())) {
                vendorIds.add(new ObjectId(invoice.getCustomerId()));
            }

            if (invoice.getCart() == null || invoice.getCart().getItems().size() == 0) {
                continue;
            }

            for (OrderItem item : invoice.getCart().getItems()) {
                if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
                if (StringUtils.isNotBlank(item.getPrepackageItemId()) && ObjectId.isValid(item.getPrepackageItemId())) {
                    prepackageItemIds.add(new ObjectId(item.getPrepackageItemId()));
                }
                if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                    cartPrepackageIds.add(new ObjectId(item.getPrepackageId()));
                }
            }
        }

        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(companyId, vendorIds);
        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, productIds);
        HashMap<String, PrepackageProductItem> prepackageItemMap = prepackageProductItemRepository.listAsMap(companyId, Lists.newArrayList(prepackageItemIds));

        Set<ObjectId> prepackageIds = new HashSet<>();
        for (String itemId : prepackageItemMap.keySet()) {
            PrepackageProductItem prepackageProductItem = prepackageItemMap.get(itemId);
            prepackageIds.add(new ObjectId(prepackageProductItem.getPrepackageId()));
        }
        prepackageIds.addAll(cartPrepackageIds);

        HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(companyId, Lists.newArrayList(prepackageIds));
        HashMap<String, ProductWeightTolerance> weightToleranceMap = weightToleranceRepository.listAsMap();

        QuickBookAccountDetails entitiesAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, customEntities.getId());

        long currentTime = DateTime.now().getMillis();
        HashMap<String, Product> invoiceProductMap;
        for (InvoiceWrapper invoice : invoiceList) {
            invoiceProductMap = new HashMap<>();
            String productErrorMessage = "At least one product of invoice is not synced with quickBook for " + invoice.getInvoiceNumber();
            String vendorErrorMessage = "Customer of invoice is not synced with quickBook for " + invoice.getInvoiceNumber();

            Vendor vendor = vendorMap.get(invoice.getCustomerId());
            QBDataMapping qbDataMapping = null;
            if (vendor != null && vendor.getQbCustomerMapping() != null && vendor.getQbCustomerMapping().size() > 0) {
                for (QBDataMapping dataMapping : vendor.getQbCustomerMapping()) {
                    if (dataMapping != null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                            && StringUtils.isNotBlank(dataMapping.getQbListId())) {
                        qbDataMapping = dataMapping;
                    }
                }

            }

            if (vendor == null || qbDataMapping == null) {
                this.addInvoiceErrorLog(companyId, shopId, invoice, Boolean.TRUE, currentTime, invoiceRepository, vendorErrorMessage, errorLogsRepository);
                continue;
            }

            for (OrderItem item : invoice.getCart().getItems()) {
                Product product = productMap.get(item.getProductId());

                if (product == null || StringUtils.isBlank(product.getQbDesktopItemRef()) || StringUtils.isBlank(product.getQbListId())) {
                    this.addInvoiceErrorLog(companyId, shopId, invoice, Boolean.TRUE, currentTime, invoiceRepository, productErrorMessage, errorLogsRepository);
                    continue;
                }

                invoiceProductMap.put(product.getId(), product);

                this.preparePrepackageQuantity(item, prepackageMap, weightToleranceMap, prepackageItemMap);
            }

            if (invoiceProductMap.size() == 0) {
                this.addInvoiceErrorLog(companyId, shopId, invoice, Boolean.TRUE, currentTime, invoiceRepository, productErrorMessage, errorLogsRepository);
                continue;
            }

            invoice.setVendorQbRef(qbDataMapping.getQbDesktopRef());
            invoice.setVendorQbListId(qbDataMapping.getQbListId());
            invoice.setVendorName(vendor.getName());
            invoice.setProductMap(invoiceProductMap);
            invoice.setReceivableAccount(entitiesAccountDetails.getReceivable());

            finalInvoices.add(invoice);
            currentSyncDataMap.put(invoice.getId(), invoice.getInvoiceNumber());
        }
    }

    private void preparePrepackageQuantity(OrderItem item, HashMap<String, Prepackage> prepackageMap, HashMap<String, ProductWeightTolerance> weightToleranceMap, HashMap<String, PrepackageProductItem> prepackageItemMap) {
        if (StringUtils.isBlank(item.getPrepackageItemId()) && StringUtils.isBlank(item.getPrepackageId())) {
            return;
        }

        Prepackage prepackage = null;
        if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
            PrepackageProductItem prepackageProductItem = prepackageItemMap.get(item.getPrepackageItemId());

            if (prepackageProductItem != null) {
                prepackage = prepackageMap.get(prepackageProductItem.getPrepackageId());
            }
        } else if (StringUtils.isNotBlank(item.getPrepackageId())) {
            prepackage = prepackageMap.get(item.getPrepackageId());
        }

        if (prepackage == null) {
            return;
        }

        ProductWeightTolerance productWeightTolerance = weightToleranceMap.get(prepackage.getToleranceId());
        if (productWeightTolerance == null) {
            return;
        }

        BigDecimal newQuantity = item.getQuantity().multiply(productWeightTolerance.getUnitValue());
        item.setQuantity(newQuantity);
    }

    private void addInvoiceErrorLog(String companyId, String shopId, InvoiceWrapper invoice, Boolean errored, long currentTime, InvoiceRepository invoiceRepository, String message, ErrorLogsRepository errorLogsRepository) {
        invoiceRepository.updateTransactionQbErrorAndTime(companyId, shopId, invoice.getId(), errored, currentTime);
        saveErrorLogs(companyId, shopId, "500", message, null, QuickbookSyncDetails.QuickbookEntityType.Invoice, errorLogsRepository);
    }

}
