package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.soap.qbxml.QBXMLParser;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.ItemInventoryQueryRqType;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.desktop.converters.ProductConverter;
import com.fourtwenty.quickbook.desktop.converters.ProductEditConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.ProductWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;

public class ItemXMLService  implements QBXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemXMLService.class);

    private String companyID;
    private String shopID;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private ProductRepository productRepository;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private VendorRepository vendorRepository;
    private QuickbookCustomEntities quickbookCustomEntities;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ProductBatchRepository productBatchRepository;
    private BatchQuantityRepository batchQuantityRepository;
    private PrepackageRepository prepackageRepository;
    private ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    private ProductWeightToleranceRepository productWeightToleranceRepository;
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    public ItemXMLService(String companyID, String shopID, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ProductRepository productRepository, QbDesktopOperationRepository qbDesktopOperationRepository, VendorRepository vendorRepository, QuickbookCustomEntities quickbookCustomEntities, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ProductBatchRepository productBatchRepository, BatchQuantityRepository batchQuantityRepository, PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository, PrepackageProductItemRepository prepackageProductItemRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        this.companyID = companyID;
        this.shopID = shopID;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.productRepository = productRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.vendorRepository = vendorRepository;
        this.quickbookCustomEntities = quickbookCustomEntities;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.productBatchRepository = productBatchRepository;
        this.batchQuantityRepository = batchQuantityRepository;
        this.prepackageRepository = prepackageRepository;
        this.productPrepackageQuantityRepository = productPrepackageQuantityRepository;
        this.productWeightToleranceRepository = productWeightToleranceRepository;
        this.prepackageProductItemRepository = prepackageProductItemRepository;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
    }

    private void prepareProduct(String companyID, String shopID,  List<ProductWrapper> products, QuickbookCustomEntities quickbookCustomEntities, VendorRepository vendorRepository, ProductBatchRepository productBatchRepository, BatchQuantityRepository batchQuantityRepository,
                                PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository, PrepackageProductItemRepository prepackageProductItemRepository) {

        List<String> productIds = new ArrayList<>();
        List<ObjectId> vendorIds = new ArrayList<>();
        for (ProductWrapper wrapper : products) {
            productIds.add(wrapper.getId());
            if (StringUtils.isNotBlank(wrapper.getVendorId())) {
                vendorIds.add(new ObjectId(wrapper.getVendorId()));
            }
        }

        // Required accounts for quickBook
        List<String> referenceIds = new ArrayList<>();
        referenceIds.add(quickbookCustomEntities.getId());
        referenceIds.addAll(productIds);
        List<QuickBookAccountDetails> accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceIds(companyID, shopID, referenceIds);

        Map<String, QuickBookAccountDetails> accountDetailsMap = new HashMap<>();
        if (accountDetails.size() > 0) {
            for (QuickBookAccountDetails accountDetail : accountDetails) {
                accountDetailsMap.put(accountDetail.getReferenceId(), accountDetail);
            }
        }

        Map<String, BigDecimal> productTotalValueMap = new HashMap<>();
        Map<String, BigDecimal> productQuantity = new HashMap<>();

        Map<String, ProductBatch> batchForProductMap = productBatchRepository.getBatchesForProductsMap(companyID, shopID, productIds);
        HashMap<String, List<BatchQuantity>> batchQuantityForProductMap = batchQuantityRepository.getBatchQuantityForProduct(companyID, shopID, productIds);
        for (ProductWrapper product : products) {
            if (batchQuantityForProductMap.containsKey(product.getId())) {
                List<BatchQuantity> batchQuantities = batchQuantityForProductMap.get(product.getId());
                if (batchQuantities.size() > 0) {
                    BigDecimal totalQuantity = new BigDecimal(0);
                    BigDecimal totalCogs = new BigDecimal(0);
                    for (BatchQuantity batchQuantity : batchQuantities) {
                        if (batchQuantity.getQuantity().doubleValue() > 0) {
                            totalQuantity = totalQuantity.add(batchQuantity.getQuantity());
                        }

                        if (batchForProductMap.containsKey(batchQuantity.getBatchId())) {
                            ProductBatch productBatch = batchForProductMap.get(batchQuantity.getBatchId());
                            BigDecimal cogs = productBatch.getFinalUnitCost().multiply(batchQuantity.getQuantity());
                            totalCogs = totalCogs.add(cogs);
                        }
                    }

                    // Prepare product quantity map for product
                    if (productQuantity.containsKey(product.getId())) {
                        BigDecimal oldQuantity = productQuantity.get(product.getId());
                        BigDecimal newQuantity = oldQuantity.add(totalQuantity);
                        productQuantity.put(product.getId(), newQuantity);
                    } else {
                        productQuantity.put(product.getId(), totalQuantity);
                    }

                    // Prepare product cogs map for product
                    if (productTotalValueMap.containsKey(product.getId())) {
                        BigDecimal oldCOGS = productTotalValueMap.get(product.getId());
                        BigDecimal newCOGS = oldCOGS.add(totalCogs);
                        productTotalValueMap.put(product.getId(), newCOGS);
                    } else {
                        productTotalValueMap.put(product.getId(), totalCogs);
                    }
                }
            }
        }

        // Calculate product prepackage quantity
        this.calculateProductPrepackagesQuantity(companyID, shopID, productIds, prepackageRepository, productPrepackageQuantityRepository, productWeightToleranceRepository, prepackageProductItemRepository, batchForProductMap, productTotalValueMap, productQuantity );

        HashMap<String, Vendor> vendorMap = vendorRepository.getNonDeletedVendors(companyID, vendorIds);
        for (ProductWrapper wrapper : products) {
            String finalName = null;
            if (StringUtils.isNotBlank(wrapper.getQbDesktopItemRef())) {
                finalName = wrapper.getQbDesktopItemRef();
            } else {
                String productName = wrapper.getName().trim();
                productName = productName.replaceAll("[^0-9a-zA-Z ]", "").trim();

                String productSku = wrapper.getSku();
                int finalLength = 30;
                if (StringUtils.isNoneBlank(productSku)) {
                    if (productSku.length() > finalLength) {
                        finalName = productSku.substring(0, finalLength);
                    } else {
                        finalLength = finalLength - (productSku.length() + 1);
                        if (finalLength > 0) {
                            if (productName.length() > finalLength) {
                                productName = productName.substring(0, finalLength);
                            }
                            finalName = productSku + "_" + productName;
                        } else {
                            finalName = productSku;
                        }
                    }
                } else {
                    if (productName.length() > finalLength) {
                        finalName = productName.substring(0, finalLength);
                    } else {
                        finalName = productName;
                    }
                }
            }

            wrapper.setQbProductName(finalName.trim());

            if (StringUtils.isNotBlank(wrapper.getVendorId())) {
                Vendor vendor = vendorMap.get(wrapper.getVendorId());
                if (vendor != null && vendor.getQbMapping() != null && vendor.getQbMapping().size() > 0) {
                    for (QBDataMapping dataMapping : vendor.getQbMapping()) {
                        if (dataMapping != null && dataMapping.getShopId().equals(shopID) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                && StringUtils.isNotBlank(dataMapping.getQbListId())) {
                            wrapper.setVendorQbRef(dataMapping.getQbDesktopRef());
                            wrapper.setVendorQbListId(dataMapping.getQbListId());
                        }
                    }
                }
            }


            QuickBookAccountDetails entitiesAccountDetails = accountDetailsMap.get(quickbookCustomEntities.getId());
            QuickBookAccountDetails details = accountDetailsMap.get(wrapper.getId());
            if (details != null && details.isStatus()) {
                if (StringUtils.isNotBlank(details.getInventory())) {
                    wrapper.setInventoryAccount(details.getInventory());
                } else {
                    wrapper.setInventoryAccount(entitiesAccountDetails.getInventory());
                }

                if (StringUtils.isNotBlank(details.getSales())) {
                    wrapper.setIncomeAccount(details.getSales());
                } else {
                    wrapper.setIncomeAccount(entitiesAccountDetails.getSales());
                }

                if (StringUtils.isNotBlank(details.getSuppliesAndMaterials())) {
                    wrapper.setCogsAccount(details.getSuppliesAndMaterials());
                } else {
                    wrapper.setCogsAccount(entitiesAccountDetails.getSuppliesAndMaterials());
                }
            } else {
                wrapper.setIncomeAccount(entitiesAccountDetails.getSales());
                wrapper.setInventoryAccount(entitiesAccountDetails.getInventory());
                wrapper.setCogsAccount(entitiesAccountDetails.getSuppliesAndMaterials());
            }


            if (productQuantity.get(wrapper.getId()) != null) {
                wrapper.setCurrentQuantity(productQuantity.get(wrapper.getId()).doubleValue());
            } else {
                wrapper.setCurrentQuantity(0.0);
            }

            if (productTotalValueMap.get(wrapper.getId()) != null) {
                wrapper.setCogs(productTotalValueMap.get(wrapper.getId()));
            } else {
                wrapper.setCogs(new BigDecimal(0));
            }
        }
    }

    private void calculateProductPrepackagesQuantity(String companyID, String shopID, List<String> productIds, PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository,
                                             PrepackageProductItemRepository prepackageProductItemRepository, Map<String, ProductBatch> batchForProductMap, Map<String, BigDecimal> productTotalValueMap, Map<String, BigDecimal> productQuantity) {

        Iterable<Prepackage> prepackages = prepackageRepository.getPrepackagesForProducts(companyID, shopID, productIds, Prepackage.class);
        Iterable<PrepackageProductItem> prepackagesForProducts = prepackageProductItemRepository.getPrepackagesForProductIds(companyID, shopID, productIds, PrepackageProductItem.class);
        Iterable<ProductPrepackageQuantity> quantitiesForProducts = productPrepackageQuantityRepository.getQuantitiesForProducts(companyID, shopID, productIds);
        Set<ObjectId> toleranceIds = new HashSet<>();
        for (Prepackage prepackage : prepackages) {
            if (prepackage != null && StringUtils.isNotBlank(prepackage.getToleranceId())) {
                toleranceIds.add(new ObjectId(prepackage.getToleranceId()));
            }
        }
        HashMap<String, ProductWeightTolerance> productWeightToleranceHashMap = productWeightToleranceRepository.listAsMap(companyID, Lists.newArrayList(toleranceIds));
        for (Prepackage prepackage : prepackages) {
            ProductWeightTolerance productWeightTolerance = null;
            if (StringUtils.isNotBlank(prepackage.getToleranceId())) {
                productWeightTolerance = productWeightToleranceHashMap.get(prepackage.getToleranceId());
            }
            BigDecimal totalValue = new BigDecimal(0);
            BigDecimal totalCOGS = new BigDecimal(0);

            for (PrepackageProductItem prepackagesForProduct : prepackagesForProducts) {
                ProductBatch productBatch = null;
                if (StringUtils.isNotBlank(prepackagesForProduct.getBatchId())) {
                    productBatch = batchForProductMap.get(prepackagesForProduct.getBatchId());
                }

                if (prepackage.getProductId().equalsIgnoreCase(prepackagesForProduct.getProductId()) &&
                        prepackage.getId().equalsIgnoreCase(prepackagesForProduct.getPrepackageId())) {
                    for (ProductPrepackageQuantity quantitiesForProduct : quantitiesForProducts) {
                        if (prepackagesForProduct.getId().equalsIgnoreCase(quantitiesForProduct.getPrepackageItemId())) {
                            BigDecimal cogs = new BigDecimal(0);
                            BigDecimal quantity = new BigDecimal(0);
                            if (productWeightTolerance != null) {
                                quantity = new BigDecimal(quantitiesForProduct.getQuantity()).multiply(productWeightTolerance.getUnitValue());
                            }
                            if (productBatch != null) {
                                cogs = quantity.multiply(productBatch.getFinalUnitCost());
                            }
                            totalValue = totalValue.add(quantity);
                            totalCOGS = totalCOGS.add(cogs);
                        }
                    }
                }
            }

            // Prepare product quantity map for product
            if (productQuantity.containsKey(prepackage.getProductId())) {
                BigDecimal oldQuantity = productQuantity.get(prepackage.getProductId());
                BigDecimal newQuantity = oldQuantity.add(totalValue);
                productQuantity.put(prepackage.getProductId(), newQuantity);
            } else {
                productQuantity.put(prepackage.getProductId(), totalValue);
            }

            // Prepare product cogs map for product
            if (productTotalValueMap.containsKey(prepackage.getProductId())) {
                BigDecimal oldCOGS = productTotalValueMap.get(prepackage.getProductId());
                BigDecimal newCOGS = oldCOGS.add(totalCOGS);
                productTotalValueMap.put(prepackage.getProductId(), newCOGS);
            } else {
                productTotalValueMap.put(prepackage.getProductId(), totalCOGS);
            }
        }
    }

    @Override
    public String generatePushRequest() {
        StringBuilder itemQuery = new StringBuilder();

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.Item);
        if(qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Item sync is not available for shop {}, skipping", shopID);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickbookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Item, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        ArrayList<QuickbookSyncDetails> detailsList = Lists.newArrayList(quickbookSyncDetailList);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        for (QuickbookSyncDetails details : detailsList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Item, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }


        int start = 0;
        int limit = 1000;
        int total = 0;
        List<ProductWrapper> products = new ArrayList<>();
        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Item);
        if (latestQbDesktopCurrentSync != null) {
            products = productRepository.getProductsByLimitsWithQBError(companyID, shopID, latestQbDesktopCurrentSync.getCreated(), ProductWrapper.class);
        }

        limit = 1000 - products.size();
        List<ProductWrapper> productsLimits = productRepository.getProductsLimitsWithoutQbDesktopRef(companyID, shopID, start, limit, ProductWrapper.class);
        products.addAll(productsLimits);

        limit = limit - products.size();
        List<ProductWrapper> newProductsLimits = productRepository.getNewProductsLimitsWithoutQbDesktopRef(companyID, shopID, start, limit, ProductWrapper.class);
        products.addAll(newProductsLimits);

        Map<String, String> productCurrentSyncDataMap = new HashMap<>();

        if (products.size() > 0) {

            this.prepareProduct(companyID, shopID, products, quickbookCustomEntities, vendorRepository, productBatchRepository, batchQuantityRepository, prepackageRepository, productPrepackageQuantityRepository, productWeightToleranceRepository, prepackageProductItemRepository);

            for (ProductWrapper product : products) {
                productCurrentSyncDataMap.put(product.getId(), product.getQbProductName());
            }

            ProductConverter productConverter = null;
            try {
                productConverter = new ProductConverter(products, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = total + productConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(productConverter));
            itemQuery.append(qbxmlConverter.getXMLString());
        }

        int modifiedLimit = 1000 - products.size();
        List<ProductWrapper> modifiedProducts = new ArrayList<>();
        if (modifiedLimit > 0) {
            if (detailsList.size() > 0 && quickbookSyncDetails != null) {
                List<ProductWrapper> qbExistProducts = productRepository.getQBExistProducts(companyID, shopID, start, modifiedLimit, quickbookSyncDetails.getStartTime(), ProductWrapper.class);
                if (qbExistProducts.size() > 0) {
                    modifiedProducts.addAll(qbExistProducts);
                }

                if (modifiedProducts != null && modifiedProducts.size() > 0) {
                    this.prepareProduct(companyID, shopID, modifiedProducts, quickbookCustomEntities, vendorRepository, productBatchRepository, batchQuantityRepository, prepackageRepository, productPrepackageQuantityRepository, productWeightToleranceRepository, prepackageProductItemRepository);

                    for (ProductWrapper product : modifiedProducts) {
                        productCurrentSyncDataMap.put(product.getId(), product.getQbProductName());
                    }

                    ProductEditConverter editConverter = null;
                    try {
                        editConverter = new ProductEditConverter(modifiedProducts, qbDesktopOperationRef.getQbDesktopFieldMap());
                        total = total + editConverter.getmList().size();
                    } catch (Exception e) {
                        LOGGER.error("Error while initiating product edit converter : " + e.getMessage());
                    }

                    QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(editConverter));
                    itemQuery.append(qbxmlConverter.getXMLString());
                }
            }
        }

        if (total > 0) {
            QuickbookSyncDetails details = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Item, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyID, shopID, productCurrentSyncDataMap, details.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Item);
        }

        return itemQuery.toString();
    }

    @Override
    public String generatePullRequest(String timeZone) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        List<QuickbookSyncDetails> detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Item, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 1);

        if (detailsByStatus == null || detailsByStatus.isEmpty()) {
            // Maybe this is first time for pull, so let's use push
            detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Item, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            if (detailsByStatus == null || detailsByStatus.isEmpty()) {
                return "";
            }
        }

        ItemInventoryQueryRqType itemInventoryQueryRqType = new ItemInventoryQueryRqType();
        if (detailsByStatus != null && detailsByStatus.size() > 0) {
            itemInventoryQueryRqType.setFromModifiedDate(DateUtil.convertToISO8601(detailsByStatus.get(0).getStartTime(), timeZone));
        }
        // Save sync details
        quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Item, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull);
        return QBXMLParser.parseRequest(itemInventoryQueryRqType, ItemInventoryQueryRqType.class, "ItemInventoryQueryRq");
    }
}
