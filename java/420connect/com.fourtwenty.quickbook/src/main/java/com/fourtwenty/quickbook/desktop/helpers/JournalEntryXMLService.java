package com.fourtwenty.quickbook.desktop.helpers;


import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.desktop.converters.JournalEntryConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.JournalEntryWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.TransactionWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class JournalEntryXMLService {


    public String qbJournalEntryService(String companyID, String shopID, QuickbookCustomEntities entities, TransactionRepository transactionRepository, ProductRepository productRepository, MemberRepository memberRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, PrepackageProductItemRepository prepackageProductItemRepository, PrepackageRepository prepackageRepository, ProductWeightToleranceRepository weightToleranceRepository, ProductBatchRepository batchRepository) {

        String qbJournalEntryRequest = "";
        List<TransactionWrapper> transactionWrapperList = new ArrayList<>();

        int start = 0;
        int limit = 1000;
        long startDateTime = DateTime.now().getMillis();
        long endDateTime = DateTime.now().getMillis();

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);

        Iterable<QuickbookSyncDetails> syncJournalEntryList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.JournalEntry, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        List<QuickbookSyncDetails> syncJournalEntry = Lists.newArrayList(syncJournalEntryList);

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
            QuickbookSyncDetails details = null;
            if (syncJournalEntry.size() > 0) {
                for (QuickbookSyncDetails quickbookSyncDetails : syncJournalEntry) {
                    details = quickbookSyncDetails;
                    break;
                }
            }

            if (details != null) {
                startDateTime = details.getEndTime();
            } else {
                startDateTime = DateTime.now().withTimeAtStartOfDay().getMillis();
            }

            SearchResult<TransactionWrapper> transactions = transactionRepository.getLimitedTransactionWithoutJournalEntryRef(companyID, shopID, startDateTime, endDateTime, Transaction.TransactionType.Sale, Transaction.TransactionStatus.Completed, TransactionWrapper.class);
            if (transactions.getValues().size() > 0) {
                transactionWrapperList = transactions.getValues();
            }
        } else {
            SearchResult<TransactionWrapper> transactions = transactionRepository.getLimitedTransactionWithoutJournalEntryRef(companyID, shopID, start, limit, Transaction.TransactionType.Sale, Transaction.TransactionStatus.Completed, TransactionWrapper.class);
            if (transactions.getValues().size() > 0) {
                transactionWrapperList = transactions.getValues();
            }
        }

        if (transactionWrapperList.size() > 0) {
            List<ObjectId> memberIds = new ArrayList<>();
            for (TransactionWrapper transactionWrapper : transactionWrapperList) {
                if (StringUtils.isNotBlank(transactionWrapper.getMemberId())) {
                    memberIds.add(new ObjectId(transactionWrapper.getMemberId()));
                }
            }
            HashMap<String, Member> memberHashMap = memberRepository.listAsMap(companyID, memberIds);

            HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(companyID, shopID);
            HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(companyID, shopID);
            HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(companyID);

            // Prepare all batch map data
            HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(companyID);
            HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
            for (ProductBatch batch : allBatchMap.values()) {
                ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
                if (oldBatch == null) {
                    recentBatchMap.put(batch.getProductId(), batch);
                } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                    recentBatchMap.put(batch.getProductId(), batch);
                }
            }

            // Prepare product by company link id map data
            HashMap<String, Product> productMap = productRepository.listAsMap(companyID);
            HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
            for (String productId : productMap.keySet()) {
                Product product = productMap.get(productId);
                if (product != null) {
                    List<Product> items = productsByCompanyLinkId.get(product.getCompanyLinkId());
                    if (items == null) {
                        items = new ArrayList<>();
                    }
                    items.add(product);
                    productsByCompanyLinkId.put(product.getCompanyLinkId(), items);
                }
            }


            List<JournalEntryWrapper> journalEntryWrappers = new ArrayList<>();
            for (TransactionWrapper transactionWrapper : transactionWrapperList) {
                if (transactionWrapper != null && StringUtils.isNotBlank(transactionWrapper.getMemberId())) {
                    Member member = memberHashMap.get(transactionWrapper.getMemberId());
                    QBDataMapping qbMapping = null;
                    if (member.getQbMapping() != null && member.getQbMapping().size()  >0) {
                        for (QBDataMapping dataMapping : member.getQbMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopID) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getEditSequence()) && StringUtils.isNotBlank(dataMapping.getQbListId())) {
                                qbMapping = dataMapping;
                            }
                        }
                    }

                    if (member != null && qbMapping != null && StringUtils.isNotBlank(qbMapping.getQbDesktopRef())) {
                        double totalCogs = calculateCOGSPerTransaction(transactionWrapper, allBatchMap, prepackageHashMap, productItemHashMap, toleranceHashMap, productMap, recentBatchMap,productsByCompanyLinkId);
                        JournalEntryWrapper journalEntryWrapper = new JournalEntryWrapper();
                        journalEntryWrapper.setTransNo(transactionWrapper.getTransNo());
                        journalEntryWrapper.setTransactionProcessedDate(transactionWrapper.getPreparedDate());

                        journalEntryWrapper.setCreditAccountRef(entities.getClearance());
                        journalEntryWrapper.setCreditAccountAmount(new BigDecimal(totalCogs).setScale(2, RoundingMode.HALF_UP));
                        journalEntryWrapper.setDebitAccountRef(entities.getClearance());
                        journalEntryWrapper.setDebitAccountAmount(new BigDecimal(totalCogs).setScale(2, RoundingMode.HALF_UP));
                        journalEntryWrappers.add(journalEntryWrapper);
                    }
                }
            }

            if (journalEntryWrappers.size() > 0) {
                JournalEntryConverter journalEntryConverter = null;
                try {
                    journalEntryConverter = new JournalEntryConverter(journalEntryWrappers, null);
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }

                quickbookSyncDetailsRepository.saveQuickbookEntity(journalEntryConverter.getmList().size(), 0, 0, companyID, startDateTime, endDateTime, shopID, QuickbookSyncDetails.QuickbookEntityType.JournalEntry, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(journalEntryConverter));
                qbJournalEntryRequest = qbxmlConverter.getXMLString();
            }
        }

        return qbJournalEntryRequest;
    }

    public double calculateCOGSPerTransaction(TransactionWrapper transactionWrapper, HashMap<String, ProductBatch> allBatchMap, HashMap<String, Prepackage> prepackageHashMap,
                                              HashMap<String, PrepackageProductItem> productItemHashMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, HashMap<String, Product> productMap, HashMap<String, ProductBatch> recentBatchMap, HashMap<String, List<Product>> productsByCompanyLinkId) {
        List<TransactionWrapper> results = new ArrayList<>();
        results.add(transactionWrapper);

        for (TransactionWrapper ts : results) {
            double transCogs = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if (item.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                boolean calculated = false;
                double itemCogs = 0;
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        itemCogs += calcCOGS(unitsSold, targetBatch);

                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                itemCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                            }
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    itemCogs = unitCost * item.getQuantity().doubleValue();
                }
                transCogs += itemCogs;
                return transCogs;
            }
        }
        return 0.0;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {
        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();
        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

}
