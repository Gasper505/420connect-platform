package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.desktop.converters.PaymentReceivedConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.PaymentReceivedWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;

public class PaymentReceivedXMLService {
    private static final int TOTAL_LIMIT = 1000;
    private static final Logger LOG = LoggerFactory.getLogger(PaymentReceivedXMLService.class);
    private String companyId;
    private String shopId;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private QuickbookCustomEntities customEntities;
    private InvoiceRepository invoiceRepository;
    private InvoicePaymentsRepository invoicePaymentsRepository;
    private VendorRepository vendorRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    public PaymentReceivedXMLService(String companyId, String shopId, QbDesktopOperationRepository qbDesktopOperationRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntities customEntities, InvoiceRepository invoiceRepository, InvoicePaymentsRepository invoicePaymentsRepository, VendorRepository vendorRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ErrorLogsRepository errorLogsRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.customEntities = customEntities;
        this.invoiceRepository = invoiceRepository;
        this.vendorRepository = vendorRepository;
        this.invoicePaymentsRepository = invoicePaymentsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
    }

    public String generatePushRequest() {
        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.PaymentReceived);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOG.warn("Payment received sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.PaymentReceived, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.PaymentReceived, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        String paymentReceivedQuery = "";
        int start = 0;
        List<PaymentReceivedWrapper> paymentReceivedWrapper = new ArrayList<>();
        if (customEntities.getSyncTime() > 0) {
            if (details != null && details.getEndTime() > 0) {
                SearchResult<PaymentReceivedWrapper> paymentReceivedByInvoiceWithQBError = invoicePaymentsRepository.getPaymentReceivedByInvoiceWithQBError(companyId, shopId, customEntities.getSyncTime(), details.getEndTime(), start, TOTAL_LIMIT, PaymentReceivedWrapper.class);
                if (paymentReceivedByInvoiceWithQBError != null && paymentReceivedByInvoiceWithQBError.getValues().size() > 0) {
                    paymentReceivedWrapper.addAll(paymentReceivedByInvoiceWithQBError.getValues());
                }
            }

            int limit = TOTAL_LIMIT - paymentReceivedWrapper.size();
            SearchResult<PaymentReceivedWrapper> paymentReceivedByInvoiceWithoutQbRef = invoicePaymentsRepository.getPaymentReceivedByInvoiceWithoutQbRef(companyId, shopId, customEntities.getSyncTime(), start, limit, PaymentReceivedWrapper.class);
            if (paymentReceivedByInvoiceWithoutQbRef != null && paymentReceivedByInvoiceWithoutQbRef.getValues().size() > 0) {
                paymentReceivedWrapper.addAll(paymentReceivedByInvoiceWithoutQbRef.getValues());
            }
        }

        Set<ObjectId> invoiceIdsSet = new HashSet<>();
        for (PaymentReceivedWrapper receivedWrapper : paymentReceivedWrapper) {
            invoiceIdsSet.add(new ObjectId(receivedWrapper.getInvoiceId()));
        }

        Map<String, Invoice> invoiceMap = new HashMap<>();
        SearchResult<Invoice> invoiceByLimits = invoiceRepository.getInvoiceByLimitsWithInvoiceIds(companyId, shopId, Lists.newArrayList(invoiceIdsSet), Invoice.class);
        for (Invoice invoice : invoiceByLimits.getValues()) {
            invoiceMap.put(invoice.getId(), invoice);
        }

        QuickBookAccountDetails entitiesAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, customEntities.getId());
        List<PaymentReceivedWrapper> successReceivedWrapper = new ArrayList<>();
        List<PaymentReceivedWrapper> failedReceivedWrapper = new ArrayList<>();
        if (paymentReceivedWrapper.size() > 0) {
            HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyId);
            for (PaymentReceivedWrapper wrapper : paymentReceivedWrapper) {
                Invoice invoice = invoiceMap.get(wrapper.getInvoiceId());
                if (invoice != null && StringUtils.isNotBlank(invoice.getCustomerId())) {
                    Vendor vendor = vendorHashMap.get(invoice.getCustomerId());
                    if (vendor != null && vendor.getQbCustomerMapping() != null && vendor.getQbCustomerMapping().size() > 0) {
                        for (QBDataMapping dataMapping : vendor.getQbCustomerMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getQbListId()) && StringUtils.isNotBlank(dataMapping.getEditSequence())) {
                                wrapper.setQbCustomerRef(dataMapping.getQbDesktopRef());
                                wrapper.setQbCustomerListId(dataMapping.getQbListId());
                                wrapper.setMemo(invoice.getInvoiceNumber());
                                wrapper.setTxnId(invoice.getQbDesktopRef());
                                wrapper.setTotalAmount(wrapper.getAmountPaid());
                                wrapper.setReceivableAccount(entitiesAccountDetails.getReceivable());
                                wrapper.setDepositAccount(entitiesAccountDetails.getChecking());
                                successReceivedWrapper.add(wrapper);
                            }
                        }
                    } else {
                        failedReceivedWrapper.add(wrapper);
                    }
                }
            }
        }

        if (successReceivedWrapper.size() > 0) {
            Map<String, String> currentSyncPaymentReceived = new HashMap<>();
            for (PaymentReceivedWrapper wrapper : successReceivedWrapper) {
                currentSyncPaymentReceived.put(wrapper.getId(), wrapper.getPaymentNo());
            }

            PaymentReceivedConverter paymentReceivedConverter = null;
            try {
                paymentReceivedConverter = new PaymentReceivedConverter(successReceivedWrapper, qbDesktopOperationRef.getQbDesktopFieldMap());
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(successReceivedWrapper.size(), 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.PaymentReceived, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(paymentReceivedConverter));
            paymentReceivedQuery = qbxmlConverter.getXMLString();

            saveCurrentSync(companyId, shopId, currentSyncPaymentReceived, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.PaymentReceived);
        }

        if (failedReceivedWrapper.size() > 0) {
            long currentTime = DateTime.now().getMillis();
            String errorMsg = "Payment's received are not synchronized";
            for (PaymentReceivedWrapper wrapper : failedReceivedWrapper) {
                Invoice invoice = invoiceMap.get(wrapper.getInvoiceId());
                if (invoice != null) {
                    invoiceRepository.updateQbDesktopPaymentReceivedRef(companyId,shopId, invoice.getId(), false, true, DateTime.now().getMillis());
                    invoicePaymentsRepository.updateDesktopPaymentReceivedQbErrorAndTime(companyId, shopId, wrapper.getId(), Boolean.TRUE, currentTime);
                    saveErrorLogs(companyId, shopId, "500", errorMsg, null, QuickbookSyncDetails.QuickbookEntityType.PaymentReceived, errorLogsRepository);
                }
            }
        }

        return paymentReceivedQuery;
    }
}
