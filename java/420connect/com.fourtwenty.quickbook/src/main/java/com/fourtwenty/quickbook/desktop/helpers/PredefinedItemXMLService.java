package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.desktop.converters.PredefinedItemConvertor;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.data.PredefinedDataRequest;
import com.fourtwenty.quickbook.desktop.wrappers.PredefinedItemWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PredefinedItemXMLService {

    public String convertIntoItemListXML(String companyId, String shopId, QbDesktopOperationRepository qbDesktopOperationRepository, QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository, QuickbookCustomEntities entities, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        String predefinedItemsData = "";

        List<QBDesktopOperation> quickBookDesktopOperations = qbDesktopOperationRepository.getQuickBookDesktopOperations(companyId, shopId);
        if (quickBookDesktopOperations.size() > 0) {

            QBDesktopOperation qbDesktopOperationRef = null;
            for (QBDesktopOperation quickBookDesktopOperation : quickBookDesktopOperations) {
                if(quickBookDesktopOperation.getOperationType() == QBDesktopOperation.OperationType.Sale) {
                    qbDesktopOperationRef = quickBookDesktopOperation;
                    break;
                }
            }

            List<PredefinedItemWrapper> predefinedItemWrappers = new ArrayList<>();
            PredefinedItemWrapper itemWrapper = new PredefinedItemWrapper();

            List<PredefinedDataRequest> requests = new ArrayList<>();
            QuickBookAccountDetails accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, entities.getId());

            Map<String, QBDesktopField> qbDesktopFieldMap = qbDesktopOperationRef.getQbDesktopFieldMap();
            for (String item : qbDesktopFieldMap.keySet()) {
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("CityTax")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getClearance()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("StateTax")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getClearance()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("CountyTax")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getClearance()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("ExciseTax")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add( getPredefinedData(qbDesktopField.getName(), accountDetails.getClearance()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("PreTaxDiscount")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getClearance()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("AfterTaxDiscount")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getClearance()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("DeliveryFees")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getSales()));
                }
                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("CreditCardFees")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getCc_clearance()));
                }

                if (StringUtils.isNotBlank(item) && item.equalsIgnoreCase("Discount")) {
                    QBDesktopField qbDesktopField = qbDesktopFieldMap.get(item);
                    requests.add(getPredefinedData(qbDesktopField.getName(), accountDetails.getSales()));
                }
            }

            requests.add(getPredefinedData("Total Product Discount", accountDetails.getSales()));
            requests.add(getPredefinedData("Cultivation Tax", accountDetails.getClearance()));
            requests.add(getPredefinedData("Adjustment Amount", accountDetails.getSales()));

            if (entities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                requests.add(getPredefinedData("Daily Purchase Order", accountDetails.getSales()));
            }

            itemWrapper.setRequests(requests);
            predefinedItemWrappers.add(itemWrapper);

            if (predefinedItemWrappers.size() > 0) {
                PredefinedItemConvertor itemConvertor = null;
                try {
                    itemConvertor = new PredefinedItemConvertor(predefinedItemWrappers, null);
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(itemConvertor));
                predefinedItemsData = qbxmlConverter.getXMLString();

                // Custom items sync update in quick book custom entities
                if (StringUtils.isNotBlank(predefinedItemsData)) {
                    entities.setSyncCustomItems(true);
                    quickbookCustomEntitiesRepository.update(companyId, entities.getId(), entities);
                }
            }
        }
        return predefinedItemsData;
    }

    private PredefinedDataRequest getPredefinedData(String name, String accountRef) {
        PredefinedDataRequest request = new PredefinedDataRequest();
        request.setName(name);
        request.setAccountName(accountRef);
        return request;
    }
}
