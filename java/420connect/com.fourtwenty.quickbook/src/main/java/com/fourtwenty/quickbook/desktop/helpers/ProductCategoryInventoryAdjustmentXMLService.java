package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.quickbook.desktop.converters.InventoryAdjustmentConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.InventoryAdjustmentWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;

public class ProductCategoryInventoryAdjustmentXMLService implements  QBXMLService{
    private static final Logger LOG = LoggerFactory.getLogger(ProductCategoryInventoryAdjustmentXMLService.class);

   private String companyId;
   private String shopId;
   private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
   private QbDesktopOperationRepository qbDesktopOperationRepository;
   private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
   private ProductRepository productRepository;
   private InventoryRepository inventoryRepository;
   private BatchQuantityRepository batchQuantityRepository;
   private InventoryActionRepository inventoryActionRepository;
   private QuickbookCustomEntities qbCustomEntities;
   private ProductBatchRepository batchRepository;
   private PrepackageRepository prepackageRepository;
   private ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
   private ProductWeightToleranceRepository productWeightToleranceRepository;
   private ProductCategoryRepository productCategoryRepository;
   private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    public ProductCategoryInventoryAdjustmentXMLService(String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QbDesktopOperationRepository qbDesktopOperationRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ProductRepository productRepository, InventoryRepository inventoryRepository, BatchQuantityRepository batchQuantityRepository, InventoryActionRepository inventoryActionRepository, QuickbookCustomEntities qbCustomEntities, ProductBatchRepository batchRepository, PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository, ProductCategoryRepository productCategoryRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.productRepository = productRepository;
        this.inventoryRepository = inventoryRepository;
        this.batchQuantityRepository = batchQuantityRepository;
        this.inventoryActionRepository = inventoryActionRepository;
        this.qbCustomEntities = qbCustomEntities;
        this.batchRepository = batchRepository;
        this.prepackageRepository = prepackageRepository;
        this.productPrepackageQuantityRepository = productPrepackageQuantityRepository;
        this.productWeightToleranceRepository = productWeightToleranceRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
    }

    @Override
    public String generatePushRequest() {
        String inventoryAdjustmentQuery = "";
        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.InventoryAdjustment);

        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickBookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.ProductCategoryInventoryAdjustment, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        ArrayList<QuickbookSyncDetails> detailsList = Lists.newArrayList(quickBookSyncDetailList);

        int failedCount = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        long latestFailTime = 0;
        for (QuickbookSyncDetails details : detailsList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
                failedCount += 1;
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.ProductCategoryInventoryAdjustment, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }

        Set<String> updatedCategoryIds = new HashSet<>();
        List<ProductCategory> newCategoriesList = new ArrayList<>();
        List<ProductCategory> categoriesList = new ArrayList<>();
        List<ProductCategory> productCategories= new ArrayList<>();
        int start = 0;
        long dateTime = 0;
        int total = 0;

        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.ProductCategoryInventoryAdjustment);
        if (latestQbDesktopCurrentSync != null) {
            productCategories = productCategoryRepository.getProductCategoriesByLimitsWithQbQuantityError(companyId, shopId, latestQbDesktopCurrentSync.getCreated(), ProductCategory.class);
        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(companyId, shopId);
        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyId, shopId);
        int limit = 1000 - productCategories.size();
        if (quickbookSyncDetails == null) {
            newCategoriesList = productCategoryRepository.getProductCategoriesLimitsWithoutQuantitySynced(companyId, shopId, start, limit, ProductCategory.class);
            productCategories.addAll(newCategoriesList);

            limit = limit - productCategories.size();
            categoriesList = productCategoryRepository.getProductCategoriesLimits(companyId, shopId, start, limit, ProductCategory.class);
            productCategories.addAll(categoriesList);
        } else {
            dateTime = quickbookSyncDetails.getStartTime();

            List<ProductCategory> updatedProductCategories = productCategoryRepository.getProductCategoriesLimitsWithQuantitySynced(companyId, shopId, quickbookSyncDetails.getEndTime(), ProductCategory.class);
            List<ProductCategory> updatedNewProductCategories = productCategoryRepository.getProductNewCategoriesLimitsWithQuantitySynced(companyId, shopId, quickbookSyncDetails.getEndTime(), ProductCategory.class);
            if (updatedProductCategories.size() > 0) {
                for (ProductCategory updatedProductCategory : updatedProductCategories) {
                    updatedCategoryIds.add(updatedProductCategory.getId());
                }
            }

            if (updatedNewProductCategories.size() > 0) {
                for (ProductCategory updatedProductCategory : updatedNewProductCategories) {
                    updatedCategoryIds.add(updatedProductCategory.getId());
                }
            }

            Iterable<InventoryOperation> updatedOperations = inventoryActionRepository.getActionsByTime(companyId, shopId, dateTime);
            Set<String> productIds = new HashSet<>();
            for (InventoryOperation operation : updatedOperations) {
                if (StringUtils.isNotBlank(operation.getProductId()) && ObjectId.isValid(operation.getProductId())) {
                    productIds.add(operation.getProductId());
                }
            }

            if (productIds.size() > 0) {
                for (String productId : productIds) {
                    Product product = productHashMap.get(productId);
                    if (product != null && StringUtils.isNotBlank(product.getCategoryId())) {
                        ProductCategory productCategory = categoryHashMap.get(product.getCategoryId());
                        if (productCategory != null) {
                            updatedCategoryIds.add(productCategory.getId());
                        }
                    }
                }
            }

            limit = limit - updatedCategoryIds.size();
            if (limit > 0) {
                newCategoriesList = productCategoryRepository.getProductCategoriesLimitsWithoutQuantitySynced(companyId, shopId, start, limit, ProductCategory.class);
                productCategories.addAll(newCategoriesList);
            }
        }

        if (productCategories.size() > 0 || updatedCategoryIds.size() > 0) {

            List<InventoryOperation.SourceType> sourceTypes = new ArrayList<>();
            sourceTypes.add(InventoryOperation.SourceType.None);
            sourceTypes.add(InventoryOperation.SourceType.Transfer);
            sourceTypes.add(InventoryOperation.SourceType.Reconciliation);
            sourceTypes.add(InventoryOperation.SourceType.ProductBatch);
            sourceTypes.add(InventoryOperation.SourceType.StockReset);
            sourceTypes.add(InventoryOperation.SourceType.Product);
            sourceTypes.add(InventoryOperation.SourceType.System);
            sourceTypes.add(InventoryOperation.SourceType.DerivedProduct);
            sourceTypes.add(InventoryOperation.SourceType.BundleProduct);
            sourceTypes.add(InventoryOperation.SourceType.Transaction);
            sourceTypes.add(InventoryOperation.SourceType.RefundTransaction);
            sourceTypes.add(InventoryOperation.SourceType.ShippingManifest);


            List<InventoryOperation> inventoryOperations = new ArrayList<>();
            HashMap<String,List<Product>> productCategoryHashMap = new HashMap<>();

            for (String productId : productHashMap.keySet()) {
                Product product = productHashMap.get(productId);
                if (product != null && StringUtils.isNotBlank(product.getCategoryId())) {
                    if (productCategoryHashMap.containsKey(product.getCategoryId())) {
                        List<Product> productList = productCategoryHashMap.get(product.getCategoryId());
                        productList.add(product);
                        productCategoryHashMap.put(product.getCategoryId(), productList);
                    } else {
                        productCategoryHashMap.put(product.getCategoryId(), Lists.newArrayList(product));
                    }
                }
            }


            // For new products that do we need to sync first time inventory adjustment
            List<Product> newProductList = new ArrayList<>();
            List<String> newProductIds = new ArrayList<>();
            for (ProductCategory productCategory : newCategoriesList) {
                if (productCategory != null) {
                    List<Product> productList = productCategoryHashMap.get(productCategory.getId());
                    if (productList != null && productList.size() > 0) {
                        newProductList.addAll(productList);
                    }
                }
            }

            if (newProductList.size() >  0) {
                for (Product product : newProductList) {
                    newProductIds.add(product.getId());
                }
            }

            // For updated products that do we need to sync inventory adjustment
            List<Product> updatedProductList = new ArrayList<>();
            List<String> updatedProductIds = new ArrayList<>();
            for (String categoryId : updatedCategoryIds) {
                ProductCategory productCategory = categoryHashMap.get(categoryId);
                if (productCategory != null) {
                    List<Product> productList = productCategoryHashMap.get(productCategory.getId());
                    if (productList.size() > 0) {
                        updatedProductList.addAll(productList);
                    }
                }
            }

            if (updatedProductList.size() >  0) {
                for (Product product : updatedProductList) {
                    updatedProductIds.add(product.getId());
                }
            }


            // For all products
            List<Product> productList = new ArrayList<>();
            List<String> productIds = new ArrayList<>();
            for (ProductCategory productCategory : productCategories) {
                if (productCategory != null) {
                    List<Product> products = productCategoryHashMap.get(productCategory.getId());
                    if (products != null && products.size() > 0) {
                        productList.addAll(products);
                    }
                }
            }

            for (Product product : productList) {
                productIds.add(product.getId());
            }



            HashMap<String, List<BatchQuantity>> batchQuantityHashMap = new HashMap<>();
            Map<String, BigDecimal> prepackageQuantityMap = new HashMap<>();
            if (productIds.size() > 0) {
                List<String> inventoryIds = new ArrayList<>();
                HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(companyId, shopId);
                for (String inventoryId : inventoryHashMap.keySet()) {
                    Inventory inventory = inventoryHashMap.get(inventoryId);
                    if (inventory != null) {
                        inventoryIds.add(inventoryId);
                    }
                }

                batchQuantityHashMap = batchQuantityRepository.getBatchQuantityByInventoryForProduct(companyId, shopId, inventoryIds, productIds);
                prepackageQuantityMap = prepackageQuantityForProducts(companyId, shopId, productIds, prepackageRepository, productPrepackageQuantityRepository, productWeightToleranceRepository);
            }


            if (updatedProductIds.size() > 0) {
                productList.addAll(updatedProductList);
                Iterable<InventoryOperation> updatedInventoryOperations = inventoryActionRepository.getOperationsBySourceTypeForProducts(companyId, shopId, dateTime, sourceTypes, Lists.newArrayList(updatedProductIds));
                inventoryOperations.addAll(Lists.newArrayList(updatedInventoryOperations));
            }

            if (productIds.size() == updatedProductIds.size() && inventoryOperations.size() == 0) {
                return StringUtils.EMPTY;
            }

            HashMap<String, ProductBatch> allBatchMap = batchRepository.getBatchesForProductsMap(companyId, new ArrayList<>(productIds));
            HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
            for (ProductBatch batch : allBatchMap.values()) {
                ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
                if (oldBatch == null) {
                    recentBatchMap.put(batch.getProductId(), batch);
                } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                    recentBatchMap.put(batch.getProductId(), batch);
                }
            }

            List<InventoryAdjustmentWrapper> wrappers = new ArrayList<>();
            this.prepareInventoryAdjustment(productList, inventoryOperations, wrappers, qbCustomEntities, recentBatchMap, batchQuantityHashMap, prepackageQuantityMap, categoryHashMap);

            Map<String, String> productCurrentSyncDataMap = new HashMap<>();
            for (InventoryAdjustmentWrapper wrapper : wrappers) {
                productCurrentSyncDataMap.put(wrapper.getProductId(), wrapper.getProductQBDesktopRef());
            }

            InventoryAdjustmentConverter converter = null;
            try {
                converter = new InventoryAdjustmentConverter(wrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
                total += converter.getmList().size();
            } catch (Exception e) {
                LOG.error("Error while creating adjustment converter " + e.getMessage());
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(converter));
            inventoryAdjustmentQuery = qbxmlConverter.getXMLString();

            if (total > 0) {
                QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(converter.getmList().size(), 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.ProductCategoryInventoryAdjustment, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
                saveCurrentSync(companyId, shopId, productCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.ProductCategoryInventoryAdjustment);
            }
        }
        return inventoryAdjustmentQuery;
    }

    private Map<String, BigDecimal> prepackageQuantityForProducts(String companyId, String shopId, List<String> productIds, PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository productPrepackageQuantityRepository, ProductWeightToleranceRepository productWeightToleranceRepository){
        Map<String, BigDecimal> productQuantityMap = new HashMap<>();
        Set<ObjectId> toleranceIds = new HashSet<>();
        Iterable<Prepackage> prepackagesForProducts = prepackageRepository.getPrepackagesForProducts(companyId, shopId, productIds, Prepackage.class);
        Map<String, Prepackage> prepackageMap = new HashMap<>();
        for (Prepackage prepackage : prepackagesForProducts) {
            if (!prepackageMap.containsKey(prepackage.getId())) {
                prepackageMap.put(prepackage.getId(), prepackage);
                if (StringUtils.isNotBlank(prepackage.getToleranceId())) {
                    toleranceIds.add(new ObjectId(prepackage.getToleranceId()));
                }
            }
        }

        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = productWeightToleranceRepository.listAsMap(companyId, Lists.newArrayList(toleranceIds));

        Iterable<ProductPrepackageQuantity> quantitiesForProducts = productPrepackageQuantityRepository.getQuantitiesForProducts(companyId, shopId, productIds);
        for (ProductPrepackageQuantity quantitiesForProduct : quantitiesForProducts) {
            if (quantitiesForProduct != null) {
                int quantity = quantitiesForProduct.getQuantity();
                BigDecimal unitValue = new BigDecimal(0);
                Prepackage prepackage = prepackageMap.get(quantitiesForProduct.getPrepackageId());
                if (prepackage != null && StringUtils.isNotBlank(prepackage.getToleranceId())) {
                    ProductWeightTolerance productWeightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                    if (productWeightTolerance != null) {
                        unitValue = productWeightTolerance.getUnitValue();
                    }
                }

                BigDecimal totalQuantity = unitValue.multiply(new BigDecimal(quantity));
                if (productQuantityMap.containsKey(quantitiesForProduct.getProductId())) {
                    BigDecimal oldQuantity = productQuantityMap.get(quantitiesForProduct.getProductId());
                    BigDecimal newQuantity = oldQuantity.add(totalQuantity);
                    productQuantityMap.put(quantitiesForProduct.getProductId(), newQuantity);
                } else {
                    productQuantityMap.put(quantitiesForProduct.getProductId(), totalQuantity);
                }
            }
        }
        return productQuantityMap;
    }

    private void prepareInventoryAdjustment(List<Product> products,
                                            Iterable<InventoryOperation> newInventoryOperations,
                                            List<InventoryAdjustmentWrapper> wrappers,
                                            QuickbookCustomEntities qbCustomEntities, HashMap<String, ProductBatch> recentBatchMap, HashMap<String, List<BatchQuantity>> batchQuantityHashMap, Map<String, BigDecimal> prepackageQuantityMap, HashMap<String, ProductCategory> productCategoryHashMap) {
        // Required accounts for quickBook
        List<String> referenceIds = new ArrayList<>();
        referenceIds.add(qbCustomEntities.getId());
        for (Product product : products) {
            referenceIds.add(product.getId());
        }

        List<QuickBookAccountDetails> accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceIds(companyId, shopId, referenceIds);
        Map<String, QuickBookAccountDetails> accountDetailsMap = new HashMap<>();
        if (accountDetails.size() > 0) {
            for (QuickBookAccountDetails accountDetail : accountDetails) {
                accountDetailsMap.put(accountDetail.getReferenceId(), accountDetail);
            }
        }

        Map<String, List<InventoryOperation>> invOperationMap = new HashMap<>();
        for (InventoryOperation operation : newInventoryOperations) {
            invOperationMap.putIfAbsent(operation.getProductId(), new ArrayList<>());
            List<InventoryOperation> operations = invOperationMap.get(operation.getProductId());
            operations.add(operation);
            invOperationMap.put(operation.getProductId(), operations);
        }

        Map<String, InventoryAdjustmentWrapper> adjustmentMap = new HashMap<>();
        for (Product product : products) {
            BigDecimal quantity = new BigDecimal(0);
            if (batchQuantityHashMap.containsKey(product.getId())) {
                List<BatchQuantity> batchQuantities = batchQuantityHashMap.get(product.getId());
                if (batchQuantities.size() > 0) {
                    for (BatchQuantity batchQuantity : batchQuantities) {
                        quantity = quantity.add(batchQuantity.getQuantity());
                    }
                }
                if (prepackageQuantityMap.containsKey(product.getId())) {
                    BigDecimal prepackageQuantity = prepackageQuantityMap.get(product.getId());
                    quantity = quantity.add(prepackageQuantity);
                }
            } else {
                List<InventoryOperation> operations = invOperationMap.get(product.getId());
                if (operations != null && operations.size() > 0) {
                    quantity = this.prepareQuantity(operations);
                }
            }

            BigDecimal costPerUnit = BigDecimal.ZERO;
            ProductBatch newestBatch = recentBatchMap.get(product.getId());
            if (newestBatch != null) {
                costPerUnit = newestBatch.getFinalUnitCost();
            }
            BigDecimal cogs = costPerUnit.multiply(quantity);

            QuickBookAccountDetails entitiesAccountDetails = accountDetailsMap.get(qbCustomEntities.getId());
            QuickBookAccountDetails details = accountDetailsMap.get(product.getId());
            if (StringUtils.isNotBlank(product.getCategoryId())) {
                ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                if (productCategory != null) {
                    if (adjustmentMap.containsKey(product.getCategoryId())) {
                        InventoryAdjustmentWrapper adjustments = adjustmentMap.get(productCategory.getId());

                        BigDecimal qty = adjustments.getQuantity();
                        BigDecimal value = adjustments.getValue();
                        BigDecimal newQty = qty.add(quantity);
                        BigDecimal newValue = value.add(cogs);

                        adjustments.setQuantity(newQty);
                        adjustments.setValue(newValue);

                        adjustmentMap.put(productCategory.getId(), adjustments);

                    } else {
                        InventoryAdjustmentWrapper wrapper = new InventoryAdjustmentWrapper();
                        wrapper.setProductQBDesktopRef(productCategory.getQbDesktopRef());
                        wrapper.setProductListId(productCategory.getQbListId());
                        wrapper.setProductId(productCategory.getId());
                        if (details != null && details.isStatus()) {
                            if (StringUtils.isNotBlank(details.getInventory())) {
                                wrapper.setAccountName(details.getInventory());
                            } else {
                                wrapper.setAccountName(entitiesAccountDetails.getInventory());
                            }
                        } else {
                            wrapper.setAccountName(entitiesAccountDetails.getInventory());
                        }

                        wrapper.setQuantitySynced(productCategory.isQbQuantitySynced());
                        wrapper.setValue(cogs.abs());
                        wrapper.setQuantity(quantity);

                        adjustmentMap.put(productCategory.getId(), wrapper);
                    }
                }
            }
        }

        if (adjustmentMap.size() > 0) {
            for (String categoryId : adjustmentMap.keySet()) {
                InventoryAdjustmentWrapper wrapper = adjustmentMap.get(categoryId);
                wrappers.add(wrapper);
            }
        }
    }

    private BigDecimal prepareQuantity(List<InventoryOperation> operations) {
        BigDecimal quantity = BigDecimal.ZERO;
        for (InventoryOperation operation : operations) {
            quantity = quantity.add(operation.getQuantity());
        }
        return quantity;
    }

    @Override
    public String generatePullRequest(String timeZone) {
        return null;
    }
}
