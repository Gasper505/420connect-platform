package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBUniqueSequence;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.converters.ProductCategoryConverter;
import com.fourtwenty.quickbook.desktop.converters.ProductCategoryEditConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.soap.qbxml.QBXMLParser;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.ItemInventoryQueryRqType;
import com.fourtwenty.quickbook.desktop.wrappers.ProductCategoryWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;

public class ProductCategoryXMLService implements QBXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductCategoryXMLService.class);

    private String companyID;
    private String shopID;
    private QuickbookCustomEntities quickbookCustomEntities;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ProductCategoryRepository productCategoryRepository;
    private QBUniqueSequenceRepository qbUniqueSequenceRepository;
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    public ProductCategoryXMLService(String companyID, String shopID, QuickbookCustomEntities quickbookCustomEntities, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                     QbDesktopOperationRepository qbDesktopOperationRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ProductCategoryRepository productCategoryRepository, QBUniqueSequenceRepository qbUniqueSequenceRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        this.companyID = companyID;
        this.shopID = shopID;
        this.quickbookCustomEntities = quickbookCustomEntities;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.qbUniqueSequenceRepository = qbUniqueSequenceRepository;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
    }

    @Override
    public String generatePushRequest() {
        StringBuilder categoryQuery = new StringBuilder();

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.ProductCategory);
        if(qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Item sync is not available for shop {}, skipping", shopID);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickBookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        ArrayList<QuickbookSyncDetails> detailsList = Lists.newArrayList(quickBookSyncDetailList);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        for (QuickbookSyncDetails details : detailsList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }

        int start = 0;
        int total = 0;
        List<ProductCategoryWrapper> productCategories = new ArrayList<>();
        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct);
        if (latestQbDesktopCurrentSync != null) {
            productCategories = productCategoryRepository.getProductCategoriesByLimitsWithQBError(companyID, shopID, latestQbDesktopCurrentSync.getCreated(), ProductCategoryWrapper.class);
        }

        int limit = 1000 - productCategories.size();
        List<ProductCategoryWrapper> productCategoryWithoutQbRef = productCategoryRepository.getProductCategoriesLimitsWithoutQbDesktopRef(companyID, shopID, start, limit, ProductCategoryWrapper.class);
        productCategories.addAll(productCategoryWithoutQbRef);

        limit = limit - productCategories.size();
        List<ProductCategoryWrapper> productCategoriesWithoutQbRefWithoutError = productCategoryRepository.getProductCategoriesLimitsWithoutQbDesktopRefWithoutError(companyID, shopID, start, limit, ProductCategoryWrapper.class);
        productCategories.addAll(productCategoriesWithoutQbRefWithoutError);


        Map<String, String> productCategoryCurrentSyncDataMap = new HashMap<>();

        if (productCategories.size() > 0) {
            this.prepareProductCategory(productCategories, quickbookCustomEntities, quickBookAccountDetailsRepository);

            for (ProductCategoryWrapper productCategory : productCategories) {
                productCategoryCurrentSyncDataMap.put(productCategory.getId(), productCategory.getQbCategoryName());
            }

            ProductCategoryConverter productCategoryConverter = null;
            try {
                productCategoryConverter = new ProductCategoryConverter(productCategories, qbDesktopOperationRef.getQbDesktopFieldMap());
                total += productCategoryConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(productCategoryConverter));
            categoryQuery.append(qbxmlConverter.getXMLString());
        }

        int modifiedLimit = 1000 - productCategories.size();
        List<ProductCategoryWrapper> modifiedProductCategories = null;
        if (modifiedLimit > 0) {
            if (detailsList.size() > 0 && quickbookSyncDetails != null) {
                modifiedProductCategories = productCategoryRepository.getQBExistProductCategories(companyID, shopID, start, modifiedLimit, quickbookSyncDetails.getStartTime(), ProductCategoryWrapper.class);
            }

            if (modifiedProductCategories != null && modifiedProductCategories.size() > 0) {
                this.prepareProductCategory(modifiedProductCategories, quickbookCustomEntities, quickBookAccountDetailsRepository);

                for (ProductCategoryWrapper modifiedProductCategory : modifiedProductCategories) {
                    productCategoryCurrentSyncDataMap.put(modifiedProductCategory.getId(), modifiedProductCategory.getQbCategoryName());
                }

                ProductCategoryEditConverter productCategoryEditConverter = null;
                try {
                    productCategoryEditConverter = new ProductCategoryEditConverter(modifiedProductCategories, qbDesktopOperationRef.getQbDesktopFieldMap());
                    total = total + productCategoryEditConverter.getmList().size();
                } catch (Exception e) {
                    LOGGER.error("Error while initiating product edit converter : " + e.getMessage());
                }
                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(productCategoryEditConverter));
                categoryQuery.append(qbxmlConverter.getXMLString());
            }
        }

        if (total > 0) {
            QuickbookSyncDetails details = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyID, shopID, productCategoryCurrentSyncDataMap, details.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct);
        }

        return categoryQuery.toString();
    }

    private void prepareProductCategory(List<ProductCategoryWrapper> productCategories, QuickbookCustomEntities entities, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository){
        // Required accounts for quickBook
        List<String> referenceIds = new ArrayList<>();
        referenceIds.add(entities.getId());
        for (ProductCategoryWrapper productCategory : productCategories) {
            referenceIds.add(productCategory.getId());
        }

        List<QuickBookAccountDetails> accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceIds(companyID, shopID, referenceIds);
        Map<String, QuickBookAccountDetails> accountDetailsMap = new HashMap<>();
        if (accountDetails.size() > 0) {
            for (QuickBookAccountDetails accountDetail : accountDetails) {
                accountDetailsMap.put(accountDetail.getReferenceId(), accountDetail);
            }
        }


        Map<String, Integer> dbUniqueSequenceMap = new HashMap<>();
        List<QBUniqueSequence> qbUniqueSequenceByType = qbUniqueSequenceRepository.getQbUniqueSequenceByType(companyID, shopID);
        for (QBUniqueSequence qbUniqueSequence : qbUniqueSequenceByType) {
            if (qbUniqueSequence != null) {
                dbUniqueSequenceMap.put(qbUniqueSequence.getName(), qbUniqueSequence.getCurrentSequence());
            }
        }

        Map<String, Integer> newUniqueSequenceMap = new HashMap<>();
        for (ProductCategoryWrapper wrapper : productCategories) {
            String categoryName = wrapper.getName().trim();
            categoryName = categoryName.replaceAll("-"," ");
            categoryName = categoryName.replaceAll("[^0-9a-zA-Z ]", "").trim();

            if (categoryName.length() > 35) {
                categoryName =  categoryName.substring(0, 34);
            }
            categoryName = categoryName.toLowerCase();

            if (dbUniqueSequenceMap.containsKey(categoryName)) {
                Integer integer = dbUniqueSequenceMap.get(categoryName);
                String name = categoryName + "_" + (integer + 1);
                wrapper.setQbCategoryName(name);
                dbUniqueSequenceMap.put(categoryName, (integer + 1));
            } else {
                if (newUniqueSequenceMap.containsKey(categoryName)) {
                    Integer integer = newUniqueSequenceMap.get(categoryName);
                    String name = categoryName + "_" + (integer + 1);
                    wrapper.setQbCategoryName(name);
                    newUniqueSequenceMap.put(categoryName, (integer + 1));
                } else {
                    wrapper.setQbCategoryName(categoryName);
                    newUniqueSequenceMap.put(categoryName, 0);
                }
            }
            QuickBookAccountDetails entitiesAccountDetails = accountDetailsMap.get(entities.getId());
            QuickBookAccountDetails details = accountDetailsMap.get(wrapper.getId());
            if (details != null && details.isStatus()) {
                if (StringUtils.isNotBlank(details.getInventory())) {
                    wrapper.setInventoryAccount(details.getInventory());
                } else {
                    wrapper.setInventoryAccount(entitiesAccountDetails.getInventory());
                }

                if (StringUtils.isNotBlank(details.getSales())) {
                    wrapper.setIncomeAccount(details.getSales());
                } else {
                    wrapper.setIncomeAccount(entitiesAccountDetails.getSales());
                }

                if (StringUtils.isNotBlank(details.getSuppliesAndMaterials())) {
                    wrapper.setCogsAccount(details.getSuppliesAndMaterials());
                } else {
                    wrapper.setCogsAccount(entitiesAccountDetails.getSuppliesAndMaterials());
                }
            } else {
                wrapper.setIncomeAccount(entitiesAccountDetails.getSales());
                wrapper.setInventoryAccount(entitiesAccountDetails.getInventory());
                wrapper.setCogsAccount(entitiesAccountDetails.getSuppliesAndMaterials());
            }
        }


        if (newUniqueSequenceMap.size() > 0) {
            List<QBUniqueSequence> qbUniqueSequences = new ArrayList<>();
            for (String name : newUniqueSequenceMap.keySet()) {
                QBUniqueSequence qbUniqueSequence = new QBUniqueSequence();
                qbUniqueSequence.prepare(companyID);
                qbUniqueSequence.setShopId(shopID);
                qbUniqueSequence.setName(name);
                qbUniqueSequence.setCurrentSequence(newUniqueSequenceMap.get(name));

                qbUniqueSequences.add(qbUniqueSequence);
            }

            qbUniqueSequenceRepository.save(qbUniqueSequences);
        }


        if (dbUniqueSequenceMap.size() > 0) {
            for (String name : dbUniqueSequenceMap.keySet()) {
                qbUniqueSequenceRepository.updateQbUniqueSequence(companyID, shopID, name, dbUniqueSequenceMap.get(name));
            }
        }
    }

    @Override
    public String generatePullRequest(String timeZone) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        List<QuickbookSyncDetails> detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 1);

        ItemInventoryQueryRqType itemInventoryQueryRqType = new ItemInventoryQueryRqType();
        if (detailsByStatus != null && detailsByStatus.size() > 0) {
            itemInventoryQueryRqType.setFromModifiedDate(DateUtil.convertToISO8601(detailsByStatus.get(0).getEndTime(), timeZone));
        }
        // Save sync details
        quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull);
        return QBXMLParser.parseRequest(itemInventoryQueryRqType, ItemInventoryQueryRqType.class, "ItemInventoryQueryRq");
    }
}
