package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.converters.PurchaseOrderEditConvertor;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.desktop.converters.PurchaseOrderConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.PurchaseOrderWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;


public class PurchaseOrderXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderXMLService.class);

    public String qbPurchaseOrderXMLRequest(String companyID, String shopID, PurchaseOrderRepository purchaseOrderRepository, ProductRepository productRepository,
                                            QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntities entities,
                                            VendorRepository vendorRepository, QbDesktopOperationRepository qbDesktopOperationRepository,
                                            QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ErrorLogsRepository errorLogsRepository) {

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.PurchaseOrder);
        if(qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Purchase order sync is not available for shop {}, skipping", shopID);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = syncDetails.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        StringBuilder purchaserOrderQuery = new StringBuilder();
        List<PurchaseOrderWrapper> purchaseOrderWrapperList = new ArrayList<>();

        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesReceipt);
        if (latestQbDesktopCurrentSync != null) {
            purchaseOrderWrapperList = purchaseOrderRepository.getPOByLimitsWithQBError(companyID, shopID, latestQbDesktopCurrentSync.getCreated(), PurchaseOrderWrapper.class);
        }

        int start = 0;
        int limit = 1000 - purchaseOrderWrapperList.size();

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
            long startDateTime = DateTime.now().getMillis();
            long endDateTime = DateTime.now().getMillis();

            if (details != null) {
                startDateTime = details.getEndTime();
            } else  {
                startDateTime = DateTime.now().withTimeAtStartOfDay().getMillis();
            }

            SearchResult<PurchaseOrderWrapper> purchaseOrders = purchaseOrderRepository.getLimitedPurchaseOrderWithoutQbDesktopRef(companyID, shopID, startDateTime, endDateTime, PurchaseOrderWrapper.class);

            if (purchaseOrders.getValues().size() > 0) {
                purchaseOrderWrapperList = purchaseOrders.getValues();
            }

        } else {
            if (entities.getSyncTime() > 0) {
                SearchResult<PurchaseOrderWrapper> purchaseOrders = purchaseOrderRepository.getLimitedPurchaseOrderWithoutQbDesktopRef(companyID, shopID, entities.getSyncTime(), start, limit, PurchaseOrderWrapper.class);
                if (purchaseOrders.getValues().size() > 0) {
                    purchaseOrderWrapperList.addAll(purchaseOrders.getValues());
                }
            }
        }

        List<PurchaseOrderWrapper> errorOrderWrappers = new ArrayList<>();
        Map<String, String> purchaseOrderCurrentSyncDataMap = new HashMap<>();
        int total = 0;

        if (purchaseOrderWrapperList.size() > 0) {
            List<PurchaseOrderWrapper> orderWrappers = new ArrayList<>();
            List<String> poCurrentSyncIds = new ArrayList<>();
            for (PurchaseOrderWrapper wrapper : purchaseOrderWrapperList) {
                poCurrentSyncIds.add(wrapper.getPoNumber());
            }

            this.preparePurchaseOrder(companyID, shopID, productRepository, vendorRepository, purchaseOrderWrapperList, orderWrappers, errorOrderWrappers);

            if (orderWrappers.size() > 0) {
                for (PurchaseOrderWrapper orderWrapper : orderWrappers) {
                    purchaseOrderCurrentSyncDataMap.put(orderWrapper.getId(), orderWrapper.getPoNumber());
                }

                PurchaseOrderConverter purchaseOrderConverter = null;
                try {
                    purchaseOrderConverter = new PurchaseOrderConverter(orderWrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
                    total = purchaseOrderConverter.getmList().size();
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(purchaseOrderConverter));
                purchaserOrderQuery.append(qbxmlConverter.getXMLString());

            }
        }

        int modifiedLimit = 1000 - purchaseOrderWrapperList.size();
        List<PurchaseOrderWrapper> modifiedPurchaseOrders= null;

        if (modifiedLimit > 0) {
            if (syncDetailsByStatus.size() > 0 && details != null) {
                modifiedPurchaseOrders = purchaseOrderRepository.getQBExistPurchaseOrders(companyID, shopID, start, modifiedLimit, details.getStartTime(), PurchaseOrderWrapper.class);

                if (modifiedPurchaseOrders != null && modifiedPurchaseOrders.size() > 0) {
                    List<PurchaseOrderWrapper> modifiedOrderWrappers = new ArrayList<>();

                    this.preparePurchaseOrder(companyID, shopID, productRepository, vendorRepository, modifiedPurchaseOrders, modifiedOrderWrappers, errorOrderWrappers);
                    if (modifiedOrderWrappers.size() > 0) {
                        for (PurchaseOrderWrapper modifiedOrderWrapper : modifiedOrderWrappers) {
                            purchaseOrderCurrentSyncDataMap.put(modifiedOrderWrapper.getId(), modifiedOrderWrapper.getPoNumber());
                        }

                        PurchaseOrderEditConvertor editConvertor = null;
                        try {
                            editConvertor = new PurchaseOrderEditConvertor(modifiedOrderWrappers, qbDesktopOperationRef.getQbDesktopFieldMap());
                            total += editConvertor.getmList().size();
                        } catch (XMLStreamException e) {
                            e.printStackTrace();
                        }

                        QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(editConvertor));
                        purchaserOrderQuery.append(qbxmlConverter.getXMLString());
                    }
                }
            }
        }

        if (total > 0) {
            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyID, shopID, purchaseOrderCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder);
        }


        if (errorOrderWrappers.size() > 0) {
            long currentTime = DateTime.now().getMillis();
            String errorMsg = "PO's product are not synchronized";
            for (PurchaseOrderWrapper wrapper : errorOrderWrappers) {
                purchaseOrderRepository.updatePurchaseOrderQbErrorAndTime(companyID, wrapper.getPoNumber(), Boolean.TRUE, currentTime);
                saveErrorLogs(companyID, shopID, "500", errorMsg, null, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, errorLogsRepository);
            }
        }

        return purchaserOrderQuery.toString();
    }


    private void preparePurchaseOrder(String companyID, String shopID, ProductRepository productRepository, VendorRepository vendorRepository, List<PurchaseOrderWrapper> purchaseOrderWrapperList, List<PurchaseOrderWrapper> orderWrappers, List<PurchaseOrderWrapper> errorOrderWrappers) {
        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyID, shopID);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyID);

        for (PurchaseOrderWrapper purchaseOrderWrapper : purchaseOrderWrapperList) {
            if (purchaseOrderWrapper != null) {
                if (StringUtils.isNotBlank(purchaseOrderWrapper.getVendorId())) {
                    Vendor vendor = vendorHashMap.get(purchaseOrderWrapper.getVendorId());
                    if (vendor != null && vendor.getQbMapping() != null && vendor.getQbMapping().size() > 0) {
                        for (QBDataMapping dataMapping : vendor.getQbMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopID) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getQbListId())) {
                                purchaseOrderWrapper.setVendorQbRef(dataMapping.getQbDesktopRef());
                                purchaseOrderWrapper.setVendorQbListId(dataMapping.getQbListId());
                            }
                        }
                    }
                }

                if (purchaseOrderWrapper.getPoProductRequestList().size() > 0) {
                    HashMap<String, Product> availableProductMap = new HashMap<>();
                    for (POProductRequest poProductRequest : purchaseOrderWrapper.getPoProductRequestList()) {
                        if (StringUtils.isNotBlank(poProductRequest.getProductId())) {
                            Product product = productHashMap.get(poProductRequest.getProductId());
                            if (product != null && StringUtils.isNotBlank(product.getQbDesktopItemRef()) && StringUtils.isNotBlank(product.getQbListId())) {
                                if (!availableProductMap.containsKey(product.getId())) {
                                    availableProductMap.put(product.getId(), product);
                                }
                            }
                        }
                    }
                    purchaseOrderWrapper.setProductMap(availableProductMap);
                }

                if (purchaseOrderWrapper.getProductMap().size() != 0) {
                    orderWrappers.add(purchaseOrderWrapper);
                } else {
                    errorOrderWrappers.add(purchaseOrderWrapper);
                }
            }
        }
    }

}
