package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

public class QBSalesByConsumer {


    public String syncSalesByConsumerType(String companyID, String shopID, TransactionRepository transactionRepository, ProductRepository productRepository, MemberRepository memberRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, PrepackageProductItemRepository prepackageProductItemRepository, PrepackageRepository prepackageRepository, ProductWeightToleranceRepository weightToleranceRepository, ProductBatchRepository batchRepository, QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository, ProductCategoryRepository productCategoryRepository, ShopRepository shopRepository) {

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> results = null;
        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();
        Iterable<QuickbookSyncDetails> syncSalesReceiptList = quickbookSyncDetailsRepository.findByEntityType(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, QBConstants.QUICKBOOK_DESKTOP);
        LinkedList<QuickbookSyncDetails> syncSaleseceipt = Lists.newLinkedList(syncSalesReceiptList);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(companyID, shopID);
        boolean itemSync = false;
        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncSaleseceipt.isEmpty()) {
            long beforeDate = DateTime.now().getMillis() - 8640000000l + 864000000l;

            results = transactionRepository.getBracketSalesQB(companyID, shopID, beforeDate, startTime);
            itemSync = true;
        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncSaleseceipt.isEmpty()) {
            //Entry in custom entities table
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, companyID, startTime, endTime, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_DESKTOP);
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, companyID, startTime, endTime, shopID, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumerJournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_DESKTOP);
            itemSync = true;
        } else {
            QuickbookSyncDetails quickbookSyncSalesReceipt = syncSaleseceipt.getLast();
            endTime = quickbookSyncSalesReceipt.getEndTime();
            results = transactionRepository.getDailySalesforQB(companyID, shopID, endTime, startTime);


        }


        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(companyID, shopID);
        // fetch the data
        Iterable<Product> products = productRepository.list(companyID);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(companyID, shopID);
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(companyID, shopID);
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(companyID);
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(companyID);

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }


        List<Transaction> transactions = Lists.newArrayList(results);


        RevenueSummary totalRevenuesSummary = new RevenueSummary();
        RevenueSummary adultUseRevenuesSummary = new RevenueSummary();
        RevenueSummary mmicRevenuesSummary = new RevenueSummary();
        RevenueSummary otherSummary = new RevenueSummary();
        RevenueSummary thirdPartyRevenuesSummary = new RevenueSummary();

        totalRevenuesSummary.name = "Total Revenue";
        adultUseRevenuesSummary.name = "Adult Use Revenues";
        mmicRevenuesSummary.name = "Medicinal MMIC Revenues";
        thirdPartyRevenuesSummary.name = "Medicinal Third Party Revenues";
        otherSummary.name = "Other";

        Shop shop = shopRepository.get(companyID, shopID);
        // use federal

        boolean useFederal = false;
        if (shop.getTaxInfo() != null && shop.getTaxInfo().getFederalTax().doubleValue() > 0) {
            useFederal = true;
        }
        int factor = 1;
        int count = 0;
        for (Transaction ts : transactions) {
            count++;
            double cannabisRevenue = 0d;
            double nonCannabisRevenue = 0d;
            double discounts = 0;
            double exciseTax = 0;
            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;
            double federalTax = 0;
            double totalTax = 0;
            double deliveryFees = 0;
            double creditCardFees = 0d;
            double grossReceipts = 0;
            double totalCogs = 0;
            double numOfVisits = 1;
            double cannabisDiscounts = 0d;
            double nonCananbisDiscounts = 0d;
            double grossRevenue = 0d;
            double afterTaxDiscount = 0d;
            double preALExciseTax = 0.0;
            double preNALExciseTax = 0.0;
            double postALExciseTax = 0.0;
            double postNALExciseTax = 0.0;

            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            RevenueSummary curSummary = otherSummary;
            ConsumerType consumerType = ConsumerType.Other;
            if (ts.getCart().getTaxTable() != null) {
                CompoundTaxTable taxTable = ts.getCart().getTaxTable();
                consumerType = taxTable.getConsumerType();
            } else {
                if (ts.getCart().getConsumerType() != null) {
                    consumerType = ts.getCart().getConsumerType();
                }
            }

            if (consumerType == ConsumerType.MedicinalThirdParty) {
                curSummary = thirdPartyRevenuesSummary;
            } else if (consumerType == ConsumerType.MedicinalState) {
                curSummary = mmicRevenuesSummary;
            } else if (consumerType == ConsumerType.AdultUse) {
                curSummary = adultUseRevenuesSummary;
            }


            grossReceipts = ts.getCart().getTotal().doubleValue();
            discounts = ts.getCart().getTotalDiscount().doubleValue();
            deliveryFees = ts.getCart().getDeliveryFee().doubleValue();
            if (ts.getCart().getAppliedAfterTaxDiscount() != null && ts.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount().doubleValue();
            }

            if (ts.getCart().isEnableCreditCardFee() && ts.getCart().getCreditCardFee().doubleValue() > 0) {
                creditCardFees = ts.getCart().getCreditCardFee().doubleValue();
            }

            if (ts.getCart().getTaxResult() != null) {
                TaxResult taxResult = ts.getCart().getTaxResult();
                exciseTax = taxResult.getTotalExciseTax().doubleValue() + taxResult.getTotalALPostExciseTax().doubleValue();

                cityTax = NumberUtils.round(taxResult.getTotalCityTax().doubleValue(), 2);
                countyTax = NumberUtils.round(taxResult.getTotalCountyTax().doubleValue(), 2);
                stateTax = NumberUtils.round(taxResult.getTotalStateTax().doubleValue(), 2);
                federalTax = NumberUtils.round(taxResult.getTotalFedTax().doubleValue(), 2);
                totalTax = NumberUtils.round(taxResult.getTotalPostCalcTax().doubleValue(), 2);


                preALExciseTax = ts.getCart().getTaxResult().getTotalALExciseTax().doubleValue();
                postALExciseTax = ts.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();

                preNALExciseTax = ts.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue();
                postNALExciseTax = ts.getCart().getTaxResult().getTotalExciseTax().doubleValue();
            }
            totalTax = NumberUtils.round(cityTax, 3) + NumberUtils.round(countyTax, 3) +
                    NumberUtils.round(stateTax, 3) + federalTax;

            int totalFinalCost = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if ((ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && item.getStatus() == OrderItem.OrderItemStatus.Refunded)) {
                    totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
                }
            }

            // Calculate COGs & cannabis revenue
            for (OrderItem orderItem : ts.getCart().getItems()) {
                if (!(ts.getTransType() == Transaction.TransactionType.Sale
                        || (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded))) {
                    continue;
                }
                Product product = productMap.get(orderItem.getProductId());
                boolean cannabis = false;


                int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                double targetCartDiscount = ts.getCart().getCalcCartDiscount().doubleValue() * ratio;

                if (product != null && categoryHashMap.get(product.getCategoryId()) != null) {
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());

                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        cannabis = true;
                    }


                    // now calccreditCardFeesulate COGs
                    // calculate cost of goods
                    boolean calculated = false;
                    if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        // if prepackage is set, use prepackage
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null && targetBatch != null) {
                                calculated = true;

                                BigDecimal unitValue = prepackage.getUnitValue();
                                if (unitValue == null || unitValue.doubleValue() == 0) {
                                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                    unitValue = weightTolerance.getUnitValue();
                                }
                                // calculate the total quantity based on the prepackage value
                                totalCogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                            }
                        }
                    } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    totalCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                }
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                        totalCogs = unitCost * orderItem.getQuantity().doubleValue();
                    }
                }

                if (cannabis) {
                    cannabisRevenue += orderItem.getCost().doubleValue();
                    cannabisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                } else {
                    nonCannabisRevenue += orderItem.getCost().doubleValue();
                    nonCananbisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                }
            }

            double cannabisRevenueFactor = cannabisRevenue * factor;
            double nonCannabisRevenueFactor = nonCannabisRevenue * factor;
            double discountFactor = discounts * factor;
            double grossReceiptsFactor = grossReceipts * factor;
            double totalCogsFactor = totalCogs * factor;


            // apply current summary
            curSummary.cannabisRevenue += cannabisRevenueFactor;
            curSummary.nonCannabisRevenue += nonCannabisRevenueFactor;
            curSummary.cannabisDiscounts += cannabisDiscounts * factor;
            curSummary.nonCannabisDiscounts += nonCananbisDiscounts * factor;
            curSummary.discount += discountFactor;
            curSummary.afterTaxDiscount += afterTaxDiscount * factor;
            curSummary.grossRevenue += (cannabisRevenueFactor + nonCannabisRevenueFactor - discountFactor);
            curSummary.exciseTax += (preALExciseTax + preNALExciseTax + postALExciseTax + postNALExciseTax) * factor;
            curSummary.cityTax += cityTax * factor;
            curSummary.countyTax += countyTax * factor;
            curSummary.stateTax += stateTax * factor;
            curSummary.fedralTax += federalTax * factor;
            curSummary.totalTax += totalTax * factor;
            curSummary.cogs += totalCogsFactor;
            curSummary.deliveryFees += deliveryFees * factor;
            curSummary.creditCardFees += creditCardFees * factor;
            curSummary.netProfit += (grossReceiptsFactor - totalCogsFactor);
            curSummary.numberOfVisits += numOfVisits;
            curSummary.preNALExciseTax += preNALExciseTax * factor;
            curSummary.preALExciseTax += preALExciseTax * factor;
            curSummary.grossReceipt += (grossRevenue - (preALExciseTax + preNALExciseTax) + deliveryFees + totalTax + exciseTax) * factor;
        }

        if (count > 0) {

            List<RevenueSummary> revenueSummaryList = new ArrayList<RevenueSummary>();
            revenueSummaryList.add(thirdPartyRevenuesSummary);
            revenueSummaryList.add(adultUseRevenuesSummary);
            revenueSummaryList.add(otherSummary);
            revenueSummaryList.add(mmicRevenuesSummary);
            List<String> namelist = new ArrayList<String>();
            namelist.add(thirdPartyRevenuesSummary.name);
            namelist.add(adultUseRevenuesSummary.name);
            namelist.add(otherSummary.name);
            namelist.add(mmicRevenuesSummary.name);
            String productReq = "";
            if (itemSync) {
                productReq = createProductByConsumer(namelist, entities);
            }

            String salesReq = createSalesByConsumerInQB(revenueSummaryList, entities, quickbookSyncDetailsRepository, companyID, shopID);
            String journalEntry = createSalesByCategoryJEInQB(revenueSummaryList, entities, quickbookSyncDetailsRepository, companyID, shopID);
            String syncAll = productReq + salesReq + journalEntry;
            return syncAll;
        }

        return "";
    }

    String createProductByConsumer(List<String> namelist, QuickbookCustomEntities entities) {

        String itemQuery = "";
        for (String productName : namelist) {
            itemQuery = itemQuery + "<ItemInventoryAddRq requestID=\"'" + RandomStringUtils.randomNumeric(3) + "'\">" +
                    "<ItemInventoryAdd>";
            itemQuery = itemQuery + "<Name>" + productName + "</Name>";
            itemQuery = itemQuery + "<SalesDesc></SalesDesc>" +
                    "<SalesPrice>1</SalesPrice>" +
                    "<IncomeAccountRef>" +
                    "<FullName>" + entities.getSales() + "</FullName>" +
                    "</IncomeAccountRef>" +
                    "<COGSAccountRef>" +
                    "<FullName>" + entities.getSuppliesandMaterials() + "</FullName>" +
                    "</COGSAccountRef>" +
                    "<AssetAccountRef>" +
                    "<FullName>" + entities.getInventory() + "</FullName>" +
                    "</AssetAccountRef>" +
                    "</ItemInventoryAdd>" +
                    "</ItemInventoryAddRq>";
        }

        return itemQuery;
    }


    String createSalesByConsumerInQB(List<RevenueSummary> revenueSummaryList, QuickbookCustomEntities entities, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, String companyID, String shopID) {

        String salesReceiptXml = "";
        int count = 10;
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateFormat = formatter.format(date);
        int total = revenueSummaryList.size();
        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();
        int success = 0;
        int totalFail = 0;
        for (RevenueSummary revenue : revenueSummaryList) {
            count++;
            salesReceiptXml = salesReceiptXml + " <SalesReceiptAddRq requestID=\"'" + RandomStringUtils.randomNumeric(3) + "'\" >" +
                    "      <SalesReceiptAdd>";
            salesReceiptXml = salesReceiptXml + "<TxnDate>" + dateFormat + "</TxnDate>" +
                    "        <RefNumber>" + RandomStringUtils.randomNumeric(3) + "</RefNumber>";
            salesReceiptXml = salesReceiptXml + "<ShipAddress>" +
                    "        </ShipAddress>" +
                    "        <IsToBePrinted>true</IsToBePrinted>" +
                    "        <IsToBeEmailed>false</IsToBeEmailed>";


            salesReceiptXml = salesReceiptXml + "<SalesReceiptLineAdd>" +
                    "          <ItemRef>";
            salesReceiptXml = salesReceiptXml + "<FullName>" + revenue.name + "</FullName>";
            salesReceiptXml = salesReceiptXml + "</ItemRef>" +
                    "          <Quantity>1</Quantity>" +
                    "          <Amount>" + new BigDecimal(revenue.cannabisRevenue + revenue.nonCannabisRevenue - (revenue.preALExciseTax + revenue.preNALExciseTax)).setScale(2, RoundingMode.HALF_UP) + "</Amount>" +
                    "          <SalesTaxCodeRef>" +
                    "            <FullName>TAX</FullName>" +
                    "          </SalesTaxCodeRef>" +
                    "        </SalesReceiptLineAdd>";


            // Sales Tax
            salesReceiptXml = salesReceiptXml + "<SalesReceiptLineAdd>" +
                    "          <ItemRef>" +
                    "            <FullName>Sales Tax</FullName>" +
                    "          </ItemRef>" +
                    "          <Amount>" + new BigDecimal(revenue.totalTax).setScale(2, RoundingMode.HALF_UP) + "</Amount>" +
                    "        </SalesReceiptLineAdd>";


            salesReceiptXml = salesReceiptXml + "<SalesReceiptLineAdd>" +
                    "          <ItemRef>" +
                    "            <FullName>Delivery Charge</FullName>" +
                    "          </ItemRef>" +
                    "          <Quantity>1</Quantity>" +
                    "          <Rate>" + NumberUtils.round(revenue.deliveryFees, 2) + "</Rate>" +
                    "          <SalesTaxCodeRef>" +
                    "            <FullName>TAX</FullName>" +
                    "          </SalesTaxCodeRef>" +
                    "        </SalesReceiptLineAdd>";

            salesReceiptXml = salesReceiptXml + "<SalesReceiptLineAdd>" +
                    "          <ItemRef>" +
                    "            <FullName>Discount</FullName>" +
                    "          </ItemRef>" +
                    "          <Quantity>-1</Quantity>" +
                    "          <Rate>" + NumberUtils.round(revenue.discount, 2) + "</Rate>" +
                    "          <SalesTaxCodeRef>" +
                    "            <FullName>TAX</FullName>" +
                    "          </SalesTaxCodeRef>" +
                    "        </SalesReceiptLineAdd>";

            // Excise Tax add
            salesReceiptXml = salesReceiptXml + "<SalesReceiptLineAdd>" +
                    "          <ItemRef>" +
                    "            <FullName>Total Excise Tax</FullName>" +
                    "          </ItemRef>" +
                    "          <Quantity>1</Quantity>" +
                    "          <Rate>" + NumberUtils.round(revenue.exciseTax, 2) + "</Rate>" +
                    "          <SalesTaxCodeRef>" +
                    "            <FullName>TAX</FullName>" +
                    "          </SalesTaxCodeRef>" +
                    "        </SalesReceiptLineAdd>";

            //Credit card fees
            salesReceiptXml = salesReceiptXml + "<SalesReceiptLineAdd>" +
                    "          <ItemRef>" +
                    "            <FullName>Credit Card Fees</FullName>" +
                    "          </ItemRef>" +
                    "          <Quantity>1</Quantity>" +
                    "          <Rate>" + NumberUtils.round(revenue.creditCardFees, 2) + "</Rate>" +
                    "          <SalesTaxCodeRef>" +
                    "            <FullName>TAX</FullName>" +
                    "          </SalesTaxCodeRef>" +
                    "        </SalesReceiptLineAdd>";


            salesReceiptXml = salesReceiptXml + "</SalesReceiptAdd>" +
                    "</SalesReceiptAddRq>";
            success++;
        }
        totalFail = total - success;


        quickbookSyncDetailsRepository.saveQuickbookEntity(total, success, totalFail, companyID, endTime, startTime, shopID,
                QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_DESKTOP);
        return salesReceiptXml;
    }

    String createSalesByCategoryJEInQB(List<RevenueSummary> revenueSummaryList, QuickbookCustomEntities entities, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, String companyID, String shopID) {

        String salesReceiptXmlNew = "";
        int count = 0;
        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();
        int total = revenueSummaryList.size();
        int success = 0;
        int totalFail = 0;
        for (RevenueSummary revenue : revenueSummaryList) {
            if (revenue.grossReceipt > 0) {
                String salesReceiptXml = "";
                salesReceiptXml = salesReceiptXml + "<JournalEntryAddRq requestID=\"'" + RandomStringUtils.randomNumeric(3) + "'\"> " +
                        "<JournalEntryAdd > " +
                        "<TxnDate >2018-07-27</TxnDate>  " +
                        "<RefNumber >" + count + "</RefNumber>  " +
                        "<JournalDebitLine>  " +
                        "<AccountRef>  " +
                        "<FullName>" + entities.getSuppliesandMaterials() + "</FullName>  " +
                        "</AccountRef>" +
                        "<Amount>" + new BigDecimal(revenue.cogs).setScale(2, RoundingMode.HALF_UP) + "</Amount>  " +
                        "<Memo >" + revenue.name + "</Memo>  " +
                        "</JournalDebitLine>" +
                        "<JournalCreditLine>  " +
                        "<AccountRef>  " +
                        "<FullName>" + entities.getInventory() + "</FullName>  " +
                        "</AccountRef>" +
                        "<Amount >" + new BigDecimal(revenue.cogs).setScale(2, RoundingMode.HALF_UP) + "</Amount>  " +
                        "<Memo >" + revenue.name + "</Memo>  " +

                        "</JournalCreditLine>" +


                        "</JournalEntryAdd>" +
                        "</JournalEntryAddRq>";


                salesReceiptXmlNew += salesReceiptXml;
                count++;
                success++;
            }
        }
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, success, totalFail, companyID, endTime, startTime, shopID,
                QuickbookSyncDetails.QuickbookEntityType.SalesByConsumerJournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_DESKTOP);
        return salesReceiptXmlNew;
    }

    private String prepareAmount(double amount) {
        return String.format("$%.2f", amount);
    }

    public class RevenueSummary {
        public String name = "";
        double revenue = 0d;
        double cannabisRevenue = 0d;
        double nonCannabisRevenue = 0d;
        double cannabisDiscounts = 0d;
        double nonCannabisDiscounts = 0d;
        double grossRevenue = 0d;
        double discount = 0d;
        double afterTaxDiscount = 0d;
        double exciseTax = 0d;
        double cityTax = 0d;
        double countyTax = 0d;
        double stateTax = 0d;
        double fedralTax = 0d;
        double totalTax = 0d;
        double grossReceipt = 0d;
        double cogs = 0d;
        double netProfit = 0d;
        double numberOfVisits = 0d;
        double deliveryFees = 0d;
        double creditCardFees = 0d;
        public double preALExciseTax = 0.0;
        public double preNALExciseTax = 0.0;
    }


    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

}
