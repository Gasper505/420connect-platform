package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.desktop.converters.ProductCategoryJournalEntryConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.CategorySaleWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.JournalEntryProductCategoryWrapper;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.*;

public class QBSalesByProductCategory {
    private static final Logger LOGGER = LoggerFactory.getLogger(QBSalesByProductCategory.class);

    public String syncSalesByProductCategory(String companyId, String shopId, TransactionRepository transactionRepository, ProductRepository productRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, PrepackageProductItemRepository prepackageProductItemRepository, PrepackageRepository prepackageRepository, ProductWeightToleranceRepository weightToleranceRepository, ProductBatchRepository batchRepository, ProductCategoryRepository categoryRepository, QuickbookCustomEntities entities, QbDesktopOperationRepository qbDesktopOperationRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.ProductCategoryJournalEntry);
        if(qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Item sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> transactions = null;
        int counter = 0;
        long startTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
            if (details != null) {
                startTime = details.getEndTime();
            } else  {
                startTime = DateTime.now().withTimeAtStartOfDay().getMillis();
            }
            transactions = transactionRepository.getBracketSalesQB(companyId, shopId, startTime, endTime);
        } else {
            if (details != null) {
                startTime = details.getEndTime();
            } else {
                startTime = entities.getSyncTime();
            }
            transactions = transactionRepository.getBracketSalesQB(companyId, shopId, startTime, endTime);
        }

        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(companyId, shopId);
        HashMap<String, ProductBatch> batchHashMap = batchRepository.listAllAsMap(companyId);
        HashMap<String, CategorySale> results = new HashMap<>();
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(companyId, shopId);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(companyId, shopId);
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(companyId);


        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        Iterable<Product> products = productRepository.listAll(companyId);
        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        int factor = 1;
        List<Transaction> transactionList = Lists.newArrayList(transactions);
        if (transactionList.size() > 0) {
            for (Transaction t : transactionList) {
                factor = 1;
                counter++;
                if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                    factor = -1;
                }

                // Calculate discounts proportionally so we can apply taxes

                HashMap<String, Double> propRatioMap = new HashMap<>();

                if (t.getCart().getItems().size() > 0) {
                    double total = 0.0;
                    for (OrderItem orderItem : t.getCart().getItems()) {
                        Product product = productMap.get(orderItem.getProductId());
                        if (product != null) {
                            if (product.isDiscountable()) {
                                total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                            }
                        }
                    }

                    // Calculate the ratio to be applied
                    for (OrderItem orderItem : t.getCart().getItems()) {
                        if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                                && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                            continue;
                        }
                        Product product = productMap.get(orderItem.getProductId());
                        if (product != null) {

                            propRatioMap.put(orderItem.getId(), 1d); // 100 %
                            if (product.isDiscountable() && total > 0) {
                                double finalCost = orderItem.getFinalPrice().doubleValue();
                                double ratio = finalCost / total;

                                propRatioMap.put(orderItem.getId(), ratio);
                            }
                        }

                    }
                }

                double totalPreTax = 0;
                double cTotal = 0;
                List<String> visited = new ArrayList<>();
                for (OrderItem item : t.getCart().getItems()) {

                    if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                        continue;
                    }

                    double taxRate = 0d;

                    taxRate = t.getCart().getTax().doubleValue();
                    if (item.getTaxInfo() != null
                            && item.getTaxType() == TaxInfo.TaxType.Custom
                            && item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        taxRate = item.getTaxInfo().getTotalTax().doubleValue();
                    }
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PreTaxed) {
                        taxRate = 0;
                    }

                    Product p = productMap.get(item.getProductId());
                    if (p != null) {
                        totalPreTax += item.getCalcPreTax().doubleValue();

                        ProductCategory productCategory = categoryMap.get(p.getCategoryId());
                        String categoryId = productCategory.getId();
                        CategorySale categorySale = results.get(productCategory.getId());
                        if (categorySale == null) {
                            categorySale = new CategorySale();
                            results.put(productCategory.getId(), categorySale);
                        }
                        // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                        if (!visited.contains(p.getCategoryId())) {
                            categorySale.transCount++;
                            visited.add(p.getCategoryId());
                        }


                        Double ratio = propRatioMap.get(item.getId());
                        if (ratio == null) {
                            ratio = 1d;
                        }

                        Double propCartDiscount = t.getCart().getCalcCartDiscount() != null ? t.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                        Double propDeliveryFee = t.getCart().getDeliveryFee() != null ? t.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                        Double propCCFee = t.getCart().getCreditCardFee() != null ? t.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                        Double propAfterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount() != null ? t.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;

                        cTotal += propCartDiscount;

                        // calculated postTax
                        double itemDiscount = item.getCalcDiscount().doubleValue();


                        double expectedFinalCost = (item.getCost().doubleValue() - itemDiscount);
                        if (NumberUtils.round(expectedFinalCost, 2) != NumberUtils.round(item.getFinalPrice().doubleValue(), 2)) {
                            System.out.println("Difference in final cost");
                        }

                        double sales = NumberUtils.round(item.getFinalPrice().doubleValue() - propCartDiscount, 5);


                        double postTax = 0;

                        double preALExciseTax = 0;
                        double preNALExciseTax = 0;
                        double postALExciseTax = 0;
                        double postNALExciseTax = 0;
                        double totalCityTax = 0;
                        double totalCountyTax = 0;
                        double totalStateTax = 0;
                        double totalFedTax = 0;

                        double subTotalAfterdiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                        if (item.getTaxResult() != null) {
                            preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                            preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                            postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                            postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                                totalCityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                                totalStateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                                totalCountyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                                totalFedTax = item.getTaxResult().getTotalFedTax().doubleValue();

                                postTax += item.getTaxResult().getTotalPostCalcTax().doubleValue() + postALExciseTax + postNALExciseTax;
                            }
                        }
                        if (postTax == 0) {
                            if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                                TaxInfo taxInfo = item.getTaxInfo();
                                if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                                    totalCityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                                    totalStateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                                    totalFedTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();

                                    postTax = totalCityTax + totalStateTax + totalFedTax;
                                }
                            }
                        }

                        if (postTax == 0) {
                            postTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                        }

                        double totalTax = postTax;

                        categorySale.categoryId = categoryId;
                        categorySale.retailValue += item.getCost().doubleValue() * factor;
                        categorySale.itemDiscounts += itemDiscount * factor;
                        categorySale.subTotals += item.getFinalPrice().doubleValue() * factor;
                        categorySale.discounts += propCartDiscount * factor;
                        categorySale.sales += sales * factor;

                        categorySale.totalPreTax += totalPreTax * factor;
                        categorySale.preALExciseTax += preALExciseTax * factor;
                        categorySale.preNALExciseTax += preNALExciseTax * factor;
                        categorySale.postALExciseTax += postALExciseTax * factor;
                        categorySale.postNALExciseTax += postNALExciseTax * factor;
                        categorySale.cityTax += totalCityTax * factor;
                        categorySale.countyTax += totalCountyTax * factor;
                        categorySale.stateTax += totalStateTax * factor;
                        categorySale.federalTax += totalFedTax * factor;
                        categorySale.tax += totalTax * factor;

                        categorySale.deliveryFees += propDeliveryFee * factor;
                        categorySale.creditCardFees += propCCFee * factor;
                        categorySale.afterTaxDiscount += propAfterTaxDiscount * factor;

                        double gross = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4) + NumberUtils.round(propDeliveryFee, 4) +
                                NumberUtils.round(totalTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);
                        categorySale.grossReceipt += gross * factor;

                        // caclulate cost of goods
                        boolean calculated = false;
                        double cogs = 0;
                        if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                            // if prepackage is set, use prepackage
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null && targetBatch != null) {
                                    calculated = true;

                                    BigDecimal unitValue = prepackage.getUnitValue();
                                    if (unitValue == null || unitValue.doubleValue() == 0) {
                                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                        unitValue = weightTolerance.getUnitValue();
                                    }
                                    // calculate the total quantity based on the prepackage value
                                    cogs += calcCOGS(item.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                }
                            }
                        } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                            // otherwise, use quantity logs
                            for (QuantityLog quantityLog : item.getQuantityLogs()) {
                                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                    ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                    if (targetBatch != null) {
                                        calculated = true;
                                        cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                    }
                                }
                            }
                        }

                        if (!calculated) {
                            double unitCost = getUnitCost(p, recentBatchMap, productsByCompanyLinkId);
                            cogs = unitCost * item.getQuantity().doubleValue();
                        }

                        categorySale.cogs += cogs * factor;
                    }
                }

                int cartDiscount = (int) (t.getCart().getCalcCartDiscount().doubleValue() * 100);
                int myCTotal = (int) (NumberUtils.round(cTotal, 2) * 100);
                if (myCTotal != cartDiscount) {
                    System.out.println("cart discount doesn't match");
                }
            }
        }


        StringBuilder syncAll = new StringBuilder();
        if (counter > 0) {
            List<CategorySaleWrapper> categorySaleWrappers = new ArrayList<>();
            for (String categoryId : results.keySet()) {
                CategorySale categorySale = results.get(categoryId);
                ProductCategory productCategory = categoryMap.get(categorySale.categoryId);
                if (productCategory != null && StringUtils.isNotBlank(productCategory.getQbDesktopRef())) {
                    CategorySaleWrapper wrapper = new CategorySaleWrapper();
                    wrapper.setCategoryId(productCategory.getId());
                    wrapper.setQbCategoryName(productCategory.getQbDesktopRef());
                    double gross = NumberUtils.round(categorySale.subTotals, 2) + NumberUtils.round(categorySale.deliveryFees, 2)
                            + NumberUtils.round(categorySale.tax, 2) - NumberUtils.round(categorySale.discounts, 2);
                    wrapper.setCogs(gross);
//                    wrapper.setDiscounts(categorySale.discounts);
//                    wrapper.setExciseTax(categorySale.exciseTax);
//                    wrapper.setDeliveryFees(categorySale.deliveryFees);
                    wrapper.setCreditCardFees(categorySale.creditCardFees);
                    categorySaleWrappers.add(wrapper);
                }
            }


            QuickBookAccountDetails entitiesAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, entities.getId());
            int total = 0;
            if (categorySaleWrappers.size() > 0) {
                List<JournalEntryProductCategoryWrapper> salesByCategoryJEInQB = createSalesByCategoryJEInQB(categorySaleWrappers, entitiesAccountDetails);
                ProductCategoryJournalEntryConverter productCategoryJournalEntryConverter = null;
                try {
                    productCategoryJournalEntryConverter = new ProductCategoryJournalEntryConverter(salesByCategoryJEInQB, qbDesktopOperationRef.getQbDesktopFieldMap());
                    total = productCategoryJournalEntryConverter.getmList().size();
                } catch (XMLStreamException e) {
                  e.printStackTrace();
                }

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(productCategoryJournalEntryConverter));
                syncAll.append(qbxmlConverter.getXMLString());
            }

            if (total > 0) {
                quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            }
        }
        return syncAll.toString();
    }



    private List<JournalEntryProductCategoryWrapper> createSalesByCategoryJEInQB(List<CategorySaleWrapper> categorySaleList, QuickBookAccountDetails entitiesAccountDetails) {
        List<JournalEntryProductCategoryWrapper> journalEntryWrappers = new ArrayList<>();
        for (CategorySaleWrapper wrapper : categorySaleList) {
            JournalEntryProductCategoryWrapper journalEntryWrapper = new JournalEntryProductCategoryWrapper();
            double cogs = wrapper.getCogs();
            journalEntryWrapper.setQbCategoryRef(wrapper.getQbCategoryName());
            journalEntryWrapper.setDebitAccountRef(entitiesAccountDetails.getChecking());
            journalEntryWrapper.setDebitAccountAmount(new BigDecimal(cogs));
            journalEntryWrapper.setCreditAccountRef(entitiesAccountDetails.getInventory());
            journalEntryWrapper.setCreditAccountAmount(new BigDecimal(cogs));
            journalEntryWrapper.setDiscounts(wrapper.getDiscounts());
            journalEntryWrapper.setExciseTax(wrapper.getExciseTax());
            journalEntryWrapper.setCreditCardFees(wrapper.getCreditCardFees());
            journalEntryWrapper.setDeliveryFees(wrapper.getDeliveryFees());

            journalEntryWrappers.add(journalEntryWrapper);
        }

        return journalEntryWrappers;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public static final class CategorySale {
        public String categoryId;
        public double retailValue = 0;
        public double itemDiscounts = 0;
        public double subTotals = 0;
        public double discounts = 0;
        public double sales = 0;
        public double deliveryFees = 0;
        public double creditCardFees = 0;
        public double totalPreTax = 0;
        double preALExciseTax = 0;
        double preNALExciseTax = 0;
        double postALExciseTax = 0;
        double postNALExciseTax = 0;
        public double cityTax = 0;
        public double stateTax = 0;
        public double countyTax = 0;
        public double tax = 0;
        public double grossReceipt = 0;
        public double cogs = 0;
        public double transCount = 0;
        public double exciseTax = 0;
        public double afterTaxDiscount = 0;
        public double federalTax = 0;
    }

}
