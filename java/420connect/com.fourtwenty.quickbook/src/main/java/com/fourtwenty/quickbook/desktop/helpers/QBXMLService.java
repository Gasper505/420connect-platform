package com.fourtwenty.quickbook.desktop.helpers;

public interface QBXMLService {
    String generatePushRequest();
    String generatePullRequest(String timeZone);
}
