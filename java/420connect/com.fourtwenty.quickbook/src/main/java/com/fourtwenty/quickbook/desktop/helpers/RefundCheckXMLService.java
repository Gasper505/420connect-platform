package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.converters.CheckPaymentConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.CheckWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.*;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.*;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;

public class RefundCheckXMLService implements QBXMLService {
    private static final Logger LOG = LoggerFactory.getLogger(RefundCheckXMLService.class);

    private String companyId;
    private String shopId;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private TransactionRepository transactionRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QuickbookCustomEntities customEntities;
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;
    private MemberRepository memberRepository;

    public RefundCheckXMLService(String companyId, String shopId, QbDesktopOperationRepository qbDesktopOperationRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ErrorLogsRepository errorLogsRepository, QuickbookCustomEntities customEntities, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository, MemberRepository memberRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.transactionRepository = transactionRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.customEntities = customEntities;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
        this.memberRepository = memberRepository;
    }

    @Override
    public String generatePushRequest() {
        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.Expenses);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOG.warn("Payment received sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        StringBuilder checkPayment = new StringBuilder();
        int start = 0;
        int limit = 1000;

        List<Transaction> transactions = new ArrayList<>();
        if (customEntities.getSyncTime() > 0) {
            if (details != null && details.getEndTime() > 0) {
                List<Transaction> transactionList = transactionRepository.getRefundTransactionByLimitsWithQBRefundPaymentError(companyId, shopId, customEntities.getSyncTime(), details.getEndTime(), Transaction.class);
                if (transactionList.size() > 0) {
                    transactions.addAll(transactionList);
                }
            }

            limit = limit - transactions.size();
            List<Transaction> refundedTransactions = transactionRepository.getLimitedTransactionWithoutRefundPayment(companyId, shopId, start, limit, Transaction.class);
            if (refundedTransactions.size() > 0) {
                transactions.addAll(refundedTransactions);
            }
        }


        Set<ObjectId> memberIds = new HashSet<>();
        for (Transaction transaction : transactions) {
            memberIds.add(new ObjectId(transaction.getMemberId()));
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(companyId, Lists.newArrayList(memberIds));

        QuickBookAccountDetails entitiesAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, customEntities.getId());
        Map<String, String> currentSyncPaymentReceived = new HashMap<>();
        List<CheckWrapper> successReceivedWrapper = new ArrayList<>();
        List<String> failedTransactionIds = new ArrayList<>();
        if (transactions.size() > 0) {
            for (Transaction transaction : transactions) {
                if (transaction != null) {
                    Member member = memberHashMap.get(transaction.getMemberId());
                    if (member != null && member.getQbMapping() != null) {
                        QBDataMapping qbDataMapping = null;
                        for (QBDataMapping qbMapping : member.getQbMapping()) {
                            if (qbMapping.getShopId().equalsIgnoreCase(shopId)) {
                                qbDataMapping = qbMapping;
                                break;
                            }
                        }

                        if (qbDataMapping != null) {
                            CheckWrapper checkWrapper = new CheckWrapper();
                            checkWrapper.setAccountRef(entitiesAccountDetails.getChecking());
                            checkWrapper.setAccountReceivable(entitiesAccountDetails.getReceivable());
                            checkWrapper.setPayeeEntityRef(qbDataMapping.getQbDesktopRef());
                            checkWrapper.setPayeeRefListId(qbDataMapping.getQbListId());
                            checkWrapper.setRefNumber(transaction.getTransNo());
                            checkWrapper.setTxnDate(transaction.getProcessedTime());
                            checkWrapper.setTxnId(transaction.getTxnId());
                            if (transaction.getCart().getTaxResult() != null && transaction.getCart().getTaxResult().getTotalFedTax().doubleValue() > 0) {
                                BigDecimal total = transaction.getCart().getTotal().subtract(transaction.getCart().getTaxResult().getTotalFedTax());
                                checkWrapper.setAmount(total);
                            } else {
                                checkWrapper.setAmount(transaction.getCart().getTotal());
                            }
                            successReceivedWrapper.add(checkWrapper);
                            currentSyncPaymentReceived.put(transaction.getTransNo(), transaction.getId());
                        }

                    }
                } else {
                    failedTransactionIds.add(transaction.getId());
                    }
                }
            }

        int total = 0;
        if (successReceivedWrapper.size() > 0) {
            CheckPaymentConverter checkPaymentConverter = null;
            try {
                checkPaymentConverter = new CheckPaymentConverter(successReceivedWrapper, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = checkPaymentConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(checkPaymentConverter));
            checkPayment.append(qbxmlConverter.getXMLString());
        }

        if (total > 0) {
            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyId, shopId, currentSyncPaymentReceived, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Expenses);
        }

        if (failedTransactionIds.size() > 0) {
            String errorMsg = "Payment's received are not synchronized";
            for (String transactionId : failedTransactionIds) {

                transactionRepository.updateTransactionQbRefundPaymentErrorAndTime(companyId, shopId, transactionId, false, true, DateTime.now().getMillis());
                saveErrorLogs(companyId, shopId, "500", errorMsg, null, QuickbookSyncDetails.QuickbookEntityType.Expenses, errorLogsRepository);
            }
        }

        return checkPayment.toString();
    }

    @Override
    public String generatePullRequest(String timeZone) {
        return null;
    }
}
