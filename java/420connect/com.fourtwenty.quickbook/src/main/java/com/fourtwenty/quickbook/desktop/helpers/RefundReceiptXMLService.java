package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.desktop.converters.CreditMemoConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.TransactionWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;


public class RefundReceiptXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RefundReceiptXMLService.class);

    public String qbRefundReceiptService(String companyID, String shopID, TransactionRepository transactionRepository, ProductRepository productRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntities entities, MemberRepository memberRepository, QbDesktopOperationRepository qbDesktopOperationRepository,
                                         PrepackageRepository prepackageRepository, PrepackageProductItemRepository prepackageProductItemRepository, ProductWeightToleranceRepository productWeightToleranceRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        String qbRefundReceiptRequest = "";

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.Refund);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Sale sync is not available for shop {}, skipping", shopID);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = syncDetails.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        List<TransactionWrapper> transactionWrapperList = new ArrayList<>();
        int start = 0;
        int limit = 1000;

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
            long startDateTime = DateTime.now().getMillis();
            long endDateTime = DateTime.now().getMillis();

            if (details != null) {
                startDateTime = details.getEndTime();
            } else {
                startDateTime = DateTime.now().withTimeAtStartOfDay().getMillis();
            }

            SearchResult<TransactionWrapper> transactions = transactionRepository.getLimitedTransactionWithoutQbRefundReceipt(companyID, shopID, startDateTime, endDateTime, Transaction.TransactionType.Refund, TransactionWrapper.class);
            if (transactions.getValues().size() > 0) {
                transactionWrapperList = transactions.getValues();
            }

        } else {
            if (entities.getSyncTime() > 0) {
                SearchResult<TransactionWrapper> transactions = transactionRepository.getLimitedTransactionWithoutQbRefundReceipt(companyID, shopID, start, limit, entities.getSyncTime(), Transaction.TransactionType.Refund, TransactionWrapper.class);
                if (transactions.getValues().size() > 0) {
                    transactionWrapperList = transactions.getValues();
                }
            }
        }

        if (transactionWrapperList.size() > 0) {
            List<ObjectId> prepackageItemIds = new ArrayList<>();
            List<ObjectId> memberIds = new ArrayList<>();
            Map<String, HashMap<String, Product>> productHashMapForTransactions = new HashMap<>();
            HashMap<String, Product> productHashMap = productRepository.listAsMap(companyID, shopID);
            for (TransactionWrapper transactionWrapper : transactionWrapperList) {
                if (transactionWrapper.getCart() != null && transactionWrapper.getCart().getItems().size() > 0) {
                    HashMap<String, Product> stringProductHashMap = new HashMap<>();
                    for (OrderItem item : transactionWrapper.getCart().getItems()) {
                        Product product = productHashMap.get(item.getProductId());
                        if (product != null && StringUtils.isNotBlank(product.getQbDesktopItemRef()) && StringUtils.isNotBlank(product.getQbListId())) {
                            if (!stringProductHashMap.containsKey(item.getProductId())) {
                                stringProductHashMap.put(item.getProductId(), product);
                            }
                        }

                        if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                            prepackageItemIds.add(new ObjectId(item.getPrepackageItemId()));
                        }
                    }

                    if (stringProductHashMap.size() > 0) {
                        productHashMapForTransactions.put(transactionWrapper.getId(), stringProductHashMap);
                    }
                }

                if (StringUtils.isNotBlank(transactionWrapper.getMemberId())) {
                    memberIds.add(new ObjectId(transactionWrapper.getMemberId()));
                }
            }

            Map<String, String> refundTransactionCurrentSyncDataMap = new HashMap<>();
            List<TransactionWrapper> newRefundTransactionWrapperList = new ArrayList<>();
            HashMap<String, Member> memberHashMap = memberRepository.listAsMap(companyID, memberIds);
            for (TransactionWrapper transactionWrapper : transactionWrapperList) {
                if (StringUtils.isNotBlank(transactionWrapper.getMemberId())) {
                    Member member = memberHashMap.get(transactionWrapper.getMemberId());
                    if (member != null && member.getQbMapping() != null && member.getQbMapping().size() > 0) {
                        for (QBDataMapping dataMapping : member.getQbMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopID) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getQbListId())) {
                                transactionWrapper.setMemberQbRef(dataMapping.getQbDesktopRef());
                                transactionWrapper.setMemberListId(dataMapping.getQbListId());
                            }
                        }
                    }
                }
                if (productHashMapForTransactions.containsKey(transactionWrapper.getId())) {
                    HashMap<String, Product> stringProductHashMap = productHashMapForTransactions.get(transactionWrapper.getId());
                    if (stringProductHashMap.size() > 0) {
                        transactionWrapper.setProductMap(stringProductHashMap);
                        newRefundTransactionWrapperList.add(transactionWrapper);
                        refundTransactionCurrentSyncDataMap.put(transactionWrapper.getId(), transactionWrapper.getTransNo());
                    }
                } else {
                    transactionRepository.updateTransactionQbRefundErrorAndTime(companyID, shopID, transactionWrapper.getTransNo(),true, DateTime.now().getMillis());
                    String message = "At least one product is not sync in quickBook for " + transactionWrapper.getTransNo();
                    saveErrorLogs(companyID, shopID, "500", message, null, QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, errorLogsRepository);
                }
            }

            if (newRefundTransactionWrapperList.size() > 0) {
                this.updateRefundTransactionQuantityForPrepackage(companyID, prepackageItemIds, newRefundTransactionWrapperList, prepackageRepository, prepackageProductItemRepository, productWeightToleranceRepository);

                CreditMemoConverter creditMemoConverter = null;
                try {
                    creditMemoConverter = new CreditMemoConverter(newRefundTransactionWrapperList, qbDesktopOperationRef.getQbDesktopFieldMap());
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }

                QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(creditMemoConverter.getmList().size(), 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
                saveCurrentSync(companyID, shopID, refundTransactionCurrentSyncDataMap, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt);

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(creditMemoConverter));
                qbRefundReceiptRequest = qbxmlConverter.getXMLString();
            }

        }
        return qbRefundReceiptRequest;
    }


    // If order item is prepackage then we can change quantity into units base quantity
    private void updateRefundTransactionQuantityForPrepackage(String companyId, List<ObjectId> prepackageItemIds, List<TransactionWrapper> transactionWrappers, PrepackageRepository prepackageRepository, PrepackageProductItemRepository prepackageProductItemRepository, ProductWeightToleranceRepository productWeightToleranceRepository) {
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(companyId, prepackageItemIds);
        Set<ObjectId> prepackageIds = new HashSet<>();
        for (String itemId : prepackageProductItemHashMap.keySet()) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(itemId);
            prepackageIds.add(new ObjectId(prepackageProductItem.getPrepackageId()));
        }

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(companyId, Lists.newArrayList(prepackageIds));
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = productWeightToleranceRepository.listAsMap();

        for (TransactionWrapper transactionWrapper : transactionWrappers) {
            if (transactionWrapper.getCart() != null && transactionWrapper.getCart().getItems() != null
                    && transactionWrapper.getCart().getItems().size() > 0) {
                for (OrderItem item : transactionWrapper.getCart().getItems()) {
                    if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                        if (prepackageProductItem != null && StringUtils.isNotBlank(prepackageProductItem.getPrepackageId())) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null && StringUtils.isNotBlank(prepackage.getToleranceId())) {
                                ProductWeightTolerance productWeightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                if (productWeightTolerance != null ) {
                                    BigDecimal newQuantity = item.getQuantity().multiply(productWeightTolerance.getUnitValue());
                                    item.setQuantity(newQuantity);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
