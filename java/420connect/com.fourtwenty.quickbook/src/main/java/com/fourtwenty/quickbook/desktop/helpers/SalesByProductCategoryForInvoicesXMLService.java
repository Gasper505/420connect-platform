package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByProductCategory;
import com.fourtwenty.quickbook.desktop.converters.ProductCategoryJournalEntryConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.CategorySaleWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.JournalEntryProductCategoryWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SalesByProductCategoryForInvoicesXMLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalesByProductCategoryForInvoicesXMLService.class);

    public String syncSalesByProductCategoryForInvoices(String companyId, String shopId, InvoiceRepository invoiceRepository, ProductRepository productRepository, ProductCategoryRepository productCategoryRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntities entities, QbDesktopOperationRepository qbDesktopOperationRepository, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.ProductCategoryJournalEntry);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Item sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntryForInvoice, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntryForInvoice, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        List<Invoice> invoiceList = new ArrayList<>();
        long startTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        if (entities.getSyncTime() > 0) {
            if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE)) {
                if (details != null) {
                    startTime = details.getEndTime();
                } else {
                    startTime = DateTime.now().withTimeAtStartOfDay().getMillis();
                }

                Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(companyId, shopId, "{modified:-1}", startTime, endTime);
                invoiceList.addAll(Lists.newArrayList(invoiceResult));
            } else {
                if (details != null) {
                    startTime = details.getEndTime();
                } else {
                    startTime = entities.getSyncTime();
                }
                Iterable<Invoice> invoiceResult = invoiceRepository.getInvoicesNonDraft(companyId, shopId, "{modified:-1}", startTime, endTime);
                invoiceList.addAll(Lists.newArrayList(invoiceResult));
            }
        }

        List<ObjectId> productIds = new ArrayList<>();
        Set<ObjectId> productCategoryIds = new HashSet<>();
        HashMap<String, Product> productHashMap = new HashMap<>();
        HashMap<String, ProductCategory> categoryHashMap = new HashMap<>();
        HashMap<String, SalesByProductCategory> salesByCategoryMap = new HashMap<>();

        if (invoiceList.size() > 0) {
            for (Invoice invoice : invoiceList) {
                if (invoice.getCart() == null || CollectionUtils.isEmpty(invoice.getCart().getItems())) {
                    continue;
                }
                for (OrderItem productRequest : invoice.getCart().getItems()) {
                    if (StringUtils.isNotBlank(productRequest.getProductId()) && ObjectId.isValid(productRequest.getProductId())) {
                        productIds.add(new ObjectId(productRequest.getProductId()));
                    }
                }
            }

            if (productIds.size() > 0) {
                productHashMap = productRepository.listAsMap(companyId, productIds);
                for (Product product : productHashMap.values()) {
                    if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                        productCategoryIds.add(new ObjectId(product.getCategoryId()));
                    }
                }
            }

            if (productCategoryIds.size() > 0) {
                categoryHashMap = productCategoryRepository.listAsMap(companyId, Lists.newArrayList(productCategoryIds));
            }

            for (Invoice invoice : invoiceList) {
                if (invoice.getCart() == null || CollectionUtils.isEmpty(invoice.getCart().getItems())) {
                    continue;
                }
                for (OrderItem item : invoice.getCart().getItems()) {
                    Product product = productHashMap.get(item.getProductId());
                    if (product == null || StringUtils.isBlank(product.getCategoryId())) {
                        continue;
                    }

                    ProductCategory category = categoryHashMap.get(product.getCategoryId());

                    if (category == null) {
                        continue;
                    }

                    SalesByProductCategory salesByProductCategory = salesByCategoryMap.getOrDefault(category.getId(), new SalesByProductCategory());

                    salesByProductCategory.setCategoryId(category.getId());

                    double totalCost = item.getFinalPrice().doubleValue();
                    double totalSoldQuantity = item.getQuantity().doubleValue();


                    salesByProductCategory.setTotalSoldCost(salesByProductCategory.getTotalSoldCost() + totalCost);
                    salesByProductCategory.setSoldQuantity(salesByProductCategory.getSoldQuantity() + totalSoldQuantity);
                    salesByProductCategory.setCount(salesByProductCategory.getCount() + 1);
                    salesByCategoryMap.put(category.getId(), salesByProductCategory);
                }
            }
        }


        StringBuilder syncAllInvoices = new StringBuilder();
        if (!salesByCategoryMap.isEmpty()) {
            List<CategorySaleWrapper> categorySaleWrappers = new ArrayList<>();
            for (SalesByProductCategory salesByCat : salesByCategoryMap.values()) {
                ProductCategory productCategory = categoryHashMap.get(salesByCat.getCategoryId());
                if (productCategory != null && StringUtils.isNotBlank(productCategory.getQbDesktopRef())) {
                    CategorySaleWrapper wrapper = new CategorySaleWrapper();
                    wrapper.setCategoryId(productCategory.getId());
                    wrapper.setQbCategoryName(productCategory.getQbDesktopRef());
                    wrapper.setCogs(salesByCat.getTotalSoldCost());
                    categorySaleWrappers.add(wrapper);
                }
            }

            QuickBookAccountDetails entitiesAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, entities.getId());
            int total = 0;
            if (categorySaleWrappers.size() > 0) {
                List<JournalEntryProductCategoryWrapper> salesByCategoryJEInQB = createSalesByCategoryJEInQB(categorySaleWrappers, entitiesAccountDetails);
                ProductCategoryJournalEntryConverter productCategoryJournalEntryConverter = null;
                try {
                    productCategoryJournalEntryConverter = new ProductCategoryJournalEntryConverter(salesByCategoryJEInQB, qbDesktopOperationRef.getQbDesktopFieldMap());
                    total = productCategoryJournalEntryConverter.getmList().size();
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }

                QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(productCategoryJournalEntryConverter));
                syncAllInvoices.append(qbxmlConverter.getXMLString());
            }

            if (total > 0) {
                quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntryForInvoice, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            }
        }

        return syncAllInvoices.toString();
    }

    private List<JournalEntryProductCategoryWrapper> createSalesByCategoryJEInQB(List<CategorySaleWrapper> categorySaleList, QuickBookAccountDetails entitiesAccountDetails) {
        List<JournalEntryProductCategoryWrapper> journalEntryWrappers = new ArrayList<>();
        for (CategorySaleWrapper wrapper : categorySaleList) {
            JournalEntryProductCategoryWrapper journalEntryWrapper = new JournalEntryProductCategoryWrapper();
            double cogs = wrapper.getCogs();
            journalEntryWrapper.setQbCategoryRef(wrapper.getQbCategoryName());
            journalEntryWrapper.setDebitAccountRef(entitiesAccountDetails.getChecking());
            journalEntryWrapper.setDebitAccountAmount(new BigDecimal(cogs));
            journalEntryWrapper.setCreditAccountRef(entitiesAccountDetails.getInventory());
            journalEntryWrapper.setCreditAccountAmount(new BigDecimal(cogs));
            journalEntryWrapper.setDiscounts(wrapper.getDiscounts());
            journalEntryWrapper.setExciseTax(wrapper.getExciseTax());
            journalEntryWrapper.setCreditCardFees(wrapper.getCreditCardFees());
            journalEntryWrapper.setDeliveryFees(wrapper.getDeliveryFees());

            journalEntryWrappers.add(journalEntryWrapper);
        }

        return journalEntryWrappers;
    }

}
