package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.ErrorLogs;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import org.joda.time.DateTime;

public class SaveErrorLogs {

    public static void saveErrorLogs(String companyId, String shopId, String statusCode, String statusMessage, String refrence, QuickbookSyncDetails.QuickbookEntityType entityType, ErrorLogsRepository errorLogsRepository) {
        ErrorLogs errorLogs = new ErrorLogs();
        errorLogs.prepare(companyId);
        errorLogs.setShopId(shopId);
        errorLogs.setDescription(statusMessage);
        errorLogs.setStatusCode(statusCode);
        errorLogs.setQbType(QBConstants.QUICKBOOK_DESKTOP);
        errorLogs.setQuickBookEntityType(entityType);
        errorLogs.setErrorTime(DateTime.now().getMillis());
        errorLogs.setReference(refrence);
        errorLogsRepository.save(errorLogs);
    }
}
