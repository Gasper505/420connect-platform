package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.desktop.converters.BillPaymentConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.BillPaymentReceivedWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;

public class ShipmentBillPaymentXMLService implements QBXMLService{
    private static final Logger LOG = LoggerFactory.getLogger(ShipmentBillPaymentXMLService.class);

    private String companyId;
    private String shopId;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private ShipmentBillRepository shipmentBillRepository;
    private VendorRepository vendorRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QuickbookCustomEntities customEntities;
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    public ShipmentBillPaymentXMLService(String companyId, String shopId, QbDesktopOperationRepository qbDesktopOperationRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ShipmentBillRepository shipmentBillRepository, VendorRepository vendorRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ErrorLogsRepository errorLogsRepository, QuickbookCustomEntities customEntities, QuickBookAccountDetailsRepository quickBookAccountDetailsRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.shipmentBillRepository = shipmentBillRepository;
        this.vendorRepository = vendorRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.customEntities = customEntities;
        this.quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;
    }

    @Override
    public String generatePushRequest() {

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.BillPayment);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOG.warn("Payment received sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.BillPayment, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        long latestFailTime = 0;
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails syncDetails : syncDetailsByStatus) {
            if (QuickbookSyncDetails.Status.Fail == syncDetails.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0 && details != null) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (details == null && (QuickbookSyncDetails.Status.Completed == syncDetails.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == syncDetails.getStatus())) {
                details = syncDetails;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && details == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.BillPayment, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                details = detailsListTemp.get(0);
            }
        }

        StringBuilder billPayment = new StringBuilder();
        int start = 0;
        int limit = 1000;

        List<ShipmentBill> billList = new ArrayList<>();
        if (customEntities.getSyncTime() > 0) {
            if (details != null && details.getEndTime() > 0) {
                SearchResult<ShipmentBill> billSearchResult = shipmentBillRepository.getBillByLimitsWithQBPaymentReceivedError(companyId, shopId, ShipmentBill.PaymentStatus.PAID, customEntities.getSyncTime(), details.getEndTime(), ShipmentBill.class);
                if (billSearchResult != null && billSearchResult.getValues().size() > 0) {
                    billList.addAll(billSearchResult.getValues());
                }
            }

            limit = limit - billList.size();
            SearchResult<ShipmentBill> billByLimits = shipmentBillRepository.getBillByLimitsWithoutQBPaymentReceived(companyId, shopId, ShipmentBill.PaymentStatus.PAID, customEntities.getSyncTime(), start, limit, ShipmentBill.class);
            if (billByLimits != null && billByLimits.getValues().size() > 0) {
                billList.addAll(billByLimits.getValues());
            }
        }

        QuickBookAccountDetails entitiesAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(companyId, shopId, customEntities.getId());
        Map<String, String> currentSyncPaymentReceived = new HashMap<>();

        List<BillPaymentReceivedWrapper> successReceivedWrapper = new ArrayList<>();
        List<String> failedBillIds = new ArrayList<>();
        if (billList.size() > 0) {
            HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyId);
            for (ShipmentBill bill : billList) {
                if (bill != null && StringUtils.isNotBlank(bill.getVendorId())) {
                    Vendor vendor = vendorHashMap.get(bill.getVendorId());
                    if (vendor != null && vendor.getQbMapping() != null && vendor.getQbMapping().size() > 0) {
                        for (QBDataMapping dataMapping : vendor.getQbMapping()) {
                            if (dataMapping != null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                    && StringUtils.isNotBlank(dataMapping.getQbListId()) && StringUtils.isNotBlank(dataMapping.getEditSequence())) {
                                boolean status = false;
                                for (ShipmentBillPayment shipmentBillPayment : bill.getPaymentHistory()) {
                                    BillPaymentReceivedWrapper receivedWrapper = new BillPaymentReceivedWrapper();
                                    receivedWrapper.setQbVendorRef(dataMapping.getQbDesktopRef());
                                    receivedWrapper.setQbVendorListId(dataMapping.getQbListId());
                                    receivedWrapper.setMemo(bill.getShipmentBillNumber());
                                    receivedWrapper.setTxnId(bill.getQbDesktopBillRef());
                                    receivedWrapper.setAmountPaid(shipmentBillPayment.getAmountPaid());
                                    receivedWrapper.setPayableAccount(entitiesAccountDetails.getPayableAccount());
                                    receivedWrapper.setBankAccount(entitiesAccountDetails.getChecking());
                                    receivedWrapper.setPaidDate(shipmentBillPayment.getPaidDate());
                                    receivedWrapper.setPaymentType(shipmentBillPayment.getPaymentType());
                                    String paymentNumber = shipmentBillPayment.getPaymentNo();
                                    if (paymentNumber.length() > 11) {
                                        int length = paymentNumber.length();
                                        paymentNumber = paymentNumber.substring((length - 11), length);
                                        shipmentBillPayment.setPaymentNo(paymentNumber);
                                        status = true;
                                    }
                                    receivedWrapper.setRefNo(paymentNumber);
                                    successReceivedWrapper.add(receivedWrapper);
                                    currentSyncPaymentReceived.put(paymentNumber, bill.getId());
                                }
                                if (status) {
                                    shipmentBillRepository.update(companyId, bill.getId(), bill);
                                }
                            } else {
                                failedBillIds.add(bill.getId());
                            }
                        }
                    } else {
                        failedBillIds.add(bill.getId());
                    }
                }
            }
        }

        int total = 0;
        if (successReceivedWrapper.size() > 0) {
            BillPaymentConverter billPaymentConverter = null;
            try {
                billPaymentConverter = new BillPaymentConverter(successReceivedWrapper, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = billPaymentConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(billPaymentConverter));
            billPayment.append(qbxmlConverter.getXMLString());
        }

        if (total > 0) {
            QuickbookSyncDetails syncDetails = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId, DateTime.now().getMillis(), DateTime.now().getMillis(), shopId, QuickbookSyncDetails.QuickbookEntityType.BillPayment, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyId, shopId, currentSyncPaymentReceived, syncDetails.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.BillPayment);
        }

        if (failedBillIds.size() > 0) {
            String errorMsg = "Payment's received are not synchronized";
            for (String billId : failedBillIds) {
                shipmentBillRepository.updateQbDesktopPaymentReceivedRef(companyId, shopId, billId, false, true, DateTime.now().getMillis());
                saveErrorLogs(companyId, shopId, "500", errorMsg, null, QuickbookSyncDetails.QuickbookEntityType.BillPayment, errorLogsRepository);
            }
        }

        return billPayment.toString();
    }

    @Override
    public String generatePullRequest(String timeZone) {
        return null;
    }
}
