package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.util.DateUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;

public class UpdateQbSyncDetail {

    static Logger log = LoggerFactory.getLogger(UpdateQbSyncDetail.class);

    public static void qbSyncDetailsUpdate(String companyId, int total, int success, int failed, QuickbookSyncDetails details, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository) {
        if (details != null) {
            failed = total - success;
            if (total == success) {
                details.setStatus(QuickbookSyncDetails.Status.Completed);
            } else if (total == failed){
                details.setStatus(QuickbookSyncDetails.Status.Fail);
            } else {
                details.setStatus(QuickbookSyncDetails.Status.PartialSuccess);
            }

            details.setTotal_Sync(success);
            details.setTotal_fail(failed);
            details.setEndTime(DateTime.now().getMillis());
            quickbookSyncDetailsRepository.update(companyId, details.getId(), details);
        }
    }

    public static boolean checkRequestInProcess(String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ErrorLogsRepository errorLogsRepository) {
        boolean status = false;
        SearchResult<QuickbookSyncDetails> quickBookSyncDetailsByStatus = quickbookSyncDetailsRepository.getQuickBookSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.Status.Inprogress);
        if (quickBookSyncDetailsByStatus.getValues() != null && quickBookSyncDetailsByStatus.getValues().size() > 0) {
            List<QuickbookSyncDetails> detailsList = new ArrayList<>();

            for (QuickbookSyncDetails syncDetail : quickBookSyncDetailsByStatus.getValues()) {
                log.info("Sync detail is : " + syncDetail.getId());
                int mint = DateUtil.getMinutesBetweenTwoDates(DateTime.now().getMillis(), syncDetail.getCreated());
                if (mint > 10 ) {
                    syncDetail.setStatus(QuickbookSyncDetails.Status.Fail);
                    quickbookSyncDetailsRepository.update(companyId, syncDetail.getId(), syncDetail);
                    // Save error logs
                    String errorMsg = "This request was timed out and marked as failed";
                    saveErrorLogs(companyId, shopId, "500", errorMsg, syncDetail.getId(), syncDetail.getQuickbookEntityType(), errorLogsRepository);
                } else {
                    detailsList.add(syncDetail);
                }
            }

            if (detailsList.size() > 0) {
                status = true;
            }
        } else {
            log.info("Sync details not found");
        }
        return status;
    }
}
