package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBUniqueSequence;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.converters.CustomerConverter;
import com.fourtwenty.quickbook.desktop.converters.CustomerEditConverter;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.wrappers.MemberWrapper;
import com.fourtwenty.quickbook.desktop.wrappers.VendorWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;

public class VendorAsCustomerXMLService {

    private static final int NAME_LENGTH = 35;

    private static final Logger LOG = LoggerFactory.getLogger(VendorAsCustomerXMLService.class);

    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private VendorRepository vendorRepository;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private QBUniqueSequenceRepository qbUniqueSequenceRepository;

    public VendorAsCustomerXMLService(String companyId, String shopId,
                                      QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                      VendorRepository vendorRepository, QbDesktopOperationRepository qbDesktopOperationRepository,
                                      QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository,
                                      QBUniqueSequenceRepository qbUniqueSequenceRepository) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.vendorRepository = vendorRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.qbUniqueSequenceRepository = qbUniqueSequenceRepository;
    }

    public String generatePushRequest() {
        LOG.info("Vendor as a customer startTime {}", DateTime.now());
        StringBuilder request = new StringBuilder();
        
        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyId, shopId, QBDesktopOperation.OperationType.Customer);
        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOG.warn("Customer(Vendor) sync is not available for shop {}, skipping", shopId);
            return StringUtils.EMPTY;
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickbookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);

        int failedCount = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        long latestFailTime = 0;
        for (QuickbookSyncDetails details : quickbookSyncDetailList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                failedCount += 1;
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyId, shopId, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }


        List<Vendor.VendorType> vendorTypes = new ArrayList<>();
        vendorTypes.add(Vendor.VendorType.BOTH);
        vendorTypes.add(Vendor.VendorType.CUSTOMER);

        List<VendorWrapper> vendorList = new ArrayList<>();
        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer);
        if (latestQbDesktopCurrentSync != null) {
            vendorList = vendorRepository.getCustomerByLimitsWithQBError(companyId, shopId, latestQbDesktopCurrentSync.getCreated(), vendorTypes, VendorWrapper.class);
        }

        int start = 0;
        int limit = 1000 - vendorList.size();

        List<VendorWrapper> customersByLimits = vendorRepository.getCustomerByLimitsWithoutQbDesktopRef(companyId, start, limit, vendorTypes, VendorWrapper.class);
        vendorList.addAll(customersByLimits);

        limit = 1000- vendorList.size();
        if (limit > 0) {
            List<VendorWrapper> checkQBMappingVendors = vendorRepository.getCustomerByLimitsWithoutQbDesktopRef(companyId, shopId, start, limit, vendorTypes, VendorWrapper.class);
            vendorList.addAll(checkQBMappingVendors);
        }


        int total = 0;
        Map<String, String> currentCustomerMap = new HashMap<>();
        List<MemberWrapper> memberList = new ArrayList<>();
        if (vendorList.size() > 0) {
            this.prepareMemberData(companyId, shopId, vendorList, memberList, qbUniqueSequenceRepository, true);

            for (MemberWrapper member : memberList) {
                if (member != null) {
                    currentCustomerMap.put(member.getId(), member.getQbMemberName());
                }
            }

            CustomerConverter customerConverter = null;
            try {
                customerConverter = new CustomerConverter(memberList, qbDesktopOperationRef.getQbDesktopFieldMap());
                total = customerConverter.getmList().size();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(customerConverter));
            request.append(qbxmlConverter.getXMLString());
        }

        List<VendorWrapper> modifiedVendors = new ArrayList<>();
        int modifiedLimit = 1000 - memberList.size();
        if (modifiedLimit > 0) {
            if (quickbookSyncDetails != null) {
                List<VendorWrapper> qbExistingCustomer = vendorRepository.getQBExistingCustomer(companyId, shopId, start, modifiedLimit, quickbookSyncDetails.getStartTime(), vendorTypes, VendorWrapper.class);
                if (qbExistingCustomer.size() > 0) {
                    modifiedVendors.addAll(qbExistingCustomer);
                }

                if (modifiedVendors != null && modifiedVendors.size() > 0) {
                    List<MemberWrapper> modifiedMembers = new ArrayList<>();
                    this.prepareMemberData(companyId, shopId, modifiedVendors, modifiedMembers, qbUniqueSequenceRepository, false);

                    for (MemberWrapper member : modifiedMembers) {
                        if (member != null) {
                            currentCustomerMap.put(member.getId(), member.getQbMemberName());
                        }
                    }

                    CustomerEditConverter customerEditConverter = null;
                    try {
                        customerEditConverter = new CustomerEditConverter(modifiedMembers, qbDesktopOperationRef.getQbDesktopFieldMap());
                        total = total + customerEditConverter.getmList().size();
                    } catch (Exception e) {
                        LOG.error("Error while initiating Customer edit converter : " + e.getMessage());
                    }

                    QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(customerEditConverter));
                    request.append(qbxmlConverter.getXMLString());
                }
            }
        }

        if (total > 0) {
            long currentTime = DateTime.now().getMillis();
            QuickbookSyncDetails details = quickbookSyncDetailsRepository.saveQuickbookEntity(total, 0, 0, companyId, currentTime, currentTime, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyId, shopId, currentCustomerMap, details.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Customer);
        }
        LOG.info("Vendor as a customer endTime {}", DateTime.now());
        return request.toString();
    }

    private void prepareMemberData(String companyId, String shopId, List<VendorWrapper> vendorList, List<MemberWrapper> memberList, QBUniqueSequenceRepository sequenceRepository, boolean checkStatus) {
        Map<String, Integer> dbUniqueSequenceMap = new HashMap<>();
        List<QBUniqueSequence> qbUniqueSequenceByType = sequenceRepository.getQbUniqueSequenceByType(companyId, shopId);
        for (QBUniqueSequence qbUniqueSequence : qbUniqueSequenceByType) {
            if (qbUniqueSequence != null) {
                dbUniqueSequenceMap.put(qbUniqueSequence.getName(), qbUniqueSequence.getCurrentSequence());
            }
        }

        Map<String, Integer> updatedUniqueSequenceMap = new HashMap<>();
        Map<String, Integer> newUniqueSequenceMap = new HashMap<>();
        for (VendorWrapper customer : vendorList) {
            MemberWrapper member = new MemberWrapper();

            if (checkStatus) {
                String vendorName = customer.getName().trim();
                vendorName = vendorName.replaceAll("[^0-9a-zA-Z ]", "").trim();

                if (vendorName.length() > NAME_LENGTH) {
                    vendorName = customer.getName().substring(0, 34);
                }
                vendorName = vendorName.toLowerCase();

                if (dbUniqueSequenceMap.containsKey(vendorName)) {
                    Integer integer = dbUniqueSequenceMap.get(vendorName);
                    String name = vendorName + "_" + (integer + 1);
                    member.setQbMemberName(name);
                    updatedUniqueSequenceMap.put(vendorName, (integer + 1));
                } else {
                    if (newUniqueSequenceMap.containsKey(vendorName)) {
                        Integer integer = newUniqueSequenceMap.get(vendorName);
                        String name = vendorName + "_" + (integer + 1);
                        member.setQbMemberName(name);
                        newUniqueSequenceMap.put(vendorName, (integer + 1));
                    } else {
                        member.setQbMemberName(vendorName);
                        newUniqueSequenceMap.put(vendorName, 0);
                    }
                }
            }

            member.setPrimaryPhone(customer.getPhone());
            member.setEmail(customer.getEmail());

            if (!checkStatus) {
                if (customer.getQbCustomerMapping() != null && customer.getQbCustomerMapping().size() > 0) {
                    for (QBDataMapping dataMapping : customer.getQbCustomerMapping()) {
                        if (dataMapping != null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                && StringUtils.isNotBlank(dataMapping.getQbListId()) && StringUtils.isNotBlank(dataMapping.getEditSequence())) {
                            member.setQbMemberName(dataMapping.getQbDesktopRef());
                            member.setQbRef(dataMapping.getQbDesktopRef());
                            member.setListId(dataMapping.getQbListId());
                            member.setQbEditSequence(dataMapping.getEditSequence());
                        }
                    }
                }
            }
            member.setId(customer.getId());
            memberList.add(member);
        }

        if (newUniqueSequenceMap.size() > 0) {
            List<QBUniqueSequence> qbUniqueSequences = new ArrayList<>();
            for (String name : newUniqueSequenceMap.keySet()) {
                QBUniqueSequence qbUniqueSequence = new QBUniqueSequence();
                qbUniqueSequence.prepare(companyId);
                qbUniqueSequence.setShopId(shopId);
                qbUniqueSequence.setName(name);
                qbUniqueSequence.setCurrentSequence(newUniqueSequenceMap.get(name));

                qbUniqueSequences.add(qbUniqueSequence);
            }

            sequenceRepository.save(qbUniqueSequences);
        }

        if (updatedUniqueSequenceMap.size() > 0) {
            for (String name : updatedUniqueSequenceMap.keySet()) {
                sequenceRepository.updateQbUniqueSequence(companyId, shopId, name, updatedUniqueSequenceMap.get(name));
            }
        }
    }
}
