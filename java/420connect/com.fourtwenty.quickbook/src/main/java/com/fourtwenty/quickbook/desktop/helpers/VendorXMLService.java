package com.fourtwenty.quickbook.desktop.helpers;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBUniqueSequence;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.quickbook.desktop.converters.QBXMLConverter;
import com.fourtwenty.quickbook.desktop.converters.VendorConverter;
import com.fourtwenty.quickbook.desktop.converters.VendorEditConverter;
import com.fourtwenty.quickbook.desktop.soap.qbxml.QBXMLParser;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.VendorQueryRqType;
import com.fourtwenty.quickbook.desktop.wrappers.VendorWrapper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.saveCurrentSync;


public class VendorXMLService implements QBXMLService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VendorXMLService.class);
    private String companyID;
    private String shopID;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private VendorRepository vendorRepository;
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private QBUniqueSequenceRepository qbUniqueSequenceRepository;

    public VendorXMLService(String companyID, String shopID, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, VendorRepository vendorRepository, QbDesktopOperationRepository qbDesktopOperationRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, QBUniqueSequenceRepository qbUniqueSequenceRepository) {
        this.companyID = companyID;
        this.shopID = shopID;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.vendorRepository = vendorRepository;
        this.qbDesktopOperationRepository = qbDesktopOperationRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.qbUniqueSequenceRepository = qbUniqueSequenceRepository;
    }

    public String generatePushRequest() {
        StringBuilder vendorQuery = new StringBuilder();

        QBDesktopOperation qbDesktopOperationRef = qbDesktopOperationRepository.getQuickBookDesktopOperationsByType(companyID, shopID, QBDesktopOperation.OperationType.
                Vendor);

        if (qbDesktopOperationRef == null || qbDesktopOperationRef.isSyncPaused()) {
            LOGGER.warn("Vendor sync is not available for shop {}, skipping", shopID);
            return vendorQuery.toString();
        }

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        statuses.add(QuickbookSyncDetails.Status.PartialSuccess);
        statuses.add(QuickbookSyncDetails.Status.Fail);

        Iterable<QuickbookSyncDetails> quickbookSyncDetailList = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor, statuses, QBConstants.QUICKBOOK_DESKTOP, 5);
        ArrayList<QuickbookSyncDetails> detailsList = Lists.newArrayList(quickbookSyncDetailList);

        int failedCount = 0;
        QuickbookSyncDetails quickbookSyncDetails = null;
        long latestFailTime = 0;
        for (QuickbookSyncDetails details : detailsList) {
            if (QuickbookSyncDetails.Status.Fail == details.getStatus()) {
                if (latestFailTime == 0) {
                    latestFailTime = details.getEndTime();
                }
                failedCount += 1;
            }
            if (quickbookSyncDetails == null && (QuickbookSyncDetails.Status.Completed == details.getStatus() || QuickbookSyncDetails.Status.PartialSuccess == details.getStatus())) {
                quickbookSyncDetails = details;
            }
        }

        //If failed count is 5 then pause syncing
        if (failedCount == 5 && (!qbDesktopOperationRef.isSyncPaused() && latestFailTime > qbDesktopOperationRef.getEnableSyncTime())) {
            qbDesktopOperationRepository.updateSyncPauseStatus(companyID, shopID, new ObjectId(qbDesktopOperationRef.getId()), Boolean.TRUE);
            return StringUtils.EMPTY;
        }

        if (failedCount == 5 && !qbDesktopOperationRef.isSyncPaused() && quickbookSyncDetails == null) {
            statuses.remove(QuickbookSyncDetails.Status.Fail);
            Iterable<QuickbookSyncDetails> quickbookSyncDetailListIterator = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            ArrayList<QuickbookSyncDetails> detailsListTemp = Lists.newArrayList(quickbookSyncDetailListIterator);
            if (detailsListTemp.size() > 0) {
                quickbookSyncDetails = detailsListTemp.get(0);
            }
        }

        int start = 0;
        int limit = 1000;
        List<VendorWrapper> vendorList = new ArrayList<>();
        QbDesktopCurrentSync latestQbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getLatestQbDesktopCurrentSync(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor);
        if (latestQbDesktopCurrentSync != null) {
            vendorList = vendorRepository.getVendorsByLimitsWithQBError(companyID, shopID, latestQbDesktopCurrentSync.getCreated(), VendorWrapper.class);
        }

        limit = limit - vendorList.size();
        List<VendorWrapper> vendorsByLimits = vendorRepository.getVendorsByLimits(companyID, start, limit, VendorWrapper.class);
        vendorList.addAll(vendorsByLimits);

        limit = limit - vendorList.size();
        if (limit > 0) {
            List<VendorWrapper> checkQBMappingVendors = vendorRepository.getVendorsByLimits(companyID, shopID, start, limit, VendorWrapper.class);
            vendorList.addAll(checkQBMappingVendors);
        }

        Map<String, String> vendorCurrentSyncDataMap = new HashMap<>();
        int totalVendor = 0;
        if (vendorList.size() > 0) {
            vendorList = prepareUniqueSequenceData(companyID, shopID, vendorList, qbUniqueSequenceRepository, true);
            for (VendorWrapper vendor : vendorList) {
                if (vendor != null) {
                    vendorCurrentSyncDataMap.put(vendor.getId(), vendor.getQbVendorName());
                }
            }

            VendorConverter vendorConverter = null;
            try {
                vendorConverter = new VendorConverter(vendorList, qbDesktopOperationRef.getQbDesktopFieldMap());
                totalVendor = vendorConverter.getmList().size();
            } catch (Exception e) {
                e.printStackTrace();
            }

            QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(vendorConverter));
            vendorQuery.append(qbxmlConverter.getXMLString());
        }


        List<VendorWrapper> modifiedVendorList = new ArrayList<>();
        int modifiedLimit = 1000 - vendorList.size();
        if (modifiedLimit > 0) {
            if (detailsList.size() > 0 && quickbookSyncDetails != null) {
                List<VendorWrapper> qbExistVendors = vendorRepository.getQBExistVendors(companyID, shopID, start, modifiedLimit, quickbookSyncDetails.getStartTime(), VendorWrapper.class);
                if (qbExistVendors.size() > 0) {
                    modifiedVendorList.addAll(qbExistVendors);
                }
                if (modifiedVendorList.size() > 0) {
                    modifiedVendorList = prepareUniqueSequenceData(companyID, shopID, modifiedVendorList, qbUniqueSequenceRepository, false);

                    for (VendorWrapper vendor : modifiedVendorList) {
                        if (vendor != null) {
                            vendorCurrentSyncDataMap.put(vendor.getId(), vendor.getQbVendorName());
                        }
                    }

                    VendorEditConverter vendorEditConverter = null;
                    try {
                        vendorEditConverter = new VendorEditConverter(modifiedVendorList, qbDesktopOperationRef.getQbDesktopFieldMap());
                        totalVendor = totalVendor + vendorEditConverter.getmList().size();
                    } catch (Exception e) {
                        LOGGER.error("Error while initiating vendor edit converter : " + e.getMessage());
                    }

                    QBXMLConverter qbxmlConverter = new QBXMLConverter(Lists.newArrayList(vendorEditConverter));
                    vendorQuery.append(qbxmlConverter.getXMLString());
                }
            }
        }

        if (totalVendor > 0) {
            // Save sync details
            QuickbookSyncDetails details = quickbookSyncDetailsRepository.saveQuickbookEntity(totalVendor, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP);
            saveCurrentSync(companyID, shopID, vendorCurrentSyncDataMap, details.getId(), qbDesktopCurrentSyncRepository, QuickbookSyncDetails.QuickbookEntityType.Vendor);
        }

        return vendorQuery.toString();
    }

    private List<VendorWrapper> prepareUniqueSequenceData(String companyId, String shopId, List<VendorWrapper> vendorWrapperList, QBUniqueSequenceRepository qbUniqueSequenceRepository, boolean checkStatus) {
        Map<String, Integer> dbUniqueSequenceMap = new HashMap<>();
        List<QBUniqueSequence> qbUniqueSequenceByType = qbUniqueSequenceRepository.getQbUniqueSequenceByType(companyId, shopId);
        for (QBUniqueSequence qbUniqueSequence : qbUniqueSequenceByType) {
            if (qbUniqueSequence != null) {
                dbUniqueSequenceMap.put(qbUniqueSequence.getName(), qbUniqueSequence.getCurrentSequence());
            }
        }

        Map<String, Integer> newUniqueSequenceMap = new HashMap<>();
        Map<String, Integer> updatedUniqueSequenceMap = new HashMap<>();
        for (VendorWrapper vendorWrapper : vendorWrapperList) {
            if (checkStatus) {
                String vendorName = vendorWrapper.getName().trim();
                vendorName = vendorName.replaceAll("[^0-9a-zA-Z ]", "").trim();
                if (vendorName.length() > 35) {
                    vendorName = vendorName.substring(0, 34);
                }
                vendorName = vendorName.toLowerCase();

                if (dbUniqueSequenceMap.containsKey(vendorName)) {
                    Integer integer = dbUniqueSequenceMap.get(vendorName);
                    String name = vendorName + "_" + (integer + 1);
                    vendorWrapper.setQbVendorName(name);
                    updatedUniqueSequenceMap.put(vendorName, (integer + 1));
                } else {
                    if (newUniqueSequenceMap.containsKey(vendorName)) {
                        Integer integer = newUniqueSequenceMap.get(vendorName);
                        String name = vendorName + "_" + (integer + 1);
                        vendorWrapper.setQbVendorName(name);
                        newUniqueSequenceMap.put(vendorName, (integer + 1));
                    } else {
                        vendorWrapper.setQbVendorName(vendorName);
                        newUniqueSequenceMap.put(vendorName, 0);
                    }
                }
            }

            if (!checkStatus) {
                if (vendorWrapper.getQbMapping() != null && vendorWrapper.getQbMapping().size() > 0) {
                    for (QBDataMapping dataMapping : vendorWrapper.getQbMapping()) {
                        if (dataMapping!= null && dataMapping.getShopId().equals(shopId) && StringUtils.isNotBlank(dataMapping.getQbDesktopRef())
                                && StringUtils.isNotBlank(dataMapping.getQbListId()) && StringUtils.isNotBlank(dataMapping.getEditSequence())) {
                            vendorWrapper.setQbVendorName(dataMapping.getQbDesktopRef());
                            vendorWrapper.setVendorQbRef(dataMapping.getQbDesktopRef());
                            vendorWrapper.setListId(dataMapping.getQbListId());
                            vendorWrapper.setQbEditSequence(dataMapping.getEditSequence());
                        }
                    }
                }
            }
        }

        if (newUniqueSequenceMap.size() > 0) {
            List<QBUniqueSequence> qbUniqueSequences = new ArrayList<>();
            for (String name : newUniqueSequenceMap.keySet()) {
                QBUniqueSequence qbUniqueSequence = new QBUniqueSequence();
                qbUniqueSequence.prepare(companyId);
                qbUniqueSequence.setShopId(shopId);
                qbUniqueSequence.setName(name);
                qbUniqueSequence.setCurrentSequence(newUniqueSequenceMap.get(name));

                qbUniqueSequences.add(qbUniqueSequence);
            }

            qbUniqueSequenceRepository.save(qbUniqueSequences);
        }

        if (updatedUniqueSequenceMap.size() > 0) {
            for (String name : updatedUniqueSequenceMap.keySet()) {
                qbUniqueSequenceRepository.updateQbUniqueSequence(companyId, shopId, name, dbUniqueSequenceMap.get(name));
            }
        }
        return vendorWrapperList;
    }

    @Override
    public String generatePullRequest(String timeZone) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Completed);
        List<QuickbookSyncDetails> detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 1);

        if (detailsByStatus == null || detailsByStatus.isEmpty()) {
            // Maybe this is first time for pull, so let's use push
            detailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyID, shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor, statuses, QBConstants.QUICKBOOK_DESKTOP, 1);
            if (detailsByStatus == null || detailsByStatus.isEmpty()) {
                return "";
            }
        }

        VendorQueryRqType vendorQueryRqType = new VendorQueryRqType();
        if (detailsByStatus != null && detailsByStatus.size() > 0) {
            vendorQueryRqType.setFromModifiedDate(DateUtil.convertToISO8601(detailsByStatus.get(0).getStartTime(), timeZone));
        }
        // Save sync details
        quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, companyID, DateTime.now().getMillis(), DateTime.now().getMillis(), shopID, QuickbookSyncDetails.QuickbookEntityType.Vendor, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull);
        return QBXMLParser.parseRequest(vendorQueryRqType, VendorQueryRqType.class, "VendorQueryRq");
    }
}
