package com.fourtwenty.quickbook.desktop.request;

import java.math.BigDecimal;

public class ProductCategoryPORequest {

    private String categoryRef;
    private String categoryListId;
    private BigDecimal totalCost;
    private double quantity;

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getCategoryListId() {
        return categoryListId;
    }

    public void setCategoryListId(String categoryListId) {
        this.categoryListId = categoryListId;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
}
