package com.fourtwenty.quickbook.desktop.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QBDesktopOperationRequest {
    private QBDesktopOperation.OperationType operationType;
    private boolean enabled;
    private List<QBDesktopField> qbDesktopFields;
    private boolean syncPaused = Boolean.FALSE;

    public QBDesktopOperation.OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(QBDesktopOperation.OperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<QBDesktopField> getQbDesktopFields() {
        return qbDesktopFields;
    }

    public void setQbDesktopFields(List<QBDesktopField> qbDesktopFields) {
        this.qbDesktopFields = qbDesktopFields;
    }

    public boolean isSyncPaused() {
        return syncPaused;
    }

    public void setSyncPaused(boolean syncPaused) {
        this.syncPaused = syncPaused;
    }
}

