package com.fourtwenty.quickbook.desktop.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickBookDesktopOperationResponse {

    private QBDesktopOperation.OperationType operationType;
    private boolean enabled;
    private List<QBDesktopField> qbDesktopFields;
    private String displayName;
    private boolean syncPaused = Boolean.FALSE;
    private String operationId;

    public QBDesktopOperation.OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(QBDesktopOperation.OperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<QBDesktopField> getQbDesktopFields() {
        return qbDesktopFields;
    }

    public void setQbDesktopFields(List<QBDesktopField> qbDesktopFields) {
        this.qbDesktopFields = qbDesktopFields;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isSyncPaused() {
        return syncPaused;
    }

    public void setSyncPaused(boolean syncPaused) {
        this.syncPaused = syncPaused;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }
}
