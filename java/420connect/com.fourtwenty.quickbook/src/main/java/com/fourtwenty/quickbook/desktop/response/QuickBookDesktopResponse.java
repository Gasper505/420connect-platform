package com.fourtwenty.quickbook.desktop.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickBookDesktopResponse {

    private List<QuickBookDesktopOperationResponse> qbDesktopOperations;
    private Map<String, List<QBDesktopAccounts>> quickBookDesktopAccountsMap;

    public List<QuickBookDesktopOperationResponse> getQbDesktopOperations() {
        return qbDesktopOperations;
    }

    public void setQbDesktopOperations(List<QuickBookDesktopOperationResponse> qbDesktopOperations) {
        this.qbDesktopOperations = qbDesktopOperations;
    }

    public Map<String, List<QBDesktopAccounts>> getQuickBookDesktopAccountsMap() {
        return quickBookDesktopAccountsMap;
    }

    public void setQuickBookDesktopAccountsMap(Map<String, List<QBDesktopAccounts>> quickBookDesktopAccountsMap) {
        this.quickBookDesktopAccountsMap = quickBookDesktopAccountsMap;
    }
}
