package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.AccountAddRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.AccountQueryRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.AccountRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbAccountRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class AccountSyncStore {
    private static Logger log = Logger.getLogger(AccountSyncStore.class.getName());


    public int saveQBAccountQueryResponse(String companyId, String shopId, QBXML qbxml, QbAccountRepository qbAccountRepository, QuickbookCustomEntities qbCustomEntities, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository, ErrorLogsRepository errorLogsRepository) {
        quickBookAccountDataSave(companyId, shopId, qbxml, qbAccountRepository, qbCustomEntities, quickbookSyncDetailsRepository, quickbookCustomEntitiesRepository, errorLogsRepository);
        return 100;
    }


    public int saveQbDesktopAccount(String companyId, String shopId, AccountRet accountRet, QbAccountRepository qbAccountRepository, int total) {
        QBDesktopAccounts accounts = new QBDesktopAccounts();
        accounts.setShopId(shopId);
        accounts.setCompanyId(companyId);
        accounts.setAccountName(accountRet.getFullName());
        accounts.setAccountType(QBDesktopAccounts.AccountType.valueOf(accountRet.getAccountType()));
        accounts.setAccountId(accountRet.getListID());
        qbAccountRepository.save(accounts);
        total++;
        return total;
    }


    private void quickBookAccountDataSave(String companyId, String shopId, QBXML qbxml,
                                          QbAccountRepository qbAccountRepository, QuickbookCustomEntities qbCustomEntities,
                                          QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository, ErrorLogsRepository errorLogsRepository) {

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);

        List<QBDesktopAccounts> qbAccountList = qbAccountRepository.getQBAccountList(companyId, shopId);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Account, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 5);
        QuickbookSyncDetails details = null;

        if (syncDetailsByStatus != null && syncDetailsByStatus.size() > 0) {
            for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
                details = detailsByStatus;
                break;
            }
        }

        List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
        List<AccountQueryRsType> queryRsTypes = new ArrayList<>();
        List<AccountAddRsType> accountAddRsTypes = new ArrayList<>();

        for (Object o : queryRs) {
            if (o instanceof AccountQueryRsType) {
                queryRsTypes.add((AccountQueryRsType) o);
            } else if (o instanceof AccountAddRsType) {
                accountAddRsTypes.add((AccountAddRsType) o);
            }
        }

        if (QuickbookCustomEntities.QbCustomAccount.QbSync.equals(qbCustomEntities.getQbCustomAccount())) {
            int success = 0;
            Map<QBDesktopAccounts.AccountType, List<QBDesktopAccounts>> accountTypeMap = new HashMap<>();
            for (QBDesktopAccounts accounts : qbAccountList) {
                if (accountTypeMap.containsKey(accounts.getAccountType())) {
                    List<QBDesktopAccounts> desktopAccounts = accountTypeMap.get(accounts.getAccountType());
                    desktopAccounts.add(accounts);
                    accountTypeMap.put(accounts.getAccountType(), desktopAccounts);
                } else {
                    accountTypeMap.put(accounts.getAccountType(), Lists.newArrayList(accounts));
                }
            }

            for (QBDesktopAccounts.AccountType value : QBDesktopAccounts.AccountType.values()) {
                {
                    for (AccountQueryRsType accountQueryRsType : queryRsTypes) {
                        BigInteger statusCode = accountQueryRsType.getStatusCode();
                        if (statusCode.intValue() == 0) {

                            List<AccountRet> accountRetList = accountQueryRsType.getAccountRet();
                            for (AccountRet accountRet : accountRetList) {

                                if (value.toString().equalsIgnoreCase(accountRet.getAccountType())) {
                                    if (accountTypeMap.containsKey(value)) {
                                        List<QBDesktopAccounts> desktopAccounts = accountTypeMap.get(value);
                                        boolean status = false;
                                        for (QBDesktopAccounts desktopAccount : desktopAccounts) {
                                            if (accountRet.getListID().equalsIgnoreCase(desktopAccount.getAccountId())) {
                                                //Update account data in quickBook desktop accounts
                                                desktopAccount.setAccountName(accountRet.getName());
                                                qbAccountRepository.update(companyId, desktopAccount.getId(), desktopAccount);
                                            }
                                            status = true;
                                        }
                                        if (!status) {
                                            success = saveQbDesktopAccount(companyId, shopId, accountRet, qbAccountRepository, success);
                                        }
                                    } else {
                                        // Save account data in quickBook desktop accounts
                                        success = saveQbDesktopAccount(companyId, shopId, accountRet, qbAccountRepository, success);
                                    }
                                }
                            }
                        } else {
                            // Save error logs
                            String message = accountQueryRsType.getStatusMessage();
                            String code = statusCode.toString();
                            saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Account, errorLogsRepository);
                        }
                    }
                }
            }

            if(details != null) {
                details.setTotalRecords(success);
                // update quick book sync details
                qbSyncDetailsUpdate(companyId, success, success, 0, details, quickbookSyncDetailsRepository);
            }

            qbCustomEntities.setAccounts(true);
            quickbookCustomEntitiesRepository.update(companyId, qbCustomEntities.getId(), qbCustomEntities);
        } else {
            Map<String, QBDesktopAccounts> desktopAccountsMap = new HashMap<>();
            if (qbAccountList.size() > 0) {
                for (QBDesktopAccounts accounts : qbAccountList) {
                    desktopAccountsMap.put(accounts.getAccountName(), accounts);
                }
            }

            int total = 0;
            int success = 0;
            int failed = 0;
            if (details != null) {
                total = details.getTotalRecords();
            }

            for (AccountAddRsType accountAddRsType : accountAddRsTypes) {
                BigInteger statusCode = accountAddRsType.getStatusCode();
                if (statusCode.intValue() == 0) {
                    AccountRet accountRet = accountAddRsType.getAccountRet();
                    QBDesktopAccounts desktopAccounts = desktopAccountsMap.get(accountRet.getName());
                    if (desktopAccounts != null) {
                        if (StringUtils.isBlank(desktopAccounts.getAccountId())) {
                            //Update account data in quickBook desktop accounts
                            desktopAccounts.setAccountId(accountRet.getListID());
                            qbAccountRepository.update(companyId, desktopAccounts.getId(), desktopAccounts);
                            success++;
                        }
                    }
                } else {
                    // Save error logs
                    String message = accountAddRsType.getStatusMessage();
                    String code = statusCode.toString();
                    saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Account, errorLogsRepository);
                }
            }

            if (details != null) {
                failed = total - success;
                //update sync details
                details.setTotal_fail(failed);
                details.setTotal_Sync(success);
                if (total == success) {
                    // Custom all accounts sync update in custom entities
                    qbCustomEntities.setSyncCustomAccount(true);
                    quickbookCustomEntitiesRepository.update(companyId, qbCustomEntities.getId(), qbCustomEntities);

                    details.setStatus(QuickbookSyncDetails.Status.Completed);
                } else if (total == failed) {
                    details.setStatus(QuickbookSyncDetails.Status.Fail);
                } else {
                    details.setStatus(QuickbookSyncDetails.Status.PartialSuccess);
                }

                quickbookSyncDetailsRepository.update(companyId, details.getId(), details);
            }
        }
    }

}