package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class InventoryAdjustmentStore {

    private static Logger LOG = Logger.getLogger(SyncItemStore.class.getName());
    private XPathExpression xpathExpression_ItemRef = null;
    private XPath xpath;

    public InventoryAdjustmentStore() {

        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_ItemRef = xpath.compile("//InventoryAdjustmentAddRs");
        } catch (XPathExpressionException e) {
            LOG.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }

    }

    public int syncAdjustmentQueryResponse(String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                           ProductRepository productRepository,
                                           ProductCategoryRepository productCategoryRepository,
                                           ErrorLogsRepository errorLogsRepository,
                                           QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository,
                                           QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository,
                                           QuickbookCustomEntities quickbookCustomEntities) {

        if (xpathExpression_ItemRef == null) {
            LOG.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_ItemRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            LOG.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            LOG.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        LOG.info("Total Item in list : " + nl.getLength());
        if (nl.getLength() > 0) {
            processAdjustmentResponse(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, productRepository, productCategoryRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, quickbookCustomEntitiesRepository, quickbookCustomEntities);
        }
        return 100;

    }

    private void processAdjustmentResponse(String companyId, String shopId, XPath xpath, NodeList nl, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ProductRepository productRepository, ProductCategoryRepository productCategoryRepository, ErrorLogsRepository errorLogsRepository,
                                           QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository, QuickbookCustomEntities quickbookCustomEntities) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        QuickbookSyncDetails.QuickbookEntityType entityType;
        if (quickbookCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
            entityType = QuickbookSyncDetails.QuickbookEntityType.ProductCategoryInventoryAdjustment;
        } else {
            entityType = QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment;
        }
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, entityType, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details == null) {
            return;
        }

        QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());
        int total = details.getTotalRecords();
        int failed = 0;
        Element n = null;

        Map<String, QBResponseData> qbResponseMap = new HashMap<>();

        for (int i = 0; i < nl.getLength(); i++) {
            n = (Element) nl.item(i);
            try {
                String localName = n.getLocalName();
                if (localName.equalsIgnoreCase("InventoryAdjustmentAddRs")) {
                    QBResponseData qbResponseData = prepareProductResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                    if (qbResponseData != null) {
                        qbResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                    }
                }
            } catch (Exception ex) {
                LOG.info("Error : " + ex.getMessage());
            }
        }
        Map<String, String> referenceError = new HashMap<>();
        Map<String, String> referenceSuccess = new HashMap<>();
        Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();

        if (quickbookCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
            // For quick book sale by product category methodology
            List<ObjectId> categoryIds = new ArrayList<>();
            for (String categoryId : referenceIdsMap.keySet()) {
                categoryIds.add(new ObjectId(categoryId));
                if (!qbResponseMap.containsKey(referenceIdsMap.get(categoryId))) {
                    referenceError.put(categoryId, referenceIdsMap.get(categoryId));
                } else {
                    referenceSuccess.put(categoryId, referenceIdsMap.get(categoryId));
                }
            }

            HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(companyId, shopId);
            if (referenceError.size() > 0 && productCategoryHashMap.size() > 0) {
                // Update error time and qb errored in product category
                for (String categoryId : referenceError.keySet()) {
                    if (productCategoryHashMap.containsKey(categoryId)) {
                        ProductCategory productCategory = productCategoryHashMap.get(categoryId);
                        if (productCategory != null) {
                            productCategoryRepository.updateProductCategoryQbQuantityErrorAndTime(companyId, productCategory.getId(), true, DateTime.now().getMillis());
                        }
                    }
                }
            }

            if (referenceSuccess.size() > 0 && productCategoryHashMap.size() > 0) {
                // Update error time and qb errored in product category
                for (String categoryId : referenceSuccess.keySet()) {
                    if (productCategoryHashMap.containsKey(categoryId)) {
                        ProductCategory productCategory = productCategoryHashMap.get(categoryId);
                        if (productCategory != null) {
                            productCategoryRepository.updateProductCategoryQbQuantitySynced(companyId, productCategory.getId(), true);
                        }
                    }
                }
            }

            List<ProductCategory> productCategoriesSyncedQuantityStatus = productCategoryRepository.getProductCategoriesLimitsWithoutSyncedQuantityStatus(companyId, shopId, ProductCategory.class);
            if (productCategoriesSyncedQuantityStatus.size() == 0 && quickbookCustomEntities.getSyncTime() == 0) {
                quickbookCustomEntities.setSyncTime(DateTime.now().getMillis());
                quickbookCustomEntitiesRepository.update(companyId, quickbookCustomEntities.getId(), quickbookCustomEntities);
            }
        } else {
            // For quick book sync individual methodology
            List<ObjectId> productIds = new ArrayList<>();
            for (String productId : referenceIdsMap.keySet()) {
                productIds.add(new ObjectId(productId));
                if (!qbResponseMap.containsKey(referenceIdsMap.get(productId))) {
                    referenceError.put(productId, referenceIdsMap.get(productId));
                } else {
                    referenceSuccess.put(productId, referenceIdsMap.get(productId));
                }
            }

            HashMap<String, Product> productHashMap = productRepository.listAsMap(companyId, productIds);
            if (referenceError.size() > 0 && productHashMap.size() > 0) {
                // Update error time and qb errored in product
                for (String productId : referenceError.keySet()) {
                    if (productHashMap.containsKey(productId)) {
                        Product product = productHashMap.get(productId);
                        if (product != null) {
                            productRepository.updateProductQbQuantityErrorAndTime(companyId, product.getId(), true, DateTime.now().getMillis());
                        }
                    }
                }
            }

            if (referenceSuccess.size() > 0 && productHashMap.size() > 0) {
                // Update error time and qb errored in product
                for (String productId : referenceSuccess.keySet()) {
                    if (productHashMap.containsKey(productId)) {
                        Product product = productHashMap.get(productId);
                        if (product != null) {
                            productRepository.updateProductQbQuantitySynced(companyId, product.getId(), true);
                        }
                    }
                }
            }
            productRepository.updateQBQuantitySyncedStatus(companyId, shopId, productIds, Boolean.TRUE);

            List<Product> productsWithoutSyncedStatus = productRepository.getProductsLimitsWithoutSyncedStatus(companyId, shopId, Product.class);
            if (productsWithoutSyncedStatus.size() == 0 && quickbookCustomEntities.getSyncTime() == 0) {
                quickbookCustomEntities.setSyncTime(DateTime.now().getMillis());
                quickbookCustomEntitiesRepository.update(companyId, quickbookCustomEntities.getId(), quickbookCustomEntities);
            }
        }

        int success = total - referenceError.size();
        updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);
        qbSyncDetailsUpdate(companyId, total, success, failed, details, quickbookSyncDetailsRepository);
    }

    private QBResponseData prepareProductResponse(String companyId, String shopId, Element n, XPath xpath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository) {
        QBResponseData qbResponseData = null;
        try {
            Element itemNl = null;
            Element item = null;
            Element itemData = null;
            String statusCode = n.getAttribute("statusCode");
            if (statusCode.equalsIgnoreCase("0")) {
                qbResponseData = new QBResponseData();
                NodeList itemRet = n.getElementsByTagName("InventoryAdjustmentRet");
                for (int j = 0; j<itemRet.getLength(); j++) {
                    itemNl = (Element) itemRet.item(j);
                    NodeList lintItems = itemNl.getElementsByTagName("InventoryAdjustmentLineRet");

                    for (int i = 0; i < lintItems.getLength(); i++) {
                        item = (Element) lintItems.item(j);
                        NodeList itemRef = item.getElementsByTagName("ItemRef");

                        for (int k = 0; k < itemRef.getLength(); k ++) {
                            itemData = (Element) itemRef.item(j);

                            qbResponseData.setQbRef(xpath.evaluate("./FullName", itemData));
                            qbResponseData.setListId(xpath.evaluate("./ListID", itemData));
                        }
                    }
                }

            } else {
                // Save error logs
                String message = n.getAttribute("statusMessage");
                String code = n.getAttribute("statusCode");
                saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.InventoryAdjustment, errorLogsRepository);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qbResponseData;
    }
}
