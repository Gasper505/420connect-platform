package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.quickbook.desktop.QuickBookSessionData;
import com.fourtwenty.quickbook.desktop.converters.QBFinalXMLConverter;
import com.fourtwenty.quickbook.desktop.helpers.AccountXMLService;
import com.fourtwenty.quickbook.desktop.helpers.AddAccount;
import com.fourtwenty.quickbook.desktop.helpers.AddPredefinedData;
import com.fourtwenty.quickbook.desktop.helpers.BillXMLService;
import com.fourtwenty.quickbook.desktop.helpers.CustomerXMLService;
import com.fourtwenty.quickbook.desktop.helpers.DailyPurchaseOrder;
import com.fourtwenty.quickbook.desktop.helpers.InventoryAdjustmentXMLService;
import com.fourtwenty.quickbook.desktop.helpers.InvoiceXMLService;
import com.fourtwenty.quickbook.desktop.helpers.ItemXMLService;
import com.fourtwenty.quickbook.desktop.helpers.PaymentReceivedXMLService;
import com.fourtwenty.quickbook.desktop.helpers.PredefinedItemXMLService;
import com.fourtwenty.quickbook.desktop.helpers.ProductCategoryInventoryAdjustmentXMLService;
import com.fourtwenty.quickbook.desktop.helpers.ProductCategoryXMLService;
import com.fourtwenty.quickbook.desktop.helpers.PurchaseOrderXMLService;
import com.fourtwenty.quickbook.desktop.helpers.QBSalesByProductCategory;
import com.fourtwenty.quickbook.desktop.helpers.RefundCheckXMLService;
import com.fourtwenty.quickbook.desktop.helpers.RefundReceiptXMLService;
import com.fourtwenty.quickbook.desktop.helpers.SalesByProductCategoryForInvoicesXMLService;
import com.fourtwenty.quickbook.desktop.helpers.SalesReceiptXMLService;
import com.fourtwenty.quickbook.desktop.helpers.ShipmentBillPaymentXMLService;
import com.fourtwenty.quickbook.desktop.helpers.VendorAsCustomerXMLService;
import com.fourtwenty.quickbook.desktop.helpers.VendorXMLService;
import com.fourtwenty.quickbook.desktop.soap.model.ArrayOfString;
import com.fourtwenty.quickbook.desktop.soap.qbxml.QBXMLParser;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.desktop.soap.response.PaymentReceivedStore;
import com.fourtwenty.quickbook.desktop.soap.response.SyncShipmentBillPaymentStore;
import com.fourtwenty.quickbook.desktop.soap.response.SyncBillStore;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.fourtwenty.quickbook.repositories.QbAccountRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookDesktopAccountRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.service.QuickBooksSessionService;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.checkRequestInProcess;


/**
 * Implements the service contracts as defined by quick book's web connector
 * (QBWC) WSDL :
 * <p>
 * http://developer.intuit.com/uploadedFiles/Support/QBWebConnectorSvc.wsdl
 * <p>
 * This code forwards a request of getting all the items detail information from
 * Quick book.
 * <p>
 * NOTE: This is a POC code, hence does not follow strict conventions. Code has
 * been developed just so it can work and prove the solution.
 */
@WebService(targetNamespace = "http://developer.intuit.com/")
public class QBSyncService {


    private static final QBSyncService instance = new QBSyncService();
    static Logger log = LoggerFactory.getLogger(QBSyncService.class);
    public QuickbookDesktopAccountRepository quickbookDesktopAccountRepository;
    public QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    public ThirdPartyAccountRepository thirdPartyAccountRepository;
    public QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    public ProductRepository productRepository;
    public MemberRepository memberRepository;
    public VendorRepository vendorRepository;
    public TransactionRepository transactionRepository;
    public PrepackageProductItemRepository prepackageProductItemRepository;
    public PrepackageRepository prepackageRepository;
    public ProductWeightToleranceRepository weightToleranceRepository;
    public ProductBatchRepository batchRepository;
    public PurchaseOrderRepository purchaseOrderRepository;
    public ProductCategoryRepository productCategoryRepository;
    public ShopRepository shopRepository;
    public QbAccountRepository qbAccountRepository;
    public QbDesktopOperationRepository qbDesktopOperationRepository;
    public ErrorLogsRepository errorLogsRepository;
    public QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    public QBUniqueSequenceRepository qbUniqueSequenceRepository;
    public BatchQuantityRepository batchQuantityRepository;
    public ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    public InventoryActionRepository inventoryActionRepository;
    public InventoryRepository inventoryRepository;
    public InvoiceRepository invoiceRepository;
    public InvoicePaymentsRepository invoicePaymentsRepository;
    public QuickBooksSessionService quickBookSessionService;

    public ShipmentBillRepository shipmentBillRepository;
    public QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    private QBSyncService() {
    }

    public static QBSyncService getInstance() {
        return instance;
    }

    @WebMethod(action = "http://developer.intuit.com/authenticate")
    @WebResult(name = "authenticateResult", targetNamespace = "http://developer.intuit.com/")
    public ArrayOfString authenticate(
            @WebParam(name = "strUserName", targetNamespace = "http://developer.intuit.com/") java.lang.String strUserName,
            @WebParam(name = "strPassword", targetNamespace = "http://developer.intuit.com/") java.lang.String strPassword) {
        boolean invalidUser = false;


        /*
         * This method should ideally look at a datastore and do robust
         * user/password validations. But for this poc, it just does a string
         * compare and authenticates the user as valid.
         *
         * For details about this method please refer to quicbook SDK.
         *//* *//*
         */
        log.info("Authenticating user : " + strUserName + " ...");
        QuickBookSessionData sessionData = quickBookSessionService.authenticate(strUserName, strPassword);
        String[] asRtn = new String[2];
        asRtn[0] = "{" + sessionData.getSessionId() + "}";
        asRtn[1] = sessionData.getQbRetCode();

        ArrayOfString asRtn2 = new ArrayOfString(asRtn);
        return asRtn2;
    }

    @WebMethod(action = "http://developer.intuit.com/sendRequestXML")
    @WebResult(name = "sendRequestXMLResult", targetNamespace = "http://developer.intuit.com/")
    public java.lang.String sendRequestXML(
            @WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket,
            @WebParam(name = "strHCPResponse", targetNamespace = "http://developer.intuit.com/") java.lang.String strHCPResponse,
            @WebParam(name = "strCompanyFileName", targetNamespace = "http://developer.intuit.com/") java.lang.String strCompanyFileName,
            @WebParam(name = "qbXMLCountry", targetNamespace = "http://developer.intuit.com/") java.lang.String qbXMLCountry,
            @WebParam(name = "qbXMLMajorVers", targetNamespace = "http://developer.intuit.com/") int qbXMLMajorVers,
            @WebParam(name = "qbXMLMinorVers", targetNamespace = "http://developer.intuit.com/") int qbXMLMinorVers) {

        log.debug("sendRequestXMLResult with session token {}", ticket);

        QuickBookSessionData session = quickBookSessionService.getSession(ticket);

        log.debug("is session {} Authenticated {}", ticket, session.isAuthenticated());
        if (session.isAuthenticated()) {
            String companyID = session.getCompanyId();
            String shopID = session.getShopId();

            String addRequestQuery = "";
            String getRequestQuery = "";
            QuickbookCustomEntities qbCustomEntities = quickbookCustomEntitiesRepository.findQuickbookEntities(companyID, shopID);

            Shop shop = shopRepository.get(companyID, shopID);
            if (shop == null) {
                log.warn("Shop not found, returning immediately");
                return addRequestQuery;
            }

            CompanyFeatures.AppTarget appTarget = shop.getAppTarget();

            if (qbCustomEntities != null && qbCustomEntities.isSyncDesktop()) {
                if (qbCustomEntities.getQbtypes().equals(QBConstants.QUICKBOOK_DESKTOP)) {
                    VendorXMLService vendorXMLService = new VendorXMLService(companyID, shopID, quickbookSyncDetailsRepository, vendorRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, qbUniqueSequenceRepository);
                    CustomerXMLService customerXMLService = new CustomerXMLService(companyID, shopID, quickbookSyncDetailsRepository, memberRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, qbUniqueSequenceRepository);
                    ItemXMLService itemXMLService = new ItemXMLService(companyID, shopID, quickbookSyncDetailsRepository, productRepository, qbDesktopOperationRepository, vendorRepository, qbCustomEntities, qbDesktopCurrentSyncRepository, batchRepository,
                            batchQuantityRepository, prepackageRepository, productPrepackageQuantityRepository, weightToleranceRepository, prepackageProductItemRepository, quickBookAccountDetailsRepository);
                    VendorAsCustomerXMLService vendorAsCustomerXMLService = new VendorAsCustomerXMLService(companyID, shopID, quickbookSyncDetailsRepository,
                            vendorRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, qbUniqueSequenceRepository);

                    ProductCategoryXMLService productCategoryXMLService = null;
                    if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                        productCategoryXMLService = new ProductCategoryXMLService(companyID, shopID, qbCustomEntities, quickbookSyncDetailsRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, productCategoryRepository, qbUniqueSequenceRepository, quickBookAccountDetailsRepository);
                    }

                    log.debug("[" + shop.getName() + "] Types : " + qbCustomEntities.getQbtypes());
                    // check required accounts is updated or not in blaze quick book custom entities
                    boolean checkAccountsExist = new AddAccount().checkAccountsIsExist(companyID, shopID, qbCustomEntities, quickBookAccountDetailsRepository);
                    boolean status = checkRequestInProcess(companyID, shopID, quickbookSyncDetailsRepository, errorLogsRepository);

//                    if (qbCustomEntities.getQbCustomAccount().equals(QuickbookCustomEntities.QbCustomAccount.Custom)) {
//                        if (!status && !qbCustomEntities.isSyncCustomAccount()) {
//                            log.info("[" + shop.getName() + "] Syncing accounts");
//                            addRequestQuery += new AccountXMLService().convertIntoAccountXML(companyID, shopID, quickbookSyncDetailsRepository, qbAccountRepository, qbCustomEntities);
//                        }
//                    }

                    log.debug("[" + shop.getName() + "] Checking if custom account needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && !status && !qbCustomEntities.isSyncCustomItems()) {
                        if (qbCustomEntities.getQbCustomAccount().equals(QuickbookCustomEntities.QbCustomAccount.QbSync) && checkAccountsExist) {
                            addRequestQuery += new PredefinedItemXMLService().convertIntoItemListXML(companyID, shopID, qbDesktopOperationRepository, quickbookCustomEntitiesRepository, qbCustomEntities, quickBookAccountDetailsRepository);
                        } /*else if (qbCustomEntities.getQbCustomAccount().equals(QuickbookCustomEntities.QbCustomAccount.Custom)) {
                            if (qbCustomEntities.isSyncCustomAccount()) {
                                addRequestQuery += new PredefinedItemXMLService().convertIntoItemListXML(companyID, shopID, qbDesktopOperationRepository, quickbookCustomEntitiesRepository, qbCustomEntities);
                            }
                        }*/
                    } else {
                        log.debug("[" + shop.getName() + "] Custom Accounts are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if vendor needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        log.info("[" + shop.getName() + "] Syncing vendors");
                        addRequestQuery += vendorXMLService.generatePushRequest();
                    } else {
                        log.debug("[" + shop.getName() + "] Vendors are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if customer needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Retail == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.info("[" + shop.getName() + "] Syncing customer");
                            addRequestQuery += customerXMLService.generatePushRequest();
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Customer are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if vendor as customer needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Distribution == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.info("[" + shop.getName() + "] Syncing vendor as customer");
                            addRequestQuery += vendorAsCustomerXMLService.generatePushRequest();
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Vendor as customer are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if items needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] Syncing items");
                            addRequestQuery += itemXMLService.generatePushRequest();
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Items are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if inventory needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.info("[" + shop.getName() + "] Syncing inventory adjustment");
                            addRequestQuery += new InventoryAdjustmentXMLService().dbAdjustmentXMLRequest(companyID, shopID, quickbookSyncDetailsRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, productRepository, inventoryRepository, batchQuantityRepository, inventoryActionRepository, qbCustomEntities, batchRepository, prepackageRepository, productPrepackageQuantityRepository, weightToleranceRepository, quickBookAccountDetailsRepository);
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Inventory adjustment is skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if sales needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Retail == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] Syncing sales");
                            addRequestQuery += new SalesReceiptXMLService().qbSalesReceiptService(companyID, shopID, transactionRepository, productRepository, quickbookSyncDetailsRepository, quickbookCustomEntitiesRepository, memberRepository, qbDesktopOperationRepository, qbCustomEntities, qbDesktopCurrentSyncRepository, errorLogsRepository, prepackageProductItemRepository, weightToleranceRepository, prepackageRepository);
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] sales are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }
//                if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
//                    if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
//                        addRequestQuery += new JournalEntryXMLService().qbJournalEntryService(companyID, shopID, qbCustomEntities, transactionRepository, productRepository, memberRepository, quickbookSyncDetailsRepository, prepackageProductItemRepository, prepackageRepository, weightToleranceRepository, batchRepository);
//                    }
//                }

                    log.debug("[" + shop.getName() + "] Checking if PO needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] Syncing PO");
                            addRequestQuery += new PurchaseOrderXMLService().qbPurchaseOrderXMLRequest(companyID, shopID, purchaseOrderRepository, productRepository, quickbookSyncDetailsRepository, qbCustomEntities, vendorRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, errorLogsRepository);
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] PO are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if shipment bill needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        log.debug("[" + shop.getName() + "] Syncing Shipment Bill");
                        addRequestQuery += new BillXMLService().billXMLRequest(companyID, shopID, productRepository, vendorRepository, purchaseOrderRepository, shipmentBillRepository, qbDesktopOperationRepository, quickbookSyncDetailsRepository, qbDesktopCurrentSyncRepository, errorLogsRepository, qbCustomEntities, productCategoryRepository);
                    } else {
                        log.debug("[" + shop.getName() + "] Shipment bill are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if shipment bill payment needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        log.debug("[" + shop.getName() + "] Syncing Shipment Bill payment");
                        ShipmentBillPaymentXMLService shipmentBillPaymentXMLService = new ShipmentBillPaymentXMLService(companyID, shopID, qbDesktopOperationRepository, quickbookSyncDetailsRepository, shipmentBillRepository, vendorRepository, qbDesktopCurrentSyncRepository, errorLogsRepository, qbCustomEntities, quickBookAccountDetailsRepository);
                        addRequestQuery += shipmentBillPaymentXMLService.generatePushRequest();
                    } else {
                        log.debug("[" + shop.getName() + "] Shipment bill payment are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if refund transactions needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Retail == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] syncing Refund transaction");
                            addRequestQuery += new RefundReceiptXMLService().qbRefundReceiptService(companyID, shopID, transactionRepository, productRepository, quickbookSyncDetailsRepository, qbCustomEntities, memberRepository, qbDesktopOperationRepository, prepackageRepository, prepackageProductItemRepository, weightToleranceRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);

                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Refund transactions are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }


                    log.debug("[" + shop.getName() + "] Checking if refund transactions paymemt needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Retail == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] syncing Refund transaction payment");
                            RefundCheckXMLService refundCheckXMLService = new RefundCheckXMLService(companyID, shopID, qbDesktopOperationRepository, quickbookSyncDetailsRepository, transactionRepository, qbDesktopCurrentSyncRepository, errorLogsRepository, qbCustomEntities, quickBookAccountDetailsRepository, memberRepository);
                            addRequestQuery += refundCheckXMLService.generatePushRequest();
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Refund transactions payment are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if invoice needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Distribution == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] Syncing invoice");
                            InvoiceXMLService xmlService = new InvoiceXMLService(companyID, shopID, qbDesktopOperationRepository, quickbookSyncDetailsRepository,
                                    qbCustomEntities, invoiceRepository, qbDesktopCurrentSyncRepository, vendorRepository, productRepository,
                                    prepackageRepository, prepackageProductItemRepository, weightToleranceRepository, errorLogsRepository, quickBookAccountDetailsRepository);
                            addRequestQuery += xmlService.generatePushRequest();
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Invoice are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }

                    log.debug("[" + shop.getName() + "] Checking if received payments needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    if (CompanyFeatures.AppTarget.Distribution == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                        if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                            log.debug("[" + shop.getName() + "] Syncing received payments, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                            PaymentReceivedXMLService xmlService = new PaymentReceivedXMLService(companyID, shopID, qbDesktopOperationRepository, quickbookSyncDetailsRepository,
                                    qbCustomEntities, invoiceRepository, invoicePaymentsRepository, vendorRepository, qbDesktopCurrentSyncRepository, errorLogsRepository, quickBookAccountDetailsRepository);
                            addRequestQuery += xmlService.generatePushRequest();
                        }
                    } else {
                        log.debug("[" + shop.getName() + "] Received payments are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                    }


                    if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                        DailyPurchaseOrder dailyPurchaseOrder = new DailyPurchaseOrder(companyID, shopID, qbCustomEntities, purchaseOrderRepository, quickbookSyncDetailsRepository, qbDesktopOperationRepository, vendorRepository, productRepository, productCategoryRepository, shipmentBillRepository, qbDesktopCurrentSyncRepository);
                        ProductCategoryInventoryAdjustmentXMLService productCategoryInventoryAdjustmentXMLService = new ProductCategoryInventoryAdjustmentXMLService(companyID, shopID, quickbookSyncDetailsRepository, qbDesktopOperationRepository, qbDesktopCurrentSyncRepository, productRepository, inventoryRepository, batchQuantityRepository, inventoryActionRepository, qbCustomEntities, batchRepository, prepackageRepository, productPrepackageQuantityRepository, weightToleranceRepository, productCategoryRepository, quickBookAccountDetailsRepository);
                        log.debug("[" + shop.getName() + "] Checking if product category needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status && productCategoryXMLService != null) {
                            log.debug("[" + shop.getName() + "] Syncing product categories");
                            addRequestQuery += productCategoryXMLService.generatePushRequest();
                        } else {
                            log.debug("[" + shop.getName() + "] Product categories are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        }

                        log.debug("[" + shop.getName() + "] Checking if product category inventory adjustment needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status && productCategoryXMLService != null) {
                            log.debug("[" + shop.getName() + "] Syncing inventory adjustments");
                            addRequestQuery += productCategoryInventoryAdjustmentXMLService.generatePushRequest();
                        } else {
                            log.debug("[" + shop.getName() + "] Inventory adjustment are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        }

                        log.debug("[" + shop.getName() + "] Checking if journal entries needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        if (CompanyFeatures.AppTarget.Retail == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                            log.debug("[" + shop.getName() + "] Syncing journal entries");
                            addRequestQuery += new QBSalesByProductCategory().syncSalesByProductCategory(companyID, shopID, transactionRepository, productRepository, quickbookSyncDetailsRepository, prepackageProductItemRepository, prepackageRepository, weightToleranceRepository, batchRepository, productCategoryRepository, qbCustomEntities, qbDesktopOperationRepository, quickBookAccountDetailsRepository);
                        } else {
                            log.debug("[" + shop.getName() + "] Journal entries are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        }

                        log.debug("[" + shop.getName() + "]Checking if journal entries needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        if (CompanyFeatures.AppTarget.Distribution == appTarget && StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                            log.debug("[" + shop.getName() + "] Syncing journal entries");
                            addRequestQuery += new SalesByProductCategoryForInvoicesXMLService().syncSalesByProductCategoryForInvoices(companyID, shopID, invoiceRepository, productRepository, productCategoryRepository, quickbookSyncDetailsRepository, qbCustomEntities, qbDesktopOperationRepository, quickBookAccountDetailsRepository);
                        } else {
                            log.debug("[" + shop.getName() + "] Journal entries are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        }

                        log.debug("[" + shop.getName() + "] Checking if purchase order needs to be synced, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        if (StringUtils.isBlank(addRequestQuery) && checkAccountsExist && !status) {
                            log.debug("[" + shop.getName() + "] Syncing purchase order");
                            addRequestQuery += dailyPurchaseOrder.generatePushRequest();
                        } else {
                            log.debug("[" + shop.getName() + "] Purchase order are skipped, length of request : {}, status : {}, checkAccountExist : {}", addRequestQuery.length(), status, checkAccountsExist);
                        }
                    }



                    // QB PULL QUERIES

                    //1. Accounts
                    if (qbCustomEntities.getQbCustomAccount().equals(QuickbookCustomEntities.QbCustomAccount.QbSync)) {
                        log.debug("[" + shop.getName() + "] pulling accounts");
                        getRequestQuery += new AccountXMLService().convertIntoAccountXML(companyID, shopID, quickbookSyncDetailsRepository, qbAccountRepository, qbCustomEntities);
                    }

                    if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                        //2. Vendors
                        getRequestQuery += vendorXMLService.generatePullRequest(shop.getTimeZone());

                        //3. Customer
                        getRequestQuery += customerXMLService.generatePullRequest(shop.getTimeZone());

                        //4. Products
                        getRequestQuery += itemXMLService.generatePullRequest(shop.getTimeZone());
                    }

                    if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY) && productCategoryXMLService != null) {
                        // 5. Product category
                        log.debug("[" + shop.getName() + "] Pulling product categories");
                        getRequestQuery += productCategoryXMLService.generatePullRequest(shop.getTimeZone());
                    }
                }
            } else {

                addRequestQuery += new AddPredefinedData().addPredefinedData(appTarget);
                qbCustomEntities.setSyncDesktop(true);
                quickbookCustomEntitiesRepository.update(companyID, qbCustomEntities.getId(), qbCustomEntities);

            }

            QBFinalXMLConverter qbFinalXMLConverter = new QBFinalXMLConverter();
            String requestQuery = qbFinalXMLConverter.getFinalXmlString(addRequestQuery + getRequestQuery);

            log.info("requestQuery : " + requestQuery);
            return requestQuery;
        } else {
            return "";
        }
    }

    @WebMethod(action = "http://developer.intuit.com/receiveResponseXML")
    @WebResult(name = "receiveResponseXMLResult", targetNamespace = "http://developer.intuit.com/")
    public int receiveResponseXML(
            @WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket,
            @WebParam(name = "response", targetNamespace = "http://developer.intuit.com/") java.lang.String response,
            @WebParam(name = "hresult", targetNamespace = "http://developer.intuit.com/") java.lang.String hresult,
            @WebParam(name = "message", targetNamespace = "http://developer.intuit.com/") java.lang.String message) {
        log.debug("receiveResponseXML with session token {}", ticket);
        log.info("hresult : " + hresult);
        log.info("message : " + message);
        log.info("Query response : " + response);

        QuickBookSessionData session = quickBookSessionService.getSession(ticket);

        log.debug("is session {} Authenticated {}", ticket, session.isAuthenticated());
        if (session.isAuthenticated()) {
            String companyID = session.getCompanyId();
            String shopID = session.getShopId();

            response = "" + response;
            if (response.trim().length() <= 0) return 100;

            Shop shop = shopRepository.get(companyID, shopID);
            CompanyFeatures.AppTarget appTarget = shop.getAppTarget();

            QuickbookCustomEntities qbCustomEntities = quickbookCustomEntitiesRepository.findQuickbookEntities(companyID, shopID);

            final QBXML qbxml = QBXMLParser.parseResponse(response);

            // Account response and sync details update
            AccountSyncStore store = new AccountSyncStore();
            store.saveQBAccountQueryResponse(companyID, shopID, qbxml, qbAccountRepository, qbCustomEntities, quickbookSyncDetailsRepository, quickbookCustomEntitiesRepository, errorLogsRepository);

            // Vendor response and sync details update
            SyncVendorStore vendorStore = new SyncVendorStore(qbxml, response, companyID, shopID, quickbookSyncDetailsRepository, vendorRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
            vendorStore.syncVendorAddModResponse();
            vendorStore.syncVendorQueryResponse();

            if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
                // Customer response and sync details update
                SyncCustomerStore customerStore = new SyncCustomerStore(qbxml, response, companyID, shopID, quickbookSyncDetailsRepository, memberRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, shopRepository, vendorRepository);
                customerStore.syncCustomerAddModResponse();
                customerStore.syncCustomerQueryResponse();

                // Product response and sync details update
                SyncItemStore syncItemStore = new SyncItemStore(qbxml, response, companyID, shopID, quickbookSyncDetailsRepository, productRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
                syncItemStore.syncItemAddModResponse();
                syncItemStore.syncItemQueryResponse();

                // Sales receipt response and sync details update
                SyncSalesReceiptStore syncSalesReceiptStore = new SyncSalesReceiptStore();
                syncSalesReceiptStore.syncSalesReceiptQueryResponse(response, companyID, shopID, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);

                // Journal entry response and sync details update
                SyncJournalEntryStore syncJournalEntryStore = new SyncJournalEntryStore();
                syncJournalEntryStore.syncJournalEntryQueryResponse(response, companyID, shopID, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository);

                // Refund receipt entry response and sync details update
                SyncRefundStore syncRefundStore = new SyncRefundStore();
                syncRefundStore.syncRefundReceiptQueryResponse(response, companyID, shopID, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);

                // Refund payment response and sync details update
                SyncRefundPaymentStore syncRefundPaymentStore = new SyncRefundPaymentStore();
                syncRefundPaymentStore.syncShipmentBillPaymentQueryResponse(response, companyID, shopID, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);

                //Handle invoice response and sync details update
                SyncInvoiceStore invoiceStore = new SyncInvoiceStore(qbxml, response, companyID, shopID, quickbookSyncDetailsRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, invoiceRepository, productRepository);
                invoiceStore.syncInvoiceAddModQueryResponse();

                // Payment received response and sync details update
                PaymentReceivedStore paymentReceivedStore = new PaymentReceivedStore();
                paymentReceivedStore.syncPaymentReceivedQueryResponse(response, companyID, shopID, invoicePaymentsRepository, quickbookSyncDetailsRepository, invoiceRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
            }

            if (qbCustomEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                // Product category received response and sync details update
                SyncProductCategoryStore syncProductCategoryStore = new SyncProductCategoryStore(qbxml, response, companyID, shopID, quickbookSyncDetailsRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, productCategoryRepository);
                syncProductCategoryStore.syncCategoryAddModResponse();
                syncProductCategoryStore.syncProductCategoryQueryResponse();

                // Product category journal entry received response and sync details update
                SyncJournalEntryProductCategoryStore syncJEProductCategoryStore = new SyncJournalEntryProductCategoryStore(qbxml, response, companyID, shopID,quickbookSyncDetailsRepository,errorLogsRepository, appTarget);
                syncJEProductCategoryStore.syncJournalEntryProductCategoryAddResponse();
            }

            // Purchase Order entry response and sync details update
            SyncPurchaseOrderStore syncPurchaseOrderStore = new SyncPurchaseOrderStore(qbxml, companyID, shopID, quickbookSyncDetailsRepository, purchaseOrderRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, qbCustomEntities.getQuickbookmethology(), productRepository, productCategoryRepository);
            syncPurchaseOrderStore.syncPurchaseOrderAddModQueryResponse();

            // Shipment Bill response and sync details update
            SyncBillStore syncBillStore = new SyncBillStore();
            syncBillStore.syncBillQueryResponse(response, companyID, shopID, shipmentBillRepository, quickbookSyncDetailsRepository, purchaseOrderRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, qbCustomEntities);

            // Shipment Bill Payment response and sync details update
            SyncShipmentBillPaymentStore syncShipmentBillPaymentStore = new SyncShipmentBillPaymentStore();
            syncShipmentBillPaymentStore.syncShipmentBillPaymentQueryResponse(response, companyID, shopID, quickbookSyncDetailsRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, shipmentBillRepository, qbCustomEntities);


            //Inventory adjustment response and sync details update
            InventoryAdjustmentStore adjustmentStore = new InventoryAdjustmentStore();
            adjustmentStore.syncAdjustmentQueryResponse(response, companyID, shopID, quickbookSyncDetailsRepository, productRepository, productCategoryRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, quickbookCustomEntitiesRepository, qbCustomEntities);
        } else {
            log.info("User is not authenticated, sessionId : {}", ticket);
        }

        return 100;
    }

    @WebMethod(action = "http://developer.intuit.com/connectionError")
    @WebResult(name = "connectionErrorResult", targetNamespace = "http://developer.intuit.com/")
    public java.lang.String connectionError(
            @WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket,
            @WebParam(name = "hresult", targetNamespace = "http://developer.intuit.com/") java.lang.String hresult,
            @WebParam(name = "message", targetNamespace = "http://developer.intuit.com/") java.lang.String message) {

        log.info("connectionError : " + ticket + " \n" + message);

        return null;
    }

    @WebMethod(action = "http://developer.intuit.com/getLastError")
    @WebResult(name = "getLastErrorResult", targetNamespace = "http://developer.intuit.com/")
    public java.lang.String getLastError(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket) {

        return null;
    }

    @WebMethod(action = "http://developer.intuit.com/closeConnection")
    @WebResult(name = "closeConnectionResult", targetNamespace = "http://developer.intuit.com/")
    public java.lang.String closeConnection(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket) {
        log.info("Closing connection for session : " + ticket);
        quickBookSessionService.clearSession(ticket);
        return ("close with this message");
    }

    @WebMethod(action = "http://developer.intuit.com/clientVersion")
    @WebResult(name = "clientVersionResult", targetNamespace = "http://developer.intuit.com/")
    public String clientVersion(@WebParam(name = "strVersion", targetNamespace = "http://developer.intuit.com/") java.lang.String strVersion) {
        log.info("Client version : " + strVersion);

        return "W:" + strVersion;
    }

    @WebMethod(action = "http://developer.intuit.com/serverVersion")
    @WebResult(name = "serverVersionResult", targetNamespace = "http://developer.intuit.com/")
    public java.lang.String serverVersion(@WebParam(name = "strVersion", targetNamespace = "http://developer.intuit.com/") java.lang.String strVersion) {
        log.info("Server version : " + strVersion);
        return "0.1";
    }

}
