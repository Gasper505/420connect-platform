package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.CustomerQueryRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.CustomerRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncCustomerStore {

    private static Logger log = Logger.getLogger(SyncCustomerStore.class.getName());
    private XPathExpression xpathExpression_CustomerRef = null;
    private XPath xpath;
    private QBXML qbxml;
    private String response;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private MemberRepository memberRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ShopRepository shopRepository;
    private VendorRepository vendorRepository;

    //Declaration of XML Expression
    //TODO: Change this implementation to jaxb
    public SyncCustomerStore(QBXML qbxml, String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, MemberRepository memberRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ShopRepository shopRepository, VendorRepository vendorRepository) {
        this.qbxml = qbxml;
        this.response = response;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.memberRepository = memberRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.shopRepository = shopRepository;
        this.vendorRepository = vendorRepository;
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_CustomerRef = xpath.compile("//CustomerAddRs | //CustomerModRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncCustomerAddModResponse() {
        if (xpathExpression_CustomerRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_CustomerRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total Customer in list : " + nl.getLength());
        if (nl.getLength() > 0) {
            quickBookCustomerDataSave(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, memberRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, shopRepository);
        }
        return 100;
    }


    public int syncCustomerQueryResponse() {
        CustomerQueryRsType customerQueryRsType = null;
        Shop shop = shopRepository.get(companyId, shopId);
        List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
        for (Object queryR : queryRs) {
            if (queryR instanceof CustomerQueryRsType) {
                customerQueryRsType = (CustomerQueryRsType) queryR;
                break;
            }
        }

        int success = 0;
        int failure = 0;
        int total = 0;

        if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
            if (customerQueryRsType != null) {
                List<CustomerRet> customerRets = customerQueryRsType.getCustomerRet();
                if (customerRets != null && !customerRets.isEmpty()) {
                    for (CustomerRet customerRet : customerRets) {
                        vendorRepository.updateCustomerEditSequence(companyId, shopId, customerRet.getListID(), customerRet.getEditSequence(), customerRet.getName());
                        success++;
                    }
                    total = customerRets.size();
                }
            }
        } else {
            if (customerQueryRsType != null) {
                List<CustomerRet> customerRets = customerQueryRsType.getCustomerRet();
                if (customerRets != null && !customerRets.isEmpty()) {
                    for (CustomerRet customerRet : customerRets) {
                        memberRepository.updateEditSequence(companyId, shopId, customerRet.getListID(), customerRet.getEditSequence(), customerRet.getName());
                        success++;
                    }
                    total = customerRets.size();
                }
            }
        }

        // Update sync details
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 5);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }
        if (details != null) {
            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, success, failure, details, quickbookSyncDetailsRepository);
        }

        return 100;
    }

    private void quickBookCustomerDataSave(String companyId, String shopId, XPath xpath, NodeList nl,
                                           QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, MemberRepository memberRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ShopRepository shopRepository) {

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        Shop shop = shopRepository.get(companyId, shopId);

        if (details != null && shop != null) {
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            int total = details.getTotalRecords();
            int success = 0;
            int failed = 0;
            Element n = null;

            Map<String, QBResponseData> qbMemberAddResponseMap = new HashMap<>();
            Map<String, QBResponseData> qbMemberModResponseMap = new HashMap<>();
            Map<String, QBResponseData> qbMemberResponseMap = new HashMap<>();

            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("CustomerAddRs")) {
                        QBResponseData qbAddMemberResponse = prepareCustomerResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbAddMemberResponse != null) {
                            qbMemberAddResponseMap.put(qbAddMemberResponse.getQbRef(), qbAddMemberResponse);
                            qbMemberResponseMap.put(qbAddMemberResponse.getQbRef(), qbAddMemberResponse);
                        }

                    } else if (localName.equalsIgnoreCase("CustomerModRs")) {
                        QBResponseData qbModifiedMemberResponse = prepareCustomerResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbModifiedMemberResponse != null) {
                            qbMemberModResponseMap.put(qbModifiedMemberResponse.getQbRef(), qbModifiedMemberResponse);
                            qbMemberResponseMap.put(qbModifiedMemberResponse.getQbRef(), qbModifiedMemberResponse);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }

            List<ObjectId> members = new ArrayList<>();

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            for (String memberId : referenceIdsMap.keySet()) {
                members.add(new ObjectId(memberId));
                if (!qbMemberResponseMap.containsKey(referenceIdsMap.get(memberId))) {
                    referenceError.put(memberId, referenceIdsMap.get(memberId));
                } else {
                    referenceSuccess.put(memberId, referenceIdsMap.get(memberId));
                }
            }

            Map<String, String> newSuccessDataMap = new HashMap<>();
            if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
                HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(companyId, members);
                if (referenceError.size() > 0 && vendorMap.size() > 0) {
                    // Update error time and qb errored in member
                    for (String memberId : referenceError.keySet()) {
                        if (vendorMap.containsKey(memberId)) {
                            Vendor vendor = vendorMap.get(memberId);
                            if (vendor != null) {
                                List<QBDataMapping> qbMapping = vendor.getQbCustomerMapping();
                                if (qbMapping != null && qbMapping.size() > 0) {
                                    boolean status = false;
                                    for (QBDataMapping qbDataMapping : qbMapping) {
                                        if (qbDataMapping.getShopId().equals(shopId)) {
                                            status = true;
                                            qbDataMapping.setQbErrored(true);
                                            qbDataMapping.setErrorTime(DateTime.now().getMillis());
                                        }
                                    }

                                    if (!status) {
                                        QBDataMapping mapping = new QBDataMapping();
                                        mapping.setShopId(shopId);
                                        mapping.setQbErrored(true);
                                        mapping.setErrorTime(DateTime.now().getMillis());
                                        qbMapping.add(mapping);
                                    }
                                } else {
                                    QBDataMapping dataMapping = new QBDataMapping();
                                    dataMapping.setShopId(shopId);
                                    dataMapping.setQbErrored(true);
                                    dataMapping.setErrorTime(DateTime.now().getMillis());

                                    qbMapping = new ArrayList<>();
                                    qbMapping.add(dataMapping);
                                }

                                vendorRepository.updateVendorQbCustomerMapping(companyId, vendor.getId(), qbMapping);
                            }
                        }
                    }
                }

                updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

                for (String memberId : referenceSuccess.keySet()) {
                    newSuccessDataMap.put(referenceSuccess.get(memberId), memberId);
                }

                // Update quickBook memberRef, listId, editSequence in member for new added member
                if (newSuccessDataMap.size() > 0 && qbMemberAddResponseMap.size() > 0 && vendorMap.size() > 0) {
                    for (String reference : qbMemberAddResponseMap.keySet()) {
                        QBResponseData qbResponseData = qbMemberAddResponseMap.get(reference);
                        if (qbResponseData != null) {
                            String vendorId = newSuccessDataMap.get(reference);
                            Vendor vendor = vendorMap.get(vendorId);
                            if (vendor != null) {
                                QBDataMapping dataMapping = new QBDataMapping();
                                dataMapping.setShopId(shopId);
                                dataMapping.setQbDesktopRef(qbResponseData.getQbRef());
                                dataMapping.setQbListId(qbResponseData.getListId());
                                dataMapping.setEditSequence(qbResponseData.getEditSequence());

                                List<QBDataMapping> qbMapping = vendor.getQbCustomerMapping();
                                if (qbMapping != null && qbMapping.size() > 0) {
                                    qbMapping.add(dataMapping);
                                } else {
                                    qbMapping = new ArrayList<>();
                                    qbMapping.add(dataMapping);
                                }

                                vendorRepository.updateVendorQbCustomerMapping(companyId, vendor.getId(), qbMapping);
                            }
                        }
                    }
                }

                // Update quickBook memberRef, listId, editSequence in member for updated member
                if (newSuccessDataMap.size() > 0 && qbMemberModResponseMap.size() > 0 && vendorMap.size() > 0) {
                    for (String reference : qbMemberModResponseMap.keySet()) {
                        QBResponseData qbResponseData = qbMemberModResponseMap.get(reference);
                        if (qbResponseData != null) {
                            String memberId = newSuccessDataMap.get(reference);
                            Vendor vendor = vendorMap.get(memberId);
                            if (vendor != null) {
                                List<QBDataMapping> qbMapping = vendor.getQbCustomerMapping();
                                for (QBDataMapping qbDataMapping : qbMapping) {
                                    if (qbDataMapping.getShopId().equals(shopId) && qbDataMapping.getQbListId().equals(qbResponseData.getListId())) {
                                        qbDataMapping.setEditSequence(qbResponseData.getEditSequence());
                                        qbDataMapping.setQbDesktopRef(qbResponseData.getQbRef());
                                    }
                                }

                                vendorRepository.updateVendorQbCustomerMapping(companyId, vendor.getId(), qbMapping);
                            }
                        }
                    }
                }
            } else {
                HashMap<String, Member> memberHashMap = memberRepository.listAsMap(companyId, members);
                if (referenceError.size() > 0 && memberHashMap.size() > 0) {
                    // Update error time and qb errored in member
                    for (String memberId : referenceError.keySet()) {
                        if (memberHashMap.containsKey(memberId)) {
                            Member member = memberHashMap.get(memberId);
                            if (member != null) {
                                List<QBDataMapping> qbMapping = member.getQbMapping();
                                if (qbMapping != null && qbMapping.size() > 0) {
                                    boolean status = false;
                                    for (QBDataMapping qbDataMapping : qbMapping) {
                                        if (qbDataMapping.getShopId().equals(shopId)) {
                                            status = true;
                                            qbDataMapping.setQbErrored(true);
                                            qbDataMapping.setErrorTime(DateTime.now().getMillis());
                                        }
                                    }

                                    if (!status) {
                                        QBDataMapping mapping = new QBDataMapping();
                                        mapping.setShopId(shopId);
                                        mapping.setQbErrored(true);
                                        mapping.setErrorTime(DateTime.now().getMillis());
                                        qbMapping.add(mapping);
                                    }
                                } else {
                                    QBDataMapping dataMapping = new QBDataMapping();
                                    dataMapping.setShopId(shopId);
                                    dataMapping.setQbErrored(true);
                                    dataMapping.setErrorTime(DateTime.now().getMillis());

                                    qbMapping = new ArrayList<>();
                                    qbMapping.add(dataMapping);
                                }

                                memberRepository.updateMemberQbMapping(companyId, member.getId(), qbMapping);
                            }
                        }
                    }
                }

                updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

                for (String memberId : referenceSuccess.keySet()) {
                    newSuccessDataMap.put(referenceSuccess.get(memberId), memberId);
                }

                // Update quickBook memberRef, listId, editSequence in member for new added member
                if (newSuccessDataMap.size() > 0 && qbMemberAddResponseMap.size() > 0 && memberHashMap.size() > 0) {
                    for (String reference : qbMemberAddResponseMap.keySet()) {
                        QBResponseData qbResponseData = qbMemberAddResponseMap.get(reference);
                        if (qbResponseData != null) {
                            String memberId = newSuccessDataMap.get(reference);
                            Member member = memberHashMap.get(memberId);
                            if (member != null) {
                                QBDataMapping dataMapping = new QBDataMapping();
                                dataMapping.setShopId(shopId);
                                dataMapping.setQbDesktopRef(qbResponseData.getQbRef());
                                dataMapping.setQbListId(qbResponseData.getListId());
                                dataMapping.setEditSequence(qbResponseData.getEditSequence());

                                List<QBDataMapping> qbMapping = member.getQbMapping();
                                if (qbMapping != null && qbMapping.size() > 0) {
                                    qbMapping.add(dataMapping);
                                } else {
                                    qbMapping = new ArrayList<>();
                                    qbMapping.add(dataMapping);
                                }

                                memberRepository.updateMemberQbMapping(companyId, member.getId(), qbMapping);
                            }
                        }
                    }
                }

                // Update quickBook memberRef, listId, editSequence in member for updated member
                if (newSuccessDataMap.size() > 0 && qbMemberModResponseMap.size() > 0 && memberHashMap.size() > 0) {
                    for (String reference : qbMemberModResponseMap.keySet()) {
                        QBResponseData qbResponseData = qbMemberModResponseMap.get(reference);
                        if (qbResponseData != null) {
                            String memberId = newSuccessDataMap.get(reference);
                            Member member = memberHashMap.get(memberId);
                            if (member != null) {
                                List<QBDataMapping> qbMapping = member.getQbMapping();
                                for (QBDataMapping qbDataMapping : qbMapping) {
                                    if (qbDataMapping.getShopId().equals(shopId) && qbDataMapping.getQbListId().equals(qbResponseData.getListId())) {
                                        qbDataMapping.setEditSequence(qbResponseData.getEditSequence());
                                        qbDataMapping.setQbDesktopRef(qbResponseData.getQbRef());
                                    }
                                }

                                memberRepository.updateMemberQbMapping(companyId, member.getId(), qbMapping);
                            }
                        }
                    }
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, newSuccessDataMap.size(), failed, details, quickbookSyncDetailsRepository);
        }
    }


    private QBResponseData prepareCustomerResponse(String companyId, String shopId, Element n, XPath xpath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository) {
        QBResponseData qbResponseData = null;
        try {
            Element customerNl = null;
            String statusCode = n.getAttribute("statusCode");
            if (statusCode.equalsIgnoreCase("0")) {
                qbResponseData = new QBResponseData();
                NodeList customerRet = n.getElementsByTagName("CustomerRet");
                for (int j = 0; j<customerRet.getLength(); j++) {
                    customerNl = (Element) customerRet.item(j);
                    qbResponseData.setQbRef(xpath.evaluate("./Name", customerNl));
                    qbResponseData.setEditSequence(xpath.evaluate("./EditSequence", customerNl));
                    qbResponseData.setListId(xpath.evaluate("./ListID", customerNl));
                }

            } else {
                // Save error logs
                String message = n.getAttribute("statusMessage");
                String code = n.getAttribute("statusCode");
                saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Customer, errorLogsRepository);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qbResponseData;
    }
}
