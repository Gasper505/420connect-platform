package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.InvoiceAddRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.InvoiceLineRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.InvoiceModRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.InvoiceRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncInvoiceStore {

    private static Logger LOG = Logger.getLogger(SyncItemStore.class.getName());
    private QBXML qbxml;
    private String response;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private InvoiceRepository invoiceRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ProductRepository productRepository;

    public SyncInvoiceStore(QBXML qbxml, String response, String companyId, String shopId,
                            QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                            ErrorLogsRepository errorLogsRepository,
                            QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository,
                            InvoiceRepository invoiceRepository,
                            ProductRepository productRepository) {
        this.qbxml = qbxml;
        this.response = response;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.invoiceRepository = invoiceRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.productRepository = productRepository;
    }

    public int syncInvoiceAddModQueryResponse() {

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Invoice, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Push, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {

            List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
            List<InvoiceAddRsType> invoiceAddRsTypes = new ArrayList<>();
            List<InvoiceModRsType> invoiceModRsTypes = new ArrayList<>();

            for (Object queryR : queryRs) {
                if (queryR instanceof InvoiceAddRsType) {
                    invoiceAddRsTypes.add((InvoiceAddRsType) queryR);
                }
                if (queryR instanceof InvoiceModRsType) {
                    invoiceModRsTypes.add((InvoiceModRsType) queryR);
                }
            }

            int success = 0;
            int failure = 0;
            int total = 0;

            List<String> invoiceNumbers = new ArrayList<>();
            for (InvoiceAddRsType invoice : invoiceAddRsTypes) {
                if (invoice.getInvoiceRet() != null) {
                    InvoiceRet invoiceRet = invoice.getInvoiceRet();
                    invoiceNumbers.add(invoiceRet.getRefNumber());
                } else {
                    saveErrorLogs(companyId, shopId, invoice.getStatusCode().toString(), invoice.getStatusMessage(), details.getId(), QuickbookSyncDetails.QuickbookEntityType.Invoice, errorLogsRepository);
                }
            }
            for (InvoiceModRsType invoice : invoiceModRsTypes) {
                if (invoice.getInvoiceRet() != null) {
                    InvoiceRet invoiceRet = invoice.getInvoiceRet();
                    invoiceNumbers.add(invoiceRet.getRefNumber());
                } else {
                    saveErrorLogs(companyId, shopId, invoice.getStatusCode().toString(), invoice.getStatusMessage(), details.getId(), QuickbookSyncDetails.QuickbookEntityType.Invoice, errorLogsRepository);
                }
            }

            Map<String, Invoice> invoiceMap = invoiceRepository.listAsMapByInvoiceNumber(companyId, invoiceNumbers);

            List<ObjectId> productIds = new ArrayList<>();
            for (String id : invoiceMap.keySet()) {
                Invoice invoice = invoiceMap.get(id);
                if (invoice == null) {
                    continue;
                }

                for (OrderItem item : invoice.getCart().getItems()) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
            }

            Iterable<Product> products = productRepository.list(companyId, productIds);
            Map<String, Product> productMapByListId = new HashMap<>();
            for (Product product : products) {
                productMapByListId.put(product.getId(), product);
            }

            long currentTime = DateTime.now().getMillis();
            List<String> successInvoiceNumbers = new ArrayList<>();
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());
            if(!invoiceAddRsTypes.isEmpty()) {

                for (InvoiceAddRsType invoiceAddRsType : invoiceAddRsTypes) {
                    InvoiceRet invoiceRet = invoiceAddRsType.getInvoiceRet();
                    if (invoiceRet == null) {
                        continue;
                    }

                    Invoice invoice = invoiceRepository.getInvoiceByInvoiceNumber(companyId, invoiceRet.getRefNumber());
                    if (invoice == null) {
                        continue;
                    }
                    success += this.updateInvoice(invoiceRet, invoice, productMapByListId, successInvoiceNumbers);
                }
                total += invoiceAddRsTypes.size();
            }
            if (!invoiceModRsTypes.isEmpty()) {
                for (InvoiceModRsType invoiceModRsType : invoiceModRsTypes) {
                    InvoiceRet invoiceRet = invoiceModRsType.getInvoiceRet();
                    if (invoiceRet == null) {
                        continue;
                    }
                    Invoice invoice = invoiceRepository.getInvoiceByInvoiceNumber(companyId, invoiceRet.getRefNumber());
                    if (invoice == null) {
                        continue;
                    }
                    success += this.updateInvoice(invoiceRet, invoice, productMapByListId, successInvoiceNumbers);
                }
                total += invoiceModRsTypes.size();
            }

            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();

            Map<String, String> referenceError = new HashMap<>();
            for (String invoiceId : referenceIdsMap.keySet()) {
                String invoiceNumber = referenceIdsMap.get(invoiceId);
                if (!successInvoiceNumbers.contains(invoiceNumber)) {
                    invoiceRepository.updateQbErrorAndTime(companyId, shopId, invoiceNumber, true, currentTime);
                    referenceError.put(invoiceId, invoiceNumber);
                    failure++;
                }
            }

            qbSyncDetailsUpdate(companyId, total, success, failure, details, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    private int updateInvoice(InvoiceRet invoiceRet, Invoice invoice, Map<String, Product> productMapByListId, List<String> successInvoiceNumbers) {
        try {
            List<InvoiceLineRet> groupRet = invoiceRet.getInvoiceLineRetOrInvoiceLineGroupRet()
                    .stream()
                    .map(elem -> (InvoiceLineRet) elem)
                    .collect(Collectors.toList());

            invoice.setQbDesktopRef(invoiceRet.getTxnID());
            invoice.setEditSequence(invoiceRet.getEditSequence());
            for (InvoiceLineRet lineRet : groupRet) {
                for (OrderItem item : invoice.getCart().getItems()) {
                    Product product = productMapByListId.get(item.getProductId());
                    if (product == null) {
                        continue;
                    }

                    if (product.getQbListId().equalsIgnoreCase(lineRet.getItemRef().getListID())) {
                        item.setTxnLineID(lineRet.getTxnLineID());
                        break;
                    }
                }
            }

            invoiceRepository.updateInvoice(companyId, invoice.getId(), invoice);
            successInvoiceNumbers.add(invoiceRet.getRefNumber());
            return 1;

        } catch (Exception e) {
            LOG.warning("Error while processing invoice line item response :" + e.getMessage());
        }
        return 0;
    }
}
