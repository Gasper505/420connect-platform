package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.ItemInventoryQueryRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.ItemInventoryRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncItemStore {

    private static Logger log = Logger.getLogger(SyncItemStore.class.getName());
    private XPathExpression xpathExpression_ItemRef = null;
    private XPath xpath;
    private QBXML qbxml;
    private String response;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private ProductRepository productRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;

    //Declaration of XML Expression
    public SyncItemStore(QBXML qbxml, String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ProductRepository productRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        this.qbxml = qbxml;
        this.response = response;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.productRepository = productRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_ItemRef = xpath.compile("//ItemInventoryAddRs | //ItemInventoryModRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncItemAddModResponse() {
        if (xpathExpression_ItemRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_ItemRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total Item in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookProductDataSave(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, productRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    public int syncItemQueryResponse() {
        ItemInventoryQueryRsType itemInventoryQueryRsType = null;

        List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
        for (Object queryR : queryRs) {
            if (queryR instanceof ItemInventoryQueryRsType) {
                itemInventoryQueryRsType = (ItemInventoryQueryRsType) queryR;
                break;
            }
        }

        int success = 0;
        int failure = 0;
        int total = 0;

        if (itemInventoryQueryRsType != null) {
            List<ItemInventoryRet> itemInventoryRets = itemInventoryQueryRsType.getItemInventoryRet();
            if (itemInventoryRets != null && !itemInventoryRets.isEmpty()) {
                for (ItemInventoryRet itemInventoryRet : itemInventoryRets) {
                    productRepository.updateEditSequence(companyId, shopId, itemInventoryRet.getListID(), itemInventoryRet.getEditSequence(), itemInventoryRet.getName());
                    success++;
                }
                total = itemInventoryRets.size();
            }
        }

        log.info("Total Item in item query list : " + total);
        // Update sync details
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Item, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 5);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }
        if (details != null) {
            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, success, failure, details, quickbookSyncDetailsRepository);
        }

        return 100;
    }

    private void quickBookProductDataSave(String companyId, String shopId, XPath xpath, NodeList nl,
                                         QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ProductRepository productRepositoy, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Item, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            int total = details.getTotalRecords();
            int success = 0;
            int failed = 0;
            Element n = null;

            Map<String, QBResponseData> qbProductAddResponseMap = new HashMap<>();
            Map<String, QBResponseData> qbProductModResponseMap = new HashMap<>();
            Map<String, QBResponseData> qbProductResponseMap = new HashMap<>();

            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("ItemInventoryAddRs")) {
                        QBResponseData qbResponseData = prepareProductResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbResponseData != null) {
                            qbProductAddResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            qbProductResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    } else if (localName.equalsIgnoreCase("ItemInventoryModRs")) {
                        QBResponseData qbResponseData = prepareProductResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbResponseData != null) {
                            qbProductModResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            qbProductResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }


            List<ObjectId> products = new ArrayList<>();

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            for (String productId : referenceIdsMap.keySet()) {
                products.add(new ObjectId(productId));
                if (!qbProductResponseMap.containsKey(referenceIdsMap.get(productId))) {
                    referenceError.put(productId, referenceIdsMap.get(productId));
                } else {
                    referenceSuccess.put(productId, referenceIdsMap.get(productId));
                }
            }

            HashMap<String, Product> productHashMap = productRepositoy.listAsMap(companyId, products);
            if (referenceError.size() > 0 && productHashMap.size() > 0) {
                // Update error time and qb errored in product
                for (String productId : referenceError.keySet()) {
                    if (productHashMap.containsKey(productId)) {
                        Product product = productHashMap.get(productId);
                        if (product != null) {
                            boolean qbQuantityErrored = false;
                            boolean qbQuantitySynced = false;
                            if (!product.isQbQuantitySynced()) {
                                qbQuantityErrored = true;
                                qbQuantitySynced = false;
                            }
                            productRepositoy.updateProductQbErrorAndTime(companyId, product.getId(), true, DateTime.now().getMillis(), qbQuantitySynced, qbQuantityErrored);
                        }
                    }
                }
            }

            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

            Map<String, String> newSuccessDataMap = new HashMap<>();
            for (String productId : referenceSuccess.keySet()) {
                newSuccessDataMap.put(referenceSuccess.get(productId), productId);
            }

            // Update quickBook reference for product add
            if (newSuccessDataMap.size() > 0 && qbProductAddResponseMap.size() > 0 && productHashMap.size() > 0) {
                for (String reference : qbProductAddResponseMap.keySet()) {
                    QBResponseData qbResponseData = qbProductAddResponseMap.get(reference);
                    if (qbResponseData != null) {
                        String productId = newSuccessDataMap.get(reference);
                        Product product = productHashMap.get(productId);
                        if (product != null) {
                            productRepositoy.updateProductRef(companyId, product.getId(), qbResponseData.getQbRef(), qbResponseData.getEditSequence(), qbResponseData.getListId());
                        }
                    }
                }
            }

            if (newSuccessDataMap.size() > 0 && qbProductModResponseMap.size() > 0 && productHashMap.size() > 0) {
                for (String reference : qbProductModResponseMap.keySet()) {
                    QBResponseData qbResponseData = qbProductModResponseMap.get(reference);
                    if (qbResponseData != null) {
                        String productId = newSuccessDataMap.get(reference);
                        Product product = productHashMap.get(productId);
                        if (product != null) {
                            productRepositoy.updateEditSequenceAndRef(companyId, shopId, product.getId(), qbResponseData.getListId(), qbResponseData.getEditSequence(), qbResponseData.getQbRef());
                        }
                    }
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, qbProductResponseMap.size(), failed, details, quickbookSyncDetailsRepository);
        }
    }

    private QBResponseData prepareProductResponse(String companyId, String shopId, Element n, XPath xpath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository) {
        QBResponseData qbResponseData = null;
        try {
            Element itemNl = null;
            String statusCode = n.getAttribute("statusCode");
            if (statusCode.equalsIgnoreCase("0")) {
                qbResponseData = new QBResponseData();
                NodeList itemRet = n.getElementsByTagName("ItemInventoryRet");
                for (int j = 0; j<itemRet.getLength(); j++) {
                    itemNl = (Element) itemRet.item(j);
                    qbResponseData.setQbRef(xpath.evaluate("./Name", itemNl));
                    qbResponseData.setEditSequence(xpath.evaluate("./EditSequence", itemNl));
                    qbResponseData.setListId(xpath.evaluate("./ListID", itemNl));
                }

            } else {
                // Save error logs
                String message = n.getAttribute("statusMessage");
                String code = n.getAttribute("statusCode");
                saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Item, errorLogsRepository);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qbResponseData;
    }
}
