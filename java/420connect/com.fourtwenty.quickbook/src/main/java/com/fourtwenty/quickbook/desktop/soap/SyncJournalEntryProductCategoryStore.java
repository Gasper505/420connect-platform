package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncJournalEntryProductCategoryStore {

    private static Logger log = Logger.getLogger(SyncItemStore.class.getName());
    private XPathExpression xpathExpression_JournalEntryProductCategoryRef = null;
    private XPath xpath;
    private QBXML qbxml;
    private String response;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private ErrorLogsRepository errorLogsRepository;
    private CompanyFeatures.AppTarget appTarget;

    //Declaration of XML Expression
    public SyncJournalEntryProductCategoryStore(QBXML qbxml, String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ErrorLogsRepository errorLogsRepository, CompanyFeatures.AppTarget appTarget) {
        this.qbxml = qbxml;
        this.response = response;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.appTarget = appTarget;
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_JournalEntryProductCategoryRef = xpath.compile("//JournalEntryAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }


    public int syncJournalEntryProductCategoryAddResponse() {
        if (xpathExpression_JournalEntryProductCategoryRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_JournalEntryProductCategoryRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total product category in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookProductCategoryJournalEntryDataUpdate(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, errorLogsRepository, appTarget);
        }
        return 100;
    }


    public void quickBookProductCategoryJournalEntryDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl,
                                                QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ErrorLogsRepository errorLogsRepository, CompanyFeatures.AppTarget appTarget) {

        QuickbookSyncDetails details = null;
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        if (CompanyFeatures.AppTarget.Distribution.equals(appTarget)) {
            List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntryForInvoice, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
            for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
                details = detailsByStatus;
                break;
            }
        } else {
            List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
            for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
                details = detailsByStatus;
                break;
            }
        }

        if (details != null) {
            int total = details.getTotalRecords();
            int failed = 0;
            Element n = null;

            Map<String, QBResponseData> categoryJEAddResponseMap = new HashMap<>();
            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("JournalEntryAddRs")) {
                        QBResponseData qbResponseData = prepareProductResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbResponseData != null) {
                            categoryJEAddResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }

            if (categoryJEAddResponseMap.size() > 0) {
                // update quick book sync details
                qbSyncDetailsUpdate(companyId, total, categoryJEAddResponseMap.size(), failed, details, quickbookSyncDetailsRepository);
            }
        }
    }

    private QBResponseData prepareProductResponse(String companyId, String shopId, Element n, XPath xpath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository) {
        QBResponseData qbResponseData = null;
        try {
            Element journalNl = null;
            String statusCode = n.getAttribute("statusCode");
            if (statusCode.equalsIgnoreCase("0")) {
                qbResponseData = new QBResponseData();
                NodeList journalRet = n.getElementsByTagName("JournalEntryRet");
                for (int j = 0; j<journalRet.getLength(); j++) {
                    journalNl = (Element) journalRet.item(j);
                }
                qbResponseData.setQbRef(xpath.evaluate("./RefNumber", journalNl));

            } else {
                // Save error logs
                String message = n.getAttribute("statusMessage");
                String code = n.getAttribute("statusCode");
                saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Item, errorLogsRepository);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qbResponseData;
    }
}
