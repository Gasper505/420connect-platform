package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncJournalEntryStore {

    private static Logger log = Logger.getLogger(SyncJournalEntryStore.class.getName());
    private XPathExpression xpathExpression_JournalEntryRef = null;
    private XPath xpath;

    //Declaration of XML Expression
    public SyncJournalEntryStore() {
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_JournalEntryRef = xpath.compile("//JournalEntryAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncJournalEntryQueryResponse(String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository, ErrorLogsRepository errorLogsRepository) {
        if (xpathExpression_JournalEntryRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_JournalEntryRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total Journal Entry in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookJournalEntryDataUpdate(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository);
        }
        return 100;
    }

    public void quickBookJournalEntryDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl,
                                                QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository, ErrorLogsRepository errorLogsRepository) {

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.JournalEntry, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            int total = details.getTotalRecords();
            int success = 0;
            int failed = 0;
            Element n = null;
            Element journalNl = null;
            List<String> journalEntryQbRefs = new ArrayList<>();
            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("JournalEntryAddRs")) {
                        String statusCode = n.getAttribute("statusCode");
                        if (statusCode.equalsIgnoreCase("0")) {
                            NodeList journalRet = n.getElementsByTagName("JournalEntryRet");
                            for (int j = 0; j<journalRet.getLength(); j++) {
                                journalNl = (Element) journalRet.item(j);
                            }
                            journalEntryQbRefs.add(xpath.evaluate("./RefNumber", journalNl));

                        } else {
                            // Save error logs
                            String message = n.getAttribute("statusMessage");
                            String code = n.getAttribute("statusCode");
                            saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.JournalEntry, errorLogsRepository);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }

            if (journalEntryQbRefs.size() > 0) {
                for (String journalEntryQbRef : journalEntryQbRefs) {
                    if (StringUtils.isNotBlank(journalEntryQbRef)) {
                        transactionRepository.updateQbDesktopJournalEntryRef(companyId, shopId, journalEntryQbRef);
                        success++;
                    }
                }

                // update quick book sync details
                qbSyncDetailsUpdate(companyId, total, success, failed, details, quickbookSyncDetailsRepository);
            }
        }
    }
}
