package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.ItemInventoryQueryRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.ItemInventoryRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncProductCategoryStore {
    private static Logger log = Logger.getLogger(SyncItemStore.class.getName());
    private XPathExpression xpathExpression_CategoryRef = null;
    private XPath xpath;
    private QBXML qbxml;
    private String response;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private ProductCategoryRepository productCategoryRepository;

    //Declaration of XML Expression
    public SyncProductCategoryStore(QBXML qbxml, String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ProductCategoryRepository productCategoryRepository) {
        this.qbxml = qbxml;
        this.response = response;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.productCategoryRepository  = productCategoryRepository;
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_CategoryRef = xpath.compile("//ItemInventoryAddRs | //ItemInventoryModRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }


    public int syncCategoryAddModResponse() {
        if (xpathExpression_CategoryRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_CategoryRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total product category in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookProductCategoryDataSave(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, productCategoryRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    public int syncProductCategoryQueryResponse() {
        ItemInventoryQueryRsType itemInventoryQueryRsType = null;

        List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
        for (Object queryR : queryRs) {
            if (queryR instanceof ItemInventoryQueryRsType) {
                itemInventoryQueryRsType = (ItemInventoryQueryRsType) queryR;
                break;
            }
        }

        int success = 0;
        int failure = 0;
        int total = 0;

        if (itemInventoryQueryRsType != null) {
            List<ItemInventoryRet> itemInventoryRets = itemInventoryQueryRsType.getItemInventoryRet();
            if (itemInventoryRets != null && !itemInventoryRets.isEmpty()) {
                for (ItemInventoryRet itemInventoryRet : itemInventoryRets) {
                    productCategoryRepository.updateEditSequence(companyId, shopId, itemInventoryRet.getListID(), itemInventoryRet.getEditSequence());
                    success++;
                }
                total = itemInventoryRets.size();
            }
        }

        log.info("Total product category in item query list : " + total);
        // Update sync details
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 5);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }
        if (details != null) {
            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, success, failure, details, quickbookSyncDetailsRepository);
        }

        return 100;
    }


    private void quickBookProductCategoryDataSave(String companyId, String shopId, XPath xpath, NodeList nl,
                                          QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ProductCategoryRepository productCategoryRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {

        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            int total = details.getTotalRecords();
            int failed = 0;
            Element n = null;

            Map<String, QBResponseData> categoryAddResponseMap = new HashMap<>();
            Map<String, QBResponseData> categoryModResponseMap = new HashMap<>();
            Map<String, QBResponseData> categoryResponseMap = new HashMap<>();

            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("ItemInventoryAddRs")) {
                        QBResponseData qbResponseData = prepareProductResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbResponseData != null) {
                            categoryAddResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            categoryResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    } else if (localName.equalsIgnoreCase("ItemInventoryModRs")) {
                        QBResponseData qbResponseData = prepareProductResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbResponseData != null) {
                            categoryModResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            categoryResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }


            List<ObjectId> categories = new ArrayList<>();

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            for (String categoryId : referenceIdsMap.keySet()) {
                categories.add(new ObjectId(categoryId));
                if (!categoryResponseMap.containsKey(referenceIdsMap.get(categoryId))) {
                    referenceError.put(categoryId, referenceIdsMap.get(categoryId));
                } else {
                    referenceSuccess.put(categoryId, referenceIdsMap.get(categoryId));
                }
            }

            HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(companyId, categories);
            if (referenceError.size() > 0 && productCategoryHashMap.size() > 0) {
                // Update error time and qb errored in product category
                for (String categoryId : referenceError.keySet()) {
                    if (productCategoryHashMap.containsKey(categoryId)) {
                        ProductCategory productCategory = productCategoryHashMap.get(categoryId);
                        if (productCategory != null) {
                            productCategoryRepository.updateProductCategoryQbErrorAndTime(companyId, shopId, productCategory.getId(), true, DateTime.now().getMillis());
                        }
                    }
                }
            }

            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

            Map<String, String> newSuccessDataMap = new HashMap<>();
            for (String categoryId : referenceSuccess.keySet()) {
                newSuccessDataMap.put(referenceSuccess.get(categoryId), categoryId);
            }

            // Update quickBook reference for product category add
            if (newSuccessDataMap.size() > 0 && categoryAddResponseMap.size() > 0 && productCategoryHashMap.size() > 0) {
                for (String reference : categoryAddResponseMap.keySet()) {
                    QBResponseData qbResponseData = categoryAddResponseMap.get(reference);
                    if (qbResponseData != null) {
                        String categoryId = newSuccessDataMap.get(reference);
                        ProductCategory productCategory = productCategoryHashMap.get(categoryId);
                        if (productCategory != null) {
                            productCategoryRepository.updateProductCategoryRef(companyId, shopId, productCategory.getId(), qbResponseData.getQbRef(), qbResponseData.getEditSequence(), qbResponseData.getListId());
                        }
                    }
                }
            }

            if (newSuccessDataMap.size() > 0 && categoryModResponseMap.size() > 0 && productCategoryHashMap.size() > 0) {
                for (String reference : categoryModResponseMap.keySet()) {
                    QBResponseData qbResponseData = categoryModResponseMap.get(reference);
                    if (qbResponseData != null) {
                        String categoryId = newSuccessDataMap.get(reference);
                        ProductCategory productCategory = productCategoryHashMap.get(categoryId);
                        if (productCategory != null) {
                            productCategoryRepository.updateEditSequenceAndRef(companyId, shopId, productCategory.getId(), qbResponseData.getListId(), qbResponseData.getEditSequence(), qbResponseData.getQbRef());
                        }
                    }
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), failed, details, quickbookSyncDetailsRepository);
        }
    }

    private QBResponseData prepareProductResponse(String companyId, String shopId, Element n, XPath xpath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository) {
        QBResponseData qbResponseData = null;
        try {
            Element itemNl = null;
            String statusCode = n.getAttribute("statusCode");
            if (statusCode.equalsIgnoreCase("0")) {
                qbResponseData = new QBResponseData();
                NodeList itemRet = n.getElementsByTagName("ItemInventoryRet");
                for (int j = 0; j<itemRet.getLength(); j++) {
                    itemNl = (Element) itemRet.item(j);
                    qbResponseData.setQbRef(xpath.evaluate("./Name", itemNl));
                    qbResponseData.setEditSequence(xpath.evaluate("./EditSequence", itemNl));
                    qbResponseData.setListId(xpath.evaluate("./ListID", itemNl));
                }

            } else {
                // Save error logs
                String message = n.getAttribute("statusMessage");
                String code = n.getAttribute("statusCode");
                saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Item, errorLogsRepository);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qbResponseData;
    }
}
