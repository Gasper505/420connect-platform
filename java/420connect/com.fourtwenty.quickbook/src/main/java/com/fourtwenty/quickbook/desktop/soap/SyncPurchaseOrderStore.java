package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.PurchaseOrderAddRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.PurchaseOrderLineRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.PurchaseOrderModRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.PurchaseOrderRet;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncPurchaseOrderStore {
    private static Logger LOG = Logger.getLogger(SyncJournalEntryStore.class.getName());

    private QBXML qbxml;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private PurchaseOrderRepository purchaseOrderRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    private String methodology;
    private ProductRepository productRepository;
    private ProductCategoryRepository productCategoryRepository;

    public SyncPurchaseOrderStore(QBXML qbxml, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, PurchaseOrderRepository purchaseOrderRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, String methodology, ProductRepository productRepository, ProductCategoryRepository productCategoryRepository) {
        this.qbxml = qbxml;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
        this.methodology = methodology;
        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
    }

    public int syncPurchaseOrderAddModQueryResponse() {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);

        QuickbookSyncDetails.QuickbookEntityType entityType;
        if (methodology.equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
            entityType = QuickbookSyncDetails.QuickbookEntityType.Expenses;
        } else {
            entityType = QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder;
        }
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, entityType, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
            List<PurchaseOrderAddRsType> purchaseOrderAddRsTypes = new ArrayList<>();
            List<PurchaseOrderModRsType> purchaseOrderModRsTypes = new ArrayList<>();

            for (Object queryR : queryRs) {
                if (queryR instanceof PurchaseOrderAddRsType) {
                    purchaseOrderAddRsTypes.add((PurchaseOrderAddRsType) queryR);
                }
                if (queryR instanceof PurchaseOrderModRsType) {
                    purchaseOrderModRsTypes.add((PurchaseOrderModRsType) queryR);
                }
            }

            int success = 0;
            int failure = 0;
            int total = 0;
            List<String> poNumbers = new ArrayList<>();
            for (PurchaseOrderAddRsType purchaseOrderAddRsType : purchaseOrderAddRsTypes) {
                if (purchaseOrderAddRsType.getPurchaseOrderRet() != null) {
                    PurchaseOrderRet purchaseOrderRet = purchaseOrderAddRsType.getPurchaseOrderRet();
                    poNumbers.add(purchaseOrderRet.getRefNumber());
                } else {
                    saveErrorLogs(companyId, shopId, purchaseOrderAddRsType.getStatusCode().toString(), purchaseOrderAddRsType.getStatusMessage(), details.getId(), QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, errorLogsRepository);
                }
            }
            for (PurchaseOrderModRsType purchaseOrderModRsType : purchaseOrderModRsTypes) {
                if (purchaseOrderModRsType.getPurchaseOrderRet() != null) {
                    PurchaseOrderRet purchaseOrderRet = purchaseOrderModRsType.getPurchaseOrderRet();
                    poNumbers.add(purchaseOrderRet.getRefNumber());
                } else {
                    saveErrorLogs(companyId, shopId, purchaseOrderModRsType.getStatusCode().toString(), purchaseOrderModRsType.getStatusMessage(), details.getId(), QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, errorLogsRepository);
                }
            }

            Map<String, PurchaseOrder> purchaseOrderMap = purchaseOrderRepository.listAsMapByPONumber(companyId, poNumbers);

            List<ObjectId> productIds = new ArrayList<>();
            for (String id : purchaseOrderMap.keySet()) {
                PurchaseOrder purchaseOrder = purchaseOrderMap.get(id);
                if (purchaseOrder == null) {
                    continue;
                }

                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    productIds.add(new ObjectId(poProductRequest.getProductId()));
                }
            }

            Map<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(companyId, shopId);
            Iterable<Product> products = productRepository.list(companyId, productIds);
            Map<String, Product> productMapByListId = new HashMap<>();
            for (Product product : products) {
                productMapByListId.put(product.getId(), product);
            }

            List<String> successPONumbers = new ArrayList<>();
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());
            if(!purchaseOrderAddRsTypes.isEmpty()) {

                for (PurchaseOrderAddRsType purchaseOrderAddRsType : purchaseOrderAddRsTypes) {
                    PurchaseOrderRet purchaseOrderRet = purchaseOrderAddRsType.getPurchaseOrderRet();
                    if (purchaseOrderRet == null) {
                        continue;
                    }

                    PurchaseOrder purchaseOrder = purchaseOrderRepository.getPOByPONumber(companyId, purchaseOrderRet.getRefNumber());
                    if (purchaseOrder == null) {
                        continue;
                    }
                    if (methodology.equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                        success += this.updatePurchaseOrderForSalesByCategory(purchaseOrderRet, purchaseOrder, productMapByListId, successPONumbers, productCategoryHashMap);
                    } else {
                        success += this.updatePurchaseOrder(purchaseOrderRet, purchaseOrder, productMapByListId, successPONumbers);
                    }
                }
            }

            if (!purchaseOrderModRsTypes.isEmpty()) {
                for (PurchaseOrderModRsType purchaseOrderModRsType : purchaseOrderModRsTypes) {
                    PurchaseOrderRet purchaseOrderRet = purchaseOrderModRsType.getPurchaseOrderRet();
                    if (purchaseOrderRet == null) {
                        continue;
                    }
                    PurchaseOrder purchaseOrder = purchaseOrderRepository.getPOByPONumber(companyId, purchaseOrderRet.getRefNumber());
                    if (purchaseOrder == null) {
                        continue;
                    }

                    if (methodology.equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                        success += this.updatePurchaseOrderForSalesByCategory(purchaseOrderRet, purchaseOrder, productMapByListId, successPONumbers, productCategoryHashMap);
                    } else {
                        success += this.updatePurchaseOrder(purchaseOrderRet, purchaseOrder, productMapByListId, successPONumbers);
                    }
                }
            }

            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();

            total = referenceIdsMap.size();
            Map<String, String> referenceError = new HashMap<>();
            for (String poId : referenceIdsMap.keySet()) {
                String poNumber = referenceIdsMap.get(poId);
                if (!successPONumbers.contains(poNumber)) {
                    purchaseOrderRepository.updatePurchaseOrderQbErrorAndTime(companyId, poNumber, Boolean.TRUE, DateTime.now().getMillis());
                    referenceError.put(poId, poNumber);
                    failure++;
                }
            }

            qbSyncDetailsUpdate(companyId, total, success, failure, details, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

        }
        return 100;
    }


    private int updatePurchaseOrder(PurchaseOrderRet purchaseOrderRet, PurchaseOrder purchaseOrder, Map<String, Product> productMapByListId, List<String> successPONumbers) {
        try {
            List<PurchaseOrderLineRet> groupRet = purchaseOrderRet.getPurchaseOrderLineRetOrPurchaseOrderLineGroupRet()
                    .stream()
                    .map(elem -> (PurchaseOrderLineRet) elem)
                    .collect(Collectors.toList());

            purchaseOrder.setQbDesktopPurchaseOrderRef(purchaseOrderRet.getTxnID());
            purchaseOrder.setEditSequence(purchaseOrderRet.getEditSequence());
            for (PurchaseOrderLineRet lineRet : groupRet) {
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    Product product = productMapByListId.get(poProductRequest.getProductId());
                    if (product == null) {
                        continue;
                    }
                    if (product.getQbListId().equalsIgnoreCase(lineRet.getItemRef().getListID())) {
                        poProductRequest.setTxnLineID(lineRet.getTxnLineID());
                        break;
                    }
                }
            }

            purchaseOrderRepository.updatePurchaseOrder(companyId, purchaseOrder.getId(), purchaseOrder);
            successPONumbers.add(purchaseOrderRet.getRefNumber());
            return 1;

        } catch (Exception e) {
            LOG.warning("Error while processing invoice line item response :" + e.getMessage());
        }
        return 0;
    }

    private int updatePurchaseOrderForSalesByCategory(PurchaseOrderRet purchaseOrderRet, PurchaseOrder purchaseOrder, Map<String, Product> productMapByListId, List<String> successPONumbers,Map<String, ProductCategory> productCategoryMap ) {
        try {
            List<PurchaseOrderLineRet> groupRet = purchaseOrderRet.getPurchaseOrderLineRetOrPurchaseOrderLineGroupRet()
                    .stream()
                    .map(elem -> (PurchaseOrderLineRet) elem)
                    .collect(Collectors.toList());

            purchaseOrder.setQbDesktopPurchaseOrderRef(purchaseOrderRet.getTxnID());
            purchaseOrder.setEditSequence(purchaseOrderRet.getEditSequence());
            for (PurchaseOrderLineRet lineRet : groupRet) {
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    Product product = productMapByListId.get(poProductRequest.getProductId());
                    if (product == null) {
                        continue;
                    }

                    ProductCategory productCategory = productCategoryMap.get(product.getCategoryId());
                    if (productCategory != null) {
                        if (productCategory.getQbListId().equalsIgnoreCase(lineRet.getItemRef().getListID())) {
                            poProductRequest.setTxnLineID(lineRet.getTxnLineID());
                            break;
                        }
                    }
                }
            }

            purchaseOrderRepository.updatePurchaseOrder(companyId, purchaseOrder.getId(), purchaseOrder);
            successPONumbers.add(purchaseOrderRet.getRefNumber());
            return 1;

        } catch (Exception e) {
            LOG.warning("Error while processing invoice line item response :" + e.getMessage());
        }
        return 0;
    }
}
