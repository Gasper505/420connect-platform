package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncRefundPaymentStore {
    private static Logger log = Logger.getLogger(SyncRefundPaymentStore.class.getName());
    private XPathExpression xpathExpression_RefundPaymentRef = null;
    private XPath xpath;

    //Declaration of XML Expression
    public SyncRefundPaymentStore() {
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_RefundPaymentRef = xpath.compile("//CheckAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncShipmentBillPaymentQueryResponse(String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository,
                                                    ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        if (xpathExpression_RefundPaymentRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_RefundPaymentRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Payment received Entry in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookRefundPaymentDataUpdate(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    private void quickBookRefundPaymentDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository,
                                                  ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);

        List<QuickbookSyncDetails> syncDetailsByStatusForBillPayments = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails refundPaymentDetails = syncDetailsByStatusForBillPayments.get(0);

        if (refundPaymentDetails != null) {
            int total = refundPaymentDetails.getTotalRecords();
            Map<String, QBResponseData> qbPaymentReceivedResponseMap = new HashMap<>();
            this.prepareRefundPaymentResponseMap(companyId, shopId, nl, xpath, refundPaymentDetails, errorLogsRepository, qbPaymentReceivedResponseMap, QuickbookSyncDetails.QuickbookEntityType.Expenses);

            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, refundPaymentDetails.getId());

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            List<ObjectId> transactionIds = new ArrayList<>();

            for (String transactionNo : referenceIdsMap.keySet()) {
                String transactionId = referenceIdsMap.get(transactionNo);
                transactionIds.add(new ObjectId(transactionId));
                if (qbPaymentReceivedResponseMap.containsKey(transactionNo)) {
                    referenceSuccess.put(transactionNo, transactionId);
                } else {
                    referenceError.put(transactionNo, transactionId);
                }
            }

            if (referenceSuccess.size() > 0) {
                for (String transactionNo : referenceSuccess.keySet()) {
                    transactionRepository.updateTransactionQbRefundPayment(companyId, shopId, transactionNo, true);
                }
            }

            if (referenceError.size() > 0) {
                for (String transactionNo : referenceError.keySet()) {
                    transactionRepository.updateTransactionQbRefundPaymentErrorAndTime(companyId, shopId, transactionNo, false, true, DateTime.now().getMillis());
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), referenceError.size(), refundPaymentDetails, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);
            }
    }

    private void  prepareRefundPaymentResponseMap(String companyId, String  shopId, NodeList nl, XPath xPath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository, Map<String, QBResponseData> qbPaymentReceivedResponseMap, QuickbookSyncDetails.QuickbookEntityType entityType) {
        Element n = null;
        Element refundNl = null;
        for (int i = 0; i < nl.getLength(); i++) {
            n = (Element) nl.item(i);
            try {
                String localName = n.getLocalName();
                if (localName.equalsIgnoreCase("CheckAddRs")) {
                    String statusCode = n.getAttribute("statusCode");
                    if (statusCode.equalsIgnoreCase("0")) {
                        QBResponseData qbResponseData = new QBResponseData();
                        NodeList paymentRet = n.getElementsByTagName("CheckRet");
                        for (int j = 0; j<paymentRet.getLength(); j++) {
                            refundNl = (Element) paymentRet.item(j);
                            qbResponseData.setQbRef(xPath.evaluate("./LinkedTxn/RefNumber", refundNl));
                            qbPaymentReceivedResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    } else {
                        // Save error logs
                        String message = n.getAttribute("statusMessage");
                        String code = n.getAttribute("statusCode");
                        saveErrorLogs(companyId, shopId, code, message, details.getId(), entityType, errorLogsRepository);
                    }
                }
            } catch (Exception ex) {
                log.info("Error : " + ex.getMessage());
            }
        }
    }
}

