package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncRefundStore {

    private static Logger log = Logger.getLogger(SyncJournalEntryStore.class.getName());
    private XPathExpression xpathExpression_RefundRef = null;
    private XPath xpath;

    //Declaration of XML Expression
    public SyncRefundStore() {
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_RefundRef = xpath.compile("//CreditMemoAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncRefundReceiptQueryResponse(String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        if (xpathExpression_RefundRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_RefundRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total Refund Entry in list : " + nl.getLength());
        if (nl.getLength() > 0) {
            quickBookRefundTransactionDataUpdate(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, transactionRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    public void quickBookRefundTransactionDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl,
                                                     QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, TransactionRepository transactionRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            int total = details.getTotalRecords();
            int failed = 0;
            Element n = null;
            Element refundNl = null;
            Map<String, QBResponseData> qbRefundResponseMap = new HashMap<>();
            List<String> salesRefundQbRefs = new ArrayList<>();
            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("CreditMemoAddRs")) {
                        String statusCode = n.getAttribute("statusCode");
                        if (statusCode.equalsIgnoreCase("0")) {
                            QBResponseData qbResponseData = new QBResponseData();
                            NodeList salesRet = n.getElementsByTagName("CreditMemoRet");
                            for (int j = 0; j<salesRet.getLength(); j++) {
                                refundNl = (Element) salesRet.item(j);
                                qbResponseData.setQbRef(xpath.evaluate("./RefNumber", refundNl));
                                qbResponseData.setTxnId(xpath.evaluate("./TxnID", refundNl));
                                qbRefundResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            }
                        } else {
                            // Save error logs
                            String message = n.getAttribute("statusMessage");
                            String code = n.getAttribute("statusCode");
                            saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, errorLogsRepository);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            for (String transactionId : referenceIdsMap.keySet()) {
                if (!qbRefundResponseMap.containsKey(referenceIdsMap.get(transactionId))) {
                    referenceError.put(transactionId, referenceIdsMap.get(transactionId));
                } else {
                    referenceSuccess.put(transactionId, referenceIdsMap.get(transactionId));
                }
            }

            if (referenceSuccess.size() > 0) {
                for (String transactionId : referenceSuccess.keySet()) {
                    String reference = referenceSuccess.get(transactionId);
                    QBResponseData qbResponseData = qbRefundResponseMap.get(reference);
                    if (qbResponseData != null) {
                        transactionRepository.updateQbDesktopQbRefundReceipt(companyId, shopId, qbResponseData.getQbRef(), qbResponseData.getTxnId());
                    }
                }
            }

            if (referenceError.size() > 0) {
                // Update error time and qb errored in transaction
                for (String transactionId : referenceError.keySet()) {
                    String reference = referenceError.get(transactionId);
                    transactionRepository.updateTransactionQbRefundErrorAndTime(companyId, shopId, reference,true, DateTime.now().getMillis());
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), failed, details, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

        }
    }
}
