package com.fourtwenty.quickbook.desktop.soap;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.VendorQueryRsType;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.VendorRet;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncVendorStore {

    private static Logger log = Logger.getLogger(SyncVendorStore.class.getName());
    private XPathExpression xpathExpression_VendorRef = null;
    private XPath xpath;
    private QBXML qbxml;
    private String response;
    private String companyId;
    private String shopId;
    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    private VendorRepository vendorRepository;
    private ErrorLogsRepository errorLogsRepository;
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;

    //Declaration of XML Expression
    //TODO : Change this to jaxb implementation
    public SyncVendorStore(QBXML qbxml, String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, VendorRepository vendorRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        this.qbxml = qbxml;
        this.response = response;
        this.companyId = companyId;
        this.shopId = shopId;
        this.quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
        this.vendorRepository = vendorRepository;
        this.errorLogsRepository = errorLogsRepository;
        this.qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;

        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_VendorRef = xpath.compile("//VendorAddRs | //VendorModRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }


    public int syncVendorAddModResponse() {
        if (xpathExpression_VendorRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));


        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_VendorRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            return 100;
        }

        log.info("Total Vendor in list : " + nl.getLength());
        if (nl.getLength() > 0) {
            quickBookVendorDataSave(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, vendorRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    public int syncVendorQueryResponse() {

        VendorQueryRsType vendorQueryRsType = null;

        List<Object> queryRs = qbxml.getQBXMLMsgsRs().getHostQueryRsOrCompanyQueryRsOrCompanyActivityQueryRs();
        for (Object queryR : queryRs) {
            if (queryR instanceof VendorQueryRsType) {
                vendorQueryRsType = (VendorQueryRsType) queryR;
                break;
            }
        }

        int success = 0;
        int failure = 0;
        int total = 0;

        if (vendorQueryRsType != null) {
            List<VendorRet> vendorRets = vendorQueryRsType.getVendorRet();
            if (vendorRets != null && !vendorRets.isEmpty()) {
                for (VendorRet vendorRet : vendorRets) {
                    vendorRepository.updateVendorEditSequence(companyId, shopId, vendorRet.getListID(), vendorRet.getEditSequence(), vendorRet.getName());
                    success++;
                }
                total = vendorRets.size();
            }
        }
        // Update sync details
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Vendor, statuses, QBConstants.QUICKBOOK_DESKTOP, QuickbookSyncDetails.SyncType.Pull, 5);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }
        if (details != null) {
            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, success, failure, details, quickbookSyncDetailsRepository);
        }

        return 100;
    }


    private void quickBookVendorDataSave(String companyId, String shopId, XPath xpath, NodeList nl,
                                         QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, VendorRepository vendorRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);

        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Vendor, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            int total = details.getTotalRecords();
            int failed = 0;
            Element n = null;

            Map<String, QBResponseData> qbVendorAddResponseMap = new HashMap<>();
            Map<String, QBResponseData> qbVendorModResponseMap = new HashMap<>();
            Map<String, QBResponseData> qbVendorResponseMap = new HashMap<>();

            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("VendorAddRs")) {
                        QBResponseData qbResponseData = prepareVendorResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbResponseData != null) {
                            qbVendorAddResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            qbVendorResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    } else if (localName.equalsIgnoreCase("VendorModRs")) {
                        QBResponseData qbModifiedVendorResponse = prepareVendorResponse(companyId, shopId, n, xpath, details, errorLogsRepository);
                        if (qbModifiedVendorResponse != null) {
                            qbVendorModResponseMap.put(qbModifiedVendorResponse.getQbRef(), qbModifiedVendorResponse);
                            qbVendorResponseMap.put(qbModifiedVendorResponse.getQbRef(), qbModifiedVendorResponse);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }

            List<ObjectId> vendors = new ArrayList<>();

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            for (String vendorId : referenceIdsMap.keySet()) {
                vendors.add(new ObjectId(vendorId));
                if (!qbVendorResponseMap.containsKey(referenceIdsMap.get(vendorId))) {
                    referenceError.put(vendorId, referenceIdsMap.get(vendorId));
                } else {
                    referenceSuccess.put(vendorId, referenceIdsMap.get(vendorId));
                }
            }

            HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyId, vendors);
            if (referenceError.size() > 0 && vendorHashMap.size() > 0) {
                // Update error time and qb errored in vendor
                for (String vendorId : referenceError.keySet()) {
                    if (vendorHashMap.containsKey(vendorId)) {
                        Vendor vendor = vendorHashMap.get(vendorId);
                        if (vendor != null) {
                            List<QBDataMapping> qbMapping = vendor.getQbMapping();
                            if (qbMapping != null && qbMapping.size() > 0) {
                                boolean status = false;
                                for (QBDataMapping qbDataMapping : qbMapping) {
                                    if (qbDataMapping.getShopId().equals(shopId)) {
                                        status = true;
                                        qbDataMapping.setQbErrored(true);
                                        qbDataMapping.setErrorTime(DateTime.now().getMillis());
                                    }
                                }

                                if (!status) {
                                    QBDataMapping mapping = new QBDataMapping();
                                    mapping.setShopId(shopId);
                                    mapping.setQbErrored(true);
                                    mapping.setErrorTime(DateTime.now().getMillis());
                                    qbMapping.add(mapping);
                                }
                            } else {
                                QBDataMapping dataMapping = new QBDataMapping();
                                dataMapping.setShopId(shopId);
                                dataMapping.setQbErrored(true);
                                dataMapping.setErrorTime(DateTime.now().getMillis());

                                qbMapping = new ArrayList<>();
                                qbMapping.add(dataMapping);
                            }
                            vendorRepository.updateVendorQbMapping(companyId, vendor.getId(), qbMapping);
                        }
                    }
                }
            }
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);

            Map<String, String> newSuccessDataMap = new HashMap<>();
            for (String vendorId : referenceSuccess.keySet()) {
                newSuccessDataMap.put(referenceSuccess.get(vendorId), vendorId);
            }

            // Update quickBook reference for vendor add
            if (newSuccessDataMap.size() > 0 && qbVendorAddResponseMap.size() > 0 && vendorHashMap.size() > 0) {
                for (String reference : qbVendorAddResponseMap.keySet()) {
                    QBResponseData qbResponseData = qbVendorAddResponseMap.get(reference);
                    if (qbResponseData != null) {
                        String vendorId = newSuccessDataMap.get(reference);
                        Vendor vendor = vendorHashMap.get(vendorId);
                        if (vendor != null) {
                            QBDataMapping dataMapping = new QBDataMapping();
                            dataMapping.setShopId(shopId);
                            dataMapping.setQbDesktopRef(qbResponseData.getQbRef());
                            dataMapping.setQbListId(qbResponseData.getListId());
                            dataMapping.setEditSequence(qbResponseData.getEditSequence());

                            List<QBDataMapping> qbMapping = vendor.getQbMapping();
                            if (qbMapping != null && qbMapping.size() > 0) {
                                qbMapping.add(dataMapping);
                            } else {
                                qbMapping = new ArrayList<>();
                                qbMapping.add(dataMapping);
                            }
                            vendorRepository.updateVendorQbMapping(companyId, vendor.getId(), qbMapping);
                        }
                    }
                }
            }

            if (newSuccessDataMap.size() > 0 && qbVendorModResponseMap.size() > 0 && vendorHashMap.size() > 0) {
                for (String reference : qbVendorModResponseMap.keySet()) {
                    QBResponseData qbResponseData = qbVendorModResponseMap.get(reference);
                    if (qbResponseData != null) {
                        String vendorId = newSuccessDataMap.get(reference);
                        Vendor vendor = vendorHashMap.get(vendorId);
                        if (vendor != null) {
                            List<QBDataMapping> qbMapping = vendor.getQbMapping();
                            for (QBDataMapping qbDataMapping : qbMapping) {
                                if (qbDataMapping.getShopId().equals(shopId) && qbDataMapping.getQbListId().equals(qbResponseData.getListId())) {
                                    qbDataMapping.setEditSequence(qbResponseData.getEditSequence());
                                    qbDataMapping.setQbDesktopRef(qbResponseData.getQbRef());
                                }
                            }
                            vendorRepository.updateVendorQbMapping(companyId, vendor.getId(), qbMapping);
                        }
                    }
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), failed, details, quickbookSyncDetailsRepository);
        }
    }

    private QBResponseData prepareVendorResponse(String companyId, String shopId, Element n, XPath xpath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository) {
        QBResponseData qbResponseData = null;
        try {
            Element vendorNl = null;
            String statusCode = n.getAttribute("statusCode");
            if (statusCode.equalsIgnoreCase("0")) {
                qbResponseData = new QBResponseData();
                NodeList vendorRet = n.getElementsByTagName("VendorRet");
                for (int j = 0; j < vendorRet.getLength(); j++) {
                    vendorNl = (Element) vendorRet.item(j);
                    qbResponseData.setQbRef(xpath.evaluate("./Name", vendorNl));
                    qbResponseData.setEditSequence(xpath.evaluate("./EditSequence", vendorNl));
                    qbResponseData.setListId(xpath.evaluate("./ListID", vendorNl));
                }
            } else {
                // Save error logs
                String message = n.getAttribute("statusMessage");
                String code = n.getAttribute("statusCode");
                saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Vendor, errorLogsRepository);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qbResponseData;
    }
}
