package com.fourtwenty.quickbook.desktop.soap.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "clientVersion", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clientVersion", namespace = "http://developer.intuit.com/")
public class ClientVersion {

    @XmlElement(name = "strVersion", namespace = "http://developer.intuit.com/")
    private String strVersion;

    /**
     * @return returns String
     */
    public String getStrVersion() {
        return this.strVersion;
    }

    /**
     * @param strVersion the value for the strVersion property
     */
    public void setStrVersion(String strVersion) {
        this.strVersion = strVersion;
    }

}