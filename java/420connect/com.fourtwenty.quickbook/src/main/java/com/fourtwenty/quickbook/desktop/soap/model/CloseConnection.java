package com.fourtwenty.quickbook.desktop.soap.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "closeConnection", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "closeConnection", namespace = "http://developer.intuit.com/")
public class CloseConnection {

    @XmlElement(name = "ticket", namespace = "http://developer.intuit.com/")
    private String ticket;

    /**
     * @return returns String
     */
    public String getTicket() {
        return this.ticket;
    }

    /**
     * @param ticket the value for the ticket property
     */
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

}