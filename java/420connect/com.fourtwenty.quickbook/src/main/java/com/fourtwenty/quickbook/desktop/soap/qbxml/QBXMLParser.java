package com.fourtwenty.quickbook.desktop.soap.qbxml;

import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.quickbook.desktop.soap.qbxml.generated.QBXML;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.StringReader;
import java.io.StringWriter;

public final class QBXMLParser {

    public static QBXML parseResponse(String message) {
        StringReader reader = new StringReader(message);
        try {
            return (QBXML) JAXBContext.newInstance(QBXML.class).createUnmarshaller().unmarshal(reader);
        } catch (Exception e) {
            throw new BlazeOperationException("Can not parseResponse response from QB", e);
        }
    }

    public static <T> String parseRequest(T t, Class<T> tClass, String localPart) {
        StringWriter stringWriter = new StringWriter();
        try {
            Marshaller marshaller = JAXBContext.newInstance(tClass).createMarshaller();
            JAXBElement<T> jaxbElement = new JAXBElement<>(new QName("", localPart), tClass, t);
            marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
            marshaller.marshal(jaxbElement, stringWriter);
            return stringWriter.toString();
        } catch (Exception e) {
            throw new BlazeOperationException("Can not parseResponse response from QB", e);
        }
    }

}
