package com.fourtwenty.quickbook.desktop.soap.response;

import com.fourtwenty.quickbook.desktop.soap.model.ArrayOfString;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "authenticateResponse", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "authenticateResponse", namespace = "http://developer.intuit.com/")
public class AuthenticateResponse {

    @XmlElement(name = "authenticateResult", namespace = "http://developer.intuit.com/")
    private ArrayOfString authenticateResult;

    /**
     * @return returns ArrayOfString
     */
    public ArrayOfString getAuthenticateResult() {
        return this.authenticateResult;
    }

    /**
     * @param authenticateResult the value for the authenticateResult property
     */
    public void setAuthenticateResult(ArrayOfString authenticateResult) {
        this.authenticateResult = authenticateResult;
    }

}