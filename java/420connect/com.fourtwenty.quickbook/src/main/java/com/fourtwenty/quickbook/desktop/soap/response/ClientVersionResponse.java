package com.fourtwenty.quickbook.desktop.soap.response;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "clientVersionResponse", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clientVersionResponse", namespace = "http://developer.intuit.com/")
public class ClientVersionResponse {

    @XmlElement(name = "clientVersionResult", namespace = "http://developer.intuit.com/")
    private String clientVersionResult;

    /**
     * @return returns String
     */
    public String getClientVersionResult() {
        return this.clientVersionResult;
    }

    /**
     * @param clientVersionResult the value for the clientVersionResult property
     */
    public void setClientVersionResult(String clientVersionResult) {
        this.clientVersionResult = clientVersionResult;
    }

}