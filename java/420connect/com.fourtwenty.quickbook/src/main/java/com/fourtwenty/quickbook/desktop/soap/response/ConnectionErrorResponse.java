package com.fourtwenty.quickbook.desktop.soap.response;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "connectionErrorResponse", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "connectionErrorResponse", namespace = "http://developer.intuit.com/")
public class ConnectionErrorResponse {

    @XmlElement(name = "connectionErrorResult", namespace = "http://developer.intuit.com/")
    private String connectionErrorResult;

    /**
     * @return returns String
     */
    public String getConnectionErrorResult() {
        return this.connectionErrorResult;
    }

    /**
     * @param connectionErrorResult the value for the connectionErrorResult property
     */
    public void setConnectionErrorResult(String connectionErrorResult) {
        this.connectionErrorResult = connectionErrorResult;
    }

}