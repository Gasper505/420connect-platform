package com.fourtwenty.quickbook.desktop.soap.response;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "getLastErrorResponse", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLastErrorResponse", namespace = "http://developer.intuit.com/")
public class GetLastErrorResponse {

    @XmlElement(name = "getLastErrorResult", namespace = "http://developer.intuit.com/")
    private String getLastErrorResult;

    /**
     * @return returns String
     */
    public String getGetLastErrorResult() {
        return this.getLastErrorResult;
    }

    /**
     * @param getLastErrorResult the value for the getLastErrorResult property
     */
    public void setGetLastErrorResult(String getLastErrorResult) {
        this.getLastErrorResult = getLastErrorResult;
    }

}