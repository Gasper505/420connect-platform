package com.fourtwenty.quickbook.desktop.soap.response;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.warehouse.core.domain.models.invoice.PaymentsReceived;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class PaymentReceivedStore {

    private static Logger log = Logger.getLogger(SyncBillStore.class.getName());
    private XPathExpression xpathExpression_PaymentReceivedRef = null;
    private XPath xpath;

    //Declaration of XML Expression
    public PaymentReceivedStore() {
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_PaymentReceivedRef = xpath.compile("//ReceivePaymentAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }


    public int syncPaymentReceivedQueryResponse(String response, String companyId, String shopId, InvoicePaymentsRepository invoicePaymentsRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                            InvoiceRepository invoiceRepository, ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {
        if (xpathExpression_PaymentReceivedRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_PaymentReceivedRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Payment received Entry in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookPaymentReceivedDataUpdate(companyId, shopId, xpath, nl, invoicePaymentsRepository, quickbookSyncDetailsRepository, invoiceRepository, errorLogsRepository, qbDesktopCurrentSyncRepository);
        }
        return 100;
    }

    private void quickBookPaymentReceivedDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl, InvoicePaymentsRepository invoicePaymentsRepository,
                                                  QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, InvoiceRepository invoiceRepository,
                                                  ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository) {


        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.PaymentReceived, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            int total = details.getTotalRecords();
            Element n = null;
            Element billNl = null;

            Map<String, QBResponseData> qbPaymentReceivedResponseMap = new HashMap<>();
            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("ReceivePaymentAddRs")) {
                        String statusCode = n.getAttribute("statusCode");
                        if (statusCode.equalsIgnoreCase("0")) {
                            QBResponseData qbResponseData = new QBResponseData();
                            NodeList purchaseRet = n.getElementsByTagName("ReceivePaymentRet");
                            for (int j = 0; j<purchaseRet.getLength(); j++) {
                                billNl = (Element) purchaseRet.item(j);
                                qbResponseData.setQbRef(xpath.evaluate("./RefNumber", billNl));
                                qbPaymentReceivedResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            }
                        } else {
                            // Save error logs
                            String message = n.getAttribute("statusMessage");
                            String code = n.getAttribute("statusCode");
                            saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.PaymentReceived, errorLogsRepository);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            List<ObjectId> paymentReceivedIds = new ArrayList<>();
            for (String paymentReceivedId : referenceIdsMap.keySet()) {
                paymentReceivedIds.add(new ObjectId(paymentReceivedId));
                if (!qbPaymentReceivedResponseMap.containsKey(referenceIdsMap.get(paymentReceivedId))) {
                    referenceError.put(paymentReceivedId, referenceIdsMap.get(paymentReceivedId));
                } else {
                    referenceSuccess.put(paymentReceivedId, referenceIdsMap.get(paymentReceivedId));
                }
            }

            HashMap<String, PaymentsReceived> paymentsReceivedHashMap = invoicePaymentsRepository.listAsMap(companyId, paymentReceivedIds);

            if (referenceSuccess.size() > 0) {
                for (String paymentReceivedId : referenceSuccess.keySet()) {
                    PaymentsReceived paymentsReceived = paymentsReceivedHashMap.get(paymentReceivedId);
                    if (StringUtils.isNotBlank(paymentsReceived.getInvoiceId())) {
                        invoiceRepository.updateQbDesktopPaymentReceivedRef(companyId, shopId, paymentsReceived.getInvoiceId(), Boolean.TRUE, Boolean.FALSE,0);
                    }

                    String reference = referenceSuccess.get(paymentReceivedId);
                    QBResponseData qbResponseData = qbPaymentReceivedResponseMap.get(reference);
                    if (qbResponseData != null) {
                        invoicePaymentsRepository.updateQbPaymentReceivedRef(companyId, shopId, paymentReceivedId, qbResponseData.getQbRef());
                    }
                }
            }

            if (referenceError.size() > 0) {
                // Update error time and qb errored in payment received and purchase order
                for (String paymentReceivedId : referenceError.keySet()) {
                    PaymentsReceived paymentsReceived = paymentsReceivedHashMap.get(paymentReceivedId);
                    if (StringUtils.isNotBlank(paymentsReceived.getInvoiceId())) {
                        invoiceRepository.updateQbDesktopPaymentReceivedRef(companyId, shopId, paymentsReceived.getInvoiceId(), Boolean.FALSE, Boolean.TRUE, DateTime.now().getMillis());
                    }
                    invoicePaymentsRepository.updateDesktopPaymentReceivedQbErrorAndTime(companyId, shopId, paymentsReceived.getId(), Boolean.TRUE, DateTime.now().getMillis());
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), referenceError.size(), details, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);
        }
    }
}
