package com.fourtwenty.quickbook.desktop.soap.response;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "sendRequestXMLResponse", namespace = "http://developer.intuit.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendRequestXMLResponse", namespace = "http://developer.intuit.com/")
public class SendRequestXMLResponse {

    @XmlElement(name = "sendRequestXMLResult", namespace = "http://developer.intuit.com/")
    private String sendRequestXMLResult;

    /**
     * @return returns String
     */
    public String getSendRequestXMLResult() {
        return this.sendRequestXMLResult;
    }

    /**
     * @param sendRequestXMLResult the value for the sendRequestXMLResult property
     */
    public void setSendRequestXMLResult(String sendRequestXMLResult) {
        this.sendRequestXMLResult = sendRequestXMLResult;
    }

}