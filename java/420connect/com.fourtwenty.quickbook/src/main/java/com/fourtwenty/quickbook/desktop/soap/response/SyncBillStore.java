package com.fourtwenty.quickbook.desktop.soap.response;


import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncBillStore {

    private static Logger log = Logger.getLogger(SyncBillStore.class.getName());
    private XPathExpression xpathExpression_BillRef = null;
    private XPath xpath;

    //Declaration of XML Expression
    public SyncBillStore() {
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_BillRef = xpath.compile("//BillAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncBillQueryResponse(String response, String companyId, String shopId, ShipmentBillRepository shipmentBillRepository, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                     PurchaseOrderRepository purchaseOrderRepository, ErrorLogsRepository errorLogsRepository,
                                     QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, QuickbookCustomEntities quickbookCustomEntities) {
        if (xpathExpression_BillRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_BillRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Total Bill Entry in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookBillDataUpdate(companyId, shopId, xpath, nl, shipmentBillRepository, quickbookSyncDetailsRepository, purchaseOrderRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, quickbookCustomEntities);
        }
        return 100;
    }

    public void quickBookBillDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl, ShipmentBillRepository shipmentBillRepository,
                                                 QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, PurchaseOrderRepository purchaseOrderRepository,
                                                 ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, QuickbookCustomEntities quickbookCustomEntities) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);
        List<QuickbookSyncDetails> syncDetailsByStatus = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Bill, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails details = null;
        for (QuickbookSyncDetails detailsByStatus : syncDetailsByStatus) {
            details = detailsByStatus;
            break;
        }

        if (details != null) {
            int total = details.getTotalRecords();
            Element n = null;
            Element billNl = null;

            Map<String, QBResponseData> qbBillResponseMap = new HashMap<>();
            for (int i = 0; i < nl.getLength(); i++) {
                n = (Element) nl.item(i);
                try {
                    String localName = n.getLocalName();
                    if (localName.equalsIgnoreCase("BillAddRs")) {
                        String statusCode = n.getAttribute("statusCode");
                        if (statusCode.equalsIgnoreCase("0")) {
                            QBResponseData qbResponseData = new QBResponseData();
                            NodeList purchaseRet = n.getElementsByTagName("BillRet");
                            for (int j = 0; j<purchaseRet.getLength(); j++) {
                                billNl = (Element) purchaseRet.item(j);
                                qbResponseData.setQbRef(xpath.evaluate("./RefNumber", billNl));
                                qbResponseData.setTxnId(xpath.evaluate("./TxnID", billNl));
                                qbBillResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                            }
                        } else {
                            // Save error logs
                            String message = n.getAttribute("statusMessage");
                            String code = n.getAttribute("statusCode");
                            saveErrorLogs(companyId, shopId, code, message, details.getId(), QuickbookSyncDetails.QuickbookEntityType.Bill, errorLogsRepository);
                        }
                    }
                } catch (Exception ex) {
                    log.info("Error : " + ex.getMessage());
                }
            }


            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, details.getId());

            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            List<ObjectId> shipmentBillIds = new ArrayList<>();
            for (String billId : referenceIdsMap.keySet()) {
                shipmentBillIds.add(new ObjectId(billId));
                if (!qbBillResponseMap.containsKey(referenceIdsMap.get(billId))) {
                    referenceError.put(billId, referenceIdsMap.get(billId));
                } else {
                    referenceSuccess.put(billId, referenceIdsMap.get(billId));
                }
            }

            HashMap<String, ShipmentBill> shipmentBillHashMap = shipmentBillRepository.listAsMap(companyId, shipmentBillIds);

            if (referenceSuccess.size() > 0) {
                for (String billId : referenceSuccess.keySet()) {
                    ShipmentBill shipmentBill = shipmentBillHashMap.get(billId);
                    if (StringUtils.isNotBlank(shipmentBill.getPurchaseOrderId())) {
                        purchaseOrderRepository.updateQbDesktopPurchaseOrderBillRef(companyId, shopId, shipmentBill.getPurchaseOrderId(), Boolean.TRUE);
                    }

                    String reference = referenceSuccess.get(billId);
                    QBResponseData qbResponseData = qbBillResponseMap.get(reference);
                    if (qbResponseData != null) {
                        shipmentBillRepository.updateQbDesktopBillRef(companyId, shopId, qbResponseData.getQbRef(), qbResponseData.getTxnId());
                    }
                }
            }

            if (referenceError.size() > 0) {
                // Update error time and qb errored in shipment bill and purchase order
                for (String billId : referenceError.keySet()) {
                    ShipmentBill shipmentBill = shipmentBillHashMap.get(billId);
                    if (StringUtils.isNotBlank(shipmentBill.getPurchaseOrderId())) {
                        purchaseOrderRepository.updateQbDesktopPurchaseOrderBillRef(companyId, shopId, shipmentBill.getPurchaseOrderId(), Boolean.FALSE);
                    }
                    String reference = referenceError.get(billId);
                    shipmentBillRepository.updateDesktopBillQbErrorAndTime(companyId, shopId, reference, Boolean.TRUE, DateTime.now().getMillis());
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), referenceError.size(), details, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);
        }
    }
}
