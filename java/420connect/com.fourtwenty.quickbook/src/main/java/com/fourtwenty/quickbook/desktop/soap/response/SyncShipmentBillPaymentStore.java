package com.fourtwenty.quickbook.desktop.soap.response;

import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.quickbook.desktop.data.QBResponseData;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import static com.fourtwenty.quickbook.desktop.helpers.CurrentSync.updateCurrentSync;
import static com.fourtwenty.quickbook.desktop.helpers.SaveErrorLogs.saveErrorLogs;
import static com.fourtwenty.quickbook.desktop.helpers.UpdateQbSyncDetail.qbSyncDetailsUpdate;

public class SyncShipmentBillPaymentStore {
    private static Logger log = Logger.getLogger(SyncBillStore.class.getName());
    private XPathExpression xpathExpression_ShipmentBillPaymentRef = null;
    private XPath xpath;

    //Declaration of XML Expression
    public SyncShipmentBillPaymentStore() {
        xpath = XPathFactory.newInstance().newXPath();
        try {
            xpathExpression_ShipmentBillPaymentRef = xpath.compile("//BillPaymentCheckAddRs");
        } catch (XPathExpressionException e) {
            log.severe("Error while compiling xpath expression : "
                    + e.getMessage());
        }
    }

    public int syncShipmentBillPaymentQueryResponse(String response, String companyId, String shopId, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository,
                                                    ErrorLogsRepository errorLogsRepository, QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ShipmentBillRepository shipmentBillRepository, QuickbookCustomEntities customEntities) {
        if (xpathExpression_ShipmentBillPaymentRef == null) {
            log.severe("Xpath expression was not initialized, hence not parsing the response");
        }

        InputSource s = new InputSource(new StringReader(response));

        NodeList nl = null;
        try {
            nl = (NodeList) xpathExpression_ShipmentBillPaymentRef.evaluate(s, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            log.severe("Unable to evaluate xpath on the response xml doc. hence returning "
                    + e.getMessage());
            log.info("Query response : " + response);
            e.printStackTrace();
            return 100;
        }

        log.info("Payment received Entry in list : " + nl.getLength());
        if (nl.getLength() > 0 ) {
            quickBookShipmentPaymentDataUpdate(companyId, shopId, xpath, nl, quickbookSyncDetailsRepository, errorLogsRepository, qbDesktopCurrentSyncRepository, shipmentBillRepository, customEntities);
        }
        return 100;
    }

    private void quickBookShipmentPaymentDataUpdate(String companyId, String shopId, XPath xpath, NodeList nl, QuickbookSyncDetailsRepository quickbookSyncDetailsRepository, ErrorLogsRepository errorLogsRepository,
                                                    QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository, ShipmentBillRepository shipmentBillRepository, QuickbookCustomEntities quickbookCustomEntities) {
        List<QuickbookSyncDetails.Status> statuses = new ArrayList<>();
        statuses.add(QuickbookSyncDetails.Status.Inprogress);

        List<QuickbookSyncDetails> syncDetailsByStatusForBillPayments = quickbookSyncDetailsRepository.getSyncDetailsByStatus(companyId, shopId, QuickbookSyncDetails.QuickbookEntityType.BillPayment, statuses, QBConstants.QUICKBOOK_DESKTOP, 0);
        QuickbookSyncDetails billPaymentDetails = null;
        for (QuickbookSyncDetails syncDetailsByStatusForBillPayment : syncDetailsByStatusForBillPayments) {
            billPaymentDetails = syncDetailsByStatusForBillPayment;
            break;
        }

        if (billPaymentDetails != null) {
            int total = billPaymentDetails.getTotalRecords();
            Map<String, QBResponseData> qbPaymentReceivedResponseMap = new HashMap<>();
            this.preparePaymentReceivedResponseMap(companyId, shopId, nl, xpath, billPaymentDetails, errorLogsRepository, qbPaymentReceivedResponseMap, QuickbookSyncDetails.QuickbookEntityType.BillPayment);

            QbDesktopCurrentSync qbDesktopCurrentSync = qbDesktopCurrentSyncRepository.getQbDesktopCurrentSync(companyId, shopId, billPaymentDetails.getId());

            Set<ObjectId> shipmentBillIds = new HashSet<>();
            Map<String, String> referenceError = new HashMap<>();
            Map<String, String> referenceSuccess = new HashMap<>();
            Map<String, String> referenceIdsMap = qbDesktopCurrentSync.getReferenceIdsMap();
            List<String> paymentReceivedIds = new ArrayList<>();

            for (String paymentNo : referenceIdsMap.keySet()) {
                paymentReceivedIds.add(paymentNo);
                String shipmentBillId = referenceIdsMap.get(paymentNo);
                if (qbPaymentReceivedResponseMap.containsKey(paymentNo)) {
                    referenceSuccess.put(paymentNo, shipmentBillId);
                } else {
                    referenceError.put(paymentNo, shipmentBillId);
                }
                shipmentBillIds.add(new ObjectId(referenceIdsMap.get(paymentNo)));
            }

            HashMap<String, ShipmentBill> shipmentBillHashMap = shipmentBillRepository.listAsMap(companyId, Lists.newArrayList(shipmentBillIds));
            HashMap<String, List<ShipmentBillPayment>> paymentHasMapForBill = new HashMap<>();

            for (String billId : shipmentBillHashMap.keySet()) {
                ShipmentBill bill = shipmentBillHashMap.get(billId);
                if (bill != null && bill.getPaymentHistory().size() > 0) {
                    paymentHasMapForBill.put(bill.getId(), bill.getPaymentHistory());
                }
            }

            if (referenceSuccess.size() > 0) {
                for (String paymentNo : referenceSuccess.keySet()) {
                    List<ShipmentBillPayment> shipmentBillPayments = paymentHasMapForBill.get(referenceSuccess.get(paymentNo));
                    if (shipmentBillPayments != null && shipmentBillPayments.size() > 0) {
                        for (ShipmentBillPayment shipmentBillPayment : shipmentBillPayments) {
                            if (shipmentBillPayment.getPaymentNo().equals(paymentNo)) {
                                shipmentBillPayment.setQbPaymentReceived(true);
                                shipmentBillPayment.setQbPaymentReceivedError(false);
                            }
                        }
                    }
                }
            }

            if (referenceError.size() > 0) {
                for (String paymentNo : referenceError.keySet()) {
                    List<ShipmentBillPayment> shipmentBillPayments = paymentHasMapForBill.get(referenceError.get(paymentNo));
                    if (shipmentBillPayments != null && shipmentBillPayments.size() > 0) {
                        for (ShipmentBillPayment shipmentBillPayment : shipmentBillPayments) {
                            if (shipmentBillPayment.getPaymentNo().equals(paymentNo)) {
                                shipmentBillPayment.setQbPaymentReceived(false);
                                shipmentBillPayment.setQbPaymentReceivedError(true);
                                shipmentBillPayment.setQbPaymentReceivedErrorTime(DateTime.now().getMillis());
                            }
                        }
                    }
                }
            }

            for (String shipmentBillId : paymentHasMapForBill.keySet()) {
                List<ShipmentBillPayment> shipmentBillPayments = paymentHasMapForBill.get(shipmentBillId);
                if (shipmentBillPayments.size() > 0) {
                    shipmentBillRepository.updateShipmentBillPaymentHistory(companyId, shipmentBillId, shipmentBillPayments);
                }
            }

            // update quick book sync details
            qbSyncDetailsUpdate(companyId, total, referenceSuccess.size(), referenceError.size(), billPaymentDetails, quickbookSyncDetailsRepository);
            updateCurrentSync(qbDesktopCurrentSync, referenceError, qbDesktopCurrentSyncRepository);
        }

    }

    private void  preparePaymentReceivedResponseMap(String companyId, String  shopId, NodeList nl, XPath xPath, QuickbookSyncDetails details, ErrorLogsRepository errorLogsRepository, Map<String, QBResponseData> qbPaymentReceivedResponseMap, QuickbookSyncDetails.QuickbookEntityType entityType) {
        Element n = null;
        Element billNl = null;
        for (int i = 0; i < nl.getLength(); i++) {
            n = (Element) nl.item(i);
            try {
                String localName = n.getLocalName();
                if (localName.equalsIgnoreCase("BillPaymentCheckAddRs")) {
                    String statusCode = n.getAttribute("statusCode");
                    if (statusCode.equalsIgnoreCase("0")) {
                        QBResponseData qbResponseData = new QBResponseData();
                        NodeList purchaseRet = n.getElementsByTagName("BillPaymentCheckRet");
                        for (int j = 0; j<purchaseRet.getLength(); j++) {
                            billNl = (Element) purchaseRet.item(j);
                            qbResponseData.setQbRef(xPath.evaluate("./RefNumber", billNl));
                            qbPaymentReceivedResponseMap.put(qbResponseData.getQbRef(), qbResponseData);
                        }
                    } else {
                        // Save error logs
                        String message = n.getAttribute("statusMessage");
                        String code = n.getAttribute("statusCode");
                        saveErrorLogs(companyId, shopId, code, message, details.getId(), entityType, errorLogsRepository);
                    }
                }
            } catch (Exception ex) {
                log.info("Error : " + ex.getMessage());
            }
        }
    }
}
