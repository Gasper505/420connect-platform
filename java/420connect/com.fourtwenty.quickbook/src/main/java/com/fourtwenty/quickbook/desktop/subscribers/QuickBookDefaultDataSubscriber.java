package com.fourtwenty.quickbook.desktop.subscribers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.quickbook.repositories.QbAccountRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.quickbook.QuickBookDefaultDataEvent;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookDefaultData;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class QuickBookDefaultDataSubscriber implements BlazeSubscriber {
    private static final Logger LOGGER = LoggerFactory.getLogger(QuickBookDefaultDataSubscriber.class);

    @Inject
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    @Inject
    private QbAccountRepository qbAccountRepository;

    @Subscribe
    public void subscribe(QuickBookDefaultDataEvent event) {
        String companyId = event.getCompanyId();
        String shopId = event.getShopId();

        InputStream inputStream = QuickBookDefaultDataSubscriber.class.getResourceAsStream("/qbdata.json");
        ObjectMapper objectMapper = new ObjectMapper();
        QuickBookDefaultData quickBookDefaultData = new QuickBookDefaultData();
        try {
            quickBookDefaultData = objectMapper.readValue(inputStream, QuickBookDefaultData.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        List<QBDesktopOperation> qbDesktopOperationList = new ArrayList<>();
        for (QBDesktopOperation qbDesktopOperation : quickBookDefaultData.getQbDesktopOperations()) {
            qbDesktopOperation.prepare(companyId);
            qbDesktopOperation.setShopId(shopId);
            qbDesktopOperationList.add(qbDesktopOperation);
        }

        qbDesktopOperationRepository.save(qbDesktopOperationList);
    }
}
