package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;

import java.math.BigDecimal;

public class BillPaymentReceivedWrapper {
    private String qbVendorRef;
    private String qbVendorListId;
    private String memo;
    private String txnId;
    private BigDecimal amountPaid;
    private String payableAccount;
    private String bankAccount;
    private ShipmentBillPayment.PaymentType paymentType;
    private long paidDate;
    private String refNo;

    public String getQbVendorRef() {
        return qbVendorRef;
    }

    public void setQbVendorRef(String qbVendorRef) {
        this.qbVendorRef = qbVendorRef;
    }

    public String getQbVendorListId() {
        return qbVendorListId;
    }

    public void setQbVendorListId(String qbVendorListId) {
        this.qbVendorListId = qbVendorListId;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getPayableAccount() {
        return payableAccount;
    }

    public void setPayableAccount(String payableAccount) {
        this.payableAccount = payableAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public long getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(long paidDate) {
        this.paidDate = paidDate;
    }

    public ShipmentBillPayment.PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(ShipmentBillPayment.PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }
}
