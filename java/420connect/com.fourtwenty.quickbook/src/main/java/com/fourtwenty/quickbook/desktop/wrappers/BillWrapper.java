package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;

import java.util.HashMap;

public class BillWrapper extends ShipmentBill {
    private String vendorQbRef;
    private String vendorListId;
    private Address address;
    private HashMap<String, Product> productMap = new HashMap<>();

    public String getVendorQbRef() {
        return vendorQbRef;
    }

    public void setVendorQbRef(String vendorQbRef) {
        this.vendorQbRef = vendorQbRef;
    }

    public String getVendorListId() {
        return vendorListId;
    }

    public void setVendorListId(String vendorListId) {
        this.vendorListId = vendorListId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public HashMap<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(HashMap<String, Product> productMap) {
        this.productMap = productMap;
    }
}
