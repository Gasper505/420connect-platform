package com.fourtwenty.quickbook.desktop.wrappers;

public class CategorySaleWrapper {
    private String categoryId;
    private String qbCategoryName;
    private double cogs;
    private double discounts;
    private double exciseTax;
    private double deliveryFees;
    public double creditCardFees;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getQbCategoryName() {
        return qbCategoryName;
    }

    public void setQbCategoryName(String qbCategoryName) {
        this.qbCategoryName = qbCategoryName;
    }

    public double getCogs() {
        return cogs;
    }

    public void setCogs(double cogs) {
        this.cogs = cogs;
    }

    public double getDiscounts() {
        return discounts;
    }

    public void setDiscounts(double discounts) {
        this.discounts = discounts;
    }

    public double getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(double exciseTax) {
        this.exciseTax = exciseTax;
    }

    public double getDeliveryFees() {
        return deliveryFees;
    }

    public void setDeliveryFees(double deliveryFees) {
        this.deliveryFees = deliveryFees;
    }

    public double getCreditCardFees() {
        return creditCardFees;
    }

    public void setCreditCardFees(double creditCardFees) {
        this.creditCardFees = creditCardFees;
    }
}
