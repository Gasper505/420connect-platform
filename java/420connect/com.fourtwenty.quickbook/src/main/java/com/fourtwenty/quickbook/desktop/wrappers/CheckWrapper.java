package com.fourtwenty.quickbook.desktop.wrappers;

import java.math.BigDecimal;

public class CheckWrapper {

    private String accountRef;
    private String accountReceivable;
    private String payeeEntityRef;
    private String payeeRefListId;
    private long txnDate;
    private String RefNumber;
    private String txnId;
    private BigDecimal amount;

    public String getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(String accountRef) {
        this.accountRef = accountRef;
    }

    public String getAccountReceivable() {
        return accountReceivable;
    }

    public void setAccountReceivable(String accountReceivable) {
        this.accountReceivable = accountReceivable;
    }

    public String getPayeeEntityRef() {
        return payeeEntityRef;
    }

    public void setPayeeEntityRef(String payeeEntityRef) {
        this.payeeEntityRef = payeeEntityRef;
    }

    public String getPayeeRefListId() {
        return payeeRefListId;
    }

    public void setPayeeRefListId(String payeeRefListId) {
        this.payeeRefListId = payeeRefListId;
    }

    public long getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(long txnDate) {
        this.txnDate = txnDate;
    }

    public String getRefNumber() {
        return RefNumber;
    }

    public void setRefNumber(String refNumber) {
        RefNumber = refNumber;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
