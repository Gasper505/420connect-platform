package com.fourtwenty.quickbook.desktop.wrappers;

import java.math.BigDecimal;

public class InventoryAdjustmentWrapper {

    private String productListId;
    private String productQBDesktopRef;
    private String productId;
    private BigDecimal quantity;
    private BigDecimal value;
    private String accountName;
    private boolean quantitySynced = Boolean.FALSE;

    public String getProductListId() {
        return productListId;
    }

    public void setProductListId(String productListId) {
        this.productListId = productListId;
    }

    public String getProductQBDesktopRef() {
        return productQBDesktopRef;
    }

    public void setProductQBDesktopRef(String productQBDesktopRef) {
        this.productQBDesktopRef = productQBDesktopRef;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public boolean isQuantitySynced() {
        return quantitySynced;
    }

    public void setQuantitySynced(boolean quantitySynced) {
        this.quantitySynced = quantitySynced;
    }
}
