package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.product.Product;
import com.warehouse.core.domain.models.invoice.Invoice;

import java.util.HashMap;

public class InvoiceWrapper extends Invoice {

    private String vendorName;
    private String vendorQbRef;
    private String vendorQbListId;
    private HashMap<String, Product> productMap = new HashMap<>();
    private String receivableAccount;

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorQbRef() {
        return vendorQbRef;
    }

    public void setVendorQbRef(String vendorQbRef) {
        this.vendorQbRef = vendorQbRef;
    }

    @Override
    public String getVendorQbListId() {
        return vendorQbListId;
    }

    @Override
    public void setVendorQbListId(String vendorQbListId) {
        this.vendorQbListId = vendorQbListId;
    }

    public HashMap<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(HashMap<String, Product> productMap) {
        this.productMap = productMap;
    }

    public String getReceivableAccount() {
        return receivableAccount;
    }

    public void setReceivableAccount(String receivableAccount) {
        this.receivableAccount = receivableAccount;
    }
}
