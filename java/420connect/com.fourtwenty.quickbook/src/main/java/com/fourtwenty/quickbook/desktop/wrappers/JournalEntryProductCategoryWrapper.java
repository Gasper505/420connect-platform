package com.fourtwenty.quickbook.desktop.wrappers;

import java.math.BigDecimal;

public class JournalEntryProductCategoryWrapper {

    private String  qbCategoryRef;
    private String debitAccountRef;
    private BigDecimal debitAccountAmount;
    private String creditAccountRef;
    private BigDecimal creditAccountAmount;
    private double discounts;
    private double exciseTax;
    private double deliveryFees;
    public double creditCardFees;

    public String getQbCategoryRef() {
        return qbCategoryRef;
    }

    public void setQbCategoryRef(String qbCategoryRef) {
        this.qbCategoryRef = qbCategoryRef;
    }

    public String getDebitAccountRef() {
        return debitAccountRef;
    }

    public void setDebitAccountRef(String debitAccountRef) {
        this.debitAccountRef = debitAccountRef;
    }

    public BigDecimal getDebitAccountAmount() {
        return debitAccountAmount;
    }

    public void setDebitAccountAmount(BigDecimal debitAccountAmount) {
        this.debitAccountAmount = debitAccountAmount;
    }

    public String getCreditAccountRef() {
        return creditAccountRef;
    }

    public void setCreditAccountRef(String creditAccountRef) {
        this.creditAccountRef = creditAccountRef;
    }

    public BigDecimal getCreditAccountAmount() {
        return creditAccountAmount;
    }

    public void setCreditAccountAmount(BigDecimal creditAccountAmount) {
        this.creditAccountAmount = creditAccountAmount;
    }

    public double getDiscounts() {
        return discounts;
    }

    public void setDiscounts(double discounts) {
        this.discounts = discounts;
    }

    public double getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(double exciseTax) {
        this.exciseTax = exciseTax;
    }

    public double getDeliveryFees() {
        return deliveryFees;
    }

    public void setDeliveryFees(double deliveryFees) {
        this.deliveryFees = deliveryFees;
    }

    public double getCreditCardFees() {
        return creditCardFees;
    }

    public void setCreditCardFees(double creditCardFees) {
        this.creditCardFees = creditCardFees;
    }
}
