package com.fourtwenty.quickbook.desktop.wrappers;

import java.math.BigDecimal;

public class JournalEntryWrapper {

    private Long transactionProcessedDate;
    private String transNo;
    private String debitAccountRef;
    private BigDecimal debitAccountAmount;
    private String creditAccountRef;
    private BigDecimal creditAccountAmount;

    public Long getTransactionProcessedDate() {
        return transactionProcessedDate;
    }

    public void setTransactionProcessedDate(Long transactionProcessedDate) {
        this.transactionProcessedDate = transactionProcessedDate;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public String getDebitAccountRef() {
        return debitAccountRef;
    }

    public void setDebitAccountRef(String debitAccountRef) {
        this.debitAccountRef = debitAccountRef;
    }

    public BigDecimal getDebitAccountAmount() {
        return debitAccountAmount;
    }

    public void setDebitAccountAmount(BigDecimal debitAccountAmount) {
        this.debitAccountAmount = debitAccountAmount;
    }

    public String getCreditAccountRef() {
        return creditAccountRef;
    }

    public void setCreditAccountRef(String creditAccountRef) {
        this.creditAccountRef = creditAccountRef;
    }

    public BigDecimal getCreditAccountAmount() {
        return creditAccountAmount;
    }

    public void setCreditAccountAmount(BigDecimal creditAccountAmount) {
        this.creditAccountAmount = creditAccountAmount;
    }
}
