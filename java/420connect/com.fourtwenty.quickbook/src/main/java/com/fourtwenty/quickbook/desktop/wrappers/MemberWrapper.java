package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.customer.Member;

public class MemberWrapper extends Member {
    private String qbMemberName;
    private String qbRef;
    private String listId;
    private String qbEditSequence;

    public String getQbMemberName() {
        return qbMemberName;
    }

    public void setQbMemberName(String qbMemberName) {
        this.qbMemberName = qbMemberName;
    }

    public String getQbRef() {
        return qbRef;
    }

    public void setQbRef(String qbRef) {
        this.qbRef = qbRef;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getQbEditSequence() {
        return qbEditSequence;
    }

    public void setQbEditSequence(String qbEditSequence) {
        this.qbEditSequence = qbEditSequence;
    }
}
