package com.fourtwenty.quickbook.desktop.wrappers;

import com.warehouse.core.domain.models.invoice.PaymentsReceived;

import java.math.BigDecimal;

public class PaymentReceivedWrapper extends PaymentsReceived {

    private String qbCustomerRef;
    private String qbCustomerListId;
    private String memo;
    private String txnId;
    private BigDecimal totalAmount;
    private String receivableAccount;
    private String depositAccount;

    public String getQbCustomerRef() {
        return qbCustomerRef;
    }

    public void setQbCustomerRef(String qbCustomerRef) {
        this.qbCustomerRef = qbCustomerRef;
    }

    public String getQbCustomerListId() {
        return qbCustomerListId;
    }

    public void setQbCustomerListId(String qbCustomerListId) {
        this.qbCustomerListId = qbCustomerListId;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getReceivableAccount() {
        return receivableAccount;
    }

    public void setReceivableAccount(String receivableAccount) {
        this.receivableAccount = receivableAccount;
    }

    public String getDepositAccount() {
        return depositAccount;
    }

    public void setDepositAccount(String depositAccount) {
        this.depositAccount = depositAccount;
    }
}
