package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.quickbook.desktop.data.PredefinedDataRequest;

import java.util.List;

public class PredefinedItemWrapper {
    private List<PredefinedDataRequest> requests;

    public List<PredefinedDataRequest> getRequests() {
        return requests;
    }

    public void setRequests(List<PredefinedDataRequest> requests) {
        this.requests = requests;
    }
}
