package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.quickbook.desktop.request.ProductCategoryPORequest;

import java.util.List;

public class ProductCategoryBillWrapper extends ShipmentBill {
    private String vendorQbRef;
    private String vendorListId;
    private Address address;
    private List<ProductCategoryPORequest> requests;

    public String getVendorQbRef() {
        return vendorQbRef;
    }

    public void setVendorQbRef(String vendorQbRef) {
        this.vendorQbRef = vendorQbRef;
    }

    public String getVendorListId() {
        return vendorListId;
    }

    public void setVendorListId(String vendorListId) {
        this.vendorListId = vendorListId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<ProductCategoryPORequest> getRequests() {
        return requests;
    }

    public void setRequests(List<ProductCategoryPORequest> requests) {
        this.requests = requests;
    }
}
