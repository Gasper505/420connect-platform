package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.quickbook.desktop.request.ProductCategoryPORequest;

import java.util.List;

public class ProductCategoryPOWrapper extends PurchaseOrder {
    private String qbVendorRef;
    private String qbVendorListId;
    private List<ProductCategoryPORequest> requests;

    public String getQbVendorRef() {
        return qbVendorRef;
    }

    public void setQbVendorRef(String qbVendorRef) {
        this.qbVendorRef = qbVendorRef;
    }

    public String getQbVendorListId() {
        return qbVendorListId;
    }

    public void setQbVendorListId(String qbVendorListId) {
        this.qbVendorListId = qbVendorListId;
    }

    public List<ProductCategoryPORequest> getRequests() {
        return requests;
    }

    public void setRequests(List<ProductCategoryPORequest> requests) {
        this.requests = requests;
    }
}
