package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.product.ProductCategory;

public class ProductCategoryWrapper extends ProductCategory {

    private String qbCategoryName;
    private String incomeAccount;
    private String inventoryAccount;
    private String cogsAccount;

    public String getQbCategoryName() {
        return qbCategoryName;
    }

    public void setQbCategoryName(String qbCategoryName) {
        this.qbCategoryName = qbCategoryName;
    }

    public String getIncomeAccount() {
        return incomeAccount;
    }

    public void setIncomeAccount(String incomeAccount) {
        this.incomeAccount = incomeAccount;
    }

    public String getInventoryAccount() {
        return inventoryAccount;
    }

    public void setInventoryAccount(String inventoryAccount) {
        this.inventoryAccount = inventoryAccount;
    }

    public String getCogsAccount() {
        return cogsAccount;
    }

    public void setCogsAccount(String cogsAccount) {
        this.cogsAccount = cogsAccount;
    }
}
