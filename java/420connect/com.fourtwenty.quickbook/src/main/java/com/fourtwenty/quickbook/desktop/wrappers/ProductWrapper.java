package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.product.Product;

import java.math.BigDecimal;

public class ProductWrapper extends Product {

    private String qbProductName;
    private double currentQuantity;
    private String vendorQbRef;
    private String incomeAccount;
    private String inventoryAccount;
    private String cogsAccount;
    private String vendorQbListId;
    private BigDecimal cogs;

    public String getQbProductName() {
        return qbProductName;
    }

    public void setQbProductName(String qbProductName) {
        this.qbProductName = qbProductName;
    }

    public double getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(double currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public String getVendorQbRef() {
        return vendorQbRef;
    }

    public void setVendorQbRef(String vendorQbRef) {
        this.vendorQbRef = vendorQbRef;
    }

    public String getIncomeAccount() {
        return incomeAccount;
    }

    public void setIncomeAccount(String incomeAccount) {
        this.incomeAccount = incomeAccount;
    }

    public String getInventoryAccount() {
        return inventoryAccount;
    }

    public void setInventoryAccount(String inventoryAccount) {
        this.inventoryAccount = inventoryAccount;
    }

    public String getCogsAccount() {
        return cogsAccount;
    }

    public void setCogsAccount(String cogsAccount) {
        this.cogsAccount = cogsAccount;
    }

    public String getVendorQbListId() {
        return vendorQbListId;
    }

    public void setVendorQbListId(String vendorQbListId) {
        this.vendorQbListId = vendorQbListId;
    }

    public BigDecimal getCogs() {
        return cogs;
    }

    public void setCogs(BigDecimal cogs) {
        this.cogs = cogs;
    }
}
