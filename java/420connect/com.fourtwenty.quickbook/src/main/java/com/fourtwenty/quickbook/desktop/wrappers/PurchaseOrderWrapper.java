package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;

import java.util.HashMap;

public class PurchaseOrderWrapper extends PurchaseOrder {

    private String vendorName;
    private String vendorQbRef;
    private String vendorQbListId;
    private HashMap<String, Product> productMap = new HashMap<>();

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorQbRef() {
        return vendorQbRef;
    }

    public void setVendorQbRef(String vendorQbRef) {
        this.vendorQbRef = vendorQbRef;
    }

    public HashMap<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(HashMap<String, Product> productMap) {
        this.productMap = productMap;
    }

    public String getVendorQbListId() {
        return vendorQbListId;
    }

    public void setVendorQbListId(String vendorQbListId) {
        this.vendorQbListId = vendorQbListId;
    }
}
