package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.util.HashMap;

public class TransactionWrapper extends Transaction {

    private String memberName;
    private String memberQbRef;
    private String memberListId;
    private HashMap<String, Product> productMap = new HashMap<>();

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberQbRef() {
        return memberQbRef;
    }

    public void setMemberQbRef(String memberQbRef) {
        this.memberQbRef = memberQbRef;
    }

    public HashMap<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(HashMap<String, Product> productMap) {
        this.productMap = productMap;
    }

    public String getMemberListId() {
        return memberListId;
    }

    public void setMemberListId(String memberListId) {
        this.memberListId = memberListId;
    }
}
