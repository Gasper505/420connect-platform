package com.fourtwenty.quickbook.desktop.wrappers;

import com.fourtwenty.core.domain.models.product.Vendor;

public class VendorWrapper extends Vendor {
    private String qbVendorName;
    private String vendorQbRef;
    private String listId;
    private String qbEditSequence;

    public String getQbVendorName() {
        return qbVendorName;
    }

    public void setQbVendorName(String qbVendorName) {
        this.qbVendorName = qbVendorName;
    }

    public String getVendorQbRef() {
        return vendorQbRef;
    }

    public void setVendorQbRef(String vendorQbRef) {
        this.vendorQbRef = vendorQbRef;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getQbEditSequence() {
        return qbEditSequence;
    }

    public void setQbEditSequence(String qbEditSequence) {
        this.qbEditSequence = qbEditSequence;
    }
}
