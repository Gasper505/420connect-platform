package com.fourtwenty.quickbook.lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookDefaultData;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QBDefaultDataLoader implements AppStartup {
    private static final Logger LOGGER = LoggerFactory.getLogger(QBDefaultDataLoader.class);

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private QbDesktopOperationRepository qbDesktopOperationRepository;

    @Override
    public void run() {
        List<Shop> shops = shopRepository.listNonDeleted();

        InputStream inputStream = QBDefaultDataLoader.class.getResourceAsStream("/qbdata.json");
        ObjectMapper objectMapper = new ObjectMapper();
        QuickBookDefaultData quickBookDefaultData = new QuickBookDefaultData();
        try {
            quickBookDefaultData = objectMapper.readValue(inputStream, QuickBookDefaultData.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        for (Shop shop : shops) {
            List<QBDesktopOperation> dbQbDesktopOperations = qbDesktopOperationRepository.getQuickBookDesktopOperations(shop.getCompanyId(), shop.getId());

            if (dbQbDesktopOperations.size() > 0) {
                Map<String, QBDesktopOperation> operationMap = new HashMap<>();
                for (QBDesktopOperation dbQbDesktopOperation : dbQbDesktopOperations) {
                    operationMap.put(dbQbDesktopOperation.getOperationType().toString(), dbQbDesktopOperation);
                }

                List<QBDesktopOperation> defaultQbDesktopOperations = quickBookDefaultData.getQbDesktopOperations();
                if (defaultQbDesktopOperations != null && defaultQbDesktopOperations.size() > 0) {
                    for (QBDesktopOperation defaultQbDesktopOperation : defaultQbDesktopOperations) {
                        boolean status = false;
                        QBDesktopOperation qbDesktopOperation = operationMap.get(defaultQbDesktopOperation.getOperationType().toString());
                        if (qbDesktopOperation != null) {
                            if (qbDesktopOperation.getAppTarget() == null) {
                                qbDesktopOperation.setAppTarget(defaultQbDesktopOperation.getAppTarget());
                                status = true;
                            } else {
                                if (qbDesktopOperation.getAppTarget().size() == 0) {
                                    qbDesktopOperation.setAppTarget(defaultQbDesktopOperation.getAppTarget());
                                    status = true;
                                }
                            }
                            if (qbDesktopOperation.getMethodology() == null) {
                                qbDesktopOperation.setMethodology(defaultQbDesktopOperation.getMethodology());
                                status = true;
                            }

                            Map<String, QBDesktopField> defaultQbDesktopFieldMap = defaultQbDesktopOperation.getQbDesktopFieldMap();
                            Map<String, QBDesktopField> dbQbDesktopFieldMap = qbDesktopOperation.getQbDesktopFieldMap();

                            for (String fieldKey : defaultQbDesktopFieldMap.keySet()) {
                                if (!dbQbDesktopFieldMap.containsKey(fieldKey)) {
                                    dbQbDesktopFieldMap.put(defaultQbDesktopFieldMap.get(fieldKey).getName(), defaultQbDesktopFieldMap.get(fieldKey));
                                    status = true;
                                }
                            }
                            if (status) {
                                qbDesktopOperation.setQbDesktopFieldMap(dbQbDesktopFieldMap);
                                qbDesktopOperationRepository.update(qbDesktopOperation.getCompanyId(), qbDesktopOperation.getId(), qbDesktopOperation);
                            }
                        } else  {
                            QBDesktopOperation operation = new QBDesktopOperation();
                            operation.prepare(shop.getCompanyId());
                            operation.setShopId(shop.getId());
                            operation.setEnabled(defaultQbDesktopOperation.isEnabled());
                            operation.setOperationType(defaultQbDesktopOperation.getOperationType());
                            operation.setQbDesktopFieldMap(defaultQbDesktopOperation.getQbDesktopFieldMap());
                            operation.setAppTarget(defaultQbDesktopOperation.getAppTarget());
                            operation.setMethodology(defaultQbDesktopOperation.getMethodology());

                            qbDesktopOperationRepository.save(operation);
                        }
                    }
                }
            }
        }
    }
}
