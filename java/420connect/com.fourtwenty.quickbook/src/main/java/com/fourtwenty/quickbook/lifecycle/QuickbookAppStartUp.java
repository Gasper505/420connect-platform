package com.fourtwenty.quickbook.lifecycle;

import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.quickbook.desktop.soap.QBSyncService;
import com.fourtwenty.quickbook.repositories.*;
import com.fourtwenty.quickbook.service.QuickBooksSessionService;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class QuickbookAppStartUp implements AppStartup {

    private static final Logger LOG = LoggerFactory.getLogger(QuickbookAppStartUp.class);

    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookDesktopAccountRepository quickbookDesktopAccountRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    QbAccountRepository qbAccountRepository;
    @Inject
    QbDesktopOperationRepository qbDesktopOperationRepository;
    @Inject
    ErrorLogsRepository errorLogsRepository;
    @Inject
    QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    @Inject
    QBUniqueSequenceRepository qbUniqueSequenceRepository;
    @Inject
    BatchQuantityRepository batchQuantityRepository;
    @Inject
    ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private ShipmentBillRepository shipmentBillRepository;
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;
    @Inject
    private QuickBooksSessionService quickBooksSessionService;
    @Inject
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    @com.google.inject.Inject
    private BlazeEventBus blazeEventBus;

    @Override
    public void run() {
        try {

            QBSyncService.getInstance().quickbookCustomEntitiesRepository = quickbookCustomEntitiesRepository;
            QBSyncService.getInstance().quickbookDesktopAccountRepository = quickbookDesktopAccountRepository;
            QBSyncService.getInstance().thirdPartyAccountRepository = thirdPartyAccountRepository;
            QBSyncService.getInstance().quickbookSyncDetailsRepository = quickbookSyncDetailsRepository;
            QBSyncService.getInstance().productRepository = productRepository;
            QBSyncService.getInstance().memberRepository = memberRepository;
            QBSyncService.getInstance().vendorRepository = vendorRepository;
            QBSyncService.getInstance().transactionRepository = transactionRepository;
            QBSyncService.getInstance().purchaseOrderRepository = purchaseOrderRepository;
            QBSyncService.getInstance().productCategoryRepository = productCategoryRepository;
            QBSyncService.getInstance().batchRepository = batchRepository;
            QBSyncService.getInstance().prepackageProductItemRepository = prepackageProductItemRepository;
            QBSyncService.getInstance().prepackageRepository = prepackageRepository;
            QBSyncService.getInstance().weightToleranceRepository = weightToleranceRepository;
            QBSyncService.getInstance().shopRepository = shopRepository;
            QBSyncService.getInstance().qbAccountRepository = qbAccountRepository;
            QBSyncService.getInstance().qbDesktopOperationRepository = qbDesktopOperationRepository;
            QBSyncService.getInstance().errorLogsRepository = errorLogsRepository;
            QBSyncService.getInstance().qbDesktopCurrentSyncRepository = qbDesktopCurrentSyncRepository;
            QBSyncService.getInstance().qbUniqueSequenceRepository = qbUniqueSequenceRepository;
            QBSyncService.getInstance().batchQuantityRepository = batchQuantityRepository;
            QBSyncService.getInstance().productPrepackageQuantityRepository = productPrepackageQuantityRepository;
            QBSyncService.getInstance().inventoryActionRepository = inventoryActionRepository;
            QBSyncService.getInstance().productPrepackageQuantityRepository =productPrepackageQuantityRepository;
            QBSyncService.getInstance().shipmentBillRepository = shipmentBillRepository;
            QBSyncService.getInstance().invoiceRepository = invoiceRepository;
            QBSyncService.getInstance().inventoryRepository = inventoryRepository;
            QBSyncService.getInstance().invoicePaymentsRepository = invoicePaymentsRepository;
            QBSyncService.getInstance().quickBookSessionService = quickBooksSessionService;
            QBSyncService.getInstance().quickBookAccountDetailsRepository = quickBookAccountDetailsRepository;

        } catch (Exception e) {

        }
    }
}
