package com.fourtwenty.quickbook.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QWCPassword {
    private String qwcPassword;

    public String getQwcPassword() {
        return qwcPassword;
    }

    public void setQwcPassword(String qwcPassword) {
        this.qwcPassword = qwcPassword;
    }
}
