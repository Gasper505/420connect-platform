package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.AccountInfo;
import com.fourtwenty.quickbook.repositories.QuickbookAccountRepository;
import com.google.inject.Inject;
import com.intuit.ipp.data.Account;
import com.intuit.ipp.data.ReferenceType;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.DataService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class AccountHelper {

    @Inject
    QuickbookAccountRepository quickbookAccountRepository;


    public static ReferenceType getAccountRef(Account account) {
        ReferenceType accountRef = new ReferenceType();
        accountRef.setName(account.getName());
        accountRef.setValue(account.getId());
        return accountRef;
    }

    public static Account taxReducerItem(DataService service, String taxReducer) throws FMSException {
        List<Account> accounts = (List<Account>) service.findAll(new Account());
        if (!accounts.isEmpty()) {
            Iterator<Account> itr = accounts.iterator();
            while (itr.hasNext()) {
                Account account = itr.next();
                if (account.getName().equals(taxReducer)) {

                    return account;
                }
            }
        }
        return null;
    }


    public List<AccountInfo> getAccountList(DataService service) {
        List<AccountInfo> accountInfoList = null;
        try {
            List<Account> accounts = service.findAll(new Account());
            accountInfoList = new ArrayList<AccountInfo>();
            for (Account account : accounts) {
                AccountInfo accountInfo = new AccountInfo();
                accountInfo.setAccountType(account.getAccountType().toString());
                accountInfo.setAccountSubType(account.getAccountSubType());
                accountInfo.setAcctNum(account.getAcctNum());
                accountInfo.setBankNum(account.getBankNum());
                accountInfo.setFullyQualifiedName(account.getFullyQualifiedName());
                accountInfo.setName(account.getName());
                accountInfoList.add(accountInfo);
            }
        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling get AccountList:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::" + error.getCode());

            }

        }
        return accountInfoList;
    }


    public static Account getAccount(DataService service, String name) throws FMSException {
        List<Account> accounts = (List<Account>) service.findAll(new Account());
        if (!accounts.isEmpty()) {
            Iterator<Account> itr = accounts.iterator();
            while (itr.hasNext()) {
                Account account = itr.next();
                if (account.getName().equals(name)) {
                    return account;
                }
            }
        }
        return null;

    }


}