package com.fourtwenty.quickbook.online.helperservices;

import com.esotericsoftware.minlog.Log;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.util.DateUtils;
import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class BillHelper {
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    ShipmentBillRepository shipmentBillRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;

    public void syncBills(DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {
        long currentTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        Iterable<ShipmentBill> shipmentBillList = null;
        List<ShipmentBill> blazeshipmentBillList = null;

        Iterable<QuickbookSyncDetails> syncBillsList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId, shopId
                , QuickbookSyncDetails.QuickbookEntityType.Bill, QBConstants.QUICKBOOK_ONLINE);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        LinkedList<QuickbookSyncDetails> syncBills = Lists.newLinkedList(syncBillsList);

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncBills.isEmpty()) {
            shipmentBillList = shipmentBillRepository.listAllByShop(blazeCompanyId, shopId);
            blazeshipmentBillList = Lists.newArrayList(shipmentBillList);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncBills.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, currentTime, endTime,
                    shopId, QuickbookSyncDetails.QuickbookEntityType.Bill, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

        } else {
            QuickbookSyncDetails quickbookSyncBills = syncBills.getLast();
            LOG.info("Last sync Date:::::" + quickbookSyncBills.getEndTime());
            endTime = quickbookSyncBills.getEndTime();
            long startDate = endTime - 864000000l;

            List<ShipmentBill> billListwitoutQbRef = shipmentBillRepository.getBillsListWithoutQbRef(blazeCompanyId, shopId, startDate, endTime);
            shipmentBillList = shipmentBillRepository.listByShopWithDates(blazeCompanyId, shopId, endTime, currentTime);

            blazeshipmentBillList = Lists.newArrayList(shipmentBillList);
            blazeshipmentBillList.addAll(billListwitoutQbRef);
        }

        try {
            if (blazeshipmentBillList != null) {
                LOG.info("Blaze Bill Count: " + blazeshipmentBillList.size());
                Iterator<ShipmentBill> shipmentBillIterator = blazeshipmentBillList.iterator();
                syncBillBatch(service, shipmentBillIterator, blazeCompanyId, shopId, quickbookCompanyId, endTime, blazeshipmentBillList.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Bills: ");
        }

    }


    //Method used to convert blaze product  to QB object
    public List<Bill> convertBlazeBillintoQB(Iterator<ShipmentBill> blazeBillList, DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {

        List<Bill> billDetails = new ArrayList<Bill>();
        QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        try {
            if (accountType != null) {
                //Payment account
                LOG.info("Bill Income Account Type:-" + accountType.getSales());
                Account payableAccount = accountHelper.getAccount(service, accountType.getPayableAccount());
                int counter = 0;

                while ((counter < 30) && (blazeBillList.hasNext())) {
                    ShipmentBill blazeBill = blazeBillList.next();

                    try {

                        Bill bill = new Bill();
                        com.fourtwenty.core.domain.models.product.Vendor vendorDetails = vendorRepository.get(blazeCompanyId, blazeBill.getVendorId());
                        LOG.info("vendorDetails: " + vendorDetails + " : " + " Bill vendor: " + blazeBill.getVendorId() + " : " + "CompanyId: " + blazeBill.getCompanyId());
                        if (vendorDetails != null) {
                            LOG.info("Vendor Id Associted with: " + blazeBill.getVendorId());
                            ReferenceType vendorRef = vendorRef(blazeBill, vendorDetails, quickbookCompanyId, blazeCompanyId);
                            bill.setVendorRef(vendorRef);
                            bill.setAPAccountRef(AccountHelper.getAccountRef(payableAccount));

                            List<Line> lines = qbLineList(blazeBill, blazeCompanyId, shopId, service);
                            bill.setLine(lines);

                            Long nowMillis = blazeBill.getCreated();
                            DateTime jodatime = new DateTime(nowMillis);

                            bill.setTxnDate(jodatime.toDate());
                            bill.setTotalAmt(blazeBill.getGrandTotal());
                            bill.setDueDate(DateUtils.getDateWithNextDays(45));
                            bill.setPrivateNote(blazeBill.getId());
                            bill.setDocNumber(blazeBill.getShipmentBillNumber());
                            billDetails.add(bill);
                            counter++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.info("Error While Creating Bill :");
                    }
                }
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create Bill: " + error.getMessage() + "Detail::: " + error.getDetail() + "Status Code::::::" + error.getCode());
                //Get new Access token if Current expires in running mode
                if (error.getCode().equals(QBConstants.ERRORCODE)) {
                    LOG.info("Inside 3200 code");
                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Bills: ");
        }
        return billDetails;
    }

    // Sync Bill Batch of 30 in QB
    public void syncBillBatch(DataService service, Iterator<ShipmentBill> blazeBillList, String
            blazeCompanyId, String shopId, String quickbookCompanyId, long endTime, int total) {
        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int totalSuccess = 0;
        int counter = 0;
        for (int itr = 0; blazeBillList.hasNext(); itr++) {
            List<Bill> qbBill = convertBlazeBillintoQB(blazeBillList, service, blazeCompanyId, shopId, quickbookCompanyId);
            LOG.info(" Bill Size:   " + qbBill.size());
            batchOperation = new BatchOperation();
            try {
                for (Bill bill : qbBill) {
                    try {
                        counter++;
                        ShipmentBill blazeBill = shipmentBillRepository.get(blazeCompanyId, bill.getPrivateNote());
                        LOG.info("blazeBill Details:    " + blazeBill);
                        String billRef = blazeBill.getQbBillRef();
                        LOG.info("Bill Ref: " + billRef);
                        if (billRef == null) {
                            batchOperation.addEntity(bill, OperationEnum.CREATE, "batchId" + counter);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.info("Error while creating Bills:");
                    }
                }

                service.executeBatch(batchOperation);
                Thread.sleep(1000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    try {
                        if (batchOperation.isFault(bId)) {
                            Fault fault = batchOperation.getFault(bId);
                            // fault has a list of errors
                            for (com.intuit.ipp.data.Error error : fault.getError()) {
                                LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());

                            }

                        }


                        Bill singleBill = (Bill) batchOperation.getEntity(bId);
                        LOG.info("Result Bill: " + singleBill);
                        String billId = singleBill.getPrivateNote();
                        ShipmentBill shipmentBill = shipmentBillRepository.get(blazeCompanyId, billId);
                        LOG.info("Bill details By Id" + shipmentBill);
                        if (singleBill != null) {
                            if (shipmentBill != null)
                                totalSuccess++;
                            {
                                LOG.info("Bill Ref saved--" + singleBill.getId());
                                //Update Quickbook Ref into vendors table in Blaze
                                BasicDBObject updateQuery = new BasicDBObject();
                                updateQuery.append("$set", new BasicDBObject().append("qbBillRef", singleBill.getId()));
                                BasicDBObject searchQuery = new BasicDBObject();
                                searchQuery.append("_id", new ObjectId(shipmentBill.getId()));
                                shipmentBillRepository.updateBillRef(searchQuery, updateQuery);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling Create Bill: " + error.getMessage() + "Detail::: " + error.getDetail() + "Status Code::::::" + error.getCode());
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId()
                        );
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while calling Bulk Bill Create: ");
            }

        }


        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.Bill, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }

    public ReferenceType vendorRef(ShipmentBill bill, com.fourtwenty.core.domain.models.product.Vendor vendorDetails, String quickbookCompanyId, String blazeCompanyId) {
        ReferenceType vendorRef = new ReferenceType();
        if (bill != null) {
            com.fourtwenty.core.domain.models.product.Vendor vendorDetail = vendorRepository.get(blazeCompanyId, bill.getVendorId());
            if (vendorDetail.getQbVendorRef() != null) {
                LOG.info("vendor Reference :" + vendorDetail.getQbVendorRef());
                List<HashMap<String, String>> getQbrefMaps = vendorDetail.getQbVendorRef();
                for (HashMap<String, String> getQbrefmap : getQbrefMaps) {
                    LOG.info("Check Ref In DB" + getQbrefmap.get(quickbookCompanyId));
                    if (getQbrefmap.get(quickbookCompanyId) != null) {
                        vendorRef.setValue(getQbrefmap.get(quickbookCompanyId));
                    }
                    vendorRef.setName(vendorDetails.getName());
                }
            }
        }
        return vendorRef;
    }


    public List<Line> qbLineList(ShipmentBill blazeBill, String blazecompanyId, String shopId, DataService service) throws FMSException {
        List<Line> lineList = new ArrayList<Line>();
        double exciseTax = 0.0;
        for (POProductRequest product : blazeBill.getPoProductRequest()) {
            Product blazeProduct = productRepository.get(product.getCompanyId(), product.getProductId());
            //Line item
            Line lineObject = new Line();
            lineObject.setAmount(product.getTotalCost());
            lineObject.setDescription(blazeProduct.getDescription());
            lineObject.setDetailType(LineDetailTypeEnum.ITEM_BASED_EXPENSE_LINE_DETAIL);
            ItemBasedExpenseLineDetail lineItemDetails = new ItemBasedExpenseLineDetail();

            //Customer ref
            ReferenceType customerRef = new ReferenceType();
            lineItemDetails.setCustomerRef(customerRef);
            LOG.info(" Quickbook ItemRef : " + blazeProduct.getQbItemRef());
            LOG.info("Blaze ProductId :" + blazeProduct.getId());

            ReferenceType itemref = new ReferenceType();
            if (blazeProduct.getQbItemRef() != null) {
                itemref.setValue(blazeProduct.getQbItemRef());
                itemref.setName(blazeProduct.getName());
                lineItemDetails.setItemRef(itemref);
            }

            ReferenceType taxcode = new ReferenceType();
            taxcode.setValue(QBConstants.TAX);

            lineItemDetails.setTaxCodeRef(taxcode);
            lineItemDetails.setUnitPrice(product.getUnitPrice());
            lineItemDetails.setQty(product.getReceivedQuantity());
            lineItemDetails.setBillableStatus(BillableStatusEnum.NOT_BILLABLE);

            lineObject.setItemBasedExpenseLineDetail(lineItemDetails);
            exciseTax += product.getTotalExciseTax().doubleValue();
            lineList.add(lineObject);

        }
        //Add Excise Tax as a line item
        ItemBasedExpenseLineDetail exciseTaxItemDetails = new ItemBasedExpenseLineDetail();
        ReferenceType taxItemRef = new ReferenceType();

        QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, QBConstants.TOTAL_EXCISE_TAX);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
        LOG.info("quickbookenity:" + quickbookEntity);
        if (quickbookEntity == null) {
            //create Item
            Item taxItem = ItemHelper.createExciseTaxItem(service, entities);
            if (taxItem.getId() != null) {
                quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, taxItem.getId(), QBConstants.TOTAL_EXCISE_TAX);
                LOG.info("TaxItemId: " + taxItem.getId());
                taxItemRef.setName(taxItem.getName());
                taxItemRef.setValue(taxItem.getId());
                exciseTaxItemDetails.setItemRef(taxItemRef);
            }

        } else {
            if (quickbookEntity.getProductRef() != null) {
                taxItemRef.setName(quickbookEntity.getProductName());
                taxItemRef.setValue(quickbookEntity.getProductRef());
                exciseTaxItemDetails.setItemRef(taxItemRef);
            }

        }

        Line totalExciseLine = new Line();
        totalExciseLine.setAmount(new BigDecimal(exciseTax));
        totalExciseLine.setDetailType(LineDetailTypeEnum.ITEM_BASED_EXPENSE_LINE_DETAIL);
        exciseTaxItemDetails.setUnitPrice(new BigDecimal(exciseTax));
        exciseTaxItemDetails.setQty(QBConstants.UNIT_PRICE);
        exciseTaxItemDetails.setBillableStatus(BillableStatusEnum.NOT_BILLABLE);
        totalExciseLine.setItemBasedExpenseLineDetail(exciseTaxItemDetails);
        lineList.add(totalExciseLine);

        return lineList;
    }


}
