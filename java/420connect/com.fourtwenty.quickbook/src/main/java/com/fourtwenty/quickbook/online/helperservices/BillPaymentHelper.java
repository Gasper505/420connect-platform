package com.fourtwenty.quickbook.online.helperservices;

import com.esotericsoftware.minlog.Log;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import org.apache.commons.lang.RandomStringUtils;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class BillPaymentHelper {
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    ShipmentBillRepository shipmentBillRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductRepository productRepository;

    public void syncBillPayments(DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {
        long currentTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        Iterable<ShipmentBill> shipmentBillList = null;
        List<ShipmentBill> blazeshipmentBillList = null;

        Iterable<QuickbookSyncDetails> syncBillsList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId, shopId,
                QuickbookSyncDetails.QuickbookEntityType.BillPayment, QBConstants.QUICKBOOK_ONLINE);
        LinkedList<QuickbookSyncDetails> syncBills = Lists.newLinkedList(syncBillsList);

        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncBills.isEmpty()) {
            shipmentBillList = shipmentBillRepository.listAllByShop(blazeCompanyId, shopId);
            blazeshipmentBillList = Lists.newArrayList(shipmentBillList);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncBills.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, currentTime, endTime,
                    shopId, QuickbookSyncDetails.QuickbookEntityType.BillPayment, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);
        } else {
            QuickbookSyncDetails quickbookSyncBills = syncBills.getLast();
            LOG.info("Last sync Date:::::" + quickbookSyncBills.getEndTime());
            endTime = quickbookSyncBills.getEndTime();
            long startDate = endTime - 864000000l;

            List<ShipmentBill> billListwitoutQbRef = shipmentBillRepository.getBillsListWithoutQbRef(blazeCompanyId, shopId, startDate, endTime);
            shipmentBillList = shipmentBillRepository.listByShopWithDates(blazeCompanyId, shopId, endTime, currentTime);

            blazeshipmentBillList = Lists.newArrayList(shipmentBillList);
            blazeshipmentBillList.addAll(billListwitoutQbRef);
        }

        try {
            if (blazeshipmentBillList != null) {
                LOG.info("Blaze Bill payment Count: " + blazeshipmentBillList.size());
                Iterator<ShipmentBill> shipmentBillIterator = blazeshipmentBillList.iterator();
                syncBillBatch(service, shipmentBillIterator, blazeCompanyId, shopId, quickbookCompanyId, endTime, blazeshipmentBillList.size());
            }


        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Bills: ");
        }

    }


    //Method used to convert blaze product  to QB object
    public List<BillPayment> convertBlazeBillintoQB(Iterator<ShipmentBill> blazeBillList, DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {

        List<BillPayment> billPaymentDetails = new ArrayList<BillPayment>();
        QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        try {
            if (accountType != null) {
                int counter = 0;
                while ((counter < 30) && (blazeBillList.hasNext())) {
                    ShipmentBill blazeBill = blazeBillList.next();
                    if (blazeBill.getPaymentHistory() != null) {
                        try {
                            BillPayment billPayment = new BillPayment();
                            Long nowMillis = blazeBill.getReceivedDate();
                            DateTime jodatime = new DateTime(nowMillis);
                            billPayment.setTxnDate(jodatime.toDate());
                            billPayment.setPrivateNote(blazeBill.getId());
                            com.fourtwenty.core.domain.models.product.Vendor vendorDetails = vendorRepository.get(blazeCompanyId, blazeBill.getVendorId());
                            LOG.info("vendorDetails: " + vendorDetails + " : " + " Bill vendor: " + blazeBill.getVendorId() + " : " + "CompanyId: " + blazeBill.getCompanyId());
                            if (vendorDetails != null) {
                                LOG.info("Vendor Id Associted with: " + blazeBill.getVendorId());
                                System.out.println("comapnyId: " + quickbookCompanyId + ":" + blazeCompanyId);
                                ReferenceType vendorRef = vendorRef(blazeBill, vendorDetails, quickbookCompanyId, blazeCompanyId);
                                billPayment.setVendorRef(vendorRef);
                                List<Line> lines = qbLineList(blazeBill, blazeCompanyId, shopId, service);
                                billPayment.setLine(lines);
                                BillPaymentCheck billPaymentCheck = new BillPaymentCheck();
                                Account bankAccount = AccountHelper.getAccount(service, accountType.getChecking());
                                billPaymentCheck.setBankAccountRef(AccountHelper.getAccountRef(bankAccount));
                                billPaymentCheck.setCheckDetail(getCheckPayment());
                                billPaymentCheck.setPrintStatus(PrintStatusEnum.NEED_TO_PRINT);
                                billPayment.setCheckPayment(billPaymentCheck);
                                billPayment.setPayType(BillPaymentTypeEnum.CHECK);
                                billPayment.setTotalAmt(blazeBill.getAmountPaid());
                                billPayment.setDocNumber(blazeBill.getShipmentBillNumber());
                                billPaymentDetails.add(billPayment);
                                counter++;
                            }
                        } catch (FMSException e) {
                            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                                LOG.info("Error while calling Create Bill payment: " + error.getMessage() + "Detail::: " + error.getDetail() + "Status Code::::::" + error.getCode());
                                if (error.getCode().equals(QBConstants.ERRORCODE)) {
                                    LOG.info("Inside 3200 code");
                                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.info("Error while Creating bill Payment: ");
                        }

                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Bill Payments: ");
        }
        return billPaymentDetails;
    }

    // Sync Bill Batch of 30 in QB
    public void syncBillBatch(DataService service, Iterator<ShipmentBill> blazeBillList, String
            blazeCompanyId, String shopId, String quickbookCompanyId, long endTime, int total) throws FMSException {
        int totalSuccess = 0;
        int counter = 0;
        long startTime = DateTime.now().getMillis();
        for (int itr = 0; blazeBillList.hasNext(); itr++) {
            BatchOperation batchOperation = null;

            List<BillPayment> qbBillPayment = convertBlazeBillintoQB(blazeBillList, service, blazeCompanyId, shopId, quickbookCompanyId);

            LOG.info(" Bill Size:   " + qbBillPayment.size());
            batchOperation = new BatchOperation();
            try {
                for (BillPayment billPayment : qbBillPayment) {
                    try {
                        counter++;
                        ShipmentBill blazeBillPayment = shipmentBillRepository.get(blazeCompanyId, billPayment.getPrivateNote());
                        LOG.info("blazeBill Details:    " + blazeBillPayment);

                        batchOperation.addEntity(billPayment, OperationEnum.CREATE, "batchId" + counter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                service.executeBatch(batchOperation);
                Thread.sleep(1000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    try {
                        if (batchOperation.isFault(bId)) {
                            Fault fault = batchOperation.getFault(bId);
                            // fault has a list of errors
                            for (com.intuit.ipp.data.Error error : fault.getError()) {
                                LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());

                            }

                        }


                        BillPayment singleBill = (BillPayment) batchOperation.getEntity(bId);
                        if (singleBill != null) {
                            totalSuccess++;
                            LOG.info("Result Bill payment: " + singleBill);
                            String billId = singleBill.getPrivateNote();
                            ShipmentBill shipmentBill = shipmentBillRepository.get(blazeCompanyId, billId);
                            LOG.info("Bill payment details By Id" + shipmentBill);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while calling Bulk Bill payment Create: ");
            }


            int totalFail = total - totalSuccess;
            quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.BillPayment, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

        }

    }


    public ReferenceType vendorRef(ShipmentBill bill, com.fourtwenty.core.domain.models.product.Vendor
            vendorDetails, String quickbookCompanyId, String blazeCompanyId) {
        ReferenceType vendorRef = new ReferenceType();
        com.fourtwenty.core.domain.models.product.Vendor vendorDetail = vendorRepository.get(blazeCompanyId, bill.getVendorId());
        if (vendorDetail.getQbVendorRef() != null) {
            LOG.info("vendor Reference :" + vendorDetail.getQbVendorRef());
            List<HashMap<String, String>> getQbrefMaps = vendorDetail.getQbVendorRef();
            for (HashMap<String, String> getQbrefmap : getQbrefMaps) {
                LOG.info("Check Ref In DB" + getQbrefmap.get(quickbookCompanyId));
                if (getQbrefmap.get(quickbookCompanyId) != null) {
                    vendorRef.setValue(getQbrefmap.get(quickbookCompanyId));
                }
                vendorRef.setName(vendorDetails.getName());
            }
        }
        return vendorRef;
    }


    public List<Line> qbLineList(ShipmentBill blazeBill, String blazecompanyId, String shopId, DataService service) throws
            FMSException {
        List<Line> lineList = new ArrayList<Line>();
        if (blazeBill != null) {
            double amount = 0.0;
            for (ShipmentBillPayment shipmentBillPayment : blazeBill.getPaymentHistory()) {
                amount += shipmentBillPayment.getAmountPaid().doubleValue();
                LOG.info("Payment Type:" + shipmentBillPayment.getPaymentType());
                LOG.info("Payment amount:" + amount);
            }
            Line lineObj = new Line();
            lineObj.setAmount(new BigDecimal(amount));
            List<LinkedTxn> linkedTxnList1 = new ArrayList<LinkedTxn>();
            LinkedTxn linkedTxn1 = new LinkedTxn();
            linkedTxn1.setTxnId(blazeBill.getQbBillRef());
            linkedTxn1.setTxnType(TxnTypeEnum.BILL.value());
            linkedTxnList1.add(linkedTxn1);
            lineObj.setLinkedTxn(linkedTxnList1);
            lineList.add(lineObj);

        }
        return lineList;
    }


    public static CheckPayment getCheckPayment() throws FMSException {
        String uuid = RandomStringUtils.randomAlphanumeric(8);
        CheckPayment checkPayment = new CheckPayment();
        checkPayment.setAcctNum("AccNum" + uuid);
        checkPayment.setBankName("BankName" + uuid);
        checkPayment.setCheckNum("CheckNum" + uuid);
        checkPayment.setNameOnAcct("Name" + uuid);
        checkPayment.setStatus("Status" + uuid);
        return checkPayment;
    }

}
