package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.util.DateUtils;
import com.mongodb.BasicDBObject;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

/**
 * @author dderose
 */
public class CustomerHelper {
    @Inject
    MemberRepository memberRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSync quickbookSync;


    public void syncCustomers(DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {
        int total = 0;
        long currentTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        Iterable<Member> getMemberDetailsList = null;
        List<Member> blazeCustomersList = null;

        Iterable<QuickbookSyncDetails> syncCustomerListDetails = quickbookSyncDetailsRepository.findByEntityType
                (blazeCompanyId, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncCustomerList = Lists.newLinkedList(syncCustomerListDetails);

        if (syncCustomerList.isEmpty()) {
            getMemberDetailsList = memberRepository.listActiveAndPendingMembers(blazeCompanyId);
            blazeCustomersList = Lists.newArrayList(getMemberDetailsList);
        } else {
            QuickbookSyncDetails quickbookSyncCustomers = syncCustomerList.getLast();
            endTime = quickbookSyncCustomers.getEndTime();
            long startDate = endTime - 864000000l;

            List<Member> memberListwithoutQbRef = memberRepository.getMembersListWithoutQbRef(blazeCompanyId, startDate, endTime);
            getMemberDetailsList = memberRepository.listWithDate(blazeCompanyId, endTime, currentTime);

            blazeCustomersList = Lists.newArrayList(getMemberDetailsList);
            blazeCustomersList.addAll(memberListwithoutQbRef);
        }


        try {
            if (blazeCustomersList != null) {
                total = blazeCustomersList.size();
                LOG.info("Total member count: " + total);
                Iterator<Member> memberIterator = blazeCustomersList.iterator();
                syncCustomerBatch(service, memberIterator, blazeCompanyId, quickbookCompanyId, shopId, currentTime, endTime, blazeCustomersList.size());
            }

        } catch (Exception e) {
            LOG.info("Error while creating customers in Quickbook:");
            e.printStackTrace();
        }

    }


    public void syncCustomerBatch(DataService service, Iterator<Member> blazeCustomerList, String blazeCompanyId, String quickbookCompanyId, String shopId, long startTime, long endTime, int total) {
        BatchOperation batchOperation = null;
        int counter = 0, totalSuccess = 0;
        int count = 1;
        while (blazeCustomerList.hasNext()) {
            try {
                List<Customer> qbCustomerList = convertBlazeCustomerIntoQB(blazeCustomerList, quickbookCompanyId, count);
                LOG.info("customer Batch Size" + qbCustomerList.size());
                Map<String, Member> customerMap = new HashMap<>();
                batchOperation = new BatchOperation();
                count++;
                for (Customer customer : qbCustomerList) {
                    counter++;

                    try {

                        List<CustomField> customFieldList = customer.getCustomField();
                        String memberId = null;
                        for (CustomField customField : customFieldList) {
                            memberId = customField.getName();
                        }
                        Member memberDetail = memberRepository.get(blazeCompanyId, memberId);

                        if (customer.getAcctNum() == null) {
                            batchOperation.addEntity(customer, OperationEnum.CREATE, "custbid" + counter);
                            customerMap.put("custbid" + counter, memberDetail);

                        } else {
                            totalSuccess++;
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }


                }

                service.executeBatch(batchOperation);
                Thread.sleep(2000);

                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    try {
                        Customer customer = (Customer) batchOperation.getEntity(bId);

                        LOG.info("Member Details After save" + customer);
                        if (customer != null) {
                            Member memberDetails = customerMap.get(bId);

                            if (memberDetails != null) {
                                totalSuccess++;
                                HashMap<String, String> saveQbrefMap = new HashMap<String, String>();
                                saveQbrefMap.put(quickbookCompanyId, customer.getId());
                                LOG.info("saveQbrefMap save In DB:-" + saveQbrefMap);
                                BasicDBObject updateQuery = new BasicDBObject();
                                updateQuery.append("$push", new BasicDBObject().append("qbCustomerRef", saveQbrefMap));
                                BasicDBObject searchQuery = new BasicDBObject();
                                searchQuery.append("_id", new ObjectId(memberDetails.getId()));
                                memberRepository.updateCustomerRef(searchQuery, updateQuery);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            LOG.info("Errror Message" + error.getMessage() + "\nError Detail" + error.getDetail());

                        }

                    }

                }

            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling Create Customer:: " + error.getMessage() + " Details ::" + error.getDetail() + "Status ::" + error.getCode());
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code: ");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while creating customer:");
            }
        }

        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.Customer, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }

    public List<Customer> convertBlazeCustomerIntoQB(Iterator<Member> blazeCustomerList, String quickbookCompanyId, int counts) throws Exception {
        int count = counts * 30 + 1;
        List<Customer> qbCustomerList = new LinkedList<Customer>();
        int counter = 0;
        while ((counter < 30) && (blazeCustomerList.hasNext())) {
            Member member = blazeCustomerList.next();
            Customer qbCustomer = new Customer();
            CustomField customField = new CustomField();
            customField.setName(member.getId());
            customField.setDefinitionId(RandomStringUtils.randomNumeric(3));
            List<CustomField> customFieldList = new ArrayList<CustomField>();
            customFieldList.add(customField);
            qbCustomer.setCustomField(customFieldList);

            String customerRef = null;
            List<HashMap<String, String>> getQbrefMaps = member.getQbCustomerRef();
            for (HashMap<String, String> getQbrefmap : getQbrefMaps) {
                customerRef = getQbrefmap.get(quickbookCompanyId);
            }
            count++;
            qbCustomer.setAcctNum(customerRef);
            String displayName = member.getFirstName() + " " + count;
            if (displayName.length() < 16) {
                qbCustomer.setDisplayName(displayName);
            } else {
                String name = member.getLastName() + " " + org.apache.commons.lang.RandomStringUtils.randomNumeric(4);
                qbCustomer.setDisplayName(name);

            }

            if (member.getFirstName() != null) {
                qbCustomer.setTitle(member.getFirstName());

            }
            if (member.getFirstName() != null) {
                qbCustomer.setGivenName(member.getFirstName());

            }
            if (member.getMiddleName() != null) {
                qbCustomer.setMiddleName(member.getMiddleName());

            }
            if (member.getLastName() != null) {
                qbCustomer.setFamilyName(member.getLastName());

            }
            // Optional Fields
            qbCustomer.setOrganization(false);
            qbCustomer.setActive(true);
            qbCustomer.setTaxable(false);
            TelephoneNumber primaryNum = new TelephoneNumber();
            if (member.getPrimaryPhone() != null) {
                primaryNum.setFreeFormNumber(member.getPrimaryPhone());
                primaryNum.setDefault(true);
                qbCustomer.setPrimaryPhone(primaryNum);
            }

            EmailAddress emailAddr = new EmailAddress();
            if (member.getEmail() != null) {
                emailAddr.setAddress(member.getEmail());
                qbCustomer.setPrimaryEmailAddr(emailAddr);

            }
            if (member.getFirstName() != null) {
                qbCustomer.setContactName(member.getFirstName());
            }
            if (member.getLastName() != null) {
                qbCustomer.setAltContactName(member.getLastName());
            }


            for (Note memberDetails : member.getNotes()) {
                String notes = memberDetails.getMessage();
                qbCustomer.setNotes(notes);
            }
            PhysicalAddress address = new PhysicalAddress();
            if (member.getAddress() != null) {
                if (member.getAddress().getAddress() != null) {
                    address.setLine1(member.getAddress().getAddress());
                }

                if (member.getAddress().getCity() != null) {
                    address.setCity(member.getAddress().getCity());
                }
                if (member.getAddress().getCountry() != null) {
                    address.setCountry(member.getAddress().getCountry());

                }
                if (member.getAddress().getZipCode() != null) {
                    address.setPostalCode(member.getAddress().getZipCode());
                }
                qbCustomer.setBillAddr(address);

                qbCustomer.setShipAddr(address);
                qbCustomer.setTaxExemptionReasonId("1");
            }
            qbCustomer.setOpenBalanceDate(DateUtils.getCurrentDateTime());
            qbCustomerList.add(qbCustomer);
            counter++;


        }


        return qbCustomerList;

    }


}