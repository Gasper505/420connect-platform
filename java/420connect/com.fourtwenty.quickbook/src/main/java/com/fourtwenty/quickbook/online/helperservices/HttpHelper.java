package com.fourtwenty.quickbook.online.helperservices;


import com.fourtwenty.core.config.ConnectConfiguration;
import com.google.inject.Inject;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HttpHelper {
    @Inject
    ConnectConfiguration configuration;


    public StringBuffer getResult(HttpResponse response) throws Exception {
        StringBuffer result = null;
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public HttpPost addHeader(HttpPost post) {
        String base64ClientIdSec = Base64.encodeBase64String((configuration.getQuickBookConfig().getClientID() + ":" + configuration.getQuickBookConfig().getClientSecret()).getBytes());
        post.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        post.setHeader("Authorization", "Basic " + base64ClientIdSec);
        post.setHeader("Accept", "application/json");
        return post;
    }


}
