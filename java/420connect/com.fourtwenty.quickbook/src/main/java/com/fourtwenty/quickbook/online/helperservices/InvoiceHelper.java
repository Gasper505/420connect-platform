package com.fourtwenty.quickbook.online.helperservices;

import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.util.DateUtils;
import org.apache.commons.lang.RandomStringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dderose
 */
public class InvoiceHelper {

    InvoiceHelper() {

    }


    public Invoice getInvoiceFields(DataService service) throws FMSException, ParseException {
        Invoice invoice = new Invoice();

        // Mandatory Fields
        invoice.setDocNumber(RandomStringUtils.randomAlphanumeric(5));

        try {
            invoice.setTxnDate(DateUtils.getCurrentDateTime());
        } catch (ParseException e) {
            throw new FMSException("ParseException while getting current date.");
        }

        ReferenceType customerRef = new ReferenceType();
        customerRef.setName(" Test");
        customerRef.setValue("12");
        invoice.setCustomerRef(customerRef);

        invoice.setPrivateNote("Testing");
        invoice.setTxnStatus("Payable");
        invoice.setBalance(new BigDecimal("10000"));
        PhysicalAddress address = new PhysicalAddress();

        invoice.setBillAddr(address);

        List<Line> invLine = new ArrayList<Line>();
        Line line = new Line();
        line.setDescription("test");
        line.setAmount(new BigDecimal("10000"));
        line.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);

        SalesItemLineDetail silDetails = new SalesItemLineDetail();

        ReferenceType itemRef = new ReferenceType();
        itemRef.setName("test");
        itemRef.setValue("12");
        silDetails.setItemRef(itemRef);

        line.setSalesItemLineDetail(silDetails);
        invLine.add(line);
        invoice.setLine(invLine);

        invoice.setRemitToRef(customerRef);

        invoice.setPrintStatus(PrintStatusEnum.NEED_TO_PRINT);
        invoice.setTotalAmt(new BigDecimal("10000"));
        invoice.setFinanceCharge(false);

        return invoice;
    }

    public Invoice getInvoice(DataService service) throws FMSException, ParseException {
        List<Invoice> invoices = (List<Invoice>) service.findAll(new Invoice());
        if (!invoices.isEmpty()) {
            return invoices.get(0);
        }
        return createItem(service);
    }


    private Invoice createItem(DataService service) throws FMSException, ParseException {
        return service.add(getInvoiceFields(service));
    }

    public ReferenceType getInvoiceRef(Invoice invoice) {
        ReferenceType invoiceRef = new ReferenceType();
        invoiceRef.setValue(invoice.getId());
        return invoiceRef;
    }

}