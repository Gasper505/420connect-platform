package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

/**
 * @author dderose
 */
public class ItemHelper {
    @Inject
    private ProductRepository productRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSync quickbookSync;


    public void syncItems(DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {
        long currentTime = DateTime.now().getMillis();
        long endTime = DateTime.now().getMillis();

        List<Product> blazeProductList = null;
        Iterable<Product> productsDetailsList = null;

        Iterable<QuickbookSyncDetails> syncItemsList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId, shopId,
                QuickbookSyncDetails.QuickbookEntityType.Item, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncItems = Lists.newLinkedList(syncItemsList);

        if (syncItems.isEmpty()) {
            blazeProductList = productRepository.getProductForQB(blazeCompanyId, shopId);
        } else {
            QuickbookSyncDetails quickbookSyncItems = syncItems.getLast();
            endTime = quickbookSyncItems.getEndTime();
            long startDate = endTime - 864000000l;

            List<Product> productListwitoutQbRef = productRepository.getProductsListWithoutQbRef(blazeCompanyId, shopId, startDate, endTime);
            productsDetailsList = productRepository.listByShopWithDates(blazeCompanyId, shopId, endTime, currentTime);

            blazeProductList = Lists.newArrayList(productsDetailsList);
            blazeProductList.addAll(productListwitoutQbRef);
        }
        LOG.info("Blaze product Count: " + blazeProductList.size());
        try {
            if (blazeProductList != null) {
                Iterator<Product> blazeProductIterator = blazeProductList.iterator();
                syncItemBatch(service, blazeProductIterator, blazeCompanyId, shopId, quickbookCompanyId, endTime, blazeProductList.size());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Items: ");
        }

    }

    //Method used to convert blaze product  to QB object
    public List<Item> convertBlazeProductIntoQB(Iterator<com.fourtwenty.core.domain.models.product.Product> blazeProductList, DataService service, String blazeCompanyId, String shopId) {

        List<Item> itemDetails = new ArrayList<Item>();

        QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        try {
            if (accountType != null) {
                //Income account
                LOG.info("Item Income Account Type:-" + accountType.getSales());
                Account incomeAccount = accountHelper.getAccount(service, accountType.getSales());

                //Asset account
                LOG.info("Item Asset Account Type:-" + accountType.getInventory());
                Account assetAccount = accountHelper.getAccount(service, accountType.getInventory());

                //Expense account
                LOG.info("Item Expense Account Type:-" + accountType.getSuppliesandMaterials());
                Account expenseAccount = accountHelper.getAccount(service, accountType.getSuppliesandMaterials());

                BigDecimal quantity = QBConstants.QUANTITY_MIN;
                int counter = 0;
                while ((counter < 30) && (blazeProductList.hasNext())) {
                    Product blazeProduct = blazeProductList.next();
                    Item item = new Item();
                    item.setPurchaseDesc(blazeProduct.getId());
                    item.setSyncToken(blazeProduct.getQbItemRef());

                    if (blazeProduct.getName() != null)
                        item.setName(blazeProduct.getName() + " " + "(" + blazeProduct.getSku() + ")");

                    item.setActive(blazeProduct.isActive());
                    item.setTaxable(blazeProduct.isEnableExciseTax());
                    if (blazeProduct.getUnitPrice() != null)
                        item.setUnitPrice(blazeProduct.getUnitPrice());

                    if (blazeProduct.getDescription() != null)

                        item.setDescription(blazeProduct.getDescription());

                    item.setType(ItemTypeEnum.INVENTORY);
                    item.setItemCategoryType(QBConstants.ITEM_CATEGORY);
                    if (blazeProduct.getUnitPrice() != null)
                        item.setPurchaseCost(blazeProduct.getUnitPrice());


                    if (blazeProduct.getQuantities().size() > 0) {
                        for (ProductQuantity productQuantity : blazeProduct.getQuantities()) {
                            quantity = quantity.add(productQuantity.getQuantity());
                            int result = quantity.compareTo(QBConstants.QUANTITY_MAX);
                            if (quantity != null) {
                                if (result == 0)
                                    item.setQtyOnHand(quantity);

                                if (result == 1)
                                    item.setQtyOnHand(QBConstants.QUANTITY_MAX);

                                if (result == -1)
                                    item.setQtyOnHand(quantity);

                            }

                        }

                    } else {
                        item.setQtyOnHand(QBConstants.QUANTITY_MIN);
                    }
                    item.setTrackQtyOnHand(true);
                    item.setSku(blazeProduct.getSku());
                    Long nowMillis = blazeProduct.getCreated();
                    DateTime jodatime = new DateTime(nowMillis);
                    item.setInvStartDate(jodatime.toDate());
                    item.setIncomeAccountRef(accountHelper.getAccountRef(incomeAccount));
                    item.setAssetAccountRef(accountHelper.getAccountRef(assetAccount));
                    item.setExpenseAccountRef(accountHelper.getAccountRef(expenseAccount));
                    itemDetails.add(item);
                    counter++;
                }
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create Item: " + error.getMessage() + "Detail::: " + error.getDetail() + "Status Code::::::" + error.getCode());
                //Get new Access token if Current expires in running mode
                if (error.getCode().equals(QBConstants.ERRORCODE)) {
                    LOG.info("Inside 3200 code");
                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Items: ");
        }
        return itemDetails;
    }


    // Method used to create non-inventory product for Delivery charge
    public static Item createDeliveryItem(DataService service, QuickbookCustomEntities accountType) {
        Item item = new Item();
        item.setName(QBConstants.DELIVERY_CHARGE);
        item.setActive(true);
        item.setTaxable(false);
        item.setUnitPrice(QBConstants.UNIT_PRICE);
        item.setType(ItemTypeEnum.NON_INVENTORY);
        try {
            Account incomeAccount = AccountHelper.getAccount(service, accountType.getSales());
            item.setIncomeAccountRef(AccountHelper.getAccountRef(incomeAccount));
            item.setPurchaseCost(QBConstants.UNIT_PRICE);
            Account expenseAccount = AccountHelper.getAccount(service, accountType.getSuppliesandMaterials());
            item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
            Account assetAccount = AccountHelper.getAccount(service, accountType.getInventory());
            item.setAssetAccountRef(AccountHelper.getAccountRef(assetAccount));
            item.setTrackQtyOnHand(false);
            Item savedItem = service.add(item);
            if (savedItem != null) {
                LOG.info("Item Created: " + savedItem.getId());
                return savedItem;
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create Delivery Item: " + error.getMessage() + "Detail::: " + error.getDetail());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Items: ");
        }


        return null;

    }

    // Method used to create non-inventory product for Excise charge
    public static Item createExciseTaxItem(DataService service, QuickbookCustomEntities entities) {
        Item item = new Item();
        item.setName(QBConstants.TOTAL_EXCISE_TAX.trim());
        item.setActive(true);
        item.setTaxable(false);
        item.setUnitPrice(QBConstants.UNIT_PRICE);
        item.setType(ItemTypeEnum.SERVICE);
        try {
            Account incomeAccount = AccountHelper.getAccount(service, entities.getClearance());
            item.setIncomeAccountRef(AccountHelper.getAccountRef(incomeAccount));
            item.setPurchaseCost(QBConstants.UNIT_PRICE);
            Account expenseAccount = AccountHelper.getAccount(service, entities.getSuppliesandMaterials());
            item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
            item.setTrackQtyOnHand(false);
            Item savedItem = service.add(item);
            if (savedItem != null) {
                LOG.info("Excise Tax Item Created: " + savedItem.getId());
                return savedItem;
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create Excise Tax Item: " + error.getMessage() + "Detail::: " + error.getDetail());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Excise tax Item: ");
        }

        return null;


    }

    // Method used to create non-inventory product for Excise tax reducer
    public static Item createExciseTaxReducerItem(DataService service, QuickbookCustomEntities accountType) {
        Item item = new Item();
        item.setName(QBConstants.PRE_EXCISE_TAX_REDUCER.trim());
        item.setActive(true);
        item.setTaxable(false);
        item.setUnitPrice(QBConstants.UNIT_PRICE);
        item.setType(ItemTypeEnum.SERVICE);
        try {
            Account taxReducerAccount = AccountHelper.getAccount(service, QBConstants.UNCATEGORIZED_ASSET);
            item.setIncomeAccountRef(AccountHelper.getAccountRef(taxReducerAccount));
            item.setPurchaseCost(QBConstants.UNIT_PRICE);
            Account expenseAccount = AccountHelper.getAccount(service, accountType.getSuppliesandMaterials());
            item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
            item.setTrackQtyOnHand(false);
            Item savedItem = service.add(item);
            if (savedItem != null) {
                LOG.info("Excise Tax Item Created: " + savedItem.getId());
                return savedItem;
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create Excise Tax Item reducer: " + error.getMessage() + "Detail::: " + error.getDetail());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Excise tax Item reducer: ");
        }

        return null;


    }


    // Sync Item Batch of 30 in QB
    public void syncItemBatch(DataService service, Iterator<com.fourtwenty.core.domain.models.product.Product> blazeProductList, String
            blazeCompanyId, String shopId, String quickbookCompanyId, long endTime, int total) {
        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int counter = 0, totalSuccess = 0;
        while (blazeProductList.hasNext()) {
            List<Item> qbItem = convertBlazeProductIntoQB(blazeProductList, service, blazeCompanyId, shopId);
            LOG.info(" Item Size:   " + qbItem.size());
            batchOperation = new BatchOperation();
            try {
                for (Item item : qbItem) {
                    try {
                        counter++;
                        com.fourtwenty.core.domain.models.product.Product productDetails = productRepository.get(blazeCompanyId, item.getPurchaseDesc());
                        LOG.info("Product Details:    " + productDetails);
                        String itemRef = productDetails.getQbItemRef();
                        LOG.info("Item Ref: " + itemRef);
                        if (itemRef == null) {
                            batchOperation.addEntity(item, OperationEnum.CREATE, "batchId" + counter);

                        } else {
                            totalSuccess++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                service.executeBatch(batchOperation);
                Thread.sleep(1000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    try {
                        if (batchOperation.isFault(bId)) {
                            Fault fault = batchOperation.getFault(bId);
                            // fault has a list of errors
                            for (com.intuit.ipp.data.Error error : fault.getError()) {
                                LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());

                            }

                        }


                        Item singleItem = (Item) batchOperation.getEntity(bId);
                        if (singleItem != null) {
                            totalSuccess++;
                            LOG.info("Result Item: " + singleItem);
                            String sku = singleItem.getSku();
                            Product productDetails = productRepository.findProductBySku(blazeCompanyId, shopId,sku);
                            LOG.info("Product details By SKU" + productDetails);
                            if (productDetails != null) {
                                LOG.info("Item Ref saved--" + singleItem.getId());
                                //Update Quickbook Ref into vendors table in Blaze
                                BasicDBObject updateQuery = new BasicDBObject();
                                updateQuery.append("$set", new BasicDBObject().append("qbItemRef", singleItem.getId()));
                                BasicDBObject searchQuery = new BasicDBObject();
                                searchQuery.append("_id", new ObjectId(productDetails.getId()));
                                productRepository.updateItemRef(searchQuery, updateQuery);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling Create Item: " + error.getMessage() + "Detail::: " + error.getDetail() + "Status Code::::::" + error.getCode());
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while calling Bulk Item Create: ");
            }

        }


        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.Item, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }

    // Get Batch of 30 Products
    public static List<Item> getItemBatch(List<Item> myList, int startIndex) {
        List<Item> sub = new ArrayList<Item>();
        sub = myList.subList(startIndex, Math.min(myList.size(), startIndex + 30));
        return sub;

    }


    // Method used to create non-inventory product for Expense
    public static Item createExpenseItem(DataService service, QuickbookCustomEntities accountType) {
        Item item = new Item();
        item.setName(QBConstants.EXPENSE_ITEM);
        item.setActive(true);
        item.setTaxable(false);
        item.setUnitPrice(QBConstants.UNIT_PRICE);
        item.setType(ItemTypeEnum.NON_INVENTORY);
        try {
            Account incomeAccount = AccountHelper.getAccount(service, accountType.getSales());
            item.setIncomeAccountRef(AccountHelper.getAccountRef(incomeAccount));
            item.setPurchaseCost(QBConstants.UNIT_PRICE);
            Account expenseAccount = AccountHelper.getAccount(service, accountType.getSuppliesandMaterials());
            item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
            item.setTrackQtyOnHand(false);
            Item savedItem = service.add(item);
            if (savedItem != null) {
                LOG.info(" Expense Item Created: " + savedItem.getId());
                return savedItem;
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create Expense Item: " + error.getMessage() + "Detail::: " + error.getDetail());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating expense item: ");
        }


        return null;

    }

    // Method used to create non-inventory product for Delivery charge
    public static Item createCreditCardFees(DataService service, QuickbookCustomEntities accountType) {
        Item item = new Item();
        item.setName(QBConstants.CREDIT_CARD_FEES);
        item.setActive(true);
        item.setTaxable(false);
        item.setUnitPrice(QBConstants.UNIT_PRICE);
        item.setType(ItemTypeEnum.SERVICE);
        try {
            Account incomeAccount = AccountHelper.getAccount(service, accountType.getCc_clearance());
            item.setIncomeAccountRef(AccountHelper.getAccountRef(incomeAccount));
            item.setPurchaseCost(QBConstants.UNIT_PRICE);
            Account expenseAccount = AccountHelper.getAccount(service, accountType.getSuppliesandMaterials());
            item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
            item.setTrackQtyOnHand(false);
            Item savedItem = service.add(item);
            if (savedItem != null) {
                LOG.info("Item Created: " + savedItem.getId());
                return savedItem;
            }


        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create credit card fees line Item: " + error.getMessage() + "Detail::: " + error.getDetail());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating Items: ");
        }


        return null;

    }


}