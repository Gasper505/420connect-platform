package com.fourtwenty.quickbook.online.helperservices;


import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.mongodb.BasicDBObject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

/**
 * @author dderose
 */
public final class JournalEntryHelper {
    @Inject
    ItemHelper itemHelper;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductBatchRepository batchRepository;


    //Sync Journal Entry into Quickbooks
    public void syncJournalEntry(DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {
        long endTime = DateTime.now().getMillis();
        long currentTime = DateTime.now().getMillis();

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> transactionListDetails = null;
        Iterable<QuickbookSyncDetails> syncJournalEntryList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId, shopId, QuickbookSyncDetails.QuickbookEntityType.JournalEntry, QBConstants.QUICKBOOK_ONLINE);


        LinkedList<QuickbookSyncDetails> syncJournalEntry = Lists.newLinkedList(syncJournalEntryList);
        List<Transaction> transactionList = null;

        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncJournalEntry.isEmpty()) {
            transactionListDetails = transactionRepository.getTransactionByTypeAndStatus(blazeCompanyId, shopId, Transaction.TransactionType.Sale, Transaction.TransactionStatus.Completed);
            transactionList = Lists.newArrayList(transactionListDetails);
        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncJournalEntry.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, currentTime, endTime, shopId, QuickbookSyncDetails.QuickbookEntityType.JournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

        } else {
            QuickbookSyncDetails quickbookSyncJournalEntry = syncJournalEntry.getLast();
            endTime = quickbookSyncJournalEntry.getEndTime();
            long startDate = endTime - 864000000l;

            List<Transaction> salesOrdersListWithoutQbRef = transactionRepository.getSalesOrdersListWithoutQbRef(blazeCompanyId, shopId, startDate, endTime);
            Iterable<Transaction> transactionIterable = transactionRepository.listByShopWithDateAndStatus(blazeCompanyId, shopId, endTime, currentTime, Transaction.TransactionType.Sale, Transaction.TransactionStatus.Completed);

            transactionList = Lists.newArrayList(transactionIterable);
            transactionList.addAll(salesOrdersListWithoutQbRef);


        }

        try {
            if (transactionList != null) {
                LOG.info("Transaction List Size: " + transactionList.size());
                Iterator<Transaction> transactionIterator = transactionList.iterator();
                syncJournalEntryBatch(service, transactionIterator, blazeCompanyId, quickbookCompanyId, shopId, endTime, transactionList.size(), entities);
            }


        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while calling create sales receipt :");
        }

    }

    //Convert Journal Entry into Quickbooks
    public List<JournalEntry> convertJournalEntryIntoQB(Iterator<Transaction> transactionIterator, DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId, QuickbookCustomEntities entities) {
        List<JournalEntry> resultJournalEntryList = new ArrayList<>();
        int counter = 0;
        while ((counter < 30) && (transactionIterator.hasNext())) {
            Transaction transaction = transactionIterator.next();
            JournalEntry qbJournalEntry = new JournalEntry();
            double amount = calculateCOGSperTransaction(blazeCompanyId, shopId, transaction);

            List<Line> linesList = qbLinesList(transaction, service, entities, amount, blazeCompanyId, shopId);
            TxnTaxDetail txnTaxDetail = new TxnTaxDetail();

            qbJournalEntry.setLine(linesList);
            qbJournalEntry.setTotalAmt(new BigDecimal(amount));
            qbJournalEntry.setDocNumber(transaction.getTransNo());
            qbJournalEntry.setTxnTaxDetail(txnTaxDetail);
            qbJournalEntry.setSyncToken(transaction.getQbJournalEntryRef());

            Long nowMillis = transaction.getCreated();
            DateTime jodatime = new DateTime(nowMillis);
            qbJournalEntry.setTxnDate(jodatime.toDate());
            resultJournalEntryList.add(qbJournalEntry);
            counter++;

        }


        return resultJournalEntryList;
    }

    //Add Line item of carts
    public List<Line> qbLinesList(Transaction transaction, DataService service, QuickbookCustomEntities entities, double amount, String blazeCompanyId, String shopId) {
        List<Line> linesList = new ArrayList<Line>();
        //Credit Line Item
        try {
            Line lineObject = new Line();
            lineObject.setAmount(new BigDecimal(amount));
            lineObject.setDetailType(LineDetailTypeEnum.JOURNAL_ENTRY_LINE_DETAIL);
            JournalEntryLineDetail journalEntryLineDetail = new JournalEntryLineDetail();
            journalEntryLineDetail.setPostingType(PostingTypeEnum.CREDIT);
            Account creditCardBankAccount = AccountHelper.getAccount(service, entities.getInventory());
            journalEntryLineDetail.setAccountRef(accountHelper.getAccountRef(creditCardBankAccount));
            lineObject.setJournalEntryLineDetail(journalEntryLineDetail);
            linesList.add(lineObject);

            //Debit Line Item
            Line debitLine = new Line();
            debitLine.setAmount(new BigDecimal(amount));
            debitLine.setDetailType(LineDetailTypeEnum.JOURNAL_ENTRY_LINE_DETAIL);
            JournalEntryLineDetail journalEntryLineDetailDebit = new JournalEntryLineDetail();
            journalEntryLineDetailDebit.setPostingType(PostingTypeEnum.DEBIT);
            Account debitAccount = AccountHelper.getAccount(service, entities.getSuppliesandMaterials());
            journalEntryLineDetailDebit.setAccountRef(accountHelper.getAccountRef(debitAccount));
            debitLine.setJournalEntryLineDetail(journalEntryLineDetailDebit);
            linesList.add(debitLine);
        } catch (FMSException e) {
            quickbookSync.getAccessTokenByShop(shopId);
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling entity Sales Receipt Journal entry:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::" + error.getCode());
                //Get Access token if Access token Expires after an hour
                if (error.getCode().equals(QBConstants.ERRORCODE)) {
                    LOG.info("Inside 3200 code");
                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                }
            }
        }


        return linesList;

    }

    //Sync Batch
    public void syncJournalEntryBatch(DataService service, Iterator<Transaction> transactions, String blazeCompanyId, String quickbookCompanyId, String shopId, long endTime, int total, QuickbookCustomEntities entities) {
        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int totalSuccess = 0;
        int count = 0;

        while (transactions.hasNext()) {

            List<JournalEntry> qbJournalEntry = convertJournalEntryIntoQB(transactions, service, quickbookCompanyId, blazeCompanyId, shopId, entities);
            LOG.info("JournalEntry Batch Size: " + qbJournalEntry.size());
            batchOperation = new BatchOperation();
            for (JournalEntry journalEntry : qbJournalEntry) {
                if (journalEntry.getSyncToken() == null) {
                    count++;
                    batchOperation.addEntity(journalEntry, OperationEnum.CREATE, "bID" + count);
                    LOG.info("create JournalEntry : " + journalEntry.getSyncToken());

                } else {
                    totalSuccess++;
                }
            }
            try {
                //Execute Batch
                service.executeBatch(batchOperation);
                Thread.sleep(2000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());
                        }

                    }

                    JournalEntry journalEntry = (JournalEntry) batchOperation.getEntity(bId);
                    if (journalEntry != null) {
                        totalSuccess++;
                        String journalEntryId = journalEntry.getDocNumber();
                        Transaction transactionRepositoryorDetails = transactionRepository.getTransactionByTransNo(blazeCompanyId, shopId, journalEntryId);
                        LOG.info("Journal Entry inside result: " + journalEntry.getId());

                        if (transactionRepositoryorDetails != null) {
                            LOG.info("Reference saved :" + journalEntry.getId());
                            BasicDBObject updateQuery = new BasicDBObject();
                            updateQuery.append("$set", new BasicDBObject().append("qbJournalEntryRef", journalEntry.getId()));
                            BasicDBObject searchQuery = new BasicDBObject();
                            searchQuery.append("_id", new ObjectId(transactionRepositoryorDetails.getId()));
                            transactionRepository.updateJournalEntryRef(searchQuery, updateQuery);
                            LOG.info("Add journal entry ref in DB");

                        }

                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling sales receipt journal entry:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::" + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.JournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }


    public double calculateCOGSperTransaction(String blazeCompanyId, String shopId, Transaction transaction) {
        List<Transaction> results = new ArrayList<Transaction>();
        results.add(transaction);
        Iterable<Product> products = productRepository.list(blazeCompanyId);
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(blazeCompanyId);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(blazeCompanyId, shopId);
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(blazeCompanyId);

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        for (Transaction ts : results) {
            double transCogs = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if (item.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                boolean calculated = false;
                double itemCogs = 0;
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        itemCogs += calcCOGS(unitsSold, targetBatch);

                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                itemCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                            }
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    itemCogs = unitCost * item.getQuantity().doubleValue();
                }
                transCogs += itemCogs;
                LOG.info("COgs per Transaction:" + transCogs);
                return transCogs;
            }


        }
        return 0.0;
    }


    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }


}