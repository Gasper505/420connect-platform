package com.fourtwenty.quickbook.online.helperservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class OauthHelper extends AbstractAuthServiceImpl {
    @Inject
    HttpHelper httpHelper;
    @Inject
    BearerTokenResponse bearerTokenResponse;
    @Inject
    private ConnectConfiguration configuration;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    @Inject
    public OauthHelper(Provider<ConnectAuthToken> token) {
        super(token);
    }

    private HttpClient httpClient = HttpClientBuilder.create().build();


    public BearerTokenResponse getBearerTokenResponse(String authCode, String shopId) {
        String url = configuration.getQuickBookConfig().getRefreshTokenURI();
        HttpPost post = new HttpPost(url);
        // add header
        post = httpHelper.addHeader(post);
        ArrayList<NameValuePair> urlParameters;
        urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("code", authCode));
        urlParameters.add(new BasicNameValuePair("redirect_uri", configuration.getQuickBookConfig().getRedirectURI()));
        urlParameters.add(new BasicNameValuePair("grant_type", "authorization_code"));
        try {
            post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
            HttpResponse response = httpClient.execute(post);
            LOG.info("Response Code : " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() != 200) {
                LOG.info("failed getting access token:" + response.getStatusLine().getReasonPhrase());
                return null;
            }

            StringBuffer result = httpHelper.getResult(response);
            LOG.debug("raw result for bearer tokens: " + result);
            ObjectMapper objectMapper = new ObjectMapper();
            bearerTokenResponse = objectMapper.readValue(result.toString(), BearerTokenResponse.class);
        } catch (Exception ex) {
            LOG.info("Exception while retrieving bearer tokens:", ex);
        }
        return bearerTokenResponse;
    }


    public HttpResponse revokeQuickbookAccess() {
        String url = configuration.getQuickBookConfig().getRevokeTokenURI();
        HttpPost post = new HttpPost(url);
        HttpResponse response = null;
        try {

            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(token.getCompanyId(), token.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            if (thirdPartyAccountsDetail != null) {
                post = httpHelper.addHeader(post);
                ArrayList<NameValuePair> urlParameters;
                urlParameters = new ArrayList<NameValuePair>();
                urlParameters.add(new BasicNameValuePair("token", thirdPartyAccountsDetail.getUsername()));
                post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
                response = httpClient.execute(post);
                LOG.info("Disconnect Quickbook status" + response.getStatusLine().getStatusCode());
                if (response.getStatusLine().getStatusCode() == 200) {
                    LOG.info("Disconnect Quickbook status" + response.getStatusLine().getStatusCode());
                    thirdPartyAccountRepository.deleteAccount(token.getCompanyId(), token.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                    return response;
                } else {
                    LOG.info("Disconnect Quickbook status" + response.getStatusLine().getStatusCode());
                    return null;
                }
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return response;
    }


}
