package com.fourtwenty.quickbook.online.helperservices;

import com.intuit.ipp.data.PaymentMethod;
import com.intuit.ipp.data.PaymentTypeEnum;
import com.intuit.ipp.data.ReferenceType;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.DataService;

import java.util.List;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class PaymentHelper {


    public PaymentMethod createPaymentMethod(DataService service, String paymentMethodname) {
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setName(paymentMethodname);
        if (paymentMethodname.equals("Cash")) {
            paymentMethod.setType(PaymentTypeEnum.CASH.name());
        }
        if (paymentMethodname.equals("Credit")) {
            paymentMethod.setType(PaymentTypeEnum.CREDIT_CARD.name());
        }
        if (paymentMethodname.equals("Check")) {
            paymentMethod.setType(PaymentTypeEnum.CHECK.name());
        }

        try {
            PaymentMethod paymentMethod1 = service.add(paymentMethod);
            System.out.println("paymentMethod created:" + paymentMethod1);
            return paymentMethod1;

        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling payment method:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::"
                        + error.getCode());
            }
        }
        return null;
    }

    public PaymentMethod getPaymentMethod(DataService service, String paymentMethodname) {
        try {
            List<PaymentMethod> paymentMethods = (List<PaymentMethod>) service.findAll(new PaymentMethod());
            if (!paymentMethods.isEmpty()) {
                for (PaymentMethod paymentMethod : paymentMethods) {
                    if (paymentMethod.getName().equals(paymentMethodname)) {
                        return paymentMethod;
                    }
                }

            }
        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling get payment method:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::"
                        + error.getCode());
            }
        }
        return createPaymentMethod(service, paymentMethodname);

    }


    public ReferenceType getPaymentMethodRef(PaymentMethod paymentMethod) {
        ReferenceType paymentMethodRef = new ReferenceType();
        paymentMethodRef.setValue(paymentMethod.getId());
        return paymentMethodRef;
    }

}



