package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class PurchaseHelper {
    @Inject
    PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;
    @Inject
    ItemHelper itemHelper;


    public void syncPurchaseOrders(DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {
        long endTime = DateTime.now().getMillis();
        long currentTime = DateTime.now().getMillis();

        Iterable<com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder> purchaseOrderListDetails = null;
        Iterable<QuickbookSyncDetails> syncPurchaseList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId,
                shopId, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, QBConstants.QUICKBOOK_ONLINE);

        List<com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder> purchaseOrderList = null;
        LinkedList<QuickbookSyncDetails> syncPurchaseOrder = Lists.newLinkedList(syncPurchaseList);

        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncPurchaseOrder.isEmpty()) {

            purchaseOrderListDetails = purchaseOrderRepository.getPurchasOrderByStatus(blazeCompanyId, shopId);
            purchaseOrderList = Lists.newArrayList(purchaseOrderListDetails);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncPurchaseOrder.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, currentTime, endTime,
                    shopId, QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

        } else {
            QuickbookSyncDetails quickbookSyncPurchaseOrder = syncPurchaseOrder.getLast();
            endTime = quickbookSyncPurchaseOrder.getEndTime();
            long startDate = endTime - 864000000l;

            List<com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder> purchaseOrderListwithoutQbRef =
                    purchaseOrderRepository.getPurchaseOrdersListWithoutQbRef(blazeCompanyId, shopId, startDate, endTime);

            purchaseOrderListDetails = purchaseOrderRepository.listByShopWithDate(blazeCompanyId, shopId, endTime, currentTime);

            purchaseOrderList = Lists.newArrayList(purchaseOrderListDetails);
            purchaseOrderList.addAll(purchaseOrderListwithoutQbRef);

        }

        try {
            if (purchaseOrderList != null) {
                LOG.info("Purchase OrderList:" + purchaseOrderList.size());
                Iterator<com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder> purchaseOrderIterator = purchaseOrderList.iterator();
                syncPurchaseOrderBatch(service, purchaseOrderIterator, blazeCompanyId, quickbookCompanyId, shopId, endTime, purchaseOrderList.size());
            }


        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Exception while Adding purchase order ");

        }


    }

    public List<PurchaseOrder> convertBlazePurchaseOrderIntoQB(Iterator<com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder>
                                                                       purchaseOrderDetailsList, String quickbookCompanyId, String blazeCompanyId, String shopId, DataService service) {

        double exciseTax = 0;
        double cityTax = 0;
        double countyTax = 0;
        double stateTax = 0;
        double federalTax = 0;
        double totalTax = 0;

        List<PurchaseOrder> purchaseOrderList = new ArrayList<PurchaseOrder>();
        int counter = 0;
        while ((counter < 30) && (purchaseOrderDetailsList.hasNext())) {
            com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder purchaseOrderDetails = purchaseOrderDetailsList.next();
            try {
                PurchaseOrder purchaseOrder = new PurchaseOrder();
                LOG.info("purchaseOrderDetails ID :" + purchaseOrderDetails.getId());
                com.fourtwenty.core.domain.models.product.Vendor vendorDetails = vendorRepository.get(purchaseOrderDetails.getCompanyId(),
                        purchaseOrderDetails.getVendorId());

                LOG.info("Product Request size :" + purchaseOrderDetails.getPoProductRequestList().size());
                if (purchaseOrderDetails.getQbPurchaseOrderRef() != null) {
                    purchaseOrder.setPrivateNote(purchaseOrderDetails.getQbPurchaseOrderRef());
                }
                purchaseOrder.setMemo(purchaseOrderDetails.getId());

                List<Line> lines = qbLineList(purchaseOrderDetails, blazeCompanyId, shopId, service);
                LOG.info("Linelist Size :" + lines.size());
                purchaseOrder.setLine(lines);

                TxnTaxDetail txnTaxDetail = new TxnTaxDetail();
                txnTaxDetail.setTotalTax(purchaseOrderDetails.getTotalCalcTax());
                purchaseOrder.setTxnTaxDetail(txnTaxDetail);
                if (vendorDetails != null) {
                    ReferenceType vendorRef = vendorRef(purchaseOrderDetails, vendorDetails, quickbookCompanyId, blazeCompanyId);
                    if (vendorRef != null) {
                        purchaseOrder.setVendorRef(vendorRef);

                        PhysicalAddress physicalAddress = new PhysicalAddress();
                        if (vendorDetails != null) {
                            if (vendorDetails.getAddress() != null) {
                                if (vendorDetails.getAddress().getCity() != null) {
                                    physicalAddress.setCity(vendorDetails.getAddress().getCity());
                                }
                                if (vendorDetails.getAddress().getCountry() != null) {
                                    physicalAddress.setCountry(vendorDetails.getAddress().getCountry());
                                }

                                if (vendorDetails.getAddress().getZipCode() != null) {
                                    physicalAddress.setPostalCode(vendorDetails.getAddress().getZipCode());
                                }
                                purchaseOrder.setVendorAddr(physicalAddress);
                            }

                        }

                        EmailAddress emailAddress = new EmailAddress();
                        if (vendorDetails.getEmail() != null) {
                            emailAddress.setAddress(vendorDetails.getEmail());
                        }
                        purchaseOrder.setPOStatus(PurchaseOrderStatusEnum.OPEN);
                        purchaseOrder.setDocNumber(purchaseOrderDetails.getPoNumber());
                        purchaseOrder.setPOEmail(emailAddress);
                        purchaseOrder.setDomain(purchaseOrderDetails.getPoType().toString());
                        purchaseOrder.setGlobalTaxCalculation(GlobalTaxCalculationEnum.NOT_APPLICABLE);
                        purchaseOrder.setReplyEmail(emailAddress);
                        purchaseOrder.setTotalAmt(purchaseOrderDetails.getTotalCost());
                        Long nowMillis = purchaseOrderDetails.getCreated();
                        DateTime jodatime = new DateTime(nowMillis);
                        purchaseOrder.setTxnDate(jodatime.toDate());


                        Account payableAccount = accountRef(blazeCompanyId, shopId, service);
                        if (payableAccount != null)
                            purchaseOrder.setAPAccountRef(accountHelper.getAccountRef(payableAccount));


                        //Add tax Sepearation to Private Note Field

                        if (purchaseOrderDetails.getTaxResult() != null) {
                            TaxResult taxResult = purchaseOrderDetails.getTaxResult();
                            cityTax = NumberUtils.round(taxResult.getTotalCityTax().doubleValue(), 2);
                            countyTax = NumberUtils.round(taxResult.getTotalCountyTax().doubleValue(), 2);
                            stateTax = NumberUtils.round(taxResult.getTotalStateTax().doubleValue(), 2);
                            federalTax = NumberUtils.round(taxResult.getTotalFedTax().doubleValue(), 2);
                            exciseTax = NumberUtils.round(purchaseOrderDetails.getTaxResult().getTotalExciseTax().doubleValue(), 2);
                        }

                        double totalTaxDetails = purchaseOrderDetails.getTotalCalcTax().doubleValue() - purchaseOrderDetails.getTotalPreCalcTax().doubleValue();
                        totalTax = NumberUtils.round(totalTaxDetails, 2);
                        if (totalTax < 0) {
                            totalTax = 0;
                        }


                        String taxSeperation = "";
                        if (exciseTax > 0) {
                            taxSeperation = "Total ExciseTax Tax: " + "$ " + exciseTax + "\n" + "Total City Tax: " + "$ " + cityTax + "\n" + "Total County Tax: " + "$ "
                                    + countyTax + "\n" + "Total State Tax:" + "$ " + stateTax + "\n" + "Total Federal Tax: " + "$ " + federalTax + "\n" + "Total Tax: " + "$ " + totalTax;
                        } else {
                            taxSeperation = "Total City Tax: " + "$ " + cityTax + "\n" + "Total County Tax: " + "$ " + countyTax + "\n" +
                                    "Total State Tax:" + "$ " + stateTax + "\n" + "Total Federal Tax: " + "$ " + federalTax + "\n" + "Total Tax: " + "$ " + totalTax;

                        }


                        LOG.info("Total Tax Separation: " + taxSeperation);
                        if (exciseTax > 0) {
                            purchaseOrder.setPrivateNote(taxSeperation);
                        }
                        purchaseOrderList.add(purchaseOrder);
                        counter++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while returning Purchase Object");
            }
        }


        return purchaseOrderList;
    }

    public List<Line> qbLineList(com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder purchaseOrderDetails, String blazecompanyId,
                                 String shopId, DataService service) {

        List<Line> lineList = new ArrayList<Line>();

        for (POProductRequest product : purchaseOrderDetails.getPoProductRequestList()) {

            Product blazeProduct = productRepository.get(product.getCompanyId(), product.getProductId());

            ItemBasedExpenseLineDetail lineItemDetails = new ItemBasedExpenseLineDetail();

            //Line item
            Line lineObject = new Line();
            lineObject.setAmount(product.getTotalCost());
            lineObject.setDescription(blazeProduct.getDescription());
            lineObject.setDetailType(LineDetailTypeEnum.ITEM_BASED_EXPENSE_LINE_DETAIL);


            //Customer ref
            ReferenceType customerRef = new ReferenceType();
            lineItemDetails.setCustomerRef(customerRef);
            LOG.info(" Quickbook ItemRef : " + blazeProduct.getQbItemRef());
            LOG.info("Blaze ProductId :" + blazeProduct.getId());

            ReferenceType itemref = new ReferenceType();

            if (blazeProduct.getQbItemRef() != null) {
                itemref.setValue(blazeProduct.getQbItemRef());
                itemref.setName(blazeProduct.getName());
                lineItemDetails.setItemRef(itemref);
            }

            ReferenceType taxcode = new ReferenceType();
            taxcode.setValue(QBConstants.TAX);

            lineItemDetails.setTaxCodeRef(taxcode);

            lineItemDetails.setUnitPrice(product.getUnitPrice());
            lineItemDetails.setQty(product.getRequestQuantity());
            lineItemDetails.setBillableStatus(BillableStatusEnum.NOT_BILLABLE);

            lineObject.setItemBasedExpenseLineDetail(lineItemDetails);
            lineList.add(lineObject);
        }

        //Add Excise Tax as a line item
        ItemBasedExpenseLineDetail exciseTaxItemDetails = new ItemBasedExpenseLineDetail();
        ReferenceType taxItemRef = new ReferenceType();
        QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                QBConstants.TOTAL_EXCISE_TAX);

        LOG.info("quickbookenity:" + quickbookEntity);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
        if (quickbookEntity == null) {
            //create Item
            Item taxItem = ItemHelper.createExciseTaxItem(service, entities);
            if (taxItem.getId() != null) {
                quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, taxItem.getId(),
                        QBConstants.TOTAL_EXCISE_TAX);

                LOG.info("TaxItemId: " + taxItem.getId());
                taxItemRef.setName(taxItem.getName());
                taxItemRef.setValue(taxItem.getId());
                exciseTaxItemDetails.setItemRef(taxItemRef);
            }

        } else {
            if (quickbookEntity.getProductRef() != null) {
                taxItemRef.setName(quickbookEntity.getProductName());
                taxItemRef.setValue(quickbookEntity.getProductRef());
                exciseTaxItemDetails.setItemRef(taxItemRef);
            }

        }

        Line totalExciseLine = new Line();
        if (purchaseOrderDetails.getTaxResult() != null) {
            totalExciseLine.setAmount(purchaseOrderDetails.getTaxResult().getTotalExciseTax());
        } else {
            totalExciseLine.setAmount(QBConstants.QUANTITY_MIN);
        }
        totalExciseLine.setDetailType(LineDetailTypeEnum.ITEM_BASED_EXPENSE_LINE_DETAIL);


        if (purchaseOrderDetails.getTaxResult() != null) {
            exciseTaxItemDetails.setUnitPrice(purchaseOrderDetails.getTaxResult().getTotalExciseTax());
        } else {
            exciseTaxItemDetails.setUnitPrice(QBConstants.QUANTITY_MIN);
        }

        exciseTaxItemDetails.setQty(QBConstants.UNIT_PRICE);
        exciseTaxItemDetails.setBillableStatus(BillableStatusEnum.NOT_BILLABLE);
        totalExciseLine.setItemBasedExpenseLineDetail(exciseTaxItemDetails);
        lineList.add(totalExciseLine);


        return lineList;
    }

    public ReferenceType vendorRef(com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder purchaseOrderDetails, Vendor vendorDetails,
                                   String quickbookCompanyId, String blazeCompanyId) {

        ReferenceType vendorRef = new ReferenceType();
        Vendor vendorDetail = vendorRepository.get(blazeCompanyId, purchaseOrderDetails.getVendorId());
        if (vendorDetail.getQbVendorRef() != null) {
            LOG.info("vendor Reference :" + vendorDetail.getQbVendorRef());
            List<HashMap<String, String>> getQbrefMaps = vendorDetail.getQbVendorRef();
            for (HashMap<String, String> getQbrefmap : getQbrefMaps) {
                LOG.info("Check Ref In DB" + getQbrefmap.get(quickbookCompanyId));
                if (getQbrefmap.get(quickbookCompanyId) != null) {
                    vendorRef.setValue(getQbrefmap.get(quickbookCompanyId));
                }
                vendorRef.setName(vendorDetails.getName());
            }
        }
        return vendorRef;
    }

    public Account accountRef(String blazeCompanyId, String shopId, DataService service) {
        Account payableAccount = new Account();
        try {
            QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
            payableAccount = accountHelper.getAccount(service, accountType.getPayableAccount());

        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling entity add purchase order:: " + error.getMessage() + " Details:::" + error.getDetail() + " statuscode===" + error.getCode());
                if (error.getCode().equals(QBConstants.ERRORCODE)) {
                    LOG.info("Inside 3200 code");
                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId,
                            ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                }

            }
        }
        return payableAccount;

    }

    //sync purchase Order Details In QB
    public void syncPurchaseOrderBatch(DataService service, Iterator<com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder> blazePurchaseList,
                                       String blazeCompanyId, String quickbookCompanyId, String shopId, long endTime, int total) {

        BatchOperation batchOperation = null;
        int totalSuccess = 0;
        long startTime = DateTime.now().getMillis();
        while (blazePurchaseList.hasNext()) {
            try {
                List<PurchaseOrder> qbPurchase = convertBlazePurchaseOrderIntoQB(blazePurchaseList, quickbookCompanyId, blazeCompanyId, shopId, service);
                int counter = 0;
                LOG.info("Purchase batch Size: " + qbPurchase.size());
                batchOperation = new BatchOperation();
                for (PurchaseOrder purchaseOrder : qbPurchase) {
                    counter++;
                    System.out.println("purchase name:" + purchaseOrder.getMemo());
                    if (purchaseOrder.getSyncToken() == null) {
                        batchOperation.addEntity(purchaseOrder, OperationEnum.CREATE, "bid" + counter);
                        System.out.println("create Purchase: ");
                    } else {
                        totalSuccess++;
                    }

                }

                service.executeBatch(batchOperation);
                Thread.sleep(1000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    try {
                        if (batchOperation.isFault(bId)) {
                            Fault fault = batchOperation.getFault(bId);
                            // fault has a list of errors
                            for (com.intuit.ipp.data.Error error : fault.getError()) {
                                LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());

                            }
                        }

                        PurchaseOrder singelPurchase = (PurchaseOrder) batchOperation.getEntity(bId);
                        if (singelPurchase != null) {
                            totalSuccess++;
                            String purchaseId = singelPurchase.getMemo();
                            com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder purchase = purchaseOrderRepository.get(blazeCompanyId, purchaseId);
                            LOG.info("purchase inside result: " + purchase);

                            if (purchase != null) {
                                LOG.info("Saved Reference: " + singelPurchase.getId());
                                //Update Quickbook Ref into purchase order table in Blaze
                                BasicDBObject updateQuery = new BasicDBObject();
                                updateQuery.append("$set", new BasicDBObject().append("qbPurchaseOrderRef", singelPurchase.getId()));
                                BasicDBObject searchQuery = new BasicDBObject();
                                searchQuery.append("_id", new ObjectId(purchase.getId()));
                                purchaseOrderRepository.updatePurchaseOrderRef(searchQuery, updateQuery);
                                LOG.info("PurchaseOrder created: " + singelPurchase.getId() + " ::purchaseorder doc num: " + singelPurchase.getDocNumber());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (FMSException e) {

                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling entity add purchase orders:: " + error.getMessage() + " Details:::" + error.getDetail() + " statuscode===" + error.getCode());
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId,
                                ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Exception While creating  purchase Orders");
            }
        }

        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId,
                QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

    }


}