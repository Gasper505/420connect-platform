package com.fourtwenty.quickbook.online.helperservices;

import java.math.BigDecimal;

public class QBConstants {

    public static final BigDecimal QUANTITY_MIN = new BigDecimal("0.0");
    public static final BigDecimal QUANTITY_MAX = new BigDecimal("999999");
    public static final BigDecimal UNIT_PRICE = new BigDecimal("1.0");
    public static final String ITEM_CATEGORY = "Service";
    public static final String DELIVERY_CHARGE = "Delivery charge";
    public static final String TOTAL_EXCISE_TAX = "Total Excise Tax";
    public static final String PRE_EXCISE_TAX_REDUCER = "Pre Excise Tax Reducer";
    public static final String TOTAL_TAX = "Total Tax";
    public static final String ERRORCODE = "3200";
    public static final String TAX = "TAX";
    public static final String SYNC_ALL_DATA = "all data";
    public static final String SYNC_FROM_CURRENT_DATE = "from current date";
    public static final String SYNC_INDIVIDUAL = "sync individual";
    public static final String SYNC_SALES_BY_PRODUCT_CATEGORY = "sync sales by product category";
    public static final String SYNC_SALES_BY_CONSUMER_TYPE = "sync sales by consumer type";
    public static final String DAILY_SYNC_VENDOR = "Daily Sync Vendor";
    public static final String DAILY_SYNC_EXPENSES = "Daily Sync Expenses";
    public static final String UNCATEGORIZED_ASSET = "Uncategorized Asset";
    public static final String EXPENSE_ITEM = "Expense Item";
    public static final String QUICKBOOK_ONLINE = "quickbook_online";
    public static final String QUICKBOOK_DESKTOP = "quickbook_desktop";
    public static final String CREDIT_CARD_FEES = "Credit card fees";


}
