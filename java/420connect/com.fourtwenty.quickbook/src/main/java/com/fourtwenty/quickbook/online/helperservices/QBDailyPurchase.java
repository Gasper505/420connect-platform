package com.fourtwenty.quickbook.online.helperservices;


import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.DataService;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class QBDailyPurchase {
    @Inject
    PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    PurchaseHelper purchaseHelper;
    @Inject
    AccountHelper accountHelper;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;


    public void dailyPurchaseInQB(String blazeCompanyId, String shopId, String accessToken, String qbCompanyId) throws Exception {
        int count = 0;
        double total = 0.0;
        double excisetax = 0.0;

        long endTime = DateTime.now().getMillis();
        long currentTime = DateTime.now().getMillis();

        DataService service = quickbookSync.getService(accessToken, qbCompanyId);
        Iterable<PurchaseOrder> purchaseOrderIterable = null;
        Iterable<QuickbookSyncDetails> syncPurchase = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId,
                shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncPurchaseList = Lists.newLinkedList(syncPurchase);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        List<PurchaseOrder> purchaseList = null;

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncPurchaseList.isEmpty()) {
            purchaseOrderIterable = purchaseOrderRepository.listAllByShop(blazeCompanyId, shopId);
            purchaseList = Lists.newArrayList(purchaseOrderIterable);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncPurchaseList.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, currentTime, endTime, shopId,
                    QuickbookSyncDetails.QuickbookEntityType.Expenses, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


        } else {
            QuickbookSyncDetails blazePurchase = syncPurchaseList.getLast();
            endTime = blazePurchase.getEndTime();
            Iterable<PurchaseOrder> purchaseIterable = purchaseOrderRepository.listByShopWithDate(blazeCompanyId, shopId, endTime, currentTime);
            purchaseList = Lists.newArrayList(purchaseIterable);
        }

        for (PurchaseOrder purchaseOrder : purchaseList) {
            count++;
            total += purchaseOrder.getTotalCost().doubleValue();
            if (purchaseOrder.getTaxResult() != null) {
                excisetax += purchaseOrder.getTaxResult().getTotalExciseTax().doubleValue();
            }

            LOG.info("total Expenses: " + total);
            LOG.info("total excise tax: " + excisetax);
        }


        if ((count > 0)) {
            syncDailyPurchaseInQB(service, total, blazeCompanyId, shopId, endTime, excisetax);
        }

    }

    public void syncDailyPurchaseInQB(DataService service, double total, String blazeCompanyId, String shopId, long endTime, double exciseTax) {
        int totalSuccess = 0;
        int totalRecords = 1;

        long startTime = DateTime.now().getMillis();
        com.intuit.ipp.data.PurchaseOrder purchaseOrder = new com.intuit.ipp.data.PurchaseOrder();
        QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        try {
            Vendor savedVendor = null;
            ReferenceType vendorRef = new ReferenceType();
            QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazeCompanyId, shopId,
                    QuickbookEntity.SyncStrategy.DailySyncVendorRef, QBConstants.DAILY_SYNC_VENDOR);
            LOG.info("quickbook entity result: " + quickbookEntity);
            if (quickbookEntity == null) {
                Vendor vendor = new Vendor();
                vendor.setDisplayName(QBConstants.DAILY_SYNC_VENDOR);
                savedVendor = service.add(vendor);
                vendorRef.setName(savedVendor.getDisplayName());
                vendorRef.setValue(savedVendor.getId());
                quickbookEntityRepository.saveQuickbookEntity(blazeCompanyId, shopId, QuickbookEntity.SyncStrategy.DailySyncVendorRef,
                        savedVendor.getId(), savedVendor.getDisplayName());
            } else {
                vendorRef.setName(quickbookEntity.getProductName());
                vendorRef.setValue(quickbookEntity.getProductRef());
            }

            purchaseOrder.setVendorRef(vendorRef);
            purchaseOrder.setMemo("For Internal usage");

            List<Line> lineList = new ArrayList<Line>();

            //Add Expense Item
            ItemBasedExpenseLineDetail expenseLineDetail = new ItemBasedExpenseLineDetail();
            ReferenceType expenseItemRef = new ReferenceType();
            QuickbookEntity expenseItemDetail = quickbookEntityRepository.getQbEntityByName(blazeCompanyId, shopId,
                    QuickbookEntity.SyncStrategy.ProductRef, QBConstants.EXPENSE_ITEM);

            LOG.info("quickbookenity:" + expenseItemDetail);
            if (expenseItemDetail == null) {
                //create Item
                Item expenseItem = ItemHelper.createExpenseItem(service, accountType);
                if (expenseItem != null) {
                    quickbookEntityRepository.saveQuickbookEntity(blazeCompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                            expenseItem.getId(), QBConstants.EXPENSE_ITEM);

                    LOG.info("TaxItemId: " + expenseItem.getId());
                    expenseItemRef.setName(expenseItem.getName());
                    expenseItemRef.setValue(expenseItem.getId());
                    expenseLineDetail.setItemRef(expenseItemRef);
                }

            } else {
                if (expenseItemDetail.getProductRef() != null) {
                    expenseItemRef.setName(expenseItemDetail.getProductName());
                    expenseItemRef.setValue(expenseItemDetail.getProductRef());
                    expenseLineDetail.setItemRef(expenseItemRef);
                }

            }

            Line expenseLine = new Line();
            expenseLine.setAmount(new BigDecimal(total));
            expenseLine.setDetailType(LineDetailTypeEnum.ITEM_BASED_EXPENSE_LINE_DETAIL);
            expenseLineDetail.setUnitPrice(new BigDecimal(total));
            expenseLineDetail.setQty(QBConstants.UNIT_PRICE);
            expenseLineDetail.setBillableStatus(BillableStatusEnum.NOT_BILLABLE);
            expenseLine.setItemBasedExpenseLineDetail(expenseLineDetail);
            lineList.add(expenseLine);


            //Add Excise Tax as a line item
            ItemBasedExpenseLineDetail exciseTaxItemDetails = new ItemBasedExpenseLineDetail();
            ReferenceType taxItemRef = new ReferenceType();
            QuickbookEntity exciseTaxItem = quickbookEntityRepository.getQbEntityByName(blazeCompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                    QBConstants.TOTAL_EXCISE_TAX);

            LOG.info("quickbookenity:" + exciseTaxItem);
            if (exciseTaxItem == null) {
                //create Item

                Item taxItem = ItemHelper.createExciseTaxItem(service, accountType);
                if (taxItem != null) {
                    quickbookEntityRepository.saveQuickbookEntity(blazeCompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                            taxItem.getId(), QBConstants.TOTAL_EXCISE_TAX);
                    LOG.info("TaxItemId: " + taxItem.getId());
                    taxItemRef.setName(taxItem.getName());
                    taxItemRef.setValue(taxItem.getId());
                    exciseTaxItemDetails.setItemRef(taxItemRef);
                }

            } else {
                if (exciseTaxItem.getProductRef() != null) {
                    taxItemRef.setName(exciseTaxItem.getProductName());
                    taxItemRef.setValue(exciseTaxItem.getProductRef());
                    exciseTaxItemDetails.setItemRef(taxItemRef);
                }

            }

            Line totalExciseLine = new Line();
            totalExciseLine.setAmount(new BigDecimal(exciseTax));
            totalExciseLine.setDetailType(LineDetailTypeEnum.ITEM_BASED_EXPENSE_LINE_DETAIL);
            exciseTaxItemDetails.setUnitPrice(new BigDecimal(exciseTax));
            exciseTaxItemDetails.setQty(QBConstants.UNIT_PRICE);
            exciseTaxItemDetails.setBillableStatus(BillableStatusEnum.NOT_BILLABLE);
            totalExciseLine.setItemBasedExpenseLineDetail(exciseTaxItemDetails);
            lineList.add(totalExciseLine);

            purchaseOrder.setPOStatus(PurchaseOrderStatusEnum.OPEN);
            purchaseOrder.setLine(lineList);
            purchaseOrder.setDomain(QBConstants.DAILY_SYNC_EXPENSES);
            purchaseOrder.setGlobalTaxCalculation(GlobalTaxCalculationEnum.NOT_APPLICABLE);
            purchaseOrder.setTotalAmt(new BigDecimal(total));

            Account payableAccount = purchaseHelper.accountRef(blazeCompanyId, shopId, service);

            if (payableAccount != null)
                purchaseOrder.setAPAccountRef(accountHelper.getAccountRef(payableAccount));
            com.intuit.ipp.data.PurchaseOrder savedPurchaseOrder = service.add(purchaseOrder);
            LOG.info("SavedPurchaseOrder: " + savedPurchaseOrder.getId());

            if (savedPurchaseOrder.getId() != null) {
                totalSuccess++;
            }
        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while creating expenes: " + error.getMessage() + "Detail::: " + error.getDetail());
                ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId,
                        shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
            }
        }
        int totalFail = totalRecords - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(totalRecords, totalSuccess, totalFail, blazeCompanyId, endTime,
                startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.Expenses, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }


}
