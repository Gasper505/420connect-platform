package com.fourtwenty.quickbook.online.helperservices;


import com.fourtwenty.core.domain.models.company.CompoundTaxRate;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;


public class QBSalesByCategoryJournalEntry {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ItemHelper itemHelper;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;
    @Inject
    SalesReceiptHelper salesReceiptHelper;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSync quickbookSync;

    public void syncSaleByProductCategoryJEInQB(DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {

        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> transactions = null;
        Iterable<QuickbookSyncDetails> syncJournalEntryList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId, shopId,
                QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncSaleseceipt = Lists.newLinkedList(syncJournalEntryList);

        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncSaleseceipt.isEmpty()) {

            long beforeDate = DateTime.now().getMillis() - 8640000000l + 864000000l;
            transactions = transactionRepository.getBracketSalesQB(blazeCompanyId, shopId, beforeDate, startTime);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncSaleseceipt.isEmpty()) {

            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, startTime, endTime, shopId,
                    QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


        } else {
            QuickbookSyncDetails quickbookSyncSalesReceipt = syncSaleseceipt.getLast();
            endTime = quickbookSyncSalesReceipt.getEndTime();
            transactions = transactionRepository.getDailySalesforQB(blazeCompanyId, shopId, endTime, startTime);


        }

        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductBatch> batchHashMap = batchRepository.listAllAsMap(blazeCompanyId);
        HashMap<String, QBSalesByProductCategory.CategorySale> results = new HashMap<>();
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(blazeCompanyId, shopId);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(blazeCompanyId);


        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        Iterable<Product> products = productRepository.listAll(blazeCompanyId);
        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }
        if (transactions != null) {
            for (Transaction t : transactions) {

                int totalFinalCost = 0;
                for (OrderItem item : t.getCart().getItems()) {
                    if (item.getStatus() != OrderItem.OrderItemStatus.Refunded) {
                        totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
                    }
                }

                double avgDeliveryFee = 0;
                if (t.getCart().getItems().size() > 0) {
                    avgDeliveryFee = t.getCart().getDeliveryFee().doubleValue() / t.getCart().getItems().size();
                }
                double totalPreTax = 0;
                double cTotal = 0;
                List<String> visited = new ArrayList<>();
                for (OrderItem item : t.getCart().getItems()) {

                    double taxRate = 0d;

                    taxRate = t.getCart().getTax().doubleValue();
                    if (item.getTaxInfo() != null
                            && item.getTaxType() == TaxInfo.TaxType.Custom
                            && item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        taxRate = item.getTaxInfo().getTotalTax().doubleValue();
                    }
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PreTaxed) {
                        taxRate = 0;
                    }

                    Product p = productMap.get(item.getProductId());
                    if (p != null) {
                        totalPreTax += item.getCalcPreTax().doubleValue();
                    }

                    if (item.getStatus() != OrderItem.OrderItemStatus.Refunded) {
                        if (p != null) {

                            String catName = categoryMap.get(p.getCategoryId()).getName();
                            QBSalesByProductCategory.CategorySale categorySale = results.get(catName);
                            if (categorySale == null) {
                                categorySale = new QBSalesByProductCategory.CategorySale();
                                results.put(catName, categorySale);
                            }
                            // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                            if (!visited.contains(p.getCategoryId())) {
                                categorySale.transCount++;
                                visited.add(p.getCategoryId());
                            }


                            int finalCost = (int) (item.getFinalPrice().doubleValue() * 100);
                            double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                            double itemDiscount = item.getCost().subtract(item.getFinalPrice()).doubleValue();
                            double targetCartDiscount = t.getCart().getCalcCartDiscount().doubleValue() * ratio;
                            cTotal += targetCartDiscount;
                            // calculated postTax

                            categorySale.name = catName;
                            categorySale.retailValue += item.getCost().doubleValue();
                            categorySale.itemDiscounts += itemDiscount;
                            categorySale.deliveryFees += avgDeliveryFee;
                            categorySale.discounts += targetCartDiscount;
                            categorySale.totalPreTax += totalPreTax;

                            double expectedFinalCost = item.getCost().doubleValue() - itemDiscount;
                            if (NumberUtils.round(expectedFinalCost, 2) != NumberUtils.round(item.getFinalPrice().doubleValue(), 2)) {
                            }

                            double sales = NumberUtils.round(item.getFinalPrice().doubleValue() - targetCartDiscount, 5);


                            double postTax = NumberUtils.round(sales * taxRate, 6);
                            double totalExciseTax = 0;
                            totalExciseTax += item.getExciseTax().doubleValue();

                            double preALExciseTax = 0;
                            double preNALExciseTax = 0;
                            double postALExciseTax = 0;
                            double postNALExciseTax = 0;
                            if (item.getTaxResult() != null) {
                                preALExciseTax += item.getTaxResult().getTotalALExciseTax().doubleValue();
                                postALExciseTax += item.getTaxResult().getTotalALPostExciseTax().doubleValue();

                                preNALExciseTax += item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                                postNALExciseTax += item.getTaxResult().getTotalExciseTax().doubleValue();
                            }

                            categorySale.subTotals += item.getFinalPrice().doubleValue() - (preALExciseTax + preNALExciseTax);
                            double totalCityTax = 0;
                            double totalCountyTax = 0;
                            double totalStateTax = 0;
                            double totalFedTax = 0;
                            if (item.getTaxTable() != null) {
                                CompoundTaxTable taxTable = item.getTaxTable();
                                double curTax = 0d;
                                double subTotalWithTax = sales;
                                for (CompoundTaxRate curTaxRate : taxTable.getActivePostTaxes()) {
                                    if (curTaxRate.isActive()) {
                                        double productTax = 0d;
                                        if (curTaxRate.isCompound()) {
                                            subTotalWithTax += NumberUtils.round(curTax, 3);

                                            // reset curTax
                                            curTax = 0d;
                                            productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                            curTax += productTax;

                                        } else {
                                            productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                            curTax += productTax;
                                        }

                                        if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.City) {
                                            totalCityTax += productTax;
                                        }

                                        if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.County) {
                                            totalCountyTax += productTax;
                                        }

                                        if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.State) {
                                            totalStateTax += productTax;
                                        }

                                        if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.Federal) {
                                            totalFedTax += productTax;
                                        }
                                    }
                                }
                                postTax = NumberUtils.round(totalCityTax, 3) + NumberUtils.round(totalCountyTax, 3) + NumberUtils.round(totalStateTax, 3) + totalFedTax;
                                postTax = NumberUtils.round(postTax, 3);
                            }

                            categorySale.preALExciseTax += preALExciseTax;
                            categorySale.preNALExciseTax += preNALExciseTax;
                            categorySale.postALExciseTax += postALExciseTax;
                            categorySale.postNALExciseTax += postNALExciseTax;
                            categorySale.exciseTax += (preALExciseTax + preNALExciseTax + postALExciseTax + postNALExciseTax);
                            categorySale.cityTax += totalCityTax;
                            categorySale.countyTax += totalCountyTax;
                            categorySale.stateTax += totalStateTax;
                            categorySale.tax += postTax;
                            categorySale.sales += sales;


                            // caclulate cost of goods
                            boolean calculated = false;
                            double cogs = 0;
                            if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                                // if prepackage is set, use prepackage
                                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                                if (prepackageProductItem != null) {
                                    ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());

                                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                    if (prepackage != null && targetBatch != null) {
                                        calculated = true;

                                        BigDecimal unitValue = prepackage.getUnitValue();
                                        if (unitValue == null || unitValue.doubleValue() == 0) {
                                            ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                            unitValue = weightTolerance.getUnitValue();
                                        }
                                        // calculate the total quantity based on the prepackage value
                                        cogs += calcCOGS(item.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                    }
                                }
                            } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                                // otherwise, use quantity logs
                                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                                    if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                        ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                        if (targetBatch != null) {
                                            calculated = true;
                                            cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                        }
                                    }
                                }
                            }

                            if (!calculated) {
                                double unitCost = getUnitCost(p, recentBatchMap, productsByCompanyLinkId);
                                cogs = unitCost * item.getQuantity().doubleValue();
                            }

                            categorySale.cogs += cogs;

                        }
                    }


                }

                int cartDiscount = (int) (t.getCart().getCalcCartDiscount().doubleValue() * 100);
                int myCTotal = (int) (NumberUtils.round(cTotal, 2) * 100);

            }
        }


        int counter = 0;
        List<QBSalesByProductCategory.CategorySale> categorySaleList = new ArrayList<QBSalesByProductCategory.CategorySale>();
        for (String name : results.keySet()) {
            counter++;
            QBSalesByProductCategory.CategorySale categorySale = results.get(name);
            categorySaleList.add(categorySale);


        }
        if (counter > 0) {
            Iterator<QBSalesByProductCategory.CategorySale> categorySaleIterator = categorySaleList.iterator();
            createJESalesByProduct(service, categorySaleIterator, blazeCompanyId, shopId, endTime, categorySaleList.size(), entities);

        }
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public List<JournalEntry> convertSalesByCategoryInJE(DataService service, Iterator<QBSalesByProductCategory.CategorySale> categorySaleList,
                                                         QuickbookCustomEntities entities, String blazeCompanyId, String shopId) {

        List<JournalEntry> resultJournalEntryList = new ArrayList<>();
        int counter = 0;
        while ((counter < 30) && (categorySaleList.hasNext())) {
            QBSalesByProductCategory.CategorySale categorySale = categorySaleList.next();
            JournalEntry qbJournalEntry = new JournalEntry();
            List<Line> linesList = qbLinesList(categorySale, service, entities, blazeCompanyId, shopId);
            TxnTaxDetail txnTaxDetail = new TxnTaxDetail();
            qbJournalEntry.setLine(linesList);

            qbJournalEntry.setTotalAmt(new BigDecimal(categorySale.subTotals));
            if (categorySale.name.length() < 16) {
                qbJournalEntry.setDocNumber(categorySale.name);
            } else {
                qbJournalEntry.setDocNumber(categorySale.name.substring(0, 15));
            }

            qbJournalEntry.setTxnTaxDetail(txnTaxDetail);


            resultJournalEntryList.add(qbJournalEntry);
            counter++;

        }


        return resultJournalEntryList;
    }

    public void createJESalesByProduct(DataService service, Iterator<QBSalesByProductCategory.CategorySale> categorySaleList, String blazecompanyId,
                                       String shopId, long endTime, int total, QuickbookCustomEntities entities) {

        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int totalSuccess = 0;
        int count = 0;

        while (categorySaleList.hasNext()) {
            List<JournalEntry> qbJournalEntry = convertSalesByCategoryInJE(service, categorySaleList, entities, blazecompanyId, shopId);
            GzipHttpOutputInterceptor.LOG.info("JournalEntry  Sales by product category Batch Size: " + qbJournalEntry.size());
            batchOperation = new BatchOperation();
            for (JournalEntry journalEntry : qbJournalEntry) {
                count++;
                batchOperation.addEntity(journalEntry, OperationEnum.CREATE, "bID" + count);
                GzipHttpOutputInterceptor.LOG.info("create JournalEntry  sales by product category: " + journalEntry.getSyncToken());


            }
            try {
                //Execute Batch
                service.executeBatch(batchOperation);
                Thread.sleep(2000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            GzipHttpOutputInterceptor.LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() +
                                    " error code: " + error.getCode());

                        }

                    }

                    JournalEntry journalEntry = (JournalEntry) batchOperation.getEntity(bId);
                    if (journalEntry != null) {
                        totalSuccess++;

                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling Sales by category journal entry:: " + error.getMessage() + "Details:: " + error.getDetail() +
                            "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazecompanyId, endTime, startTime, shopId,
                QuickbookSyncDetails.QuickbookEntityType.SalesByproductJournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }


    //Add Line item of carts
    public List<Line> qbLinesList(QBSalesByProductCategory.CategorySale categorySale, DataService service, QuickbookCustomEntities entities,
                                  String blazeCompanyId, String shopId) {

        List<Line> linesList = new ArrayList<Line>();
        //Credit Line Item
        try {
            Line lineObject = new Line();
            lineObject.setAmount(new BigDecimal(categorySale.cogs));
            lineObject.setDetailType(LineDetailTypeEnum.JOURNAL_ENTRY_LINE_DETAIL);
            JournalEntryLineDetail journalEntryLineDetail = new JournalEntryLineDetail();
            journalEntryLineDetail.setPostingType(PostingTypeEnum.CREDIT);
            Account creditCardBankAccount = AccountHelper.getAccount(service, entities.getInventory());
            journalEntryLineDetail.setAccountRef(accountHelper.getAccountRef(creditCardBankAccount));
            lineObject.setJournalEntryLineDetail(journalEntryLineDetail);
            linesList.add(lineObject);

            //Debit Line Item
            Line debitLine = new Line();
            debitLine.setAmount(new BigDecimal(categorySale.cogs));
            debitLine.setDetailType(LineDetailTypeEnum.JOURNAL_ENTRY_LINE_DETAIL);
            JournalEntryLineDetail journalEntryLineDetailDebit = new JournalEntryLineDetail();
            journalEntryLineDetailDebit.setPostingType(PostingTypeEnum.DEBIT);
            Account debitAccount = AccountHelper.getAccount(service, entities.getSuppliesandMaterials());
            journalEntryLineDetailDebit.setAccountRef(accountHelper.getAccountRef(debitAccount));
            debitLine.setJournalEntryLineDetail(journalEntryLineDetailDebit);
            linesList.add(debitLine);

        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create vendor:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                //Get Access token if Access token Expires after an hour
                if (error.getCode().equals(QBConstants.ERRORCODE)) {

                    LOG.info("Inside 3200 code");
                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId,
                            shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                }
            }
        }
        return linesList;
    }

}
