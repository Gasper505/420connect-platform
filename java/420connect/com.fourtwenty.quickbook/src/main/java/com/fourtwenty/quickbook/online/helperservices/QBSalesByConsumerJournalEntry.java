package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public class QBSalesByConsumerJournalEntry {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    public void syncSalesByConsumerJEInQB(DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {
        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> results = null;
        Iterable<QuickbookSyncDetails> syncJournalEntryList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumerJournalEntry, QBConstants.QUICKBOOK_ONLINE);


        LinkedList<QuickbookSyncDetails> syncSaleseceipt = Lists.newLinkedList(syncJournalEntryList);

        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncSaleseceipt.isEmpty()) {

            long beforeDate = DateTime.now().getMillis() - 8640000000l + 864000000l;
            results = transactionRepository.getBracketSalesQB(blazeCompanyId, shopId, beforeDate, startTime);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncSaleseceipt.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, startTime, endTime, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumerJournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);
        } else {
            QuickbookSyncDetails quickbookSyncSalesReceipt = syncSaleseceipt.getLast();
            endTime = quickbookSyncSalesReceipt.getEndTime();
            results = transactionRepository.getDailySalesforQB(blazeCompanyId, shopId, endTime, startTime);


        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(blazeCompanyId, shopId);
        // fetch the data
        Iterable<Product> products = productRepository.list(blazeCompanyId);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(blazeCompanyId, shopId);
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(blazeCompanyId);
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(blazeCompanyId);

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }


        RevenueSummary adultUseRevenuesSummary = new RevenueSummary();
        RevenueSummary mmicRevenuesSummary = new RevenueSummary();
        RevenueSummary otherSummary = new RevenueSummary();
        RevenueSummary thirdPartyRevenuesSummary = new RevenueSummary();

        adultUseRevenuesSummary.name = "Adult Use Revenues";
        mmicRevenuesSummary.name = "Medicinal MMIC Revenues";
        thirdPartyRevenuesSummary.name = "Medicinal Third Party Revenues";
        otherSummary.name = "Other";

        Shop shop = shopRepository.get(blazeCompanyId, shopId);
        // use federal

        boolean useFederal = false;
        if (shop.getTaxInfo() != null && shop.getTaxInfo().getFederalTax().doubleValue() > 0) {
            useFederal = true;
        }

        int count = 0;
        if (results != null) {
            for (Transaction ts : results) {
                count++;
                double cannabisRevenue = 0d;
                double nonCannabisRevenue = 0d;
                double discounts = 0;
                double exciseTax = 0;
                double cityTax = 0;
                double countyTax = 0;
                double stateTax = 0;
                double federalTax = 0;
                double totalTax = 0;
                double deliveryFees = 0;
                double grossReceipts = 0;
                double totalCogs = 0;
                double numOfVisits = 1;
                double cannabisDiscounts = 0d;
                double nonCananbisDiscounts = 0d;
                double grossRevenue = 0d;


                RevenueSummary curSummary = otherSummary;
                ConsumerType consumerType = ConsumerType.Other;
                if (ts.getCart().getTaxTable() != null) {
                    CompoundTaxTable taxTable = ts.getCart().getTaxTable();
                    consumerType = taxTable.getConsumerType();
                } else {
                    if (ts.getCart().getConsumerType() != null) {
                        consumerType = ts.getCart().getConsumerType();
                    }
                }

                if (consumerType == ConsumerType.MedicinalThirdParty) {
                    curSummary = thirdPartyRevenuesSummary;
                } else if (consumerType == ConsumerType.MedicinalState) {
                    curSummary = mmicRevenuesSummary;
                } else if (consumerType == ConsumerType.AdultUse) {
                    curSummary = adultUseRevenuesSummary;
                }


                grossReceipts = ts.getCart().getTotal().doubleValue();
                discounts = ts.getCart().getTotalDiscount().doubleValue();
                deliveryFees = ts.getCart().getDeliveryFee().doubleValue();

                if (ts.getCart().getTaxResult() != null) {
                    TaxResult taxResult = ts.getCart().getTaxResult();
                    exciseTax = taxResult.getTotalExciseTax().doubleValue() + taxResult.getTotalALPostExciseTax().doubleValue();
                    cityTax = NumberUtils.round(taxResult.getTotalCityTax().doubleValue(), 2);
                    countyTax = NumberUtils.round(taxResult.getTotalCountyTax().doubleValue(), 2);
                    stateTax = NumberUtils.round(taxResult.getTotalStateTax().doubleValue(), 2);
                    federalTax = NumberUtils.round(taxResult.getTotalFedTax().doubleValue(), 2);
                    totalTax = NumberUtils.round(taxResult.getTotalPostCalcTax().doubleValue(), 2);
                }
                totalTax = ts.getCart().getTotalCalcTax().doubleValue() - ts.getCart().getTotalPreCalcTax().doubleValue();
                if (totalTax < 0) {
                    totalTax = 0;
                }

                int totalFinalCost = 0;
                for (OrderItem item : ts.getCart().getItems()) {
                    if (item.getStatus() != OrderItem.OrderItemStatus.Refunded) {
                        totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
                    }
                }

                // Calculate COGs & cannabis revenue
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                        continue; //
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    boolean cannabis = false;


                    int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                    double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                    double targetCartDiscount = ts.getCart().getCalcCartDiscount().doubleValue() * ratio;

                    if (product != null && categoryHashMap.get(product.getCategoryId()) != null) {
                        ProductCategory category = categoryHashMap.get(product.getCategoryId());

                        if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                                || (product.getCannabisType() != Product.CannabisType.CBD
                                && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                                && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                            cannabis = true;
                        }


                        // now calculate COGs
                        // calculate cost of goods
                        boolean calculated = false;
                        if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                            // if prepackage is set, use prepackage
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null && targetBatch != null) {
                                    calculated = true;

                                    BigDecimal unitValue = prepackage.getUnitValue();
                                    if (unitValue == null || unitValue.doubleValue() == 0) {
                                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                        unitValue = weightTolerance.getUnitValue();
                                    }
                                    // calculate the total quantity based on the prepackage value
                                    totalCogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                }
                            }
                        } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                            // otherwise, use quantity logs
                            for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                    ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                                    if (targetBatch != null) {
                                        calculated = true;
                                        totalCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                    }
                                }
                            }
                        }

                        if (!calculated) {
                            double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                            totalCogs = unitCost * orderItem.getQuantity().doubleValue();
                        }
                    }

                    if (cannabis) {
                        cannabisRevenue += orderItem.getCost().doubleValue();
                        cannabisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                    } else {
                        nonCannabisRevenue += orderItem.getCost().doubleValue();
                        nonCananbisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                    }
                }

                // apply current summary
                curSummary.grossReceipt += grossReceipts;
                curSummary.cannabisRevenue += cannabisRevenue;
                curSummary.nonCannabisRevenue += nonCannabisRevenue;
                curSummary.cannabisDiscounts += cannabisDiscounts;
                curSummary.nonCannabisDiscounts += nonCananbisDiscounts;
                curSummary.discount += discounts;
                curSummary.grossRevenue += (cannabisRevenue + nonCannabisRevenue - discounts);
                curSummary.exciseTax += exciseTax;
                curSummary.cityTax += cityTax;
                curSummary.countyTax += countyTax;
                curSummary.stateTax += stateTax;
                curSummary.fedralTax += federalTax;
                curSummary.totalTax += totalTax;
                curSummary.cogs += totalCogs;
                curSummary.deliveryFees += deliveryFees;
                curSummary.netProfit += (grossReceipts - totalCogs);
                curSummary.numberOfVisits += numOfVisits;

            }
        }


        if (count > 0) {
            List<RevenueSummary> revenueSummaryList = new ArrayList<RevenueSummary>();
            revenueSummaryList.add(thirdPartyRevenuesSummary);
            revenueSummaryList.add(adultUseRevenuesSummary);
            revenueSummaryList.add(otherSummary);
            revenueSummaryList.add(mmicRevenuesSummary);

            Iterator<RevenueSummary> revenueSummaryIterator = revenueSummaryList.iterator();
            createJESalesByConsumer(service, revenueSummaryIterator, blazeCompanyId, shopId, endTime, revenueSummaryList.size(), entities);
        }
    }


    public class RevenueSummary {
        public String name = "";
        double revenue = 0d;
        public double cannabisRevenue = 0d;
        public double nonCannabisRevenue = 0d;
        public double cannabisDiscounts = 0d;
        public double nonCannabisDiscounts = 0d;
        public double grossRevenue = 0d;
        public double discount = 0d;
        public double exciseTax = 0d;
        public double cityTax = 0d;
        public double countyTax = 0d;
        public double stateTax = 0d;
        public double fedralTax = 0d;
        public double totalTax = 0d;
        public double grossReceipt = 0d;
        public double cogs = 0d;
        public double netProfit = 0d;
        public double numberOfVisits = 0d;
        public double deliveryFees = 0d;
    }


    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }


    public List<JournalEntry> convertSalesByConsumerInJE(DataService service, Iterator<RevenueSummary> revenueSummaryIterator, QuickbookCustomEntities entities, String blazeCompanyid, String shopId) {
        List<JournalEntry> resultJournalEntryList = new ArrayList<>();
        int counter = 0;
        while ((counter < 30) && (revenueSummaryIterator.hasNext())) {
            RevenueSummary revenueSummary = revenueSummaryIterator.next();
            JournalEntry qbJournalEntry = new JournalEntry();
            List<Line> linesList = qbLinesList(revenueSummary, service, entities, blazeCompanyid, shopId);
            TxnTaxDetail txnTaxDetail = new TxnTaxDetail();
            qbJournalEntry.setLine(linesList);
            qbJournalEntry.setTotalAmt(new BigDecimal(revenueSummary.cogs));
            if (revenueSummary.name.length() < 16)
                qbJournalEntry.setDocNumber(revenueSummary.name);
            else
                qbJournalEntry.setDocNumber(revenueSummary.name.substring(0, 15));


            qbJournalEntry.setTxnTaxDetail(txnTaxDetail);
            resultJournalEntryList.add(qbJournalEntry);
            counter++;

        }


        return resultJournalEntryList;
    }

    public void createJESalesByConsumer(DataService service, Iterator<RevenueSummary> revenueSummaryIterator, String blazecompanyId, String shopId, long endTime, int total, QuickbookCustomEntities entities) {
        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int totalSuccess = 0;
        int count = 0;

        while (revenueSummaryIterator.hasNext()) {
            List<JournalEntry> qbJournalEntry = convertSalesByConsumerInJE(service, revenueSummaryIterator, entities, blazecompanyId, shopId);
            GzipHttpOutputInterceptor.LOG.info("JournalEntry  Sales by consumer Batch Size: " + qbJournalEntry.size());
            batchOperation = new BatchOperation();
            for (JournalEntry journalEntry : qbJournalEntry) {
                count++;
                batchOperation.addEntity(journalEntry, OperationEnum.CREATE, "bID" + count);
                GzipHttpOutputInterceptor.LOG.info("create JournalEntry sales by consumer: " + journalEntry.getSyncToken());


            }
            try {
                //Execute Batch
                service.executeBatch(batchOperation);
                Thread.sleep(2000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            GzipHttpOutputInterceptor.LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());
                        }

                    }

                    JournalEntry journalEntry = (JournalEntry) batchOperation.getEntity(bId);
                    if (journalEntry != null) {
                        totalSuccess++;

                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling Create Qb Sales by consumer journal entry:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazecompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumerJournalEntry, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }


    //Add Line item of carts
    public List<Line> qbLinesList(RevenueSummary revenueSummary, DataService service, QuickbookCustomEntities entities, String blazeCompanyId, String shopId) {
        List<Line> linesList = new ArrayList<Line>();
        try {
            //Credit Line Item
            Line lineObject = new Line();
            lineObject.setAmount(new BigDecimal(revenueSummary.cogs));
            lineObject.setDetailType(LineDetailTypeEnum.JOURNAL_ENTRY_LINE_DETAIL);
            JournalEntryLineDetail journalEntryLineDetail = new JournalEntryLineDetail();
            journalEntryLineDetail.setPostingType(PostingTypeEnum.CREDIT);
            Account creditCardBankAccount = AccountHelper.getAccount(service, entities.getInventory());
            journalEntryLineDetail.setAccountRef(accountHelper.getAccountRef(creditCardBankAccount));
            lineObject.setJournalEntryLineDetail(journalEntryLineDetail);
            linesList.add(lineObject);

            //Debit Line Item
            Line debitLine = new Line();
            debitLine.setAmount(new BigDecimal(revenueSummary.cogs));
            debitLine.setDetailType(LineDetailTypeEnum.JOURNAL_ENTRY_LINE_DETAIL);
            JournalEntryLineDetail journalEntryLineDetailDebit = new JournalEntryLineDetail();
            journalEntryLineDetailDebit.setPostingType(PostingTypeEnum.DEBIT);
            Account debitAccount = AccountHelper.getAccount(service, entities.getSuppliesandMaterials());
            journalEntryLineDetailDebit.setAccountRef(accountHelper.getAccountRef(debitAccount));
            debitLine.setJournalEntryLineDetail(journalEntryLineDetailDebit);
            linesList.add(debitLine);

        } catch (FMSException e) {
            for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                LOG.info("Error while calling Create QB sales by consumer type journal entry:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                //Get Access token if Access token Expires after an hour
                if (error.getCode().equals(QBConstants.ERRORCODE)) {

                    LOG.info("Inside 3200 code");
                    ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId,
                            shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                    BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                    service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                }
            }
        }


        return linesList;

    }


}
