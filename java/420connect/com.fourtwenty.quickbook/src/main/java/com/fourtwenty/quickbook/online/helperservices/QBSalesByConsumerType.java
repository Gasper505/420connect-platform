package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.reporting.gather.impl.transaction.TaxSummaryGatherer;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.eclipse.jetty.http.HttpParser.LOG;

public class QBSalesByConsumerType {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;
    @Inject
    SalesReceiptHelper salesReceiptHelper;
    @Inject
    ItemHelper itemHelper;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSync quickbookSync;

    public void salesByConsumerInQB(DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {

        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> results = null;
        Iterable<QuickbookSyncDetails> syncSalesReceiptList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId,
                shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncSaleseceipt = Lists.newLinkedList(syncSalesReceiptList);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncSaleseceipt.isEmpty()) {
            long beforeDate = DateTime.now().getMillis() - 8640000000l + 864000000l;
            results = transactionRepository.getBracketSalesQB(blazeCompanyId, shopId, beforeDate, startTime);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncSaleseceipt.isEmpty()) {
            //Entry in custom entities table
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, startTime, endTime, shopId,
                    QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

        } else {
            QuickbookSyncDetails quickbookSyncSalesReceipt = syncSaleseceipt.getLast();
            endTime = quickbookSyncSalesReceipt.getEndTime();
            results = transactionRepository.getDailySalesforQB(blazeCompanyId, shopId, endTime, startTime);


        }


        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(blazeCompanyId, shopId);

        // fetch the data
        Iterable<Product> products = productRepository.list(blazeCompanyId);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(blazeCompanyId, shopId);
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(blazeCompanyId);
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(blazeCompanyId);

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }


        List<Transaction> transactions = Lists.newArrayList(results);


        RevenueSummary totalRevenuesSummary = new RevenueSummary();
        RevenueSummary adultUseRevenuesSummary = new RevenueSummary();
        RevenueSummary mmicRevenuesSummary = new RevenueSummary();
        RevenueSummary otherSummary = new RevenueSummary();
        RevenueSummary thirdPartyRevenuesSummary = new RevenueSummary();

        totalRevenuesSummary.name = "Total Revenue";
        adultUseRevenuesSummary.name = "Adult Use Revenues";
        mmicRevenuesSummary.name = "Medicinal MMIC Revenues";
        thirdPartyRevenuesSummary.name = "Medicinal Third Party Revenues";
        otherSummary.name = "Other";

        Shop shop = shopRepository.get(blazeCompanyId, shopId);
        // use federal

        boolean useFederal = false;
        if (shop.getTaxInfo() != null && shop.getTaxInfo().getFederalTax().doubleValue() > 0) {
            useFederal = true;
        }
        int factor = 1;
        int count = 0;
        for (Transaction ts : transactions) {
            count++;
            double cannabisRevenue = 0d;
            double nonCannabisRevenue = 0d;
            double discounts = 0;
            double exciseTax = 0;
            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;
            double federalTax = 0;
            double totalTax = 0;
            double deliveryFees = 0;
            double creditCardFees = 0d;
            double grossReceipts = 0;
            double totalCogs = 0;
            double numOfVisits = 1;
            double cannabisDiscounts = 0d;
            double nonCananbisDiscounts = 0d;
            double grossRevenue = 0d;
            double afterTaxDiscount = 0d;
            double preALExciseTax = 0.0;
            double preNALExciseTax = 0.0;
            double postALExciseTax = 0.0;
            double postNALExciseTax = 0.0;

            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            RevenueSummary curSummary = otherSummary;
            ConsumerType consumerType = ConsumerType.Other;
            if (ts.getCart().getTaxTable() != null) {
                CompoundTaxTable taxTable = ts.getCart().getTaxTable();
                consumerType = taxTable.getConsumerType();
            } else {
                if (ts.getCart().getConsumerType() != null) {
                    consumerType = ts.getCart().getConsumerType();
                }
            }

            if (consumerType == ConsumerType.MedicinalThirdParty) {
                curSummary = thirdPartyRevenuesSummary;
            } else if (consumerType == ConsumerType.MedicinalState) {
                curSummary = mmicRevenuesSummary;
            } else if (consumerType == ConsumerType.AdultUse) {
                curSummary = adultUseRevenuesSummary;
            }


            grossReceipts = ts.getCart().getTotal().doubleValue();
            discounts = ts.getCart().getTotalDiscount().doubleValue();
            deliveryFees = ts.getCart().getDeliveryFee().doubleValue();
            if (ts.getCart().getAppliedAfterTaxDiscount() != null && ts.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount().doubleValue();
            }

            if (ts.getCart().isEnableCreditCardFee() && ts.getCart().getCreditCardFee().doubleValue() > 0) {
                creditCardFees = ts.getCart().getCreditCardFee().doubleValue();
            }

            if (ts.getCart().getTaxResult() != null) {
                TaxResult taxResult = ts.getCart().getTaxResult();
                exciseTax = taxResult.getTotalExciseTax().doubleValue() + taxResult.getTotalALPostExciseTax().doubleValue();

                cityTax = NumberUtils.round(taxResult.getTotalCityTax().doubleValue(), 2);
                countyTax = NumberUtils.round(taxResult.getTotalCountyTax().doubleValue(), 2);
                stateTax = NumberUtils.round(taxResult.getTotalStateTax().doubleValue(), 2);
                federalTax = NumberUtils.round(taxResult.getTotalFedTax().doubleValue(), 2);
                totalTax = NumberUtils.round(taxResult.getTotalPostCalcTax().doubleValue(), 2);


                preALExciseTax = ts.getCart().getTaxResult().getTotalALExciseTax().doubleValue();
                postALExciseTax = ts.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();

                preNALExciseTax = ts.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue();
                postNALExciseTax = ts.getCart().getTaxResult().getTotalExciseTax().doubleValue();
            }
            totalTax = NumberUtils.round(cityTax, 3) + NumberUtils.round(countyTax, 3) +
                    NumberUtils.round(stateTax, 3) + federalTax;

            int totalFinalCost = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if ((ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && item.getStatus() == OrderItem.OrderItemStatus.Refunded)) {
                    totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
                }
            }

            // Calculate COGs & cannabis revenue
            for (OrderItem orderItem : ts.getCart().getItems()) {
                if (!(ts.getTransType() == Transaction.TransactionType.Sale
                        || (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded))) {
                    continue;
                }
                Product product = productMap.get(orderItem.getProductId());
                boolean cannabis = false;


                int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                double targetCartDiscount = ts.getCart().getCalcCartDiscount().doubleValue() * ratio;

                if (product != null && categoryHashMap.get(product.getCategoryId()) != null) {
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());

                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        cannabis = true;
                    }


                    // now calccreditCardFeesulate COGs
                    // calculate cost of goods
                    boolean calculated = false;
                    if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        // if prepackage is set, use prepackage
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null && targetBatch != null) {
                                calculated = true;

                                BigDecimal unitValue = prepackage.getUnitValue();
                                if (unitValue == null || unitValue.doubleValue() == 0) {
                                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                    unitValue = weightTolerance.getUnitValue();
                                }
                                // calculate the total quantity based on the prepackage value
                                totalCogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                            }
                        }
                    } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    totalCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                }
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                        totalCogs = unitCost * orderItem.getQuantity().doubleValue();
                    }
                }

                if (cannabis) {
                    cannabisRevenue += orderItem.getCost().doubleValue();
                    cannabisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                } else {
                    nonCannabisRevenue += orderItem.getCost().doubleValue();
                    nonCananbisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                }
            }

            double cannabisRevenueFactor = cannabisRevenue * factor;
            double nonCannabisRevenueFactor = nonCannabisRevenue * factor;
            double discountFactor = discounts * factor;
            double grossReceiptsFactor = grossReceipts * factor;
            double totalCogsFactor = totalCogs * factor;


            // apply current summary
            curSummary.cannabisRevenue += cannabisRevenueFactor;
            curSummary.nonCannabisRevenue += nonCannabisRevenueFactor;
            curSummary.cannabisDiscounts += cannabisDiscounts * factor;
            curSummary.nonCannabisDiscounts += nonCananbisDiscounts * factor;
            curSummary.discount += discountFactor;
            curSummary.afterTaxDiscount += afterTaxDiscount * factor;
            curSummary.grossRevenue += (cannabisRevenueFactor + nonCannabisRevenueFactor - discountFactor);
            curSummary.exciseTax += (preALExciseTax + preNALExciseTax + postALExciseTax + postNALExciseTax) * factor;
            curSummary.cityTax += cityTax * factor;
            curSummary.countyTax += countyTax * factor;
            curSummary.stateTax += stateTax * factor;
            curSummary.fedralTax += federalTax * factor;
            curSummary.totalTax += totalTax * factor;
            curSummary.cogs += totalCogsFactor;
            curSummary.deliveryFees += deliveryFees * factor;
            curSummary.creditCardFees += creditCardFees * factor;
            curSummary.netProfit += (grossReceiptsFactor - totalCogsFactor);
            curSummary.numberOfVisits += numOfVisits;
            curSummary.preNALExciseTax += preNALExciseTax * factor;
            curSummary.preALExciseTax += preALExciseTax * factor;
            curSummary.grossReceipt += (grossRevenue - (preALExciseTax + preNALExciseTax) + deliveryFees + totalTax + exciseTax) * factor;
        }


        if (count > 0) {
            List<RevenueSummary> revenueSummaryList = new ArrayList<RevenueSummary>();
            revenueSummaryList.add(thirdPartyRevenuesSummary);
            revenueSummaryList.add(adultUseRevenuesSummary);
            revenueSummaryList.add(otherSummary);
            revenueSummaryList.add(mmicRevenuesSummary);
            List<String> namelist = new ArrayList<String>();
            namelist.add(thirdPartyRevenuesSummary.name);
            namelist.add(adultUseRevenuesSummary.name);
            namelist.add(otherSummary.name);
            namelist.add(mmicRevenuesSummary.name);
            createProductByConsumer(service, namelist, blazeCompanyId, shopId, entities);
            createSalesByConsumerInQB(service, revenueSummaryList, blazeCompanyId, shopId, endTime);
        }
    }


    private String prepareAmount(double amount) {
        return String.format("$%.2f", amount);
    }

    private class RevenueSummary {
        String name = "";
        double revenue = 0d;
        double cannabisRevenue = 0d;
        double nonCannabisRevenue = 0d;
        double cannabisDiscounts = 0d;
        double nonCannabisDiscounts = 0d;
        double grossRevenue = 0d;
        double discount = 0d;
        double afterTaxDiscount = 0d;
        double exciseTax = 0d;
        double cityTax = 0d;
        double countyTax = 0d;
        double stateTax = 0d;
        double fedralTax = 0d;
        double totalTax = 0d;
        double grossReceipt = 0d;
        double cogs = 0d;
        double netProfit = 0d;
        double numberOfVisits = 0d;
        double deliveryFees = 0d;
        double creditCardFees = 0d;
        public double preALExciseTax = 0.0;
        public double preNALExciseTax = 0.0;
    }


    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public void createProductByConsumer(DataService service, List<String> revenueSummaryList, String blazecompanyId,
                                        String shopId, QuickbookCustomEntities entities) {

        BatchOperation batchOperation = null;
        List<Item> itemList = convertProductCategorybyConsumerToQb(service, revenueSummaryList, entities);
        int counter = 0;
        int parts = itemList.size() / 30;
        int remainder = itemList.size() % 30;
        if (remainder > 0) {
            parts = parts + 1;
        }
        for (int i = 0; i < parts; i++) {
            List<Item> itemBatch = itemHelper.getItemBatch(itemList, i * 30);
            GzipHttpOutputInterceptor.LOG.info(" Item Size:   " + itemBatch.size());
            batchOperation = new BatchOperation();

            try {
                for (Item item : itemBatch) {
                    counter++;
                    QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId,
                            QuickbookEntity.SyncStrategy.CosumerProductRef, item.getName());

                    if (quickbookEntity == null) {
                        batchOperation.addEntity(item, OperationEnum.CREATE, "CategoryBatch" + counter);
                    }

                }

                service.executeBatch(batchOperation);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            GzipHttpOutputInterceptor.LOG.info("errror message:" + error.getMessage() + "error Details: " +
                                    error.getDetail() + " error code: " + error.getCode());

                        }

                    }

                    Item singleItem = (Item) batchOperation.getEntity(bId);
                    GzipHttpOutputInterceptor.LOG.info("Result Item: " + singleItem);
                    if (singleItem != null) {
                        quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.CosumerProductRef,
                                singleItem.getId(), singleItem.getName());
                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Create QBsale Items:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        GzipHttpOutputInterceptor.LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while batch Create:");
            }
        }
    }

    public static List<Item> convertProductCategorybyConsumerToQb(DataService service, List<String> revenueSummaryList,
                                                                  QuickbookCustomEntities entities) {

        List<Item> itemList = new ArrayList<Item>();
        for (String revenueSummary : revenueSummaryList) {
            Item item = new Item();
            item.setName(revenueSummary);
            item.setActive(true);
            item.setTaxable(false);
            item.setUnitPrice(QBConstants.UNIT_PRICE);
            item.setType(ItemTypeEnum.NON_INVENTORY);
            try {
                Account incomeAccount = AccountHelper.getAccount(service, entities.getSales());
                item.setIncomeAccountRef(AccountHelper.getAccountRef(incomeAccount));
                item.setPurchaseCost(QBConstants.UNIT_PRICE);
                Account expenseAccount = AccountHelper.getAccount(service, entities.getSuppliesandMaterials());
                item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
                Account assetAccount = AccountHelper.getAccount(service, entities.getInventory());
                item.setAssetAccountRef(AccountHelper.getAccountRef(assetAccount));
                item.setTrackQtyOnHand(false);
                itemList.add(item);

            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Create Category Item: " + error.getMessage() +
                            "Detail::: " + error.getDetail());
                }

            } catch (Exception e) {
                e.printStackTrace();
                GzipHttpOutputInterceptor.LOG.info("Error while creating  category Items: ");
            }
        }

        return itemList;

    }


    public List<SalesReceipt> convertSalesByConsumerInQB(DataService service, List<RevenueSummary> RevenueSummaryList,
                                                         String blazecompanyId, String shopId) {

        List<SalesReceipt> salesReceiptList = new ArrayList<SalesReceipt>();
        for (RevenueSummary revenueSummary : RevenueSummaryList) {
            SalesReceipt qbSalesReceipt = new SalesReceipt();

            List<Line> linesList = new ArrayList<Line>();
            Line lineObject = new Line();
            lineObject.setAmount(new BigDecimal(revenueSummary.cannabisRevenue + revenueSummary.nonCannabisRevenue - (revenueSummary.preALExciseTax +
                    revenueSummary.preNALExciseTax)));

            lineObject.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
            SalesItemLineDetail salesItemLineDetail1 = new SalesItemLineDetail();

            QuickbookEntity quickbookEntityCategoryRef = quickbookEntityRepository.getQbEntityByName
                    (blazecompanyId, shopId, QuickbookEntity.SyncStrategy.CosumerProductRef, revenueSummary.name);

            if (quickbookEntityCategoryRef != null) {

                LOG.info("Reference: " + quickbookEntityCategoryRef.getProductRef());
                ReferenceType categoryItemref = new ReferenceType();
                categoryItemref.setName(revenueSummary.name);
                categoryItemref.setValue(quickbookEntityCategoryRef.getProductRef());
                salesItemLineDetail1.setItemRef(categoryItemref);
                salesItemLineDetail1.setQty(QBConstants.UNIT_PRICE);
                salesItemLineDetail1.setUnitPrice(new BigDecimal(revenueSummary.cannabisRevenue + revenueSummary.nonCannabisRevenue -
                        (revenueSummary.preALExciseTax + revenueSummary.preNALExciseTax)));


                //apply Sales tax
                ReferenceType taxcode = new ReferenceType();
                taxcode.setValue(QBConstants.TAX);

                salesItemLineDetail1.setTaxCodeRef(taxcode);
                lineObject.setSalesItemLineDetail(salesItemLineDetail1);
                lineObject.setDescription(revenueSummary.name);
                linesList.add(lineObject);

                //Discount Amount
                Line lineDetails = new Line();
                lineDetails.setDetailType(LineDetailTypeEnum.DISCOUNT_LINE_DETAIL);
                DiscountLineDetail discountLineDetail = new DiscountLineDetail();
                discountLineDetail.setPercentBased(false);
                lineDetails.setAmount(new BigDecimal(revenueSummary.discount));
                lineDetails.setDiscountLineDetail(discountLineDetail);
                linesList.add(lineDetails);

                //Shipping
                QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
                SalesItemLineDetail deliveryItemLineDetail1 = new SalesItemLineDetail();
                ReferenceType deliveryItemref = new ReferenceType();
                QuickbookEntity deliveryEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId,
                        QuickbookEntity.SyncStrategy.ProductRef, QBConstants.DELIVERY_CHARGE.trim());

                GzipHttpOutputInterceptor.LOG.info("quickbookenity:" + deliveryEntity);
                if (deliveryEntity == null) {
                    Item deliveryItem = ItemHelper.createDeliveryItem(service, accountType);

                    if (deliveryItem != null) {

                        quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                                deliveryItem.getId(), QBConstants.DELIVERY_CHARGE.trim());
                        GzipHttpOutputInterceptor.LOG.info("deliveryItem: " + deliveryItem.getId());
                        deliveryItemref.setName(deliveryItem.getName());
                        deliveryItemref.setValue(deliveryItem.getId());
                        deliveryItemLineDetail1.setItemRef(deliveryItemref);
                    }

                } else {
                    if (deliveryEntity.getProductRef() != null) {
                        deliveryItemref.setName(deliveryEntity.getProductName());
                        deliveryItemref.setValue(deliveryEntity.getProductRef());
                        deliveryItemLineDetail1.setItemRef(deliveryItemref);
                    }

                }
                Line lineShipping = new Line();
                lineShipping.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
                deliveryItemLineDetail1.setUnitPrice(new BigDecimal(revenueSummary.deliveryFees));
                deliveryItemLineDetail1.setQty(QBConstants.UNIT_PRICE);
                lineShipping.setSalesItemLineDetail(deliveryItemLineDetail1);
                lineShipping.setAmount(new BigDecimal(revenueSummary.deliveryFees));
                linesList.add(lineShipping);


                //Excise Tax Item
                SalesItemLineDetail exciseTaxItemDetails = new SalesItemLineDetail();
                ReferenceType taxItemRef = new ReferenceType();
                QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                        QBConstants.TOTAL_EXCISE_TAX.trim());

                QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);

                GzipHttpOutputInterceptor.LOG.info("quickbookenity:" + quickbookEntity);

                if (quickbookEntity == null) {
                    Item taxItem = ItemHelper.createExciseTaxItem(service, entities);

                    if (taxItem.getId() != null) {
                        quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                                taxItem.getId(), QBConstants.TOTAL_EXCISE_TAX.trim());

                        GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + taxItem.getId());
                        taxItemRef.setName(taxItem.getName());
                        taxItemRef.setValue(taxItem.getId());
                        exciseTaxItemDetails.setItemRef(taxItemRef);
                    }

                } else {
                    if (quickbookEntity.getProductRef() != null) {
                        taxItemRef.setName(quickbookEntity.getProductName());
                        taxItemRef.setValue(quickbookEntity.getProductRef());
                        exciseTaxItemDetails.setItemRef(taxItemRef);
                    }

                }

                Line totalExciseLine = new Line();
                totalExciseLine.setAmount(new BigDecimal(revenueSummary.exciseTax));
                totalExciseLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
                exciseTaxItemDetails.setUnitPrice(new BigDecimal((revenueSummary.exciseTax)));
                exciseTaxItemDetails.setQty(QBConstants.UNIT_PRICE);
                totalExciseLine.setSalesItemLineDetail(exciseTaxItemDetails);
                linesList.add(totalExciseLine);

                //Credit card fee Line Item
                SalesItemLineDetail creditcardFees = new SalesItemLineDetail();
                ReferenceType cardFeeRef = new ReferenceType();
                QuickbookEntity creditCardfeeItem = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId,
                        QuickbookEntity.SyncStrategy.ProductRef, QBConstants.CREDIT_CARD_FEES);

                GzipHttpOutputInterceptor.LOG.info("Credit card fee Item:" + quickbookEntity);
                if (creditCardfeeItem == null) {
                    Item cardFeeItemDetails = ItemHelper.createCreditCardFees(service, accountType);
                    if (cardFeeItemDetails != null) {
                        quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                                cardFeeItemDetails.getId(), QBConstants.CREDIT_CARD_FEES);

                        GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + cardFeeItemDetails.getId());
                        cardFeeRef.setName(cardFeeItemDetails.getName());
                        cardFeeRef.setValue(cardFeeItemDetails.getId());
                        creditcardFees.setItemRef(cardFeeRef);
                    }

                } else {
                    if (creditCardfeeItem.getProductRef() != null) {
                        cardFeeRef.setName(creditCardfeeItem.getProductName());
                        cardFeeRef.setValue(creditCardfeeItem.getProductRef());
                        creditcardFees.setItemRef(cardFeeRef);
                    }

                }

                Line creditcardfeeLine = new Line();
                creditcardfeeLine.setAmount(new BigDecimal(revenueSummary.creditCardFees));
                creditcardfeeLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
                creditcardFees.setUnitPrice(new BigDecimal(revenueSummary.creditCardFees));
                creditcardFees.setQty(QBConstants.UNIT_PRICE);
                creditcardfeeLine.setSalesItemLineDetail(creditcardFees);
                linesList.add(creditcardfeeLine);


                TxnTaxDetail txnTaxDetail = new TxnTaxDetail();
                txnTaxDetail.setTotalTax(new BigDecimal(revenueSummary.totalTax));
                qbSalesReceipt.setLine(linesList);
                qbSalesReceipt.setTotalAmt(new BigDecimal(revenueSummary.grossReceipt));
                qbSalesReceipt.setTxnTaxDetail(txnTaxDetail);


                if (revenueSummary.name.length() < 16)
                    qbSalesReceipt.setDocNumber(revenueSummary.name);
                else
                    qbSalesReceipt.setDocNumber(revenueSummary.name.substring(0, 15));
                //Tax Separation

                String taxSeperation = "City Tax: " + "$ " + NumberUtils.round(revenueSummary.cityTax, 2) + "\n"
                        + "County Tax: " + "$ " + NumberUtils.round(revenueSummary.countyTax, 2) + "\n" + "State Tax:" + "$ "
                        + NumberUtils.round(revenueSummary.stateTax, 2) + "\n" + "Total Tax: " + "$ " + NumberUtils.round(revenueSummary.totalTax, 2);


                if (revenueSummary.totalTax > 0) {
                    MemoRef memoRef = new MemoRef();
                    memoRef.setValue(taxSeperation);
                    qbSalesReceipt.setCustomerMemo(memoRef);
                }
                try {
                    if (accountType != null) {
                        Account depositAccount = AccountHelper.getAccount(service, accountType.getChecking());
                        qbSalesReceipt.setDepositToAccountRef(AccountHelper.getAccountRef(depositAccount));
                    }

                    salesReceiptList.add(qbSalesReceipt);
                } catch (FMSException e) {
                    for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                        GzipHttpOutputInterceptor.LOG.info("Error while calling Create QB sales by consumer type:: " + error.getMessage() + "Details:: "
                                + error.getDetail() + "statusCode: " + error.getCode());

                        //Get Access token if Access token Expires after an hour
                        if (error.getCode().equals(QBConstants.ERRORCODE)) {

                            GzipHttpOutputInterceptor.LOG.info("Inside 3200 code");
                            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                    shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                            BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                            service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                        }
                    }
                }
            }
        }

        return salesReceiptList;
    }


    public void createSalesByConsumerInQB(DataService service, List<RevenueSummary> revenueSummaryList, String blazecompanyId,
                                          String shopId, long endTime) {

        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        List<SalesReceipt> salesReceiptList = convertSalesByConsumerInQB(service, revenueSummaryList, blazecompanyId, shopId);
        int counter = 0;
        int parts = salesReceiptList.size() / 30;
        int remainder = salesReceiptList.size() % 30;
        if (remainder > 0) {
            parts = parts + 1;
        }
        int total = salesReceiptList.size();
        int totalsuccess = 0;
        for (int i = 0; i < parts; i++) {
            List<SalesReceipt> saleReceiptBatch = salesReceiptHelper.getSaleReceiptBatch(salesReceiptList, i * 30);
            GzipHttpOutputInterceptor.LOG.info(" Sales Receipt Size:   " + saleReceiptBatch.size());
            batchOperation = new BatchOperation();
            try {
                for (SalesReceipt salesReceipt : saleReceiptBatch) {
                    counter++;
                    batchOperation.addEntity(salesReceipt, OperationEnum.CREATE, "CategoryBatch" + counter);

                }

                service.executeBatch(batchOperation);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            GzipHttpOutputInterceptor.LOG.info("errror message:" + error.getMessage() + "error Details: "
                                    + error.getDetail() + " error code: " + error.getCode());


                        }

                    }

                    SalesReceipt salesReceiptObj = (SalesReceipt) batchOperation.getEntity(bId);
                    if (salesReceiptObj != null) {
                        totalsuccess++;
                    }
                    GzipHttpOutputInterceptor.LOG.info("Sales Receipt Created: " + salesReceiptObj);

                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Sales by consumer type:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        GzipHttpOutputInterceptor.LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while batch Create by sales Category:");
            }
        }
        int totalFail = total - totalsuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalsuccess, totalFail, blazecompanyId, endTime, startTime, shopId,
                QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);
    }


}


