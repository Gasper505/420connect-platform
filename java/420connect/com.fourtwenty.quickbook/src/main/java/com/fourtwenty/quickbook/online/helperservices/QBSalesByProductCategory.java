package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.company.CompoundTaxRate;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.http.HttpParser.LOG;


public class QBSalesByProductCategory {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ItemHelper itemHelper;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;
    @Inject
    SalesReceiptHelper salesReceiptHelper;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    public void syncSaleByProductCategoryInQB(DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {

        long endTime = DateTime.now().getMillis();
        long startTime = DateTime.now().getMillis();

        int counter = 0;

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> transactions = null;
        Iterable<QuickbookSyncDetails> syncSalesReceiptList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId,
                shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncSaleseceipt = Lists.newLinkedList(syncSalesReceiptList);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncSaleseceipt.isEmpty()) {
            long beforeDate = DateTime.now().getMillis() - 8640000000l + 864000000l;
            transactions = transactionRepository.getBracketSalesQB(blazeCompanyId, shopId, beforeDate, startTime);

        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncSaleseceipt.isEmpty()) {
            //Entry in custom entities table
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, startTime, endTime, shopId,
                    QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


        } else {
            QuickbookSyncDetails quickbookSyncSalesReceipt = syncSaleseceipt.getLast();
            endTime = quickbookSyncSalesReceipt.getEndTime();
            transactions = transactionRepository.getDailySalesforQB(blazeCompanyId, shopId, endTime, startTime);


        }
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductBatch> batchHashMap = batchRepository.listAllAsMap(blazeCompanyId);
        HashMap<String, CategorySale> results = new HashMap<>();
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(blazeCompanyId, shopId);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(blazeCompanyId, shopId);
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(blazeCompanyId);


        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        Iterable<Product> products = productRepository.listAll(blazeCompanyId);
        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        int factor = 1;
        for (Transaction t : transactions) {
            factor = 1;
            counter++;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            int totalFinalCost = 0;
            for (OrderItem item : t.getCart().getItems()) {
                if ((t.getTransType() == Transaction.TransactionType.Refund && t.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && item.getStatus() != OrderItem.OrderItemStatus.Refunded)) {
                    totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
                }
            }

            double avgDeliveryFee = 0;
            if (t.getCart().getItems().size() > 0) {
                avgDeliveryFee = t.getCart().getDeliveryFee().doubleValue() / t.getCart().getItems().size();
            }

            double avgCreditCardFees = 0;
            if (t.getCart().getItems().size() > 0 && t.getCart().isEnableCreditCardFee() && t.getCart().getCreditCardFee().doubleValue() > 0) {
                avgCreditCardFees = t.getCart().getCreditCardFee().doubleValue() / t.getCart().getItems().size();
            }

            double avgAfterTaxDiscount = 0;
            if (t.getCart().getAppliedAfterTaxDiscount() != null && t.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                avgAfterTaxDiscount += t.getCart().getAppliedAfterTaxDiscount().doubleValue() / t.getCart().getItems().size();
            }

            double totalPreTax = 0;
            double cTotal = 0;
            List<String> visited = new ArrayList<>();
            for (OrderItem item : t.getCart().getItems()) {

                double taxRate = 0d;

                taxRate = t.getCart().getTax().doubleValue();
                if (item.getTaxInfo() != null
                        && item.getTaxType() == TaxInfo.TaxType.Custom
                        && item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                    taxRate = item.getTaxInfo().getTotalTax().doubleValue();
                }
                if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PreTaxed) {
                    taxRate = 0;
                }

                Product p = productMap.get(item.getProductId());
                if (p != null) {
                    totalPreTax += item.getCalcPreTax().doubleValue();
                }

                if (t.getTransType() == Transaction.TransactionType.Sale
                        || (t.getTransType() == Transaction.TransactionType.Refund && t.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && item.getStatus() == OrderItem.OrderItemStatus.Refunded)) {

                    if (p != null) {

                        String catName = categoryMap.get(p.getCategoryId()).getName();
                        CategorySale categorySale = results.get(catName);
                        if (categorySale == null) {
                            categorySale = new CategorySale();
                            results.put(catName, categorySale);
                        }
                        // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                        if (!visited.contains(p.getCategoryId())) {
                            categorySale.transCount++;
                            visited.add(p.getCategoryId());
                        }


                        int finalCost = (int) (item.getFinalPrice().doubleValue() * 100);
                        double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                        double itemDiscount = item.getCost().subtract(item.getFinalPrice()).doubleValue();
                        double targetCartDiscount = t.getCart().getCalcCartDiscount().doubleValue() * ratio;
                        cTotal += targetCartDiscount;

                        // calculated postTax

                        categorySale.name = catName;
                        categorySale.itemDiscounts += itemDiscount;
                        categorySale.deliveryFees += avgDeliveryFee * factor;
                        categorySale.creditCardFees += avgCreditCardFees * factor;
                        categorySale.discounts += targetCartDiscount * factor;
                        categorySale.afterTaxDiscount += avgAfterTaxDiscount * factor;
                        categorySale.totalPreTax += totalPreTax * factor;

                        double expectedFinalCost = (item.getCost().doubleValue() - itemDiscount) * factor;
                        if (NumberUtils.round(expectedFinalCost, 2) != NumberUtils.round(item.getFinalPrice().doubleValue(), 2)) {
                            System.out.println("Difference in final cost");
                        }

                        double sales = NumberUtils.round(item.getFinalPrice().doubleValue() - targetCartDiscount, 5);


                        double postTax = NumberUtils.round(sales * taxRate, 6);
                        double totalExciseTax = 0;
                        totalExciseTax += item.getExciseTax().doubleValue() * factor;

                        double preALExciseTax = 0;
                        double preNALExciseTax = 0;
                        double postALExciseTax = 0;
                        double postNALExciseTax = 0;
                        if (item.getTaxResult() != null) {
                            preALExciseTax += item.getTaxResult().getTotalALExciseTax().doubleValue();
                            postALExciseTax += item.getTaxResult().getTotalALPostExciseTax().doubleValue();

                            preNALExciseTax += item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                            postNALExciseTax += item.getTaxResult().getTotalExciseTax().doubleValue();
                        }
                        categorySale.subTotals += item.getCost().doubleValue() * factor - (preALExciseTax + preNALExciseTax) * factor;
                        categorySale.retailValue += item.getCost().doubleValue() * factor;

                        double totalCityTax = 0;
                        double totalCountyTax = 0;
                        double totalStateTax = 0;
                        double totalFedTax = 0;
                        if (item.getTaxTable() != null) {
                            CompoundTaxTable taxTable = item.getTaxTable();
                            double curTax = 0d;
                            double subTotalWithTax = sales;
                            for (CompoundTaxRate curTaxRate : taxTable.getActivePostTaxes()) {
                                if (curTaxRate.isActive()) {
                                    double productTax = 0d;
                                    if (curTaxRate.isCompound()) {
                                        subTotalWithTax += NumberUtils.round(curTax, 3);

                                        // reset curTax
                                        curTax = 0d;
                                        productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                        curTax += productTax;

                                    } else {
                                        productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                        curTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.City) {
                                        totalCityTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.County) {
                                        totalCountyTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.State) {
                                        totalStateTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.Federal) {
                                        totalFedTax += productTax;
                                    }
                                }
                            }
                            postTax = NumberUtils.round(totalCityTax, 3) + NumberUtils.round(totalCountyTax, 3) + NumberUtils.round(totalStateTax, 3) + totalFedTax;
                            postTax = NumberUtils.round(postTax, 3);
                        }

                        categorySale.preALExciseTax += preALExciseTax * factor;
                        categorySale.preNALExciseTax += preNALExciseTax * factor;
                        categorySale.postALExciseTax += postALExciseTax * factor;
                        categorySale.postNALExciseTax += postNALExciseTax * factor;
                        categorySale.exciseTax += (preALExciseTax + preNALExciseTax + postALExciseTax + postNALExciseTax) * factor;
                        categorySale.cityTax += totalCityTax * factor;
                        categorySale.countyTax += totalCountyTax * factor;
                        categorySale.stateTax += totalStateTax * factor;
                        categorySale.tax += (postTax) * factor;
                        categorySale.sales += sales * factor;

                        // caclulate cost of goods
                        boolean calculated = false;
                        double cogs = 0;
                        if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                            // if prepackage is set, use prepackage
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null && targetBatch != null) {
                                    calculated = true;

                                    BigDecimal unitValue = prepackage.getUnitValue();
                                    if (unitValue == null || unitValue.doubleValue() == 0) {
                                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                        unitValue = weightTolerance.getUnitValue();
                                    }
                                    // calculate the total quantity based on the prepackage value
                                    cogs += calcCOGS(item.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                }
                            }
                        } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                            // otherwise, use quantity logs
                            for (QuantityLog quantityLog : item.getQuantityLogs()) {
                                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                    ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                    if (targetBatch != null) {
                                        calculated = true;
                                        cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                    }
                                }
                            }
                        }

                        if (!calculated) {
                            double unitCost = getUnitCost(p, recentBatchMap, productsByCompanyLinkId);
                            cogs = unitCost * item.getQuantity().doubleValue();
                        }

                        categorySale.cogs += cogs * factor;

                    }
                }


            }

            int cartDiscount = (int) (t.getCart().getCalcCartDiscount().doubleValue() * 100);
            int myCTotal = (int) (NumberUtils.round(cTotal, 2) * 100);
            if (myCTotal != cartDiscount) {
                System.out.println("cart discount doesn't match");
            }

        }

        if (counter > 0) {
            double gross = 0.0;
            List<CategorySale> categorySaleList = new ArrayList<CategorySale>();
            List<String> nameList = new ArrayList<String>();
            for (String name : results.keySet()) {
                CategorySale categorySale = results.get(name);
                gross = NumberUtils.round(categorySale.subTotals, 2) + NumberUtils.round(categorySale.deliveryFees, 2)
                        + NumberUtils.round(categorySale.tax, 2) - NumberUtils.round(categorySale.itemDiscounts, 2);

                categorySaleList.add(categorySale);
                nameList.add(name);


            }
            Iterator<String> nameIterator = nameList.iterator();
            Iterator<CategorySale> categorySaleIterator = categorySaleList.iterator();
            createProductByCategory(service, nameIterator, blazeCompanyId, shopId, entities);
            createSalesByCategoryInQB(service, new BigDecimal(gross), categorySaleIterator, blazeCompanyId, shopId, endTime, categorySaleList.size());
        }


    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }


    public void createProductByCategory(DataService service, Iterator<String> nameList, String blazecompanyId, String shopId,
                                        QuickbookCustomEntities quickbookDetail) {

        BatchOperation batchOperation = null;
        int counter = 0;
        for (int itr = 0; nameList.hasNext(); itr++) {
            List<Item> itemList = convertProductCategoryToQb(service, nameList, quickbookDetail);
            GzipHttpOutputInterceptor.LOG.info(" Item Size:   " + itemList.size());
            batchOperation = new BatchOperation();
            try {
                for (Item item : itemList) {
                    counter++;

                    QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId,
                            QuickbookEntity.SyncStrategy.ProductRef, item.getName());

                    if (quickbookEntity == null) {
                        batchOperation.addEntity(item, OperationEnum.CREATE, "CategoryBatch" + counter);
                    }
                }

                service.executeBatch(batchOperation);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            GzipHttpOutputInterceptor.LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() +
                                    " error code: " + error.getCode());

                        }

                    }

                    Item singleItem = (Item) batchOperation.getEntity(bId);
                    GzipHttpOutputInterceptor.LOG.info("Result Item: " + singleItem);
                    if (singleItem != null) {
                        quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, singleItem.getId(),
                                singleItem.getName());
                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Create sales by product category:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        GzipHttpOutputInterceptor.LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while batch Create:");
            }
        }

    }

    public static List<Item> convertProductCategoryToQb(DataService service, Iterator<String> nameList, QuickbookCustomEntities quickbookDetail) {
        List<Item> itemList = new ArrayList<Item>();
        int counter = 0;
        while ((counter < 30) && (nameList.hasNext())) {
            String productName = nameList.next();
            Item item = new Item();
            item.setName(productName);
            item.setActive(true);
            item.setTaxable(false);
            item.setUnitPrice(QBConstants.UNIT_PRICE);
            item.setType(ItemTypeEnum.NON_INVENTORY);
            try {
                Account incomeAccount = AccountHelper.getAccount(service, quickbookDetail.getSales());
                item.setIncomeAccountRef(AccountHelper.getAccountRef(incomeAccount));
                item.setPurchaseCost(QBConstants.UNIT_PRICE);
                Account expenseAccount = AccountHelper.getAccount(service, quickbookDetail.getSuppliesandMaterials());
                item.setExpenseAccountRef(AccountHelper.getAccountRef(expenseAccount));
                Account assetAccount = AccountHelper.getAccount(service, quickbookDetail.getInventory());
                item.setAssetAccountRef(AccountHelper.getAccountRef(assetAccount));
                item.setTrackQtyOnHand(false);
                itemList.add(item);
                counter++;

            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Create Category Item: " + error.getMessage() + "Detail::: " + error.getDetail());
                }

            } catch (Exception e) {
                e.printStackTrace();
                GzipHttpOutputInterceptor.LOG.info("Error while creating  category Items: ");
            }
        }

        return itemList;

    }


    public List<SalesReceipt> convertSalesByCategoryInQB(DataService service, BigDecimal grossReceiptAmount, Iterator<CategorySale> categorySaleList,
                                                         SalesReceipt salesReceipt, String blazecompanyId, String shopId) {

        List<SalesReceipt> salesReceiptList = new ArrayList<SalesReceipt>();
        int counter = 0;

        while ((counter < 30) && (categorySaleList.hasNext())) {
            CategorySale categorySale = categorySaleList.next();
            SalesReceipt qbSalesReceipt = new SalesReceipt();
            List<Line> linesList = new ArrayList<Line>();
            Line lineObject = new Line();
            lineObject.setAmount(new BigDecimal(categorySale.subTotals));
            lineObject.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
            SalesItemLineDetail salesItemLineDetail1 = new SalesItemLineDetail();

            QuickbookEntity quickbookEntityCategoryRef = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId,
                    QuickbookEntity.SyncStrategy.ProductRef, categorySale.name);

            if (quickbookEntityCategoryRef != null) {
                LOG.info("Item Ref:" + quickbookEntityCategoryRef.getProductRef());
                ReferenceType categoryItemref = new ReferenceType();
                categoryItemref.setName(categorySale.name);
                categoryItemref.setValue(quickbookEntityCategoryRef.getProductRef());
                salesItemLineDetail1.setItemRef(categoryItemref);

            }

            salesItemLineDetail1.setQty(QBConstants.UNIT_PRICE);
            salesItemLineDetail1.setUnitPrice(new BigDecimal(categorySale.subTotals));

            //apply Sales tax
            ReferenceType taxcode = new ReferenceType();
            taxcode.setValue(QBConstants.TAX);

            salesItemLineDetail1.setTaxCodeRef(taxcode);
            lineObject.setSalesItemLineDetail(salesItemLineDetail1);
            lineObject.setDescription(categorySale.name);
            linesList.add(lineObject);

            //Discount Amount
            Line lineDetails = new Line();
            lineDetails.setDetailType(LineDetailTypeEnum.DISCOUNT_LINE_DETAIL);
            DiscountLineDetail discountLineDetail = new DiscountLineDetail();
            discountLineDetail.setPercentBased(false);
            lineDetails.setAmount(new BigDecimal(categorySale.itemDiscounts));
            lineDetails.setDiscountLineDetail(discountLineDetail);
            linesList.add(lineDetails);

            //Shipping
            QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
            SalesItemLineDetail deliveryItemLineDetail1 = new SalesItemLineDetail();
            ReferenceType deliveryItemref = new ReferenceType();
            QuickbookEntity deliveryEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId,
                    QuickbookEntity.SyncStrategy.ProductRef, QBConstants.DELIVERY_CHARGE.trim());

            GzipHttpOutputInterceptor.LOG.info("quickbookenity:" + deliveryEntity);
            if (deliveryEntity == null) {
                Item deliveryItem = ItemHelper.createDeliveryItem(service, accountType);
                if (deliveryItem != null) {
                    quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                            deliveryItem.getId(), QBConstants.DELIVERY_CHARGE.trim());
                    GzipHttpOutputInterceptor.LOG.info("deliveryItem: " + deliveryItem.getId());
                    deliveryItemref.setName(deliveryItem.getName());
                    deliveryItemref.setValue(deliveryItem.getId());
                    deliveryItemLineDetail1.setItemRef(deliveryItemref);
                }

            } else {
                if (deliveryEntity.getProductRef() != null) {
                    deliveryItemref.setName(deliveryEntity.getProductName());
                    deliveryItemref.setValue(deliveryEntity.getProductRef());
                    deliveryItemLineDetail1.setItemRef(deliveryItemref);
                }

            }
            Line lineShipping = new Line();
            lineShipping.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
            deliveryItemLineDetail1.setUnitPrice(new BigDecimal(categorySale.deliveryFees));
            deliveryItemLineDetail1.setQty(QBConstants.UNIT_PRICE);
            lineShipping.setSalesItemLineDetail(deliveryItemLineDetail1);
            lineShipping.setAmount(new BigDecimal(categorySale.deliveryFees));
            linesList.add(lineShipping);


            SalesItemLineDetail exciseTaxItemDetails = new SalesItemLineDetail();
            ReferenceType taxItemRef = new ReferenceType();
            QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef,
                    QBConstants.TOTAL_EXCISE_TAX);

            GzipHttpOutputInterceptor.LOG.info("quickbookenity:" + quickbookEntity);
            QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
            if (quickbookEntity == null) {
                //create Item
                Item taxItem = ItemHelper.createExciseTaxItem(service, entities);
                if (taxItem.getId() != null) {
                    quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, taxItem.getId(),
                            QBConstants.TOTAL_EXCISE_TAX);

                    GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + taxItem.getId());
                    taxItemRef.setName(taxItem.getName());
                    taxItemRef.setValue(taxItem.getId());
                    exciseTaxItemDetails.setItemRef(taxItemRef);
                }

            } else {
                if (quickbookEntity.getProductRef() != null) {
                    taxItemRef.setName(quickbookEntity.getProductName());
                    taxItemRef.setValue(quickbookEntity.getProductRef());
                    exciseTaxItemDetails.setItemRef(taxItemRef);
                }

            }

            Line totalExciseLine = new Line();
            totalExciseLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
            totalExciseLine.setAmount(new BigDecimal(categorySale.exciseTax));
            exciseTaxItemDetails.setUnitPrice(new BigDecimal(categorySale.exciseTax));
            exciseTaxItemDetails.setQty(QBConstants.UNIT_PRICE);
            totalExciseLine.setSalesItemLineDetail(exciseTaxItemDetails);
            linesList.add(totalExciseLine);


            //Credit card fee Line Item
            SalesItemLineDetail creditcardFees = new SalesItemLineDetail();
            ReferenceType cardFeeRef = new ReferenceType();
            QuickbookEntity creditCardfeeItem = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, QBConstants.CREDIT_CARD_FEES);
            GzipHttpOutputInterceptor.LOG.info("Credit card fee Item:" + quickbookEntity);
            if (creditCardfeeItem == null) {
                Item cardFeeItemDetails = ItemHelper.createCreditCardFees(service, accountType);
                if (cardFeeItemDetails != null) {
                    quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, cardFeeItemDetails.getId(), QBConstants.CREDIT_CARD_FEES);
                    GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + cardFeeItemDetails.getId());
                    cardFeeRef.setName(cardFeeItemDetails.getName());
                    cardFeeRef.setValue(cardFeeItemDetails.getId());
                    creditcardFees.setItemRef(cardFeeRef);
                }

            } else {
                if (creditCardfeeItem.getProductRef() != null) {
                    cardFeeRef.setName(creditCardfeeItem.getProductName());
                    cardFeeRef.setValue(creditCardfeeItem.getProductRef());
                    creditcardFees.setItemRef(cardFeeRef);
                }

            }

            Line creditcardfeeLine = new Line();
            creditcardfeeLine.setAmount(new BigDecimal(categorySale.creditCardFees));
            creditcardfeeLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
            creditcardFees.setUnitPrice(new BigDecimal(categorySale.creditCardFees));
            creditcardFees.setQty(QBConstants.UNIT_PRICE);
            creditcardfeeLine.setSalesItemLineDetail(creditcardFees);
            linesList.add(creditcardfeeLine);


            TxnTaxDetail txnTaxDetail = new TxnTaxDetail();
            txnTaxDetail.setTotalTax(new BigDecimal(categorySale.tax));
            qbSalesReceipt.setLine(linesList);
            qbSalesReceipt.setTotalAmt(grossReceiptAmount);
            qbSalesReceipt.setTxnTaxDetail(txnTaxDetail);
            if (categorySale.name.length() < 16) {
                qbSalesReceipt.setDocNumber(categorySale.name);
            } else {
                qbSalesReceipt.setDocNumber(categorySale.name.substring(0, 15));
            }


            //tax Seperation
            String taxSeperation = "";
            taxSeperation = "Pre  AL ExciseTax Tax: " + "$ " + NumberUtils.round(categorySale.preALExciseTax, 2) + "\n" + "Pre  NAL ExciseTax Tax: "
                    + "$ " + NumberUtils.round(categorySale.preNALExciseTax, 2) + "\n" + "Post  AL ExciseTax Tax: " + "$ "
                    + NumberUtils.round(categorySale.postALExciseTax, 2) + "\n" + "Post  NAL ExciseTax Tax: " + "$ " + NumberUtils.round(categorySale.postNALExciseTax, 2) + "\n" + "Total City Tax: " + "$ " + NumberUtils.round(categorySale.cityTax, 2) + "\n" + "Total County Tax: " + "$ " + NumberUtils.round(categorySale.countyTax, 2) + "\n" + "Total State Tax:" + "$ " + NumberUtils.round(categorySale.stateTax, 2) + "\n" + "Total Tax: " + "$ " + NumberUtils.round(categorySale.tax, 2);


            if (categorySale.tax > 0) {
                MemoRef memoRef = new MemoRef();
                memoRef.setValue(taxSeperation);
                qbSalesReceipt.setCustomerMemo(memoRef);
            }
            try {
                if (accountType != null) {
                    Account depositAccount = AccountHelper.getAccount(service, accountType.getChecking());
                    qbSalesReceipt.setDepositToAccountRef(AccountHelper.getAccountRef(depositAccount));
                }


                salesReceiptList.add(qbSalesReceipt);
                counter++;
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Create vendor:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        GzipHttpOutputInterceptor.LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            }
        }


        return salesReceiptList;
    }

    public void createSalesByCategoryInQB(DataService service, BigDecimal grossReceiptAmount, Iterator<CategorySale> categorySaleList,
                                          String blazecompanyId, String shopId, long endTime, int total) {
        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int totalsuccess = 0, counter = 0;
        while (categorySaleList.hasNext()) {
            SalesReceipt salesReceipt = new SalesReceipt();
            List<SalesReceipt> salesReceiptList = convertSalesByCategoryInQB(service, grossReceiptAmount, categorySaleList, salesReceipt, blazecompanyId, shopId);
            GzipHttpOutputInterceptor.LOG.info(" salesReceipt Size:   " + salesReceiptList.size());
            batchOperation = new BatchOperation();
            try {
                for (SalesReceipt salesReceipt1 : salesReceiptList) {
                    counter++;
                    batchOperation.addEntity(salesReceipt1, OperationEnum.CREATE, "CategoryBatch" + counter);
                }

                service.executeBatch(batchOperation);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            GzipHttpOutputInterceptor.LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail()
                                    + " error code: " + error.getCode());

                        }

                    }

                    SalesReceipt salesReceiptObj = (SalesReceipt) batchOperation.getEntity(bId);
                    if (salesReceiptObj != null) {
                        totalsuccess++;
                    }
                    GzipHttpOutputInterceptor.LOG.info("Sales Receipt Created: " + salesReceiptObj);


                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    GzipHttpOutputInterceptor.LOG.info("Error while calling Create vendor:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        GzipHttpOutputInterceptor.LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazecompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("Error while  Sales By consumer batch Create:");
            }

        }
        int totalFail = total - totalsuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalsuccess, totalFail, blazecompanyId, endTime, startTime,
                shopId, QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);
    }


    public static final class CategorySale {
        public String name;
        public double retailValue = 0;
        public double itemDiscounts = 0;
        public double subTotals = 0;
        public double discounts = 0;
        public double sales = 0;
        public double deliveryFees = 0;
        public double creditCardFees = 0;
        public double totalPreTax = 0;
        double preALExciseTax = 0;
        double preNALExciseTax = 0;
        double postALExciseTax = 0;
        double postNALExciseTax = 0;
        public double cityTax = 0;
        public double stateTax = 0;
        public double countyTax = 0;
        public double tax = 0;
        public double grossReceipt = 0;
        public double cogs = 0;
        public double transCount = 0;
        public double exciseTax = 0;
        public double afterTaxDiscount = 0;


    }

}
