package com.fourtwenty.quickbook.online.helperservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.AccountInfo;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;

import java.util.List;

import static org.eclipse.jetty.http.HttpParser.LOG;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickbookSettings {

    private String entitiesId;
    private String qbtypes;
    private String quickbookmethology;
    private String syncStrategy;

    private String inventory;
    private String sales;
    private String suppliesandMaterials;
    private String payableAccount;
    private String clearance;
    private String checking;
    private String cc_clearance;
    private String income;
    private String receivable;
    private List<AccountInfo> accountList;

    private boolean purchaseOrder;
    private boolean refundReceipt;
    private boolean invoice;
    private boolean salesReceipt;
    private boolean bill;
    private boolean accounts;

    private boolean inprogress;
    private String qbCompanyId;


    private String saleslastSyncDate;
    private String refundLastSyncDate;
    private String purchaseLastSyncDate;
    private String billLastSyncDate;
    private String salesbyconsumerLastDate;
    private String salesByProductLastDate;
    private String expenseSyncLastDate;


    private long purchaseTotalSync;
    private long purchaseTotalFail;

    private long salesTotalFail;
    private long salesTotalSync;

    private long refundTotalFail;
    private long refundTotalSync;

    private long billTotalSync;
    private long billTotalFail;

    private long salesConsumerTotalSync;
    private long salesConsumerTotalFail;

    private long salesByproductTotalSync;
    private long salesbyproductTotalFail;

    private long expenseTotalSync;
    private long expenseTotalFail;

    private QuickbookCustomEntities.QbCustomAccount qbCustomAccount = QuickbookCustomEntities.QbCustomAccount.None;

    private List<QBDesktopAccounts> qbDesktopAccounts;
    private boolean syncCustomAccount;


    public QuickbookSettings(QuickbookCustomEntities quickbookCustomEntities) {
        LOG.info("Quickbook custom entity Details: " + quickbookCustomEntities);
        if (quickbookCustomEntities != null) {
            this.setEntitiesId(quickbookCustomEntities.getId());
            this.setQbtypes(quickbookCustomEntities.getQbtypes());
            this.setQuickbookmethology(quickbookCustomEntities.getQuickbookmethology());
            this.setSyncStrategy(quickbookCustomEntities.getSyncStrategy());

            this.setInventory(quickbookCustomEntities.getInventory());
            this.setSales(quickbookCustomEntities.getSales());
            this.setSuppliesandMaterials(quickbookCustomEntities.getSuppliesandMaterials());
            this.setPayableAccount(quickbookCustomEntities.getPayableAccount());
            this.setChecking(quickbookCustomEntities.getChecking());
            this.setCcClearance(quickbookCustomEntities.getCc_clearance());
            this.setClearance(quickbookCustomEntities.getClearance());
            this.setIncome(quickbookCustomEntities.getIncome());
            this.setReceivable(quickbookCustomEntities.getReceivable());

            this.setPurchaseOrder(quickbookCustomEntities.isPurchaseOrder());
            this.setRefundReceipt(quickbookCustomEntities.isRefundReceipt());
            this.setInvoice(quickbookCustomEntities.isInvoice());
            this.setSalesReceipt(quickbookCustomEntities.isSalesReceipt());
            this.setBill(quickbookCustomEntities.isBill());
            this.setQbCompanyId(quickbookCustomEntities.getQbCompanyId());
            this.setAccounts(quickbookCustomEntities.isAccounts());

            this.setQbCustomAccount(quickbookCustomEntities.getQbCustomAccount());
            this.setSyncCustomAccount(quickbookCustomEntities.isSyncCustomAccount());
        }

    }

    public String getEntitiesId() {
        return entitiesId;
    }

    public void setEntitiesId(String entitiesId) {
        this.entitiesId = entitiesId;
    }

    public boolean isInprogress() {
        return inprogress;
    }

    public void setInprogress(boolean inprogress) {
        this.inprogress = inprogress;
    }

    public List<AccountInfo> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<AccountInfo> accountList) {
        this.accountList = accountList;
    }

    public String getQuickbookmethology() {
        return quickbookmethology;
    }

    public void setQuickbookmethology(String quickbookmethology) {
        this.quickbookmethology = quickbookmethology;
    }

    public String getSyncStrategy() {
        return syncStrategy;
    }

    public void setSyncStrategy(String syncStrategy) {
        this.syncStrategy = syncStrategy;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getSuppliesandMaterials() {
        return suppliesandMaterials;
    }

    public void setSuppliesandMaterials(String suppliesandMaterials) {
        this.suppliesandMaterials = suppliesandMaterials;
    }

    public String getPayableAccount() {
        return payableAccount;
    }

    public void setPayableAccount(String payableAccount) {
        this.payableAccount = payableAccount;
    }

    public String getChecking() {
        return checking;
    }

    public void setChecking(String checking) {
        this.checking = checking;
    }

    public boolean isPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(boolean purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public boolean isRefundReceipt() {
        return refundReceipt;
    }

    public void setRefundReceipt(boolean refundReceipt) {
        this.refundReceipt = refundReceipt;
    }

    public boolean isInvoice() {
        return invoice;
    }

    public void setInvoice(boolean invoice) {
        this.invoice = invoice;
    }

    public boolean isSalesReceipt() {
        return salesReceipt;
    }

    public void setSalesReceipt(boolean salesReceipt) {
        this.salesReceipt = salesReceipt;
    }

    public String getQbtypes() {
        return qbtypes;
    }

    public void setQbtypes(String qbtypes) {
        this.qbtypes = qbtypes;
    }


    public String getQbCompanyId() {
        return qbCompanyId;
    }

    public void setQbCompanyId(String qbCompanyId) {
        this.qbCompanyId = qbCompanyId;
    }

    public boolean isBill() {
        return bill;
    }

    public void setBill(boolean bill) {
        this.bill = bill;
    }

    public String getClearance() {
        return clearance;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public long getPurchaseTotalSync() {
        return purchaseTotalSync;
    }

    public void setPurchaseTotalSync(long purchaseTotalSync) {
        this.purchaseTotalSync = purchaseTotalSync;
    }

    public long getSalesTotalSync() {
        return salesTotalSync;
    }

    public void setSalesTotalSync(long salesTotalSync) {
        this.salesTotalSync = salesTotalSync;
    }

    public long getRefundTotalSync() {
        return refundTotalSync;
    }

    public void setRefundTotalSync(long refundTotalSync) {
        this.refundTotalSync = refundTotalSync;
    }

    public long getPurchaseTotalFail() {
        return purchaseTotalFail;
    }

    public void setPurchaseTotalFail(long purchaseTotalFail) {
        this.purchaseTotalFail = purchaseTotalFail;
    }

    public long getSalesTotalFail() {
        return salesTotalFail;
    }

    public void setSalesTotalFail(long salesTotalFail) {
        this.salesTotalFail = salesTotalFail;
    }

    public long getRefundTotalFail() {
        return refundTotalFail;
    }

    public void setRefundTotalFail(long refundTotalFail) {
        this.refundTotalFail = refundTotalFail;
    }

    public String getSaleslastSyncDate() {
        return saleslastSyncDate;
    }

    public void setSaleslastSyncDate(String saleslastSyncDate) {
        this.saleslastSyncDate = saleslastSyncDate;
    }

    public String getRefundLastSyncDate() {
        return refundLastSyncDate;
    }

    public void setRefundLastSyncDate(String refundLastSyncDate) {
        this.refundLastSyncDate = refundLastSyncDate;
    }

    public String getPurchaseLastSyncDate() {
        return purchaseLastSyncDate;
    }

    public void setPurchaseLastSyncDate(String purchaseLastSyncDate) {
        this.purchaseLastSyncDate = purchaseLastSyncDate;
    }

    public String getBillLastSyncDate() {
        return billLastSyncDate;
    }

    public void setBillLastSyncDate(String billLastSyncDate) {
        this.billLastSyncDate = billLastSyncDate;
    }

    public long getBillTotalSync() {
        return billTotalSync;
    }

    public void setBillTotalSync(long billTotalSync) {
        this.billTotalSync = billTotalSync;
    }

    public long getBillTotalFail() {
        return billTotalFail;
    }

    public void setBillTotalFail(long billTotalFail) {
        this.billTotalFail = billTotalFail;
    }

    public String getSalesbyconsumerLastDate() {
        return salesbyconsumerLastDate;
    }

    public void setSalesbyconsumerLastDate(String salesbyconsumerLastDate) {
        this.salesbyconsumerLastDate = salesbyconsumerLastDate;
    }

    public String getSalesByProductLastDate() {
        return salesByProductLastDate;
    }

    public void setSalesByProductLastDate(String salesByProductLastDate) {
        this.salesByProductLastDate = salesByProductLastDate;
    }

    public long getSalesConsumerTotalSync() {
        return salesConsumerTotalSync;
    }

    public void setSalesConsumerTotalSync(long salesConsumerTotalSync) {
        this.salesConsumerTotalSync = salesConsumerTotalSync;
    }

    public long getSalesConsumerTotalFail() {
        return salesConsumerTotalFail;
    }

    public void setSalesConsumerTotalFail(long salesConsumerTotalFail) {
        this.salesConsumerTotalFail = salesConsumerTotalFail;
    }

    public long getSalesByproductTotalSync() {
        return salesByproductTotalSync;
    }

    public void setSalesByproductTotalSync(long salesByproductTotalSync) {
        this.salesByproductTotalSync = salesByproductTotalSync;
    }

    public long getSalesbyproductTotalFail() {
        return salesbyproductTotalFail;
    }

    public void setSalesbyproductTotalFail(long salesbyproductTotalFail) {
        this.salesbyproductTotalFail = salesbyproductTotalFail;
    }

    public String getExpenseSyncLastDate() {
        return expenseSyncLastDate;
    }

    public void setExpenseSyncLastDate(String expenseSyncLastDate) {
        this.expenseSyncLastDate = expenseSyncLastDate;
    }

    public long getExpenseTotalSync() {
        return expenseTotalSync;
    }

    public void setExpenseTotalSync(long expenseTotalSync) {
        this.expenseTotalSync = expenseTotalSync;
    }

    public long getExpenseTotalFail() {
        return expenseTotalFail;
    }

    public void setExpenseTotalFail(long expenseTotalFail) {
        this.expenseTotalFail = expenseTotalFail;
    }

    public String getCcClearance() {
        return cc_clearance;
    }

    public void setCcClearance(String ccClearance) {
        this.cc_clearance = ccClearance;
    }

    public String getCc_clearance() {
        return cc_clearance;
    }

    public void setCc_clearance(String cc_clearance) {
        this.cc_clearance = cc_clearance;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public boolean isAccounts() {
        return accounts;
    }

    public void setAccounts(boolean accounts) {
        this.accounts = accounts;
    }

    public QuickbookCustomEntities.QbCustomAccount getQbCustomAccount() {
        return qbCustomAccount;
    }

    public void setQbCustomAccount(QuickbookCustomEntities.QbCustomAccount qbCustomAccount) {
        this.qbCustomAccount = qbCustomAccount;
    }

    public List<QBDesktopAccounts> getQbDesktopAccounts() {
        return qbDesktopAccounts;
    }

    public void setQbDesktopAccounts(List<QBDesktopAccounts> qbDesktopAccounts) {
        this.qbDesktopAccounts = qbDesktopAccounts;
    }

    public boolean isSyncCustomAccount() {
        return syncCustomAccount;
    }

    public void setSyncCustomAccount(boolean syncCustomAccount) {
        this.syncCustomAccount = syncCustomAccount;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }
}
