package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;
import org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public final class SalesReceiptHelper {
    @Inject
    ItemHelper itemHelper;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    AccountHelper accountHelper;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookEntityRepository quickbookEntityRepository;
    @Inject
    PaymentHelper paymentHelper;


    //Sync Blaze Sales Receipt into Quickbooks
    public void syncSalesReceipts(DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {

        long endTime = DateTime.now().getMillis();
        long currentTime = DateTime.now().getMillis();

        Iterable<com.fourtwenty.core.domain.models.transaction.Transaction> transactionListDetails = null;
        Iterable<QuickbookSyncDetails> syncSalesReceiptList = quickbookSyncDetailsRepository.findByEntityType
                (blazeCompanyId, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesReceipt, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncSaleseceipt = Lists.newLinkedList(syncSalesReceiptList);
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
        List<Transaction> transactionList = null;

        if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_ALL_DATA) && syncSaleseceipt.isEmpty()) {
            transactionListDetails = transactionRepository.getTransactionByTypeAndStatus(blazeCompanyId, shopId,
                    Transaction.TransactionType.Sale, Transaction.TransactionStatus.Completed);

            transactionList = Lists.newArrayList(transactionListDetails);
        } else if (entities.getSyncStrategy().equalsIgnoreCase(QBConstants.SYNC_FROM_CURRENT_DATE) && syncSaleseceipt.isEmpty()) {
            quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, currentTime, endTime,
                    shopId, QuickbookSyncDetails.QuickbookEntityType.SalesReceipt, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);

        } else {

            QuickbookSyncDetails quickbookSyncSalesReceipt = syncSaleseceipt.getLast();
            endTime = quickbookSyncSalesReceipt.getEndTime();
            long startDate = endTime - 864000000l;
            List<Transaction> salesReceiptListWithoutQbRef = transactionRepository.getSalesOrdersListWithoutQbRef
                    (blazeCompanyId, shopId, startDate, endTime);
            Iterable<Transaction> transactionIterable = transactionRepository.listByShopWithDateAndStatus(blazeCompanyId, shopId, endTime, currentTime, Transaction.TransactionType.Sale, Transaction.TransactionStatus.Completed);

            transactionList = Lists.newArrayList(transactionIterable);
            transactionList.addAll(salesReceiptListWithoutQbRef);
        }

        try {
            if (transactionList != null) {
                LOG.info("Transaction List Size: " + transactionList.size());

                Iterator<Transaction> transactionIterator = transactionList.iterator();
                syncSalesReceiptBatch(service, transactionIterator, blazeCompanyId, quickbookCompanyId, shopId, endTime, transactionList.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while calling create sales receipt :");
        }

    }

    //Convert Blaze Sale Receipt into Quickbooks
    public List<SalesReceipt> convertBlazeSalesReceiptIntoQB(Iterator<Transaction> transactionIterator, DataService service, String quickbookCompanyId, String blazeCompanyId, String shopId) {
        double exciseTax = 0;
        double cityTax = 0;
        double countyTax = 0;
        double stateTax = 0;
        double federalTax = 0;
        double totalTax = 0;
        int counter = 0;
        double preALExciseTax = 0.0;
        double preNALExciseTax = 0.0;
        double postALExciseTax = 0.0;
        double postNALExciseTax = 0.0;

        List<SalesReceipt> resultSalesReceiptLst = new ArrayList<>();
        while ((counter < 30) && (transactionIterator.hasNext())) {
            Transaction transaction = transactionIterator.next();

            SalesReceipt qbSalesReceipt = new SalesReceipt();
            List<Line> linesList = qbLinesList(transaction, service, blazeCompanyId, shopId);
            TxnTaxDetail txnTaxDetail = new TxnTaxDetail();
            qbSalesReceipt.setLine(linesList);

            if (transaction.getCart().getTaxResult() != null) {
                TaxResult taxResult = transaction.getCart().getTaxResult();
                cityTax = NumberUtils.round(taxResult.getTotalCityTax().doubleValue(), 2);
                countyTax = NumberUtils.round(taxResult.getTotalCountyTax().doubleValue(), 2);
                stateTax = NumberUtils.round(taxResult.getTotalStateTax().doubleValue(), 2);
                federalTax = NumberUtils.round(taxResult.getTotalFedTax().doubleValue(), 2);

                preALExciseTax += transaction.getCart().getTaxResult().getTotalALExciseTax().doubleValue();
                preNALExciseTax += transaction.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue();
                postALExciseTax += transaction.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax += transaction.getCart().getTaxResult().getTotalExciseTax().doubleValue();


            }

            double totalAmount = 0.0;
            totalAmount = transaction.getCart().getTotal().doubleValue();
            totalTax = NumberUtils.round(cityTax, 2) + NumberUtils.round(countyTax, 2) + NumberUtils.round(stateTax, 2) + federalTax;

            exciseTax = NumberUtils.round(preALExciseTax, 2) + NumberUtils.round(preNALExciseTax, 2) +
                    NumberUtils.round(postALExciseTax, 2) + NumberUtils.round(postNALExciseTax, 2);

            txnTaxDetail.setTotalTax(new BigDecimal(totalTax));
            qbSalesReceipt.setTotalAmt(new BigDecimal(totalAmount));
            qbSalesReceipt.setTxnTaxDetail(txnTaxDetail);
            qbSalesReceipt.setDocNumber(transaction.getTransNo());
            qbSalesReceipt.setPrivateNote(transaction.getQbSalesReceiptRef());


            //Set custom field-consumer type for sales receipt
            CustomField customField = new CustomField();
            customField.setDefinitionId("1");
            customField.setName("Consumer Type");
            customField.setType(CustomFieldTypeEnum.STRING_TYPE);
            customField.setStringValue(transaction.getCart().getConsumerType().getDisplayName());

            List<CustomField> customFieldList = new ArrayList<CustomField>();
            customFieldList.add(customField);
            qbSalesReceipt.setCustomField(customFieldList);

            //Set Payment method to sales Receipt
            PaymentMethod paymentMethod = paymentHelper.getPaymentMethod(service, transaction.getCart().getPaymentOption().toString());
            qbSalesReceipt.setPaymentMethodRef(paymentHelper.getPaymentMethodRef(paymentMethod));

            Long nowMillis = transaction.getCreated();
            DateTime jodatime = new DateTime(nowMillis);
            qbSalesReceipt.setTxnDate(jodatime.toDate());


            if (transaction != null) {
                if (transaction.getDeliveryAddress() != null) {
                    PhysicalAddress billAddress = new PhysicalAddress();
                    if (transaction.getDeliveryAddress().getCity() != null) {
                        billAddress.setCity(transaction.getDeliveryAddress().getCity());
                    }
                    if (transaction.getDeliveryAddress().getCountry() != null) {
                        billAddress.setCountry(transaction.getDeliveryAddress().getCountry());
                    }
                    if (transaction.getDeliveryAddress().getZipCode() != null) {
                        billAddress.setPostalCode(transaction.getDeliveryAddress().getZipCode());
                    }
                    qbSalesReceipt.setBillAddr(billAddress);
                }
            }

            String taxSeperation = "Total City Tax: " + "$ " + cityTax + "\n" + "Total County Tax: " + "$ " + countyTax + "\n" + "Total State Tax:" + "$ "
                    + stateTax + "\n" + "Total Federal Tax: " + "$ " + federalTax + "\n" + "Total Tax: " + "$ " + totalTax;

            LOG.info("Total Tax Separation: " + taxSeperation);

            if (totalTax > 0) {
                MemoRef memoRef = new MemoRef();
                memoRef.setValue(taxSeperation);
                qbSalesReceipt.setCustomerMemo(memoRef);
            }

            if (transaction.getTransNo() != null) {
                qbSalesReceipt.setPaymentRefNum(transaction.getTransNo());
            }

            Member memberDetails = memberRepository.get(transaction.getCompanyId(), transaction.getMemberId());
            if (memberDetails != null) {
                ReferenceType customerRef = new ReferenceType();
                customerRef.setName(memberDetails.getFirstName());

                LOG.info("Customer Ref:  " + memberDetails.getQbCustomerRef());
                List<HashMap<String, String>> getQbrefMaps = memberDetails.getQbCustomerRef();
                for (HashMap<String, String> getQbrefmap : getQbrefMaps) {
                    LOG.info("Check Ref In DB" + getQbrefmap.get(quickbookCompanyId));
                    if (getQbrefmap.get(quickbookCompanyId) != null) {
                        customerRef.setValue(getQbrefmap.get(quickbookCompanyId));
                    }
                }

                qbSalesReceipt.setCustomerRef(customerRef);
            }

            EmailAddress emailAddress = new EmailAddress();
            if (memberDetails.getEmail() != null) {
                emailAddress.setAddress(memberDetails.getEmail());
            }
            qbSalesReceipt.setBillEmail(emailAddress);
            try {
                QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);
                if (accountType != null) {
                    LOG.info("Sales  Receipt checking Account Type:-" + accountType.getChecking());
                    Account depositAccount = accountHelper.getAccount(service, accountType.getChecking());
                    qbSalesReceipt.setDepositToAccountRef(accountHelper.getAccountRef(depositAccount));
                }

                resultSalesReceiptLst.add(qbSalesReceipt);
                counter++;
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling entity  Sales Receipt:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::"
                            + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.
                                findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            }

        }


        return resultSalesReceiptLst;
    }

    //Add Line item of carts
    public List<Line> qbLinesList(Transaction transaction, DataService service, String blazecompanyId, String shopId) {
        System.out.println("blazeCompanyId2:" + blazecompanyId + " " + shopId);
        List<Line> linesList = new ArrayList<Line>();
        BigDecimal quantity = QBConstants.QUANTITY_MIN;
        double totalCalcTax = transaction.getCart().getTotalCalcTax().doubleValue();
        double totalPreTaxes = transaction.getCart().getTotalPreCalcTax().doubleValue();
        double totalPostTaxes = totalCalcTax - totalPreTaxes;
        if (totalPostTaxes < 0) {
            totalPostTaxes = 0;
        }
        double roundTotal = 0.0;
        double totalAmount = 0.0;
        for (OrderItem orderItem : transaction.getCart().getItems()) {

            Line lineObject = new Line();
            lineObject.setAmount(orderItem.getCost());
            lineObject.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
            SalesItemLineDetail salesItemLineDetail1 = new SalesItemLineDetail();
            Product productDetail = productRepository.get(orderItem.getCompanyId(), orderItem.getProductId());
            ReferenceType itemref = new ReferenceType();
            if (productDetail.getQbItemRef() != null) {
                itemref.setValue(productDetail.getQbItemRef());
                if (productDetail != null) {
                    itemref.setName(productDetail.getName());
                }
                salesItemLineDetail1.setItemRef(itemref);

            }
            double unitPrice = orderItem.getCost().doubleValue() / orderItem.getQuantity().doubleValue();
            salesItemLineDetail1.setQty(orderItem.getQuantity());
            if (unitPrice > 0) {
                salesItemLineDetail1.setUnitPrice(new BigDecimal(unitPrice));
            } else {
                salesItemLineDetail1.setUnitPrice(QBConstants.QUANTITY_MIN);
            }
            ReferenceType taxcode = new ReferenceType();
            taxcode.setValue(QBConstants.TAX);
            salesItemLineDetail1.setTaxCodeRef(taxcode);
            lineObject.setSalesItemLineDetail(salesItemLineDetail1);
            lineObject.setDescription(productDetail.getDescription());
            linesList.add(lineObject);

            //Discount Amount
            Line lineDetails = new Line();
            lineDetails.setDetailType(LineDetailTypeEnum.DISCOUNT_LINE_DETAIL);
            DiscountLineDetail discountLineDetail = new DiscountLineDetail();
            discountLineDetail.setPercentBased(false);
            lineDetails.setAmount(transaction.getCart().getTotalDiscount());
            lineDetails.setDiscountLineDetail(discountLineDetail);
            linesList.add(lineDetails);

            LOG.info("Cart Subtotal:" + transaction.getCart().getSubTotal());
            LOG.info("Delivery Amount: " + transaction.getCart().getDeliveryFee());
            LOG.info("Total tax Amount: " + transaction.getCart().getTotalCalcTax());
            LOG.info("Total Discount amount: " + transaction.getCart().getTotalDiscount());


        }

        roundTotal = transaction.getCart().getSubTotalDiscount().doubleValue() + totalPostTaxes + transaction.getCart().getDeliveryFee().doubleValue();
        totalAmount = transaction.getCart().getTotal().doubleValue();

        //Shipping
        double deliverycharge = transaction.getCart().getDeliveryFee().doubleValue();
        double roundAmt = totalAmount - roundTotal;
        double roundoffvalue = NumberUtils.round(roundAmt, 2);
        if (roundoffvalue <= 0.03 && roundoffvalue >= -0.03) {
            deliverycharge = transaction.getCart().getDeliveryFee().doubleValue() + roundoffvalue;
        }
        //Delivery Charge
        QuickbookCustomEntities accountType = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
        SalesItemLineDetail deliveryItemLineDetail1 = new SalesItemLineDetail();

        ReferenceType deliveryItemref = new ReferenceType();
        QuickbookEntity deliveryEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, QBConstants.DELIVERY_CHARGE.trim());
        GzipHttpOutputInterceptor.LOG.info("quickbookenity:" + deliveryEntity);

        if (deliveryEntity == null) {
            Item deliveryItem = ItemHelper.createDeliveryItem(service, accountType);
            if (deliveryItem != null) {
                quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, deliveryItem.getId(), QBConstants.DELIVERY_CHARGE.trim());
                GzipHttpOutputInterceptor.LOG.info("deliveryItem: " + deliveryItem.getId());
                deliveryItemref.setName(deliveryItem.getName());
                deliveryItemref.setValue(deliveryItem.getId());
                deliveryItemLineDetail1.setItemRef(deliveryItemref);
            }

        } else {
            if (deliveryEntity.getProductRef() != null) {
                deliveryItemref.setName(deliveryEntity.getProductName());
                deliveryItemref.setValue(deliveryEntity.getProductRef());
                deliveryItemLineDetail1.setItemRef(deliveryItemref);
            }

        }


        Line lineShipping = new Line();
        lineShipping.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
        deliveryItemLineDetail1.setUnitPrice(new BigDecimal(deliverycharge));
        deliveryItemLineDetail1.setQty(QBConstants.UNIT_PRICE);
        lineShipping.setSalesItemLineDetail(deliveryItemLineDetail1);
        lineShipping.setAmount(new BigDecimal(deliverycharge));
        linesList.add(lineShipping);


        //Excise Tax Item
        SalesItemLineDetail exciseTaxItemDetails = new SalesItemLineDetail();
        ReferenceType taxItemRef = new ReferenceType();

        QuickbookEntity quickbookEntity = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, QBConstants.TOTAL_EXCISE_TAX.trim());
        QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazecompanyId, shopId);
        GzipHttpOutputInterceptor.LOG.info("quickbookenity:" + quickbookEntity);

        if (quickbookEntity == null) {
            Item taxItem = ItemHelper.createExciseTaxItem(service, entities);
            if (taxItem != null) {
                quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, taxItem.getId(), QBConstants.TOTAL_EXCISE_TAX.trim());
                GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + taxItem.getId());
                taxItemRef.setName(taxItem.getName());
                taxItemRef.setValue(taxItem.getId());
                exciseTaxItemDetails.setItemRef(taxItemRef);
            }

        } else {
            if (quickbookEntity.getProductRef() != null) {
                taxItemRef.setName(quickbookEntity.getProductName());
                taxItemRef.setValue(quickbookEntity.getProductRef());
                exciseTaxItemDetails.setItemRef(taxItemRef);
            }

        }
        double preALExciseTax = 0.0;
        double preNALExciseTax = 0.0;
        double postALExciseTax = 0.0;
        double postNALExciseTax = 0.0;
        double exciseTax = 0.0;

        if (transaction.getCart().getTaxResult() != null) {
            preALExciseTax += transaction.getCart().getTaxResult().getTotalALExciseTax().doubleValue();
            preNALExciseTax += transaction.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue();
            postALExciseTax += transaction.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();
            postNALExciseTax += transaction.getCart().getTaxResult().getTotalExciseTax().doubleValue();
        }
        exciseTax = NumberUtils.round(preALExciseTax, 2) + NumberUtils.round(preNALExciseTax, 2) + NumberUtils.round(postALExciseTax, 2) + NumberUtils.round(postNALExciseTax, 2);
        double preTax = NumberUtils.round(preALExciseTax, 2) + NumberUtils.round(preNALExciseTax, 2);
        LOG.info("Excise Tax:  " + exciseTax);
        LOG.info("Pre Excise Tax:  " + preTax);
        Line totalExciseLine = new Line();
        totalExciseLine.setAmount(new BigDecimal(exciseTax));
        totalExciseLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
        exciseTaxItemDetails.setUnitPrice(new BigDecimal(exciseTax));
        exciseTaxItemDetails.setQty(QBConstants.UNIT_PRICE);
        totalExciseLine.setSalesItemLineDetail(exciseTaxItemDetails);
        linesList.add(totalExciseLine);

        //Excise Tax Reducer Item
        SalesItemLineDetail exciseTaxIReducertemDetails = new SalesItemLineDetail();
        ReferenceType taxReducerItemRef = new ReferenceType();

        QuickbookEntity quickbookEntityDetails = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, QBConstants.PRE_EXCISE_TAX_REDUCER);
        GzipHttpOutputInterceptor.LOG.info("quickbookenity tax reducer:" + quickbookEntity);

        if (quickbookEntityDetails == null) {
            Item taxItemDetails = ItemHelper.createExciseTaxReducerItem(service, accountType);
            if (taxItemDetails != null) {
                quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, taxItemDetails.getId(), QBConstants.PRE_EXCISE_TAX_REDUCER);
                GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + taxItemDetails.getId());
                taxReducerItemRef.setName(taxItemDetails.getName());
                taxReducerItemRef.setValue(taxItemDetails.getId());
                exciseTaxIReducertemDetails.setItemRef(taxReducerItemRef);
            }

        } else {
            if (quickbookEntityDetails.getProductRef() != null) {
                taxReducerItemRef.setName(quickbookEntityDetails.getProductName());
                taxReducerItemRef.setValue(quickbookEntityDetails.getProductRef());
                exciseTaxIReducertemDetails.setItemRef(taxReducerItemRef);
            }

        }

        Line totalExcisetaxLine = new Line();
        totalExcisetaxLine.setAmount(new BigDecimal(-preTax));
        totalExcisetaxLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
        exciseTaxIReducertemDetails.setUnitPrice(new BigDecimal((-preTax)));
        exciseTaxIReducertemDetails.setQty(QBConstants.UNIT_PRICE);
        totalExcisetaxLine.setSalesItemLineDetail(exciseTaxIReducertemDetails);
        linesList.add(totalExcisetaxLine);


        //Credit card fee Line Item
        SalesItemLineDetail creditcardFees = new SalesItemLineDetail();
        ReferenceType cardFeeRef = new ReferenceType();
        QuickbookEntity creditCardfeeItem = quickbookEntityRepository.getQbEntityByName(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, QBConstants.CREDIT_CARD_FEES);
        GzipHttpOutputInterceptor.LOG.info("Credit card fee Item:" + quickbookEntity);

        if (creditCardfeeItem == null) {
            Item cardFeeItemDetails = ItemHelper.createCreditCardFees(service, accountType);
            if (cardFeeItemDetails != null) {
                quickbookEntityRepository.saveQuickbookEntity(blazecompanyId, shopId, QuickbookEntity.SyncStrategy.ProductRef, cardFeeItemDetails.getId(), QBConstants.CREDIT_CARD_FEES);
                GzipHttpOutputInterceptor.LOG.info("TaxItemId: " + cardFeeItemDetails.getId());

                cardFeeRef.setName(cardFeeItemDetails.getName());
                cardFeeRef.setValue(cardFeeItemDetails.getId());
                creditcardFees.setItemRef(cardFeeRef);
            }

        } else {
            if (creditCardfeeItem.getProductRef() != null) {
                cardFeeRef.setName(creditCardfeeItem.getProductName());
                cardFeeRef.setValue(creditCardfeeItem.getProductRef());
                creditcardFees.setItemRef(cardFeeRef);
            }

        }

        Line creditcardfeeLine = new Line();
        creditcardfeeLine.setAmount(transaction.getCart().getCreditCardFee());
        creditcardfeeLine.setDetailType(LineDetailTypeEnum.SALES_ITEM_LINE_DETAIL);
        creditcardFees.setUnitPrice(transaction.getCart().getCreditCardFee());
        creditcardFees.setQty(QBConstants.UNIT_PRICE);
        creditcardfeeLine.setSalesItemLineDetail(creditcardFees);
        linesList.add(creditcardfeeLine);


        return linesList;

    }

    //Sync Batch
    public void syncSalesReceiptBatch(DataService service, Iterator<Transaction> transactions, String blazeCompanyId, String quickbookCompanyId, String shopId, long endTime, int total) {
        BatchOperation batchOperation = null;
        long startTime = DateTime.now().getMillis();
        int totalSuccess = 0;
        int counter = 0;
        while (transactions.hasNext()) {
            List<SalesReceipt> qbSalesReceipt = convertBlazeSalesReceiptIntoQB(transactions, service, quickbookCompanyId, blazeCompanyId, shopId);
            LOG.info("SalesReceipt Batch Size: " + qbSalesReceipt.size());
            batchOperation = new BatchOperation();
            for (SalesReceipt salesReceipt : qbSalesReceipt) {

                counter++;
                if (salesReceipt.getPrivateNote() == null) {
                    batchOperation.addEntity(salesReceipt, OperationEnum.CREATE, "bID" + counter);
                    LOG.info("create Sales Receipt: ");

                } else {
                    totalSuccess++;
                }

            }
            try {
                //Execute Batch
                service.executeBatch(batchOperation);
                Thread.sleep(2000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());
                        }

                    }

                    SalesReceipt salesReceipt = (SalesReceipt) batchOperation.getEntity(bId);
                    if (salesReceipt != null) {
                        totalSuccess++;
                        String salesReceiptId = salesReceipt.getDocNumber();
                        Transaction transactionRepositoryorDetails = transactionRepository.getTransactionByTransNo(blazeCompanyId, shopId, salesReceiptId);
                        LOG.info("Sales Receipt inside result: " + transactionRepositoryorDetails.getTransNo());

                        if (transactionRepositoryorDetails != null) {
                            LOG.info("Reference saved :" + salesReceipt.getId());
                            BasicDBObject updateQuery = new BasicDBObject();
                            updateQuery.append("$set", new BasicDBObject().append("qbSalesReceiptRef", salesReceipt.getId()));
                            BasicDBObject searchQuery = new BasicDBObject();
                            searchQuery.append("_id", new ObjectId(transactionRepositoryorDetails.getId()));
                            transactionRepository.updateSalesReceiptRef(searchQuery, updateQuery);
                            LOG.info("Add Sales Receipt Reference in DB");

                        }

                    }
                }
            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling entity add sales receipt:: " + error.getMessage() + "Details:::" + error.getDetail() + "status::::"
                            + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {
                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.
                                findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId, QuickbookSyncDetails.QuickbookEntityType.SalesReceipt, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }

    //Get Batch
    public static List<SalesReceipt> getSaleReceiptBatch(List<SalesReceipt> myList, int startIndex) {
        return myList.subList(startIndex, Math.min(myList.size(), startIndex + 30));
    }


}