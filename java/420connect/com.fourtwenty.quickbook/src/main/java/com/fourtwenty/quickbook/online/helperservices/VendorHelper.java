package com.fourtwenty.quickbook.online.helperservices;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.data.*;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.BatchOperation;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.util.DateUtils;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.apache.commons.lang.RandomStringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.*;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;

public final class VendorHelper {
    @Inject
    VendorRepository vendorRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    //Get All data & sync vendors
    public void syncVendors(DataService service, String blazeCompanyId, String shopId, String quickbookCompanyId) {
        long endTime = DateTime.now().getMillis();
        long currenTime = DateTime.now().getMillis();

        List<com.fourtwenty.core.domain.models.product.Vendor> vendorList;
        Iterable<com.fourtwenty.core.domain.models.product.Vendor> vendorListDetails;

        Iterable<QuickbookSyncDetails> syncVendorsList = quickbookSyncDetailsRepository.findByEntityType(blazeCompanyId,
                shopId, QuickbookSyncDetails.QuickbookEntityType.Vendor, QBConstants.QUICKBOOK_ONLINE);

        LinkedList<QuickbookSyncDetails> syncVendors = Lists.newLinkedList(syncVendorsList);

        if (syncVendors.isEmpty()) {
            vendorListDetails = vendorRepository.listAll(blazeCompanyId);
            vendorList = Lists.newArrayList(vendorListDetails);
        } else {
            QuickbookSyncDetails quickbookSyncvendors = syncVendors.getLast();
            endTime = quickbookSyncvendors.getEndTime();
            long startDate = endTime - 864000000l;

            List<com.fourtwenty.core.domain.models.product.Vendor> vendorListwithoutQbRef = vendorRepository.getVendorListWithoutQbRef
                    (blazeCompanyId, startDate, endTime);
            vendorListDetails = vendorRepository.listWithDate(blazeCompanyId, endTime, currenTime);

            vendorList = Lists.newArrayList(vendorListDetails);
            vendorList.addAll(vendorListwithoutQbRef);
        }

        try {
            if (vendorList != null) {
                LOG.info("Blaze Vendor Count: " + vendorList.size());
                Iterator<com.fourtwenty.core.domain.models.product.Vendor> vendorIterator = vendorList.iterator();
                syncVendorBatch(service, vendorIterator, blazeCompanyId, quickbookCompanyId, shopId, endTime, vendorList.size());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.info("Error while creating Vendors: ");
        }
    }

    //Convert Blaze Vendor into Quickbook
    public List<Vendor> convertBlazeVendorIntoQB(Iterator<com.fourtwenty.core.domain.models.product.Vendor> vendorList, int counts) throws ParseException {
        int count = counts * 30 + 1;
        List<Vendor> venderDetails = new ArrayList<Vendor>();
        int counter = 0;
        while ((counter < 30) && (vendorList.hasNext())) {
            com.fourtwenty.core.domain.models.product.Vendor vendorDetail = vendorList.next();
            // Mandatory Fields
            Vendor qbVendor = new Vendor();
            if (vendorDetail.getName() != null) {
                LOG.info("qbvendor company NAME: " + vendorDetail.getName());
                String displayName = vendorDetail.getName() + " " + count;
                if (displayName.length() < 16) {
                    qbVendor.setDisplayName(displayName);
                } else {
                    String name = vendorDetail.getFirstName() + " " + org.apache.commons.lang.RandomStringUtils.randomNumeric(4);
                    qbVendor.setDisplayName(name);
                }
                // Optional Fields
                qbVendor.setCompanyName(vendorDetail.getName());
                qbVendor.setTitle(vendorDetail.getFirstName());
                qbVendor.setGivenName(vendorDetail.getFirstName());
                qbVendor.setMiddleName(RandomStringUtils.randomAlphanumeric(1));
                qbVendor.setFamilyName(vendorDetail.getLastName());
                qbVendor.setBusinessNumber(vendorDetail.getId());

            }

            PhysicalAddress address = new PhysicalAddress();
            if (vendorDetail.getAddress() != null) {
                if (vendorDetail.getAddress().getCity() != null) {
                    address.setCity(vendorDetail.getAddress().getCity());
                    address.setCountry(vendorDetail.getAddress().getCountry());
                    address.setCountryCode(vendorDetail.getAddress().getZipCode());
                }

                qbVendor.setBillAddr(address);
            }


            TelephoneNumber fax = new TelephoneNumber();
            if (vendorDetail.getFax() != null) {
                fax.setFreeFormNumber(vendorDetail.getFax());
                qbVendor.setFax(fax);
            }


            TelephoneNumber primaryNum = new TelephoneNumber();
            if (vendorDetail.getPhone() != null) {
                primaryNum.setFreeFormNumber(vendorDetail.getPhone());
                qbVendor.setPrimaryPhone(primaryNum);
            }
            qbVendor.setOpenBalanceDate(DateUtils.getCurrentDateTime());
            venderDetails.add(qbVendor);
            count++;
            counter++;
        }
        return venderDetails;
    }


    //sync vendor Details In QB
    public void syncVendorBatch(DataService service, Iterator<com.fourtwenty.core.domain.models.product.Vendor> blazeVendorList,
                                String blazeCompanyId, String quickbookCompanyId, String shopId, long endTime, int total) throws Exception {
        BatchOperation batchOperation = null;
        int totalSuccess = 0;
        long startTime = DateTime.now().getMillis();
        int count = 1;
        while (blazeVendorList.hasNext()) {
            List<Vendor> qbVendor = convertBlazeVendorIntoQB(blazeVendorList, count);
            count++;
            try {
                int counter = 0;
                LOG.info("Vendor Batch Size:" + qbVendor.size());
                batchOperation = new BatchOperation();
                Map<String, com.fourtwenty.core.domain.models.product.Vendor> vendorMap = new HashMap<>();

                for (Vendor vendor : qbVendor) {
                    counter++;

                    String vendorRef = null;
                    com.fourtwenty.core.domain.models.product.Vendor vendorDetails = vendorRepository.get(blazeCompanyId, vendor.getBusinessNumber());


                    List<HashMap<String, String>> getQbrefMaps = vendorDetails.getQbVendorRef();
                    for (HashMap<String, String> getQbrefmap : getQbrefMaps) {
                        vendorRef = getQbrefmap.get(quickbookCompanyId);
                    }

                    if (vendorRef == null) {
                        batchOperation.addEntity(vendor, OperationEnum.CREATE, "bid" + counter);
                        vendorMap.put("bid" + counter, vendorDetails);
                        LOG.info("create Vendor: ");
                    } else {
                        totalSuccess++;
                    }

                }

                service.executeBatch(batchOperation);
                Thread.sleep(1000);
                List<String> bIds = batchOperation.getBIds();
                for (String bId : bIds) {
                    LOG.info("In AsyncCallBackFind callback..." + bId);
                    if (batchOperation.isFault(bId)) {
                        Fault fault = batchOperation.getFault(bId);
                        // fault has a list of errors
                        for (com.intuit.ipp.data.Error error : fault.getError()) {
                            LOG.info("errror message:" + error.getMessage() + "error Details: " + error.getDetail() + " error code: " + error.getCode());

                        }

                    }
                    try {
                        com.fourtwenty.core.domain.models.product.Vendor vendorDetails = vendorMap.get(bId);
                        Vendor singleVendor = (Vendor) batchOperation.getEntity(bId);
                        if (singleVendor != null) {
                            totalSuccess++;
                            LOG.info("vendorDetails Inside result: " + vendorDetails);
                            if (vendorDetails != null) {
                                LOG.info("Vendor Reference Saved:" + singleVendor.getId());
                                // //Update Quickbook Ref into vendors table in Blaze
                                HashMap<String, String> saveQbrefMap = new HashMap<String, String>();
                                saveQbrefMap.put(quickbookCompanyId, singleVendor.getId());
                                BasicDBObject updateQuery = new BasicDBObject();
                                updateQuery.append("$push", new BasicDBObject().append("qbVendorRef", saveQbrefMap));
                                BasicDBObject searchQuery = new BasicDBObject();
                                searchQuery.append("_id", new ObjectId(vendorDetails.getId()));
                                WriteResult writeResult = vendorRepository.updateVendorRef(searchQuery, updateQuery);
                                LOG.info("Add Vendor Reference in DB: ");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (FMSException e) {
                for (com.intuit.ipp.data.Error error : e.getErrorList()) {
                    LOG.info("Error while calling Create vendor:: " + error.getMessage() + "Details:: " + error.getDetail() + "statusCode: " + error.getCode());
                    //Get Access token if Access token Expires after an hour
                    if (error.getCode().equals(QBConstants.ERRORCODE)) {

                        LOG.info("Inside 3200 code");
                        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId,
                                shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountsDetail.getShopId());
                        service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountsDetail.getQuickbook_companyId());
                    }
                }
            }
        }
        int totalFail = total - totalSuccess;
        quickbookSyncDetailsRepository.saveQuickbookEntity(total, totalSuccess, totalFail, blazeCompanyId, endTime, startTime, shopId,
                QuickbookSyncDetails.QuickbookEntityType.Vendor, QuickbookSyncDetails.Status.Completed, QBConstants.QUICKBOOK_ONLINE);


    }

}