package com.fourtwenty.quickbook.online.quickbookservices;

import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.ErrorLogs;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookEnabledRequest;
import com.fourtwenty.quickbook.models.QWCPassword;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.desktop.request.QBDesktopOperationRequest;
import com.fourtwenty.quickbook.desktop.response.QuickBookDesktopResponse;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.online.helperservices.QuickbookSettings;

import java.util.List;

public interface QuickbookService {
    String getAuthorizationCode();

    Object getRefreshToken(String authCode, String companyId, String quickbook_companyId);

    Object syncCustomQuickbookEntities();

    Object disconnectQuickbook(String qbType);

    QuickbookCustomEntities saveQuickbookCustomEntities(QuickbookCustomEntities quickbookCustomEntities);

    QuickbookCustomEntities getQuickbookEntity();

    public QuickbookSettings getAllDetailEntityReponse();

    QuickbookSettings getQuickbooksSettings();

    public String getQwcFileURL(QWCPassword qwcPassword);

    QuickBookDesktopResponse getQuickBookDesktopOperations();

    List<QBDesktopOperation> updateQuickBookDesktopOperations(List<QBDesktopOperationRequest> request);

    SearchResult<QuickbookSyncDetails> getLimitedQuickBookSyncDetails(int start, int limit);

    SearchResult<ErrorLogs> getLimitedQuickBookErrorLogs(int start, int limit, String reference);

    QBDesktopOperation updateSyncStatus(String operationId, StatusRequest request);

    QuickBookAccountDetails saveQuickBookAccountDetails(QuickBookAccountDetails quickBookAccountDetails);

    QuickBookAccountDetails updateQuickBookAccountDetails(String referenceId, QuickBookAccountDetails quickBookAccountDetails);

    QuickBookAccountDetails getQuickBookAccountDetailsByReferenceId(String referenceId);

    void hardResetQuickBookData();

    String getEnabledQuickBook(QuickBookEnabledRequest quickBookEnabledRequest);

}
