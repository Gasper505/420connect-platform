package com.fourtwenty.quickbook.online.quickbookservices;

import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.intuit.ipp.services.DataService;

public interface QuickbookSync {
    Object syncCustomQuickbookEntities();

    DataService getService(String accesstoken, String quickbookCompanyId);

    BearerTokenResponse getNewRefreshToken(String refreshToken);

    public BearerTokenResponse getAccestoken();

    BearerTokenResponse getAccessTokenByShop(String shopId);


}
