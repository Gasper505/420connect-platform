package com.fourtwenty.quickbook.online.quickbookservices.impl;

import com.esotericsoftware.minlog.Log;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.AccountInfo;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.ErrorLogs;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopField;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookEnabledRequest;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.quickbook.models.QWCPassword;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.fourtwenty.quickbook.repositories.QbAccountRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.fourtwenty.quickbook.repositories.QuickBookSessionDataRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookDesktopAccountRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.quickbook.QuickBookDefaultDataEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.quickbook.desktop.request.QBDesktopOperationRequest;
import com.fourtwenty.quickbook.desktop.response.QuickBookDesktopOperationResponse;
import com.fourtwenty.quickbook.desktop.response.QuickBookDesktopResponse;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.quickbook.online.helperservices.AccountHelper;
import com.fourtwenty.quickbook.online.helperservices.OauthHelper;
import com.fourtwenty.quickbook.online.helperservices.QBConstants;
import com.fourtwenty.quickbook.online.helperservices.QuickbookSettings;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookService;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.intuit.ipp.services.DataService;
import com.mongodb.WriteResult;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;


public class QuickbookServiceimpl extends AbstractAuthServiceImpl implements QuickbookService {

    private static final int Sync_Details_Limit = 10;
    private static final int Error_Logs_Limit = 10;
    @Inject
    BearerTokenResponse bearerTokenResponse;
    @Inject
    private ConnectConfiguration configuration;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    @Inject
    public QuickbookServiceimpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    OauthHelper oauthHelper;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    AccountHelper accountHelper;
    @Inject
    ShopRepository shopRepository;
    @Inject
    QuickbookService service;
    @Inject
    ProductRepository productRepository;
    @Inject
    AmazonS3Service amazonS3Service;
    @Inject
    QuickbookDesktopAccountRepository quickbookDesktopAccountRepository;
    @Inject
    private QbDesktopOperationRepository qbDesktopOperationRepository;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private QbAccountRepository qbAccountRepository;
    @Inject
    private ErrorLogsRepository errorLogsRepository;
    @Inject
    private QuickBookAccountDetailsRepository quickBookAccountDetailsRepository;

    @Inject
    private QbDesktopCurrentSyncRepository qbDesktopCurrentSyncRepository;
    @Inject
    private QBUniqueSequenceRepository qbUniqueSequenceRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private InvoiceRepository invoiceRepository;
    @Inject
    private InvoicePaymentsRepository invoicePaymentsRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private ShipmentBillRepository shipmentBillRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    private QuickBookSessionDataRepository quickBookSessionDataRepository;

    public String getAuthorizationCode() {
        String url = configuration.getQuickBookConfig().getIntuitAccountingAPIHost() + "/connect/oauth2";
        String sampleURL = url
                + "?client_id=" + configuration.getQuickBookConfig().getClientID()
                + "&response_type=code&scope=" + configuration.getQuickBookConfig().getScope()
                + "&redirect_uri=" + configuration.getQuickBookConfig().getRedirectURI()
                + "&state=" + token.getShopId();
        return sampleURL;
    }

    public Object getRefreshToken(String authCode, String shopId, String quickbook_companyId) {
        Shop shop = shopRepository.getById(shopId);
        BearerTokenResponse bearerTokenResponse = oauthHelper.getBearerTokenResponse(authCode, shopId);
        if (bearerTokenResponse != null) {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(shop.getCompanyId(), shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            LOG.info("Shop Id:  " + shop.getId() + "company Id: " + shop.getCompanyId());
            if (thirdPartyAccountsDetail != null) {
                thirdPartyAccountRepository.updteQuickbookDetails(thirdPartyAccountsDetail, bearerTokenResponse, shop.getCompanyId(), quickbook_companyId, thirdPartyAccountsDetail.getShopId());
            } else {
                thirdPartyAccountRepository.addQuickbookDetails(bearerTokenResponse, shop.getCompanyId(), quickbook_companyId, shopId);

            }
        }
        URI uri = UriBuilder.fromUri(configuration.getQuickBookConfig().getBaseUrl()).build();
        return Response.seeOther(uri).build();
    }


    public Object syncCustomQuickbookEntities() {
        Object obj = null;
        try {
            quickbookSync.syncCustomQuickbookEntities();
            obj = "true";
        } catch (Exception ex) {
            obj = "false";
        }

        return obj;
    }


    public Object disconnectQuickbook(String qbtypes) {
        Object obj = "Sucess";
        HttpResponse response = oauthHelper.revokeQuickbookAccess();

        if (StringUtils.isNotBlank(qbtypes) && qbtypes.equalsIgnoreCase(QBConstants.QUICKBOOK_DESKTOP)) {
            qbAccountRepository.deleteQBAccounts(token.getCompanyId(), token.getShopId());
            quickbookSyncDetailsRepository.removeQuickbookSyncDetailByStatus(token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.Status.Pending);
        } else {
            thirdPartyAccountRepository.deleteAccount(token.getCompanyId(), token.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            WriteResult result = quickbookSyncDetailsRepository.removeQuickbookSyncDetailByStatus(token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.Status.Inprogress);
        }

        quickbookCustomEntitiesRepository.deleteEntityDetails(token.getCompanyId(), token.getShopId());
        Log.info("Disconnect API Response::::" + response);
        return obj;
    }


    public QuickbookCustomEntities saveQuickbookCustomEntities(QuickbookCustomEntities quickbookCustomEntities) {
        quickbookCustomEntities.setCompanyId(token.getCompanyId());
        quickbookCustomEntities.setShopId(token.getShopId());

        if (!StringUtils.isNotBlank(quickbookCustomEntities.getQbtypes())) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook type can not be blank.");
        }

        if (quickbookCustomEntities.getQbtypes().equalsIgnoreCase("quickbook_desktop")) {
            List<QBDesktopOperation> quickBookDesktopOperations = qbDesktopOperationRepository.getQuickBookDesktopOperations(token.getCompanyId(), token.getShopId());
            if (quickBookDesktopOperations.size() == 0) {
                QuickBookDefaultDataEvent quickBookDefaultDataEvent = new QuickBookDefaultDataEvent();
                quickBookDefaultDataEvent.setCompanyId(token.getCompanyId());
                quickBookDefaultDataEvent.setShopId(token.getShopId());
                blazeEventBus.post(quickBookDefaultDataEvent);
            }
        }
        QuickbookCustomEntities getQuickbookeEntity = quickbookCustomEntitiesRepository.findQuickbookEntities(quickbookCustomEntities.getCompanyId(), quickbookCustomEntities.getShopId());
        if (getQuickbookeEntity != null) {
            if (StringUtils.isBlank(quickbookCustomEntities.getQbPassword())) {
                quickbookCustomEntities.setQbPassword(getQuickbookeEntity.getQbPassword());
                quickbookCustomEntities.setSyncTime(getQuickbookeEntity.getSyncTime());
            }
            quickbookCustomEntitiesRepository.updateQuickbookeEntities(quickbookCustomEntities);
            return quickbookCustomEntities;
        }
        return quickbookCustomEntitiesRepository.saveQuickbookCustomEntities(quickbookCustomEntities);
    }

    public QuickbookCustomEntities getQuickbookEntity() {
        QuickbookCustomEntities getQuickbookeEntity = quickbookCustomEntitiesRepository.findQuickbookEntities(token.getCompanyId(), token.getShopId());
        return getQuickbookeEntity;
    }

    @Override
    public QuickbookSettings getQuickbooksSettings() {
        //Add accounts List
        ThirdPartyAccount thirdPartyAccountDetils = thirdPartyAccountRepository.findByAccountTypeByShopId(token.getCompanyId(), token.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
        QuickbookSettings qbSettings = new QuickbookSettings(quickbookCustomEntitiesRepository.findQuickbookEntities(token.getCompanyId(), token.getShopId()));
        List<AccountInfo> accountInfoList = new ArrayList<AccountInfo>();
        if (qbSettings != null && qbSettings.getQbtypes() != null) {
            if (qbSettings.getQbtypes().equals("quickbook_online")) {
                if (thirdPartyAccountDetils != null) {
                    if (thirdPartyAccountDetils.getPassword() != null) {

                        BearerTokenResponse bearerTokenResponse = quickbookSync.getAccessTokenByShop(thirdPartyAccountDetils.getShopId());
                        DataService service = quickbookSync.getService(bearerTokenResponse.getAccessToken(), thirdPartyAccountDetils.getQuickbook_companyId());
                        thirdPartyAccountRepository.updteQuickbookDetails(thirdPartyAccountDetils, bearerTokenResponse, thirdPartyAccountDetils.getCompanyId(), thirdPartyAccountDetils.getQuickbook_companyId(), thirdPartyAccountDetils.getShopId());

                        accountInfoList = accountHelper.getAccountList(service);
                        if (accountInfoList != null && !accountInfoList.isEmpty()) {
                            qbSettings.setAccountList(accountInfoList);

                        }
                        Iterable<QuickbookSyncDetails> quickbookSyncDetails = quickbookSyncDetailsRepository.findByEntityTypeByStatus(token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.Status.Inprogress);
                        for (QuickbookSyncDetails quickbookSyncDetailsStatus : quickbookSyncDetails) {
                            qbSettings.setInprogress(true);
                        }

                    }


                }
            } else {
                List<QBDesktopAccounts> accountList = qbAccountRepository.getQBAccountList(token.getCompanyId(), token.getShopId());
                qbSettings.setQbDesktopAccounts(accountList);
            }
        }
        qbSettings.setQbCompanyId(token.getShopId());
        long lastSyncDate = 0l;

        Iterable<QuickbookSyncDetails> syncSalesReceiptList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.SalesReceipt, qbSettings.getQbtypes());

        for (QuickbookSyncDetails quickbookSyncDetails : syncSalesReceiptList) {
            qbSettings.setSaleslastSyncDate(ProcessorUtil.dateTimeString((Long) quickbookSyncDetails.getEndTime()));
            qbSettings.setSalesTotalSync(quickbookSyncDetails.getTotal_Sync());
            qbSettings.setSalesTotalFail(quickbookSyncDetails.getTotal_fail());
        }

        Iterable<QuickbookSyncDetails> syncPurchaseList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.PurchaseOrder, qbSettings.getQbtypes());

        for (QuickbookSyncDetails purchaseDetails : syncPurchaseList) {
            qbSettings.setPurchaseLastSyncDate(ProcessorUtil.dateTimeString((Long) purchaseDetails.getEndTime()));
            qbSettings.setPurchaseTotalSync(purchaseDetails.getTotal_Sync());
            qbSettings.setPurchaseTotalFail(purchaseDetails.getTotal_fail());

        }

        Iterable<QuickbookSyncDetails> syncRefundList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.RefundReCeipt, qbSettings.getQbtypes());

        for (QuickbookSyncDetails refundDetails : syncRefundList) {
            qbSettings.setRefundLastSyncDate(ProcessorUtil.dateTimeString((Long) refundDetails.getEndTime()));
            qbSettings.setRefundTotalSync(refundDetails.getTotal_Sync());
            qbSettings.setRefundTotalFail(refundDetails.getTotal_fail());
        }


        Iterable<QuickbookSyncDetails> syncBillList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.Bill, qbSettings.getQbtypes());

        for (QuickbookSyncDetails billdetails : syncBillList) {
            qbSettings.setBillLastSyncDate(ProcessorUtil.dateTimeString((Long) billdetails.getEndTime()));
            qbSettings.setBillTotalSync(billdetails.getTotal_Sync());
            qbSettings.setBillTotalFail(billdetails.getTotal_fail());
        }

        Iterable<QuickbookSyncDetails> salesByConsumerList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.SalesByConsumer, qbSettings.getQbtypes());

        for (QuickbookSyncDetails salesByConsumer : salesByConsumerList) {
            qbSettings.setSalesbyconsumerLastDate(ProcessorUtil.dateTimeString((Long) salesByConsumer.getEndTime()));
            qbSettings.setSalesConsumerTotalSync(salesByConsumer.getTotal_Sync());
            qbSettings.setSalesConsumerTotalFail(salesByConsumer.getTotal_fail());
        }


        Iterable<QuickbookSyncDetails> salesByProductList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.SalesByProduct, qbSettings.getQbtypes());

        for (QuickbookSyncDetails salesByProduct : salesByProductList) {
            qbSettings.setSalesByProductLastDate(ProcessorUtil.dateTimeString((Long) salesByProduct.getEndTime()));
            qbSettings.setSalesByproductTotalSync(salesByProduct.getTotal_Sync());
            qbSettings.setSalesbyproductTotalFail(salesByProduct.getTotal_fail());
        }


        Iterable<QuickbookSyncDetails> expenseList = quickbookSyncDetailsRepository.findByEntityType
                (token.getCompanyId(), token.getShopId(), QuickbookSyncDetails.QuickbookEntityType.Expenses, qbSettings.getQbtypes());

        for (QuickbookSyncDetails expense : expenseList) {
            qbSettings.setExpenseSyncLastDate(ProcessorUtil.dateTimeString((Long) expense.getEndTime()));
            qbSettings.setExpenseTotalSync(expense.getTotal_Sync());
            qbSettings.setExpenseTotalFail(expense.getTotal_fail());
        }

        return qbSettings;
    }


    public QuickbookSettings getAllDetailEntityReponse() {
        return service.getQuickbooksSettings();
    }


    public String getQwcFileURL(QWCPassword qwcPassword) {
        if (qwcPassword == null || StringUtils.isBlank(qwcPassword.getQwcPassword())) {
            throw new BlazeInvalidArgException("QuickBook", " QuickBook password was not found.");
        }
        QuickbookCustomEntities quickbookCustomEntities = quickbookCustomEntitiesRepository.findQuickbookEntities(token.getCompanyId(), token.getShopId());
        if (quickbookCustomEntities == null) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook custom entities was not found.");
        }
        quickbookCustomEntities.setQbPassword(qwcPassword.getQwcPassword());
        quickbookCustomEntitiesRepository.updateQuickbookeEntities(quickbookCustomEntities);

        Shop shop = shopRepository.getById(token.getShopId());

        String filedata = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<QBWCXML>\n" +
                "   <AppName>QuickBooks Integrator</AppName>\n" +
                "   <AppID>b7ti3guaz9</AppID>\n" +
                "   <AppURL>" + configuration.getAppApiUrl() + "/soap/QBSyncService?wsdl</AppURL>\n" +
                "   <AppDescription>Retail App</AppDescription>\n" +
                "   <AppSupport>" + configuration.getAppApiUrl() + "/soap/QBSyncService?wsdl</AppSupport>\n" +
                "   <UserName>" + token.getCompanyId().concat("_").concat(token.getShopId()) + "</UserName>\n" +
                "   <OwnerID>{90A44FB7-33D9-4815-AC85-AC86A7E7D113}</OwnerID>\n" +
                "   <FileID>{57F3B9B6-86F1-4FCC-B1FF-967DE1813D22}</FileID>\n" +
                "   <QBType>QBFS</QBType>\n" +
                "   <Scheduler>\n" +
                "      <RunEveryNMinutes>3</RunEveryNMinutes>\n" +
                "   </Scheduler>\n" +
                "   <IsReadOnly>false</IsReadOnly>\n" +
                "</QBWCXML>";


        InputStream inputStream = new ByteArrayInputStream(filedata.getBytes());
        String name = shop.getName();
        name = name.replaceAll("[^a-z A-Z 0-9]","-");
        UploadFileResult result = amazonS3Service.uploadFilePublic(inputStream, name + "_WebConnector", ".qwc");
        LOG.info("URL:" + result.getUrl());

        return result.getUrl();
    }


    @Override
    public QuickBookDesktopResponse getQuickBookDesktopOperations() {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }
        QuickbookCustomEntities quickBookEntities = quickbookCustomEntitiesRepository.findQuickbookEntities(token.getCompanyId(), token.getShopId());
        if (quickBookEntities == null) {
            return null;
            //throw new BlazeInvalidArgException("QuickBook","QuickBook custom entities does  not found.");
        }

        List<QBDesktopOperation> quickBookDesktopOperations = new ArrayList<>();
        if (quickBookEntities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
            quickBookDesktopOperations = qbDesktopOperationRepository.getQuickBookDesktopOperationsWithAppTarget(token.getCompanyId(), token.getShopId(), shop.getAppTarget().toString(), QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY);
        } else {
            quickBookDesktopOperations = qbDesktopOperationRepository.getQuickBookDesktopOperationsWithAppTarget(token.getCompanyId(), token.getShopId(), shop.getAppTarget().toString(), QBConstants.SYNC_INDIVIDUAL);
        }

        List<QBDesktopAccounts> accountList = qbAccountRepository.getQBAccountList(token.getCompanyId(), token.getShopId());

        QuickBookDesktopResponse response = new QuickBookDesktopResponse();

        List<QuickBookDesktopOperationResponse> desktopOperationResponses = new ArrayList<>();

        if (quickBookDesktopOperations.size() > 0) {
            for (QBDesktopOperation quickBookDesktopOperation : quickBookDesktopOperations) {
                QuickBookDesktopOperationResponse qbOperationResponse = new QuickBookDesktopOperationResponse();
                qbOperationResponse.setEnabled(quickBookDesktopOperation.isEnabled());
                qbOperationResponse.setOperationType(quickBookDesktopOperation.getOperationType());
                qbOperationResponse.setDisplayName(quickBookDesktopOperation.getDisplayName());
                qbOperationResponse.setSyncPaused(quickBookDesktopOperation.isSyncPaused());
                qbOperationResponse.setOperationId(quickBookDesktopOperation.getId());

                List<QBDesktopField> fieldList = new ArrayList<>();
                Map<String, QBDesktopField> qbDesktopFieldMap = quickBookDesktopOperation.getQbDesktopFieldMap();
                if (qbDesktopFieldMap.size() > 0) {
                    for (Map.Entry<String, QBDesktopField> stringQBDesktopFieldEntry : qbDesktopFieldMap.entrySet()) {
                        fieldList.add(stringQBDesktopFieldEntry.getValue());
                    }
                }
                qbOperationResponse.setQbDesktopFields(fieldList);

                desktopOperationResponses.add(qbOperationResponse);
            }
        }


        Map<String, List<QBDesktopAccounts>> qbDesktopAccountsMap = new HashMap<>();

        if (accountList.size() > 0) {
            for (QBDesktopAccounts quickBookDesktopAccounts : accountList) {
                if (quickBookDesktopAccounts.getAccountType() != null) {
                    if (qbDesktopAccountsMap.containsKey(quickBookDesktopAccounts.getAccountType().toString())) {
                        List<QBDesktopAccounts> quickBookDesktopAccountsList = qbDesktopAccountsMap.get(quickBookDesktopAccounts.getAccountType().toString());
                        quickBookDesktopAccountsList.add(quickBookDesktopAccounts);
                        qbDesktopAccountsMap.put(quickBookDesktopAccounts.getAccountType().toString(), quickBookDesktopAccountsList);

                    } else {
                        qbDesktopAccountsMap.put(quickBookDesktopAccounts.getAccountType().toString(), Lists.newArrayList(quickBookDesktopAccounts));
                    }
                }
            }
        }

        response.setQbDesktopOperations(desktopOperationResponses);
        response.setQuickBookDesktopAccountsMap(qbDesktopAccountsMap);
        return response;
    }


    @Override
    public List<QBDesktopOperation> updateQuickBookDesktopOperations(List<QBDesktopOperationRequest> request) {
        if (request == null || request.size() == 0) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook request data doesn't found.");
        }

        List<QBDesktopOperation> qbDesktopOperations = new ArrayList<>();

        Map<QBDesktopOperation.OperationType, QBDesktopOperation> qbDesktopOperationHashMap = new HashMap<>();

        List<QBDesktopOperation> quickBookDesktopOperations = qbDesktopOperationRepository.getQuickBookDesktopOperations(token.getCompanyId(), token.getShopId());

        for (QBDesktopOperation qbDesktopOperation : quickBookDesktopOperations) {
            qbDesktopOperationHashMap.put(qbDesktopOperation.getOperationType(), qbDesktopOperation);
        }

        if (quickBookDesktopOperations.size() > 0) {
            for (QBDesktopOperationRequest qbDesktopOperationRequest : request) {
                Map<String, QBDesktopField> qbDesktopFieldMap = prepareDesktopFieldMap(qbDesktopOperationRequest);

                QBDesktopOperation qbDesktopOperation = qbDesktopOperationHashMap.get(qbDesktopOperationRequest.getOperationType());
                if (qbDesktopOperation != null) {
                    qbDesktopOperation.setEnabled(qbDesktopOperationRequest.isEnabled());
                    qbDesktopOperation.setQbDesktopFieldMap(qbDesktopFieldMap);
                    qbDesktopOperation.setSyncPaused(qbDesktopOperationRequest.isSyncPaused());
                    QBDesktopOperation dbQuickBookOperation = qbDesktopOperationRepository.update(token.getCompanyId(), qbDesktopOperation.getId(), qbDesktopOperation);
                    qbDesktopOperations.add(dbQuickBookOperation);
                } else {
                    QBDesktopOperation qbOperation = new QBDesktopOperation();
                    qbOperation.prepare(token.getCompanyId());
                    qbOperation.setShopId(token.getShopId());
                    qbOperation.setOperationType(qbDesktopOperationRequest.getOperationType());
                    qbOperation.setEnabled(qbDesktopOperationRequest.isEnabled());
                    qbOperation.setQbDesktopFieldMap(qbDesktopFieldMap);
                    qbOperation.setSyncPaused(qbDesktopOperationRequest.isSyncPaused());
                    QBDesktopOperation dbDesktopOperation = qbDesktopOperationRepository.save(qbOperation);
                    qbDesktopOperations.add(dbDesktopOperation);
                }
            }
        } else {
            List<QBDesktopOperation> operations = new ArrayList<>();
            for (QBDesktopOperationRequest qbDesktopOperationRequest : request) {
                QBDesktopOperation qbDesktopOperation = new QBDesktopOperation();
                qbDesktopOperation.prepare(token.getCompanyId());
                qbDesktopOperation.setShopId(token.getShopId());
                qbDesktopOperation.setOperationType(qbDesktopOperationRequest.getOperationType());
                qbDesktopOperation.setEnabled(qbDesktopOperationRequest.isEnabled());
                qbDesktopOperation.setSyncPaused(qbDesktopOperationRequest.isSyncPaused());

                Map<String, QBDesktopField> qBFieldMap= prepareDesktopFieldMap(qbDesktopOperationRequest);
                qbDesktopOperation.setQbDesktopFieldMap(qBFieldMap);
                operations.add(qbDesktopOperation);
            }
            qbDesktopOperations =  qbDesktopOperationRepository.save(operations);
        }

        return qbDesktopOperations;
    }

    private Map<String, QBDesktopField> prepareDesktopFieldMap(QBDesktopOperationRequest request) {
        Map<String, QBDesktopField> qbDesktopFieldMap = new HashMap<>();
        if (request.getQbDesktopFields().size() > 0) {
            for (QBDesktopField qbDesktopField : request.getQbDesktopFields()) {
                qbDesktopFieldMap.put(qbDesktopField.getFieldKey().toString(), qbDesktopField);
            }
        }
        return qbDesktopFieldMap;
    }

    @Override
    public SearchResult<QuickbookSyncDetails> getLimitedQuickBookSyncDetails(int start, int limit) {
        if (limit <= 0) {
            limit = Sync_Details_Limit;
        }
        return quickbookSyncDetailsRepository.getLimitedSyncDetailsByType(token.getCompanyId(), token.getShopId(), start, limit, QBConstants.QUICKBOOK_DESKTOP);
    }

    @Override
    public SearchResult<ErrorLogs> getLimitedQuickBookErrorLogs(int start, int limit, String reference) {
        if (StringUtils.isBlank(reference)) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook reference is not correct");
        }

        if (limit <= 0) {
            limit = Error_Logs_Limit;
        }

        return errorLogsRepository.getLimitedErrorLogs(token.getCompanyId(), token.getShopId(), start, limit, reference);
    }

    @Override
    public QBDesktopOperation updateSyncStatus(String operationId, StatusRequest request) {
        QBDesktopOperation operation = qbDesktopOperationRepository.get(token.getCompanyId(), operationId);
        if (operation == null) {
            throw new BlazeInvalidArgException("QBDesktop Operation", "Operation is not found");
        }
        qbDesktopOperationRepository.updateSyncPauseStatus(token.getCompanyId(), token.getShopId(), new ObjectId(operationId), request.isStatus());
        return qbDesktopOperationRepository.get(token.getCompanyId(), operationId);
    }

    @Override
    public QuickBookAccountDetails saveQuickBookAccountDetails(QuickBookAccountDetails quickBookAccountDetails) {
        QuickBookAccountDetails accountDetails = null;
        if (StringUtils.isBlank(quickBookAccountDetails.getReferenceId())) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook reference does not exists.");
        }

        if (QuickBookAccountDetails.AccountType.Default.equals(quickBookAccountDetails.getAccountType())) {
            if (StringUtils.isBlank(quickBookAccountDetails.getInventory())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found inventory account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getSales())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found sales account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getSuppliesAndMaterials())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found cogs account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getChecking())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found bank account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getPayableAccount())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found payable account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getReceivable())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found receivable account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getCc_clearance())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found credit card account.");
            }

            if (StringUtils.isBlank(quickBookAccountDetails.getClearance())) {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found cannabis clearance account.");
            }
        }

        accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(token.getCompanyId(), token.getShopId(), quickBookAccountDetails.getReferenceId());
        if (accountDetails != null) {
            accountDetails.setStatus(quickBookAccountDetails.isStatus());
            accountDetails.setInventory(quickBookAccountDetails.getInventory());
            accountDetails.setSales(quickBookAccountDetails.getSales());
            accountDetails.setSuppliesAndMaterials(quickBookAccountDetails.getSuppliesAndMaterials());
            if (quickBookAccountDetails.getAccountType().equals(QuickBookAccountDetails.AccountType.Default)) {
                accountDetails.setChecking(quickBookAccountDetails.getChecking());
                accountDetails.setPayableAccount(quickBookAccountDetails.getPayableAccount());
                accountDetails.setCc_clearance(quickBookAccountDetails.getCc_clearance());
                accountDetails.setReceivable(quickBookAccountDetails.getReceivable());
            }
            quickBookAccountDetailsRepository.update(token.getCompanyId(), accountDetails.getId(), accountDetails);
        } else {
            quickBookAccountDetails.prepare(token.getCompanyId());
            quickBookAccountDetails.setShopId(token.getShopId());
            accountDetails = quickBookAccountDetailsRepository.save(quickBookAccountDetails);
        }

        return accountDetails;
    }

    @Override
    public QuickBookAccountDetails updateQuickBookAccountDetails(String referenceId, QuickBookAccountDetails quickBookAccountDetails) {
        QuickBookAccountDetails accountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(token.getCompanyId(), token.getShopId(), referenceId);
        if (accountDetails == null) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook account details doesn't not found.");
        }

        accountDetails.setStatus(quickBookAccountDetails.isStatus());

        if (QuickBookAccountDetails.AccountType.Default.equals(quickBookAccountDetails.getAccountType())) {
            if (StringUtils.isNotBlank(quickBookAccountDetails.getInventory())) {
                accountDetails.setInventory(quickBookAccountDetails.getInventory());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found inventory account.");
            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getSales())) {
                accountDetails.setSales(quickBookAccountDetails.getSales());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found sales account.");
            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getSuppliesAndMaterials())) {
                accountDetails.setSuppliesAndMaterials(quickBookAccountDetails.getSuppliesAndMaterials());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found cogs account.");
            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getChecking())) {
                accountDetails.setChecking(quickBookAccountDetails.getChecking());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found bank account.");
            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getPayableAccount())) {
                accountDetails.setPayableAccount(quickBookAccountDetails.getPayableAccount());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found payable account.");
            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getReceivable())) {
                accountDetails.setReceivable(quickBookAccountDetails.getReceivable());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found receivable account.");

            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getCc_clearance())) {
                accountDetails.setCc_clearance(quickBookAccountDetails.getCc_clearance());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found credit card account.");
            }

            if (StringUtils.isNotBlank(quickBookAccountDetails.getClearance())) {
                accountDetails.setClearance(quickBookAccountDetails.getClearance());
            } else {
                throw new BlazeInvalidArgException("QuickBook", "QuickBook doesn't found cannabis clearance account.");
            }
        } else {
            accountDetails.setInventory(quickBookAccountDetails.getInventory());
            accountDetails.setSales(quickBookAccountDetails.getSales());
            accountDetails.setSuppliesAndMaterials(quickBookAccountDetails.getSuppliesAndMaterials());

        }

        return quickBookAccountDetailsRepository.update(token.getCompanyId(), accountDetails.getId(), accountDetails);
    }

    @Override
    public QuickBookAccountDetails getQuickBookAccountDetailsByReferenceId(String referenceId) {
        if (StringUtils.isBlank(referenceId)) {
            throw new BlazeInvalidArgException("QuickBook", "QuickBook reference doesn't found.");
        }
        QuickBookAccountDetails quickBookAccountDetails = quickBookAccountDetailsRepository.getQuickBookAccountDetailsByReferenceId(token.getCompanyId(), token.getShopId(), referenceId);
        if (quickBookAccountDetails == null) {
            return null;
        }
        return quickBookAccountDetails;
    }

    @Override
    public void hardResetQuickBookData() {
        // Quickbook Session Data
        quickBookSessionDataRepository.hardRemoveQuickSessionData(token.getCompanyId(), token.getShopId());

        // QuickBook account
        qbAccountRepository.deleteQBAccounts(token.getCompanyId(), token.getShopId());

        // Custom entities
        quickbookCustomEntitiesRepository.deleteEntityDetails(token.getCompanyId(), token.getShopId());

        // Desktop operation
        qbDesktopOperationRepository.hardResetQbDesktopOperation(token.getCompanyId(), token.getShopId());

        // Sync details
        quickbookSyncDetailsRepository.hardResetQuickBookSyncDetails(token.getCompanyId(), token.getShopId(), QBConstants.QUICKBOOK_DESKTOP);

        // Current Sync
        qbDesktopCurrentSyncRepository.hardResetQbDesktopCurrentSync(token.getCompanyId(), token.getShopId());

        // Unique sequence
        qbUniqueSequenceRepository.hardResetQbUniqueSequence(token.getCompanyId(),token.getShopId());

        // Error logs
        errorLogsRepository.hardResetErrorLogs(token.getCompanyId(), token.getShopId(), QBConstants.QUICKBOOK_DESKTOP);

        // Products
        productRepository.hardRemoveQuickBookDataInProducts(token.getCompanyId(), token.getShopId());

        // Product Category
        productCategoryRepository.hardRemoveQuickBookDataInProductCategories(token.getCompanyId(), token.getShopId());

        // Vendor
        vendorRepository.hardQbMappingRemove(token.getCompanyId(), token.getShopId());
        vendorRepository.hardQbCustomerMappingRemove(token.getCompanyId(), token.getShopId());

        // Customer
        memberRepository.hardQbMappingRemove(token.getCompanyId(), token.getShopId());

        // Account details
        quickBookAccountDetailsRepository.hardAccountDetailsRemove(token.getCompanyId(), token.getShopId());

        // Invoice
        invoiceRepository.hardRemoveQuickBookDataInInvoices(token.getCompanyId(), token.getShopId());

        // Invoice Payment
        invoicePaymentsRepository.hardRemoveQuickBookDataInInvoicePayments(token.getCompanyId(), token.getShopId());

        // Purchase Order
        purchaseOrderRepository.hardRemoveQuickBookDataInPOs(token.getCompanyId(), token.getShopId());

        // Shipment Bill
        shipmentBillRepository.hardRemoveQuickBookDataInBills(token.getCompanyId(), token.getShopId());

        // Bill Payment
        shipmentBillRepository.hardRemoveQuickBookDataInBillPayments(token.getCompanyId(), token.getShopId());

        // Transaction
        transactionRepository.hardRemoveQuickBookDataInTransactions(token.getCompanyId(), token.getShopId());

    }

    @Override
    public String getEnabledQuickBook(QuickBookEnabledRequest quickBookEnabledRequest) {
        CompanyFeatures companyFeature = companyFeaturesRepository.getCompanyFeature(quickBookEnabledRequest.getCompanyId());
        if (companyFeature == null) {
            throw new BlazeInvalidArgException("CompanyFeature", "Company feature doesn't found.");
        }

        Shop shop = shopRepository.get(quickBookEnabledRequest.getCompanyId(), quickBookEnabledRequest.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop doesn't found");
        }

        Map<String, Boolean> availableQbShops;
        if (companyFeature.getAvailableQbShops() != null) {
            availableQbShops = companyFeature.getAvailableQbShops();
        } else {
            availableQbShops = new HashMap<>();
        }
        availableQbShops.put(quickBookEnabledRequest.getShopId(), quickBookEnabledRequest.isStatus());
        companyFeature.setAvailableQbShops(availableQbShops);

        companyFeaturesRepository.update(quickBookEnabledRequest.getCompanyId(), companyFeature.getId(), companyFeature);
        String message = null;
        if (quickBookEnabledRequest.isStatus()) {
            message = "QuickBook is enabled for " + shop.getName() + " shop.";
        } else {
            message = "QuickBook is disabled for " + shop.getName() + " shop.";
        }
        return message;
    }
}
