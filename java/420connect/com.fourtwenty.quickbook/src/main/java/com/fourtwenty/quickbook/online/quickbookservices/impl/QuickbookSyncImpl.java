package com.fourtwenty.quickbook.online.quickbookservices.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.quickbook.online.helperservices.*;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.intuit.ipp.core.Context;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.util.Config;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;


public class QuickbookSyncImpl implements QuickbookSync {

    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    ItemHelper itemHelper;
    @Inject
    CustomerHelper customerHelper;
    @Inject
    VendorHelper vendorHelper;
    @Inject
    PurchaseHelper purchaseHelper;
    @Inject
    SalesReceiptHelper salesReceiptHelper;
    @Inject
    RefundReceiptHelper refundReceiptHelper;
    @Inject
    private ConnectConfiguration configuration;
    @Inject
    HttpHelper httpHelper;
    @Inject
    BearerTokenResponse bearerTokenResponse;
    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    QBSalesByProductCategory qbSalesByProductCategory;
    @Inject
    QBSalesByConsumerType qbSalesByConsumerType;
    @Inject
    QBDailyPurchase qbDailyPurchase;
    @Inject
    BillHelper billHelper;
    @Inject
    BillPaymentHelper billPaymentHelper;
    @Inject
    JournalEntryHelper journalEntryHelper;
    @Inject
    QBSalesByCategoryJournalEntry qbSalesByCategoryJournalEntry;
    @Inject
    QBSalesByConsumerJournalEntry qbSalesByConsumerJournalEntry;
    @Inject
    QuickbookSync quickbookSync;
    @Inject
    RefundReceiptJournalEntry refundReceiptJournalEntry;

    private HttpClient httpClient = HttpClientBuilder.create().build();

    //Get DataService
    public DataService getService(String accesstoken, String quickbookCompanyId) {
        DataService service = null;
        Config.setProperty(Config.BASE_URL_QBO, configuration.getQuickBookConfig().getSanboxURI());
        if (accesstoken != null) {
            OAuth2Authorizer oauth = new OAuth2Authorizer(accesstoken);
            if (quickbookCompanyId != null) {
                try {
                    Context context = new Context(oauth, ServiceType.QBO, quickbookCompanyId);
                    service = new DataService(context);

                } catch (FMSException e) {
                    List<com.intuit.ipp.data.Error> list = e.getErrorList();
                    for (com.intuit.ipp.data.Error error : list) {
                        LOG.info("Error while calling  Data service : " + error.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LOG.info("Exception while getting Oauth Service : ");

                }
            }
        }
        return service;
    }

    // sync vendors
    public void syncVendors(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            vendorHelper.syncVendors(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Erroe while creating vendors in Quickbook:");
        }

    }

    // sync items
    public void syncItems(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            itemHelper.syncItems(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while adding items in quickbook:");
        }

    }

    // sync customers
    public void syncCustomers(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            customerHelper.syncCustomers(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while creating customers in Quickbook:");

        }

    }

    // sync salesreceipts
    public void syncSalesReceipts(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            salesReceiptHelper.syncSalesReceipts(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while calling create sales receipt::");
        }


    }

    // sync purchaseorders
    public void syncPurchaseOrders(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            purchaseHelper.syncPurchaseOrders(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error While adding purchase order:");
        }

    }

    //sync RefundReceipts
    public void syncRefundReceipts(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            refundReceiptHelper.syncRefundReceipts(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error  while creating Refund Receipt: ");
        }

    }

    //sync sales by product Category
    public void syncSaleByProductCategory(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        DataService service = null;
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            qbSalesByProductCategory.syncSaleByProductCategoryInQB(service, quickbookCompanyId, blazeCompanyId, shopId);


        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while Create Sales By product category: ");
        }

    }

    //sync sales By consumer Type
    public void syncSaleByConsumerTypeInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        DataService service = null;
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            qbSalesByConsumerType.salesByConsumerInQB(service, quickbookCompanyId, blazeCompanyId, shopId);

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error  while creating Sales by consumer type: ");
        }

    }

    //sync Bills
    public void syncBillsInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            billHelper.syncBills(service, quickbookCompanyId, blazeCompanyId, shopId);

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error  while creating Bills: ");
        }

    }

    //sync Bill Payments
    public void syncBillPaymentsInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            billPaymentHelper.syncBillPayments(service, quickbookCompanyId, blazeCompanyId, shopId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error  while creating Bill Payments: ");
        }

    }

    // sync Journal Entry for all sales Receipt & Excise Tax
    public void syncJournalEntryInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            journalEntryHelper.syncJournalEntry(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while calling create  Journal Entry::");
        }


    }

    // sync Journal Entry for all sales Receipt & Excise Tax
    public void syncRefundJournalEntryInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            DataService service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            refundReceiptJournalEntry.syncRefundJournalEntry(service, blazeCompanyId, shopId, quickbookCompanyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error while calling create Refund Journal Entry::");
        }


    }


    // sync Journal Entry for sales by product category & Excise Tax
    public void syncSalesByproductCategoryJEInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {
        DataService service = null;
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            qbSalesByCategoryJournalEntry.syncSaleByProductCategoryJEInQB(service, quickbookCompanyId, blazeCompanyId, shopId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error  while creating Journal entry for sales by product category: ");
        }


    }

    // sync Journal Entry for sales by consumer & Excise Tax
    public void syncSalesByConsumerJEInQB(String quickbookCompanyId, String blazeCompanyId, String shopId) {

        DataService service = null;
        try {
            ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(blazeCompanyId, shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            service = getService(thirdPartyAccountsDetail.getPassword(), quickbookCompanyId);
            qbSalesByConsumerJournalEntry.syncSalesByConsumerJEInQB(service, quickbookCompanyId, blazeCompanyId, shopId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("Error  while creating Journal entry for sales by Consumer: ");
        }


    }


    public Object syncCustomQuickbookEntities() {
        List<Company> companyList = companyRepository.list();
        for (Company company : companyList) {
            LOG.info("Company Size: " + companyList.size());
            if (company.isDeleted() || company.isActive() == false) {
                continue;
            }

            Iterable<Shop> shops;
            shops = shopRepository.list(company.getId());
            List<Shop> shopList = Lists.newArrayList(shops);

            LOG.info("company id: " + company.getId());
            for (Shop shop : shopList) {
                if (shop.isActive() == false) {
                    continue;
                }
                ThirdPartyAccount thirdPartyAccountsDetails = thirdPartyAccountRepository.findByAccountTypeByShopId(company.getId(),
                        shop.getId(), ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                if (thirdPartyAccountsDetails != null) {
                    BearerTokenResponse bearerTokenResponseNew = getNewRefreshToken(thirdPartyAccountsDetails.getUsername());
                    if (bearerTokenResponseNew != null) {

                        if (bearerTokenResponse.getAccessToken() != null) {
                            //Update token in database
                            thirdPartyAccountsDetails = thirdPartyAccountRepository.updteQuickbookDetails(thirdPartyAccountsDetails,
                                    bearerTokenResponseNew, thirdPartyAccountsDetails.getCompanyId(), thirdPartyAccountsDetails.getQuickbook_companyId(),
                                    thirdPartyAccountsDetails.getShopId());

                            try {
                                String accesstoken = bearerTokenResponseNew.getAccessToken();
                                String qbCompanyId = thirdPartyAccountsDetails.getQuickbook_companyId();
                                String blazeCompanyId = thirdPartyAccountsDetails.getCompanyId();
                                String shopId = thirdPartyAccountsDetails.getShopId();

                                // save in-progress status
                                quickbookSyncDetailsRepository.saveQuickbookEntity(0, 0, 0, blazeCompanyId, 0L,
                                        0L, shopId, QuickbookSyncDetails.QuickbookEntityType.StatusCheck, QuickbookSyncDetails.Status.Inprogress, QBConstants.QUICKBOOK_ONLINE);

                                //get custom entity from DB
                                QuickbookCustomEntities entities = quickbookCustomEntitiesRepository.findQuickbookEntities(blazeCompanyId, shopId);

                                if (entities != null) {
                                    //sync vendor,items, customer, sales-receipt, purchase order, refund receipt
                                    if (entities.getQuickbookmethology().equals(QBConstants.SYNC_INDIVIDUAL)) {
//                                        syncVendors(qbCompanyId, blazeCompanyId, shopId);
//                                        syncItems(qbCompanyId, blazeCompanyId, shopId);
                                        syncCustomers(qbCompanyId, blazeCompanyId, shopId);

                                    }
                                    qbDailyPurchase.dailyPurchaseInQB(blazeCompanyId, shopId, accesstoken, thirdPartyAccountsDetails.getQuickbook_companyId());


                                    if (entities.isPurchaseOrder()) {
                                        syncPurchaseOrders(qbCompanyId, blazeCompanyId, shopId);

                                    }

                                    if (entities.isBill()) {
                                        syncBillsInQB(qbCompanyId, blazeCompanyId, shopId);
                                        syncBillPaymentsInQB(qbCompanyId, blazeCompanyId, shopId);

                                    }

                                    if (entities.isSalesReceipt()) {
                                        syncSalesReceipts(qbCompanyId, blazeCompanyId, shopId);
                                        syncJournalEntryInQB(qbCompanyId, blazeCompanyId, shopId);

                                    }

                                    if (entities.isRefundReceipt()) {
                                        syncRefundReceipts(qbCompanyId, blazeCompanyId, shopId);
                                        syncRefundJournalEntryInQB(qbCompanyId, blazeCompanyId, shopId);

                                    }


                                    if (entities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_PRODUCT_CATEGORY)) {
                                        syncSaleByProductCategory(qbCompanyId, blazeCompanyId, shopId);
                                        syncSalesByproductCategoryJEInQB(qbCompanyId, blazeCompanyId, shopId);


                                    }

                                    if (entities.getQuickbookmethology().equals(QBConstants.SYNC_SALES_BY_CONSUMER_TYPE)) {
                                        syncSaleByConsumerTypeInQB(qbCompanyId, blazeCompanyId, shopId);
                                        syncSalesByConsumerJEInQB(qbCompanyId, blazeCompanyId, shopId);


                                    }


                                }
                                // when sync completed then delete In-progress status from DB
                                quickbookSyncDetailsRepository.removeQuickbookSyncDetailByStatus(blazeCompanyId, shopId, QuickbookSyncDetails.Status.Inprogress);


                            } catch (Exception e) {
                                LOG.info("Error while optional data sync :");
                                quickbookSyncDetailsRepository.removeQuickbookSyncDetailByStatus(thirdPartyAccountsDetails.getCompanyId(),
                                        thirdPartyAccountsDetails.getShopId(), QuickbookSyncDetails.Status.Inprogress);
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }


        return null;
    }


    public BearerTokenResponse getNewRefreshToken(String refreshToken) {
        String url = configuration.getQuickBookConfig().getRefreshTokenURI();
        HttpPost post = new HttpPost(url);

        // add header
        post = httpHelper.addHeader(post);
        ArrayList<NameValuePair> urlParameters;
        urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
        urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
        try {
            post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
            HttpResponse response = httpClient.execute(post);

            LOG.info("Response Code in New RefreshToken: " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() != 200) {
                LOG.info("failed getting new access token:");
                return null;
            }

            StringBuffer result = httpHelper.getResult(response);
            LOG.debug("raw result for bearer tokens new token response: " + result);

            ObjectMapper objectMapper = new ObjectMapper();
            bearerTokenResponse = objectMapper.readValue(result.toString(), BearerTokenResponse.class);
            LOG.info("new token response: " + bearerTokenResponse.getAccessToken());


        } catch (Exception ex) {
            LOG.info("Exception while retrieving bearer tokens new bearer: ", ex);
        }
        return bearerTokenResponse;
    }

    public BearerTokenResponse getAccestoken() {
        BearerTokenResponse bearerTokenResponse = null;
        try {
            List<com.fourtwenty.core.domain.models.company.Company> companyList = companyRepository.list();
            for (Company company : companyList) {
                ThirdPartyAccount thirdPartyAccountDetils = thirdPartyAccountRepository.findByAccountType(company.getId(),
                        ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                if (thirdPartyAccountDetils != null) {
                    bearerTokenResponse = getNewRefreshToken(thirdPartyAccountDetils.getUsername());
                    Iterable<Shop> shops = shopRepository.list(company.getId());
                    List<Shop> shopList = Lists.newArrayList(shops);
                    for (Shop shop : shopList) {
                        if (bearerTokenResponse != null) {
                            if (bearerTokenResponse.getAccessToken() != null) {
                                thirdPartyAccountRepository.updteQuickbookDetails(thirdPartyAccountDetils, bearerTokenResponse,
                                        thirdPartyAccountDetils.getCompanyId(), thirdPartyAccountDetils.getQuickbook_companyId(), thirdPartyAccountDetils.getShopId());
                            }
                        }
                    }
                }

            }


        } catch (Exception ex) {
            LOG.info("Error while getting company info");
            ex.printStackTrace();


        }
        return bearerTokenResponse;
    }

    @java.lang.Override
    public BearerTokenResponse getAccessTokenByShop(String shopId) {
        BearerTokenResponse bearerTokenResponse = null;
        try {
            Shop shop = shopRepository.getById(shopId);
            ThirdPartyAccount thirdPartyAccountDetils = thirdPartyAccountRepository.findByAccountTypeByShopId(shop.getCompanyId(),
                    shopId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
            if (thirdPartyAccountDetils != null) {
                bearerTokenResponse = getNewRefreshToken(thirdPartyAccountDetils.getUsername());
                if (bearerTokenResponse != null) {
                    if (bearerTokenResponse.getAccessToken() != null) {
                        thirdPartyAccountRepository.updteQuickbookDetails(thirdPartyAccountDetils, bearerTokenResponse,
                                thirdPartyAccountDetils.getCompanyId(), thirdPartyAccountDetils.getQuickbook_companyId(), thirdPartyAccountDetils.getShopId());
                    }
                }
            }


        } catch (Exception ex) {
            LOG.info("Error while getting company info");
            ex.printStackTrace();


        }
        return bearerTokenResponse;
    }


}