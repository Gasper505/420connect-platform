package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.ErrorLogs;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.WriteResult;


public interface ErrorLogsRepository extends MongoShopBaseRepository<ErrorLogs> {

    SearchResult<ErrorLogs> getLimitedErrorLogs(String companyId, String shopId, int start, int limit, String reference);

    WriteResult hardResetErrorLogs(String companyId, String shopId, String qbType);

}
