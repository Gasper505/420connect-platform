package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopSyncReference;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface QBDesktopSyncReferenceRepository extends MongoShopBaseRepository<QBDesktopSyncReference> {
}
