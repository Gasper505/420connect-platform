package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBUniqueSequence;
import com.mongodb.WriteResult;

import java.util.List;

public interface QBUniqueSequenceRepository extends MongoShopBaseRepository<QBUniqueSequence> {
    List<QBUniqueSequence> getQbUniqueSequenceByType(String companyId, String shopId);

    void updateQbUniqueSequence(String companyId, String shopId, String name, int currentSequence);

    WriteResult hardResetQbUniqueSequence(String companyId, String shopId);
}
