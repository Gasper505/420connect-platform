package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.WriteResult;

import java.util.List;

public interface QbAccountRepository extends MongoShopBaseRepository<QBDesktopAccounts> {
    List<QBDesktopAccounts> getQBAccountList(String companyId, String shopId);
    WriteResult deleteQBAccounts(String companyId, String shopId);
    List<QBDesktopAccounts> getQBAccountListWihoutAccountId(String companyId, String shopId);

}
