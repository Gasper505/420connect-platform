package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.WriteResult;


public interface QbDesktopCurrentSyncRepository extends MongoShopBaseRepository<QbDesktopCurrentSync> {
    QbDesktopCurrentSync getLatestQbDesktopCurrentSync(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType);
    QbDesktopCurrentSync getQbDesktopCurrentSync(String companyId, String shopId, String referenceId);
    WriteResult hardResetQbDesktopCurrentSync(String companyId, String shopId);
}
