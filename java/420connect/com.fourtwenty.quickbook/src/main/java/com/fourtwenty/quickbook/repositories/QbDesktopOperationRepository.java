package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.List;

public interface QbDesktopOperationRepository extends MongoShopBaseRepository<QBDesktopOperation> {
    List<QBDesktopOperation> getQuickBookDesktopOperations(String companyId, String shopId);

    List<QBDesktopOperation> getQuickBookDesktopOperationsWithAppTarget(String companyId, String shopId, String appTarget, String methodology);

    QBDesktopOperation getQuickBookDesktopOperationsByType(String companyId, String shopId, QBDesktopOperation.OperationType type);

    void updateSyncPauseStatus(String companyId, String shopId, ObjectId operationId, Boolean isPaused);

    WriteResult hardResetQbDesktopOperation(String companyId, String shopId);
}