package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.List;

public interface QuickBookAccountDetailsRepository extends MongoShopBaseRepository<QuickBookAccountDetails> {
    QuickBookAccountDetails getQuickBookAccountDetailsByReferenceId(String companyId, String shopId, String referenceId);
    List<QuickBookAccountDetails> getQuickBookAccountDetailsByReferenceIds(String companyId, String shopId, List<String> referenceIds);
    void hardAccountDetailsRemove(String companyId, String shopId);

}
