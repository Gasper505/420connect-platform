package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.quickbook.desktop.QuickBookSessionData;

public interface QuickBookSessionDataRepository extends MongoShopBaseRepository<QuickBookSessionData> {
    QuickBookSessionData getQuickBookSessionDataBySessionId(String sessionId, boolean status);
    void removeQuickBookSessionData(String sessionId);
    void hardRemoveQuickSessionData(String companyId, String shopId);
}