package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.AccountInfo;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface QuickbookAccountRepository extends MongoShopBaseRepository<AccountInfo> {
    void saveQuickbookAccount(AccountInfo accountInfo);
}
