package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.WriteResult;

public interface QuickbookCustomEntitiesRepository extends MongoShopBaseRepository<QuickbookCustomEntities> {
    QuickbookCustomEntities saveQuickbookCustomEntities(QuickbookCustomEntities quickbookCustomEntities);

    QuickbookCustomEntities findQuickbookEntities(String companyId, String shopId);

    void updateQuickbookeEntities(QuickbookCustomEntities quickbookCustomEntities);

    WriteResult deleteEntityDetails(String companyId, String shopId);
}
