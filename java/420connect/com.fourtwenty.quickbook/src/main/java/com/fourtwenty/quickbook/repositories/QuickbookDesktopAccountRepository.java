package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookDesktopAccounts;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.List;

public interface QuickbookDesktopAccountRepository extends MongoShopBaseRepository<QuickbookDesktopAccounts> {

    List<QuickbookDesktopAccounts> getAccountList(String companyId, String shopId);

}
