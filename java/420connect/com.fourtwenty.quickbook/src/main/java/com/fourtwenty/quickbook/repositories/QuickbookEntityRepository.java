package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.WriteResult;

public interface QuickbookEntityRepository extends MongoShopBaseRepository<QuickbookEntity> {

    QuickbookEntity saveQuickbookEntity(String companyId, String shopId, QuickbookEntity.SyncStrategy strategy, String productRef, String productName);

    QuickbookEntity getQbEntityByName(String companyId, String shopId, QuickbookEntity.SyncStrategy strategy, String reference);

    WriteResult hardResetQuickBookEntity(String companyId, String shopId);

}
