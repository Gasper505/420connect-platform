package com.fourtwenty.quickbook.repositories;

import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.WriteResult;

import java.util.List;

public interface QuickbookSyncDetailsRepository extends MongoShopBaseRepository<QuickbookSyncDetails> {

    QuickbookSyncDetails saveQuickbookEntity(int total, int total_sucess, int totalFail, String companyId, long startTime, long endTime, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, QuickbookSyncDetails.Status status, String qbType);

    QuickbookSyncDetails saveQuickbookEntity(int total, int totalSuccess, int totalFail, String companyId, long startTime, long endTime, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, QuickbookSyncDetails.Status status, String qbType, QuickbookSyncDetails.SyncType syncType);

    Iterable<QuickbookSyncDetails> findByEntityType(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, String qbType);

    WriteResult removeQuickbookSyncDetailByStatus(String companyId, String shopId, QuickbookSyncDetails.Status status);

    Iterable<QuickbookSyncDetails> findByEntityTypeByStatus(String companyId, String shopId, QuickbookSyncDetails.Status status);

    Iterable<QuickbookSyncDetails> listByShopWithDateQb(String companyId, String shopId, Long afterDate, Long beforeDate);

    List<QuickbookSyncDetails> getSyncDetailsByStatus(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, List<QuickbookSyncDetails.Status> status, String qbType, QuickbookSyncDetails.SyncType syncType, int limit);

    List<QuickbookSyncDetails> getSyncDetailsByStatus(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, List<QuickbookSyncDetails.Status> status, String qbType, int limit);

    SearchResult<QuickbookSyncDetails> getLimitedSyncDetailsByType(String companyId, String shopId, int start, int limit, String qbType);

    SearchResult<QuickbookSyncDetails> getQuickBookSyncDetailsByStatus(String companyId, String shopId, QuickbookSyncDetails.Status status);

    WriteResult hardResetQuickBookSyncDetails(String companyId, String shopId, String qbType);


}
