package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.ErrorLogs;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.ErrorLogsRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.WriteResult;

public class ErrorLogsRepositoryImpl extends ShopBaseRepositoryImpl<ErrorLogs> implements ErrorLogsRepository {

    @Inject
    public ErrorLogsRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ErrorLogs.class, mongoManager);
    }

    @Override
    public SearchResult<ErrorLogs> getLimitedErrorLogs(String companyId, String shopId, int start, int limit, String reference) {
        Iterable<ErrorLogs> errorLogs = coll.find("{companyId:#, shopId:#, reference:#}", companyId, shopId, reference)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,reference:#}", companyId, shopId, reference);

        SearchResult<ErrorLogs> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(errorLogs));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public WriteResult hardResetErrorLogs(String companyId, String shopId, String qbType) {
        WriteResult result = coll.remove("{companyId:#,shopId:#, qbType:#}", companyId, shopId, qbType);
        return result;
    }
}
