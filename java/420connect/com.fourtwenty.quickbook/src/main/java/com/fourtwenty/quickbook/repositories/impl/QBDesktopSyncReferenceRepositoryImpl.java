package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopSyncReference;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QBDesktopSyncReferenceRepository;

import javax.inject.Inject;

public class QBDesktopSyncReferenceRepositoryImpl extends ShopBaseRepositoryImpl<QBDesktopSyncReference> implements QBDesktopSyncReferenceRepository {
    @Inject
    public QBDesktopSyncReferenceRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QBDesktopSyncReference.class, mongoManager);
    }
}
