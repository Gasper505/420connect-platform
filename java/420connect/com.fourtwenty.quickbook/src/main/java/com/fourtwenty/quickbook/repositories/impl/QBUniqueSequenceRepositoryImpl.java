package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBUniqueSequence;
import com.fourtwenty.quickbook.repositories.QBUniqueSequenceRepository;
import com.google.common.collect.Lists;
import com.mongodb.WriteResult;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;

public class QBUniqueSequenceRepositoryImpl extends ShopBaseRepositoryImpl<QBUniqueSequence> implements QBUniqueSequenceRepository {

    @Inject
    public QBUniqueSequenceRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QBUniqueSequence.class, mongoManager);
    }

    @Override
    public List<QBUniqueSequence> getQbUniqueSequenceByType(String companyId, String shopId) {
        Iterable<QBUniqueSequence> qbUniqueSequences = coll.find("{companyId:#, shopId:#}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(qbUniqueSequences);
    }

    @Override
    public void updateQbUniqueSequence(String companyId, String shopId, String name, int currentSequence) {
        coll.update("{companyId:#, shopId:#, name:#}", companyId, shopId, name).with("{$set: {currentSequence:#, modified:#}}", currentSequence, DateTime.now().getMillis());

    }

    @Override
    public WriteResult hardResetQbUniqueSequence(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result;
    }
}
