package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopAccounts;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QbAccountRepository;
import com.google.common.collect.Lists;
import com.mongodb.WriteResult;

import javax.inject.Inject;
import java.util.List;


public class QbDesktopAccountRepositoryImpl extends ShopBaseRepositoryImpl<QBDesktopAccounts> implements QbAccountRepository {
    @Inject
    public QbDesktopAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QBDesktopAccounts.class, mongoManager);
    }

    @Override
    public List<QBDesktopAccounts> getQBAccountList(String companyId, String shopId) {
        Iterable<QBDesktopAccounts> accountList = coll.find("{companyId:#,shopId:#, deleted:false}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(accountList);
    }

    @Override
    public WriteResult deleteQBAccounts(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result;
    }

    @Override
    public List<QBDesktopAccounts> getQBAccountListWihoutAccountId(String companyId, String shopId) {
        Iterable<QBDesktopAccounts> accountList = coll.find("{companyId:#,shopId:#, deleted:false, accountId:{$exists: false}}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(accountList);
    }
}