package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QbDesktopCurrentSync;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QbDesktopCurrentSyncRepository;
import com.google.common.collect.Lists;
import com.mongodb.WriteResult;

import javax.inject.Inject;

public class QbDesktopCurrentRepositoryImpl extends ShopBaseRepositoryImpl<QbDesktopCurrentSync> implements QbDesktopCurrentSyncRepository {

    @Inject
    public QbDesktopCurrentRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QbDesktopCurrentSync.class, mongoManager);
    }

    @Override
    public QbDesktopCurrentSync getLatestQbDesktopCurrentSync(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType) {
        Iterable<QbDesktopCurrentSync> qbDesktopCurrentSyncs = coll.find("{companyId:#, shopId:#, quickBookEntityType:#}", companyId, shopId, entityType)
                .sort("{modified:-1}")
                .as(entityClazz);

        QbDesktopCurrentSync currentSync = null;
        for (QbDesktopCurrentSync qbDesktopCurrentSync : Lists.newArrayList(qbDesktopCurrentSyncs)) {
            currentSync = qbDesktopCurrentSync;
            break;
        }

        return currentSync;
    }

    @Override
    public QbDesktopCurrentSync getQbDesktopCurrentSync(String companyId, String shopId, String referenceId) {
        return coll.findOne("{companyId:#, shopId:#, syncReference:#}", companyId, shopId, referenceId).as(entityClazz);
    }

    @Override
    public WriteResult hardResetQbDesktopCurrentSync(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result;
    }
}
