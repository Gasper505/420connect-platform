package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QbDesktopOperationRepository;
import com.mongodb.WriteResult;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;

public class QbDesktopOperationRepositoryImpl extends ShopBaseRepositoryImpl<QBDesktopOperation> implements QbDesktopOperationRepository {
    @Inject
    public QbDesktopOperationRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QBDesktopOperation.class, mongoManager);
    }

    @Override
    public List<QBDesktopOperation> getQuickBookDesktopOperations(String companyId, String shopId) {
        Iterable<QBDesktopOperation> qbDesktopOperations = coll.find("{companyId:#, shopId:#, deleted:false}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(qbDesktopOperations);
    }

    @Override
    public List<QBDesktopOperation> getQuickBookDesktopOperationsWithAppTarget(String companyId, String shopId, String appTarget, String methodology) {
        Iterable<QBDesktopOperation> qbDesktopOperations = coll.find("{companyId:#, shopId:#, deleted:false, appTarget:#, methodology:#}", companyId, shopId, appTarget, methodology).as(entityClazz);
        return Lists.newArrayList(qbDesktopOperations);
    }

    @Override
    public QBDesktopOperation getQuickBookDesktopOperationsByType(String companyId, String shopId, QBDesktopOperation.OperationType type) {
        QBDesktopOperation qbDesktopOperations = coll.findOne("{companyId:#, shopId:#, operationType:#, deleted:false, enabled:true}", companyId, shopId, type).as(entityClazz);
        return qbDesktopOperations;
    }

    @Override
    public void updateSyncPauseStatus(String companyId, String shopId, ObjectId operationId, Boolean isPaused) {
        long currentTime = DateTime.now().getMillis();
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, operationId).with("{$set: {syncPaused:#, enableSyncTime:#, modified:#}}", isPaused, currentTime, currentTime);
    }

    @Override
    public WriteResult hardResetQbDesktopOperation(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result;
    }
}
