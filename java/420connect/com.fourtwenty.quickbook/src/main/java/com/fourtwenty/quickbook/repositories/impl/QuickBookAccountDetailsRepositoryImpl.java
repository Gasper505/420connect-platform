package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QuickBookAccountDetailsRepository;
import com.google.inject.Inject;
import org.assertj.core.util.Lists;

import java.util.List;

public class QuickBookAccountDetailsRepositoryImpl extends ShopBaseRepositoryImpl<QuickBookAccountDetails> implements QuickBookAccountDetailsRepository {
    @Inject
    public QuickBookAccountDetailsRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QuickBookAccountDetails.class, mongoManager);
    }

    @Override
    public QuickBookAccountDetails getQuickBookAccountDetailsByReferenceId(String companyId, String shopId, String referenceId) {
        return coll.findOne("{companyId:#, shopId:#, deleted:false, referenceId:#}", companyId, shopId, referenceId).as(entityClazz);
    }

    @Override
    public List<QuickBookAccountDetails> getQuickBookAccountDetailsByReferenceIds(String companyId, String shopId, List<String> referenceIds) {
        Iterable<QuickBookAccountDetails> accountDetails = coll.find("{companyId:#, shopId:#, deleted:false, referenceId:{$in:#}}", companyId, shopId, referenceIds).as(entityClazz);
        return Lists.newArrayList(accountDetails);
    }

    @Override
    public void hardAccountDetailsRemove(String companyId, String shopId) {
        coll.remove("{companyId:#, shopId:#}", companyId, shopId);
    }
}
