package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.desktop.QuickBookSessionData;
import com.fourtwenty.quickbook.repositories.QuickBookSessionDataRepository;

import javax.inject.Inject;

public class QuickBookSessionDataRepositoryImpl extends ShopBaseRepositoryImpl<QuickBookSessionData> implements QuickBookSessionDataRepository {


    @Inject
    public QuickBookSessionDataRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QuickBookSessionData.class, mongoManager);
    }

    @Override
    public QuickBookSessionData getQuickBookSessionDataBySessionId(String sessionId, boolean status) {
        return coll.findOne("{sessionId:#, deleted:false, authenticated:#}", sessionId, status).as(entityClazz);
    }

    @Override
    public void removeQuickBookSessionData(String sessionId) {
        coll.remove("{sessionId:#}", sessionId);
    }

    @Override
    public void hardRemoveQuickSessionData(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}", companyId, shopId);
    }
}