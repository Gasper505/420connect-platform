package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.AccountInfo;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QuickbookAccountRepository;
import com.google.inject.Inject;


public class QuickbookAccountRepositoryImpl extends ShopBaseRepositoryImpl<AccountInfo> implements QuickbookAccountRepository {
    @Inject
    public QuickbookAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(AccountInfo.class, mongoManager);
    }

    @Inject
    QuickbookAccountRepository quickbookAccountRepository;


    @java.lang.Override
    public void saveQuickbookAccount(AccountInfo accountInfo) {
        quickbookAccountRepository.save(accountInfo);
    }
}


