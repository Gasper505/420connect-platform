package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.google.inject.Inject;
import com.mongodb.WriteResult;


public class QuickbookCustomEntitiesRepositoryImpl extends ShopBaseRepositoryImpl<QuickbookCustomEntities> implements QuickbookCustomEntitiesRepository {
    @Inject
    public QuickbookCustomEntitiesRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QuickbookCustomEntities.class, mongoManager);
    }

    @Inject
    QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;

    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    @java.lang.Override
    public QuickbookCustomEntities saveQuickbookCustomEntities(QuickbookCustomEntities quickbookCustomEntities) {
        return quickbookCustomEntitiesRepository.save(quickbookCustomEntities);

    }

    @java.lang.Override
    public QuickbookCustomEntities findQuickbookEntities(String companyId, String shopId) {
        return coll.findOne("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);

    }

    @java.lang.Override
    public void updateQuickbookeEntities(QuickbookCustomEntities quickbookCustomEntities) {
        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountTypeByShopId(quickbookCustomEntities.getCompanyId(), quickbookCustomEntities.getShopId(), ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
        Object message = "false";
        if (thirdPartyAccountsDetail != null || quickbookCustomEntities.getQbtypes().equals("quickbook_desktop")) {
            coll.update("{companyId:#,shopId:#}", quickbookCustomEntities.getCompanyId(), quickbookCustomEntities.getShopId())
                    .with("{$set: {purchaseOrder:#, refundReceipt:#, invoice:#, salesReceipt:#,inventory:#,sales:#,suppliesandMaterials:#,payableAccount:#,checking:#,bill:#,clearance:#,cc_clearance:#,income:#,receivable:#, qbtypes:#, accounts:#, qbCustomAccount:#, syncCustomAccount:#, qbPassword:#, syncTime:#}}", quickbookCustomEntities.isPurchaseOrder(), quickbookCustomEntities.isRefundReceipt(),
                            quickbookCustomEntities.isInvoice(), quickbookCustomEntities.isSalesReceipt(), quickbookCustomEntities.getInventory(), quickbookCustomEntities.getSales(), quickbookCustomEntities.getSuppliesandMaterials(), quickbookCustomEntities.getPayableAccount(),
                            quickbookCustomEntities.getChecking(), quickbookCustomEntities.isBill(), quickbookCustomEntities.getClearance(), quickbookCustomEntities.getCc_clearance(),quickbookCustomEntities.getIncome(), quickbookCustomEntities.getReceivable(), quickbookCustomEntities.getQbtypes(), quickbookCustomEntities.isAccounts(), quickbookCustomEntities.getQbCustomAccount(), quickbookCustomEntities.isSyncCustomAccount(), quickbookCustomEntities.getQbPassword(), quickbookCustomEntities.getSyncTime());

        } else {
            coll.update("{companyId:#,shopId:#}", quickbookCustomEntities.getCompanyId(), quickbookCustomEntities.getShopId())
                    .with("{$set: {syncStrategy:#, quickbookmethology:#,qbtypes:#,qbCustomAccount:#}}", quickbookCustomEntities.getSyncStrategy(), quickbookCustomEntities.getQuickbookmethology(), quickbookCustomEntities.getQbtypes(), quickbookCustomEntities.getQbCustomAccount());
        }
    }

    @Override
    public WriteResult deleteEntityDetails(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result;

    }


}
