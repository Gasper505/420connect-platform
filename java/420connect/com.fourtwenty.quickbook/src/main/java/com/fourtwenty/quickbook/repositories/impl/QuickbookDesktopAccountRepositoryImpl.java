package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookDesktopAccounts;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QuickbookDesktopAccountRepository;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import java.util.List;

public class QuickbookDesktopAccountRepositoryImpl extends ShopBaseRepositoryImpl<QuickbookDesktopAccounts> implements QuickbookDesktopAccountRepository {

    @Inject
    public QuickbookDesktopAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QuickbookDesktopAccounts.class, mongoManager);
    }

    @Override
    public List<QuickbookDesktopAccounts> getAccountList(String companyId, String shopId) {
        Iterable<QuickbookDesktopAccounts> accountList = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(accountList);
    }
}
