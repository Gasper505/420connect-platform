package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookEntity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QuickbookEntityRepository;
import com.google.inject.Inject;
import com.mongodb.WriteResult;

public class QuickbookEntityRepositoryImpl extends ShopBaseRepositoryImpl<QuickbookEntity> implements QuickbookEntityRepository {
    @Inject
    public QuickbookEntityRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QuickbookEntity.class, mongoManager);
    }

    @Inject
    QuickbookEntityRepository quickbookEntityRepository;


    public QuickbookEntity saveQuickbookEntity(String companyId, String shopId, QuickbookEntity.SyncStrategy strategy, String productRef, String productName) {
        QuickbookEntity quickbookEntity = new QuickbookEntity();
        quickbookEntity.setCompanyId(companyId);
        quickbookEntity.setShopId(shopId);
        quickbookEntity.setProductRef(productRef);
        quickbookEntity.setSyncStrategy(strategy);
        quickbookEntity.setProductName(productName);
        quickbookEntityRepository.save(quickbookEntity);
        return quickbookEntity;
    }

    public QuickbookEntity getQbEntityByName(String companyId, String shopId, QuickbookEntity.SyncStrategy strategy, String productName) {
        productName = productName.trim();
        return coll.findOne("{companyId:#,shopId:#,SyncStrategy:#,productName:#}", companyId, shopId, strategy, productName).as(entityClazz);
    }


    @Override
    public WriteResult hardResetQuickBookEntity(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result;
    }
}
