package com.fourtwenty.quickbook.repositories.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.WriteResult;
import org.assertj.core.util.Lists;

import javax.inject.Inject;
import java.util.List;

public class QuickbookSyncDetailsRepositoryImpl extends ShopBaseRepositoryImpl<QuickbookSyncDetails> implements QuickbookSyncDetailsRepository {
    @Inject
    public QuickbookSyncDetailsRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QuickbookSyncDetails.class, mongoManager);
    }

    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;

    @java.lang.Override
    public QuickbookSyncDetails saveQuickbookEntity(int total, int totalSuccess, int totalFail, String companyId, long startTime, long endTime, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, QuickbookSyncDetails.Status status, String qbType) {
        return saveQuickbookEntity(total,totalSuccess, totalFail, companyId, startTime, endTime, shopId, entityType, status, qbType, QuickbookSyncDetails.SyncType.Push);
    }

    @java.lang.Override
    public QuickbookSyncDetails saveQuickbookEntity(int total, int totalSuccess, int totalFail, String companyId, long startTime, long endTime, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, QuickbookSyncDetails.Status status, String qbType, QuickbookSyncDetails.SyncType syncType) {
        QuickbookSyncDetails quickbookSyncDetails = new QuickbookSyncDetails();
        quickbookSyncDetails.setCompanyId(companyId);
        quickbookSyncDetails.setTotalRecords(total);
        quickbookSyncDetails.setTotal_Sync(totalSuccess);
        quickbookSyncDetails.setTotal_fail(totalFail);
        quickbookSyncDetails.setStartTime(startTime);
        quickbookSyncDetails.setEndTime(endTime);
        quickbookSyncDetails.setShopId(shopId);
        quickbookSyncDetails.setStatus(status);
        quickbookSyncDetails.setQuickbookEntityType(entityType);
        quickbookSyncDetails.setQbType(qbType);
        quickbookSyncDetails.setSyncType(syncType);
        ThirdPartyAccount thirdPartyAccountsDetail = thirdPartyAccountRepository.findByAccountType(companyId, ThirdPartyAccount.ThirdPartyAccountType.Quickbook);

        if (thirdPartyAccountsDetail != null) {
            quickbookSyncDetails.setQuickbook_Company_Id(thirdPartyAccountsDetail.getQuickbook_companyId());

        }
        quickbookSyncDetailsRepository.save(quickbookSyncDetails);

        return quickbookSyncDetails;
    }


    @Override
    public Iterable<QuickbookSyncDetails> findByEntityType(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, String qbType) {
        Iterable<QuickbookSyncDetails> quickbookSyncEntity;
        quickbookSyncEntity = coll.find("{companyId:#,shopId:#,quickbookEntityType:#,qbType:#}", companyId, shopId, entityType, qbType).as(entityClazz);
        return quickbookSyncEntity;

    }


    @Override
    public Iterable<QuickbookSyncDetails> findByEntityTypeByStatus(String companyId, String shopId, QuickbookSyncDetails.Status status) {
        Iterable<QuickbookSyncDetails> quickbookSyncEntity;
        quickbookSyncEntity = coll.find("{companyId:#,shopId:#,status:#}", companyId, shopId, status).as(entityClazz);
        return quickbookSyncEntity;

    }


    @Override
    public WriteResult removeQuickbookSyncDetailByStatus(String companyId, String shopId, QuickbookSyncDetails.Status status) {
        WriteResult result = coll.remove("{shopId:#,status:#}", shopId, status);
        return result;

    }

    @Override
    public Iterable<QuickbookSyncDetails> listByShopWithDateQb(String companyId, String shopId, Long afterDate, Long beforeDate) {
        Iterable<QuickbookSyncDetails> records = coll.find("{companyId:#,shopId:#, modified:{$gt:#, $lt:#}}", companyId, shopId, afterDate, beforeDate).as(entityClazz);
        return records;
    }

    @Override
    public List<QuickbookSyncDetails> getSyncDetailsByStatus(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, List<QuickbookSyncDetails.Status> status, String qbType, QuickbookSyncDetails.SyncType syncType, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        Iterable<QuickbookSyncDetails> syncDetails = coll.find("{companyId:#, shopId:#, deleted: false, quickbookEntityType:#, status:{$in:#}, qbType:#, syncType:#}", companyId, shopId, entityType, status, qbType, syncType)
                .sort("{created:-1}")
                .limit(limit)
                .as(entityClazz);

        return Lists.newArrayList(syncDetails);
    }

    @Override
    public List<QuickbookSyncDetails> getSyncDetailsByStatus(String companyId, String shopId, QuickbookSyncDetails.QuickbookEntityType entityType, List<QuickbookSyncDetails.Status> status, String qbType, int limit) {
        return getSyncDetailsByStatus(companyId, shopId, entityType, status, qbType, QuickbookSyncDetails.SyncType.Push, limit);
    }

    @Override
    public SearchResult<QuickbookSyncDetails> getLimitedSyncDetailsByType(String companyId, String shopId, int start, int limit, String qbType) {
        Iterable<QuickbookSyncDetails> syncDetails = coll.find("{companyId:#,shopId:#,qbType:#, syncType:#}", companyId, shopId, qbType, QuickbookSyncDetails.SyncType.Push)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,qbType:#, syncType:#}", companyId, shopId, qbType, QuickbookSyncDetails.SyncType.Push);

        SearchResult<QuickbookSyncDetails> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(syncDetails));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<QuickbookSyncDetails> getQuickBookSyncDetailsByStatus(String companyId, String shopId, QuickbookSyncDetails.Status status) {
        Iterable<QuickbookSyncDetails> syncDetails = coll.find("{companyId:#, shopId:#, deleted:false, status:#}", companyId, shopId, status)
                .sort("{created:-1}")
                .as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false, status:#}", companyId, shopId, status);

        SearchResult<QuickbookSyncDetails> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(syncDetails));
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public WriteResult hardResetQuickBookSyncDetails(String companyId, String shopId, String qbType) {
        WriteResult result = coll.remove("{companyId:#,shopId:#, qbType:#}", companyId, shopId, qbType);
        return result;
    }
}