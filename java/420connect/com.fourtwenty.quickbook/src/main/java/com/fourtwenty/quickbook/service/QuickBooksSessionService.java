package com.fourtwenty.quickbook.service;

import com.fourtwenty.quickbook.desktop.QuickBookSessionData;

public interface QuickBooksSessionService {
    QuickBookSessionData authenticate(String userName, String password);

    QuickBookSessionData getSession(String sessionId);

    void clearSession(String sessionId);
}
