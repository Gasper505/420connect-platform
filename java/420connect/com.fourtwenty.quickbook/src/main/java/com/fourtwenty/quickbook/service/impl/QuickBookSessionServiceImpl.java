package com.fourtwenty.quickbook.service.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.quickbook.desktop.QuickBookSessionData;
import com.fourtwenty.quickbook.repositories.QuickBookSessionDataRepository;
import com.fourtwenty.quickbook.repositories.QuickbookCustomEntitiesRepository;
import com.fourtwenty.quickbook.service.QuickBooksSessionService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.UUID;

public class QuickBookSessionServiceImpl implements QuickBooksSessionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuickBookSessionServiceImpl.class);

    private QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository;
    private ShopRepository shopRepository;
    private QuickBookSessionDataRepository quickBookSessionDataRepository;

    @Inject
    public QuickBookSessionServiceImpl(QuickbookCustomEntitiesRepository quickbookCustomEntitiesRepository, ShopRepository shopRepository, QuickBookSessionDataRepository quickBookSessionDataRepository) {
        this.quickbookCustomEntitiesRepository = quickbookCustomEntitiesRepository;
        this.shopRepository = shopRepository;
        this.quickBookSessionDataRepository = quickBookSessionDataRepository;
    }

    @Override
    public QuickBookSessionData authenticate(String userName, String password) {

        String[] lists = userName.split("_");
        String companyId = lists[0];
        String shopId = lists[1];
        QuickbookCustomEntities quickbookCustomEntities = quickbookCustomEntitiesRepository.findQuickbookEntities(companyId, shopId);
        boolean invalidUser = false;
        if (quickbookCustomEntities == null) {
            LOGGER.warn("Invalid user : " + userName);
            invalidUser = true;
        } else {
            if (StringUtils.isNotBlank(quickbookCustomEntities.getQbPassword()) && quickbookCustomEntities.getQbPassword().equals(password)) {
                Shop shop = shopRepository.get(companyId, shopId);
                if (shop == null) {
                    LOGGER.warn("Incorrect Shop/Company : " + userName);
                    invalidUser = true;
                } else {
                    LOGGER.info("User [" + userName + "] is valid.");
                }
            } else {
                LOGGER.warn("Invalid user : " + userName);
                invalidUser = true;
            }
        }

        QuickBookSessionData sessionData = new QuickBookSessionData();
        if(!invalidUser) {
            String sessionId = UUID.randomUUID().toString();
            sessionData.prepare(companyId);
            sessionData.setShopId(shopId);
            sessionData.setSessionId(sessionId);
            sessionData.setAuthenticated(true);
            quickBookSessionDataRepository.save(sessionData);
        } else {
            sessionData.setQbRetCode("nvu");
        }

        return sessionData;
    }


    @Override
    public QuickBookSessionData getSession(String sessionId) {
        QuickBookSessionData sessionData = quickBookSessionDataRepository.getQuickBookSessionDataBySessionId(cleanSessionId(sessionId), true);
        return sessionData == null ? new QuickBookSessionData() : sessionData;
    }

    private String cleanSessionId(String sessionId) {
        return sessionId.replaceAll("\\{|\\}", "");
    }

    @Override
    public void clearSession(String sessionId) {
        quickBookSessionDataRepository.removeQuickBookSessionData(cleanSessionId(sessionId));
    }

}
