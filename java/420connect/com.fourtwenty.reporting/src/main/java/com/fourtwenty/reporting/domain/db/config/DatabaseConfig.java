package com.fourtwenty.reporting.domain.db.config;

import com.fourtwenty.core.config.PostgreServerConfiguration;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

@Singleton
public class DatabaseConfig implements Managed {

    private static final Log LOGGER = LogFactory.getLog(DatabaseConfig.class);

    private static final String NO_DB_PROPERTIES = "No database configuration provided";
    private static final String NO_DATASOURCE = "No datasource configured for application. Check database properties/configuration for blaze reporting";
    private static final String DATASOURCE_ALREADY_CONFIGURED = "Datasource already configured. Proceeding further";
    private static final String TIME_ZONE_FIX = "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static HikariDataSource dataSource;
    private PostgreServerConfiguration dbProp;
    private boolean enableReportingCache;

    public DatabaseConfig(PostgreServerConfiguration dbProp, boolean enableReportingCache) {
        this.dbProp = dbProp;
        this.enableReportingCache = enableReportingCache;
    }

    public static Connection getConnection() throws SQLException {
        if (Objects.isNull(dataSource)) {
            LOGGER.debug(NO_DATASOURCE);
            throw new BlazeOperationException(NO_DATASOURCE);
        } else {
            return dataSource.getConnection();
        }
    }

    private void init(PostgreServerConfiguration dbProp) {
        if (!enableReportingCache)
            return;

        if (Objects.isNull(dbProp))
            throw new BlazeOperationException(NO_DB_PROPERTIES);


        if (Objects.isNull(dataSource)) {
            final HikariConfig jdbcConfig = new HikariConfig();
            jdbcConfig.setPoolName(dbProp.getPoolName());
            jdbcConfig.setMaximumPoolSize(dbProp.getMaximumPoolSize());
            jdbcConfig.setMinimumIdle(dbProp.getMinimumIdle());
            jdbcConfig.setJdbcUrl(dbProp.getJdbcUrl() + dbProp.getHost() + ":" + dbProp.getPort() + "/" + dbProp.getDbName());
            jdbcConfig.setUsername(dbProp.getUser());
            jdbcConfig.setPassword(dbProp.getSecret());
            jdbcConfig.addDataSourceProperty("cachePrepStmts", dbProp.isCachePrepStmts());
            jdbcConfig.addDataSourceProperty("prepStmtCacheSize", dbProp.getPrepStmtCacheSize());
            jdbcConfig.addDataSourceProperty("prepStmtCacheSqlLimit", dbProp.getPrepStmtCacheSqlLimit());
            jdbcConfig.addDataSourceProperty("useServerPrepStmts", dbProp.isUseServerPrepStmts());
            jdbcConfig.setConnectionTestQuery("SELECT version()");
            LOGGER.info("Postgres db config : \turl " + jdbcConfig.getJdbcUrl() + "\tuser :" + jdbcConfig.getUsername() + "\tpwd : " + jdbcConfig.getPassword());

            dataSource = new HikariDataSource(jdbcConfig);
        } else {
            LOGGER.warn(DATASOURCE_ALREADY_CONFIGURED);
        }
    }

    public HikariDataSource getDataSource() {
        if (!enableReportingCache)
            return null;

        if (Objects.isNull(dataSource)) {
            init(dbProp);
        }
        if (Objects.isNull(dataSource)) {
            LOGGER.debug(NO_DATASOURCE);
            throw new BlazeOperationException(NO_DATASOURCE);
        } else {
            return dataSource;
        }
    }

    @Override
    public void start() throws Exception {
        init(dbProp);
    }

    @Override
    public void stop() throws Exception {
        if (dataSource != null) {
            dataSource.close();
        }
    }
}
