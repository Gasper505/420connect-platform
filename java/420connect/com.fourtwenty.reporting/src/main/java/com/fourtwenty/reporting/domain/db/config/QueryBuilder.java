package com.fourtwenty.reporting.domain.db.config;

import org.apache.commons.dbutils.AsyncQueryRunner;
import org.apache.commons.dbutils.QueryRunner;

import javax.sql.DataSource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Example: queryRunner.query("SELECT * FROM Person WHERE name=?", <Handler>, "John Doe");
 * <Handler> - ResultSet Handler
 */
public class QueryBuilder {

    private ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * Query manger for parsing result set directly into an object
     */
    private QueryRunner queryRunner;

    /**
     * Async Query manger for parsing result set directly into an object
     */
    private AsyncQueryRunner asyncQueryRunner;

    public QueryBuilder(DataSource dataSource) {
        setQueryRunner(dataSource);
        setAsyncQueryRunner();
    }

    public QueryRunner getQueryRunner() {
        return queryRunner;
    }

    private void setQueryRunner(DataSource dataSource) {
        queryRunner = new QueryRunner(dataSource);
    }

    public AsyncQueryRunner getAsyncQueryRunner() {
        return asyncQueryRunner;
    }

    private void setAsyncQueryRunner() {
        this.asyncQueryRunner = new AsyncQueryRunner(executorService);
    }
}
