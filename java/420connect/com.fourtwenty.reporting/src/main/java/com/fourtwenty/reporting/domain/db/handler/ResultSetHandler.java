package com.fourtwenty.reporting.domain.db.handler;

import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.reporting.domain.models.CachedReport;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ResultSetHandler<T extends CachedReport> {

    BeanListHandler<T> getBeanListHandler();

    BeanHandler<T> getBeanHandler();

    Map<String, String> getColumnMap();

    default List<T> parseList(ResultSet resultSet) {
        List<T> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                list.add(this.buildEntity(resultSet));
            }
        } catch (Exception e) {
            throw new BlazeInvalidArgException("CachedReport", e.getMessage());
        }
        return list;
    }

    default T parse(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return this.buildEntity(resultSet);
            }
        } catch (Exception e) {
            throw new BlazeInvalidArgException("CachedReport", e.getMessage());
        }
        return null;
    }

    default T buildEntity(ResultSet resultSet) {
        throw new BlazeInvalidArgException("", "No implementation found for buildEntity");
    }
}
