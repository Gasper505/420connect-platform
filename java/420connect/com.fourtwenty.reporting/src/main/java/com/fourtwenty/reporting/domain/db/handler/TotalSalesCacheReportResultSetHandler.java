package com.fourtwenty.reporting.domain.db.handler;

import com.fourtwenty.reporting.domain.models.TotalSalesCache;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.Map;

public class TotalSalesCacheReportResultSetHandler implements ResultSetHandler<TotalSalesCache> {
    @Override
    public BeanListHandler<TotalSalesCache> getBeanListHandler() {
        return new BeanListHandler<>(TotalSalesCache.class, new BasicRowProcessor(new BeanProcessor()));
    }

    @Override
    public BeanHandler<TotalSalesCache> getBeanHandler() {
        return new BeanHandler<>(TotalSalesCache.class, new BasicRowProcessor(new BeanProcessor()));
    }

    @Override
    public Map<String, String> getColumnMap() {
        return null;
    }
}
