package com.fourtwenty.reporting.domain.db.handler;

import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.Map;

public class TotalSalesDetailResultSetHandler implements ResultSetHandler<TotalSalesDetailCache> {
    @Override
    public BeanListHandler<TotalSalesDetailCache> getBeanListHandler() {
        return new BeanListHandler<>(TotalSalesDetailCache.class, new BasicRowProcessor(new BeanProcessor()));
    }

    @Override
    public BeanHandler<TotalSalesDetailCache> getBeanHandler() {
        return new BeanHandler<>(TotalSalesDetailCache.class, new BasicRowProcessor(new BeanProcessor()));
    }

    @Override
    public Map<String, String> getColumnMap() {
        return null;
    }
}
