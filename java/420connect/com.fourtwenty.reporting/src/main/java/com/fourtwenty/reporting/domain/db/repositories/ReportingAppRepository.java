package com.fourtwenty.reporting.domain.db.repositories;

public interface ReportingAppRepository {

    Boolean isTableExist(String tableName);

    Boolean createTable(String query);

}
