package com.fourtwenty.reporting.domain.db.repositories;

import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.reporting.domain.db.config.QueryBuilder;

import java.io.Serializable;
import java.util.List;


public interface Repository<E extends BaseModel> extends Serializable {

    /**
     * Return query builder for parsing Result set to Models
     *
     * @return instance of QueryBuilder
     */
    QueryBuilder getQueryBuilder();

    /**
     * Get table name for generic entity
     *
     * @return String
     */
    String getTableName();

    /**
     * Find by id
     *
     * @param id Primary key for entity
     * @return Reference of type E
     */
    E findById(String id);

    /**
     * Find by id and active
     *
     * @param id Primary key for entity
     * @return Reference of type E
     */
    E findByIdAndActive(String id);

    /**
     * Find by id
     *
     * @param id Primary key for entity
     * @return Reference of type E
     */
    E findById(Number id);

    /**
     * Get all records
     *
     * @return List of E
     */
    List<E> findAll();

    /**
     * Delete by primary key
     *
     * @param id Primary key of E
     */
    int delete(Long id);

    /**
     * Get value by column name and column value
     *
     * @param column Column Name
     * @param value  column value
     * @return <E>
     */
    E findOneBy(String column, String value);

    /**
     * Get value by column name and column value
     *
     * @param column Column Name
     * @param value  column value
     * @return <E>
     */
    E findOneBy(String column, Number value);

    /**
     * Get value by column name in column values
     *
     * @param column Column Name
     * @param value  column values
     * @return <E>
     */
    List<E> findAllIn(String column, List<?> value);

    /**
     * Delete rows by column name in column values
     *
     * @param column Column Name
     * @param value List of ids
     */
    void deleteByIn(String column, List<Long> value);

    /**
     * Get list of rows by column and value
     *
     * @param column column name
     * @param value column value
     * @return List<E>
     */
    List<E> findAllBy(String column, String value);

    /**
     * Get list of rows by column and value
     *
     * @param column column name
     * @param value column value
     * @return List<E>
     */
    List<E> findAllBy(String column, Number value);

    /**
     * Persist entity into DB
     * @param e Entity<extends IdentityModel>
     * @return Newly inserted entity ID
     */
    E save(E e);

    /**
     * Updates entity into DB
     * @param e Entity<extends IdentityModel>
     * @return Updated entity
     */
    void update(E e);

    /**
     * prepare query with WHERE IN statement
     * @param list
     * @return
     */
    String convertToDBList(List list);
}
