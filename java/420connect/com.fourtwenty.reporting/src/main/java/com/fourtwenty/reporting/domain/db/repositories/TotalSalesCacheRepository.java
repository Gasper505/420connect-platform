package com.fourtwenty.reporting.domain.db.repositories;

import com.fourtwenty.reporting.domain.models.TotalSalesCache;

import java.util.List;

public interface TotalSalesCacheRepository  extends Repository<TotalSalesCache>  {

    Boolean bulkInsert(List<TotalSalesCache> totalSalesCaches);

    List<TotalSalesCache> getBracketSales(String companyId, String shopId, Long startDate, Long endDate);
}
