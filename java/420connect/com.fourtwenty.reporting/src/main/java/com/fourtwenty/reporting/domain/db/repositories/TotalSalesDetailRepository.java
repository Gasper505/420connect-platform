package com.fourtwenty.reporting.domain.db.repositories;

import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;

import java.util.List;

public interface TotalSalesDetailRepository extends Repository<TotalSalesDetailCache> {

    Boolean bulkInsert(List<TotalSalesDetailCache> totalSalesDetailCaches);

    List<TotalSalesDetailCache> getBracketSales(String companyId, String shopId, Long startDate, Long endDate);
}
