package com.fourtwenty.reporting.domain.db.repositories.impl;


import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.reporting.domain.db.config.QueryBuilder;
import com.fourtwenty.reporting.domain.db.repositories.Repository;
import com.fourtwenty.reporting.domain.models.CachedReport;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.persistence.Table;
import javax.sql.DataSource;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class BaseRepository<E extends CachedReport> implements Repository<E> {

    private static final String INSTANTIATION_ERROR = "Not able to create instance of ";
    private static final String CONNECTION_ERROR = "Error while getting connection";
    private static final Logger LOGGER = LogManager.getLogger(BaseRepository.class);
    protected CachedReport dummyModel;
    protected String tableName;
    private Class<E> entityClass;
    private QueryBuilder queryBuilder;
    private DataSource dataSource;

    @Inject
    private ConnectConfiguration connectConfiguration;

    protected BaseRepository(DataSource dataSource, Class<E> model) {
        entityClass = model;
        tableName = entityClass.getAnnotation(Table.class).name();
        queryBuilder = new QueryBuilder(dataSource);

        try {
            this.dataSource = dataSource;
            dummyModel = entityClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.warn(e.getMessage(), e);
            throw new BlazeInvalidArgException(entityClass.getSimpleName(), INSTANTIATION_ERROR);
        }
    }

    @Override
    public QueryBuilder getQueryBuilder() {
        return queryBuilder;
    }

    protected Connection getConnection() {
        if (dataSource != null) {
            try {
                return dataSource.getConnection();
            } catch (Exception e) {
                throw new BlazeOperationException(CONNECTION_ERROR, e);
            }
        } else {
            throw new BlazeOperationException(CONNECTION_ERROR);
        }
    }

    @Override
    public String getTableName() {
        String env = connectConfiguration.getEnv().toString().toLowerCase();
        return env + "_" + tableName;
    }

    @Override
    public E findById(String id) {
        try {
            return (E) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE id = ?", dummyModel.getEntityHandler().getBeanHandler(), id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public E findById(Number id) {
        try {
            return (E) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE id = ?", dummyModel.getEntityHandler().getBeanHandler(), id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public E findByIdAndActive(String id) {
        try {
            return (E) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE id = ? AND active = ?",
                    dummyModel.getEntityHandler().getBeanHandler(), id, Boolean.TRUE);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<E> findAll() {
        try {
            return (List<E>) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName(), dummyModel.getEntityHandler().getBeanListHandler());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    @Override
    public int delete(Long id) {
        try {
            return this.getQueryBuilder().getQueryRunner().update("UPDATE table " + getTableName() + "SET deleted = true WHERE id = ?", id);
        } catch (Exception e) {
            throw new BlazeInvalidArgException(e.getMessage(), e.getMessage());
        }
    }

    @Override
    public E findOneBy(String column, String value) {
        try {
            return (E) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE " + column + " = ?", dummyModel.getEntityHandler().getBeanHandler(), value);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public E findOneBy(String column, Number value) {
        try {
            return (E) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE " + column + " = ?", dummyModel.getEntityHandler().getBeanHandler(), value);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<E> findAllIn(String column, List<?> value) {
        try {
            return (List<E>) this.getQueryBuilder().getQueryRunner()
                    .query("SELECT * FROM " + getTableName() + " WHERE " + column + " IN (" + this.convertToDBList(value) + ")", dummyModel.getEntityHandler().getBeanListHandler());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    @Override
    public void deleteByIn(String column, List<Long> value) {
        try {
            this.getQueryBuilder().getQueryRunner().query("DELETE FROM " + getTableName() + " WHERE " + column + " IN (" + this.convertToDBList(value) + ")", new ScalarHandler<BigInteger>());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    @Override
    public List<E> findAllBy(String column, String value) {
        try {
            return (List<E>) getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE " + column + " = ?", dummyModel.getEntityHandler().getBeanListHandler(), value);
        } catch (SQLException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    @Override
    public List<E> findAllBy(String column, Number value) {
        try {
            return (List<E>) this.getQueryBuilder().getQueryRunner().query("SELECT * FROM " + getTableName() + " WHERE " + column + " = ?", dummyModel.getEntityHandler().getBeanListHandler(), value);
        } catch (SQLException e) {
            LOGGER.debug(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    @Override
    public E save(E e) {
        throw new BlazeInvalidArgException(getTableName(), "No implementation found for saving " + getTableName());
    }

    @Override
    public void update(E e) {
        throw new BlazeInvalidArgException(getTableName(), "No implementation found for updating " + getTableName());
    }

    @Override
    public String convertToDBList(List list) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            stringBuilder.append("'").append(String.valueOf(list.get(i))).append("'");
            if (i < list.size() - 1) {
                stringBuilder.append(",");
            }
        }
        return stringBuilder.toString().equals("") ? "'0'" : stringBuilder.toString();
    }
}
