package com.fourtwenty.reporting.domain.db.repositories.impl;

import com.fourtwenty.reporting.domain.db.config.DatabaseConfig;
import com.fourtwenty.reporting.domain.db.config.QueryBuilder;
import com.fourtwenty.reporting.domain.db.repositories.ReportingAppRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportingAppRepositoryImpl implements ReportingAppRepository {

    private static final Log LOG = LogFactory.getLog(ReportingAppRepositoryImpl.class);

    private QueryBuilder queryBuilder;

    @Inject
    public ReportingAppRepositoryImpl(DatabaseConfig config) {
        queryBuilder = new QueryBuilder(config.getDataSource());
    }

    @Override
    public Boolean isTableExist(String tableName) {
        ResultSet rs = null;
        try {
            DatabaseMetaData metaData = this.queryBuilder.getQueryRunner().getDataSource().getConnection().getMetaData();
            rs = metaData.getTables(null, null, tableName, null);

            if (rs.next()) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } catch (Exception e) {
            LOG.error("Error while getting table existence for " + tableName + "\t Error : " + e);
            return Boolean.FALSE;
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                LOG.error("Error while closing result set" + "\t Error : " + e);
            }
        }
    }

    @Override
    public Boolean createTable(String query) {
        try {
            this.queryBuilder.getQueryRunner().update(query);
        } catch (Exception e) {
            LOG.error("Error while creating table with query : " + query + "\tError : " + e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
