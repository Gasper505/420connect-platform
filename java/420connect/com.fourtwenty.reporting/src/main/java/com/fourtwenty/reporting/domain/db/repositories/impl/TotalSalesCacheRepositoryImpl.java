package com.fourtwenty.reporting.domain.db.repositories.impl;

import com.fourtwenty.reporting.domain.db.config.DatabaseConfig;
import com.fourtwenty.reporting.domain.db.handler.TotalSalesCacheReportResultSetHandler;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesCacheRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesCache;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class TotalSalesCacheRepositoryImpl extends BaseRepository<TotalSalesCache> implements TotalSalesCacheRepository {

    private static final Log LOG = LogFactory.getLog(TotalSalesCacheRepositoryImpl.class);

    @Inject
    protected TotalSalesCacheRepositoryImpl(DatabaseConfig config) {
        super(config.getDataSource(), TotalSalesCache.class);
    }

    @Override
    public Boolean bulkInsert(List<TotalSalesCache> totalSalesCaches) {

        String repeat = StringUtils.repeat("?,", 38);
        String values = repeat.substring(0, repeat.length() - 1);

        String query = "INSERT INTO " + this.getTableName() + "(id, created, modified, deleted, updated, companyId, shopId, transactionId, processedTime," +
                "memberId, transNo, transType, transactionStatus, consumerTaxType, cogs, retailValue, discounts, preAlExciseTax, preNalExciseTax, preCityTax, " +
                "preCountyTax, preStateTax, postAlExciseTax, postNalExciseTax, cityTax, countyTax, stateTax, totalTax, deliveryFee, creditCardFee, " +
                "afterTaxDiscount, grossReceipt, employeeId, sellerTerminalId, paymentOption, promotions, traceSubmitStatus, metrcId) values (" + values + ");";

        try (Connection connection = this.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            connection.setAutoCommit(Boolean.FALSE);
            this.prepareTotalSalesData(statement, totalSalesCaches, connection);
            statement.executeBatch();
            connection.commit();
            statement.close();

            return Boolean.TRUE;

        } catch (Exception e) {
            LOG.error("Error while saving total sales report data. \tError :" + e);
        }

        return Boolean.FALSE;
    }

    private void prepareTotalSalesData(PreparedStatement statement, List<TotalSalesCache> totalSalesCaches, Connection connection) throws SQLException {
        int count = 0;
        int[] result;
        int batchSize = 2000;

        for (TotalSalesCache sale : totalSalesCaches) {
            statement.setString(1, sale.getId());
            statement.setLong(2, sale.getCreated());
            statement.setLong(3, sale.getModified());
            statement.setBoolean(4, sale.isDeleted());
            statement.setBoolean(5, sale.isUpdated());
            statement.setString(6, sale.getCompanyId());
            statement.setString(7, sale.getShopId());
            statement.setString(8, sale.getTransactionId());
            statement.setLong(9, sale.getProcessedTime());
            statement.setString(10, sale.getMemberId());
            statement.setString(11, sale.getTransNo());
            statement.setString(12, sale.getTransType().toString());
            statement.setString(13, sale.getTransactionStatus().toString());
            statement.setString(14, sale.getConsumerTaxType());

            statement.setDouble(15, sale.getCogs());
            statement.setDouble(16, sale.getRetailValue());
            statement.setDouble(17, sale.getDiscounts());
            statement.setDouble(18, sale.getPreAlExciseTax());
            statement.setDouble(19, sale.getPreNalExciseTax());

            statement.setDouble(20, sale.getPreCityTax());
            statement.setDouble(21, sale.getCountyTax());
            statement.setDouble(22, sale.getStateTax());

            statement.setDouble(23, sale.getPostAlExciseTax());
            statement.setDouble(24, sale.getPostNalExciseTax());

            statement.setDouble(25, sale.getCityTax());
            statement.setDouble(26, sale.getCityTax());
            statement.setDouble(27, sale.getCountyTax());
            statement.setDouble(28, sale.getTotalTax());
            statement.setDouble(29, sale.getDeliveryFee());
            statement.setDouble(30, sale.getCreditCardFee());

            statement.setDouble(31, sale.getAfterTaxDiscount());
            statement.setDouble(32, sale.getGrossReceipt());

            statement.setString(33, sale.getEmployeeId());
            statement.setString(34, sale.getSellerTerminalId());
            statement.setString(35, sale.getPaymentOption().toString());
            statement.setString(36, sale.getPromotions());

            statement.setString(37, sale.getTraceSubmitStatus().toString());
            statement.setLong(38, sale.getMetrcId());

            statement.addBatch();

            count++;

            if (count % batchSize == 0) {
                result = statement.executeBatch();
                LOG.info("Total sales record inserted : " + result.length);
            }
        }
    }

    @Override
    public List<TotalSalesCache> getBracketSales(String companyId, String shopId, Long startDate, Long endDate) {

        String query = "SELECT * " +
                "FROM " + this.getTableName() +
                " WHERE companyId= ? AND shopId= ? AND processedTime > ? AND processedTime < ? AND deleted = FALSE ORDER BY processedTime ASC;";

        try {
            return this.getQueryBuilder().getQueryRunner().query(query, new TotalSalesCacheReportResultSetHandler().getBeanListHandler(), companyId, shopId, startDate, endDate);
        } catch (Exception e) {
            LOG.error("Error while fetching bracket sales : " + e);
        }

        return null;
    }

}
