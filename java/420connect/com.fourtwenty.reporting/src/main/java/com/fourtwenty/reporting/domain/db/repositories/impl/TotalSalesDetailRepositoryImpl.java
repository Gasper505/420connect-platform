package com.fourtwenty.reporting.domain.db.repositories.impl;

import com.fourtwenty.reporting.domain.db.config.DatabaseConfig;
import com.fourtwenty.reporting.domain.db.handler.TotalSalesDetailResultSetHandler;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesDetailRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public class TotalSalesDetailRepositoryImpl extends BaseRepository<TotalSalesDetailCache> implements TotalSalesDetailRepository {

    private static final Log LOG = LogFactory.getLog(TotalSalesDetailRepositoryImpl.class);

    @Inject
    public TotalSalesDetailRepositoryImpl(DatabaseConfig config) {
        super(config.getDataSource(), TotalSalesDetailCache.class);
    }

    @Override
    public Boolean bulkInsert(List<TotalSalesDetailCache> totalSalesDetailCaches) {

        String repeat = StringUtils.repeat("?,", 51);
        String values = repeat.substring(0, repeat.length() - 1);

        String query = "INSERT INTO " + this.getTableName() + "(id, created, modified, deleted, updated, companyId, shopId, orderItemId," +
                "transactionId, prepackageId, transCreated, processedTime, transNo, transType, transactionStatus, productId, memberId, consumerTaxType," +
                "quantity, batch, cogs, retailValue, productDiscount, subTotal, cartDiscount, preAlExciseTax, preNalExciseTax, preCityTax, preCountyTax," +
                "preStateTax, preFederalTax, postAlExciseTax, postNalExciseTax, cityTax, countyTax, stateTax, federalTax, totalTax, afterTaxDiscount, deliveryFee," +
                "creditCardFee, grossReceipt, employeeId, terminalId, paymentOption, promotions, prepackageItemId, factor, cartRefundOption, itemStatus, calcPreTax) values (" + values + ");";

        try (Connection connection = this.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            connection.setAutoCommit(Boolean.FALSE);
            this.prepareSalesDetailInsertData(statement, totalSalesDetailCaches, connection);
            statement.executeBatch();
            connection.commit();
            statement.close();

            return Boolean.TRUE;
        } catch (Exception e) {
            LOG.error("Error while saving total sales detail report data. \tError :" + e);
        }
        return Boolean.FALSE;
    }

    private void prepareSalesDetailInsertData(PreparedStatement statement, List<TotalSalesDetailCache> totalSalesDetailCaches, Connection connection) throws Exception {

        int count = 0;
        int[] result;
        int batchSize = 2000;
        for (TotalSalesDetailCache detail : totalSalesDetailCaches) {
            statement.setString(1, detail.getId());
            statement.setLong(2, detail.getCreated());
            statement.setLong(3, detail.getModified());
            statement.setBoolean(4, detail.isDeleted());
            statement.setBoolean(5, detail.isUpdated());
            statement.setString(6, detail.getCompanyId());
            statement.setString(7, detail.getShopId());
            statement.setString(8, detail.getOrderItemId());
            statement.setString(9, detail.getTransactionId());
            statement.setString(10, detail.getPrepackageId());
            statement.setLong(11, detail.getTransCreated());
            statement.setLong(12, detail.getProcessedTime());
            statement.setString(13, detail.getTransNo());
            statement.setString(14, detail.getTransType().toString());
            statement.setString(15, detail.getTransactionStatus().toString());
            statement.setString(16, detail.getProductId());
            statement.setString(17, detail.getMemberId());
            statement.setString(18, detail.getConsumerTaxType());
            statement.setDouble(19, detail.getQuantity());
            statement.setString(20, detail.getBatch());
            statement.setDouble(21, detail.getCogs());
            statement.setDouble(22, detail.getRetailValue());
            statement.setDouble(23, detail.getProductDiscount());
            statement.setDouble(24, detail.getSubTotal());
            statement.setDouble(25, detail.getCartDiscount());
            statement.setDouble(26, detail.getPreAlExciseTax());
            statement.setDouble(27, detail.getPreNalExciseTax());
            statement.setDouble(28, detail.getPreCityTax());
            statement.setDouble(29, detail.getPreCountyTax());
            statement.setDouble(30, detail.getPreStateTax());
            statement.setDouble(31, detail.getPreFederalTax());
            statement.setDouble(32, detail.getPostAlExciseTax());
            statement.setDouble(33, detail.getPostNalExciseTax());
            statement.setDouble(34, detail.getCityTax());
            statement.setDouble(35, detail.getCountyTax());
            statement.setDouble(36, detail.getStateTax());
            statement.setDouble(37, detail.getFederalTax());
            statement.setDouble(38, detail.getTotalTax());
            statement.setDouble(39, detail.getAfterTaxDiscount());
            statement.setDouble(40, detail.getDeliveryFee());
            statement.setDouble(41, detail.getCreditCardFee());
            statement.setDouble(42, detail.getGrossReceipt());
            statement.setString(43, detail.getEmployeeId());
            statement.setString(44, detail.getTerminalId());
            statement.setString(45, detail.getPaymentOption().toString());
            statement.setString(46, detail.getPromotions());
            statement.setString(47, detail.getPrepackageItemId());
            statement.setLong(48, detail.getFactor());
            statement.setString(49, detail.getCartRefundOption().toString());
            statement.setString(50, detail.getItemStatus().toString());
            statement.setDouble(51, detail.getCalcPreTax());

            statement.addBatch();

            count++;

            if (count % batchSize == 0) {
                result = statement.executeBatch();
                LOG.info("Total sales detail record inserted : " + result.length);
            }
        }
    }

    @Override
    public List<TotalSalesDetailCache> getBracketSales(String companyId, String shopId, Long startDate, Long endDate) {

        String query = "SELECT * " +
                "FROM " + this.getTableName() +
                " WHERE companyId= ? AND shopId= ? AND processedTime > ? AND processedTime < ? AND deleted = FALSE ORDER BY processedTime ASC;";

        try {
            return this.getQueryBuilder().getQueryRunner().query(query, new TotalSalesDetailResultSetHandler().getBeanListHandler(), companyId, shopId, startDate, endDate);
        } catch (Exception e) {
            LOG.error("Error while fetch bracket sales : " + e);
        }

        return null;
    }
}
