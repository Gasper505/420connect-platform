package com.fourtwenty.reporting.domain.models;

import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.reporting.domain.db.handler.ResultSetHandler;

public abstract class CachedReport extends BaseModel {

    public abstract ResultSetHandler getEntityHandler();

}
