package com.fourtwenty.reporting.domain.models;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.reporting.domain.db.handler.ResultSetHandler;
import com.fourtwenty.reporting.domain.db.handler.TotalSalesCacheReportResultSetHandler;

import javax.persistence.Table;

@Table(name = "total_sales")
public class TotalSalesCache extends CachedReport {

    private String shopId;
    private String companyId;
    private String transactionId;
    private long processedTime;
    private String memberId;
    private String transNo;
    private Transaction.TransactionType transType = Transaction.TransactionType.Sale;
    private Transaction.TransactionStatus transactionStatus = Transaction.TransactionStatus.Queued;
    private String consumerTaxType;

    private double cogs;
    private double retailValue;
    private double discounts;

    private double preAlExciseTax;
    private double preNalExciseTax;
    private double preCityTax;
    private double preCountyTax;
    private double preStateTax;

    private double postAlExciseTax;
    private double postNalExciseTax;

    private double cityTax;
    private double countyTax;
    private double stateTax;
    private double totalTax;
    private double deliveryFee;
    private double creditCardFee;
    private double afterTaxDiscount;
    private double grossReceipt;

    private String employeeId;
    private String sellerTerminalId;

    private Cart.PaymentOption paymentOption = Cart.PaymentOption.None;
    private String promotions;
    private Transaction.TraceSubmissionStatus traceSubmitStatus = Transaction.TraceSubmissionStatus.None;
    private Long metrcId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(long processedTime) {
        this.processedTime = processedTime;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public Transaction.TransactionType getTransType() {
        return transType;
    }

    public void setTransType(Transaction.TransactionType transType) {
        this.transType = transType;
    }

    public Transaction.TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Transaction.TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getConsumerTaxType() {
        return consumerTaxType;
    }

    public void setConsumerTaxType(String consumerTaxType) {
        this.consumerTaxType = consumerTaxType;
    }

    public double getCogs() {
        return cogs;
    }

    public void setCogs(double cogs) {
        this.cogs = cogs;
    }

    public double getRetailValue() {
        return retailValue;
    }

    public void setRetailValue(double retailValue) {
        this.retailValue = retailValue;
    }

    public double getDiscounts() {
        return discounts;
    }

    public void setDiscounts(double discounts) {
        this.discounts = discounts;
    }

    public double getPreAlExciseTax() {
        return preAlExciseTax;
    }

    public void setPreAlExciseTax(double preAlExciseTax) {
        this.preAlExciseTax = preAlExciseTax;
    }

    public double getPreNalExciseTax() {
        return preNalExciseTax;
    }

    public void setPreNalExciseTax(double preNalExciseTax) {
        this.preNalExciseTax = preNalExciseTax;
    }

    public double getPreCityTax() {
        return preCityTax;
    }

    public void setPreCityTax(double preCityTax) {
        this.preCityTax = preCityTax;
    }

    public double getPreCountyTax() {
        return preCountyTax;
    }

    public void setPreCountyTax(double preCountyTax) {
        this.preCountyTax = preCountyTax;
    }

    public double getPreStateTax() {
        return preStateTax;
    }

    public void setPreStateTax(double preStateTax) {
        this.preStateTax = preStateTax;
    }

    public double getPostAlExciseTax() {
        return postAlExciseTax;
    }

    public void setPostAlExciseTax(double postAlExciseTax) {
        this.postAlExciseTax = postAlExciseTax;
    }

    public double getPostNalExciseTax() {
        return postNalExciseTax;
    }

    public void setPostNalExciseTax(double postNalExciseTax) {
        this.postNalExciseTax = postNalExciseTax;
    }

    public double getCityTax() {
        return cityTax;
    }

    public void setCityTax(double cityTax) {
        this.cityTax = cityTax;
    }

    public double getCountyTax() {
        return countyTax;
    }

    public void setCountyTax(double countyTax) {
        this.countyTax = countyTax;
    }

    public double getStateTax() {
        return stateTax;
    }

    public void setStateTax(double stateTax) {
        this.stateTax = stateTax;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public double getCreditCardFee() {
        return creditCardFee;
    }

    public void setCreditCardFee(double creditCardFee) {
        this.creditCardFee = creditCardFee;
    }

    public double getAfterTaxDiscount() {
        return afterTaxDiscount;
    }

    public void setAfterTaxDiscount(double afterTaxDiscount) {
        this.afterTaxDiscount = afterTaxDiscount;
    }

    public double getGrossReceipt() {
        return grossReceipt;
    }

    public void setGrossReceipt(double grossReceipt) {
        this.grossReceipt = grossReceipt;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getSellerTerminalId() {
        return sellerTerminalId;
    }

    public void setSellerTerminalId(String sellerTerminalId) {
        this.sellerTerminalId = sellerTerminalId;
    }

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPromotions() {
        return promotions;
    }

    public void setPromotions(String promotions) {
        this.promotions = promotions;
    }

    public Transaction.TraceSubmissionStatus getTraceSubmitStatus() {
        return traceSubmitStatus;
    }

    public void setTraceSubmitStatus(Transaction.TraceSubmissionStatus traceSubmitStatus) {
        this.traceSubmitStatus = traceSubmitStatus;
    }

    public Long getMetrcId() {
        return metrcId;
    }

    public void setMetrcId(Long metrcId) {
        this.metrcId = metrcId;
    }

    @Override
    public ResultSetHandler getEntityHandler() {
        return new TotalSalesCacheReportResultSetHandler();
    }
}
