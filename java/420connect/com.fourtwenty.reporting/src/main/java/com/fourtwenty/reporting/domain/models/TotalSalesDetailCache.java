package com.fourtwenty.reporting.domain.models;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.reporting.domain.db.handler.ResultSetHandler;
import com.fourtwenty.reporting.domain.db.handler.TotalSalesDetailResultSetHandler;

import javax.persistence.Table;

@Table(name = "total_sales_details")
public class TotalSalesDetailCache extends CachedReport {

    private String companyId;
    private String shopId;
    private String orderItemId;
    private String transactionId;
    private String prepackageId;
    private Long transCreated;
    private Long processedTime;
    private String transNo;
    private Transaction.TransactionType transType = Transaction.TransactionType.Sale;
    private Transaction.TransactionStatus transactionStatus = Transaction.TransactionStatus.Queued;
    private String productId;
    private String memberId;
    private String consumerTaxType;
    private double quantity;
    private String batch;
    private double cogs;
    private double retailValue;
    private double productDiscount;
    private double subTotal;
    private double cartDiscount;

    private double preAlExciseTax;
    private double preNalExciseTax;
    private double preCityTax;
    private double preCountyTax;
    private double preStateTax;
    private double preFederalTax;

    private double postAlExciseTax;
    private double postNalExciseTax;
    private double cityTax;
    private double countyTax;
    private double stateTax;
    private double federalTax;
    private double totalTax;

    private double afterTaxDiscount;
    private double deliveryFee;
    private double creditCardFee;
    private double grossReceipt;

    private String employeeId;
    private String terminalId;

    private Cart.PaymentOption paymentOption = Cart.PaymentOption.None;
    private String promotions;

    private String prepackageItemId;

    private int factor = 1;

    private Cart.RefundOption cartRefundOption = Cart.RefundOption.Void;
    private OrderItem.OrderItemStatus itemStatus;
    private double calcPreTax;

    @Override
    public ResultSetHandler getEntityHandler() {
        return new TotalSalesDetailResultSetHandler();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPrepackageId() {
        return prepackageId;
    }

    public void setPrepackageId(String prepackageId) {
        this.prepackageId = prepackageId;
    }

    public Long getTransCreated() {
        return transCreated;
    }

    public void setTransCreated(Long transCreated) {
        this.transCreated = transCreated;
    }

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public Transaction.TransactionType getTransType() {
        return transType;
    }

    public void setTransType(Transaction.TransactionType transType) {
        this.transType = transType;
    }

    public Transaction.TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Transaction.TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getConsumerTaxType() {
        return consumerTaxType;
    }

    public void setConsumerTaxType(String consumerTaxType) {
        this.consumerTaxType = consumerTaxType;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public double getCogs() {
        return cogs;
    }

    public void setCogs(double cogs) {
        this.cogs = cogs;
    }

    public double getRetailValue() {
        return retailValue;
    }

    public void setRetailValue(double retailValue) {
        this.retailValue = retailValue;
    }

    public double getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(double productDiscount) {
        this.productDiscount = productDiscount;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getCartDiscount() {
        return cartDiscount;
    }

    public void setCartDiscount(double cartDiscount) {
        this.cartDiscount = cartDiscount;
    }

    public double getPreAlExciseTax() {
        return preAlExciseTax;
    }

    public void setPreAlExciseTax(double preAlExciseTax) {
        this.preAlExciseTax = preAlExciseTax;
    }

    public double getPreNalExciseTax() {
        return preNalExciseTax;
    }

    public void setPreNalExciseTax(double preNalExciseTax) {
        this.preNalExciseTax = preNalExciseTax;
    }

    public double getPreCityTax() {
        return preCityTax;
    }

    public void setPreCityTax(double preCityTax) {
        this.preCityTax = preCityTax;
    }

    public double getPreCountyTax() {
        return preCountyTax;
    }

    public void setPreCountyTax(double preCountyTax) {
        this.preCountyTax = preCountyTax;
    }

    public double getPreStateTax() {
        return preStateTax;
    }

    public void setPreStateTax(double preStateTax) {
        this.preStateTax = preStateTax;
    }

    public double getPreFederalTax() {
        return preFederalTax;
    }

    public void setPreFederalTax(double preFederalTax) {
        this.preFederalTax = preFederalTax;
    }

    public double getPostAlExciseTax() {
        return postAlExciseTax;
    }

    public void setPostAlExciseTax(double postAlExciseTax) {
        this.postAlExciseTax = postAlExciseTax;
    }

    public double getPostNalExciseTax() {
        return postNalExciseTax;
    }

    public void setPostNalExciseTax(double postNalExciseTax) {
        this.postNalExciseTax = postNalExciseTax;
    }

    public double getCityTax() {
        return cityTax;
    }

    public void setCityTax(double cityTax) {
        this.cityTax = cityTax;
    }

    public double getCountyTax() {
        return countyTax;
    }

    public void setCountyTax(double countyTax) {
        this.countyTax = countyTax;
    }

    public double getStateTax() {
        return stateTax;
    }

    public void setStateTax(double stateTax) {
        this.stateTax = stateTax;
    }

    public double getFederalTax() {
        return federalTax;
    }

    public void setFederalTax(double federalTax) {
        this.federalTax = federalTax;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getAfterTaxDiscount() {
        return afterTaxDiscount;
    }

    public void setAfterTaxDiscount(double afterTaxDiscount) {
        this.afterTaxDiscount = afterTaxDiscount;
    }

    public double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public double getCreditCardFee() {
        return creditCardFee;
    }

    public void setCreditCardFee(double creditCardFee) {
        this.creditCardFee = creditCardFee;
    }

    public double getGrossReceipt() {
        return grossReceipt;
    }

    public void setGrossReceipt(double grossReceipt) {
        this.grossReceipt = grossReceipt;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPromotions() {
        return promotions;
    }

    public void setPromotions(String promotions) {
        this.promotions = promotions;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public Cart.RefundOption getCartRefundOption() {
        return cartRefundOption;
    }

    public void setCartRefundOption(Cart.RefundOption cartRefundOption) {
        this.cartRefundOption = cartRefundOption;
    }

    public OrderItem.OrderItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(OrderItem.OrderItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    public double getCalcPreTax() {
        return calcPreTax;
    }

    public void setCalcPreTax(double calcPreTax) {
        this.calcPreTax = calcPreTax;
    }
}
