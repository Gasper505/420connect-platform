package com.fourtwenty.reporting.gatherer;

import com.fourtwenty.core.event.report.EmailReportEvent;

public interface CachedReportGatherer {

    String DELIMITER = ",";
    String NEW_LINE_SEPARATOR = "\n";

    StringBuilder cacheGatherer(EmailReportEvent event);

}
