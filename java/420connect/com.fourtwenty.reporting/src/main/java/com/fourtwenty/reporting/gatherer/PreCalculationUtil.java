package com.fourtwenty.reporting.gatherer;

import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class PreCalculationUtil {

    public static HashMap<String, Double> calculatePropRatio(Transaction transaction, Cart cart, HashMap<String, Product> productMap) {
        HashMap<String, Double> propRatioMap = new HashMap<>();

        if (cart.getItems().size() > 0) {
            double total = 0.0;
            for (OrderItem orderItem : cart.getItems()) {
                Product product = productMap.get(orderItem.getProductId());
                if (product != null) {
                    if (product.isDiscountable()) {
                        total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                    }
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : cart.getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == cart.getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                Product product = productMap.get(orderItem.getProductId());
                if (product != null) {

                    propRatioMap.put(orderItem.getId(), 1d); // 100 %
                    if (product.isDiscountable() && total > 0) {
                        double finalCost = orderItem.getFinalPrice().doubleValue();
                        double ratio = finalCost / total;

                        propRatioMap.put(orderItem.getId(), ratio);
                    }
                }

            }
        }

        return propRatioMap;
    }

    public static HashMap<String, ProductBatch> getRecentBatch(HashMap<String, ProductBatch> productBatchMap) {
        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();

        for (ProductBatch batch : productBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        return recentBatchMap;
    }

    public static StringBuilder getPromotions(Cart cart, HashMap<String, Promotion> promotionMap, HashMap<String, LoyaltyReward> rewardMap) {
        LinkedHashSet<PromotionReq> promos = cart.getPromotionReqs();
        StringBuilder sb = new StringBuilder();
        Iterator<PromotionReq> it = promos.iterator();

        while (it.hasNext()) {
            PromotionReq promotionReq = it.next();
            if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                if (promotion != null) {
                    sb.append(promotion.getName());
                    if (it.hasNext()) {
                        sb.append("; ");
                    }
                }
            } else {
                LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                if (reward != null) {
                    sb.append(reward.getName());
                    if (it.hasNext()) {
                        sb.append("; ");
                    }
                }
            }
        }

        return sb;
    }

    public static double calculateItemCogs(OrderItem item, Cart cart, Product product, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageMap, HashMap<String, ProductWeightTolerance> weightToleranceHashMap, HashMap<String, ProductBatch> recentBatchMap, HashMap<String, List<Product>> productsByCompanyLinkId) {
        boolean calculated = false;
        double itemCogs = 0;
        PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(item.getPrepackageItemId());
        if (prepackageProductItem != null) {
            ProductBatch targetBatch = productBatchMap.get(prepackageProductItem.getBatchId());

            Prepackage prepackage = prepackageMap.get(prepackageProductItem.getPrepackageId());
            if (prepackage != null && targetBatch != null) {
                calculated = true;
                BigDecimal unitValue = prepackage.getUnitValue();
                if (unitValue == null || unitValue.doubleValue() == 0) {
                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                    unitValue = weightTolerance.getUnitValue();
                }
                // calculate the total quantity based on the prepackage value
                double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                itemCogs += calcCOGS(unitsSold, targetBatch);

            }
        } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
            // otherwise, use quantity logs
            for (QuantityLog quantityLog : item.getQuantityLogs()) {
                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                    ProductBatch targetBatch = productBatchMap.get(quantityLog.getBatchId());
                    if (targetBatch != null) {
                        calculated = true;
                        itemCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                    }
                }
            }
        }

        if (!calculated) {
            double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
            itemCogs = unitCost * item.getQuantity().doubleValue();
        }

        return itemCogs;
    }

    public static double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    public static double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}
