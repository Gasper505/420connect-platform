package com.fourtwenty.reporting.gatherer.impl;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.event.report.EmailReportEvent;
import com.fourtwenty.core.reporting.gather.impl.transaction.SalesByProductCategoryGatherer;
import com.fourtwenty.core.util.BLStringUtils;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesDetailRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import com.fourtwenty.reporting.gatherer.CachedReportGatherer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SalesByProductCategoryReport implements CachedReportGatherer {

    private static final String FILE_HEADERS = "Product Category, # Trans, COGS, Retail Value, Product Discounts, Subtotal Sales, Cart Discounts, Sales," +
            "Pre ALExcise Tax, Pre NALExcise Tax, Post ALExcise Tax, Post NALExcise Tax, City Tax, County Tax, State Tax, Federal Tax, Total Tax," +
            "Delivery Fees, Credit/Debit Card Fees, After Tax Discounts, Gross Receipt";
    private static final Log LOG = LogFactory.getLog(SalesByProductCategoryReport.class);
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private TotalSalesDetailRepository totalSalesDetailRepository;

    @Override
    public StringBuilder cacheGatherer(EmailReportEvent event) {

        String companyId = event.getCompanyId();
        String shopId = event.getShopId();
        Long startDate = event.getStartDate();
        Long endDate = event.getEndDate();

        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, shopId);
        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(companyId, shopId);

        List<TotalSalesDetailCache> bracketSales = totalSalesDetailRepository.getBracketSales(companyId, shopId, startDate, endDate);

        if (bracketSales == null) {
            LOG.error("Error while fetching cache data in sales by product category report");
            return null;
        }

        StringBuilder builder = new StringBuilder(FILE_HEADERS);
        builder.append(NEW_LINE_SEPARATOR);

        HashMap<String, SalesByProductCategoryGatherer.CategorySale> results = new HashMap<>();

        List<String> visited = new ArrayList<>();
        for (TotalSalesDetailCache sale : bracketSales) {

            Product product = productMap.get(sale.getProductId());
            if (product == null) {
                continue;
            }

            if ((Transaction.TransactionType.Refund == sale.getTransType() && Cart.RefundOption.Void == sale.getCartRefundOption())
                    && OrderItem.OrderItemStatus.Refunded == sale.getItemStatus()) {
                continue;
            }

            String catName = categoryMap.get(product.getCategoryId()).getName();
            SalesByProductCategoryGatherer.CategorySale categorySale = results.get(catName);
            if (categorySale == null) {
                categorySale = new SalesByProductCategoryGatherer.CategorySale();
                results.put(catName, categorySale);
            }

            if (!visited.contains(product.getCategoryId() + "#" + sale.getTransactionId())) {
                categorySale.transCount++;
                visited.add(product.getCategoryId() + "#" + sale.getTransactionId());
            }

            double sales = NumberUtils.round(sale.getSubTotal() - sale.getCartDiscount(), 5);

            categorySale.name = catName;
            categorySale.retailValue += sale.getRetailValue();
            categorySale.itemDiscounts += sale.getProductDiscount();
            categorySale.subTotals += sale.getSubTotal();
            categorySale.discounts += sale.getCartDiscount();
            categorySale.sales += sales;

            categorySale.totalPreTax += sale.getCalcPreTax();
            categorySale.preALExciseTax += sale.getPreAlExciseTax();
            categorySale.preNALExciseTax += sale.getPreNalExciseTax();
            categorySale.postALExciseTax += sale.getPostAlExciseTax();
            categorySale.postNALExciseTax += sale.getPostNalExciseTax();

            categorySale.cityTax += sale.getCityTax();
            categorySale.countyTax += sale.getCountyTax();
            categorySale.stateTax += sale.getStateTax();
            categorySale.federalTax += sale.getFederalTax();
            categorySale.tax += sale.getTotalTax();

            categorySale.deliveryFees += sale.getDeliveryFee();
            categorySale.creditCardFees += sale.getCreditCardFee();
            categorySale.afterTaxDiscount += sale.getAfterTaxDiscount();

            categorySale.grossReceipt += sale.getGrossReceipt();
            categorySale.cogs += sale.getCogs();

        }

        for (String name : results.keySet()) {

            SalesByProductCategoryGatherer.CategorySale sale = results.get(name);

            builder.append(BLStringUtils.appendDQ(sale.name)).append(DELIMITER);
            builder.append(sale.transCount).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.cogs, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.retailValue, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.itemDiscounts, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.subTotals, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.discounts, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.sales, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.preALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.preNALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.postALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.postNALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.cityTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.countyTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.stateTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.federalTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.tax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.deliveryFees, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.creditCardFees, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.afterTaxDiscount, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.grossReceipt, null))).append(DELIMITER);

            builder.append(NEW_LINE_SEPARATOR);
        }

        return builder;
    }
}
