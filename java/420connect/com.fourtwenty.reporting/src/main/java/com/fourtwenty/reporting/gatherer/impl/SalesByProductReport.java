package com.fourtwenty.reporting.gatherer.impl;

import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.event.report.EmailReportEvent;
import com.fourtwenty.core.reporting.gather.impl.transaction.SalesByProductGatherer;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.util.BLStringUtils;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesDetailRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import com.fourtwenty.reporting.gatherer.CachedReportGatherer;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class SalesByProductReport implements CachedReportGatherer {

    private static final String FILE_HEADERS = "Product, Category, Vendor, Brand, Units Sold, COGS, Retail Value, Subtotal Sales," +
            "Pre ALExcise Tax, Pre NALExcise Tax, Post ALExcise Tax, Post NALExcise Tax, City Tax, County Tax, State Tax, Federal Tax," +
            "Total Tax, Credit/Debit Card Fees, Delivery Fees, After Tax Discounts, Margin, Revenue/Unit, Gross receipts, % of Sales";
    private static final Log LOG = LogFactory.getLog(SalesByProductReport.class);

    @Inject
    private BrandRepository brandRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private TotalSalesDetailRepository totalSalesDetailRepository;

    @Override
    public StringBuilder cacheGatherer(EmailReportEvent event) {

        String companyId = event.getCompanyId();
        String shopId = event.getShopId();
        Long startDate = event.getStartDate();
        Long endDate = event.getEndDate();

        HashMap<String, Brand> brandMap = brandRepository.listAsMap(companyId);
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(companyId);
        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, shopId);
        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(companyId, shopId);
        HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(companyId, shopId);

        List<TotalSalesDetailCache> bracketSales = totalSalesDetailRepository.getBracketSales(companyId, shopId, startDate, endDate);

        if (bracketSales == null) {
            LOG.error("Error while fetching cache data in sales by product report");
            return null;
        }

        StringBuilder builder = new StringBuilder(FILE_HEADERS);
        builder.append(NEW_LINE_SEPARATOR);

        double totalSales = 0d;
        final String nAvail = "N/A";
        HashMap<String, SalesByProductGatherer.ProductSale> productSales = new HashMap<>();
        for (TotalSalesDetailCache sale : bracketSales) {

            Product product = productMap.get(sale.getProductId());

            if (product == null) {
                continue;
            }

            int factor = sale.getFactor();

            String pId = product.getId();

            SalesByProductGatherer.ProductSale productSale = productSales.get(pId);
            if (productSale == null) {
                productSale = new SalesByProductGatherer.ProductSale();
                productSales.put(pId, productSale);
            }

            String productName = product.getName();
            Prepackage prepackage = prepackageMap.get(sale.getPrepackageId());
            if (prepackage != null) {
                productName = String.format("%s (%s)", product.getName(), prepackage.getName());
            }

            ProductCategory category = categoryMap.get(product.getCategoryId());
            String categoryName = category != null ? category.getName() : nAvail;

            Vendor vendor = vendorMap.get(product.getVendorId());
            String vendorName = vendor != null ? vendor.getName() : nAvail;

            Brand brand = brandMap.get(product.getBrandId());
            String brandName = brand != null ? brand.getName() : nAvail;

            double quantitySold = sale.getQuantity();
            productSale.totalSold += NumberUtils.round((quantitySold * factor), 2);

            double newMargin = ((sale.getSubTotal() * factor) - (sale.getCogs() * factor));
            double margins = productSale.margins + (newMargin * factor);
            double revUnit = 0;
            if (productSale.totalSold > 0) {
                revUnit = margins / productSale.totalSold;
            }

            productSale.name = productName;
            productSale.categoryName = categoryName;
            productSale.vendor = vendorName;
            productSale.brand = brandName;
            productSale.retailValue += sale.getRetailValue();
            productSale.subTotals += sale.getSubTotal();
            productSale.cogs += sale.getCogs();
            productSale.preALExciseTax += sale.getPreAlExciseTax();
            productSale.preNALExciseTax += sale.getPreNalExciseTax();
            productSale.postALExciseTax += sale.getPostAlExciseTax();
            productSale.postNALExciseTax += sale.getPostNalExciseTax();
            productSale.cityTax += sale.getCityTax();
            productSale.countyTax += sale.getCountyTax();
            productSale.stateTax += sale.getStateTax();
            productSale.federalTax += sale.getFederalTax();
            productSale.totalPostTax += sale.getTotalTax();
            productSale.margins = margins * factor;
            productSale.revenuePerUnit = revUnit * factor;
            productSale.percentageOfSales = 0;
            productSale.creditCardFees += sale.getCreditCardFee();
            productSale.discount += sale.getCartDiscount();
            productSale.afterTaxDiscount += sale.getAfterTaxDiscount();
            productSale.deliveryFees += sale.getDeliveryFee();
            productSale.grossReceipt += sale.getGrossReceipt();

            totalSales += sale.getSubTotal();
        }

        List<SalesByProductGatherer.ProductSale> sales = Lists.newArrayList(productSales.values());
        sales.sort(new Comparator<SalesByProductGatherer.ProductSale>() {
            @Override
            public int compare(SalesByProductGatherer.ProductSale o1, SalesByProductGatherer.ProductSale o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        for (SalesByProductGatherer.ProductSale sale : sales) {

            builder.append(BLStringUtils.appendDQ(sale.name)).append(DELIMITER);
            builder.append(sale.categoryName).append(DELIMITER);
            builder.append(sale.vendor).append(DELIMITER);
            builder.append(sale.brand).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.totalSold, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.cogs, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.retailValue, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.subTotals, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.preALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.preNALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.postALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.postNALExciseTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.cityTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.countyTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.stateTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.federalTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.totalPostTax, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.creditCardFees, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.deliveryFees, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.afterTaxDiscount, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.margins, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.revenuePerUnit, null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.grossReceipt, null))).append(DELIMITER);

            String percentage = new Percentage(NumberUtils.round((sale.subTotals / totalSales), 2)) + "%";
            builder.append(percentage);

            builder.append(NEW_LINE_SEPARATOR);
        }


        return builder;
    }
}
