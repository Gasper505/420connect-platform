package com.fourtwenty.reporting.gatherer.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.event.report.EmailReportEvent;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.BLStringUtils;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesDetailRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import com.fourtwenty.reporting.gatherer.CachedReportGatherer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class TotalSalesDetailReport implements CachedReportGatherer {

    private static final Log LOG = LogFactory.getLog(TotalSalesDetailReport.class);

    private static final String FILE_HEADERS = "Date, Trans No., Trans Type, Trans Status, Product Name, Product Category, Brand," +
            "Vendor, Member, Consumer Tax Type, Cannabis, Quantity Sold, Batch, COGs, Retail Value, Product Discounts, Subtotal," +
            "Cart Discounts, Pre ALExcise Tax, Pre NALExcise Tax, Pre City Tax, Pre County Tax, Pre State Tax, Pre Federal Tax," +
            "Post ALExcise Tax, Post NALExcise Tax, City Tax, County Tax, State Tax, Federal Tax, Total Tax, After Tax Discount," +
            "Delivery Fees, Credit Card Fees, Gross Receipt, Employee, Terminal, Payment Type, Promotion(s), Marketing Source," +
            "Member Group, Zip Code, Member State, Date Joined, Gender, DOB, Age, Loyalty Points";
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private TotalSalesDetailRepository totalSalesDetailRepository;

    @Override
    public StringBuilder cacheGatherer(EmailReportEvent event) {

        String companyId = event.getCompanyId();
        String shopId = event.getShopId();
        Long startDate = event.getStartDate();
        Long endDate = event.getEndDate();
        int timezoneOffset = event.getTimezoneOffset();

        Shop shop = shopRepository.get(companyId, shopId);
        HashMap<String, Brand> brandMap = brandRepository.listAsMap(companyId);
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(companyId);
        HashMap<String, Member> memberMap = memberRepository.listAsMap(companyId);
        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(companyId);
        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, shopId);
        HashMap<String, Terminal> terminalMap = terminalRepository.listAsMap(companyId, shopId);
        HashMap<String, MemberGroup> memberGroupMap = memberGroupRepository.listAsMap(companyId, shopId);
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(companyId, shopId);
        HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(companyId, shopId);

        List<TotalSalesDetailCache> bracketSales = totalSalesDetailRepository.getBracketSales(companyId, shopId, startDate, endDate);

        if (bracketSales == null) {
            LOG.error("Error while fetching data in total sales report");
            return null;
        }

        StringBuilder builder = new StringBuilder(FILE_HEADERS);
        builder.append(NEW_LINE_SEPARATOR);

        final String nAvail = "N/A";
        for (TotalSalesDetailCache sale : bracketSales) {

            Product product = productMap.get(sale.getProductId());
            if (product == null) {
                continue;
            }

            ProductCategory category = productCategoryMap.get(product.getCategoryId());
            Brand brand = brandMap.get(product.getBrandId());
            Vendor vendor = vendorMap.get(product.getVendorId());
            Member member = memberMap.get(sale.getMemberId());
            Employee employee = employeeMap.get(sale.getEmployeeId());
            Terminal terminal = terminalMap.get(sale.getTerminalId());


            boolean cannabis = Boolean.FALSE;
            if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                    || (product.getCannabisType() != Product.CannabisType.CBD
                    && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                    && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                cannabis = true;
            }

            String unitTypeStr = (category != null && category.getUnitType() == ProductCategory.UnitType.grams) ? "g" : "ea";

            String productName = product.getName();
            Prepackage prepackage = prepackageMap.get(sale.getPrepackageId());
            if (prepackage != null) {
                productName = String.format("%s (%s)", product.getName(), prepackage.getName());
            }

            String empName = nAvail;
            if (employee != null) {
                empName = employee.getFirstName() + " " + employee.getLastName();
            }

            String terminalName = nAvail;
            if (terminal != null) {
                terminalName = terminal.getName();
            }

            String gender = nAvail;
            String memberName = nAvail;
            String memberGroup = nAvail;
            String zipCode = nAvail;
            String memberState = nAvail;
            String marketingSource = nAvail;
            if (member != null) {
                memberName = member.getFirstName() + " " + member.getLastName();
                gender = member.getSex().name();

                if (memberGroupMap.containsKey(member.getMemberGroupId())) {
                    memberGroup = memberGroupMap.get(member.getMemberGroupId()).getName();
                }

                if (member.getAddress() != null) {
                    zipCode = member.getAddress().getZipCode();
                    memberState = member.getAddress().getState();
                }

                marketingSource = member.getMarketingSource();
            }

            String quantity = sale.getQuantity() + " " + unitTypeStr;

            builder.append(ProcessorUtil.timeStampWithOffsetLong(sale.getProcessedTime(), timezoneOffset)).append(DELIMITER);
            builder.append(sale.getTransNo()).append(DELIMITER);
            builder.append(sale.getTransType()).append(DELIMITER);
            builder.append(sale.getTransactionStatus()).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(productName)).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(category.getName())).append(DELIMITER);
            builder.append((brand == null || StringUtils.isBlank(brand.getName())) ? nAvail : brand.getName()).append(DELIMITER);
            builder.append(vendor == null ? nAvail : vendor.getName()).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(memberName)).append(DELIMITER);
            builder.append(Objects.isNull(sale.getConsumerTaxType()) ? nAvail : sale.getConsumerTaxType()).append(DELIMITER);
            builder.append(cannabis ? "Yes" : "No").append(DELIMITER);
            builder.append(quantity).append(DELIMITER);
            builder.append(StringUtils.isBlank(sale.getBatch()) ? nAvail : BLStringUtils.appendDQ(sale.getBatch())).append(DELIMITER);

            //As there might be "," in result from TextUtil.toCurrencyFormat's result so escaping it with BLStringUtils.appendDQ
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCogs(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getRetailValue(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getProductDiscount(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getSubTotal(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCartDiscount(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreAlExciseTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreNalExciseTax(), null))).append(DELIMITER);

            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreCityTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreCountyTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreStateTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreFederalTax(), null))).append(DELIMITER);

            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPostAlExciseTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPostNalExciseTax(), null))).append(DELIMITER);

            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCityTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCountyTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getStateTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getFederalTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getTotalTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getAfterTaxDiscount(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getDeliveryFee(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCreditCardFee(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getGrossReceipt(), null))).append(DELIMITER);

            builder.append(empName).append(DELIMITER);
            builder.append(terminalName).append(DELIMITER);
            builder.append(sale.getPaymentOption()).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(sale.getPromotions())).append(DELIMITER);
            builder.append(StringUtils.isBlank(marketingSource) ? nAvail : BLStringUtils.appendDQ(marketingSource)).append(DELIMITER);
            builder.append(memberGroup).append(DELIMITER);

            builder.append(zipCode).append(DELIMITER);
            builder.append(memberState).append(DELIMITER);
            builder.append(DateUtil.toDateFormatted(member.getStartDate(), shop.getTimeZone())).append(DELIMITER);
            builder.append(gender).append(DELIMITER);
            builder.append(DateUtil.toDateFormatted(member.getDob())).append(DELIMITER);
            builder.append(member.getDob() != null ? DateUtil.getYearsBetweenTwoDates(member.getDob(), DateTime.now().getMillis()) : "").append(DELIMITER);
            builder.append(NumberUtils.round(member.getLoyaltyPoints(), 2));

            builder.append(NEW_LINE_SEPARATOR);
        }

        return builder;
    }

}
