package com.fourtwenty.reporting.gatherer.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.event.report.EmailReportEvent;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.BLStringUtils;
import com.fourtwenty.core.util.TextUtil;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesCacheRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesCache;
import com.fourtwenty.reporting.gatherer.CachedReportGatherer;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class TotalSalesReport implements CachedReportGatherer {

    private static final String FILE_HEADERS = "Date, Member, Trans No., Trans Type, Trans Status, Consumer Tax Type, COGs, Retail Value, Discounts," +
            "Pre AL Excise Tax, Pre NAL Excise Tax, Pre City Tax, Pre County Tax, Pre State Tax, Post AL Excise Tax, POST NAL Excise  Tax, City Tax, " +
            "County Tax, State Tax, Total Tax, Delivery Fee, Credit/Debit Card Fee, After Tax Discount, Gross Receipt, Employee, Terminal, Payment Type," +
            "Promotions(s), Marketing Source, MetrcId";

    @Inject
    private MetrcService metrcService;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;
    @Inject
    private TotalSalesCacheRepository totalSalesCacheRepository;

    @Override
    public StringBuilder cacheGatherer(EmailReportEvent event) {

        String companyId = event.getCompanyId();
        String shopId = event.getShopId();
        Long startDate = event.getStartDate();
        Long endDate = event.getEndDate();
        int timezoneOffset = event.getTimezoneOffset();

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(companyId);
        HashMap<String, Member> memberMap = memberRepository.listAsMap(companyId);
        HashMap<String, Terminal> terminalMap = terminalRepository.listAsMap(companyId, shopId);

        boolean enableMetrc = false;
        String stateCode = metrcService.getShopStateCode(companyId, shopId);
        MetrcAccount metrcAccount = metrcAccountRepository.getMetrcAccount(companyId, stateCode);
        if (metrcAccount != null) {
            for (MetrcFacilityAccount metrcFacilityAccount : metrcAccount.getFacilities()) {
                if (metrcFacilityAccount.getShopId() != null && metrcFacilityAccount.getShopId().equalsIgnoreCase(shopId)) {
                    if (metrcFacilityAccount.isEnabled()) {
                        enableMetrc = true;
                    }
                }
            }
        }

        StringBuilder builder = new StringBuilder(FILE_HEADERS);
        builder.append(NEW_LINE_SEPARATOR);

        List<TotalSalesCache> totalSales = totalSalesCacheRepository.getBracketSales(companyId, shopId, startDate, endDate);

        final String nAvail = "N/A";
        for (TotalSalesCache sale : totalSales) {

            Member member = memberMap.get(sale.getMemberId());
            if (member == null) {
                continue;
            }

            String memberName = member.getFirstName() + " " + member.getLastName();

            Employee employee = employeeMap.get(sale.getEmployeeId());
            String employeeName = nAvail;
            if (employee != null) {
                employeeName = employee.getFirstName() + " " + employee.getLastName();
            }

            Terminal terminal = terminalMap.get(sale.getSellerTerminalId());
            String terminalName = nAvail;
            if (terminal != null) {
                terminalName = terminal.getName();
            }

            String metrcId = "Not Submitted";
            if (enableMetrc) {
                if (sale.getMetrcId() != null && sale.getMetrcId() != 0) {
                    metrcId = String.valueOf(sale.getMetrcId());
                } else if (Transaction.TraceSubmissionStatus.None == sale.getTraceSubmitStatus()) {
                    metrcId = sale.getTraceSubmitStatus().name();
                    if (Transaction.TraceSubmissionStatus.SubmissionPending == sale.getTraceSubmitStatus()) {
                        metrcId = Transaction.TraceSubmissionStatus.SubmissionWaitingID.name();
                    }
                }
            }

            builder.append(ProcessorUtil.timeStampWithOffsetLong(sale.getProcessedTime(), timezoneOffset)).append(DELIMITER);
            builder.append(memberName).append(DELIMITER);
            builder.append(sale.getTransNo()).append(DELIMITER);
            builder.append(sale.getTransType()).append(DELIMITER);
            builder.append(sale.getTransactionStatus()).append(DELIMITER);
            builder.append(sale.getConsumerTaxType()).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCogs(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getRetailValue(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getDiscounts(), null))).append(DELIMITER);

            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreAlExciseTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreNalExciseTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreCityTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPreCountyTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getStateTax(), null))).append(DELIMITER);

            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPostAlExciseTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getPostNalExciseTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCityTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCountyTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getStateTax(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getTotalTax(), null))).append(DELIMITER);

            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getDeliveryFee(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getCreditCardFee(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getAfterTaxDiscount(), null))).append(DELIMITER);
            builder.append(BLStringUtils.appendDQ(TextUtil.toCurrencyFormat(sale.getGrossReceipt(), null))).append(DELIMITER);

            builder.append(employeeName).append(DELIMITER);
            builder.append(terminalName).append(DELIMITER);
            builder.append(sale.getPaymentOption()).append(DELIMITER);
            builder.append(StringUtils.isBlank(sale.getPromotions()) ? nAvail : BLStringUtils.appendDQ(sale.getPromotions())).append(DELIMITER);
            builder.append(StringUtils.isBlank(member.getMarketingSource()) ? nAvail : BLStringUtils.appendDQ(member.getMarketingSource())).append(DELIMITER);

            builder.append(metrcId).append(DELIMITER);
            builder.append(NEW_LINE_SEPARATOR);
        }
        return builder;
    }
}
