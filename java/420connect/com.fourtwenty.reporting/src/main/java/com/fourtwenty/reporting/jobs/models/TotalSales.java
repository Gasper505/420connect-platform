package com.fourtwenty.reporting.jobs.models;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.util.HashMap;
import java.util.List;

public class TotalSales extends PreCalculationBaseModel {

    private List<Transaction> transactionList;
    private HashMap<String, Member> memberMap;
    private HashMap<String, Product> productMap;
    private HashMap<String, Promotion> promotionMap;
    private HashMap<String, LoyaltyReward> rewardMap;
    private HashMap<String, Prepackage> prepackageMap;
    private HashMap<String, ProductBatch> productBatchMap;
    private HashMap<String, ProductWeightTolerance> weightToleranceHashMap;
    private HashMap<String, PrepackageProductItem> prepackageProductItemMap;

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public HashMap<String, Member> getMemberMap() {
        return memberMap;
    }

    public void setMemberMap(HashMap<String, Member> memberMap) {
        this.memberMap = memberMap;
    }

    public HashMap<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(HashMap<String, Product> productMap) {
        this.productMap = productMap;
    }

    public HashMap<String, Promotion> getPromotionMap() {
        return promotionMap;
    }

    public void setPromotionMap(HashMap<String, Promotion> promotionMap) {
        this.promotionMap = promotionMap;
    }

    public HashMap<String, LoyaltyReward> getRewardMap() {
        return rewardMap;
    }

    public void setRewardMap(HashMap<String, LoyaltyReward> rewardMap) {
        this.rewardMap = rewardMap;
    }

    public HashMap<String, Prepackage> getPrepackageMap() {
        return prepackageMap;
    }

    public void setPrepackageMap(HashMap<String, Prepackage> prepackageMap) {
        this.prepackageMap = prepackageMap;
    }

    public HashMap<String, ProductBatch> getProductBatchMap() {
        return productBatchMap;
    }

    public void setProductBatchMap(HashMap<String, ProductBatch> productBatchMap) {
        this.productBatchMap = productBatchMap;
    }

    public HashMap<String, ProductWeightTolerance> getWeightToleranceHashMap() {
        return weightToleranceHashMap;
    }

    public void setWeightToleranceHashMap(HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        this.weightToleranceHashMap = weightToleranceHashMap;
    }

    public HashMap<String, PrepackageProductItem> getPrepackageProductItemMap() {
        return prepackageProductItemMap;
    }

    public void setPrepackageProductItemMap(HashMap<String, PrepackageProductItem> prepackageProductItemMap) {
        this.prepackageProductItemMap = prepackageProductItemMap;
    }
}
