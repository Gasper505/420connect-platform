package com.fourtwenty.reporting.jobs.models;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TotalSalesProduct extends PreCalculationBaseModel {

    private List<Transaction> transactions = new ArrayList<>();
    private HashMap<String, Member> memberMap = new HashMap<>();
    private HashMap<String, ProductCategory> productCategoryMap = new HashMap<>();
    private HashMap<String, ProductWeightTolerance> weightToleranceMap = new HashMap<>();
    private HashMap<String, Promotion> promotionMap = new HashMap<>();
    private HashMap<String, LoyaltyReward> loyaltyRewardMap = new HashMap<>();
    private HashMap<String, Product> productMap = new HashMap<>();
    private HashMap<String, ProductBatch> productBatchMap = new HashMap<>();
    private HashMap<String, Prepackage> prepackageMap = new HashMap<>();
    private HashMap<String, PrepackageProductItem> prepackageProductItemMap = new HashMap<>();


    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public HashMap<String, Member> getMemberMap() {
        return memberMap;
    }

    public void setMemberMap(HashMap<String, Member> memberMap) {
        this.memberMap = memberMap;
    }

    public HashMap<String, ProductCategory> getProductCategoryMap() {
        return productCategoryMap;
    }

    public void setProductCategoryMap(HashMap<String, ProductCategory> productCategoryMap) {
        this.productCategoryMap = productCategoryMap;
    }

    public HashMap<String, ProductWeightTolerance> getWeightToleranceMap() {
        return weightToleranceMap;
    }

    public void setWeightToleranceMap(HashMap<String, ProductWeightTolerance> weightToleranceMap) {
        this.weightToleranceMap = weightToleranceMap;
    }

    public HashMap<String, Promotion> getPromotionMap() {
        return promotionMap;
    }

    public void setPromotionMap(HashMap<String, Promotion> promotionMap) {
        this.promotionMap = promotionMap;
    }

    public HashMap<String, LoyaltyReward> getLoyaltyRewardMap() {
        return loyaltyRewardMap;
    }

    public void setLoyaltyRewardMap(HashMap<String, LoyaltyReward> loyaltyRewardMap) {
        this.loyaltyRewardMap = loyaltyRewardMap;
    }

    public HashMap<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(HashMap<String, Product> productMap) {
        this.productMap = productMap;
    }

    public HashMap<String, ProductBatch> getProductBatchMap() {
        return productBatchMap;
    }

    public void setProductBatchMap(HashMap<String, ProductBatch> productBatchMap) {
        this.productBatchMap = productBatchMap;
    }

    public HashMap<String, Prepackage> getPrepackageMap() {
        return prepackageMap;
    }

    public void setPrepackageMap(HashMap<String, Prepackage> prepackageMap) {
        this.prepackageMap = prepackageMap;
    }

    public HashMap<String, PrepackageProductItem> getPrepackageProductItemMap() {
        return prepackageProductItemMap;
    }

    public void setPrepackageProductItemMap(HashMap<String, PrepackageProductItem> prepackageProductItemMap) {
        this.prepackageProductItemMap = prepackageProductItemMap;
    }

}
