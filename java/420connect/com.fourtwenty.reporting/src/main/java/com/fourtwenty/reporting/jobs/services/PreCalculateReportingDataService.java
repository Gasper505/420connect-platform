package com.fourtwenty.reporting.jobs.services;

import com.fourtwenty.reporting.domain.models.CachedReport;
import com.fourtwenty.reporting.jobs.models.PreCalculationBaseModel;

import java.util.List;

public interface PreCalculateReportingDataService<T extends PreCalculationBaseModel, E extends CachedReport> {

    List<E> preCalculateData(T t);

}
