package com.fourtwenty.reporting.jobs.services.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.reporting.domain.models.TotalSalesCache;
import com.fourtwenty.reporting.gatherer.PreCalculationUtil;
import com.fourtwenty.reporting.jobs.models.TotalSales;
import com.fourtwenty.reporting.jobs.services.PreCalculateReportingDataService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TotalSalesCalculations implements PreCalculateReportingDataService<TotalSales, TotalSalesCache> {


    public List<TotalSalesCache> preCalculateData(TotalSales sales) {

        Company company = sales.getCompany();
        Shop shop = sales.getShop();
        HashMap<String, Member> memberMap = sales.getMemberMap();
        HashMap<String, Product> productMap = sales.getProductMap();
        List<Transaction> transactionList = sales.getTransactionList();
        HashMap<String, LoyaltyReward> rewardMap = sales.getRewardMap();
        HashMap<String, Promotion> promotionMap = sales.getPromotionMap();
        HashMap<String, Prepackage> prepackageMap = sales.getPrepackageMap();
        HashMap<String, ProductBatch> productBatchMap = sales.getProductBatchMap();
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = sales.getWeightToleranceHashMap();
        HashMap<String, PrepackageProductItem> prepackageProductItemMap = sales.getPrepackageProductItemMap();

        HashMap<String, ProductBatch> recentBatchMap = PreCalculationUtil.getRecentBatch(productBatchMap);


        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : productMap.values()) {
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        List<TotalSalesCache> totalSalesCaches = new ArrayList<>();
        int factor = 1;
        for (Transaction trans : transactionList) {
            factor = 1;
            if (trans.getTransType() == Transaction.TransactionType.Refund && trans.getCart() != null && trans.getCart() != null && trans.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            Member member = memberMap.get(trans.getMemberId());
            if (member == null) {
                continue;
            }

            Cart cart = trans.getCart();

            String consumerTaxType = "N/A";
            if (cart.getTaxTable() != null) {
                consumerTaxType = cart.getTaxTable().getName();
            }

            double totalPostTax = 0.0;
            totalPostTax = cart.getTotalCalcTax().doubleValue() - cart.getTotalPreCalcTax().doubleValue();
            if (totalPostTax < 0) {
                totalPostTax = 0;
            }

            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;

            double preCityTax = 0;
            double preCountyTax = 0;
            double preStateTax = 0;

            double exciseTax = 0;
            double preALExciseTax = 0d;
            double preNALExciseTax = 0d;
            double postALExciseTax = 0d;
            double postNALExciseTax = 0d;
            double afterTaxDiscount = 0d;

            if (cart.getTaxResult() != null) {
                exciseTax = cart.getTaxResult().getTotalExciseTax().doubleValue();
                postALExciseTax = cart.getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = cart.getTaxResult().getTotalExciseTax().doubleValue();
                preALExciseTax = cart.getTaxResult().getTotalALExciseTax().doubleValue();
                preNALExciseTax = cart.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                cityTax = cart.getTaxResult().getTotalCityTax().doubleValue();
                countyTax = cart.getTaxResult().getTotalCountyTax().doubleValue();
                stateTax = cart.getTaxResult().getTotalStateTax().doubleValue();

                preCityTax = cart.getTaxResult().getTotalCityPreTax().doubleValue();
                preCountyTax = cart.getTaxResult().getTotalCountyPreTax().doubleValue();
                preStateTax = cart.getTaxResult().getTotalStatePreTax().doubleValue();
            }

            if (cart.getAppliedAfterTaxDiscount() != null && cart.getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = cart.getAppliedAfterTaxDiscount().doubleValue();
            }

            double creditCardFees = 0;
            if (cart.getCreditCardFee().doubleValue() > 0) {
                creditCardFees = cart.getCreditCardFee().doubleValue();
            }

            double deliveryFees = 0;
            if (cart.getDeliveryFee().doubleValue() > 0) {
                deliveryFees = cart.getDeliveryFee().doubleValue();
            }

            StringBuilder promotions = PreCalculationUtil.getPromotions(cart, promotionMap, rewardMap);

            double transCogs = 0;
            for (OrderItem item : cart.getItems()) {
                if ((Transaction.TransactionType.Refund == trans.getTransType() && Cart.RefundOption.Void == cart.getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }

                double itemCogs = PreCalculationUtil.calculateItemCogs(item, cart, product, prepackageProductItemMap, productBatchMap, prepackageMap, weightToleranceHashMap, recentBatchMap, productsByCompanyLinkId);
                transCogs += itemCogs;
            }

            double total = cart.getTotal().doubleValue() * factor;
            if (trans.getTransType() == Transaction.TransactionType.Refund
                    && cart.getRefundOption() == Cart.RefundOption.Void
                    && cart.getSubTotal().doubleValue() == 0) {
                creditCardFees = 0;
                deliveryFees = 0;
                afterTaxDiscount = 0;
                total = 0;
            }

            TotalSalesCache cache = new TotalSalesCache();
            cache.prepare();
            cache.setShopId(shop.getId());
            cache.setCompanyId(company.getId());
            cache.setTransactionId(trans.getId());
            cache.setProcessedTime(trans.getProcessedTime());
            cache.setMemberId(trans.getMemberId());
            cache.setTransNo(trans.getTransNo());
            cache.setTransType(trans.getTransType());
            cache.setTransactionStatus(trans.getStatus());
            cache.setConsumerTaxType(consumerTaxType);

            cache.setCogs(transCogs);
            cache.setRetailValue(cart.getSubTotal().doubleValue() * factor);
            cache.setDiscounts(cart.getTotalDiscount().doubleValue() * factor);

            cache.setPreAlExciseTax(preALExciseTax * factor);
            cache.setPreNalExciseTax(preNALExciseTax * factor);
            cache.setPreCityTax(preCityTax * factor);
            cache.setPreCountyTax(preCountyTax * factor);
            cache.setPreStateTax(preStateTax * factor);

            cache.setPostAlExciseTax(postALExciseTax * factor);
            cache.setPostNalExciseTax(postNALExciseTax * factor);

            cache.setCityTax(cityTax * factor);
            cache.setCountyTax(countyTax * factor);
            cache.setStateTax(stateTax * factor);
            cache.setTotalTax(totalPostTax * factor);

            cache.setDeliveryFee(deliveryFees * factor);
            cache.setCreditCardFee(creditCardFees * factor);
            cache.setAfterTaxDiscount(afterTaxDiscount * factor);
            cache.setGrossReceipt(total);

            cache.setEmployeeId(trans.getSellerId());
            cache.setSellerTerminalId(trans.getSellerTerminalId());
            cache.setPaymentOption(cart.getPaymentOption());
            cache.setPromotions(promotions.toString());
            cache.setTraceSubmitStatus(trans.getTraceSubmitStatus());
            cache.setMetrcId(trans.getMetrcId() == null ? 0 : trans.getMetrcId());

            totalSalesCaches.add(cache);
        }

        return totalSalesCaches;
    }

}
