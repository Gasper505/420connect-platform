package com.fourtwenty.reporting.jobs.services.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.reporting.gather.impl.transaction.TotalSalesProductsGatherer;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import com.fourtwenty.reporting.jobs.models.TotalSalesProduct;
import com.fourtwenty.reporting.jobs.services.PreCalculateReportingDataService;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class TotalSalesProductCalculations implements PreCalculateReportingDataService<TotalSalesProduct, TotalSalesDetailCache> {

    @Inject
    private TotalSalesProductsGatherer totalSalesProductsGatherer;

    public TotalSalesProductCalculations() {
    }

    public List<TotalSalesDetailCache> preCalculateData(TotalSalesProduct totalSalesProduct) {

        Company company = totalSalesProduct.getCompany();
        Shop shop = totalSalesProduct.getShop();

        HashMap<String, Member> memberMap = totalSalesProduct.getMemberMap();
        HashMap<String, Product> dbProductMap = totalSalesProduct.getProductMap();
        HashMap<String, Promotion> promotionMap = totalSalesProduct.getPromotionMap();
        HashMap<String, Prepackage> prepackageMap = totalSalesProduct.getPrepackageMap();
        HashMap<String, ProductBatch> productBatchMap = totalSalesProduct.getProductBatchMap();
        HashMap<String, LoyaltyReward> loyaltyRewardMap = totalSalesProduct.getLoyaltyRewardMap();
        HashMap<String, ProductCategory> productCategoryMap = totalSalesProduct.getProductCategoryMap();
        HashMap<String, ProductWeightTolerance> weightToleranceMap = totalSalesProduct.getWeightToleranceMap();
        HashMap<String, PrepackageProductItem> prepackageProductItemMap = totalSalesProduct.getPrepackageProductItemMap();

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : totalSalesProduct.getProductBatchMap().values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product product : dbProductMap.values()) {
            productMap.put(product.getId(), product);
            List<Product> items = productsByCompanyLinkId.get(product.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(product);
            productsByCompanyLinkId.put(product.getCompanyLinkId(), items);
        }

        List<TotalSalesDetailCache> salesDetailList = new ArrayList<>();

        int factor = 1;
        for (Transaction transaction : totalSalesProduct.getTransactions()) {
            Member member = memberMap.get(transaction.getMemberId());

            if (member == null) {
                continue;
            }

            Cart cart = transaction.getCart();
            if (cart == null) {
                continue;
            }

            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && cart != null && cart.getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            String consumerTaxType = "N/A";
            if (cart.getTaxTable() != null) {
                consumerTaxType = cart.getTaxTable().getName();
            }

            LinkedHashSet<PromotionReq> promos = cart.getPromotionReqs();
            StringBuilder sb = new StringBuilder();
            Iterator<PromotionReq> it = promos.iterator();

            while (it.hasNext()) {
                PromotionReq promotionReq = it.next();
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                    if (promotion != null) {
                        sb.append(promotion.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                } else {
                    LoyaltyReward reward = loyaltyRewardMap.get(promotionReq.getRewardId());
                    if (reward != null) {
                        sb.append(reward.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                }
            }

            Long processedTime = transaction.getProcessedTime();
            Cart.PaymentOption paymentOption = cart.getPaymentOption();
            String transNo = transaction.getTransNo();
            HashMap<String, Double> propRatioMap = new HashMap<>();

            if (cart.getItems().size() > 0) {
                double total = 0.0;

                for (OrderItem orderItem : cart.getItems()) {
                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {
                        if (product.isDiscountable()) {
                            total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                        }
                    }
                }

                for (OrderItem orderItem : cart.getItems()) {
                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == cart.getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {

                        propRatioMap.put(orderItem.getId(), 1d); // 100 %
                        if (product.isDiscountable() && total > 0) {
                            double finalCost = orderItem.getFinalPrice().doubleValue();
                            double ratio = finalCost / total;

                            propRatioMap.put(orderItem.getId(), ratio);
                        }
                    }
                }
            }

            for (OrderItem item : cart.getItems()) {

                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }

                boolean cannabis = false;
                double cityTax = 0;
                double countyTax = 0;
                double stateTax = 0;
                double exciseTax = 0;
                double federalTax = 0;


                double preCityTax = 0;
                double preCountyTax = 0;
                double preStateTax = 0;
                double preFederalTax = 0;


                Double ratio = propRatioMap.get(item.getId());
                if (ratio == null) {
                    ratio = 1d;
                }


                Double propCartDiscount = cart.getCalcCartDiscount() != null ? cart.getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propDeliveryFee = cart.getDeliveryFee() != null ? cart.getDeliveryFee().doubleValue() * ratio : 0;
                Double propCCFee = cart.getCreditCardFee() != null ? cart.getCreditCardFee().doubleValue() * ratio : 0;
                Double propAfterTaxDiscount = cart.getAppliedAfterTaxDiscount() != null ? cart.getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;

                double preALExciseTax = 0;
                double preNALExciseTax = 0;
                double postALExciseTax = 0;
                double postNALExciseTax = 0;

                double totalTax = 0;
                double subTotalAfterdiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                if (item.getTaxResult() != null) {
                    preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                    preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                    postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                    postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        cityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                        stateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                        countyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                        federalTax = item.getTaxResult().getTotalFedTax().doubleValue();
                        exciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                        totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                    }

                    preCityTax = cart.getTaxResult().getTotalCityPreTax().doubleValue();
                    preCountyTax = cart.getTaxResult().getTotalCountyPreTax().doubleValue();
                    preStateTax = cart.getTaxResult().getTotalStatePreTax().doubleValue();
                    preFederalTax = cart.getTaxResult().getTotalFedPreTax().doubleValue();

                    if (totalTax == 0) {
                        if ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null) {
                            TaxInfo taxInfo = item.getTaxInfo();
                            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                                cityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                                stateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                                federalTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();
                                totalTax = cityTax + stateTax + federalTax;
                            } else {
                                preCityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                                preStateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                                preFederalTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();
                            }
                        }
                    }

                    if (totalTax == 0) {
                        totalTax = item.getCalcTax().doubleValue();
                    }

                    totalTax += postALExciseTax + postNALExciseTax;

                    ProductCategory category = productCategoryMap.get(product.getCategoryId());
                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        cannabis = true;
                    }

                    double finalAfterTax = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4)
                            + NumberUtils.round(propDeliveryFee, 4) + NumberUtils.round(totalTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);

                    StringBuilder batchLogs = new StringBuilder();
                    Double cogs = 0d;
                    boolean calculated = false;
                    String prepackageId = null;
                    PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(item.getPrepackageItemId());
                    if (prepackageProductItem != null) {
                        ProductBatch targetBatch = productBatchMap.get(prepackageProductItem.getBatchId());

                        Prepackage prepackage = prepackageMap.get(prepackageProductItem.getPrepackageId());
                        if (prepackage != null && targetBatch != null) {
                            prepackageId = prepackage.getId();
                            calculated = true;
                            BigDecimal unitValue = prepackage.getUnitValue();
                            if (unitValue == null || unitValue.doubleValue() == 0) {
                                ProductWeightTolerance weightTolerance = weightToleranceMap.get(prepackage.getToleranceId());
                                unitValue = weightTolerance.getUnitValue();
                            }
                            // calculate the total quantity based on the prepackage value
                            double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                            cogs += totalSalesProductsGatherer.calcCOGS(unitsSold, targetBatch, factor);

                            batchLogs.append(targetBatch.getSku());
                        }
                    } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : item.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = productBatchMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    cogs += totalSalesProductsGatherer.calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch, factor);
                                }
                            }
                            ProductBatch batch = productBatchMap.get(quantityLog.getBatchId());
                            if (batch != null) {
                                batch = totalSalesProductsGatherer.getRecentBatch(product, recentBatchMap, productsByCompanyLinkId);
                            }
                            if (batch != null) {
                                if (StringUtils.isNotBlank(batchLogs)) {
                                    batchLogs.append(",");
                                }
                                batchLogs.append(batch.getSku());
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = totalSalesProductsGatherer.getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                        cogs = unitCost * item.getQuantity().doubleValue() * factor;
                    }

                    double itemCogs = cogs;
                    double itemCost = item.getCost().doubleValue() * factor;
                    double itemDiscount = item.getCalcDiscount().doubleValue() * factor;
                    double itemfinalPrice = item.getFinalPrice().doubleValue() * factor;
                    double itemCartDiscount = propCartDiscount * factor;
                    double itemAfterTaxDiscount = propAfterTaxDiscount * factor;
                    double itemPreALExciseTax = preALExciseTax * factor;
                    double itemPreNalExciseTax = preNALExciseTax * factor;

                    double itemPreCityTax = preCityTax * factor;
                    double itemPreCountyTax = preCountyTax * factor;
                    double itemPreStateTax = preStateTax * factor;
                    double itemPreFederalTax = preFederalTax * factor;

                    double itemPOSTALExciseTax = postALExciseTax * factor;
                    double itemPOSTNALExciseTax = postNALExciseTax * factor;
                    double itemCityTax = cityTax * factor;
                    double itemCountyTax = countyTax * factor;
                    double itemStateTax = stateTax * factor;
                    double itemFederalTAx = federalTax * factor;
                    double itemTotalTAx = totalTax * factor;
                    double itemDeliveryFee = propDeliveryFee * factor;
                    double itemPropCCFee = propCCFee * factor;
                    double itemGrossReceipt = finalAfterTax * factor;

                    if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {

                        itemCogs = 0d;
                        itemCost = 0d;
                        itemDiscount = 0d;
                        itemfinalPrice = 0d;
                        itemCartDiscount = 0d;
                        itemAfterTaxDiscount = 0d;
                        itemPreALExciseTax = 0d;
                        itemPreNalExciseTax = 0d;
                        itemPOSTALExciseTax = 0d;
                        itemPOSTNALExciseTax = 0d;
                        itemCityTax = 0d;
                        itemCountyTax = 0d;
                        itemStateTax = 0d;
                        itemFederalTAx = 0d;
                        itemTotalTAx = 0d;
                        itemDeliveryFee = 0d;
                        itemPropCCFee = 0d;
                        itemGrossReceipt = 0d;
                    }

                    TotalSalesDetailCache totalSalesDetailCache = new TotalSalesDetailCache();
                    totalSalesDetailCache.prepare();
                    totalSalesDetailCache.setCompanyId(company.getId());
                    totalSalesDetailCache.setShopId(shop.getId());

                    totalSalesDetailCache.setTransactionId(transaction.getId());
                    totalSalesDetailCache.setOrderItemId(item.getId());

                    totalSalesDetailCache.setTransCreated(transaction.getCreated());
                    totalSalesDetailCache.setProcessedTime(processedTime);
                    totalSalesDetailCache.setTransNo(transNo);
                    totalSalesDetailCache.setTransType(transaction.getTransType());
                    totalSalesDetailCache.setTransactionStatus(transaction.getStatus());

                    totalSalesDetailCache.setProductId(item.getProductId());
                    totalSalesDetailCache.setPrepackageId(prepackageId);
                    totalSalesDetailCache.setMemberId(transaction.getMemberId());

                    totalSalesDetailCache.setConsumerTaxType(consumerTaxType);
                    totalSalesDetailCache.setQuantity(item.getQuantity().doubleValue());

                    totalSalesDetailCache.setBatch(batchLogs.toString());
                    totalSalesDetailCache.setCogs(itemCogs);
                    totalSalesDetailCache.setRetailValue(itemCost);
                    totalSalesDetailCache.setProductDiscount(itemDiscount);
                    totalSalesDetailCache.setSubTotal(itemfinalPrice);
                    totalSalesDetailCache.setCartDiscount(itemCartDiscount);
                    totalSalesDetailCache.setPreAlExciseTax(itemPreALExciseTax);
                    totalSalesDetailCache.setPreNalExciseTax(itemPreNalExciseTax);

                    totalSalesDetailCache.setPreCityTax(itemPreCityTax);
                    totalSalesDetailCache.setPreCountyTax(itemPreCountyTax);
                    totalSalesDetailCache.setPreStateTax(itemPreStateTax);
                    totalSalesDetailCache.setPreFederalTax(itemPreFederalTax);

                    totalSalesDetailCache.setPostAlExciseTax(itemPOSTALExciseTax);
                    totalSalesDetailCache.setPostNalExciseTax(itemPOSTNALExciseTax);

                    totalSalesDetailCache.setCityTax(itemCityTax);
                    totalSalesDetailCache.setCountyTax(itemCountyTax);
                    totalSalesDetailCache.setStateTax(itemStateTax);
                    totalSalesDetailCache.setFederalTax(itemFederalTAx);
                    totalSalesDetailCache.setTotalTax(itemTotalTAx);
                    totalSalesDetailCache.setAfterTaxDiscount(itemAfterTaxDiscount);
                    totalSalesDetailCache.setDeliveryFee(itemDeliveryFee);
                    totalSalesDetailCache.setCreditCardFee(itemPropCCFee);
                    totalSalesDetailCache.setGrossReceipt(itemGrossReceipt);

                    totalSalesDetailCache.setPaymentOption(paymentOption);
                    totalSalesDetailCache.setPromotions(sb.toString());

                    totalSalesDetailCache.setTerminalId(transaction.getSellerTerminalId());
                    totalSalesDetailCache.setEmployeeId(transaction.getSellerId());

                    totalSalesDetailCache.setPrepackageItemId(item.getPrepackageItemId());

                    totalSalesDetailCache.setFactor(factor);

                    totalSalesDetailCache.setCartRefundOption(cart.getRefundOption());
                    totalSalesDetailCache.setItemStatus(item.getStatus());
                    totalSalesDetailCache.setCalcPreTax(item.getCalcPreTax().doubleValue() * factor);

                    salesDetailList.add(totalSalesDetailCache);
                }
            }
        }

        return salesDetailList;
    }


}
