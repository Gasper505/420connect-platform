package com.fourtwenty.reporting.lifecycle;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.domain.repositories.global.ReportingInfoRepository;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.reporting.domain.db.repositories.ReportingAppRepository;
import com.fourtwenty.reporting.lifecycle.model.ReportTableInfo;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportingAppStartup implements AppStartup {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportingAppStartup.class);

    @Inject
    private Environment environment;
    @Inject
    private ReportingAppRepository appRepository;
    @Inject
    private ReportingInfoRepository reportingInfoRepository;
    @Inject
    private ConnectConfiguration connectConfiguration;

    @Override
    public void run() {
        LOGGER.info("Starting report module start script");

        final ObjectMapper objectMapper = environment.getObjectMapper();

        if (connectConfiguration.isEnableReportingCache()) {
            this.createReportingInfo(objectMapper);
            this.createCacheTables(objectMapper);
        }
    }

    /**
     * Create reporting info data
     */
    private void createReportingInfo(ObjectMapper objectMapper) {
        InputStream stream = ReportingAppStartup.class.getResourceAsStream("/ReportingInfo.json");

        try {

            List<ReportingInfo> infoList = objectMapper.readValue(stream, new TypeReference<List<ReportingInfo>>() {
            });

            List<ReportingInfo> dbInfoList = reportingInfoRepository.list();


            for (ReportingInfo info : infoList) {

                boolean found = Boolean.FALSE;
                for (ReportingInfo reportingInfo : dbInfoList) {

                    if (info.getReportingInfoType().equals(reportingInfo.getReportingInfoType())) {
                        found = Boolean.TRUE;

                        reportingInfo.prepare();
                        reportingInfo.setSubReportTypes(info.getSubReportTypes());
                        reportingInfoRepository.update(reportingInfo.getId(), reportingInfo);
                        break;
                    }
                }
                if (!found) {
                    info.prepare();
                    reportingInfoRepository.save(info);
                }
            }

        } catch (Exception e) {
            throw new BlazeOperationException("Error while creating reporting info types");
        }

    }

    /**
     * Create tables for postgres db
     */
    private void createCacheTables(ObjectMapper objectMapper) {


        InputStream stream = ReportingAppStartup.class.getResourceAsStream("/ReportingData.json");

        try {
            ReportTableInfo info = objectMapper.readValue(stream, ReportTableInfo.class);

            //create table if does not exist
            if (info != null) {

                ConnectConfiguration.EnvironmentType env = connectConfiguration.getEnv();

                HashMap<String, String> createTableQuery = info.getCreateTableQuery();

                for (Map.Entry<String, String> entry : createTableQuery.entrySet()) {

                    String tableName = env.toString().toLowerCase() + "_" + entry.getKey();

                    Boolean isExist = this.checkTableExistenceByName(tableName);

                    if (!isExist) {
                        Boolean tableCreated = this.createTable("create table " + tableName + " " + entry.getValue());

                        if (!tableCreated) {
                            LOGGER.error("Error while creating table with name : " + entry.getKey());
                            throw new BlazeOperationException("Error while creating table with name : " + entry.getKey());
                        }
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error while running creating postgres table in reporting start up : " + e);
            throw new BlazeOperationException("Error while running creating postgres table in reporting start up : " + e);
        }

    }

    /**
     * This method creates table with provided query
     *
     * @param createQuery : create table query
     */
    private Boolean createTable(String createQuery) {
        return appRepository.createTable(createQuery);
    }

    /**
     * This method checks weather table exist with provided name or not
     *
     * @param tableName : table name
     */
    private Boolean checkTableExistenceByName(String tableName) {
        return appRepository.isTableExist(tableName);
    }
}
