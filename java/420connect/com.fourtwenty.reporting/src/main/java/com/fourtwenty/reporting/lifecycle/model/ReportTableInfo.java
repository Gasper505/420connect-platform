package com.fourtwenty.reporting.lifecycle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

public class ReportTableInfo {

    @JsonProperty("createTable")
    private HashMap<String, String> createTableQuery;

    public HashMap<String, String> getCreateTableQuery() {
        return createTableQuery;
    }

    public void setCreateTableQuery(HashMap<String, String> createTableQuery) {
        this.createTableQuery = createTableQuery;
    }
}
