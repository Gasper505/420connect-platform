package com.fourtwenty.reporting.services;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.reporting.domain.db.config.DatabaseConfig;
import com.fourtwenty.reporting.domain.db.repositories.ReportingAppRepository;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesCacheRepository;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesDetailRepository;
import com.fourtwenty.reporting.domain.db.repositories.impl.ReportingAppRepositoryImpl;
import com.fourtwenty.reporting.domain.db.repositories.impl.TotalSalesCacheRepositoryImpl;
import com.fourtwenty.reporting.domain.db.repositories.impl.TotalSalesDetailRepositoryImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportCoreModule extends AbstractModule {

    private static final Log LOG = LogFactory.getLog(ReportCoreModule.class);

    private DatabaseConfig databaseConfig;

    @Override
    protected void configure() {

        //Repositories
        bind(ReportingAppRepository.class).to(ReportingAppRepositoryImpl.class);
        bind(TotalSalesDetailRepository.class).to(TotalSalesDetailRepositoryImpl.class);
        bind(TotalSalesCacheRepository.class).to(TotalSalesCacheRepositoryImpl.class);

        //Services

    }

    @Provides
    private synchronized DatabaseConfig getDatasource(ConnectConfiguration config) {
        if (databaseConfig == null) {
            databaseConfig = new DatabaseConfig(config.getPostgreServerConfiguration(), config.isEnableReportingCache());
        }

        return databaseConfig;
    }
}
