package com.fourtwenty.reporting.subscriber;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.global.ReportingInfoRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.report.PreCalculateReportEvent;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesCacheRepository;
import com.fourtwenty.reporting.domain.db.repositories.TotalSalesDetailRepository;
import com.fourtwenty.reporting.domain.models.TotalSalesCache;
import com.fourtwenty.reporting.domain.models.TotalSalesDetailCache;
import com.fourtwenty.reporting.jobs.models.TotalSales;
import com.fourtwenty.reporting.jobs.models.TotalSalesProduct;
import com.fourtwenty.reporting.jobs.services.impl.TotalSalesCalculations;
import com.fourtwenty.reporting.jobs.services.impl.TotalSalesProductCalculations;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PreCalculateReportSubscriber implements BlazeSubscriber {

    private static final Log LOG = LogFactory.getLog(PreCalculateReportSubscriber.class);

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ReportingInfoRepository reportingInfoRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private TotalSalesCalculations totalSalesCalculations;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;

    @Inject
    private TotalSalesDetailRepository totalSalesDetailRepository;
    @Inject
    private TotalSalesCacheRepository totalSalesCacheRepository;

    @Inject
    private TotalSalesProductCalculations totalSalesProductCalculations;

    private int fetchRecordLimit = 999;

    private Company company;
    private Shop shop;
    private HashMap<String, Member> memberMap;
    private HashMap<String, Product> productMap;
    private HashMap<String, Promotion> promotionMap;
    private HashMap<String, Prepackage> prepackageMap;
    private HashMap<String, ProductBatch> productBatchMap;
    private HashMap<String, LoyaltyReward> loyaltyRewardMap;
    private HashMap<String, ProductCategory> productCategoryMap;
    private HashMap<String, ProductWeightTolerance> weightToleranceMap;
    private HashMap<String, PrepackageProductItem> prepackageProductItemMap;

    @Subscribe
    public void preCalculateReport(PreCalculateReportEvent event) {

        long currentTime = event.getTriggerTime();

        List<Company> companies = companyRepository.list();
        if (companies == null) {
            return;
        }

        List<Shop> shops = shopRepository.list();
        if (shops == null) {
            return;
        }

        HashMap<String, List<Shop>> shopByCompanyMap = new HashMap<>();
        for (Shop shop : shops) {
            shopByCompanyMap.putIfAbsent(shop.getCompanyId(), new ArrayList<>());
            shopByCompanyMap.get(shop.getCompanyId()).add(shop);
        }


        for (Company company : companies) {
            List<Shop> shopsList = shopByCompanyMap.get(company.getId());

            if (!company.isActive() || shopsList == null || shopsList.size() == 0) {
                continue;
            }
            String companyId = company.getId();
            this.company = company;

            this.memberMap = memberRepository.listAsMap(companyId);
            this.productCategoryMap = productCategoryRepository.listAsMap(companyId);
            this.weightToleranceMap = weightToleranceRepository.listAsMap(companyId);

            for (Shop shop : shopsList) {

                if (!shop.isActive()) {
                    continue;
                }

                String shopId = shop.getId();

                this.shop = shop;
                this.promotionMap = promotionRepository.listAsMap(companyId, shopId);
                this.loyaltyRewardMap = rewardRepository.listAllAsMap(companyId, shopId);
                this.productMap = productRepository.listAsMap(companyId, shopId);
                this.productBatchMap = productBatchRepository.listAsMap(companyId, shopId);
                this.prepackageMap = prepackageRepository.listAsMap(companyId, shopId);
                this.prepackageProductItemMap = prepackageProductItemRepository.listAsMap(companyId, shopId);


                for (ReportingInfo reportingInfo : reportingInfoRepository.list()) {
                    if (!reportingInfo.getActive()) {
                        continue;
                    }

                    Boolean found = Boolean.FALSE;
                    switch (reportingInfo.getReportingInfoType()) {
                        case TOTAL_SALES_PRODUCTS:
                            calculateTotalSalesProduct(reportingInfo.getLastSync(), currentTime);
                            found = Boolean.TRUE;
                            break;
                        case TOTAL_SALES:
                            calculateTotalSales(reportingInfo.getLastSync(), currentTime);
                            found = Boolean.TRUE;
                            break;
                    }

                    if (found) this.updateReportingInfo(reportingInfo, currentTime);
                }

            }
        }

    }

    private void calculateTotalSales(long lastSync, long currentTime) {
        List<Transaction> transactions;
        List<TotalSalesCache> salesDetailList = new ArrayList<>();
        int startingIndex = 0;
        do {

            Iterable<Transaction> bracketSales = transactionRepository.getBracketSales(company.getId(), shop.getId(), lastSync, currentTime, startingIndex, fetchRecordLimit);
            transactions = Lists.newArrayList(bracketSales);

            if (transactions.size() == 0) {
                break;
            }

            TotalSales sales = new TotalSales();
            sales.setShop(shop);
            sales.setCompany(company);
            sales.setMemberMap(memberMap);
            sales.setProductMap(productMap);
            sales.setPromotionMap(promotionMap);
            sales.setRewardMap(loyaltyRewardMap);
            sales.setPrepackageMap(prepackageMap);
            sales.setTransactionList(transactions);
            sales.setProductBatchMap(productBatchMap);
            sales.setWeightToleranceHashMap(weightToleranceMap);
            sales.setPrepackageProductItemMap(prepackageProductItemMap);

            List<TotalSalesCache> totalSalesCaches = totalSalesCalculations.preCalculateData(sales);
            salesDetailList.addAll(totalSalesCaches);
            startingIndex += (fetchRecordLimit + 1);
        } while (transactions.size() == 999);

        if (salesDetailList.size() > 0) {
            totalSalesCacheRepository.bulkInsert(salesDetailList);
        }
    }

    private void calculateTotalSalesProduct(long lastSync, long currentTime) {
        List<Transaction> transactions;
        int startingIndex = 0;
        List<TotalSalesDetailCache> salesDetailList = new ArrayList<>();
        do {
            Iterable<Transaction> bracketSales = transactionRepository.getBracketSales(company.getId(), shop.getId(), lastSync, currentTime, startingIndex, fetchRecordLimit);
            transactions = Lists.newArrayList(bracketSales);

            if (transactions.size() == 0) {
                break;
            }

            TotalSalesProduct totalSalesProduct = new TotalSalesProduct();
            totalSalesProduct.setCompany(company);
            totalSalesProduct.setShop(shop);
            totalSalesProduct.setTransactions(transactions);
            totalSalesProduct.setMemberMap(memberMap);
            totalSalesProduct.setProductCategoryMap(productCategoryMap);
            totalSalesProduct.setWeightToleranceMap(weightToleranceMap);
            totalSalesProduct.setPromotionMap(promotionMap);
            totalSalesProduct.setLoyaltyRewardMap(loyaltyRewardMap);
            totalSalesProduct.setProductMap(productMap);
            totalSalesProduct.setProductBatchMap(productBatchMap);
            totalSalesProduct.setPrepackageMap(prepackageMap);
            totalSalesProduct.setPrepackageProductItemMap(prepackageProductItemMap);

            List<TotalSalesDetailCache> totalSalesDetailCaches = totalSalesProductCalculations.preCalculateData(totalSalesProduct);
            salesDetailList.addAll(totalSalesDetailCaches);

            startingIndex += (fetchRecordLimit + 1);
        } while (transactions.size() == 999);
        if (salesDetailList.size() > 0) {
            totalSalesDetailRepository.bulkInsert(salesDetailList);
        }
    }

    private void updateReportingInfo(ReportingInfo reportingInfo, long currentTime) {
        reportingInfo.setLastSync(currentTime);
        reportingInfoRepository.update(reportingInfo.getId(), reportingInfo);
    }

}
