package com.fourtwenty.reporting.subscriber;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.report.EmailReportEvent;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.reporting.gatherer.impl.SalesByProductCategoryReport;
import com.fourtwenty.reporting.gatherer.impl.SalesByProductReport;
import com.fourtwenty.reporting.gatherer.impl.TotalSalesDetailReport;
import com.fourtwenty.reporting.gatherer.impl.TotalSalesReport;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;

public class ReportingInfoSubscriber implements BlazeSubscriber {

    private static final Log LOG = LogFactory.getLog(ReportingInfoSubscriber.class);

    @Inject
    private TotalSalesReport totalSalesReport;
    @Inject
    private SalesByProductReport salesByProductReport;
    @Inject
    private ConnectConfiguration connectConfiguration;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private TotalSalesDetailReport totalSalesDetailReport;
    @Inject
    private SalesByProductCategoryReport salesByProductCategoryReport;

    @Subscribe
    public void sendReport(EmailReportEvent event) {

        StringBuilder data = null;
        String reportName = null;
        ReportType type = event.getReportingInfoType();
        switch (type) {
            case TOTAL_SALES_PRODUCTS:
                data = totalSalesDetailReport.cacheGatherer(event);
                reportName = "Total Sales Detail Report.csv";
                break;
            case TOTAL_SALES:
                data = totalSalesReport.cacheGatherer(event);
                reportName = "Total Sales Report.csv";
                break;
            case SALES_BY_PRODUCT:
                data = salesByProductReport.cacheGatherer(event);
                reportName = "Sales By Product Report.csv";
                break;
            case SALES_BY_PRODUCT_CATEGORY:
                data = salesByProductCategoryReport.cacheGatherer(event);
                reportName = "Sales By Product Category.csv";
                break;
        }

        if (data != null && StringUtils.isNotBlank(reportName)) {
            this.sendEmail(event.getEmail(), data, reportName);
        }

    }

    private void sendEmail(String email, StringBuilder data, String fileName) {

        byte[] bytes = data.toString().getBytes();
        InputStream stream = new ByteArrayInputStream(bytes);

        HashMap<String, InputStream> attachmentData = new HashMap<>();
        attachmentData.put(fileName, stream);

        HashMap<String, HashMap<String, InputStream>> attachmentMap = new HashMap<>();
        attachmentMap.put("attachment", attachmentData);

        String body = "Hello";

        amazonServiceManager.sendMultiPartEmail("support@blaze.me", email, connectConfiguration.getAppName(), body, null, null, attachmentMap, "Content-Disposition", "", "");

    }

}
