package com.fourtwenty.core;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.caching.CacheService;
import com.fourtwenty.core.caching.guice.CacheInterceptor;
import com.fourtwenty.core.caching.service.CacheServiceImpl;
import com.fourtwenty.core.domain.repositories.common.BackgroundTaskRepository;
import com.fourtwenty.core.domain.repositories.common.impl.BackgroundTaskRepositoryImpl;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncRepository;
import com.fourtwenty.core.domain.repositories.onprem.impl.OnPremSyncDetailsRepositoryImpl;
import com.fourtwenty.core.domain.repositories.onprem.impl.OnPremSyncRepositoryImpl;
import com.fourtwenty.core.domain.repositories.payment.CloverReceiptRepository;
import com.fourtwenty.core.domain.repositories.payment.impl.CloverReceiptRepositoryImpl;
import com.fourtwenty.core.domain.repositories.reportrequest.ReportRequestRepository;
import com.fourtwenty.core.domain.repositories.reportrequest.impl.ReportRequestRepositoryImpl;
import com.fourtwenty.core.security.tokens.OnPremToken;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.global.impl.CompanyUniqueSequenceServiceImpl;
import com.fourtwenty.core.services.notification.FcmService;
import com.fourtwenty.core.services.notification.impl.FcmServiceImpl;
import com.fourtwenty.core.services.onprem.OnPremSyncService;
import com.fourtwenty.core.services.onprem.OnPremCloudSyncService;
import com.fourtwenty.core.services.onprem.impl.OnPremSyncServiceImpl;
import com.fourtwenty.core.services.onprem.impl.OnPremCloudSyncServiceImpl;
import com.fourtwenty.core.services.orders.CommonPOSService;
import com.fourtwenty.core.services.orders.impl.CommonPOSServiceImpl;
import com.fourtwenty.core.services.partners.*;
import com.fourtwenty.core.services.partners.impl.*;
import com.fourtwenty.core.services.reportrequest.ReportRequestService;
import com.fourtwenty.core.services.reportrequest.impl.ReportRequestServiceImpl;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.services.taxes.impl.TaxCalulationServiceImpl;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;
import com.fourtwenty.core.domain.repositories.checkin.CheckInRepository;
import com.fourtwenty.core.domain.repositories.checkin.impl.CheckInRepositoryImpl;
import com.fourtwenty.core.domain.repositories.comments.UserActivityRepository;
import com.fourtwenty.core.domain.repositories.comments.impl.UserActivityRepositoryImpl;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.common.impl.IntegrationSettingRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.*;
import com.fourtwenty.core.domain.repositories.compliance.impl.*;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.domain.repositories.developer.PartnerKeyRepository;
import com.fourtwenty.core.domain.repositories.developer.impl.DeveloperKeyRepositoryImpl;
import com.fourtwenty.core.domain.repositories.developer.impl.PartnerKeyRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.dispensary.impl.*;
import com.fourtwenty.core.domain.repositories.global.ReportingInfoRepository;
import com.fourtwenty.core.domain.repositories.global.StateCannabisLimitRepository;
import com.fourtwenty.core.domain.repositories.global.impl.ReportingInfoRepositoryImpl;
import com.fourtwenty.core.domain.repositories.global.impl.StateCannabisLimitRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyUsageLogRepository;
import com.fourtwenty.core.domain.repositories.loyalty.impl.LoyaltyRewardRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.impl.LoyaltyUsageLogRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.StorewideSaleRepository;
import com.fourtwenty.core.domain.repositories.loyalty.impl.StorewideSaleRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.DiscountVersionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.impl.DiscountVersionRepositoryImpl;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.domain.repositories.notification.impl.NotificationInfoRepositoryImpl;
import com.fourtwenty.core.domain.repositories.partner.PartnerWebHookRepository;
import com.fourtwenty.core.domain.repositories.partner.WebHookLogRepository;
import com.fourtwenty.core.domain.repositories.partner.impl.PartnerWebHookRepositoryImpl;
import com.fourtwenty.core.domain.repositories.partner.impl.WebHookLogRepositoryImpl;
import com.fourtwenty.core.domain.repositories.payment.PaymentRepository;
import com.fourtwenty.core.domain.repositories.payment.ShopPaymentOptionRepository;
import com.fourtwenty.core.domain.repositories.payment.impl.PaymentRepositoryImpl;
import com.fourtwenty.core.domain.repositories.payment.impl.ShopPaymentOptionRepositoryImpl;
import com.fourtwenty.core.domain.repositories.plugins.BlazePluginProductRepository;
import com.fourtwenty.core.domain.repositories.plugins.KioskPluginRepository;
import com.fourtwenty.core.domain.repositories.plugins.MessagingPluginRepository;
import com.fourtwenty.core.domain.repositories.plugins.TVPluginRepository;
import com.fourtwenty.core.domain.repositories.plugins.impl.BlazePluginProductRepositoryImpl;
import com.fourtwenty.core.domain.repositories.plugins.impl.KioskPluginRepositoryImpl;
import com.fourtwenty.core.domain.repositories.plugins.impl.MessagingPluginRepositoryImpl;
import com.fourtwenty.core.domain.repositories.plugins.impl.TVPluginRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerPasswordResetRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.domain.repositories.store.StoreWidgetKeyRepository;
import com.fourtwenty.core.domain.repositories.store.impl.ConsumerCartRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.impl.ConsumerPasswordResetRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.impl.ConsumerUserRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.impl.StoreWidgetKeyRepositoryImpl;
import com.fourtwenty.core.domain.repositories.sync.ComplianceSyncJobRepository;
import com.fourtwenty.core.domain.repositories.sync.SyncRequestRepository;
import com.fourtwenty.core.domain.repositories.sync.impl.ComplianceSyncJobRepositoryImpl;
import com.fourtwenty.core.domain.repositories.sync.impl.SyncRequestRepositoryImpl;
import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.domain.repositories.testsample.impl.TestSampleRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.*;
import com.fourtwenty.core.domain.repositories.thirdparty.impl.*;
import com.fourtwenty.core.domain.repositories.tookan.TookanAccountRepository;
import com.fourtwenty.core.domain.repositories.tookan.TookanErrorLogRepository;
import com.fourtwenty.core.domain.repositories.tookan.TookanTeamRepository;
import com.fourtwenty.core.domain.repositories.tookan.impl.TookanAccountRepositoryImpl;
import com.fourtwenty.core.domain.repositories.tookan.impl.TookanErrorLogRepositoryImpl;
import com.fourtwenty.core.domain.repositories.tookan.impl.TookanTeamRepositoryImpl;
import com.fourtwenty.core.engine.PromoRuleValidation;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.rules.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.BlazeEventBusImpl;
import com.fourtwenty.core.event.DeadEventListener;
import com.fourtwenty.core.jobs.TransactionJob;
import com.fourtwenty.core.lifecycle.DefaultTransactionDidCompleteSaleReceiver;
import com.fourtwenty.core.lifecycle.DefaultTransactionDidRefundReceiver;
import com.fourtwenty.core.lifecycle.TransactionDidCompleteSaleReceiver;
import com.fourtwenty.core.lifecycle.TransactionDidRefundReceiver;
import com.fourtwenty.core.lifecycle.metrc.MetrcTransactionDidCompleteSaleReceiverImpl;
import com.fourtwenty.core.lifecycle.metrc.MetrcTransactionDidRefundReceiverImpl;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.fourtwenty.core.lifecycle.model.IndexesData;
import com.fourtwenty.core.lifecycle.refunds.RewardsTransactionRefundReceiver;
import com.fourtwenty.core.managed.MongoManager;
import com.fourtwenty.core.security.developer.DeveloperSecured;
import com.fourtwenty.core.security.developer.DeveloperSecurityInterceptor;
import com.fourtwenty.core.security.developer.DeveloperSecurityProvider;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.dispensary.SecuredInterceptor;
import com.fourtwenty.core.security.dispensary.SecurityProvider;
import com.fourtwenty.core.security.internal.InternalApiSecurityInterceptor;
import com.fourtwenty.core.security.internal.InternalApiSecurityProvider;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.onprem.OnPremInterceptor;
import com.fourtwenty.core.security.onprem.OnPremProvider;
import com.fourtwenty.core.security.onprem.OnPremSecured;
import com.fourtwenty.core.security.partner.PartnerSecurityProvider;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.store.StoreSecurityInterceptor;
import com.fourtwenty.core.security.store.StoreSecurityProvider;
import com.fourtwenty.core.security.tokens.*;
import com.fourtwenty.core.services.checkin.CheckInService;
import com.fourtwenty.core.services.checkin.impl.CheckInServiceImpl;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.fourtwenty.core.services.comments.impl.UserActivityServiceImpl;
import com.fourtwenty.core.services.common.*;
import com.fourtwenty.core.services.common.impl.*;
import com.fourtwenty.core.services.compliance.ComplianceService;
import com.fourtwenty.core.services.compliance.impl.ComplianceServiceImpl;
import com.fourtwenty.core.services.compliance.metrc.MetrcProcessingService;
import com.fourtwenty.core.services.compliance.metrc.impl.MetrcProcessingServiceImpl;
import com.fourtwenty.core.services.credits.CreditService;
import com.fourtwenty.core.services.credits.impl.CreditServiceImpl;
import com.fourtwenty.core.services.developer.DeveloperInventoryService;
import com.fourtwenty.core.services.developer.DeveloperKeyService;
import com.fourtwenty.core.services.developer.PartnerKeyService;
import com.fourtwenty.core.services.developer.impl.DeveloperInventoryServiceImpl;
import com.fourtwenty.core.services.developer.impl.DeveloperKeyServiceImpl;
import com.fourtwenty.core.services.developer.impl.PartnerKeyServiceImpl;
import com.fourtwenty.core.services.engine.RecommendEngineService;
import com.fourtwenty.core.services.engine.impl.RecommendEngineServiceImpl;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.fourtwenty.core.services.global.ReportingInfoService;
import com.fourtwenty.core.services.global.impl.CannabisLimitServiceImpl;
import com.fourtwenty.core.services.global.impl.CashDrawerProcessorServiceImpl;
import com.fourtwenty.core.services.global.impl.ReportingInfoServiceImpl;
import com.fourtwenty.core.services.imaging.ImageProcessorService;
import com.fourtwenty.core.services.imaging.impl.ImageProcessorServiceImpl;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.inventory.CommonInventoryService;
import com.fourtwenty.core.services.inventory.InventoryOperationService;
import com.fourtwenty.core.services.inventory.ProductWeightToleranceService;
import com.fourtwenty.core.services.inventory.impl.BatchQuantityServiceImpl;
import com.fourtwenty.core.services.inventory.impl.CommonInventoryServiceImpl;
import com.fourtwenty.core.services.inventory.impl.InventoryOperationServiceImpl;
import com.fourtwenty.core.services.inventory.impl.ProductWeightToleranceServiceImpl;
import com.fourtwenty.core.services.labels.LabelService;
import com.fourtwenty.core.services.labels.impl.LabelServiceImpl;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.mgmt.impl.*;
import com.fourtwenty.core.services.notification.NotificationInfoService;
import com.fourtwenty.core.services.notification.impl.NotificationInfoServiceImpl;
import com.fourtwenty.core.services.payments.BlazePaymentService;
import com.fourtwenty.core.services.payments.StripeService;
import com.fourtwenty.core.services.payments.impl.BlazePaymentServiceImpl;
import com.fourtwenty.core.services.payments.impl.StripeServiceImpl;
import com.fourtwenty.core.services.plugins.BlazePluginProductService;
import com.fourtwenty.core.services.plugins.KioskPluginService;
import com.fourtwenty.core.services.plugins.MessagingPluginService;
import com.fourtwenty.core.services.plugins.TVPluginService;
import com.fourtwenty.core.services.plugins.impl.BlazePluginProductServiceImpl;
import com.fourtwenty.core.services.plugins.impl.KioskPluginServiceImpl;
import com.fourtwenty.core.services.plugins.impl.MessagingPluginServiceImpl;
import com.fourtwenty.core.services.plugins.impl.TVPluginServiceImpl;
import com.fourtwenty.core.services.pos.*;
import com.fourtwenty.core.services.pos.impl.*;
import com.fourtwenty.core.services.sms.IncomingSMSService;
import com.fourtwenty.core.services.sms.impl.IncomingSMSServiceImpl;
import com.fourtwenty.core.services.store.*;
import com.fourtwenty.core.services.store.impl.*;
import com.fourtwenty.core.services.test.SMSTestService;
import com.fourtwenty.core.services.test.impl.SMSTestServiceImpl;
import com.fourtwenty.core.services.thirdparty.*;
import com.fourtwenty.core.services.thirdparty.impl.*;
import com.fourtwenty.core.services.tookan.TookanService;
import com.fourtwenty.core.services.tookan.impl.TookanServiceImpl;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.fourtwenty.core.services.twilio.impl.TwilioMessageServiceImpl;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchCommunicatorService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchIndexingService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.impl.ElasticSearchCommunicatorServiceImpl;
import com.fourtwenty.core.thirdparty.elasticsearch.services.impl.ElasticSearchIndexingServiceImpl;
import com.fourtwenty.core.thirdparty.elasticsearch.services.impl.ElasticSearchServiceImpl;
import com.fourtwenty.core.thirdparty.headset.HeadsetAPIService;
import com.fourtwenty.core.thirdparty.headset.impl.HeadsetAPIServiceImpl;
import com.fourtwenty.core.thirdparty.leafly.services.LeaflyAPIService;
import com.fourtwenty.core.thirdparty.leafly.services.impl.LeaflyAPIServiceImpl;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetAPIService;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetErrorLogService;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import com.fourtwenty.core.thirdparty.onfleet.services.impl.OnFleetAPIServiceImpl;
import com.fourtwenty.core.thirdparty.onfleet.services.impl.OnFleetErrorLogServiceImpl;
import com.fourtwenty.core.thirdparty.onfleet.services.impl.OnFleetServiceImpl;
import com.fourtwenty.core.thirdparty.springbig.services.SpringBigAPIService;
import com.fourtwenty.core.thirdparty.springbig.services.impl.SpringBigAPIServiceImpl;
import com.fourtwenty.core.thirdparty.tookan.service.TookanApiService;
import com.fourtwenty.core.thirdparty.tookan.service.TookanErrorLogService;
import com.fourtwenty.core.thirdparty.tookan.service.impl.TookanApiServiceImpl;
import com.fourtwenty.core.thirdparty.tookan.service.impl.TookanErrorLogServiceImpl;
import com.fourtwenty.core.thirdparty.weedmap.services.WeedmapAPIService;
import com.fourtwenty.core.thirdparty.weedmap.services.impl.WeedmapAPIServiceImpl;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.matcher.Matchers;
import com.google.inject.multibindings.Multibinder;
import io.dropwizard.setup.Environment;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;

import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import java.io.InputStream;

import static com.google.inject.matcher.Matchers.any;
import static com.google.inject.matcher.Matchers.subclassesOf;


/**
 * Created by mdo on 8/16/15.
 */
public class CoreModule extends AbstractModule {
    static SecurityUtil securityUtil;
    static DefaultData defaultData;
    static AmazonServiceFactory s3Factory;
    static MongoManager mongoDBManager;
    static IndexesData indexesData;

    @Override
    protected void configure() {
        // Repositories
        bind(AppRepository.class).to(AppRepositoryImpl.class);
        bind(EmployeeRepository.class).to(EmployeeRepositoryImpl.class);
        bind(CompanyRepository.class).to(CompanyRepositoryImpl.class);
        bind(ShopRepository.class).to(ShopRepositoryImpl.class);
        bind(MemberRepository.class).to(MemberRepositoryImpl.class);
        bind(DoctorRepository.class).to(DoctorRepositoryImpl.class);
        bind(ProductRepository.class).to(ProductRepositoryImpl.class);
        bind(TerminalRepository.class).to(TerminalRepositoryImpl.class);
        bind(ProductCategoryRepository.class).to(ProductCategoryRepositoryImpl.class);
        bind(ProductWeightToleranceRepository.class).to(ProductWeightToleranceRepositoryImpl.class);
        bind(MemberGroupRepository.class).to(MemberGroupRepositoryImpl.class);
        bind(VendorRepository.class).to(VendorRepositoryImpl.class);
        bind(ProductBatchRepository.class).to(ProductBatchRepositoryImpl.class);
        bind(CompanyAssetRepository.class).to(CompanyAssetRepositoryImpl.class);
        bind(MedicalConditionRepository.class).to(MedicalConditionRepositoryImpl.class);
        bind(ContractRepository.class).to(ContractRepositoryImpl.class);
        bind(TransactionRepository.class).to(TransactionRepositoryImpl.class);
        bind(InventoryRepository.class).to(InventoryRepositoryImpl.class);
        bind(UniqueSequenceRepository.class).to(UniqueSequenceRepositoryImpl.class);
        bind(QueuedTransactionRepository.class).to(QueuedTransactionRepositoryImpl.class);
        bind(CompanyFeaturesRepository.class).to(CompanyFeaturesRepositoryImpl.class);
        bind(RoleRepository.class).to(RoleRepositoryImpl.class);
        bind(TimeCardRepository.class).to(TimeCardRepositoryImpl.class);
        bind(TerminalLocationRepository.class).to(TerminalLocationRepositoryImpl.class);
        bind(AuditLogRepository.class).to(AuditLogRepositoryImpl.class);
        bind(PasswordResetRepository.class).to(PasswordResetRepositoryImpl.class);
        bind(BouncedEmailRepository.class).to(BouncedEmailRepositoryImpl.class);
        bind(ConnectProductRepository.class).to(ConnectProductRepositoryImpl.class);
        bind(ProductChangeLogRepository.class).to(ProductChangeLogRepositoryImpl.class);
        bind(CashDrawerSessionRepository.class).to(CashDrawerSessionRepositoryImpl.class);
        bind(CashDrawerActivityLogRepository.class).to(CashDrawerActivityLogRepositoryImpl.class);
        bind(PaidInOutItemRepository.class).to(PaidInOutItemRepositoryImpl.class);
        bind(PromotionRepository.class).to(PromotionRepositoryImpl.class);
        bind(PromoUsageRepository.class).to(PromoUsageRepositoryImpl.class);
        bind(ThirdPartyAccountRepository.class).to(ThirdPartyAccountRepositoryImpl.class);
        bind(BarcodeItemRepository.class).to(BarcodeItemRepositoryImpl.class);
        bind(PrepackageProductItemRepository.class).to(PrepackageProductItemRepositoryImpl.class);
        bind(CompanyUniqueSequenceRepository.class).to(CompanyUniqueSequenceRepositoryImpl.class);
        bind(PrepackageRepository.class).to(PrepackageRepositoryImpl.class);
        bind(WeedmapAccountRepository.class).to(WeedmapAccountRepositoryImpl.class);
        bind(ConsumerCartRepository.class).to(ConsumerCartRepositoryImpl.class);
        bind(ProductPrepackageQuantityRepository.class).to(ProductPrepackageQuantityRepositoryImpl.class);
        bind(MemberGroupPricesRepository.class).to(MemberGroupPricesRepositoryImpl.class);
        bind(HeadsetLocationRepository.class).to(HeadsetLocationRepositoryImpl.class);
        bind(MarketingJobRepository.class).to(MarketingJobRepositoryImpl.class);
        bind(CreditSaleLineItemRepository.class).to(CreditSaleLineItemRepositoryImpl.class);
        bind(LoyaltyUsageLogRepository.class).to(LoyaltyUsageLogRepositoryImpl.class);
        bind(InvalidPhoneRepository.class).to(InvalidPhoneRepositoryImpl.class);
        bind(LoyaltyRewardRepository.class).to(LoyaltyRewardRepositoryImpl.class);
        bind(ThirdPartyShopAccountRepository.class).to(ThirdPartyShopAccountRepositoryImpl.class);
        bind(MetrcAccountRepository.class).to(MetrcAccountRepositoryImpl.class);
        bind(ConversationRepository.class).to(ConversationRepositoryImpl.class);
        bind(PurchaseOrderRepository.class).to(PurchaseOrderRepositoryImpl.class);
        bind(POActivityRepository.class).to(POActivityRepositoryImpl.class);
        bind(ShipmentBillRepository.class).to(ShipmentBillRepositoryImpl.class);
        bind(BatchQuantityRepository.class).to(BatchQuantityRepositoryImpl.class);
        bind(ShipmentBillRepository.class).to(ShipmentBillRepositoryImpl.class);
        bind(KioskPluginRepository.class).to(KioskPluginRepositoryImpl.class);
        bind(BlazePluginProductRepository.class).to(BlazePluginProductRepositoryImpl.class);
        bind(MessagingPluginRepository.class).to(MessagingPluginRepositoryImpl.class);
        bind(TVPluginRepository.class).to(TVPluginRepositoryImpl.class);
        bind(TVDisplayRepository.class).to(TVDisplayRepositoryImpl.class);
        bind(PaymentRepository.class).to(PaymentRepositoryImpl.class);
        bind(InviteEmployeeRepository.class).to(InviteEmployeeRepositoryImpl.class);
        bind(DeliveryAreaRepository.class).to(DeliveryAreaRepositoryImpl.class);
        bind(CareGiverRepository.class).to(CareGiverRepositoryImpl.class);
        bind(ReconciliationHistoryRepository.class).to(ReconciliationHistoryRepositoryImpl.class);
        bind(InventoryTransferHistoryRepository.class).to(InventoryTransferHistoryRepositoryImpl.class);
        bind(OnFleetErrorLogRepository.class).to(OnFleetErrorLogRepositoryImpl.class);
        bind(CustomerCompanyRepository.class).to(CustomerCompanyRepositoryImpl.class);
        bind(CompanyContactRepository.class).to(CompanyContactRepositoryImpl.class);
        bind(CompanyContactLogRepository.class).to(CompanyContactLogRepositoryImpl.class);
        bind(CreditHistoryRepository.class).to(CreditHistoryRepositoryImpl.class);
        bind(MarketingJobLogRepository.class).to(MarketingJobLogRepositoryImpl.class);
        bind(BrandRepository.class).to(BrandRepositoryImpl.class);
        bind(ReportTrackRepository.class).to(ReportTrackRepositoryImpl.class);
        bind(ExciseTaxInfoRepository.class).to(ExciseTaxInfoRepositoryImpl.class);
        bind(OnFleetTeamsRepository.class).to(OnFleetTeamsRepositoryImpl.class);
        bind(AppRolePermissionRepository.class).to(AppRolePermissionRepositoryImpl.class);
        bind(UserActivityRepository.class).to(UserActivityRepositoryImpl.class);
        bind(CultivationTaxInfoRepository.class).to(CultivationTaxInfoRepositoryImpl.class);
        bind(PartnerWebHookRepository.class).to(PartnerWebHookRepositoryImpl.class);
        bind(BouncedNumberRepository.class).to(BouncedNumberRepositoryImpl.class);
        bind(RefundTransactionRequestRepository.class).to(RefundTransactionRequestRepositoryImpl.class);
        bind(MemberActivityRepository.class).to(MemberActivityRepositoryImpl.class);
        bind(SpringbigRepository.class).to(SpringbigRepositoryImpl.class);
        bind(WebHookLogRepository.class).to(WebHookLogRepositoryImpl.class);
        bind(BatchActivityLogRepository.class).to(BatchActivityLogRepositoryImpl.class);
        bind(DriverLicenseLogRepository.class).to(DriverLicenseLogRepositoryImpl.class);
        bind(InventoryActionRepository.class).to(InventoryActionRepositoryImpl.class);
        bind(TestSampleRepository.class).to(TestSampleRepositoryImpl.class);
        bind(PricingTemplateRepository.class).to(PricingTemplateRepositoryImpl.class);
        bind(IntegrationSettingRepository.class).to(IntegrationSettingRepositoryImpl.class);
        bind(NotificationInfoRepository.class).to(NotificationInfoRepositoryImpl.class);
        bind(TookanAccountRepository.class).to(TookanAccountRepositoryImpl.class);
        bind(TookanTeamRepository.class).to(TookanTeamRepositoryImpl.class);
        bind(CompanyBounceNumberRepository.class).to(CompanyBounceNumberRepositoryImpl.class);
        bind(MarketingJobLogRepository.class).to(MarketingJobLogRepositoryImpl.class);
        bind(TookanErrorLogRepository.class).to(TookanErrorLogRepositoryImpl.class);
        bind(TestSampleRepository.class).to(TestSampleRepositoryImpl.class);
        bind(ShopPaymentOptionRepository.class).to(ShopPaymentOptionRepositoryImpl.class);
        bind(ReportingInfoRepository.class).to(ReportingInfoRepositoryImpl.class);
        bind(BlazeRegionRepository.class).to(BlazeRegionRepositoryImpl.class);
        bind(DerivedProductBatchLogRepository.class).to(DerivedProductBatchLogRepositoryImpl.class);
        bind(ReportRequestRepository.class).to(ReportRequestRepositoryImpl.class);
        bind(DeliveryTaxRateRepository.class).to(DeliveryTaxRateRepositoryImpl.class);
        bind(BackgroundTaskRepository.class).to(BackgroundTaskRepositoryImpl.class);
        bind(CheckInRepository.class).to(CheckInRepositoryImpl.class);
        bind(AdjustmentRepository.class).to(AdjustmentRepositoryImpl.class);
        bind(SyncRequestRepository.class).to(SyncRequestRepositoryImpl.class);
        bind(CompliancePackageRepository.class).to(CompliancePackageRepositoryImpl.class);
        bind(ComplianceTransferRepository.class).to(ComplianceTransferRepositoryImpl.class);
        bind(ComplianceItemRepository.class).to(ComplianceItemRepositoryImpl.class);
        bind(ComplianceCategoryRepository.class).to(ComplianceCategoryRepositoryImpl.class);
        bind(ComplianceSaleReceiptRepository.class).to(ComplianceSaleReceiptRepositoryImpl.class);
        bind(ComplianceStrainRepository.class).to(ComplianceStrainRepositoryImpl.class);
        bind(ComplianceSyncJobRepository.class).to(ComplianceSyncJobRepositoryImpl.class);
        bind(WeedmapSyncItemRepository.class).to(WeedmapSyncItemRepositoryImpl.class);
        bind(CloverReceiptRepository.class).to(CloverReceiptRepositoryImpl.class);
        bind(WmTagGroupRepository.class).to(WmTagGroupRepositoryImpl.class);
        bind(StorewideSaleRepository.class).to(StorewideSaleRepositoryImpl.class);
        bind(WmMappingRepository.class).to(WmMappingRepositoryImpl.class);
        bind(ProductVersionRepository.class).to(ProductVersionRepositoryImpl.class);
        bind(DiscountVersionRepository.class).to(DiscountVersionRepositoryImpl.class);
        bind(OrganizationRepository.class).to(OrganizationRepositoryImpl.class);
        bind(WmSyncJobRepository.class).to(WmSyncJobRepositoryImpl.class);
        bind(ShipmentRepository.class).to(ShipmentRepositoryImpl.class);
        bind(InventoryTransferKitRepository.class).to(InventoryTransferKitRepositoryImpl.class);
        bind(LeaflyAccountRepository.class).to(LeaflyAccountRepositoryImpl.class);
        bind(LeaflySyncJobRepository.class).to(LeaflySyncJobRepositoryImpl.class);

        // Services
        bind(AuthenticationService.class).to(AuthenticationServiceImpl.class);
        bind(MemberService.class).to(MemberServiceImpl.class);
        bind(EmployeeService.class).to(EmployeeServiceImpl.class);
        bind(DoctorService.class).to(DoctorServiceImpl.class);
        bind(ProductService.class).to(ProductServiceImpl.class);
        bind(TerminalService.class).to(TerminalServiceImpl.class);
        bind(ShopService.class).to(ShopServiceImpl.class);
        bind(ProductCategoryService.class).to(ProductCategoryServiceImpl.class);
        bind(SettingService.class).to(SettingServiceImpl.class);
        bind(VendorService.class).to(VendorServiceImpl.class);
        bind(com.fourtwenty.core.verificationsite.WebsiteService.class).to(com.fourtwenty.core.verificationsite.impl.WebsiteServiceImpl.class);
        bind(InventoryService.class).to(InventoryServiceImpl.class);
        bind(AssetService.class).to(AssetServiceImpl.class);
        bind(AmazonS3Service.class).to(AmazonS3ServiceImpl.class);
        bind(MedicalConditionService.class).to(MedicalConditionServiceImpl.class);
        bind(DefaultDataService.class).to(DefaultDataServiceImpl.class);
        bind(ContractService.class).to(ContractServiceImpl.class);
        bind(POSMembershipService.class).to(POSMembershipServiceImpl.class);
        bind(SignedContractService.class).to(SignedContractServiceImpl.class);
        bind(RealtimeService.class).to(RealtimeServiceImpl.class);
        bind(ImportService.class).to(ImportServiceImpl.class);
        bind(ReportingService.class).to(ReportingServiceImpl.class);
        bind(BackgroundJobService.class).to(BackgroundJobServiceImpl.class);
        bind(CompanyService.class).to(CompanyServiceImpl.class);
        bind(RoleService.class).to(RoleServiceImpl.class);
        bind(TimeCardService.class).to(TimeCardServiceImpl.class);
        bind(MemberGroupService.class).to(MemberGroupServiceImpl.class);
        bind(AuditLogService.class).to(AuditLogServiceImpl.class);
        bind(PasswordResetService.class).to(PasswordResetServiceImpl.class);
        bind(SupportService.class).to(SupportServiceImpl.class);
        bind(EmailService.class).to(EmailServiceImpl.class);
        bind(CashDrawerService.class).to(CashDrawerServiceImpl.class);
        bind(CashDrawerActivityLogService.class).to(CashDrawerActivityLogServiceImpl.class);
        bind(PromotionService.class).to(PromotionServiceImpl.class);
        bind(ThirdPartyAccountService.class).to(ThirdPartyAccountServiceImpl.class);
        bind(BarcodeService.class).to(BarcodeServiceImpl.class);
        bind(PrepackageProductService.class).to(PrepackageProductServiceImpl.class);
        bind(CommonLabelService.class).to(CommonLabelServiceImpl.class);
        bind(CartService.class).to(CartServiceImpl.class);
        bind(WeedmapService.class).to(WeedmapServiceImpl.class);
        bind(VerificationSiteService.class).to(VerificationSiteServiceImpl.class);
        bind(AmazonSNSClientService.class).to(AmazonSNSClientServiceImpl.class);
        bind(POSStoreService.class).to(POSStoreServiceImpl.class);
        bind(HeadsetAPIService.class).to(HeadsetAPIServiceImpl.class);
        bind(MarketingJobService.class).to(MarketingJobServiceImpl.class);
        bind(EmployeeNotificationService.class).to(EmployeeNotificationServiceImpl.class);
        bind(HeadsetService.class).to(HeadsetServiceImpl.class);
        bind(CreditService.class).to(CreditServiceImpl.class);
        bind(LoyaltyRewardService.class).to(LoyaltyRewardServiceImpl.class);
        bind(LoyaltyActivityService.class).to(LoyaltyActivityServiceImpl.class);
        bind(ThirdPartyShopAccountService.class).to(ThirdPartyShopAccountServiceImpl.class);
        bind(BlazePaymentService.class).to(BlazePaymentServiceImpl.class);
        bind(ResetDataService.class).to(ResetDataServiceImpl.class);
        bind(QueuePOSService.class).to(QueuePOSServiceImpl.class);
        bind(HypurService.class).to(HypurServiceImpl.class);
        bind(ConversationService.class).to(ConversationServiceImpl.class);
        bind(TwilioMessageService.class).to(TwilioMessageServiceImpl.class);
        bind(PurchaseOrderService.class).to(PurchaseOrderServiceImpl.class);
        bind(ShipmentBillService.class).to(ShipmentBillServiceImpl.class);
        bind(POActivityService.class).to(POActivityServiceImpl.class);
        bind(BlazePluginProductService.class).to(BlazePluginProductServiceImpl.class);
        bind(MessagingPluginService.class).to(MessagingPluginServiceImpl.class);
        bind(KioskPluginService.class).to(KioskPluginServiceImpl.class);
        bind(TVPluginService.class).to(TVPluginServiceImpl.class);
        bind(InventoryManagerService.class).to(InventoryManagerServiceImpl.class);
        bind(TVDisplayService.class).to(TVDisplayServiceImpl.class);
        bind(BatchQuantityService.class).to(BatchQuantityServiceImpl.class);
        bind(DeliveryService.class).to(DeliveryServiceImpl.class);
        bind(InviteEmployeeService.class).to(InviteEmployeeServiceImpl.class);
        bind(PromotionProcessService.class).to(PromotionProcessServiceImpl.class);
        bind(InventoryTransferHistoryService.class).to(InventoryTransferHistoryServiceImpl.class);
        bind(ExportService.class).to(ExportServiceImpl.class);
        bind(RecommendEngineService.class).to(RecommendEngineServiceImpl.class);
        bind(CustomerCompanyService.class).to(CustomerCompanyServiceImpl.class);
        bind(OnFleetService.class).to(OnFleetServiceImpl.class);
        bind(OnFleetAPIService.class).to(OnFleetAPIServiceImpl.class);
        bind(OnFleetErrorLogService.class).to(OnFleetErrorLogServiceImpl.class);
        bind(CannabisLimitService.class).to(CannabisLimitServiceImpl.class);
        bind(StateCannabisLimitRepository.class).to(StateCannabisLimitRepositoryImpl.class);
        bind(CompanyContactService.class).to(CompanyContactServiceImpl.class);
        bind(CompanyContactLogService.class).to(CompanyContactLogServiceImpl.class);
        bind(CreditHistoryService.class).to(CreditHistoryServiceImpl.class);
        bind(ExciseTaxInfoService.class).to(ExciseTaxInfoServiceImpl.class);
        bind(ImageProcessorService.class).to(ImageProcessorServiceImpl.class);
        bind(ProductBatchService.class).to(ProductBatchServiceImpl.class);
        bind(UserActivityService.class).to(UserActivityServiceImpl.class);
        bind(CultivationTaxInfoService.class).to(CultivationTaxInfoServiceImpl.class);
        bind(PartnerWebHookService.class).to(PartnerWebHookServiceImpl.class);
        bind(CompositeDataSyncService.class).to(CompositeDataSyncServiceImpl.class);
        bind(SpringBigAPIService.class).to(SpringBigAPIServiceImpl.class);
        bind(MemberActivityService.class).to(MemberActivityServiceImpl.class);
        bind(SpringBigService.class).to(SpringBigServiceImpl.class);
        bind(CashDrawerProcessorService.class).to(CashDrawerProcessorServiceImpl.class);
        bind(BatchActivityLogService.class).to(BatchActivityLogServiceImpl.class);
        bind(PosBulkService.class).to(PosBulkServiceImpl.class);
        bind(DriverLicenseLogService.class).to(DriverLicenseLogServiceImpl.class);
        bind(PricingTemplateService.class).to(PricingTemplateServiceImpl.class);
        bind(ReportingInfoService.class).to(ReportingInfoServiceImpl.class);
        bind(DerivedProductBatchLogService.class).to(DerivedProductBatchLogServiceImpl.class);
        bind(DeliveryTaxRateService.class).to(DeliveryTaxRateServiceImpl.class);
        bind(OnlineCustomerService.class).to(OnlineCustomerServiceImpl.class);
        bind(AdjustmentService.class).to(AdjustmentServiceImpl.class);
        bind(PartnerLoyaltyService.class).to(PartnerLoyaltyServiceImpl.class);
        bind(ComplianceService.class).to(ComplianceServiceImpl.class);
        bind(LabelService.class).to(LabelServiceImpl.class);
        bind(StorewideSaleService.class).to(StorewideSaleServiceImpl.class);
        bind(StripeService.class).to(StripeServiceImpl.class);
        bind(OrganizationService.class).to(OrganizationServiceImpl.class);
        bind(ReportDetailsService.class).to(ReportDetailsServiceImpl.class);
        bind(ShipmentService.class).to(ShipmentServiceImpl.class);
        bind(InventoryTransferKitService.class).to(InventoryTransferKitServiceImpl.class);

        bind(MetrcService.class).to(MetrcServiceImpl.class);
        bind(MetrcService.class).to(MetrcServiceImpl.class);
        bind(TookanService.class).to(TookanServiceImpl.class);
        bind(TookanApiService.class).to(TookanApiServiceImpl.class);
        bind(IncomingSMSService.class).to(IncomingSMSServiceImpl.class);
        bind(TookanErrorLogService.class).to(TookanErrorLogServiceImpl.class);
        bind(NotificationInfoService.class).to(NotificationInfoServiceImpl.class);
        bind(ShopPaymentOptionService.class).to(ShopPaymentOptionServiceImpl.class);
        bind(BlazeRegionService.class).to(BlazeRegionServiceImpl.class);
        bind(PoTestSampleService.class).to(PoTestSampleServiceImpl.class);
        bind(ReportRequestService.class).to(ReportRequestServiceImpl.class);
        bind(CheckInService.class).to(CheckInServiceImpl.class);

        bind(TaxCalulationService.class).to(TaxCalulationServiceImpl.class);
        bind(CommonPurchaseOrderService.class).to(CommonPurchaseOrderServiceImpl.class);
        bind(StorePurchaseOrderService.class).to(StorePurchaseOrderServiceImpl.class);
        bind(PartnerAssetService.class).to(PartnerAssetServiceImpl.class);
        bind(CompanyUniqueSequenceService.class).to(CompanyUniqueSequenceServiceImpl.class);

        bind(MemberUserService.class).to(MemberUserServiceImpl.class);

        // Delivery
        bind(CommonPOSService.class).to(CommonPOSServiceImpl.class);
        bind(DeliveryUtilService.class).to(DeliveryUtilServiceImpl.class);
        // Inventory
        bind(InventoryOperationService.class).to(InventoryOperationServiceImpl.class);
        bind(CommonInventoryService.class).to(CommonInventoryServiceImpl.class);
        bind(ProductWeightToleranceService.class).to(ProductWeightToleranceServiceImpl.class);

        // Consumer Repos
        bind(ConsumerUserRepository.class).to(ConsumerUserRepositoryImpl.class);
        bind(ConsumerPasswordResetRepository.class).to(ConsumerPasswordResetRepositoryImpl.class);
        bind(BrandService.class).to(BrandServiceImpl.class);

        //Consumer Services
        bind(ConsumerAuthService.class).to(ConsumerAuthServiceImpl.class);
        bind(ConsumerNotificationService.class).to(ConsumerNotificationServiceImpl.class);
        bind(FcmService.class).to(FcmServiceImpl.class);

        // Developers
        bind(DeveloperKeyRepository.class).to(DeveloperKeyRepositoryImpl.class);
        bind(DeveloperInventoryService.class).to(DeveloperInventoryServiceImpl.class);
        bind(DeveloperKeyService.class).to(DeveloperKeyServiceImpl.class);
        bind(PartnerKeyRepository.class).to(PartnerKeyRepositoryImpl.class);
        bind(PartnerKeyService.class).to(PartnerKeyServiceImpl.class);
        bind(PartnerMemberService.class).to(PartnerMemberServiceImpl.class);
        bind(PartnerVendorService.class).to(PartnerVendorServiceImpl.class);
        bind(PartnerProductService.class).to(PartnerProductServiceImpl.class);
        bind(PartnerCompanyContactService.class).to(PartnerCompanyContactServiceImpl.class);

        bind(CommonVendorService.class).to(CommonVendorServiceImpl.class);
        bind(CommonProductService.class).to(CommonProductServiceImpl.class);
        bind(CommonBarCodeService.class).to(CommonBarCodeServiceImpl.class);
        bind(CommonCompanyContactService.class).to(CommonCompanyContactServiceImpl.class);

        // Store Widget Stuff
        bind(StoreWidgetKeyRepository.class).to(StoreWidgetKeyRepositoryImpl.class);
        bind(StoreWidgetKeyService.class).to(StoreWidgetKeyServiceImpl.class);
        bind(StoreInventoryService.class).to(StoreInventoryServiceImpl.class);
        bind(StoreInfoService.class).to(StoreInfoServiceImpl.class);
        bind(ConsumerCartService.class).to(ConsumerCartServiceImpl.class);
        bind(ConsumerUserService.class).to(ConsumerUserServiceImpl.class);
        bind(PartnerTransactionService.class).to(PartnerTransactionServiceImpl.class);


        // Third parties
        bind(WeedmapAPIService.class).to(WeedmapAPIServiceImpl.class);
        bind(GoogleAPIService.class).to(GoogleAPIServiceImpl.class);
        bind(HeadsetAPIService.class).to(HeadsetAPIServiceImpl.class);
        bind(MetrcAuthorization.class).to(MetrcAuthorizationImpl.class);
        bind(MetrcAccountService.class).to(MetrcAccountServiceImpl.class);
        bind(MetrcProcessingService.class).to(MetrcProcessingServiceImpl.class);
        bind(LeaflyAccountService.class).to(LeaflyAccountServiceImpl.class);
        bind(LeaflyAPIService.class).to(LeaflyAPIServiceImpl.class);
        bind(LeaflyService.class).to(LeaflyServiceImpl.class);

        //Amazon
        bind(AmazonSQSFifoService.class).to(AmazonSQSFifoServiceImpl.class);
        bind(ElasticSearchCommunicatorService.class).to(ElasticSearchCommunicatorServiceImpl.class);
        bind(ElasticSearchService.class).to(ElasticSearchServiceImpl.class);
        bind(ElasticSearchIndexingService.class).to(ElasticSearchIndexingServiceImpl.class);

        //Sendgrid
        bind(SendgridSerive.class).to(SendgridServiceImpl.class);

        // Tests
        bind(SMSTestService.class).to(SMSTestServiceImpl.class);
        // Cache Service
        bind(CacheService.class).to(CacheServiceImpl.class);

        // OnPrem
        bind(OnPremCloudSyncService.class).to(OnPremCloudSyncServiceImpl.class);
        bind(OnPremSyncRepository.class).to(OnPremSyncRepositoryImpl.class);
        bind(OnPremSyncDetailsRepository.class).to(OnPremSyncDetailsRepositoryImpl.class);
        bind(OnPremSyncService.class).to(OnPremSyncServiceImpl.class);

        // RabbitMQ Queue
        bind(RabbitMQFifoService.class).to(RabbitMQFifoServiceImpl.class);

        // secured interceptors
        SecuredInterceptor interceptor = new SecuredInterceptor();
        requestInjection(interceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(Secured.class), interceptor);

        // Developer secured interceptor
        DeveloperSecurityInterceptor devInterceptor = new DeveloperSecurityInterceptor();
        requestInjection(devInterceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(DeveloperSecured.class), devInterceptor);

        // Store Secured Interceptor
        StoreSecurityInterceptor storeInterceptor = new StoreSecurityInterceptor();
        requestInjection(storeInterceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(StoreSecured.class), storeInterceptor);

        // Internal Secured Interceptor
        InternalApiSecurityInterceptor internalApiSecurityInterceptor = new InternalApiSecurityInterceptor();
        requestInjection(internalApiSecurityInterceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(InternalSecured.class), internalApiSecurityInterceptor);
        // OnPremInterceptor
        OnPremInterceptor onPremInterceptor = new OnPremInterceptor();
        requestInjection(onPremInterceptor);
        bindInterceptor(Matchers.annotatedWith(Path.class), Matchers.annotatedWith(OnPremSecured.class), onPremInterceptor);

        // Jobs
        bind(TransactionJob.class);

        // Transaction Complete Event binder
        Multibinder<TransactionDidCompleteSaleReceiver> mb = Multibinder.newSetBinder(binder(), TransactionDidCompleteSaleReceiver.class);
        mb.addBinding().to(DefaultTransactionDidCompleteSaleReceiver.class);
        mb.addBinding().to(MetrcTransactionDidCompleteSaleReceiverImpl.class);

        // Transaction Refund Event binder
        Multibinder<TransactionDidRefundReceiver> refundMB = Multibinder.newSetBinder(binder(), TransactionDidRefundReceiver.class);
        refundMB.addBinding().to(DefaultTransactionDidRefundReceiver.class);
        refundMB.addBinding().to(MetrcTransactionDidRefundReceiverImpl.class);
        refundMB.addBinding().to(RewardsTransactionRefundReceiver.class);


        // PromotionValidations
        Multibinder<PromoValidation> promoValdations = Multibinder.newSetBinder(binder(), PromoValidation.class);
        promoValdations.addBinding().to(ActiveDateRangeRule.class);
        promoValdations.addBinding().to(ActiveStateRule.class);
        promoValdations.addBinding().to(DayAvailabilityRule.class);
        promoValdations.addBinding().to(DayDurationRule.class);
        promoValdations.addBinding().to(MaxUsageRule.class);
        promoValdations.addBinding().to(MemberGroupRestrictRule.class);
        promoValdations.addBinding().to(CustomerUsageRule.class);
        promoValdations.addBinding().to(DeliveryFeeRule.class);


        Multibinder<PromoRuleValidation> promoRuleValdations = Multibinder.newSetBinder(binder(), PromoRuleValidation.class);
        promoRuleValdations.addBinding().to(CartPromoFirstTimeMemberRule.class);
        promoRuleValdations.addBinding().to(CartPromoNumberMinOrdersRule.class);
        promoRuleValdations.addBinding().to(CartPromoNumberMinOrdersRule.class);
        promoRuleValdations.addBinding().to(CartPromoOrderMinRule.class);
        promoRuleValdations.addBinding().to(CartPromoVendorRule.class);
        promoRuleValdations.addBinding().to(ProductPromoMatchedProductsRule.class);
        promoRuleValdations.addBinding().to(ProductPromoMatchedVendorsRule.class);
        promoRuleValdations.addBinding().to(ProductPromoMatchedCategoriesRule.class);


        // Token providers
        bind(ConnectAuthToken.class).toProvider(SecurityProvider.class);
        bind(DeveloperAuthToken.class).toProvider(DeveloperSecurityProvider.class);
        bind(StoreAuthToken.class).toProvider(StoreSecurityProvider.class);
        bind(PartnerAuthToken.class).toProvider(PartnerSecurityProvider.class);
        bind(InternalApiAuthToken.class).toProvider(InternalApiSecurityProvider.class);
        bind(OnPremToken.class).toProvider(OnPremProvider.class);


        // Caching support
        CacheInterceptor cacheInterceptor = new CacheInterceptor();
        binder().requestInjection(cacheInterceptor);
        bindInterceptor(subclassesOf(BaseRepository.class),
                any(),
                cacheInterceptor);

        // PubSub support
        DeadEventListener deadEventListener = new DeadEventListener();
        BlazeEventBusImpl blazeEventBus = new BlazeEventBusImpl();
        blazeEventBus.register(deadEventListener);
        blazeEventBus.autoRegisterSubscribers(binder());
        bind(BlazeEventBus.class).toInstance(blazeEventBus);

    }


    @Provides
    private synchronized SecurityUtil getSecurityUtil(ConnectConfiguration config) {
        if (securityUtil == null) {
            securityUtil = new SecurityUtil(config);
        }
        return securityUtil;
    }

    @Provides
    private synchronized AmazonServiceFactory getAmazonFactory(ConnectConfiguration config) {
        if (s3Factory == null) {
            s3Factory = new AmazonServiceFactory(config);
        }
        return s3Factory;
    }

    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }

    @Provides
    public synchronized DefaultData getDefaultData(Environment environment) {
        if (defaultData == null) {

            final ObjectMapper objectMapper = environment.getObjectMapper();
            InputStream stream = CoreModule.class.getResourceAsStream("/defaultdata.json");

            try {
                defaultData = objectMapper.readValue(stream, DefaultData.class);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return new DefaultData();
            }
        }
        return defaultData;
    }

    @Provides
    public synchronized IndexesData getIndexesData(Environment environment) {
        if (indexesData == null) {
            final ObjectMapper objectMapper = environment.getObjectMapper();
            InputStream inputStream = CoreModule.class.getResourceAsStream("/indexes.json");

            try {
                indexesData = objectMapper.readValue(inputStream, IndexesData.class);
            } catch (Exception e) {
                return new IndexesData();
            }
        }
        return indexesData;
    }


    @Provides
    private synchronized MongoDb getMongoManager(ConnectConfiguration config) {
        if (mongoDBManager == null) {
            mongoDBManager = new MongoManager(config);
        }
        return mongoDBManager;
    }


}
