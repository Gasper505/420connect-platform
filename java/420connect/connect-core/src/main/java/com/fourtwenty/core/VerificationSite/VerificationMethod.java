package com.fourtwenty.core.verificationsite;

/**
 * Created by anuragdhunna on 24/5/17.
 */
public enum VerificationMethod {

    DEFAULT,
    MANUAL,
    HELLOMD;

    public static VerificationMethod getType(String typeString) {
        try {
            return Enum.valueOf(VerificationMethod.class, typeString.toUpperCase());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return VerificationMethod.DEFAULT;
        }
    }
}
