package com.fourtwenty.core.verificationsite;

import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Gaurav Saini on 24/5/17.
 */
public class WebsiteManager {

    @Inject
    private WebsiteService websiteService;

    public String getWebsiteDataForType(String websiteType, String verificationCode) {
        String data = null;

        if (StringUtils.isNotBlank(verificationCode)) {
            verificationCode = verificationCode.replaceAll(" ", "");
        }

        VerificationMethod type = VerificationMethod.getType(websiteType);
        if (type == VerificationMethod.DEFAULT) {
            throw new BlazeInvalidArgException("WebsiteService", "Invalid Argument - Website Type");
        }

        switch (type) {
            case HELLOMD:
                data = websiteService.getHelloMDPatient(verificationCode);
        }
        return data;
    }
}
