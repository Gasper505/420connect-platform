package com.fourtwenty.core.verificationsite;

/**
 * Created by Gaurav Saini on 24/5/17.
 */
public interface WebsiteService {

    String getHelloMDPatient(String verificationCode);
}
