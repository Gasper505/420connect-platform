package com.fourtwenty.core.verificationsite.impl;

import com.fourtwenty.core.verificationsite.WebsiteService;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.google.inject.Inject;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Gaurav Saini on 24/5/17.
 */
public class WebsiteServiceImpl implements WebsiteService {

    static final Log LOG = LogFactory.getLog(WebsiteServiceImpl.class);

    @Inject
    private ConnectConfiguration config;

    public String getHelloMDPatient(String verificationCode) {

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet("https://www.hellomd.com/api/v1/recommendation/" + verificationCode);
        getRequest.setHeader("Accept", "application/json");
        getRequest.setHeader("Content-Type", "application/json");

        String apiKey = config.getVerificationSiteConfig().getApiKey();
        String secretKey = config.getVerificationSiteConfig().getSecretKey();

        String binaryData = apiKey + ":" + secretKey;
        getRequest.setHeader("Authorization", "Basic " + Base64.encodeBase64String(binaryData.getBytes()));

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(getRequest);
        } catch (IOException e) {
            LOG.error("Error while connecting to HelloMD " + e.getMessage(), e);
        }
        int statusCode = (httpResponse != null && httpResponse.getStatusLine() != null) ? httpResponse.getStatusLine().getStatusCode() : 401;

        if (statusCode == 401) {
            throw new BlazeInvalidArgException("Authentication", "api and secret Keys not valid.");
        }

        BufferedReader br = null;
        try {
            br = new BufferedReader(
                    new InputStreamReader((httpResponse.getEntity().getContent())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder builder = new StringBuilder();
        String output;
        try {
            while ((output = br.readLine()) != null) {

                builder.append(output);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (statusCode != 200) {
            JSONObject jsonObject = JSONObject.fromObject(builder.toString());
            throw new BlazeInvalidArgException("error_message", jsonObject.getString("error_message"));
        }
        return builder.toString();
    }
}
