package com.fourtwenty.core.caching;

public interface CacheClient extends AutoCloseable {

    <T extends Object> T getItem(String key, Class[] classes);

    <T> void putItem(String key, T obj);

    void invalidateItems(String[] keys);

    boolean isWorking();
}
