package com.fourtwenty.core.caching;

public interface CacheService {

    <T> T getItem(String key, Class<T> tClass);

    <T> void putItem(String key, T obj);

    void invalidateItems(String[] keys);

    String generateKey(String key, CacheType cacheType);

    enum CacheType {
        DATA("D:");

        private String prefix;
        CacheType(String prefix) {
            this.prefix = prefix;
        }

        public String getPrefix() {
            return prefix;
        }
    }
}
