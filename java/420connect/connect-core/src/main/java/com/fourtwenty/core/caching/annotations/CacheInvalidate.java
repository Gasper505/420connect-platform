package com.fourtwenty.core.caching.annotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface CacheInvalidate {
    boolean global() default false;

    String entity() default "";

    String[] keys() default {};
}
