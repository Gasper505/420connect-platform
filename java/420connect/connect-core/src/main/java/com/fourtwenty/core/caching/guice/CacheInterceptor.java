package com.fourtwenty.core.caching.guice;

import com.fourtwenty.core.caching.CacheClient;
import com.fourtwenty.core.caching.annotations.CacheInvalidate;
import com.fourtwenty.core.caching.annotations.Cached;
import com.fourtwenty.core.caching.redis.RedisCacheClient;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.impl.CartServiceImpl;
import com.google.common.collect.Lists;
import org.aopalliance.intercept.Invocation;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.inject.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CacheInterceptor implements MethodInterceptor {

    private static final Log LOG = LogFactory.getLog(CartServiceImpl.class);

    @Inject
    private ConnectConfiguration connectConfiguration;
    @Inject
    private Provider<ConnectAuthToken> tokenProvider;


    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        try (CacheClient cacheClient = initCacheClient()) {

            if (cacheClient != null) {
                // Invalidate Cache entries
                List<Annotation> annotations = null;

                annotations = getAnnotations(invocation, CacheInvalidate.class);
                annotations.stream().forEach(a -> invalidateCache(cacheClient, invocation, (CacheInvalidate) a));

                // lazy load Cache lookup
                annotations = getAnnotations(invocation, Cached.class);
                if (!annotations.isEmpty()) {
                    return cachedInvoke(cacheClient, invocation, (Cached) annotations.get(0));
                }
            }
        }

        return invocation.proceed();
    }

    private void invalidateCache(CacheClient cacheClient, MethodInvocation invocation, CacheInvalidate annotation) {
        try {
            String keyPrefix = generateCacheKey(invocation, annotation);

            if (annotation.keys().length == 0) {
                cacheClient.invalidateItems(new String[]{keyPrefix});
            } else {
                for (String key : annotation.keys()) {
                    cacheClient.invalidateItems(new String[]{keyPrefix + key});
                }
            }
        } finally {
        }
    }

    private Object cachedInvoke(CacheClient cacheClient, MethodInvocation invocation, Cached annotation) {
        try {
            String cacheKey = generateCacheKey(invocation, annotation);
            Type objType = invocation.getMethod().getGenericReturnType();
            List<Class> classes = new ArrayList<>();

            if (annotation.returnClasses().length > 0) {
                classes.addAll(Lists.newArrayList(annotation.returnClasses()));
            } else {
                classes.add(invocation.getMethod().getReturnType());

                // Attempt to discover return classes
                Class modelClass = findClassInArguments(invocation.getArguments());
                if (modelClass != null) {
                    classes.add(modelClass);
                } else if (invocation.getMethod().getReturnType().getTypeParameters().length > 0) {
                    classes.addAll(getGenericClasses(invocation.getMethod().getDeclaringClass()));
                }
            }

            Object obj = cacheClient.getItem(cacheKey, classes.toArray(new Class[0]));
            if (obj == null) {
                try {
                    //System.out.println("MISS: " + invocation.getMethod().getName());
                    obj = invocation.proceed();
                    cacheClient.putItem(cacheKey, obj);
                } catch (Throwable e) {
                    throw new BlazeOperationException("Failed to execute cached method. " + e.getMessage());
                }
            }
//        else {
//            System.out.println("HIT: " + invocation.getMethod().getName());
//        }
            return obj;
        } catch (Exception e) {
            return null;
        }
    }


    private String generateCacheKey(MethodInvocation invocation, CacheInvalidate annotation) {
        String entity = !annotation.entity().isEmpty() ?
                annotation.entity() : getProxyClassEntity(invocation);

        return generateCacheKey(invocation, annotation.global(), entity, false);
    }

    private String generateCacheKey(MethodInvocation invocation, Cached annotation) {
        String entity = !annotation.entity().isEmpty() ?
                annotation.entity() : getProxyClassEntity(invocation);

        return generateCacheKey(invocation, annotation.global(), entity, true);
    }

    private String generateCacheKey(MethodInvocation invocation, boolean isGlobal, String entity, boolean generateKeySuffix) {
        StringBuilder builder = new StringBuilder();
        builder.append(connectConfiguration.getEnv()).append("_");
        if (!isGlobal) {
            try {
                ConnectAuthToken token = tokenProvider.get();
                builder.append("C:");
                builder.append(token.getCompanyId());
            } catch (Exception e) {
            }
        }

        if (!entity.isEmpty()) {
            builder.append("E:");
            builder.append(entity);
        }

        if (generateKeySuffix) {
            builder.append("M:");
            builder.append(invocation.getMethod().getName());
            builder.append("A:");
            Arrays.stream(invocation.getArguments()).forEach(a -> builder.append(a + "-"));
        }

        return builder.toString();
    }


    private CacheClient initCacheClient() {
        if (connectConfiguration.getRedisHost() != null && !connectConfiguration.getRedisHost().isEmpty()) {
            return new RedisCacheClient(connectConfiguration.getRedisHost(), connectConfiguration.getRedisPort());
        }
        return null;
    }

    private String getProxyClassEntity(Invocation invocation) {
        try {
            Field proxyField = invocation.getClass().getDeclaredField("proxy");
            proxyField.setAccessible(true);
            Object proxy = proxyField.get(invocation);
            return getGenericParameterClassName(proxy.getClass().getSuperclass());
        } catch (Exception e) {
            LOG.error(e);
        }
        return null;
    }

    private Class findClassInArguments(Object[] objs) {
        for (int i = 0; i < objs.length; i++) {
            if (objs[i] != null && Class.class == objs[i].getClass()) {
                return (Class) objs[i];
            }
        }
        return null;
    }

    private List<Class> getGenericClasses(Class clazz) {
        List<Class> classes = new ArrayList<>();
        if (ParameterizedType.class.isAssignableFrom(clazz.getGenericSuperclass().getClass())) {
            Type[] types = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments();

            for (int i = 0; i < types.length; i++) {
                try {
                    String className = types[i].toString().split(" ")[1];
                    Class c = Class.forName(className);
                    classes.add(c);
                } catch (Exception e) {
                }
            }
        }
        return classes;
    }

    private String getGenericParameterClassName(Class clazz) {
        List<Class> classes = getGenericClasses(clazz);
        if (!classes.isEmpty()) return classes.get(0).getName();
        return "";
    }

    private List<Annotation> getAnnotations(MethodInvocation invocation, Class annotationClazz) {
        List<Annotation> annotations = new ArrayList<>();

        String methodName = invocation.getMethod().getName();
        Class<?>[] methodArgs = invocation.getMethod().getParameterTypes();
        List<Class> classes = getInterfacesRecursive(invocation.getMethod().getDeclaringClass());
        for (Class<?> clazz : classes) {
            try {
                Method interfaceMethod = clazz.getMethod(methodName, methodArgs);
                if (interfaceMethod != null && interfaceMethod.isAnnotationPresent(annotationClazz))
                    annotations.addAll(Lists.newArrayList(interfaceMethod.getDeclaredAnnotationsByType(annotationClazz)));
            } catch (NoSuchMethodException e) {
            }
        }
        return annotations;
    }

    private List<Class> getInterfacesRecursive(Class clazz) {
        List<Class> interfaces = new ArrayList<>();
        interfaces.add(clazz);

        Class[] interfaceArray = clazz.getInterfaces();
        if (interfaceArray != null) {
            for (Class interfaceClass : interfaceArray) {
                interfaces.addAll(getInterfacesRecursive(interfaceClass));
            }
        }

        return interfaces;
    }
}
