package com.fourtwenty.core.caching.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.caching.CacheClient;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import org.apache.commons.lang.math.NumberUtils;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

public class RedisCacheClient implements CacheClient {

    private static final int REDIS_DEFAULT_PORT = 6379;
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final Jedis jedis;

    public RedisCacheClient(String server, String port) {
        int portNumber = !port.isEmpty() && NumberUtils.isNumber(port) ?
                Integer.valueOf(port) : REDIS_DEFAULT_PORT;

        jedis = new Jedis(server, portNumber);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public Object getItem(String key, Class[] classes) {
        Object obj = null;

        String json = jedis.get(key);
        if (json != null) {
            try {
                if (classes != null && classes.length > 0) {
                    Class<?>[] parameterizedClasses = Arrays.asList(classes).stream()
                            .skip(1)
                            .collect(Collectors.toList())
                            .toArray(new Class<?>[]{});
                    if (parameterizedClasses.length == 0) {
                        // Normal
                        obj = objectMapper.readValue(json, classes[0]);
                    } else {
                        // Generic
                        JavaType type = objectMapper.getTypeFactory().constructParametricType(classes[0], parameterizedClasses);
                        obj = objectMapper.readValue(json, type);
                    }
                }
            } catch (IOException e) {
                throw new BlazeOperationException("Failed to serialize object. " + e.getMessage());
            }
        }

        return obj;
    }

    @Override
    public void putItem(String key, Object obj) {
        try {
            String data = objectMapper.writeValueAsString(obj);
            jedis.set(key, data);
            jedis.expire(key, 120);
        } catch (JsonProcessingException e) {
            throw new BlazeOperationException("Failed to deserialize object." + e.getMessage());
        }
    }

    @Override
    public void invalidateItems(String... keys) {
        for (String checkKey : keys) {
            for (String key : jedis.keys(checkKey + "*")) {
                jedis.del(key);
            }
        }
    }

    @Override
    public boolean isWorking() {
        try {
            String msg = jedis.ping();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public void close() throws Exception {
        try {
            jedis.close();
        } catch (Exception e) {
        }
    }
}
