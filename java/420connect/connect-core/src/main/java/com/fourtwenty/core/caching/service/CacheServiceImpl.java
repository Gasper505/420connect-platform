package com.fourtwenty.core.caching.service;

import com.fourtwenty.core.caching.CacheClient;
import com.fourtwenty.core.caching.CacheService;
import com.fourtwenty.core.caching.redis.RedisCacheClient;
import com.fourtwenty.core.config.ConnectConfiguration;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class CacheServiceImpl implements CacheService {

    private CacheClient cacheClient;
    private ConnectConfiguration connectConfiguration;

    @Inject
    public CacheServiceImpl(ConnectConfiguration connectConfiguration) {
        this.connectConfiguration = connectConfiguration;
        initClient();
    }

    private void initClient() {
        if (connectConfiguration.getRedisHost() != null && connectConfiguration.getRedisPort() != null) {
            this.cacheClient = new RedisCacheClient(connectConfiguration.getRedisHost(), connectConfiguration.getRedisPort());
        }
    }

    @Override
    @Nullable
    public <T> T getItem(String key, Class<T> tClass) {
        if (isInactive()) {
            return null;
        }
        return cacheClient.getItem(key, new Class[]{tClass});
    }

    @Override
    public <T> void putItem(String key, T obj) {
        if (isInactive()) {
            return;
        }
        cacheClient.putItem(key, obj);
    }

    @Override
    public void invalidateItems(String[] keys) {
        if (!isInactive()) {
            return;
        }
        cacheClient.invalidateItems(keys);
    }

    @Override
    public String generateKey(String key, CacheType cacheType) {
        return connectConfiguration.getEnv().name() + "_" + cacheType.getPrefix() + key;
    }

    private boolean isInactive() {
        return cacheClient == null || !cacheClient.isWorking();
    }
}
