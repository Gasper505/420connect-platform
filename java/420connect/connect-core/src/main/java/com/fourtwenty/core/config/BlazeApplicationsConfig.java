package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlazeApplicationsConfig {

    @JsonProperty(value = "tokenExpire")
    private int tokenExpire;
    @JsonProperty(value = "apps")
    private List<SwitchableApplication> apps = new ArrayList<>();

    public int getTokenExpire() {
        return tokenExpire;
    }

    public void setTokenExpire(int tokenExpire) {
        this.tokenExpire = tokenExpire;
    }

    public List<SwitchableApplication> getApps() {
        return apps;
    }

    public void setApps(List<SwitchableApplication> apps) {
        this.apps = apps;
    }

    public SwitchableApplication getRedirectURL(String appName) {
        if (appName == null) {
            return null;
        } else {
            for (SwitchableApplication app : apps) {
                if (app.getAppName().equals(appName)) {
                    return app;
                }
            }
        }
        return null;
    }
}
