package com.fourtwenty.core.config;

import com.blaze.clients.hypur.configs.HypurConfig;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mdo on 8/16/15.
 */
public class ConnectConfiguration extends Configuration {
    public enum EnvironmentType {
        LOCAL,
        DEV,
        STAGE,
        PROD,
        ONPREM
    }

    public enum BlazeAppType {
        BlazeRetail,
        BlazeJobScheduler,
        BlazeGrow,
        BlazeWareHouse
    }

    @Valid
    @JsonProperty("appType")
    private BlazeAppType appType = BlazeAppType.BlazeRetail;

    @Valid
    @JsonProperty("env")
    private EnvironmentType env = EnvironmentType.DEV;


    @Valid
    @JsonProperty("app.migration.enable")
    private boolean enableMigration = true;

    @Valid
    @NotNull
    @JsonProperty("appName")
    private String appName = "420Connect";

    @Valid
    @JsonProperty("google.api.key")
    private String googleAPIKey = "AIzaSyB03btOhaWdGzNVoCPv4PI2uz26ZVJEHQI";

    @Valid
    @NotNull
    @JsonProperty("app.support.email")
    private String supportEmail = "support@blaze.me";


    @Valid
    @JsonProperty("app.images.genthumbs")
    private boolean regenerateThumbs = true;

    @Valid
    @JsonProperty("cors.urls")
    private String coreUrls = "http://localhost:8080,http://420connect.com,https://www.420connect.com,https://app.420connect.com,http://app.420connect.com";


    @Valid
    @NotNull
    @JsonProperty("app.secret")
    private String appSecret;

    @Valid
    @NotNull
    @JsonProperty("app.website.url")
    private String appWebsiteURL;

    @Valid
    @NotNull
    @JsonProperty("app.api.url")
    private String appApiUrl = "https://api.blaze.me"; // production app api

    @Valid
    @JsonProperty("auth.ttl") //In Minutes
    private int authTTL = 2880;

    @Valid
    @JsonProperty("app.processorConfig")
    private TransProcessorConfig processorConfig = new TransProcessorConfig();


    @Valid
    @NotNull
    @JsonProperty("amazons3")
    private S3Config amazonS3Config;


    @Valid
    @JsonProperty("quickbook")
    private QuickBookConfig quickBookConfig;


    @Valid
    @JsonProperty("pusherConfig")
    private PusherConfig pusherConfig;

    @Valid
    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    @Valid
    @JsonProperty("mongoserver")
    public MongoConfiguration mongoConfiguration;

    @Valid
    @JsonProperty("redisHost")
    private String redisHost = "localhost";

    @Valid
    @JsonProperty("redisPort")
    private String redisPort = "6379";

    @Valid
    @JsonProperty("applePushCertName")
    private String applePushCertName;

    @Valid
    @JsonProperty("applePushCertPath")
    private String applePushCertPath;

    @Valid
    @JsonProperty("applePushCertPassword")
    private String applePushCertPassword;

    @Valid
    @NotNull
    @JsonProperty("hellomd")
    private VerificationSiteConfig verificationSiteConfig;

    @Valid
    @JsonProperty("requiredVersion")
    private String requiredVersion = "2.0.0";

    @Valid
    @JsonProperty("quartz")
    private QuartzConfiguration quartzConfiguration = new QuartzConfiguration();

    @JsonProperty("trackingURL")
    private String trackingURL = "https://track.blaze.me";


    @Valid
    @JsonProperty("hypur")
    private HypurConfig hypurConfig = new HypurConfig();

    @Valid
    @JsonProperty("wepay")
    private WepayConfiguration wepayConfiguration;

    @Valid
    @JsonProperty("twilio")
    private TwilioConfiguration twilioConfiguration;

    @Valid
    @JsonProperty("platformDNS")
    private String platformDNS;

    @Valid
    @JsonProperty("stripe")
    private StripeConfiguration stripeConfiguration;

    public boolean isRegenerateThumbs() {
        return regenerateThumbs;
    }

    @Valid
    @JsonProperty("amazonelastic")
    private AWSElasticConfig awsElasticConfig;

    @Valid
    @JsonProperty("blazeApplications")
    private BlazeApplicationsConfig blazeApplicationsConfig;

    @Valid
    @JsonProperty("sendgrid")
    private SendgridConfiguration sendgridConfiguration;

    @Valid
    @JsonProperty("bounceEmailQueueConfig")
    private BounceEmailProcessorConfig bounceEmailProcessorConfig;

    @Valid
    @JsonProperty("springbig")
    private SpringBigConfiguration springBigConfiguration;

    @Valid
    @JsonProperty("postgresql")
    private PostgreServerConfiguration postgreServerConfiguration;


    @Valid
    @JsonProperty("tookan")
    private TookanConfiguration tookanConfiguration;

    @Valid
    @JsonProperty("enableReportingCache")
    private boolean enableReportingCache = Boolean.FALSE;

    @Valid
    @NotNull
    @JsonProperty("app.api.internal.secret")
    private String internalApiSecret;

    @Valid
    @NotNull
    @JsonProperty("app.api.internal.iv")
    private String internalApiIV;
    @JsonProperty("platform")
    private PlatformModeConfiguration platformModeConfiguration;

    @JsonProperty("rabbitmq")
    private RabbitMQConfiguration rabbitMQConfiguration;

    @Valid
    @NotNull
    @JsonProperty("app.api.internal.salesAppLink")
    private String salesAppLink;

    public boolean isEnableMigration() {
        return enableMigration;
    }

    public void setEnableMigration(boolean enableMigration) {
        this.enableMigration = enableMigration;
    }

    public void setRegenerateThumbs(boolean regenerateThumbs) {
        this.regenerateThumbs = regenerateThumbs;
    }

    public TransProcessorConfig getProcessorConfig() {
        return processorConfig;
    }

    public void setProcessorConfig(TransProcessorConfig processorConfig) {
        this.processorConfig = processorConfig;
    }

    public HypurConfig getHypurConfig() {
        return hypurConfig;
    }

    public void setHypurConfig(HypurConfig hypurConfig) {
        this.hypurConfig = hypurConfig;
    }

    public String getTrackingURL() {
        return trackingURL;
    }

    public void setTrackingURL(String trackingURL) {
        this.trackingURL = trackingURL;
    }

    public String getGoogleAPIKey() {
        return googleAPIKey;
    }

    public void setGoogleAPIKey(String googleAPIKey) {
        this.googleAPIKey = googleAPIKey;
    }

    public VerificationSiteConfig getVerificationSiteConfig() {
        return verificationSiteConfig;
    }

    public void setVerificationSiteConfig(VerificationSiteConfig verificationSiteConfig) {
        this.verificationSiteConfig = verificationSiteConfig;
    }

    public QuartzConfiguration getQuartzConfiguration() {
        return quartzConfiguration;
    }

    public void setQuartzConfiguration(QuartzConfiguration quartzConfiguration) {
        this.quartzConfiguration = quartzConfiguration;
    }

    public BlazeAppType getAppType() {
        return appType;
    }

    public void setAppType(BlazeAppType appType) {
        this.appType = appType;
    }

    public String getRequiredVersion() {
        return requiredVersion;
    }

    public void setRequiredVersion(String requiredVersion) {
        this.requiredVersion = requiredVersion;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getAppWebsiteURL() {
        return appWebsiteURL;
    }

    public void setAppWebsiteURL(String appWebsiteURL) {
        this.appWebsiteURL = appWebsiteURL;
    }

    public String getAppApiUrl() {
        return appApiUrl;
    }

    public void setAppApiUrl(String appApiUrl) {
        this.appApiUrl = appApiUrl;
    }

    public String getApplePushCertName() {
        return applePushCertName;
    }

    public void setApplePushCertName(String applePushCertName) {
        this.applePushCertName = applePushCertName;
    }

    public String getApplePushCertPassword() {
        return applePushCertPassword;
    }

    public void setApplePushCertPassword(String applePushCertPassword) {
        this.applePushCertPassword = applePushCertPassword;
    }

    public String getApplePushCertPath() {
        return applePushCertPath;
    }

    public void setApplePushCertPath(String applePushCertPath) {
        this.applePushCertPath = applePushCertPath;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public String getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(String redisPort) {
        this.redisPort = redisPort;
    }

    public MongoConfiguration getMongoConfiguration() {
        return mongoConfiguration;
    }

    public void setMongoConfiguration(MongoConfiguration mongoConfiguration) {
        this.mongoConfiguration = mongoConfiguration;
    }

    public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
        return swaggerBundleConfiguration;
    }

    public void setSwaggerBundleConfiguration(SwaggerBundleConfiguration swaggerBundleConfiguration) {
        this.swaggerBundleConfiguration = swaggerBundleConfiguration;
    }


    public PusherConfig getPusherConfig() {
        return pusherConfig;
    }

    public void setPusherConfig(PusherConfig pusherConfig) {
        this.pusherConfig = pusherConfig;
    }

    public S3Config getAmazonS3Config() {
        return amazonS3Config;
    }

    public void setAmazonS3Config(S3Config amazonS3Config) {
        this.amazonS3Config = amazonS3Config;
    }

    public int getAuthTTL() {
        return authTTL;
    }

    public void setAuthTTL(int authTTL) {
        this.authTTL = authTTL;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }


    public EnvironmentType getEnv() {
        return env;
    }

    public void setEnv(EnvironmentType env) {
        this.env = env;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCoreUrls() {
        return coreUrls;
    }

    public void setCoreUrls(String coreUrls) {
        this.coreUrls = coreUrls;
    }


    public WepayConfiguration getWepayConfiguration() {
        return wepayConfiguration;
    }

    public void setWepayConfiguration(WepayConfiguration wepayConfiguration) {
        this.wepayConfiguration = wepayConfiguration;
    }

    public TwilioConfiguration getTwilioConfiguration() {
        return twilioConfiguration;
    }

    public void setTwilioConfiguration(TwilioConfiguration twilioConfiguration) {
        this.twilioConfiguration = twilioConfiguration;
    }

    public String getPlatformDNS() {
        return platformDNS;
    }

    public void setPlatformDNS(String platformDNS) {
        this.platformDNS = platformDNS;
    }

    public StripeConfiguration getStripeConfiguration() {
        return stripeConfiguration;
    }

    public void setStripeConfiguration(StripeConfiguration stripeConfiguration) {
        this.stripeConfiguration = stripeConfiguration;
    }

    public AWSElasticConfig getAwsElasticConfig() {
        return awsElasticConfig;
    }

    public void setAwsElasticConfig(AWSElasticConfig awsElasticConfig) {
        this.awsElasticConfig = awsElasticConfig;
    }

    public BlazeApplicationsConfig getBlazeApplicationsConfig() {
        return blazeApplicationsConfig;
    }

    public QuickBookConfig getQuickBookConfig() {
        return quickBookConfig;
    }

    public void setQuickBookConfig(QuickBookConfig quickBookConfig) {
        this.quickBookConfig = quickBookConfig;
    }

    public void setBlazeApplicationsConfig(BlazeApplicationsConfig blazeApplicationsConfig) {
        this.blazeApplicationsConfig = blazeApplicationsConfig;
    }

    public SendgridConfiguration getSendgridConfiguration() {
        return sendgridConfiguration;
    }

    public void setSendgridConfiguration(SendgridConfiguration sendgridConfiguration) {
        this.sendgridConfiguration = sendgridConfiguration;
    }

    public BounceEmailProcessorConfig getBounceEmailProcessorConfig() {
        return bounceEmailProcessorConfig;
    }

    public void setBounceEmailProcessorConfig(BounceEmailProcessorConfig bounceEmailProcessorConfig) {
        this.bounceEmailProcessorConfig = bounceEmailProcessorConfig;
    }

    public SpringBigConfiguration getSpringBigConfiguration() {
        return springBigConfiguration;
    }

    public void setSpringBigConfiguration(SpringBigConfiguration springBigConfiguration) {
        this.springBigConfiguration = springBigConfiguration;
    }

    public PostgreServerConfiguration getPostgreServerConfiguration() {
        return postgreServerConfiguration;
    }

    public void setPostgreServerConfiguration(PostgreServerConfiguration postgreServerConfiguration) {
        this.postgreServerConfiguration = postgreServerConfiguration;
    }

    public TookanConfiguration getTookanConfiguration() {
        return tookanConfiguration;
    }

    public void setTookanConfiguration(TookanConfiguration tookanConfiguration) {
        this.tookanConfiguration = tookanConfiguration;
    }

    public boolean isEnableReportingCache() {
        return enableReportingCache;
    }

    public void setEnableReportingCache(boolean enableReportingCache) {
        this.enableReportingCache = enableReportingCache;
    }

    public String getInternalApiSecret() {
        return internalApiSecret;
    }

    public void setInternalApiSecret(String internalApiSecret) {
        this.internalApiSecret = internalApiSecret;
    }

    public String getInternalApiIV() {
        return internalApiIV;
    }

    public void setInternalApiIV(String internalApiIV) {
        this.internalApiIV = internalApiIV;
    }
    public PlatformModeConfiguration getPlatformModeConfiguration() {
        return platformModeConfiguration;
    }

    public void setPlatformModeConfiguration(PlatformModeConfiguration platformModeConfiguration) {
        this.platformModeConfiguration = platformModeConfiguration;
    }

    public RabbitMQConfiguration getRabbitMQConfiguration() {
        return rabbitMQConfiguration;
    }

    public void setRabbitMQConfiguration(RabbitMQConfiguration rabbitMQConfiguration) {
        this.rabbitMQConfiguration = rabbitMQConfiguration;
    }

    public String getSalesAppLink() {
        return salesAppLink;
    }

    public void setSalesAppLink(String salesAppLink) {
        this.salesAppLink = salesAppLink;
    }
}
