package com.fourtwenty.core.config;

import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;

import java.util.List;

public class FcmConfig {


    private String secret;
    private String host;

    public FcmConfig() {
    }

    public FcmConfig(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {

            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.FCM.HOST:
                    host = setting.getValue();
                    break;
                case IntegrationSettingConstants.Integrations.FCM.SECRET:
                    secret = setting.getValue();
                    break;
            }
        }
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
