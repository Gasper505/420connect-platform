package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class MongoConfiguration {
    @NotEmpty
    @JsonProperty
    private String uri;

    /**
     * Returns the {@code uri} of this {@link MongoConfiguration}.
     *
     * @return Returns the {@code uri} of this {@link MongoConfiguration}.
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the {@code uri} of this {@link MongoConfiguration}.
     *
     * @param uri The {@code uri} to set.
     */
    public void setUri(String uri) {
        this.uri = uri;
    }


}
