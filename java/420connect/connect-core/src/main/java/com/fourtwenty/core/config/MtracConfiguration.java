package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MtracConfiguration {
    private String host;

    public MtracConfiguration(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {

            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.Weedmap.HOST:
                    host = setting.getValue();
                    break;
            }
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
