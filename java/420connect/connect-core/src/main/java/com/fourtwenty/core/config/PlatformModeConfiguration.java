package com.fourtwenty.core.config;

public class PlatformModeConfiguration {

    public enum PlatformMode {
        OnPrem, OnCloud
    }

    private PlatformMode mode = PlatformMode.OnCloud;
    private String companyId = "";
    private String secret = "";
    private String iv = "";
    private String cloudURL = "";
    private String premisesURL = "";
    private boolean enablePremSync = true;

    public boolean isEnablePremSync() {
        return enablePremSync;
    }

    public void setEnablePremSync(boolean enablePremSync) {
        this.enablePremSync = enablePremSync;
    }

    public PlatformMode getMode() {
        return mode;
    }

    public void setMode(PlatformMode mode) {
        this.mode = mode;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getCloudURL() {
        return cloudURL;
    }

    public void setCloudURL(String cloudURL) {
        this.cloudURL = cloudURL;
    }

    public String getPremisesURL() {
        return premisesURL;
    }

    public void setPremisesURL(String premisesURL) {
        this.premisesURL = premisesURL;
    }
}
