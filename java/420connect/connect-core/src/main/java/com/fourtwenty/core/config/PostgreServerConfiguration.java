package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PostgreServerConfiguration {

    @JsonProperty(value = "host")
    private String host = "localhost";

    @JsonProperty(value = "port")
    private Integer port = 5432;

    @JsonProperty(value = "dbname")
    private String dbName = "reporting_dev";

    @JsonProperty(value = "user")
    private String user = "postgres";

    @JsonProperty(value = "password")
    private String secret = "postgres";

    @JsonProperty(value = "poolName")
    private String poolName = "blaze-reporting-db-pool";

    @JsonProperty(value = "jdbcUrl")
    private String jdbcUrl = "jdbc:postgresql://";

    @JsonProperty(value = "maximumPoolSize")
    private int maximumPoolSize = 10;

    @JsonProperty(value = "minimumIdle")
    private int minimumIdle = 2;

    @JsonProperty(value = "cachePrepStmts")
    private boolean cachePrepStmts = Boolean.TRUE;

    @JsonProperty(value = "prepStmtCacheSize")
    private int prepStmtCacheSize = 256;

    @JsonProperty(value = "prepStmtCacheSqlLimit")
    private int prepStmtCacheSqlLimit = 2048;

    @JsonProperty(value = "useServerPrepStmts")
    private boolean useServerPrepStmts = Boolean.TRUE;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getMinimumIdle() {
        return minimumIdle;
    }

    public void setMinimumIdle(int minimumIdle) {
        this.minimumIdle = minimumIdle;
    }

    public boolean isCachePrepStmts() {
        return cachePrepStmts;
    }

    public void setCachePrepStmts(boolean cachePrepStmts) {
        this.cachePrepStmts = cachePrepStmts;
    }

    public int getPrepStmtCacheSize() {
        return prepStmtCacheSize;
    }

    public void setPrepStmtCacheSize(int prepStmtCacheSize) {
        this.prepStmtCacheSize = prepStmtCacheSize;
    }

    public int getPrepStmtCacheSqlLimit() {
        return prepStmtCacheSqlLimit;
    }

    public void setPrepStmtCacheSqlLimit(int prepStmtCacheSqlLimit) {
        this.prepStmtCacheSqlLimit = prepStmtCacheSqlLimit;
    }

    public boolean isUseServerPrepStmts() {
        return useServerPrepStmts;
    }

    public void setUseServerPrepStmts(boolean useServerPrepStmts) {
        this.useServerPrepStmts = useServerPrepStmts;
    }
}
