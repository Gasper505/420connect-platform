package com.fourtwenty.core.config;

/**
 * The PusherConfig object.
 * User: mdo
 * Date: 12/31/13
 * Time: 10:57 PM
 */
public class PusherConfig {
    private String appId;
    private String authKey;
    private String authSecret;
    private String channelPrefix;

    public String getAppId() {
        return appId;
    }

    public String getChannelPrefix() {
        return channelPrefix;
    }

    public void setChannelPrefix(String channelPrefix) {
        this.channelPrefix = channelPrefix;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getAuthSecret() {
        return authSecret;
    }

    public void setAuthSecret(String authSecret) {
        this.authSecret = authSecret;
    }
}
