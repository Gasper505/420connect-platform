package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuickBookConfig {

    @JsonProperty("IntuitAccountingAPIHost")
    private String intuitAccountingAPIHost;

    @JsonProperty("clientID")
    private String clientID;

    @JsonProperty("clientSecret")
    private String clientSecret;

    @JsonProperty("scope")
    private String scope;

    @JsonProperty("redirect_URI")
    private String redirectURI;

    @JsonProperty("state")
    private String state;

    @JsonProperty("sanbox_URI")
    private String sanboxURI;

    @JsonProperty("name")
    private String name;

    @JsonProperty("refreshToken_URI")
    private String refreshTokenURI;

    @JsonProperty("revokeToken_URI")
    private String revokeTokenURI;

    @JsonProperty("base_url")
    private String baseUrl;

    @JsonProperty("enable")
    private boolean enabled = false;

    @JsonProperty("desktopSOAPAddress")
    private String desktopSOAPAddress;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getIntuitAccountingAPIHost() {
        return intuitAccountingAPIHost;
    }

    public void setIntuitAccountingAPIHost(String intuitAccountingAPIHost) {
        this.intuitAccountingAPIHost = intuitAccountingAPIHost;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getRedirectURI() {
        return redirectURI;
    }

    public void setRedirectURI(String redirectURI) {
        this.redirectURI = redirectURI;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSanboxURI() {
        return sanboxURI;
    }

    public void setSanboxURI(String sanboxURI) {
        this.sanboxURI = sanboxURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefreshTokenURI() {
        return refreshTokenURI;
    }

    public void setRefreshTokenURI(String refreshTokenURI) {
        this.refreshTokenURI = refreshTokenURI;
    }

    public String getRevokeTokenURI() {
        return revokeTokenURI;
    }

    public void setRevokeTokenURI(String revokeTokenURI) {
        this.revokeTokenURI = revokeTokenURI;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getDesktopSOAPAddress() {
        return desktopSOAPAddress;
    }

    public void setDesktopSOAPAddress(String desktopSOAPAddress) {
        this.desktopSOAPAddress = desktopSOAPAddress;
    }
}