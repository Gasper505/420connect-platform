package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The S3Config object.
 * User: mdo
 * Date: 11/1/13
 * Time: 2:58 AM
 */
public class S3Config extends SocialConfiguration {
    @JsonProperty("bucketName")
    private String bucketName;
    @JsonProperty("publicBucketName")
    private String publicBucketName = "connect-files-public";

    @JsonProperty("host")
    private String host;

    @JsonProperty("endpoint")
    private String endpoint;

    @JsonProperty("region")
    private String region;

    @JsonProperty("serviceName")
    private String serviceName;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getPublicBucketName() {
        return publicBucketName;
    }

    public void setPublicBucketName(String publicBucketName) {
        this.publicBucketName = publicBucketName;
    }

    @JsonIgnore
    public String getFilePath(String fileName) {
        return String.format("http://%s.%s/%s", bucketName, host, fileName);
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
