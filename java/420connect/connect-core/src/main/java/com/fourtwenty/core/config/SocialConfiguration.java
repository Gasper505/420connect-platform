package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SocialConfiguration {
    @JsonProperty
    private String apiKey;
    @JsonProperty
    private String apiSecret;

    /**
     * Returns the {@code apiKey} of this {@link SocialConfiguration}.
     *
     * @return Returns the {@code apiKey} of this {@link SocialConfiguration}.
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * Sets the {@code apiKey} of this {@link SocialConfiguration}.
     *
     * @param apiKey The {@code apiKey} to set.
     */
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * Returns the {@code apiSecret} of this {@link SocialConfiguration}.
     *
     * @return Returns the {@code apiSecret} of this {@link SocialConfiguration}.
     */
    public String getApiSecret() {
        return apiSecret;
    }

    /**
     * Sets the {@code apiSecret} of this {@link SocialConfiguration}.
     *
     * @param apiSecret The {@code apiSecret} to set.
     */
    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }


}
