package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpringBigConfiguration {

    private String apiKey;
    private String stageEndPoint;
    private String prodEndPoint;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getProdEndPoint() {
        return prodEndPoint;
    }

    public void setProdEndPoint(String prodEndPoint) {
        this.prodEndPoint = prodEndPoint;
    }


    public String getStageEndPoint() {
        return stageEndPoint;
    }

    public void setStageEndPoint(String stageEndPoint) {
        this.stageEndPoint = stageEndPoint;
    }
}
