package com.fourtwenty.core.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 10/13/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransProcessorConfig {
    public static final String QUEUE_NAME = "TransProcessorFifo";

    public enum TransProcessorType {
        SQSQueue,
        Local,
        RabbitMQ
    }

    @JsonProperty("enabled")
    private boolean enabled = true;
    @JsonProperty("processorType")
    private TransProcessorType processorType = TransProcessorType.SQSQueue;
    @JsonProperty("queueName")
    private String queueName = QUEUE_NAME;
    @JsonProperty("canRepublish")
    private boolean canRepublish = true;

    public boolean isCanRepublish() {
        return canRepublish;
    }

    public void setCanRepublish(boolean canRepublish) {
        this.canRepublish = canRepublish;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public TransProcessorType getProcessorType() {
        return processorType;
    }

    public void setProcessorType(TransProcessorType processorType) {
        this.processorType = processorType;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
}
