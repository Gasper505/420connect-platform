package com.fourtwenty.core.config;

/**
 * Created on 11/9/17 3:44 PM by Raja Dushyant Vashishtha
 * Sr. Software Engineer
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */

public class WepayConfiguration {
    private long accountId;
    private String clientSecret;
    private String accessToken;
    private long clientId;
    private Boolean stageEnabled;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public Boolean getStageEnabled() {
        return stageEnabled;
    }

    public void setStageEnabled(Boolean stageEnabled) {
        this.stageEnabled = stageEnabled;
    }

}
