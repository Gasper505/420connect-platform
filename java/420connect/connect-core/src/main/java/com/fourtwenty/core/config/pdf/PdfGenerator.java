package com.fourtwenty.core.config.pdf;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.util.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;

public final class PdfGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfGenerator.class);

    public static byte[] exportToPdfBox(final String body, final String resourceUri) {
        try {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(body, PdfGenerator.class.getResource(resourceUri).toString());
            builder.toStream(outputStream);
            builder.run();
            return outputStream.toByteArray();
        } catch (Exception e) {
            LOGGER.error("Error while generating PDF", e);
        }
        return getErrorPDF();
    }

    private static byte[] getErrorPDF() {
        try {

            byte[] htmlBytes = IOUtils
                    .toByteArray(PdfGenerator.class.getResourceAsStream("/pdf-error.html"));
            String html = new String(htmlBytes, Charsets.UTF_8);

            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(html, PdfGenerator.class.getResource("/pdf-error.html").toString());
            builder.toStream(outputStream);
            builder.run();
            return outputStream.toByteArray();
        } catch (Exception e) {
            LOGGER.error("Error while generating PDF", e);
        }
        return null;
    }

    public static byte[] getNoDocumentPdf() {
        try {

            byte[] htmlBytes = IOUtils
                    .toByteArray(PdfGenerator.class.getResourceAsStream("/doc-not-found.html"));
            String html = new String(htmlBytes, Charsets.UTF_8);

            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(html, PdfGenerator.class.getResource("/doc-not-found.html").toString());
            builder.toStream(outputStream);
            builder.run();
            return outputStream.toByteArray();
        } catch (Exception e) {
            LOGGER.error("Error while generating PDF", e);
        }
        return null;
    }

}
