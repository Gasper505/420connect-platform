package com.fourtwenty.core.domain.db;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

/**
 * Created by mdo on 8/31/15.
 */
public interface MongoDb {
    MongoCollection getJongoCollection(String collectionName) throws Exception;

    DBCollection getDBCollection(String collectionName) throws Exception;

    Jongo getJongo() throws Exception;

    DB getDB() throws Exception;
}
