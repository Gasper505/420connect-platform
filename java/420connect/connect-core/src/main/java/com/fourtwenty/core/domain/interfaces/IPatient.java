package com.fourtwenty.core.domain.interfaces;

import com.fourtwenty.core.domain.models.company.ConsumerType;

/**
 * Created by mdo on 1/10/18.
 */
public interface IPatient {
    ConsumerType getConsumerType();
}
