package com.fourtwenty.core.domain.interfaces;

public interface InternalAllowable {

    void setExternalId(String externalId);

    String getExternalId();
}
