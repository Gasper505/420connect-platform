package com.fourtwenty.core.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.payment.PaymentProvider;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by mdo on 7/12/16.
 */
@CollectionName(name = "app", uniqueIndexes = {"{name:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class App extends BaseModel implements OnPremSyncable {

    public enum EmailSender {
        SES, SENDGRID
    }

    public static final String BLAZE_APP_NAME = "Connect App";
    private String name;
    private int version;
    private String reqIOSVersion = "2.1.1";
    private String iosVersionUpdateMsg = "We've released new critical features. A new update is required.";
    private boolean enableSMS = false;
    private boolean fakeSMS = true;
    private boolean fakeEmail = true;
    private boolean fakeTwilio = true;
    private boolean enableHeadset = false;

    private PaymentProvider defaultSmsPaymentProvider = PaymentProvider.STRIPE;

    private long lowInventoryLastSent = 0;
    private String twilioPhoneNumber = "14159805158";
    private int defaultTaxRoundOff = 2;
    private EmailSender emailSender = EmailSender.SES;
    private boolean enableMemberElasticSearch = false;
    private boolean enableTransactionElasticSearch = false;
    private boolean enableProductElasticSearch = false;
    private boolean enableProductBatchElasticSearch = false;
    private boolean enableQBDesktop = false;
    private boolean enableQBOnline = false;
    private boolean sendShopSms = false;
    private int maxReportDuration = 60; // 60 days
    private int numOfQueues = 2;
    private boolean enableV2WeedmapSync = false;
    private HashMap<String,Boolean> queueServers = new HashMap<>();
    private HashMap<String,Boolean> weedmapsBetaTesters = new HashMap<>();

    public boolean isEnableV2WeedmapSync() {
        return enableV2WeedmapSync;
    }

    public void setEnableV2WeedmapSync(boolean enableV2WeedmapSync) {
        this.enableV2WeedmapSync = enableV2WeedmapSync;
    }

    public HashMap<String, Boolean> getWeedmapsBetaTesters() {
        return weedmapsBetaTesters;
    }

    public void setWeedmapsBetaTesters(HashMap<String, Boolean> weedmapsBetaTesters) {
        this.weedmapsBetaTesters = weedmapsBetaTesters;
    }

    public HashMap<String, Boolean> getQueueServers() {
        return queueServers;
    }

    public void setQueueServers(HashMap<String, Boolean> queueServers) {
        this.queueServers = queueServers;
    }

    public boolean isEnableQBDesktop() {
        return enableQBDesktop;
    }

    public void setEnableQBDesktop(boolean enableQBDesktop) {
        this.enableQBDesktop = enableQBDesktop;
    }

    public boolean isEnableQBOnline() {
        return enableQBOnline;
    }

    public void setEnableQBOnline(boolean enableQBOnline) {
        this.enableQBOnline = enableQBOnline;
    }

    public int getNumOfQueues() {
        return numOfQueues;
    }

    public void setNumOfQueues(int numOfQueues) {
        this.numOfQueues = numOfQueues;
    }

    public int getMaxReportDuration() {
        return maxReportDuration;
    }

    public void setMaxReportDuration(int maxReportDuration) {
        this.maxReportDuration = maxReportDuration;
    }

    public int getDefaultTaxRoundOff() {
        return defaultTaxRoundOff;
    }

    public void setDefaultTaxRoundOff(int defaultTaxRoundOff) {
        this.defaultTaxRoundOff = defaultTaxRoundOff;
    }

    public long getLowInventoryLastSent() {
        return lowInventoryLastSent;
    }

    public void setLowInventoryLastSent(long lowInventoryLastSent) {
        this.lowInventoryLastSent = lowInventoryLastSent;
    }

    public boolean isEnableHeadset() {
        return enableHeadset;
    }

    public void setEnableHeadset(boolean enableHeadset) {
        this.enableHeadset = enableHeadset;
    }

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal smsCost = new BigDecimal(2); // in cents


    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal mmsCost = new BigDecimal(4); // in cents


    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal emailCost = new BigDecimal(.5); // in cents

    public BigDecimal getMmsCost() {
        return mmsCost;
    }

    public void setMmsCost(BigDecimal mmsCost) {
        this.mmsCost = mmsCost;
    }

    public BigDecimal getEmailCost() {
        return emailCost;
    }

    public void setEmailCost(BigDecimal emailCost) {
        this.emailCost = emailCost;
    }

    public boolean isFakeEmail() {
        return fakeEmail;
    }

    public void setFakeEmail(boolean fakeEmail) {
        this.fakeEmail = fakeEmail;
    }

    public boolean isFakeSMS() {
        return fakeSMS;
    }

    public void setFakeSMS(boolean fakeSMS) {
        this.fakeSMS = fakeSMS;
    }

    public static String getBlazeAppName() {
        return BLAZE_APP_NAME;
    }

    public boolean isEnableSMS() {
        return enableSMS;
    }

    public void setEnableSMS(boolean enableSMS) {
        this.enableSMS = enableSMS;
    }

    public BigDecimal getSmsCost() {
        return smsCost;
    }

    public void setSmsCost(BigDecimal smsCost) {
        this.smsCost = smsCost;
    }

    public String getIosVersionUpdateMsg() {
        return iosVersionUpdateMsg;
    }

    public void setIosVersionUpdateMsg(String iosVersionUpdateMsg) {
        this.iosVersionUpdateMsg = iosVersionUpdateMsg;
    }

    public String getReqIOSVersion() {
        return reqIOSVersion;
    }

    public void setReqIOSVersion(String reqIOSVersion) {
        this.reqIOSVersion = reqIOSVersion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isFakeTwilio() {
        return fakeTwilio;
    }

    public void setFakeTwilio(boolean fakeTwilio) {
        this.fakeTwilio = fakeTwilio;
    }

    public String getTwilioPhoneNumber() {
        return twilioPhoneNumber;
    }

    public void setTwilioPhoneNumber(String twilioPhoneNumber) {
        this.twilioPhoneNumber = twilioPhoneNumber;
    }

    public PaymentProvider getDefaultSmsPaymentProvider() {
        return defaultSmsPaymentProvider;
    }

    public void setDefaultSmsPaymentProvider(PaymentProvider defaultSmsPaymentProvider) {
        this.defaultSmsPaymentProvider = defaultSmsPaymentProvider;
    }

    public EmailSender getEmailSender() {
        return emailSender;
    }

    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    public boolean isEnableMemberElasticSearch() {
        return enableMemberElasticSearch;
    }

    public void setEnableMemberElasticSearch(boolean enableMemberElasticSearch) {
        this.enableMemberElasticSearch = enableMemberElasticSearch;
    }

    public boolean isEnableTransactionElasticSearch() {
        return enableTransactionElasticSearch;
    }

    public void setEnableTransactionElasticSearch(boolean enableTransactionElasticSearch) {
        this.enableTransactionElasticSearch = enableTransactionElasticSearch;
    }

    public boolean isEnableProductElasticSearch() {
        return enableProductElasticSearch;
    }

    public void setEnableProductElasticSearch(boolean enableProductElasticSearch) {
        this.enableProductElasticSearch = enableProductElasticSearch;
    }

    public boolean isSendShopSms() {
        return sendShopSms;
    }

    public void setSendShopSms(boolean sendShopSms) {
        this.sendShopSms = sendShopSms;
    }

    public boolean isEnableProductBatchElasticSearch() {
        return enableProductBatchElasticSearch;
    }

    public void setEnableProductBatchElasticSearch(boolean enableProductBatchElasticSearch) {
        this.enableProductBatchElasticSearch = enableProductBatchElasticSearch;
    }
}
