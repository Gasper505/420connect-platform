package com.fourtwenty.core.domain.models.checkin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "checkIn", indexes = {"{companyId:1,shopId:1,delete:1},{companyId:1,shopId:1,checkInTime:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckIn extends ShopBaseModel {
    public enum CheckInStatus {
        InProgress,
        Accepted,
        Declined,
        AddedToQueue
    }

    public static final String UNIQUE_SEQ_PRIORITY = "CheckIn";
    private String memberId;
    private long checkInTime;
    private String approvedBy;
    private long approvedDate;
    private String declinedBy;
    private long declineDate;
    private boolean active = true;
    private String employeeId;
    private CheckInStatus status = CheckInStatus.InProgress;
    private String transactionId;
    private String checkInNo;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public long getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(long approvedDate) {
        this.approvedDate = approvedDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public CheckInStatus getStatus() {
        return status;
    }

    public void setStatus(CheckInStatus status) {
        this.status = status;
    }

    public String getDeclinedBy() {
        return declinedBy;
    }

    public void setDeclinedBy(String declinedBy) {
        this.declinedBy = declinedBy;
    }

    public long getDeclineDate() {
        return declineDate;
    }

    public void setDeclineDate(long declineDate) {
        this.declineDate = declineDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCheckInNo() {
        return checkInNo;
    }

    public void setCheckInNo(String checkInNo) {
        this.checkInNo = checkInNo;
    }


}
