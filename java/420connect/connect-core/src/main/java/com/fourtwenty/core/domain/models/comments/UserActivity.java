package com.fourtwenty.core.domain.models.comments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;


@CollectionName(name = "user_activity", indexes = {"{referenceId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserActivity extends ShopBaseModel {
    public enum ActivityType {
        COMMENT,
        ACTION
    }

    public enum Action {
        ADD,
        EDIT,
        DELETE
    }

    public enum ReferenceType {
        INVOICE,
        EXPENSE,
        INCOMING_BATCH,
        PO
    }

    private String employeeId;
    private String message;
    private ReferenceType referenceType;
    private String referenceId;
    private ActivityType activityType;
    private Action action;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReferenceType getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(ReferenceType referenceType) {
        this.referenceType = referenceType;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
