package com.fourtwenty.core.domain.models.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "background_tasks", indexes = {"{companyId:1,shopId:1,modified:1,status:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BackgroundTask extends ShopBaseModel {
    public enum TaskType {
        None,
        DailySnapshot,
        DailyEmailSummary
    }

    public enum TaskStatus {
        Pending,
        InProgress,
        Completed,
        Error
    }

    private TaskType type = TaskType.None;
    private long targetRunDate = 0;
    private TaskStatus status = TaskStatus.Pending;
    private String employeeId;
    private long requestDate = 0;
    private long completedTime = 0;

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public long getTargetRunDate() {
        return targetRunDate;
    }

    public void setTargetRunDate(long targetRunDate) {
        this.targetRunDate = targetRunDate;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(long requestDate) {
        this.requestDate = requestDate;
    }

    public long getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(long completedTime) {
        this.completedTime = completedTime;
    }
}
