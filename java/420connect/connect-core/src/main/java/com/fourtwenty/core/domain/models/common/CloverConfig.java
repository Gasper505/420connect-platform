package com.fourtwenty.core.domain.models.common;

import java.util.List;

public class CloverConfig {
    private String host;

    public CloverConfig(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {
            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.Clover.HOST:
                    host = setting.getValue();
                    break;
            }
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
