package com.fourtwenty.core.domain.models.common;

import java.util.List;

public class HeadsetConfig {

    private String host;
    private String partner;
    private String partnerKey;
    private int syncInterval = 600; // 10 minutes

    public HeadsetConfig(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {
            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.Headset.HOST:
                    host = setting.getValue();
                    break;

                case IntegrationSettingConstants.Integrations.Headset.PARTNER:
                    partner = setting.getValue();
                    break;

                case IntegrationSettingConstants.Integrations.Headset.PARTNER_KEY:
                    partnerKey = setting.getValue();
                    break;

                case IntegrationSettingConstants.Integrations.Headset.SYNC_INTERVAL:
                    syncInterval = setting.getValueInt();
                    break;
            }
        }
    }

    public String getHost() {
        return host;
    }

    public String getPartner() {
        return partner;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public int getSyncInterval() {
        return syncInterval;
    }
}
