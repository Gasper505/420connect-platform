package com.fourtwenty.core.domain.models.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.BaseModel;

@CollectionName(name = "integration_settings", uniqueIndexes = {"{integration:1, environment:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntegrationSetting extends BaseModel implements OnPremSyncable {
    public enum Environment {
        Development,
        Production
    }

    private String integration;
    private Environment environment;
    private String key;
    private String value;

    public IntegrationSetting() {
    }

    public IntegrationSetting(String integration, Environment environment, String key, String value) {
        this.integration = integration;
        this.environment = environment;
        this.key = key;
        this.value = value;
    }

    public String getIntegration() {
        return integration;
    }

    public void setIntegration(String integration) {
        this.integration = integration;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @JsonIgnore
    public int getValueInt() {
        return Integer.parseInt(value);
    }
}
