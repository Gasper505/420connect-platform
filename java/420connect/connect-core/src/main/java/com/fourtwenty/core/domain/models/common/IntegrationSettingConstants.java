package com.fourtwenty.core.domain.models.common;

public class IntegrationSettingConstants {
    public static class Integrations {
        public static class Headset {
            public static final String NAME = "Headset";
            // Changed for integration settings constants after updates done through reformatting
            public static final String SYNC_INTERVAL = "SyncInterval";
            public static final String HOST = "Host";
            public static final String PARTNER = "Partner";
            public static final String PARTNER_KEY = "PartnerKey";
        }

        public static class Linx {
            public static final String NAME = "Linx";
            // Changed for integration settings constants after updates done through reformatting
            public static final String HOST = "Host";
        }

        public static class Weedmap {
            public static final String NAME = "Weedmap2";
            // Changed for integration settings constants after updates done through reformatting
            public static final String HOST = "Host";
            public static final String CLIENT_ID = "CLIENT_ID";
            public static final String CLIENT_SECRET = "CLIENT_SECRET";
            public static final String REDIRECT_URL = "REDIRECT_URL";

        }

        public static class Clover {
            public static final String NAME = "Clover";
            public static final String HOST = "Host";

        }

        public static class Mtrac {
            public static final String NAME = "Mtrac";
            public static final String HOST = "Host";

        }

        public static class Metrc {
            public static String makeName(MetrcState state) {
                return String.format("Metrc-%s", state.name());
            }

            // Changed for integration settings constants after updates done through reformatting
            public static final String HOST = "Host";
            public static final String VENDOR_KEY = "VendorKey";

            public enum MetrcState {
                Alaska("AK"),
                California("CA"),
                Colorado("CO"),
                Maryland("MA"),
                Michigan("MI"),
                Nevada("NV"),
                Oregon("OR");


                MetrcState(String stateCode) {
                    this.stateCode = stateCode;
                }

                public String stateCode;

                public String getStateCode() {
                    return this.stateCode;
                }

                public static MetrcState fromPrefix(String text) {
                    for (MetrcState state : MetrcState.values()) {
                        if (state.stateCode.equalsIgnoreCase(text)) {
                            return state;
                        }
                    }
                    return null;
                }
            }
        }

        //FCM Integration Setting
        public static class FCM {
            public static final String NAME = "Fcm";
            // Changed for integration settings constants after updates done through reformatting
            public static final String HOST = "Host";
            public static final String SECRET = "Secret";


        }
    }
}
