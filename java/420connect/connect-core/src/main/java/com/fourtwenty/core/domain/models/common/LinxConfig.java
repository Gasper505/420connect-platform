package com.fourtwenty.core.domain.models.common;

import java.util.List;

public class LinxConfig {

    private String host;

    public LinxConfig(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {
            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.Headset.HOST:
                    host = setting.getValue();
                    break;
            }
        }
    }

    public String getHost() {
        return host;
    }

    @Override
    public String toString() {
        return "LinxConfig{" +
                "host='" + host + '\'' +
                '}';
    }
}
