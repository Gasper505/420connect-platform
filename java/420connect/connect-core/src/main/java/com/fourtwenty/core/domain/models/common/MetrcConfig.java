package com.fourtwenty.core.domain.models.common;

import java.util.List;

public class MetrcConfig {

    private String host;
    private String vendorKey;

    public MetrcConfig(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {

            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.Metrc.HOST:
                    host = setting.getValue();
                    break;
                case IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY:
                    vendorKey = setting.getValue();
                    break;
            }

        }
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getVendorKey() {
        return vendorKey;
    }

    public void setVendorKey(String vendorKey) {
        this.vendorKey = vendorKey;
    }
}
