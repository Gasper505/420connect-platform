package com.fourtwenty.core.domain.models.common;

import java.util.List;

public class WeedmapConfig {
    private String host;
    private String secret;
    private String clientId;
    private String redirectURL;


    public WeedmapConfig() {
    }

    public WeedmapConfig(List<IntegrationSetting> settings) {
        for (IntegrationSetting setting : settings) {

            switch (setting.getKey()) {
                case IntegrationSettingConstants.Integrations.Weedmap.HOST:
                    host = setting.getValue();
                    break;
                case IntegrationSettingConstants.Integrations.Weedmap.CLIENT_SECRET:
                    secret = setting.getValue();
                    break;
                case IntegrationSettingConstants.Integrations.Weedmap.CLIENT_ID:
                    clientId = setting.getValue();
                    break;
                case IntegrationSettingConstants.Integrations.Weedmap.REDIRECT_URL:
                    redirectURL = setting.getValue();
                    break;
            }
        }
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }
}
