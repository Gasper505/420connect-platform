package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
@CollectionName(name = "adjustments", indexes = {"{companyId:1,shopId:1,delete:1}"})
public class Adjustment extends ShopBaseModel {

    public enum AdjustmentType {
        TAX,
        FEE,
        DISCOUNT,
        OTHER
    }

    @NotEmpty
    private String name;
    private boolean active = Boolean.FALSE;
    private AdjustmentType type = AdjustmentType.OTHER;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public AdjustmentType getType() {
        return type;
    }

    public void setType(AdjustmentType type) {
        this.type = type;
    }
}
