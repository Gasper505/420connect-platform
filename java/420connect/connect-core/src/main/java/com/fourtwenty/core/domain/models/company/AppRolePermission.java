package com.fourtwenty.core.domain.models.company;

import com.fourtwenty.core.domain.annotations.CollectionName;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.LinkedHashSet;

@JsonIgnoreProperties(ignoreUnknown = true)
@CollectionName(name = "app_role_permissions", uniqueIndexes = {"{companyId:1,appTarget:1}"})
public class AppRolePermission extends CompanyBaseModel {

    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Retail;
    private LinkedHashSet<String> roleList;

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }

    public LinkedHashSet<String> getRoleList() {
        return roleList;
    }

    public void setRoleList(LinkedHashSet<String> roleList) {
        this.roleList = roleList;
    }
}
