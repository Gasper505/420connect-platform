package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 7/13/16.
 */
@CollectionName(name = "audit_logs", premSyncDown = false, indexes = {"{companyId:1,shopId:1}", "{companyId:1,shopId:1,terminalId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuditLog extends ShopBaseModel {
    private String apiPath;
    private String employeeId;
    private String category;
    private String action;
    private String terminalId;
    private String deviceId;
    private String timeCardId;
    private String tokenId;
    private String employeeSessionId;
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getApiPath() {
        return apiPath;
    }

    public void setApiPath(String apiPath) {
        this.apiPath = apiPath;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEmployeeSessionId() {
        return employeeSessionId;
    }

    public void setEmployeeSessionId(String employeeSessionId) {
        this.employeeSessionId = employeeSessionId;
    }

    public String getTimeCardId() {
        return timeCardId;
    }

    public void setTimeCardId(String timeCardId) {
        this.timeCardId = timeCardId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
