package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 2/28/17.
 */
@CollectionName(name = "barcode_items", uniqueIndexes = {"{companyId:1,shopId:1,barcode:1,entityType:1}"}, indexes = {"{companyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BarcodeItem extends ShopBaseModel {
    public enum BarcodeEntityType {
        Product("PRO"),
        Batch("BAT"),
        Prepackage("PRE");

        BarcodeEntityType(String code) {
            this.code = code;
        }

        String code;

        public String getCode() {
            return this.code;
        }
    }

    private String productId;
    private String entityId;
    private BarcodeEntityType entityType;
    private String barcode; // unique per company
    private int number = 0;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public BarcodeEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(BarcodeEntityType entityType) {
        this.entityType = entityType;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
