package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

/**
 * Created by mdo on 8/8/16.
 */
@CollectionName(name = "bounced_emails", uniqueIndexes = {"{email:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BouncedEmail extends BaseModel {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
