package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

@CollectionName(name = "bounced_numbers", uniqueIndexes = {"{number:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BouncedNumber extends BaseModel {

    private String number;

    private String reason;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
