package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CartMinimumDetails implements Comparable<CartMinimumDetails> {

    private BigDecimal minimum;
    private Set<String> zipCodes;
    private BigDecimal distance;
    private Boolean enabled = Boolean.TRUE;

    public BigDecimal getMinimum() {
        return minimum;
    }

    public void setMinimum(BigDecimal minimum) {
        this.minimum = minimum;
    }

    public Set<String> getZipCodes() {
        return zipCodes;
    }

    public void setZipCodes(Set<String> zipCodes) {
        this.zipCodes = zipCodes;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int compareTo(CartMinimumDetails o) {
        if (!this.getEnabled()) {
            return 1;
        }
        if (!o.getEnabled()) {
            return -1;
        }
        return this.getMinimum().compareTo(o.getMinimum());
    }
}
