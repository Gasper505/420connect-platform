package com.fourtwenty.core.domain.models.company;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.SortedSet;
import java.util.TreeSet;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CartMinimums extends CompanyBaseModel {

    public enum MinimumsType {
        SIMPLE,
        DISTANCE,
        ZIP_CODE
    }

    private MinimumsType minimumsType = MinimumsType.SIMPLE;
    private SortedSet<CartMinimumDetails> cartMinimumDetails = new TreeSet<>();

    public MinimumsType getMinimumsType() {
        return minimumsType;
    }

    public void setMinimumsType(MinimumsType minimumsType) {
        this.minimumsType = minimumsType;
    }

    public SortedSet<CartMinimumDetails> getCartMinimumDetails() {
        return cartMinimumDetails;
    }

    public void setCartMinimumDetails(SortedSet<CartMinimumDetails> cartMinimumDetails) {
        this.cartMinimumDetails = cartMinimumDetails;
    }
}
