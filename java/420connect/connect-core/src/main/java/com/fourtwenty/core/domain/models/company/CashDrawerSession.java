package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.domain.serializers.PaymentOptionEnumSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by mdo on 12/7/16.
 */
@CollectionName(name = "cashdrawer_sessions", uniqueIndexes = {"{companyId:1,shopId:1, terminalId:1, date:-1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CashDrawerSession extends ShopBaseModel implements Comparable<CashDrawerSession> {
    public enum CashDrawerLogStatus {
        Open,
        Closed
    }

    private String terminalId;
    private String date;
    private long dateTimestamp;
    private long startTime; // utc start time
    private String startEmployeeId;
    private String endEmployeeId;
    private long endTime; // utc end time
    private CashDrawerLogStatus status = CashDrawerLogStatus.Open;
    private String comment = "";
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amtBeforeCarryOver = new BigDecimal(0); // Calculated based on day sales

    // Sales
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cashSales = new BigDecimal(0); // Calculated based on day sales

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal checkSales = new BigDecimal(0); // Calculated based on day sales

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal creditSales = new BigDecimal(0); // Calculated based on day sales

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal storeCreditSales = new BigDecimal(0); // Calculated based on day sales


    // Refunds
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cashRefunds = new BigDecimal(0); // Calculated based on day refunds

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal checkRefunds = new BigDecimal(0); // Calculated based on day refunds

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal creditRefunds = new BigDecimal(0); // Calculated based on day refunds
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal storeCreditRefunds = new BigDecimal(0); // Calculated based on day refunds

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalRefunds = new BigDecimal(0); // Calculated based on day refunds


    // Cash Specific
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal expectedCash = new BigDecimal(0); // Formula: startCash + cashSales + checkSales + cashIn - cashOut
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal startCash = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal endCash = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal actualCash = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCashIn = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCashOut = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCashDrop = new BigDecimal(0);


    @JsonSerialize(
            keyUsing = PaymentOptionEnumSerializer.class,
            contentUsing = BigDecimalTwoDigitsSerializer.class)
    private HashMap<Cart.PaymentOption, BigDecimal> customSalesTotals = new HashMap<>();
    @JsonSerialize(
            keyUsing = PaymentOptionEnumSerializer.class,
            contentUsing = BigDecimalTwoDigitsSerializer.class)
    private HashMap<Cart.PaymentOption, BigDecimal> customRefundTotals = new HashMap<>();

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCashlessAtmSales = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCashlessAtmRefunds = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCashlessAtmChange = new BigDecimal(0);

    public BigDecimal getCheckRefunds() {
        return checkRefunds;
    }

    public void setCheckRefunds(BigDecimal checkRefunds) {
        this.checkRefunds = checkRefunds;
    }

    public BigDecimal getCreditRefunds() {
        return creditRefunds;
    }

    public void setCreditRefunds(BigDecimal creditRefunds) {
        this.creditRefunds = creditRefunds;
    }

    public BigDecimal getTotalRefunds() {
        return totalRefunds;
    }

    public void setTotalRefunds(BigDecimal totalRefunds) {
        this.totalRefunds = totalRefunds;
    }

    public BigDecimal getAmtBeforeCarryOver() {
        return amtBeforeCarryOver;
    }

    public void setAmtBeforeCarryOver(BigDecimal amtBeforeCarryOver) {
        this.amtBeforeCarryOver = amtBeforeCarryOver;
    }

    public BigDecimal getActualCash() {
        return actualCash;
    }

    public void setActualCash(BigDecimal actualCash) {
        this.actualCash = actualCash;
    }

    public BigDecimal getCashRefunds() {
        return cashRefunds;
    }

    public void setCashRefunds(BigDecimal cashRefunds) {
        this.cashRefunds = cashRefunds;
    }

    public BigDecimal getCashSales() {
        return cashSales;
    }

    public void setCashSales(BigDecimal cashSales) {
        this.cashSales = cashSales;
    }

    public BigDecimal getCheckSales() {
        return checkSales;
    }

    public void setCheckSales(BigDecimal checkSales) {
        this.checkSales = checkSales;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getCreditSales() {
        return creditSales;
    }

    public void setCreditSales(BigDecimal creditSales) {
        this.creditSales = creditSales;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getEndCash() {
        return endCash;
    }

    public void setEndCash(BigDecimal endCash) {
        this.endCash = endCash;
    }

    public String getEndEmployeeId() {
        return endEmployeeId;
    }

    public void setEndEmployeeId(String endEmployeeId) {
        this.endEmployeeId = endEmployeeId;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getExpectedCash() {
        return expectedCash;
    }

    public void setExpectedCash(BigDecimal expectedCash) {
        this.expectedCash = expectedCash;
    }

    public BigDecimal getStartCash() {
        return startCash;
    }

    public void setStartCash(BigDecimal startCash) {
        this.startCash = startCash;
    }

    public String getStartEmployeeId() {
        return startEmployeeId;
    }

    public void setStartEmployeeId(String startEmployeeId) {
        this.startEmployeeId = startEmployeeId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public CashDrawerLogStatus getStatus() {
        return status;
    }

    public void setStatus(CashDrawerLogStatus status) {
        this.status = status;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public BigDecimal getTotalCashDrop() {
        return totalCashDrop;
    }

    public void setTotalCashDrop(BigDecimal totalCashDrop) {
        this.totalCashDrop = totalCashDrop;
    }

    public BigDecimal getTotalCashIn() {
        return totalCashIn;
    }

    public void setTotalCashIn(BigDecimal totalCashIn) {
        this.totalCashIn = totalCashIn;
    }

    public BigDecimal getTotalCashOut() {
        return totalCashOut;
    }

    public void setTotalCashOut(BigDecimal totalCashOut) {
        this.totalCashOut = totalCashOut;
    }

    public BigDecimal getStoreCreditSales() {
        return storeCreditSales;
    }

    public void setStoreCreditSales(BigDecimal storeCreditSales) {
        this.storeCreditSales = storeCreditSales;
    }

    public BigDecimal getStoreCreditRefunds() {
        return storeCreditRefunds;
    }

    public void setStoreCreditRefunds(BigDecimal storeCreditRefunds) {
        this.storeCreditRefunds = storeCreditRefunds;
    }

    public HashMap<Cart.PaymentOption, BigDecimal> getCustomSalesTotals() {
        return customSalesTotals;
    }

    public void setCustomSalesTotals(HashMap<Cart.PaymentOption, BigDecimal> customSalesTotals) {
        this.customSalesTotals = customSalesTotals;
    }

    public HashMap<Cart.PaymentOption, BigDecimal> getCustomRefundTotals() {
        return customRefundTotals;
    }

    public void setCustomRefundTotals(HashMap<Cart.PaymentOption, BigDecimal> customRefundTotals) {
        this.customRefundTotals = customRefundTotals;
    }

    public BigDecimal getTotalCashlessAtmSales() {
        return totalCashlessAtmSales;
    }

    public void setTotalCashlessAtmSales(BigDecimal totalCashlessAtmSales) {
        this.totalCashlessAtmSales = totalCashlessAtmSales;
    }

    public BigDecimal getTotalCashlessAtmRefunds() {
        return totalCashlessAtmRefunds;
    }

    public void setTotalCashlessAtmRefunds(BigDecimal totalCashlessAtmRefunds) {
        this.totalCashlessAtmRefunds = totalCashlessAtmRefunds;
    }

    public BigDecimal getTotalCashlessAtmChange() {
        return totalCashlessAtmChange;
    }

    public void setTotalCashlessAtmChange(BigDecimal totalCashlessAtmChange) {
        this.totalCashlessAtmChange = totalCashlessAtmChange;
    }


    @Override
    public int compareTo(CashDrawerSession o) {
        return new Long(created - o.created).intValue();
    }


    public long getDateTimestamp() {
        return dateTimestamp;
    }

    public void setDateTimestamp(long dateTimestamp) {
        this.dateTimestamp = dateTimestamp;
    }
}
