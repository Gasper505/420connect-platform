package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.math.BigDecimal;

@CollectionName(name = "cash_drawer_activities")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CashDrawerSessionActivityLog extends ShopBaseModel {
    public enum CashDrawerType {
        AddPaidIn("New Paid In"), UpdatePaidIn("Update Paid In"), DeletePaidIn("Delete Paid In"),
        AddPaidOut("New Paid Out"), UpdatePaidOut("Update Paid Out"), DeletePaidOut("Delete Paid Out"),
        AddCashDrop("New Cash Drop"), UpdateCashDrop("Update Cash Drop"), DeleteCashDrop("Delete Cash Drop"),
        RefreshDrawer("Drawer Refreshed"),
        StartDrawer("Drawer Started"),
        EndDrawer("Drawer End"),
        ReopenDrawer("Drawer Reopened"),
        UpdateDrawer("Drawer Updated"),
        UpdateStartingCash("Start Cash Updated"),
        Sale("Sale"), Refund("Refund");

        String message;

        CashDrawerType(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    private String padIoItemId;
    private String cashDrawerSessionId;
    private CashDrawerType cashDrawerType;
    private String memberId;
    private String transactionId;
    private String createdByEmployeeId;
    private BigDecimal saleAmount;
    private String message;

    public String getPadIoItemId() {
        return padIoItemId;
    }

    public void setPadIoItemId(String padIoItemId) {
        this.padIoItemId = padIoItemId;
    }

    public String getCashDrawerSessionId() {
        return cashDrawerSessionId;
    }

    public void setCashDrawerSessionId(String cashDrawerSessionId) {
        this.cashDrawerSessionId = cashDrawerSessionId;
    }

    public CashDrawerType getCashDrawerType() {
        return cashDrawerType;
    }

    public void setCashDrawerType(CashDrawerType cashDrawerType) {
        this.cashDrawerType = cashDrawerType;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCreatedByEmployeeId() {
        return createdByEmployeeId;
    }

    public void setCreatedByEmployeeId(String createdByEmployeeId) {
        this.createdByEmployeeId = createdByEmployeeId;
    }

    public BigDecimal getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(BigDecimal saleAmount) {
        this.saleAmount = saleAmount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
