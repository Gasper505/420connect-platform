package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.rest.dispensary.requests.company.PrimaryContact;
import com.stripe.model.Customer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Objects;

@CollectionName(name = "companies", uniqueIndexes = {"{email:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company extends BaseModel implements InternalAllowable, OnPremSyncable {

    private static final String DEFAULT_EMAIL_COLOR = "#0FC3E9";

    public enum CompanyMembersShareOption {
        Isolated,
        Shared
    }


    public enum CompanyPricing {
        Normal,
        Discounted
    }

    public enum LoyaltyAccrueOption {
        Subtotal,
        SubtotalWithDiscount,
        FinalTotal
    }

    private CompanyMembersShareOption membersShareOption = CompanyMembersShareOption.Shared;
    private String isId; // InfusionSoft ContactId
    private String name;
    private String phoneNumber;
    private String email;
    private Address address;
    private String logoURL;
    private String supportEmail;
    private boolean showNameWithLogo = true;
    private boolean active = true;
    private String website;
    private String productSKU;
    private String queueUrl;
    private String preferredEmailColor;
    private CompanyPricing pricingOpt = CompanyPricing.Discounted;
    private boolean enableLoyalty = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("1")
    private BigDecimal dollarToPointRatio = new BigDecimal(1);

    private Integer duration;
    private String portalUrl;
    private String businessLocation;
    private String fax;
    private PrimaryContact primaryContact;
    private String taxId;

    private LoyaltyAccrueOption loyaltyAccrueOpt = LoyaltyAccrueOption.Subtotal;

    private boolean enableSpringBig = Boolean.FALSE;

    private String externalId;

    private PurchaseOrder.POPaymentTerms defaultPaymentTerm;
    private String salesPerson;
    private String contactPerson;
    private OnPremCompanyConfig onPremCompanyConfig;


    private StateCannabisLimit adultUseLimit; // this is an override version
    private StateCannabisLimit thirdPartyLimit; // this is an override version
    private StateCannabisLimit mmicLimit; // this is an override version

    private Customer stripeCustomer;
    private String stripeCustomerId;

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }

    public StateCannabisLimit getAdultUseLimit() {
        return adultUseLimit;
    }

    public void setAdultUseLimit(StateCannabisLimit adultUseLimit) {
        this.adultUseLimit = adultUseLimit;
    }

    public StateCannabisLimit getThirdPartyLimit() {
        return thirdPartyLimit;
    }

    public void setThirdPartyLimit(StateCannabisLimit thirdPartyLimit) {
        this.thirdPartyLimit = thirdPartyLimit;
    }

    public StateCannabisLimit getMmicLimit() {
        return mmicLimit;
    }

    public void setMmicLimit(StateCannabisLimit mmicLimit) {
        this.mmicLimit = mmicLimit;
    }

    public LoyaltyAccrueOption getLoyaltyAccrueOpt() {
        return loyaltyAccrueOpt;
    }

    public void setLoyaltyAccrueOpt(LoyaltyAccrueOption loyaltyAccrueOpt) {
        this.loyaltyAccrueOpt = loyaltyAccrueOpt;
    }

    public CompanyMembersShareOption getMembersShareOption() {
        return membersShareOption;
    }

    public void setMembersShareOption(CompanyMembersShareOption membersShareOption) {
        this.membersShareOption = membersShareOption;
    }

    public CompanyPricing getPricingOpt() {
        return pricingOpt;
    }

    public void setPricingOpt(CompanyPricing pricingOpt) {
        this.pricingOpt = pricingOpt;
    }

    public BigDecimal getDollarToPointRatio() {
        return dollarToPointRatio;
    }

    public void setDollarToPointRatio(BigDecimal dollarToPointRatio) {
        this.dollarToPointRatio = dollarToPointRatio;
    }

    public boolean isEnableLoyalty() {
        return enableLoyalty;
    }

    public void setEnableLoyalty(boolean enableLoyalty) {
        this.enableLoyalty = enableLoyalty;
    }

    public String getIsId() {
        return isId;
    }

    public void setIsId(String isId) {
        this.isId = isId;
    }

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isShowNameWithLogo() {
        return showNameWithLogo;
    }

    public void setShowNameWithLogo(boolean showNameWithLogo) {
        this.showNameWithLogo = showNameWithLogo;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getQueueUrl() {
        return queueUrl;
    }

    public void setQueueUrl(String queueUrl) {
        this.queueUrl = queueUrl;
    }

    public String getPreferredEmailColor() {
        return Objects.isNull(preferredEmailColor) ? DEFAULT_EMAIL_COLOR : preferredEmailColor;
    }

    public void setPreferredEmailColor(String preferredEmailColor) {
        this.preferredEmailColor = preferredEmailColor;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getPortalUrl() {
        return portalUrl;
    }

    public void setPortalUrl(String portalUrl) {
        this.portalUrl = portalUrl;
    }

    public String getBusinessLocation() {
        return businessLocation;
    }

    public void setBusinessLocation(String businessLocation) {
        this.businessLocation = businessLocation;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public PrimaryContact getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(PrimaryContact primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }


    public boolean isEnableSpringBig() {
        return enableSpringBig;
    }

    public void setEnableSpringBig(boolean enableSpringBig) {
        this.enableSpringBig = enableSpringBig;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }


    public PurchaseOrder.POPaymentTerms getDefaultPaymentTerm() {
        return defaultPaymentTerm;
    }

    public void setDefaultPaymentTerm(PurchaseOrder.POPaymentTerms defaultPaymentTerm) {
        this.defaultPaymentTerm = defaultPaymentTerm;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public OnPremCompanyConfig getOnPremCompanyConfig() {
        return onPremCompanyConfig;
    }

    public void setOnPremCompanyConfig(OnPremCompanyConfig onPremCompanyConfig) {
        this.onPremCompanyConfig = onPremCompanyConfig;
    }

    public Customer getStripeCustomer() {
        return stripeCustomer;
    }

    public void setStripeCustomer(Customer stripeCustomer) {
        this.stripeCustomer = stripeCustomer;
    }

    public static class OnPremCompanyConfig {
        private boolean onPremEnable;
        private String host;
        private String onPremKey;
        private String iv;

        public boolean isOnPremEnable() {
            return onPremEnable;
        }

        public void setOnPremEnable(boolean onPremEnable) {
            this.onPremEnable = onPremEnable;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }


        public String getOnPremKey() {
            return onPremKey;
        }

        public void setOnPremKey(String onPremKey) {
            this.onPremKey = onPremKey;
        }

        public String getIv() {
            return iv;
        }

        public void setIv(String iv) {
            this.iv = iv;
        }
    }

}