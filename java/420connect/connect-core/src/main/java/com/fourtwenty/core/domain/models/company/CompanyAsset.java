package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.consumer.ConsumerAsset;
import com.fourtwenty.core.domain.models.generic.Asset;

@CollectionName(name = "company_assets", uniqueIndexes = {"{companyId:1,key:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyAsset extends CompanyBaseModel implements Asset {
    private String name;
    private String key;
    private AssetType type;
    private String publicURL;
    private boolean active;
    private int priority;
    private boolean secured = true; // Default is secured

    // Thumb, medium, and large are all public
    private String thumbURL;
    private String mediumURL;
    private String largeURL;
    private String largeX2URL;
    private String origURL;

    public ConsumerAsset toConsumerAsset() {
        ConsumerAsset consumerAsset = new ConsumerAsset();

        consumerAsset.prepare();
        consumerAsset.setName(name);
        consumerAsset.setKey(key);
        consumerAsset.setType(type);
        consumerAsset.setActive(active);

        return consumerAsset;
    }

    public String getLargeX2URL() {
        return largeX2URL;
    }

    public void setLargeX2URL(String largeX2URL) {
        this.largeX2URL = largeX2URL;
    }

    public String getOrigURL() {
        return origURL;
    }

    public void setOrigURL(String origURL) {
        this.origURL = origURL;
    }

    public String getLargeURL() {
        return largeURL;
    }

    public void setLargeURL(String largeURL) {
        this.largeURL = largeURL;
    }

    public String getMediumURL() {
        return mediumURL;
    }

    public void setMediumURL(String mediumURL) {
        this.mediumURL = mediumURL;
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }

    public String getPublicURL() {
        return publicURL;
    }

    public void setPublicURL(String publicURL) {
        this.publicURL = publicURL;
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public AssetType getType() {
        return type;
    }

    public void setType(AssetType type) {
        this.type = type;
    }

    @Override
    public Asset.AssetType getAssetType() {
        return type;
    }
}