package com.fourtwenty.core.domain.models.company;

import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.BaseModel;

/**
 * Created by Stephen Schmidt on 9/4/2015.
 */
public abstract class CompanyBaseModel extends BaseModel implements OnPremSyncable {
    protected String companyId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }


    public void prepare(String companyId) {
        this.companyId = companyId;
        this.prepare();
    }

    public void resetPrepare(String companyId) {
        this.id = null;
        this.companyId = companyId;
        this.prepare();
    }
}
