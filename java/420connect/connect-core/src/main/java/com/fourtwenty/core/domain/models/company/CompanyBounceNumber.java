package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

@CollectionName(name = "company_bounced_numbers", uniqueIndexes = {"{companyId:1,number:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyBounceNumber extends CompanyBaseModel {
    public enum BounceNumberStatus {
        UNREACHABLE,
        OPT_OUT,
        OPT_IN
    }

    private BounceNumberStatus status = BounceNumberStatus.OPT_OUT;

    private String number;
    private String fromNumber;

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BounceNumberStatus getStatus() {
        return status;
    }

    public void setStatus(BounceNumberStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("ID: %s, companyId: %s, number: %s", id, companyId, number);
    }
}
