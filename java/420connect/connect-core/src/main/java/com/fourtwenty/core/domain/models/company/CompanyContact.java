package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@CollectionName(name = "company_contacts", uniqueIndexes = {"{companyId:1,email:1}"}, indexes = {"{customerCompanyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyContact extends ShopBaseModel {

    @NotEmpty
    private String customerCompanyId; //Vendor
    private String salutation;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private String email;
    private String role;
    private String phoneNumber;
    private CompanyAsset userPhoto;
    private Note note;
    private String officeNumber;
    private List<CompanyAsset> assets;

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CompanyAsset getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(CompanyAsset userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    @JsonIgnore
    public StringBuilder getName(){
        StringBuilder employeeName = new StringBuilder();
        if (StringUtils.isNotBlank(getFirstName())) {
            employeeName = employeeName.append(getFirstName());
        }
        if (StringUtils.isNotBlank(getLastName())) {
            employeeName = employeeName.append(StringUtils.SPACE).append(getLastName());
        }
        return employeeName;
    }
}
