package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.hibernate.validator.constraints.NotEmpty;

@CollectionName(name = "company_contact_logs", indexes = {"{customerCompanyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyContactLog extends ShopBaseModel {
    @NotEmpty
    private String customerCompanyId;
    @NotEmpty
    private String companyContactId;
    @NotEmpty
    private String employeeId;
    private String log;

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getCompanyContactId() {
        return companyContactId;
    }

    public void setCompanyContactId(String companyContactId) {
        this.companyContactId = companyContactId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
