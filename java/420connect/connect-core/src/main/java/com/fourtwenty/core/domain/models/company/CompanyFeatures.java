package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 5/15/16.
 */
@CollectionName(name = "company_features", uniqueIndexes = {"{companyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyFeatures extends CompanyBaseModel implements OnPremSyncable {

    public enum AppTarget {
        AuthenticationApp,
        Retail,
        Distribution,
        Grow
    }

    private static final int MAX_INVENTORY_PER_SHOP = 10; // 9 custom, 1 safe
    private static final int DEFAULT_TERMINALS = 2;
    private static final int DEFAULT_SHOPS = 1;
    private static final int DEFAULT_EMPLOYEES = 50;
    private int maxTerminals = DEFAULT_TERMINALS;
    private int maxShop = DEFAULT_SHOPS;
    private int maxEmployees = DEFAULT_EMPLOYEES;
    private int maxInventories = MAX_INVENTORY_PER_SHOP;

    private List<Transaction.QueueType> availableQueues = new ArrayList<>();
    private List<AppTarget> availableApps = Lists.newArrayList(AppTarget.AuthenticationApp);

    private int smsMarketingCredits = 0;
    private int emailMarketingCredits = 0;

    private Map<String, Boolean> availableQbShops;

    public static int getDefaultShops() {
        return DEFAULT_SHOPS;
    }

    public int getEmailMarketingCredits() {
        return emailMarketingCredits;
    }

    public void setEmailMarketingCredits(int emailMarketingCredits) {
        this.emailMarketingCredits = emailMarketingCredits;
    }

    public int getMaxEmployees() {
        return maxEmployees;
    }

    public void setMaxEmployees(int maxEmployees) {
        this.maxEmployees = maxEmployees;
    }

    public int getMaxInventories() {
        return maxInventories;
    }

    public void setMaxInventories(int maxInventories) {
        this.maxInventories = maxInventories;
    }

    public int getSmsMarketingCredits() {
        return smsMarketingCredits;
    }

    public void setSmsMarketingCredits(int smsMarketingCredits) {
        this.smsMarketingCredits = smsMarketingCredits;
    }

    public List<Transaction.QueueType> getAvailableQueues() {
        return availableQueues;
    }

    public void setAvailableQueues(List<Transaction.QueueType> availableQueues) {
        this.availableQueues = availableQueues;
    }

    public static int getDefaultTerminals() {
        return DEFAULT_TERMINALS;
    }

    public int getMaxShop() {
        return maxShop;
    }

    public void setMaxShop(int maxShop) {
        this.maxShop = maxShop;
    }

    public int getMaxTerminals() {
        return maxTerminals;
    }

    public void setMaxTerminals(int maxTerminals) {
        this.maxTerminals = maxTerminals;
    }

    public List<AppTarget> getAvailableApps() {
        return availableApps;
    }

    public void setAvailableApps(List<AppTarget> availableApps) {
        this.availableApps = availableApps;
    }

    public Map<String, Boolean> getAvailableQbShops() {
        return availableQbShops;
    }

    public void setAvailableQbShops(Map<String, Boolean> availableQbShops) {
        this.availableQbShops = availableQbShops;
    }
}
