package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

/**
 * Created by Stephen on 9/10/2015.
 */
@CollectionName(name = "company_logs")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyLog extends CompanyBaseModel {
}
