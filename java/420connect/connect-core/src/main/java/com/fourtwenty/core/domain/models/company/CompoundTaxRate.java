package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 1/9/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompoundTaxRate extends ShopBaseModel {
    public enum CompoundTaxTerritory {
        Default, City, County, State, Federal
    }

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal taxRate = new BigDecimal(0);
    private boolean compound = false;
    private boolean active = false;
    private CompoundTaxTerritory territory = CompoundTaxTerritory.Default;
    private boolean activeExciseTax = Boolean.FALSE;
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed

    public CompoundTaxRate() {
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public CompoundTaxRate(CompoundTaxTerritory territory) {
        this.territory = territory;
    }

    public CompoundTaxTerritory getTerritory() {
        return territory;
    }

    public void setTerritory(CompoundTaxTerritory territory) {
        this.territory = territory;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isCompound() {
        return compound;
    }

    public void setCompound(boolean compound) {
        this.compound = compound;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public boolean isActiveExciseTax() {
        return activeExciseTax;
    }

    public void setActiveExciseTax(boolean activeExciseTax) {
        this.activeExciseTax = activeExciseTax;
    }
}
