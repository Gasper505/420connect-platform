package com.fourtwenty.core.domain.models.company;

import com.esotericsoftware.kryo.Kryo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.util.NumberUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mdo on 1/9/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompoundTaxTable extends ShopBaseModel {
    public static class ActiveTax {
        public BigDecimal taxRate = new BigDecimal(0);
        public CompoundTaxRate.CompoundTaxTerritory territory;
        public boolean compound = false;
    }


    private String name;
    private boolean active = false;
    private TaxInfo.TaxType taxType = TaxInfo.TaxType.Inherit;
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private CompoundTaxRate cityTax = new CompoundTaxRate(CompoundTaxRate.CompoundTaxTerritory.City);
    private CompoundTaxRate countyTax = new CompoundTaxRate(CompoundTaxRate.CompoundTaxTerritory.County);
    private CompoundTaxRate stateTax = new CompoundTaxRate(CompoundTaxRate.CompoundTaxTerritory.State);
    private CompoundTaxRate federalTax = new CompoundTaxRate(CompoundTaxRate.CompoundTaxTerritory.Federal);

    public void reset() {
        if (cityTax != null) {
            cityTax.prepare();
            cityTax.setTerritory(CompoundTaxRate.CompoundTaxTerritory.City);
        }
        if (countyTax != null) {
            countyTax.prepare();
            countyTax.setTerritory(CompoundTaxRate.CompoundTaxTerritory.County);
        }
        if (stateTax != null) {
            stateTax.prepare();
            stateTax.setTerritory(CompoundTaxRate.CompoundTaxTerritory.State);
        }
        if (federalTax != null) {
            federalTax.prepare();
            federalTax.setTerritory(CompoundTaxRate.CompoundTaxTerritory.Federal);
        }
    }

    @JsonIgnore
    public LinkedList<CompoundTaxRate> getActiveTaxesAsList() {
        LinkedList<CompoundTaxRate> availableTaxes = new LinkedList<>();
        availableTaxes.add(cityTax);
        availableTaxes.add(countyTax);
        availableTaxes.add(stateTax);
        availableTaxes.add(federalTax);
        return availableTaxes;
    }

    @JsonIgnore
    public LinkedList<CompoundTaxRate> getActivePostTaxes() {

        LinkedList<CompoundTaxRate> availableTaxes = new LinkedList<>();
        for (CompoundTaxRate curTaxRate : this.getActiveTaxesAsList()) {
            if (curTaxRate.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                availableTaxes.add(curTaxRate);
            }
        }
        return availableTaxes;
    }

    @JsonIgnore
    public List<ActiveTax> getActivePreTaxes() {
        List<ActiveTax> activeTaxes = new ArrayList<>();

        for (CompoundTaxRate curTaxRate : this.getActiveTaxesAsList()) {
            if ((curTaxRate.getTaxOrder() == TaxInfo.TaxProcessingOrder.PreTaxed)
                    && curTaxRate.isActive() && (NumberUtils.round(curTaxRate.getTaxRate().doubleValue(), 6) > 0)) {
                ActiveTax activeTax = new ActiveTax();
                activeTax.territory = curTaxRate.getTerritory();
                activeTax.compound = curTaxRate.isCompound();
                activeTax.taxRate = curTaxRate.getTaxRate();
                activeTaxes.add(activeTax);
            }
        }
        /*activeTaxes.add(new ActiveTax());

        while (availableTaxes.size() > 0) {
            ActiveTax activeTax = activeTaxes.get(activeTaxes.size() - 1);
            CompoundTaxRate taxTable = availableTaxes.get(0);

            if (taxTable.isActive()) {
                if (taxTable.isCompound() && (NumberUtils.round(activeTax.taxRate.doubleValue(), 6) > 0)) {
                    ActiveTax activeCompTax = new ActiveTax(); // create new rate
                    activeCompTax.territory = taxTable.getTerritory();
                    activeCompTax.compound = taxTable.isCompound();
                    activeCompTax.taxRate = taxTable.getTaxRate();
                    activeTaxes.add(activeCompTax);
                    continue;
                } else {
                    if(taxTable.isCompound()) {
                        // This is a non-compound and current rate is 0 so just append current rate
                        activeTax.taxRate = activeTax.taxRate.add(taxTable.getTaxRate());
                        activeTax.territory = taxTable.getTerritory();
                    } else {
                        activeTax.taxRateList = activeTax.taxRateList.add(taxTable);
                    }
                }
            }

            availableTaxes.removeFirst();
        }*/
        return activeTaxes;
    }

    @JsonIgnore
    public CompoundTaxTable copyTaxTable() {
        CompoundTaxTable newTaxTable = new CompoundTaxTable();
        Kryo kryo = new Kryo();
        newTaxTable = kryo.copy(this);
        newTaxTable.setId(null);
        newTaxTable.prepare(this.getCompanyId());
        newTaxTable.setShopId(this.getShopId());
        if (newTaxTable.getTaxType() != TaxInfo.TaxType.Exempt) {
            newTaxTable.setTaxType(TaxInfo.TaxType.Custom);


            if (newTaxTable.getCityTax() != null) {
                newTaxTable.getCityTax().setId(null);
                newTaxTable.getCityTax().prepare(this.getCompanyId());
                newTaxTable.getCityTax().setShopId(this.getShopId());
            }

            if (newTaxTable.getStateTax() != null) {
                newTaxTable.getCountyTax().setId(null);
                newTaxTable.getCountyTax().prepare(this.getCompanyId());
                newTaxTable.getCountyTax().setShopId(this.getShopId());
            }

            if (newTaxTable.getCountyTax() != null) {
                newTaxTable.getStateTax().setId(null);
                newTaxTable.getStateTax().prepare(this.getCompanyId());
                newTaxTable.getStateTax().setShopId(this.getShopId());
            }
            if (newTaxTable.getFederalTax() != null) {
                newTaxTable.getFederalTax().setId(null);
                newTaxTable.getFederalTax().prepare(this.getCompanyId());
                newTaxTable.getFederalTax().setShopId(this.getShopId());
            }
        }
        newTaxTable.reset();

        return newTaxTable;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public TaxInfo.TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxInfo.TaxType taxType) {
        this.taxType = taxType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public CompoundTaxRate getCityTax() {
        return cityTax;
    }

    public void setCityTax(CompoundTaxRate cityTax) {
        this.cityTax = cityTax;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public CompoundTaxRate getCountyTax() {
        return countyTax;
    }

    public void setCountyTax(CompoundTaxRate countyTax) {
        this.countyTax = countyTax;
    }

    public CompoundTaxRate getFederalTax() {
        return federalTax;
    }

    public void setFederalTax(CompoundTaxRate federalTax) {
        this.federalTax = federalTax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompoundTaxRate getStateTax() {
        return stateTax;
    }

    public void setStateTax(CompoundTaxRate stateTax) {
        this.stateTax = stateTax;
    }
}
