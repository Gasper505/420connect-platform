package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 1/9/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum ConsumerType {
    Other("Other"),
    AdultUse("Adult Use"),
    MedicinalState("Medicinal - MMIC"),
    MedicinalThirdParty("Medicinal - Third Party");

    ConsumerType(String displayName) {
        this.displayName = displayName;
    }

    private String displayName;

    public String getDisplayName() {
        return displayName;
    }
}
