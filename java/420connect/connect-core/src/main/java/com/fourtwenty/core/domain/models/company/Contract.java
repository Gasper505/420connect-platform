package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.hibernate.validator.constraints.NotEmpty;

@CollectionName(name = "contracts", indexes = {"{companyId:1,shopId:1,name:1}", "{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contract extends ShopBaseModel {
    public enum ContractContentType {
        TEXT,
        PDF
    }

    @NotEmpty
    private String name;
    private boolean active;
    private String text;
    private int version;
    private boolean required;
    private CompanyAsset pdfFile;
    private Boolean enableWitnessSignature;
    private Boolean enableEmployeeSignature;
    private ContractContentType contentType = ContractContentType.TEXT;

    public ContractContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContractContentType contentType) {
        this.contentType = contentType;
    }

    public CompanyAsset getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(CompanyAsset pdfFile) {
        this.pdfFile = pdfFile;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Boolean getEnableWitnessSignature() {
        return enableWitnessSignature;
    }

    public void setEnableWitnessSignature(Boolean enableWitnessSignature) {
        this.enableWitnessSignature = enableWitnessSignature;
    }

    public Boolean getEnableEmployeeSignature() {
        return enableEmployeeSignature;
    }

    public void setEnableEmployeeSignature(Boolean enableEmployeeSignature) {
        this.enableEmployeeSignature = enableEmployeeSignature;
    }
}