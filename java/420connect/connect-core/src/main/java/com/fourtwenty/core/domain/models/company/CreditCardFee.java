package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Comparator;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditCardFee extends CompanyBaseModel {

    private boolean enabled = true;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal fee = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal subTotalThreshold;
    private OrderItem.DiscountType type = OrderItem.DiscountType.Cash;

    public static class CreditCardFeeComparator implements Comparator<CreditCardFee> {
        @Override
        public int compare(CreditCardFee o1, CreditCardFee o2) {
            if (!o1.isEnabled()) {
                return 1;
            }
            if (!o2.isEnabled()) {
                return -1;
            }
            int result = o1.getSubTotalThreshold().compareTo(o2.getSubTotalThreshold());

            return result;
        }
    }

    public static class CreditCardFeeDescendingComparator implements Comparator<CreditCardFee> {
        @Override
        public int compare(CreditCardFee o1, CreditCardFee o2) {
            if (!o1.isEnabled()) {
                return 1;
            }
            if (!o2.isEnabled()) {
                return -1;
            }
            int result = o2.getSubTotalThreshold().compareTo(o1.getSubTotalThreshold());

            return result;
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getSubTotalThreshold() {
        return subTotalThreshold;
    }

    public void setSubTotalThreshold(BigDecimal subTotalThreshold) {
        this.subTotalThreshold = subTotalThreshold;
    }

    public OrderItem.DiscountType getType() {
        return type;
    }

    public void setType(OrderItem.DiscountType type) {
        this.type = type;
    }
}
