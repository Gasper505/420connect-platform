package com.fourtwenty.core.domain.models.company;

public class CreditCardReader {
    private CreditCardReaderEncryptionType encryptionType;
    private String secretKey;

    public CreditCardReaderEncryptionType getEncryptionType() {
        return encryptionType;
    }

    public void setEncryptionType(CreditCardReaderEncryptionType encryptionType) {
        this.encryptionType = encryptionType;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}
