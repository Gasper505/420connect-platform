package com.fourtwenty.core.domain.models.company;

public enum CreditCardReaderEncryptionType {
    BlazeSymmetricEncryption,
    DUKPT
}
