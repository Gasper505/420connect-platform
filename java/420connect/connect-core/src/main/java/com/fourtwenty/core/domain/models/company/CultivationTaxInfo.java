package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@CollectionName(name = "cultivation_taxes", uniqueIndexes = {"{state:1,country:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CultivationTaxInfo extends BaseModel implements OnPremSyncable {

    private String state;
    private String country;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal flowersTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal leavesTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal plantTax = new BigDecimal(0);

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public BigDecimal getFlowersTax() {
        return flowersTax;
    }

    public void setFlowersTax(BigDecimal flowersTax) {
        this.flowersTax = flowersTax;
    }

    public BigDecimal getLeavesTax() {
        return leavesTax;
    }

    public void setLeavesTax(BigDecimal leavesTax) {
        this.leavesTax = leavesTax;
    }

    public BigDecimal getPlantTax() {
        return plantTax;
    }

    public void setPlantTax(BigDecimal plantTax) {
        this.plantTax = plantTax;
    }
}
