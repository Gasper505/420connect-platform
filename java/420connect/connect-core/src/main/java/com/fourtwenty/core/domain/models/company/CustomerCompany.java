package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "customer_companies", uniqueIndexes = {"{email:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerCompany extends ShopBaseModel {
    public enum CompanyType {
        DISTRIBUTOR,
        CULTIVATOR,
        RETAILER,
        MANUFACTURER,
        TRANSPORTER,
        OTHER
    }

    public enum EntityTaxType {
        ARMS_LENGTH,
        NON_ARMS_LENGTH
    }

    public enum LicenceType {
        RECREATIONAL,
        MEDICINAL,
        BOTH
    }

    private long maxAttachment = 15;
    @NotEmpty
    private String name;
    private String email;
    private CompanyType companyType;
    private List<Address> addresses = new ArrayList<>();
    private String license;
    private String website;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal credits = new BigDecimal(0);
    private List<CompanyAsset> attachments = new ArrayList<>();
    private EntityTaxType entityTaxType;
    private String officePhoneNumber;
    private String mobileNumber;
    private LicenceType licenceType;
    private boolean relatedEntity;
    private boolean active;
    private Note note;
    private Long licenceExpirationDate;
    private Boolean backOrderEnabled = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BigDecimal getCredits() {
        return credits;
    }

    public void setCredits(BigDecimal credits) {
        this.credits = credits;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public EntityTaxType getEntityTaxType() {
        return entityTaxType;
    }

    public void setEntityTaxType(EntityTaxType entityTaxType) {
        this.entityTaxType = entityTaxType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getMaxAttachment() {
        return maxAttachment;
    }

    public void setMaxAttachment(long maxAttachment) {
        this.maxAttachment = maxAttachment;
    }

    public String getOfficePhoneNumber() {
        return officePhoneNumber;
    }

    public void setOfficePhoneNumber(String officePhoneNumber) {
        this.officePhoneNumber = officePhoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public LicenceType getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(LicenceType licenceType) {
        this.licenceType = licenceType;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Long getLicenceExpirationDate() {
        return licenceExpirationDate;
    }

    public void setLicenceExpirationDate(Long licenceExpirationDate) {
        this.licenceExpirationDate = licenceExpirationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getBackOrderEnabled() {
        return backOrderEnabled;
    }

    public void setBackOrderEnabled(Boolean backOrderEnabled) {
        this.backOrderEnabled = backOrderEnabled;
    }
}
