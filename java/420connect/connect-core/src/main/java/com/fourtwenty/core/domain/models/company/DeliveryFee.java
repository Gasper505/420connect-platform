package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;

@CollectionName(name = "delivery_fee", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryFee extends CompanyBaseModel {

    public enum DeliveryFeeType {
        CART_SUB_TOTAL,
        DISTANCE,
        ZIP_Code
    }

    public static class DeliveryFeeComparator implements Comparator<DeliveryFee> {
        @Override
        public int compare(DeliveryFee o1, DeliveryFee o2) {
            if (!o1.isEnabled()) {
                return 1;
            }
            if (!o2.isEnabled()) {
                return -1;
            }
            int result = o1.getSubTotalThreshold().compareTo(o2.getSubTotalThreshold());

            return result;
        }
    }

    private boolean enabled = true;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal subTotalThreshold;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal fee;
    private DeliveryFeeType feeType = DeliveryFeeType.CART_SUB_TOTAL;
    private Set<String> zipCodes;
    private BigDecimal distance;
    private OrderItem.DiscountType type = OrderItem.DiscountType.Cash;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getSubTotalThreshold() {
        return subTotalThreshold;
    }

    public void setSubTotalThreshold(BigDecimal subTotalThreshold) {
        this.subTotalThreshold = subTotalThreshold;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public DeliveryFeeType getFeeType() {
        return feeType;
    }

    public void setFeeType(DeliveryFeeType feeType) {
        this.feeType = feeType;
    }

    public Set<String> getZipCodes() {
        return zipCodes;
    }

    public void setZipCodes(Set<String> zipCodes) {
        this.zipCodes = zipCodes;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public OrderItem.DiscountType getType() {
        return type;
    }

    public void setType(OrderItem.DiscountType type) {
        this.type = type;
    }
}
