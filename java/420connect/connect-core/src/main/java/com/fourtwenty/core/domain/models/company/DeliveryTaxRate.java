package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CollectionName(name = "delivery_tax_rate", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryTaxRate extends ShopBaseModel {

    private String name;
    private Set<String> regionIds = new HashSet<>();
    private boolean active;
    private boolean useComplexTax = false;
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed;
    private TaxInfo taxInfo;
    private List<CompoundTaxTable> taxTables = new ArrayList<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getRegionIds() {
        return regionIds;
    }

    public void setRegionIds(Set<String> regionIds) {
        this.regionIds = regionIds;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isUseComplexTax() {
        return useComplexTax;
    }

    public void setUseComplexTax(boolean useComplexTax) {
        this.useComplexTax = useComplexTax;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public TaxInfo getTaxInfo() {
        return taxInfo;
    }

    public void setTaxInfo(TaxInfo taxInfo) {
        this.taxInfo = taxInfo;
    }

    public List<CompoundTaxTable> getTaxTables() {
        return taxTables;
    }

    public void setTaxTables(List<CompoundTaxTable> taxTables) {
        this.taxTables = taxTables;
    }
}
