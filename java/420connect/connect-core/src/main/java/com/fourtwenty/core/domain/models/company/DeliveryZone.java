package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by decipher on 28/11/17 5:04 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "delivery_zones", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryZone extends ShopBaseModel {

    public enum DeliveryZoneType {
        NonDelivery,
        Delivery
    }

    private DeliveryZoneType zoneType;
    private String loc;

    public DeliveryZoneType getZoneType() {
        return zoneType;
    }

    public void setZoneType(DeliveryZoneType zoneType) {
        this.zoneType = zoneType;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }
}
