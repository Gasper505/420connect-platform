package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on 18/11/17 12:21 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DrawerStartCashUpdateRequest {

    private double startCashAmount;

    public double getStartCashAmount() {
        return startCashAmount;
    }

    public void setStartCashAmount(double startCashAmount) {
        this.startCashAmount = startCashAmount;
    }
}
