package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;


@CollectionName(name = "employees", uniqueIndexes = {"{companyId:1,email:1}", "{companyId:1,pin:1}"}, indexes = {"{email:1,role:1}", "{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee extends CompanyBaseModel implements InternalAllowable,OnPremSyncable {

    public enum SortOption {
        NAME,
        DISTANCE,
        RANDOM
    }

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;

    private String pin;
    @NotEmpty
    private String roleId;

    private String email;
    private String password;
    private String driversLicense;
    private String dlExpirationDate;
    private String vehicleMake;


    private List<Note> notes = new ArrayList<>();
    private List<String> shops = new ArrayList<>();
    private boolean disabled = false;
    private String phoneNumber;
    @Deprecated
    private String assignedInventoryId;
    private String assignedTerminalId;
    private Address address;


    private String timecardId; // Recent timecardId
    private TimeCard timeCard;
    // TRANSIENT. DO NOT SAVE
    private Role role;
    private Boolean canApplyCustomDiscount = true; // default is true

    private long insuranceExpireDate;
    private String insuranceCompanyName;
    private String policyNumber;
    private long registrationExpireDate;
    private String vehiclePin;
    private String vinNo;
    private String vehicleModel;
    private String vehicleLicensePlate;

    private TerminalLocation recentLocation = null;
    private List<EmployeeOnFleetInfo> employeeOnFleetInfoList;
    private LinkedHashSet<CompanyFeatures.AppTarget> appAccessList = new LinkedHashSet<>();
    private List<EmployeeTookanInfo> tookanInfoList;

    private String lastLoggedInShopId;
    private Long lastLoginTime;
    private boolean driver = false;

    private String externalId;

    private List<String> organizationIds = new ArrayList<>();

    public boolean isDriver() {
        return driver;
    }

    public Long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public void setDriver(boolean driver) {
        this.driver = driver;
    }

    public TerminalLocation getRecentLocation() {
        return recentLocation;
    }

    public void setRecentLocation(TerminalLocation recentLocation) {
        this.recentLocation = recentLocation;
    }

    public String getDlExpirationDate() {
        return dlExpirationDate;
    }

    public void setDlExpirationDate(String dlExpirationDate) {
        this.dlExpirationDate = dlExpirationDate;
    }

    public String getDriversLicense() {
        return driversLicense;
    }

    public void setDriversLicense(String driversLicense) {
        this.driversLicense = driversLicense;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getAssignedTerminalId() {
        return assignedTerminalId;
    }

    public void setAssignedTerminalId(String assignedTerminalId) {
        this.assignedTerminalId = assignedTerminalId;
    }

    public TimeCard getTimeCard() {
        return timeCard;
    }

    public void setTimeCard(TimeCard timeCard) {
        this.timeCard = timeCard;
    }

    public String getTimecardId() {
        return timecardId;
    }

    public void setTimecardId(String timecardId) {
        this.timecardId = timecardId;
    }

    public String getAssignedInventoryId() {
        return assignedInventoryId;
    }

    public void setAssignedInventoryId(String assignedInventoryId) {
        this.assignedInventoryId = assignedInventoryId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<String> getShops() {
        return shops;
    }

    public void setShops(List<String> shops) {
        this.shops = shops;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getCanApplyCustomDiscount() {
        return canApplyCustomDiscount;
    }

    public void setCanApplyCustomDiscount(Boolean canApplyCustomDiscount) {
        this.canApplyCustomDiscount = canApplyCustomDiscount;
    }

    public long getInsuranceExpireDate() {
        return insuranceExpireDate;
    }

    public void setInsuranceExpireDate(long insuranceExpireDate) {
        this.insuranceExpireDate = insuranceExpireDate;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public long getRegistrationExpireDate() {
        return registrationExpireDate;
    }

    public void setRegistrationExpireDate(long registrationExpireDate) {
        this.registrationExpireDate = registrationExpireDate;
    }

    public String getVehiclePin() {
        return vehiclePin;
    }

    public void setVehiclePin(String vehiclePin) {
        this.vehiclePin = vehiclePin;
    }

    public List<EmployeeOnFleetInfo> getEmployeeOnFleetInfoList() {
        return employeeOnFleetInfoList;
    }

    public void setEmployeeOnFleetInfoList(List<EmployeeOnFleetInfo> employeeOnFleetInfoList) {
        this.employeeOnFleetInfoList = employeeOnFleetInfoList;
    }

    public LinkedHashSet<CompanyFeatures.AppTarget> getAppAccessList() {
        return appAccessList;
    }

    public void setAppAccessList(LinkedHashSet<CompanyFeatures.AppTarget> appAccessList) {
        this.appAccessList = appAccessList;
    }

    public String getVinNo() {
        return vinNo;
    }

    public void setVinNo(String vinNo) {
        this.vinNo = vinNo;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlate() {
        return vehicleLicensePlate;
    }

    public void setVehicleLicensePlate(String vehicleLicensePlate) {
        this.vehicleLicensePlate = vehicleLicensePlate;
    }

    public String getLastLoggedInShopId() {
        return lastLoggedInShopId;
    }

    public void setLastLoggedInShopId(String lastLoggedInShopId) {
        this.lastLoggedInShopId = lastLoggedInShopId;
    }

    public List<EmployeeTookanInfo> getTookanInfoList() {
        return tookanInfoList;
    }

    public void setTookanInfoList(List<EmployeeTookanInfo> tookanInfoList) {
        this.tookanInfoList = tookanInfoList;
    }

    public List<String> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(List<String> organizationIds) {
        this.organizationIds = organizationIds;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getExternalId() {
        return this.externalId;
    }

    @JsonIgnore
    public StringBuilder getName(){
        StringBuilder employeeName = new StringBuilder();
        if (StringUtils.isNotBlank(getFirstName())) {
            employeeName = employeeName.append(getFirstName());
        }
        if (StringUtils.isNotBlank(getLastName())) {
            employeeName = employeeName.append(StringUtils.SPACE).append(getLastName());
        }
        return employeeName;
    }
}