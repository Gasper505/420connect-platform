package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.LinkedHashSet;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeOnFleetInfo {

    private String shopId;
    private String onFleetWorkerId;
    private LinkedHashSet<String> onFleetTeamList;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getOnFleetWorkerId() {
        return onFleetWorkerId;
    }

    public void setOnFleetWorkerId(String onFleetWorkerId) {
        this.onFleetWorkerId = onFleetWorkerId;
    }

    public LinkedHashSet<String> getOnFleetTeamList() {
        return onFleetTeamList;
    }

    public void setOnFleetTeamList(LinkedHashSet<String> onFleetTeamList) {
        this.onFleetTeamList = onFleetTeamList;
    }
}
