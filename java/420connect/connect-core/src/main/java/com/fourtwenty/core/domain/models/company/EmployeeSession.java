package com.fourtwenty.core.domain.models.company;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by Stephen Schmidt on 9/2/2015.
 */
public class EmployeeSession extends ShopBaseModel {
    private String terminalId;
    private String employeeId;
    private String timeCardId;
    private Long startTime;
    private Long endTime;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTimeCardId() {
        return timeCardId;
    }

    public void setTimeCardId(String timeCardId) {
        this.timeCardId = timeCardId;
    }
}
