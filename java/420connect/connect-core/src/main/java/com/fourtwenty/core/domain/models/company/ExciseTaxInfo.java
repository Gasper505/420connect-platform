package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
@CollectionName(name = "excise_taxes", uniqueIndexes = {"{state:1, country : 1, cannabisTaxType: 1}"})
public class ExciseTaxInfo extends BaseModel implements OnPremSyncable {
    public enum ExciseTaxType {
        PER_ITEM,
        TOTAL_AMOUNT
    }

    public enum CannabisTaxType {
        Cannabis,
        NonCannabis
    }
    private String state;
    private String country;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal stateMarkUp = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal exciseTax = new BigDecimal(0);
    private CannabisTaxType cannabisTaxType = CannabisTaxType.Cannabis;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public BigDecimal getStateMarkUp() {
        return stateMarkUp;
    }

    public void setStateMarkUp(BigDecimal stateMarkUp) {
        this.stateMarkUp = stateMarkUp;
    }

    public BigDecimal getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(BigDecimal exciseTax) {
        this.exciseTax = exciseTax;
    }

    public CannabisTaxType getCannabisTaxType() {
        return cannabisTaxType;
    }

    public void setCannabisTaxType(CannabisTaxType cannabisTaxType) {
        this.cannabisTaxType = cannabisTaxType;
    }
}
