package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

/**
 * Created by mdo on 7/23/17.
 */
@CollectionName(name = "invalid_phones", uniqueIndexes = {"{phoneNumber:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvalidPhone extends BaseModel {
    private String phoneNumber;
    private boolean valid = false;
    private boolean optOut = false;

    public boolean isOptOut() {
        return optOut;
    }

    public void setOptOut(boolean optOut) {
        this.optOut = optOut;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
