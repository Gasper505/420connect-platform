package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

/**
 * Created by decipher on 22/11/17 11:52 AM
 * Raja (Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "invite_employees", uniqueIndexes = {"{companyId:1,email:1}", "{companyId:1,pin:1}"}, indexes = {"{email:1,role:1}", "{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteEmployee extends Employee {

    private String employeeId;

    private long registrationDate;

    private boolean registered = false;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
}