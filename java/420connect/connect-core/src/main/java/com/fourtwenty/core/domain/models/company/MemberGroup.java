package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;


@CollectionName(name = "member_groups", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberGroup extends ShopBaseModel implements OnPremSyncable {
    @NotEmpty
    private String name;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);
    private boolean defaultGroup = false;
    private boolean active = true;

    private int memberCount = 0;
    private int memberCountTextOptIn = 0;
    private int memberCountEmailOptIn = 0;

    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Percentage;

    private boolean enablePromotion = Boolean.FALSE;
    private String promotionId;

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public int getMemberCountEmailOptIn() {
        return memberCountEmailOptIn;
    }

    public void setMemberCountEmailOptIn(int memberCountEmailOptIn) {
        this.memberCountEmailOptIn = memberCountEmailOptIn;
    }

    public int getMemberCountTextOptIn() {
        return memberCountTextOptIn;
    }

    public void setMemberCountTextOptIn(int memberCountTextOptIn) {
        this.memberCountTextOptIn = memberCountTextOptIn;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDefaultGroup() {
        return defaultGroup;
    }

    public void setDefaultGroup(boolean defaultGroup) {
        this.defaultGroup = defaultGroup;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    public boolean isEnablePromotion() {
        return enablePromotion;
    }

    public void setEnablePromotion(boolean enablePromotion) {
        this.enablePromotion = enablePromotion;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }
}