package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NonCannabisTaxInfo {
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed
    private TaxInfo taxInfo = new TaxInfo();

    private boolean useComplexTax = false;
    private List<CompoundTaxTable> taxTables = new ArrayList<>();
    private boolean enableNonCannabisTax;
    private ExciseTaxInfo.ExciseTaxType nonCannabisType = ExciseTaxInfo.ExciseTaxType.TOTAL_AMOUNT;

    private boolean nalExciseFromRetailCost = false; // default is false
    private boolean alExciseOnZeroPrice = false; // default is false

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public TaxInfo getTaxInfo() {
        return taxInfo;
    }

    public void setTaxInfo(TaxInfo taxInfo) {
        this.taxInfo = taxInfo;
    }

    public boolean isUseComplexTax() {
        return useComplexTax;
    }

    public void setUseComplexTax(boolean useComplexTax) {
        this.useComplexTax = useComplexTax;
    }

    public List<CompoundTaxTable> getTaxTables() {
        return taxTables;
    }

    public void setTaxTables(List<CompoundTaxTable> taxTables) {
        this.taxTables = taxTables;
    }

    public boolean isEnableNonCannabisTax() {
        return enableNonCannabisTax;
    }

    public void setEnableNonCannabisTax(boolean enableNonCannabisTax) {
        this.enableNonCannabisTax = enableNonCannabisTax;
    }

    public ExciseTaxInfo.ExciseTaxType getNonCannabisType() {
        return nonCannabisType;
    }

    public void setNonCannabisType(ExciseTaxInfo.ExciseTaxType nonCannabisType) {
        this.nonCannabisType = nonCannabisType;
    }

    public boolean isNalExciseFromRetailCost() {
        return nalExciseFromRetailCost;
    }

    public void setNalExciseFromRetailCost(boolean nalExciseFromRetailCost) {
        this.nalExciseFromRetailCost = nalExciseFromRetailCost;
    }

    public boolean isAlExciseOnZeroPrice() {
        return alExciseOnZeroPrice;
    }

    public void setAlExciseOnZeroPrice(boolean alExciseOnZeroPrice) {
        this.alExciseOnZeroPrice = alExciseOnZeroPrice;
    }
}
