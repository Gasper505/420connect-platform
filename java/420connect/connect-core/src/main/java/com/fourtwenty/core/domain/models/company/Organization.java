package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.BaseModel;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "organizations", uniqueIndexes = {"{name:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Organization extends BaseModel implements InternalAllowable, OnPremSyncable {

    private String externalId;
    private String name;
    private List<String> companyIds = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCompanyIds() {
        return companyIds;
    }

    public void setCompanyIdss(List<String> companyIds) {
        this.companyIds = companyIds;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getExternalId() {
        return this.externalId;
    }
}