package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 12/7/16.
 */
@CollectionName(name = "paidio_items", indexes = {"{companyId:1,shopId:1,cdSessionId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaidInOutItem extends ShopBaseModel {
    public enum PaidInOutType {
        PaidIn,
        PaidOut,
        CashDrop,
    }

    private String cdSessionId;
    private String employeeId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    private String reason;
    private String terminalId;
    private String toTerminalId;
    private String cashDropFromTerminalId;
    private String otherPaidItemId;
    private boolean cashVault = false;
    private PaidInOutType type = PaidInOutType.PaidIn;
    private boolean carryOver = false;

    public String getOtherPaidItemId() {
        return otherPaidItemId;
    }

    public void setOtherPaidItemId(String otherPaidItemId) {
        this.otherPaidItemId = otherPaidItemId;
    }

    public String getCashDropFromTerminalId() {
        return cashDropFromTerminalId;
    }

    public void setCashDropFromTerminalId(String cashDropFromTerminalId) {
        this.cashDropFromTerminalId = cashDropFromTerminalId;
    }

    public boolean isCarryOver() {
        return carryOver;
    }

    public void setCarryOver(boolean carryOver) {
        this.carryOver = carryOver;
    }

    public boolean isCashVault() {
        return cashVault;
    }

    public void setCashVault(boolean cashVault) {
        this.cashVault = cashVault;
    }

    public PaidInOutType getType() {
        return type;
    }

    public void setType(PaidInOutType type) {
        this.type = type;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCdSessionId() {
        return cdSessionId;
    }

    public void setCdSessionId(String cdSessionId) {
        this.cdSessionId = cdSessionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getToTerminalId() {
        return toTerminalId;
    }

    public void setToTerminalId(String toTerminalId) {
        this.toTerminalId = toTerminalId;
    }
}