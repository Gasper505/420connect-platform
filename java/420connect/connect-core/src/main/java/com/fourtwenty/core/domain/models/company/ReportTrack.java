package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.ReportTypeDeSerializer;
import com.fourtwenty.core.domain.serializers.ReportTypeSerializer;
import com.fourtwenty.core.reporting.ReportType;

@CollectionName(name = "reports_track", uniqueIndexes = {"{companyId:1,shopId:1,reportType:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportTrack extends ShopBaseModel {

    public enum ReportSectionType {
        REPORT,
        DASHBOARD
    }

    @JsonSerialize(using = ReportTypeSerializer.class)
    @JsonDeserialize(using = ReportTypeDeSerializer.class)
    private ReportType reportType;
    private Integer count;

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
