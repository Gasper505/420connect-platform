package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.google.common.collect.Lists;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;

/**
 * Created by mdo on 6/15/16.
 */
@CollectionName(name = "roles", uniqueIndexes = {"{companyId:1,name:1}"}, indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role extends CompanyBaseModel implements OnPremSyncable {
    public enum Permission {
        None("", "", "", "None", "Web"),
        //iPad
        iPadMemberAdd("You do not have permission to add members.", "Add member in iPad", "It will have permission to add members.", "Members", "iPad"),
        iPadMemberEdit("You do not have permission to edit members.", "Edit member in iPad", "It will have permission to edit members.", "Members", "iPad"),
        iPadQueueAddMember("You do not have permission to add members to queue.", "Queue add member in iPad", "It will have permission to add members to queue.", "Members", "iPad"),
        iPadTransactionsViews("You do not have permission to view transactions.", "Transaction view in iPad", "It will have permission to view transactions.", "Transactions", "iPad"),
        iPadProcessSales("You do not have permission to process sales.", "Process sale in iPad", "It will have permission to process sales.", "POS", "iPad"),
        iPadRefund("You do not have permission to refund items.", "Refund in iPad", "It will have permission to refund items.", "Transactions", "iPad"),
        iPadApplyCartDiscounts("You do not have permission to apply cart discounts.", "Apply cart discount in iPad", "It will have permission to apply cart discounts.", "POS", "iPad"),
        iPadApplyLineItemDiscounts("You do not have permission to apply line item discounts.", "Apply line item discount in iPad", "It will have permission to apply line item discounts.", "POS", "iPad"),
        iPadApplyCoupons("You do not have permission to apply coupons.", "Apply coupon in iPad", "It will have permission to apply coupons.", "POS", "iPad"),
        iPadCashDrawerOpen("You do not have permission to open cash drawer.", "Cash drawer session open in iPad", "It will have permission to open cash drawer session.", "Cash Drawers", "iPad"),
        iPadCashDrawerCloseOut("You do not have permission to close out cash drawer.", "Cash drawer session close out in iPad", "It will have permission to close out cash drawer session.", "Cash Drawers", "iPad"),
        iPadTransactionsLookup("You do not have permission to look up transactions.", "Transaction lookup in iPad", "It will have permission to look up transaction.", "Transactions", "iPad"),
        iPadInitiation("You do not have permission to initiate iPad terminals.", "Initiation in iPad", "It will have permission to initiation iPad terminals.", "Others", "iPad"),
        iPadQuickPinLogin("You do not have permission to log in with Quick Pin", "Quick pin login in iPad", "It will have permission to log in with Quick Pin.", "Others", "iPad"),
        iPadManageSettingsView("You do not have permission to access manager settings.", "Manager settings view in iPad", "It will have permission to access manager settings.", "Others", "iPad"),
        iPadManagerSettingsManage("You do not have manager permissions.", "Manager settings manage in iPad", "It will have manager permissions.", "Others", "iPad"),
        // Web
        WebAdminLogin("You do not have access to this site.", "Web Login", "Ability to log in to web panel.", "Others", "Web"),
        WebAllMembersView("You do not have permission to view members section.", "All member view in web", "It will have permission to view all members section.", "Members", "Web"),
        WebMembersView("You do not have permission to view members section.", "Member view in web", "It will have permission to view members section.", "Members", "Web"),
        WebMembersManage("You do not have permission to manage members section.", "Member manage in web", "It will have permission to manage members section.", "Members", "Web"),
        WebMembersAddToQueue("You do not have permission to add member to queue.", "Member add to queue in web", "It will have permission to add member to queue.", "Members", "Web"),
        WebPromotionsView("You do not have permission to view promotions section.", "Promotion view in web", "It will have permission to view promotions section.", "Others", "Web"),
        WebPromotionsManage("You do not have permission to manage promotions section.", "Promotion manage in web", "It will have permission to manage promotions section.", "Others", "Web"),
        WebTransactionsView("You do not have permission to view transactions.", "Transaction view in web", "It will have permission to view transactions.", "Transactions", "Web"),
        WebTransactionsRefund("You do not have permission to refund transactions.", "Transaction refund in web", "It will have permission to refund transactions.", "Transactions", "Web"),
        WebInventoryView("You do not have permission to view inventory section.", "Inventory view in web", "It will have permission to view inventory section.", "Inventory", "Web"),
        WebInventoryManage("You do not have permission to manage inventory section.", "Inventory manage in web", "It will have permission to manage inventory section.", "Inventory", "Web"),
        WebInventoryTransfer("You do not have permission to transfer inventory section.", "Inventory transfer in web", "It will have permission to transfer inventory section.", "Inventory", "Web"),
        WebInventoryCategoryManage("You do not have permission to manage categories section.", "Inventory category manage in web", "It will have permission to manage categories section.", "Inventory", "Web"),
        WebEmployeeView("You do not have permission to view employee section.", "Employee view in web", "It will have permission to view employee section.", "Employees", "Web"),
        WebEmployeeManage("You do not have permission to manage employee section.", "Employee manage in web", "It will have permission to manage employee section.", "Employees", "Web"),
        WebEmployeeClockout("You do not have permission to clock out employees.", "Employee clock out in web", "It will have permission to clock out employees.", "Employees", "Web"),
        WebVendorView("You do not have permission to view vendor section.", "Vendor view in web", "It will have permission to view vendor section.", "Vendors", "Web"),
        WebVendorManage("You do not have permission to manage vendor section.", "Vendor manage in web", "It will have permission to manage vendor section.", "Vendors", "Web"),
        WebSettingsView("You do not have permission to view settings section.", "Settings view in web", "It will have permission to view settings section.", "Others", "Web"),
        WebSettingsManage("You do not have permission to manage settings section.", "Settings manage in web", "It will have permission to manage settings section.", "Others", "Web"),
        WebPhysiciansView("You do not have permission to view physicians section.", "Physician view in web", "It will have permission to view physicians section.", "Others", "Web"),
        WebPhysiciansManage("You do not have permission to manage physicians section.", "Physician manage in web", "It will have permission to manage physicians section.", "Others", "Web"),
        WebReportsView("You do not have permission to view reports section.", "Report view in web", "It will have permission to view reports section.", "Others", "Web"),
        WebDeliveryView("You do not have permission to view delivery section.", "Delivery view in web", "It will have permission to view delivery section.", "Others", "Web"),
        WebCashDrawerView("You do not have permission to view cash drawer section.", "Cash drawer view in web", "It will have permission to view cash drawer section.", "Cash Drawers", "Web"),
        WebUnlockOrder("You do not have permission to unlock order", "Unlock order in web", "It will have permission to unlock order", "POS", "Web"),
        WebEditBatch("You do not have permission to edit batch", "Edit batch in web", "It will have permission to edit batch", "Inventory", "Web"),
        WebAddBatch("You do not have permission to add batch", "Add batch in web", "It will have permission to add batch", "Inventory", "Web"),
        WebInvoiceView("You do not have permission to view invoice", "Invoice view in web", "It will have permission to view invoice", "Invoice", "Web"),
        WebInvoiceManage("You do not have permission to manage invoice section.", "Invoice manage in web", "It will have permission to manage invoice section.", "Invoice", "Web"),
        WebInventoryReconciliation("You do not have permission to inventory reconciliation section.", "Inventory reconciliation manage in web", "It will have permission to inventory reconciliation section.", "Inventory", "Web"),
        WebDashboardView("You do not have permission to view dashboard.", "Dashboard view in web", "It will have permission to dashboard.", "Dashboard", "Web"),
        WebPurchaseOrderView("You do not have permission to view purchase order.", "Purchase order view in web", "It will have permission to view purchase order.", "Purchase order", "Web"),
        WebPurchaseOrderManage("You do not have permission to manage purchase order.", "Purchase order manage in web", "It will have permission to manage purchase order.", "Purchase order", "Web"),
        WebExpenseView("You do not have permission to view expense.", "Expense view in web", "It will have permission to view expense.", "Expense", "Web"),
        WebExpenseManage("You do not have permission to manage expense.", "Expense manage in web", "It will have permission to manage expense.", "Expense", "Web"),
        WebQuickBookManage("You do not have permission to manage quick book.", "Quick book manage in web", "It will have permission to manage quick book.", "Others", "Web"),
        WebInvoiceManifestChange("You do not have permission to revert/reject manifest.", "Invoice manifest update in web", "It will have permission to manage invoice manifest.", "Invoice", "Web"),
        WebEditPosPayment("You do not have permission to edit payment of transaction", "Edit transaction payment method", "It will have permission to update payment method of completed transactions", "POS", "Web");

        private String errorMsg;
        private String permission;
        private String description;
        private String group;
        private String section;

        private static Map<Permission, List<Permission>> toDisablePermission = new HashMap<>();
        private static Map<Permission, List<Permission>> toEnablePermission = new HashMap<>();

        Permission(String errorMsg, String permission, String description, String group, String section) {
            this.errorMsg = errorMsg;
            this.permission = permission;
            this.description = description;
            this.group = group;
            this.section = section;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public String getPermission() {
            return permission;
        }

        public String getDescription() {
            return description;
        }

        public String getGroup() {
            return group;
        }

        public String getSection() {
            return section;
        }

        private static void buildEnabled() {
            toEnablePermission.put(WebEmployeeManage, Lists.newArrayList(WebEmployeeView));
            toEnablePermission.put(WebMembersManage, Lists.newArrayList(WebMembersView, WebAllMembersView));
            toEnablePermission.put(WebMembersAddToQueue, Lists.newArrayList(WebMembersView, WebAllMembersView));
            toEnablePermission.put(WebPromotionsManage, Lists.newArrayList(WebPromotionsView));
            toEnablePermission.put(WebInventoryManage, Lists.newArrayList(WebInventoryView));
            toEnablePermission.put(WebInventoryTransfer, Lists.newArrayList(WebInventoryView));
            toEnablePermission.put(WebInventoryCategoryManage, Lists.newArrayList(WebInventoryView));
            toEnablePermission.put(WebVendorManage, Lists.newArrayList(WebVendorView));
            toEnablePermission.put(WebSettingsManage, Lists.newArrayList(WebSettingsView));
            toEnablePermission.put(WebPhysiciansManage, Lists.newArrayList(WebPhysiciansView));
            toEnablePermission.put(WebTransactionsRefund, Lists.newArrayList(WebTransactionsView));
            toEnablePermission.put(iPadManagerSettingsManage, Lists.newArrayList(iPadManageSettingsView));
            toEnablePermission.put(iPadRefund, Lists.newArrayList(iPadTransactionsViews, iPadTransactionsLookup));
            toEnablePermission.put(iPadApplyCartDiscounts, Lists.newArrayList(iPadTransactionsViews, iPadTransactionsLookup));
            toEnablePermission.put(iPadApplyLineItemDiscounts, Lists.newArrayList(iPadTransactionsViews, iPadTransactionsLookup));
            toEnablePermission.put(WebInvoiceManage, Lists.newArrayList(WebInvoiceView));
            toEnablePermission.put(WebInventoryReconciliation, Lists.newArrayList(WebInventoryView));
            toEnablePermission.put(WebPurchaseOrderManage, Lists.newArrayList(WebPurchaseOrderView));
            toEnablePermission.put(WebExpenseManage, Lists.newArrayList(WebExpenseView));
            toEnablePermission.put(WebInvoiceManifestChange, Lists.newArrayList(WebInvoiceView));
            toEnablePermission.put(WebEditPosPayment, Lists.newArrayList(WebTransactionsView));
        }

        private static void buildDisabled() {
            toDisablePermission.put(WebEmployeeView, Lists.newArrayList(WebEmployeeManage));
            toDisablePermission.put(WebMembersView, Lists.newArrayList(WebMembersManage, WebMembersAddToQueue, iPadMemberAdd, iPadMemberEdit, iPadQueueAddMember));
            toDisablePermission.put(WebAllMembersView, Lists.newArrayList(WebMembersView, WebMembersManage, WebMembersAddToQueue, iPadMemberAdd, iPadMemberEdit, iPadQueueAddMember));
            toDisablePermission.put(WebPromotionsView, Lists.newArrayList(WebPromotionsManage));
            toDisablePermission.put(WebInventoryView, Lists.newArrayList(WebInventoryManage, WebInventoryTransfer, WebInventoryCategoryManage, WebInventoryReconciliation));
            toDisablePermission.put(WebVendorView, Lists.newArrayList(WebVendorManage));
            toDisablePermission.put(WebSettingsView, Lists.newArrayList(WebSettingsManage, iPadManageSettingsView, iPadManagerSettingsManage));
            toDisablePermission.put(WebPhysiciansView, Lists.newArrayList(WebPhysiciansManage));
            toDisablePermission.put(WebTransactionsView, Lists.newArrayList(WebTransactionsRefund, iPadRefund, iPadTransactionsViews, iPadTransactionsLookup));
            toDisablePermission.put(iPadManageSettingsView, Lists.newArrayList(iPadManagerSettingsManage));
            toDisablePermission.put(iPadProcessSales, Lists.newArrayList(iPadApplyCoupons, iPadApplyCartDiscounts, iPadApplyLineItemDiscounts));
            toDisablePermission.put(iPadTransactionsLookup, Lists.newArrayList(iPadRefund, iPadApplyCartDiscounts, iPadApplyLineItemDiscounts));
            toDisablePermission.put(WebCashDrawerView, Lists.newArrayList(iPadCashDrawerOpen, iPadCashDrawerCloseOut));
            toDisablePermission.put(WebInvoiceView, Lists.newArrayList(WebInvoiceManage));
            toDisablePermission.put(WebPurchaseOrderView, Lists.newArrayList(WebPurchaseOrderManage));
            toDisablePermission.put(WebExpenseView, Lists.newArrayList(WebExpenseManage));
            toDisablePermission.put(WebInvoiceManifestChange, Lists.newArrayList(WebInvoiceView));
            toEnablePermission.put(WebEditPosPayment, Lists.newArrayList(WebTransactionsView));
        }

        public static List<Permission> getDisabledPermission(Permission permission) {
            if (toDisablePermission.isEmpty()) {
                buildDisabled();
            }
            return toDisablePermission.get(permission);
        }

        public static List<Permission> getEnabledPermission(Permission permission) {
            if (toEnablePermission.isEmpty()) {
                buildEnabled();
            }
            return toEnablePermission.get(permission);
        }

    }

    private Set<Permission> permissions = new HashSet<>();

    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
}
