package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.receipt.ReceiptInfo;
import com.fourtwenty.core.domain.models.store.OnlineStoreInfo;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.util.DateUtil;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;


@CollectionName(name = "shops", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shop extends CompanyBaseModel implements OnPremSyncable {
    private static final int DEFAULT_PIN_TIMEOUT_DURATION = 5; // 3 minut
    private static final int DEFAULT_AGE_LIMIT = 21; // 3 minut
    private static final int DEFAULT_HANDOVER_DURATION = 5; // 5 minutes

    public enum ShopType {
        Medicinal,
        Recreational,
        Both;

        public static ShopType toShopType(String name) {
            if (name == null) {
                return Medicinal;
            }
            if (name.equalsIgnoreCase("Medicinal")) {
                return Medicinal;
            } else if (name.equalsIgnoreCase("Recreational")) {
                return Recreational;
            } else {
                return Both;
            }
        }
    }


    public enum ShopCheckoutType {
        Direct,
        Fulfillment,
        FulfillmentThreeStep
    }

    public enum TaxRoundOffType {
        ONE_CENT,
        FIVE_CENT, // CASH ONLY
        NEAREST_DOLLAR, // EVERYTHING
        UP_DOLLAR, // EVERYTHING
        DOWN_DOLLAR // EVERYTHTING
    }

    @Deprecated
    public enum TotalRoundOffType {
        NONE, ROUND_UP, ROUND_DOWN
    }

    public enum FeeRefundType {
        NONE,
        FULL,
        PARTIAL
    }

    public enum TerminalSalesOption {
        DeviceTerminal,
        AssignedEmployeeTerminal,
        SellerAssignedTerminal
    }

    private String shortIdentifier;
    @NotEmpty
    private String name;
    private ShopType shopType;
    private Address address;
    private String phoneNumber;
    private String emailAdress;
    private String license;
    private boolean enableDeliveryFee = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryFee = new BigDecimal(0);
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed
    private TaxInfo taxInfo;
    private boolean showWalkInQueue = true;
    private boolean showDeliveryQueue = true;
    private boolean showOnlineQueue = false;
    private boolean enableCashInOut = false;
    private String timeZone = ConnectAuthToken.DEFAULT_REQ_TIMEZONE;
    private double latitude;
    private double longitude;

    private boolean active = true;
    private Long snapshopTime;
    private Long nextSnapshotTime;
    private String defaultCountry;
    private OnlineStoreInfo onlineStoreInfo = new OnlineStoreInfo();
    private List<DeliveryFee> deliveryFees = new ArrayList<>();
    private boolean enableSaleLogout = false;
    private List<CompanyAsset> assets = new ArrayList<>();
    private boolean enableBCCReceipt = false;
    private String bccEmailAddress;
    private boolean enableGPSTracking = false;
    private String receivingInventoryId;
    private int defaultPinTimeout = DEFAULT_PIN_TIMEOUT_DURATION;
    private boolean showSpecialQueue = false;
    private List<String> emailList = new ArrayList<>();
    private boolean enableLowInventoryEmail;
    private boolean restrictedViews = false;
    private String emailMessage;
    private TaxRoundOffType taxRoundOffType = TaxRoundOffType.ONE_CENT;
    private boolean enforceCashDrawers = false; // this enable require cash drawers before a sale can be made
    @Deprecated
    private boolean useAssignedEmployee = false;
    private TerminalSalesOption termSalesOption = TerminalSalesOption.DeviceTerminal;
    private boolean showProductByAvailableQuantity = false;
    private boolean autoCashDrawer = false;
    private int numAllowActiveTrans = 1;
    private boolean requireValidRecDate = false;
    private boolean enableDeliverySignature = false;
    private boolean restrictIncomingOrderNotifications;
    private List<String> restrictedNotificationTerminals = new ArrayList<>();
    @Deprecated
    private TotalRoundOffType roundOffType = TotalRoundOffType.NONE;
    private String roundUpMessage;
    private Long membersCountSyncDate = null;

    // cannabis limit
    private boolean enableCannabisLimit = false;

    // New Tax Stuff
    private boolean useComplexTax = false;
    private List<CompoundTaxTable> taxTables = new ArrayList<>();
    private boolean enableExciseTax;
    private ExciseTaxInfo.ExciseTaxType exciseTaxType = ExciseTaxInfo.ExciseTaxType.TOTAL_AMOUNT;

    private boolean nalExciseFromRetailCost = false; // default is false
    private boolean alExciseOnZeroPrice = false; // default is false

    private LinkedHashSet<String> marketingSources = new LinkedHashSet<>();

    private LinkedHashSet<String> productsTag = new LinkedHashSet<>();

    private CompanyAsset logo;

    private String hubId;
    private String hubName;
    private boolean enableOnFleet;
    private String onFleetApiKey;
    private String onFleetOrganizationId;
    private String onFleetOrganizationName;
    private boolean enableUnAssignedOnfleetOrder;
    private CompanyAsset emailAttachment;

    private List<ReceiptInfo> receiptInfo;

    private boolean enablePinForCashDrawer = false;
    private boolean enableWooCommerce = Boolean.FALSE;

    private CartMinimums cartMinimums = new CartMinimums();
    private List<CreditCardFee> creditCardFeeList = new ArrayList<>();
    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Retail;

    private String twilioNumber;
    private boolean enableAgeLimit = Boolean.FALSE;
    private int ageLimit = DEFAULT_AGE_LIMIT; // Default age limit is 21
    private int medicinalAge = DEFAULT_AGE_LIMIT;
    private boolean enableMedicinalAge = Boolean.FALSE;
    private FeeRefundType deliveryRefundType = FeeRefundType.NONE;
    private FeeRefundType creditFefundType = FeeRefundType.NONE;

    @Deprecated
    private boolean enableSpringBig = Boolean.FALSE;
    private boolean enableDailySummaryEmail;
    private boolean enableHarvestTax;
    private boolean enableTookan;
    private String legalLanguage;
    private Set<String> orderTags = new HashSet<>();
    private boolean allowAnonymousOrders = false;
    private Set<String> membersTag = new LinkedHashSet<>();

    private String poTermCondition;
    private String poNote;
    private String invoiceTermCondition;
    private String invoiceNote;

    private int defaultHandOverMin = DEFAULT_HANDOVER_DURATION;
    private boolean enableTip = false;
    private LinkedHashSet<String> flowerType = new LinkedHashSet<>();
    private boolean enableRefundNote = false;
    private boolean enableAutoPrintAtFulfillment = Boolean.FALSE;
    private NonCannabisTaxInfo nonCannabisTaxes = new NonCannabisTaxInfo();
    private boolean appendBrandNameWM = false;
    private List<String> blazeConnections = new ArrayList<>();
    private boolean disableMedicalProductsForAdultUse = Boolean.FALSE;
    private boolean enableDiscountNotes = Boolean.FALSE;
    private boolean enableCartDiscountNotes = Boolean.FALSE;
    private boolean enableAfterTaxDiscountNotes = Boolean.FALSE;
    private boolean checkDuplicateDl = false;

    public TerminalSalesOption getTermSalesOption() {
        return termSalesOption;
    }

    public void setTermSalesOption(TerminalSalesOption termSalesOption) {
        this.termSalesOption = termSalesOption;
    }

    public boolean isAllowAnonymousOrders() {
        return allowAnonymousOrders;
    }

    public void setAllowAnonymousOrders(boolean allowAnonymousOrders) {
        this.allowAnonymousOrders = allowAnonymousOrders;
    }

    public FeeRefundType getCreditFefundType() {
        return creditFefundType;
    }

    public void setCreditFefundType(FeeRefundType creditFefundType) {
        this.creditFefundType = creditFefundType;
    }


    public FeeRefundType getDeliveryRefundType() {
        return deliveryRefundType;
    }

    public void setDeliveryRefundType(FeeRefundType deliveryRefundType) {
        this.deliveryRefundType = deliveryRefundType;
    }

    private boolean productPriceIncludeExciseTax = Boolean.FALSE;

    public boolean isAlExciseOnZeroPrice() {
        return alExciseOnZeroPrice;
    }

    public void setAlExciseOnZeroPrice(boolean alExciseOnZeroPrice) {
        this.alExciseOnZeroPrice = alExciseOnZeroPrice;
    }

    public boolean isNalExciseFromRetailCost() {
        return nalExciseFromRetailCost;
    }

    public void setNalExciseFromRetailCost(boolean nalExciseFromRetailCost) {
        this.nalExciseFromRetailCost = nalExciseFromRetailCost;
    }

    public boolean isEnableCannabisLimit() {
        return enableCannabisLimit;
    }

    public void setEnableCannabisLimit(boolean enableCannabisLimit) {
        this.enableCannabisLimit = enableCannabisLimit;
    }

    public List<CompoundTaxTable> getTaxTables() {
        return taxTables;
    }


    public Long getMembersCountSyncDate() {
        return membersCountSyncDate;
    }

    public void setMembersCountSyncDate(Long membersCountSyncDate) {
        this.membersCountSyncDate = membersCountSyncDate;
    }

    public void setTaxTables(List<CompoundTaxTable> taxTables) {
        this.taxTables = taxTables;
    }

    public boolean isUseComplexTax() {
        return useComplexTax;
    }

    public void setUseComplexTax(boolean useComplexTax) {
        this.useComplexTax = useComplexTax;
    }

    public boolean isEnableDeliverySignature() {
        return enableDeliverySignature;
    }

    public void setEnableDeliverySignature(boolean enableDeliverySignature) {
        this.enableDeliverySignature = enableDeliverySignature;
    }

    public boolean isRequireValidRecDate() {
        return requireValidRecDate;
    }

    public void setRequireValidRecDate(boolean requireValidRecDate) {
        this.requireValidRecDate = requireValidRecDate;
    }

    public int getNumAllowActiveTrans() {
        return numAllowActiveTrans;
    }

    public void setNumAllowActiveTrans(int numAllowActiveTrans) {
        this.numAllowActiveTrans = numAllowActiveTrans;
    }

    public boolean isAutoCashDrawer() {
        return autoCashDrawer;
    }

    public void setAutoCashDrawer(boolean autoCashDrawer) {
        this.autoCashDrawer = autoCashDrawer;
    }

    public boolean isUseAssignedEmployee() {
        return this.termSalesOption == TerminalSalesOption.AssignedEmployeeTerminal;
        //return useAssignedEmployee;
    }

    public void setUseAssignedEmployee(boolean useAssignedEmployee) {
        this.useAssignedEmployee = useAssignedEmployee;
    }

    public boolean isEnforceCashDrawers() {
        return enforceCashDrawers;
    }

    public void setEnforceCashDrawers(boolean enforceCashDrawers) {
        this.enforceCashDrawers = enforceCashDrawers;
    }

    public int getDefaultPinTimeout() {
        return defaultPinTimeout;
    }

    public void setDefaultPinTimeout(int defaultPinTimeout) {
        this.defaultPinTimeout = defaultPinTimeout;
    }

    public String getShortIdentifier() {
        return shortIdentifier;
    }

    public void setShortIdentifier(String shortIdentifier) {
        this.shortIdentifier = shortIdentifier;
    }

    public boolean isEnableGPSTracking() {
        return enableGPSTracking;
    }

    public void setEnableGPSTracking(boolean enableGPSTracking) {
        this.enableGPSTracking = enableGPSTracking;
    }

    public boolean isEnableSaleLogout() {
        return enableSaleLogout;
    }

    public void setEnableSaleLogout(boolean enableSaleLogout) {
        this.enableSaleLogout = enableSaleLogout;
    }

    private ShopCheckoutType checkoutType = ShopCheckoutType.Direct;

    public ShopCheckoutType getCheckoutType() {
        return checkoutType;
    }

    public void setCheckoutType(ShopCheckoutType checkoutType) {
        this.checkoutType = checkoutType;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public OnlineStoreInfo getOnlineStoreInfo() {
        return onlineStoreInfo;
    }

    public void setOnlineStoreInfo(OnlineStoreInfo onlineStoreInfo) {
        this.onlineStoreInfo = onlineStoreInfo;
    }

    public String getDefaultCountry() {
        if (defaultCountry == null) {
            if (this.getAddress() != null) {
                return this.getAddress().getCountry();
            }
        }
        return defaultCountry;
    }

    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    public boolean isEnableDeliveryFee() {
        return enableDeliveryFee;
    }

    public void setEnableDeliveryFee(boolean enableDeliveryFee) {
        this.enableDeliveryFee = enableDeliveryFee;
    }

    public BigDecimal getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(BigDecimal deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public int getTimezoneOffsetInMinutes() {
        return DateUtil.getTimezoneOffsetInMinutes(timeZone);
    }

    public Long getSnapshopTime() {
        return snapshopTime;
    }

    public void setSnapshopTime(Long snapshopTime) {
        this.snapshopTime = snapshopTime;
    }

    public Long getNextSnapshotTime() {
        return nextSnapshotTime;
    }

    public void setNextSnapshotTime(Long nextSnapshotTime) {
        this.nextSnapshotTime = nextSnapshotTime;
    }

    public boolean isEnableCashInOut() {
        return enableCashInOut;
    }

    public void setEnableCashInOut(boolean enableCashInOut) {
        this.enableCashInOut = enableCashInOut;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ShopType getShopType() {
        return shopType;
    }

    public void setShopType(ShopType shopType) {
        this.shopType = shopType;
    }

    public boolean isShowDeliveryQueue() {
        return showDeliveryQueue;
    }

    public void setShowDeliveryQueue(boolean showDeliveryQueue) {
        this.showDeliveryQueue = showDeliveryQueue;
    }

    public boolean isShowOnlineQueue() {
        return showOnlineQueue;
    }

    public void setShowOnlineQueue(boolean showOnlineQueue) {
        this.showOnlineQueue = showOnlineQueue;
    }

    public boolean isShowWalkInQueue() {
        return showWalkInQueue;
    }

    public void setShowWalkInQueue(boolean showWalkInQueue) {
        this.showWalkInQueue = showWalkInQueue;
    }

    public TaxInfo getTaxInfo() {
        return taxInfo;
    }

    public void setTaxInfo(TaxInfo taxInfo) {
        this.taxInfo = taxInfo;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<DeliveryFee> getDeliveryFees() {
        return deliveryFees;
    }

    public void setDeliveryFees(List<DeliveryFee> deliveryFees) {
        this.deliveryFees = deliveryFees;
    }

    public boolean isEnableBCCReceipt() {
        return enableBCCReceipt;
    }

    public void setEnableBCCReceipt(boolean enableBCCReceipt) {
        this.enableBCCReceipt = enableBCCReceipt;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public String getBccEmailAddress() {
        return bccEmailAddress;
    }

    public void setBccEmailAddress(String bccEmailAddress) {
        this.bccEmailAddress = bccEmailAddress;
    }

    public String getReceivingInventoryId() {
        return receivingInventoryId;
    }

    public void setReceivingInventoryId(String receivingInventoryId) {
        this.receivingInventoryId = receivingInventoryId;
    }

    public TaxRoundOffType getTaxRoundOffType() {
        return Objects.isNull(taxRoundOffType) ? TaxRoundOffType.ONE_CENT : taxRoundOffType;
    }

    public void setTaxRoundOffType(TaxRoundOffType taxRoundOffType) {
        this.taxRoundOffType = taxRoundOffType;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public boolean isEnableLowInventoryEmail() {
        return enableLowInventoryEmail;
    }

    public void setEnableLowInventoryEmail(boolean enableLowInventoryEmail) {
        this.enableLowInventoryEmail = enableLowInventoryEmail;
    }

    public boolean isShowSpecialQueue() {
        return showSpecialQueue;
    }

    public void setShowSpecialQueue(boolean showSpecialQueue) {
        this.showSpecialQueue = showSpecialQueue;
    }

    public boolean isRestrictedViews() {
        return restrictedViews;
    }

    public void setRestrictedViews(boolean restrictedViews) {
        this.restrictedViews = restrictedViews;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public boolean isShowProductByAvailableQuantity() {
        return showProductByAvailableQuantity;
    }

    public void setShowProductByAvailableQuantity(boolean showProductByAvailableQuantity) {
        this.showProductByAvailableQuantity = showProductByAvailableQuantity;
    }

    public boolean isRestrictIncomingOrderNotifications() {
        return restrictIncomingOrderNotifications;
    }

    public void setRestrictIncomingOrderNotifications(boolean restrictIncomingOrderNotifications) {
        this.restrictIncomingOrderNotifications = restrictIncomingOrderNotifications;
    }

    public List<String> getRestrictedNotificationTerminals() {
        return restrictedNotificationTerminals;
    }

    public void setRestrictedNotificationTerminals(List<String> restrictedNotificationTerminals) {
        this.restrictedNotificationTerminals = restrictedNotificationTerminals;
    }

    public TotalRoundOffType getRoundOffType() {
        return roundOffType;
    }

    public void setRoundOffType(TotalRoundOffType roundOffType) {
        this.roundOffType = roundOffType;
    }

    public String getRoundUpMessage() {
        return roundUpMessage;
    }

    public void setRoundUpMessage(String roundUpMessage) {
        this.roundUpMessage = roundUpMessage;
    }

    public LinkedHashSet<String> getMarketingSources() {
        return marketingSources;
    }

    public void setMarketingSources(LinkedHashSet<String> marketingSources) {
        this.marketingSources = marketingSources;
    }

    public LinkedHashSet<String> getProductsTag() {
        return productsTag;
    }

    public void setProductsTag(LinkedHashSet<String> productsTag) {
        this.productsTag = productsTag;
    }

    public CompanyAsset getLogo() {
        return logo;
    }

    public void setLogo(CompanyAsset logo) {
        this.logo = logo;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public boolean isEnableOnFleet() {
        return enableOnFleet;
    }

    public void setEnableOnFleet(boolean enableOnFleet) {
        this.enableOnFleet = enableOnFleet;
    }

    public String getOnFleetApiKey() {
        return onFleetApiKey;
    }

    public void setOnFleetApiKey(String onFleetApiKey) {
        this.onFleetApiKey = onFleetApiKey;
    }

    public String getOnFleetOrganizationId() {
        return onFleetOrganizationId;
    }

    public void setOnFleetOrganizationId(String onFleetOrganizationId) {
        this.onFleetOrganizationId = onFleetOrganizationId;
    }

    public String getOnFleetOrganizationName() {
        return onFleetOrganizationName;
    }

    public void setOnFleetOrganizationName(String onFleetOrganizationName) {
        this.onFleetOrganizationName = onFleetOrganizationName;
    }

    public CompanyAsset getEmailAttachment() {
        return emailAttachment;
    }

    public void setEmailAttachment(CompanyAsset emailAttachment) {
        this.emailAttachment = emailAttachment;
    }

    public boolean isEnableExciseTax() {
        return enableExciseTax;
    }

    public void setEnableExciseTax(boolean enableExciseTax) {
        this.enableExciseTax = enableExciseTax;
    }

    public ExciseTaxInfo.ExciseTaxType getExciseTaxType() {
        return exciseTaxType;
    }

    public void setExciseTaxType(ExciseTaxInfo.ExciseTaxType exciseTaxType) {
        this.exciseTaxType = exciseTaxType;
    }

    public List<ReceiptInfo> getReceiptInfo() {
        return receiptInfo;
    }

    public void setReceiptInfo(List<ReceiptInfo> receiptInfo) {
        this.receiptInfo = receiptInfo;
    }

    public boolean isEnablePinForCashDrawer() {
        return enablePinForCashDrawer;
    }

    public void setEnablePinForCashDrawer(boolean enablePinForCashDrawer) {
        this.enablePinForCashDrawer = enablePinForCashDrawer;
    }

    public boolean isEnableWooCommerce() {
        return enableWooCommerce;
    }

    public void setEnableWooCommerce(boolean enableWooCommerce) {
        this.enableWooCommerce = enableWooCommerce;
    }

    public CartMinimums getCartMinimums() {
        return cartMinimums;
    }

    public void setCartMinimums(CartMinimums cartMinimums) {
        this.cartMinimums = cartMinimums;
    }

    public List<CreditCardFee> getCreditCardFeeList() {
        return creditCardFeeList;
    }

    public void setCreditCardFeeList(List<CreditCardFee> creditCardFeeList) {
        this.creditCardFeeList = creditCardFeeList;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }


    public String getTwilioNumber() {
        return twilioNumber;
    }

    public void setTwilioNumber(String twilioNumber) {
        this.twilioNumber = twilioNumber;
    }

    public boolean isEnableAgeLimit() {
        return enableAgeLimit;
    }

    public void setEnableAgeLimit(boolean enableAgeLimit) {
        this.enableAgeLimit = enableAgeLimit;
    }

    public int getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(int ageLimit) {
        this.ageLimit = ageLimit;
    }

    public int getMedicinalAge() {
        return medicinalAge;
    }

    public void setMedicinalAge(int medicinalAge) {
        this.medicinalAge = medicinalAge;
    }

    public boolean isEnableMedicinalAge() {
        return enableMedicinalAge;
    }

    public void setEnableMedicinalAge(boolean enableMedicinalAge) {
        this.enableMedicinalAge = enableMedicinalAge;
    }

    public boolean isEnableSpringBig() {
        return enableSpringBig;
    }

    public void setEnableSpringBig(boolean enableSpringBig) {
        this.enableSpringBig = enableSpringBig;
    }

    public boolean isProductPriceIncludeExciseTax() {
        return productPriceIncludeExciseTax;
    }

    public void setProductPriceIncludeExciseTax(boolean productPriceIncludeExciseTax) {
        this.productPriceIncludeExciseTax = productPriceIncludeExciseTax;
    }

    public boolean isEnableDailySummaryEmail() {
        return enableDailySummaryEmail;
    }

    public void setEnableDailySummaryEmail(boolean enableDailySummaryEmail) {
        this.enableDailySummaryEmail = enableDailySummaryEmail;
    }

    public boolean isEnableHarvestTax() {
        return enableHarvestTax;
    }

    public void setEnableHarvestTax(boolean enableHarvestTax) {
        this.enableHarvestTax = enableHarvestTax;
    }

    public boolean isEnableTookan() {
        return enableTookan;
    }

    public void setEnableTookan(boolean enableTookan) {
        this.enableTookan = enableTookan;
    }

    public String getLegalLanguage() {
        return legalLanguage;
    }

    public void setLegalLanguage(String legalLanguage) {
        this.legalLanguage = legalLanguage;
    }

    public Set<String> getOrderTags() {
        return orderTags;
    }

    public void setOrderTags(Set<String> orderTags) {
        this.orderTags = orderTags;
    }

    public Set<String> getMembersTag() {
        return membersTag;
    }

    public void setMembersTag(Set<String> membersTag) {
        this.membersTag = membersTag;
    }

    public String getPoTermCondition() {
        return poTermCondition;
    }

    public void setPoTermCondition(String poTermCondition) {
        this.poTermCondition = poTermCondition;
    }

    public String getPoNote() {
        return poNote;
    }

    public void setPoNote(String poNote) {
        this.poNote = poNote;
    }

    public String getInvoiceTermCondition() {
        return invoiceTermCondition;
    }

    public void setInvoiceTermCondition(String invoiceTermCondition) {
        this.invoiceTermCondition = invoiceTermCondition;
    }

    public String getInvoiceNote() {
        return invoiceNote;
    }

    public void setInvoiceNote(String invoiceNote) {
        this.invoiceNote = invoiceNote;
    }

    public int getDefaultHandOverMin() {
        return defaultHandOverMin;
    }

    public void setDefaultHandOverMin(int defaultHandOverMin) {
        this.defaultHandOverMin = defaultHandOverMin;
    }

    public boolean isEnableTip() {
        return enableTip;
    }

    public void setEnableTip(boolean enableTip) {
        this.enableTip = enableTip;
    }

    public LinkedHashSet<String> getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(LinkedHashSet<String> flowerType) {
        this.flowerType = flowerType;
    }

    public boolean isEnableRefundNote() {
        return enableRefundNote;
    }

    public void setEnableRefundNote(boolean enableRefundNote) {
        this.enableRefundNote = enableRefundNote;
    }

    public NonCannabisTaxInfo getNonCannabisTaxes() {
        return nonCannabisTaxes;
    }

    public void setNonCannabisTaxes(NonCannabisTaxInfo nonCannabisTaxes) {
        this.nonCannabisTaxes = nonCannabisTaxes;
    }

    public boolean isEnableAutoPrintAtFulfillment() {
        return enableAutoPrintAtFulfillment;
    }

    public void setEnableAutoPrintAtFulfillment(boolean enableAutoPrintAtFulfillment) {
        this.enableAutoPrintAtFulfillment = enableAutoPrintAtFulfillment;
    }

    @JsonIgnore
    public Boolean isRetail() {
        return appTarget.equals(CompanyFeatures.AppTarget.Retail) ? Boolean.TRUE : Boolean.FALSE;
    }

    @JsonIgnore
    public Boolean isGrow() {
        return appTarget == CompanyFeatures.AppTarget.Grow;
    }

    @JsonIgnore
    public boolean isEnableNonCannabisTax() {
        return (nonCannabisTaxes != null && nonCannabisTaxes.isEnableNonCannabisTax());
    }

    public boolean isAppendBrandNameWM() {
        return appendBrandNameWM;
    }

    public void setAppendBrandNameWM(boolean appendBrandNameWM) {
        this.appendBrandNameWM = appendBrandNameWM;
    }

    public List<String> getBlazeConnections() {
        return blazeConnections;
    }

    public void setBlazeConnections(List<String> blazeConnections) {
        this.blazeConnections = blazeConnections;
    }

    public boolean isDisableMedicalProductsForAdultUse() {
        return disableMedicalProductsForAdultUse;
    }

    public void setDisableMedicalProductsForAdultUse(boolean disableMedicalProductsForAdultUse) {
        this.disableMedicalProductsForAdultUse = disableMedicalProductsForAdultUse;
    }

    public boolean isEnableDiscountNotes() {
        return enableDiscountNotes;
    }

    public void setEnableDiscountNotes(boolean enableDiscountNotes) {
        this.enableDiscountNotes = enableDiscountNotes;
    }

    public boolean isEnableCartDiscountNotes() {
        return enableCartDiscountNotes;
    }

    public void setEnableCartDiscountNotes(boolean enableCartDiscountNotes) {
        this.enableCartDiscountNotes = enableCartDiscountNotes;
    }

    public boolean isEnableAfterTaxDiscountNotes() {
        return enableAfterTaxDiscountNotes;
    }

    public void setEnableAfterTaxDiscountNotes(boolean enableAfterTaxDiscountNotes) {
        this.enableAfterTaxDiscountNotes = enableAfterTaxDiscountNotes;
    }

    public boolean isCheckDuplicateDl() {
        return checkDuplicateDl;
    }

    public void setCheckDuplicateDl(boolean checkDuplicateDl) {
        this.checkDuplicateDl = checkDuplicateDl;
    }

    public boolean isEnableUnAssignedOnfleetOrder() {
        return enableUnAssignedOnfleetOrder;
    }

    public void setEnableUnAssignedOnfleetOrder(boolean enableUnAssignedOnfleetOrder) {
        this.enableUnAssignedOnfleetOrder = enableUnAssignedOnfleetOrder;
    }
}
