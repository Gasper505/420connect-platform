package com.fourtwenty.core.domain.models.company;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 12/29/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignedContract extends ShopBaseModel {
    @NotEmpty
    private String contractId;
    private String membershipId;
    private Long signedDate;
    private CompanyAsset signaturePhoto;
    private boolean signedDigitally = false;
    private String consumerId;
    private String memberName;
    private String employeeName;
    private CompanyAsset employeeSignaturePhoto;
    private String witnessName;
    private CompanyAsset witnessSignaturePhoto;

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public boolean isSignedDigitally() {
        return signedDigitally;
    }

    public void setSignedDigitally(boolean signedDigitally) {
        this.signedDigitally = signedDigitally;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public CompanyAsset getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(CompanyAsset signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public Long getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(Long signedDate) {
        this.signedDate = signedDate;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getWitnessName() {
        return witnessName;
    }

    public void setWitnessName(String witnessName) {
        this.witnessName = witnessName;
    }

    public CompanyAsset getEmployeeSignaturePhoto() {
        return employeeSignaturePhoto;
    }

    public void setEmployeeSignaturePhoto(CompanyAsset employeeSignaturePhoto) {
        this.employeeSignaturePhoto = employeeSignaturePhoto;
    }

    public CompanyAsset getWitnessSignaturePhoto() {
        return witnessSignaturePhoto;
    }

    public void setWitnessSignaturePhoto(CompanyAsset witnessSignaturePhoto) {
        this.witnessSignaturePhoto = witnessSignaturePhoto;
    }
}
