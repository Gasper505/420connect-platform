package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalPercentageSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by Stephen Schmidt on 9/2/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxInfo extends BaseModel {
    public enum TaxType {
        Default, // Same as inherit. @Deprecated and use Inherit instead
        Inherit, // Inherits from the shop tax. This is default
        Custom, // Custom implies user can specify custom tax for the product
        Exempt
    }

    public enum TaxProcessingOrder {
        PostTaxed,
        PreTaxed
    }

    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    private BigDecimal cityTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    private BigDecimal stateTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    private BigDecimal federalTax = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    @JsonIgnore
    private BigDecimal totalTax = new BigDecimal(0);

    public BigDecimal getCityTax() {
        return cityTax;
    }

    public void setCityTax(BigDecimal cityTax) {
        this.cityTax = cityTax;
    }

    public BigDecimal getFederalTax() {
        return federalTax;
    }

    public void setFederalTax(BigDecimal federalTax) {
        this.federalTax = federalTax;
    }

    public BigDecimal getStateTax() {
        return stateTax;
    }

    public void setStateTax(BigDecimal stateTax) {
        this.stateTax = stateTax;
    }

    @JsonIgnore
    public BigDecimal getTotalTax() {
        return cityTax.add(stateTax).add(federalTax);
    }
}
