package com.fourtwenty.core.domain.models.company;

import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephen Schmidt on 9/2/2015.
 */

@CollectionName(name = "terminals", uniqueIndexes = {"{shopId:1,name:1}", "{companyId:1,deviceId:1}"}, indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Terminal extends ShopBaseModel implements OnPremSyncable {
    public enum TerminalType {
        NotAssigned,
        WalkIn,
        Delivery,
        SelfCheckout
    }

    public enum DeviceType {
        Android,
        iOS
    }

    private boolean active;
    private String deviceId;
    private String name;
    private String deviceModel;
    private String deviceVersion;
    private String deviceName;
    private String appVersion;
    private String deviceToken;
    private String deviceType;
    private String assignedInventoryId;
    private String cvAccountId;
    private String currentEmployeeId;
    private TerminalType terminalType = TerminalType.NotAssigned;

    // Credit Card Reader
    private CreditCardReader creditCardReader;
    private boolean onPremEnabled = false;

    // READONLY
    private List<TerminalLocation> terminalLocations = new ArrayList<>();

    private Shop.ShopCheckoutType checkoutType = Shop.ShopCheckoutType.Direct;

    private List<DeviceDetail> deviceDetails = new ArrayList<>();
    public List<TerminalLocation> getTerminalLocations() {
        return terminalLocations;
    }


    public TerminalType getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(TerminalType terminalType) {
        this.terminalType = terminalType;
    }

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }

    public String getCvAccountId() {
        return cvAccountId;
    }

    public void setCvAccountId(String cvAccountId) {
        this.cvAccountId = cvAccountId;
    }

    public void setTerminalLocations(List<TerminalLocation> terminalLocations) {
        this.terminalLocations = terminalLocations;
    }

    public String getAssignedInventoryId() {
        return assignedInventoryId;
    }

    public void setAssignedInventoryId(String assignedInventoryId) {
        this.assignedInventoryId = assignedInventoryId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreditCardReader getCreditCardReader() {
        return creditCardReader;
    }

    public void setCreditCardReader(CreditCardReader creditCardReader) {
        this.creditCardReader = creditCardReader;
    }

    public Shop.ShopCheckoutType getCheckoutType() {
        return checkoutType;
    }

    public void setCheckoutType(Shop.ShopCheckoutType checkoutType) {
        this.checkoutType = checkoutType;
    }

    public boolean isOnPremEnabled() {
        return onPremEnabled;
    }

    public void setOnPremEnabled(boolean onPremEnabled) {
        this.onPremEnabled = onPremEnabled;
    }

    public List<DeviceDetail> getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(List<DeviceDetail> deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    public static class DeviceDetail {
        private String deviceId;
        private String version = "1.0.0";
        private String deviceToken;
        private ConnectAuthToken.ConnectAppType appType = ConnectAuthToken.ConnectAppType.Blaze;
        private DeviceType deviceType = Terminal.DeviceType.iOS;
        private String deviceVersion;
        private String sdkVersion;
        private String deviceModel;
        private String deviceName;
        private boolean active;

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public ConnectAuthToken.ConnectAppType getAppType() {
            return appType;
        }

        public void setAppType(ConnectAuthToken.ConnectAppType appType) {
            this.appType = appType;
        }

        public DeviceType getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(DeviceType deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceVersion() {
            return deviceVersion;
        }

        public void setDeviceVersion(String deviceVersion) {
            this.deviceVersion = deviceVersion;
        }

        public String getSdkVersion() {
            return sdkVersion;
        }

        public void setSdkVersion(String sdkVersion) {
            this.sdkVersion = sdkVersion;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }
    }

    /**
     * Prepare method to get device detail for notifications
     * @param appType    : app type
     * @param deviceId   : deviceId
     * @param deviceType : deviceType
     * @param isOld      : isOld
     * @return
     */
    @JsonIgnore
    public DeviceDetail getDeviceDetail(ConnectAuthToken.ConnectAppType appType, String deviceId, DeviceType deviceType, boolean isOld) {
        DeviceDetail deviceDetail = null;
        if (!CollectionUtils.isNullOrEmpty(getDeviceDetails())) {
            deviceDetail = getDeviceDetails().stream().filter(detail ->
                    (isOld
                            && StringUtils.isNotBlank(detail.getDeviceId())
                            && detail.getDeviceId().equalsIgnoreCase(deviceId)
                            && detail.getAppType() == appType)
                            || (detail.getAppType() == appType)).findFirst().orElse(null);
        }
        if (deviceDetail == null) {
            deviceDetail = new DeviceDetail();
            deviceDetail.setDeviceId(getDeviceId());
            deviceDetail.setAppType(appType);
            deviceDetail.setDeviceToken(getDeviceToken());
            deviceDetail.setDeviceType(deviceType);
            deviceDetail.setDeviceVersion(getDeviceVersion());
            deviceDetail.setDeviceModel(getDeviceModel());
            deviceDetail.setDeviceName(getDeviceName());
            this.getDeviceDetails().add(deviceDetail);
        }
        if (!isOld) {
            toggleState(deviceDetail);
        }

        return deviceDetail;
    }
    @JsonIgnore
    private void toggleState(DeviceDetail deviceDetail) {
        getDeviceDetails().forEach(detail -> {
            detail.setActive(false);
        });
    }

    @JsonIgnore
    public ConnectAuthToken.ConnectAppType getActiveAppType() {
        DeviceDetail deviceDetail = CollectionUtils.isNullOrEmpty(getDeviceDetails()) ? null : getDeviceDetails().stream().filter(DeviceDetail::isActive).findFirst().orElse(null);
        return (deviceDetail == null) ? ConnectAuthToken.ConnectAppType.Blaze : deviceDetail.getAppType();
    }
}
