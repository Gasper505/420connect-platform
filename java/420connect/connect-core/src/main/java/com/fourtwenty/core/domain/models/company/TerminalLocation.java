package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 7/3/16.
 */

@CollectionName(name = "term_locs", premSyncDown = false, indexes = {"{companyId:1,shopId:1,terminalId:1}", "{companyId:1,shopId:1,employeeId:1}", "{companyId:1,shopId:1,created:-1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalLocation extends ShopBaseModel {
    private String terminalId;
    private String employeeId;
    private String timeCardId;
    private String deviceId;
    private String name;
    private List<Double> loc = new ArrayList<>(); // Location in [lon,lat] // array format

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTimeCardId() {
        return timeCardId;
    }

    public void setTimeCardId(String timeCardId) {
        this.timeCardId = timeCardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<Double> getLoc() {
        return loc;
    }

    public void setLoc(List<Double> loc) {
        this.loc = loc;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
}
