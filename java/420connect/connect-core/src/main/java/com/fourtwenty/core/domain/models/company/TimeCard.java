package com.fourtwenty.core.domain.models.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephen Schmidt on 9/2/2015.
 */
@CollectionName(name = "timecards", indexes = {"{companyId:1,shopId:1,employeeId:1}", "{companyId:1,shopId:1,delete:1,clockInTime:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeCard extends ShopBaseModel {
    private String employeeId;
    private Long clockInTime;
    private Long clockOutTime;
    private boolean clockin;
    private List<EmployeeSession> sessions = new ArrayList<>();

    public Long getClockInTime() {
        return clockInTime;
    }

    public void setClockInTime(Long clockInTime) {
        this.clockInTime = clockInTime;
    }

    public boolean isClockin() {
        return clockin;
    }

    public void setClockin(boolean clockin) {
        this.clockin = clockin;
    }

    public Long getClockOutTime() {
        return clockOutTime;
    }

    public void setClockOutTime(Long clockOutTime) {
        this.clockOutTime = clockOutTime;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public List<EmployeeSession> getSessions() {
        return sessions;
    }

    public void setSessions(List<EmployeeSession> sessions) {
        this.sessions = sessions;
    }
}
