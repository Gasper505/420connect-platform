package com.fourtwenty.core.domain.models.compliance;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

public class ComplianceBaseModel extends ShopBaseModel {
    private String key;
    private String searchField;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }
}
