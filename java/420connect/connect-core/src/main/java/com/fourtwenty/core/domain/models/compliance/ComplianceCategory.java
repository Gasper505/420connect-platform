package com.fourtwenty.core.domain.models.compliance;

import com.blaze.clients.metrcs.models.items.MetricsItemCategory;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties
@CollectionName(name = "compliance_categories", uniqueIndexes = {"{companyId:1,shopId:1,key:1}"})
public class ComplianceCategory extends ComplianceBaseModel {
    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;
    private MetricsItemCategory data;

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }

    public MetricsItemCategory getData() {
        return data;
    }

    public void setData(MetricsItemCategory data) {
        this.data = data;
    }
}
