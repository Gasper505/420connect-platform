package com.fourtwenty.core.domain.models.compliance;

import com.blaze.clients.metrcs.models.items.MetricsItems;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties
@CollectionName(name = "compliance_items", uniqueIndexes = {"{companyId:1,shopId:1,key:1}"})
public class ComplianceItem extends ComplianceBaseModel {
    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;
    private String productId;
    private MetricsItems data;

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public MetricsItems getData() {
        return data;
    }

    public void setData(MetricsItems data) {
        this.data = data;
    }
}
