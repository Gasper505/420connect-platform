package com.fourtwenty.core.domain.models.compliance;

import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;

import java.math.BigDecimal;

@JsonIgnoreProperties
@CollectionName(name = "compliance_packages", uniqueIndexes = {"{companyId:1,shopId:1,key:1}"})
public class CompliancePackage extends ComplianceBaseModel {
    public enum PackageStatus {
        ACTIVE,
        ONHOLD,
        INACTIVE
    }

    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;
    private PackageStatus status = PackageStatus.ACTIVE;
    private String productBatchId;
    private String productId;
    private BigDecimal blazeQuantity;
    private MetricsPackages data;
    private ProductWeightTolerance.WeightKey blazeMeasurement = ProductWeightTolerance.WeightKey.UNIT;


    public ProductWeightTolerance.WeightKey getBlazeMeasurement() {
        return blazeMeasurement;
    }

    public void setBlazeMeasurement(ProductWeightTolerance.WeightKey blazeMeasurement) {
        this.blazeMeasurement = blazeMeasurement;
    }

    public BigDecimal getBlazeQuantity() {
        return blazeQuantity;
    }

    public void setBlazeQuantity(BigDecimal blazeQuantity) {
        this.blazeQuantity = blazeQuantity;
    }

    public MetricsPackages getData() {
        return data;
    }

    public void setData(MetricsPackages data) {
        this.data = data;
    }

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }

    public PackageStatus getStatus() {
        return status;
    }

    public void setStatus(PackageStatus status) {
        this.status = status;
    }

    public String getProductBatchId() {
        return productBatchId;
    }

    public void setProductBatchId(String productBatchId) {
        this.productBatchId = productBatchId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
