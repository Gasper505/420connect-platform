package com.fourtwenty.core.domain.models.compliance;

import com.blaze.clients.metrcs.models.sales.MetricsReceipt;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties
@CollectionName(name = "compliance_sale_receipts", uniqueIndexes = {"{companyId:1,shopId:1,key:1}"})
public class ComplianceSaleReceipt extends ComplianceBaseModel {
    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;
    private MetricsReceipt data;

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }

    public MetricsReceipt getData() {
        return data;
    }

    public void setData(MetricsReceipt data) {
        this.data = data;
    }
}
