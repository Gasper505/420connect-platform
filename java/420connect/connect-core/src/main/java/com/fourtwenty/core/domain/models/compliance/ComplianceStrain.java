package com.fourtwenty.core.domain.models.compliance;

import com.blaze.clients.metrcs.models.strains.MetricsStrain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties
@CollectionName(name = "compliance_strains", uniqueIndexes = {"{companyId:1,shopId:1,key:1}"})
public class ComplianceStrain extends ComplianceBaseModel {
    public enum DefaultStrain {
        GeneralFlower("General Flower");
        DefaultStrain(String strainName) {
            this.strainName = strainName;
        }
        public String strainName;
    }

    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;
    private MetricsStrain data;

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }


    public MetricsStrain getData() {
        return data;
    }

    public void setData(MetricsStrain data) {
        this.data = data;
    }
}
