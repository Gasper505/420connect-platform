package com.fourtwenty.core.domain.models.compliance;

import com.blaze.clients.metrcs.models.strains.MetrcStrainRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductBatch;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties
@CollectionName(name = "compliance_sync_jobs", indexes = {"{companyId:1,shopId:1,created:-1}"})
public class ComplianceSyncJob  extends ShopBaseModel {
    public enum ComplianceSyncJobType {
        INTAKE_PACKAGES, // Intake packages from external system
        CREATE_EXT_PACKAGES, // create packages in the external system
        SYNC,
        SYNC_UP,
        CREATE_ITEMS,
        CREATE_STRAINS
    }

    public enum ComplianceSyncJobStatus {
        QUEUED,
        INPROGRESS,
        COMPLETE,
        ERROR
    }

    public enum ComplianceDataKeys {
        PackageTags
    }

    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;
    private ComplianceSyncJobType jobType = ComplianceSyncJobType.CREATE_EXT_PACKAGES;
    private ComplianceSyncJobStatus status = ComplianceSyncJobStatus.QUEUED;
    private String employeeId;
    private long requestTime = 0;
    private Long startTime;
    private Long completeTime;
    private String errorMsg;
    private String jobTargetId; // this is the ID of the target data model
    private List<ComplianceBatchPackagePair> packages = new ArrayList<>();
    private List<MetrcStrainRequest> strains = new ArrayList<>();

    public List<ComplianceBatchPackagePair> getPackages() {
        return packages;
    }

    public void setPackages(List<ComplianceBatchPackagePair> packages) {
        this.packages = packages;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }


    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }

    public ComplianceSyncJobType getJobType() {
        return jobType;
    }

    public void setJobType(ComplianceSyncJobType jobType) {
        this.jobType = jobType;
    }

    public ComplianceSyncJobStatus getStatus() {
        return status;
    }

    public void setStatus(ComplianceSyncJobStatus status) {
        this.status = status;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getJobTargetId() {
        return jobTargetId;
    }

    public void setJobTargetId(String jobTargetId) {
        this.jobTargetId = jobTargetId;
    }


    public List<MetrcStrainRequest> getStrains() {
        return strains;
    }

    public void setStrains(List<MetrcStrainRequest> strains) {
        this.strains = strains;
    }
}
