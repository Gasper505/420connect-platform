package com.fourtwenty.core.domain.models.compliance;


import com.blaze.clients.metrcs.models.transfers.MetricsTransfer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties
@CollectionName(name = "compliance_transfers", uniqueIndexes = {"{companyId:1,shopId:1,key:1}"})
public class ComplianceTransfer extends ComplianceBaseModel {
    private ProductBatch.TrackTraceSystem complianceType = ProductBatch.TrackTraceSystem.METRC;

    public enum ComplianceTransferStatus {
        INCOMING,
        OUTGOING,
        REJECTED
    }

    private ComplianceTransferStatus status = ComplianceTransferStatus.INCOMING;
    private MetricsTransfer data;

    public ProductBatch.TrackTraceSystem getComplianceType() {
        return complianceType;
    }

    public void setComplianceType(ProductBatch.TrackTraceSystem complianceType) {
        this.complianceType = complianceType;
    }

    public ComplianceTransferStatus getStatus() {
        return status;
    }

    public void setStatus(ComplianceTransferStatus status) {
        this.status = status;
    }

    public MetricsTransfer getData() {
        return data;
    }

    public void setData(MetricsTransfer data) {
        this.data = data;
    }
}
