package com.fourtwenty.core.domain.models.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.BaseModel;

/**
 * Created by mdo on 12/19/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerAsset extends BaseModel implements Asset {
    private String name;
    private String key;
    private Asset.AssetType type;
    private boolean active;

    public CompanyAsset toCompanyAsset(String companyId) {
        CompanyAsset companyAsset = new CompanyAsset();
        companyAsset.prepare(companyId);
        companyAsset.setName(name);
        companyAsset.setKey(key);
        companyAsset.setType(type);
        companyAsset.setActive(active);
        companyAsset.setSecured(true);

        return companyAsset;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssetType getType() {
        return type;
    }

    public void setType(AssetType type) {
        this.type = type;
    }

    @Override
    public AssetType getAssetType() {
        return type;
    }
}
