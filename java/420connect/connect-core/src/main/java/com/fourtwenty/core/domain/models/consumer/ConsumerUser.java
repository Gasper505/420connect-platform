package com.fourtwenty.core.domain.models.consumer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.IPatient;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.serializers.GenderEnumDeserializer;
import com.fourtwenty.core.domain.serializers.GenderEnumSerializer;
import com.fourtwenty.core.domain.serializers.JsonExpirationDateSerializer;
import com.fourtwenty.core.importer.main.Importable;
import org.hibernate.validator.constraints.Email;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 12/19/16.
 */
@CollectionName(name = "consumer_users", uniqueIndexes = {"{companyId:1, email:1}"}, indexes = {"{companyId:1, cpn:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerUser extends CompanyBaseModel implements IPatient, Importable {

    public enum ConsumerVerificationMethod {
        Website,
        Phone
    }

    public enum ConsumerOrderNotificationType {
        None,
        Email,
        Text
    }

    public enum RegistrationSource {
        Blaze,
        WooCommerce
    }


    @Email
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private Address address;
    private Long dob;
    private String primaryPhone;
    private String cpn; // Clean phone numbers
    private boolean textOptIn;
    private boolean emailOptIn;
    private boolean medical = false; // default is true
    private String searchText;

    @JsonSerialize(using = GenderEnumSerializer.class)
    @JsonDeserialize(using = GenderEnumDeserializer.class)
    private BaseMember.Gender sex = BaseMember.Gender.OTHER;
    private String marketingSource;
    private String sourceCompanyId;

    // Verification Info
    private ConsumerVerificationMethod verifyMethod = ConsumerVerificationMethod.Website;
    private String verificationWebsite;
    private String verificationPhone;
    private String doctorFirstName;
    private String doctorLastName;
    private String doctorLicense;
    private String dlNo;
    @JsonSerialize(using = JsonExpirationDateSerializer.class)
    private Long dlExpiration;
    private String dlState;
    private ConsumerAsset dlPhoto;
    private ConsumerAsset recPhoto;
    private String recNo;
    @JsonSerialize(using = JsonExpirationDateSerializer.class)
    private Long recExpiration;
    @JsonSerialize(using = JsonExpirationDateSerializer.class)
    private Long recIssueDate;
    private ConsumerOrderNotificationType notificationType = ConsumerOrderNotificationType.Email;
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private boolean verified;
    private boolean active = true;
    private List<String> memberIds = new ArrayList<>(); // List of memberids associated to this consumer

    @Deprecated
    private List<ConsumerMemberStatus> memberStatuses = new ArrayList<>();
    private List<SignedContract> signedContracts = new ArrayList<>();
    private RegistrationSource source = RegistrationSource.Blaze;
    private String importId;

    private String memberId;
    private boolean accepted = false;
    private Long acceptedDate;
    private String reason;
    private Long lastSyncDate;
    private Long  rejectedDate;
    private ArrayList<Address> addresses = new ArrayList<>();


    public String getMemberIdForShop(String shopId) {
        if (memberStatuses != null) {
            for (ConsumerMemberStatus memberStatus : memberStatuses) {
                if (shopId.equalsIgnoreCase(memberStatus.getShopId())) {
                    return memberStatus.getMemberId();
                }
            }
        }

        return null;
    }

    public String getMemberIdForCompany(String companyId) {
        /*if (memberStatuses != null) {
            for (ConsumerMemberStatus memberStatus : memberStatuses) {
                if (companyId.equalsIgnoreCase(memberStatus.getCompanyId())) {
                    return memberStatus.getMemberId();
                }
            }
        }*/

        return memberId;
    }


    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getCpn() {
        return cpn;
    }

    public void setCpn(String cpn) {
        this.cpn = cpn;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public String getSourceCompanyId() {
        return sourceCompanyId;
    }

    public void setSourceCompanyId(String sourceCompanyId) {
        this.sourceCompanyId = sourceCompanyId;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public Long getRecIssueDate() {
        return recIssueDate;
    }

    public void setRecIssueDate(Long recIssueDate) {
        this.recIssueDate = recIssueDate;
    }

    public ConsumerOrderNotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(ConsumerOrderNotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public List<ConsumerMemberStatus> getMemberStatuses() {
        return memberStatuses;
    }

    public void setMemberStatuses(List<ConsumerMemberStatus> memberStatuses) {
        this.memberStatuses = memberStatuses;
    }

    public List<SignedContract> getSignedContracts() {
        return signedContracts;
    }

    public void setSignedContracts(List<SignedContract> signedContracts) {
        this.signedContracts = signedContracts;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getDlExpiration() {
        return dlExpiration;
    }

    public void setDlExpiration(Long dlExpiration) {
        this.dlExpiration = dlExpiration;
    }

    public String getDlNo() {
        return dlNo;
    }

    public void setDlNo(String dlNo) {
        this.dlNo = dlNo;
    }

    public ConsumerAsset getDlPhoto() {
        return dlPhoto;
    }

    public void setDlPhoto(ConsumerAsset dlPhoto) {
        this.dlPhoto = dlPhoto;
    }

    public String getDlState() {
        return dlState;
    }

    public void setDlState(String dlState) {
        this.dlState = dlState;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getDoctorLicense() {
        return doctorLicense;
    }

    public void setDoctorLicense(String doctorLicense) {
        this.doctorLicense = doctorLicense;
    }

    public List<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(List<String> memberIds) {
        this.memberIds = memberIds;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isMedical() {
        return medical;
    }

    public void setMedical(boolean medical) {
        this.medical = medical;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public Long getRecExpiration() {
        return recExpiration;
    }

    public void setRecExpiration(Long recExpiration) {
        this.recExpiration = recExpiration;
    }

    public String getRecNo() {
        return recNo;
    }

    public void setRecNo(String recNo) {
        this.recNo = recNo;
    }

    public ConsumerAsset getRecPhoto() {
        return recPhoto;
    }

    public void setRecPhoto(ConsumerAsset recPhoto) {
        this.recPhoto = recPhoto;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public BaseMember.Gender getSex() {
        return sex;
    }

    public void setSex(BaseMember.Gender sex) {
        this.sex = sex;
    }

    public boolean isTextOptIn() {
        return textOptIn;
    }

    public void setTextOptIn(boolean textOptIn) {
        this.textOptIn = textOptIn;
    }

    public String getVerificationPhone() {
        return verificationPhone;
    }

    public void setVerificationPhone(String verificationPhone) {
        this.verificationPhone = verificationPhone;
    }

    public String getVerificationWebsite() {
        return verificationWebsite;
    }

    public void setVerificationWebsite(String verificationWebsite) {
        this.verificationWebsite = verificationWebsite;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public ConsumerVerificationMethod getVerifyMethod() {
        return verifyMethod;
    }

    public void setVerifyMethod(ConsumerVerificationMethod verifyMethod) {
        this.verifyMethod = verifyMethod;
    }

    public RegistrationSource getSource() {
        return source;
    }

    public void setSource(RegistrationSource source) {
        this.source = source;
    }

    @Override
    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public Long getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Long acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Long lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public Long getRejectedDate() {
        return rejectedDate;
    }

    public void setRejectedDate(Long rejectedDate) {
        this.rejectedDate = rejectedDate;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

}
