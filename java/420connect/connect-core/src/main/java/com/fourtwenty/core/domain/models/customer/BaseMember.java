package com.fourtwenty.core.domain.models.customer;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.DobDeserializer;
import com.fourtwenty.core.domain.serializers.DobSerializer;
import com.fourtwenty.core.domain.serializers.GenderEnumDeserializer;
import com.fourtwenty.core.domain.serializers.GenderEnumSerializer;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonValue;

public class BaseMember extends ShopBaseModel {
    public enum Gender {
        MALE(0), FEMALE(1), OTHER(2);

        private final int id;

        Gender(int id) {
            this.id = id;
        }

        @JsonValue
        public int toValue() {
            return id;
        }

        public static Gender toGender(String genderName) {
            if (StringUtils.isNotBlank(genderName)) {
                return OTHER;
            }

            if (genderName.equalsIgnoreCase("male")) {
                return MALE;
            } else if (genderName.equalsIgnoreCase("female")) {
                return FEMALE;
            } else {
                return OTHER;
            }
        }

        public static Gender toGender(int genderValue) {
            if (genderValue == 0) {
                return MALE;
            } else if (genderValue == 1) {
                return FEMALE;
            } else {
                return OTHER;
            }
        }
    }

    private String firstName;
    private String lastName;
    private String middleName;
    private Address address;
    private String uppercaseCity;

    private String email;
    @JsonSerialize(using = DobSerializer.class)
    @JsonDeserialize(using = DobDeserializer.class)
    private Long dob;
    private String primaryPhone;
    private boolean textOptIn = true;
    private boolean emailOptIn = true;
    private boolean medical;
    private String searchText;

    @JsonSerialize(using = GenderEnumSerializer.class)
    @JsonDeserialize(using = GenderEnumDeserializer.class)
    private Gender sex = Gender.OTHER;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
        if (address != null && address.getCity() != null) {
            this.uppercaseCity = address.getCity().toUpperCase();
        }
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isMedical() {
        return medical;
    }

    public void setMedical(boolean medical) {
        this.medical = medical;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Gender getSex() {
        return sex;
    }

    public void setSex(Gender sex) {
        this.sex = sex;
    }

    public boolean isTextOptIn() {
        return textOptIn;
    }

    public void setTextOptIn(boolean textOptIn) {
        this.textOptIn = textOptIn;
    }

}