package com.fourtwenty.core.domain.models.customer;

import com.fourtwenty.core.domain.interfaces.IPatient;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.product.Vendor;

/**
 * Created by mdo on 1/10/18.
 */
public class CustomerInfo {
    IPatient patient;
    MemberGroup memberGroup;
    ConsumerType consumerType = ConsumerType.AdultUse;
    String zipCode;
    Vendor.ArmsLengthType armsLengthType = Vendor.ArmsLengthType.ARMS_LENGTH; // Use for distribution
    Vendor.CompanyType companyType = Vendor.CompanyType.RETAILER;
    private boolean enableCultivationTax;

    public Vendor.CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Vendor.CompanyType companyType) {
        this.companyType = companyType;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public MemberGroup getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(MemberGroup memberGroup) {
        this.memberGroup = memberGroup;
    }

    public IPatient getPatient() {
        return patient;
    }

    public void setPatient(IPatient patient) {
        this.patient = patient;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Vendor.ArmsLengthType getArmsLengthType() {
        return armsLengthType;
    }

    public void setArmsLengthType(Vendor.ArmsLengthType armsLengthType) {
        this.armsLengthType = armsLengthType;
    }

    public boolean isEnableCultivationTax() {
        return enableCultivationTax;
    }

    public void setEnableCultivationTax(boolean enableCultivationTax) {
        this.enableCultivationTax = enableCultivationTax;
    }

    @Override
    public String toString() {
        return "CustomerInfo{" +
                "patient=" + patient +
                ", memberGroup=" + memberGroup +
                ", consumerType=" + consumerType +
                ", zipCode='" + zipCode + '\'' +
                ", armsLengthType=" + armsLengthType +
                '}';
    }
}
