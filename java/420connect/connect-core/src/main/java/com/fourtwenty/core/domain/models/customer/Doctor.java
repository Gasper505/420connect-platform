package com.fourtwenty.core.domain.models.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.importer.main.Importable;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "doctors", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Doctor extends CompanyBaseModel implements Importable {
    private String importId;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String license;
    private String website;
    private String phoneNumber;
    private String email;
    private String fax;
    private String degree;
    private String state;
    private String searchText;
    private Address address;
    private boolean active;
    private List<Note> notes = new ArrayList<>();
    private List<CompanyAsset> assets = new ArrayList<>();

    @JsonIgnore
    public boolean complete() {
        return firstName != null && lastName != null && website != null && license != null;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}