package com.fourtwenty.core.domain.models.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.serializers.JsonExpirationDateSerializer;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephen Schmidt on 9/2/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Identification extends CompanyBaseModel {
    public static final String VERIFY_TAG = "ID Verified";

    public enum IdentificationType {
        Drivers("Drivers License"),
        Passport("Passport"),
        StateId("State ID"),
        Other("");

        IdentificationType(String typeName) {
            this.typeName = typeName;
        }

        private String typeName;

        public String getTypeName() {
            return typeName;
        }

        public static IdentificationType getTypeByValue(String typeName) {
            if (StringUtils.isBlank(typeName)) return IdentificationType.Other;
            if (typeName.equals("Drivers License")) {
                return Drivers;
            } else if (typeName.equals("Passport")) {
                return Passport;
            } else if (typeName.equals("State ID")) {
                return StateId;
            } else {
                return Other;
            }
        }
    }

    private boolean verified = false;
    private String licenseNumber;
    @JsonSerialize(using = JsonExpirationDateSerializer.class)
    private Long expirationDate;
    private CompanyAsset frontPhoto;
    private List<CompanyAsset> assets = new ArrayList<>();
    private String state;
    private IdentificationType type;

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public IdentificationType getType() {
        return type;
    }

    public void setType(IdentificationType type) {
        this.type = type;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public CompanyAsset getFrontPhoto() {
        return frontPhoto;
    }

    public void setFrontPhoto(CompanyAsset frontPhoto) {
        this.frontPhoto = frontPhoto;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
