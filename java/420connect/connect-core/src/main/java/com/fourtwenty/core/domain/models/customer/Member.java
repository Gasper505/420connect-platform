package com.fourtwenty.core.domain.models.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.IPatient;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponseHit;
import com.fourtwenty.core.thirdparty.elasticsearch.models.types.Analyzer;
import com.fourtwenty.core.util.BLStringUtils;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.*;

@CollectionName(name = "members", indexes = {"{companyId:1,shopId:1,delete:1}", "{companyId:1,shopId:1,searchText:1}", "{companyId:1,shopId:1,email:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Member extends BaseMember implements Importable, IPatient, Comparable<Member>, ElasticSearchCapable, OnPremSyncable {

    public enum MembershipStatus {
        Active,
        Pending,
        Inactive
    }
    private boolean anonymous = false;
    private MembershipStatus status = MembershipStatus.Inactive;
    private List<Note> notes = new ArrayList<>();
    private List<SignedContract> contracts = new ArrayList<>();
    private List<Preference> preferences = new ArrayList<>();
    private List<Identification> identifications = new ArrayList<>();
    private List<Recommendation> recommendations = new ArrayList<>();
    private Long lastVisitDate;
    private Long startDate;
    private String importId;
    private String memberGroupId;
    private MemberGroup memberGroup;
    private List<String> recentProducts = new ArrayList<>();
    private boolean dlExpired;
    private boolean recommendationExpired;
    private boolean agreementExpired;
    private boolean emailVerified;
    private String consumerUserId; // tied to an existing consumer user
    private String marketingSource;
    private ConsumerType consumerType = ConsumerType.AdultUse;

    private boolean enableLoyalty = true;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal loyaltyPoints = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal lifetimePoints = new BigDecimal(0);
    private boolean enabledCareGiver = Boolean.FALSE;
    private List<String> careGivers = new ArrayList<>();
    private String regionId; // Correlate to a particular region
    private ArrayList<Address> addresses = new ArrayList<>();

    List<HashMap<String, String>> qbCustomerRef = new ArrayList<HashMap<String, String>>();
    private List<QBDataMapping> qbMapping;

    private boolean banPatient = false;
    private LinkedHashSet<String> tags = new LinkedHashSet<>();
    private long recommendationExpiryLeft;

    public boolean hasValidRecommendation() {
        DateTime nowTime = DateTime.now();
        nowTime = nowTime.withTimeAtStartOfDay();
        if (this.getRecommendations() != null) {
            for (Recommendation recommendation : this.getRecommendations()) {
                if (StringUtils.isNoneBlank(recommendation.getRecommendationNumber()) && recommendation.getExpirationDate() != null) {
                    DateTime recExpDate = new DateTime(recommendation.getExpirationDate());
                    if (!recExpDate.isBefore(nowTime)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public boolean isEnableLoyalty() {
        return enableLoyalty;
    }

    public void setEnableLoyalty(boolean enableLoyalty) {
        this.enableLoyalty = enableLoyalty;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public BigDecimal getLifetimePoints() {
        return lifetimePoints;
    }

    public void setLifetimePoints(BigDecimal lifetimePoints) {
        this.lifetimePoints = lifetimePoints;
    }

    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(BigDecimal loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public String getConsumerUserId() {
        return consumerUserId;
    }

    public void setConsumerUserId(String consumerUserId) {
        this.consumerUserId = consumerUserId;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    private List<String> expStatuses = new ArrayList<>();

    public List<String> getExpStatuses() {
        return expStatuses;
    }

    public void setExpStatuses(List<String> expStatuses) {
        this.expStatuses = expStatuses;
    }

    public boolean isAgreementExpired() {
        return agreementExpired;
    }

    public void setAgreementExpired(boolean agreementExpired) {
        this.agreementExpired = agreementExpired;
    }

    public boolean isDlExpired() {
        return dlExpired;
    }

    public void setDlExpired(boolean dlExpired) {
        this.dlExpired = dlExpired;
    }

    public boolean isRecommendationExpired() {
        return recommendationExpired;
    }

    public void setRecommendationExpired(boolean recommendationExpired) {
        this.recommendationExpired = recommendationExpired;
    }

    public MemberGroup getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(MemberGroup memberGroup) {
        this.memberGroup = memberGroup;
    }

    public List<String> getRecentProducts() {
        return recentProducts;
    }

    public void setRecentProducts(List<String> recentProducts) {
        this.recentProducts = recentProducts;
    }

    public List<SignedContract> getContracts() {
        return contracts;
    }

    public void setContracts(List<SignedContract> contracts) {
        this.contracts = contracts;
    }


    public List<Identification> getIdentifications() {
        return identifications;
    }

    public void setIdentifications(List<Identification> identifications) {
        this.identifications = identifications;
    }

    @Override
    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public Long getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Long lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }


    public String getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public MembershipStatus getStatus() {
        return status;
    }

    public void setStatus(MembershipStatus status) {
        this.status = status;
    }

    public boolean isEnabledCareGiver() {
        return enabledCareGiver;
    }

    public void setEnabledCareGiver(boolean enabledCareGiver) {
        this.enabledCareGiver = enabledCareGiver;
    }

    public List<String> getCareGivers() {
        return careGivers;
    }

    public void setCareGivers(List<String> careGivers) {
        this.careGivers = careGivers;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public boolean isBanPatient() {
        return banPatient;
    }

    public void setBanPatient(boolean banPatient) {
        this.banPatient = banPatient;
    }

    @Override
    public int compareTo(Member o) {
        return this.getId().equals(o.getId()) ? 0 : 1;
    }


    public List<HashMap<String, String>> getQbCustomerRef() {
        return qbCustomerRef;
    }

    public void setQbCustomerRef(List<HashMap<String, String>> qbCustomerRef) {
        this.qbCustomerRef = qbCustomerRef;
    }

    public LinkedHashSet<String> getTags() {
        return tags;
    }

    public void setTags(LinkedHashSet<String> tags) {
        this.tags = tags;
    }

    public long getRecommendationExpiryLeft() {
        return recommendationExpiryLeft;
    }

    public void setRecommendationExpiryLeft(long recommendationExpiryLeft) {
        this.recommendationExpiryLeft = recommendationExpiryLeft;
    }

    public List<QBDataMapping> getQbMapping() {
        return qbMapping;
    }

    public void setQbMapping(List<QBDataMapping> qbMapping) {
        this.qbMapping = qbMapping;
    }

    ///
    /// Elastic Search Support
    ///

    @Override
    public ElasticSearchIndex getElasticSearchIndex() {
        ElasticSearchIndex index = new ElasticSearchIndex();
        index.setIndex("members");
        index.setType("member");

        final Map<String, FieldIndex> properties = new HashMap<>();
        properties.put("companyId", new FieldIndex("keyword", null, true));
        properties.put("shopId", new FieldIndex("keyword", null, true));
        properties.put("firstName", new FieldIndex("keyword", null, true));
        properties.put("lastName", new FieldIndex("keyword", null, true));
        properties.put("primaryPhone", new FieldIndex("keyword", null, true));
        properties.put("email", new FieldIndex("keyword", null, true));
        properties.put("license", new FieldIndex("keyword", null, true));
        properties.put("memberGroup", new FieldIndex("keyword", null, true));
        properties.put("startDate", new FieldIndex("keyword", null, true));
        properties.put("status", new FieldIndex("keyword", null, true));
        properties.put("deleted", new FieldIndex("keyword", null, true));
        properties.put("recommendationNo", new FieldIndex("keyword", null, true));

        // search fields
        properties.put("firstName_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("lastName_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("primaryPhone_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("email_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("license_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("memberGroup_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("recommendationNo_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));


        index.setProperties(properties);

        return index;
    }

    @Override
    public void loadFrom(AWSSearchResponseHit hit) {
        // Not currently used, see MemberLimitedView for mapping of ElasticSearch members
    }

    @Override
    public JSONObject toElasticSearchObject() {
        final JSONObject jsonObject = new JSONObject();

        String license = "";
        if (getIdentifications() != null && !getIdentifications().isEmpty()) {
            Identification identification = getIdentifications().get(0);

            license = identification.getLicenseNumber();
        }

        String memberGroup = "";
        if (getMemberGroup() != null) {
            memberGroup = getMemberGroup().getName();
        }
        List<String> recommendationNo = new ArrayList<>();
        if (getRecommendations() != null && !getRecommendations().isEmpty()) {
            for (Recommendation recommendation : getRecommendations()) {
                if (StringUtils.isNotBlank(recommendation.getRecommendationNumber())) {
                    recommendationNo.add(recommendation.getRecommendationNumber());
                }
                if (recommendation.getExpirationDate() != null) {
                    setRecommendationExpiryLeft(DateUtil.getStandardDaysBetweenTwoDates(DateTime.now().getMillis(), recommendation.getExpirationDate()));
                    setRecommendationExpired(getRecommendationExpiryLeft() < 0);
                }

            }
        }


        jsonObject.put("companyId", getCompanyId());
        jsonObject.put("shopId", getShopId());
        jsonObject.put("firstName", getFirstName());
        jsonObject.put("lastName", getLastName());
        jsonObject.put("primaryPhone", getPrimaryPhone());
        jsonObject.put("email", getEmail());
        jsonObject.put("license", license);
        jsonObject.put("memberGroup", memberGroup);
        jsonObject.put("startDate", getStartDate());
        jsonObject.put("status", getStatus());
        jsonObject.put("deleted", isDeleted());
        jsonObject.put("banPatient", isBanPatient());
        jsonObject.put("consumerType", getConsumerType());
        jsonObject.put("recommendationExpired", isRecommendationExpired());
        jsonObject.put("recommendationExpiryLeft", getRecommendationExpiryLeft());
        jsonObject.put("recommendationNo", recommendationNo);


        String strippedPhone = getPrimaryPhone() != null ?
                BLStringUtils.cleanPhoneNumber(getPrimaryPhone().toLowerCase()) : "";

        jsonObject.put("firstName_l", getFirstName() != null ? getFirstName().toLowerCase() : "");
        jsonObject.put("lastName_l", getLastName() != null ? getLastName().toLowerCase() : "");
        jsonObject.put("primaryPhone_l", strippedPhone);
        jsonObject.put("email_l", getEmail() != null ? getEmail().toLowerCase() : "");
        jsonObject.put("license_l", license != null ? license.toLowerCase() : "");
        jsonObject.put("memberGroup_l", memberGroup != null ? memberGroup.toLowerCase() : "");
        jsonObject.put("status_l", getStatus() != null ? getStatus().toString().toLowerCase() : "");
        jsonObject.put("banPatient_l", isBanPatient());
        jsonObject.put("consumerType_l", (getConsumerType() == null) ? "" : getConsumerType().toString().toLowerCase());
        jsonObject.put("recommendationExpired_l", isRecommendationExpired());
        jsonObject.put("recommendationNo_l", recommendationNo);
        return jsonObject;
    }
}