package com.fourtwenty.core.domain.models.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "member_logs", indexes = {"{targetId:1,companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberActivity extends ShopBaseModel {
    private String targetId;
    private String log;
    private String employeeId;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
