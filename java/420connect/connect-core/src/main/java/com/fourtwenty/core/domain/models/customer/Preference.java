package com.fourtwenty.core.domain.models.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

/**
 * Created by Stephen Schmidt on 9/2/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Preference extends CompanyBaseModel {
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
