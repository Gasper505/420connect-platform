package com.fourtwenty.core.domain.models.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.verificationsite.VerificationMethod;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.serializers.JsonExpirationDateSerializer;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Recommendation extends CompanyBaseModel {
    public static final String VERIFY_TAG = "REC Verified";
    private String recommendationNumber;
    private String verifyWebsite;
    private String verifyPhoneNumber;
    private String state;
    @JsonSerialize(using = JsonExpirationDateSerializer.class)
    private Long issueDate;
    @JsonSerialize(using = JsonExpirationDateSerializer.class)
    private Long expirationDate;
    private CompanyAsset frontPhoto;
    private List<CompanyAsset> assets = new ArrayList<>();
    private String doctorId;

    private boolean verified = false;
    private VerificationMethod verifyMethod = VerificationMethod.MANUAL;

    // Do not save
    private Doctor doctor;

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public VerificationMethod getVerifyMethod() {
        return verifyMethod;
    }

    public void setVerifyMethod(VerificationMethod verifyMethod) {
        this.verifyMethod = verifyMethod;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public CompanyAsset getFrontPhoto() {
        return frontPhoto;
    }

    public void setFrontPhoto(CompanyAsset frontPhoto) {
        this.frontPhoto = frontPhoto;
    }

    public Long getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Long issueDate) {
        this.issueDate = issueDate;
    }

    public String getRecommendationNumber() {
        return recommendationNumber;
    }

    public void setRecommendationNumber(String recommendationNumber) {
        this.recommendationNumber = recommendationNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVerifyPhoneNumber() {
        return verifyPhoneNumber;
    }

    public void setVerifyPhoneNumber(String verifyPhoneNumber) {
        this.verifyPhoneNumber = verifyPhoneNumber;
    }

    public String getVerifyWebsite() {
        return verifyWebsite;
    }

    public void setVerifyWebsite(String verifyWebsite) {
        this.verifyWebsite = verifyWebsite;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }
}