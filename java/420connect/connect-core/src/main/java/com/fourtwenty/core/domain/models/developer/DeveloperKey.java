package com.fourtwenty.core.domain.models.developer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 2/2/17.
 */

@CollectionName(name = "developer_keys", premSyncDown = false,uniqueIndexes = {"{key:1}"}, indexes = {"{companyId:1,active:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeveloperKey extends ShopBaseModel {
    private String key;
    private String secret;
    private boolean active = true; // default is true
    private boolean enableExposeSales = false;
    private String name;
    private boolean enableExposeMembers = Boolean.FALSE;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public boolean isEnableExposeSales() {
        return enableExposeSales;
    }

    public void setEnableExposeSales(boolean enableExposeSales) {
        this.enableExposeSales = enableExposeSales;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnableExposeMembers() {
        return enableExposeMembers;
    }

    public void setEnableExposeMembers(boolean enableExposeMembers) {
        this.enableExposeMembers = enableExposeMembers;
    }
}
