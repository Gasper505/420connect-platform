package com.fourtwenty.core.domain.models.developer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

/**
 * Created by mdo on 2/9/18.
 */
@CollectionName(name = "partner_keys",premSyncDown = false,uniqueIndexes = {"{key:1}"}, indexes = {"{active:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerKey extends BaseModel {
    public enum PartnerAccessLevel {
        Normal,
        Restrictive // Can access restricted commands
    }
    private String name;
    private String key;
    private boolean active = true; // default is true
    private PartnerAccessLevel partnerType = PartnerAccessLevel.Normal;

    public PartnerAccessLevel getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(PartnerAccessLevel partnerType) {
        this.partnerType = partnerType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
