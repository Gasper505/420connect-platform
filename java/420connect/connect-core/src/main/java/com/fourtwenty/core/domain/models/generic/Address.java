package com.fourtwenty.core.domain.models.generic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by mdo on 9/6/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address extends CompanyBaseModel {
    public static final String DEFAULT_COUNTRY = "US";
    private String address = "";
    private String city = "";
    private String state = "";
    private String zipCode = "";
    private String country = DEFAULT_COUNTRY;

    private String addressLine2 = "";

    public void clean() {
        if (address == null) {
            address = "";
        }
        if (city == null) {
            city = "";
        }
        if (state == null) {
            state = "";
        }
        if (zipCode == null) {
            zipCode = "";
        }
        if (addressLine2 == null) {
            addressLine2 = "";
        }
    }

    @JsonIgnore
    public boolean isValid() {
        return StringUtils.isNotBlank(address)
                || StringUtils.isNotBlank(city)
                || StringUtils.isNotBlank(state)
                || StringUtils.isNotBlank(zipCode)
                || StringUtils.isNotBlank(addressLine2);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @JsonIgnore
    public String getAddressString() {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(address)) {
            sb.append(address);
        }
        if (StringUtils.isNotBlank(addressLine2)) {
            sb.append(StringUtils.isNotBlank(sb) ? " " : "");
            sb.append(addressLine2);
        }
        if (StringUtils.isNotBlank(city)) {
            sb.append(StringUtils.isNotBlank(sb) ? "," : "");
            sb.append(city);
        }
        if (StringUtils.isNotBlank(state)) {
            sb.append(StringUtils.isNotBlank(sb) ? "," : "");
            sb.append(state);
        }
        if (StringUtils.isNotBlank(zipCode)) {
            sb.append(StringUtils.isNotBlank(sb) ? "," : "");
            sb.append(zipCode);
        }

        return sb.toString();
    }
}
