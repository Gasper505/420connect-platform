package com.fourtwenty.core.domain.models.generic;

/**
 * Created by mdo on 12/27/15.
 */
public interface Asset {
    enum AssetType {
        Photo,
        Document
    }

    AssetType getAssetType();
}
