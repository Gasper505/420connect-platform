package com.fourtwenty.core.domain.models.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

/**
 * Created by mdo on 3/6/17.
 */
@CollectionName(name = "company_uniquesequences", uniqueIndexes = {"{companyId:1,name:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyUniqueSequence extends CompanyBaseModel {
    private String name; // proximity ID
    private long count; // increment (major_minor)

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
