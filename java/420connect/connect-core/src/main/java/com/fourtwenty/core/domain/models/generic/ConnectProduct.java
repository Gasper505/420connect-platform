package com.fourtwenty.core.domain.models.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 8/8/16.
 */
@CollectionName(name = "connect_products", uniqueIndexes = {"{productSKU:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectProduct extends BaseModel {
    private String productSKU;
    private String description;
    private int maxTerminals;
    private int maxShop;
    private int maxEmployees = 10; // default;
    private List<Transaction.QueueType> availableQueues = new ArrayList<>();
    private List<CompanyFeatures.AppTarget> availableApps = Lists.newArrayList(CompanyFeatures.AppTarget.AuthenticationApp);


    public List<Transaction.QueueType> getAvailableQueues() {
        return availableQueues;
    }

    public void setAvailableQueues(List<Transaction.QueueType> availableQueues) {
        this.availableQueues = availableQueues;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxShop() {
        return maxShop;
    }

    public void setMaxShop(int maxShop) {
        this.maxShop = maxShop;
    }

    public int getMaxTerminals() {
        return maxTerminals;
    }

    public void setMaxTerminals(int maxTerminals) {
        this.maxTerminals = maxTerminals;
    }

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public List<CompanyFeatures.AppTarget> getAvailableApps() {
        return availableApps;
    }

    public void setAvailableApps(List<CompanyFeatures.AppTarget> availableApps) {
        this.availableApps = availableApps;
    }

    public int getMaxEmployees() {
        return maxEmployees;
    }

    public void setMaxEmployees(int maxEmployees) {
        this.maxEmployees = maxEmployees;
    }
}
