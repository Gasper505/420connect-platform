package com.fourtwenty.core.domain.models.generic;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditCard {
    private String number;
    private String name;
    private String cvv;

    private String expirationMonth;
    private String expirationYear;
    private String zipCode;

    // Backward compatibility
    @JsonProperty("expiry_month")
    private String expiration_month;
    @JsonProperty("expiry_year")
    private String expiration_year;
    @JsonProperty("zip_code")
    private String zip_code;


    public CreditCard() {
    }

    public CreditCard(String number, String name, String expirationMonth, String expirationYear, String cvv, String zipCode) {
        this.number = number;
        this.name = name;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.cvv = cvv;
        this.zipCode = zipCode;
    }

    public String getExpiration_month() {
        return expiration_month;
    }

    public void setExpiration_month(String expiration_month) {
        this.expiration_month = expiration_month;
    }

    public String getExpiration_year() {
        return expiration_year;
    }

    public void setExpiration_year(String expiration_year) {
        this.expiration_year = expiration_year;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }


    @Override
    public String toString() {
        return "CreditCard{" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", cvv='" + cvv + '\'' +
                ", expirationMonth='" + expirationMonth + '\'' +
                ", expirationYear='" + expirationYear + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", expiration_month='" + expiration_month + '\'' +
                ", expiration_year='" + expiration_year + '\'' +
                ", zip_code='" + zip_code + '\'' +
                '}';
    }
}