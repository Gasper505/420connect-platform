package com.fourtwenty.core.domain.models.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Stephen Schmidt on 9/16/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Note extends BaseModel {
    private String writerId;
    private String writerName;
    private String message;
    private boolean enableOnFleet;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWriterId() {
        return writerId;
    }

    public void setWriterId(String writerId) {
        this.writerId = writerId;
    }

    public String getWriterName() {
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }

    public boolean isEnableOnFleet() {
        return enableOnFleet;
    }

    public void setEnableOnFleet(boolean enableOnFleet) {
        this.enableOnFleet = enableOnFleet;
    }
}
