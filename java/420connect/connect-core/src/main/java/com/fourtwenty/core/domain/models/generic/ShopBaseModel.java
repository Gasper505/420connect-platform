package com.fourtwenty.core.domain.models.generic;

import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

/**
 * Created by mdo on 10/12/15.
 */
public class ShopBaseModel extends CompanyBaseModel {
    protected String shopId;
    protected boolean dirty = false;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }


    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }
}
