package com.fourtwenty.core.domain.models.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

/**
 * The ShareIdentifier object.
 * User: mdo
 * Date: 4/2/15
 * Time: 4:11 PM
 */
@CollectionName(name = "uniquesequences", uniqueIndexes = {"{companyId:1,shopId:1,name:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UniqueSequence extends ShopBaseModel {
    private String name; // proximity ID
    private long count; // increment (major_minor)

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
