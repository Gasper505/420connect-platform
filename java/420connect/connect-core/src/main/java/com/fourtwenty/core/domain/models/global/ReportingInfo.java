package com.fourtwenty.core.domain.models.global;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.reporting.ReportType;

import java.util.List;

@CollectionName(name = "reporting_Info", uniqueIndexes = {"{reportingInfoType:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportingInfo extends BaseModel {

    private long lastSync = 0;
    private Boolean active = Boolean.FALSE;
    private ReportType reportingInfoType;
    private List<ReportType> subReportTypes;

    public long getLastSync() {
        return lastSync;
    }

    public void setLastSync(long lastSync) {
        this.lastSync = lastSync;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ReportType getReportingInfoType() {
        return reportingInfoType;
    }

    public void setReportingInfoType(ReportType reportingInfoType) {
        this.reportingInfoType = reportingInfoType;
    }

    public List<ReportType> getSubReportTypes() {
        return subReportTypes;
    }

    public void setSubReportTypes(List<ReportType> subReportTypes) {
        this.subReportTypes = subReportTypes;
    }
}
