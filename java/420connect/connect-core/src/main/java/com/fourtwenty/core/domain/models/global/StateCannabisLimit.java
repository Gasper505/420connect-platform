package com.fourtwenty.core.domain.models.global;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import java.math.BigDecimal;

/**
 * Created by mdo on 3/26/18.
 */
@CollectionName(name = "state_limits", uniqueIndexes = {"{state:1,consumerType:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class StateCannabisLimit extends BaseModel implements OnPremSyncable {
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private USState state;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal concentrates = new BigDecimal(-1); // if -1, then it's unlimited or not applicable
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal nonConcentrates = new BigDecimal(-1);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal plants = new BigDecimal(-1);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal seeds = new BigDecimal(-1);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal max = new BigDecimal(-1);

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal thc = new BigDecimal(-1);

    private int duration = 1; // In days. This identify how much is allow for each patient per duration in days

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public BigDecimal getConcentrates() {
        return concentrates;
    }

    public void setConcentrates(BigDecimal concentrates) {
        this.concentrates = concentrates;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

    public BigDecimal getNonConcentrates() {
        return nonConcentrates;
    }

    public void setNonConcentrates(BigDecimal nonConcentrates) {
        this.nonConcentrates = nonConcentrates;
    }

    public BigDecimal getPlants() {
        return plants;
    }

    public void setPlants(BigDecimal plants) {
        this.plants = plants;
    }

    public USState getState() {
        return state;
    }

    public void setState(USState state) {
        this.state = state;
    }

    public BigDecimal getThc() {
        return thc;
    }

    public void setThc(BigDecimal thc) {
        this.thc = thc;
    }

    public BigDecimal getSeeds() {
        return seeds;
    }

    public void setSeeds(BigDecimal seeds) {
        this.seeds = seeds;
    }
}
