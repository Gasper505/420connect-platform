package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.annotations.CollectionName;


@CollectionName(name = "discount_versions", premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}","{created:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscountVersion extends ShopBaseModel implements OnPremSyncable{
    private String promotionId;
    private String rewardId;
    private Promotion promotion;
    private LoyaltyReward reward;

    public DiscountVersion() {
    }

    public DiscountVersion(String promotionId, String rewardId) {
        this.promotionId = promotionId;
        this.rewardId = rewardId;
    }

    public DiscountVersion(String promotionId, String rewardId, Promotion promotion, LoyaltyReward reward) {
        this.promotionId = promotionId;
        this.rewardId = rewardId;
        this.setPromotion(promotion);
        this.setReward(reward);
    }

    public DiscountVersion(Promotion promotion) {
        this.promotionId = promotion.getId();
        this.promotion = promotion;
        this.setCompanyId(promotion.getCompanyId());
        this.setShopId(promotion.getShopId());
    }

    public DiscountVersion(LoyaltyReward reward) {
        this.rewardId = reward.getId();
        this.reward = reward;
        this.setCompanyId(reward.getCompanyId());
        this.setShopId(reward.getShopId());
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public LoyaltyReward getReward() {
        return reward;
    }

    public void setReward(LoyaltyReward reward) {
        this.reward = reward;
    }
}
