package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 7/22/17.
 */
@CollectionName(name = "loyalty_activity_logs", indexes = {"{companyId:1,shopId:1,memberId:1}", "{companyId:1,shopId:1,memberId:1,transNo:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoyaltyActivityLog extends ShopBaseModel {
    public enum LoyaltyActivityType {
        Accrue, Usage;

        public static final LoyaltyActivityType convert(String name) {
            if (StringUtils.isBlank(name)) {
                return Usage;
            } else if (name.equalsIgnoreCase("accrue") || name.equalsIgnoreCase("earn")) {
                return Accrue;
            } else if (name.equalsIgnoreCase("Usage")) {
                return Usage;
            }
            throw new BlazeInvalidArgException("RewardsActivity", "Invalid activity type. Must be of [Accrue or Usage]");
        }
    }

    private LoyaltyActivityType activityType = LoyaltyActivityType.Accrue;
    private String memberId;
    private String employeeId; // applied by active employee

    private String rewardNo;
    private String rewardId;


    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    private Long activityDate;

    private String transNo; // Referenced to transactionNo. Used during accrue type
    private String note; // special note if any


    public String getNote() {
        return note;
    }


    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardNo() {
        return rewardNo;
    }

    public void setRewardNo(String rewardNo) {
        this.rewardNo = rewardNo;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public Long getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Long activityDate) {
        this.activityDate = activityDate;
    }

    public LoyaltyActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(LoyaltyActivityType activityType) {
        this.activityType = activityType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
