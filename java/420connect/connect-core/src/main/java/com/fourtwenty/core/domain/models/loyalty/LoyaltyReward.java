package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 7/31/17.
 */
@CollectionName(name = "loyalty_rewards", uniqueIndexes = {"{companyId:1,rewardNo:1}"}, indexes = {"{companyId:1,shop:1}", "{companyId:1,shop:1,name:1,deleted:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoyaltyReward extends ShopBaseModel implements OrderedDiscountable, OnPremSyncable {
    public enum LoyaltyRewardType {
        Regular,
        Promotion
    }

    public enum LoyaltyRewardTarget {
        WholeCart,
        DeliveryFee,
        AllSelectedProducts,
        OneFromProducts,
        OneFromCategories,
        OneFromVendors,
        OneFromProductTags
    }

    private String rewardNo;
    private String slug;
    @Min(0)
    private int points = 0; // required points
    @NotEmpty
    private String name;
    private String detail;
    private boolean active = true;
    private LoyaltyRewardType loyaltyType = LoyaltyRewardType.Regular;
    private boolean published = false;
    private Long publishedDate;
    private Long unpublishedDate;

    // For promotion type
    private String promotionId; // Optional

    // For paymentcard type
    // limited to products
    private List<String> productIds = new ArrayList<>(); // Optional, can be empty
    // limited to categories
    private List<String> categoryIds = new ArrayList<>(); // Optional, can be empty
    // limited to vendorIds
    private List<String> vendorIds = new ArrayList<>(); // Optional, can be empty

    private List<String> productTags = new ArrayList<>(); // Optional, can be empty

    // Discounts
    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Cash;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discountAmt = new BigDecimal(0);
    private ProductWeightTolerance.WeightKey discountUnitType = ProductWeightTolerance.WeightKey.UNIT;

    private Long startDate;
    private Long endDate;

    private LoyaltyRewardTarget rewardTarget = LoyaltyRewardTarget.WholeCart;

    private boolean assigned = true;

    private String finalizedDiscountId;

    @Override
    public int getPriority() {
        if (rewardTarget == LoyaltyRewardTarget.AllSelectedProducts
                || rewardTarget == LoyaltyRewardTarget.OneFromCategories
                || rewardTarget == LoyaltyRewardTarget.AllSelectedProducts
                || rewardTarget == LoyaltyRewardTarget.OneFromVendors
                || rewardTarget == LoyaltyRewardTarget.OneFromProductTags) {
            if (discountType == OrderItem.DiscountType.Cash) {
                return OrderedDiscountable.PRODUCT_CASH;
            }
            return OrderedDiscountable.PRODUCT_PERCENTAGE;
        } else if (rewardTarget == LoyaltyRewardTarget.WholeCart) {

            if (discountType == OrderItem.DiscountType.Cash) {
                return OrderedDiscountable.CART_CASH;
            }
            return OrderedDiscountable.CART_PERCENTAGE;
        } else if (rewardTarget == LoyaltyRewardTarget.DeliveryFee) {
            return OrderedDiscountable.DELIVERY;
        }
        return 0;
    }


    @Override
    public int applyLowestPriceFirst() {
        return HIGHEST_PRICE_FIST;
    }

    public Long getUnpublishedDate() {
        return unpublishedDate;
    }

    public void setUnpublishedDate(Long unpublishedDate) {
        this.unpublishedDate = unpublishedDate;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Long getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Long publishedDate) {
        this.publishedDate = publishedDate;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getRewardNo() {
        return rewardNo;
    }

    public void setRewardNo(String rewardNo) {
        this.rewardNo = rewardNo;
    }

    public LoyaltyRewardTarget getRewardTarget() {
        return rewardTarget;
    }

    public void setRewardTarget(LoyaltyRewardTarget rewardTarget) {
        this.rewardTarget = rewardTarget;
    }

    public LoyaltyRewardType getLoyaltyType() {
        return loyaltyType;
    }

    public void setLoyaltyType(LoyaltyRewardType loyaltyType) {
        this.loyaltyType = loyaltyType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    public ProductWeightTolerance.WeightKey getDiscountUnitType() {
        return discountUnitType;
    }

    public void setDiscountUnitType(ProductWeightTolerance.WeightKey discountUnitType) {
        this.discountUnitType = discountUnitType;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public List<String> getProductTags() {
        return productTags;
    }

    public void setProductTags(List<String> productTags) {
        this.productTags = productTags;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public String getFinalizedDiscountId() {
        return finalizedDiscountId;
    }

    public void setFinalizedDiscountId(String finalizedDiscountId) {
        this.finalizedDiscountId = finalizedDiscountId;
    }
}
