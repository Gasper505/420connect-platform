package com.fourtwenty.core.domain.models.loyalty;

import java.util.Comparator;

/**
 * Created by mdo on 12/5/17.
 */
public interface OrderedDiscountable {
    public static final int HIGHEST_PRICE_FIST = 1;
    public static final int LOWEST_PRICE_FIST = -1;


    // SORTING FOR STACKABLES/NON-STACKABLES
    public static final int PRODUCT_BOGO_CASH = 0;
    public static final int PRODUCT_BOGO_PERCENTAGE = 1;
    public static final int PRODUCT_BOGO_CART_CASH = 2;
    public static final int PRODUCT_BOGO_CART_PERCENTAGE = 3;


    public static final int PRODUCT_CASH = 5;
    public static final int PRODUCT_PERCENTAGE = 6;
    public static final int CART_CASH = 7;
    public static final int CART_PERCENTAGE = 8;

    // STACKABLE GOES LAST
    public static final int BOGO_STACKABLE_PRODUCT_CASH = 30;
    public static final int BOGO_STACKABLE_PRODUCT_PERCENTAGE = 31;
    public static final int BOGO_STACKABLE_CART_CASH = 32;
    public static final int BOGO_STACKABLE_CART_PERCENTAGE = 33;

    public static final int STACKABLE_PRODUCT_CASH = 35;
    public static final int STACKABLE_PRODUCT_PERCENTAGE = 36;
    public static final int STACKABLE_CART_CASH = 37;
    public static final int STACKABLE_CART_PERCENTAGE = 38;



    // DELIVERY
    public static final int DELIVERY = 50;

    int getPriority();

    public static final class OrderedDiscountableComparator implements Comparator<OrderedDiscountable> {

        @Override
        public int compare(OrderedDiscountable o1, OrderedDiscountable o2) {
            Integer p1 = o1 == null ? Integer.MAX_VALUE : o1.getPriority();
            Integer p2 = o2 == null ? Integer.MAX_VALUE : o2.getPriority();
            return p1.compareTo(p2);
        }
    }

    int applyLowestPriceFirst();
}
