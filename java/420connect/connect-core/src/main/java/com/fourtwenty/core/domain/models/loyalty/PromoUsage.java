package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 1/3/17.
 */
@CollectionName(name = "promo_usages", uniqueIndexes = {"{companyId:1,shopId:1,promoId:1,memberId:1,transId:1}"}, indexes = {"{companyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromoUsage extends ShopBaseModel {
    private String promoId;
    private String memberId;
    private String transId;
    private int count = 1; // default is 1

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

}
