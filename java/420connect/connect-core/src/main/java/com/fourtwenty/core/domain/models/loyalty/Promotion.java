package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.importer.main.Importable;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 1/3/17.
 */

@CollectionName(name = "promotions", indexes = {"{companyId:1,shopId:1}", "{companyId:1,shopId:1,promoCodes:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Promotion extends ShopBaseModel implements OrderedDiscountable, Importable, OnPremSyncable {
    public enum PromotionType {
        Cart,
        Product,
        DeliveryFee
    }

    public enum PromoSource {
        Manual,
        MemberGroup
    }

    private String name;
    private String promoDesc;
    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Cash;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discountAmt = new BigDecimal(0);
    private ProductWeightTolerance.WeightKey discountUnitType = ProductWeightTolerance.WeightKey.UNIT;
    @Deprecated
    private String promocode;
    private boolean promoCodeRequired = false;
    private boolean consumerFacing = false;
    private Long startDate;
    private Long endDate;
    private boolean active = true;
    private int maxAvailable = 0;
    private int usageCount = 0;
    private boolean enableMaxAvailable = false;
    private int limitPerCustomer = 0;
    private boolean enableLimitPerCustomer = false;
    private boolean restrictMemberGroups = false;
    private List<String> memberGroupIds = new ArrayList<>();
    private List<PromotionRule> rules = new ArrayList<>();
    private PromotionType promotionType = PromotionType.Cart;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal maxCashValue = new BigDecimal(0);
    private boolean enableBOGO = false; // IF BOGO, THEN DISCOUNT IS PERCENTAGE. Only available if PromotionType == Product

    // Time available
    private boolean enableDayDuration = false;
    private Integer dayStartTime; // In MILLISECONDS
    private Integer dayEndTime;   // In MILLISECONDS


    private boolean scalable = false;

    private boolean targetPriceRange = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal lowerPriceBound = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal upperPriceBound = new BigDecimal(0);


    // Days available
    private boolean mon = false;
    private boolean tues = false;
    private boolean wed = false;
    private boolean thur = false;
    private boolean fri = false;
    private boolean sat = false;
    private boolean sun = false;

    private Set<String> promoCodes = new HashSet<>();

    private PromoSource promoSource = PromoSource.Manual;
    private boolean stackable = Boolean.TRUE;
    private String importId;
    private boolean lowestPriceFirst = false;

    private boolean assigned = true;

    private String finalizedDiscountId;

    public ProductWeightTolerance.WeightKey getDiscountUnitType() {
        return discountUnitType;
    }

    public void setDiscountUnitType(ProductWeightTolerance.WeightKey discountUnitType) {
        this.discountUnitType = discountUnitType;
    }

    @Override
    public int getPriority() {

        if (promotionType == PromotionType.DeliveryFee) {
            return OrderedDiscountable.DELIVERY;
        }

        // IF NOT-STACKABLE, DO THIS INSTEAD
        if (!stackable) {
            if (promotionType == PromotionType.Product) {
                if (enableBOGO) {
                    return discountType == OrderItem.DiscountType.Cash ? OrderedDiscountable.PRODUCT_BOGO_CASH : OrderedDiscountable.PRODUCT_BOGO_PERCENTAGE;
                }
                if (discountType == OrderItem.DiscountType.Cash) {
                    return OrderedDiscountable.PRODUCT_CASH;
                }
                return OrderedDiscountable.PRODUCT_PERCENTAGE;
            } else if (promotionType == PromotionType.Cart) {
                if (enableBOGO) {
                    return discountType == OrderItem.DiscountType.Cash ? OrderedDiscountable.PRODUCT_BOGO_CART_CASH : OrderedDiscountable.PRODUCT_BOGO_CART_PERCENTAGE;
                }
                if (discountType == OrderItem.DiscountType.Cash) {
                    return OrderedDiscountable.CART_CASH;
                }
                return OrderedDiscountable.CART_PERCENTAGE;
            }
        }

        // STACKABLE
        if (promotionType == PromotionType.Product) {
            if (enableBOGO) {
                return discountType == OrderItem.DiscountType.Cash ? OrderedDiscountable.BOGO_STACKABLE_PRODUCT_CASH : OrderedDiscountable.BOGO_STACKABLE_PRODUCT_PERCENTAGE;
            }

            if (discountType == OrderItem.DiscountType.Cash) {
                return OrderedDiscountable.STACKABLE_PRODUCT_CASH;
            }
            return OrderedDiscountable.STACKABLE_PRODUCT_PERCENTAGE;
        } else if (promotionType == PromotionType.Cart) {
            if (enableBOGO) {
                return discountType == OrderItem.DiscountType.Cash ? OrderedDiscountable.BOGO_STACKABLE_CART_CASH : OrderedDiscountable.BOGO_STACKABLE_CART_PERCENTAGE;
            }
            if (discountType == OrderItem.DiscountType.Cash) {
                return OrderedDiscountable.STACKABLE_CART_CASH;
            }
            return OrderedDiscountable.STACKABLE_CART_PERCENTAGE;
        } else if (promotionType == PromotionType.DeliveryFee) {
            return OrderedDiscountable.DELIVERY;
        }


        return 0;
    }

    @Override
    public int applyLowestPriceFirst() {
        return lowestPriceFirst ? LOWEST_PRICE_FIST : HIGHEST_PRICE_FIST;
    }

    public boolean isLowestPriceFirst() {
        return lowestPriceFirst;
    }

    public void setLowestPriceFirst(boolean lowestPriceFirst) {
        this.lowestPriceFirst = lowestPriceFirst;
    }

    public BigDecimal getLowerPriceBound() {
        return lowerPriceBound;
    }

    public void setLowerPriceBound(BigDecimal lowerPriceBound) {
        this.lowerPriceBound = lowerPriceBound;
    }

    public boolean isScalable() {
        return scalable;
    }

    public void setScalable(boolean scalable) {
        this.scalable = scalable;
    }

    public boolean isTargetPriceRange() {
        return targetPriceRange;
    }

    public void setTargetPriceRange(boolean targetPriceRange) {
        this.targetPriceRange = targetPriceRange;
    }

    public BigDecimal getUpperPriceBound() {
        return upperPriceBound;
    }

    public void setUpperPriceBound(BigDecimal upperPriceBound) {
        this.upperPriceBound = upperPriceBound;
    }

    public boolean isEnableDayDuration() {
        return enableDayDuration;
    }

    public void setEnableDayDuration(boolean enableDayDuration) {
        this.enableDayDuration = enableDayDuration;
    }

    public boolean isEnableBOGO() {
        return enableBOGO;
    }

    public void setEnableBOGO(boolean enableBOGO) {
        this.enableBOGO = enableBOGO;
    }

    public Integer getDayEndTime() {
        return dayEndTime;
    }

    public void setDayEndTime(Integer dayEndTime) {
        this.dayEndTime = dayEndTime;
    }

    public Integer getDayStartTime() {
        return dayStartTime;
    }

    public void setDayStartTime(Integer dayStartTime) {
        this.dayStartTime = dayStartTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isConsumerFacing() {
        return consumerFacing;
    }

    public void setConsumerFacing(boolean consumerFacing) {
        this.consumerFacing = consumerFacing;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    public boolean isEnableLimitPerCustomer() {
        return enableLimitPerCustomer;
    }

    public void setEnableLimitPerCustomer(boolean enableLimitPerCustomer) {
        this.enableLimitPerCustomer = enableLimitPerCustomer;
    }

    public boolean isEnableMaxAvailable() {
        return enableMaxAvailable;
    }

    public void setEnableMaxAvailable(boolean enableMaxAvailable) {
        this.enableMaxAvailable = enableMaxAvailable;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public boolean isFri() {
        return fri;
    }

    public void setFri(boolean fri) {
        this.fri = fri;
    }

    public int getLimitPerCustomer() {
        return limitPerCustomer;
    }

    public void setLimitPerCustomer(int limitPerCustomer) {
        this.limitPerCustomer = limitPerCustomer;
    }

    public int getMaxAvailable() {
        return maxAvailable;
    }

    public void setMaxAvailable(int maxAvailable) {
        this.maxAvailable = maxAvailable;
    }

    public boolean isMon() {
        return mon;
    }

    public void setMon(boolean mon) {
        this.mon = mon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Deprecated
    public String getPromocode() {
        return promocode;
    }

    @Deprecated
    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public boolean isPromoCodeRequired() {
        return promoCodeRequired;
    }

    public void setPromoCodeRequired(boolean promoCodeRequired) {
        this.promoCodeRequired = promoCodeRequired;
    }

    public String getPromoDesc() {
        return promoDesc;
    }

    public void setPromoDesc(String promoDesc) {
        this.promoDesc = promoDesc;
    }

    public PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    public List<PromotionRule> getRules() {
        return rules;
    }

    public void setRules(List<PromotionRule> rules) {
        this.rules = rules;
    }

    public boolean isSat() {
        return sat;
    }

    public void setSat(boolean sat) {
        this.sat = sat;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public boolean isSun() {
        return sun;
    }

    public void setSun(boolean sun) {
        this.sun = sun;
    }

    public boolean isThur() {
        return thur;
    }

    public void setThur(boolean thur) {
        this.thur = thur;
    }

    public boolean isTues() {
        return tues;
    }

    public void setTues(boolean tues) {
        this.tues = tues;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    public boolean isWed() {
        return wed;
    }

    public void setWed(boolean wed) {
        this.wed = wed;
    }

    public BigDecimal getMaxCashValue() {
        return maxCashValue;
    }

    public void setMaxCashValue(BigDecimal maxCashValue) {
        this.maxCashValue = maxCashValue;
    }

    public boolean isRestrictMemberGroups() {
        return restrictMemberGroups;
    }

    public void setRestrictMemberGroups(boolean restrictMemberGroups) {
        this.restrictMemberGroups = restrictMemberGroups;
    }

    public List<String> getMemberGroupIds() {
        return memberGroupIds;
    }

    public void setMemberGroupIds(List<String> memberGroupIds) {
        this.memberGroupIds = memberGroupIds;
    }

    public Set<String> getPromoCodes() {
        return promoCodes;
    }

    public void setPromoCodes(Set<String> promoCodes) {
        this.promoCodes = promoCodes;
    }

    public PromoSource getPromoSource() {
        return promoSource;
    }

    public void setPromoSource(PromoSource promoSource) {
        this.promoSource = promoSource;
    }

    public boolean isStackable() {
        return stackable;
    }

    public void setStackable(boolean stackable) {
        this.stackable = stackable;
    }

    @Override
    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public String getFinalizedDiscountId() {
        return finalizedDiscountId;
    }

    public void setFinalizedDiscountId(String finalizedDiscountId) {
        this.finalizedDiscountId = finalizedDiscountId;
    }

}
