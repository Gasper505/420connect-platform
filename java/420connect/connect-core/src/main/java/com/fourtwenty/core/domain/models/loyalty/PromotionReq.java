package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

/**
 * Created by mdo on 1/17/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionReq extends CompanyBaseModel {
    private String promotionId;
    private String rewardId;
    private String finalizedDiscountId; //discount version id
    private boolean assigned = false;

    public PromotionReq() {
    }

    public PromotionReq(String promotionId, String rewardId) {
        this.promotionId = promotionId;
        this.rewardId = rewardId;
    }

    public PromotionReq(String promotionId, String rewardId, boolean assigned) {
        this.promotionId = promotionId;
        this.rewardId = rewardId;
        this.setAssigned(assigned);
    }

    public PromotionReq(String promotionId, String rewardId, String finalizedDiscountId) {
        this.promotionId = promotionId;
        this.rewardId = rewardId;
        this.finalizedDiscountId = finalizedDiscountId;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public String getFinalizedDiscountId() {
        return finalizedDiscountId;
    }

    public void setFinalizedDiscountId(String finalizedDiscountId) {
        this.finalizedDiscountId = finalizedDiscountId;
    }
}
