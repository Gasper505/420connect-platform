package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 7/28/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionReqLog extends PromotionReq {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    private boolean stackable = false;

    public boolean isStackable() {
        return stackable;
    }

    public void setStackable(boolean stackable) {
        this.stackable = stackable;
    }

    private Promotion.PromotionType promotionType = Promotion.PromotionType.Cart;

    public Promotion.PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Promotion.PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
