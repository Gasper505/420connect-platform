package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.LinkedHashSet;

/**
 * Created by mdo on 1/3/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionRule extends CompanyBaseModel {
    public enum RuleCategory {
        Misc,
        OneOf,
        Multiple
    }

    public enum PromotionRuleType {
        None(-1,RuleCategory.Misc),
        LastVisit(0,RuleCategory.Misc), // In Days
        FirstTimeMember(1,RuleCategory.Misc),
        LastVist(2,RuleCategory.Misc),
        CartOrderMin(3,RuleCategory.Misc),
        NumberMinOrders(4,RuleCategory.Misc),
        // all with matches
        ProductTag(5,RuleCategory.Multiple),
        ByVendor(6,RuleCategory.Multiple),
        ProductCategory(7,RuleCategory.Multiple),
        Product(8,RuleCategory.Multiple),
        ByBrand(9,RuleCategory.Multiple),
        // one from
        OneFromVendors(10,RuleCategory.OneOf),
        OneFromCategories(11,RuleCategory.OneOf),
        OneFromProducts(12,RuleCategory.OneOf),
        OneFromProductTags(13,RuleCategory.OneOf),
        OneFromBrands(14,RuleCategory.OneOf);

        PromotionRuleType(int priority, RuleCategory category) {
            this.priority = priority;
            this.category = category;
        }

        public Integer priority;
        public RuleCategory category = RuleCategory.Misc;

        public boolean isOneOf() {
            return category == RuleCategory.OneOf;
        }

        public boolean isProductRule() {
            return category == RuleCategory.Multiple || category == RuleCategory.OneOf;
        }

        public boolean isMultiple() {
            return category == RuleCategory.Multiple;
        }

    }

    private PromotionRuleType ruleType = PromotionRuleType.None;
    private LinkedHashSet<String> categoryIds = new LinkedHashSet<>();
    private LinkedHashSet<String> productIds = new LinkedHashSet<>();
    private LinkedHashSet<String> vendorIds = new LinkedHashSet<>();
    private LinkedHashSet<String> productTags = new LinkedHashSet<>();
    private String categoryId;
    private String productId;
    private String vendorId;


    private ProductWeightTolerance.WeightKey unitWeight = ProductWeightTolerance.WeightKey.UNIT; // If it's flowers, then Unit == Gram

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal minAmt = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal maxAmt = new BigDecimal(0);
    private LinkedHashSet<String> brandIds = new LinkedHashSet<>();

    public BigDecimal getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(BigDecimal maxAmt) {
        this.maxAmt = maxAmt;
    }

    public BigDecimal getMinAmt() {
        return minAmt;
    }

    public void setMinAmt(BigDecimal minAmt) {
        this.minAmt = minAmt;
    }

    public PromotionRuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(PromotionRuleType ruleType) {
        this.ruleType = ruleType;
    }

    public LinkedHashSet<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(LinkedHashSet<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public LinkedHashSet<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(LinkedHashSet<String> productIds) {
        this.productIds = productIds;
    }

    public LinkedHashSet<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(LinkedHashSet<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public ProductWeightTolerance.WeightKey getUnitWeight() {
        return unitWeight;
    }

    public void setUnitWeight(ProductWeightTolerance.WeightKey unitWeight) {
        this.unitWeight = unitWeight;
    }

    public LinkedHashSet<String> getProductTags() {
        return productTags;
    }

    public void setProductTags(LinkedHashSet<String> productTags) {
        this.productTags = productTags;
    }

    public LinkedHashSet<String> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(LinkedHashSet<String> brandIds) {
        this.brandIds = brandIds;
    }
}
