package com.fourtwenty.core.domain.models.loyalty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.LinkedHashSet;

/**
 * Created on 9/10/19.
 */

@CollectionName(name = "storewide_sales", indexes = {"{companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class StorewideSale extends ShopBaseModel implements OnPremSyncable {

    private String name;
    private String saleDesc;
    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Percentage;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discountAmt = new BigDecimal(0);
    private boolean active = true;

    private LinkedHashSet<String> brandsIds = new LinkedHashSet<>();


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSaleDesc() {
        return saleDesc;
    }

    public void setSaleDesc(String saleDesc) {
        this.saleDesc = saleDesc;
    }

    public LinkedHashSet<String> getBrandsIds() {
        return brandsIds;
    }

    public void setBrandsIds(LinkedHashSet<String> brandsIds) {
        this.brandsIds = brandsIds;
    }
    
}
