package com.fourtwenty.core.domain.models.marketing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 7/6/17.
 */
@CollectionName(name = "marketing_jobs",premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketingJob extends ShopBaseModel {

    public enum MarketingType {
        Blasts,
        Automation
    }

    public enum MarketingJobType {
        SMS,
        Email
    }

    public enum MarketingJobStatus {
        Pending,
        InProcess,
        Completed,
        Failed
    }

    public enum MarketingRecipientType {
        AllMembers,
        MemberGroup,
        InactiveMembers,
        MembersBoughtXCategoryInLastYDateRange,
        MembersBoughtProductsFromVendorXInYDateRange,
        MembersVisitedXTimesInYDateRange,
        MembersSpentXDollarsInYDateRange,
        MembersHadTransactionsBetweenDateRange,
        AutomatedBirthdayText,
        AutomatedRecommendationExpireText,
        SpecificMembers,
        MembersWithSpecificTags
    }


    private String reqEmployeeId;
    private String message; // can be html if email

    private MarketingJobType jobType = MarketingJobType.SMS;
    private MarketingJobStatus status = MarketingJobStatus.Pending;
    private MarketingRecipientType recipientType = MarketingRecipientType.AllMembers;

    private List<String> memberGroupIds = new ArrayList<>();
    private Long reqSendDate;
    private Long completedDate;
    private boolean active = true;
    private int inactiveDays = 0;
    private int sentCount = 0;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal sentCost = new BigDecimal(0);


    private int pendingCount = 0;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal pendingCost = new BigDecimal(0);

    private String vendorId;
    private String categoryId;
    private long startDate;
    private long endDate;
    private int noOfVisits;
    private BigDecimal amountDollars;
    private MarketingType type;
    private int dayBefore;
    private boolean enable = true;
    private CompanyAsset memberAsset;
    private Set<String> memberTags = new LinkedHashSet<>();

    public BigDecimal getPendingCost() {
        return pendingCost;
    }

    public void setPendingCost(BigDecimal pendingCost) {
        this.pendingCost = pendingCost;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }

    public int getInactiveDays() {
        return inactiveDays;
    }

    public void setInactiveDays(int inactiveDays) {
        this.inactiveDays = inactiveDays;
    }

    public BigDecimal getSentCost() {
        return sentCost;
    }

    public void setSentCost(BigDecimal sentCost) {
        this.sentCost = sentCost;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getSentCount() {
        return sentCount;
    }

    public void setSentCount(int sentCount) {
        this.sentCount = sentCount;
    }

    public Long getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Long completedDate) {
        this.completedDate = completedDate;
    }

    public MarketingJobType getJobType() {
        return jobType;
    }

    public void setJobType(MarketingJobType jobType) {
        this.jobType = jobType;
    }

    public List<String> getMemberGroupIds() {
        return memberGroupIds;
    }

    public void setMemberGroupIds(List<String> memberGroupIds) {
        this.memberGroupIds = memberGroupIds;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MarketingRecipientType getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(MarketingRecipientType recipientType) {
        this.recipientType = recipientType;
    }

    public String getReqEmployeeId() {
        return reqEmployeeId;
    }

    public void setReqEmployeeId(String reqEmployeeId) {
        this.reqEmployeeId = reqEmployeeId;
    }

    public Long getReqSendDate() {
        return reqSendDate;
    }

    public void setReqSendDate(Long reqSendDate) {
        this.reqSendDate = reqSendDate;
    }

    public MarketingJobStatus getStatus() {
        return status;
    }

    public void setStatus(MarketingJobStatus status) {
        this.status = status;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public int getNoOfVisits() {
        return noOfVisits;
    }

    public void setNoOfVisits(int noOfVisits) {
        this.noOfVisits = noOfVisits;
    }

    public BigDecimal getAmountDollars() {
        return amountDollars;
    }

    public void setAmountDollars(BigDecimal amountDollars) {
        this.amountDollars = amountDollars;
    }

    public MarketingType getType() {
        return type;
    }

    public void setType(MarketingType type) {
        this.type = type;
    }

    public int getDayBefore() {
        return dayBefore;
    }

    public void setDayBefore(int dayBefore) {
        this.dayBefore = dayBefore;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public CompanyAsset getMemberAsset() {
        return memberAsset;
    }

    public void setMemberAsset(CompanyAsset memberAsset) {
        this.memberAsset = memberAsset;
    }

    public Set<String> getMemberTags() {
        return memberTags;
    }

    public void setMemberTags(Set<String> memberTags) {
        this.memberTags = memberTags;
    }
}
