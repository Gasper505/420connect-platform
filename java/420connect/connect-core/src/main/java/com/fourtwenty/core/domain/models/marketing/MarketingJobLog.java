package com.fourtwenty.core.domain.models.marketing;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "marketing_job_logs", premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketingJobLog extends ShopBaseModel {

    public enum MSentStatus {
        PENDING,
        SENT,
        DELIVERED,
        OPT_OUT,
        UNDELIVERED,
        UNKNOWN_ERROR,
        FAILED,
        OPT_IN
    }

    private String marketingJobId;
    private String fromNumber;
    private String toNumber;
    private MSentStatus status = MSentStatus.PENDING;
    private String memberId;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMarketingJobId() {
        return marketingJobId;
    }

    public void setMarketingJobId(String marketingJobId) {
        this.marketingJobId = marketingJobId;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public MSentStatus getStatus() {
        return status;
    }

    public void setStatus(MSentStatus status) {
        this.status = status;
    }
}
