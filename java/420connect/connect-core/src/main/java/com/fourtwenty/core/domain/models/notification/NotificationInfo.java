package com.fourtwenty.core.domain.models.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "notification_info", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)

public class NotificationInfo extends ShopBaseModel {
    public enum NotificationType {
        Daily_Summary,
        Low_Inventory,
        Incoming_Order,
        Consumer_New_Order,
        Consumer_Accept_Order,
        Consumer_Update_Order,
        Company_Licence_Expiration,
        Product_Batch_Expiration
    }

    private long lastSent = 0;
    private NotificationType notificationType;
    private boolean active = false;
    private String defaultEmail;
    private String defaultNumber;
    private long nextNotificationTime = 0;

    public long getNextNotificationTime() {
        return nextNotificationTime;
    }

    public void setNextNotificationTime(long nextNotificationTime) {
        this.nextNotificationTime = nextNotificationTime;
    }

    public long getLastSent() {
        return lastSent;
    }

    public void setLastSent(long lastSent) {
        this.lastSent = lastSent;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDefaultEmail() {
        return defaultEmail;
    }

    public void setDefaultEmail(String defaultEmail) {
        this.defaultEmail = defaultEmail;
    }

    public String getDefaultNumber() {
        return defaultNumber;
    }

    public void setDefaultNumber(String defaultNumber) {
        this.defaultNumber = defaultNumber;
    }
}
