package com.fourtwenty.core.domain.models.onprem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

import java.util.HashMap;
import java.util.Map;

@CollectionName(name = "onprem_sync",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OnPremSyncDetails extends CompanyBaseModel {

    private String entity;
    private SyncType syncType;
    private long lastSync;
    private Map<Long, Integer> syncRecords = new HashMap<>(); // Map of SyncTime & Total Synced Records

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public SyncType getSyncType() {
        return syncType;
    }

    public void setSyncType(SyncType syncType) {
        this.syncType = syncType;
    }

    public long getLastSync() {
        return lastSync;
    }

    public void setLastSync(long lastSync) {
        this.lastSync = lastSync;
    }

    public Map<Long, Integer> getSyncRecords() {
        return syncRecords;
    }

    public void setSyncRecords(Map<Long, Integer> syncRecords) {
        this.syncRecords = syncRecords;
    }

    public enum SyncType {
        Pull, Push
    }

}
