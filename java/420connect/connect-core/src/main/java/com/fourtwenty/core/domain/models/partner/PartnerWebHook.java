package com.fourtwenty.core.domain.models.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.List;

@CollectionName(name = "partner_webHooks")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerWebHook extends ShopBaseModel {
    public enum PartnerWebHookType {
        NEW_CONSUMER_ORDER,
        NEW_CONSUMER_SIGNUP,
        UPDATE_CONSUMER_ORDER,
        NEW_MEMBER,
        UPDATE_MEMBER,
        COMPLETE_TRANSACTION,
        UPDATE_PRODUCT
    }

    @Deprecated
    private String url;
    private PartnerWebHookType webHookType;
    private List<String> webHookUrls;

    public PartnerWebHookType getWebHookType() {
        return webHookType;
    }

    public void setWebHookType(PartnerWebHookType webHookType) {
        this.webHookType = webHookType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getWebHookUrls() {
        return webHookUrls;
    }

    public void setWebHookUrls(List<String> webHookUrls) {
        this.webHookUrls = webHookUrls;
    }
}
