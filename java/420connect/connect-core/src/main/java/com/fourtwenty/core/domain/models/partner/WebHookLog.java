package com.fourtwenty.core.domain.models.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "webHook_errorLog")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookLog extends ShopBaseModel {
    private PartnerWebHook.PartnerWebHookType webHookType;
    private String webHookId;
    private String message;
    private String url;

    public PartnerWebHook.PartnerWebHookType getWebHookType() {
        return webHookType;
    }

    public void setWebHookType(PartnerWebHook.PartnerWebHookType webHookType) {
        this.webHookType = webHookType;
    }

    public String getWebHookId() {
        return webHookId;
    }

    public void setWebHookId(String webHookId) {
        this.webHookId = webHookId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
