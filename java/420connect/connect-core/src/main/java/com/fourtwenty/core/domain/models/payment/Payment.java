package com.fourtwenty.core.domain.models.payment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created on 23/10/17 5:41 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */

@CollectionName(name = "payment_history", indexes = {"{companyId:1,shopId:1,purchaseReferenceId:1,paymentComponent:1}", "{companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Payment extends ShopBaseModel implements OnPremSyncable {

    private PaymentRecurringType paymentRecurringType;
    private String paymentTransactionId;
    private double amount;
    private long paymentDate;
    private PaymentStatus paymentStatus;
    private PaymentComponent paymentComponent;
    private PaymentProvider paymentProvider;
    private String failureReason;
    private String requestId;
    private String purchaseReferenceId;
    private String checkoutUrl;

    public PaymentRecurringType getPaymentRecurringType() {
        return paymentRecurringType;
    }

    public void setPaymentRecurringType(PaymentRecurringType paymentRecurringType) {
        this.paymentRecurringType = paymentRecurringType;
    }

    public String getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public void setPaymentTransactionId(String paymentTransactionId) {
        this.paymentTransactionId = paymentTransactionId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(long paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PaymentComponent getPaymentComponent() {
        return paymentComponent;
    }

    public void setPaymentComponent(PaymentComponent paymentComponent) {
        this.paymentComponent = paymentComponent;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPurchaseReferenceId() {
        return purchaseReferenceId;
    }

    public void setPurchaseReferenceId(String purchaseReferenceId) {
        this.purchaseReferenceId = purchaseReferenceId;
    }

    public String getCheckoutUrl() {
        return checkoutUrl;
    }

    public void setCheckoutUrl(String checkoutUrl) {
        this.checkoutUrl = checkoutUrl;
    }
}
