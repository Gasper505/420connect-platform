package com.fourtwenty.core.domain.models.payment;

/**
 * Created on 24/10/17 12:01 AM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public enum PaymentComponent {
    SMS_MARKETING, PLUGINS
}
