package com.fourtwenty.core.domain.models.payment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Raja Dushyant Vashishtha
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentProviderDetails {

    private PaymentProvider paymentProvider;

    private String stripePublishKey;
    private long wepayClientKey;

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public String getStripePublishKey() {
        return stripePublishKey;
    }

    public void setStripePublishKey(String stripePublishKey) {
        this.stripePublishKey = stripePublishKey;
    }

    public long getWepayClientKey() {
        return wepayClientKey;
    }

    public void setWepayClientKey(long wepayClientKey) {
        this.wepayClientKey = wepayClientKey;
    }
}
