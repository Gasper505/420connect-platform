package com.fourtwenty.core.domain.models.paymentcard;

public enum PaymentCardType {
    Linx,
    Clover,
    Mtrac
}
