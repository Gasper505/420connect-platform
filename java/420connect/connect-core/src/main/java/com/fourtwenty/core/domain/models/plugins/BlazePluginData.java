package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlazePluginData {

    @JsonProperty("messagingProducts")
    private List<BlazePluginProduct> messagingProducts = new ArrayList<>();

    @JsonProperty("tvProducts")
    private List<BlazePluginProduct> tvProducts = new ArrayList<>();

    @JsonProperty("consumerKioskProducts")
    private List<BlazePluginProduct> consumerKioskProducts = new ArrayList<>();

    public List<BlazePluginProduct> getMessagingProducts() {
        return messagingProducts;
    }

    public void setMessagingProducts(List<BlazePluginProduct> messagingProducts) {
        this.messagingProducts = messagingProducts;
    }

    public List<BlazePluginProduct> getTvProducts() {
        return tvProducts;
    }

    public void setTvProducts(List<BlazePluginProduct> tvProducts) {
        this.tvProducts = tvProducts;
    }

    public List<BlazePluginProduct> getConsumerKioskProducts() {
        return consumerKioskProducts;
    }

    public void setConsumerKioskProducts(List<BlazePluginProduct> consumerKioskProducts) {
        this.consumerKioskProducts = consumerKioskProducts;
    }
}
