package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@CollectionName(name = "blaze_plugin_product")
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlazePluginProduct extends BaseModel {

    public enum PluginType {
        MESSAGING,
        TV,
        CONSUMER_KIOSK
    }

    public enum PriceType {
        MONTHLY,
        YEARLY
    }

    private String productSKU;
    private PluginType pluginType;
    private Integer availableDevices;
    private Integer messages;
    @DecimalMin("0")
    private BigDecimal price;
    private PriceType priceType;

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public PluginType getPluginType() {
        return pluginType;
    }

    public void setPluginType(PluginType pluginType) {
        this.pluginType = pluginType;
    }

    public Integer getAvailableDevices() {
        return availableDevices;
    }

    public void setAvailableDevices(Integer availableDevices) {
        this.availableDevices = availableDevices;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public PriceType getPriceFrequency() {
        return priceType;
    }

    public void setPriceFrequency(PriceType priceFrequency) {
        this.priceType = priceFrequency;
    }

    public PriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(PriceType priceType) {
        this.priceType = priceType;
    }

    public Integer getMessages() {
        return messages;
    }

    public void setMessages(Integer messages) {
        this.messages = messages;
    }

    @Override
    public int hashCode() {
        int hash = 31;
        hash = 31 * hash + (this.productSKU != null ? this.productSKU.hashCode() : 0);
        hash = 31 * hash + (this.pluginType != null ? this.pluginType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        final BlazePluginProduct product = (BlazePluginProduct) obj;

        //Check if productSKU is unique
        if (product.productSKU == null) {
            if (this.productSKU != null)
                return false;
        } else if (!product.productSKU.equals(productSKU))
            return false;

        if (product.pluginType == null) {
            if (this.pluginType != null)
                return false;
        } else if (!product.pluginType.equals(pluginType))
            return false;

        return true;
    }
}
