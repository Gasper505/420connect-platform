package com.fourtwenty.core.domain.models.plugins;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

import java.util.List;

@CollectionName(name = "plugins_kiosk", indexes = {"{companyId:1,pluginId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskPluginCompanySetting extends PluginCompanySetting {

    private List<KioskPluginShopSetting> kioskSetting;

    public List<KioskPluginShopSetting> getKioskSetting() {
        return kioskSetting;
    }

    public void setKioskSetting(List<KioskPluginShopSetting> kioskSetting) {
        this.kioskSetting = kioskSetting;
    }
}
