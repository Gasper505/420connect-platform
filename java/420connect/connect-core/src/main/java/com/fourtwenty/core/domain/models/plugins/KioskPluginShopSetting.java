package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskPluginShopSetting extends PluginShopSetting {
    private String deviceName;
    private String welcomeText;
    private String backgroundColor;
    private CompanyAsset backgroundImage;

    private String themeColor;
    private String buttonTextColor;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getWelcomeText() {
        return welcomeText;
    }

    public void setWelcomeText(String welcomeText) {
        this.welcomeText = welcomeText;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public CompanyAsset getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(CompanyAsset backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String getButtonTextColor() {
        return buttonTextColor;
    }

    public void setButtonTextColor(String buttonTextColor) {
        this.buttonTextColor = buttonTextColor;
    }

    @Override
    public int hashCode() {

        int hash = 31;
        hash = 31 * hash + (this.companyId != null ? this.companyId.hashCode() : 0);
        hash = 31 * hash + (this.shopId != null ? this.shopId.hashCode() : 0);
        hash = 31 * hash + (this.deviceName != null ? this.deviceName.hashCode() : 0);

        return hash;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass())
            return false;

        final KioskPluginShopSetting that = (KioskPluginShopSetting) obj;

        // Check for company id
        if (that.companyId == null) {
            if (this.companyId != null)
                return false;
        } else if (!that.companyId.equals(this.companyId)) {
            return false;
        }

        // Check for shop
        if (that.shopId == null) {
            if (this.shopId != null)
                return false;
        } else if (!that.shopId.equals(this.shopId)) {
            return false;
        }

        // Check for device name
        if (that.deviceName == null) {
            if (this.deviceName != null)
                return false;
        } else if (!that.deviceName.equals(this.deviceName)) {
            return false;
        }

        return true;
    }
}
