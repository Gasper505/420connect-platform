package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

import java.util.List;

@CollectionName(name = "plugins_messaging", indexes = {"{companyId:1},{pluginId:1}, {messagingSetting.shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessagingPluginCompanySetting extends PluginCompanySetting {

    private List<MessagingPluginShopSetting> messagingSetting;

    public List<MessagingPluginShopSetting> getMessagingSetting() {
        return messagingSetting;
    }

    public void setMessagingSetting(List<MessagingPluginShopSetting> messagingSetting) {
        this.messagingSetting = messagingSetting;
    }
}