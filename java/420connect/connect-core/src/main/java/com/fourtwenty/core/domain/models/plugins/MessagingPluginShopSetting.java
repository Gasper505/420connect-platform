package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessagingPluginShopSetting extends PluginShopSetting {

    private String twilioPhoneNumber;

    public String getTwilioPhoneNumber() {
        return twilioPhoneNumber;
    }

    public void setTwilioPhoneNumber(String twilioPhoneNumber) {
        this.twilioPhoneNumber = twilioPhoneNumber;
    }

    @Override
    public int hashCode() {

        int hash = 31;
        hash = 31 * hash + (this.companyId != null ? this.companyId.hashCode() : 0);
        hash = 31 * hash + (this.shopId != null ? this.shopId.hashCode() : 0);

        return hash;
    }

    /**
     * Since there will be 1 Shop setting per plugin per shop in a company so we have used only company & shop for equality
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass())
            return false;

        final MessagingPluginShopSetting that = (MessagingPluginShopSetting) obj;

        // Check for company id
        if (that.companyId == null) {
            if (this.companyId != null)
                return false;
        } else if (!that.companyId.equals(this.companyId)) {
            return false;
        }

        // Check for shop
        if (that.shopId == null) {
            if (this.shopId != null)
                return false;
        } else if (!that.shopId.equals(this.shopId)) {
            return false;
        }

        return true;
    }
}
