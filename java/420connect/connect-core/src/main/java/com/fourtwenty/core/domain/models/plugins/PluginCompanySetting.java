package com.fourtwenty.core.domain.models.plugins;

import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

public class PluginCompanySetting extends CompanyBaseModel {
    private String pluginId;
    private Boolean enabled;

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
