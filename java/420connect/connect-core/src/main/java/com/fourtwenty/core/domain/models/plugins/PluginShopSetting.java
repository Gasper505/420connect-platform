package com.fourtwenty.core.domain.models.plugins;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

public class PluginShopSetting extends ShopBaseModel {

    private Boolean enabled;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
