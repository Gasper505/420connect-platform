package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by decipher on 14/10/17 12:50 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "tv_display", indexes = {"{tvDisplayId:1,companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TVDisplay extends ShopBaseModel {

    public enum TVDisplayTheme {
        Light,
        Dark,
        Warm,
        Cool,
        LightBlue
    }

    public enum TVDisplayOrientation {
        Portrait,
        Landscape
    }

    public enum TVDisplayColumn {
        Column_2("2 Column"),
        Column_2_No_Tests("2 Column No Tests"),
        Column_3("3 Column"),
        Column_4("4 Column"),
        Column_2_Portrait("Portrait mode: 2 Columns"),
        Column_2_Landscape("Landscape mode: 2 Columns"),
        Column_3_Landscape("Landscape mode: 3 Columns");

        private String column;

        TVDisplayColumn(String column) {
            this.column = column;
        }

        public String getColumn() {
            return column;
        }
    }

    private Long tvNumber;

    private String identifier;

    private String name;

    private TVDisplayTheme theme = TVDisplayTheme.Light;

    private TVDisplayOrientation orientation = TVDisplayOrientation.Portrait;

    private TVDisplayColumn column = TVDisplayColumn.Column_2;

    private Integer rotationTime = 0;

    private boolean published = false;

    private List<TVDisplayContent> contentList = new ArrayList<>();

    public Long getTvNumber() {
        return tvNumber;
    }

    public void setTvNumber(Long tvNumber) {
        this.tvNumber = tvNumber;
    }

    public TVDisplayColumn getColumn() {
        return column;
    }

    public void setColumn(TVDisplayColumn column) {
        this.column = column;
    }

    public List<TVDisplayContent> getContentList() {
        return contentList;
    }

    public void setContentList(List<TVDisplayContent> contentList) {
        this.contentList = contentList;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TVDisplayOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(TVDisplayOrientation orientation) {
        this.orientation = orientation;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Integer getRotationTime() {
        return rotationTime;
    }

    public void setRotationTime(Integer rotationTime) {
        this.rotationTime = rotationTime;
    }

    public TVDisplayTheme getTheme() {
        return theme;
    }

    public void setTheme(TVDisplayTheme theme) {
        this.theme = theme;
    }
}