package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Transaction;

/**
 * Created by decipher on 14/10/17 2:10 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "tv_display_content")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TVDisplayContent extends ShopBaseModel {
    public enum TVDisplayContentType {
        WaitList,
        ProductCategory,
        SingleImage,
        EmployeeStats,
        FulfillmentQueue
    }

    private String productCategoryId;
    private String secondCategoryId;
    private Integer rotationTime;
    private TVDisplayContentType tvDisplayContentType;
    private ProductCategory productCategory;
    private ProductCategory secondCategory;
    private CompanyAsset singleImage;
    private Transaction.FulfillmentStep fulfillmentStep = Transaction.FulfillmentStep.Fulfill;
    private Transaction.QueueType queueType = Transaction.QueueType.WalkIn;

    public CompanyAsset getSingleImage() {
        return singleImage;
    }

    public void setSingleImage(CompanyAsset singleImage) {
        this.singleImage = singleImage;
    }

    public ProductCategory getSecondCategory() {
        return secondCategory;
    }

    public void setSecondCategory(ProductCategory secondCategory) {
        this.secondCategory = secondCategory;
    }

    public String getSecondCategoryId() {
        return secondCategoryId;
    }

    public void setSecondCategoryId(String secondCategoryId) {
        this.secondCategoryId = secondCategoryId;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getRotationTime() {
        return rotationTime;
    }

    public void setRotationTime(Integer rotationTime) {
        this.rotationTime = rotationTime;
    }

    public TVDisplayContentType getTvDisplayContentType() {
        return tvDisplayContentType;
    }

    public void setTvDisplayContentType(TVDisplayContentType tvDisplayContentType) {
        this.tvDisplayContentType = tvDisplayContentType;
    }

    public Transaction.FulfillmentStep getFulfillmentStep() {
        return fulfillmentStep;
    }

    public void setFulfillmentStep(Transaction.FulfillmentStep fulfillmentStep) {
        this.fulfillmentStep = fulfillmentStep;
    }

    public Transaction.QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(Transaction.QueueType queueType) {
        this.queueType = queueType;
    }
}