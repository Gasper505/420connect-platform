package com.fourtwenty.core.domain.models.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;

@CollectionName(name = "plugins_tv", indexes = {"{companyId:1},{pluginId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TVPluginCompanySetting extends PluginCompanySetting {
}
