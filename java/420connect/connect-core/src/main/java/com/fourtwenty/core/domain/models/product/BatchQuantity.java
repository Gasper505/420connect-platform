package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 10/11/17.
 */
@CollectionName(name = "batch_quantities", premSyncDown = false,uniqueIndexes = {"{companyId:1,shopId:1,productId:1,inventoryId:1,batchId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchQuantity extends ShopBaseModel {
    private String productId;
    private String inventoryId;
    private String batchId;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal quantity;

    private ProductCategory.UnitType unitType = ProductCategory.UnitType.units;
    private long batchPurchaseDate;

    public long getBatchPurchaseDate() {
        return batchPurchaseDate;
    }

    public void setBatchPurchaseDate(long batchPurchaseDate) {
        this.batchPurchaseDate = batchPurchaseDate;
    }

    public ProductCategory.UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(ProductCategory.UnitType unitType) {
        this.unitType = unitType;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
