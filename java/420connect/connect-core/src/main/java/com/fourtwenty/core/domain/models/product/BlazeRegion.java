package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;

import java.util.LinkedHashSet;


@CollectionName(name = "regions", uniqueIndexes = {"{companyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlazeRegion extends CompanyBaseModel {
    private String name; // Region name
    private boolean active = true;
    private double latitude;
    private double longitude;
    private LinkedHashSet<String> zipCodes = new LinkedHashSet<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public LinkedHashSet<String> getZipCodes() {
        return zipCodes;
    }

    public void setZipCodes(LinkedHashSet<String> zipCodes) {
        this.zipCodes = zipCodes;
    }
}
