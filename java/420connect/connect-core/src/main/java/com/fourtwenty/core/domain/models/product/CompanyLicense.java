package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.BaseModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyLicense extends BaseModel {
    private Vendor.CompanyType companyType;
    private Vendor.LicenceType licenseType;
    private String licenseNumber;
    private long licenseExpirationDate;
    private boolean toDefault;

    public Vendor.CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Vendor.CompanyType companyType) {
        this.companyType = companyType;
    }

    public Vendor.LicenceType getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(Vendor.LicenceType licenseType) {
        this.licenseType = licenseType;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public long getLicenseExpirationDate() {
        return licenseExpirationDate;
    }

    public void setLicenseExpirationDate(long licenseExpirationDate) {
        this.licenseExpirationDate = licenseExpirationDate;
    }

    public boolean isToDefault() {
        return toDefault;
    }

    public void setToDefault(boolean toDefault) {
        this.toDefault = toDefault;
    }
}
