package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DerivedProductBatchInfo extends BaseModel {

    @NotEmpty
    private String batchId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantity = new BigDecimal(0);
    private String inventoryId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }
}
