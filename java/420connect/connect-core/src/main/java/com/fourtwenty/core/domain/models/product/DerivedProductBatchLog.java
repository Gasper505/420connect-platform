package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CollectionName(name = "derived_batch_log", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DerivedProductBatchLog extends ShopBaseModel {

    private String derivedProductId;
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal derivedQuantity = BigDecimal.ZERO;

    private Map<String, List<DerivedProductBatchInfo>> productMap = new HashMap<>();

    public String getDerivedProductId() {
        return derivedProductId;
    }

    public void setDerivedProductId(String derivedProductId) {
        this.derivedProductId = derivedProductId;
    }

    public BigDecimal getDerivedQuantity() {
        return derivedQuantity;
    }

    public void setDerivedQuantity(BigDecimal derivedQuantity) {
        this.derivedQuantity = derivedQuantity;
    }

    public Map<String, List<DerivedProductBatchInfo>> getProductMap() {
        return productMap;
    }

    public void setProductMap(Map<String, List<DerivedProductBatchInfo>> productMap) {
        this.productMap = productMap;
    }
}
