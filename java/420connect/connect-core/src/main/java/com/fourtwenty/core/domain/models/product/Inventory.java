package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.LinkedHashSet;

@CollectionName(name = "inventories", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Inventory extends ShopBaseModel implements OnPremSyncable, InternalAllowable {
    public static final String SAFE = "Safe";
    public static final String EXCHANGE = "Exchange";
    public static final String QUARANTINE = "Quarantine";

    public enum InventoryType {
        Storage,
        Field,
        Quarantine
    }

    private InventoryType type = InventoryType.Field;
    private String name;
    private boolean active = true; // default is active
    private LinkedHashSet<String> regionIds = new LinkedHashSet<>();
    private String externalId;
    private boolean dryRoom = true;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InventoryType getType() {
        return type;
    }

    public void setType(InventoryType type) {
        this.type = type;
    }

    public LinkedHashSet<String> getRegionIds() {
        return regionIds;
    }

    public void setRegionIds(LinkedHashSet<String> regionIds) {
        this.regionIds = regionIds;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public boolean isDryRoom() {
        return dryRoom;
    }

    public void setDryRoom(boolean dryRoom) {
        this.dryRoom = dryRoom;
    }
}
