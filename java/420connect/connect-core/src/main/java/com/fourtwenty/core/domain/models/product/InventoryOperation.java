package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@CollectionName(name = "inventory_operations",premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryOperation extends ShopBaseModel {

    public enum Operation {
        Adjust(1),
        Revert(2);

        int operation;

        Operation(int operation) {
            this.operation = operation;
        }

        public int getOperation() {
            return operation;
        }
    }

    public enum SourceType {
        None,
        PurchaseOrder,
        Transaction,
        RefundTransaction,
        Transfer,
        Reconciliation,
        ProductBatch,
        Prepackage,
        ShippingManifest,
        StockReset,
        Product,
        System,
        DerivedProduct,
        BundleProduct,
        RevertShippingManifest,
        RejectShippingManifest
    }

    public enum SubSourceAction {
        AddBatch,
        UpdateBatch,
        AddPrepackage,
        UpdatePrepackage,
        ReducePrepackage,
        DeletePrepackage,
        DeletePrepackageItem,
        Sale,
        Refund,
        Reconcile,
        Transfer,
        AddTestSample,
        DeleteTestSample,
        UpdateTestSample,
        ConvertToProduct
    }

    private Operation action = Operation.Adjust;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal quantity = new BigDecimal(0);

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal runningTotal = new BigDecimal(0);

    private boolean prepackaged = false;
    private String prepackageItemId;
    private String batchId;
    private String inventoryId;
    private String productId;
    private String employeeId;
    private SourceType sourceType = SourceType.None;
    private String sourceId;
    private String sourceChildId;
    private String requestId;
    private SubSourceAction subSourceAction;


    public boolean isPrepackaged() {
        return prepackaged;
    }

    public void setPrepackaged(boolean prepackaged) {
        this.prepackaged = prepackaged;
    }

    public BigDecimal getRunningTotal() {
        return runningTotal;
    }

    public void setRunningTotal(BigDecimal runningTotal) {
        this.runningTotal = runningTotal;
    }

    public Operation getAction() {
        return action;
    }

    public void setAction(Operation action) {
        this.action = action;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceChildId() {
        return sourceChildId;
    }

    public void setSourceChildId(String sourceChildId) {
        this.sourceChildId = sourceChildId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public SubSourceAction getSubSourceAction() {
        return subSourceAction;
    }

    public void setSubSourceAction(SubSourceAction subSourceAction) {
        this.subSourceAction = subSourceAction;
    }
}
