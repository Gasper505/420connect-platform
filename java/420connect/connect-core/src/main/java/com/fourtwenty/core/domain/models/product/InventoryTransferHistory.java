package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.Transactionable;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Created by Raja Dushyant Vashishtha
 */
@CollectionName(name = "inventory_transfer_history", indexes = {"{companyId:1,shopId:1,delete:1,status:1,created:-1}", "{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryTransferHistory extends ShopBaseModel implements Transactionable {

    public enum TransferStatus {
        ACCEPTED,
        DECLINED,
        PENDING
    }

    private TransferStatus status;
    private String createByEmployeeId;
    private String acceptByEmployeeId;
    private String declineByEmployeeId;


    private String fromShopId;
    private String toShopId;

    @NotEmpty
    private String fromInventoryId;
    @NotEmpty
    private String toInventoryId;

    LinkedHashSet<InventoryTransferLog> transferLogs = new LinkedHashSet<>();
    Map<String, Double> batchQuantityInfoMap = new HashMap<>();

    private Long declinedDate;
    private Long acceptedDate;
    private String transferNo;
    private boolean completeTransfer;
    private boolean transferByBatch;
    private Long processedTime;
    private boolean processing = Boolean.FALSE;
    private String driverId;

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public String getCreateByEmployeeId() {
        return createByEmployeeId;
    }

    public void setCreateByEmployeeId(String createByEmployeeId) {
        this.createByEmployeeId = createByEmployeeId;
    }

    public String getFromShopId() {
        return fromShopId;
    }

    public void setFromShopId(String fromShopId) {
        this.fromShopId = fromShopId;
    }

    public String getToShopId() {
        return toShopId;
    }

    public void setToShopId(String toShopId) {
        this.toShopId = toShopId;
    }

    public LinkedHashSet<InventoryTransferLog> getTransferLogs() {
        return transferLogs;
    }

    public void setTransferLogs(LinkedHashSet<InventoryTransferLog> transferLogs) {
        this.transferLogs = transferLogs;
    }

    public String getFromInventoryId() {
        return fromInventoryId;
    }

    public void setFromInventoryId(String fromInventoryId) {
        this.fromInventoryId = fromInventoryId;
    }

    public String getToInventoryId() {
        return toInventoryId;
    }

    public void setToInventoryId(String toInventoryId) {
        this.toInventoryId = toInventoryId;
    }

    public String getAcceptByEmployeeId() {
        return acceptByEmployeeId;
    }

    public void setAcceptByEmployeeId(String acceptByEmployeeId) {
        this.acceptByEmployeeId = acceptByEmployeeId;
    }

    public String getDeclineByEmployeeId() {
        return declineByEmployeeId;
    }

    public void setDeclineByEmployeeId(String declineByEmployeeId) {
        this.declineByEmployeeId = declineByEmployeeId;
    }

    public Long getDeclinedDate() {
        return declinedDate;
    }

    public void setDeclinedDate(Long declinedDate) {
        this.declinedDate = declinedDate;
    }

    public Long getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Long acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public String getTransferNo() {
        return transferNo;
    }

    public void setTransferNo(String transferNo) {
        this.transferNo = transferNo;
    }

    public boolean isCompleteTransfer() {
        return completeTransfer;
    }

    public void setCompleteTransfer(boolean completeTransfer) {
        this.completeTransfer = completeTransfer;
    }

    public boolean isTransferByBatch() {
        return transferByBatch;
    }

    public void setTransferByBatch(boolean transferByBatch) {
        this.transferByBatch = transferByBatch;
    }

    public Map<String, Double> getBatchQuantityInfoMap() {
        return batchQuantityInfoMap;
    }

    public void setBatchQuantityInfoMap(Map<String, Double> batchQuantityInfoMap) {
        this.batchQuantityInfoMap = batchQuantityInfoMap;
    }

    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
