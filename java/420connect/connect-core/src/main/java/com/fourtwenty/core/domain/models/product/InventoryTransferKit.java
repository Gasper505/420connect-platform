package com.fourtwenty.core.domain.models.product;

import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;


import java.util.LinkedHashSet;

@CollectionName(name = "inventory_transfer_kit", indexes = {"{companyId:1,shopId:1,delete:1}"})
public class InventoryTransferKit  extends ShopBaseModel {

    private String kitName;
    private String fromShopId;
    private String toShopId;
    private String fromInventoryId;
    private String toInventoryId;
    private boolean transferByBatch;
    private LinkedHashSet<InventoryTransferLog> transferLogs = new LinkedHashSet<>();

    public String getKitName() {
        return kitName;
    }

    public void setKitName(String kitName) {
        this.kitName = kitName;
    }

    public String getFromShopId() {
        return fromShopId;
    }

    public void setFromShopId(String fromShopId) {
        this.fromShopId = fromShopId;
    }

    public String getToShopId() {
        return toShopId;
    }

    public void setToShopId(String toShopId) {
        this.toShopId = toShopId;
    }

    public String getFromInventoryId() {
        return fromInventoryId;
    }

    public void setFromInventoryId(String fromInventoryId) {
        this.fromInventoryId = fromInventoryId;
    }

    public String getToInventoryId() {
        return toInventoryId;
    }

    public void setToInventoryId(String toInventoryId) {
        this.toInventoryId = toInventoryId;
    }

    public boolean isTransferByBatch() {
        return transferByBatch;
    }

    public void setTransferByBatch(boolean transferByBatch) {
        this.transferByBatch = transferByBatch;
    }

    public LinkedHashSet<InventoryTransferLog> getTransferLogs() {
        return transferLogs;
    }

    public void setTransferLogs(LinkedHashSet<InventoryTransferLog> transferLogs) {
        this.transferLogs = transferLogs;
    }
}
