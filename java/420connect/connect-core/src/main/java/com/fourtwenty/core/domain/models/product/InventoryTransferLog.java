package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Raja Dushyant Vashishtha
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryTransferLog {

    @NotEmpty
    private String productId;
    private String prepackageItemId;


    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal transferAmount = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal finalInventory;


    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal origFromQty;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal finalFromQty;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal origToQty;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal prevTransferAmt;

    private String fromBatchId;
    private String fromBatchInfo;
    private String prepackageName;

    private String toBatchId;
    private ProductBatch fromProductBatch;
    private ProductBatch toProductBatch;
    //<batchId, quantity>
    private Map<String, BigDecimal> batchQuantityMap = new HashMap<>();

    private transient Product product;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InventoryTransferLog that = (InventoryTransferLog) o;

        if (that.productId == null) {
            if (this.productId != null)
                return false;
        } else if (!productId.equals(that.productId))
            return false;

        if (that.prepackageItemId == null) {
            if (this.prepackageItemId != null)
                return false;
        } else if (!prepackageItemId.equals(that.prepackageItemId))
            return false;

        if (that.fromBatchId == null) {
            if (this.fromBatchId != null)
                return false;
        } else if (!this.fromBatchId.equals(that.fromBatchId))
            return false;

        return true;
        //return !(productId != null ? !productId.equals(that.productId) : that.productId != null);
    }

    @Override
    public int hashCode() {
        int hash = 31;
        hash = 31 * hash + (this.productId != null ? this.productId.hashCode() : 0);
        hash = 31 * hash + (this.prepackageItemId != null ? this.prepackageItemId.hashCode() : 0);
        hash = 31 * hash + (this.fromBatchId != null ? this.fromBatchId.hashCode() : 0);

        return hash;
        //return productId != null ? productId.hashCode() : 0;
    }


    public BigDecimal getPrevTransferAmt() {
        return prevTransferAmt;
    }

    public void setPrevTransferAmt(BigDecimal prevTransferAmt) {
        this.prevTransferAmt = prevTransferAmt;
    }

    public BigDecimal getFinalFromQty() {
        return finalFromQty;
    }

    public void setFinalFromQty(BigDecimal finalFromQty) {
        this.finalFromQty = finalFromQty;
    }

    public BigDecimal getOrigFromQty() {
        return origFromQty;
    }

    public void setOrigFromQty(BigDecimal origFromQty) {
        this.origFromQty = origFromQty;
    }

    public BigDecimal getOrigToQty() {
        return origToQty;
    }

    public void setOrigToQty(BigDecimal origToQty) {
        this.origToQty = origToQty;
    }

    public String getPrepackageName() {
        return prepackageName;
    }

    public void setPrepackageName(String prepackageName) {
        this.prepackageName = prepackageName;
    }

    public String getFromBatchInfo() {
        return fromBatchInfo;
    }

    public void setFromBatchInfo(String fromBatchInfo) {
        this.fromBatchInfo = fromBatchInfo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public BigDecimal getFinalInventory() {
        return finalInventory;
    }

    public void setFinalInventory(BigDecimal finalInventory) {
        this.finalInventory = finalInventory;
    }

    public String getFromBatchId() {
        return fromBatchId;
    }

    public void setFromBatchId(String fromBatchId) {
        this.fromBatchId = fromBatchId;
    }

    public String getToBatchId() {
        return toBatchId;
    }

    public void setToBatchId(String toBatchId) {
        this.toBatchId = toBatchId;
    }

    public ProductBatch getFromProductBatch() {
        return fromProductBatch;
    }

    public void setFromProductBatch(ProductBatch fromProductBatch) {
        this.fromProductBatch = fromProductBatch;
    }

    public ProductBatch getToProductBatch() {
        return toProductBatch;
    }

    public void setToProductBatch(ProductBatch toProductBatch) {
        this.toProductBatch = toProductBatch;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Map<String, BigDecimal> getBatchQuantityMap() {
        return batchQuantityMap;
    }

    public void setBatchQuantityMap(Map<String, BigDecimal> batchQuantityMap) {
        this.batchQuantityMap = batchQuantityMap;
    }



}
