package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalPercentageSerializer;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PotencyMG {
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    private BigDecimal thc;
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    private BigDecimal cbd;
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    private BigDecimal cbn;
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    private BigDecimal thca;
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    private BigDecimal cbda;


    public BigDecimal getThc() {
        return thc;
    }

    public void setThc(BigDecimal thc) {
        this.thc = thc;
    }

    public BigDecimal getCbd() {
        return cbd;
    }

    public void setCbd(BigDecimal cbd) {
        this.cbd = cbd;
    }

    public BigDecimal getCbn() {
        return cbn;
    }

    public void setCbn(BigDecimal cbn) {
        this.cbn = cbn;
    }

    public BigDecimal getThca() {
        return thca;
    }

    public void setThca(BigDecimal thca) {
        this.thca = thca;
    }

    public BigDecimal getCbda() {
        return cbda;
    }

    public void setCbda(BigDecimal cbda) {
        this.cbda = cbda;
    }
}
