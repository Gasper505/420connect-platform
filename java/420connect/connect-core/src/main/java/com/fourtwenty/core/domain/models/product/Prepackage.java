package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 3/15/17.
 */
@CollectionName(name = "prepackages", uniqueIndexes = {"{productId:1,name:1}"}, indexes = {"{companyId:1,shopId:1,delete:1}", "{companyId:1,shopId:1,productId:1,delete:1}", "{productId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel()
public class Prepackage extends ShopBaseModel {
    private boolean customWeight = true;
    private String toleranceId; //IF CUSTOM, THEN UNIT, PRICE, NAME, can be specified. Otherwise
    private String productId;
    private boolean active;

    // Custom if weightKey == Custom
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitValue = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal price = new BigDecimal(0);
    private String name;
    private int runningQuantity;
    private String sku;

    public String getToleranceId() {
        return toleranceId;
    }

    public void setToleranceId(String toleranceId) {
        this.toleranceId = toleranceId;
    }

    public boolean isCustomWeight() {
        return customWeight;
    }

    public void setCustomWeight(boolean customWeight) {
        this.customWeight = customWeight;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }

    public int getRunningQuantity() {
        return runningQuantity;
    }

    public void setRunningQuantity(int runningQuantity) {
        this.runningQuantity = runningQuantity;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
