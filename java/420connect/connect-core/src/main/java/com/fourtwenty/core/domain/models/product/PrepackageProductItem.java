package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.IBarcode;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.Transactionable;
import io.swagger.annotations.ApiModel;

/**
 * Created by mdo on 2/28/17.
 */
@CollectionName(name = "prepackage_items", uniqueIndexes = {"{productId:1,prepackageId:1,batchId:1}"}, indexes = {"{companyId:1,shopId:1,delete:1}", "{productId:1}", "{prepackageId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel()
public class PrepackageProductItem extends ShopBaseModel implements IBarcode, Transactionable {
    private String productId;
    private String batchId;
    private String batchSKU;
    private String prepackageId;
    private String sku;
    private boolean active;
    private int runningQuantity;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getBatchSKU() {
        return batchSKU;
    }

    public void setBatchSKU(String batchSKU) {
        this.batchSKU = batchSKU;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrepackageId() {
        return prepackageId;
    }

    public void setPrepackageId(String prepackageId) {
        this.prepackageId = prepackageId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getRunningQuantity() {
        return runningQuantity;
    }

    public void setRunningQuantity(int runningQuantity) {
        this.runningQuantity = runningQuantity;
    }
}
