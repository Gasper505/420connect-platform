package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@CollectionName(name = "pricing_template")
public class PricingTemplate extends ShopBaseModel implements OnPremSyncable {

    @NotEmpty
    private String name;
    private ProductCategory.UnitType unitType = ProductCategory.UnitType.grams;
    private Product.WeightPerUnit weightPerUnit = Product.WeightPerUnit.EACH;
    private List<ProductPriceRange> priceRanges;
    private List<ProductPriceBreak> priceBreaks;
    private boolean active = Boolean.FALSE;


    public Product.WeightPerUnit getWeightPerUnit() {
        return weightPerUnit;
    }

    public void setWeightPerUnit(Product.WeightPerUnit weightPerUnit) {
        this.weightPerUnit = weightPerUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductCategory.UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(ProductCategory.UnitType unitType) {
        this.unitType = unitType;
    }

    public List<ProductPriceRange> getPriceRanges() {
        return priceRanges;
    }

    public void setPriceRanges(List<ProductPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
    }

    public List<ProductPriceBreak> getPriceBreaks() {
        return priceBreaks;
    }

    public void setPriceBreaks(List<ProductPriceBreak> priceBreaks) {
        this.priceBreaks = priceBreaks;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
