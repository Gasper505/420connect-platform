package com.fourtwenty.core.domain.models.product;

import com.blaze.clients.metrcs.MetrcMeasurement;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.IBarcode;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.importer.main.Importable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


@CollectionName(name = "products", premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}", "{companyId:1,shopId:1,active:1}", "{name:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel()
public class Product extends ShopBaseModel implements Importable, IBarcode, InternalAllowable,OnPremSyncable {

    public enum ProductSaleType {
        Medicinal,
        Recreational,
        Both
    }

    public enum WeightPerUnit {
        EACH("1 ea"),
        HALF_GRAM(".5 g"),
        FULL_GRAM("1 g"),
        EIGHTH("1/8th g"),
        FOURTH("1/4th g"),
        HALF_OUNCE(".5 oz"),
        OUNCE("1 oz"),
        CUSTOM_GRAMS("");

        String weightPerUnit;
        WeightPerUnit(String weight) {
            this.weightPerUnit = weight;
        }

        public String getWeightPerUnit() {
            return weightPerUnit;
        }

        public ProductWeightTolerance.WeightKey getCorrespondingWeightKey() {
            switch (this) {
                case EACH:
                    return ProductWeightTolerance.WeightKey.UNIT;
                case HALF_GRAM:
                    return ProductWeightTolerance.WeightKey.HALF_GRAM;
                case FULL_GRAM:
                    return ProductWeightTolerance.WeightKey.GRAM;
                case EIGHTH:
                    return ProductWeightTolerance.WeightKey.ONE_EIGHTTH;
                case FOURTH:
                    return ProductWeightTolerance.WeightKey.QUARTER;
                case HALF_OUNCE:
                    return ProductWeightTolerance.WeightKey.HALF;
                case OUNCE:
                    return ProductWeightTolerance.WeightKey.OUNCE;
                case CUSTOM_GRAMS:
                    return ProductWeightTolerance.WeightKey.UNIT;
                default:
                    return ProductWeightTolerance.WeightKey.UNIT;
            }
        }

        public MetrcMeasurement toMetrcMeasurement() {
            switch (this) {
                case EACH:
                    return MetrcMeasurement.Each;
                case HALF_GRAM:
                    return MetrcMeasurement.Grams;
                case FULL_GRAM:
                    return MetrcMeasurement.Grams;
                case EIGHTH:
                    return MetrcMeasurement.Grams;
                case FOURTH:
                    return MetrcMeasurement.Grams;
                case HALF_OUNCE:
                    return MetrcMeasurement.Grams;
                case OUNCE:
                    return MetrcMeasurement.Ounces;
                case CUSTOM_GRAMS:
                    return MetrcMeasurement.Grams;
                default:
                    return MetrcMeasurement.Each;
            }
        }


    }

    public enum CustomGramType {
        GRAM("g"),
        MILLIGRAM("mg"),
        MILLILITER("ml");

        String type;

        CustomGramType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public enum CannabisType {
        @Deprecated
        NONE(-1, "None"), // DEPRECREATED, DO NOT USE
        DEFAULT(1, "Default"), // non-concentrates
        NON_CANNABIS(-1, "Non-cannabis"),
        CBD(-1, "CBD"), //non-cannabis
        CBD_CANNABIS(0, "CBD-Cannabis"), //cannabis
        CONCENTRATE(0, "Concentrate"), //concentrates
        NON_CONCENTRATE(1, "Non-concentrate"), // non-concentrates
        PLANT(2, "Plant"), //plants
        EDIBLE(0, "Edible"), //concentrates
        EXTRACT(0, "Extract"), //concentrates
        DRY_FLOWER(1, "Dried Flower"), // non-concentrates
        KIEF(0, "Kief"), //concentrates
        DRY_LEAF(1, "Dried Leaf"), // non-concentrates
        LIQUID(0, "Liquid"), //concentrates
        SUPPOSITORY(0, "Suppository"), //concentrates
        TINCTURE(0, "Tincture"), //concentrates
        TOPICAL(0, "Topical"), //concentrates
        OIL(0, "Oil"), //concentrates
        SEEDS(3, "Seeds"),
        THC(4, "THC"),
        LIVE_PLANT(5, "Live Plants"); //live plants;

        CannabisType(int type, String displayName) {
            this.type = type;
            this.displayName = displayName;
            // 0 - concentrate
            // 1 - non-concentrates
            // 2 - plants
            // 3 - seeds
        }
        private String displayName = "";
        private int type = -1;
        public int getType() {
            return type;
        }

        public String getDisplayName() {
            return displayName;
        }

        public static HashMap<CannabisType, CannabisType> getCannabisMapping() {
            HashMap<Product.CannabisType, Product.CannabisType> mapping = new HashMap<Product.CannabisType, Product.CannabisType>();
            Product.CannabisType[] types = Product.CannabisType.values();
            for (Product.CannabisType cannabisType : types) {
                switch (cannabisType.getType()) {
                    case 0: // concentrate
                        mapping.put(cannabisType, Product.CannabisType.CONCENTRATE);
                        break;
                    case 1:
                        mapping.put(cannabisType, Product.CannabisType.NON_CONCENTRATE);
                        // non-concentrate
                        break;
                    case 2:
                        mapping.put(cannabisType, Product.CannabisType.PLANT);
                        // plants
                        break;
                    case 3:
                        mapping.put(cannabisType, CannabisType.SEEDS);
                        // seeds
                        break;
                    case 4:
                        mapping.put(cannabisType, CannabisType.THC);
                        // thc
                        break;
                    case 5:
                        mapping.put(cannabisType, CannabisType.LIVE_PLANT);
                        // live plants
                        break;
                    default:
                        mapping.put(cannabisType, Product.CannabisType.NON_CANNABIS);
                        // unknown or n/a
                        break;
                }
            }

            return mapping;
        }
    }

    public enum ProductType {
        REGULAR,
        DERIVED,
        BUNDLE
    }

    private String importId;
    private String importSrc;
//    @NotEmpty Commenting it as it may empty in case of derived product
    private String categoryId;
    private String sku;
    private String vendorId;
    private ProductSaleType productSaleType = ProductSaleType.Both;
    @ApiModelProperty(value = "product name", required = true)
    @NotEmpty
    private String name;
    private String description = "";
    private String flowerType;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0f);
    private WeightPerUnit weightPerUnit = WeightPerUnit.EACH;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitValue = new BigDecimal(1f); // Default is 1. Unit Value indicate the value of a single product starting point.
    private double thc;
    private double cbn;
    private double cbd;
    private double cbda;
    private double thca;
    private boolean active = true; // default is true
    private String genetics;
    private List<MedicalCondition> medicalConditions = new ArrayList<>();
    private List<ProductPriceRange> priceRanges = new ArrayList<>();
    private List<ProductPriceBreak> priceBreaks = new ArrayList<>();
    private List<CompanyAsset> assets = new ArrayList<>();
    private List<ProductQuantity> quantities = new ArrayList<>();
    private boolean instock = false;
    private String brandId;
    private ProductCategory category;
    private List<Note> notes = new ArrayList<>();
    private boolean enableMixMatch = false; // MixMatch within same categoryId and same price points
    private boolean enableWeedmap = false;
    private boolean showInWidget = true;
    private TaxInfo.TaxType taxType = TaxInfo.TaxType.Inherit;
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed
    private TaxInfo customTaxInfo;
    private boolean discountable = true;
    // Transient. Will be override based on vendorId on return
    private Vendor vendor;
    private Brand brand;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal lowThreshold;
    private boolean lowInventoryNotification = false;
    @Deprecated
    private boolean medicinal = true;
    private boolean byGram = true;
    private boolean byPrepackage = false;
    private boolean enableExciseTax = Boolean.FALSE;
    private boolean priceIncludesExcise = Boolean.FALSE;
    private boolean priceIncludesALExcise = Boolean.TRUE;
    private CannabisType cannabisType = CannabisType.DEFAULT;
    private PotencyMG potencyAmount;
    private List<CompoundTaxTable> taxTables = new ArrayList<>();
    private LinkedHashSet<String> tags = new LinkedHashSet<>();
    private String qbItemRef;
    private boolean isPotency;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal customWeight = new BigDecimal(0f);
    private boolean automaticReOrder = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal reOrderLevel = new BigDecimal(0f);
    private CustomGramType customGramType = CustomGramType.GRAM;
    private String pricingTemplateId;
    private ProductType productType = ProductType.REGULAR;
    private List<BundleItem> bundleItems = new ArrayList<>();
    private String externalId;

    private String qbDesktopItemRef;
    private String editSequence;
    private String qbListId;
    private boolean qbErrored;
    private long errorTime;
    private boolean qbQuantitySynced = Boolean.FALSE;
    private boolean qbQuantityErrored;
    private long qbQuantityErrorTime;
    private String complianceId;
    private String toleranceId;
    private String producerMfg;
    private String producerLicense;
    private Address producerAddress;
    private boolean sellable = true;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal wmThreshold = BigDecimal.ZERO;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal salesPrice;

    private long lastWMSyncTime;
    private long lastLeaflySyncTime;
    private Boolean lastLeaflySyncStatus = Boolean.FALSE;

    public long getLastWMSyncTime() {
        return lastWMSyncTime;
    }

    public void setLastWMSyncTime(long lastWMSyncTime) {
        this.lastWMSyncTime = lastWMSyncTime;
    }

    public String getComplianceId() {
        return complianceId;
    }

    public void setComplianceId(String complianceId) {
        this.complianceId = complianceId;
    }
    private Set<String> secondaryVendors = new HashSet<>();

    public boolean isInstock() {
        return instock;
    }

    public void setInstock(boolean instock) {
        this.instock = instock;
    }

    public String getImportSrc() {
        return importSrc;
    }

    public void setImportSrc(String importSrc) {
        this.importSrc = importSrc;
    }

    public boolean isPriceIncludesALExcise() {
        return priceIncludesALExcise;
    }

    public void setPriceIncludesALExcise(boolean priceIncludesALExcise) {
        this.priceIncludesALExcise = priceIncludesALExcise;
    }

    public List<CompoundTaxTable> getTaxTables() {
        return taxTables;
    }

    public void setTaxTables(List<CompoundTaxTable> taxTables) {
        this.taxTables = taxTables;
    }

    public CannabisType getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(CannabisType cannabisType) {
        this.cannabisType = cannabisType;
    }

    public double getCbd() {
        return cbd;
    }

    public void setCbd(double cbd) {
        this.cbd = cbd;
    }

    public double getCbda() {
        return cbda;
    }

    public void setCbda(double cbda) {
        this.cbda = cbda;
    }

    public double getCbn() {
        return cbn;
    }

    public void setCbn(double cbn) {
        this.cbn = cbn;
    }

    public double getThc() {
        return thc;
    }

    public void setThc(double thc) {
        this.thc = thc;
    }

    public double getThca() {
        return thca;
    }

    public void setThca(double thca) {
        this.thca = thca;
    }

    public boolean isDiscountable() {
        return discountable;
    }

    public void setDiscountable(boolean discountable) {
        this.discountable = discountable;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Brand getBrand() {
        return brand;
    }

    public WeightPerUnit getWeightPerUnit() {
        return weightPerUnit;
    }

    public void setWeightPerUnit(WeightPerUnit weightPerUnit) {
        this.weightPerUnit = weightPerUnit;
    }

    public BigDecimal getUnitPriceWithPriceBreaks(int qty, MemberGroupPrices memberGroupPrices, BigDecimal overridePrice) {
        BigDecimal cUnitPrice = this.unitPrice;

        if (qty > 0 && overridePrice == null) {
            // Sort Price Breaks by qty
            Comparator<ProductPriceBreak> comparator = new Comparator<ProductPriceBreak>() {
                @Override
                public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                    Integer q1 = o1.getQuantity();
                    Integer q2 = o2.getQuantity();
                    return q2.compareTo(q1);
                }
            };
            priceBreaks.sort(comparator);

            List<ProductPriceBreak> groupPriceBreaks = new ArrayList<>();
            if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
                groupPriceBreaks = memberGroupPrices.getPriceBreaks();
            }

            groupPriceBreaks.sort(comparator);

            boolean found = false;

            for (ProductPriceBreak priceBreak : groupPriceBreaks) {
                if (priceBreak.isActive() && qty >= priceBreak.getQuantity()) {
                    double price = priceBreak.getAssignedPrice().doubleValue();
                    double newUnitPrice = price / priceBreak.getQuantity();
                    cUnitPrice = new BigDecimal(newUnitPrice);
                    found = true;
                    break;
                }
            }

            // if not found, check the price breaks
            if (!found) {
                for (ProductPriceBreak priceBreak : priceBreaks) {
                    if (priceBreak.isActive() && qty >= priceBreak.getQuantity()) {
                        if (priceBreak.getPrice() == null || priceBreak.getQuantity() <= 0) {
                            continue;
                        }
                        double price = priceBreak.getAssignedPrice().doubleValue();
                        double newUnitPrice = price / priceBreak.getQuantity();
                        cUnitPrice = new BigDecimal(newUnitPrice);
                        break;
                    }
                }
            }
        } else if (qty > 0 && overridePrice != null) {

            cUnitPrice = overridePrice;
        }
        return cUnitPrice;
    }

    @JsonProperty("companyLinkId")
    public String getCompanyLinkId() {
        /*String categoryName = "";
        ProductCategory.UnitType unitType = ProductCategory.UnitType.units;
        if (this.getCategory() != null) {
            categoryName = this.getCategory().getName();
            unitType = this.getCategory().getUnitType();
<<<<<<< HEAD
        }
=======
        }*/
        String key = this.getSku();//String.format("%s_%s_%s_%s", categoryName, this.getName().toLowerCase(), this.getVendorId(), unitType);

        String encoded = Base64.getEncoder().encodeToString(key.getBytes(Charset.forName("UTF8")));
        return encoded;
    }

    public TaxInfo getCustomTaxInfo() {
        return customTaxInfo;
    }

    public void setCustomTaxInfo(TaxInfo customTaxInfo) {
        this.customTaxInfo = customTaxInfo;
    }

    public TaxInfo.TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxInfo.TaxType taxType) {
        this.taxType = taxType;
    }

    public List<ProductPriceBreak> getPriceBreaks() {
        return priceBreaks;
    }

    public void setPriceBreaks(List<ProductPriceBreak> priceBreaks) {
        this.priceBreaks = priceBreaks;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }

    public boolean isEnableWeedmap() {
        return enableWeedmap;
    }

    public void setEnableWeedmap(boolean enableWeedmap) {
        this.enableWeedmap = enableWeedmap;
    }

    public boolean isEnableMixMatch() {
        return enableMixMatch;
    }

    public void setEnableMixMatch(boolean enableMixMatch) {
        this.enableMixMatch = enableMixMatch;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public List<ProductQuantity> getQuantities() {
        return quantities;
    }

    public void setQuantities(List<ProductQuantity> quantities) {
        this.quantities = quantities;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(String flowerType) {
        this.flowerType = flowerType;
    }

    @Override
    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public List<MedicalCondition> getMedicalConditions() {
        return medicalConditions;
    }

    public void setMedicalConditions(List<MedicalCondition> medicalConditions) {
        this.medicalConditions = medicalConditions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public List<ProductPriceRange> getPriceRanges() {
        return priceRanges;
    }

    public void setPriceRanges(List<ProductPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getGenetics() {
        return genetics;
    }

    public void setGenetics(String genetics) {
        this.genetics = genetics;
    }

    public boolean isLowInventoryNotification() {
        return lowInventoryNotification;
    }

    public void setLowInventoryNotification(boolean lowInventoryNotification) {
        this.lowInventoryNotification = lowInventoryNotification;
    }

    public BigDecimal getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(BigDecimal lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public boolean isShowInWidget() {
        return showInWidget;
    }

    public void setShowInWidget(boolean showInWidget) {
        this.showInWidget = showInWidget;
    }

    public boolean isMedicinal() {
        return medicinal;
    }

    public void setMedicinal(boolean medicinal) {
        this.medicinal = medicinal;
    }

    public boolean isByGram() {
        return byGram;
    }

    public void setByGram(boolean byGram) {
        this.byGram = byGram;
    }

    public boolean isByPrepackage() {
        return byPrepackage;
    }

    public void setByPrepackage(boolean byPrepackage) {
        this.byPrepackage = byPrepackage;
    }

    public LinkedHashSet<String> getTags() {
        return tags;
    }

    public void setTags(LinkedHashSet<String> tags) {
        this.tags = tags;
    }

    public ProductSaleType getProductSaleType() {
        return productSaleType;
    }

    public void setProductSaleType(ProductSaleType type) {
        this.productSaleType = type;
    }

    public boolean isEnableExciseTax() {
        return enableExciseTax;
    }

    public void setEnableExciseTax(boolean enableExciseTax) {
        this.enableExciseTax = enableExciseTax;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public PotencyMG getPotencyAmount() {
        return potencyAmount;
    }

    public void setPotencyAmount(PotencyMG potencyAmount) {
        this.potencyAmount = potencyAmount;
    }

    public boolean isPriceIncludesExcise() {
        return priceIncludesExcise;
    }

    public void setPriceIncludesExcise(boolean priceIncludesExcise) {
        this.priceIncludesExcise = priceIncludesExcise;
    }

    public boolean isPotency() {
        return isPotency;
    }

    public void setPotency(boolean potency) {
        isPotency = potency;
    }

    public String getQbItemRef() {
        return qbItemRef;
    }

    public void setQbItemRef(String qbItemRef) {
        this.qbItemRef = qbItemRef;
    }

    public BigDecimal getCustomWeight() {
        return customWeight;
    }

    public void setCustomWeight(BigDecimal customWeight) {
        this.customWeight = customWeight;
    }

    public boolean isAutomaticReOrder() {
        return automaticReOrder;
    }

    public void setAutomaticReOrder(boolean automaticReOrder) {
        this.automaticReOrder = automaticReOrder;
    }

    public BigDecimal getReOrderLevel() {
        return reOrderLevel;
    }

    public void setReOrderLevel(BigDecimal reOrderLevel) {
        this.reOrderLevel = reOrderLevel;
    }

    public CustomGramType getCustomGramType() {
        return customGramType;
    }

    public void setCustomGramType(CustomGramType customGramType) {
        this.customGramType = customGramType;
    }

    public String getQbDesktopItemRef() {
        return qbDesktopItemRef;
    }

    public void setQbDesktopItemRef(String qbDesktopItemRef) {
        this.qbDesktopItemRef = qbDesktopItemRef;
    }

    public String getPricingTemplateId() {
        return pricingTemplateId;
    }

    public void setPricingTemplateId(String pricingTemplateId) {
        this.pricingTemplateId = pricingTemplateId;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public List<BundleItem> getBundleItems() {
        return bundleItems;
    }

    public void setBundleItems(List<BundleItem> bundleItems) {
        this.bundleItems = bundleItems;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getQbListId() {
        return qbListId;
    }

    public void setQbListId(String qbListId) {
        this.qbListId = qbListId;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }

    public boolean isQbQuantitySynced() {
        return qbQuantitySynced;
    }

    public void setQbQuantitySynced(boolean qbQuantitySynced) {
        this.qbQuantitySynced = qbQuantitySynced;
    }

    public boolean isQbQuantityErrored() {
        return qbQuantityErrored;
    }

    public void setQbQuantityErrored(boolean qbQuantityErrored) {
        this.qbQuantityErrored = qbQuantityErrored;
    }

    public long getQbQuantityErrorTime() {
        return qbQuantityErrorTime;
    }

    public void setQbQuantityErrorTime(long qbQuantityErrorTime) {
        this.qbQuantityErrorTime = qbQuantityErrorTime;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getExternalId() {
        return this.externalId;
    }

    public String getToleranceId() {
        return toleranceId;
    }

    public void setToleranceId(String toleranceId) {
        this.toleranceId = toleranceId;
    }

    public String getProducerMfg() {
        return producerMfg;
    }

    public void setProducerMfg(String producerMfg) {
        this.producerMfg = producerMfg;
    }

    public String getProducerLicense() {
        return producerLicense;
    }

    public void setProducerLicense(String producerLicense) {
        this.producerLicense = producerLicense;
    }

    public Address getProducerAddress() {
        return producerAddress;
    }

    public void setProducerAddress(Address producerAddress) {
        this.producerAddress = producerAddress;
    }

    public Set<String> getSecondaryVendors() {
        return secondaryVendors;
    }

    public void setSecondaryVendors(Set<String> secondaryVendors) {
        this.secondaryVendors = secondaryVendors;
    }

    public boolean isSellable() {
        return sellable;
    }

    public void setSellable(boolean sellable) {
        this.sellable = sellable;
    }

    public BigDecimal getWmThreshold() {
        return wmThreshold;
    }

    public void setWmThreshold(BigDecimal wmThreshold) {
        this.wmThreshold = wmThreshold;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(BigDecimal salesPrice) {
        this.salesPrice = salesPrice;
    }

    public long getLastLeaflySyncTime() {
        return lastLeaflySyncTime;
    }

    public void setLastLeaflySyncTime(long lastLeaflySyncTime) {
        this.lastLeaflySyncTime = lastLeaflySyncTime;
    }

    public Boolean getLastLeaflySyncStatus() {
        return lastLeaflySyncStatus;
    }

    public void setLastLeaflySyncStatus(Boolean lastLeaflySyncStatus) {
        this.lastLeaflySyncStatus = lastLeaflySyncStatus;
    }

    public void setProductVersion(ProductVersion productVersion) {
        this.priceBreaks = productVersion.getPriceBreaks();
        this.priceRanges = productVersion.getPriceRanges();
        this.categoryId = productVersion.getCategoryId();
        this.name = productVersion.getName();
        this.unitPrice  = productVersion.getUnitPrice();
        this.weightPerUnit = productVersion.getWeightPerUnit();
        this.unitValue  = productVersion.getUnitValue();
        this.active  = productVersion.isActive();
        this.medicalConditions  = productVersion.getMedicalConditions();
        this.quantities  = productVersion.getQuantities();
        this.brandId = productVersion.getBrandId();
        this.category = productVersion.getCategory();
        this.enableMixMatch  = productVersion.isEnableMixMatch();
        this.enableWeedmap  = productVersion.isEnableWeedmap();
        this.showInWidget = productVersion.isShowInWidget();
        this.taxType = productVersion.getTaxType();
        this.taxOrder  = productVersion.getTaxOrder();
        this.customTaxInfo = productVersion.getCustomTaxInfo();
        this.discountable  = productVersion.isDiscountable();
        this.byGram = productVersion.isByGram();
        this.byPrepackage  = productVersion.isByPrepackage();
        this.enableExciseTax  = productVersion.isEnableExciseTax();
        this.priceIncludesExcise  = productVersion.isPriceIncludesExcise();
        this.priceIncludesALExcise = productVersion.isPriceIncludesALExcise();
        this.salesPrice = productVersion.getSalesPrice();
    }

    @JsonIgnore
    public boolean isCannabisProduct(boolean cannabisCategory) {
        return ((cannabisType == Product.CannabisType.DEFAULT && cannabisCategory)
                || (cannabisType != Product.CannabisType.CBD
                && cannabisType != Product.CannabisType.NON_CANNABIS
                && cannabisType != Product.CannabisType.DEFAULT));
    }
}