package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.IBarcode;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.Transactionable;
import com.fourtwenty.core.domain.serializers.*;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchBundleItems;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponseHit;
import com.fourtwenty.core.thirdparty.elasticsearch.models.types.Analyzer;
import com.fourtwenty.core.util.NumberUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.json.JSONObject;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CollectionName(name = "product_batches", indexes = {"{companyId:1,shopId:1,productId:1,delete:1}", "{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatch extends ShopBaseModel implements IBarcode, Transactionable, ElasticSearchCapable, InternalAllowable,OnPremSyncable {
    public enum TrackTraceSystem {
        MANUAL,
        METRC
    }

    public enum BatchStatus {
        RECEIVED,
        IN_TESTING,
        READY_FOR_SALE
    }

    public enum BatchFilter {
        RECEIVE_DATE,
        BATCH_ID,
        PRODUCT_NAME,
        CATEGORY,
        PO_NUMBER,
        STATUS,
        VENDOR_NAME,
        BRAND_NAME,
        QUANTITY_RECEIVED
    }

    @NotEmpty
    private String productId;
    @Deprecated
    private String vendorId;
    private int batchNo = 0;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal cost;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal costPerUnit;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal quantity;



    private String sku; // unique id
    private double thc;
    private double cbn;
    private double cbd;
    private double cbda;
    private double thca;

    @Min(0)
    private long purchasedDate;
    private long sellBy;
    private boolean archived = false;
    private Long archivedDate;
    private boolean published = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal publishedQuantity;
    private Long publishedDate;

    private boolean trackTraceVerified = false;
    private Integer trackPackageId;
    private String trackPackageLabel;
    private String trackHarvestBatch;
    private String trackHarvestDate;
    @JsonDeserialize(using = ProductBatchTrackTraceSystemDeserializer.class)
    private TrackTraceSystem trackTraceSystem = TrackTraceSystem.MANUAL;
    private ProductWeightTolerance.WeightKey trackWeight = ProductWeightTolerance.WeightKey.CUSTOM;

    private String purchaseOrderId;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal perUnitExciseTax = new BigDecimal(0);
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal totalExciseTax = new BigDecimal(0);
    private String customerCompanyId;

    private Vendor.ArmsLengthType armsLengthType = null;

    // changes from incoming batch
    private String brandId;
    @Deprecated
    private Long receiveDate;
    private String poNumber;
    private BatchStatus status = BatchStatus.RECEIVED;
    private String metrcCategory;
    private Boolean voidStatus = false;
    private boolean active;
    private CompanyAsset batchQRAsset;

    private ProductBatchLabel productBatchLabel;
    private String lotId;
    private String metrcTagId;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCultivationTax = new BigDecimal(0);
    private PotencyMG potencyAmount;
    private boolean isPrepaidTax;
    private Note referenceNote;
    private PurchaseOrder.FlowerSourceType flowerSourceType;
    private List<CompanyAsset> attachments;
    private String productName;
    private String vendorName;
    private String brandName;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal liveQuantity = BigDecimal.ZERO;

    private String derivedLogId;
    private long expirationDate;
    private String licenseId;
    private String labelInfo;

    private List<BatchBundleItems> bundleItems = new ArrayList<>();

    /* operations fields*/
    private String externalId;
    private String externalLicense;
    private String roomId; //inventoryId

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal actualWeightPerUnit;
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal waste;
    private boolean unProcessed = false;
    private double moistureLoss;


    private String connectedBatchId; //Batch id of assigned distro batch when a product is bundle


    @JsonIgnore
    public BigDecimal getFinalUnitCost() {
        if (costPerUnit != null && NumberUtils.round(costPerUnit, 2) > 0) {
            return costPerUnit;
        } else if (cost != null && quantity != null && NumberUtils.round(quantity, 2) > 0) {
            return new BigDecimal(NumberUtils.round(NumberUtils.round(cost, 2) / NumberUtils.round(quantity, 2), 2));
        } else {
            return new BigDecimal(0);
        }
    }


    public BigDecimal getActualWeightPerUnit() {
        return actualWeightPerUnit;
    }

    public void setActualWeightPerUnit(BigDecimal actualWeightPerUnit) {
        this.actualWeightPerUnit = actualWeightPerUnit;
    }

    public Vendor.ArmsLengthType getArmsLengthType() {
        return armsLengthType;
    }

    public void setArmsLengthType(Vendor.ArmsLengthType armsLengthType) {
        this.armsLengthType = armsLengthType;
    }

    public String getTrackHarvestDate() {
        return trackHarvestDate;
    }

    public void setTrackHarvestDate(String trackHarvestDate) {
        this.trackHarvestDate = trackHarvestDate;
    }

    public ProductWeightTolerance.WeightKey getTrackWeight() {
        return trackWeight;
    }

    public void setTrackWeight(ProductWeightTolerance.WeightKey trackWeight) {
        this.trackWeight = trackWeight;
    }

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public BigDecimal getPublishedQuantity() {
        return publishedQuantity;
    }

    public void setPublishedQuantity(BigDecimal publishedQuantity) {
        this.publishedQuantity = publishedQuantity;
    }

    public Long getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(Long archivedDate) {
        this.archivedDate = archivedDate;
    }

    public Long getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Long publishedDate) {
        this.publishedDate = publishedDate;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean isTrackTraceVerified() {
        return trackTraceVerified;
    }

    public void setTrackTraceVerified(boolean trackTraceVerified) {
        this.trackTraceVerified = trackTraceVerified;
    }

    public long getSellBy() {
        return sellBy;
    }

    public void setSellBy(long sellBy) {
        this.sellBy = sellBy;
    }

    private List<Note> notes = new ArrayList<>();

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public int getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(int batchNo) {
        this.batchNo = batchNo;
    }

    public double getCbd() {
        return cbd;
    }

    public void setCbd(double cbd) {
        this.cbd = cbd;
    }

    public double getCbda() {
        return cbda;
    }

    public void setCbda(double cbda) {
        this.cbda = cbda;
    }

    public double getCbn() {
        return cbn;
    }

    public void setCbn(double cbn) {
        this.cbn = cbn;
    }


    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public long getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(long purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public double getThc() {
        return thc;
    }

    public void setThc(double thc) {
        this.thc = thc;
    }

    public double getThca() {
        return thca;
    }

    public void setThca(double thca) {
        this.thca = thca;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getTrackPackageLabel() {
        return trackPackageLabel;
    }

    public void setTrackPackageLabel(String trackPackageLabel) {
        this.trackPackageLabel = trackPackageLabel;
    }

    public TrackTraceSystem getTrackTraceSystem() {
        return trackTraceSystem;
    }

    public void setTrackTraceSystem(TrackTraceSystem trackTraceSystem) {
        this.trackTraceSystem = trackTraceSystem;
    }

    public Integer getTrackPackageId() {
        return trackPackageId;
    }

    public void setTrackPackageId(Integer trackPackageId) {
        this.trackPackageId = trackPackageId;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }


    public String getTrackHarvestBatch() {
        return trackHarvestBatch;
    }

    public void setTrackHarvestBatch(String trackHarvestBatch) {
        this.trackHarvestBatch = trackHarvestBatch;
    }

    public BigDecimal getPerUnitExciseTax() {
        return perUnitExciseTax;
    }

    public void setPerUnitExciseTax(BigDecimal perUnitExciseTax) {
        this.perUnitExciseTax = perUnitExciseTax;
    }

    public BigDecimal getTotalExciseTax() {
        return totalExciseTax;
    }

    public void setTotalExciseTax(BigDecimal totalExciseTax) {
        this.totalExciseTax = totalExciseTax;
    }

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Long getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Long receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public BatchStatus getStatus() {
        return status;
    }

    public void setStatus(BatchStatus status) {
        this.status = status;
    }

    public String getMetrcCategory() {
        return metrcCategory;
    }

    public void setMetrcCategory(String metrcCategory) {
        this.metrcCategory = metrcCategory;
    }

    public Boolean getVoidStatus() {
        return voidStatus;
    }

    public void setVoidStatus(Boolean voidStatus) {
        this.voidStatus = voidStatus;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public CompanyAsset getBatchQRAsset() {
        return batchQRAsset;
    }

    public void setBatchQRAsset(CompanyAsset batchQRAsset) {
        this.batchQRAsset = batchQRAsset;
    }

    public ProductBatchLabel getProductBatchLabel() {
        return productBatchLabel;
    }

    public void setProductBatchLabel(ProductBatchLabel productBatchLabel) {
        this.productBatchLabel = productBatchLabel;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getMetrcTagId() {
        return metrcTagId;
    }

    public void setMetrcTagId(String metrcTagId) {
        this.metrcTagId = metrcTagId;
    }

    public BigDecimal getTotalCultivationTax() {
        return totalCultivationTax;
    }

    public void setTotalCultivationTax(BigDecimal totalCultivationTax) {
        this.totalCultivationTax = totalCultivationTax;
    }

    public PotencyMG getPotencyAmount() {
        return potencyAmount;
    }

    public void setPotencyAmount(PotencyMG potencyAmount) {
        this.potencyAmount = potencyAmount;
    }

    public boolean isPrepaidTax() {
        return isPrepaidTax;
    }

    public void setPrepaidTax(boolean prepaidTax) {
        isPrepaidTax = prepaidTax;
    }

    public Note getReferenceNote() {
        return referenceNote;
    }

    public void setReferenceNote(Note referenceNote) {
        this.referenceNote = referenceNote;
    }

    public PurchaseOrder.FlowerSourceType getFlowerSourceType() {
        return flowerSourceType;
    }

    public void setFlowerSourceType(PurchaseOrder.FlowerSourceType flowerSourceType) {
        this.flowerSourceType = flowerSourceType;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDerivedLogId() {
        return derivedLogId;
    }

    public void setDerivedLogId(String derivedLogId) {
        this.derivedLogId = derivedLogId;
    }

    public BigDecimal getLiveQuantity() {
        return liveQuantity;
    }

    public void setLiveQuantity(BigDecimal liveQuantity) {
        this.liveQuantity = liveQuantity;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getLabelInfo() {
        return labelInfo;
    }

    public void setLabelInfo(String labelInfo) {
        this.labelInfo = labelInfo;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }


    public List<BatchBundleItems> getBundleItems() {
        return bundleItems;
    }

    public void setBundleItems(List<BatchBundleItems> bundleItems) {
        this.bundleItems = bundleItems;
    }

    public String getExternalLicense() {
        return externalLicense;
    }

    public void setExternalLicense(String externalLicense) {
        this.externalLicense = externalLicense;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public BigDecimal getWaste() {
        return waste;
    }

    public void setWaste(BigDecimal waste) {
        this.waste = waste;
    }

    public String getConnectedBatchId() {
        return connectedBatchId;
    }

    public void setConnectedBatchId(String connectedBatchId) {
        this.connectedBatchId = connectedBatchId;
    }

    public boolean isUnProcessed() {
        return unProcessed;
    }

    public void setUnProcessed(boolean unProcessed) {
        this.unProcessed = unProcessed;
    }

    public double getMoistureLoss() {
        return moistureLoss;
    }

    public void setMoistureLoss(double moistureLoss) {
        this.moistureLoss = moistureLoss;
    }



    @Override
    public ElasticSearchIndex getElasticSearchIndex() {
        ElasticSearchIndex index = new ElasticSearchIndex();
        index.setIndex("batches");
        index.setType("batch");

        final Map<String, FieldIndex> properties = new HashMap<>();
        properties.put("companyId", new FieldIndex("keyword", null, true));
        properties.put("shopId", new FieldIndex("keyword", null, true));
        properties.put("sku", new FieldIndex("keyword", null, true));
        properties.put("productId", new FieldIndex("keyword", null, true));
        properties.put("vendorId", new FieldIndex("keyword", null, true));
        properties.put("brandId", new FieldIndex("keyword", null, true));

        properties.put("brandName", new FieldIndex("keyword", null, true));
        properties.put("vendorName", new FieldIndex("keyword", null, true));
        properties.put("productName", new FieldIndex("keyword", null, true));
        properties.put("status", new FieldIndex("keyword", null, true));
        properties.put("quantity", new FieldIndex("keyword", null, true));
        properties.put("created", new FieldIndex("keyword", null, true));
        properties.put("modified", new FieldIndex("keyword", null, true));
        properties.put("poNumber",  new FieldIndex("keyword", null, true));
        properties.put("purchaseDate",  new FieldIndex("keyword", null, true));
        properties.put("purchaseOrderId",  new FieldIndex("keyword", null, true));
        properties.put("receiveDate",  new FieldIndex("keyword", null, true));
        properties.put("trackHarvestBatch", new FieldIndex("keyword", null, true));
        properties.put("liveQuantity", new FieldIndex("keyword", null, true));
        properties.put("actualWeightPerUnit", new FieldIndex("keyword", null, true));

        // search fields
        properties.put("sku_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("brandName_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("vendorName_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("productName_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));
        properties.put("status_l", new FieldIndex("text", Analyzer.STANDARD.getValue(), true));

        index.setProperties(properties);

        return index;
    }

    @Override
    public void loadFrom(AWSSearchResponseHit hit) {
        for (String key : hit.source.keySet()) {
            switch (key) {
                case "companyId":
                    setCompanyId(hit.source.get(key).toString());
                    break;
                case "shopId":
                    setShopId(hit.source.get(key).toString());
                    break;
                case "sku":
                    setSku(hit.source.get(key).toString());
                    break;
                case "productId":
                    setProductId(hit.source.get(key).toString());
                    break;
                case "vendorId":
                    setVendorId(hit.source.get(key).toString());
                    break;
                case "brandId":
                    setBrandId(hit.source.get(key).toString());
                    break;
                case "brandName":
                    setBrandName(hit.source.get(key).toString());
                    break;
                case "vendorName":
                    setVendorName(hit.source.get(key).toString());
                    break;
                case "productName":
                    setProductName(hit.source.get(key).toString());
                    break;
                case "quantity":
                    setQuantity(hit.source.get(key) != null ? new BigDecimal(hit.source.get(key).toString()) : BigDecimal.ZERO);
                    break;
                case "status":
                    setStatus(BatchStatus.valueOf(hit.source.get(key).toString()));
                    break;
                case "created":
                    setCreated(Long.parseLong(hit.source.get(key).toString()));
                    break;
                case "modified":
                    setModified(Long.parseLong(hit.source.get(key).toString()));
                    break;
                case "purchaseDate":
                    setPurchasedDate(Long.parseLong(hit.source.get(key).toString()));
                    break;
                case "poNumber":
                    setPoNumber(hit.source.get(key).toString());
                    break;
                case "purchaseOrderId":
                    setPurchaseOrderId(hit.source.get(key).toString());
                    break;
                case "receiveDate" :
                    setReceiveDate(Long.parseLong(hit.source.get(key).toString()));
                    break;
                case "trackHarvestBatch" :
                    setTrackHarvestBatch(hit.source.get(key).toString());
                    break;
                case "liveQuantity" :
                    setLiveQuantity(hit.source.get(key) != null ? new BigDecimal(hit.source.get(key).toString()) : BigDecimal.ZERO);
                    break;
                case "actualWeightPerUnit" :
                    setActualWeightPerUnit(hit.source.get(key) != null ? new BigDecimal(hit.source.get(key).toString()) : BigDecimal.ZERO);
                    break;
            }
        }
    }

    @Override
    public JSONObject toElasticSearchObject() {
        final JSONObject jsonObject = new JSONObject();

        jsonObject.put("companyId", getCompanyId());
        jsonObject.put("shopId", getShopId());
        jsonObject.put("sku", getSku());
        jsonObject.put("productId", getProductId());
        jsonObject.put("vendorId", getVendorId());
        jsonObject.put("brandId", getBrandId());
        jsonObject.put("brandName", getBrandName());
        jsonObject.put("vendorName", getVendorName());
        jsonObject.put("productName", getProductName());
        jsonObject.put("deleted", isDeleted());
        jsonObject.put("quantity", String.valueOf(getQuantity()));
        jsonObject.put("created", getCreated());
        jsonObject.put("modified", getModified());
        jsonObject.put("poNumber", getPoNumber());
        jsonObject.put("purchaseDate", getPurchasedDate());
        jsonObject.put("purchaseOrderId", getPurchaseOrderId());
        jsonObject.put("status", getStatus());
        jsonObject.put("trackHarvestBatch", getTrackHarvestBatch());
        jsonObject.put("liveQuantity", getLiveQuantity());
        jsonObject.put("actualWeightPerUnit", getActualWeightPerUnit());

        jsonObject.put("sku_l", getSku() != null ? getSku().toLowerCase() : "");
        jsonObject.put("brandName_l", getBrandName() != null ? getBrandName().toLowerCase() : "");
        jsonObject.put("vendorName_l", getVendorName() != null ? getVendorName().toLowerCase() : "");
        jsonObject.put("productName_l", getProductName() != null ? getProductName().toLowerCase() : "");
        jsonObject.put("status_l", getStatus() != null ? getStatus().toString().toLowerCase() : "");

        return jsonObject;
    }
}