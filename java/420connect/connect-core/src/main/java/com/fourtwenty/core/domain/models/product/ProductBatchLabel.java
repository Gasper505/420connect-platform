package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatchLabel {
    public enum ProductBatchLabelType {
        TEST_RESULT,
        PRODUCT_ID

    }

    private ProductBatchLabelType labelType = ProductBatchLabelType.TEST_RESULT;
    private boolean enabledProductName = false;
    private boolean enablesBatchId = false;
    private boolean enablesLotId = false;
    private boolean enabledPackageId = false;
    private boolean enablesNetWeight = false;
    private boolean enabledBarCode = false;
    private boolean enabledQRCode = false;
    private boolean enableCultivationName = false;
    private boolean enableTestResults = false;
    private boolean enableTestDate = false;

    public ProductBatchLabelType getLabelType() {
        return labelType;
    }

    public void setLabelType(ProductBatchLabelType labelType) {
        this.labelType = labelType;
    }

    public boolean isEnabledProductName() {
        return enabledProductName;
    }

    public void setEnabledProductName(boolean enabledProductName) {
        this.enabledProductName = enabledProductName;
    }

    public boolean isEnablesBatchId() {
        return enablesBatchId;
    }

    public void setEnablesBatchId(boolean enablesBatchId) {
        this.enablesBatchId = enablesBatchId;
    }

    public boolean isEnablesLotId() {
        return enablesLotId;
    }

    public void setEnablesLotId(boolean enablesLotId) {
        this.enablesLotId = enablesLotId;
    }

    public boolean isEnabledPackageId() {
        return enabledPackageId;
    }

    public void setEnabledPackageId(boolean enabledPackageId) {
        this.enabledPackageId = enabledPackageId;
    }

    public boolean isEnablesNetWeight() {
        return enablesNetWeight;
    }

    public void setEnablesNetWeight(boolean enablesNetWeight) {
        this.enablesNetWeight = enablesNetWeight;
    }

    public boolean isEnabledBarCode() {
        return enabledBarCode;
    }

    public void setEnabledBarCode(boolean enabledBarCode) {
        this.enabledBarCode = enabledBarCode;
    }

    public boolean isEnabledQRCode() {
        return enabledQRCode;
    }

    public void setEnabledQRCode(boolean enabledQRCode) {
        this.enabledQRCode = enabledQRCode;
    }

    public boolean isEnableCultivationName() {
        return enableCultivationName;
    }

    public void setEnableCultivationName(boolean enableCultivationName) {
        this.enableCultivationName = enableCultivationName;
    }

    public boolean isEnableTestResults() {
        return enableTestResults;
    }

    public void setEnableTestResults(boolean enableTestResults) {
        this.enableTestResults = enableTestResults;
    }

    public boolean isEnableTestDate() {
        return enableTestDate;
    }

    public void setEnableTestDate(boolean enableTestDate) {
        this.enableTestDate = enableTestDate;
    }
}
