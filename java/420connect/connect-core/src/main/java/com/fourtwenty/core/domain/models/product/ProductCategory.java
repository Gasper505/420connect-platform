package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;


@CollectionName(name = "product_categories", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategory extends ShopBaseModel implements InternalAllowable,OnPremSyncable {
    //public static final String UNITS = "units";
    //public static final String GRAMS = "grams";
    public enum UnitType {
        units("units"),
        grams("grams");

        UnitType(String type) {
            this.type = type;
        }

        String type;

        public String getType() {
            return type;
        }

        public static UnitType toUnitType(String unitType) {
            if (StringUtils.isBlank(unitType)) {
                return units;
            }
            if (unitType.trim().equalsIgnoreCase(units.getType())) {
                return units;
            } else {
                return grams;
            }
        }
    }

    @NotEmpty
    private String name;
    private boolean cannabis;
    private CompanyAsset photo;
    private UnitType unitType = UnitType.units;
    private boolean active = true;
    private int priority = 0;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal lowThreshold;
    private Product.CannabisType cannabisType = Product.CannabisType.DEFAULT;
    private String qbDesktopRef;
    private String editSequence;
    private String qbListId;
    private boolean qbErrored;
    private long errorTime;

    private boolean qbQuantitySynced;
    private boolean qbQuantityErrored;
    private long qbQuantityErrorTime;
    private String complianceId;
    private String wmCategory;

    public String getComplianceId() {
        return complianceId;
    }

    public void setComplianceId(String complianceId) {
        this.complianceId = complianceId;
    }

    private String externalId;

    public Product.CannabisType getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(Product.CannabisType cannabisType) {
        this.cannabisType = cannabisType;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public CompanyAsset getPhoto() {
        return photo;
    }

    public void setPhoto(CompanyAsset photo) {
        this.photo = photo;
    }

    public boolean isCannabis() {
        return cannabis;
    }

    public void setCannabis(boolean cannabis) {
        this.cannabis = cannabis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }

    public BigDecimal getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(BigDecimal lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public String getQbDesktopRef() {
        return qbDesktopRef;
    }

    public void setQbDesktopRef(String qbDesktopRef) {
        this.qbDesktopRef = qbDesktopRef;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getQbListId() {
        return qbListId;
    }

    public void setQbListId(String qbListId) {
        this.qbListId = qbListId;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }

    public boolean isQbQuantitySynced() {
        return qbQuantitySynced;
    }

    public void setQbQuantitySynced(boolean qbQuantitySynced) {
        this.qbQuantitySynced = qbQuantitySynced;
    }

    public boolean isQbQuantityErrored() {
        return qbQuantityErrored;
    }

    public void setQbQuantityErrored(boolean qbQuantityErrored) {
        this.qbQuantityErrored = qbQuantityErrored;
    }

    public long getQbQuantityErrorTime() {
        return qbQuantityErrorTime;
    }

    public void setQbQuantityErrorTime(long qbQuantityErrorTime) {
        this.qbQuantityErrorTime = qbQuantityErrorTime;
    }

    public String getWmCategory() {
        return wmCategory;
    }

    public void setWmCategory(String wmCategory) {
        this.wmCategory = wmCategory;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

}