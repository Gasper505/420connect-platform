package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import io.swagger.annotations.ApiModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 11/6/16.
 */
@CollectionName(name = "product_log", premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}", "{companyId:1,shopId:1,productId:1,created:-1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel()
public class ProductChangeLog extends ShopBaseModel {
    private String employeeId;
    private String productId;
    private String reference;
    private List<ProductQuantity> productQuantities = new ArrayList<ProductQuantity>();
    private List<ProductPrepackageQuantity> prepackageQuantities = new ArrayList<>();
    private List<BatchQuantityLogs> batchQuantities = new ArrayList<>();
    private List<PrepackageQuantityLog> prepackageQuantityLogs = new ArrayList<>();

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<ProductQuantity> getProductQuantities() {
        return productQuantities;
    }

    public void setProductQuantities(List<ProductQuantity> productQuantities) {
        this.productQuantities = productQuantities;
    }

    public List<ProductPrepackageQuantity> getPrepackageQuantities() {
        return prepackageQuantities;
    }

    public void setPrepackageQuantities(List<ProductPrepackageQuantity> prepackageQuantities) {
        this.prepackageQuantities = prepackageQuantities;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<BatchQuantityLogs> getBatchQuantities() {
        return batchQuantities;
    }

    public void setBatchQuantities(List<BatchQuantityLogs> batchQuantities) {
        this.batchQuantities = batchQuantities;
    }

    public List<PrepackageQuantityLog> getPrepackageQuantityLogs() {
        return prepackageQuantityLogs;
    }

    public void setPrepackageQuantityLogs(List<PrepackageQuantityLog> prepackageQuantityLogs) {
        this.prepackageQuantityLogs = prepackageQuantityLogs;
    }
}
