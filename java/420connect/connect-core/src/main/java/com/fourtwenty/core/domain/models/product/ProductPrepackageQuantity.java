package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 3/31/17.
 */
@CollectionName(name = "prepackage_quantities", uniqueIndexes = {"{companyId:1,shopId:1,inventoryId:1,prepackageItemId:1}"}, indexes = {"{companyId:1,shopId:1,productId:1}", "{companyId:1,shopId:1,prepackageItemId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPrepackageQuantity extends ShopBaseModel {
    private String inventoryId;
    private String productId;
    private String prepackageId;
    private String prepackageItemId;
    private int quantity;


    public String getPrepackageId() {
        return prepackageId;
    }

    public void setPrepackageId(String prepackageId) {
        this.prepackageId = prepackageId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
