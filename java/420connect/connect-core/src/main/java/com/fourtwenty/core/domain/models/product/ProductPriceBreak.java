package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 4/16/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPriceBreak extends CompanyBaseModel {
    public enum PriceBreakType {
        None(0, 0, "None", "None", "", ""),
        HalfGramUnit(1, 1, ".5g", "1 each", "Unit Price", "Unit Price"),
        OneGramUnit(2, 1, "1g", "1 each", "Unit Price", "2 Units Price"),
        TwoGramUnits(4, 2, "2g", "2 each", "2 Units Price", "3 Units Price"),
        ThreeGramUnits(6, 3, "3g", "3 each", "3 Units Price", "4 Units Price"),
        FourGramUnits(8, 4, "4g", "4 each", "4 Units Price", "5 Units Price"),
        FiveGramUnits(10, 5, "5g", "5 each", "5 Units Price", "6 Units Price"),
        SixGramUnits(12, 6, "6g", "6 each", "6 Units Price", "7 Units Price"),
        SevenGramUnits(14, 7, "7g", "7 each", "7 Units Price", "8 Units Price"),
        EightGramUnits(16, 8, "8g", "8 each", "8 Units Price", "9 Units Price");

        PriceBreakType(int halfGram, int fullGram, String gramName, String eachName, String unitImportHeader, String halfUnitImportHeader) {
            this.halfGramValue = halfGram;
            this.fullGramValue = fullGram;
            this.gramName = gramName;
            this.eachName = eachName;
            this.unitImportHeader = unitImportHeader;
            this.halfUnitImportHeader = halfUnitImportHeader;
        }

        public int halfGramValue;
        public int fullGramValue;
        public String gramName;
        public String eachName;
        public String unitImportHeader;
        public String halfUnitImportHeader;
    }

    private PriceBreakType priceBreakType = PriceBreakType.None;
    private String name;
    private String displayName;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal price;
    private int quantity = 1; // In Units. If product type == half gram, then break = HalfGram with quantity = 1
    private boolean active = true; // Default is true
    private int priority = 0;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal salePrice;

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public PriceBreakType getPriceBreakType() {
        return priceBreakType;
    }

    public void setPriceBreakType(PriceBreakType priceBreakType) {
        this.priceBreakType = priceBreakType;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAssignedPrice(){
        double assignedPrice = 0;
        if (this.salePrice !=null){
            assignedPrice = this.salePrice.doubleValue();
        }else if (this.price!=null){
            assignedPrice = this.price.doubleValue();
        }
        return new BigDecimal(assignedPrice);
    }
}
