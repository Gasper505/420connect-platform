package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import java.math.BigDecimal;

/**
 * Created by mdo on 10/14/15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPriceRange {
    private String id;
    private String weightToleranceId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal price = new BigDecimal(0);
    private int priority;
    private ProductWeightTolerance weightTolerance;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal salePrice;

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public ProductWeightTolerance getWeightTolerance() {
        return weightTolerance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setWeightTolerance(ProductWeightTolerance weightTolerance) {
        this.weightTolerance = weightTolerance;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getWeightToleranceId() {
        return weightToleranceId;
    }

    public void setWeightToleranceId(String weightToleranceId) {
        this.weightToleranceId = weightToleranceId;
    }

    public BigDecimal getAssignedPrice(){
        double assignedPrice = 0;
        if (this.salePrice !=null){
            assignedPrice = this.salePrice.doubleValue();
        }else if (this.price!=null){
            assignedPrice = this.price.doubleValue();
        }
        return new BigDecimal(assignedPrice);
    }
}
