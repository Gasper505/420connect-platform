package com.fourtwenty.core.domain.models.product;

/**
 * Created by mdo on 3/13/16.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;


@CollectionName(name = "product_quantities", uniqueIndexes = {"{companyId:1,shopId:1,inventoryId:1,productId:1}"}, indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductQuantity extends ShopBaseModel {
    private String inventoryId;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal quantity = new BigDecimal(0);

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
