package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductPrepackageQuantityResult;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductQuantityInfo {
    private String productId;
    private List<ProductQuantity> currentQuantity = new ArrayList<>();
    private List<ProductQuantity> transferQuantity = new ArrayList<>();
    private List<HoldQuantityInfo> holdQuantity = new ArrayList<>();
    private List<ProductPrepackageQuantityResult> productPrepackageQuantityResult = new ArrayList<>();
    private List<ProductQuantity> hiddenQuantity = new ArrayList<>();
    private List<ProductPrepackageQuantityResult> productPrepackageTransferResult = new ArrayList<>();
    private List<ProductPrepackageQuantityResult> productPrepackageHoldResult = new ArrayList<>();


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<ProductQuantity> getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(List<ProductQuantity> currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public List<ProductQuantity> getTransferQuantity() {
        return transferQuantity;
    }

    public void setTransferQuantity(List<ProductQuantity> transferQuantity) {
        this.transferQuantity = transferQuantity;
    }

    public List<HoldQuantityInfo> getHoldQuantity() {
        return holdQuantity;
    }

    public void setHoldQuantity(List<HoldQuantityInfo> holdQuantity) {
        this.holdQuantity = holdQuantity;
    }

    public List<ProductPrepackageQuantityResult> getProductPrepackageQuantityResult() {
        return productPrepackageQuantityResult;
    }

    public void setProductPrepackageQuantityResult(List<ProductPrepackageQuantityResult> productPrepackageQuantityResult) {
        this.productPrepackageQuantityResult = productPrepackageQuantityResult;
    }

    public List<ProductQuantity> getHiddenQuantity() {
        return hiddenQuantity;
    }

    public void setHiddenQuantity(List<ProductQuantity> hiddenQuantity) {
        this.hiddenQuantity = hiddenQuantity;
    }

    public List<ProductPrepackageQuantityResult> getProductPrepackageTransferResult() {
        return productPrepackageTransferResult;
    }

    public void setProductPrepackageTransferResult(List<ProductPrepackageQuantityResult> productPrepackageTransferResult) {
        this.productPrepackageTransferResult = productPrepackageTransferResult;
    }

    public List<ProductPrepackageQuantityResult> getProductPrepackageHoldResult() {
        return productPrepackageHoldResult;
    }

    public void setProductPrepackageHoldResult(List<ProductPrepackageQuantityResult> productPrepackageHoldResult) {
        this.productPrepackageHoldResult = productPrepackageHoldResult;
    }
}
