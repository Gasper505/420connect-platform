package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.Product.WeightPerUnit;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "product_versions", premSyncDown = false, indexes = {"{companyId:1,shopId:1,delete:1}","{companyId:1,shopId:1,delete:1,productId:1}","{created:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel()
public class ProductVersion extends ShopBaseModel implements OnPremSyncable{
    private String productId;
    private String categoryId;
    @ApiModelProperty(value = "product name", required = true)
    @NotEmpty
    private String name;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0f);
    private WeightPerUnit weightPerUnit = WeightPerUnit.EACH;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitValue = new BigDecimal(1f); // Default is 1. Unit Value indicate the value of a single product starting point.
    private boolean active = true; // default is true
    private List<MedicalCondition> medicalConditions = new ArrayList<>();
    private List<ProductPriceRange> priceRanges = new ArrayList<>();
    private List<ProductPriceBreak> priceBreaks = new ArrayList<>();
    private List<ProductQuantity> quantities = new ArrayList<>();
    private boolean instock = false;
    private String brandId;
    private ProductCategory category;
    private List<Note> notes = new ArrayList<>();
    private boolean enableMixMatch = false; // MixMatch within same categoryId and same price points
    private boolean enableWeedmap = false;
    private boolean showInWidget = true;
    private TaxInfo.TaxType taxType = TaxInfo.TaxType.Inherit;
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed
    private TaxInfo customTaxInfo;
    private boolean discountable = true;
    private boolean byGram = true;
    private boolean byPrepackage = false;
    private boolean enableExciseTax = Boolean.FALSE;
    private boolean priceIncludesExcise = Boolean.FALSE;
    private boolean priceIncludesALExcise = Boolean.TRUE;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal salesPrice;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public WeightPerUnit getWeightPerUnit() {
        return weightPerUnit;
    }

    public void setWeightPerUnit(WeightPerUnit weightPerUnit) {
        this.weightPerUnit = weightPerUnit;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<MedicalCondition> getMedicalConditions() {
        return medicalConditions;
    }

    public void setMedicalConditions(List<MedicalCondition> medicalConditions) {
        this.medicalConditions = medicalConditions;
    }

    public List<ProductPriceRange> getPriceRanges() {
        return priceRanges;
    }

    public void setPriceRanges(List<ProductPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
    }

    public List<ProductPriceBreak> getPriceBreaks() {
        return priceBreaks;
    }

    public void setPriceBreaks(List<ProductPriceBreak> priceBreaks) {
        this.priceBreaks = priceBreaks;
    }

    public List<ProductQuantity> getQuantities() {
        return quantities;
    }

    public void setQuantities(List<ProductQuantity> quantities) {
        this.quantities = quantities;
    }

    public boolean isInstock() {
        return instock;
    }

    public void setInstock(boolean instock) {
        this.instock = instock;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public boolean isEnableWeedmap() {
        return enableWeedmap;
    }

    public void setEnableWeedmap(boolean enableWeedmap) {
        this.enableWeedmap = enableWeedmap;
    }

    public boolean isShowInWidget() {
        return showInWidget;
    }

    public void setShowInWidget(boolean showInWidget) {
        this.showInWidget = showInWidget;
    }

    public TaxInfo.TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxInfo.TaxType taxType) {
        this.taxType = taxType;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public TaxInfo getCustomTaxInfo() {
        return customTaxInfo;
    }

    public void setCustomTaxInfo(TaxInfo customTaxInfo) {
        this.customTaxInfo = customTaxInfo;
    }

    public boolean isDiscountable() {
        return discountable;
    }

    public void setDiscountable(boolean discountable) {
        this.discountable = discountable;
    }

    public boolean isByGram() {
        return byGram;
    }

    public void setByGram(boolean byGram) {
        this.byGram = byGram;
    }

    public boolean isByPrepackage() {
        return byPrepackage;
    }

    public void setByPrepackage(boolean byPrepackage) {
        this.byPrepackage = byPrepackage;
    }

    public boolean isEnableExciseTax() {
        return enableExciseTax;
    }

    public void setEnableExciseTax(boolean enableExciseTax) {
        this.enableExciseTax = enableExciseTax;
    }

    public boolean isPriceIncludesExcise() {
        return priceIncludesExcise;
    }

    public void setPriceIncludesExcise(boolean priceIncludesExcise) {
        this.priceIncludesExcise = priceIncludesExcise;
    }

    public boolean isPriceIncludesALExcise() {
        return priceIncludesALExcise;
    }

    public void setPriceIncludesALExcise(boolean priceIncludesALExcise) {
        this.priceIncludesALExcise = priceIncludesALExcise;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(BigDecimal salesPrice) {
        this.salesPrice = salesPrice;
    }

    public boolean isEnableMixMatch() {
        return enableMixMatch;
    }

    public void setEnableMixMatch(boolean enableMixMatch) {
        this.enableMixMatch = enableMixMatch;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setProduct(Product product) {
        this.productId = product.getId();

        this.setShopId(product.getShopId());
        this.setCompanyId(product.getCompanyId());
        this.priceBreaks = product.getPriceBreaks();
        this.priceRanges = product.getPriceRanges();
        this.categoryId = product.getCategoryId();
        this.name = product.getName();
        this.unitPrice  = product.getUnitPrice();
        this.weightPerUnit = product.getWeightPerUnit();
        this.unitValue  = product.getUnitValue();
        this.active  = product.isActive();
        this.medicalConditions  = product.getMedicalConditions();
        this.quantities  = product.getQuantities();
        this.brandId = product.getBrandId();
        this.category = product.getCategory();
        this.enableMixMatch = product.isEnableMixMatch();
        this.enableWeedmap  = product.isEnableWeedmap();
        this.showInWidget = product.isShowInWidget();
        this.taxType = product.getTaxType();
        this.taxOrder  = product.getTaxOrder();
        this.customTaxInfo = product.getCustomTaxInfo();
        this.discountable  = product.isDiscountable();
        this.byGram = product.isByGram();
        this.byPrepackage  = product.isByPrepackage();
        this.enableExciseTax  = product.isEnableExciseTax();
        this.priceIncludesExcise  = product.isPriceIncludesExcise();
        this.priceIncludesALExcise = product.isPriceIncludesALExcise();
        this.salesPrice = product.getSalesPrice();
    }
}
