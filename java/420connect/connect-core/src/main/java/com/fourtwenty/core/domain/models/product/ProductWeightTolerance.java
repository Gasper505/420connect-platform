package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalWeightSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 10/14/15.
 */

@CollectionName(name = "weight_tolerances", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWeightTolerance extends CompanyBaseModel implements OnPremSyncable {
    public enum WeightKey {
        UNIT("Unit", 1.0),
        HALF_GRAM(".5 Gram", 0.5),
        GRAM("Gram", 1.0),
        TWO_GRAMS("2 Grams", 2.0),
        THREE_GRAMS("3 Grams", 3.0),
        ONE_EIGHTTH("1/8th", 3.54369),
        QUARTER("1/4th", 7.087381),
        HALF("1/2", 14.17476),
        OUNCE("1 Oz", 28.34952),
        CUSTOM("Custom", 1.0);

        WeightKey(String weightName, Double aWeightValue) {
            this.weightName = weightName;
            this.weightValue = new BigDecimal(aWeightValue);
        }

        public String weightName;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        public BigDecimal weightValue;

        public static WeightKey getKey(String weightName) {
            if (weightName == null) {
                return UNIT;
            }

            if (weightName.equalsIgnoreCase(UNIT.weightName)) {
                return UNIT;
            } else if (weightName.equalsIgnoreCase(GRAM.weightName)) {
                return GRAM;
            } else if (weightName.equalsIgnoreCase(ONE_EIGHTTH.weightName)) {
                return ONE_EIGHTTH;
            } else if (weightName.equalsIgnoreCase(QUARTER.weightName)) {
                return QUARTER;
            } else if (weightName.equalsIgnoreCase(HALF.weightName)) {
                return HALF;
            } else if (weightName.equalsIgnoreCase(OUNCE.weightName)) {
                return OUNCE;
            } else if (weightName.equalsIgnoreCase(HALF_GRAM.weightName)) {
                return HALF_GRAM;
            } else if (weightName.equalsIgnoreCase(TWO_GRAMS.weightName)) {
                return TWO_GRAMS;
            } else if (weightName.equalsIgnoreCase(THREE_GRAMS.weightName)) {
                return THREE_GRAMS;
            }

            return UNIT;
        }

    }

    @NotEmpty
    private String name;
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal startWeight = new BigDecimal(0);
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal endWeight = new BigDecimal(0);
    private int priority;
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal unitValue;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalWeightSerializer.class)
    private BigDecimal weightValue = new BigDecimal(0);
    private WeightKey weightKey = WeightKey.UNIT;
    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public WeightKey getWeightKey() {
        return weightKey;
    }

    public void setWeightKey(WeightKey weightKey) {
        this.weightKey = weightKey;
    }

    public BigDecimal getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(BigDecimal weightValue) {
        this.weightValue = weightValue;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public BigDecimal getEndWeight() {
        return endWeight;
    }

    public void setEndWeight(BigDecimal endWeight) {
        this.endWeight = endWeight;
    }

    public BigDecimal getStartWeight() {
        return startWeight;
    }

    public void setStartWeight(BigDecimal startWeight) {
        this.startWeight = startWeight;
    }
}
