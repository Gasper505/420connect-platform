package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.Transactionable;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CollectionName(name = "reconciliation_history", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)

public class ReconciliationHistory extends ShopBaseModel implements Transactionable {

    private Long requestNo;
    private String employeeId;
    private String transNo;

    private List<ReconciliationHistoryList> reconciliations = new ArrayList<>();

    private Map<String, Double> batchQuantityInfoMap = new HashMap<>();


    private boolean batchReconcile = Boolean.FALSE;

    public Long getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(Long requestNo) {
        this.requestNo = requestNo;
    }

    public List<ReconciliationHistoryList> getReconciliations() {
        return reconciliations;
    }

    public void setReconciliations(List<ReconciliationHistoryList> reconciliations) {
        this.reconciliations = reconciliations;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public boolean isBatchReconcile() {
        return batchReconcile;
    }

    public void setBatchReconcile(boolean batchReconcile) {
        this.batchReconcile = batchReconcile;
    }

    public Map<String, Double> getBatchQuantityInfoMap() {
        return batchQuantityInfoMap;
    }

    public void setBatchQuantityInfoMap(Map<String, Double> batchQuantityInfoMap) {
        this.batchQuantityInfoMap = batchQuantityInfoMap;
    }
}
