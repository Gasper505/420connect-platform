package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import java.math.BigDecimal;

/**
 * Created by mdo on 3/31/17.
 */
@CollectionName(name = "test_models")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestModel extends CompanyBaseModel {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
