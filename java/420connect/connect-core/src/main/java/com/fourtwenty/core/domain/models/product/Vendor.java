package com.fourtwenty.core.domain.models.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.quickbook.QBDataMapping;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;


@CollectionName(name = "vendors", indexes = {"{companyId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vendor extends CompanyBaseModel implements Importable , InternalAllowable,OnPremSyncable {

    public enum ArmsLengthType {
        ARMS_LENGTH,
        NON_ARMS_LENGTH
    }

    public enum CompanyType {
        DISTRIBUTOR,
        CULTIVATOR,
        RETAILER,
        MANUFACTURER,
        TRANSPORTER,
        OTHER,
        TESTING_FACILITY
    }

    public enum LicenceType {
        RECREATIONAL,
        MEDICINAL,
        BOTH
    }

    public enum VendorType {
        CUSTOMER,
        VENDOR,
        BOTH
    }

    private String importId;
    private Boolean active = true;
    @NotEmpty
    private String name;
    private String phone;
    private String email;
    private String fax;
    private Address address;
    private String description;
    private String website;
    private String firstName;
    private String lastName;
    private List<Note> notes = new ArrayList<>();
    private String licenseNumber;
    private List<CompanyAsset> assets = new ArrayList<>();
    private Boolean backOrderEnabled = false;
    private long licenseExpirationDate;
    private ArmsLengthType armsLengthType = ArmsLengthType.ARMS_LENGTH;
    private LinkedHashSet<String> brands = new LinkedHashSet<>();

    List<HashMap<String, String>> qbVendorRef = new ArrayList<HashMap<String, String>>();

    // Quick book desktop mapping
    private List<QBDataMapping> qbMapping;
    private List<QBDataMapping> qbCustomerMapping;

    private long maxAttachment = 15;
    private Vendor.CompanyType companyType;
    private List<Address> additionalAddressList = new ArrayList<>();
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal credits = new BigDecimal(0);
    private String mobileNumber;
    private Vendor.LicenceType licenceType;
    private boolean relatedEntity;
    private VendorType vendorType = VendorType.VENDOR;
    private String dbaName; // DBA : doing business name as
    private List<CompanyLicense> companyLicenses = new ArrayList<>();
    private String createdBy;
    private String accountOwnerId;
    private String defaultPaymentTerm;
    private String salesPerson;
    private String contactPerson;
    private String defaultContactId;

    private String externalId;
    private boolean toDefault;
    private String connectedShop;

    @JsonProperty("vendorKey")
    public String getVendorKey() {
        String key = String.format("%s_%s", companyId, this.getName().toLowerCase());

        String encoded = Base64.getEncoder().encodeToString(key.getBytes(Charset.forName("UTF8")));
        return encoded;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Boolean getBackOrderEnabled() {
        return backOrderEnabled;
    }

    public void setBackOrderEnabled(Boolean backOrderEnabled) {
        this.backOrderEnabled = backOrderEnabled;
    }

    public long getLicenseExpirationDate() {
        return licenseExpirationDate;
    }

    public void setLicenseExpirationDate(long licenseExpirationDate) {
        this.licenseExpirationDate = licenseExpirationDate;
    }

    public ArmsLengthType getArmsLengthType() {
        return armsLengthType;
    }

    public void setArmsLengthType(ArmsLengthType armsLengthType) {
        this.armsLengthType = armsLengthType;
    }

    public List<HashMap<String, String>> getQbVendorRef() {
        return qbVendorRef;
    }

    public void setQbVendorRef(List<HashMap<String, String>> qbVendorRef) {
        this.qbVendorRef = qbVendorRef;
    }

    public LinkedHashSet<String> getBrands() {
        return brands;
    }

    public void setBrands(LinkedHashSet<String> brands) {
        this.brands = brands;
    }

    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    public List<Address> getAdditionalAddressList() {
        return additionalAddressList;
    }

    public void setAdditionalAddressList(List<Address> additionalAddressList) {
        this.additionalAddressList = additionalAddressList;
    }

    public BigDecimal getCredits() {
        return credits;
    }

    public void setCredits(BigDecimal credits) {
        this.credits = credits;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public LicenceType getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(LicenceType licenceType) {
        this.licenceType = licenceType;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public VendorType getVendorType() {
        return vendorType;
    }

    public void setVendorType(VendorType vendorType) {
        this.vendorType = vendorType;
    }

    public String getDbaName() {
        return dbaName;
    }

    public void setDbaName(String dbaName) {
        this.dbaName = dbaName;
    }

    public List<CompanyLicense> getCompanyLicenses() {
        return companyLicenses;
    }

    public void setCompanyLicenses(List<CompanyLicense> companyLicenses) {
        this.companyLicenses = companyLicenses;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getAccountOwnerId() {
        return accountOwnerId;
    }

    public void setAccountOwnerId(String accountOwnerId) {
        this.accountOwnerId = accountOwnerId;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    /**
     * Filter from company licenses for given number or default license
     * and still if license is blank then first item from list.
     * Also if company license list is blank then Retailer
     *
     * @param licenseNumber : license number
     * @return returns CompanyLicense
     */
    public CompanyLicense getCompanyLicense(String licenseNumber) {
        CompanyLicense license = null;
        if (!CollectionUtils.isEmpty(getCompanyLicenses())) {
            for (CompanyLicense companyLicense : getCompanyLicenses()) {
                if (companyLicense.isToDefault()) {
                    license = companyLicense; // set default company license
                }
                if (companyLicense.getId().equals(licenseNumber)) {
                    license = companyLicense;
                    break;
                }
            }
            license = (license == null) ? getCompanyLicenses().get(0) : license;
        } else {
            //Default should be retailer
            license = new CompanyLicense();
            license.setCompanyType(Vendor.CompanyType.RETAILER);
        }

        return license;
    }

    public List<QBDataMapping> getQbMapping() {
        return qbMapping;
    }

    public void setQbMapping(List<QBDataMapping> qbMapping) {
        this.qbMapping = qbMapping;
    }

    public List<QBDataMapping> getQbCustomerMapping() {
        return qbCustomerMapping;
    }

    public void setQbCustomerMapping(List<QBDataMapping> qbCustomerMapping) {
        this.qbCustomerMapping = qbCustomerMapping;
    }

    public boolean isToDefault() {
        return toDefault;
    }

    public void setToDefault(boolean toDefault) {
        this.toDefault = toDefault;
    }

    public String getDefaultPaymentTerm() {
        return defaultPaymentTerm;
    }

    public void setDefaultPaymentTerm(String defaultPaymentTerm) {
        this.defaultPaymentTerm = defaultPaymentTerm;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getDefaultContactId() {
        return defaultContactId;
    }

    public void setDefaultContactId(String defaultContactId) {
        this.defaultContactId = defaultContactId;
    }

    public String getConnectedShop() {
        return connectedShop;
    }

    public void setConnectedShop(String connectedShop) {
        this.connectedShop = connectedShop;
    }
}