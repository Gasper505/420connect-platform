package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdjustmentInfo {

    private String adjustmentId;
    private boolean negative = Boolean.FALSE;
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal amount = BigDecimal.ZERO;

    public String getAdjustmentId() {
        return adjustmentId;
    }

    public void setAdjustmentId(String adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
