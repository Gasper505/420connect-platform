package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by decipher on 4/10/17 5:33 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "purchase_order_activity", indexes = {"{purchaseOrderActivityId:1,companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class POActivity extends ShopBaseModel {
    public enum ActivityType {
        NORMAL_LOG,
        COMMENT
    }

    private String purchaseOrderId;

    private String employeeId;

    private String log;

    private ActivityType activityType;

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }
}
