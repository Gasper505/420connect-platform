package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.serializers.ProductBatchTrackTraceSystemDeserializer;
import com.fourtwenty.core.domain.serializers.TruncateSixDigitSerialzer;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import com.fourtwenty.core.services.testsample.request.POBatchAddRequest;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by decipher on 3/10/17 12:30 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "poproductrequest", indexes = {"{poProductRequestId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class POProductRequest extends ShopBaseModel {

    public enum RequestStatus {
        PENDING,
        DECLINED,
        ACCEPTED
    }


    private String productId;
    private String productName;
    private RequestStatus requestStatus = RequestStatus.PENDING;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal requestQuantity = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal receivedQuantity = new BigDecimal(0);

    @JsonDeserialize(using = ProductBatchTrackTraceSystemDeserializer.class)
    private ProductBatch.TrackTraceSystem trackTraceSystem;
    private List<TrackingPackages> trackingPackagesList = new ArrayList<>();
    private String notes;

    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0);

    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal totalCost = new BigDecimal(0);

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal exciseTax = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalExciseTax = new BigDecimal(0);

    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal totalCultivationTax = new BigDecimal(0);

    private String declineReason;
    //<batchId, quantity>
    private Map<String, BigDecimal> batchQuantityMap = new HashMap<>();

    private ProductBatch.BatchStatus receiveBatchStatus = ProductBatch.BatchStatus.READY_FOR_SALE;

    private List<POBatchAddRequest> batchAddDetails;

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal  discount = new BigDecimal(0);

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal finalTotalCost = new BigDecimal(0);

    private String txnLineID; //QB txn line id for PO

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public BigDecimal getRequestQuantity() {
        return requestQuantity;
    }

    public void setRequestQuantity(BigDecimal requestQuantity) {
        this.requestQuantity = requestQuantity;
    }

    public BigDecimal getReceivedQuantity() {
        return receivedQuantity;
    }

    public void setReceivedQuantity(BigDecimal receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public ProductBatch.TrackTraceSystem getTrackTraceSystem() {
        return trackTraceSystem;
    }

    public void setTrackTraceSystem(ProductBatch.TrackTraceSystem trackTraceSystem) {
        this.trackTraceSystem = trackTraceSystem;
    }

    public List<TrackingPackages> getTrackingPackagesList() {
        return trackingPackagesList;
    }

    public void setTrackingPackagesList(List<TrackingPackages> trackingPackagesList) {
        this.trackingPackagesList = trackingPackagesList;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public BigDecimal getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(BigDecimal exciseTax) {
        this.exciseTax = exciseTax;
    }

    public BigDecimal getTotalExciseTax() {
        return totalExciseTax;
    }

    public void setTotalExciseTax(BigDecimal totalExciseTax) {
        this.totalExciseTax = totalExciseTax;
    }

    public Map<String, BigDecimal> getBatchQuantityMap() {
        return batchQuantityMap;
    }

    public void setBatchQuantityMap(Map<String, BigDecimal> batchQuantityMap) {
        this.batchQuantityMap = batchQuantityMap;
    }

    public BigDecimal getTotalCultivationTax() {
        return totalCultivationTax;
    }

    public void setTotalCultivationTax(BigDecimal totalCultivationTax) {
        this.totalCultivationTax = totalCultivationTax;
    }

    public ProductBatch.BatchStatus getReceiveBatchStatus() {
        return receiveBatchStatus;
    }

    public void setReceiveBatchStatus(ProductBatch.BatchStatus receiveBatchStatus) {
        this.receiveBatchStatus = receiveBatchStatus;
    }

    public List<POBatchAddRequest> getBatchAddDetails() {
        return batchAddDetails;
    }

    public void setBatchAddDetails(List<POBatchAddRequest> batchAddDetails) {
        this.batchAddDetails = batchAddDetails;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
    public BigDecimal getFinalTotalCost() {
        return finalTotalCost;
    }

    public void setFinalTotalCost(BigDecimal finalTotalCost) {
        this.finalTotalCost = finalTotalCost;
    }

    public String getTxnLineID() {
        return txnLineID;
    }

    public void setTxnLineID(String txnLineID) {
        this.txnLineID = txnLineID;
    }
}
