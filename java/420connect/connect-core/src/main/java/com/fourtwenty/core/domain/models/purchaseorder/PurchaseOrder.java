package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.models.transaction.Transactionable;
import com.fourtwenty.core.domain.serializers.TruncateSixDigitSerialzer;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import org.joda.time.DateTime;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by decipher on 3/10/17 12:13 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "purchaseorders", indexes = {"{purchaseOrderId:1,companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseOrder extends ShopBaseModel implements Transactionable {

    public enum PurchaseOrderStatus {
        InProgress,
        ReceivingShipment,
        RequiredApproval,
        Approved,
        WaitingShipment,
        Closed,
        Cancel,
        Decline,
        ReceivedShipment
    }

    public enum POPaymentOptions {
        CASH("Cash"),
        CREDIT("Credit"),
        DEBIT("Debit"),
        COD("Cod"),
        ACH_TRANSFER("Ach_Transfer"),
        CHEQUE("Cheque"),
        OTHER("Other");

        private String paymentType;

        POPaymentOptions(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public static POPaymentOptions toPOPaymentOptions(String option) {
            if (option.trim().equalsIgnoreCase(CASH.getPaymentType())) {
                return CASH;
            } else if (option.trim().equalsIgnoreCase(CREDIT.getPaymentType())) {
                return CREDIT;
            } else if (option.trim().equalsIgnoreCase(DEBIT.getPaymentType())) {
                return DEBIT;
            } else if (option.trim().equalsIgnoreCase(ACH_TRANSFER.getPaymentType())) {
                return ACH_TRANSFER;
            } else if (option.trim().equalsIgnoreCase(CHEQUE.getPaymentType())) {
                return CHEQUE;
            } else if (option.trim().equalsIgnoreCase(OTHER.getPaymentType())) {
                return OTHER;
            } else {
                return COD;
            }
        }
    }

    public enum POType {
        Normal,
        BackOrder
    }

    public enum POPaymentTerms {
        NET_30,
        COD,
        NET_45,
        NET_60,
        NET_15,
        NET_7,
        CUSTOM_DATE
    }

    public enum CustomerType {
        VENDOR,
        CUSTOMER_COMPANY
    }

    public enum FlowerSourceType {
        CULTIVATOR_DIRECT,
        DISTRIBUTOR
    }

    public enum PurchaseOrderSort {
        DATE,
        CREATED_DATE,
        DELIVERY_DATE
    }

    private String poNumber;
    private String parentPOId;
    private String parentPONumber;
    private Long approvedDate;
    private String approvedBy;
    private CompanyAsset approvedSignature;
    private POPaymentTerms poPaymentTerms;
    private PurchaseOrderStatus purchaseOrderStatus = PurchaseOrderStatus.InProgress;
    private POPaymentOptions poPaymentOptions;
    private List<CompanyAsset> companyAssetList = new ArrayList<>();
    private List<POProductRequest> poProductRequestList = new ArrayList<>();
    private String vendorId;
    private Long receivedDate;
    private String deliveredBy;
    private String receivedByEmployeeId;
    private String createdByEmployeeId;
    private String completedByEmployeeId;
    private POType poType = POType.Normal;
    private Long completedDate;
    private String notes;
    private String declineReason;
    private Long declineDate;
    //Cost without tax
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal totalCost;
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal amountPaid;
    private Long submitForApprovalDate;
    private boolean archive = Boolean.FALSE;
    private Long archiveDate;
    private long customTermDate;
    private CompanyAsset managerReceiveSignature;
    private String reference;
    private String qbPurchaseOrderRef;
    //Total with taxes
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    private BigDecimal grandTotal = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalTax = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPreCalcTax = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCalcTax = new BigDecimal(0);
    private TaxResult taxResult;
    private Vendor.ArmsLengthType transactionType;
    private CustomerType customerType = CustomerType.VENDOR;

    private String deliveryAddress;
    private Long deliveryTime;
    private long deliveryDate;
    private String termsAndCondition;

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryCharge = new BigDecimal(0);
    private boolean enableDeliveryCharge = false;
    private CompanyAsset poQrCodeAsset;
    private String poQrCodeUrl;
    private FlowerSourceType flowerSourceType;
    private String shipmentBillId;

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal fees = new BigDecimal(0);

    private long purchaseOrderDate;
    private long dueDate;
    private String licenseId;
    private boolean manualClose;
    private String qbDesktopPurchaseOrderRef;
    private String editSequence;
    private boolean qbDesktopBillRef;
    private boolean qbErrored;
    private long errorTime;

    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalDiscount = new BigDecimal(0);

    private List<AdjustmentInfo> adjustmentInfoList = new ArrayList<>();

    private PurchaseOrderStatus purchaseOrderPreviousStatus;

    private List<Shipment> assignedShipments = new ArrayList<>();

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getParentPOId() {
        return parentPOId;
    }

    public void setParentPOId(String parentPOId) {
        this.parentPOId = parentPOId;
    }

    public Long getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Long approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public CompanyAsset getApprovedSignature() {
        return approvedSignature;
    }

    public void setApprovedSignature(CompanyAsset approvedSignature) {
        this.approvedSignature = approvedSignature;
    }

    public POPaymentTerms getPoPaymentTerms() {
        return poPaymentTerms;
    }

    public void setPoPaymentTerms(POPaymentTerms poPaymentTerms) {
        this.poPaymentTerms = poPaymentTerms;
    }

    public PurchaseOrderStatus getPurchaseOrderStatus() {
        return purchaseOrderStatus;
    }

    public void setPurchaseOrderStatus(PurchaseOrderStatus purchaseOrderStatus) {
        this.purchaseOrderStatus = purchaseOrderStatus;
    }

    public POPaymentOptions getPoPaymentOptions() {
        return poPaymentOptions;
    }

    public void setPoPaymentOptions(POPaymentOptions poPaymentOptions) {
        this.poPaymentOptions = poPaymentOptions;
    }

    public List<CompanyAsset> getCompanyAssetList() {
        return companyAssetList;
    }

    public void setCompanyAssetList(List<CompanyAsset> companyAssetList) {
        this.companyAssetList = companyAssetList;
    }

    public List<POProductRequest> getPoProductRequestList() {
        return poProductRequestList;
    }

    public void setPoProductRequestList(List<POProductRequest> poProductRequestList) {
        this.poProductRequestList = poProductRequestList;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public Long getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Long receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public String getReceivedByEmployeeId() {
        return receivedByEmployeeId;
    }

    public void setReceivedByEmployeeId(String receivedByEmployeeId) {
        this.receivedByEmployeeId = receivedByEmployeeId;
    }

    public String getCreatedByEmployeeId() {
        return createdByEmployeeId;
    }

    public void setCreatedByEmployeeId(String createdByEmployeeId) {
        this.createdByEmployeeId = createdByEmployeeId;
    }

    public String getCompletedByEmployeeId() {
        return completedByEmployeeId;
    }

    public void setCompletedByEmployeeId(String completedByEmployeeId) {
        this.completedByEmployeeId = completedByEmployeeId;
    }

    public POType getPoType() {
        return poType;
    }

    public void setPoType(POType poType) {
        this.poType = poType;
    }

    public Long getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Long completedDate) {
        this.completedDate = completedDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public Long getDeclineDate() {
        return declineDate;
    }

    public void setDeclineDate(Long declineDate) {
        this.declineDate = declineDate;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Long getSubmitForApprovalDate() {
        return submitForApprovalDate;
    }

    public void setSubmitForApprovalDate(Long submitForApprovalDate) {
        this.submitForApprovalDate = submitForApprovalDate;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public Long getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(Long archiveDate) {
        this.archiveDate = archiveDate;
    }

    public String getParentPONumber() {
        return parentPONumber;
    }

    public void setParentPONumber(String parentPONumber) {
        this.parentPONumber = parentPONumber;
    }

    public long getCustomTermDate() {
        return customTermDate;
    }

    public void setCustomTermDate(long customTermDate) {
        this.customTermDate = customTermDate;
    }

    public CompanyAsset getManagerReceiveSignature() {
        return managerReceiveSignature;
    }

    public void setManagerReceiveSignature(CompanyAsset managerReceiveSignature) {
        this.managerReceiveSignature = managerReceiveSignature;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public BigDecimal getTotalPreCalcTax() {
        return totalPreCalcTax;
    }

    public void setTotalPreCalcTax(BigDecimal totalPreCalcTax) {
        this.totalPreCalcTax = totalPreCalcTax;
    }

    public BigDecimal getTotalCalcTax() {
        return totalCalcTax;
    }

    public void setTotalCalcTax(BigDecimal totalCalcTax) {
        this.totalCalcTax = totalCalcTax;
    }

    public TaxResult getTaxResult() {
        return taxResult;
    }

    public void setTaxResult(TaxResult taxResult) {
        this.taxResult = taxResult;
    }

    public Vendor.ArmsLengthType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Vendor.ArmsLengthType transactionType) {
        this.transactionType = transactionType;
    }

    public String getQbPurchaseOrderRef() {
        return qbPurchaseOrderRef;
    }

    public void setQbPurchaseOrderRef(String qbPurchaseOrderRef) {
        this.qbPurchaseOrderRef = qbPurchaseOrderRef;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

    public BigDecimal getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(BigDecimal deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public boolean isEnableDeliveryCharge() {
        return enableDeliveryCharge;
    }

    public void setEnableDeliveryCharge(boolean enableDeliveryCharge) {
        this.enableDeliveryCharge = enableDeliveryCharge;
    }

    public String getPoQrCodeUrl() {
        return poQrCodeUrl;
    }

    public void setPoQrCodeUrl(String poQrCodeUrl) {
        this.poQrCodeUrl = poQrCodeUrl;
    }

    public CompanyAsset getPoQrCodeAsset() {
        return poQrCodeAsset;
    }

    public void setPoQrCodeAsset(CompanyAsset poQrCodeAsset) {
        this.poQrCodeAsset = poQrCodeAsset;
    }

    public String getShipmentBillId() {
        return shipmentBillId;
    }

    public void setShipmentBillId(String shipmentBillId) {
        this.shipmentBillId = shipmentBillId;
    }

    public FlowerSourceType getFlowerSourceType() {
        return flowerSourceType;
    }

    public void setFlowerSourceType(FlowerSourceType flowerSourceType) {
        this.flowerSourceType = flowerSourceType;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getFees() {
        return fees;
    }

    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    public long getPurchaseOrderDate() {
        return purchaseOrderDate;
    }

    public void setPurchaseOrderDate(long purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }


    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public boolean isManualClose() {
        return manualClose;
    }

    public void setManualClose(boolean manualClose) {
        this.manualClose = manualClose;
    }

    public String getQbDesktopPurchaseOrderRef() {
        return qbDesktopPurchaseOrderRef;
    }

    public void setQbDesktopPurchaseOrderRef(String qbDesktopPurchaseOrderRef) {
        this.qbDesktopPurchaseOrderRef = qbDesktopPurchaseOrderRef;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public boolean isQbDesktopBillRef() {
        return qbDesktopBillRef;
    }

    public void setQbDesktopBillRef(boolean qbDesktopBillRef) {
        this.qbDesktopBillRef = qbDesktopBillRef;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }

    public List<AdjustmentInfo> getAdjustmentInfoList() {
        return adjustmentInfoList;
    }

    public void setAdjustmentInfoList(List<AdjustmentInfo> adjustmentInfoList) {
        this.adjustmentInfoList = adjustmentInfoList;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    @JsonIgnore
    public long getPODueDate() {
        long dueDate = getDueDate();
        DateTime poDate = new DateTime(purchaseOrderDate == 0 ? created : purchaseOrderDate);

        switch (poPaymentTerms) {
            case NET_7:
                dueDate = poDate.plusDays(7).getMillis();
                break;
            case NET_15:
                dueDate = poDate.plusDays(15).getMillis();
                break;
            case NET_30:
                dueDate = poDate.plusDays(30).getMillis();
                break;
            case NET_45:
                dueDate = poDate.plusDays(45).getMillis();
                break;
            case NET_60:
                dueDate = poDate.plusDays(60).getMillis();
                break;
            case CUSTOM_DATE:
                dueDate = customTermDate;
                break;
            case COD:
            default:
                dueDate = (dueDate == 0) ? poDate.getMillis() : created;
                break;
        }

        return dueDate;
    }

    public PurchaseOrderStatus getPurchaseOrderPreviousStatus() {
        return purchaseOrderPreviousStatus;
    }

    public void setPurchaseOrderPreviousStatus(PurchaseOrderStatus purchaseOrderPreviousStatus) {
        this.purchaseOrderPreviousStatus = purchaseOrderPreviousStatus;
    }

    @JsonIgnore
    public  boolean isEditEnable(){
        if ((this.getPurchaseOrderStatus() == PurchaseOrderStatus.ReceivedShipment) || (this.getPurchaseOrderStatus() == PurchaseOrderStatus.Closed) || (this.getPurchaseOrderStatus() == PurchaseOrderStatus.Decline)) {
        return false;
        }
        return true;
     }

    public List<Shipment> getAssignedShipments() {
        return assignedShipments;
    }

    public void sendAssignedShipments(List<Shipment> assignedShipments) {
        this.assignedShipments = assignedShipments;
    }

}