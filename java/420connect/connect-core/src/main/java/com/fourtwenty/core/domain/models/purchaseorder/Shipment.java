package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name="shipments", indexes = {"{shipmentId:1,companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shipment extends ShopBaseModel {

    public enum ShipmentStatus {
        Incoming,
        Accepted,
        Rejected
    }

    public enum ShipmentSort {
        DATE,
        CREATED_DATE
    }

    private String shippingManifestId;
    private String shippingManifestNo;
    private List<ProductMetrcInfo> productMetrcInfo = new ArrayList<>();
    private ShipperInformation shipperInformation;
    private long deliveryDate;
    private long deliveryTime;
    private Long shippedDateTime;
    private Long estimatedArrival;
    private List<Note> notes;
    private DistributorInformation distributorInformation;
    private ShipmentStatus shipmentStatus = ShipmentStatus.Incoming;
    private long acceptedDate;
    private String acceptedBy;
    private CompanyAsset acceptedSignature;
    private boolean assigned = false;


    public String getShippingManifestId() {
        return shippingManifestId;
    }

    public void setShippingManifestId(String shippingManifestId) {
        this.shippingManifestId = shippingManifestId;
    }

    public List<ProductMetrcInfo> getProductMetrcInfo() {
        return productMetrcInfo;
    }

    public void setProductMetrcInfo(List<ProductMetrcInfo> productMetrcInfo) {
        this.productMetrcInfo = productMetrcInfo;
    }

    public ShipperInformation getShipperInformation() {
        return shipperInformation;
    }

    public void setShipperInformation(ShipperInformation shipperInformation) {
        this.shipperInformation = shipperInformation;
    }

    public String getShippingManifestNo() {
        return shippingManifestNo;
    }

    public void setShippingManifestNo(String shippingManifestNo) {
        this.shippingManifestNo = shippingManifestNo;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Long getShippedDateTime() {
        return shippedDateTime;
    }

    public void setShippedDateTime(Long shippedDateTime) {
        this.shippedDateTime = shippedDateTime;
    }

    public Long getEstimatedArrival() {
        return estimatedArrival;
    }

    public void setEstimatedArrival(Long estimatedArrival) {
        this.estimatedArrival = estimatedArrival;
    }

    public DistributorInformation getDistributorInformation() {
        return distributorInformation;
    }

    public void setDistributorInformation(DistributorInformation distributorInformation) {
        this.distributorInformation = distributorInformation;
    }

    public ShipmentStatus getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(ShipmentStatus shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public long getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(long acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public String getAcceptedBy() {
        return acceptedBy;
    }

    public void setAcceptedBy(String acceptedBy) {
        this.acceptedBy = acceptedBy;
    }

    public CompanyAsset getAcceptedSignature() {
        return acceptedSignature;
    }

    public void setAcceptedSignature(CompanyAsset acceptedSignature) {
        this.acceptedSignature = acceptedSignature;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

}