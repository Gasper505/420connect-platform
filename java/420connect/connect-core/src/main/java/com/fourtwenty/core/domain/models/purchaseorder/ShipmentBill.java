package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.serializers.TruncateSixDigitSerialzer;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.List;

@CollectionName(name = "shipmentbills", indexes = {"{shipmentBillId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentBill extends ShopBaseModel {
    @NotEmpty
    private String purchaseOrderId;
    private List<ShipmentBillPayment> paymentHistory;
    private PaymentStatus paymentStatus = PaymentStatus.UNPAID;
    private String vendorId;
    private String createdBy;
    private List<CompanyAsset> assets;
    private List<POProductRequest> poProductRequest;
    private Long receivedDate;
    private String deliveredBy;
    private String receivedByEmployeeId;
    private String createdByEmployeeId;
    private String completedByEmployeeId;
    private Long completedDate;

    public enum PaymentStatus {
        PAID,
        UNPAID,
        PARTIAL
    }

    private String shipmentBillNumber;
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal amountPaid;
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal totalCost;
    private boolean archive = Boolean.FALSE;
    private Long archiveDate;
    private String notes;
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal grandTotal;
    private TaxResult taxResult;
    private String customerCompanyId;
    private PurchaseOrder.CustomerType customerType = PurchaseOrder.CustomerType.VENDOR;

    private String termsAndCondition;
    private String reference;
    private String qbBillRef;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal fees = new BigDecimal(0);
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal adjustmentAmount = BigDecimal.ZERO;

    private String qbDesktopBillRef;
    private boolean qbErrored;
    private long errorTime;

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public List<ShipmentBillPayment> getPaymentHistory() {
        return paymentHistory;
    }

    public void setPaymentHistory(List<ShipmentBillPayment> paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public List<POProductRequest> getPoProductRequest() {
        return poProductRequest;
    }

    public void setPoProductRequest(List<POProductRequest> poProductRequest) {
        this.poProductRequest = poProductRequest;
    }

    public Long getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Long receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public String getReceivedByEmployeeId() {
        return receivedByEmployeeId;
    }

    public void setReceivedByEmployeeId(String receivedByEmployeeId) {
        this.receivedByEmployeeId = receivedByEmployeeId;
    }

    public Long getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Long completedDate) {
        this.completedDate = completedDate;
    }

    public String getCreatedByEmployeeId() {
        return createdByEmployeeId;
    }

    public void setCreatedByEmployeeId(String createdByEmployeeId) {
        this.createdByEmployeeId = createdByEmployeeId;
    }

    public String getCompletedByEmployeeId() {
        return completedByEmployeeId;
    }

    public void setCompletedByEmployeeId(String completedByEmployeeId) {
        this.completedByEmployeeId = completedByEmployeeId;
    }

    public String getShipmentBillNumber() {
        return shipmentBillNumber;
    }

    public void setShipmentBillNumber(String shipmentBillNumber) {
        this.shipmentBillNumber = shipmentBillNumber;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public Long getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(Long archiveDate) {
        this.archiveDate = archiveDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public TaxResult getTaxResult() {
        return taxResult;
    }

    public void setTaxResult(TaxResult taxResult) {
        this.taxResult = taxResult;
    }

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public PurchaseOrder.CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(PurchaseOrder.CustomerType customerType) {
        this.customerType = customerType;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }


    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getQbBillRef() {
        return qbBillRef;
    }

    public void setQbBillRef(String qbBillRef) {
        this.qbBillRef = qbBillRef;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getFees() {
        return fees;
    }

    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    public BigDecimal getAdjustmentAmount() {
        return adjustmentAmount;
    }

    public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
        this.adjustmentAmount = adjustmentAmount;
    }

    public String getQbDesktopBillRef() {
        return qbDesktopBillRef;
    }

    public void setQbDesktopBillRef(String qbDesktopBillRef) {
        this.qbDesktopBillRef = qbDesktopBillRef;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }
}
