package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@CollectionName(name = "shipmentbillpayment", indexes = {"{shipmentBillPaymentId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentBillPayment extends ShopBaseModel {

    public enum PaymentType {
        ACH_Transfer,
        CASH,
        CHECK,
        CREDIT,
        DEBIT,
        OTHER
    }

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal amountPaid = new BigDecimal(0);
    private long paidDate;
    private String notes;
    private PaymentType paymentType;
    private String referenceNo;
    private String paymentNo;
    private boolean qbPaymentReceived;
    private boolean qbPaymentReceivedError;
    private long qbPaymentReceivedErrorTime;

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public long getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(long paidDate) {
        this.paidDate = paidDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public boolean isQbPaymentReceived() {
        return qbPaymentReceived;
    }

    public void setQbPaymentReceived(boolean qbPaymentReceived) {
        this.qbPaymentReceived = qbPaymentReceived;
    }

    public boolean isQbPaymentReceivedError() {
        return qbPaymentReceivedError;
    }

    public void setQbPaymentReceivedError(boolean qbPaymentReceivedError) {
        this.qbPaymentReceivedError = qbPaymentReceivedError;
    }

    public long getQbPaymentReceivedErrorTime() {
        return qbPaymentReceivedErrorTime;
    }

    public void setQbPaymentReceivedErrorTime(long qbPaymentReceivedErrorTime) {
        this.qbPaymentReceivedErrorTime = qbPaymentReceivedErrorTime;
    }
}
