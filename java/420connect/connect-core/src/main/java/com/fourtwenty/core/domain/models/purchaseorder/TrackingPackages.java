package com.fourtwenty.core.domain.models.purchaseorder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by decipher on 3/10/17 12:44 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@CollectionName(name = "trackingpackage", indexes = {"{trackingPackagesId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackingPackages extends ShopBaseModel {

    @DecimalMin("0")
    private BigDecimal quantity = new BigDecimal(0);
    private String packageLabel;
    private String batchSku;
    private String referenceNo;
    private Long dateSent = 0L;
    private String testingCompanyId;
    private String trackHarvestBatchId;
    private String trackHarvestBatchDate;
    private long batchExpirationDate;
    private long sellBy;

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getPackageLabel() {
        return packageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        this.packageLabel = packageLabel;
    }

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Long getDateSent() {
        return dateSent;
    }

    public void setDateSent(Long dateSent) {
        this.dateSent = dateSent;
    }

    public String getTestingCompanyId() {
        return testingCompanyId;
    }

    public void setTestingCompanyId(String testingCompanyId) {
        this.testingCompanyId = testingCompanyId;
    }

    public String getTrackHarvestBatchId() {
        return trackHarvestBatchId;
    }

    public void setTrackHarvestBatchId(String trackHarvestBatchId) {
        this.trackHarvestBatchId = trackHarvestBatchId;
    }

    public String getTrackHarvestBatchDate() {
        return trackHarvestBatchDate;
    }

    public void setTrackHarvestBatchDate(String trackHarvestBatchDate) {
        this.trackHarvestBatchDate = trackHarvestBatchDate;
    }


    public long getBatchExpirationDate() {
        return batchExpirationDate;
    }

    public void setBatchExpirationDate(long batchExpirationDate) {
        this.batchExpirationDate = batchExpirationDate;
    }

    public long getSellBy() {
        return sellBy;
    }

    public void setSellBy(long sellBy) {
        this.sellBy = sellBy;
    }
}
