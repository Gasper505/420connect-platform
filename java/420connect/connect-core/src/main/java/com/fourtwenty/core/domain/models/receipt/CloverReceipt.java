package com.fourtwenty.core.domain.models.receipt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;

import java.math.BigDecimal;

@CollectionName(name = "clover_receipts",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloverReceipt extends ShopBaseModel {
    public enum ReceiptType {
        CLOVER,
        MTRAC
    }

    private String cloverTxId;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal amount = new BigDecimal(0);
    private String employeeId;
    private String transactionId;
    private long timestamp;
    private ReceiptType receiptType = ReceiptType.CLOVER;
    private String lastFour;

    @JsonProperty("paymentId")
    private String paymentId;

    public String getLastFour() {
        return lastFour;
    }

    public void setLastFour(String lastFour) {
        this.lastFour = lastFour;
    }

    public ReceiptType getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(ReceiptType receiptType) {
        this.receiptType = receiptType;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getCloverTxId() {
        return cloverTxId;
    }

    public void setCloverTxId(String cloverTxId) {
        this.cloverTxId = cloverTxId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

}
