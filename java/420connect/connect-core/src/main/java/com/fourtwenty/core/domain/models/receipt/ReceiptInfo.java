package com.fourtwenty.core.domain.models.receipt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceiptInfo extends ShopBaseModel {
    public enum ReceiptType {
        SALES,
        FULFILLMENT
    }

    private ReceiptType receiptType = ReceiptType.SALES;
    private boolean enableMemberName = true;
    private boolean enableMemberId = true;
    private boolean enableMemberAddress = false;
    private boolean enableShopAddress = true;
    private boolean enableShopPhoneNo = true;
    private boolean enableIncludeItemInSKU = true;
    private boolean enableExciseTaxAsItem = true;
    private boolean enableMemberLoyaltyPoints = true;
    private boolean enableNotes = false;
    private String freeText;
    private boolean enabledFreeText = false;
    private boolean enabledBottomFreeText = false;
    private String aboveFreeText;
    private boolean enableEmployeeName = false;
    private boolean enableMemberPhoneNo = false;
    private boolean enableBrand = false;
    private boolean enableItemDiscount = false;
    private boolean enableItemDiscountNotes = false;

    public boolean isEnableBrand() {
        return enableBrand;
    }

    public void setEnableBrand(boolean enableBrand) {
        this.enableBrand = enableBrand;
    }

    public ReceiptType getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(ReceiptType receiptType) {
        this.receiptType = receiptType;
    }

    public boolean isEnableMemberName() {
        return enableMemberName;
    }

    public void setEnableMemberName(boolean enableMemberName) {
        this.enableMemberName = enableMemberName;
    }

    public boolean isEnableMemberId() {
        return enableMemberId;
    }

    public void setEnableMemberId(boolean enableMemberId) {
        this.enableMemberId = enableMemberId;
    }

    public boolean isEnableMemberAddress() {
        return enableMemberAddress;
    }

    public void setEnableMemberAddress(boolean enableMemberAddress) {
        this.enableMemberAddress = enableMemberAddress;
    }

    public boolean isEnableShopAddress() {
        return enableShopAddress;
    }

    public void setEnableShopAddress(boolean enableShopAddress) {
        this.enableShopAddress = enableShopAddress;
    }

    public boolean isEnableShopPhoneNo() {
        return enableShopPhoneNo;
    }

    public void setEnableShopPhoneNo(boolean enableShopPhoneNo) {
        this.enableShopPhoneNo = enableShopPhoneNo;
    }

    public boolean isEnableIncludeItemInSKU() {
        return enableIncludeItemInSKU;
    }

    public void setEnableIncludeItemInSKU(boolean enableIncludeItemInSKU) {
        this.enableIncludeItemInSKU = enableIncludeItemInSKU;
    }

    public boolean isEnableExciseTaxAsItem() {
        return enableExciseTaxAsItem;
    }

    public void setEnableExciseTaxAsItem(boolean enableExciseTaxAsItem) {
        this.enableExciseTaxAsItem = enableExciseTaxAsItem;
    }

    public boolean isEnableMemberLoyaltyPoints() {
        return enableMemberLoyaltyPoints;
    }

    public void setEnableMemberLoyaltyPoints(boolean enableMemberLoyaltyPoints) {
        this.enableMemberLoyaltyPoints = enableMemberLoyaltyPoints;
    }

    public boolean isEnableNotes() {
        return enableNotes;
    }

    public void setEnableNotes(boolean enableNotes) {
        this.enableNotes = enableNotes;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public boolean isEnabledFreeText() {
        return enabledFreeText;
    }

    public void setEnabledFreeText(boolean enabledFreeText) {
        this.enabledFreeText = enabledFreeText;
    }

    public boolean isEnabledBottomFreeText() {
        return enabledBottomFreeText;
    }

    public void setEnabledBottomFreeText(boolean enabledBottomFreeText) {
        this.enabledBottomFreeText = enabledBottomFreeText;
    }

    public String getAboveFreeText() {
        return aboveFreeText;
    }

    public void setAboveFreeText(String aboveFreeText) {
        this.aboveFreeText = aboveFreeText;
    }

    public boolean isEnableEmployeeName() {
        return enableEmployeeName;
    }

    public void setEnableEmployeeName(boolean enableEmployeeName) {
        this.enableEmployeeName = enableEmployeeName;
    }

    public boolean isEnableMemberPhoneNo() {
        return enableMemberPhoneNo;
    }

    public void setEnableMemberPhoneNo(boolean enableMemberPhoneNo) {
        this.enableMemberPhoneNo = enableMemberPhoneNo;
    }

    public boolean isEnableItemDiscount() {
        return enableItemDiscount;
    }

    public void setEnableItemDiscount(boolean enableItemDiscount) {
        this.enableItemDiscount = enableItemDiscount;
    }

    public boolean isEnableItemDiscountNotes() {
        return enableItemDiscountNotes;
    }

    public void setEnableItemDiscountNotes(boolean enableItemDiscountNotes) {
        this.enableItemDiscountNotes = enableItemDiscountNotes;
    }

}
