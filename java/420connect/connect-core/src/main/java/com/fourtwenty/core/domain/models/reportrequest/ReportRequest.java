package com.fourtwenty.core.domain.models.reportrequest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.reporting.ReportType;

@CollectionName(name = "report_request", indexes = {"{companyId:1,shopId:1,delete:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportRequest extends ShopBaseModel {
    public enum RequestStatus {
        InProgress,
        Completed
    }
    private ReportType reportType;
    private long requestDate;
    private String employeeId;
    private CompanyAsset asset;
    private RequestStatus status;
    private long reqStartDate;
    private long reqEndDate;

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(long requestDate) {
        this.requestDate = requestDate;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public CompanyAsset getAsset() {
        return asset;
    }

    public void setAsset(CompanyAsset asset) {
        this.asset = asset;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public long getReqStartDate() {
        return reqStartDate;
    }

    public void setReqStartDate(long reqStartDate) {
        this.reqStartDate = reqStartDate;
    }

    public long getReqEndDate() {
        return reqEndDate;
    }

    public void setReqEndDate(long reqEndDate) {
        this.reqEndDate = reqEndDate;
    }
}
