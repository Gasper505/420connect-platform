package com.fourtwenty.core.domain.models.store;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.rest.store.results.WooComCartResult;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mdo on 5/10/17.
 */
@CollectionName(name = "consumer_cart", indexes = {"{consumerId:1,cartStatus:1}, {companyId:1,shopId:1,consumerId:1,cartStatus:1}",
        "{companyId:1,shopId:1,sessionId:1,cartStatus:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerCart extends ShopBaseModel {
    public enum ConsumerCartStatus {
        InProgress, // Order in progress; has not been placed
        Placed, // Order placed
        Accepted, // Order accepted
        Completed, // Order Completed
        Declined,
        CanceledByConsumer,
        CanceledByDispensary
    }

    public enum ConsumerTrackingStatus {
        NotStarted("Not Started"), // tracking has not begun
        Placed("Placed"),  // order has been replaced
        Accepted("Accepted"), // order has been accepted
        Packaged("Packaged"), // order has been packaged and waiting to be delivered
        OnTheWay("On The Way"), // order is on the way
        Delivered("Delivered"), // ordered is delivered or arrived
        WaitingPickup("Waiting Pickup"), // order is ready for pickup
        Declined("Declined");

        ConsumerTrackingStatus(String msg) {
            this.msg = msg;
        }
        public String msg;
    }

    public enum ConsumerOrderPickupType {
        Pickup,
        Delivery,
        Ship
    }

    public enum TransactionSource {
        Retail, Widget, WooCommerce
    }

    private String consumerId;
    private String memberId;
    private Cart cart;
    private Long orderPlacedTime;
    private Long acceptedTime;
    private Long packagedTime;
    private Long onTheWayTime;
    private Long completedTime;
    private Long canceledTime;
    private Long declinedTime;
    private boolean accepted = false;
    private boolean completed = false;
    private boolean packaged = false;
    private boolean onTheWay = false;
    private Integer eta;
    private String reason;
    private String sessionId;
    private String publicKey;

    private ConsumerCartStatus cartStatus = ConsumerCartStatus.InProgress;
    private ConsumerTrackingStatus trackingStatus = ConsumerTrackingStatus.NotStarted;
    private ConsumerOrderPickupType pickupType = ConsumerOrderPickupType.Delivery;
    private String source;
    private String transactionId; // If accepted, this transactionId will be filled.
    private String transNo;
    private String orderNo;
    private String employeeName;
    private String memo;
    private Long pickUpDate;
    private TransactionSource transactionSource = TransactionSource.Widget;
    private String assignedEmployeeId;
    private long deliveryDate;
    private Address deliveryAddress;
    private WooComCartResult.WooMemberGroupResult memberGroup;
    private transient String errorMsg;
    private Long completeAfter;
    private Set<String> orderTags = new HashSet<>();
    private HashMap<String,String> rewardErrorMap = null;
    private String rewardName;


    public HashMap<String, String> getRewardErrorMap() {
        return rewardErrorMap;
    }

    public void setRewardErrorMap(HashMap<String, String> rewardErrorMap) {
        this.rewardErrorMap = rewardErrorMap;
    }

    public Set<String> getOrderTags() {
        return orderTags;
    }

    public void setOrderTags(Set<String> orderTags) {
        this.orderTags = orderTags;
    }

    public Long getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(Long completeAfter) {
        this.completeAfter = completeAfter;
    }

    public Long getCanceledTime() {
        return canceledTime;
    }

    public void setCanceledTime(Long canceledTime) {
        this.canceledTime = canceledTime;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getEta() {
        return eta;
    }

    public void setEta(Integer eta) {
        this.eta = eta;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Long getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(Long completedTime) {
        this.completedTime = completedTime;
    }

    public Long getAcceptedTime() {
        return acceptedTime;
    }

    public void setAcceptedTime(Long acceptedTime) {
        this.acceptedTime = acceptedTime;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isOnTheWay() {
        return onTheWay;
    }

    public void setOnTheWay(boolean onTheWay) {
        this.onTheWay = onTheWay;
    }

    public Long getOnTheWayTime() {
        return onTheWayTime;
    }

    public void setOnTheWayTime(Long onTheWayTime) {
        this.onTheWayTime = onTheWayTime;
    }

    public boolean isPackaged() {
        return packaged;
    }

    public void setPackaged(boolean packaged) {
        this.packaged = packaged;
    }

    public Long getPackagedTime() {
        return packagedTime;
    }

    public void setPackagedTime(Long packagedTime) {
        this.packagedTime = packagedTime;
    }

    public ConsumerOrderPickupType getPickupType() {
        return pickupType;
    }

    public void setPickupType(ConsumerOrderPickupType pickupType) {
        this.pickupType = pickupType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public ConsumerCartStatus getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(ConsumerCartStatus cartStatus) {
        this.cartStatus = cartStatus;
    }

    public Long getOrderPlacedTime() {
        return orderPlacedTime;
    }

    public void setOrderPlacedTime(Long orderPlacedTime) {
        this.orderPlacedTime = orderPlacedTime;
    }

    public ConsumerTrackingStatus getTrackingStatus() {
        return trackingStatus;
    }

    public void setTrackingStatus(ConsumerTrackingStatus trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(Long pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public Long getDeclinedTime() {
        return declinedTime;
    }

    public void setDeclinedTime(Long declinedTime) {
        this.declinedTime = declinedTime;
    }

    public TransactionSource getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(TransactionSource transactionSource) {
        this.transactionSource = transactionSource;
    }

    public String getAssignedEmployeeId() {
        return assignedEmployeeId;
    }

    public void setAssignedEmployeeId(String assignedEmployeeId) {
        this.assignedEmployeeId = assignedEmployeeId;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public WooComCartResult.WooMemberGroupResult getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(WooComCartResult.WooMemberGroupResult memberGroup) {
        this.memberGroup = memberGroup;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

}
