package com.fourtwenty.core.domain.models.store;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

/**
 * Created by mdo on 5/22/17.
 */
@CollectionName(name = "consumer_pass_reset", uniqueIndexes = {"{resetCode:1}"}, indexes = {"{consumerUserId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerPasswordReset extends BaseModel {
    private String resetCode;
    private long expirationDate;
    private String consumerUserId;
    private boolean expired;

    public String getConsumerUserId() {
        return consumerUserId;
    }

    public void setConsumerUserId(String consumerUserId) {
        this.consumerUserId = consumerUserId;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }
}
