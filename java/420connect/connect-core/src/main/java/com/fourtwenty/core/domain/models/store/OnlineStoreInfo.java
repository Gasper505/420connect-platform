package com.fourtwenty.core.domain.models.store;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 5/16/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineStoreInfo extends ShopBaseModel {
    public static final int DEFAULT_ORDER_ETA = 45; // MINUTES

    public enum StoreColorTheme {
        Light,
        Dark
    }

    public enum EnableInventoryType {
        All,
        Custom
    }

    public enum ViewType {
        List,
        Grid
    }

    public enum StoreCartMinimumType {
        Subtotal,
        SubtotalWithDiscount
    }


    private boolean enableStorePickup = false;
    private boolean enableDelivery = false;
    private boolean enableOnlineShipment = false;

    private boolean enableProductReviews = true;
    @Deprecated
    private StoreColorTheme colorTheme = StoreColorTheme.Light;

    private int defaultETA = DEFAULT_ORDER_ETA; // in minutes

    private String pageOneMessageTitle;
    private String pageOneMessageBody;

    private String submissionMessage;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cartMinimum = new BigDecimal(0);

    private StoreCartMinimumType cartMinType = StoreCartMinimumType.SubtotalWithDiscount;

    private boolean enabled = false;
    private String websiteOrigins = "";
    private String supportEmail = "";
    private boolean enableOnlinePOS = false;

    private boolean enableDeliveryAreaRestrictions = false;
    private List<String> restrictedZipCodes = new ArrayList<>();

    private boolean useCustomETA = false;

    private String customMessageETA;

    private String storeHexColor;

    private ViewType viewType = ViewType.List;
    private boolean enableInventory = Boolean.FALSE;
    private EnableInventoryType enableInventoryType = EnableInventoryType.Custom;
    private String activeInventoryId;
    private boolean enableHtmlText = Boolean.FALSE;
    private String htmlText;
    private String websiteUrl;
    private boolean enableOtherMarketingSource;
    private String customCss;

    public EnableInventoryType getEnableInventoryType() {
        return enableInventoryType;
    }

    public void setEnableInventoryType(EnableInventoryType enableInventoryType) {
        this.enableInventoryType = enableInventoryType;
    }

    public StoreCartMinimumType getCartMinType() {
        return cartMinType;
    }

    public void setCartMinType(StoreCartMinimumType cartMinType) {
        this.cartMinType = cartMinType;
    }

    public boolean isEnableOnlinePOS() {
        return enableOnlinePOS;
    }

    public void setEnableOnlinePOS(boolean enableOnlinePOS) {
        this.enableOnlinePOS = enableOnlinePOS;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getWebsiteOrigins() {
        return websiteOrigins;
    }

    public void setWebsiteOrigins(String websiteOrigins) {
        this.websiteOrigins = websiteOrigins;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getCartMinimum() {
        return cartMinimum;
    }

    public void setCartMinimum(BigDecimal cartMinimum) {
        this.cartMinimum = cartMinimum;
    }

    public static int getDefaultOrderEta() {
        return DEFAULT_ORDER_ETA;
    }

    public StoreColorTheme getColorTheme() {
        return colorTheme;
    }

    public void setColorTheme(StoreColorTheme colorTheme) {
        this.colorTheme = colorTheme;
    }

    public int getDefaultETA() {
        return defaultETA;
    }

    public void setDefaultETA(int defaultETA) {
        this.defaultETA = defaultETA;
    }

    public boolean isEnableDelivery() {
        return enableDelivery;
    }

    public void setEnableDelivery(boolean enableDelivery) {
        this.enableDelivery = enableDelivery;
    }

    public boolean isEnableOnlineShipment() {
        return enableOnlineShipment;
    }

    public void setEnableOnlineShipment(boolean enableOnlineShipment) {
        this.enableOnlineShipment = enableOnlineShipment;
    }

    public boolean isEnableProductReviews() {
        return enableProductReviews;
    }

    public void setEnableProductReviews(boolean enableProductReviews) {
        this.enableProductReviews = enableProductReviews;
    }

    public boolean isEnableStorePickup() {
        return enableStorePickup;
    }

    public void setEnableStorePickup(boolean enableStorePickup) {
        this.enableStorePickup = enableStorePickup;
    }

    public String getPageOneMessageBody() {
        return pageOneMessageBody;
    }

    public void setPageOneMessageBody(String pageOneMessageBody) {
        this.pageOneMessageBody = pageOneMessageBody;
    }

    public String getPageOneMessageTitle() {
        return pageOneMessageTitle;
    }

    public void setPageOneMessageTitle(String pageOneMessageTitle) {
        this.pageOneMessageTitle = pageOneMessageTitle;
    }

    public String getSubmissionMessage() {
        return submissionMessage;
    }

    public void setSubmissionMessage(String submissionMessage) {
        this.submissionMessage = submissionMessage;
    }

    public boolean isEnableDeliveryAreaRestrictions() {
        return enableDeliveryAreaRestrictions;
    }

    public void setEnableDeliveryAreaRestrictions(boolean enableDeliveryAreaRestrictions) {
        this.enableDeliveryAreaRestrictions = enableDeliveryAreaRestrictions;
    }

    public List<String> getRestrictedZipCodes() {
        return restrictedZipCodes;
    }

    public void setRestrictedZipCodes(List<String> restrictedZipCodes) {
        this.restrictedZipCodes = restrictedZipCodes;
    }

    public boolean isUseCustomETA() {
        return useCustomETA;
    }

    public void setUseCustomETA(boolean useCustomETA) {
        this.useCustomETA = useCustomETA;
    }

    public String getCustomMessageETA() {
        return customMessageETA;
    }

    public void setCustomMessageETA(String customMessageETA) {
        this.customMessageETA = customMessageETA;
    }

    public String getStoreHexColor() {
        return storeHexColor;
    }

    public void setStoreHexColor(String storeHexColor) {
        this.storeHexColor = storeHexColor;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public boolean isEnableInventory() {
        return enableInventory;
    }

    public void setEnableInventory(boolean enableInventory) {
        this.enableInventory = enableInventory;
    }

    public String getActiveInventoryId() {
        return activeInventoryId;
    }

    public void setActiveInventoryId(String activeInventoryId) {
        this.activeInventoryId = activeInventoryId;
    }

    public boolean isEnableHtmlText() {
        return enableHtmlText;
    }

    public void setEnableHtmlText(boolean enableHtmlText) {
        this.enableHtmlText = enableHtmlText;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public boolean isEnableOtherMarketingSource() {
        return enableOtherMarketingSource;
    }

    public void setEnableOtherMarketingSource(boolean enableOtherMarketingSource) {
        this.enableOtherMarketingSource = enableOtherMarketingSource;
    }

    public String getCustomCss() {
        return customCss;
    }

    public void setCustomCss(String customCss) {
        this.customCss = customCss;
    }
}
