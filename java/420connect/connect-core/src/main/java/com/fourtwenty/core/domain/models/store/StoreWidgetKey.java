package com.fourtwenty.core.domain.models.store;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.developer.DeveloperKey;

/**
 * Created by mdo on 4/17/17.
 */
@CollectionName(name = "store_keys", uniqueIndexes = {"{key:1}"}, indexes = {"{companyId:1,shopId:1,active:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoreWidgetKey extends DeveloperKey {
}
