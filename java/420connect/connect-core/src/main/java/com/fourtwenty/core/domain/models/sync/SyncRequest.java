package com.fourtwenty.core.domain.models.sync;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "sync_requests")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SyncRequest extends ShopBaseModel {
    public enum SyncType {
        SYNC,
        RESET,
        RESYNC
    }

    public enum SyncTarget {
        METRC,
        WEEDMAP
    }

    public enum SyncStatus {
        QUEUED,
        IN_PROGRESS,
        FAILED,
        COMPLETED
    }

    private SyncType type = SyncType.SYNC;
    private SyncTarget target = SyncTarget.METRC;
    private SyncStatus status = SyncStatus.QUEUED;
    private long requestTime = 0;
    private Long startTime = null;
    private Long completeTime = null;

    public SyncType getType() {
        return type;
    }

    public void setType(SyncType type) {
        this.type = type;
    }

    public SyncTarget getTarget() {
        return target;
    }

    public void setTarget(SyncTarget target) {
        this.target = target;
    }

    public SyncStatus getStatus() {
        return status;
    }

    public void setStatus(SyncStatus status) {
        this.status = status;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }
}
