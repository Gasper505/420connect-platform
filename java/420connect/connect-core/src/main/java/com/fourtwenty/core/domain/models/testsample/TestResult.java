package com.fourtwenty.core.domain.models.testsample;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.Note;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestResult {
    @Deprecated
    private List<String> terpenes = new ArrayList<>();

    private List<Note> note = new ArrayList<>();
    private long submittedDate;
    private double thc;
    private double delta9Thc;
    private double delta8Thc;
    private double thcA;
    private double thcV;
    private double cbdA;
    private double cbd;
    private double cbn;
    private double cbc;
    private double cbgA;
    private double cbg;

    public List<String> getTerpenes() {
        return terpenes;
    }

    public void setTerpenes(List<String> terpenes) {
        this.terpenes = terpenes;
    }

    public List<Note> getNote() {
        return note;
    }

    public void setNote(List<Note> note) {
        this.note = note;
    }

    public long getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(long submittedDate) {
        this.submittedDate = submittedDate;
    }

    public double getThc() {
        return thc;
    }

    public void setThc(double thc) {
        this.thc = thc;
    }

    public double getDelta9Thc() {
        return delta9Thc;
    }

    public void setDelta9Thc(double delta9Thc) {
        this.delta9Thc = delta9Thc;
    }

    public double getDelta8Thc() {
        return delta8Thc;
    }

    public void setDelta8Thc(double delta8Thc) {
        this.delta8Thc = delta8Thc;
    }

    public double getThcA() {
        return thcA;
    }

    public void setThcA(double thcA) {
        this.thcA = thcA;
    }

    public double getThcV() {
        return thcV;
    }

    public void setThcV(double thcV) {
        this.thcV = thcV;
    }

    public double getCbdA() {
        return cbdA;
    }

    public void setCbdA(double cbdA) {
        this.cbdA = cbdA;
    }

    public double getCbd() {
        return cbd;
    }

    public void setCbd(double cbd) {
        this.cbd = cbd;
    }

    public double getCbn() {
        return cbn;
    }

    public void setCbn(double cbn) {
        this.cbn = cbn;
    }

    public double getCbc() {
        return cbc;
    }

    public void setCbc(double cbc) {
        this.cbc = cbc;
    }

    public double getCbgA() {
        return cbgA;
    }

    public void setCbgA(double cbgA) {
        this.cbgA = cbgA;
    }

    public double getCbg() {
        return cbg;
    }

    public void setCbg(double cbg) {
        this.cbg = cbg;
    }
}
