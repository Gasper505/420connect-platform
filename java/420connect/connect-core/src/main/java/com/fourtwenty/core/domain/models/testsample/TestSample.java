package com.fourtwenty.core.domain.models.testsample;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "test_sample", indexes = {"{batchId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestSample extends ShopBaseModel {
    public enum SampleStatus {
        IN_TESTING,
        PENDING,
        PASSED,
        FAILED,
        DRAFT
    }

    @NotEmpty
    private String batchId;
    private Long sampleNumber;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal sample = new BigDecimal(0);
    private Long dateSent;
    private SampleStatus status;
    private Long dateTested;
    private TestResult testResult;
    private String testingCompanyId;
    private Long datePassed;
    private List<CompanyAsset> attachments = new ArrayList<>();
    private String testId;
    private String referenceNo;
    private String licenseId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getSampleNumber() {
        return sampleNumber;
    }

    public void setSampleNumber(Long sampleNumber) {
        this.sampleNumber = sampleNumber;
    }

    public BigDecimal getSample() {
        return sample;
    }

    public void setSample(BigDecimal sample) {
        this.sample = sample;
    }

    public Long getDateSent() {
        return dateSent;
    }

    public void setDateSent(Long dateSent) {
        this.dateSent = dateSent;
    }

    public SampleStatus getStatus() {
        return status;
    }

    public void setStatus(SampleStatus status) {
        this.status = status;
    }

    public Long getDateTested() {
        return dateTested;
    }

    public void setDateTested(Long dateTested) {
        this.dateTested = dateTested;
    }

    public TestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(TestResult testResult) {
        this.testResult = testResult;
    }

    public String getTestingCompanyId() {
        return testingCompanyId;
    }

    public void setTestingCompanyId(String testingCompanyId) {
        this.testingCompanyId = testingCompanyId;
    }

    public Long getDatePassed() {
        return datePassed;
    }

    public void setDatePassed(Long datePassed) {
        this.datePassed = datePassed;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }
}
