package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by Gaurav Saini on 7/7/17.
 */

@CollectionName(name = "headset_locations", uniqueIndexes = {"{companyId:1,shopId:1}"}, indexes = {"{companyId:1,shopId:1,delete:1}", "{acceptedLiteVersion:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetLocation extends ShopBaseModel {
    private Long headsetCompanyId;
    private String apiKey;
    private boolean acceptedLiteVersion;
    private Long liteVersionAcceptedDate;
    private String dataSourceId;
    private String integrationKey;
    private Long lastSyncDate;
    private Long productLastSyncDate;
    private boolean enableSync = true;
    private Long enableSyncDate;
    private IntegrationSetting.Environment environment;


    public Long getProductLastSyncDate() {
        return productLastSyncDate;
    }

    public void setProductLastSyncDate(Long productLastSyncDate) {
        this.productLastSyncDate = productLastSyncDate;
    }

    public Long getEnableSyncDate() {
        return enableSyncDate;
    }

    public void setEnableSyncDate(Long enableSyncDate) {
        this.enableSyncDate = enableSyncDate;
    }

    public boolean isEnableSync() {
        return enableSync;
    }

    public void setEnableSync(boolean enableSync) {
        this.enableSync = enableSync;
    }

    public Long getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Long lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public Long getHeadsetCompanyId() {
        return headsetCompanyId;
    }

    public void setHeadsetCompanyId(Long headsetCompanyId) {
        this.headsetCompanyId = headsetCompanyId;
    }

    public String getIntegrationKey() {
        return integrationKey;
    }

    public void setIntegrationKey(String integrationKey) {
        this.integrationKey = integrationKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isAcceptedLiteVersion() {
        return acceptedLiteVersion;
    }

    public void setAcceptedLiteVersion(boolean acceptedLiteVersion) {
        this.acceptedLiteVersion = acceptedLiteVersion;
    }

    public Long getLiteVersionAcceptedDate() {
        return liteVersionAcceptedDate;
    }

    public void setLiteVersionAcceptedDate(Long liteVersionAcceptedDate) {
        this.liteVersionAcceptedDate = liteVersionAcceptedDate;
    }

    public String getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(String dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public IntegrationSetting.Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(IntegrationSetting.Environment environment) {
        this.environment = environment;
    }
}
