package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "linx_accounts", uniqueIndexes = {"{companyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinxAccount extends ShopBaseModel {
    private boolean enabled = false;
    private String accessToken;
    private String secret;
    private IntegrationSetting.Environment environment = IntegrationSetting.Environment.Development;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public IntegrationSetting.Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(IntegrationSetting.Environment environment) {
        this.environment = environment;
    }


    @Override
    public String toString() {
        return "LinxAccount{" +
                "enabled=" + enabled +
                ", accessToken='" + accessToken + '\'' +
                ", secret='" + secret + '\'' +
                ", environment=" + environment +
                '}';
    }
}

