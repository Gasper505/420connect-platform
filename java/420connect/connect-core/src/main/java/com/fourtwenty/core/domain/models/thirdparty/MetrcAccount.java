package com.fourtwenty.core.domain.models.thirdparty;

import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 8/25/17.
 */
@CollectionName(name = "metrc_accounts", uniqueIndexes = {"{companyId:1, stateCode:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetrcAccount extends CompanyBaseModel {
    private boolean enabled = false;
    private String userApiKey;
    private String stateCode;
    private List<MetrcFacilityAccount> facilities = new ArrayList<>();
    private IntegrationSetting.Environment environment;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<MetrcFacilityAccount> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<MetrcFacilityAccount> facilities) {
        this.facilities = facilities;
    }

    public String getUserApiKey() {
        return userApiKey;
    }

    public void setUserApiKey(String userApiKey) {
        this.userApiKey = userApiKey;
    }


    public MetrcFacilityAccount getMetrcFacilityAccount(String shopId) {
        if (shopId == null) return null;

        if (!CollectionUtils.isNullOrEmpty(getFacilities())) {
            for (MetrcFacilityAccount facility : getFacilities()) {
                if (shopId.equals(facility.getShopId())) return facility;
            }
        }
        return null;
    }

    public IntegrationSetting.Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(IntegrationSetting.Environment environment) {
        this.environment = environment;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}
