package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 8/25/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetrcFacilityAccount extends ShopBaseModel {
    private Long lastSyncDate;
    private boolean enabled = false;
    private boolean enableSync = false;
    private String facLicense;
    private boolean enableDeliveryTransaction = true;
    private String stateCode;
    private String userApiKey;
    private IntegrationSetting.Environment environment = IntegrationSetting.Environment.Development;

    public IntegrationSetting.Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(IntegrationSetting.Environment environment) {
        this.environment = environment;
    }

    public String getUserApiKey() {
        return userApiKey;
    }

    public void setUserApiKey(String userApiKey) {
        this.userApiKey = userApiKey;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnableSync() {
        return enableSync;
    }

    public void setEnableSync(boolean enableSync) {
        this.enableSync = enableSync;
    }

    public String getFacLicense() {
        return facLicense;
    }

    public void setFacLicense(String facLicense) {
        this.facLicense = facLicense;
    }

    public Long getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Long lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public boolean isEnableDeliveryTransaction() {
        return enableDeliveryTransaction;
    }

    public void setEnableDeliveryTransaction(boolean enableDeliveryTransaction) {
        this.enableDeliveryTransaction = enableDeliveryTransaction;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}
