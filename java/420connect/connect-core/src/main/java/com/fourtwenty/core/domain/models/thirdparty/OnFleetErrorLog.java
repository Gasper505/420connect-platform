package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "onFleet_error_logs",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetErrorLog extends ShopBaseModel {

    public enum ErrorType {
        Task,
        Worker,
        Team,
        Shop,
        Webhook
    }

    private String onFleetMessage;
    private String message;
    private String code;
    private String cause;
    private ErrorType errorType;
    private String errorSubType;
    private String apiKey;
    private String transactionId;

    public String getOnFleetMessage() {
        return onFleetMessage;
    }

    public void setOnFleetMessage(String onFleetMessage) {
        this.onFleetMessage = onFleetMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public String getErrorSubType() {
        return errorSubType;
    }

    public void setErrorSubType(String errorSubType) {
        this.errorSubType = errorSubType;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
