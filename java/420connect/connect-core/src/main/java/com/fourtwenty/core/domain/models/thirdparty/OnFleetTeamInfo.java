package com.fourtwenty.core.domain.models.thirdparty;

import com.fourtwenty.core.domain.models.generic.BaseModel;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTeamInfo extends BaseModel {

    private String name;
    private String teamId;
    private String hub;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }
}