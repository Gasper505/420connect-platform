package com.fourtwenty.core.domain.models.thirdparty;

import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "onFleet_teams", uniqueIndexes = {"{companyId:1,shopId:1}"},premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTeams extends ShopBaseModel {

    public enum SyncState {
        InProgress,
        Completed
    }

    private SyncState syncState;
    private long syncDate;
    private List<OnFleetTeamInfo> onFleetTeamInfoList = new ArrayList<>();

    public SyncState getSyncState() {
        return syncState;
    }

    public void setSyncState(SyncState syncState) {
        this.syncState = syncState;
    }

    public long getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(long syncDate) {
        this.syncDate = syncDate;
    }

    public List<OnFleetTeamInfo> getOnFleetTeamInfoList() {
        return onFleetTeamInfoList;
    }

    public void setOnFleetTeamInfoList(List<OnFleetTeamInfo> onFleetTeamInfoList) {
        this.onFleetTeamInfoList = onFleetTeamInfoList;
    }
}
