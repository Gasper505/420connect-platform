package com.fourtwenty.core.domain.models.thirdparty;

/**
 * Created by Gaurav Saini on 11/5/17.
 */
public class PatientVerificationStatus {

    private String patientName;
    // expires_at
    private String recommendationExpDate;
    // published_at
    private String recommendationIssueDate;
    // status
    private String recommendationStatus;

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getRecommendationExpDate() {
        return recommendationExpDate;
    }

    public void setRecommendationExpDate(String recommendationExpDate) {
        this.recommendationExpDate = recommendationExpDate;
    }

    public String getRecommendationIssueDate() {
        return recommendationIssueDate;
    }

    public void setRecommendationIssueDate(String recommendationIssueDate) {
        this.recommendationIssueDate = recommendationIssueDate;
    }

    public String getRecommendationStatus() {
        return recommendationStatus;
    }

    public void setRecommendationStatus(String recommendationStatus) {
        this.recommendationStatus = recommendationStatus;
    }
}
