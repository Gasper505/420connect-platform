package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductTagGroups {
    private String wmTagGroup;
    private String wmDiscoveryTag;
    private String wmTagGroupName;
    private String wmDiscoveryTagName;

    public String getWmTagGroup() {
        return wmTagGroup;
    }

    public void setWmTagGroup(String wmTagGroup) {
        this.wmTagGroup = wmTagGroup;
    }

    public String getWmDiscoveryTag() {
        return wmDiscoveryTag;
    }

    public void setWmDiscoveryTag(String wmDiscoveryTag) {
        this.wmDiscoveryTag = wmDiscoveryTag;
    }

    public String getWmTagGroupName() {
        return wmTagGroupName;
    }

    public void setWmTagGroupName(String wmTagGroupName) {
        this.wmTagGroupName = wmTagGroupName;
    }

    public String getWmDiscoveryTagName() {
        return wmDiscoveryTagName;
    }

    public void setWmDiscoveryTagName(String wmDiscoveryTagName) {
        this.wmDiscoveryTagName = wmDiscoveryTagName;
    }
}
