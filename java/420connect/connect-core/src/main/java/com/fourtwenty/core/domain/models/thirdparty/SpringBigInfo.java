package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "springbig_info", uniqueIndexes = {"{companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpringBigInfo extends ShopBaseModel {
    public enum SpringBigEnv {
        STAGE,
        PROD
    }

    private String authToken;
    private SpringBigEnv env = SpringBigEnv.STAGE;

    private boolean active = Boolean.FALSE;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public SpringBigEnv getEnv() {
        return env;
    }

    public void setEnv(SpringBigEnv env) {
        this.env = env;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
