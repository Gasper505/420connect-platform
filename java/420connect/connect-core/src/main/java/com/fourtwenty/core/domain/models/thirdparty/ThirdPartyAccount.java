package com.fourtwenty.core.domain.models.thirdparty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 2/10/17.
 */
@CollectionName(name = "tp_accounts", indexes = {"{companyId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThirdPartyAccount extends CompanyBaseModel {
    public enum ThirdPartyAccountType {
        None,
        CashVault,
        Weedmap,
        Quickbook,
        Mtrac,
        Clover
    }

    private ThirdPartyAccountType accountType = ThirdPartyAccountType.None;
    private boolean active = true;
    @NotEmpty
    private String name;
    @NotEmpty
    private String username;
    private String password;
    private String quickbook_companyId;
    private String shopId;
    private String clientId;
    private String clientSecret;
    private String locationId;
    private String merchantId;
    private String token;

    public ThirdPartyAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(ThirdPartyAccountType accountType) {
        this.accountType = accountType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getQuickbook_companyId() {
        return quickbook_companyId;
    }

    public void setQuickbook_companyId(String quickbook_companyId) {
        this.quickbook_companyId = quickbook_companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
