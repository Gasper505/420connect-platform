package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.HashMap;

/**
 * Created by mdo on 8/23/17.
 */
@CollectionName(name = "tp_shop_accounts", uniqueIndexes = {"{companyId:1,shopId:1,accountType:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThirdPartyShopAccount extends ShopBaseModel {
    public enum ThirdPartyShopAccountType {
        Hypur
    }

    private ThirdPartyShopAccountType accountType = ThirdPartyShopAccountType.Hypur;
    private boolean enabled = false;
    private String shopApiKey;
    private Long lastSyncDate;
    private boolean verified;
    private String entityISN;
    private HashMap<String, String> settings = new HashMap<>();

    public ThirdPartyShopAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(ThirdPartyShopAccountType accountType) {
        this.accountType = accountType;
    }

    public HashMap<String, String> getSettings() {
        return settings;
    }

    public void setSettings(HashMap<String, String> settings) {
        this.settings = settings;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Long getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Long lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getEntityISN() {
        return entityISN;
    }

    public void setEntityISN(String entityISN) {
        this.entityISN = entityISN;
    }

    public String getShopApiKey() {
        return shopApiKey;
    }

    public void setShopApiKey(String shopApiKey) {
        this.shopApiKey = shopApiKey;
    }
}
