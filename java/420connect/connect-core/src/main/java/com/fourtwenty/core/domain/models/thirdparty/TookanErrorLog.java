package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanBaseResponse;

@CollectionName(name = "tookan_error_logs")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanErrorLog extends ShopBaseModel {
    public enum ErrorType {
        TASK,
        TEAM,
        AGENT,
        SHOP
    }

    public enum SubErrorType {
        Create_Team,
        Update_Team,
        Delete_Team,
        Synchronize_Team,
        Create_Agent,
        Update_Agent,
        Delete_Agent,
        GET_Agent_Profile,
        Synchronize_Agent,
        Delivery_Task,
        PickUp_Task,
        Delete_Task,
        Get_All_Task,
        Incomplete_Details,
        Update_Task_Status,
        Update_Task,
        Get_Task
    }

    private String tookanMessage;
    private String message;
    private TookanBaseResponse.TookanErrorCode status;
    private int statusCode;
    private ErrorType errorCause;
    private SubErrorType subErrorCause;
    private String sourceId;
    private String apiKey;

    public String getTookanMessage() {
        return tookanMessage;
    }

    public void setTookanMessage(String tookanMessage) {
        this.tookanMessage = tookanMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TookanBaseResponse.TookanErrorCode getStatus() {
        return status;
    }

    public void setStatus(TookanBaseResponse.TookanErrorCode status) {
        this.status = status;
    }

    public ErrorType getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(ErrorType errorCause) {
        this.errorCause = errorCause;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public SubErrorType getSubErrorCause() {
        return subErrorCause;
    }

    public void setSubErrorCause(SubErrorType subErrorCause) {
        this.subErrorCause = subErrorCause;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
