package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 4/12/17.
 */
@CollectionName(name = "weedmap_accounts", indexes = {"{companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapAccount extends ShopBaseModel {
    public enum WeedmapSyncStatus {
        NotStarted,
        Completed,
        InProgress,
        Reset,
        Failed
    }

    public enum AuthStatus {
        Authorized,
        UnAuthorized
    }
    private String apiKey;
    private boolean active = true;
    private Long menuLastSyncDate; // null implies this weedmap account has not been sync'ed
    private Long menuSyncRequestDate; // null implies this weedmap account has not been sync'ed
    private Long catLastSyncDate;
    private WeedmapSyncStatus syncStatus = WeedmapSyncStatus.NotStarted;
    private List<WeedmapCategoryMapItem> categoryMapping = new ArrayList<>();
    private List<WeedmapApiKeyMap> apiKeyList = new ArrayList<>();
    public enum Version {
        V1,
        V2
    }
    private Version version = Version.V1;
    private IntegrationSetting.Environment environment = IntegrationSetting.Environment.Development;

    public WeedmapSyncStatus getSyncStatus() {
        return syncStatus;
    }

    public Long getMenuSyncRequestDate() {
        return menuSyncRequestDate;
    }

    public void setMenuSyncRequestDate(Long menuSyncRequestDate) {
        this.menuSyncRequestDate = menuSyncRequestDate;
    }

    public void setSyncStatus(WeedmapSyncStatus syncStatus) {
        this.syncStatus = syncStatus;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public List<WeedmapCategoryMapItem> getCategoryMapping() {
        return categoryMapping;
    }

    public void setCategoryMapping(List<WeedmapCategoryMapItem> categoryMapping) {
        this.categoryMapping = categoryMapping;
    }

    public Long getCatLastSyncDate() {
        return catLastSyncDate;
    }

    public void setCatLastSyncDate(Long catLastSyncDate) {
        this.catLastSyncDate = catLastSyncDate;
    }

    public Long getMenuLastSyncDate() {
        return menuLastSyncDate;
    }

    public void setMenuLastSyncDate(Long menuLastSyncDate) {
        this.menuLastSyncDate = menuLastSyncDate;
    }

    public List<WeedmapApiKeyMap> getApiKeyList() {
        return apiKeyList;
    }

    public void setApiKeyList(List<WeedmapApiKeyMap> apiKeyList) {
        this.apiKeyList = apiKeyList;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public IntegrationSetting.Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(IntegrationSetting.Environment environment) {
        this.environment = environment;
    }
}
