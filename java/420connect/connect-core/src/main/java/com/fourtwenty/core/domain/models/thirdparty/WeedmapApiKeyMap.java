package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.product.BlazeRegion;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Gaurav Saini on 12/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapApiKeyMap extends BaseModel {
    private String name;
    private String apiKey;
    private BlazeRegion region;
    private Long menuLastSyncDate; // null implies this weedmap account has not been sync'ed
    private Long menuSyncRequestDate; // null implies this weedmap account has not been sync'ed
    private WeedmapAccount.WeedmapSyncStatus syncStatus = WeedmapAccount.WeedmapSyncStatus.NotStarted;
    private String token;
    private String refreshToken;
    private boolean isTokenExpire;
    private Long expiresIn;
    private String listingWmId;
    private String code;
    private WeedmapAccount.AuthStatus status = WeedmapAccount.AuthStatus.UnAuthorized;
    private boolean enabled = false;

    public Long getMenuLastSyncDate() {
        return menuLastSyncDate;
    }

    public void setMenuLastSyncDate(Long menuLastSyncDate) {
        this.menuLastSyncDate = menuLastSyncDate;
    }

    public Long getMenuSyncRequestDate() {
        return menuSyncRequestDate;
    }

    public void setMenuSyncRequestDate(Long menuSyncRequestDate) {
        this.menuSyncRequestDate = menuSyncRequestDate;
    }

    public WeedmapAccount.WeedmapSyncStatus getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(WeedmapAccount.WeedmapSyncStatus syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public BlazeRegion getRegion() {
        return region;
    }

    public void setRegion(BlazeRegion region) {
        this.region = region;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public boolean isTokenExpire() {
        return isTokenExpire;
    }

    public void setTokenExpire(boolean tokenExpire) {
        isTokenExpire = tokenExpire;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getListingWmId() {
        return listingWmId;
    }

    public void setListingWmId(String listingWmId) {
        this.listingWmId = listingWmId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public WeedmapAccount.AuthStatus getStatus() {
        return status;
    }

    public void setStatus(WeedmapAccount.AuthStatus status) {
        this.status = status;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonIgnore
    public WeedmapAccount.Version getVersion() {
        if (StringUtils.isNotBlank(listingWmId)) {
            return WeedmapAccount.Version.V2;
        }
        return WeedmapAccount.Version.V1;
    }
}
