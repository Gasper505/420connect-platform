package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.weedmap.models.WeedmapCategory;

/**
 * Created by mdo on 4/12/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapCategoryMapItem {

    private String categoryId;
    private String flowerType;
    private WeedmapCategory weedmapCategory = WeedmapCategory.None;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public WeedmapCategory getWeedmapCategory() {
        return weedmapCategory;
    }

    public void setWeedmapCategory(WeedmapCategory weedmapCategory) {
        this.weedmapCategory = weedmapCategory;
    }


    public String getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(String flowerType) {
        this.flowerType = flowerType;
    }
}
