package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WmCategoryMapping {
    private String categoryId;
    private String wmCategory;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getWmCategory() {
        return wmCategory;
    }

    public void setWmCategory(String wmCategory) {
        this.wmCategory = wmCategory;
    }
}
