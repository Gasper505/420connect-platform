package com.fourtwenty.core.domain.models.thirdparty;

import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyProduct;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

@CollectionName(name = "wmMapping", uniqueIndexes = "{companyId:1, shopId:1, productId:1, deleted:false}")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
public class WmProductMapping extends ShopBaseModel {
    private List<String> wmTags = new ArrayList<>();
    private String categoryId;
    private String productId;
    private String wmTagGroup;
    private String wmDiscoveryTag;
    private String wmAccountId;
    private ThirdPartyProduct thirdPartyProduct;
    private transient BigDecimal wmThreshold = BigDecimal.ZERO;
    private List<ProductTagGroups> productTagGroups = new ArrayList<>();

    public List<String> getWmTags() {
        return wmTags;
    }

    public void setWmTags(List<String> wmTags) {
        this.wmTags = wmTags;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWmTagGroup() {
        return wmTagGroup;
    }

    public void setWmTagGroup(String wmTagGroup) {
        this.wmTagGroup = wmTagGroup;
    }

    public String getWmDiscoveryTag() {
        return wmDiscoveryTag;
    }

    public void setWmDiscoveryTag(String wmDiscoveryTag) {
        this.wmDiscoveryTag = wmDiscoveryTag;
    }

    public String getWmAccountId() {
        return wmAccountId;
    }

    public void setWmAccountId(String wmAccountId) {
        this.wmAccountId = wmAccountId;
    }

    public BigDecimal getWmThreshold() {
        return wmThreshold;
    }

    public void setWmThreshold(BigDecimal wmThreshold) {
        this.wmThreshold = wmThreshold;
    }

    public ThirdPartyProduct getThirdPartyProduct() {
        return thirdPartyProduct;
    }

    public void setThirdPartyProduct(ThirdPartyProduct thirdPartyProduct) {
        this.thirdPartyProduct = thirdPartyProduct;
    }

    public List<ProductTagGroups> getProductTagGroups() {
        return productTagGroups;
    }

    public void setProductTagGroups(List<ProductTagGroups> productTagGroups) {
        this.productTagGroups = productTagGroups;
    }

    public enum TagGroups {
        MAIN_CATEGORY(1, 1),
        SECOND_LEVEL(1, 2),
        THIRD_LEVEL(1, 3),
        FOURTH_LEVEL(1, 4),
        GENETICS(0, 5),
        CANNABIDIOL(0, 6);

        public int code;
        private int index;

        TagGroups(int code, int index) {
            this.code = code;
            this.index = index;
        }

        public static TagGroups getName(int index) {
            switch (index) {
                case 1:
                    return MAIN_CATEGORY;
                case 2:
                    return SECOND_LEVEL;
                case 3:
                    return THIRD_LEVEL;
                case 4:
                    return FOURTH_LEVEL;
                case 5:
                    return GENETICS;
                case 6:
                    return CANNABIDIOL;
                default:
                    return null;
            }
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Tags {
        FLOWER(1, "Flower", ""),
        CONCENTRATES(1, "Concentrates", ""),
        VAPE_PENS(1, "Vape Pens", ""),
        EDIBLES(1, "Edibles", ""),
        TOPICALS(1, "Topicals", ""),
        GEAR(1, "Gear", ""),
        CULTIVATION(1, "Cultivation", ""),

        INFUSED_FLOWER(2, "Infused Flower", "Flower"),
        SHAKE(2, "Shake", "Flower"),
        PRE_ROLL(2, "Pre Roll", "Flower"),
        SHATTER(2, "Shatter", "Concentrates"),
        BUDDER(2, "Budder", "Concentrates"),
        CRUMBLE(2, "Crumble", "Concentrates"),
        SAUCE(2, "Sauce", "Concentrates"),
        TAFFY(2, "Taffy", "Concentrates"),
        DISTILLATE(2, "Distillate", "Concentrates"),
        LIVE_RESIN(2, "Live Resin", "Concentrates"),
        NUG_RUN(2, "Nug Run", "Concentrates"),
        TRIM_RUN(2, "Trim Run", "Concentrates"),
        SUGAR_LEAF(2, "Sugar Leaf", "Concentrates"),
        CRYSTALLINE(2, "Crystalline", "Concentrates"),
        DRY_SIFT(2, "Dry Sift", "Concentrates"),
        ICE_HASH(2, "Ice Hash", "Concentrates"),
        ROSIN(2, "Rosin", "Concentrates"),
        CARTRIDGE(2, "Cartridge", "Vape Pens"),
        DISPOSABLE(2, "Disposable", "Vape Pens"),
        PODS(2, "Pods", "Vape Pens"),
        BATTERIES(2, "Batteries", "Vape Pens"),
        CANDY(2, "Candy", "Edibles"),
        BEVERAGES(2, "Beverages", "Edibles"),
        BAKED_GOODS(2, "Baked Goods", "Edibles"),
        CAPSULES(2, "Capsules", "Edibles"),
        SNACKS(2, "Snacks", "Edibles"),
        COOKING_INGREDIENTS(2, "Cooking Ingredients", "Edibles"),
        TINCTURE(2, "Tincture", "Edibles"),
        SUGAR_FREE(2, "Sugar Free", "Edibles"),
        GLUTEN_FREE(2, "Gluten Free", "Edibles"),
        DAIRY_FREE(2, "Dairy Free", "Edibles"),
        PALEO_EDIBLES(2, "Paleo Edibles", "Edibles"),
        NUT_FREE(2, "Nut Free", "Edibles"),
        KOSHER(2, "Kosher", "Edibles"),
        SPRAYS(2, "Sprays", "Topicals"),
        PATCHES(2, "Patches", "Topicals"),
        BALMS_SALVES(2, "Balms & Salves", "Topicals"),
        LOTIONS(2, "Lotions", "Topicals"),
        LUBRICANTS_OILS(2, "Lubricants & Oils", "Topicals"),
        BONGS(2, "Bongs", "Gear"),
        DAB_RIGS(2, "Dab Rigs", "Gear"),
        PIPES(2, "Pipes", "Gear"),
        VAPORIZER(2, "Vaporizer", "Gear"),
        ACCESSORIES(2, "Accessories", "Gear"),
        STORAGE(2, "Storage", "Gear"),
        GRINDERS(2, "Grinders", "Gear"),
        APPAREL(2, "Apparel", "Gear"),
        CLONE(2, "Clone", "Cultivation"),
        SEEDS(2, "Seeds", "Cultivation"),

        INFUSED_PRE_ROLL(3, "Infused Pre Roll", "Pre Roll"),
        BLUNT_WRAP(3, "Blunt Wrap", "Pre Roll"),
        SYRINGE(3, "Syringe", "Distillate"),
        JAR(3, "Jar", "Distillate"),
        PUSH_BUTTON(3, "Push Button", "Batteries"),
        PULL(3, "Pull", "Batteries"),
        GUMMIES(3, "Gummies", "Candy"),
        MINTS_GUM(3, "Mints & Gum", "Candy"),
        CHOCOLATES(3, "Chocolates", "Candy"),
        DRINKS(3, "Drinks", "Beverages"),
        POWDER(3, "Powder", "Beverages"),
        SYRUP(3, "Syrup", "Beverages"),
        BROWNIES(3, "Brownies", "Baked Goods"),
        COOKIES(3, "Cookies", "Baked Goods"),
        TREATS(3, "Treats", "Baked Goods"),
        CEREAL(3, "Cereal", "Snacks"),
        ICE_CREAM(3, "Ice Cream", "Snacks"),
        CONDIMENTS(3, "Condiments", "Cooking Ingredients"),
        BUTTER(3, "Butter", "Cooking Ingredients"),
        ASH_CATCHERS(3, "Ash Catchers", "Bongs"),
        BOWLS(3, "Bowls", "Bongs"),
        DOWNSTEMS(3, "Downstems", "Bongs"),
        ADAPTERS(3, "Adapters", "Bongs"),
        BUBBLERS(3, "Bubblers", "Bongs"),
        BANGER_NAILS(3, "Banger & Nails", "Dab Rigs"),
        CARP_CAP(3, "Carp Cap", "Dab Rigs"),
        DAB_TOOLS(3, "Dab Tools", "Dab Rigs"),
        DAB_MATS(3, "Dab Mats", "Dab Rigs"),
        TORCHES(3, "Torches", "Dab Rigs"),
        ENAILS(3, "E-Nails", "Dab Rigs"),
        ROLLING_PAPERS(3, "Rolling Papers", "Accessories"),
        STICKERS(3, "Stickers", "Accessories"),
        HATS(3, "Hats", "Apparel"),
        HOODIES(3, "Hoodies", "Apparel"),
        SHIRTS(3, "Shirts", "Apparel"),
        MALE(3, "Male", "Apparel"),
        FEMALE(3, "Female", "Apparel"),


        INDICA(5, "Indica", "Genetics"),
        SATIVA(5, "Sativa", "Genetics"),
        HYBRID(5, "Hybrid", "Genetics"),
        CBD(6, "CBD", "Cannabidiol");

        public int level;
        public String name;
        public String heading;
        public String value;

        Tags(int level, String name, String heading) {
            this.level = level;
            this.name = name;
            this.heading = heading;
            this.value = this.toString();
        }

        public static Tags toTag(String name) {
            name = name.toUpperCase().replaceAll("[&#$%^]", "").replaceAll("\\s* \\s*", "_");
            return Tags.valueOf(name);
        }
    }

    @JsonIgnore
    public static HashMap<String, LinkedHashMap<String, LinkedHashSet<Tags>>> getDefaultTagMapping() {
        HashMap<String, LinkedHashMap<String, LinkedHashSet<Tags>>> tagMap = new HashMap<>();
        for (WmProductMapping.Tags tag : WmProductMapping.Tags.values()) {
            WmProductMapping.TagGroups group = WmProductMapping.TagGroups.getName(tag.level);
            if (group.code != 1) {
                continue;
            }
            tagMap.putIfAbsent(group.name(), new LinkedHashMap<>());
            LinkedHashMap<String, LinkedHashSet<Tags>> map = tagMap.get(group.name());
            map.putIfAbsent(tag.heading, new LinkedHashSet<>());
            HashSet<Tags> list = map.get(tag.heading);
            list.add(tag);
        }
        return tagMap;
    }

    @JsonIgnore
    public static boolean verifyMainCategory(String tagStr) {
        if (StringUtils.isBlank(tagStr)) {
            return false;
        }
        Tags wmTag = Tags.toTag(tagStr);
        return (wmTag.level == 1);
    }

    @JsonIgnore
    public static List<String> getDefaultTags(String tagStr) {
        HashMap<String, LinkedHashMap<String, LinkedHashSet<Tags>>> defaultTagMapping = getDefaultTagMapping();

        String secondLevel = "";
        String thirdLevel = "";
        String fourthLevel = "";
        Tags tag = Tags.toTag(tagStr);
        if (defaultTagMapping.containsKey(TagGroups.SECOND_LEVEL.name()) && defaultTagMapping.get(TagGroups.SECOND_LEVEL.name()).containsKey(tag.name)) {
            secondLevel = getTag(defaultTagMapping.get(TagGroups.SECOND_LEVEL.name()).get(tag.name));
        }
        if (defaultTagMapping.containsKey(TagGroups.THIRD_LEVEL.name()) && defaultTagMapping.get(TagGroups.THIRD_LEVEL.name()).containsKey(secondLevel)) {
            thirdLevel = getTag(defaultTagMapping.get(TagGroups.THIRD_LEVEL.name()).get(secondLevel));
        }
        if (defaultTagMapping.containsKey(TagGroups.FOURTH_LEVEL.name()) && defaultTagMapping.get(TagGroups.FOURTH_LEVEL.name()).containsKey(thirdLevel)) {
            fourthLevel = getTag(defaultTagMapping.get(TagGroups.FOURTH_LEVEL.name()).get(thirdLevel));
        }
        List<String> tags = Lists.newArrayList(tagStr, secondLevel, thirdLevel, fourthLevel);
        tags.removeAll(Collections.singleton(null));
        tags.removeAll(Collections.singleton(""));
        return tags;
    }

    @JsonIgnore
    private static String getTag(LinkedHashSet<Tags> tags) {
        if (!tags.isEmpty()) {
            for (Tags tag : tags) {
                return tag.name;
            }
        }
        return null;
    }

    @JsonIgnore
    public static boolean validateTag(List<String> tags) {
        if (tags.isEmpty()) {
            return false;
        }
        HashMap<String, LinkedHashMap<String, LinkedHashSet<Tags>>> defaultTagMapping = getDefaultTagMapping();

        String currentTag = "";
        boolean isAdded = false;

        if (tags.size() < 4) {
            tags.add("");
            tags.add("");
            isAdded = true;
        }
        for (int i = 1; i <= 4; i++) {
            String tagStr = Lists.newArrayList(tags).get(i - 1);
            if (StringUtils.isBlank(tagStr) && !isAdded) {
                return false;
            }
            Tags tag = null;
            if (StringUtils.isNotBlank(tagStr)) {
                tag = Tags.toTag(tagStr);
            }
            boolean isFound = false;
            LinkedHashSet<Tags> hashSet = new LinkedHashSet<>();
            switch (i) {
                case 1 :
                    if (tag == null) {
                        return false;
                    }
                    currentTag = tag.name;
                    break;
                case 2 :
                    if (StringUtils.isBlank(currentTag)) {
                        return false;
                    }
                    if (defaultTagMapping.containsKey(TagGroups.SECOND_LEVEL.name()) && defaultTagMapping.get(TagGroups.SECOND_LEVEL.name()).containsKey(currentTag)) {
                        hashSet = defaultTagMapping.get(TagGroups.SECOND_LEVEL.name()).get(currentTag);
                    }
                    break;
                case 3:
                    if (StringUtils.isBlank(currentTag)) {
                        return false;
                    }
                    if (defaultTagMapping.containsKey(TagGroups.THIRD_LEVEL.name()) && defaultTagMapping.get(TagGroups.THIRD_LEVEL.name()).containsKey(currentTag)) {
                        hashSet = defaultTagMapping.get(TagGroups.THIRD_LEVEL.name()).get(currentTag);
                    }
                    break;
                case 4:
                    if (StringUtils.isBlank(currentTag)) {
                        return false;
                    }
                    if (defaultTagMapping.containsKey(TagGroups.FOURTH_LEVEL.name()) && defaultTagMapping.get(TagGroups.FOURTH_LEVEL.name()).containsKey(currentTag)) {
                        hashSet = defaultTagMapping.get(TagGroups.FOURTH_LEVEL.name()).get(currentTag);
                    }
                    break;
                default:
                    return false;

            }
            if (!hashSet.isEmpty()) {
                for (Tags tagSet : hashSet) {
                    if (tag != null && tagSet.name.equalsIgnoreCase(tag.name)) {
                        isFound = true;
                        currentTag = tagSet.name;
                        break;
                    }
                }
            } else {
                isFound = true;
            }
            if (!isFound) {
                return false;
            }
        }

        return true;
    }

    @JsonIgnore
    public void migrateProductTagGroups() {
        if (CollectionUtils.isNullOrEmpty(getProductTagGroups()) && StringUtils.isNotBlank(getWmTagGroup())) {
            ProductTagGroups discoveryTags = new ProductTagGroups();
            discoveryTags.setWmTagGroup(getWmTagGroup());
            discoveryTags.setWmDiscoveryTag(getWmDiscoveryTag());
            getProductTagGroups().add(discoveryTags);
        }
    }
}
