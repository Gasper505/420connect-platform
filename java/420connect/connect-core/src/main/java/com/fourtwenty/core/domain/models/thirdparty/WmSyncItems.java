package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.thirdparty.weedmap.WmSyncMenuDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CollectionName(name = "weedmap_syncItems", indexes = {"{companyId:1,shopId:1,itemId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WmSyncItems extends ShopBaseModel {

    private String itemId;
    private String wmItemId;
    private String wmsin;
    private WMItemType itemType;
    private String wmListingId;
    private HashMap<WMItemType, String> wmLinkedItems = new HashMap<>();
    private boolean verified;
    private List<WmSyncMenuDetails.WmProductVariantDetails> variantDetails = new ArrayList<>();

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getWmItemId() {
        return wmItemId;
    }

    public void setWmItemId(String wmItemId) {
        this.wmItemId = wmItemId;
    }

    public String getWmsin() {
        return wmsin;
    }

    public void setWmsin(String wmsin) {
        this.wmsin = wmsin;
    }

    public WMItemType getItemType() {
        return itemType;
    }

    public void setItemType(WMItemType itemType) {
        this.itemType = itemType;
    }

    public String getWmListingId() {
        return wmListingId;
    }

    public void setWmListingId(String wmListingId) {
        this.wmListingId = wmListingId;
    }

    public HashMap<WMItemType, String> getWmLinkedItems() {
        return wmLinkedItems;
    }

    public void setWmLinkedItems(HashMap<WMItemType, String> wmLinkedItems) {
        this.wmLinkedItems = wmLinkedItems;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public List<WmSyncMenuDetails.WmProductVariantDetails> getVariantDetails() {
        return variantDetails;
    }

    public void setVariantDetails(List<WmSyncMenuDetails.WmProductVariantDetails> variantDetails) {
        this.variantDetails = variantDetails;
    }

    public enum WMItemType {
        BRANDS("brands"),
        ORGANIZATIONS("organizations"),
        PRODUCTS("products"),
        TAGS("tags"),
        TAGS_GROUPS("tagGroups"),
        CATALOG("catalogs"),
        /**
         * Linked Items
         **/
        LINKED_TAGS("link_tags"),
        PRODUCT_IMAGE("product_images"),
        PRODUCT_VARIANTS("product_variants"),
        VARIANT_ATTRIBUTES("variant_attributes"),
        VARIANT_VALUES("variant_values"),
        VARIANT_OPTIONS("product_variant_options"),
        CATALOG_ITEMS("catalog_items"),
        BATCHES("batches"),
        INVENTORY("inventory");

        private String type;

        WMItemType(String type) {
            this.type = type;
        }

        @JsonValue
        public String getWeedmapType() {
            return type;
        }

        @JsonSetter
        public void setWeedmapType(String t) {
            type = t.toLowerCase();
        }
    }
}
