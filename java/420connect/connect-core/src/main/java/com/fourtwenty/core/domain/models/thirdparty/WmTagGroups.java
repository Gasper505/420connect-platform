package com.fourtwenty.core.domain.models.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "weedmap_tagGroups")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WmTagGroups extends BaseModel {
    private String tagGroupId;
    private String tagGroupName;

    private List<WmDiscoveryTags> discoveryTags = new ArrayList<>();

    public String getTagGroupId() {
        return tagGroupId;
    }

    public void setTagGroupId(String tagGroupId) {
        this.tagGroupId = tagGroupId;
    }

    public String getTagGroupName() {
        return tagGroupName;
    }

    public void setTagGroupName(String tagGroupName) {
        this.tagGroupName = tagGroupName;
    }

    public List<WmDiscoveryTags> getDiscoveryTags() {
        return discoveryTags;
    }

    public void setDiscoveryTags(List<WmDiscoveryTags> discoveryTags) {
        this.discoveryTags = discoveryTags;
    }

    public static class WmDiscoveryTags extends BaseModel {
        private String tagId;
        private String tagName;

        public String getTagId() {
            return tagId;
        }

        public void setTagId(String tagId) {
            this.tagId = tagId;
        }

        public String getTagName() {
            return tagName;
        }

        public void setTagName(String tagName) {
            this.tagName = tagName;
        }
    }
}
