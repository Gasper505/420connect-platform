package com.fourtwenty.core.domain.models.thirdparty.leafly;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "leafly_account", indexes = {"{companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaflyAccount extends ShopBaseModel {
    public enum LeaflySyncStatus {
        NotStarted,
        Completed,
        InProgress,
        Reset,
        Failed
    }

    private String apiKey;
    private String clientKey;
    private long lastSync;
    private boolean syncEnabled = Boolean.FALSE;
    private LeaflySyncStatus syncStatus = LeaflySyncStatus.NotStarted;
    private Long menuSyncRequestDate;
    private Boolean keyUpdate = Boolean.TRUE;
    private List<LeaflyCategoryMapItem> categoryMapping = new ArrayList<>();
    private List<LeaflyVariantUnitMap> variantUnitMapping = new ArrayList<>();

    public String getApiKey() {
        return apiKey;
    }

    public LeaflyAccount setApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public String getClientKey() {
        return clientKey;
    }

    public LeaflyAccount setClientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
    }

    public long getLastSync() {
        return lastSync;
    }

    public LeaflyAccount setLastSync(long lastSync) {
        this.lastSync = lastSync;
        return this;
    }

    public boolean isSyncEnabled() {
        return syncEnabled;
    }

    public LeaflyAccount setSyncEnabled(boolean syncEnabled) {
        this.syncEnabled = syncEnabled;
        return this;
    }

    public LeaflySyncStatus getSyncStatus() {
        return syncStatus;
    }

    public LeaflyAccount setSyncStatus(LeaflySyncStatus syncStatus) {
        this.syncStatus = syncStatus;
        return this;
    }

    public Long getMenuSyncRequestDate() {
        return menuSyncRequestDate;
    }

    public LeaflyAccount setMenuSyncRequestDate(Long menuSyncRequestDate) {
        this.menuSyncRequestDate = menuSyncRequestDate;
        return this;
    }

    public Boolean getKeyUpdate() {
        return keyUpdate;
    }

    public LeaflyAccount setKeyUpdate(Boolean keyUpdate) {
        this.keyUpdate = keyUpdate;
        return this;
    }

    public List<LeaflyCategoryMapItem> getCategoryMapping() {
        return categoryMapping;
    }

    public LeaflyAccount setCategoryMapping(List<LeaflyCategoryMapItem> categoryMapping) {
        this.categoryMapping = categoryMapping;
        return this;
    }

    public List<LeaflyVariantUnitMap> getVariantUnitMapping() {
        return variantUnitMapping;
    }

    public LeaflyAccount setVariantUnitMapping(List<LeaflyVariantUnitMap> variantUnitMapping) {
        this.variantUnitMapping = variantUnitMapping;
        return this;
    }
}
