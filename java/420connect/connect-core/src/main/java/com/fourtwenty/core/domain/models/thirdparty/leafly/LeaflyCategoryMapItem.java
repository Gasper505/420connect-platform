package com.fourtwenty.core.domain.models.thirdparty.leafly;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyCategory;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaflyCategoryMapItem {

    private String flowerType;
    private LeaflyCategory leaflyCategory;

    public String getFlowerType() {
        return flowerType;
    }

    public LeaflyCategoryMapItem setFlowerType(String flowerType) {
        this.flowerType = flowerType;
        return this;
    }

    public LeaflyCategory getLeaflyCategory() {
        return leaflyCategory;
    }

    public LeaflyCategoryMapItem setLeaflyCategory(LeaflyCategory leaflyCategory) {
        this.leaflyCategory = leaflyCategory;
        return this;
    }
}
