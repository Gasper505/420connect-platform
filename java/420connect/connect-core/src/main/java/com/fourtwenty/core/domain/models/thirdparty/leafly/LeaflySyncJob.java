package com.fourtwenty.core.domain.models.thirdparty.leafly;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@CollectionName(name = "leafly_sync_jobs", indexes = {"{companyId:1,shopId:1,created:-1}"})
public class LeaflySyncJob extends ShopBaseModel {

    public enum LeaflySyncType {
        NIGHTLY_BASIS,
        INDIVIDUAL,
        RESET
    }

    public enum LeaflySyncJobStatus {
        Queued,
        InProgress,
        Error,
        Completed
    }

    private LeaflySyncType jobType = LeaflySyncType.NIGHTLY_BASIS;
    private LeaflySyncJobStatus status = LeaflySyncJobStatus.Queued;
    private String employeeId;
    private long requestTime = 0;
    private Long startTime;
    private Long completeTime;
    private String errorMsg;
    private List<String> productIds = new ArrayList<>();
    private String accountId;

    public LeaflySyncType getJobType() {
        return jobType;
    }

    public LeaflySyncJob setJobType(LeaflySyncType jobType) {
        this.jobType = jobType;
        return this;
    }

    public LeaflySyncJobStatus getStatus() {
        return status;
    }

    public LeaflySyncJob setStatus(LeaflySyncJobStatus status) {
        this.status = status;
        return this;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public LeaflySyncJob setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
        return this;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public LeaflySyncJob setRequestTime(long requestTime) {
        this.requestTime = requestTime;
        return this;
    }

    public Long getStartTime() {
        return startTime;
    }

    public LeaflySyncJob setStartTime(Long startTime) {
        this.startTime = startTime;
        return this;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public LeaflySyncJob setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
        return this;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public LeaflySyncJob setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public LeaflySyncJob setProductIds(List<String> productIds) {
        this.productIds = productIds;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public LeaflySyncJob setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }
}
