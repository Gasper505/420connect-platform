package com.fourtwenty.core.domain.models.thirdparty.leafly;

import com.fourtwenty.core.thirdparty.leafly.model.LeaflyVariantUnit;

public class LeaflyVariantUnitMap {
    private String variantUnit;
    private LeaflyVariantUnit leaflyVariantUnit = LeaflyVariantUnit.OZ;

    public String getVariantUnit() {
        return variantUnit;
    }

    public LeaflyVariantUnitMap setVariantUnit(String variantUnit) {
        this.variantUnit = variantUnit;
        return this;
    }

    public LeaflyVariantUnit getLeaflyVariantUnit() {
        return leaflyVariantUnit;
    }

    public LeaflyVariantUnitMap setLeaflyVariantUnit(LeaflyVariantUnit leaflyVariantUnit) {
        this.leaflyVariantUnit = leaflyVariantUnit;
        return this;
    }
}
