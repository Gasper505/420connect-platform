package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.intuit.ipp.data.AccountClassificationEnum;
import com.intuit.ipp.data.ReferenceType;

import java.math.BigDecimal;

@CollectionName(name = "quickbook_account_info",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountInfo extends ShopBaseModel {
    private String accountType;
    private String accountSubType;
    private String accountAlias;
    private String acctNum;
    private String acctNumExtn;
    private String bankNum;
    private String description;
    private String fullyQualifiedName;
    private String name;
    private String txnLocationType;
    private BigDecimal currentBalance;
    private String fiName;
    private BigDecimal currentBalanceWithSubAccounts;
    private ReferenceType currencyRef;
    private AccountClassificationEnum classification;
    private String companyId;
    private String quickbookCompanyId;
    private String shopId;

    @java.lang.Override
    public String getCompanyId() {
        return companyId;
    }

    @java.lang.Override
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getQuickbookCompanyId() {
        return quickbookCompanyId;
    }

    public void setQuickbookCompanyId(String quickbookCompanyId) {
        this.quickbookCompanyId = quickbookCompanyId;
    }

    @java.lang.Override
    public String getShopId() {
        return shopId;
    }

    @java.lang.Override
    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public ReferenceType getCurrencyRef() {
        return currencyRef;
    }

    public void setCurrencyRef(ReferenceType currencyRef) {
        this.currencyRef = currencyRef;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }


    public AccountClassificationEnum getClassification() {
        return classification;
    }

    public void setClassification(AccountClassificationEnum classification) {
        this.classification = classification;
    }

    public BigDecimal getCurrentBalanceWithSubAccounts() {
        return currentBalanceWithSubAccounts;
    }

    public void setCurrentBalanceWithSubAccounts(BigDecimal currentBalanceWithSubAccounts) {
        this.currentBalanceWithSubAccounts = currentBalanceWithSubAccounts;
    }

    public String getAccountSubType() {
        return accountSubType;
    }

    public void setAccountSubType(String accountSubType) {
        this.accountSubType = accountSubType;
    }

    public String getAccountAlias() {
        return accountAlias;
    }

    public void setAccountAlias(String accountAlias) {
        this.accountAlias = accountAlias;
    }

    public String getAcctNum() {
        return acctNum;
    }

    public void setAcctNum(String acctNum) {
        this.acctNum = acctNum;
    }

    public String getAcctNumExtn() {
        return acctNumExtn;
    }

    public void setAcctNumExtn(String acctNumExtn) {
        this.acctNumExtn = acctNumExtn;
    }

    public String getBankNum() {
        return bankNum;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullyQualifiedName() {
        return fullyQualifiedName;
    }

    public void setFullyQualifiedName(String fullyQualifiedName) {
        this.fullyQualifiedName = fullyQualifiedName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTxnLocationType() {
        return txnLocationType;
    }

    public void setTxnLocationType(String txnLocationType) {
        this.txnLocationType = txnLocationType;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getFiName() {
        return fiName;
    }

    public void setFiName(String fiName) {
        this.fiName = fiName;
    }


}
