package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "error_logs",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorLogs extends ShopBaseModel {
    private QuickbookSyncDetails.QuickbookEntityType quickBookEntityType;
    private long errorTime;
    private String statusCode;
    private String description;
    private String qbType;
    private String reference;

    public QuickbookSyncDetails.QuickbookEntityType getQuickBookEntityType() {
        return quickBookEntityType;
    }

    public void setQuickBookEntityType(QuickbookSyncDetails.QuickbookEntityType quickBookEntityType) {
        this.quickBookEntityType = quickBookEntityType;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQbType() {
        return qbType;
    }

    public void setQbType(String qbType) {
        this.qbType = qbType;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
