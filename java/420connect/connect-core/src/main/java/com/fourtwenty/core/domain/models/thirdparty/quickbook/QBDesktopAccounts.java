package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "qb_desktop_accounts",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QBDesktopAccounts extends ShopBaseModel {
    public enum AccountType {
        Bank("Bank Accounts"),
        Income("Income Accounts"),
        CostOfGoodsSold("Cost of Goods Sold Accounts"),
        OtherCurrentAsset("Other Current Asset Accounts"),
        AccountsPayable("Payable Accounts"),
        AccountsReceivable("Receivable Accounts");

        AccountType(String description) {
            this.description = description;
        }
        public String description;
    }

    private String accountName;
    private AccountType accountType;
    private String accountId;
    private String description;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
