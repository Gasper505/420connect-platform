package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.List;
import java.util.Map;

@CollectionName(name = "quickbook_desktop_operations",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QBDesktopOperation extends ShopBaseModel {
    public enum OperationType {
        Vendor,
        Item,
        Customer,
        Sale,
        PurchaseOrder,
        Refund,
        Invoice,
        InventoryAdjustment,
        Bill,
        PaymentReceived,
        ProductCategory,
        ProductCategoryJournalEntry,
        SalesByproductJournalEntryForInvoice,
        Expenses,
        BillPayment
    }

    private OperationType operationType;
    private boolean enabled;
    private Map<String, QBDesktopField> qbDesktopFieldMap;
    private List<String> appTarget;
    private String displayName;
    private List<String> methodology;
    private boolean syncPaused = Boolean.FALSE;
    private long enableSyncTime;

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, QBDesktopField> getQbDesktopFieldMap() {
        return qbDesktopFieldMap;
    }

    public void setQbDesktopFieldMap(Map<String, QBDesktopField> qbDesktopFieldMap) {
        this.qbDesktopFieldMap = qbDesktopFieldMap;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isSyncPaused() {
        return syncPaused;
    }

    public void setSyncPaused(boolean syncPaused) {
        this.syncPaused = syncPaused;
    }

    public Long getEnableSyncTime() {
        return enableSyncTime;
    }

    public void setEnableSyncTime(Long enableSyncTime) {
        this.enableSyncTime = enableSyncTime;
    }

    public List<String> getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(List<String> appTarget) {
        this.appTarget = appTarget;
    }

    public List<String> getMethodology() {
        return methodology;
    }

    public void setMethodology(List<String> methodology) {
        this.methodology = methodology;
    }
}
