package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickbook_desktop_sync_reference",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QBDesktopSyncReference extends ShopBaseModel {

    private String objectId;
    private String listId;
    private String editSequence;
    private QBDesktopOperation.OperationType type;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public QBDesktopOperation.OperationType getType() {
        return type;
    }

    public void setType(QBDesktopOperation.OperationType type) {
        this.type = type;
    }
}
