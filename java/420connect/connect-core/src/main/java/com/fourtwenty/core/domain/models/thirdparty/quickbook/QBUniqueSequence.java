package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickBook_unique_sequence",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QBUniqueSequence extends ShopBaseModel {
    private String name;
    private int currentSequence;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentSequence() {
        return currentSequence;
    }

    public void setCurrentSequence(int currentSequence) {
        this.currentSequence = currentSequence;
    }
}
