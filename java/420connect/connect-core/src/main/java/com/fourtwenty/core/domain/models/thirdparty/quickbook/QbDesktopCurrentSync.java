package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.Map;

@CollectionName(name = "quickBook_desktop_current_sync",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QbDesktopCurrentSync extends ShopBaseModel {

    private Map<String, String> referenceIdsMap;
    private String syncReference;
    private QuickbookSyncDetails.QuickbookEntityType quickBookEntityType;

    public Map<String, String> getReferenceIdsMap() {
        return referenceIdsMap;
    }

    public void setReferenceIdsMap(Map<String, String> referenceIdsMap) {
        this.referenceIdsMap = referenceIdsMap;
    }

    public String getSyncReference() {
        return syncReference;
    }

    public void setSyncReference(String syncReference) {
        this.syncReference = syncReference;
    }

    public QuickbookSyncDetails.QuickbookEntityType getQuickBookEntityType() {
        return quickBookEntityType;
    }

    public void setQuickBookEntityType(QuickbookSyncDetails.QuickbookEntityType quickBookEntityType) {
        this.quickBookEntityType = quickBookEntityType;
    }
}
