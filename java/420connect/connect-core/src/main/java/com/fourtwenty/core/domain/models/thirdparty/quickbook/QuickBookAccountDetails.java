package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickbook_account_details")
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickBookAccountDetails extends ShopBaseModel {
    public enum AccountType {
        Product,
        ProductCategory,
        Default
    }

    private String referenceId;
    private AccountType accountType = AccountType.Default;
    private String inventory;
    private String sales;
    private String suppliesAndMaterials;
    private String payableAccount;
    private String checking;
    private String clearance;
    private String cc_clearance;
    private String receivable;
    private boolean status = Boolean.TRUE;

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getSuppliesAndMaterials() {
        return suppliesAndMaterials;
    }

    public void setSuppliesAndMaterials(String suppliesAndMaterials) {
        this.suppliesAndMaterials = suppliesAndMaterials;
    }

    public String getPayableAccount() {
        return payableAccount;
    }

    public void setPayableAccount(String payableAccount) {
        this.payableAccount = payableAccount;
    }

    public String getChecking() {
        return checking;
    }

    public void setChecking(String checking) {
        this.checking = checking;
    }

    public String getClearance() {
        return clearance;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public String getCc_clearance() {
        return cc_clearance;
    }

    public void setCc_clearance(String cc_clearance) {
        this.cc_clearance = cc_clearance;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
