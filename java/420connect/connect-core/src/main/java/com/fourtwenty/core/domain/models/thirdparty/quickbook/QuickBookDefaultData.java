package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickBookDefaultData {
    @JsonProperty("qbDesktopOperations")
    private List<QBDesktopOperation> qbDesktopOperations;

    public List<QBDesktopOperation> getQbDesktopOperations() {
        return qbDesktopOperations;
    }

    public void setQbDesktopOperations(List<QBDesktopOperation> qbDesktopOperations) {
        this.qbDesktopOperations = qbDesktopOperations;
    }
}
