package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.List;

@CollectionName(name = "quickbook_entities_configuration",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickbookCustomEntities extends ShopBaseModel {

    public enum QbCustomAccount {
        None,
        Custom,
        QbSync
    }

    private boolean purchaseOrder;
    private boolean refundReceipt;
    private boolean invoice;
    private boolean salesReceipt;
    private boolean bill;

    private String inventory;
    private String sales;
    private String suppliesandMaterials;
    private String payableAccount;
    private String checking;
    private String clearance;
    private String cc_clearance;
    private String income;
    private String receivable;

    private String syncStrategy;
    private String quickbookmethology;
    private String qbtypes;
    private String qbCompanyId;
    private boolean syncDesktop;
    private boolean accounts;
    private List<QBDesktopAccounts> qbDesktopAccounts;
    private QbCustomAccount qbCustomAccount = QbCustomAccount.None;
    private boolean syncCustomAccount;
    private boolean syncCustomItems;
    private String qbPassword;
    private long syncTime;


    public boolean isPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(boolean purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public boolean isRefundReceipt() {
        return refundReceipt;
    }

    public void setRefundReceipt(boolean refundReceipt) {
        this.refundReceipt = refundReceipt;
    }

    public boolean isInvoice() {
        return invoice;
    }

    public void setInvoice(boolean invoice) {
        this.invoice = invoice;
    }

    public boolean isSalesReceipt() {
        return salesReceipt;
    }

    public void setSalesReceipt(boolean salesReceipt) {
        this.salesReceipt = salesReceipt;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getSuppliesandMaterials() {
        return suppliesandMaterials;
    }

    public void setSuppliesandMaterials(String suppliesandMaterials) {
        this.suppliesandMaterials = suppliesandMaterials;
    }

    public String getPayableAccount() {
        return payableAccount;
    }

    public String getSyncStrategy() {
        return syncStrategy;
    }

    public void setSyncStrategy(String syncStrategy) {
        this.syncStrategy = syncStrategy;
    }

    public void setPayableAccount(String payableAccount) {
        this.payableAccount = payableAccount;
    }

    public String getChecking() {
        return checking;
    }

    public void setChecking(String checking) {
        this.checking = checking;
    }

    public String getQuickbookmethology() {
        return quickbookmethology;
    }

    public void setQuickbookmethology(String quickbookmethology) {
        this.quickbookmethology = quickbookmethology;
    }

    public String getQbtypes() {
        return qbtypes;
    }

    public void setQbtypes(String qbtypes) {
        this.qbtypes = qbtypes;
    }

    public String getQbCompanyId() {
        return qbCompanyId;
    }

    public void setQbCompanyId(String qbCompanyId) {
        this.qbCompanyId = qbCompanyId;
    }

    public boolean isBill() {
        return bill;
    }

    public void setBill(boolean bill) {
        this.bill = bill;
    }

    public String getClearance() {
        return clearance;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public String getCc_clearance() {
        return cc_clearance;
    }

    public void setCc_clearance(String cc_clearance) {
        this.cc_clearance = cc_clearance;
    }

    public boolean isSyncDesktop() {
        return syncDesktop;
    }

    public void setSyncDesktop(boolean syncDesktop) {
        this.syncDesktop = syncDesktop;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public boolean isAccounts() {
        return accounts;
    }

    public void setAccounts(boolean accounts) {
        this.accounts = accounts;
    }

    public List<QBDesktopAccounts> getQbDesktopAccounts() {
        return qbDesktopAccounts;
    }

    public void setQbDesktopAccounts(List<QBDesktopAccounts> qbDesktopAccounts) {
        this.qbDesktopAccounts = qbDesktopAccounts;
    }

    public QbCustomAccount getQbCustomAccount() {
        return qbCustomAccount;
    }

    public void setQbCustomAccount(QbCustomAccount qbCustomAccount) {
        this.qbCustomAccount = qbCustomAccount;
    }

    public boolean isSyncCustomAccount() {
        return syncCustomAccount;
    }

    public void setSyncCustomAccount(boolean syncCustomAccount) {
        this.syncCustomAccount = syncCustomAccount;
    }

    public boolean isSyncCustomItems() {
        return syncCustomItems;
    }

    public void setSyncCustomItems(boolean syncCustomItems) {
        this.syncCustomItems = syncCustomItems;
    }

    public String getQbPassword() {
        return qbPassword;
    }

    public void setQbPassword(String qbPassword) {
        this.qbPassword = qbPassword;
    }

    public long getSyncTime() {
        return syncTime;
    }

    public void setSyncTime(long syncTime) {
        this.syncTime = syncTime;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }
}
