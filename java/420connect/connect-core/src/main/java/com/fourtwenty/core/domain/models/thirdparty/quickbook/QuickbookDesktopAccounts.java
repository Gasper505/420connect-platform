package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickbook_desktop_accounts",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickbookDesktopAccounts extends ShopBaseModel {

    private String accountName;

    private String AccountType;

    private String AccountId;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountType() {
        return AccountType;
    }

    public void setAccountType(String accountType) {
        AccountType = accountType;
    }

    public String getAccountId() {
        return AccountId;
    }

    public void setAccountId(String accountId) {
        AccountId = accountId;
    }
}
