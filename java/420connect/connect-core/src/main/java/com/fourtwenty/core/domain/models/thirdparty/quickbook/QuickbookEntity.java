package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickbook_entity", premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickbookEntity extends ShopBaseModel {

    public enum SyncStrategy {
        SalesByProduct,
        SalesByConsumer,
        DeliveryProduct,
        ProductRef,
        CosumerProductRef,
        DailySyncVendorRef

    }

    private SyncStrategy SyncStrategy;
    private String productRef;
    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public QuickbookEntity.SyncStrategy getSyncStrategy() {
        return SyncStrategy;
    }

    public void setSyncStrategy(QuickbookEntity.SyncStrategy syncStrategy) {
        SyncStrategy = syncStrategy;
    }
}
