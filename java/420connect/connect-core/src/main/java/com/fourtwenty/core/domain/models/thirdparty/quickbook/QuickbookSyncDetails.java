package com.fourtwenty.core.domain.models.thirdparty.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

@CollectionName(name = "quickbook_sync_details",premSyncDown = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickbookSyncDetails extends ShopBaseModel {

    public enum QuickbookEntityType {
        Account,
        Item,
        Customer,
        Vendor,
        PurchaseOrder,
        SalesReceipt,
        RefundReCeipt,
        StatusCheck,
        DeliveryItem,
        RoundOffItem,
        SalesByProduct,
        SalesByConsumer,
        Bill,
        BillPayment,
        Expenses,
        JournalEntry,
        RefundJournalEntry,
        SalesByproductJournalEntry,
        SalesByproductJournalEntryForInvoice,
        SalesByConsumerJournalEntry,
        InventoryAdjustment,
        Invoice,
        PaymentReceived,
        ProductCategoryInventoryAdjustment
    }

    public enum SyncType {
        Push, Pull
    }


    public enum Status {
        Pending,
        Inprogress,
        Completed,
        Fail,
        PartialSuccess

    }

    private QuickbookEntityType quickbookEntityType;
    private int totalRecords;
    private int total_Sync;
    private int total_fail;
    private String quickbook_Company_Id;
    private long startTime;
    private long endTime;
    private Status status;
    private String qbType;
    private String description;
    private SyncType syncType = SyncType.Push;

    public QuickbookEntityType getQuickbookEntityType() {
        return quickbookEntityType;
    }

    public void setQuickbookEntityType(QuickbookEntityType quickbookEntityType) {
        this.quickbookEntityType = quickbookEntityType;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getTotal_Sync() {
        return total_Sync;
    }

    public void setTotal_Sync(int total_Sync) {
        this.total_Sync = total_Sync;
    }

    public int getTotal_fail() {
        return total_fail;
    }

    public void setTotal_fail(int total_fail) {
        this.total_fail = total_fail;
    }

    public String getQuickbook_Company_Id() {
        return quickbook_Company_Id;
    }

    public void setQuickbook_Company_Id(String quickbook_Company_Id) {
        this.quickbook_Company_Id = quickbook_Company_Id;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getQbType() {
        return qbType;
    }

    public void setQbType(String qbType) {
        this.qbType = qbType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SyncType getSyncType() {
        return syncType == null ? SyncType.Push : syncType;
    }

    public void setSyncType(SyncType syncType) {
        this.syncType = syncType;
    }
}
