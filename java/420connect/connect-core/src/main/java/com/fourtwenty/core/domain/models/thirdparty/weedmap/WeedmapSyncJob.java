package com.fourtwenty.core.domain.models.thirdparty.weedmap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@CollectionName(name = "weedmap_sync_jobs", indexes = {"{companyId:1,shopId:1,created:-1}"})
public class WeedmapSyncJob extends ShopBaseModel {
    public enum WmSyncType {
        MANUAL,
        INDIVIDUAL,
        RESET
    }

    public enum WmSyncJobStatus {
        Queued,
        InProgress,
        Error,
        Completed
    }

    private WmSyncType jobType = WmSyncType.MANUAL;
    private WmSyncJobStatus status = WmSyncJobStatus.Queued;
    private String employeeId;
    private long requestTime = 0;
    private Long startTime;
    private Long completeTime;
    private String errorMsg;
    private List<String> productIds = new ArrayList<>();
    private String accountId;

    public WmSyncType getJobType() {
        return jobType;
    }

    public void setJobType(WmSyncType jobType) {
        this.jobType = jobType;
    }

    public WmSyncJobStatus getStatus() {
        return status;
    }

    public void setStatus(WmSyncJobStatus status) {
        this.status = status;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
