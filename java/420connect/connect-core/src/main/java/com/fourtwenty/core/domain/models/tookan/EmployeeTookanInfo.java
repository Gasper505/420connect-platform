package com.fourtwenty.core.domain.models.tookan;


public class EmployeeTookanInfo {

    public enum AgentStatus {
        AVAILABLE(0),
        OFFLINE(1),
        BUSY(2);

        long code;

        AgentStatus(long code) {
            this.code = code;
        }

        public static AgentStatus toAgentStatus(int code) {
            switch (code) {
                case 0:
                    return AgentStatus.AVAILABLE;
                case 1:
                    return AgentStatus.OFFLINE;
                case 2:
                    return AgentStatus.BUSY;
                default:
                    return AgentStatus.OFFLINE;
            }
        }
    }

    private String shopId;
    private String tookanAgentId;
    private AgentStatus status;
    private String teamId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getTookanAgentId() {
        return tookanAgentId;
    }

    public void setTookanAgentId(String tookanAgentId) {
        this.tookanAgentId = tookanAgentId;
    }

    public AgentStatus getStatus() {
        return status;
    }

    public void setStatus(AgentStatus status) {
        this.status = status;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
