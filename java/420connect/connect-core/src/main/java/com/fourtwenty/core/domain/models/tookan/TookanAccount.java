package com.fourtwenty.core.domain.models.tookan;

import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

@CollectionName(name = "tookanAccount", uniqueIndexes = {"{companyId:1,shopId:1,deleted:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanAccount extends ShopBaseModel {
    @NotEmpty
    private String apiKey;
    private boolean active;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
