package com.fourtwenty.core.domain.models.tookan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.TookanFleetInfo;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTeamInfo extends BaseModel {
    public enum BatteryUsage {
        LOW(0),
        MEDIUM(1),
        HIGH(2);
        long code;

        BatteryUsage(long code) {
            this.code = code;
        }

        public static BatteryUsage toBatteryUsage(long code) {
            switch ((int) code) {
                case 0:
                    return LOW;
                case 1:
                    return MEDIUM;
                case 2:
                    return HIGH;
                default:
                    return LOW;
            }
        }
    }

    @JsonProperty("team_id")
    private Long teamId;
    @JsonProperty("team_name")
    private String teamName;
    @JsonProperty("battery_usage")
    private BatteryUsage batteryUsage = BatteryUsage.LOW;
    @JsonProperty("tags")
    private String tags;
    private List<TookanFleetInfo> fleets = new ArrayList<>();

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public BatteryUsage getBatteryUsage() {
        return batteryUsage;
    }

    public void setBatteryUsage(BatteryUsage batteryUsage) {
        this.batteryUsage = batteryUsage;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public List<TookanFleetInfo> getFleets() {
        return fleets;
    }

    public void setFleets(List<TookanFleetInfo> fleets) {
        this.fleets = fleets;
    }
}
