package com.fourtwenty.core.domain.models.tookan;

import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@CollectionName(name = "tookanteams", uniqueIndexes = {"{companyId:1,shopId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTeams extends ShopBaseModel {

    private OnFleetTeams.SyncState syncState;
    private long syncDate;
    private List<TookanTeamInfo> tookanTeamInfo = new ArrayList<>();

    public OnFleetTeams.SyncState getSyncState() {
        return syncState;
    }

    public void setSyncState(OnFleetTeams.SyncState syncState) {
        this.syncState = syncState;
    }

    public long getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(long syncDate) {
        this.syncDate = syncDate;
    }

    public List<TookanTeamInfo> getTookanTeamInfo() {
        return tookanTeamInfo;
    }

    public void setTookanTeamInfo(List<TookanTeamInfo> tookanTeamInfo) {
        this.tookanTeamInfo = tookanTeamInfo;
    }
}
