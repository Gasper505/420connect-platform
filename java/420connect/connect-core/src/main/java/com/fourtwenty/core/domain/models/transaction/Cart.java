package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.purchaseorder.AdjustmentInfo;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalPercentageSerializer;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundOrderItemRequest;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by mdo on 4/21/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart extends CompanyBaseModel {

    public enum PaymentOption {
        Cash,
        Credit,
        Check,
        Hypur,
        Split,
        StoreCredit,
        Linx,
        CashlessATM,
        Mtrac,
        None,
        Clover
    }

    public enum CashStorageLocation {
        CashVault,
        CashDrawer
    }

    public enum PaymentType {
        Full,
        Split
    }


    public enum RefundOption {
        Retail, //NEW SOLUTION
        Void // Deprecated and should not be used. This is the old solution.
    }

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);
    private OrderItem.DiscountType discountType;
    private String promoCode;
    private ConsumerType consumerType = ConsumerType.Other;

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cashReceived = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal changeDue = new BigDecimal(0);
    private PaymentOption paymentOption = PaymentOption.None;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal subTotal = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalDiscount = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal calcCartDiscount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal subTotalDiscount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal total = new BigDecimal(0);

    private CompoundTaxTable taxTable;


    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    private BigDecimal tax = new BigDecimal(0);

    private TaxResult taxResult = new TaxResult();


    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCalcTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPreCalcTax = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal refundAmount = new BigDecimal(0);

    private boolean enableDeliveryFee = false;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryFee = new BigDecimal(0);


    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryDiscount = new BigDecimal(0);
    private OrderItem.DiscountType deliveryDiscountType = OrderItem.DiscountType.Cash;
    private String deliveryPromotionId;

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal afterTaxDiscount = new BigDecimal(0);


    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal appliedAfterTaxDiscount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal roundAmt = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal pointSpent = new BigDecimal(0);

    private List<OrderItem> items = new ArrayList<>();
    private LinkedHashSet<PromotionReq> promotionReqs = new LinkedHashSet<>();
    private LinkedHashSet<PromotionReqLog> promotionReqLogs = new LinkedHashSet<>();

    private SplitPayment splitPayment;

    private PaymentType paymentType = PaymentType.Full;

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalExciseTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalALExciseTax = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal balanceDue = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal creditCardFee = new BigDecimal(0);
    private boolean enableCreditCardFee = Boolean.FALSE;
    private RefundOption refundOption = RefundOption.Void;

    private List<RefundOrderItemRequest> refundOrderItemRequests = new ArrayList<>();

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal requestRefundAmt = new BigDecimal(0);


    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal finalRefundAmt = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal adjustmentAmount = BigDecimal.ZERO;
    private List<AdjustmentInfo> adjustmentInfoList = new ArrayList<>();
    private Shop.TaxRoundOffType taxRoundOffType = Shop.TaxRoundOffType.FIVE_CENT;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal tipAmount = new BigDecimal(0);
    private CompoundTaxTable nonCannabisTaxTable;
    private String cartDiscountNotes;
    private  String afterTaxDiscountNotes;

    @JsonIgnore
    public ConsumerType getFinalConsumerTye() {
        ConsumerType consumerType = this.getConsumerType();

        if (consumerType == null) {
            if (this.getTaxTable() != null) {
                consumerType = this.getTaxTable().getConsumerType();
            }
        }
        if (consumerType == null || consumerType == ConsumerType.Other) {
            consumerType = ConsumerType.AdultUse;
        }
        return consumerType;
    }

    // getters/setters
    public RefundOption getRefundOption() {
        return refundOption;
    }

    public void setRefundOption(RefundOption refundOption) {
        this.refundOption = refundOption;
    }


    public BigDecimal getAfterTaxDiscount() {
        return afterTaxDiscount;
    }

    public void setAfterTaxDiscount(BigDecimal afterTaxDiscount) {
        this.afterTaxDiscount = afterTaxDiscount;
    }

    public BigDecimal getAppliedAfterTaxDiscount() {
        return appliedAfterTaxDiscount;
    }

    public void setAppliedAfterTaxDiscount(BigDecimal appliedAfterTaxDiscount) {
        this.appliedAfterTaxDiscount = appliedAfterTaxDiscount;
    }

    public BigDecimal getRequestRefundAmt() {
        return requestRefundAmt;
    }

    public void setRequestRefundAmt(BigDecimal requestRefundAmt) {
        this.requestRefundAmt = requestRefundAmt;
    }

    public BigDecimal getFinalRefundAmt() {
        return finalRefundAmt;
    }

    public void setFinalRefundAmt(BigDecimal finalRefundAmt) {
        this.finalRefundAmt = finalRefundAmt;
    }

    public TaxResult getTaxResult() {
        return taxResult;
    }

    public void setTaxResult(TaxResult taxResult) {
        this.taxResult = taxResult;
    }

    public CompoundTaxTable getTaxTable() {
        return taxTable;
    }

    public void setTaxTable(CompoundTaxTable taxTable) {
        this.taxTable = taxTable;
    }

    public BigDecimal getDeliveryDiscount() {
        return deliveryDiscount;
    }

    public void setDeliveryDiscount(BigDecimal deliveryDiscount) {
        this.deliveryDiscount = deliveryDiscount;
    }

    public OrderItem.DiscountType getDeliveryDiscountType() {
        return deliveryDiscountType;
    }

    public void setDeliveryDiscountType(OrderItem.DiscountType deliveryDiscountType) {
        this.deliveryDiscountType = deliveryDiscountType;
    }

    public String getDeliveryPromotionId() {
        return deliveryPromotionId;
    }

    public void setDeliveryPromotionId(String deliveryPromotionId) {
        this.deliveryPromotionId = deliveryPromotionId;
    }

    private CashStorageLocation storageLocation = CashStorageLocation.CashDrawer;

    public CashStorageLocation getStorageLocation() {
        return storageLocation;
    }

    public BigDecimal getPointSpent() {
        return pointSpent;
    }

    public void setPointSpent(BigDecimal pointSpent) {
        this.pointSpent = pointSpent;
    }

    public LinkedHashSet<PromotionReqLog> getPromotionReqLogs() {
        return promotionReqLogs;
    }

    public void setPromotionReqLogs(LinkedHashSet<PromotionReqLog> promotionReqLogs) {
        this.promotionReqLogs = promotionReqLogs;
    }

    public BigDecimal getCalcCartDiscount() {
        return calcCartDiscount;
    }

    public void setCalcCartDiscount(BigDecimal calcCartDiscount) {
        this.calcCartDiscount = calcCartDiscount;
    }

    public BigDecimal getTotalCalcTax() {
        return totalCalcTax;
    }

    public void setTotalCalcTax(BigDecimal totalCalcTax) {
        this.totalCalcTax = totalCalcTax;
    }

    public BigDecimal getTotalPreCalcTax() {
        return totalPreCalcTax;
    }

    public void setTotalPreCalcTax(BigDecimal totalPreCalcTax) {
        this.totalPreCalcTax = totalPreCalcTax;
    }

    public void setStorageLocation(CashStorageLocation storageLocation) {
        this.storageLocation = storageLocation;
    }

    public boolean isEnableDeliveryFee() {
        return enableDeliveryFee;
    }

    public void setEnableDeliveryFee(boolean enableDeliveryFee) {
        this.enableDeliveryFee = enableDeliveryFee;
    }

    public BigDecimal getCashReceived() {
        return cashReceived;
    }

    public void setCashReceived(BigDecimal cashReceived) {
        this.cashReceived = cashReceived;
    }

    public BigDecimal getChangeDue() {
        return changeDue;
    }

    public void setChangeDue(BigDecimal changeDue) {
        this.changeDue = changeDue;
    }

    public BigDecimal getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(BigDecimal deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public LinkedHashSet<PromotionReq> getPromotionReqs() {
        return promotionReqs;
    }

    public void setPromotionReqs(LinkedHashSet<PromotionReq> promotionReqs) {
        this.promotionReqs = promotionReqs;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getSubTotalDiscount() {
        return subTotalDiscount;
    }

    public void setSubTotalDiscount(BigDecimal subTotalDiscount) {
        this.subTotalDiscount = subTotalDiscount;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public SplitPayment getSplitPayment() {
        return splitPayment;
    }

    public void setSplitPayment(SplitPayment splitPayment) {
        this.splitPayment = splitPayment;
    }

    public BigDecimal getRoundAmt() {
        return roundAmt;
    }

    public void setRoundAmt(BigDecimal roundAmt) {
        this.roundAmt = roundAmt;
    }

    public BigDecimal getTotalExciseTax() {
        return totalExciseTax;
    }

    public void setTotalExciseTax(BigDecimal totalExciseTax) {
        this.totalExciseTax = totalExciseTax;
    }

    public BigDecimal getTotalALExciseTax() {
        return totalALExciseTax;
    }

    public void setTotalALExciseTax(BigDecimal totalALExciseTax) {
        this.totalALExciseTax = totalALExciseTax;
    }


    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public BigDecimal getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }

    public BigDecimal getCreditCardFee() {
        return creditCardFee;
    }

    public void setCreditCardFee(BigDecimal creditCardFee) {
        this.creditCardFee = creditCardFee;
    }

    public boolean isEnableCreditCardFee() {
        return enableCreditCardFee;
    }

    public void setEnableCreditCardFee(boolean enableCreditCardFee) {
        this.enableCreditCardFee = enableCreditCardFee;
    }

    public List<RefundOrderItemRequest> getRefundOrderItemRequests() {
        return refundOrderItemRequests;
    }

    public void setRefundOrderItemRequests(List<RefundOrderItemRequest> refundOrderItemRequests) {
        this.refundOrderItemRequests = refundOrderItemRequests;
    }

    public BigDecimal getAdjustmentAmount() {
        return adjustmentAmount;
    }

    public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
        this.adjustmentAmount = adjustmentAmount;
    }

    public List<AdjustmentInfo> getAdjustmentInfoList() {
        return adjustmentInfoList;
    }

    public void setAdjustmentInfoList(List<AdjustmentInfo> adjustmentInfoList) {
        this.adjustmentInfoList = adjustmentInfoList;
    }

    public Shop.TaxRoundOffType getTaxRoundOffType() {
        return taxRoundOffType;
    }

    public void setTaxRoundOffType(Shop.TaxRoundOffType taxRoundOffType) {
        this.taxRoundOffType = taxRoundOffType;
    }

    public BigDecimal getTipAmount() {
        return tipAmount;
    }

    public void setTipAmount(BigDecimal tipAmount) {
        this.tipAmount = tipAmount;
    }

    public CompoundTaxTable getNonCannabisTaxTable() {
        return nonCannabisTaxTable;
    }

    public void setNonCannabisTaxTable(CompoundTaxTable nonCannabisTaxTable) {
        this.nonCannabisTaxTable = nonCannabisTaxTable;
    }

    public String getCartDiscountNotes() {
        return cartDiscountNotes;
    }

    public void setCartDiscountNotes(String cartDiscountNotes) {
        this.cartDiscountNotes = cartDiscountNotes;
    }

    public String getAfterTaxDiscountNotes() {
        return afterTaxDiscountNotes;
    }

    public void setAfterTaxDiscountNotes(String afterTaxDiscountNotes) {
        this.afterTaxDiscountNotes = afterTaxDiscountNotes;
    }
}
