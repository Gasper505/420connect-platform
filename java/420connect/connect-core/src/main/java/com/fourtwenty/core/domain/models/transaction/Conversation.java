package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

import java.util.Date;

@CollectionName(name = "conversation", indexes = {"{conversationId:1}", "{transactionId:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conversation extends ShopBaseModel {
    private String transactionId;
    private String employeeId;
    private String toEmployeeId;
    private String memberId;
    private Status status;
    private Sender sender;

    private String message;

    private Date sendRequestDate;
    private Date sendCompleteDate;
    private String sid;
    private ConversationType conversationType = ConversationType.TWILLIO;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSendRequestDate() {
        return sendRequestDate;
    }

    public void setSendRequestDate(Date sendRequestDate) {
        this.sendRequestDate = sendRequestDate;
    }

    public Date getSendCompleteDate() {
        return sendCompleteDate;
    }

    public void setSendCompleteDate(Date sendCompleteDate) {
        this.sendCompleteDate = sendCompleteDate;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public ConversationType getConversationType() {
        return conversationType;
    }

    public void setConversationType(ConversationType conversationType) {
        this.conversationType = conversationType;
    }

    public String getToEmployeeId() {
        return toEmployeeId;
    }

    public void setToEmployeeId(String toEmployeeId) {
        this.toEmployeeId = toEmployeeId;
    }

    public enum Sender {
        EMPLOYEE,
        MEMBER
    }

    public enum Status {
        PENDING,
        SENT,
        FAILED,
        NEW,
        RECEIVED

    }
    public enum ConversationType {
        TWILLIO,
        PUSHER
    }

}
