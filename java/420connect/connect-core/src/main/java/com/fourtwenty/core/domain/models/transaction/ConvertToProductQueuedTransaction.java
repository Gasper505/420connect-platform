package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConvertToProductQueuedTransaction extends QueuedTransaction {

    private BigDecimal quantity;
    private ProductBatchQueuedTransaction.OperationType operationType = ProductBatchQueuedTransaction.OperationType.ConvertToProduct;

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public ProductBatchQueuedTransaction.OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ProductBatchQueuedTransaction.OperationType operationType) {
        this.operationType = operationType;
    }
}
