package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CultivationTaxResult {
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalFlowerTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalLeafTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalFlowerOz = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalLeafOz = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCultivationTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal leafTaxOz = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal flowerTaxOz = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPlantTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal plantTaxOz = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPlantOz = new BigDecimal(0);

    public BigDecimal getTotalFlowerTax() {
        return totalFlowerTax;
    }

    public void setTotalFlowerTax(BigDecimal totalFlowerTax) {
        this.totalFlowerTax = totalFlowerTax;
    }

    public BigDecimal getTotalLeafTax() {
        return totalLeafTax;
    }

    public void setTotalLeafTax(BigDecimal totalLeafTax) {
        this.totalLeafTax = totalLeafTax;
    }

    public BigDecimal getTotalFlowerOz() {
        return totalFlowerOz;
    }

    public void setTotalFlowerOz(BigDecimal totalFlowerOz) {
        this.totalFlowerOz = totalFlowerOz;
    }

    public BigDecimal getTotalLeafOz() {
        return totalLeafOz;
    }

    public void setTotalLeafOz(BigDecimal totalLeafOz) {
        this.totalLeafOz = totalLeafOz;
    }

    public BigDecimal getTotalCultivationTax() {
        return totalCultivationTax;
    }

    public void setTotalCultivationTax(BigDecimal totalCultivationTax) {
        this.totalCultivationTax = totalCultivationTax;
    }

    public BigDecimal getLeafTaxOz() {
        return leafTaxOz;
    }

    public void setLeafTaxOz(BigDecimal leafTaxOz) {
        this.leafTaxOz = leafTaxOz;
    }

    public BigDecimal getFlowerTaxOz() {
        return flowerTaxOz;
    }

    public void setFlowerTaxOz(BigDecimal flowerTaxOz) {
        this.flowerTaxOz = flowerTaxOz;
    }

    public BigDecimal getTotalPlantTax() {
        return totalPlantTax;
    }

    public void setTotalPlantTax(BigDecimal totalPlantTax) {
        this.totalPlantTax = totalPlantTax;
    }

    public BigDecimal getPlantTaxOz() {
        return plantTaxOz;
    }

    public void setPlantTaxOz(BigDecimal plantTaxOz) {
        this.plantTaxOz = plantTaxOz;
    }

    public BigDecimal getTotalPlantOz() {
        return totalPlantOz;
    }

    public void setTotalPlantOz(BigDecimal totalPlantOz) {
        this.totalPlantOz = totalPlantOz;
    }

    @JsonIgnore
    public void addTaxes(CultivationTaxResult cultivationTaxResult) {
        this.totalFlowerTax = this.totalFlowerTax.add(cultivationTaxResult.totalFlowerTax);
        this.totalLeafTax = this.totalLeafTax.add(cultivationTaxResult.totalLeafTax);
        this.totalFlowerOz = this.totalFlowerOz.add(cultivationTaxResult.totalFlowerOz);
        this.totalLeafOz = this.totalLeafOz.add(cultivationTaxResult.totalLeafOz);
        this.totalCultivationTax = this.totalCultivationTax.add(cultivationTaxResult.totalCultivationTax);
        this.leafTaxOz = this.leafTaxOz.add(cultivationTaxResult.leafTaxOz);
        this.flowerTaxOz = this.flowerTaxOz.add(cultivationTaxResult.flowerTaxOz);
        this.totalPlantTax = this.totalPlantTax.add(cultivationTaxResult.totalPlantTax);
        this.plantTaxOz = this.plantTaxOz.add(cultivationTaxResult.plantTaxOz);
        this.totalPlantOz = this.totalPlantOz.add(cultivationTaxResult.totalPlantOz);
    }
}
