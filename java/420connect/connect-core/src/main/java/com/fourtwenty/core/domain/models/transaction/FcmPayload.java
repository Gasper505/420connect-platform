package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FcmPayload {
    public enum Type {
        CHAT,
        TASK,
        ONLINE_ORDER
    }
    public enum SubType {
        UPDATE,
        DELETE,
        UNASSIGNED,
        ADD_CHAT,
        NEW_ORDER
    }
    @JsonProperty("data")
    private FcmNotification message;
    @JsonProperty("to")
    private String token;
    @JsonProperty("notification")
    private Notification notification;

    private FcmPayload(String token, Notification notification) {
        this.token = token;
        this.notification = notification;
        this.message = notification.data;
    }

    public FcmPayload(String message, String sourceId, Type notificationType, String token, String transNo, String title, SubType subType, Conversation conversation) {
        this(token, new Notification(title, new FcmNotification(message, notificationType, sourceId, transNo, title, subType, conversation), message));
    }

    public FcmNotification getMessage() {
        return message;
    }

    public void setMessage(FcmNotification message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public static class FcmNotification {
        private String body;
        private Type type;
        private String sourceId;
        private String transNo;
        private String title;
        private String priority;
        private SubType subType;
        private Conversation conversation;

        public FcmNotification(String body, Type notificationType, String sourceId, String transNo, String title, SubType subType,  Conversation conversation) {
            this.body = body;
            this.type = notificationType;
            this.sourceId = sourceId;
            this.transNo = transNo;
            this.title = title;
            this.priority = "High";
            this.subType = subType;
            this.conversation = conversation;
        }

        public FcmNotification(String body, Type notificationType, SubType subType) {
            this.body = body;
            this.type = notificationType;
            this.priority = "High";
            this.subType = subType;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public String getSourceId() {
            return sourceId;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public String getTransNo() {
            return transNo;
        }

        public void setTransNo(String transNo) {
            this.transNo = transNo;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public SubType getSubType() {
            return subType;
        }
        public void setSubType(SubType subType) {
            this.subType = subType;
        }

        public Conversation getConversation() {
            return conversation;
        }

        public void setConversation(Conversation conversation) {
            this.conversation = conversation;
        }

    }

    public static class Notification {
        private FcmNotification data;
        private String title;
        private String body;

        public Notification(String title, FcmNotification data,  String body) {
            this.title = title;
            this.data = data;
            this.body = body;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public FcmNotification getData() {
            return data;
        }

        public void setData(FcmNotification data) {
            this.data = data;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }

}
