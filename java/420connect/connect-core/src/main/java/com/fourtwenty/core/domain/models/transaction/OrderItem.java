package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.TruncateSixDigitSerialzer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderItem extends CompanyBaseModel {
    public enum OrderItemStatus {
        Active,
        Refunded,
        Exchange
    }

    public enum DiscountType {
        Cash, Percentage
    }

    private String parentId;

    private String orderItemId;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal quantity = new BigDecimal(0);


    @JsonIgnore
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal availableDiscountQty = new BigDecimal(0);

    private int unitQty = 0;
    private boolean useUnitQty = false;
    private ProductWeightTolerance.WeightKey weightKey = ProductWeightTolerance.WeightKey.UNIT;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal origQuantity = new BigDecimal(0);
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal discountedQty = new BigDecimal(0);

    private String remarks;
    private String productId;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal cost = new BigDecimal(0);
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal unitPrice = new BigDecimal(0);
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal discount = new BigDecimal(0);
    private DiscountType discountType = DiscountType.Cash;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal calcDiscount = new BigDecimal(0);

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal finalPrice = new BigDecimal(0);

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal calcTax = new BigDecimal(0);

    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed;
    private TaxInfo taxInfo;
    private TaxInfo.TaxType taxType = TaxInfo.TaxType.Inherit;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal exciseTax = new BigDecimal(0);

    private CompoundTaxTable taxTable;

    private TaxResult taxResult;


    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal calcPreTax = new BigDecimal(0);

    private OrderItemStatus status = OrderItemStatus.Active;
    private List<QuantityLog> quantityLogs = new ArrayList<QuantityLog>();
    private boolean mixMatched = false;
    private boolean ignoreMixMatch = false;
    private LinkedHashSet<PromotionReq> promotionReqs = new LinkedHashSet<>();
    private String prepackageItemId;
    private String batchId;

    private String prepackageId;

    private Prepackage prepackage;
    private PrepackageProductItem prepackageProductItem;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal preparedQty = new BigDecimal(0);

    private boolean fulfilled = false;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal totalRefundAmount;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal totalRefundQty;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal exchangeAmount;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    private BigDecimal exchangeQty;

    @DecimalMin("0.0")
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    private BigDecimal overridePrice = new BigDecimal(0);

    @DecimalMin("0.0")
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    private BigDecimal cogs = new BigDecimal(0);

    private String origOrderItemId;
    private String txnLineID; //QB txn line id for invoice

    // NOTE: DO NOT PERSIST. IT'LL BE REPLACED BY PRODUCT ID.
    private Product product;
    private String batchSku;

    @DecimalMin("0.0")
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    private transient BigDecimal perUnitRefundAmt = new BigDecimal(0);

    private String inventoryId;

    private boolean finalized = false;
    private String finalizedProductId;

    @DecimalMin("0.0")
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    private transient BigDecimal calcCultivationTax = new BigDecimal(0);
    private String discountNotes;

    // Helper methods
    @JsonIgnore
    public QuantityLog getQuantityLogByInventoryId(String inventoryId) {
        if (quantityLogs == null) {
            quantityLogs = new ArrayList<>();
            return null;
        }
        for (QuantityLog log : quantityLogs) {
            if (log.getInventoryId().equalsIgnoreCase(inventoryId)) {
                return log;
            }
        }
        return null;
    }


    public boolean isIgnoreMixMatch() {
        return ignoreMixMatch;
    }

    public void setIgnoreMixMatch(boolean ignoreMixMatch) {
        this.ignoreMixMatch = ignoreMixMatch;
    }

    public void addQuantityLog(String inventoryId, String batchId, double quantity) {
        QuantityLog quantityLog = getQuantityLogByInventoryId(inventoryId);
        if (quantityLog == null) {
            quantityLog = new QuantityLog();
            quantityLog.setId(String.format("%s_%s_%s", orderItemId, inventoryId, batchId));
            quantityLog.setInventoryId(inventoryId);
            quantityLog.setBatchId(batchId);
            quantityLogs.add(quantityLog);
        }
        double prevValue = quantityLog.getQuantity().doubleValue();
        prevValue += quantity;
        quantityLog.setQuantity(new BigDecimal(prevValue)); //prevValue);
    }

    public void addQuantityLog(String inventoryId, String prepackageItemId, String batchId, double quantity) {
        QuantityLog quantityLog = getQuantityLogByInventoryId(inventoryId);
        if (quantityLog == null) {
            quantityLog = new QuantityLog();
            quantityLog.setId(String.format("%s_%s_%s", orderItemId, inventoryId, batchId));
            quantityLog.setInventoryId(inventoryId);
            quantityLog.setPrepackageItemId(prepackageItemId);
            quantityLog.setBatchId(batchId);
            quantityLogs.add(quantityLog);
        }
        double prevValue = quantityLog.getQuantity().doubleValue();
        prevValue += quantity;
        quantityLog.setQuantity(new BigDecimal(prevValue)); //prevValue);
    }

    public void addQuantityLogs(List<QuantityLog> incomingQuantityLogs) {
        this.quantityLogs.addAll(incomingQuantityLogs);
    }

    public void resetQuantityLog() {
        if (quantityLogs == null) {
            quantityLogs = new ArrayList<>();
        }
        quantityLogs.clear();
    }


    public BigDecimal getAvailableDiscountQty() {
        return availableDiscountQty;
    }

    public void setAvailableDiscountQty(BigDecimal availableDiscountQty) {
        this.availableDiscountQty = availableDiscountQty;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public int getUnitQty() {
        return unitQty;
    }

    public void setUnitQty(int unitQty) {
        this.unitQty = unitQty;
    }

    public boolean isUseUnitQty() {
        return useUnitQty;
    }

    public void setUseUnitQty(boolean useUnitQty) {
        this.useUnitQty = useUnitQty;
    }

    public ProductWeightTolerance.WeightKey getWeightKey() {
        return weightKey;
    }

    public void setWeightKey(ProductWeightTolerance.WeightKey weightKey) {
        this.weightKey = weightKey;
    }

    public BigDecimal getOrigQuantity() {
        return origQuantity;
    }

    public void setOrigQuantity(BigDecimal origQuantity) {
        this.origQuantity = origQuantity;
    }

    public BigDecimal getDiscountedQty() {
        return discountedQty;
    }

    public void setDiscountedQty(BigDecimal discountedQty) {
        this.discountedQty = discountedQty;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public BigDecimal getCalcDiscount() {
        return calcDiscount;
    }

    @JsonIgnore
    public boolean hasSomeDiscounts() {
        return getCost().compareTo(getFinalPrice()) > 0;
    }

    public void setCalcDiscount(BigDecimal calcDiscount) {
        this.calcDiscount = calcDiscount;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public BigDecimal getCalcTax() {
        return calcTax;
    }

    public void setCalcTax(BigDecimal calcTax) {
        this.calcTax = calcTax;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public TaxInfo getTaxInfo() {
        return taxInfo;
    }

    public void setTaxInfo(TaxInfo taxInfo) {
        this.taxInfo = taxInfo;
    }

    public TaxInfo.TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxInfo.TaxType taxType) {
        this.taxType = taxType;
    }

    public BigDecimal getExciseTax() {
        return exciseTax;
    }

    public void setExciseTax(BigDecimal exciseTax) {
        this.exciseTax = exciseTax;
    }

    public CompoundTaxTable getTaxTable() {
        return taxTable;
    }

    public void setTaxTable(CompoundTaxTable taxTable) {
        this.taxTable = taxTable;
    }

    public TaxResult getTaxResult() {
        return taxResult;
    }

    public void setTaxResult(TaxResult taxResult) {
        this.taxResult = taxResult;
    }

    public BigDecimal getCalcPreTax() {
        return calcPreTax;
    }

    public void setCalcPreTax(BigDecimal calcPreTax) {
        this.calcPreTax = calcPreTax;
    }

    public OrderItemStatus getStatus() {
        return status;
    }

    public void setStatus(OrderItemStatus status) {
        this.status = status;
    }

    public List<QuantityLog> getQuantityLogs() {
        return quantityLogs;
    }

    public void setQuantityLogs(List<QuantityLog> quantityLogs) {
        this.quantityLogs = quantityLogs;
    }

    public boolean isMixMatched() {
        return mixMatched;
    }

    public void setMixMatched(boolean mixMatched) {
        this.mixMatched = mixMatched;
    }

    public LinkedHashSet<PromotionReq> getPromotionReqs() {
        return promotionReqs;
    }

    public void setPromotionReqs(LinkedHashSet<PromotionReq> promotionReqs) {
        this.promotionReqs = promotionReqs;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getPrepackageId() {
        return prepackageId;
    }

    public void setPrepackageId(String prepackageId) {
        this.prepackageId = prepackageId;
    }

    public Prepackage getPrepackage() {
        return prepackage;
    }

    public void setPrepackage(Prepackage prepackage) {
        this.prepackage = prepackage;
    }

    public PrepackageProductItem getPrepackageProductItem() {
        return prepackageProductItem;
    }

    public void setPrepackageProductItem(PrepackageProductItem prepackageProductItem) {
        this.prepackageProductItem = prepackageProductItem;
    }

    public BigDecimal getPreparedQty() {
        return preparedQty;
    }

    public void setPreparedQty(BigDecimal preparedQty) {
        this.preparedQty = preparedQty;
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public void setFulfilled(boolean fulfilled) {
        this.fulfilled = fulfilled;
    }

    public BigDecimal getTotalRefundAmount() {
        return totalRefundAmount;
    }

    public void setTotalRefundAmount(BigDecimal totalRefundAmount) {
        this.totalRefundAmount = totalRefundAmount;
    }

    public BigDecimal getTotalRefundQty() {
        return totalRefundQty;
    }

    public void setTotalRefundQty(BigDecimal totalRefundQty) {
        this.totalRefundQty = totalRefundQty;
    }

    public BigDecimal getExchangeAmount() {
        return exchangeAmount;
    }

    public void setExchangeAmount(BigDecimal exchangeAmount) {
        this.exchangeAmount = exchangeAmount;
    }

    public BigDecimal getExchangeQty() {
        return exchangeQty;
    }

    public void setExchangeQty(BigDecimal exchangeQty) {
        this.exchangeQty = exchangeQty;
    }

    public BigDecimal getOverridePrice() {
        return overridePrice;
    }

    public void setOverridePrice(BigDecimal overridePrice) {
        this.overridePrice = overridePrice;
    }

    public String getTxnLineID() {
        return txnLineID;
    }

    public void setTxnLineID(String txnLineID) {
        this.txnLineID = txnLineID;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }

    public BigDecimal getPerUnitRefundAmt() {
        return perUnitRefundAmt;
    }

    public void setPerUnitRefundAmt(BigDecimal perUnitRefundAmt) {
        this.perUnitRefundAmt = perUnitRefundAmt;
    }

    public BigDecimal getCogs() {
        return cogs;
    }

    public void setCogs(BigDecimal cogs) {
        this.cogs = cogs;
    }

    public String getOrigOrderItemId() {
        return origOrderItemId;
    }

    public void setOrigOrderItemId(String origOrderItemId) {
        this.origOrderItemId = origOrderItemId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public boolean isFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }

    public String getFinalizedProductId() {
        return finalizedProductId;
    }

    public void setFinalizedProductId(String finalizedProductId) {
        this.finalizedProductId = finalizedProductId;
    }

    public BigDecimal getCalcCultivationTax() {
        return calcCultivationTax;
    }

    public void setCalcCultivationTax(BigDecimal calcCultivationTax) {
        this.calcCultivationTax = calcCultivationTax;
    }
    public String getDiscountNotes() {
        return discountNotes;
    }

    public void setDiscountNotes(String discountNotes) {
        this.discountNotes = discountNotes;
    }
}