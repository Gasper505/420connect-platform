package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.paymentcard.clover.CloverPayRequest;
import com.fourtwenty.core.rest.paymentcard.mtrac.MtracTransactionRequest;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentCardPayment extends BaseModel {
    private PaymentCardPaymentStatus status = PaymentCardPaymentStatus.Pending;
    private long processedTime;

    private PaymentCardType type;
    private String paymentCardNumber;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    @DecimalMin("0")
    private BigDecimal amount;
    private String statusMessage;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    private BigDecimal balance;
    private String paymentId;

    // Clover
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("last4")
    private String accoutLastFourNumber;
    @JsonProperty("expYear")
    private int expYear;
    @JsonProperty("first6")
    private String accoutFirstSixNumber;
    @JsonProperty("cardEncrypted")
    private String cardEncrypted;
    @JsonProperty("expMonth")
    private int expMonth;
    @JsonProperty("cvv")
    private String cvv;

    // Mtrac
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("exp_date")
    private String expDate;


    public PaymentCardType getType() {
        return type;
    }

    public void setType(PaymentCardType type) {
        this.type = type;
    }

    public PaymentCardPaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentCardPaymentStatus status) {
        this.status = status;
    }

    public long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(long processedTime) {
        this.processedTime = processedTime;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

    public void setPaymentCardNumber(String paymentCardNumber) {
        this.paymentCardNumber = paymentCardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccoutLastFourNumber() {
        return accoutLastFourNumber;
    }

    public void setAccoutLastFourNumber(String accoutLastFourNumber) {
        this.accoutLastFourNumber = accoutLastFourNumber;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getAccoutFirstSixNumber() {
        return accoutFirstSixNumber;
    }

    public void setAccoutFirstSixNumber(String accoutFirstSixNumber) {
        this.accoutFirstSixNumber = accoutFirstSixNumber;
    }

    public String getCardEncrypted() {
        return cardEncrypted;
    }

    public void setCardEncrypted(String cardEncrypted) {
        this.cardEncrypted = cardEncrypted;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }


    public void validate() {
        if (type == null) {
            throw new BlazeInvalidArgException("type", "You must specify a card type.");
        }

        if (amount.doubleValue() > 1) {
            throw new BlazeInvalidArgException("amount", "You must specify an amount to charge.");
        }

        if (StringUtils.isBlank(paymentCardNumber)) {
            throw new BlazeInvalidArgException("paymentCardNumber", "You must specify a card number.");
        }
    }

    @Override
    public String toString() {
        return "PaymentCardPayment{" +
                "status=" + status +
                ", processedTime=" + processedTime +
                ", type=" + type +
                ", paymentCardNumber='" + paymentCardNumber + '\'' +
                ", amount=" + amount +
                ", statusMessage='" + statusMessage + '\'' +
                ", balance=" + balance +
                '}';
    }

    public CloverPayRequest cloverPaymentRequest() {
        CloverPayRequest request = new CloverPayRequest();
        request.setAmount(this.getAmount().multiply(new BigDecimal(100)).intValue());
        request.setCardNumber(this.getPaymentCardNumber());
        request.setAccoutFirstSixNumber(this.getAccoutFirstSixNumber());
        request.setAccoutLastFourNumber(this.getAccoutLastFourNumber());
        request.setCurrency(this.getCurrency());
        request.setExpMonth(this.getExpMonth());
        request.setExpYear(this.getExpYear());
        request.setCardEncrypted(this.getCardEncrypted());
        request.setCvv(this.getCvv());
        return request;
    }

    public MtracTransactionRequest mtracTransactionRequest() {
        MtracTransactionRequest request = new MtracTransactionRequest();
        request.setName(this.getFirstName() + " " + this.getLastName());
        request.setFirstName(this.getFirstName());
        request.setLastName(this.getLastName());
        request.setEmail(this.getEmail());
        request.setPhoneNumber(this.getPhoneNumber());
        request.setExpDate(this.getExpDate());
        request.setAmount(this.getAmount());
        request.setCardNumber(this.getPaymentCardNumber());
        request.setCardCvv(this.getCvv());
        return request;
    }
}
