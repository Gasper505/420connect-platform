package com.fourtwenty.core.domain.models.transaction;

public enum PaymentCardPaymentStatus {
    Pending,
    Paid,
    Declined
}
