package com.fourtwenty.core.domain.models.transaction;

import java.math.BigDecimal;

public class PrepackageProductItemQueuedTransaction extends QueuedTransaction {

    private ProductBatchQueuedTransaction.OperationType operationType = ProductBatchQueuedTransaction.OperationType.Add;

    private BigDecimal prepackageItemQuantity = new BigDecimal(0);
    private BigDecimal productQuantity = new BigDecimal(0);
    private String prepackageId;

    public ProductBatchQueuedTransaction.OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ProductBatchQueuedTransaction.OperationType operationType) {
        this.operationType = operationType;
    }

    public BigDecimal getPrepackageItemQuantity() {
        return prepackageItemQuantity;
    }

    public void setPrepackageItemQuantity(BigDecimal prepackageItemQuantity) {
        this.prepackageItemQuantity = prepackageItemQuantity;
    }

    public BigDecimal getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(BigDecimal productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getPrepackageId() {
        return prepackageId;
    }

    public void setPrepackageId(String prepackageId) {
        this.prepackageId = prepackageId;
    }
}
