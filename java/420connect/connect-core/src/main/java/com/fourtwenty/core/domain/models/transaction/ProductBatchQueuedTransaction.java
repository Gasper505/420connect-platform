package com.fourtwenty.core.domain.models.transaction;

import com.fourtwenty.core.domain.models.product.DerivedProductBatchInfo;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.models.product.Product;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductBatchQueuedTransaction extends QueuedTransaction {

    public enum OperationType {
        Add,
        Update,
        ReducePrepackage,
        DeletePrepackage,
        DeletePrepackageItem,
        AddTestSample,
        DeleteTestSample,
        UpdateTestSample,
        ConvertToProduct
    }



    private OperationType operationType = OperationType.Add;
    private BigDecimal quantity = BigDecimal.ZERO;
    private String sourceChildId; // for test sample ids etc.
    private InventoryOperation.SubSourceAction subSourceAction;
    private Map<String, List<DerivedProductBatchInfo>> productMap;
    private Product.ProductType productType = Product.ProductType.REGULAR;
    private boolean updateBatchQuantity = Boolean.FALSE; //check for derived product
    private HashMap<String, Map<String, BigDecimal>> batchQuantityMap = new HashMap<>();
    private HashMap<String, Double> bundleQuantityMap = new HashMap<>();
    private String derivedInventoryId; // inventory for deducting quantity for derived product
    private boolean inventoryUpdate;
    private String previousInventoryId;
    private BigDecimal previousQuantity = BigDecimal.ZERO;

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getSourceChildId() {
        return sourceChildId;
    }

    public void setSourceChildId(String sourceChildId) {
        this.sourceChildId = sourceChildId;
    }

    public InventoryOperation.SubSourceAction getSubSourceAction() {
        return subSourceAction;
    }

    public void setSubSourceAction(InventoryOperation.SubSourceAction subSourceAction) {
        this.subSourceAction = subSourceAction;
    }

    public Map<String, List<DerivedProductBatchInfo>> getProductMap() {
        return productMap;
    }

    public void setProductMap(Map<String, List<DerivedProductBatchInfo>> productMap) {
        this.productMap = productMap;
    }

    public Product.ProductType getProductType() {
        return productType;
    }

    public void setProductType(Product.ProductType productType) {
        this.productType = productType;
    }

    public boolean isUpdateBatchQuantity() {
        return updateBatchQuantity;
    }

    public void setUpdateBatchQuantity(boolean updateBatchQuantity) {
        this.updateBatchQuantity = updateBatchQuantity;
    }

    public HashMap<String, Map<String, BigDecimal>> getBatchQuantityMap() {
        return batchQuantityMap;
    }

    public void setBatchQuantityMap(HashMap<String, Map<String, BigDecimal>> batchQuantityMap) {
        this.batchQuantityMap = batchQuantityMap;
    }

    public HashMap<String, Double> getBundleQuantityMap() {
        return bundleQuantityMap;
    }

    public void setBundleQuantityMap(HashMap<String, Double> bundleQuantityMap) {
        this.bundleQuantityMap = bundleQuantityMap;
    }

    public String getDerivedInventoryId() {
        return derivedInventoryId;
    }

    public void setDerivedInventoryId(String derivedInventoryId) {
        this.derivedInventoryId = derivedInventoryId;
    }

    public boolean isInventoryUpdate() {
        return inventoryUpdate;
    }

    public void setInventoryUpdate(boolean inventoryUpdate) {
        this.inventoryUpdate = inventoryUpdate;
    }

    public String getPreviousInventoryId() {
        return previousInventoryId;
    }

    public void setPreviousInventoryId(String previousInventoryId) {
        this.previousInventoryId = previousInventoryId;
    }

    public BigDecimal getPreviousQuantity() {
        return previousQuantity;
    }

    public void setPreviousQuantity(BigDecimal previousQuantity) {
        this.previousQuantity = previousQuantity;
    }
}
