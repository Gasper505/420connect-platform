package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

/**
 * Created by mdo on 11/21/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuantityLog {
    private String id;
    private String inventoryId;
    private BigDecimal quantity = new BigDecimal(0);
    private String prepackageItemId;
    private String batchId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
