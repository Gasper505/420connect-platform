package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * This purpose of this model is to hold major transactional changes to the Transaction.
 * <p>
 * Major transactional changes are updating the cart (voiding, completing, holding, etc.)
 * <p>
 * Updating the cart will in turn update the related Product Quantity
 * <p>
 * Created by mdo on 4/30/16.
 */
@CollectionName(name = "queued_transactions",premSyncDown = false,indexes = {"{transactionId:1}", "{companyId:1,shopId:1,transactionId:1,status:1,modified:-1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueuedTransaction extends ShopBaseModel {
    public enum QueueStatus {
        Pending,
        Completed,
        Prepare,
        Error
    }

    public enum QueuedTransactionType {
        SalesTrans, // Regular sales transactions
        RefundRequest,
        InventoryTransfer,
        InventoryReconciliation,
        PurchaseOrder,
        ShippingManifest,
        ProductBatch,
        Prepackage,
        StockReset,
        Product,
        BulkProductBatch,
        RevertShippingManifest,
        RejectShippingManifest
    }

    private String transactionId; //Indicates source id (transaction id, purchase order id etc.)
    private String sellerId;
    private String memberId;
    private String terminalId;
    private Cart cart;
    private Transaction.TransactionStatus pendingStatus = Transaction.TransactionStatus.Queued;
    private QueueStatus status = QueueStatus.Pending;
    private long requestTime = 0;
    private boolean reassignTransfer = Boolean.FALSE;
    private String newTransferInventoryId;
    private QueuedTransactionType queuedTransType = QueuedTransactionType.SalesTrans;
    private String invoiceId;
    private String shippingManifestId;
    private String inventoryId; //Inventory in which quantity needs to add or deduct
    private String reconcilliationHistoryId;

    public QueuedTransactionType getQueuedTransType() {
        return queuedTransType;
    }

    public void setQueuedTransType(QueuedTransactionType queuedTransType) {
        this.queuedTransType = queuedTransType;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public Transaction.TransactionStatus getPendingStatus() {
        return pendingStatus;
    }

    public void setPendingStatus(Transaction.TransactionStatus pendingStatus) {
        this.pendingStatus = pendingStatus;
    }

    public QueueStatus getStatus() {
        return status;
    }

    public void setStatus(QueueStatus status) {
        this.status = status;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public boolean isReassignTransfer() {
        return reassignTransfer;
    }

    public void setReassignTransfer(boolean reassignTransfer) {
        this.reassignTransfer = reassignTransfer;
    }

    public String getNewTransferInventoryId() {
        return newTransferInventoryId;
    }

    public void setNewTransferInventoryId(String newTransferInventoryId) {
        this.newTransferInventoryId = newTransferInventoryId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getShippingManifestId() {
        return shippingManifestId;
    }

    public void setShippingManifestId(String shippingManifestId) {
        this.shippingManifestId = shippingManifestId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getReconcilliationHistoryId() {
        return reconcilliationHistoryId;
    }

    public void setReconcilliationHistoryId(String reconcilliationHistoryId) {
        this.reconcilliationHistoryId = reconcilliationHistoryId;
    }
}
