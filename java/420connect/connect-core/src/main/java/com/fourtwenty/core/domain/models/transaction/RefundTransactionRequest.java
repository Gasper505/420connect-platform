package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundOrderItemRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundRequest;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 6/12/18.
 */
@CollectionName(name = "refund_requests", indexes = {"{companyId:1,_id:1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundTransactionRequest extends ShopBaseModel {
    public enum RefundType {
        Refund,
        Exchange
    }

    public enum RefundStatus {
        Pending,
        Completed,
        Canceled
    }

    private String transactionId;
    private boolean withInventory;
    private String note;
    private Cart.PaymentOption refundAs;
    private List<Double> loc = new ArrayList<>(); // Location in [lon,lat] // array format
    private List<RefundOrderItemRequest> refundOrderItemRequests = new ArrayList<>();
    @Deprecated
    private List<String> refundOrders = new ArrayList<>(); // deprecated
    private RefundType refundType = RefundType.Refund;
    private RefundStatus status = RefundStatus.Pending;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalRefundAmt = BigDecimal.ZERO;
    private RefundRequest.RefundVersion refundVersion = RefundRequest.RefundVersion.OLD;

    public RefundStatus getStatus() {
        return status;
    }

    public void setStatus(RefundStatus status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public List<Double> getLoc() {
        return loc;
    }

    public void setLoc(List<Double> loc) {
        this.loc = loc;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Cart.PaymentOption getRefundAs() {
        return refundAs;
    }

    public void setRefundAs(Cart.PaymentOption refundAs) {
        this.refundAs = refundAs;
    }

    public List<RefundOrderItemRequest> getRefundOrderItemRequests() {
        return refundOrderItemRequests;
    }

    public void setRefundOrderItemRequests(List<RefundOrderItemRequest> refundOrderItemRequests) {
        this.refundOrderItemRequests = refundOrderItemRequests;
    }

    public List<String> getRefundOrders() {
        return refundOrders;
    }

    public void setRefundOrders(List<String> refundOrders) {
        this.refundOrders = refundOrders;
    }

    public RefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundType refundType) {
        this.refundType = refundType;
    }

    public boolean isWithInventory() {
        return withInventory;
    }

    public void setWithInventory(boolean withInventory) {
        this.withInventory = withInventory;
    }

    public BigDecimal getTotalRefundAmt() {
        return totalRefundAmt;
    }

    public void setTotalRefundAmt(BigDecimal totalRefundAmt) {
        this.totalRefundAmt = totalRefundAmt;
    }

    public RefundRequest.RefundVersion getRefundVersion() {
        return refundVersion;
    }

    public void setRefundVersion(RefundRequest.RefundVersion refundVersion) {
        this.refundVersion = refundVersion;
    }
}
