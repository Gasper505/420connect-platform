package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by decipher on 30/9/17 10:09 AM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SplitPayment extends CompanyBaseModel {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cashAmt = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal creditDebitAmt = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal checkAmt = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal storeCreditAmt = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal cashlessAmt = new BigDecimal(0);

    public BigDecimal getTotalSplits() {
        cashAmt = (cashAmt == null) ? BigDecimal.ZERO : cashAmt;
        creditDebitAmt = (creditDebitAmt == null) ? BigDecimal.ZERO : creditDebitAmt;
        checkAmt = (checkAmt == null) ? BigDecimal.ZERO : checkAmt;
        storeCreditAmt = (storeCreditAmt == null) ? BigDecimal.ZERO : storeCreditAmt;
        cashlessAmt = (cashlessAmt == null) ? BigDecimal.ZERO : cashlessAmt;

        return cashAmt.add(creditDebitAmt).add(checkAmt).add(storeCreditAmt).add(cashlessAmt);
    }

    public BigDecimal getCashAmt() {
        return cashAmt;
    }

    public void setCashAmt(BigDecimal cashAmt) {
        this.cashAmt = cashAmt;
    }

    public BigDecimal getCheckAmt() {
        return checkAmt;
    }

    public void setCheckAmt(BigDecimal checkAmt) {
        this.checkAmt = checkAmt;
    }

    public BigDecimal getCreditDebitAmt() {
        return creditDebitAmt;
    }

    public void setCreditDebitAmt(BigDecimal creditDebitAmt) {
        this.creditDebitAmt = creditDebitAmt;
    }

    public BigDecimal getStoreCreditAmt() {
        return storeCreditAmt;
    }

    public void setStoreCreditAmt(BigDecimal storeCreditAmt) {
        this.storeCreditAmt = storeCreditAmt;
    }

    public BigDecimal getCashlessAmt() {
        return cashlessAmt;
    }

    public void setCashlessAmt(BigDecimal cashlessAmt) {
        this.cashlessAmt = cashlessAmt;
    }
}
