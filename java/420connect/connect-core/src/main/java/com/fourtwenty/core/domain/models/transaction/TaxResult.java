package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 1/21/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxResult extends BaseModel {
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPreCalcTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPostCalcTax = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCityTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCountyTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalStateTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalFedTax = new BigDecimal(0);


    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCityPreTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCountyPreTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalStatePreTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalFedPreTax = new BigDecimal(0);


    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalExciseTax = new BigDecimal(0); // This is POST TAX FOR NAL

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalNALPreExciseTax = new BigDecimal(0);


    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalALExciseTax = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @DecimalMin("0")
    private BigDecimal totalALPostExciseTax = new BigDecimal(0);

    private CultivationTaxResult cultivationTaxResult;

    public void reset() {
        this.totalPreCalcTax = new BigDecimal(0);
        this.totalPostCalcTax = new BigDecimal(0);
        this.totalCityTax = new BigDecimal(0);
        this.totalCountyTax = new BigDecimal(0);
        this.totalStateTax = new BigDecimal(0);
        this.totalFedTax = new BigDecimal(0);
        this.totalCityPreTax = new BigDecimal(0);
        this.totalCountyPreTax = new BigDecimal(0);
        this.totalStatePreTax = new BigDecimal(0);
        this.totalFedPreTax = new BigDecimal(0);
        this.totalExciseTax = new BigDecimal(0);
        this.totalNALPreExciseTax = new BigDecimal(0);
        this.totalALExciseTax = new BigDecimal(0);
        this.totalALPostExciseTax = new BigDecimal(0);
        this.cultivationTaxResult = new CultivationTaxResult();
    }


    public BigDecimal getTotalCityPreTax() {
        return totalCityPreTax;
    }

    public void setTotalCityPreTax(BigDecimal totalCityPreTax) {
        this.totalCityPreTax = totalCityPreTax;
    }

    public BigDecimal getTotalCountyPreTax() {
        return totalCountyPreTax;
    }

    public void setTotalCountyPreTax(BigDecimal totalCountyPreTax) {
        this.totalCountyPreTax = totalCountyPreTax;
    }

    public BigDecimal getTotalFedPreTax() {
        return totalFedPreTax;
    }

    public void setTotalFedPreTax(BigDecimal totalFedPreTax) {
        this.totalFedPreTax = totalFedPreTax;
    }

    public BigDecimal getTotalStatePreTax() {
        return totalStatePreTax;
    }

    public void setTotalStatePreTax(BigDecimal totalStatePreTax) {
        this.totalStatePreTax = totalStatePreTax;
    }

    public BigDecimal getTotalALPostExciseTax() {
        return totalALPostExciseTax;
    }

    public void setTotalALPostExciseTax(BigDecimal totalALPostExciseTax) {
        this.totalALPostExciseTax = totalALPostExciseTax;
    }

    public BigDecimal getTotalALExciseTax() {
        return totalALExciseTax;
    }

    @JsonIgnore
    public BigDecimal getOrderItemPreALExciseTax() {
        if (this.totalALPostExciseTax == null || this.getTotalALPostExciseTax().doubleValue() == 0) {
            return this.totalALExciseTax;
        }
        return new BigDecimal(0);
    }

    public BigDecimal getTotalNALPreExciseTax() {
        return totalNALPreExciseTax;
    }

    public void setTotalNALPreExciseTax(BigDecimal totalNALPreExciseTax) {
        this.totalNALPreExciseTax = totalNALPreExciseTax;
    }

    public void setTotalALExciseTax(BigDecimal totalALExciseTax) {
        this.totalALExciseTax = totalALExciseTax;
    }

    public BigDecimal getTotalCityTax() {
        return totalCityTax;
    }

    public void setTotalCityTax(BigDecimal totalCityTax) {
        this.totalCityTax = totalCityTax;
    }

    public BigDecimal getTotalCountyTax() {
        return totalCountyTax;
    }

    public void setTotalCountyTax(BigDecimal totalCountyTax) {
        this.totalCountyTax = totalCountyTax;
    }

    public BigDecimal getTotalFedTax() {
        return totalFedTax;
    }

    public void setTotalFedTax(BigDecimal totalFedTax) {
        this.totalFedTax = totalFedTax;
    }

    public BigDecimal getTotalPostCalcTax() {
        return totalPostCalcTax;
    }

    public void setTotalPostCalcTax(BigDecimal totalPostCalcTax) {
        this.totalPostCalcTax = totalPostCalcTax;
    }

    public BigDecimal getTotalPreCalcTax() {
        return totalPreCalcTax;
    }

    public void setTotalPreCalcTax(BigDecimal totalPreCalcTax) {
        this.totalPreCalcTax = totalPreCalcTax;
    }

    public BigDecimal getTotalStateTax() {
        return totalStateTax;
    }

    public void setTotalStateTax(BigDecimal totalStateTax) {
        this.totalStateTax = totalStateTax;
    }

    public BigDecimal getTotalExciseTax() {
        return totalExciseTax;
    }

    public void setTotalExciseTax(BigDecimal totalExciseTax) {
        this.totalExciseTax = totalExciseTax;
    }

    public CultivationTaxResult getCultivationTaxResult() {
        return cultivationTaxResult;
    }

    public void setCultivationTaxResult(CultivationTaxResult cultivationTaxResult) {
        this.cultivationTaxResult = cultivationTaxResult;
    }

    @JsonIgnore
    public void addTaxes(TaxResult taxResult) {
        this.totalPreCalcTax = this.totalPreCalcTax.add(taxResult.totalPreCalcTax);
        this.totalPostCalcTax = this.totalPostCalcTax.add(taxResult.totalPostCalcTax);
        this.totalCityTax = this.totalCityTax.add(taxResult.totalCityTax);
        this.totalCountyTax = this.totalCountyTax.add(taxResult.totalCountyTax);
        this.totalStateTax = this.totalStateTax.add(taxResult.totalStateTax);
        this.totalFedTax = this.totalFedTax.add(taxResult.totalFedTax);
        this.totalCityPreTax = this.totalCityPreTax.add(taxResult.totalCityPreTax);
        this.totalCountyPreTax = this.totalCountyPreTax.add(taxResult.totalCountyPreTax);
        this.totalStatePreTax = this.totalStatePreTax.add(taxResult.totalStatePreTax);
        this.totalFedPreTax = this.totalFedPreTax.add(taxResult.totalFedPreTax);
        this.totalExciseTax = this.totalExciseTax.add(taxResult.totalExciseTax);
        this.totalNALPreExciseTax = this.totalNALPreExciseTax.add(taxResult.totalNALPreExciseTax);
        this.totalALExciseTax = this.totalALExciseTax.add(taxResult.totalALExciseTax);
        this.totalALPostExciseTax = this.totalALPostExciseTax.add(taxResult.totalALPostExciseTax);
        this.cultivationTaxResult.addTaxes(taxResult.getCultivationTaxResult());

    }
}