package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkInventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundRequest;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponseHit;
import org.json.JSONObject;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@CollectionName(name = "transactions", premSyncDown = false, indexes = {"{companyId:1,shopId:1}", "{shopId:1,memberId:1,active:1}", "{companyId:1,shopId:1,status:1,processedTime:-1}", "{companyId:1,shopId:1,transType:1,status:1,processedTime:-1}", "{companyId:1,transNo:-1}"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction extends ShopBaseModel implements ElasticSearchCapable, Transactionable {
    public static final String UNIQUE_SEQ_PRIORITY = "TransactionPriority";

    public enum TransactionStatus {
        Queued,
        Hold,
        InProgress,
        Completed,
        Canceled,
        RefundWithInventory,
        RefundWithoutInventory,
        Void
    }

    public enum TransactionType {
        Sale, Refund, Adjustment, Transfer, Exchange, ReturnToVendor, ShippingManifest
    }

    public enum QueueType {
        WalkIn, Delivery, Online, None, Special
    }

    public enum FulfillmentStep {
        Prepare, Fulfill, ReadyForDelivery, OutForDelivery
    }

    public enum OnFleetTaskStatus {
        TASK_ARRIVAL, TASK_COMPLETED, TASK_FAILED
    }

    public enum TraceSubmissionStatus {
        None,
        SubmissionError,
        SubmissionPending,
        SubmissionInProgress,
        SubmissionWaitingID,
        Completed,
        Deleted,
    }

    public enum TransactionRefundStatus {
        None, In_Progress, Complete_Refund, Partial_Refund
    }

    private Long metrcId;
    private String metrcSaleTime = null;
    private TraceSubmissionStatus traceSubmitStatus = TraceSubmissionStatus.None;
    private String traceMessage = null;

    private String transNo;
    private String parentTransactionId;
    private long priority;
    private String createdById;
    private String sellerId;
    private String memberId;
    private String terminalId;
    private String sellerTerminalId;
    private String overrideInventoryId;
    private Long checkinTime;
    private Long startTime;
    private Long endTime;
    private boolean active = false;
    private QueueType queueType = QueueType.None;
    private TransactionStatus status = TransactionStatus.Queued;
    private Note note;
    private String memo;
    private Note deleteNote;
    private Cart cart;
    private Long processedTime;
    private boolean paid = false;
    private Long paidTime;
    private Long completedTime;

    private List<Double> loc = new ArrayList<>(); // Location in [lon,lat] // array format
    private TransactionType transType = TransactionType.Sale;
    private String assignedEmployeeId;
    private String timeZone;
    private Address deliveryAddress;
    private boolean locked = false;
    private String hypurPin;
    private String hypurCheckinISN;
    private String entityISN;
    private FulfillmentStep fulfillmentStep = FulfillmentStep.Prepare;
    private Shop.ShopCheckoutType checkoutType = Shop.ShopCheckoutType.Direct;
    private Long startRouteDate;
    private boolean routing = false;
    private List<Double> startRouteLocation = new ArrayList<>(); // Location in [lon, lat] in array format
    private Long endRouteDate;
    private List<Double> endRouteLocation = new ArrayList<>(); // Location in [lon, lat] in array format
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal pointsEarned = new BigDecimal(0);

    private CompanyAsset memberSignature;

    private String transferShopId;
    private BulkInventoryTransferRequest transferRequest;

    private String consumerCartId;
    private String source;
    private int eta = 0;
    private ConsumerCart.ConsumerTrackingStatus trackingStatus = ConsumerCart.ConsumerTrackingStatus.NotStarted;
    private boolean mileageCalculated = Boolean.FALSE;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal mileage = new BigDecimal(0);

    private List<PaymentCardPayment> paymentCardPayments = new ArrayList<>();

    // Do not save this member & member
    @JsonIgnore
    private Shop shop;
    private Member member;
    private Employee seller;
    private Terminal sellerTerminal;
    private Employee assignedEmployee;
    private String preparedBy;
    private Long preparedDate;
    private String packedBy;
    private Long packedDate;
    private Long pickUpDate;

    private String onFleetTaskId;
    private Boolean createOnfleetTask = Boolean.FALSE;
    //OnFleet task's status :
    // 0 Unassigned: task has not yet been assigned to a worker.
    // 1 Assigned: task has been assigned to a worker.
    // 2 Active: task has been started by its assigned worker.
    // 3 Completed: task has been completed by its assigned worker.
    private int state;
    private String shortId;
    private OnFleetTaskStatus onFleetTaskStatus;
    private String qbSalesReceiptRef;
    private String qbRefundReceipt;
    private String qbJournalEntryRef;

    private TransactionRefundStatus transactionRefundStatus = TransactionRefundStatus.None;
    private boolean assigned;
    private String tookanTaskId;
    private boolean isCreateTookanTask;

    private Set<String> orderTags = new HashSet<>();

    private String txnId;
    private String qbDesktopSalesRef;
    private String qbDesktopJournalEntryRef;
    private String qbDesktopRefundReceipt;

    private boolean qbErrored;
    private long errorTime;
    private boolean qbRefundErrored;
    private long errorRefundTime;

    private boolean refundPayment;
    private boolean refundPaymentErrored;
    private long refundPaymentErrorTime;

    private String mtracTxnId;


    private RefundRequest.RefundVersion refundVersion;
    private long deliveryDate;
    private Long completeAfter;
    private boolean addressUpdate;
    private String paymentEditedBy;
    private long  paymentEditedTime;

    public Long getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(Long completeAfter) {
        this.completeAfter = completeAfter;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Long getPaidTime() {
        return paidTime;
    }

    public Long getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(Long completedTime) {
        this.completedTime = completedTime;
    }

    public void setPaidTime(Long paidTime) {
        this.paidTime = paidTime;
    }

    public String getTraceMessage() {
        return traceMessage;
    }

    public void setTraceMessage(String traceMessage) {
        this.traceMessage = traceMessage;
    }

    public String getQbJournalEntryRef() {
        return qbJournalEntryRef;
    }

    public void setQbJournalEntryRef(String qbJournalEntryRef) {
        this.qbJournalEntryRef = qbJournalEntryRef;
    }

    public boolean isPreparingFulfillment() {
        if ((this.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment || this.getCheckoutType() == Shop.ShopCheckoutType.FulfillmentThreeStep)
                && this.getFulfillmentStep() == FulfillmentStep.Prepare) {
            return true;
        }
        return false;
    }

    public boolean isFulfillingFulfillment() {
        if (this.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment
                && this.getFulfillmentStep() == FulfillmentStep.Fulfill) {
            return true;
        }
        return false;
    }


    public TraceSubmissionStatus getTraceSubmitStatus() {
        return traceSubmitStatus;
    }

    public void setTraceSubmitStatus(TraceSubmissionStatus traceSubmitStatus) {
        this.traceSubmitStatus = traceSubmitStatus;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public String getOverrideInventoryId() {
        return overrideInventoryId;
    }

    public void setOverrideInventoryId(String overrideInventoryId) {
        this.overrideInventoryId = overrideInventoryId;
    }

    public Long getMetrcId() {
        return metrcId;
    }

    public void setMetrcId(Long metrcId) {
        this.metrcId = metrcId;
    }

    public String getMetrcSaleTime() {
        return metrcSaleTime;
    }

    public void setMetrcSaleTime(String metrcSaleTime) {
        this.metrcSaleTime = metrcSaleTime;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public ConsumerCart.ConsumerTrackingStatus getTrackingStatus() {
        return trackingStatus;
    }

    public void setTrackingStatus(ConsumerCart.ConsumerTrackingStatus trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    public CompanyAsset getMemberSignature() {
        return memberSignature;
    }

    public void setMemberSignature(CompanyAsset memberSignature) {
        this.memberSignature = memberSignature;
    }

    public BigDecimal getPointsEarned() {
        return pointsEarned;
    }

    public void setPointsEarned(BigDecimal pointsEarned) {
        this.pointsEarned = pointsEarned;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getConsumerCartId() {
        return consumerCartId;
    }

    public void setConsumerCartId(String consumerCartId) {
        this.consumerCartId = consumerCartId;
    }

    public BulkInventoryTransferRequest getTransferRequest() {
        return transferRequest;
    }

    public void setTransferRequest(BulkInventoryTransferRequest transferRequest) {
        this.transferRequest = transferRequest;
    }

    public String getTransferShopId() {
        return transferShopId;
    }

    public void setTransferShopId(String transferShopId) {
        this.transferShopId = transferShopId;
    }

    public Employee getAssignedEmployee() {
        return assignedEmployee;
    }

    public void setAssignedEmployee(Employee assignedEmployee) {
        this.assignedEmployee = assignedEmployee;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getAssignedEmployeeId() {
        return assignedEmployeeId;
    }

    public void setAssignedEmployeeId(String assignedEmployeeId) {
        this.assignedEmployeeId = assignedEmployeeId;
    }

    public Terminal getSellerTerminal() {
        return sellerTerminal;
    }

    public void setSellerTerminal(Terminal sellerTerminal) {
        this.sellerTerminal = sellerTerminal;
    }

    public Employee getSeller() {
        return seller;
    }

    public void setSeller(Employee seller) {
        this.seller = seller;
    }

    public TransactionType getTransType() {
        return transType;
    }

    public void setTransType(TransactionType transType) {
        this.transType = transType;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public String getParentTransactionId() {
        return parentTransactionId;
    }

    public void setParentTransactionId(String parentTransactionId) {
        this.parentTransactionId = parentTransactionId;
    }

    public List<Double> getLoc() {
        return loc;
    }

    public void setLoc(List<Double> loc) {
        this.loc = loc;
    }

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
        if (this.processedTime != null && this.processedTime > 0) {
            if (this.completedTime != null && this.completedTime == 0) {
                this.completedTime = this.processedTime;
            }
        }
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Long getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(Long checkinTime) {
        this.checkinTime = checkinTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(QueueType queueType) {
        this.queueType = queueType;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerTerminalId() {
        return sellerTerminalId;
    }

    public void setSellerTerminalId(String sellerTerminalId) {
        this.sellerTerminalId = sellerTerminalId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getHypurPin() {
        return hypurPin;
    }

    public void setHypurPin(String hypurPin) {
        this.hypurPin = hypurPin;
    }

    public String getHypurCheckinISN() {
        return hypurCheckinISN;
    }

    public void setHypurCheckinISN(String hypurCheckinISN) {
        this.hypurCheckinISN = hypurCheckinISN;
    }

    public String getEntityISN() {
        return entityISN;
    }

    public void setEntityISN(String entityISN) {
        this.entityISN = entityISN;
    }

    public FulfillmentStep getFulfillmentStep() {
        return fulfillmentStep;
    }

    public void setFulfillmentStep(FulfillmentStep fulfillmentStep) {
        this.fulfillmentStep = fulfillmentStep;
    }

    public Shop.ShopCheckoutType getCheckoutType() {
        return checkoutType;
    }

    public void setCheckoutType(Shop.ShopCheckoutType checkoutType) {
        this.checkoutType = checkoutType;
    }

    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }


    public Long getStartRouteDate() {
        return startRouteDate;
    }

    public void setStartRouteDate(Long startRouteDate) {
        this.startRouteDate = startRouteDate;
    }

    public List<Double> getStartRouteLocation() {
        return startRouteLocation;
    }

    public void setStartRouteLocation(List<Double> startRouteLocation) {
        this.startRouteLocation = startRouteLocation;
    }

    public Long getEndRouteDate() {
        return endRouteDate;
    }

    public void setEndRouteDate(Long endRouteDate) {
        this.endRouteDate = endRouteDate;
    }

    public List<Double> getEndRouteLocation() {
        return endRouteLocation;
    }

    public void setEndRouteLocation(List<Double> endRouteLocation) {
        this.endRouteLocation = endRouteLocation;
    }

    public Long getPreparedDate() {
        return preparedDate;
    }

    public void setPreparedDate(Long preparedDate) {
        this.preparedDate = preparedDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public boolean isRouting() {
        return routing;
    }

    public void setRouting(boolean routing) {
        this.routing = routing;
    }


    public Note getDeleteNote() {
        return deleteNote;
    }

    public void setDeleteNote(Note deleteNote) {
        this.deleteNote = deleteNote;
    }

    public boolean isMileageCalculated() {
        return mileageCalculated;
    }

    public void setMileageCalculated(boolean mileageCalculated) {
        this.mileageCalculated = mileageCalculated;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

    public String getOnFleetTaskId() {
        return onFleetTaskId;
    }

    public void setOnFleetTaskId(String onFleetTaskId) {
        this.onFleetTaskId = onFleetTaskId;
    }

    public Long getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(Long pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public Boolean getCreateOnfleetTask() {
        return createOnfleetTask;
    }

    public void setCreateOnfleetTask(Boolean createOnfleetTask) {
        this.createOnfleetTask = createOnfleetTask;
    }

    public OnFleetTaskStatus getOnFleetTaskStatus() {
        return onFleetTaskStatus;
    }

    public void setOnFleetTaskStatus(OnFleetTaskStatus onFleetTaskStatus) {
        this.onFleetTaskStatus = onFleetTaskStatus;
    }

    public String getQbSalesReceiptRef() {
        return qbSalesReceiptRef;
    }

    public void setQbSalesReceiptRef(String qbSalesReceiptRef) {
        this.qbSalesReceiptRef = qbSalesReceiptRef;
    }

    public String getQbRefundReceipt() {
        return qbRefundReceipt;
    }

    public void setQbRefundReceipt(String qbRefundReceipt) {
        this.qbRefundReceipt = qbRefundReceipt;
    }

    public TransactionRefundStatus getTransactionRefundStatus() {
        return transactionRefundStatus;
    }

    public void setTransactionRefundStatus(TransactionRefundStatus transactionRefundStatus) {
        this.transactionRefundStatus = transactionRefundStatus;
    }

    public String getPaymentEditedBy() {
        return paymentEditedBy;
    }

    public void setPaymentEditedBy(String paymentEditedBy) {
        this.paymentEditedBy = paymentEditedBy;
    }

    public long getPaymentEditedTime() {
        return paymentEditedTime;
    }

    public void setPaymentEditedTime(long paymentEditedTime) {
        this.paymentEditedTime = paymentEditedTime;
    }
    public List<Cart.PaymentOption> getRefundPaymentOptions() {
        List<Cart.PaymentOption> options = new ArrayList<>();

        if (getCart() == null) return options;

        Cart.PaymentOption paymentOption = getCart().getPaymentOption();

        // Include original payment method as refund option
        switch (paymentOption) {
            case Cash:
            case Check:
            case Credit:
            case CashlessATM:
            case Mtrac:
            case Clover:
                options.add(Cart.PaymentOption.Cash);
                options.add(paymentOption);
                break;
        }

        // Payment methods that offer store credit refunds
        switch (paymentOption) {
            case Cash:
            case Check:
            case Credit:
            case Hypur:
            case Split:
            case Linx:
            case StoreCredit:
            case CashlessATM:
                options.add(Cart.PaymentOption.StoreCredit);
                break;
        }
        return options;
    }

    public List<PaymentCardPayment> getPaymentCardPayments() {
        return paymentCardPayments;
    }

    public void addPaymentCardPayment(PaymentCardPayment paymentCardPayment) {
        paymentCardPayments.add(paymentCardPayment);
    }

    public void addPaymentCardPayments(List<PaymentCardPayment> paymentCardPayments) {
        this.paymentCardPayments.addAll(paymentCardPayments);
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public String getTookanTaskId() {
        return tookanTaskId;
    }

    public void setTookanTaskId(String tookanTaskId) {
        this.tookanTaskId = tookanTaskId;
    }

    public boolean isCreateTookanTask() {
        return isCreateTookanTask;
    }

    public void setCreateTookanTask(boolean createTookanTask) {
        isCreateTookanTask = createTookanTask;
    }


    public String getPackedBy() {
        return packedBy;
    }

    public void setPackedBy(String packedBy) {
        this.packedBy = packedBy;
    }

    public Long getPackedDate() {
        return packedDate;
    }

    public void setPackedDate(Long packedDate) {
        this.packedDate = packedDate;
    }



    public boolean isPayingWithPaymentCard() {
        if (getCart() == null) return false;

        switch (getCart().getPaymentOption()) {
            case Clover:
            case Linx:
            case Mtrac:
                return true;
        }
        return false;
    }



    // Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }


    ///
    /// Elastic Search Support
    ///

    @Override
    public ElasticSearchIndex getElasticSearchIndex() {
        ElasticSearchIndex index = new ElasticSearchIndex();
        index.setIndex("transactions");
        index.setType("transaction");

        final Map<String, FieldIndex> properties = new HashMap<>();
        properties.put("companyId", new FieldIndex("keyword", null, true));
        properties.put("shopId", new FieldIndex("keyword", null, true));
        properties.put("transNo", new FieldIndex("keyword", null, true));
        properties.put("queueType", new FieldIndex("keyword", null, true));
        properties.put("transType", new FieldIndex("keyword", null, true));
        properties.put("terminalName", new FieldIndex("keyword", null, true));
        properties.put("sellerFirstName", new FieldIndex("keyword", null, true));
        properties.put("sellerLastName", new FieldIndex("keyword", null, true));
        properties.put("memberFirstName", new FieldIndex("keyword", null, true));
        properties.put("memberLastName", new FieldIndex("keyword", null, true));
        properties.put("total", new FieldIndex("keyword", null, true));
        properties.put("deleted", new FieldIndex("keyword", null, true));
        index.setProperties(properties);

        return index;
    }

    public Set<String> getOrderTags() {
        return orderTags;
    }

    public void setOrderTags(Set<String> orderTags) {
        this.orderTags = orderTags;
    }

    public RefundRequest.RefundVersion getRefundVersion() {
        return refundVersion;
    }

    public void setRefundVersion(RefundRequest.RefundVersion refundVersion) {
        this.refundVersion = refundVersion;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getQbDesktopSalesRef() {
        return qbDesktopSalesRef;
    }

    public void setQbDesktopSalesRef(String qbDesktopSalesRef) {
        this.qbDesktopSalesRef = qbDesktopSalesRef;
    }

    public String getQbDesktopJournalEntryRef() {
        return qbDesktopJournalEntryRef;
    }

    public void setQbDesktopJournalEntryRef(String qbDesktopJournalEntryRef) {
        this.qbDesktopJournalEntryRef = qbDesktopJournalEntryRef;
    }

    public String getQbDesktopRefundReceipt() {
        return qbDesktopRefundReceipt;
    }

    public void setQbDesktopRefundReceipt(String qbDesktopRefundReceipt) {
        this.qbDesktopRefundReceipt = qbDesktopRefundReceipt;
    }

    public boolean isQbRefundErrored() {
        return qbRefundErrored;
    }

    public void setQbRefundErrored(boolean qbRefundErrored) {
        this.qbRefundErrored = qbRefundErrored;
    }

    public long getErrorRefundTime() {
        return errorRefundTime;
    }

    public void setErrorRefundTime(long errorRefundTime) {
        this.errorRefundTime = errorRefundTime;
    }

    public boolean isRefundPayment() {
        return refundPayment;
    }

    public void setRefundPayment(boolean refundPayment) {
        this.refundPayment = refundPayment;
    }

    public boolean isRefundPaymentErrored() {
        return refundPaymentErrored;
    }

    public void setRefundPaymentErrored(boolean refundPaymentErrored) {
        this.refundPaymentErrored = refundPaymentErrored;
    }

    public long getRefundPaymentErrorTime() {
        return refundPaymentErrorTime;
    }

    public void setRefundPaymentErrorTime(long refundPaymentErrorTime) {
        this.refundPaymentErrorTime = refundPaymentErrorTime;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getMtracTxnId() {
        return mtracTxnId;
    }

    public void setMtracTxnId(String mtracTxnId) {
        this.mtracTxnId = mtracTxnId;
    }

    public boolean isAddressUpdate() {
        return addressUpdate;
    }

    public void setAddressUpdate(boolean addressUpdate) {
        this.addressUpdate = addressUpdate;
    }

    @Override
    public void loadFrom(AWSSearchResponseHit hit) {
        Employee seller = new Employee();
        Member member = new Member();

        for (String key : hit.source.keySet()) {
            switch (key) {
                case "companyId":
                    setCompanyId(hit.source.get(key).toString());
                    break;
                case "shopId":
                    setShopId(hit.source.get(key).toString());
                    break;
                case "transNo":
                    setTransNo(hit.source.get(key).toString());
                    break;
                case "queueType":
                    setQueueType(QueueType.valueOf(hit.source.get(key).toString()));
                    break;
                case "transType":
                    setTransType(TransactionType.valueOf(hit.source.get(key).toString()));
                    break;
                case "terminalName":
                    Terminal terminal = new Terminal();
                    terminal.setName(hit.source.get(key).toString());
                    setSellerTerminal(terminal);
                    break;
                case "sellerFirstName":
                    seller.setFirstName(hit.source.get(key).toString());
                    break;
                case "sellerLastName":
                    seller.setLastName(hit.source.get(key).toString());
                    break;
                case "memberFirstName":
                    member.setFirstName(hit.source.get(key).toString());
                    break;
                case "memberLastName":
                    member.setLastName(hit.source.get(key).toString());
                    break;
                case "total":
                    Cart cart = new Cart();
                    cart.setTotal(new BigDecimal(hit.source.get(key).toString()));
                    setCart(cart);
                    break;
            }
        }

        setSeller(seller);
        setMember(member);
    }

    @Override
    public JSONObject toElasticSearchObject() {
        final JSONObject jsonObject = new JSONObject();

        jsonObject.put("companyId", getCompanyId());
        jsonObject.put("shopId", getShopId());
        jsonObject.put("transNo", getTransNo());
        jsonObject.put("transNo", getTransNo());
        jsonObject.put("queueType", getQueueType());
        jsonObject.put("transType", getTransType());
        jsonObject.put("terminalName", getSellerTerminal() != null ? getSellerTerminal().getName() : "");
        jsonObject.put("sellerFirstName", getSeller() != null ? getSeller().getFirstName() : "");
        jsonObject.put("sellerLastName", getSeller() != null ? getSeller().getLastName() : "");
        jsonObject.put("memberFirstName", getMember() != null ? getMember().getFirstName() : "");
        jsonObject.put("memberLastName", getMember() != null ? getMember().getLastName() : "");
        jsonObject.put("deleted", isDeleted());

        return jsonObject;
    }
}
