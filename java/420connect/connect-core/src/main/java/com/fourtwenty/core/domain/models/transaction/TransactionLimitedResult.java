package com.fourtwenty.core.domain.models.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.Address;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionLimitedResult {
    private String id;
    private String shopId;
    private String companyId;
    private String transNo;
    private Transaction.TransactionStatus status = Transaction.TransactionStatus.Queued;
    private Long pickUpDate;
    private Transaction.QueueType queueType = Transaction.QueueType.None;
    private String sellerId;
    private String terminalId;
    private String sellerTerminalId;
    private Transaction.TransactionType transType = Transaction.TransactionType.Sale;
    private Transaction.TraceSubmissionStatus traceSubmitStatus = Transaction.TraceSubmissionStatus.None;
    private Long checkinTime;
    private Long startTime;
    private Long endTime;
    private boolean active = false;
    private Long processedTime;
    private boolean paid = false;
    private Long paidTime;
    private String memo;
    private String overrideInventoryId;
    private Address deliveryAddress;
    private long deliveryDate;
    private String memberId;
    private String assignedEmployeeId;
    private Cart cart;
    private String preparedBy;
    private Long preparedDate;

    private String consumerCartId;


    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSellerTerminalId() {
        return sellerTerminalId;
    }

    public void setSellerTerminalId(String sellerTerminalId) {
        this.sellerTerminalId = sellerTerminalId;
    }

    public Transaction.TransactionType getTransType() {
        return transType;
    }

    public void setTransType(Transaction.TransactionType transType) {
        this.transType = transType;
    }

    public Transaction.TraceSubmissionStatus getTraceSubmitStatus() {
        return traceSubmitStatus;
    }

    public void setTraceSubmitStatus(Transaction.TraceSubmissionStatus traceSubmitStatus) {
        this.traceSubmitStatus = traceSubmitStatus;
    }

    public Long getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(Long checkinTime) {
        this.checkinTime = checkinTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Long getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(Long paidTime) {
        this.paidTime = paidTime;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getOverrideInventoryId() {
        return overrideInventoryId;
    }

    public void setOverrideInventoryId(String overrideInventoryId) {
        this.overrideInventoryId = overrideInventoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public Transaction.TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(Transaction.TransactionStatus status) {
        this.status = status;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(Long pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public Transaction.QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(Transaction.QueueType queueType) {
        this.queueType = queueType;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAssignedEmployeeId() {
        return assignedEmployeeId;
    }

    public void setAssignedEmployeeId(String assignedEmployeeId) {
        this.assignedEmployeeId = assignedEmployeeId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    public Long getPreparedDate() {
        return preparedDate;
    }

    public void setPreparedDate(Long preparedDate) {
        this.preparedDate = preparedDate;
    }


    public String getConsumerCartId() {
        return consumerCartId;
    }

    public void setConsumerCartId(String consumerCartId) {
        this.consumerCartId = consumerCartId;
    }
}
