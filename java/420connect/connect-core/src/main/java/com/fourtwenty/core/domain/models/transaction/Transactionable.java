package com.fourtwenty.core.domain.models.transaction;

/**
 * Purpose : marks a class Transactionable, this is so that platform know that
 * it can go through FIFO and can be processed further, and might result in inventory changes.
 */
public interface Transactionable {
}
