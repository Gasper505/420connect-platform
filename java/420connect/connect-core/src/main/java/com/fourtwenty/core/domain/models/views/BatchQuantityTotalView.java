package com.fourtwenty.core.domain.models.views;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 10/12/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchQuantityTotalView {
    @JsonProperty("_id")
    private String id;
    private double total;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
