package com.fourtwenty.core.domain.models.views;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponseHit;
import org.json.JSONObject;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 3/20/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberLimitedView extends ShopBaseModel implements ElasticSearchCapable {
    private String firstName;
    private String lastName;
    private String primaryPhone;
    private String email;
    private String memberGroupName = "";
    private boolean medical;
    private String licenseNumber;
    private List<String> expStatuses = new ArrayList<>();
    private Long startDate;
    private Member.MembershipStatus status;

    public MemberLimitedView() {
    }

    public MemberLimitedView(String id) {
        setId(id);
    }

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal loyaltyPoints = new BigDecimal(0);

    private boolean ban = false;
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private boolean recommendationExpired;
    private long recommendationExpiryLeft;

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getMemberGroupName() {
        return memberGroupName;
    }

    public void setMemberGroupName(String memberGroupName) {
        this.memberGroupName = memberGroupName;
    }

    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(BigDecimal loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getExpStatuses() {
        return expStatuses;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public boolean isMedical() {
        return medical;
    }

    public void setMedical(boolean medical) {
        this.medical = medical;
    }

    public void setExpStatuses(List<String> expStatuses) {
        this.expStatuses = expStatuses;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Member.MembershipStatus getStatus() {
        return status;
    }

    public void setStatus(Member.MembershipStatus status) {
        this.status = status;
    }

    public boolean isBan() {
        return ban;
    }

    public void setBan(boolean ban) {
        this.ban = ban;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public boolean isRecommendationExpired() {
        return recommendationExpired;
    }

    public void setRecommendationExpired(boolean recommendationExpired) {
        this.recommendationExpired = recommendationExpired;
    }

    public long getRecommendationExpiryLeft() {
        return recommendationExpiryLeft;
    }

    public void setRecommendationExpiryLeft(long recommendationExpiryLeft) {
        this.recommendationExpiryLeft = recommendationExpiryLeft;
    }

    @Override
    public ElasticSearchIndex getElasticSearchIndex() {
        return null;
    }

    @Override
    public void loadFrom(AWSSearchResponseHit hit) {
        for (String key : hit.source.keySet()) {
            switch (key) {
                case "companyId":
                    setCompanyId(hit.source.get(key).toString());
                    break;
                case "shopId":
                    setShopId(hit.source.get(key).toString());
                    break;
                case "firstName":
                    setFirstName(hit.source.get(key).toString());
                    break;
                case "lastName":
                    setLastName(hit.source.get(key).toString());
                    break;
                case "primaryPhone":
                    setPrimaryPhone(hit.source.get(key).toString());
                    break;
                case "email":
                    setEmail(hit.source.get(key).toString());
                    break;
                case "license":
                    setLicenseNumber(hit.source.get(key).toString());
                    break;
                case "memberGroup":
                    setMemberGroupName(hit.source.get(key).toString());
                    break;
                case "startDate":
                    setStartDate(new Long(hit.source.get(key).toString()));
                    break;
                case "status":
                    setStatus(Member.MembershipStatus.valueOf(hit.source.get(key).toString()));
                    break;
                case "banPatient":
                    setBan(new Boolean(hit.source.get(key).toString()));
                    break;
                case "consumerType":
                    setConsumerType(ConsumerType.valueOf(hit.source.get(key).toString()));
                    break;
                case "recommendationExpired":
                    setRecommendationExpired(new Boolean(hit.source.get(key).toString()));
                    break;
                case "recommendationExpiryLeft":
                    setRecommendationExpiryLeft(new Long(hit.source.get(key).toString()));
                    break;
            }
        }
    }

    @Override
    public JSONObject toElasticSearchObject() {
        return null;
    }

}
