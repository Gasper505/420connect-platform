package com.fourtwenty.core.domain.models.views;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.serializers.DobDeserializer;
import com.fourtwenty.core.domain.serializers.DobSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberLimitedViewResult extends MemberLimitedView {
    private CompanyAsset frontPhoto;
    private Long lastVisitDate;
    private boolean inQueue;
    private long activeTransaction;
    private boolean agreementExpired;
    @JsonSerialize(using = DobSerializer.class)
    @JsonDeserialize(using = DobDeserializer.class)
    private Long dob;
    private List<Identification> identifications = new ArrayList<>();

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public List<Identification> getIdentifications() {
        return identifications;
    }

    public void setIdentifications(List<Identification> identifications) {
        this.identifications = identifications;
    }

    public CompanyAsset getFrontPhoto() {
        return frontPhoto;
    }

    public void setFrontPhoto(CompanyAsset frontPhoto) {
        this.frontPhoto = frontPhoto;
    }

    public Long getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Long lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public boolean isInQueue() {
        return inQueue;
    }

    public void setInQueue(boolean inQueue) {
        this.inQueue = inQueue;
    }

    public long getActiveTransaction() {
        return activeTransaction;
    }

    public void setActiveTransaction(long activeTransaction) {
        this.activeTransaction = activeTransaction;
    }

    public boolean isAgreementExpired() {
        return agreementExpired;
    }

    public void setAgreementExpired(boolean agreementExpired) {
        this.agreementExpired = agreementExpired;
    }
}
