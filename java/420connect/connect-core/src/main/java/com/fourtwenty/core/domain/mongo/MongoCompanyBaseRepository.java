package com.fourtwenty.core.domain.mongo;

import com.fourtwenty.core.caching.annotations.CacheInvalidate;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/31/16.
 */
public interface MongoCompanyBaseRepository<T extends CompanyBaseModel> extends BaseRepository<T> {
    Iterable<T> listBefore(long beforeDate);
    Iterable<T> list(String companyId);

    <E extends CompanyBaseModel> Iterable<E> list(String companyId, Class<E> clazz);

    Iterable<T> listSort(String companyId, String sortOption);

    Iterable<T> listWithOptions(String companyId, String projections);

    HashMap<String, T> listAsMap(String companyId);

    Iterable<T> listAll(String companyId);

    HashMap<String, T> listAllAsMap(String companyId);

    Iterable<T> findItemsIn(String companyId, List<ObjectId> ids);

    <E extends CompanyBaseModel> Iterable<E> findItemsIn(String companyId, List<ObjectId> ids, Class<E> clazz);

    Iterable<T> findItemsIn(String companyId, List<ObjectId> ids, String projection);

    HashMap<String, T> findItemsInAsMap(String companyId, List<ObjectId> ids);

    Iterable<T> listWithDate(String companyId, long afterDate, long beforeDate);

    Iterable<T> listWithDateWithOptions(String companyId, long afterDate, long beforeDate, String projections);

    Iterable<T> list(String companyId, List<ObjectId> ids);

    HashMap<String, T> listAsMap(String companyId, List<ObjectId> ids);

    T get(String companyId, String id);

    T get(String companyId, String id, String projection);

    <E extends CompanyBaseModel> E get(String companyid, String id, Class<E> clazz);

    long count(String companyId);

    @CacheInvalidate
    T update(String companyId, String entityId, T pojo);

    @CacheInvalidate
    void removeById(String companyId, String entityId);

    @CacheInvalidate
    void removeByIdSetState(String companyId, String entityId);

    @CacheInvalidate
    void removeAllSetState(String companyId);

    DateSearchResult<T> findItemsAfter(String companyId, long afterDate);
    DateSearchResult<T> findItemsAfterLimit(String companyId, long afterDate, int limit);

    DateSearchResult<T> findItemsAfterNonDeleted(String companyId, long afterDate);

    DateSearchResult<T> findItemsWithDateNonDeleted(String companyId, long afterDate, long beforeDate);

    DateSearchResult<T> findItemsWithDate(String companyId, long afterDate, long beforeDate);

    <E extends CompanyBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, long afterDate, long beforeDate, Class<E> clazz);

    DateSearchResult<T> findItemsWithDate(String companyId, long afterDate, long beforeDate, String projections);

    SearchResult<T> findItems(String companyId, int skip, int limit);

    <E extends CompanyBaseModel> SearchResult<E> findItems(String companyId, int skip, int limit, Class<E> clazz);

    SearchResult<T> findItemsWithSort(String companyId, String sort, int skip, int limit);

    SearchResult<T> findItemsWithSort(String companyId, List<ObjectId> ids, String sort, int skip, int limit);

    SearchResult<T> findItems(String companyId, int skip, int limit, String projections);

    <E extends T> SearchResult<E> findItems(String companyId, int skip, int limit, String projections, Class<E> clazz);

    Long countItemsIn(String companyId, List<ObjectId> documentIds);

    DateSearchResult<T> findItemsWithDateAndLimit(String companyId, long afterDate, long beforeDate, int start, int limit);
}
