package com.fourtwenty.core.domain.mongo;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/31/16.
 */
public interface MongoShopBaseRepository<T extends ShopBaseModel> extends MongoCompanyBaseRepository<T> {

    void removeAllSetState(String companyId, String shopId);
    T getLastModified(String companyId, String shopId);


    long countItemsIn(String companyId, String shopId, List<ObjectId> ids);

    Iterable<T> listByShop(String companyId, String shopId);

    Iterable<T> listAllByShop(String companyId, String shopId);

    Iterable<T> listAllByShopActive(String companyId, String shopId);

    Iterable<T> listByShopWithDates(String companyId, String shopId, long afterDate, long beforeDate);
    Iterable<T> listByShopWithDatesCreate(String companyId, String shopId, long afterDate, long beforeDate);

    DateSearchResult<T> findItemsWithDateNonDeleted(String companyId, String shopId, long afterDate, long beforeDate);

    long count(String companyId, String shopId);

    HashMap<String, T> listAsMap(String companyId, String shopId);

    HashMap<String, T> listAllAsMap(String companyId, String shopId);

    HashMap<String, T> findItemsInAsMap(String companyId, String shopId, List<ObjectId> ids);


    Iterable<T> findItemsIn(String companyId, String shopId, List<ObjectId> ids);

    Iterable<T> findItemsIn(String companyId, String shopId, List<ObjectId> ids, String projection);

    Iterable<T> listByShopSort(String companyId, String shopId, String sortOptions, int skip, int limit);

    Iterable<T> listByShop(String companyId, String shopId, int skip, int limit);

    Iterable<T> listByShopWithDate(String companyId, String shopId, long afterDate, long beforeDate);

    Iterable<T> listByShopWithDateSort(String companyId, String shopId, String sortOptions, long afterDate, long beforeDate);
    Iterable<T> listByShopWithCreatedDateSort(String companyId, String shopId, String sortOptions, long afterDate, long beforeDate);

    Iterable<T> listByShop(String companyId, String shopId, String sortField);

    Iterable<T> listAllByShop(String companyId, String shopId, String sortField);

    Iterable<T> listByShopSort(String companyId, String shopId, String sortOptions);

    T getByShopAndId(String companyId, String shopId, String entityId);

    DateSearchResult<T> findItemsAfter(String companyId, String shopId, long afterDate);
    DateSearchResult<T> findItemsAfterLimit(String companyId, String shopId, long afterDate, int limit);

    DateSearchResult<T> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate);

    SearchResult<T> findItems(String companyId, String shopId, int skip, int limit);

    SearchResult<T> findActiveItems(String companyId, String shopId, int skip, int limit);

    SearchResult<T> findItems(String companyId, String shopId, String sortOptions, int skip, int limit);

    SearchResult<T> findItems(String companyId, String shopId, String sortOptions, boolean active, int skip, int limit);

    SearchResult<T> findItems(String companyId, String shopId, String sortOptions, String projections, int skip, int limit);

    <E extends ShopBaseModel> SearchResult<E> findItems(String companyId, String shopId, String sortOptions, int skip, int limit, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> findItems(String companyId, String shopId, String sortOptions, String projections, int skip, int limit, Class<E> clazz);


    <E extends ShopBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> findItems(String companyId, String shopId, int skip, int limit, Class<E> clazz);

    DateSearchResult<T> findItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit);
    DateSearchResult<T> findItemsWithDateAndLimitSorted(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit);

}
