package com.fourtwenty.core.domain.mongo.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/31/16.
 */
public class CompanyBaseRepositoryImpl<T extends CompanyBaseModel> extends MongoBaseRepositoryImpl<T> implements MongoCompanyBaseRepository<T> {
    public CompanyBaseRepositoryImpl(Class<T> clazz, MongoDb mongoManager) throws Exception {
        super(clazz, mongoManager);
    }

    @Override
    public T get(String companyId, String id) {
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }
        return coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(id)).as(entityClazz);
    }

    @Override
    public T get(String companyId, String id, String projection) {
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }
        return coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(id)).projection(projection).as(entityClazz);
    }

    @Override
    public <E extends CompanyBaseModel> E get(String companyId, String id, Class<E> clazz) {
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }
        return coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(id)).as(clazz);
    }

    @Override
    public Iterable<T> list(String companyId) {
        return coll.find("{companyId:#,deleted:false}", companyId).as(entityClazz);
    }

    @Override
    public Iterable<T> listBefore(long beforeDate) {
        return coll.find("{modified:{$lt:#}}", beforeDate).as(entityClazz);
    }

    @Override
    public <E extends CompanyBaseModel> Iterable<E> list(String companyId, Class<E> clazz) {
        return coll.find("{companyId:#,deleted:false}", companyId).as(clazz);
    }

    @Override
    public Iterable<T> listSort(String companyId, String sortOption) {
        return coll.find("{companyId:#,deleted:false}", companyId).sort(sortOption).as(entityClazz);
    }

    @Override
    public Iterable<T> listWithOptions(String companyId, String projections) {
        return coll.find("{companyId:#,deleted:false}", companyId).projection(projections).as(entityClazz);
    }

    @Override
    public Iterable<T> listWithDate(String companyId, long afterDate, long beforeDate) {
        return coll.find("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate).as(entityClazz);
    }

    @Override
    public Iterable<T> listWithDateWithOptions(String companyId, long afterDate, long beforeDate, String projections) {
        return coll.find("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate).projection(projections).as(entityClazz);
    }


    @Override
    public HashMap<String, T> listAsMap(String companyId) {
        Iterable<T> items = list(companyId);
        return asMap(items);
    }

    @Override
    public Iterable<T> listAll(String companyId) {
        return coll.find("{companyId:#}", companyId).as(entityClazz);
    }

    @Override
    public HashMap<String, T> listAllAsMap(String companyId) {
        Iterable<T> items = listAll(companyId);
        return asMap(items);
    }

    @Override
    public Iterable<T> list(String companyId, List<ObjectId> ids) {
        return coll.find("{companyId:#,_id : {$in: #}}", companyId, ids).as(entityClazz);
    }

    @Override
    public HashMap<String, T> listAsMap(String companyId, List<ObjectId> ids) {
        return asMap(list(companyId, ids));
    }

    @Override
    public Iterable<T> findItemsIn(String companyId, List<ObjectId> ids) {
        return coll.find("{companyId:#,_id:{$in:#}}", companyId, ids).as(entityClazz);
    }

    @Override
    public <E extends CompanyBaseModel> Iterable<E> findItemsIn(String companyId, List<ObjectId> ids, Class<E> clazz) {
        return coll.find("{companyId:#,_id:{$in:#}}", companyId, ids).as(clazz);
    }

    @Override
    public Iterable<T> findItemsIn(String companyId, List<ObjectId> ids, String projection) {
        return coll.find("{companyId:#,_id:{$in:#}}", companyId, ids).projection(projection).as(entityClazz);
    }

    @Override
    public HashMap<String, T> findItemsInAsMap(String companyId, List<ObjectId> ids) {
        Iterable<T> items = findItemsIn(companyId, ids);
        return asMap(items);
    }

    @Override
    public T update(String companyId, String entityId, T pojo) {
        pojo.setModified(DateTime.now().getMillis());
        coll.update("{companyId:#,_id: #}", companyId, new ObjectId(entityId)).with(pojo);
        return pojo;
    }

    @Override
    public long count(String companyId) {
        return coll.count("{companyId:#,deleted:false}", companyId);
    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public void removeByIdSetState(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public void removeAllSetState(String companyId) {
        coll.update("{companyId:#}", companyId).multi().with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public DateSearchResult<T> findItemsWithDateNonDeleted(String companyId, long afterDate, long beforeDate) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<T> items = coll.find("{companyId:#, modified:{$lt:#, $gt:#},deleted:false}", companyId, beforeDate, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$lt:#, $gt:#},deleted:false}", companyId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsAfterNonDeleted(String companyId, long afterDate) {
        if (afterDate < 0) afterDate = 0;
        Iterable<T> items = coll.find("{companyId:#, modified:{$gt:#},deleted:false}", companyId, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$gt:#},deleted:false}", companyId, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsAfter(String companyId, long afterDate) {
        if (afterDate < 0) afterDate = 0;
        Iterable<T> items = coll.find("{companyId:#, modified:{$gt:#}}", companyId, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$gt:#}}", companyId, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsAfterLimit(String companyId, long afterDate, int limit) {
        if (afterDate < 0) afterDate = 0;
        Iterable<T> items = coll.find("{companyId:#, modified:{$gt:#}}", companyId, afterDate).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$gt:#}}", companyId, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsWithDate(String companyId, long afterDate, long beforeDate) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<T> items = coll.find("{companyId:#, modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends CompanyBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, long afterDate, long beforeDate, Class<E> clazz) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<E> items = coll.find("{companyId:#, modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate).as(clazz);

        long count = coll.count("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsWithDate(String companyId, long afterDate, long beforeDate, String projections) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<T> items = coll.find("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate).projection(projections).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItems(String companyId, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,deleted:false}", companyId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends CompanyBaseModel> SearchResult<E> findItems(String companyId, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,deleted:false}", companyId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItemsWithSort(String companyId, String sort, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,deleted:false}", companyId).sort(sort).skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,deleted:false}", companyId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<T> findItemsWithSort(String companyId, List<ObjectId> ids, String sort, int skip, int limit) {

        Iterable<T> items = coll.find("{companyId:#,_id:{$in:#}}", companyId, ids).sort(sort).skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,_id:{$in:#}}", companyId, ids);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItems(String companyId, int skip, int limit, String projections) {
        Iterable<T> items = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).projection(projections).as(entityClazz);

        long count = coll.count("{companyId:#,deleted:false}", companyId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends T> SearchResult<E> findItems(String companyId, int skip, int limit, String projections, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).projection(projections).as(clazz);

        long count = coll.count("{companyId:#,deleted:false}", companyId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Long countItemsIn(String companyId, List<ObjectId> ids) {
        return coll.count("{companyId:#,_id:{$in:#}}", companyId, ids);
    }

    @Override
    public DateSearchResult<T> findItemsWithDateAndLimit(String companyId, long afterDate, long beforeDate, int start, int limit) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        Iterable<T> items = coll.find("{companyId:#, modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,modified:{$lt:#, $gt:#}}", companyId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }
}
