package com.fourtwenty.core.domain.mongo.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.restfb.util.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/31/16.
 */
public class ShopBaseRepositoryImpl<T extends ShopBaseModel> extends CompanyBaseRepositoryImpl<T> implements MongoShopBaseRepository<T> {
    public ShopBaseRepositoryImpl(Class<T> clazz, MongoDb mongoManager) throws Exception {
        super(clazz, mongoManager);
    }

    @Override
    public void removeAllSetState(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#}", companyId, shopId).multi().with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public T getLastModified(String companyId, String shopId) {
        Iterable<T> items =  coll.find("{companyId:#,shopId:#}",companyId,shopId).sort("{modified:-1}").limit(1).as(entityClazz);
        for (T item : items) {
            return item;
        }
        return null;
    }

    @Override
    public T getByShopAndId(String companyId, String shopId, String entityId) {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(shopId) || StringUtils.isBlank(entityId)) {
            return null;
        }
        return coll.findOne("{companyId: #,shopId:#,_id:#}", companyId, shopId, new ObjectId(entityId)).as(entityClazz);
    }

    @Override
    public long count(String companyId, String shopId) {
        return coll.count("{companyId: #,shopId:#,deleted:false}", companyId, shopId);
    }

    @Override
    public long countItemsIn(String companyId, String shopId, List<ObjectId> ids) {
        return coll.count("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, ids);
    }

    @Override
    public Iterable<T> listByShop(String companyId, String shopId) {
        return coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).as(entityClazz);
    }

    @Override
    public Iterable<T> listByShop(String companyId, String shopId, int skip, int limit) {
        return coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).skip(skip).limit(limit).as(entityClazz);
    }

    @Override
    public Iterable<T> listByShopWithDate(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<T> listByShopWithDateSort(String companyId, String shopId, String sortOptions, long afterDate, long beforeDate) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort(sortOptions).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<T> listByShopWithCreatedDateSort(String companyId, String shopId, String sortOptions, long afterDate, long beforeDate) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#, created:{$lt:#, $gt:#}, deleted:false}", companyId, shopId, beforeDate, afterDate).sort(sortOptions).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<T> listByShop(String companyId, String shopId, String sortField) {
        return coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).sort("{" + sortField + ":1}").as(entityClazz);
    }

    @Override
    public Iterable<T> listByShopSort(String companyId, String shopId, String sortOptions) {
        return coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).sort(sortOptions).as(entityClazz);
    }

    @Override
    public Iterable<T> listByShopSort(String companyId, String shopId, String sortOptions, int skip, int limit) {
        return coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).sort(sortOptions).as(entityClazz);
    }

    @Override
    public Iterable<T> listAllByShop(String companyId, String shopId, String sortField) {
        return coll.find("{companyId: #,shopId:#}", companyId, shopId).sort("{" + sortField + ":1}").as(entityClazz);
    }


    @Override
    public HashMap<String, T> listAsMap(String companyId, String shopId) {
        Iterable<T> items = listByShop(companyId, shopId);
        return asMap(items);
    }

    @Override
    public HashMap<String, T> listAllAsMap(String companyId, String shopId) {
        Iterable<T> items = listAllByShop(companyId, shopId);
        return asMap(items);
    }

    @Override
    public Iterable<T> listAllByShop(String companyId, String shopId) {
        return coll.find("{companyId: #,shopId:#}", companyId, shopId).as(entityClazz);
    }

    @Override
    public Iterable<T> listAllByShopActive(String companyId, String shopId) {
        return coll.find("{companyId: #,shopId:#,deleted:false,active:true}", companyId, shopId).as(entityClazz);
    }

    @Override
    public Iterable<T> listByShopWithDates(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#, deleted: false, modified:{$lt:#, $gt:#}}",
                companyId, shopId,
                beforeDate, afterDate).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<T> listByShopWithDatesCreate(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#, deleted: false, created:{$lt:#, $gt:#}}",
                companyId, shopId,
                beforeDate, afterDate).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<T> findItemsIn(String companyId, String shopId, List<ObjectId> ids) {
        return coll.find("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, ids).as(entityClazz);
    }


    @Override
    public HashMap<String, T> findItemsInAsMap(String companyId, String shopId, List<ObjectId> ids) {
        Iterable<T> items = findItemsIn(companyId, shopId, ids);

        return asMap(items);
    }

    @Override
    public Iterable<T> findItemsIn(String companyId, String shopId, List<ObjectId> ids, String projection) {
        return coll.find("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, ids).projection(projection).as(entityClazz);
    }

    @Override
    public DateSearchResult<T> findItemsAfter(String companyId, String shopId, long afterDate) {
        if (afterDate < 0) afterDate = 0;

        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{ $gt:#}}", companyId, shopId, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$gt:#}}", companyId, shopId, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsAfterLimit(String companyId, String shopId, long afterDate, int limit) {
        if (afterDate < 0) afterDate = 0;

        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{ $gt:#}}", companyId, shopId, afterDate).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$gt:#}}", companyId, shopId, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate) {

        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();

        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsWithDateNonDeleted(String companyId, String shopId, long afterDate, long beforeDate) {

        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();

        Iterable<T> items = coll.find("{companyId:#,shopId:#,deleted:false, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItems(String companyId, String shopId, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findActiveItems(String companyId, String shopId, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#,deleted:false,active:true}", companyId, shopId).skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:true}", companyId, shopId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItems(String companyId, String shopId, String sortOptions, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItems(String companyId, String shopId, String sortOptions, boolean active, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#,active:#}", companyId, shopId, active).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,active:#}", companyId, shopId, active);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<T> findItems(String companyId, String shopId, String sortOptions, String projections, int skip, int limit) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).projection(projections).sort(sortOptions).
                skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findItems(String companyId, String shopId, String sortOptions, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).sort(sortOptions).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findItems(String companyId, String shopId, String sortOptions, String projections, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId)
                .projection(projections)
                .skip(skip).limit(limit).sort(sortOptions).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findItems(String companyId, String shopId, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();

        Iterable<E> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).as(clazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();

        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<T> findItemsWithDateAndLimitSorted(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();

        Iterable<T> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort("{modified:1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<T> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }
}
