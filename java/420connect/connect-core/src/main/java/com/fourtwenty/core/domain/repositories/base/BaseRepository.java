package com.fourtwenty.core.domain.repositories.base;

import com.fourtwenty.core.caching.annotations.CacheInvalidate;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

import java.util.HashMap;
import java.util.List;

;

/**
 * Created by mdo on 8/16/15.
 */
public interface BaseRepository<T extends BaseModel> extends GenericRepository<T> {
    MongoCollection getMongoCollection();

    Iterable<T> iterator();

    Iterable<T> getAllDeleted();

    List<T> list();

    List<T> listNonProductType();

    List<T> listNonDeleted();

    HashMap<String, T> listAsMap();

    HashMap<String, T> listNonDeletedAsMap();

    Iterable<T> findItemsIn(List<ObjectId> ids);

    HashMap<String, T> findItemsInAsMap(List<ObjectId> ids);

    T getById(String entityId);
    <E extends BaseModel> E getById(String entityId, Class<E> clazz);

    T getById(String entityId, String projection);

    T save(T model);

    void hardRemoveById(List<ObjectId> objectIds);

    @CacheInvalidate
    T update(String entityId, T model);

    @CacheInvalidate
    T upsert(String entityId, T model);

    boolean exist(String entityId);

    @CacheInvalidate
    List<T> save(List<T> models);

    @CacheInvalidate
    void removeById(String entityId);

    @CacheInvalidate
    void hardRemoveById(String entityId);

    void updateModified(String entityId);

    Long count();

    Long countActive();

    DateSearchResult<T> findItemsWithDate(long afterDate, long beforeDate);

    <E extends T> DateSearchResult<E> findItemsWithDate(long afterDate, long beforeDate, Class<E> clazz);

    DateSearchResult<T> findItemsWithDate(long afterDate, long beforeDate, String projections);

    SearchResult<T> findItems(int skip, int limit);

    SearchResult<T> findItems(String sort, int skip, int limit);

    HashMap<String, T> listNonDeletedActiveAsMap();

    @CacheInvalidate
    void hardRemoveByBeforeDate(long date);
}
