package com.fourtwenty.core.domain.repositories.checkin;

import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;

public interface CheckInRepository extends MongoShopBaseRepository<CheckIn> {
    <E extends CheckIn> DateSearchResult<E> findActiveItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit, Class<E> clazz);

    void updateCheckInState(String kioskCheckInId, boolean state, CheckIn.CheckInStatus status, String transactionId);
}
