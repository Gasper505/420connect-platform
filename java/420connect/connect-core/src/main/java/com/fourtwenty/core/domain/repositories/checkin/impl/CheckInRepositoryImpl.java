package com.fourtwenty.core.domain.repositories.checkin.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.checkin.CheckInRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

public class CheckInRepositoryImpl extends ShopBaseRepositoryImpl<CheckIn> implements CheckInRepository {

    @Inject
    public CheckInRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CheckIn.class, mongoManager);
    }

    @Override
    public <E extends CheckIn> DateSearchResult<E> findActiveItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#, active:#, modified:{$lt:#, $gt:#}}", companyId, shopId, true, beforeDate, afterDate).sort("{checkInTime:-1}").skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#, active:#, modified:{$lt:#, $gt:#}}", companyId, shopId, true, beforeDate, afterDate);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public void updateCheckInState(String kioskCheckInId, boolean state, CheckIn.CheckInStatus status, String transactionId) {
        coll.update("{_id:#}", new ObjectId(kioskCheckInId)).with("{$set: {active:#, status:#, transactionId:#, modified:#}}", state, status, transactionId, DateTime.now().getMillis());
    }
}

