package com.fourtwenty.core.domain.repositories.comments;

import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface UserActivityRepository extends MongoShopBaseRepository<UserActivity> {
    SearchResult<UserActivity> getAllCommentsByReferenceId(final String companyId, final String referenceId, final String sortOption, final int start, final int limit);
}
