package com.fourtwenty.core.domain.repositories.comments.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.comments.UserActivityRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class UserActivityRepositoryImpl extends ShopBaseRepositoryImpl<UserActivity> implements UserActivityRepository {
    @Inject
    public UserActivityRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(UserActivity.class, mongoDb);
    }

    @Override
    public SearchResult<UserActivity> getAllCommentsByReferenceId(String companyId, String referenceId, String sortOption, int start, int limit) {
        Iterable<UserActivity> items = coll.find("{companyId:#,referenceId:#,deleted:false}", companyId, referenceId).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,referenceId:#,deleted:false}", companyId, referenceId);
        SearchResult<UserActivity> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
