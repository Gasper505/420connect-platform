package com.fourtwenty.core.domain.repositories.common;

import com.fourtwenty.core.domain.models.common.BackgroundTask;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface BackgroundTaskRepository extends MongoShopBaseRepository<BackgroundTask> {
}
