package com.fourtwenty.core.domain.repositories.common;


import com.fourtwenty.core.config.FcmConfig;
import com.fourtwenty.core.config.MtracConfiguration;
import com.fourtwenty.core.domain.models.common.*;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

import java.util.List;

public interface IntegrationSettingRepository extends BaseRepository<IntegrationSetting> {
    List<IntegrationSetting> getIntegrationSettings(String integration);

    Iterable<IntegrationSetting> getIntegrationSettings(String integration, IntegrationSetting.Environment environment);

    HeadsetConfig getHeadsetConfig(IntegrationSetting.Environment environment);

    LinxConfig getLinxConfig(IntegrationSetting.Environment environment);

    WeedmapConfig getWeedmapConfig(IntegrationSetting.Environment environment);
    CloverConfig getCloverConfig(IntegrationSetting.Environment environment);

    MetrcConfig getMetricConfig(IntegrationSettingConstants.Integrations.Metrc.MetrcState state, IntegrationSetting.Environment environment);

    FcmConfig getFcmConfig(IntegrationSetting.Environment environment);

    MtracConfiguration getMtracConfig(IntegrationSetting.Environment environment);
}

