package com.fourtwenty.core.domain.repositories.common.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.common.BackgroundTask;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.common.BackgroundTaskRepository;
import com.google.inject.Inject;

public class BackgroundTaskRepositoryImpl extends ShopBaseRepositoryImpl<BackgroundTask> implements BackgroundTaskRepository {

    @Inject
    public BackgroundTaskRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BackgroundTask.class, mongoManager);
    }
}
