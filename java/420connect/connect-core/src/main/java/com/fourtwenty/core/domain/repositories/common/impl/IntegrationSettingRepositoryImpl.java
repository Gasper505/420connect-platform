package com.fourtwenty.core.domain.repositories.common.impl;


import com.fourtwenty.core.config.FcmConfig;
import com.fourtwenty.core.config.MtracConfiguration;
import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.common.*;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;

public class IntegrationSettingRepositoryImpl extends MongoBaseRepositoryImpl<IntegrationSetting>
        implements IntegrationSettingRepository {

    @Inject
    public IntegrationSettingRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(IntegrationSetting.class, mongoManager);
    }


    @Override
    public List<IntegrationSetting> getIntegrationSettings(String integration) {
        Iterable<IntegrationSetting> settings = coll.find("{integration:#}", integration).as(entityClazz);
        return Lists.newArrayList(settings);
    }

    @Override
    public Iterable<IntegrationSetting> getIntegrationSettings(String integration,
                                                               IntegrationSetting.Environment environment) {
        return coll.find("{integration:#,environment:#,deleted:false}", integration, environment.name()).as(entityClazz);
    }


    @Override
    public HeadsetConfig getHeadsetConfig(IntegrationSetting.Environment environment) {

        if (environment == null) environment = IntegrationSetting.Environment.Development;

        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                IntegrationSettingConstants.Integrations.Headset.NAME,
                environment);

        return new HeadsetConfig(Lists.newArrayList(settings));
    }

    @Override
    public LinxConfig getLinxConfig(IntegrationSetting.Environment environment) {

        if (environment == null) environment = IntegrationSetting.Environment.Development;

        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                IntegrationSettingConstants.Integrations.Linx.NAME,
                environment);

        return new LinxConfig(Lists.newArrayList(settings));
    }

    @Override
    public MetrcConfig getMetricConfig(IntegrationSettingConstants.Integrations.Metrc.MetrcState state,
                                       IntegrationSetting.Environment environment) {

        if (environment == null) environment = IntegrationSetting.Environment.Development;

        String integrationName = IntegrationSettingConstants.Integrations.Metrc.makeName(state);
        System.out.println("Env: " + environment);
        System.out.println("Metrc Integrationname: " + integrationName);
        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                integrationName,
                environment);

        return new MetrcConfig(Lists.newArrayList(settings));
    }

    @Override
    public WeedmapConfig getWeedmapConfig(IntegrationSetting.Environment environment) {
        if (environment == null) environment = IntegrationSetting.Environment.Development;

        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                IntegrationSettingConstants.Integrations.Weedmap.NAME,
                environment);

        return new WeedmapConfig(Lists.newArrayList(settings));
    }

    @Override
    public CloverConfig getCloverConfig(IntegrationSetting.Environment environment) {
        if (environment == null) environment = IntegrationSetting.Environment.Development;

        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                IntegrationSettingConstants.Integrations.Clover.NAME,
                environment);

        return new CloverConfig(Lists.newArrayList(settings));
    }


    @Override
    public FcmConfig getFcmConfig(IntegrationSetting.Environment environment) {
        if (environment == null) environment = IntegrationSetting.Environment.Development;

        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                IntegrationSettingConstants.Integrations.FCM.NAME,
                environment);

        return new FcmConfig(Lists.newArrayList(settings));
    }

    @Override
    public MtracConfiguration getMtracConfig(IntegrationSetting.Environment environment) {
        environment = (environment == null) ? IntegrationSetting.Environment.Development : environment;

        Iterable<IntegrationSetting> settings = getIntegrationSettings(
                IntegrationSettingConstants.Integrations.Mtrac.NAME,
                environment);

        return new MtracConfiguration(Lists.newArrayList(settings));
    }

}

