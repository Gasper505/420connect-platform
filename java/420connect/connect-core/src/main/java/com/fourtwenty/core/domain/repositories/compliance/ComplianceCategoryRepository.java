package com.fourtwenty.core.domain.repositories.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceCategory;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.HashMap;

public interface ComplianceCategoryRepository extends MongoShopBaseRepository<ComplianceCategory> {

    void hardDeleteCompliancePackages(String companyId, String shopId);


    HashMap<String, ComplianceCategory> getItemsAsMapById(String companyId, String shopId);
}
