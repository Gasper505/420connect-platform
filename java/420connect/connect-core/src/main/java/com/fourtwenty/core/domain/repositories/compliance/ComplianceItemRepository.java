package com.fourtwenty.core.domain.repositories.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceItem;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;
import java.util.List;

public interface ComplianceItemRepository extends MongoShopBaseRepository<ComplianceItem> {

    void hardDeleteCompliancePackages(String companyId, String shopId);


    HashMap<String,ComplianceItem> getItemsAsMapById(String companyId, String shopId);
    ComplianceItem getComplianceItemByKey(String companyId, String shopId, String key);
    void updateProductId(String companyId, String entityId, String  productId);
    SearchResult<ComplianceItem> getComplianceItems(String companyId, String shopId, int skip, int limit);
    SearchResult<ComplianceItem> getComplianceItems(String companyId, String shopId, String query, int skip, int limit);

    HashMap<String, ComplianceItem> getComplianceItemByKeyAsMap(String companyId, String shopId, List<String> keys);
}
