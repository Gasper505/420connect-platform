package com.fourtwenty.core.domain.repositories.compliance;

import com.fourtwenty.core.domain.models.compliance.CompliancePackage;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;

public interface CompliancePackageRepository extends MongoShopBaseRepository<CompliancePackage> {

    void hardDeleteCompliancePackages(String companyId, String shopId);

    void setComplianceProductBatch(String companyId, String packageId, String productId, String batchId);

    CompliancePackage getCompliancePackageByKey(String companyId, String shopId, String key);
    CompliancePackage getCompliancePackageByProductBatch(String companyId, String shopId, String batchId);

    HashMap<String, CompliancePackage> getItemsAsMapById(String companyId, String shopId);
    SearchResult<CompliancePackage> getCompliancePackages(String companyId, String shopId, CompliancePackage.PackageStatus status, int skip, int limit);
    SearchResult<CompliancePackage> getCompliancePackages(String companyId, String shopId, CompliancePackage.PackageStatus status, String query, int skip, int limit);


    SearchResult<CompliancePackage> getCompliancePackagesTags(String companyId, String shopId, CompliancePackage.PackageStatus status);
}
