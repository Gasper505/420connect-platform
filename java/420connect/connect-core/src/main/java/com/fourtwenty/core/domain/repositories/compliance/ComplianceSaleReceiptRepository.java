package com.fourtwenty.core.domain.repositories.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceSaleReceipt;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;

public interface ComplianceSaleReceiptRepository extends MongoShopBaseRepository<ComplianceSaleReceipt> {

    void hardDeleteCompliancePackages(String companyId, String shopId);


    SearchResult<ComplianceSaleReceipt> getSaleReceipts(String companyId, String shopId, long startDate, long endDate, int skip, int limit);
    SearchResult<ComplianceSaleReceipt> getSaleReceipts(String companyId, String shopId, String query, long startDate, long endDate, int skip, int limit);
    ComplianceSaleReceipt getMetrcSaleByKey(String companyId, String shopId, String key);


    HashMap<String, ComplianceSaleReceipt> getItemsAsMapById(String companyId, String shopId);
}
