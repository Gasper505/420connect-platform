package com.fourtwenty.core.domain.repositories.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceStrain;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;

public interface ComplianceStrainRepository extends MongoShopBaseRepository<ComplianceStrain> {

    void hardDeleteCompliancePackages(String companyId, String shopId);

    ComplianceStrain getComplianceStrainName(String companyId, String shopId, String name);

    SearchResult<ComplianceStrain> getComplianceStrains(String companyId, String shopId, int skip, int limit);
    SearchResult<ComplianceStrain> getComplianceStrains(String companyId, String shopId, String query, int skip, int limit);


    HashMap<String, ComplianceStrain> getItemsAsMapById(String companyId, String shopId);
}
