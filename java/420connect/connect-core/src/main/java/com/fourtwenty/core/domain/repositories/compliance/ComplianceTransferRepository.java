package com.fourtwenty.core.domain.repositories.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceTransfer;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface ComplianceTransferRepository extends MongoShopBaseRepository<ComplianceTransfer> {

    void hardDeleteCompliancePackages(String companyId, String shopId);
}
