package com.fourtwenty.core.domain.repositories.compliance.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.ComplianceCategory;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceCategoryRepository;

import javax.inject.Inject;
import java.util.HashMap;

public class ComplianceCategoryRepositoryImpl extends ShopBaseRepositoryImpl<ComplianceCategory> implements ComplianceCategoryRepository {

    @Inject
    public ComplianceCategoryRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ComplianceCategory.class, mongoManager);
    }

    @Override
    public void hardDeleteCompliancePackages(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}",companyId,shopId);
    }

    @Override
    public HashMap<String, ComplianceCategory> getItemsAsMapById(String companyId, String shopId) {
        Iterable<ComplianceCategory> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        HashMap<String,ComplianceCategory> map = new HashMap<>();
        for (ComplianceCategory item : items) {
            map.put(item.getData().getName(),item);
        }
        return map;
    }
}
