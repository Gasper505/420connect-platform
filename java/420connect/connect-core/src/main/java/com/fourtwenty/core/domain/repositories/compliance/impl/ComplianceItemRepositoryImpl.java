package com.fourtwenty.core.domain.repositories.compliance.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.ComplianceItem;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceItemRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class ComplianceItemRepositoryImpl extends ShopBaseRepositoryImpl<ComplianceItem> implements ComplianceItemRepository {

    @Inject
    public ComplianceItemRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ComplianceItem.class, mongoManager);
    }

    @Override
    public void hardDeleteCompliancePackages(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}",companyId,shopId);
    }

    @Override
    public ComplianceItem getComplianceItemByKey(String companyId, String shopId, String key) {
        return coll.findOne("{companyId:#,shopId:#,key:#}",companyId,shopId,key).as(entityClazz);
    }

    @Override
    public void updateProductId(String companyId, String entityId, String productId) {
        coll.update("{companyId:#,_id:#}",companyId,new ObjectId(entityId)).with("{$set:{productId:#}}",productId);
    }

    @Override
    public SearchResult<ComplianceItem> getComplianceItems(String companyId, String shopId, int skip, int limit) {
        Iterable<ComplianceItem> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).sort("{key:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#}", companyId, shopId);

        SearchResult<ComplianceItem> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ComplianceItem> getComplianceItems(String companyId, String shopId, String query, int skip, int limit) {
        Pattern pattern = TextUtil.createPattern(query);

        Iterable<ComplianceItem> items = coll.find("{companyId:#,shopId:#,searchField:#}", companyId, shopId,pattern).sort("{key:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,searchField:#}", companyId, shopId,pattern);

        SearchResult<ComplianceItem> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public HashMap<String, ComplianceItem> getItemsAsMapById(String companyId, String shopId) {
        Iterable<ComplianceItem> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        HashMap<String,ComplianceItem> map = new HashMap<>();
        for (ComplianceItem item : items) {
            map.put(item.getData().getId() + "",item);
        }
        return map;
    }

    @Override
    public HashMap<String, ComplianceItem> getComplianceItemByKeyAsMap(String companyId, String shopId, List<String> keys) {
        Iterable<ComplianceItem> items = coll.find("{companyId:#,shopId:#,key:{$in:#}}", companyId, shopId, keys).as(entityClazz);
        HashMap<String, ComplianceItem> complianceItemHashMap = new HashMap<>();
        for (ComplianceItem complianceItem : items) {
            complianceItemHashMap.put(complianceItem.getKey(), complianceItem);
        }
        return complianceItemHashMap;
    }
}
