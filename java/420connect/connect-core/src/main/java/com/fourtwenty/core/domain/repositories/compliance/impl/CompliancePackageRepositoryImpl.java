package com.fourtwenty.core.domain.repositories.compliance.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.CompliancePackage;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.CompliancePackageRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.regex.Pattern;

public class CompliancePackageRepositoryImpl extends ShopBaseRepositoryImpl<CompliancePackage> implements CompliancePackageRepository {

    @Inject
    public CompliancePackageRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CompliancePackage.class, mongoManager);
    }

    @Override
    public void hardDeleteCompliancePackages(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}",companyId,shopId);
    }


    @Override
    public void setComplianceProductBatch(String companyId, String packageId, String productId, String batchId) {
        coll.update("{_id:#}",new ObjectId(packageId)).with("{$set:{productId:#,productBatchId:#}}",productId,batchId);
    }

    @Override
    public CompliancePackage getCompliancePackageByKey(String companyId, String shopId, String key) {
        return coll.findOne("{companyId:#,shopId:#,key:#}",companyId,shopId,key).as(entityClazz);
    }

    @Override
    public CompliancePackage getCompliancePackageByProductBatch(String companyId, String shopId, String batchId) {
        return coll.findOne("{companyId:#,shopId:#,productBatchId:#}",companyId,shopId,batchId).as(entityClazz);
    }

    @Override
    public SearchResult<CompliancePackage> getCompliancePackages(String companyId, String shopId, CompliancePackage.PackageStatus status, int skip, int limit) {
        Iterable<CompliancePackage> items = coll.find("{companyId:#,shopId:#,status:#}", companyId, shopId,status).sort("{key:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,status:#}", companyId, shopId,status);

        SearchResult<CompliancePackage> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<CompliancePackage> getCompliancePackages(String companyId, String shopId, CompliancePackage.PackageStatus status, String query, int skip, int limit) {

        Pattern pattern = TextUtil.createPattern(query);

        Iterable<CompliancePackage> items = coll.find("{companyId:#,shopId:#,status:#,searchField:#}", companyId, shopId,status,pattern).sort("{key:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,status:#,searchField:#}", companyId, shopId,status,pattern);

        SearchResult<CompliancePackage> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<CompliancePackage> getCompliancePackagesTags(String companyId, String shopId, CompliancePackage.PackageStatus status) {
        Iterable<CompliancePackage> items = coll.find("{companyId:#,shopId:#,status:#}", companyId, shopId,status).sort("{key:1}").projection("{key:1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,status:#}", companyId, shopId,status);

        SearchResult<CompliancePackage> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(0);
        results.setLimit(0);
        results.setTotal(count);
        return results;
    }

    @Override
    public HashMap<String, CompliancePackage> getItemsAsMapById(String companyId, String shopId) {
        Iterable<CompliancePackage> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        HashMap<String,CompliancePackage> map = new HashMap<>();
        for (CompliancePackage item : items) {
            map.put(item.getData().getLabel(),item);
        }
        return map;
    }
}
