package com.fourtwenty.core.domain.repositories.compliance.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.ComplianceSaleReceipt;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceSaleReceiptRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.regex.Pattern;

public class ComplianceSaleReceiptRepositoryImpl extends ShopBaseRepositoryImpl<ComplianceSaleReceipt> implements ComplianceSaleReceiptRepository {

    @Inject
    public ComplianceSaleReceiptRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ComplianceSaleReceipt.class, mongoManager);
    }

    @Override
    public void hardDeleteCompliancePackages(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}",companyId,shopId);
    }

    @Override
    public SearchResult<ComplianceSaleReceipt> getSaleReceipts(String companyId, String shopId, long startDate, long endDate, int skip, int limit) {
        Iterable<ComplianceSaleReceipt> items = coll.find("{companyId:#,shopId:#,modified:{$gte:#,$lte:#}}", companyId, shopId,startDate,endDate).sort("{modified:-1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,modified:{$gte:#,$lte:#}}", companyId, shopId,startDate,endDate);

        SearchResult<ComplianceSaleReceipt> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ComplianceSaleReceipt> getSaleReceipts(String companyId, String shopId, String query,long startDate, long endDate,  int skip, int limit) {
        Pattern pattern = TextUtil.createPattern(query);

        Iterable<ComplianceSaleReceipt> items = coll.find("{companyId:#,shopId:#,searchField:#,modified:{$gte:#,$lte:#}}", companyId, shopId,pattern,startDate,endDate).sort("{modified:-1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,searchField:#,modified:{$gte:#,$lte:#}}", companyId, shopId,pattern,startDate,endDate);

        SearchResult<ComplianceSaleReceipt> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public ComplianceSaleReceipt getMetrcSaleByKey(String companyId, String shopId, String key) {
        return coll.findOne("{companyId:#,shopId:#,key:#}",companyId,shopId,key).as(entityClazz);
    }

    @Override
    public HashMap<String, ComplianceSaleReceipt> getItemsAsMapById(String companyId, String shopId) {
        Iterable<ComplianceSaleReceipt> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        HashMap<String,ComplianceSaleReceipt> map = new HashMap<>();
        for (ComplianceSaleReceipt item : items) {
            map.put(item.getData().getId() + "",item);
        }
        return map;
    }
}
