package com.fourtwenty.core.domain.repositories.compliance.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.ComplianceStrain;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceStrainRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.regex.Pattern;

public class ComplianceStrainRepositoryImpl extends ShopBaseRepositoryImpl<ComplianceStrain> implements ComplianceStrainRepository {

    @Inject
    public ComplianceStrainRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ComplianceStrain.class, mongoManager);
    }

    @Override
    public void hardDeleteCompliancePackages(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}",companyId,shopId);
    }

    @Override
    public ComplianceStrain getComplianceStrainName(String companyId, String shopId, String name) {
        ComplianceStrain item = coll.findOne("{companyId:#,shopId:#,key:#}", companyId, shopId,name).as(entityClazz);
        return item;
    }

    @Override
    public SearchResult<ComplianceStrain> getComplianceStrains(String companyId, String shopId, int skip, int limit) {
        Iterable<ComplianceStrain> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).sort("{key:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#}", companyId, shopId);

        SearchResult<ComplianceStrain> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ComplianceStrain> getComplianceStrains(String companyId, String shopId, String query, int skip, int limit) {
        Pattern pattern = TextUtil.createPattern(query);

        Iterable<ComplianceStrain> items = coll.find("{companyId:#,shopId:#,searchField:#}", companyId, shopId,pattern).sort("{key:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,searchField:#}", companyId, shopId,pattern);

        SearchResult<ComplianceStrain> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public HashMap<String, ComplianceStrain> getItemsAsMapById(String companyId, String shopId) {
        Iterable<ComplianceStrain> items = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        HashMap<String,ComplianceStrain> map = new HashMap<>();
        for (ComplianceStrain item : items) {
            map.put(item.getData().getName().toLowerCase(),item);
        }
        return map;
    }
}
