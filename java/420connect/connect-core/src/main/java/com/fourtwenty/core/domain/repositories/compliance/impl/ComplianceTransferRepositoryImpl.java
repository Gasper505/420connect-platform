package com.fourtwenty.core.domain.repositories.compliance.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.ComplianceTransfer;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceTransferRepository;

import javax.inject.Inject;

public class ComplianceTransferRepositoryImpl extends ShopBaseRepositoryImpl<ComplianceTransfer> implements ComplianceTransferRepository {

    @Inject
    public ComplianceTransferRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ComplianceTransfer.class, mongoManager);
    }

    @Override
    public void hardDeleteCompliancePackages(String companyId, String shopId) {
        coll.remove("{companyId:#,shopId:#}",companyId,shopId);
    }
}
