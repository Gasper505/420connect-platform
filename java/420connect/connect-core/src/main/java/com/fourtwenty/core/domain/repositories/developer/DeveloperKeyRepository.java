package com.fourtwenty.core.domain.repositories.developer;

import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 2/2/17.
 */
public interface DeveloperKeyRepository extends MongoCompanyBaseRepository<DeveloperKey> {
    DeveloperKey getDeveloperKey(final String key);

    void deleteAll(final String companyId, final String shopId);

    SearchResult<DeveloperKey> getActiveItems(final String companyId);

    DeveloperKey getDeveloperKeyByShopId(String companyId, String shopId);
}
