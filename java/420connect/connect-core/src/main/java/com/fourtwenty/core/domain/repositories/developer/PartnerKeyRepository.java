package com.fourtwenty.core.domain.repositories.developer;

import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 2/9/18.
 */
public interface PartnerKeyRepository extends BaseRepository<PartnerKey> {
    PartnerKey getPartnerKey(String partnerKey);
}
