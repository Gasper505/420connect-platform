package com.fourtwenty.core.domain.repositories.developer.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.joda.time.DateTime;

/**
 * Created by mdo on 2/2/17.
 */
public class DeveloperKeyRepositoryImpl extends CompanyBaseRepositoryImpl<DeveloperKey> implements DeveloperKeyRepository {

    @Inject
    public DeveloperKeyRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(DeveloperKey.class, mongoManager);
    }


    @Override
    public DeveloperKey getDeveloperKey(String key) {
        return coll.findOne("{key:#}", key).as(entityClazz);
    }


    @Override
    public void deleteAll(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#}", companyId, shopId).multi().with("{$set: {deleted:true,active:false, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public SearchResult<DeveloperKey> getActiveItems(String companyId) {
        Iterable<DeveloperKey> items = coll.find("{companyId:#, active:true}", companyId).as(entityClazz);

        SearchResult<DeveloperKey> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal((long) result.getValues().size());
        result.setSkip(0);
        result.setLimit(result.getTotal().intValue());

        return result;

    }

    @Override
    public DeveloperKey getDeveloperKeyByShopId(String companyId, String shopId) {
        return coll.findOne("{companyId:#, shopId:#, active : true}", companyId, shopId).as(entityClazz);
    }
}
