package com.fourtwenty.core.domain.repositories.developer.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.developer.PartnerKeyRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 2/9/18.
 */
public class PartnerKeyRepositoryImpl extends MongoBaseRepositoryImpl<PartnerKey> implements PartnerKeyRepository {
    @Inject
    public PartnerKeyRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PartnerKey.class, mongoManager);
    }

    @Override
    public PartnerKey getPartnerKey(String partnerKey) {
        return coll.findOne("{key:#}", partnerKey).as(entityClazz);
    }
}
