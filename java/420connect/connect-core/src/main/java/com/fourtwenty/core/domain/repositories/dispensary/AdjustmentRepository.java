package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.Adjustment;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface AdjustmentRepository extends MongoShopBaseRepository<Adjustment> {
    Adjustment getAdjustmentByName(String companyId, String shopId, String name);
}
