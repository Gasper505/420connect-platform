package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 7/12/16.
 */
public interface AppRepository extends BaseRepository<App> {
    App getAppByName(String name);
}
