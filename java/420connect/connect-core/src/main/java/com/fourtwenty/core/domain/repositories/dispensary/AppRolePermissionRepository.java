package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.AppRolePermission;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

public interface AppRolePermissionRepository extends MongoCompanyBaseRepository<AppRolePermission> {
    AppRolePermission getPermissionsByCompanyAndRole(String companyId, CompanyFeatures.AppTarget appTarget);
}
