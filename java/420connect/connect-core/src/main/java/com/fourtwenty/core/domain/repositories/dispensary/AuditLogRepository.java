package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.AuditLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by mdo on 7/13/16.
 */
public interface AuditLogRepository extends MongoShopBaseRepository<AuditLog> {
    Iterable<AuditLog> getLogsByEmployee(String companyId, String shopId, String employeeId);

    Iterable<AuditLog> getLogsByDateBracket(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<AuditLog> getLogsByBracketAndEmployee(String companyId, String shopId, String employeeId, Long startDate, Long endDate);
}
