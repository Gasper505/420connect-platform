package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.List;

/**
 * Created by mdo on 2/28/17.
 */
public interface BarcodeItemRepository extends MongoShopBaseRepository<BarcodeItem> {
    BarcodeItem getBarcode(String companyId, String shopId, BarcodeItem.BarcodeEntityType entityType, String barcode);

    int removeAllInEntities(List<String> entityIds);

    int removeAllInProductIds(List<String> productIds);

    int deleteAllBarcodeItems(String companyId, String shopId);

    BarcodeItem getDeletedBarcode(String companyId, String shopId, BarcodeItem.BarcodeEntityType barcodeType, String barcode);

    List<BarcodeItem> getBarCodesByLabel(String companyId, String shopId, String label, BarcodeItem.BarcodeEntityType entityType);

    int removeByBarCodes(String companyId, String shopId, List<String> batchSkuList);

    Iterable<BarcodeItem> getAvailableBarCodes(String companyId, String shopId, BarcodeItem.BarcodeEntityType entityType, List<String> barCodes);

    Iterable<BarcodeItem> getAllBarCodesForProduct(String companyId, String shopId, List<String> productIds);
}

