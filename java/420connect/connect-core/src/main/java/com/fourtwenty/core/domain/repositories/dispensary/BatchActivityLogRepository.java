package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.BatchActivityLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;
import java.util.List;

public interface BatchActivityLogRepository extends MongoShopBaseRepository<BatchActivityLog> {

    SearchResult<BatchActivityLog> getBatchActivityLogByBatchId(final String companyId, final String shopId, final String batchId, final String sortOption, final int start, final int limit);

    HashMap<String, List<BatchActivityLog>> findItemsByBatchIdAsMap(String companyId, String shopId, List<String> batchId);


}
