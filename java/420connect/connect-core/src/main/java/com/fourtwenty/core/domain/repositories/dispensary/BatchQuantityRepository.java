package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchByCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchQuantityResult;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 10/11/17.
 */
public interface BatchQuantityRepository extends MongoShopBaseRepository<BatchQuantity> {
    List<BatchQuantity> getBatchQuantities(String companyId, String shopId, String productId);

    <E extends BatchQuantity> SearchResult<E> getBatchQuantitiesForInventory(String companyId, String shopId, String productId, String inventoryId, Class<E> clazz, int start, int limit);

    Iterable<BatchQuantity> getBatchQuantitiesForInventorySorted(String companyId, String shopId, String productId, String inventoryId, String sortOptions);

    BigDecimal getBatchQuantitiesForInventoryTotal(String companyId, String shopId, String productId, String inventoryId);
    BigDecimal getBatchQuantityForByBatchIdTotal(String companyId, String shopId, String productId,  String batchId);

    BatchQuantity getBatchQuantityForInventory(String companyId, String shopId, String productId, String inventoryId, String batchId);

    SearchResult<BatchQuantityResult> getBatchQuantitiesForInventoryAndBatch(String companyId, String shopId, String inventoryId, List<String> batchIdList, String productId, int start, int limit);

    List<ProductBatchByInventory> getBatchQuantitiesByInventory(String companyId, String shopId, String batchId, String productId);

    void removeAllProductBatchQuantities(String companyId, String shopId);

    List<BatchByCategoryResult> getBatchQuantitiesForInventory(String companyId, String shopId, String inventoryId, List<String> productIds);

    void removeAnyWithoutBatchId();

    void removeAnyWithoutInventoryIds(List<String> inventoryIds);

    HashMap<String, List<BatchQuantity>> getBatchQuantityForProduct(String companyId, String shopId, List<String> productId);

    Iterable<BatchQuantity> getBatchQuantityByBatch(String companyId, String shopId, String batchId);

    HashMap<String, BatchByCategoryResult> getBatchQuantitiesForProductAsMap(String companyId, String shopId, String inventoryId, List<String> productIds);

    HashMap<String, List<BatchQuantity>> getBatchQuantityByInventoryForProduct(String companyId, String shopId, List<String> inventoryIds, List<String> productIds);

    HashMap<String, List<BatchQuantity>> getBatchQuantityForProductInventory(String companyId, String shopId, List<String> inventoryIds, List<String> productIds);

    HashMap<String, List<BatchQuantity>> getAllBatchQuantities(String companyId, String shopId, List<String> batchIds);
}
