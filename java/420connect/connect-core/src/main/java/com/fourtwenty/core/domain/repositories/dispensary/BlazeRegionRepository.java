package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

public interface BlazeRegionRepository extends MongoCompanyBaseRepository<BlazeRegion> {
}
