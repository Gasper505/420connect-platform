package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.BouncedEmail;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 8/8/16.
 */
public interface BouncedEmailRepository extends BaseRepository<BouncedEmail> {
    BouncedEmail getBouncedEmail(String email);

    boolean isBounced(String email);
}
