package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.BouncedNumber;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

import java.util.List;

public interface BouncedNumberRepository extends BaseRepository<BouncedNumber> {
    BouncedNumber getBouncedNumber(String number);

    void deleteByNumber(String number);

    Iterable<BouncedNumber> getDetailsByNumber(List<String> phoneNumber);

}
