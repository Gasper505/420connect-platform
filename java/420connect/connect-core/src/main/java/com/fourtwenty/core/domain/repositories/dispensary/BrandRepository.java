package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.caching.annotations.Cached;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

public interface BrandRepository extends MongoCompanyBaseRepository<Brand> {
    <E extends Brand> SearchResult<E> getAllBrand(final String companyId, final int start, final int limit, final String sortOption, Class<E> clazz);

    Brand getBrandByName(String companyId, String name);

    <E extends Brand> SearchResult<E> findBrandBySearchTerms(String companyId, String searchTerm, int skip, int limit, Class<E> clazz);

    <E extends Brand> SearchResult<E> getAllDeletedBrand(final String companyId, final int start, final int limit, final String sortOption, Class<E> clazz);

    SearchResult<Brand> getBrandsByIdAndTerm(final String companyId, final List<ObjectId> brandIds, final String term, final String sort, final int start, final int limit);

    SearchResult<Brand> getBrandsByIds(final String companyId, final List<ObjectId> brandIds, final String sort, final int start, final int limit);

    @Override
    @Cached(returnClasses = {HashMap.class, String.class, Brand.class})
    HashMap<String, Brand> listAsMap(String companyId, List<ObjectId> ids);

    HashMap<String, Brand> getLimitedBrandViewAsMap(String companyId);

    <E extends Brand> SearchResult<E> getAllBrandByStatus(String companyId, boolean active, int start, int limit, String sortOption, Class<E> clazz);

    <E extends Brand> SearchResult<E> findBrandByStatusAndTerm(String companyId, boolean active, String searchTerm, int skip, int limit, Class<E> clazz);

    void bulkUpdateWebsite(String companyId, List<ObjectId> brandIds, String website);

    void bulkUpdatePhoneNo(String companyId, List<ObjectId> brandIds, String phoneNumber);

    void bulkUpdateStatus(String companyId, List<ObjectId> brandIds, Boolean active);

    void deleteBrands(String companyId, List<ObjectId> brandIds);

    Brand getDefaultBrand(String companyId);
}
