package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.customer.CareGiver;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by decipher on 4/12/17 6:06 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface CareGiverRepository extends MongoShopBaseRepository<CareGiver> {
}
