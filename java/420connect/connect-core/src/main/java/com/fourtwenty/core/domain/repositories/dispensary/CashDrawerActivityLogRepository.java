package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CashDrawerSessionActivityLog;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CashDrawerActivityLogRepository extends MongoCompanyBaseRepository<CashDrawerSessionActivityLog> {

    <E extends ShopBaseModel> SearchResult<E> getActivityLogForCashDrawerSessionId(String companyId, String shopId, String cashDrawerSessionId, long startDateTime, long endDateTime, int skip, int limit, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> getAllActivityLogForCashDrawerSessionId(String companyId, String shopId, String cashDrawerSessionId, Class<E> clazz);
}
