package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;

/**
 * Created by mdo on 12/7/16.
 */
public interface CashDrawerSessionRepository extends MongoShopBaseRepository<CashDrawerSession> {
    CashDrawerSession getCashDrawerForDate(String companyId, String shopId, String terminalId, String date);

    CashDrawerSession getLatestDrawerForTerminal(String companyId, String shopId, String terminalId);

    Iterable<CashDrawerSession> getCashDrawersForDate(String companyId, String shopId, String date);

    HashMap<String, CashDrawerSession> getCashDrawersForDateAsMapTerminal(String companyId, String shopId, String date);

    Iterable<CashDrawerSession> getCashDrawerSessionsForShop(String companyId, String shopId, Long startDate, Long endDate);

    <E extends CashDrawerSession> SearchResult<E> getCashDrawerSessionsForShop(String companyId, String shopId, String terminalId, int start, int limit, Class<E> clazz);

    Iterable<CashDrawerSession> getOpenedCashDrawers();

    SearchResult<CashDrawerSession> getCashDrawerForTerminalId(String companyId, String shopId, String terminalId, String sortOptions, Long startDate, Long endDate);

    <E extends CashDrawerSession>  SearchResult<E> getOpenDrawersForTerminal(String companyId, String shopId, String terminalId, Class<E> clazz);

}
