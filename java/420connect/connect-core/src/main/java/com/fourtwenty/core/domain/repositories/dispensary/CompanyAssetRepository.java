package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 10/26/15.
 */
public interface CompanyAssetRepository extends MongoCompanyBaseRepository<CompanyAsset> {
    CompanyAsset getAssetByKey(String companyId, String key);

    HashMap<String, CompanyAsset> getAssetByKeyAsMap(String companyId, List<String> keyNames);
}
