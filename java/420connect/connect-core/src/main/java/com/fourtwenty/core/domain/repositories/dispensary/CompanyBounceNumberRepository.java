package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyBounceNumber;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

import java.util.HashMap;

public interface CompanyBounceNumberRepository extends BaseRepository<CompanyBounceNumber> {

    CompanyBounceNumber getBouncedNumber(String companyId, String number);

    HashMap<String, CompanyBounceNumber> listAllByCompany(String companyId);
}
