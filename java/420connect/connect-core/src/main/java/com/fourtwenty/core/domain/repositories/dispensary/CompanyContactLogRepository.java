package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyContactLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CompanyContactLogRepository extends MongoShopBaseRepository<CompanyContactLog> {
    SearchResult<CompanyContactLog> getAllCompanyContactLogByCustomerCompany(final String companyId, final String customerCompanyId, final String sortOption, final int start, final int limit);
}
