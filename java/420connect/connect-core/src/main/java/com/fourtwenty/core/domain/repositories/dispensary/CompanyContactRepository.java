package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CompanyContactRepository extends MongoShopBaseRepository<CompanyContact> {

    <E extends CompanyContact> E getCompanyContactById(String contactId, Class<E> clazz);

    <E extends CompanyContact> SearchResult<E> getAllCompanyContact(String companyId, String sortOptions, int skip, int limit, Class<E> clazz);

    <E extends CompanyContact> SearchResult<E> getAllCompanyContactByCustomerCompany(String companyId, String customerCompanyId, String sortOptions, int skip, int limit, Class<E> clazz);

    CompanyContact getCompanyContactByEmail(String email);

    <E extends CompanyContact> SearchResult<E> getAllCompanyContactToSearch(String companyId, String sortOption, String term, int skip, int limit, Class<E> clazz);

    <E extends CompanyContact> SearchResult<E> getCompanyContactByCustomerCompanySearch(String companyId, String customerCompanyId, String term, String sortOption, int skip, int limit, Class<E> clazz);

    CompanyContact getCompanyContactByVendor(String companyId, String vendorId);

    <E extends CompanyContact> SearchResult<E> findItemsWithDate(String companyId, long startDate, long endDate, int skip, int limit, String sortOptions, Class<E> clazz);
}
