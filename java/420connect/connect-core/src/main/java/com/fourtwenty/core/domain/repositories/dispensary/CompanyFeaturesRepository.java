package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

/**
 * Created by mdo on 5/15/16.
 */
public interface CompanyFeaturesRepository extends MongoCompanyBaseRepository<CompanyFeatures> {
    CompanyFeatures getCompanyFeature(String companyId);

    void addSMSCredit(String companyId, int smsCredits);

    void addEmailCredit(String companyId, int emailCredits);

    void subtractSMSEmailCredits(String companyId, int smsDeducts, int emailDeducts);
}
