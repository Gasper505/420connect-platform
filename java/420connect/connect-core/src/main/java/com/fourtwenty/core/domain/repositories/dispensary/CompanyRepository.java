package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;
import java.util.List;

/**
 * Created by mdo on 9/6/15.
 */
public interface CompanyRepository extends BaseRepository<Company> {
    Company getCompanyByEmail(String emailAddress);

    List<Company> findAllCompany();
}
