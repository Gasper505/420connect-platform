package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

/**
 * Created by mdo on 3/6/17.
 */
public interface CompanyUniqueSequenceRepository extends MongoCompanyBaseRepository<CompanyUniqueSequence> {
    CompanyUniqueSequence getNewIdentifier(String companyId, String keyName);
}
