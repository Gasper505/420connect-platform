package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 8/9/16.
 */
public interface ConnectProductRepository extends BaseRepository<ConnectProduct> {
    ConnectProduct getProductBySKU(String productSKU);
}
