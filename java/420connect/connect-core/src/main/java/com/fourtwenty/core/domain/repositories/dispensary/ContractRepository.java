package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Stephen Schmidt on 12/7/2015.
 */
public interface ContractRepository extends MongoShopBaseRepository<Contract> {

    List<Contract> getActiveContracts(String companyId, String shopId);

    HashMap<String, Contract> getActiveContractsAsMap(String companyId, String shopId);

    void setContractsToActive(String companyId, String shopId, boolean active);

    void activateContract(String contractId, Boolean status);
}
