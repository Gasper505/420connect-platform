package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.List;

public interface ConversationRepository extends MongoShopBaseRepository<Conversation> {
    <E extends Conversation> List<E> getTransactionConversation(final String transactionId, final long afterDate, final long beforeDate, final Conversation.ConversationType conversationType, Class<E> eClass);


    Conversation getConversationByMessageId(final String sid);

    Transaction getTransactionToAndFromNumber(final String toNUmber, final String fromNUmber, final String companyId);

    <E extends Conversation> List<E> getTransactionConversationByCurrentUser(String employeeId, long afterDate, long beforeDate, Conversation.ConversationType conversationType, Class<E> eClass);
    <E extends Conversation> List<E> getActiveTransactionChat(List<String> transactionIds, Class<E> clazz);
}
