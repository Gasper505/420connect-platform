package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CreditHistory;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface CreditHistoryRepository extends MongoShopBaseRepository<CreditHistory> {
}
