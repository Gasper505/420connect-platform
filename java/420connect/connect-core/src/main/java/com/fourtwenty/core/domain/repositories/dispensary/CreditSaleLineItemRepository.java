package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.credits.CreditSaleLineItem;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by mdo on 7/17/17.
 */
public interface CreditSaleLineItemRepository extends MongoShopBaseRepository<CreditSaleLineItem> {
}
