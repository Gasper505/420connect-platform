package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CultivationTaxInfo;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

public interface CultivationTaxInfoRepository extends BaseRepository<CultivationTaxInfo> {
    CultivationTaxInfo getCultivationTaxInfoByState(final String state, final String country);
}
