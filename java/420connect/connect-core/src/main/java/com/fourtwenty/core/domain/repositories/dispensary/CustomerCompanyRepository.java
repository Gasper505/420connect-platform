package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CustomerCompany;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CustomerCompanyRepository extends MongoShopBaseRepository<CustomerCompany> {
    CustomerCompany getCustomerCompanyByEmail(final String emailAddress);

    SearchResult<CustomerCompany> getAllCustomerCompany(String companyId, int start, int limit, String term);

    SearchResult<CustomerCompany> getAllCustomerCompany(String companyId, String sortOptions, int start, int limit);
}
