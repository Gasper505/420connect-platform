package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.DeliveryZone;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by decipher on 28/11/17 5:55 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface DeliveryAreaRepository extends MongoCompanyBaseRepository<DeliveryZone> {
    SearchResult<DeliveryZone> getAllDeliveryArea(String companyId, String shopId, int skip, int limit, String sortOptions);

    SearchResult<DeliveryZone> getAllDeliveryAreaByZoneType(String companyId, String shopId, int skip, int limit, String sortOptions, DeliveryZone.DeliveryZoneType zoneType);
}
