package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.DeliveryTaxRate;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.List;

public interface DeliveryTaxRateRepository extends MongoShopBaseRepository<DeliveryTaxRate> {

    <E extends DeliveryTaxRate> List<E> listAllByShopId(String companyId, String shopId, String sortBy, Class<E> clazz);

}
