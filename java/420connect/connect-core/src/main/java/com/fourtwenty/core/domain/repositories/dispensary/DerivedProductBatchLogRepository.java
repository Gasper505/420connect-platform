package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.DerivedProductBatchLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface DerivedProductBatchLogRepository extends MongoShopBaseRepository<DerivedProductBatchLog> {
}
