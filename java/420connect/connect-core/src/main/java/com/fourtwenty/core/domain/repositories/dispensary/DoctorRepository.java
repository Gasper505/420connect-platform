package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
public interface DoctorRepository extends MongoCompanyBaseRepository<Doctor> {
    Doctor getDoctorByLicense(String companyId, String license);

    Doctor getDoctorByImportId(String companyId, String importId);

    SearchResult<Doctor> searchDoctorsByTerm(String companyId, String term, int start, int limit);
}
