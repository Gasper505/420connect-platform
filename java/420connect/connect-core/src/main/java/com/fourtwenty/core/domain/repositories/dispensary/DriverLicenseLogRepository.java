package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.DriverLicenseLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface DriverLicenseLogRepository extends MongoShopBaseRepository<DriverLicenseLog> {

    SearchResult<DriverLicenseLog> findItems(String companyId, String shopId, int start, int limit);
}
