package com.fourtwenty.core.domain.repositories.dispensary;


import com.fourtwenty.core.caching.annotations.CacheInvalidate;
import com.fourtwenty.core.caching.annotations.Cached;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.EmployeeLocationResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

public interface EmployeeRepository extends MongoCompanyBaseRepository<Employee> {

    Employee getEmployeeByQuickPin(String companyId, String quickPin);

    Employee getEmployeeByEmail(String email);

    @CacheInvalidate
    WriteResult updateEmployeeLocation(String companyId, String employeeId, TerminalLocation recentLocation);

    <E extends CompanyBaseModel> SearchResult<E> findItems(String companyId, int skip, int limit, Class<E> clazz);

    <E extends Employee> SearchResult<E> findItems(String companyId, int skip, int limit, String projections, Class<E> clazz);

    @Cached
    <E extends Employee> SearchResult<E> findItems(String companyId, String searchTerm, int skip, int limit, String projections, Class<E> clazz);

    @CacheInvalidate
    void setRecentTimecard(String companyId, String employeeId, String timecardId);

    @CacheInvalidate
    void setTerminalId(String companyId, String employeeId, String terminalId);

    @Cached
    List<Employee> getEmployees(String companyId);

    @CacheInvalidate
    void deleteEmployee(String companyId, String employeeId, String newPin, String newEmail);

    @Cached
    Iterable<Employee> getEmployeesWithNoShops();

    @CacheInvalidate
    WriteResult setEmployeesShops(String companyId, String employeeId, List<String> shopIds);

    @Cached
    <E extends Employee> SearchResult<E> findItemsByRole(String companyId, int start, int limit, String projection, String roleId, Class<E> clazz);

    @Cached
    <E extends Employee> SearchResult<E> findItemsByRoleAndSearchTerms(String companyId, String searchTerm, int start, int limit, String projection, String roleId, Class<E> clazz);

    @Cached
    Iterable<EmployeeLocationResult> getDriverList(String companyId, String roleId);

    @Cached
    <E extends Employee> HashMap<String, E> getEmployeesAsMap(String companyId, Class<E> clazz);

    @CacheInvalidate
    WriteResult updateEmployeeAssignedTerminal(String companyId, String employeeId, String assignedTerminalId);

    <E extends Employee> E getEmployeeById(String employeeId, String companyId, String projection, Class<E> clazz);

    @Cached
    List<Employee> getEmployeesByShop(String companyId, String shopId);

    @CacheInvalidate
    void bulkUpdateEmployeeAppAccess(String companyId, List<ObjectId> objectIds, LinkedHashSet<CompanyFeatures.AppTarget> appTargets);

    @Cached
    <E extends Employee> SearchResult<E> findItemsByShop(String companyId, String shopId, int skip, int limit, String projections, Class<E> clazz);

    @Cached
    <E extends Employee> SearchResult<E> findItemsByShop(String companyId, String shopId, String searchTerm, int skip, int limit, String projections, Class<E> clazz);

    /*@Override
    @Cached
    Employee get(String companyId, String id);*/

    Employee get(String companyId, String id);

    Employee get(String companyId, String id, String projection);

    <E extends Employee> HashMap<String, E> listEmployeeAsMap(String companyId, List<ObjectId> employeeIds, Class<E> clazz);

    @CacheInvalidate
    void updateLastLogin(String companyId, String employeeId, String logInShopId);

    @CacheInvalidate
    void bulkUpdateEmployeeWithSingleAppAccess(String companyId, List<ObjectId> objectIds, CompanyFeatures.AppTarget appTarget);

    List<Employee> getTookanEmployeesByShop(String companyId, String shopId);

    <E extends Employee> SearchResult<E> findItemsByAccessConfig(String companyId, int start, int limit, String projection, List<CompanyFeatures.AppTarget> accessConfigs, Class<E> clazz);

    <E extends Employee> SearchResult<E> findItemsByAccessConfigAndTerms(String companyId, String term, int start, int limit, String projection, List<CompanyFeatures.AppTarget> accessConfigs, Class<E> clazz);

    SearchResult<EmployeeResult> findItemsByShops(String companyId, List<String> shopId, int skip, int limit, String projections);

    SearchResult<EmployeeResult> findItemsByShopsAndTerm(String companyId, List<String> shopId, String searchTerm, int skip, int limit, String projections);

    @CacheInvalidate
    void updateDriverInfo(List<String> roleIds, boolean state);

    <E extends Employee> SearchResult<E> findDriverEmployeeByShop(String companyId, String shopId, int start, int limit, String projections, Class<E> clazz);

    <E extends Employee> SearchResult<E> findAllEmployee(String companyId, int skip, int limit, String projections, Class<E> clazz);
}