package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

public interface ExciseTaxInfoRepository extends BaseRepository<ExciseTaxInfo> {
    ExciseTaxInfo getExciseTaxInfoByState(String state, String country);

    ExciseTaxInfo getExciseTaxInfoByState(String state, String country, ExciseTaxInfo.CannabisTaxType cannabisTaxType);
}
