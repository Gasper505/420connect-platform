package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.InvalidPhone;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 7/23/17.
 */
public interface InvalidPhoneRepository extends BaseRepository<InvalidPhone> {
    boolean isInvalidPhone(String phoneNumber);
}
