package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

public interface InventoryActionRepository extends MongoShopBaseRepository<InventoryOperation> {

    InventoryOperation getLastActionBySource(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType);

    Iterable<InventoryOperation> getActionsBySourceRequest(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType, String requestId);

    List<InventoryOperation> getActionsBySource(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType);


    Iterable<InventoryOperation> getActionsBySource(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate, String productId);

    Iterable<InventoryOperation> getAllActions(String companyId, String shopId, long startDate, long endDate, String productId);


    //New
    InventoryOperation getRecentInventoryAction(String companyId, String shopId, String productId, String batchId, String inventoryId);

    InventoryOperation getRecentPrepackageInventoryAction(String companyId, String shopId, String productId, String batchId, String inventoryId, String prepackageItemId);


    InventoryOperation getRecentInventoryAction(String companyId, String shopId, String sourceId);
    InventoryOperation getRecentInventoryAction(String companyId, String shopId, String sourceId, String sourceChildId);
    Iterable<InventoryOperation> getRecentInventoryActionsByRequestId(String companyId, String shopId, String sourceChildId, String requestId);

    Iterable<InventoryOperation> getBracketOperations(String companyId, String shopId, long startTime, long endTime, String projection);

    long countOperation(String companyId, String shopId, long startTime, long endTime);

    Iterable<InventoryOperation> getOperationsByProducts(String companyId, String shopId, List<String> productIds);

    Iterable<InventoryOperation> getActionsByTime(String companyId, String shopId, long startTime);

    Iterable<InventoryOperation> getOperationsBySourceTypeForProducts(String companyId, String shopId, long startTime, List<InventoryOperation.SourceType> sourceTypes, List<String> productIds);

    Iterable<InventoryOperation> getAllActionsWithBatch(String companyId, String shopId, long startDate, long endDate, String productBatchId);

    Iterable<InventoryOperation> getAllActionsBySourceWithBatch(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate, String productBatchId);

    Iterable<InventoryOperation> getActionByProductWithBatch(String companyId, String shopId, long startDate, long endDate, String productId, String productBatchId);

    Iterable<InventoryOperation> getBySourceWithProductAndBatch(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate, String productId, String productBatchId);

    SearchResult<InventoryOperation> getLogsByInventoryId(String companyId, String shopId, String assignedInventoryId, String employeeId, int start, int limit, InventoryOperation.SourceType sourceType);

    Iterable<InventoryOperation> getOperationLogsBySourceType(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate);
    Iterable<InventoryOperation> getAllLogActions(String companyId, String shopId, long startDate, long endDate);


    InventoryOperation getLastActionBySource(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType, InventoryOperation.SubSourceAction subSourceAction, String batchId);
}
