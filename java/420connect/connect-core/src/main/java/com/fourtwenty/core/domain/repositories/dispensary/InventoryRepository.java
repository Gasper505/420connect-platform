package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.caching.annotations.Cached;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/13/16.
 */
public interface InventoryRepository extends MongoShopBaseRepository<Inventory> {
    @Cached
    Inventory getInventory(String companyId, String shopId, String name);

    @Cached
    Iterable<Inventory> getInventoriesByStatus(String companyId, String shopId, List<ObjectId> inventories, boolean status);

    @Override
    @Cached
    HashMap<String, Inventory> listAllAsMap(String companyId, String shopId);

    @Override
    @Cached
    HashMap<String, Inventory> listAsMap(String companyId, String shopId);

    @Override
    @Cached
    Inventory get(String companyId, String id);

    @Cached
    Inventory getInventoryByExternalId(String companyId, String shopId, String externalId);
}
