package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;

import java.util.List;

/**
 * Created by Raja Dushyant Vashishtha
 */
public interface InventoryTransferHistoryRepository extends MongoShopBaseRepository<InventoryTransferHistory> {
    <E extends InventoryTransferHistory> SearchResult<E> findItemByModifiedDate(String companyId, String shopId, Long startDate, Long EndDate, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, boolean status, Class<E> clazz);

    <E extends InventoryTransferHistory> SearchResult<E> findItemByStatus(String companyId, String shopId, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, boolean status, Class<E> clazz);

    DateSearchResult<InventoryTransferHistoryResult> findItemByModifiedDate(String companyId, String shopId, Long startDate, Long EndDate);

    <E extends InventoryTransferHistory> E getTransferHistoryByNo(String companyId, String shopId, String transferNo, Class<E> clazz);

    <E extends InventoryTransferHistory> SearchResult<E> findItemByInventoryIdandStatus(String companyId, String shopId, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, String inventoryId, String term, Class<E> clazz);

    <E extends InventoryTransferHistory> SearchResult<E> findEmployeeItemByModifiedDate(String companyId, String shopId, Long startDate, Long EndDate, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, String inventoryId, String term, Class<E> clazz);

}
