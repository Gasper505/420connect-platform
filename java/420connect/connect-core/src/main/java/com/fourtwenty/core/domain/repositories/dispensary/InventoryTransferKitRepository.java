package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.InventoryTransferKit;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface InventoryTransferKitRepository extends MongoShopBaseRepository<InventoryTransferKit> {
    InventoryTransferKit getInventoryKitByName(String companyId, String shopId, String kitName);
}
