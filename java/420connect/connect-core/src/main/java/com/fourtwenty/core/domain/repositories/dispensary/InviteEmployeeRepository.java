package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.InviteEmployee;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by decipher on 22/11/17 11:58 AM
 * Raja (Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface InviteEmployeeRepository extends MongoCompanyBaseRepository<InviteEmployee> {
    InviteEmployee getEmployeeByEmail(String email);

    SearchResult<InviteEmployee> getInviteEmployeeList(String companyId, int skip, int limit, String sortOptions);
}
