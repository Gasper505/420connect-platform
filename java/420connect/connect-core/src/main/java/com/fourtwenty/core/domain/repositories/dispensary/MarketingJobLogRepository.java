package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface MarketingJobLogRepository extends MongoShopBaseRepository<MarketingJobLog> {
    MarketingJobLog getLog(String companyId, String fromNumber, String toNumber);
}
