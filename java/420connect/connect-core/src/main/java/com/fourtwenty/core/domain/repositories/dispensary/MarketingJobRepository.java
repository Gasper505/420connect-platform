package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

/**
 * Created by mdo on 7/6/17.
 */
public interface MarketingJobRepository extends MongoShopBaseRepository<MarketingJob> {
    <T extends MarketingJob> SearchResult<T> findMarketingJobsByStatus(String companyId, String shopId,
                                                                       List<MarketingJob.MarketingJobStatus> statuses,
                                                                       int start, int limit,
                                                                       Class<T> clazz);

    Iterable<MarketingJob> listMarketingJobsPending(long currentDate, MarketingJob.MarketingType marketingType);

    Iterable<MarketingJob> listAllPendingJobsByStatus(String companyId, String shopId, MarketingJob.MarketingJobStatus status);

    Iterable<MarketingJob> getMarketingJobsByType(MarketingJob.MarketingType blasts);

    List<MarketingJob> getMarketingJobsWithoutType();

    Iterable<MarketingJob> findMarketingJobsByStatus(MarketingJob.MarketingJobType jobType, MarketingJob.MarketingJobStatus status);
}
