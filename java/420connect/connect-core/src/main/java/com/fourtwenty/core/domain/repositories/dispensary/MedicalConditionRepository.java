package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 11/9/15.
 */
public interface MedicalConditionRepository extends BaseRepository<MedicalCondition> {
}
