package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.customer.MemberActivity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface MemberActivityRepository extends MongoShopBaseRepository<MemberActivity> {
    SearchResult<MemberActivity> getMemberActivities(String companyId, String shopId, String targetId, int start, int limit);

}
