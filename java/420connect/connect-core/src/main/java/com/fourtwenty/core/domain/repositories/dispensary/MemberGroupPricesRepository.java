package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.MemberGroupPrices;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 5/23/17.
 */
public interface MemberGroupPricesRepository extends MongoShopBaseRepository<MemberGroupPrices> {
    <E extends MemberGroupPrices> E getPricesForProductGroup(String companyId,
                                                             String shopId,
                                                             String productId,
                                                             String memberGroupId,
                                                             Class<E> clazz);

    <E extends MemberGroupPrices> Iterable<E> getPricesForProduct(String companyId, String shopId,
                                                                  String productId, Class<E> clazz);


    HashMap<String, MemberGroupPrices> getPricesMapForProductGroup(String companyId,
                                                                   String shopId,
                                                                   List<String> productIds,
                                                                   String memberGroupId);

    Iterable<MemberGroupPrices> getMemberGroupPricesByTemplate(String companyId, String shopId, String priceTemplateId);

    void updatePricesForTemplate(String companyId, String shopId, List<ObjectId> memberGroupPriceId, List<ProductPriceBreak> productPriceBreaks, List<ProductPriceRange> productPriceRanges);

    void updatePricingTemplate(String companyId, String shopId, List<ObjectId> groupPriceId, String pricingTemplateId);

    Iterable<MemberGroupPrices> getPricesForProducts(String companyId, String shopId, List<String> productIds);
}
