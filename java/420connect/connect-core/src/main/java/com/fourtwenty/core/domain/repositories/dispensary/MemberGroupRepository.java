package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 10/25/15.
 */
public interface MemberGroupRepository extends MongoShopBaseRepository<MemberGroup> {
    MemberGroup getMemberGroup(String companyId, String name);

    MemberGroup getMemberGroup(String companyId, String shopId, String name);

    MemberGroup getDefaultMemberGroup(String companyId);

    void setMemberGroupFalse(String companyId);

    void setMemberGroupFalse(String companyId, String shopId);

    <E extends MemberGroup> SearchResult<E> getActiveMemberGroups(String companyId, Class<E> clazz);

    <E extends MemberGroup> SearchResult<E> getActiveMemberGroupsShop(String companyId, String shopId, Class<E> clazz);

    void removeById(String companyId, String entityId, String name);
}
