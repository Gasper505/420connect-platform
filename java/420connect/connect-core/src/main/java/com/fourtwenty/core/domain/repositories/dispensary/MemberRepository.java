package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByField;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByFieldExtended;
import com.fourtwenty.core.reporting.model.reportmodels.MemberPerformance;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 9/27/15.
 */
public interface MemberRepository extends MongoShopBaseRepository<Member> {
    Member getMemberWithConsumerId(String companyId, String consumerUserId);

    Member getMemberWithDriversLicense(String companyId, String driversLicense);

    Iterable<Member> listWithOptions(List<String> companyIds, Member.MembershipStatus memberStatus, String projections);

    Iterable<Member> listWithOptionsTextOptIn(String companyId, Member.MembershipStatus memberStatus, String projections);

    long countWithOptionsTextOptIn(String companyId, Member.MembershipStatus memberStatus, String projections);


    Iterable<Member> listWithOptionsTextOptIn(String companyId, String shopId, Member.MembershipStatus memberStatus, String projections);

    long countWithOptionsTextOptIn(String companyId, String shopId, Member.MembershipStatus memberStatus, String projections);

    SearchResult<Member> searchMemberships(String companyId, String term, int start, int limit);

    SearchResult<Member> searchMemberships(String companyId, String term, String projections, int start, int limit);

    SearchResult<Member> searchMembershipsWithShop(String companyId, String shopId, String term, int start, int limit);

    SearchResult<Member> searchMembershipsWithShop(String companyId, String shopId, String term, String projections, int start, int limit);


    <E extends ShopBaseModel> SearchResult<E> searchMemberships(String companyId, String term, String projections, int start, int limit, Class<E> clazz);

    SearchResult<Member> getMembersWithDoctorId(String companyId, String doctorId, int start, int limit);

    SearchResult<Member> getMembersWithDoctorId(String companyId, String doctorId, String term, int start, int limit);


    SearchResult<Member> getMembersWithDoctorIdWithShop(String companyId, String shopId, String doctorId, int start, int limit);

    SearchResult<Member> getMembersWithDoctorIdWithShop(String companyId, String shopId, String doctorId, String term, int start, int limit);


    Iterable<Member> getMemberByEmail(String companyId, String memberEmail);

    Iterable<Member> getMemberByMemberGroup(String companyId, String memberGroupId);

    Iterable<Member> getMemberByMemberGroupsTextOptIn(String companyId, List<String> memberGroupIds);

    long getMemberByMemberGroupsCountTextOptIn(String companyId, List<String> memberGroupIds);


    Iterable<Member> getMembersOlderThan(long dob);

    long countMemberByEmail(String companyId, String memberEmail);

    Long getActiveMemberCount(String companyId);


    long countMemberByEmail(String companyId, String shopId, String memberEmail);

    Long getActiveMemberCount(String companyId, String shopId);

    Iterable<Member> getMembers(String companyId, List<ObjectId> memberIds);

    Iterable<Member> getMembersWithoutStartDate();

    //Update memberGroup
    void updateMemberGroup(String companyId, String memberGroupId, MemberGroup memberGroup);

    Iterable<MemberByField> getMembershipsByGroup(String companyId);

    Iterable<MemberByField> getMembershipsByGroup(String companyId, String shopId);

    Iterable<MemberByFieldExtended> getMembersByGroup(String companyId);

    Iterable<MemberByField> getMembershipsByGroupTextOptIn(String companyId);

    Iterable<MemberByField> getMembershipsByGroupEmailOptIn(String companyId);

    Iterable<MemberByField> getMembershipsByGroupTextOptIn(String companyId, String shopId);

    Iterable<MemberByField> getMembershipsByGroupEmailOptIn(String companyId, String shopId);

    ArrayList<MemberByField> getMemberCountByDoctor(String companyId);

    ArrayList<MemberByField> getMembershipsByCity(String companyId);

    Iterable<Member> getMembershipsExpiringSoon(String companyId, Long currentDate);

    Iterable<Member> getMembershipsExpiringSoon(String companyId, String shopId, Long currentDate);

    List<Member> getInactiveMembers(String companyId, Long currentDate);

    Iterable<Member> getInactiveMembersWithDays(String companyId, long olderThanDate);

    Iterable<Member> getInactiveMembersWithDaysTextOptIn(String companyId, long olderThanDate);

    long countInactiveMembersWithDaysTextOptIn(String companyId, long olderThanDate);

    long getInactiveMembersWithDaysCount(String companyId, long olderThanDate);

    long getInactiveMembersWithDaysCountTextOptIn(String companyId, long olderThanDate);

    // Set empty memberGroupId to memberGroup
    void setMemberGroupIfEmpty(String companyId, MemberGroup memberGroup);

    // Update
    void updateRecentBought(String companyId, String memberId, List<String> products);

    long getNewMembersFromToday(String companyId, Long startDate, Long endDate);

    Iterable<Member> getMembersWithStartDate(String companyId, long startDate, long endDate);

    Iterable<Member> getMembersWithStartDate(String companyId, String shopId, long startDate, long endDate);

    List<Member> getMembers(String companyId);

    List<Member> getInactiveMembersSpecificDays(String companyId, Long noOfDays, Long currentDate);

    void addLoyaltyPoints(String companyId, String memberId, double pointsEarned);

    void addLifetimePoints(String companyId, String memberId, double pointsEarned);

    long countActiveTextOptIn(String companyId, Member.MembershipStatus memberStatus);

    Member getMemberWithPhoneNumber(String companyId, String phoneNUmber);

    Iterable<Member> getActiveAndPendingMembers(String companyId, Long start, Long end);

    void bulkUpdateTextOptIn(String companyId, List<ObjectId> membersId, Boolean textOptInValue);

    void bulkUpdateEmailOptIn(String companyId, List<ObjectId> memberIds, Boolean smsOptIn);

    void bulkUpdateMemberStatus(String companyId, List<ObjectId> memberIds, Member.MembershipStatus membershipStatus);

    void bulkUpdateMemberType(String companyId, List<ObjectId> memberIds, Boolean medical, ConsumerType consumerType);

    void bulkUpdateMemberMarketingSource(String companyId, List<ObjectId> memberIds, String marketingSource);

    void bulkUpdateMemberGroup(String companyId, List<ObjectId> memberIds, MemberGroup memberGroup);

    void bulkUpdateMemberVerified(String companyId, List<ObjectId> memberIds, Boolean verified);

    void bulkAddNoteToMembers(String companyId, List<ObjectId> memberIds, Note note);

    void bulkDeleteMembers(String companyId, List<ObjectId> memberIds);

    Iterable<Member> getMembersWithNegativeLongForIdentification();

    List<Member> getMembersByCareGiver(String companyId, String careGiverId);

    void bulkUpdateMemberLoyalty(String companyId, List<ObjectId> membersList, Boolean enableLoyalty);

    HashMap<String, Member> getActiveMembers(String companyId, Member.MembershipStatus active);

    Iterable<Member> listByTextOptIn(String companyId, List<ObjectId> objectIds);

    long countByTextOptIn(String companyId, List<ObjectId> objectIds);

    Iterable<Member> listByTextOptIn(String companyId, String shopId, List<ObjectId> objectIds);

    long countByTextOptIn(String companyId, String shopId, List<ObjectId> objectIds);

    WriteResult updateCustomerRef(BasicDBObject query, BasicDBObject field);

    public List<Member> getMembersListWithoutQbRef(String companyId, long afterDate, long beforeDate);

    List<String> getAllMemberIds(String companyId);

    public Iterable<Member> listActiveAndPendingMembers(String companyId);

    void updateMemberRef(String companyId, String id, String qbCustomerRef, String editSequence, String qbListId);

    Iterable<Member> listAllTextOptIn(List<String> companyIds);

    Iterable<Member> listInActiveTextOptInAsMap(long lastVisitDate);

    Iterable<Member> listByTextOptIn(List<ObjectId> ids);

    Iterable<Member> getAllMemberByMemberGroupsTextOptIn(List<String> memberGroupId);


    Iterable<MemberPerformance> getMembersForBestPerformance(String companyId);

    HashMap<String, Member> findMembers(String companyId, String projection, ArrayList<ObjectId> objectIds);

    Iterable<Member> getMembersByEmails(String companyId, Set<String> emailIds);

    List<String> getAllMemberTags(String companyId);

    long countMembersTextOptInByTags(String companyId, List<String> memberTags);

    Iterable<Member> getMemberTextOptInByTags(String companyId, List<String> memberTags);

    void updateConsumerUserId(String companyId, String consumerUserId, String memberId);

    Iterable<Member> listByShopWithOptions(String companyId, String shopId, String projections);

    void bulkRemoveMemberTags(String companyId, String shopId, List<String> memberTagsToRemove);

    <E extends Member> List<E> getMembersByLimitsWithoutQbDesktopRef(String companyId, int start, int limit, Class<E> clazz);

    <E extends Member> List<E> getMembersByLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends Member> List<E> getQBExistingMember(String companyID, String shopId, int start, int limit, long endTime, Class<E> clazz);

    SearchResult<Member> getMemberList(String companyId, long startDate, long endDate, int skip, int limit, String sortBy);

    ArrayList<Member> getMembersByLicenceNo(String companyId, String licenceNumber, String sortBy);

    void updateEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef);

    <E extends Member> List<E> getMembersByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz);

    void updateMemberQbMapping(String companyID, String id, List<QBDataMapping> mapping);

    void bulkUpdateMemberIdVerified(String companyId, List<ObjectId> ids, boolean isVerified);

    void updateMemberAddress(String companyId, String memberId, Address address);
    void hardQbMappingRemove(String companyId, String shopId);


    long countEmptyEmailAndDobMember(String companyId, String shopId);

    <E extends Member> List<E> getEmptyEmailAndDobMember(String companyId, String shopId, int start, int limit, Class<E> clazz);

    long countMemberByDrivingLicense(String companyId, String licenseNumber);

    ArrayList<Member> getMembersByDrivingLicense(String companyId, List<String> licenseNumber);

    Member getMemberByEmailAddress(String companyId, String memberEmail);
}
