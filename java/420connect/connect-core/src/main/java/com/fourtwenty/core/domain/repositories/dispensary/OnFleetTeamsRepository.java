package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface OnFleetTeamsRepository extends MongoShopBaseRepository<OnFleetTeams> {
    OnFleetTeams getByCompanyAndShop(String companyId, String shopId);
}
