package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.Organization;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;
import java.util.List;

public interface OrganizationRepository extends BaseRepository<Organization> {

    List<Organization> getOrganizations();

    List<Organization> getOrganizationsIn(List<String> organizationIds);

    List<Organization> getOrganizationsByCompany(String companyId);
}

