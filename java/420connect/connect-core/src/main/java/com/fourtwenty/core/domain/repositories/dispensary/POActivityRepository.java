package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by decipher on 4/10/17 5:40 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface POActivityRepository extends MongoShopBaseRepository<POActivity> {
    SearchResult<POActivity> findItemsByPurchaseOrderId(String companyId, String shopId, int skip, int limit, String sortOption, String purchaseOrderId);
}
