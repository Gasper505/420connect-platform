package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 12/7/16.
 */
public interface PaidInOutItemRepository extends MongoShopBaseRepository<PaidInOutItem> {
    Iterable<PaidInOutItem> getPaidInOuts(String companyId,
                                          String shopId,
                                          String cdSessionId);

    SearchResult<PaidInOutItem> findPaidInOuts(String companyId,
                                               String shopId,
                                               String cdSessionId,
                                               int start,
                                               int limit);

    DateSearchResult<PaidInOutItem> getPaidInOutsTerminal(String companyId,
                                                          String shopId,
                                                          String terminalId,
                                                          int start,
                                                          int limit);
}
