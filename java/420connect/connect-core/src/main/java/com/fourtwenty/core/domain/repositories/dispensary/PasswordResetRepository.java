package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.PasswordReset;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

import java.util.List;

/**
 * Created by mdo on 8/3/16.
 */
public interface PasswordResetRepository extends BaseRepository<PasswordReset> {
    PasswordReset getPasswordReset(String resetCode);

    Iterable<PasswordReset> getPasswordResetForEmployee(String companyId, String employeeId);

    void setPasswordExpired(String companyId, String employeeId);

    void setPasswordExpiredForEmployees(String companyId, List<String> consumerUserIds);

}
