package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/1/17.
 */
public interface PrepackageProductItemRepository extends MongoShopBaseRepository<PrepackageProductItem> {
    PrepackageProductItem getPrepackageProduct(String companyId, String shopId, String productId, String prepackageId, String batchId);

    Iterable<PrepackageProductItem> getPrepackagesForProduct(String companyId, String shopId, String productId);

    <E extends PrepackageProductItem> Iterable<E> getPrepackagesForProduct(String companyId, String shopId, String productId, Class<E> clazz);

    Iterable<PrepackageProductItem> getPrepackagesForPrepackageId(String companyId, String shopId, String prepackageId);

    Iterable<PrepackageProductItem> getPrepackagesForProducts(String companyId, String shopId, List<String> productIds);

    HashMap<String, PrepackageProductItem> getPrepackagesForProductsAsMap(String companyId, String shopId, List<String> productIds);

    PrepackageProductItem getPrepackageProductById(String prepackageItemId);


    <E extends PrepackageProductItem> Iterable<E> getPrepackagesForProductIds(String companyId, String shopId, List<String> productIds, Class<E> clazz);

    HashMap<String, PrepackageProductItem> getPrepackagesForProductsByCompany(String companyId, List<String> productIds);

    Iterable<PrepackageProductItem> getPrepackagesForPrepackagesAndProduct(String companyId, String shopId, List<String> prepackageIds, String productId);

    <E extends PrepackageProductItem> Iterable<E> list(String companyId, List<ObjectId> prepackageLineItemIds, Class<E> clazz);

    void updateRunningQuantity(String companyId, String shopId, String entityId, Integer runningTotal);
}
