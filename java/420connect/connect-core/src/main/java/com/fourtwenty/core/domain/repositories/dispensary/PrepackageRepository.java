package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/15/17.
 */
public interface PrepackageRepository extends MongoShopBaseRepository<Prepackage> {
    Prepackage getPrepackage(String companyId, String shopId, String productId, String name);

    SearchResult<Prepackage> getPrepackages(String companyId, String shopId, String productId);

    HashMap<String, Prepackage> getPrepackagesForProductsAsMap(String companyId, String shopId, List<String> productIds);

    <E extends Prepackage> SearchResult<E> getPrepackages(String companyId, String shopId, String productId, Class<E> clazz);

    Prepackage getPrepackageById(String prepackageId);

    void removeById(String companyId, String entityId, String name);

    <E extends Prepackage> Iterable<E> getPrepackagesForProducts(String companyId, String shopId, List<String> productIds, Class<E> clazz);

    Iterable<Prepackage> getPrepackagesForProductsByCompany(String companyId, List<String> productIds);

    void updateRunningQuantity(String companyId, String shopId, String entityId, Integer runningTotal);
}
