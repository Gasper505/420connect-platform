package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.PricingTemplate;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface PricingTemplateRepository extends MongoShopBaseRepository<PricingTemplate> {
    PricingTemplate getTemplateByName(String companyId, String shopId, String name);
}
