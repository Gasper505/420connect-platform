package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 10/26/15.
 */
public interface ProductBatchRepository extends MongoShopBaseRepository<ProductBatch> {
    <E extends ProductBatch> SearchResult<E> getBatches(String companyId, String shopId, String productId, int start, int limit, String sortOptions, Class<E> clazz);

    SearchResult<ProductBatch> getBatches(String companyId, String shopId, int start, int limit);

    Iterable<ProductBatch> getBatchesForDateBracket(String companyId, String shopId, Long startDate, Long endDate);


    Iterable<ProductBatch> getBatchesLiveNonMetrcBatches(String companyId, String shopId, int start, int limit);

    HashMap<String, ProductBatch> getBatchesForProductsMap(String companyId, String shopId, List<String> productIds);

    HashMap<String, ProductBatch> getBatchesForProductsMap(String companyId, List<String> productIds);

    ProductBatch getBatchBySKU(String companyId, String shopId, String batchSKU);

    ProductBatch getLatestBatchForProduct(String companyId, String shopId, String productId);

    ProductBatch getLatestBatchForProductActive(String companyId, String shopId, String productId);

    ProductBatch getLatestBatchForProductWithMetrc(String companyId, String shopId, String productId);


    Iterable<ProductBatch> listProductBatchesSorted(String companyId, String shopId, String productId);

    List<ProductBatch> getProductBatches(String companyId, String shopId);

    Iterable<ProductBatch> getProductBatchesForLabel(String companyId, String shopId, String label, ProductBatch.TrackTraceSystem trackTraceSystem);

    long countProductBatchesForLabel(String companyId, String shopId, String label, ProductBatch.TrackTraceSystem trackTraceSystem);

    int setProductBatchArchived();

    Map<String, ProductBatch> getLatestBatchForProductList(String companyId, String shopId, List<String> productIdStrList);

    SearchResult<ProductBatchResult> getAllProductBatches(String companyId, String shopId, String sortOption, int start, int limit);

    SearchResult<ProductBatchResult> getAllArchivedProductBatches(String companyId, String shopId, String sortOption, int start, int limit);

    SearchResult<ProductBatchResult> getAllProductBatchesByState(String companyId, String shopId, boolean state, String sortOption, int start, int limit);

    <E extends ProductBatch> SearchResult<E> getAllProductBatchesByStatus(String companyId, String shopId, ProductBatch.BatchStatus status, String sortOption, int start, int limit, Class<E> clazz);

    SearchResult<ProductBatchResult> getAllScannedProductBatch(String companyId, String shopId, String metrcTagId, String productId, int start, int limit, String sortOption);

    SearchResult<ProductBatchResult> getAllDeletedProductBatch(String companyId, String shopId, String sortOption, int start, int limit);

    void bulkProductBatchDelete(String companyId, String shopId, List<ObjectId> batchIds);

    void bulkProductBatchStatusUpdate(String companyId, String shopId, List<ObjectId> batchIds, ProductBatch.BatchStatus status);

    void bulkProductBatchArchive(String companyId, String shopId, List<ObjectId> batchIds, boolean archive);

    void bulkProductBatchVoidStatusUpdate(String companyId, String shopId, List<ObjectId> batchIds, Boolean voidStatus);

    SearchResult<ProductBatchResult> getAllProductBatchesByVoidStatus(String companyId, String shopId, boolean voidStatus, String sortOption, int start, int limit);

    SearchResult<ProductBatch> getBatchesByTerm(String companyId, String shopId, int start, int limit, String sortOptions, String searchTerm);

    SearchResult<ProductBatch> getBatchesByBatchNo(String companyId, String shopId, int skip, int limit, String sortOptions, Long batchNo);

    SearchResult<ProductBatch> getBatchesByTermForStatus(String companyId, String shopId, int skip, int limit, String sortOptions, boolean state);

    <E extends ShopBaseModel> SearchResult<E> getAllProductBatchesByTermWithStatus(String companyId, String shopId, ProductBatch.BatchStatus status, String sortOption, int start, int limit, String searchTerm, Class<E> clazz);

    ProductBatch getBatchByProductAndSku(String companyId, String shopId, String productId, String sku);


    List<ProductBatch> listAllSorted(String sortOptions);

    ProductBatch getProductBatchByVendor(String companyId, String shopId, String batchId, String vendorId);

    void unArchiveBatch(String companyId, String shopId, String batchId);

    SearchResult<ProductBatch> getBatchesProductByBatchStatus(String companyId, String shopId, String productId, int start, int limit, String sortOptions, ProductBatch.BatchStatus status);

    List<ProductBatch> getLimitedBatchViewAsMap(String companyId, String shopId);

    void updateLiveQuantity(String companyId, String shopId, String batchId, double quantity);
    void updateMetrcTag(String batchId, String metrcTag, ProductWeightTolerance.WeightKey trackWeight);

    <E extends ProductBatch> List<E> getBatchesForProductListAndBatchStatus(String companyId, String shopId, List<String> productIds, List<ProductBatch.BatchStatus> statuses, Class<E> clazz);

    void updateProductBatchSku(String companyId, String shopId, String batchId, String sku);

    Map<String, ProductBatch> getOldestBatchForProductList(String companyId, String shopId, ArrayList<String> newArrayList);

    Map<String, ProductBatch> getBatchesByBatchesId(List<String> batchIdList);
}
