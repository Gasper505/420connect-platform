package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.caching.annotations.CacheInvalidate;
import com.fourtwenty.core.caching.annotations.Cached;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 10/14/15.
 */
public interface ProductCategoryRepository extends MongoShopBaseRepository<ProductCategory> {

    @CacheInvalidate
    void update(String companyId, String productCategoryId, ProductCategoryUpdateRequest request);

    @Cached
    ProductCategory getCategory(String companyId, String shopId, String name);

    @Cached
    ProductCategory getCategoryByPhotoKey(String companyId, String photoKey);

    @Override
    @Cached
    ProductCategory get(String companyId, String id);

    @Override
    @Cached
    HashMap<String, ProductCategory> listAsMap(String companyId, String shopId);

    @Override
    @Cached
    HashMap<String, ProductCategory> listAsMap();

    @Override
    @Cached
    DateSearchResult<ProductCategory> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate);

    <E extends ProductCategory> List<E> getProductCategoriesByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutQbDesktopRefWithoutError(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends ProductCategory> List<E> getQBExistProductCategories(String companyId, String shopId, int start, int limit, long startTime, Class<E> clazz);

    void updateProductCategoryQbErrorAndTime(String companyId, String shopId, String id, boolean qbErrored, long errorTime);

    void updateProductCategoryRef(String companyId, String shopId, String id, String qbDesktopRef, String editSequence, String qbListId);

    void updateEditSequenceAndRef(String companyId, String shopId, String id, String qbListId, String editSequence, String qbDesktopRef);

    void updateEditSequence(String companyId, String shopId, String qbListId, String editSequence);

    <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutSyncedStatus(String companyId, String shopId, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutQuantitySynced(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductCategoriesLimits(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductCategoriesByLimitsWithQbQuantityError(String companyId, String shopId, long errorTime, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductCategoriesLimitsWithQuantitySynced(String companyId, String shopId, long dateTime, Class<E> clazz);

    <E extends ProductCategory> List<E> getProductNewCategoriesLimitsWithQuantitySynced(String companyId, String shopId, long dateTime, Class<E> clazz);

    void updateProductCategoryQbQuantityErrorAndTime(String companyId, String id, boolean qbQuantityErrored, long errorQuantityTime);

    void updateProductCategoryQbQuantitySynced(String companyId, String id, boolean qbQuantitySynced);

    <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutSyncedQuantityStatus(String companyId, String shopId, Class<E> clazz);

    void hardRemoveQuickBookDataInProductCategories(String companyId, String shopId);

    void updateWmCategory(String companyId, String shopId, String categoryId, String wmCategory);
}
