package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductChangeLog;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.reporting.model.reportmodels.InventoryHistory;

import java.util.List;

/**
 * Created by mdo on 11/6/16.
 */
public interface ProductChangeLogRepository extends MongoShopBaseRepository<ProductChangeLog> {
    Iterable<ProductChangeLog> getProductChangeLogsDates(String companyId, String shopId, String productId, long startDate, long endDate);

    Iterable<InventoryHistory> getTopGroupProductChangeLog(String companyId, String shopId, long beforeEndDate);

    Iterable<InventoryHistory> getTopGroupProductChangeLog(String companyId, String shopId, long startDate, long beforeEndDate);

    Iterable<ProductChangeLog> getChangeLogsWithReferenceBegins(String companyId, String shopId, String refBegins, long afterDate, long beforeDate);

    ProductChangeLog createNewChangeLog(String employeeId, Product product, List<ProductPrepackageQuantity> curPrepackages, String reference);

}
