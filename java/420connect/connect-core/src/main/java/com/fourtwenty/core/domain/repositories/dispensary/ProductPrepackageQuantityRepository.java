package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/31/17.
 */
public interface ProductPrepackageQuantityRepository extends MongoShopBaseRepository<ProductPrepackageQuantity> {
    Iterable<ProductPrepackageQuantity> getQuantitiesForProduct(String companyId, String shopId, String productId);

    List<ProductPrepackageQuantity> getQuantitiesForProductAsList(String companyId, String shopId, String productId);

    Iterable<ProductPrepackageQuantity> getQuantitiesForProducts(String companyId, String shopId, List<String> productIds);

    Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackageItem(String companyId, String shopId, String prepackageItemId);

    Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackage(String companyId, String shopId, String prepackageId);

    ProductPrepackageQuantity getQuantity(String companyId, String shopId, String inventoryId, String prepackageItemId);

    void resetAllProductPrepackageQuantities(String companyId, String shopId);

    List<ProductPrepackageQuantity> getProductPrepackageQuantities(String companyId, String shopId);

    Iterable<ProductPrepackageQuantity> getQuantitiesForInventory(String companyId, String shopId, String inventoryId);

    Iterable<ProductPrepackageQuantity> getQuantitiesForInventoryWithQuantity(String companyId, String shopId, String inventoryId);

    HashMap<String, Integer> getQuantitiesForInventoryWithQuantityProductMap(String companyId, String shopId, String inventoryId);

    HashMap<String, ProductPrepackageQuantity> getQuantitiesForInventoryAsMap(String companyId, String shopId, String inventoryId);

    HashMap<String, ProductPrepackageQuantity> getQuantitiesForProductsAsMap(String companyId, String shopId, List<String> productIds);

    HashMap<String, Integer> getQuantitiesForProductsWithQuantityProductMap(String companyId, String shopId, List<String> productIds);

    Iterable<ProductPrepackageQuantity> getQuantitiesWithProductAndPrepackage(String companyId, String shopId, List<String> prepackageIds, String productId);

    HashMap<String, ProductPrepackageQuantity> getQuantitiesForPrepackageItems(String companyId, String shopId, String inventoryId, List<String> prepackageItemIdList);

    Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackageItemByInventory(String companyId, String shopId, String inventoryId, List<String> productIds);

    Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackageItem(String companyId, String shopId, List<String> prepackageItemIds);
}
