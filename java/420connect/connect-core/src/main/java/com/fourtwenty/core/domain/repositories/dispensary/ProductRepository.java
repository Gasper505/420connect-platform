package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.reporting.model.reportmodels.BestSellingBrands;
import com.fourtwenty.core.reporting.model.reportmodels.InventoryByStrain;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByProductCategory;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductResult;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.jongo.MongoCursor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Stephen Schmidt on 10/6/2015.
 */

public interface ProductRepository extends MongoShopBaseRepository<Product> {

    Iterable<Product> listItemsBreaksAfterCreated(long afterDate);

    SearchResult<Product> findProductsByCategoryId(String companyId, String shopId, String categoryId, String sortOption, int start, int limit);

    <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryId(String companyId, String shopId, String categoryId, String sortOption, int start, int limit, Class<E> eClass);

    <E extends Product> SearchResult<E> findProductsByCategoryId(String companyId, String shopId, String categoryId, List<String> tags, String sortOption, boolean active, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdSearch(String companyId, String shopId, String categoryId,
                                                                       String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdStrain(String companyId, String shopId, String categoryId, String strain, List<String> tags,
                                                                       String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdVendor(String companyId, String shopId, String categoryId,
                                                                       String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdAndVendorId(String companyId, String shopId, String categoryId, String strain,
                                                                            String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdStrainSearch(String companyId, String shopId, String categoryId,
                                                                             String strain, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdVendorSearch(String companyId, String shopId, String categoryId,
                                                                             String vendorId, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdStrainVendorSearch(String companyId, String shopId, String categoryId,
                                                                                   String strain, String vendorId, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    List<Product> getProducts(String companyId);

    <E extends Product> SearchResult<E> getProductsByVendorId(String companyId, String shopId, String vendorId, boolean active, Class<E> clazz);

    <E extends Product> SearchResult<E> findItemsSearch(String companyId, String shopId,
                                                        String term, List<String> tags, String sortOptions, int skip, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrain(String companyId, String shopId, String strain, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByVendorId(String companyId, String shopId, String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainAndVendor(String companyId, String shopId, String strain, String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainSearch(String companyId, String shopId, String strain,
                                                                 String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByVendorIdSearch(String companyId, String shopId, String vendorId,
                                                                     String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainVendorSearch(String companyId, String shopId, String strain, String vendorId,
                                                                       String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz);

    Iterable<Product> findProductsByIds(String companyId, String shopId, List<ObjectId> productIds);

    HashMap<String, Product> findProductsByIdsAsMap(String companyId, String shopId, List<ObjectId> productIds);

    Iterable<Product> getProductsForCategories(String companyId, List<String> categoryIds);

    Iterable<Product> getActiveProductsForCategories(String companyId, List<String> categoryIds);

    Iterable<Product> getProductsForCategory(String companyId, String categoryId);

    Iterable<Product> getProductsForCategoriesNewer(String companyId, String shopId, List<String> categoryIds, long afterDate);
    Iterable<Product> getProductsForCategoriesOlderThan(String companyId, String shopId, List<String> categoryIds, long afterDate);

    HashMap<String, Product> getProductsForCategoriesAsMap(String companyId, List<String> categoryIds);

    Iterable<Product> findProductsWithRanges(String companyId, String shopId);

    Iterable<InventoryByStrain> getInventoryByCannabisType(String companyId, String shopId);

    long countProductsForCategory(String companyId, String categoryId);

    void updateCachedCategory(String companyId, String shopId, String categoryId, ProductCategory category);

    void updateCachedAssets(String companyId, String productId, List<CompanyAsset> assets);

    void updateProductSKU(String companyId, String productId, String sku);

    void updateProductTaxInfo(String companyId, String productId, TaxInfo taxInfo);

    void updateProductPriceRanges(String companyId, String productId, List<ProductPriceRange> priceRanges);

    void updateProductPriceBreaks(String companyId, String productId, List<ProductPriceBreak> priceBreaks);

    void updateProductQuantities(String companyId, String productId, List<ProductQuantity> productQuantities);
    void updateInStockFlag(String companyId, String productId, boolean inStock);

    <E extends Product> SearchResult<E> findProductWithAvailableInventory(String companyId, String shopId, String inventoryId, int skip, int limit, Class<E> clazz);

    long countProductWithAvailableInventory(String companyId, String inventoryId);

    <E extends Product> SearchResult<E> getProductList(String companyId, String shopId, List<String> tags, String sortOptions, int skip, int limit, Class<E> clazz);
    <E extends Product> SearchResult<E> getProductListInStock(String companyId, String shopId, List<String> tags, String sortOptions, int skip, int limit, Class<E> clazz);

    List<Product> getActiveProductList(String companyId, String shopId);

    Product findProductWithAssetKey(String companyId, String assetKey);

    Iterable<Product> findProductsWithBreaks();

    void resetAllProductsStock(String companyId, String shopId);

    <E extends Product> SearchResult<E> findProductsByCategoryIdWithAvailableQuantity(String companyId, String shopId,
                                                                                      String categoryId, String sortOption,
                                                                                      int skip, int limit, Class<E> eClass);

    <E extends Product> SearchResult<E> findItemsWithAvailableQuantity(String companyId, String shopId, String sortOptions, int skip, int limit, Class<E> clazz);

    <E extends ShopBaseModel> SearchResult<E> findProductsByPattern(String companyId, String shopId, String sortOptions, String term, int start, int limit, Class<E> clazz);

    void bulkUpdateStatus(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean active);

    void bulkUpdateVendor(String companyId, String shopId, List<ObjectId> productObjectIdList, String vendorId);

    void bulkUpdateMixMatch(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean enableMixMatch);

    void bulkDeleteProduct(String companyId, String shopId, List<ObjectId> productObjectIdList);

    void bulkUpdateWeedMapStatus(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean weedMapsStatus);

    void bulkUpdateLowInventoryNotification(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean lowInventoryNotification, BigDecimal lowThreshold);

    void bulkUpdateTaxInfo(String companyId, String shopId, List<ObjectId> productObjectIdList, TaxInfo.TaxType taxType, TaxInfo.TaxProcessingOrder taxOrder, TaxInfo customTaxInfo);

    void bulkUpdateUnitPrice(String companyId, String shopId, List<ObjectId> productObjectIdList, BigDecimal unitPrice);

    void bulkUpdateWeightTolerancePrice(String companyId, String shopId, List<ObjectId> productObjectIdList, String weightToleranceId, BigDecimal price);

    void bulkUpdateCategory(String companyId, String shopId, List<ObjectId> productObjectIdList, ProductCategory category);

    void bulkOperationType(String companyId, String shopId, List<ObjectId> productObjectList, Product.ProductSaleType productSaleType);

    MongoCursor<Product> getVendorIdListByCategoryId(String companyId, String shopId, String categoryId, String projection);

    SearchResult<Product> getProducts(String companyId, String shopId, String sortOptions, int skip, int limit);

    void bulkUpdateProductTag(String companyId, String shopId, List<ObjectId> productObjectIdList, LinkedHashSet<String> tags);

    void bulkUpdateProductTaxtable(String companyId, String shopId, List<ObjectId> productObjectIdList, List<CompoundTaxTable> taxTables, TaxInfo.TaxType taxType);

    void updateProductTaxTable(String companyId, String productId, List<CompoundTaxTable> taxTables);

    void bulkRemoveProductTags(String companyId, String shopId, List<String> tagListToRemove);

    <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryIdAndTerm(String companyId, String shopId, String sortOption, String categoryId, String term, int start, int limit, Class<E> eClass);

    void bulkUpdateCannabisType(String companyId, String shopId, List<ObjectId> productObjectList, Product.CannabisType cannabisType);

    Iterable<BestSellingBrands> getTopSellingBrands(String companyId, String shopId, List<ObjectId> productIds, int start, int limit);

    Iterable<SalesByProductCategory> getSalesByCategory(String companyId, String shopId, List<ObjectId> productIds, int start, int limit);

    void bulkUpdatePriceIncludesExcise(String companyId, String shopId, List<ObjectId> productObjectList, Boolean priceIncludesExcise);

    void bulkUpdateBrandId(String companyId, String shopId, String brandId);


    void bulkUpdateShowInWidget(String companyId, String shopId, List<ObjectId> productObjectList, boolean showInWidget);

    void bulkUpdateBrandInProduct(String companyId, String shopId, List<ObjectId> productObjectList, String brandId);

    void bulkUpdateFlowerTypeInProduct(String companyId, String shopId, List<ObjectId> productObjectList, String flowerTYpe);

    WriteResult updateItemRef(BasicDBObject query, BasicDBObject field);

    List<Product> getProductForQB(String companyId, String shopId);

    Product findProductBySku(String companyId, String shopId, String sku);

    DateSearchResult<Product> findItemsByDate(String companyId, String shopId, long afterDate, long beforeDate, String projection);

    SearchResult<ProductResult> getProductsByVendorBrand(String companyId, String shopId, String vendorId, List<String> brands, int start, int limit);

    SearchResult<Product> findActiveProductsByCategoryId(String companyId, String shopId, String categoryId, String sort, int start, int limit, Boolean active);

    long getProductsByName(String companyId, String shopId, String name, String vendorId, String categoryId, String flowerType, Product.CannabisType cannabisType);

    <E extends Product> DateSearchResult<E> findProductsByTagsAndCategory(String companyId, String shopId, String categoryId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByStrainAndVendor(String companyId, String shopId, String categoryId, String vendorId, String strain, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByCategoryAndVendorId(String companyId, String shopId, String categoryId, String vendorId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByCategoryAndStrain(String companyId, String shopId, String categoryId, String strain, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByTags(String companyId, String shopId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByStrainAndVendor(String companyId, String shopId, String strain, String vendor, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByTagsAndVendor(String companyId, String shopId, String vendorId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    <E extends Product> DateSearchResult<E> findProductsByTagsAndStrain(String companyId, String shopId, String strain, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz);

    public List<Product> getProductsListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate);

    <E extends ShopBaseModel> SearchResult<E> findItemsByState(String companyId, String shopId, String sortOptions, int skip, int limit, Class<E> clazz, boolean status);

    void updateProductRef(String companyId, String id, String qbDesktopRef, String editSequence, String qbListId);

    <E extends ShopBaseModel> SearchResult<E> findProductsByPatternAndState(String companyId, String shopId, String sortOptions, String term, int start, int limit, Class<E> clazz, boolean state);

    <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryIdAndState(String companyId, String shopId, String categoryId, String sortOption, int start, int limit, Class<E> eClass, boolean state);

    <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryIdAndTermAndState(String companyId, String shopId, String sortOption, String categoryId, String term, int start, int limit, Class<E> eClass, boolean state);

    Iterable<Product> getAllProductByCompany(String companyId);

    void bulkUpdateWeightPerUnit(String companyId, String shopId, List<ObjectId> productId, Product.WeightPerUnit weightPerUnit, BigDecimal customGrams, Product.CustomGramType customGramType);

    void updateProductPricing(String companyId, String productId, List<ProductPriceRange> priceRanges, List<ProductPriceBreak> priceBreaks, String pricingTemplateId, BigDecimal unitPrice);

    void removeProductPricing(String companyId, String shopId, String pricingTemplateId);

    void resetPricingTemplateByProductCategory(String companyId, String shopId, String productCategoryId, List<ObjectId> productIds);

    Iterable<Product> findProductsByPricingTemplateId(String companyId, String shopId, String pricingTemplateId);

    void updateProductPrice(String companyId, String shopId, Product product);

    HashMap<String, Product> getLimitedProductViewAsMap(String companyId, String shopId);

    <E extends Product> SearchResult<E> findProductsByCategoryIdStrainBrandSearch(String companyId, String shopId, String categoryId,
                                                                             String strain, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdStrainBrand(String companyId, String shopId, String categoryId, String strain, List<String> tags,
                                                                       String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdVendorBrandSearch(String companyId, String shopId, String categoryId,
                                                                             String vendorId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdVendorBrand(String companyId, String shopId, String categoryId,
                                                                       String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdStrainVendorBrandSearch(String companyId, String shopId, String categoryId,
                                                                                   String strain, String vendorId, String term, List<String> tags, String brandIds, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryIdAndVendorIdAndBrand(String companyId, String shopId, String categoryId, String strain,
                                                                            String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainBrandSearch(String companyId, String shopId, String strain,
                                                                 String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainBrand(String companyId, String shopId, String strain, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByVendorIdBrandIdSearch(String companyId, String shopId, String vendorId,
                                                                     String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByVendorIdBrand(String companyId, String shopId, String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainVendorBrandSearch(String companyId, String shopId, String strain, String vendorId,
                                                                            String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsStrainAndVendorBrand(String companyId, String shopId, String strain, String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryAndBrandSearch(String companyId, String shopId, String categoryId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByCategoryAndBrand(String companyId, String shopId, String categoryId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByBrandSearch(String companyId, String shopId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    <E extends Product> SearchResult<E> findProductsByBrand(String companyId, String shopId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz);

    void bulkRemoveBrand(String companyId, String shopId, List<String> brandIds);

    SearchResult<Product> findItemsWithDate(String companyId, String shopId, long startTime, long endTime, int skip, int limit, String sortOptions);

    void updateEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef);

    <E extends Product> List<E> getProductsLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends Product> List<E> getNewProductsLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends Product> List<E> getQBExistProducts(String companyId, String shopId, int start, int limit, long startTime, Class<E> clazz);

    void updateEditSequenceAndRef(String companyId, String shopId, String id, String qbListId, String editSequence, String qbDesktopRef);

    <E extends Product> List<E> getProductsByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz);

    void updateProductQbErrorAndTime(String companyID, String id, boolean qbErrored, long errorTime, boolean qbQuantitySynced, boolean qbQuantityErrored);

    void updateQBQuantitySyncedStatus(String companyId, String shopId, List<ObjectId> productIds, Boolean status);

    <E extends Product> List<E> getQBExistProducts(String companyId, List<ObjectId> productIds, Class<E> clazz);

    <E extends Product> List<E> getProductsLimitsWithoutSyncedStatus(String companyId, String shopId, Class<E> clazz);

    <E extends Product> List<E> getProductsLimitsWithoutQuantitySynced(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends Product> List<E> getNewProductsLimitsWithoutQuantitySynced(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends Product> List<E> getProductsByLimitsWithQbQuantityError(String companyId, String shopId, long errorTime, Class<E> clazz);

    void updateProductQbQuantityErrorAndTime(String companyID, String id, boolean qbQuantityErrored, long errorQuantityTime);

    void updateProductQbQuantitySynced(String companyID, String id, boolean qbQuantitySynced);

    void updateComplianceId(String companyID, String id, String complianceId);

    void hardRemoveQuickBookDataInProducts(String companyId, String shopId);

    SearchResult<Product>  listAllByShopAndTerm(String companyId, String shopId, String searchTerm);

    HashMap<String, Product> getProductsWithIdsAndTerm(String companyId, String shopId, List<ObjectId> productIds, String searchTerm);

    void updateWmThreshold(String companyId, List<ObjectId> productIds, Double wmThreshold);
    void updateLastWMSyncTime(String companyId, List<ObjectId> productIds, long lastWPSyncTime);
    void updateBrandsInProduct(String companyId, String shopId, LinkedHashSet<String> brandIds, String vendorId);
    void updateLeaflySyncTimeAndStatus(String companyId, Set<ObjectId> productIds, long lastLeaflySyncTime, Boolean lastLeaflySyncStatus);

    Iterable<Product> listAfterLastLeaflySync(String companyId, List<ObjectId> ids, long lastSync);

}
