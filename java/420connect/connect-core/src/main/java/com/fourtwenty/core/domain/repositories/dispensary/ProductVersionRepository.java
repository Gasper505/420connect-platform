package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface ProductVersionRepository extends MongoShopBaseRepository<ProductVersion> {
    
    ProductVersion findLastProductVersionByProductId(String companyId,String shopId, String productId);
}
