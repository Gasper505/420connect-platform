package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.caching.annotations.Cached;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;

import java.util.HashMap;

/**
 * Created by mdo on 10/25/15.
 */
public interface ProductWeightToleranceRepository extends MongoCompanyBaseRepository<ProductWeightTolerance> {

    @Cached
    ProductWeightTolerance getToleranceForWeight(String companyId, ProductWeightTolerance.WeightKey weightKey);

    @Cached(returnClasses = {HashMap.class, String.class, ProductWeightTolerance.class})
    HashMap<String, ProductWeightTolerance> getActiveTolerancesAsMap(String companyId);

    @Override
    @Cached
    DateSearchResult<ProductWeightTolerance> findItemsWithDate(String companyId, long afterDate, long beforeDate);

    @Override
    @Cached(returnClasses = {HashMap.class, String.class, ProductWeightTolerance.class})
    HashMap<String, ProductWeightTolerance> listAsMap(String companyId);

    void enableWeightTolerance(String weightToleranceId, boolean state);

    ProductWeightTolerance getToleranceByName(String companyId, String name);
}
