package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.loyalty.PromoUsage;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

import java.util.List;

/**
 * Created by mdo on 1/3/17.
 */
public interface PromoUsageRepository extends MongoShopBaseRepository<PromoUsage> {

    long countForMember(String companyId, String shopId, String promoId, String memberId);

    long countForPromo(String companyid, String shopId, String promoId);

    Iterable<PromoUsage> listForPromo(String companyid, String shopId, String promoId);

    PromoUsage getUserFor(String companyId, String shopId, String promoId, String memberId, String transId);

    PromoUsage getPromoUsage(String companyId, String shopId, String promoId, long startDate, long endDate);

    List<PromoUsage> getPromoUsages(String companyId, String shopId, long startDate, long endDate);
}
