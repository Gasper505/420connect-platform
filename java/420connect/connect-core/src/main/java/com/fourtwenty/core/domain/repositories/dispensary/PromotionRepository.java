package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;

import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 1/3/17.
 */
public interface PromotionRepository extends MongoShopBaseRepository<Promotion> {
    Promotion getPromotionByPromoCode(String companyId, String shopId, String promocode);

    List<Promotion> getPromotionByPromoCode(String companyId, String shopId, Set<String> promoCodes);

    Promotion getPromotionByName(String companyId, String shopId, String name);

    List<Promotion> getPromotions(String companyId, String shopId);

    DBCursor getPromotionsCursor(BasicDBObject query, BasicDBObject field);

    Promotion getPromotionByNameExceptThis(String companyId, String shopId, String promotionId, String name);
}
