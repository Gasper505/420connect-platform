package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by decipher on 3/10/17 3:40 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface PurchaseOrderRepository extends MongoShopBaseRepository<PurchaseOrder> {
    SearchResult<PurchaseOrderItemResult> listAllByStatus(String companyId, String shopId, int start, int limit, String sortOptions, String status, PurchaseOrder.CustomerType customerType);

    SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(String companyId, String shopId, String sortOptions, int start, int limit, PurchaseOrder.CustomerType customerType);

    PurchaseOrderItemResult getPOById(String companyId, String purchaseOrderId);

    PurchaseOrder getBOByPOId(String companyId, String poId);

    void updatePurchaseOrderStatus(String poId, PurchaseOrder.PurchaseOrderStatus status);

    SearchResult<PurchaseOrderItemResult> listAllArchivedPO(String companyId, String shopId, PurchaseOrder.CustomerType customerType, int start, int limit, String sortOptions);

    Iterable<PurchaseOrderItemResult> getBracketSalesByCustomerType(String companyId, String shopId, String customerType, int size);

    SearchResult<PurchaseOrderItemResult> listAllByTerm(String companyId, String shopId, int start, int limit, String sortOptions, String searchTerm, PurchaseOrder.CustomerType customerType, final List<String> vendorIds);

    SearchResult<PurchaseOrderItemResult> listAllByTermWithStatus(String companyId, String shopId, int start, int limit, String sortOptions, String searchTerm, PurchaseOrder.CustomerType customerType, final String status, final List<String> vendorIds);

    SearchResult<PurchaseOrderItemResult> listAllByVendorId(String companyId, String shopId, int start, int limit, String vendorId, String sortOptions);

    PurchaseOrder getPOByPOId(String companyId, String shopId, String poId);

    Iterable<PurchaseOrder> getAllPurchaseOrderById(String companyId, String shopId, List<ObjectId> purchaseOrderIds);

    public WriteResult updatePurchaseOrderRef(final BasicDBObject query, final BasicDBObject field);

    <E extends ShopBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz, String projection, List<PurchaseOrder.PurchaseOrderStatus> status);

    <E extends ShopBaseModel> SearchResult<E> listAllArchiveByTerm(final String companyId, final String shopId, final PurchaseOrder.CustomerType customerType, final int start, final int limit, final String sortOptions, final String term, final Class<E> clazz);

    Iterable<PurchaseOrder> getPurchaseOrderByStatus(String companyId, String shopId, List<PurchaseOrder.PurchaseOrderStatus> status, PurchaseOrder.CustomerType customerType, long afterDate, long beforeDate);

    Iterable<PurchaseOrder> getPurchaseOrderByStatusPurchaseDate(String companyId, String shopId, List<PurchaseOrder.PurchaseOrderStatus> status, PurchaseOrder.CustomerType customerType, long afterDate, long beforeDate);


    SearchResult<PurchaseOrder> getAllPurchaseOrderByProductId(String companyId, String shopId, String sortOptions, List<String> productIds);

    public List<PurchaseOrder> getPurchaseOrdersListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate);

    SearchResult<PurchaseOrderItemResult> getAllPurchaseOrderByDate(String companyId, String shopId, String sortOptions, int start, int limit, PurchaseOrder.CustomerType customerType, long beforeDate, long afterDate);

    public Iterable<PurchaseOrder> getPurchasOrderByStatus(String companyId, String shopId);

    SearchResult<PurchaseOrder> getPurchaseOrdersByProductId(String companyId, String shopId, String productId, int start, int limit, String sortOptions);


    <E extends PurchaseOrder> E getPurchaseOrderByNo(String companyId, String shopId, String poNumber, Class<E> clazz);

    <E extends PurchaseOrder> DateSearchResult<E> findItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit, Class<E> clazz);

    long countPOByAdjustmentId(String companyId, String shopId, String adjustmentId);
    void updateRecievedDate(String purchaseOrderId , long receivedDate);

    <E extends PurchaseOrder> SearchResult<E> getLimitedPurchaseOrderWithoutQbDesktopRef(String companyId, String shopId, long syncTime, int start, int limit, Class<E> clazz);

    <E extends PurchaseOrder> SearchResult<E> getLimitedPurchaseOrderWithoutQbDesktopRef(String companyId, String shopId,long startDate, long endDate, Class<E> clazz);

    void updateQbDesktopPurchaseOrderRef(String companyId, String shopId, String poNumber, String editSequence, String listId);

    void updateQbDesktopPurchaseOrderBillRef(String companyId, String shopId, String id, boolean billRef);

    HashMap<String, PurchaseOrder> listAsMapByPoNumber(String companyId, List<String> poNumbers);

    void updatePurchaseOrderQbErrorAndTime(String companyId, String refNo, Boolean qbErrored, long errorTime);

    <E extends PurchaseOrder> List<E> getPOByLimitsWithQBError(String companyId, String shopId, Long errorTime, Class<E> clazz);

    <E extends PurchaseOrder> SearchResult<E> getPOByLimitsWithoutBillRef(String companyId, String shopId, long syncTime, int start, int limit, Class<E> clazz);

    <E extends PurchaseOrder> List<E> getPurchaseOrderByLimitsWithDateTime(String companyId, String shopId, long startDate, long endDate, Class<E> clazz);

    <E extends PurchaseOrder> List<E> getQBExistPurchaseOrders(String companyId, String shopId, int start, int limit, long startTime, Class<E> clazz);

    Map<String, PurchaseOrder> listAsMapByPONumber(String companyId, List<String> poNumbers);

    PurchaseOrder getPOByPONumber(final String companyId, final String poNumber);

    PurchaseOrder updatePurchaseOrder(String companyId, String id, PurchaseOrder purchaseOrder);

    SearchResult<PurchaseOrderItemResult> getAllPurchaseOrderWithSorting(String companyId, String shopId, String sortOptionStr, int start, int limit, PurchaseOrder.CustomerType customerType);

    Iterable<PurchaseOrder> getPurchaseOrderByStatusWithPODate(String companyId, String shopId, List<PurchaseOrder.PurchaseOrderStatus> statusList, PurchaseOrder.CustomerType customerType, long afterDate, long beforeDate);

    long countPurchaseOrderByProductId(String companyId, String shopId, List<String> productIds);

    void hardRemoveQuickBookDataInPOs(String companyId, String shopId);
    void updatePurchaseOrderStatus(String companyId, String purchaseOrderId, PurchaseOrder.PurchaseOrderStatus status, PurchaseOrder.PurchaseOrderStatus purchaseOrderPreviousStatus);


    long countActivePurchaseOrderByVendor(String companyId, String vendorId);

    <E extends BaseModel> List<E> getPurchaseOrderByStatus(String companyId, String shopId, PurchaseOrder.PurchaseOrderStatus status, Class<E> clazz);

    <E extends BaseModel> List<E> getPOByVendorAndProductStatus(String companyId, String shopId, List<String> vendorIds, List<String> productIds, PurchaseOrder.PurchaseOrderStatus status, Class<E> clazz);
}
