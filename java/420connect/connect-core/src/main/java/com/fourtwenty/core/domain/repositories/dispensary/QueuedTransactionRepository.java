package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

/**
 * Created by mdo on 4/30/16.
 */
public interface QueuedTransactionRepository extends MongoShopBaseRepository<QueuedTransaction> {
    long getCountStatus(String companyId, String shopId, QueuedTransaction.QueueStatus queueStatus);

    long getQueuedTransactionsCount(QueuedTransaction.QueueStatus queueStatus);

    Iterable<QueuedTransaction> getQueuedTransactions(QueuedTransaction.QueueStatus queueStatus);

    Iterable<QueuedTransaction> getQueuedTransactions(QueuedTransaction.QueueStatus queueStatus, long requestTime);

    void updateRequestTime(String queuedTransactionId);

    long getQueuedTransactionsCount(QueuedTransaction.QueueStatus queueStatus, long requestTime);

    QueuedTransaction getRecentCompletedTransaction(String companyId, String shopId, String transactionId);

    QueuedTransaction getRecentQueuedTransaction(String companyId, String shopId, String transactionId);
    QueuedTransaction getRecentQueuedTransactionModified(String companyId, String shopId, String transactionId);

    List<QueuedTransaction> getQueuedTransactionByManifest(String companyId, String shopId, String invoiceId, String manifestId, String sortOptions);

    SearchResult<QueuedTransaction> getQueuedTransactionsBySourceId(String companyId, String shopId, String transactionId);

}
