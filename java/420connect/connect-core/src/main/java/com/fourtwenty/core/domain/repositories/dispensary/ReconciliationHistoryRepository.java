package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.product.ReconciliationHistory;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ReconciliationHistoryResult;

public interface ReconciliationHistoryRepository extends MongoShopBaseRepository<ReconciliationHistory> {
    SearchResult<ReconciliationHistoryResult> getAllReconciliationHistory(String companyId, String shopId, String sortOptions, int start, int limit);

    <E extends ReconciliationHistory> E getReconciliationHistory(String companyId, String shopId, String historyId, Class<E> clazz);

    Iterable<ReconciliationHistory> getReconciliationHistoryByDateRange(String companyId, String shopId, Long startTime, Long endTime);

    <E extends ReconciliationHistory> E getReconciliationHistoryByNo(String companyId, String shopId, Long reconciliationNo, Class<E> clazz);

    ReconciliationHistory getLastReconciliationHistory(String companyId, String shopId);
}
