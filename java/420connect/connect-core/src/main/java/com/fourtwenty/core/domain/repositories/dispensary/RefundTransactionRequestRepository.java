package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.transaction.RefundTransactionRequest;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by mdo on 6/12/18.
 */
public interface RefundTransactionRequestRepository extends MongoShopBaseRepository<RefundTransactionRequest> {
}
