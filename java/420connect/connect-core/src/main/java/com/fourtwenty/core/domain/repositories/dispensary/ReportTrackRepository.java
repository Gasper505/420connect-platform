package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.reporting.ReportType;

import java.util.List;

public interface ReportTrackRepository extends MongoShopBaseRepository<ReportTrack> {
    ReportTrack getReportTrackByType(String companyId, String shopId, ReportType type);

    List<ReportTrack> getFrequentlyUsedReport(String companyId, String shopId, int limit, String sortOptions, List<ReportType> dashboardNames);
}
