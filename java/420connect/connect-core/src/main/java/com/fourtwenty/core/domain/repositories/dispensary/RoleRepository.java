package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

/**
 * Created by mdo on 6/15/16.
 */
public interface RoleRepository extends MongoCompanyBaseRepository<Role> {
    Role getRoleByName(String companyId, String roleName);

    Iterable<Role> getRoleByName(String roleName);
}
