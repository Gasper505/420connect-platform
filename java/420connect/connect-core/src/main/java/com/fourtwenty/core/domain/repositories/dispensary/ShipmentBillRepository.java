package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ShipmentBillRequestResult;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

public interface ShipmentBillRepository extends MongoShopBaseRepository<ShipmentBill> {
    ShipmentBill getShipmentBillById(final String shipmentId);

    ShipmentBill getShipmentBillByPoId(final String poId);

    SearchResult<ShipmentBillRequestResult> getAllShipmentBills(final String companyId, final String shopId, final String sortOption, final int start, final int limit, final PurchaseOrder.CustomerType customerType);

    SearchResult<ShipmentBillRequestResult> getArchivedShipmentBills(String companyId, String shopId, String sortOption, int skip, int limit);

    Iterable<ShipmentBill> getShipmentBills(String companyId, String shopId, Long timeZoneStartDateMillis, Long timeZoneEndDateMillis, String sortOptions);

    public WriteResult updateBillRef(final BasicDBObject query, final BasicDBObject field);

    public List<ShipmentBill> getBillsListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate);


    HashMap<String, ShipmentBill> listShipmentBillByPOAsMap(String companyId, String shopId, List<String> purchaseOrderId);

    void updatePaymentStatus(String shipmentBillId, ShipmentBill.PaymentStatus paymentStatus);

    <E extends ShipmentBill> SearchResult<E> getShipmentBillByPOWithoutQbRef(String companyId, String shopId, long syncTime, List<String> poIds, Class<E> clazz);

    void updateDesktopBillQbErrorAndTime(String companyId, String shopId, String refNo, Boolean qbErrored, long errorTime);

    void updateQbDesktopBillRef(String companyId, String shopId, String ref, String txnId);

    void updateReceivedDate(String id, long receivedDate);

    <E extends ShipmentBill> SearchResult<E> getBillByLimitsWithQBPaymentReceivedError(String companyId, String shopId, ShipmentBill.PaymentStatus paymentStatus, long syncTime, long lastSyncTime, Class<E> clazz);

    <E extends ShipmentBill> SearchResult<E> getBillByLimitsWithoutQBPaymentReceived(String companyId, String shopId, ShipmentBill.PaymentStatus paymentStatus, long syncTime, int start, int limit, Class<E> clazz);

    void updateQbDesktopPaymentReceivedRef(String companyId, String shopId, String id, boolean paymentReceivedRef, boolean qbPaymentReceivedError, long qbPaymentReceivedErroredTime);

    void updateShipmentBillPaymentHistory(String companyID, String id, List<ShipmentBillPayment> paymentHistory);

    <E extends ShipmentBill> List<E> getBillByIds(String companyId, String shopId, List<ObjectId> billIds, Class<E> clazz);

    void hardRemoveQuickBookDataInBills(String companyId, String shopId);

    void hardRemoveQuickBookDataInBillPayments(String companyId, String shopId);



}
