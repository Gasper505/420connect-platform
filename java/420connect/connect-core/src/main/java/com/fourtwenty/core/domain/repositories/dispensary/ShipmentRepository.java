package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import java.util.List;

public interface ShipmentRepository extends MongoShopBaseRepository<Shipment> {
    SearchResult<Shipment> listAllByStatus(String companyId, String shopId, int skip, int limit, String sortOptions, String status);

    SearchResult<Shipment> getAllShipment(String companyId, String shopId, int start, int limit, String sortOptions);

    Shipment getShipmentById(String companyId, String shipmentId);

    List<Shipment> getAvailableShipment(String companyId, String shopId);

	Shipment getShipmentByShippingManifestId(String shippingManifestId);    
}