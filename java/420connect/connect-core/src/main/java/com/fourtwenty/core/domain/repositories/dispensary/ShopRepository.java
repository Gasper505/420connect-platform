package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.CustomShopResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.List;
/**
 * Created by mdo on 9/6/15.
 */
public interface ShopRepository extends MongoCompanyBaseRepository<Shop> {
    void setShopTimeZone(String timeZone);

    void updateLastMembersSyncDate(String companyId, String shopId);
    void updateTerminalSaleOption(String companyId, String shopId, Shop.TerminalSalesOption salesOption);

    void updateSnapshoptTime(String companyId, String shopId);
    void updateNextSnapshoptTime(String companyId, String shopId, long nextSnapshotTime);

    void setShopShortIdentifier(String companyId, String shopId, String shortIdentifier);

    boolean existsWithIdentifier(String shortIdentifier);

    void updateTwilioPhoneNumber(String companyId, String shopId, String twilioPhoneNumber);

    SearchResult<Shop> nearbyShops(long lat, long lng, int radius, int skip, int limit);

    Shop getShopByPhoneNumber(String phoneNUmber);

    <E extends Shop> SearchResult<E> findShopsByAppTarget(String companyId, int skip, int limit, CompanyFeatures.AppTarget appTarget, Class<E> clazz);

    void updateEnableTookan(String shopId, boolean enable);

    Shop getShopByTwilioNumber(String number);

    void updateProductTags(String companyId, String shopId, LinkedHashSet<String> productsTag);

    void updateShopInfo(String companyId, String shopId, Map<String, Object> shopInfoMap);

    void updateOrderTags(String companyId, String shopId, Set<String> shopOrderTags);

    void updateMarketingSources(String companyId, String shopId, LinkedHashSet<String> marketingSources);

    void updateMemberTags(String companyId, String shopId, Set<String> memberTags);

    SearchResult<CustomShopResult> getAllShops(int skip, int limit);

    Iterable<ShopResult> getEmployeeShops(Employee employee);

    Iterable<ShopResult> getAllShopsInList(List<String> shopList);
}