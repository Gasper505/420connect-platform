package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

import java.util.List;

/**
 * Created by Stephen Schmidt on 12/28/2015.
 */
public interface SignedContractRepository extends MongoCompanyBaseRepository<SignedContract> {

    List<SignedContract> getSignedContracts(String companyId, String shopId);

    SignedContract getSignedContractByMembership(String companyId, String shopId,
                                                 String membershipId, Boolean active);
}

