package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface SpringbigRepository extends MongoShopBaseRepository<SpringBigInfo> {
    SpringBigInfo getInfoByShop(String companyId, String shopId);
}
