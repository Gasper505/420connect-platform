package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.plugins.TVDisplay;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by decipher on 14/10/17 2:44 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface TVDisplayRepository extends MongoShopBaseRepository<TVDisplay> {
    TVDisplay getTVDisplayByIdentifierAndTVNo(String uniqueIdentifier, Long tvNo);

    <E extends TVDisplay> E getTVDisplayByIdentifierAndTVNo(String uniqueIdentifier, Long tvNo, Class<E> clazz);

    TVDisplay getTvDisplayByIndentifier(final String uniqueIdentifier);

    boolean existsWithIdentifier(final String uniqueIdentifier);

    void removeAndUnpublish(String companyId, String entityId);
}
