package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.EmployeeRecentLocationResult;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 7/3/16.
 */
public interface TerminalLocationRepository extends MongoShopBaseRepository<TerminalLocation> {
    Iterable<TerminalLocation> getTermLocations(String companyId, String shopId, String terminalId, int limit);

    Iterable<TerminalLocation> getTermLocationsAfter(String companyId, String shopId, Long startDate, String sortOption);

    Iterable<TerminalLocation> getTermLocationsWithDate(String companyId, String shopId, Long startDate, Long endDate, String sortOption);

    Iterable<TerminalLocation> getTermLocationsWithDate(String companyId, String shopId, Long startDate, Long endDate, String sortOption, String employeeId);

    Iterable<TerminalLocation> listByShopSortAndEmployee(String companyId, String shopId, String sortOptions, int skip, int limit, String employeeId);

    TerminalLocation getRecentLocation(String companyId, String shopId, String employeeId);

    Iterable<TerminalLocation> listAllBySort(String sortOptions);

    List<TerminalLocation> getLocationByTransaction(String companyId, String shopId, String sortOptions, String assignedEmployeeId, String terminalId, int start, int limit, Long startRouteDate, Long endRouteDate);

    HashMap<String, EmployeeRecentLocationResult> getEmployeesRecentLocation(String companyId, String shopId, List<String> employeeIds);

    HashMap<String, TerminalLocation> getRecentLocationByEmployee(String companyId, String shopId, List<String> employeeIds);
}
