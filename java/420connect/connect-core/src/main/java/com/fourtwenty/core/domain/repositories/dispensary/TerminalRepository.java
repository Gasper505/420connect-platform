package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalUpdateNameRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

/**
 * Created by mdo on 10/9/15.
 */
public interface TerminalRepository extends MongoShopBaseRepository<Terminal> {
    void updateTerminalName(String companyId, String terminalId, TerminalUpdateNameRequest request);

    Terminal getTerminalByDeviceId(String companyId, String deviceId);

    Terminal getTerminalByName(String companyId, String name);

    Boolean isNameAlreadyExist(String name);

    Terminal getTerminalById(String companyId, String terminalId);

    SearchResult<Terminal> getAllTerminalsByState(String companyId, String shopId, boolean state, String sortOption, int start, int limit);

    void updateCheckoutType(List<String> fulfilmentShopIds, Shop.ShopCheckoutType checkoutType);
}
