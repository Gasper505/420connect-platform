package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.EmployeeSession;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by mdo on 6/24/16.
 */
public interface TimeCardRepository extends MongoShopBaseRepository<TimeCard> {
    TimeCard getActiveTimeCard(String companyId, String shopId, String employeeId);

    Iterable<TimeCard> getActiveTimeCards(String companyId, String shopId);

    Iterable<TimeCard> getAllTimeCards(String companyId, String shopId, Long startDate, Long endDate);

    void addTimeCardSession(String companyId, String timeCardId, EmployeeSession employeeSession);
}
