package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.reporting.model.reportmodels.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.MemberSalesDetail;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 1/22/16.
 */
public interface TransactionRepository extends MongoShopBaseRepository<Transaction> {

    void updatePointsEarned(String companyId, String transId, BigDecimal pointsEarned);

    void updateMetrcId(String companyId, String transId, long metrcId);

    void updateTraceSubmitStatus(String companyId, String transId, Transaction.TraceSubmissionStatus status);
    void updateTraceSubmitStatusMessage(String companyId, String transId, Transaction.TraceSubmissionStatus status, String message);

    void markAsUnassigned(String companyId, String transId);

    List<Transaction> getTransactions(String companyId);

    void deactivate(String companyId, String transId);

    void completeTransaction(String companyId, String transId);
    void completeTransaction(String companyId, String transId, String sellerId, String sellerTerminalId);
    void cancelTransaction(String companyId, String transId, String sellerId, String sellerTerminalId);
    void markTransactionAsPaid(String companyId, String transId, long paidDate, final Cart.PaymentOption paymentOption);
    void markTransactionAsUnpaid(String companyId, String transId, final Cart.PaymentOption paymentOption);

    void updateTransactionState(String companyId, String transId, Transaction.TransactionStatus status, String sellerId, String sellerTerminalId);
    void startTransaction(String companyId, String transId, Transaction.TransactionStatus status, String sellerId, String sellerTerminalId);

    void updateTransactionState(String companyId, String transId, Transaction.TransactionStatus status, String assignedEmployeeId, String sellerId, String sellerTerminalId);
    void startTransaction(String companyId, String transId, Transaction.TransactionStatus status, String assignedEmployeeId, String sellerId, String sellerTerminalId);


    // Onfleet
    void updateOnfleetInfo(String companyId, String transId, int onfleetState, String onfleetId, String onfleetShortId, boolean createOnfleetFlag);

    void updateOnfleetInfo(String companyId, String transId, int onfleetState, Transaction.OnFleetTaskStatus onFleetTaskStatus);

    long getActiveTransactionCount(String shopId, String memberId);

    Transaction getTransactionByTransNo(String companyId, String shopId, String transNo);

    Transaction getTransactionByConsumerCartId(String companyId, String shopId, String consumerCartId);

    Iterable<Transaction> getAllTransactionsByType(Transaction.TransactionType transactionType);

    SearchResult<Transaction> getTransactionsForMember(String companyId, String shopId, String memberId, int skip, int limit);

    Iterable<Transaction> getCompletedTransactionsForDateRange(String companyId, String shopId, long startDate, long endDate);

    DateSearchResult<Transaction> getTransactionsForMemberDates(String companyId, String shopId, String memberId, long startDate, long endDate);

    DateSearchResult<Transaction> getActiveTransactions(String companyId, String shopId, boolean active, long afterDate, long beforeDate);

    Iterable<Transaction> getAllActiveTransactions();

    Iterable<Transaction> getAllActiveTransactions(String companyId, String shopId);

    Iterable<Transaction> getAllActiveTransactionsForAssignedEmployee(String companyId, String shopId, String employeeId);

    long getAllActiveTransactionsCount(String companyId, String shopId);

    SearchResult<Transaction> getActiveTransactions(String companyId, String shopId);

    SearchResult<Transaction> getActiveTransactionsForQueue(String companyId, String shopId, Transaction.QueueType queueType);

    SearchResult<Transaction> getCompletedTransactions(String companyId, String shopId, int start, int limit);

    SearchResult<Transaction> getCompletedTransactions(String companyId, String shopId, String employeeId, int start, int limit);

    DateSearchResult<Transaction> getSalesRefundsTransactions(String companyId, String shopId, long afterDate, long beforeDate);

    DateSearchResult<Transaction> getSalesTransactions(String companyId, String shopId, long afterDate, long beforeDate);

    DateSearchResult<Transaction> getActiveTransactionsAssigned(String companyId, String shopId, String employeeId);

    DateSearchResult<Transaction> getAllTransactionsAssigned(String companyId, String shopId, String employeeId, long afterDate, long beforeDate, int start, int limit);

    DateSearchResult<Transaction> getCompletedTransactionsDates(String companyId, String shopId, long startDate, long endDate);

    DateSearchResult<Transaction> getCompletedTransactionsForTerminal(String companyId, String shopId, String terminalId, long startDate, long endDate);

    Iterable<Transaction> getCompletedTransactionsDatesList(String companyId, String shopId, long startDate, long endDate);

    SearchResult<Transaction> getCompletedTransactionsForQueue(String companyId, String shopId, Transaction.QueueType queueType, int start, int limit);

    SearchResult<Transaction> getCompletedTransactionsForQueue(String companyId, String shopId, Transaction.QueueType queueType, String employeeId, int start, int limit);

    SearchResult<Transaction> findRecentTransactions(String companyId, String shopId, String transNo);

    Iterable<Transaction> getTransactionByStatus(String companyId, String shopId, Transaction.TransactionStatus transactionStatus, long startDate, long endDate);

    Iterable<Transaction> getTransactionByStatus(String companyId, String shopId, Transaction.TransactionStatus transactionStatus);

    // Get Transactions without OrderItemId
    Iterable<Transaction> getTransactionsWithoutOrderId();


    List<SalesByMember> getSalesByMember(String companyId, String shopId, long startDate, long endDate);

    Iterable<NewMemberTransactions> getCompletedTransactionStatsByMemberIds(String companyId, String shopId, Iterable<String> newMemberIds);

    List<Transaction> getRecentCompletedTransactions(String companyId, String shopId, int size);

    List<Transaction> getRecentCompletedTransactionsSales(String companyId, String shopId, int size);

    Iterable<SalesByProduct> getSalesByCannabisType(String companyId, String shopId, Long startDate, Long endDate);

    List<SalesByMember> getTotalSalesByMember(String companyId, String shopId, String memberId);

    long countTransactionsByMember(String companyId, String shopId, String memberId);

    Iterable<Transaction> getDailySales(String companyId, String shopId, Long startDate, Long endDate);

    long getDailyTotalVisits(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<BestSellingProduct> getDailyBestSellingProducts(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getBracketSales(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getBracketSalesWithStatuses(String companyId, String shopId, Long startDate, Long endDate, List<Transaction.TransactionStatus> statuses);

    Iterable<Transaction> getBracketSalesForTerminalId(String companyId, String shopId, String terminalId, Long startDate, Long endDate);

    Iterable<Transaction> getBracketSalesWithUncompleteOrders(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<SalesByProduct> getSalesByProduct(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<SalesByPaymentType> getSalesByPaymentType(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<SalesByPaymentTypeTerminal> getSalesByPaymentTypeTerminal(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<SalesByQueue> getSalesByQueue(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<SalesByEmployee> getSalesByEmployee(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getTotalSales(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getRefundData(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getTransactionsWithDiscounts(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getLosses(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<Transaction> getTransfers(String companyId, String shopId, Long startDate, Long endDate);

    List<Transaction> getWalkinTransations(final String shopId, final Transaction.QueueType queueType);

    Transaction getActiveTransactionsForShopAndMember(String shopId, String memberId);

    HashMap<String, Transaction> getTransactionsByAssignEmployeeId(String companyId, String shopId, String employeeId, String sortOptions, int skip, int limit);

    <E extends Transaction> SearchResult<E> getTransactionsByAssignEmployeeIdAndStatus(String companyId, String shopId, String employeeId, String sortOptions, int skip, int limit, Transaction.TransactionStatus completed, Class<E> clazz);

    SearchResult<Transaction> getTransactionsByTransactionTypes(List<Transaction.TransactionType> transactionTypeList, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis);

    SearchResult<Transaction> getTransactionsByTransactionStatus(List<Transaction.TransactionStatus> transactionStatusList, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis);

    SearchResult<Transaction> getTransactionsByTransactionTypes(List<Transaction.TransactionType> transactionTypeList, String employeeId, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis);

    SearchResult<Transaction> getTransactionsByTransactionStatus(List<Transaction.TransactionStatus> transactionStatusList, String employeeId, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis);

    SearchResult<Transaction> getTransactionsByTransactionStatusAndMember(List<Transaction.TransactionStatus> transactionStatusList, String companyId, String shopId, int start, int limit, String memberId);

    SearchResult<Transaction> getTransactionsByTransactionTypesAndMember(List<Transaction.TransactionType> transactionTypeList, String companyId, String shopId, int start, int limit, String memberId);

    HashMap<String, Transaction> getTransactionsByMileageCalculated(boolean isMileageCalculated);

    HashMap<String, Transaction> listTransactionByDate(String companyId, String shopId, String sortOption, Long startDateMillis, Long endDateMillis);

    Transaction getTransactionByTaskId(String taskId);

    SearchResult<Transaction> getActiveOnFleetTransactions(String companyId, String shopId);


    Iterable<Transaction> getBracketSalesByMemberId(String companyId, String memberId, Long startDate, Long endDate);

    Iterable<Transaction> getBracketSalesByMemberId(String companyId, String shopId, String memberId, Long startDate, Long endDate);


    Iterable<Transaction> getOnFleetTransactions(String companyId, String shopId);

    Iterable<Transaction> getBracketSalesByCompany(String companyId, Long startDate, Long endDate);

    public WriteResult updateSalesReceiptRef(final BasicDBObject query, final BasicDBObject field);

    public WriteResult updateRefundReceipt(final BasicDBObject query, final BasicDBObject field);


    void updateTransactionLockStatus(String companyId, String transactionId, Boolean lockStatus);

    SearchResult<Transaction> getAllTransactionByStatusAndType(String companyId, String shopId, Transaction.TransactionStatus transactionStatus, Transaction.TransactionType transactionType, long timeZoneStartDateMillis, long timeZoneEndDateMillis);

    SearchResult<Transaction> getAllTransactionByStatusAndTypeLimit(String companyId, String shopId,
                                                               List<Transaction.TransactionStatus> transactionStatuses,
                                                               List<Transaction.TransactionType> transactionTypes,
                                                               int skip, int limit);

    SearchResult<Transaction> getAllTransactionByStatusAndType(String companyId, String shopId,
                                                               List<Transaction.TransactionStatus> transactionStatuses,
                                                               List<Transaction.TransactionType> transactionTypes,
                                                               long timeZoneStartDateMillis, long timeZoneEndDateMillis,
                                                               int skip, int limit);


    SearchResult<Transaction> getTransactionByProduct(String companyId, String shopId, String sortOption, List<String> productIds);

    public Iterable<Transaction> getTransactionByType(String companyId, String shopId, Transaction.TransactionType transactionType);

    public Iterable<Transaction> getTransactionByTypeAndStatus(String companyId, String shopId, Transaction.TransactionType transactionType, Transaction.TransactionStatus transactionStatus);

    public Iterable<Transaction> getBracketSalesQB(String companyId, String shopId);

    public Iterable<Transaction> getDailySalesforQB(String companyId, String shopId, long startDate, long endDate);

    DateSearchResult<Transaction> getAllTransactionsAssigned(String companyId, String shopId, String employeeId, long afterDate);

    DateSearchResult<Transaction> getSalesRefundsTransactions(String companyId, String shopId, long afterDate);

    public List<Transaction> getSalesOrdersListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate);

    public List<Transaction> getRefundReceiptsListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate);

    public WriteResult updateJournalEntryRef(final BasicDBObject query, final BasicDBObject field);

    Iterable<Transaction> getAllBracketSales(String companyId, String shopId, Long startDate, Long endDate);

    void updateOnFleetTaskStatus(String companyId, String shopId, String transactionId, Transaction.OnFleetTaskStatus status);

    public Iterable<Transaction> listByShopWithDateAndStatus(String companyId, String shopId, long afterDate, long beforeDate, Transaction.TransactionType transactionType, Transaction.TransactionStatus transactionStatus);

    public Iterable<Transaction> getBracketSalesQB(String companyId, String shopId, long startDate, long endDate);

    SearchResult<Transaction> getActiveTransactionsByTerm(String companyId, String shopId, int start, int limit, String term, long afterDate, long beforeDate);

    SearchResult<Transaction> getAllAssignedTransaction(String companyId, String shopId, int start, int limit, boolean isAssigned, long afterDate, long beforeDate);

    SearchResult<Transaction> getAllAssignedTransactionByTerm(String companyId, String shopId, int start, int limit, String term, boolean isAssigned, long afterDate, long beforeDate);

    long countTransactionByStatus(String companyId, String shopId, List<Transaction.TransactionStatus> statuses, boolean status);

    long countTransactionsByAssignStatus(String companyId, String shopId, boolean isAssigned);

    SearchResult<Transaction> getCompletedTransactionsByTerm(String companyId, String shopId, String term, int start, int limit, long afterDate, long beforeDate);

    SearchResult<Transaction> getCompletedTransactionsByDate(String companyId, String shopId, int start, int limit, long afterDate, long beforeDate);

    SearchResult<Transaction> getActiveTransactionByDate(String companyId, String shopId, int start, int limit, long afterDate, long beforeDate);

    MemberSalesDetail getSalesByMember(String companyId, String shopId, String memberId);

    Iterable<Transaction> getTransactionByTypes(String companyId, String shopId, Long startDate, Long endDate, List<Transaction.TransactionType> transactionTypeList);


    void updateTookanInfo(String companyId, String transactionId, String tookanJobId);

    void updateTookanTransaction(String companyId, String shopId);

    Iterable<MemberPerformanceBySale> getSalesByMemberPerformance(String companyId, String shopId, Long startDate, Long endDate);

    SearchResult<Transaction> getTransactionsByFulFillmentStepAndQueue(String companyId, String shopId, Transaction.FulfillmentStep fulfillmentStep, Transaction.QueueType queueType, int start, int limit);

    Iterable<Transaction> getBracketSales(String companyId, String shopId, long startDate, long endDate, int skip, int fetchRecordLimit);


    long getBracketSalesCount(String companyId, String shopId, Long startDate, Long endDate);

    void updateOrderTags(String companyId, String transactionId, Set<String> orderTags);

    <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbDesktopRef(String companyId, String shopId, long syncTime, int start, int limit, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz);

    <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbDesktopRef(String companyId, String shopId, long startDate, long endDate, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz);

    void updateQbDesktopSalesRef(String companyId, String shopId, String transNo, String txnId);

    <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutJournalEntryRef(String companyId, String shopId, int start, int limit, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz);

    <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutJournalEntryRef(String companyId, String shopId, long startDate, long endDate, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz);

    void updateQbDesktopJournalEntryRef(String companyId, String shopId, String transNo);


    <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbRefundReceipt(String companyId, String shopId, long startDate, long endDate, Transaction.TransactionType type, Class<E> clazz);

    <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbRefundReceipt(String companyId, String shopId, int start, int limit, long syncTime, Transaction.TransactionType type, Class<E> clazz);

    void updateQbDesktopQbRefundReceipt(String companyId, String shopId, String transNo, String txnId);

    <E extends Transaction> List<E> getTransactionsByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz);

    void updateTransactionQbErrorAndTime(String companyID, String shopId, String transNo, boolean qbErrored, long errorTime);

    void updateTransactionQbRefundErrorAndTime(String companyID, String shopId, String transNo, boolean qbRefundErrored, long errorRefundTime);

    void hardRemoveQuickBookDataInTransactions(String companyId, String shopId);

    void updatePickupDate(String id, long completeAfter, long pickupDate);

    void updateDeliveryDate(String id, long completeAfter, long deliveryDate);

    void updateDeliveryAddress(String id, Address address);
    void updatePackedBy(String id, String packedBy, long packedDate);

    long getAllActiveTransactionsCountForTerminal(String companyId, String shopId, String terminalId);

    SearchResult<Transaction> getActiveTransactionsForMember(String companyId, String shopId, String memberId);



    void updateActiveOrderTags(String companyId, List<String> memberIds, List<String> tags, List<String> removeTags);

    long countTransactionsByProductId(String companyId, String shopId,  List<String> productIds);


    void saveMtracTxn(String companyId, String shopId, String transactionId, String mtracTransaction);

    void saveCloverTxn(String companyId, String shopId, String transactionId, String cloverTransaction);


    SearchResult<Transaction>  getAllTransactions(String companyId, String shopId, int start, int limit, long afterDate, long beforeDate);

    Iterable<Transaction> getBracketSalesByEmployee(String companyId, String shopId, String employeeId, Long startDate, Long endDate);

    void updatePaymentOption(String companyId, String transId, Cart cart);

    void updateAllDeliveryAddress(String companyId, String memberId, Address address);

    SearchResult<Transaction> getAllTransactionsAssignedByStatus(String companyId, String shopId, String employeeId, List<Transaction.TransactionStatus> statuses, int start, int limit, String searchTerm);

    <E extends Transaction> List<E> getRefundTransactionByLimitsWithQBRefundPaymentError(String companyId, String shopId, long startTime, long endTime, Class<E> clazz);

    <E extends Transaction> List<E> getLimitedTransactionWithoutRefundPayment(String companyId, String shopId, int start, int limit, Class<E> clazz);

    void updateTransactionQbRefundPayment(String companyId, String shopId, String transNo, boolean refundPayment);

    void updateTransactionQbRefundPaymentErrorAndTime(String companyID, String shopId, String transNo, boolean refundPayment, boolean refundPaymentErrored, long refundPaymentErrorTime);



    SearchResult<Transaction> getAllEmployeeTransactionsWithStatus(String companyId, String shopId, String employeeId,  List<Transaction.TransactionStatus> statuses, long afterDate, long beforeDate);

    SearchResult<Transaction> getActiveTransactionsForQueue(String companyId, String shopId, String assignEmployeeId, Transaction.QueueType queueType);

    SearchResult<Transaction> getActiveTransactionsByTerm(String companyId, String shopId, String employeeId, int start, int limit, String term, long afterDate, long beforeDate);

    long countTransactionByStatus(String companyId, String shopId, String employeeId, List<Transaction.TransactionStatus> status, boolean active);

}
