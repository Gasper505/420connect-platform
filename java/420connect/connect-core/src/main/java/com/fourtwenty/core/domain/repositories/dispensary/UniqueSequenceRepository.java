package com.fourtwenty.core.domain.repositories.dispensary;


import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * The ShareIdentifierRepository object.
 * User: mdo
 * Date: 4/2/15
 * Time: 4:14 PM
 */
public interface UniqueSequenceRepository extends MongoShopBaseRepository<UniqueSequence> {
    UniqueSequence getNewIdentifier(String companyId, String shopId, String keyName);
}
