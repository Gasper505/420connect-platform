package com.fourtwenty.core.domain.repositories.dispensary;

import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.reporting.model.reportmodels.CustomerByCurrentMonth;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by Stephen Schmidt on 10/12/2015.
 */
public interface VendorRepository extends MongoCompanyBaseRepository<Vendor> {

    long countVendorByVendorName(String companyId, String vendorName);

    List<Vendor> getVendorList(String companyId, String shopId);

    WriteResult updateVendorRef(BasicDBObject query, BasicDBObject field);

    <E extends Vendor> SearchResult<E> findItemsByVendorAndCompanyType(String companyId, String sortOption, int start, int limit, Vendor.VendorType vendorType, List<Vendor.CompanyType> companyType, Class<E> clazz);

    <E extends Vendor> SearchResult<E> findItemsByTerm(String companyId, String sortOption, int start, int limit, Vendor.VendorType vendorType, String term, Class<E> clazz);

    Iterable<Vendor> getVendorsByBrand(String companyId, String brandId);

    <E extends Vendor> SearchResult<E> findItemsWithSort(String companyId, String sortOptions, int start, int limit, Class<E> clazz);

    void bulkRemoveBrand(String companyId, List<String> brands);

    void bulkUpdateStatus(String companyId, List<ObjectId> vendorIds, boolean active);

    void bulkUpdateBackOrderEnabled(String companyId, List<ObjectId> vendorIds, boolean backOrderEnabled);

    void bulkUpdateTax(String companyId, List<ObjectId> vendorIds, Vendor.ArmsLengthType armsLengthType);

    void bulkUpdateBrands(String companyId, List<ObjectId> vendorIds, LinkedHashSet<String> brands);


    CustomerByCurrentMonth getCustomerByCurrentMonth(String companyId, long start, long end);

    <E extends CompanyBaseModel> SearchResult<E> findVendorsByTerm(String companyId, String shopId, String term, Class<E> clazz);

    <E extends CompanyBaseModel> SearchResult<E> findItemsByCompanyTypeAndTerm(String companyId, String sortOption, int start, int limit, Vendor.VendorType vendorType, List<Vendor.CompanyType> companyType, String term, Class<E> clazz);

    List<Vendor> getVendorListWithoutQbRef(String companyId, long afterDate, long beforeDate);

    <E extends CompanyBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, long afterDate, long beforeDate, Class<E> clazz);

    void updateVendorRef(String companyID, String id, String qbDesktopRef, String editSequence, String qbListId);

    HashMap<String, Vendor> getLimitedVendorViewAsMap(String companyId);

    Iterable<Vendor> findItems(int start, int limit, String projection);

    void updateCompanyLicense(String id, List<CompanyLicense> companyLicenses);

    SearchResult<Vendor> findItemsWithDate(String companyId, long startTime, long endTime, int skip, int limit, String sortOptions);

    <E extends Vendor> List<E> getVendorsByLimits(String companyId, int start, int limit, Class<E> clazz);

    <E extends Vendor> List<E> getVendorsByLimits(String companyId, String shopId, int start, int limit, Class<E> clazz);

    <E extends Vendor> List<E> getVendorsByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz);

    HashMap<String, Vendor> getNonDeletedVendors(String companyId, List<ObjectId> vendorIds);

    <E extends Vendor> List<E> getQBExistVendors(String companyId, String shopId, int start, int limit, long endTime, Class<E> clazz);

    void updateVendorEditSequence(String companyId, String  shopId, String qbListId, String editSequence, String desktopRef);

    <E extends Vendor> List<E> getCustomerByLimitsWithQBError(String companyId, String shopId, Long errorTime, List<Vendor.VendorType> vendorTypes, Class<E> clazz);

    <E extends Vendor> List<E> getCustomerByLimitsWithoutQbDesktopRef(String companyId, int start, int limit, List<Vendor.VendorType> vendorTypes, Class<E> clazz);

    <E extends Vendor> List<E> getCustomerByLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, List<Vendor.VendorType> vendorTypes, Class<E> clazz);

    <E extends Vendor> List<E> getQBExistingCustomer(String companyId, String shopId, int start, int limit, long time, List<Vendor.VendorType> vendorTypes, Class<E> clazz);

    void updateCustomerEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef);

    void updateVendorQbMapping(String companyID, String id, List<QBDataMapping> mapping);

    void updateVendorQbCustomerMapping(String companyID, String id, List<QBDataMapping> mapping);

    void hardQbMappingRemove(String companyId, String shopId);

    void hardQbCustomerMappingRemove(String companyId, String shopId);

    Vendor getDefaultVendor(String companyId);
}
