package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Adjustment;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.AdjustmentRepository;

import javax.inject.Inject;

public class AdjustmentRepositoryImpl extends ShopBaseRepositoryImpl<Adjustment> implements AdjustmentRepository {

    @Inject
    public AdjustmentRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Adjustment.class, mongoManager);
    }

    @Override
    public Adjustment getAdjustmentByName(String companyId, String shopId, String name) {
        return coll.findOne("{companyId:#, deleted:false, shopId:#, name:#}", companyId, shopId, name).as(entityClazz);
    }
}
