package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 7/12/16.
 */
public class AppRepositoryImpl extends MongoBaseRepositoryImpl<App> implements AppRepository {

    @Inject
    public AppRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(App.class, mongoManager);
    }

    @Override
    public App getAppByName(String name) {
        return coll.findOne("{name:#}", name).as(entityClazz);
    }
}
