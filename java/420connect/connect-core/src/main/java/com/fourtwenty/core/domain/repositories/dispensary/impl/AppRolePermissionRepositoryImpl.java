package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.AppRolePermission;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.AppRolePermissionRepository;

import javax.inject.Inject;

public class AppRolePermissionRepositoryImpl extends CompanyBaseRepositoryImpl<AppRolePermission> implements AppRolePermissionRepository {
    @Inject
    public AppRolePermissionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(AppRolePermission.class, mongoManager);
    }

    @Override
    public AppRolePermission getPermissionsByCompanyAndRole(String companyId, CompanyFeatures.AppTarget appTarget) {
        return coll.findOne("{companyId:#, appTarget:#}", companyId, appTarget).as(entityClazz);
    }
}
