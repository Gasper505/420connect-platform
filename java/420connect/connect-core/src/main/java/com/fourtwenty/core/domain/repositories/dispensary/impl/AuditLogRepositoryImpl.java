package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.AuditLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.AuditLogRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 7/13/16.
 */
public class AuditLogRepositoryImpl extends ShopBaseRepositoryImpl<AuditLog> implements AuditLogRepository {
    @Inject
    public AuditLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(AuditLog.class, mongoManager);
    }

    @Override
    public Iterable<AuditLog> getLogsByEmployee(String companyId, String shopId, String employeeId) {
        return coll.find("{companyId:#, shopId:#, employeeId:#}", companyId, shopId, employeeId).as(entityClazz);
    }

    @Override
    public Iterable<AuditLog> getLogsByDateBracket(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, created: {$gt:#, $lt:#}}", companyId, shopId, startDate, endDate).as(entityClazz);
    }

    @Override
    public Iterable<AuditLog> getLogsByBracketAndEmployee(String companyId, String shopId, String employeeId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, employeeId:#, created: {$gt:#, $lt:#}}", companyId, shopId, employeeId, startDate, endDate).as(entityClazz);
    }


}
