package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BarcodeItemRepository;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.WriteResult;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by mdo on 2/28/17.
 */
public class BarcodeItemRepositoryImpl extends ShopBaseRepositoryImpl<BarcodeItem> implements BarcodeItemRepository {

    @Inject
    public BarcodeItemRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BarcodeItem.class, mongoManager);
    }

    @Override
    public BarcodeItem getBarcode(String companyId, String shopId, BarcodeItem.BarcodeEntityType barcodeType,
                                  String barcode) {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(barcode)) {
            return null;
        }
        barcode = barcode.toUpperCase();
        return coll.findOne("{companyId:#,shopId:#,barcode:#,entityType:#}", companyId, shopId, barcode, barcodeType).as(entityClazz);
    }

    @Override
    public BarcodeItem getDeletedBarcode(String companyId, String shopId, BarcodeItem.BarcodeEntityType barcodeType,
                                         String barcode) {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(barcode)) {
            return null;
        }
        return coll.findOne("{companyId:#,shopId:#,barcode:#,entityType:#,deleted:true}", companyId, shopId, barcode, barcodeType).as(entityClazz);
    }

    @Override
    public List<BarcodeItem> getBarCodesByLabel(String companyId, String shopId, String label, BarcodeItem.BarcodeEntityType entityType) {
        Iterable<BarcodeItem> barcodeItems = coll.find("{companyId:#,shopId:#,barcode:#,entityType:#}", companyId, shopId, label, entityType).as(entityClazz);
        return Lists.newArrayList(barcodeItems);
    }

    @Override
    public int removeByBarCodes(String companyId, String shopId, List<String> batchSkuList) {
        WriteResult result = coll.remove("{companyId:#, shopId:#, barcode:{$in:#}}", companyId, shopId, batchSkuList);
        return result.getN();
    }

    @Override
    public int removeAllInEntities(List<String> entityIds) {
        WriteResult result = coll.remove("{entityId:{$in:#}}", entityIds);
        return result.getN();
    }

    @Override
    public int removeAllInProductIds(List<String> productIds) {
        WriteResult result = coll.remove("{productId:{$in:#}}", productIds);
        return result.getN();
    }

    @Override
    public int deleteAllBarcodeItems(String companyId, String shopId) {
        WriteResult result = coll.remove("{companyId:#,shopId:#}", companyId, shopId);
        return result.getN();
    }

    @Override
    public Iterable<BarcodeItem> getAvailableBarCodes(String companyId, String shopId, BarcodeItem.BarcodeEntityType entityType, List<String> barCodes) {
        return coll.find("{ companyId:#, shopId:#, deleted: false, entityType:#, barcode:{$in:#}}", companyId, shopId, entityType, barCodes).as(entityClazz);
    }

    @Override
    public Iterable<BarcodeItem> getAllBarCodesForProduct(String companyId, String shopId, List<String> productIds) {
        return coll.find("{companyId:#, shopId:#, productId:{$in:#}}", companyId, shopId, productIds).as(entityClazz);
    }
}
