package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.BatchActivityLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BatchActivityLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BatchActivityLogRepositoryImpl extends ShopBaseRepositoryImpl<BatchActivityLog> implements BatchActivityLogRepository {

    @Inject
    public BatchActivityLogRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(BatchActivityLog.class, mongoDb);
    }

    @Override
    public SearchResult<BatchActivityLog> getBatchActivityLogByBatchId(String companyId, String shopId, String batchId, String sortOption, int start, int limit) {
        Iterable<BatchActivityLog> items = coll.find("{companyId:#,shopId:#,deleted:false,targetId:#}", companyId, shopId, batchId).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,targetId:#}", companyId, shopId, batchId);
        SearchResult<BatchActivityLog> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public HashMap<String, List<BatchActivityLog>> findItemsByBatchIdAsMap(String companyId, String shopId, List<String> batchId) {
        Iterable<BatchActivityLog> items = coll.find("{companyId:#,shopId:#,deleted:false,targetId:{$in:#}}", companyId, shopId, batchId).sort("{modified:-1}").as(entityClazz);

        HashMap<String, List<BatchActivityLog>> itemsMap = new HashMap<>();
        if (items != null) {
            for (BatchActivityLog batchActivityLog : items) {
                List<BatchActivityLog> batchActivityLogs = new ArrayList<>();
                if (itemsMap.containsKey(batchActivityLog.getTargetId())) {
                    batchActivityLogs = itemsMap.get(batchActivityLog.getTargetId());
                }
                batchActivityLogs.add(batchActivityLog);
                itemsMap.put(batchActivityLog.getTargetId(), batchActivityLogs);
            }
        }
        return itemsMap;
    }
}
