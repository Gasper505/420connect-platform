package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchByCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchQuantityResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.AggregationOptions;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 10/11/17.
 */
public class BatchQuantityRepositoryImpl extends ShopBaseRepositoryImpl<BatchQuantity> implements BatchQuantityRepository {

    @Inject
    public BatchQuantityRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BatchQuantity.class, mongoManager);
    }

    @Override
    public List<BatchQuantity> getBatchQuantities(String companyId, String shopId, String productId) {
        Iterable<BatchQuantity> batchQuantities = coll.find("{companyId:#,shopId:#,productId:#}", companyId, shopId, productId).as(entityClazz);
        return Lists.newArrayList(batchQuantities);
    }


    @Override
    public <E extends BatchQuantity> SearchResult<E> getBatchQuantitiesForInventory(String companyId, String shopId, String productId, String inventoryId, Class<E> clazz, int skip, int limit) {
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> batchQuantities = coll.find("{companyId:#,shopId:#,productId:#,inventoryId:#}", companyId, shopId, productId, inventoryId).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,productId:#,inventoryId:#}", companyId, shopId, productId, inventoryId);

        searchResult.setValues(Lists.newArrayList(batchQuantities));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public Iterable<BatchQuantity> getBatchQuantitiesForInventorySorted(String companyId, String shopId, String productId, String inventoryId, String sortOptions) {
        Iterable<BatchQuantity> batchQuantities = coll.find("{companyId:#,shopId:#,productId:#,inventoryId:#}", companyId, shopId, productId, inventoryId).sort(sortOptions).as(entityClazz);
        return batchQuantities;
    }

    @Override
    public BigDecimal getBatchQuantitiesForInventoryTotal(String companyId, String shopId, String productId, String inventoryId) {
        Iterable<BatchQuantity> batchQuantities = coll.find("{companyId:#,shopId:#,productId:#,inventoryId:#}", companyId, shopId, productId, inventoryId).as(entityClazz);
        BigDecimal total = new BigDecimal(0);
        for (BatchQuantity quantity : batchQuantities) {
            if (StringUtils.isNotBlank(quantity.getBatchId())) {
                total = total.add(quantity.getQuantity());
            }
        }
        return total;
    }


    @Override
    public BigDecimal getBatchQuantityForByBatchIdTotal(String companyId, String shopId, String productId, String batchId) {
        Iterable<BatchQuantity> batchQuantities = coll.find("{companyId:#,shopId:#,productId:#,batchId:#}", companyId, shopId, productId, batchId).as(entityClazz);
        BigDecimal total = new BigDecimal(0);
        for (BatchQuantity quantity : batchQuantities) {
            if (quantity != null && quantity.getQuantity().doubleValue() > 0) {
                total = total.add(quantity.getQuantity());
            }
        }
        return total;
    }

    @Override
    public BatchQuantity getBatchQuantityForInventory(String companyId, String shopId, String productId, String inventoryId, String batchId) {
        return coll.findOne("{companyId:#,shopId:#,productId:#,inventoryId:#,batchId:#}", companyId, shopId, productId, inventoryId, batchId).as(entityClazz);
    }


    @Override
    public SearchResult<BatchQuantityResult> getBatchQuantitiesForInventoryAndBatch(String companyId, String shopId, String inventoryId, List<String> batchIdList, String productId, int skip, int limit) {
        SearchResult<BatchQuantityResult> searchResult = new SearchResult<>();
        Iterable<BatchQuantityResult> batchQuantities = coll.find("{companyId:#,shopId:#,inventoryId:#,batchId:{$in:#},productId:#}", companyId, shopId, inventoryId, batchIdList, productId)
                .skip(skip).limit(limit).as(BatchQuantityResult.class);
        long count = coll.count("{companyId:#,shopId:#,inventoryId:#,batchId:{$in:#},productId:#}", companyId, shopId, inventoryId, batchIdList, productId);

        searchResult.setValues(Lists.newArrayList(batchQuantities));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public List<ProductBatchByInventory> getBatchQuantitiesByInventory(String companyId, String shopId, String batchId, String productId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<ProductBatchByInventory> results = coll.aggregate("{$match: {companyId:#, shopId:#, productId:#, batchId:#}}", companyId, shopId, productId, batchId)
                .and("{$group: {_id: '$inventoryId',  batchQuantity: { $push: '$$ROOT' }}}")
                .options(aggregationOptions)
                .as(ProductBatchByInventory.class);

        return Lists.newArrayList((Iterable<ProductBatchByInventory>) results);
    }

    @Override
    public void removeAllProductBatchQuantities(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#,deleted:false}", companyId, shopId).multi().with("{$set: {quantity:0,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public void removeAnyWithoutBatchId() {
        coll.remove("{ batchId: { $exists: false} }");
        coll.remove("{ batchId: '' }");
    }

    @Override
    public void removeAnyWithoutInventoryIds(List<String> inventoryIds) {
        coll.remove("{ inventoryId: { $nin: # } }", inventoryIds);
    }

    @Override
    public List<BatchByCategoryResult> getBatchQuantitiesForInventory(String companyId, String shopId, String inventoryId, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<BatchByCategoryResult> results = coll.aggregate("{$match : { companyId :#,shopId:#,deleted:false,inventoryId:#}}", companyId, shopId, inventoryId)
                .and("{ $group : { _id : '$productId' , batchResult : {$push : '$$ROOT' }}}")
                .and("{$match: {_id : {$in : #}}}", productIds)
                .options(aggregationOptions)
                .as(BatchByCategoryResult.class);

        return Lists.newArrayList((Iterable<BatchByCategoryResult>) results);
    }

    @Override
    public HashMap<String, List<BatchQuantity>> getBatchQuantityForProduct(String companyId, String shopId, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<BatchQuantity> results = coll.aggregate("{$match : { companyId :#,shopId:#,deleted:false,quantity:{$ne:0.0},productId:{$in:#}}}", companyId, shopId, productIds)
                .and("{ $group : {'_id':{'productId':'$productId','batchId':'$batchId'},'quantity':{$sum:'$quantity'}}}")
                .and("{$project: {'productId':'$_id.productId','batchId':'$_id.batchId','quantity':'$quantity','_id':'$_id.productId'}}")
                .options(aggregationOptions)
                .as(BatchQuantity.class);

        HashMap<String, List<BatchQuantity>> returnMap = new HashMap<>();
        for (BatchQuantity batchQuantity : results) {
            List<BatchQuantity> batchQuantityList = returnMap.getOrDefault(batchQuantity.getProductId(), new ArrayList<>());
            batchQuantityList.add(batchQuantity);
            returnMap.put(batchQuantity.getProductId(), batchQuantityList);
        }
        return returnMap;
    }

    @Override
    public Iterable<BatchQuantity> getBatchQuantityByBatch(String companyId, String shopId, String batchId) {
        return coll.find("{companyId:#, shopId:#, batchId:#, quantity: {$ne: 0}, deleted: false}", companyId, shopId, batchId).as(entityClazz);
    }


    @Override
    public HashMap<String, BatchByCategoryResult> getBatchQuantitiesForProductAsMap(String companyId, String shopId, String inventoryId, List<String> productIds) {
        List<BatchByCategoryResult> items = getBatchQuantitiesForInventory(companyId, shopId, inventoryId, productIds);
        HashMap<String, BatchByCategoryResult> map = new HashMap<>();
        for (BatchByCategoryResult item : items) {
            map.put(item.getProductId(), item);
        }
        return map;
    }

    @Override
    public HashMap<String, List<BatchQuantity>> getBatchQuantityByInventoryForProduct(String companyId, String shopId, List<String> inventoryIds, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<BatchQuantity> results = coll.aggregate("{$match : { companyId :#,shopId:#,deleted:false,quantity:{$ne:0.0},productId:{$in:#},inventoryId:{$in:#}}}", companyId, shopId, productIds, inventoryIds)
                .and("{ $group : {'_id':'$productId','quantity':{$sum:'$quantity'}}}")
                .and("{$project: {'productId':'$_id','quantity':'$quantity'}}")
                .options(aggregationOptions)
                .as(BatchQuantity.class);

        HashMap<String, List<BatchQuantity>> returnMap = new HashMap<>();
        for (BatchQuantity batchQuantity : results) {
            List<BatchQuantity> batchQuantityList = returnMap.getOrDefault(batchQuantity.getProductId(), new ArrayList<>());
            batchQuantityList.add(batchQuantity);
            returnMap.put(batchQuantity.getProductId(), batchQuantityList);
        }
        return returnMap;
    }

    @Override
    public HashMap<String, List<BatchQuantity>> getBatchQuantityForProductInventory(String companyId, String shopId, List<String> inventoryIds, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<BatchQuantity> results = coll.aggregate("{$match : { companyId :#,shopId:#,deleted:false,quantity:{$ne:0.0},productId:{$in:#},inventoryId:{$in:#}}}", companyId, shopId, productIds, inventoryIds)
                .and("{ $group : {'_id':{'productId': '$productId', 'inventoryId':'$inventoryId', 'batchId':'$batchId'},'quantity':{$sum:'$quantity'}}}")
                .and("{$project: {'productId':'$_id.productId','inventoryId':'$_id.inventoryId','batchId':'$_id.batchId','quantity':'$quantity', '_id':'$_id.productId'}}")
                .options(aggregationOptions)
                .as(BatchQuantity.class);

        HashMap<String, List<BatchQuantity>> returnMap = new HashMap<>();
        for (BatchQuantity batchQuantity : results) {
            List<BatchQuantity> batchQuantityList = returnMap.getOrDefault(batchQuantity.getProductId(), new ArrayList<>());
            batchQuantityList.add(batchQuantity);
            returnMap.put(batchQuantity.getProductId(), batchQuantityList);
        }
        return returnMap;
    }

    @Override
    public HashMap<String, List<BatchQuantity>> getAllBatchQuantities(String companyId, String shopId, List<String> batchIds) {
        Iterable<BatchQuantity> results = coll.find("{companyId :#, shopId:#, deleted:false, batchId:{$in:#}}", companyId, shopId, batchIds).as(BatchQuantity.class);

        HashMap<String, List<BatchQuantity>> returnMap = new HashMap<>();
        for (BatchQuantity batchQuantity : results) {
            List<BatchQuantity> batchQuantityList = returnMap.getOrDefault(batchQuantity.getBatchId(), new ArrayList<>());
            batchQuantityList.add(batchQuantity);
            returnMap.put(batchQuantity.getBatchId(), batchQuantityList);
        }
        return returnMap;
    }


}
