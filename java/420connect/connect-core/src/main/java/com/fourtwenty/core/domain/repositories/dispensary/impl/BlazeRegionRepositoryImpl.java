package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BlazeRegionRepository;

import javax.inject.Inject;

public class BlazeRegionRepositoryImpl extends CompanyBaseRepositoryImpl<BlazeRegion> implements BlazeRegionRepository {

    @Inject
    public BlazeRegionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BlazeRegion.class, mongoManager);
    }
}
