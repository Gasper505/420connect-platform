package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.BouncedEmail;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BouncedEmailRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 8/8/16.
 */
public class BouncedEmailRepositoryImpl extends MongoBaseRepositoryImpl<BouncedEmail> implements BouncedEmailRepository {

    @Inject
    public BouncedEmailRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BouncedEmail.class, mongoManager);
    }

    @Override
    public BouncedEmail getBouncedEmail(String email) {
        return coll.findOne("{email:#}", email).as(entityClazz);
    }

    @Override
    public boolean isBounced(String email) {
        return coll.count("{email:#}", email) > 0;
    }
}
