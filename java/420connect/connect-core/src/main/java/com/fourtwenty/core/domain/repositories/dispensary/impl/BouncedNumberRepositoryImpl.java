package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.BouncedNumber;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BouncedNumberRepository;

import javax.inject.Inject;
import java.util.List;

public class BouncedNumberRepositoryImpl extends MongoBaseRepositoryImpl<BouncedNumber> implements BouncedNumberRepository {

    @Inject
    public BouncedNumberRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BouncedNumber.class, mongoManager);
    }

    @Override
    public BouncedNumber getBouncedNumber(String number) {
        return coll.findOne("{number:#}", number).as(entityClazz);
    }

    @Override
    public void deleteByNumber(String number) {
        coll.update("{number:#}", number).with("{$set:{deleted:true,reason:#}}", "OPT-IN");
    }

    @Override
    public Iterable<BouncedNumber> getDetailsByNumber(List<String> phoneNumbers) {
        return coll.find("{deleted:false,number : {$in:#}}", phoneNumbers).as(entityClazz);
    }
}
