package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class BrandRepositoryImpl extends CompanyBaseRepositoryImpl<Brand> implements BrandRepository {

    @Inject
    public BrandRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Brand.class, mongoManager);
    }

    @Override
    public <E extends Brand> SearchResult<E> getAllBrand(String companyId, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,deleted:false}", companyId).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,deleted:false}", companyId);
        SearchResult<E> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public Brand getBrandByName(String companyId, String name) {

        return coll.findOne("{companyId:#,name:#,deleted:false}", companyId, name).as(entityClazz);
    }

    @Override
    public <E extends Brand> SearchResult<E> findBrandBySearchTerms(String companyId, String searchTerm, int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        Iterable<E> items = coll.find("{$and: [{companyId:#}, {$or: [{name:#,deleted:false}]}]}", companyId, pattern).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#}, {$or: [{name:#,deleted:false}]}]}", companyId, pattern);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Brand> SearchResult<E> getAllDeletedBrand(String companyId, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> deleteditems = coll.find("{companyId:#,deleted:true}", companyId).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,deleted:true}", companyId);
        SearchResult<E> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(deleteditems));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<Brand> getBrandsByIdAndTerm(String companyId, List<ObjectId> brandIds, String term, String sort, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Brand> items = coll.find("{$and : [{companyId:#,deleted:false,_id:{$in:#}},{$or:[{name:#}]}]}", companyId, brandIds, pattern).sort(sort).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{$and : [{companyId:#,deleted:false,_id:{$in:#}},{$or:[{name:#}]}]}", companyId, brandIds, pattern);
        SearchResult<Brand> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setSkip(start);
        result.setLimit(limit);
        result.setTotal(count);
        return result;

    }

    @Override
    public SearchResult<Brand> getBrandsByIds(String companyId, List<ObjectId> brandIds, String sort, int start, int limit) {
        Iterable<Brand> items = coll.find("{companyId:#,deleted:false,_id:{$in:#}}", companyId, brandIds).sort(sort).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,deleted:false,_id:{$in:#}}", companyId, brandIds);
        SearchResult<Brand> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setSkip(start);
        result.setLimit(limit);
        result.setTotal(count);
        return result;
    }


    @Override
    public HashMap<String, Brand> getLimitedBrandViewAsMap(String companyId) {
        HashMap<String, Brand> brandMap = new HashMap<>();
        Iterable<Brand> brands = coll.find("{companyId:#, deleted:false}", companyId).projection("{_id:1, name:1}").as(entityClazz);
        for (Brand brand : brands) {
            brandMap.put(brand.getId(), brand);
        }
        return brandMap;
    }

    @Override
    public <E extends Brand> SearchResult<E> getAllBrandByStatus(String companyId, boolean active, int start, int limit, String sortOption, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,deleted:false,active:#}", companyId, active).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,deleted:false,active:#}", companyId, active);
        SearchResult<E> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends Brand> SearchResult<E> findBrandByStatusAndTerm(String companyId, boolean active, String searchTerm, int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        Iterable<E> items = coll.find("{$and: [{companyId:#, deleted:false, active:#}, {$or: [{name:#}]}]}", companyId, active, pattern).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#, deleted:false, active:#}, {$or: [{name:#}]}]}", companyId, active, pattern);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkUpdateWebsite(String companyId, List<ObjectId> brandIds, String website) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, brandIds).multi().with("{$set: {website:#, modified:#}}", website, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdatePhoneNo(String companyId, List<ObjectId> brandIds, String phoneNumber) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, brandIds).multi().with("{$set: {phoneNo:#, modified:#}}", phoneNumber, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateStatus(String companyId, List<ObjectId> brandIds, Boolean active) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, brandIds).multi().with("{$set: {active:#, modified:#}}", active, DateTime.now().getMillis());
    }

    @Override
    public void deleteBrands(String companyId, List<ObjectId> brandIds) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, brandIds).multi().with("{$set: {deleted:true, active:false, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public Brand getDefaultBrand(String companyId) {
        return coll.findOne("{companyId:#, default:true}", companyId).as(entityClazz);
    }
}
