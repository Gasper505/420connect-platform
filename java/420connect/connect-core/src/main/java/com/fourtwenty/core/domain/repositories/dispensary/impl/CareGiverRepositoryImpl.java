package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.customer.CareGiver;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CareGiverRepository;

import javax.inject.Inject;

/**
 * Created by decipher on 4/12/17 6:06 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class CareGiverRepositoryImpl extends ShopBaseRepositoryImpl<CareGiver> implements CareGiverRepository {

    @Inject
    public CareGiverRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CareGiver.class, mongoManager);
    }
}
