package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CashDrawerSessionActivityLog;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CashDrawerActivityLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class CashDrawerActivityLogRepositoryImpl extends CompanyBaseRepositoryImpl<CashDrawerSessionActivityLog> implements CashDrawerActivityLogRepository {

    @Inject
    public CashDrawerActivityLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CashDrawerSessionActivityLog.class, mongoManager);
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getActivityLogForCashDrawerSessionId(String companyId, String shopId, String cashDrawerSessionId, long startDateTime, long endDateTime, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,cashDrawerSessionId:#,created:{$gte:#,$lte:#}}", companyId, shopId, cashDrawerSessionId, startDateTime, endDateTime).sort("{created:-1}").skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,cashDrawerSessionId:#,created:{$gte:#,$lte:#}}", companyId, shopId, cashDrawerSessionId, startDateTime, endDateTime);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllActivityLogForCashDrawerSessionId(String companyId, String shopId, String cashDrawerSessionId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,cashDrawerSessionId:#}", companyId, shopId, cashDrawerSessionId).sort("{created:-1}").as(clazz);

        long count = coll.count("{companyId:#,shopId:#,cashDrawerSessionId:#}", companyId, shopId, cashDrawerSessionId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setTotal(count);
        return results;
    }
}
