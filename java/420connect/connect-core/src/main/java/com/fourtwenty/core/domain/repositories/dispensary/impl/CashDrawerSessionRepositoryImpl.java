package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CashDrawerSessionRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 12/7/16.
 */
public class CashDrawerSessionRepositoryImpl extends ShopBaseRepositoryImpl<CashDrawerSession> implements CashDrawerSessionRepository {
    @Inject
    public CashDrawerSessionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CashDrawerSession.class, mongoManager);
    }

    @Override
    public CashDrawerSession getCashDrawerForDate(String companyId, String shopId, String terminalId, String date) {
        return coll.findOne("{companyId:#,shopId:#,terminalId:#,date:#}", companyId, shopId, terminalId, date).as(entityClazz);
    }

    @Override
    public CashDrawerSession getLatestDrawerForTerminal(String companyId, String shopId, String terminalId) {
        Iterable<CashDrawerSession> items = coll.find("{companyId:#,shopId:#,terminalId:#}", companyId, shopId, terminalId).sort("{startTime:-1}").as(entityClazz);

        List<CashDrawerSession> list = Lists.newArrayList(items);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Iterable<CashDrawerSession> getCashDrawersForDate(String companyId, String shopId, String date) {
        return coll.find("{companyId:#,shopId:#,date:#}", companyId, shopId, date).as(entityClazz);
    }

    @Override
    public HashMap<String, CashDrawerSession> getCashDrawersForDateAsMapTerminal(String companyId, String shopId, String date) {
        Iterable<CashDrawerSession> cashDrawerSessions = getCashDrawersForDate(companyId, shopId, date);

        HashMap<String, CashDrawerSession> map = new HashMap<>();
        for (CashDrawerSession cashDrawerSession : cashDrawerSessions) {
            map.put(cashDrawerSession.getTerminalId(), cashDrawerSession);
        }
        return map;
    }

    @Override
    public Iterable<CashDrawerSession> getCashDrawerSessionsForShop(String companyId, String shopId, Long startDate, Long endDate) {
        Iterable<CashDrawerSession> items = coll.find("{companyId:#, shopId:#, created: {$gt:#, $lt:#}}", companyId, shopId, startDate, endDate)
                .as(entityClazz);
        return items;
    }


    @Override
    public <E extends CashDrawerSession> SearchResult<E> getCashDrawerSessionsForShop(String companyId, String shopId, String terminalId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,terminalId:#}", companyId, shopId, terminalId).sort("{startTime:-1}").as(clazz);

        long count = coll.count("{companyId:#,shopId:#,terminalId:#}", companyId, shopId, terminalId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<CashDrawerSession> getOpenedCashDrawers() {
        return coll.find("{status:#}", CashDrawerSession.CashDrawerLogStatus.Open).as(entityClazz);
    }

    @Override
    public SearchResult<CashDrawerSession> getCashDrawerForTerminalId(String companyId, String shopId, String terminalId, String sortOptions, Long startDate, Long endDate) {
        Iterable<CashDrawerSession> items = coll.find("{companyId:#, shopId:#,terminalId:#, created: {$gt:#, $lt:#}}", companyId, shopId, terminalId, startDate, endDate).sort(sortOptions)
                .as(entityClazz);
        SearchResult<CashDrawerSession> sessionSearchResult = new SearchResult<>();
        sessionSearchResult.setValues(Lists.newArrayList(items));
        return sessionSearchResult;
    }

    @Override
    public <E extends CashDrawerSession> SearchResult<E> getOpenDrawersForTerminal(String companyId, String shopId, String terminalId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,terminalId:#,status:#}", companyId, shopId, terminalId, CashDrawerSession.CashDrawerLogStatus.Open).sort("{startTime:-1}").as(clazz);
        long count = coll.count("{companyId:#,shopId:#,terminalId:#,status:#}", companyId, shopId, terminalId, CashDrawerSession.CashDrawerLogStatus.Open);

        SearchResult<E> sessionSearchResult = new SearchResult<>();
        sessionSearchResult.setValues(Lists.newArrayList(items));
        sessionSearchResult.setTotal(count);
        return sessionSearchResult;
    }
}
