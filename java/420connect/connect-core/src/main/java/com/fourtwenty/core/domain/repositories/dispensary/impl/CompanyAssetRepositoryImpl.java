package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;


/**
 * Created by mdo on 10/26/15.
 */
public class CompanyAssetRepositoryImpl extends CompanyBaseRepositoryImpl<CompanyAsset> implements CompanyAssetRepository {
    @Inject
    public CompanyAssetRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CompanyAsset.class, mongoManager);
    }

    @Override
    public CompanyAsset getAssetByKey(String companyId, String key) {
        return coll.findOne("{companyId:#,key:#}", companyId, key).as(entityClazz);
    }

    @Override
    public HashMap<String, CompanyAsset> getAssetByKeyAsMap(String companyId, List<String> keyNames) {
        Iterable<CompanyAsset> items = coll.find("{ companyId:#, key: {$in:#} }", companyId, keyNames).as(entityClazz);

        HashMap<String, CompanyAsset> map = new HashMap<>();
        for (CompanyAsset asset : items) {
            map.put(asset.getKey(), asset);
        }

        return map;
    }
}
