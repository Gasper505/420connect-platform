package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyBounceNumber;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyBounceNumberRepository;

import javax.inject.Inject;
import java.util.HashMap;

public class CompanyBounceNumberRepositoryImpl extends CompanyBaseRepositoryImpl<CompanyBounceNumber> implements CompanyBounceNumberRepository {
    @Inject
    public CompanyBounceNumberRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CompanyBounceNumber.class, mongoManager);
    }

    @Override
    public CompanyBounceNumber getBouncedNumber(String companyId, String number) {
        return coll.findOne("{companyId:#,number:#}", companyId, number).as(entityClazz);
    }

    @Override
    public HashMap<String, CompanyBounceNumber> listAllByCompany(String companyId) {
        Iterable<CompanyBounceNumber> items = coll.find("{companyId:#}", companyId).as(entityClazz);
        HashMap<String, CompanyBounceNumber> bounceNumbers = new HashMap<>();

        for (CompanyBounceNumber companyBounceNumber : items) {
            bounceNumbers.put(companyBounceNumber.getNumber(), companyBounceNumber);
        }

        return bounceNumbers;
    }
}
