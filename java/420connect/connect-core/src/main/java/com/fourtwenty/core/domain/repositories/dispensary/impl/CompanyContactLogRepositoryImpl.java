package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyContactLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyContactLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class CompanyContactLogRepositoryImpl extends ShopBaseRepositoryImpl<CompanyContactLog> implements CompanyContactLogRepository {

    @Inject
    public CompanyContactLogRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(CompanyContactLog.class, mongoDb);
    }

    @Override
    public SearchResult<CompanyContactLog> getAllCompanyContactLogByCustomerCompany(String companyId, String customerCompanyId, String sortOption, int start, int limit) {
        Iterable<CompanyContactLog> items = coll.find("{companyId:#,customerCompanyId:#,deleted:false}", companyId, customerCompanyId).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,customerCompanyId:#,deleted:false}", companyId, customerCompanyId);
        SearchResult<CompanyContactLog> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
