package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyContactRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.regex.Pattern;

public class CompanyContactRepositoryImpl extends ShopBaseRepositoryImpl<CompanyContact> implements CompanyContactRepository {

    @Inject
    public CompanyContactRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CompanyContact.class, mongoManager);
    }

    @Override
    public <E extends CompanyContact> E getCompanyContactById(String contactId, Class<E> clazz) {
        return coll.findOne(new ObjectId(contactId)).as(clazz);
    }

    @Override
    public <E extends CompanyContact> SearchResult<E> getAllCompanyContact(String companyId, String sortOptions, int skip, int limit, Class<E> clazz) {
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> customerCompanies = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).sort(sortOptions).as(clazz);
        long count = coll.count("{companyId:#,deleted:false}", companyId);

        searchResult.setValues(Lists.newArrayList(customerCompanies));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends CompanyContact> SearchResult<E> getAllCompanyContactByCustomerCompany(String companyId, String customerCompanyId, String sortOptions, int skip, int limit, Class<E> clazz) {
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> customerCompanies = coll.find("{companyId:#,deleted:false, customerCompanyId:#}", companyId, customerCompanyId).skip(skip).limit(limit).sort(sortOptions).as(clazz);
        long count = coll.count("{companyId:#,deleted:false, customerCompanyId:#}", companyId, customerCompanyId);

        searchResult.setValues(Lists.newArrayList(customerCompanies));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public CompanyContact getCompanyContactByEmail(String email) {
        return coll.findOne("{email:#}", email).as(entityClazz);
    }

    @Override
    public <E extends CompanyContact> SearchResult<E> getAllCompanyContactToSearch(String companyId, String sortOption, String term, int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> companyContacts = coll.find("{$and: [{companyId:#,deleted:false},{$or:[{firstName:#},{lastName:#},{email:#},{phoneNumber:#},{officeNumber:#}]}]}", companyId, pattern, pattern, pattern, pattern, pattern).sort(sortOption).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,deleted:false},{$or: [{firstName:#},{lastName:#},{email:#},{phoneNumber:#},{officeNumber:#}]}]}", companyId, pattern, pattern, pattern, pattern, pattern);

        searchResult.setValues(Lists.newArrayList(companyContacts));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends CompanyContact> SearchResult<E> getCompanyContactByCustomerCompanySearch(String companyId, String customerCompanyId, String term, String sortOption, int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> companyContacts = coll.find("{$and: [{companyId:#,deleted:false,customerCompanyId:#},{$or:[{firstName:#},{lastName:#},{email:#},{phoneNumber:#},{officeNumber:#}]}]}", companyId, customerCompanyId, pattern, pattern, pattern, pattern, pattern).sort(sortOption).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,deleted:false,customerCompanyId:#},{$or: [{firstName:#},{lastName:#},{email:#},{phoneNumber:#},{officeNumber:#}]}]}", companyId, customerCompanyId, pattern, pattern, pattern, pattern, pattern);

        searchResult.setValues(Lists.newArrayList(companyContacts));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public CompanyContact getCompanyContactByVendor(String companyId, String vendorId) {
        Iterable<CompanyContact> items = coll.find("{companyId:#, customerCompanyId:#}", companyId, vendorId).sort("{modified:-1}").as(entityClazz);

        for (CompanyContact companyContact : items) {
            return companyContact;
        }

        return null;
    }

    @Override
    public <E extends CompanyContact> SearchResult<E> findItemsWithDate(String companyId, long startTime, long endTime, int skip, int limit, String sortOptions, Class<E> clazz) {
        if (startTime < 0) startTime = 0;
        if (endTime <= 0) endTime = DateTime.now().getMillis();

        Iterable<E> items = coll.find("{companyId:#, deleted:false, modified:{$gt:#, $lt:#}}", companyId, startTime, endTime).skip(skip).limit(limit).sort(sortOptions).as(clazz);

        long count = coll.count("{companyId:#, deleted:false, modified:{$gt:#, $lt:#}}", companyId, startTime, endTime);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
