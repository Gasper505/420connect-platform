package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;

import javax.inject.Inject;

/**
 * Created by mdo on 5/15/16.
 */
public class CompanyFeaturesRepositoryImpl extends CompanyBaseRepositoryImpl<CompanyFeatures> implements CompanyFeaturesRepository {

    @Inject
    public CompanyFeaturesRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CompanyFeatures.class, mongoManager);
    }

    @Override
    public CompanyFeatures getCompanyFeature(String companyId) {
        CompanyFeatures features = coll.findOne("{companyId:#}", companyId).as(entityClazz);
        return features;
    }

    @Override
    public void addEmailCredit(String companyId, int emailCredits) {
        coll.update("{companyId:#}", companyId).with("{$inc: {emailMarketingCredits: #}}", emailCredits);
    }

    @Override
    public void addSMSCredit(String companyId, int smsCredits) {

        coll.update("{companyId:#}", companyId).with("{$inc: {smsMarketingCredits: #}}", smsCredits);
    }


    @Override
    public void subtractSMSEmailCredits(String companyId, int smsDeducts, int emailDeducts) {
        if (smsDeducts > 0) {
            smsDeducts *= -1;
        }
        if (emailDeducts > 0) {
            emailDeducts *= -1;
        }
        coll.update("{companyId:#}", companyId).with("{$inc: {smsMarketingCredits: #,emailMarketingCredits:#}}", smsDeducts, emailDeducts);

    }
}
