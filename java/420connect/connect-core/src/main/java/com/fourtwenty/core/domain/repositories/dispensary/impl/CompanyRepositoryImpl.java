package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;

import java.util.List;
import javax.inject.Inject;
import com.google.common.collect.Lists;

/**
 * Created by mdo on 9/6/15.
 */
public class CompanyRepositoryImpl extends MongoBaseRepositoryImpl<Company> implements CompanyRepository {

    @Inject
    public CompanyRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Company.class, mongoManager);
    }

    @Override
    public Company getCompanyByEmail(String emailAddress) {
        return coll.findOne("{email:#}", emailAddress).as(entityClazz);
    }

    @Override
    public List<Company> findAllCompany() {
        Iterable<Company> result = coll.find("{active:true, deleted:false}}").as(entityClazz);
        return Lists.newArrayList(result.iterator());
    }
}
