package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyUniqueSequenceRepository;

import javax.inject.Inject;

/**
 * Created by mdo on 3/6/17.
 */
public class CompanyUniqueSequenceRepositoryImpl extends CompanyBaseRepositoryImpl<CompanyUniqueSequence> implements CompanyUniqueSequenceRepository {
    @Inject
    public CompanyUniqueSequenceRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CompanyUniqueSequence.class, mongoManager);
    }

    @Override
    public CompanyUniqueSequence getNewIdentifier(String companyId, String keyName) {
        CompanyUniqueSequence sequence = coll.findAndModify("{companyId:#,name:#}", companyId, keyName)
                .with("{$set:{companyId:#,name:#},$inc:{count:1}}", companyId, keyName)
                .upsert().returnNew().as(CompanyUniqueSequence.class);
        return sequence;
    }
}
