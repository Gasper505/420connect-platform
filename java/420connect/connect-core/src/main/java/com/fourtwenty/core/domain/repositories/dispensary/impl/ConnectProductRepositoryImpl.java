package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ConnectProductRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 8/9/16.
 */
public class ConnectProductRepositoryImpl extends MongoBaseRepositoryImpl<ConnectProduct> implements ConnectProductRepository {

    @Inject
    public ConnectProductRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ConnectProduct.class, mongoManager);
    }

    @Override
    public ConnectProduct getProductBySKU(String productSKU) {
        return coll.findOne("{productSKU:#}", productSKU).as(entityClazz);
    }
}
