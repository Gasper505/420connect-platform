package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ContractRepository;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Stephen Schmidt on 12/7/2015.
 */
public class ContractRepositoryImpl extends ShopBaseRepositoryImpl<Contract> implements ContractRepository {

    @Inject
    public ContractRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Contract.class, mongoManager);
    }

    @Override
    public List<Contract> getActiveContracts(String companyId, String shopId) {
        Iterable<Contract> iters = coll.find("{companyId:#,shopId:#,active:#}", companyId, shopId, true).as(entityClazz);
        return Lists.newArrayList(iters);
    }

    @Override
    public HashMap<String, Contract> getActiveContractsAsMap(String companyId, String shopId) {
        Iterable<Contract> iters = coll.find("{companyId:#,shopId:#,active:#}", companyId, shopId, true).as(entityClazz);
        return asMap(iters);
    }

    @Override
    public void activateContract(String contractId, Boolean status) {
        coll.update(new ObjectId(contractId)).with("{$set: {active:#,modified:#}}", status, DateTime.now().getMillis());
    }


    @Override
    public void setContractsToActive(String companyId, String shopId, boolean active) {
        coll.update("{companyId:#,shopId:#}", companyId, shopId).multi().with("{$set: {active:#,modified:#}}", active, DateTime.now().getMillis());
    }
}
