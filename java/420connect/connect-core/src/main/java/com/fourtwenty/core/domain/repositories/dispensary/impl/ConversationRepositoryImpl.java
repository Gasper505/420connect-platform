package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ConversationRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;

public class ConversationRepositoryImpl extends ShopBaseRepositoryImpl<Conversation> implements ConversationRepository {

    @Inject
    private MemberRepository memberRepository;

    @Inject
    private ShopRepository shopRepository;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    public ConversationRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Conversation.class, mongoManager);
    }

    @Override
    public <E extends Conversation> List<E> getTransactionConversation(final String transactionId, final long afterDate, final long beforeDate, final Conversation.ConversationType conversationType, Class<E> clazz) {
        Iterable<E> result;
        if (conversationType == Conversation.ConversationType.TWILLIO) {
            result = coll.find("{transactionId:#, $or: [{conversationType:{$exists:false}}, {conversationType:#}], modified:{$lte:#, $gte:#}}", transactionId, conversationType, beforeDate, afterDate).as(clazz);
        } else {
            result = coll.find("{transactionId:#, conversationType:#, modified:{$lte:#, $gte:#}}", transactionId, conversationType, beforeDate, afterDate).as(clazz);
        }
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public Conversation getConversationByMessageId(final String sid) {
        return coll.findOne("{sid:#}", sid).as(entityClazz);
    }

    @Override
    public Transaction getTransactionToAndFromNumber(final String toNUmber, final String fromNUmber, final String companyId) {
        final Member member = memberRepository.getMemberWithPhoneNumber(companyId, fromNUmber);
        final Shop shop = shopRepository.getShopByPhoneNumber(toNUmber);
        return transactionRepository.getActiveTransactionsForShopAndMember(shop.getId(), member.getId());

    }

    @Override
    public <E extends Conversation> List<E> getTransactionConversationByCurrentUser(String employeeId, long afterDate, long beforeDate, Conversation.ConversationType conversationType, Class<E> clazz) {
        Iterable<E> result;
        if (conversationType == Conversation.ConversationType.TWILLIO) {
            result = coll.find("{employeeId:#, $or: [{conversationType:{$exists:false}}, {conversationType:#}], modified:{$lte:#, $gte:#}}", employeeId, conversationType, beforeDate, afterDate).as(clazz);
        } else {
            result = coll.find("{employeeId:#, conversationType:#, modified:{$lte:#, $gte:#}}", employeeId, conversationType, beforeDate, afterDate).as(clazz);

        }
        return Lists.newArrayList(result.iterator());

    }

    @Override
    public <E extends Conversation> List<E> getActiveTransactionChat(List<String> transactionIds, Class<E> clazz) {
        Iterable<E> result = coll.find("{transactionId:{$in:#}}", transactionIds).as(clazz);
        return Lists.newArrayList(result.iterator());
    }
}
