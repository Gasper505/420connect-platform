package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CreditHistory;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CreditHistoryRepository;

import javax.inject.Inject;

public class CreditHistoryRepositoryImpl extends ShopBaseRepositoryImpl<CreditHistory> implements CreditHistoryRepository {

    @Inject
    public CreditHistoryRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(CreditHistory.class, mongoDb);
    }
}
