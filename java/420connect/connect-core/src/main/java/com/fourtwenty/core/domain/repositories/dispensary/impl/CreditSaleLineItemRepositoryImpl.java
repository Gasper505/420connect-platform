package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.credits.CreditSaleLineItem;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CreditSaleLineItemRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 7/17/17.
 */
public class CreditSaleLineItemRepositoryImpl extends ShopBaseRepositoryImpl<CreditSaleLineItem>
        implements CreditSaleLineItemRepository {

    @Inject
    public CreditSaleLineItemRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CreditSaleLineItem.class, mongoManager);
    }
}
