package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CultivationTaxInfo;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CultivationTaxInfoRepository;

import javax.inject.Inject;

public class CultivationTaxInfoRepositoryImpl extends MongoBaseRepositoryImpl<CultivationTaxInfo> implements CultivationTaxInfoRepository {

    @Inject
    public CultivationTaxInfoRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CultivationTaxInfo.class, mongoManager);
    }

    @Override
    public CultivationTaxInfo getCultivationTaxInfoByState(String state, String country) {
        return coll.findOne("{state:#,country:#,deleted:false}", state, country).as(entityClazz);
    }
}
