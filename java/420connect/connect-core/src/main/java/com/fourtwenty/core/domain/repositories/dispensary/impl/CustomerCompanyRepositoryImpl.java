package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CustomerCompany;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.CustomerCompanyRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.regex.Pattern;

public class CustomerCompanyRepositoryImpl extends ShopBaseRepositoryImpl<CustomerCompany> implements CustomerCompanyRepository {

    @Inject
    public CustomerCompanyRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CustomerCompany.class, mongoManager);
    }

    @Override
    public CustomerCompany getCustomerCompanyByEmail(String emailAddress) {
        return coll.findOne("{email:#}", emailAddress).as(entityClazz);
    }


    @Override
    public SearchResult<CustomerCompany> getAllCustomerCompany(String companyId, int skip, int limit, String term) {
        Pattern pattern = TextUtil.createPattern(term);
        SearchResult<CustomerCompany> searchResult = new SearchResult<>();
        Iterable<CustomerCompany> customerCompanies = coll.find("{$and: [{companyId:#,deleted:false},{$or:[{name:#},{companyType:#},{licenceType:#}]}]}", companyId, pattern, pattern, pattern).skip(skip).limit(limit).as(CustomerCompany.class);
        long count = coll.count("{$and: [{companyId:#,deleted:false},{$or: [{name:#},{companyType:#},{licenceType:#}]}]}", companyId, pattern, pattern, pattern);

        searchResult.setValues(Lists.newArrayList(customerCompanies));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<CustomerCompany> getAllCustomerCompany(String companyId, String sortOptions, int skip, int limit) {
        SearchResult<CustomerCompany> searchResult = new SearchResult<>();
        Iterable<CustomerCompany> customerCompanies = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).sort(sortOptions).as(CustomerCompany.class);
        long count = coll.count("{companyId:#,deleted:false}", companyId);

        searchResult.setValues(Lists.newArrayList(customerCompanies));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

}
