package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.DeliveryZone;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.DeliveryAreaRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

/**
 * Created by decipher on 28/11/17 5:55 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class DeliveryAreaRepositoryImpl extends CompanyBaseRepositoryImpl<DeliveryZone> implements DeliveryAreaRepository {

    @Inject
    public DeliveryAreaRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(DeliveryZone.class, mongoManager);
    }

    @Override
    public SearchResult<DeliveryZone> getAllDeliveryArea(String companyId, String shopId, int skip, int limit, String sortOptions) {
        SearchResult<DeliveryZone> searchResult = new SearchResult<>();
        Iterable<DeliveryZone> deliveryAreas = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);

        searchResult.setValues(Lists.newArrayList(deliveryAreas));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<DeliveryZone> getAllDeliveryAreaByZoneType(String companyId, String shopId, int skip, int limit, String sortOptions, DeliveryZone.DeliveryZoneType zoneType) {
        SearchResult<DeliveryZone> searchResult = new SearchResult<>();
        Iterable<DeliveryZone> deliveryAreas = coll.find("{companyId:#,shopId:#,zoneType:#,deleted:false}", companyId, shopId, zoneType).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,zoneType:#,deleted:false}", companyId, shopId, zoneType);

        searchResult.setValues(Lists.newArrayList(deliveryAreas));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }
}
