package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.DeliveryTaxRate;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.DeliveryTaxRateRepository;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import java.util.List;

public class DeliveryTaxRateRepositoryImpl extends ShopBaseRepositoryImpl<DeliveryTaxRate> implements DeliveryTaxRateRepository {

    @Inject
    public DeliveryTaxRateRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(DeliveryTaxRate.class, mongoManager);
    }

    @Override
    public <E extends DeliveryTaxRate> List<E> listAllByShopId(String companyId, String shopId, String sortBy, Class<E> clazz) {
        Iterable<E> result = coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).sort(sortBy).as(clazz);
        return Lists.newArrayList(result);
    }
}
