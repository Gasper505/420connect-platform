package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.DerivedProductBatchLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.DerivedProductBatchLogRepository;

import javax.inject.Inject;

public class DerivedProductBatchLogRepositoryImpl  extends ShopBaseRepositoryImpl<DerivedProductBatchLog> implements DerivedProductBatchLogRepository {

    @Inject
    public DerivedProductBatchLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(DerivedProductBatchLog.class, mongoManager);
    }
}
