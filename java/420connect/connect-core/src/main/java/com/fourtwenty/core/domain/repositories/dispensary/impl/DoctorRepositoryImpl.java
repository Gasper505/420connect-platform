package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.DoctorRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.regex.Pattern;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
public class DoctorRepositoryImpl extends CompanyBaseRepositoryImpl<Doctor> implements DoctorRepository {
    @Inject
    public DoctorRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Doctor.class, mongoManager);
    }

    @Override
    public Doctor getDoctorByLicense(String companyId, String license) {
        return coll.findOne("{companyId:#,license:#}", companyId, license).as(entityClazz);
    }

    @Override
    public Doctor getDoctorByImportId(String companyId, String importId) {
        return coll.findOne("{companyId:#,importId:#}", companyId, importId).as(entityClazz);
    }

    @Override
    public SearchResult<Doctor> searchDoctorsByTerm(String companyId, String term, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Doctor> items = coll.find("{$and: [{companyId:#},{$or: [{firstName:#},{lastName:#},{license:#}]}]}", companyId, pattern, pattern, pattern).as(entityClazz);

        long count = coll.count("{$and: [{companyId:#},{$or: [{firstName:#},{lastName:#},{license:#}]}]}", companyId, pattern, pattern, pattern);

        SearchResult<Doctor> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }
}
