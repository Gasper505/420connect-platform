package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.DriverLicenseLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.DriverLicenseLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class DriverLicenseLogRepositoryImpl extends ShopBaseRepositoryImpl<DriverLicenseLog> implements DriverLicenseLogRepository {

    @Inject
    public DriverLicenseLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(DriverLicenseLog.class, mongoManager);
    }

    @Override
    public SearchResult<DriverLicenseLog> findItems(String companyId, String shopId, int start, int limit) {
        Iterable<DriverLicenseLog> items = coll.find("{companyId:#,shopId:#,deleted:false,active:true}", companyId, shopId).sort("{modified:-1}").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:true}", companyId, shopId);

        SearchResult<DriverLicenseLog> searchResult = new SearchResult<>();
        searchResult.setLimit(limit);
        searchResult.setSkip(start);
        searchResult.setTotal(count);
        searchResult.setValues(Lists.newArrayList(items));
        return searchResult;
    }
}
