package com.fourtwenty.core.domain.repositories.dispensary.impl;


import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Organization;
import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.OrganizationRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.EmployeeLocationResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.mongodb.WriteResult;

import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

public class EmployeeRepositoryImpl extends CompanyBaseRepositoryImpl<Employee> implements EmployeeRepository {
    private static final String ORGANIZATION_QUERY = "$or:[{companyId:#}, {organizationIds:{$in:#}}]";

    @Inject
    public EmployeeRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Employee.class, mongoManager);
    }

    @Inject
    private OrganizationRepository organizationRepository;

    private boolean belongToCompanyOrganization(Employee employee, String companyId){
        boolean belongs = false;
        for(String organizationId :  employee.getOrganizationIds()){
            Organization organization = organizationRepository.getById(organizationId);
            if(organization.getCompanyIds().contains(companyId)){
                belongs = true;
                break;
            }
        }
        return belongs;
    }

    private List<String> getCompanyOrganizationIds(String companyId){
        List<Organization> organizations = organizationRepository.getOrganizationsByCompany(companyId);
        List<String> organizationIds = organizations.stream().map(e -> e.getId()).collect(Collectors.toList());
        return organizationIds;
    }

    @Override
    public Employee get(String companyId, String id) {
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }

        Employee employee = coll.findOne("{_id:#}", new ObjectId(id)).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            employee = coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(id)).as(entityClazz);
        }

         return employee;
    }

    /* Get employee and check if employee belongs to company's organization */
    @Override
    public Employee get(String companyId, String id, String projection) {
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }

        Employee employee = coll.findOne("{_id:#}", new ObjectId(id)).projection(projection).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            employee = coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(id)).projection(projection).as(entityClazz);
        }

        return employee;
    }


    @Override
    public WriteResult updateEmployeeLocation(String companyId, String employeeId, TerminalLocation recentLocation) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);
        if(belongToCompanyOrganization(employee, companyId)) {
            companyId = employee.getCompanyId();
        }
        return coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {recentLocation:#, modified:#}}", recentLocation, DateTime.now().getMillis());
    }

    @Override
    public Employee getEmployeeByQuickPin(String companyId, String quickPin) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        return coll.findOne("{$or:[{companyId:#}, {organizationIds:{$in:#}}],pin:#}", companyId, organizationIds, quickPin).projection("{password:0,pin:0}").as(entityClazz);
    }

    @Override
    public Employee getEmployeeByEmail(String email) {
        return coll.findOne("{email:#}", email).as(entityClazz);
    }

    @Override
    public DateSearchResult<Employee> findItemsWithDate(String companyId, long afterDate, long beforeDate) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<Employee> items = coll.find("{" + ORGANIZATION_QUERY +", modified:{$lt:#, $gt:#}}", companyId, organizationIds, beforeDate, afterDate)
                .projection("{password:0,pin:0}")
                .as(entityClazz);

        long count = coll.count("{" + ORGANIZATION_QUERY +", modified:{$lt:#, $gt:#}}", companyId, organizationIds, beforeDate, afterDate);

        DateSearchResult<Employee> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends CompanyBaseModel> SearchResult<E> findItems(String companyId, int skip, int limit, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);

        Iterable<E> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + "},{deleted:false}]}", companyId, organizationIds).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + "},{deleted:false}]}", companyId, organizationIds);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Employee> SearchResult<E> findItems(String companyId, int skip, int limit, String projections, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + "},{deleted:false}]}", companyId, organizationIds).skip(skip).limit(limit).projection(projections).as(clazz);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + "},{deleted:false}]}", companyId, organizationIds);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Employee> SearchResult<E> findItems(String companyId, String searchTerm, int skip, int limit, String projections, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        List<String> organizationIds = getCompanyOrganizationIds(companyId);

        Iterable<E> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + "}, {$or: [{firstName:#},{lastName:#},{email:#}]}],deleted:false}", companyId, organizationIds, pattern, pattern, pattern).skip(skip).limit(limit)
                .projection(projections)
                .as(clazz);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + "}, {$or: [{firstName:#},{lastName:#},{email:#}]}],deleted:false}", companyId, organizationIds, pattern, pattern, pattern);

        SearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Employee> SearchResult<E> findItemsByShop(String companyId, String shopId, String searchTerm, int skip, int limit, String projections, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + ",shops:#,deleted:false,driver:true,disabled:false}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds, shopId, pattern, pattern, pattern).skip(skip).limit(limit)
                .projection("{password:0,pin:0}")
                .as(clazz);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + ",shops:#,deleted:false,driver:true,disabled:false}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds, shopId, pattern, pattern, pattern);

        SearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Employee> SearchResult<E> findItemsByShop(String companyId, String shopId, int skip, int limit, String projections, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{" + ORGANIZATION_QUERY + ",shops:#,deleted:false,driver:true,disabled:false}", companyId, organizationIds, shopId).skip(skip).limit(limit).projection(projections).as(clazz);

        long count = coll.count("{" + ORGANIZATION_QUERY + ",shops:#,deleted:false,driver:true,disabled:false}", companyId, organizationIds, shopId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void setRecentTimecard(String companyId, String employeeId, String timecardId) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {timecardId:#,modified:#}}", timecardId, DateTime.now().getMillis());
        } else {
            coll.update("{_id:#}", new ObjectId(employeeId)).with("{$set: {timecardId:#,modified:#}}", timecardId, DateTime.now().getMillis());
        }
    }

    @Override
    public void setTerminalId(String companyId, String employeeId, String terminalId) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {assignedTerminalId:#,modified:#}}", terminalId, DateTime.now().getMillis());
        } else {
            coll.update("{_id:#}", new ObjectId(employeeId)).with("{$set: {assignedTerminalId:#,modified:#}}", terminalId, DateTime.now().getMillis());
        }
    }

    @Override
    public List<Employee> getEmployees(String companyId) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<Employee> result = coll.find("{" + ORGANIZATION_QUERY + ",deleted:false}", companyId, organizationIds).as(entityClazz);
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public void deleteEmployee(String companyId, String employeeId, String newPin, String newEmail) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {deleted:true,active:false,disabled:true,modified:#,pin:#,email:#}}", DateTime.now().getMillis(), newPin, newEmail);
        } else {
            coll.update("{_id:#}", new ObjectId(employeeId)).with("{$set: {deleted:true,active:false,disabled:true,modified:#,pin:#,email:#}}", DateTime.now().getMillis(), newPin, newEmail);
        }
    }


    @Override
    public Iterable<Employee> getEmployeesWithNoShops() {
        return coll.find("{shops.0:{$exists:false}}").as(entityClazz);
    }

    @Override
    public WriteResult setEmployeesShops(String companyId, String employeeId, List<String> shopIds) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            return coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {shops:#, modified:#}}", shopIds, DateTime.now().getMillis());
        }else {
            return coll.update("{_id:#}", new ObjectId(employeeId)).with("{$set: {shops:#, modified:#}}", shopIds, DateTime.now().getMillis());
        }
    }

    @Override
    public WriteResult updateEmployeeAssignedTerminal(String companyId, String employeeId, String assignedTerminalId) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);

        if(!belongToCompanyOrganization(employee, companyId)) {
            return coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {assignedTerminalId:#, modified:#}}", assignedTerminalId, DateTime.now().getMillis());
        }else {
            return coll.update("{_id:#}", new ObjectId(employeeId)).with("{$set: {assignedTerminalId:#, modified:#}}", assignedTerminalId, DateTime.now().getMillis());
        }
    }

    @Override
    public <E extends Employee> SearchResult findItemsByRole(String companyId, int skip, int limit, String projections, String roleId, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{" + ORGANIZATION_QUERY + ",roleId:#,deleted:false}", companyId, organizationIds, roleId).skip(skip).limit(limit).projection(projections).as(clazz);

        long count = coll.count("{" + ORGANIZATION_QUERY + ",roleId:#,deleted:false}", companyId, organizationIds, roleId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Employee> SearchResult findItemsByRoleAndSearchTerms(String companyId, String searchTerm, int skip, int limit, String projection, String roleId, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + ",roleId:#}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds , roleId, pattern, pattern, pattern).skip(skip).projection(projection).limit(limit)
                .as(clazz);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + ",roleId:#}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds , roleId, pattern, pattern, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<EmployeeLocationResult> getDriverList(String companyId, String roleId) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        return coll.find("{" + ORGANIZATION_QUERY + ",roleId:#,deleted:false}", companyId, organizationIds,roleId).as(EmployeeLocationResult.class);
    }

    @Override
    public <E extends Employee> HashMap<String, E> getEmployeesAsMap(String companyId, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{" + ORGANIZATION_QUERY + ",deleted:false}", companyId, organizationIds).as(clazz);
        HashMap<String, E> map = new HashMap<>();
        for (E item : items) {
            map.put(item.getId(), item);
        }
        return map;
    }

    @Override
    public <E extends Employee> E getEmployeeById(String employeeId, String companyId, String projection, Class<E> clazz) {
        if (employeeId == null || !ObjectId.isValid(employeeId)) {
            return null;
        }
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);
        if(!belongToCompanyOrganization(employee, companyId)) {
            return coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(employeeId)).projection(projection).as(clazz);
        } else{
            return coll.findOne("{_id:#}", new ObjectId(employeeId)).projection(projection).as(clazz);
        }
    }

    @Override
    public List<Employee> getEmployeesByShop(String companyId, String shopId) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Employee> results = coll.aggregate("{$match: {employeeOnFleetInfoList : { $elemMatch: {shopId: #}}}}", shopId)
                .and("{$match:{" + ORGANIZATION_QUERY + ",deleted:false}}", companyId, organizationIds)
                .options(aggregationOptions)
                .as(entityClazz);

        return Lists.newArrayList((Iterable<Employee>) results);
    }

    @Override
    public void bulkUpdateEmployeeAppAccess(String companyId, List<ObjectId> objectIds, LinkedHashSet<CompanyFeatures.AppTarget> appTargets) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        coll.update("{" + ORGANIZATION_QUERY + ",_id: {$in:#}}", companyId, organizationIds, objectIds).multi().with("{$set: {appAccessList: #, modified:#}}", appTargets, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateEmployeeWithSingleAppAccess(String companyId, List<ObjectId> objectIds, CompanyFeatures.AppTarget appTarget) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        coll.update("{" + ORGANIZATION_QUERY + ",_id: {$in:#}}", companyId, organizationIds, objectIds).multi().with("{$addToSet: {appAccessList: #}}", appTarget);
    }

    @Override
    public <E extends Employee> HashMap<String, E> listEmployeeAsMap(String companyId, List<ObjectId> employeeIds, Class<E> clazz) {
        Iterable<E> items = findItemsIn(companyId, employeeIds, clazz);
        HashMap<String, E> map = new HashMap<>();
        for (E item : items) {
            item.setPassword(null);
            item.setPin(null);
            map.put(item.getId(), item);
        }
        return map;
    }

    @Override
    public void updateLastLogin(String companyId, String employeeId, String logInShopId) {
        Employee employee = coll.findOne("{_id:#}", new ObjectId(employeeId)).as(entityClazz);
        if(!belongToCompanyOrganization(employee, companyId)) {
            coll.update("{companyId:#,_id:#}", companyId, new ObjectId(employeeId)).with("{$set: {lastLoggedInShopId:#, lastLoginTime:#, modified:#}}", logInShopId, DateTime.now().getMillis(),DateTime.now().getMillis());
        }else{
            coll.update("{_id:#}", new ObjectId(employeeId)).with("{$set: {lastLoggedInShopId:#, lastLoginTime:#, modified:#}}", logInShopId, DateTime.now().getMillis(),DateTime.now().getMillis());
        }
    }

    @Override
    public List<Employee> getTookanEmployeesByShop(String companyId, String shopId) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<Employee> items = coll.find("{" + ORGANIZATION_QUERY + ",deleted:false,tookanInfoList : { $elemMatch: {shopId: #}}}", companyId, organizationIds, shopId).as(entityClazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Employee> SearchResult<E> findItemsByAccessConfig(String companyId, int start, int limit, String projection, List<CompanyFeatures.AppTarget> accessConfigs, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{" + ORGANIZATION_QUERY + ", deleted:false, appAccessList:{$in:#}}", companyId, organizationIds, accessConfigs).skip(start).projection(projection).limit(limit)
                .as(clazz);

        long count = coll.count("{" + ORGANIZATION_QUERY + ", deleted:false, appAccessList:{$in:#}}", companyId, organizationIds, accessConfigs);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Employee> SearchResult<E> findItemsByAccessConfigAndTerms(String companyId, String term, int start, int limit, String projection, List<CompanyFeatures.AppTarget> accessConfigs, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + ",deleted:false, appAccessList:{$in:#}}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds, accessConfigs, pattern, pattern, pattern).skip(start).projection(projection).limit(limit)
                .as(clazz);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + ",deleted:false, appAccessList:{$in:#}}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds, accessConfigs, pattern, pattern, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<EmployeeResult> findItemsByShops(String companyId, List<String> shopIds, int skip, int limit, String projections) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<EmployeeResult> items = coll.find("{" + ORGANIZATION_QUERY + ",shops:{$in:#},deleted:false}", companyId, organizationIds, shopIds).skip(skip).limit(limit).projection(projections).as(EmployeeResult.class);

        long count = coll.count("{" + ORGANIZATION_QUERY + ",shops:{$in:#},deleted:false}", companyId, organizationIds, shopIds);

        SearchResult<EmployeeResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<EmployeeResult> findItemsByShopsAndTerm(String companyId, List<String> shopIds, String searchTerm, int skip, int limit, String projections) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<EmployeeResult> items = coll.find("{$and: [{" + ORGANIZATION_QUERY + ",shops:{$in:#}}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds, shopIds, pattern, pattern, pattern).skip(skip).limit(limit)
                .projection("{password:0,pin:0}")
                .as(EmployeeResult.class);

        long count = coll.count("{$and: [{" + ORGANIZATION_QUERY + ",shops:{$in:#}}, {$or: [{firstName:#},{lastName:#},{email:#}]}]}", companyId, organizationIds, shopIds, pattern, pattern, pattern);

        SearchResult<EmployeeResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void updateDriverInfo(List<String> roleIds, boolean state) {
        coll.update("{roleId: {$in:#}}", roleIds).multi().with("{$set: {driver: #, modified:#}}", state, DateTime.now().getMillis());
    }

    @Override
    public <E extends Employee> SearchResult<E> findDriverEmployeeByShop(String companyId, String shopId, int start, int limit, String projection, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{ " + ORGANIZATION_QUERY + ", shops:#, deleted: false, disabled:false, driver:true}", companyId, organizationIds, shopId).sort("{firstName:-1}").skip(start).limit(limit).projection(projection).as(clazz);
        long count = coll.count("{ " + ORGANIZATION_QUERY + ", shops:#, deleted: false, disabled:false, driver:true}", companyId, organizationIds,shopId);

        SearchResult<E> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setSkip(start);
        result.setLimit(limit);

        return result;
    }

    @Override
    public <E extends Employee> SearchResult<E> findAllEmployee(String companyId, int skip, int limit, String projections, Class<E> clazz) {
        List<String> organizationIds = getCompanyOrganizationIds(companyId);
        Iterable<E> items = coll.find("{" + ORGANIZATION_QUERY + "}", companyId, organizationIds).skip(skip).limit(limit).projection(projections).as(clazz);

        long count = coll.count("{" + ORGANIZATION_QUERY + "}", companyId, organizationIds);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}