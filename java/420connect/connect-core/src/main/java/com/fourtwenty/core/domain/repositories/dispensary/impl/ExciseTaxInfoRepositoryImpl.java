package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ExciseTaxInfoRepository;

import javax.inject.Inject;

public class ExciseTaxInfoRepositoryImpl extends MongoBaseRepositoryImpl<ExciseTaxInfo> implements ExciseTaxInfoRepository {

    @Inject
    public ExciseTaxInfoRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ExciseTaxInfo.class, mongoManager);
    }

    @Override
    public ExciseTaxInfo getExciseTaxInfoByState(String state, String country) {
        return coll.findOne("{state:#,country:#,deleted:false, $or:[{cannabisTaxType:{$exists:false}},{cannabisTaxType:#}]}", state, country, ExciseTaxInfo.CannabisTaxType.Cannabis).as(entityClazz);
    }

    @Override
    public ExciseTaxInfo getExciseTaxInfoByState(String state, String country, ExciseTaxInfo.CannabisTaxType cannabisTaxType) {
        return (cannabisTaxType == ExciseTaxInfo.CannabisTaxType.Cannabis) ? getExciseTaxInfoByState(state, country) :
                coll.findOne("{state:#,country:#,deleted:false, cannabisTaxType:#}", state, country, cannabisTaxType).as(entityClazz);
    }
}
