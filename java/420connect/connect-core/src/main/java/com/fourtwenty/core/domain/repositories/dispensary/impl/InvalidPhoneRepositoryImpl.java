package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.InvalidPhone;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.InvalidPhoneRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 7/23/17.
 */
public class InvalidPhoneRepositoryImpl extends MongoBaseRepositoryImpl<InvalidPhone> implements InvalidPhoneRepository {

    @Inject
    public InvalidPhoneRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(InvalidPhone.class, mongoManager);
    }

    @Override
    public boolean isInvalidPhone(String phoneNumber) {
        return coll.count("{phoneNumber:#,valid:false}", phoneNumber) > 0;
    }
}
