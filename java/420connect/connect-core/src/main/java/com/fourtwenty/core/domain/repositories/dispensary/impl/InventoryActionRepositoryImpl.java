package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class InventoryActionRepositoryImpl extends ShopBaseRepositoryImpl<InventoryOperation> implements InventoryActionRepository {

    @javax.inject.Inject
    public InventoryActionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(InventoryOperation.class, mongoManager);
    }


    @Override
    public InventoryOperation getLastActionBySource(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType) {
        final Iterable<InventoryOperation> inventoryActionsIterable = coll.find("{companyId:#, shopId:#, sourceId:#, sourceType:#}", companyId, shopId, sourceId, sourceType).sort("{created:-1}").as(entityClazz);
        final ArrayList<InventoryOperation> inventoryOperations = Lists.newArrayList(inventoryActionsIterable);
        if (!inventoryOperations.isEmpty()) {
            return inventoryOperations.get(0);
        }
        return null;
    }


    @Override
    public Iterable<InventoryOperation> getActionsBySourceRequest(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType, String requestId) {
        final Iterable<InventoryOperation> inventoryActions = coll.find("{companyId:#, shopId:#, sourceId:#, sourceType:#,requestId:#}", companyId, shopId, sourceId, sourceType, requestId).as(entityClazz);
        return inventoryActions;
    }

    @Override
    public List<InventoryOperation> getActionsBySource(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType) {
        final Iterable<InventoryOperation> inventoryActions = coll.find("{companyId:#, shopId:#, sourceId:#, sourceType:#}", companyId, shopId, sourceId, sourceType).sort("{created:1}").as(entityClazz);
        return Lists.newArrayList(inventoryActions);
    }


    @Override
    public Iterable<InventoryOperation> getActionsBySource(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate, String productId) {
        if (StringUtils.isNotBlank(productId)) {
            return coll.find("{ companyId:#,shopId:#,sourceType:#,created : {$gte:#,$lte:#}, productId:#}", companyId, shopId, sourceType, startDate, endDate, productId).sort("{created:1}").as(entityClazz);
        } else {
            return coll.find("{ companyId:#,shopId:#,sourceType:#,created : {$gte:#,$lte:#}}", companyId, shopId, sourceType, startDate, endDate).sort("{created:1}").as(entityClazz);
        }
    }

    @Override
    public Iterable<InventoryOperation> getAllActions(String companyId, String shopId, long startDate, long endDate, String productId) {
        if (StringUtils.isNotBlank(productId)) {
            return coll.find("{ companyId:#,shopId:#,created : {$gte:#,$lte:#}, productId:#}", companyId, shopId, startDate, endDate, productId).sort("{created:1}").as(entityClazz);
        } else {
            return coll.find("{ companyId:#,shopId:#,created : {$gte:#,$lte:#}}", companyId, shopId, startDate, endDate).sort("{created:1}").as(entityClazz);
        }
    }


    @Override
    public InventoryOperation getRecentInventoryAction(String companyId, String shopId, String productId, String batchId, String inventoryId) {
        return coll.findOne("{companyId:#,shopId:#,productId:#,batchId:#,inventoryId:#,prepackaged:false}", companyId, shopId, productId, batchId, inventoryId).orderBy("{created:-1}").as(entityClazz);

    }

    @Override
    public InventoryOperation getRecentPrepackageInventoryAction(String companyId, String shopId, String productId, String batchId, String inventoryId, String prepackageItemId) {
        return coll.findOne("{companyId:#,shopId:#,productId:#,batchId:#,inventoryId:#,prepackaged:true,prepackageItemId:#}", companyId, shopId, productId, batchId, inventoryId, prepackageItemId).orderBy("{created:-1}").as(entityClazz);
    }


    @Override
    public InventoryOperation getRecentInventoryAction(String companyId, String shopId, String sourceId) {
        return coll.findOne("{companyId:#,shopId:#,sourceId:#}", companyId, shopId, sourceId).orderBy("{created:-1}").as(entityClazz);
    }

    @Override
    public InventoryOperation getRecentInventoryAction(String companyId, String shopId, String sourceId, String sourceChildId) {
        return coll.findOne("{companyId:#,shopId:#,sourceId:#,sourceChildId:#}", companyId, shopId, sourceId,sourceChildId).orderBy("{created:-1}").as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getRecentInventoryActionsByRequestId(String companyId, String shopId, String sourceChildId, String requestId) {
        return coll.find("{companyId:#,shopId:#,sourceChildId:#,requestId:#}", companyId, shopId,sourceChildId,requestId).as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getBracketOperations(String companyId, String shopId, long startDate, long endDate, String projection) {
        return coll.find("{companyId:#, shopId:#, created: {$gt:#, $lt:#}}", companyId, shopId, startDate, endDate).projection(projection).as(InventoryOperation.class);
    }

    @Override
    public long countOperation(String companyId, String shopId, long startTime, long endTime) {
        return coll.count("{companyId:#, shopId:#, created: {$gt:#, $lt:#}}", companyId, shopId, startTime, endTime);
    }

    @Override
    public Iterable<InventoryOperation> getOperationsByProducts(String companyId, String shopId, List<String> productIds) {
        return coll.find("{ companyId:#, shopId:#, productId:{$in:#}}", companyId, shopId, productIds).as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getActionsByTime(String companyId, String shopId, long startTime) {
        return coll.find("{companyId:#, shopId:#, created: {$gte:#}}", companyId, shopId, startTime).as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getOperationsBySourceTypeForProducts(String companyId, String shopId, long startTime, List<InventoryOperation.SourceType> sourceTypes, List<String> productIds) {
        return coll.find("{ companyId:#, shopId:#, sourceType:{$in:#}, created : {$gte:#}, productId:{$in:#}}", companyId, shopId, sourceTypes, startTime, productIds).as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getAllActionsWithBatch(String companyId, String shopId, long startDate, long endDate, String productBatchId) {
        return coll.find("{ companyId:#,shopId:#,created : {$gte:#,$lte:#}, batchId:#}", companyId, shopId, startDate, endDate, productBatchId).sort("{created:1}").as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getAllActionsBySourceWithBatch(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate, String productBatchId) {
        return coll.find("{ companyId:#,shopId:#,sourceType:#,created : {$gte:#,$lte:#}, batchId:#}", companyId, shopId, sourceType, startDate, endDate, productBatchId).sort("{created:1}").as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getActionByProductWithBatch(String companyId, String shopId, long startDate, long endDate, String productId, String productBatchId) {
        return coll.find("{ companyId:#,shopId:#,created : {$gte:#,$lte:#},  productId:#, batchId:#}", companyId, shopId, startDate, endDate, productId, productBatchId).sort("{created:1}").as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getBySourceWithProductAndBatch(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate, String productId, String productBatchId) {
        return coll.find("{ companyId:#,shopId:#,sourceType:#,created : {$gte:#,$lte:#}, productId:#, batchId:#}", companyId, shopId, sourceType, startDate, endDate, productId, productBatchId).sort("{created:1}").as(entityClazz);
    }

    @Override
    public SearchResult<InventoryOperation> getLogsByInventoryId(String companyId, String shopId, String assignedInventoryId, String employeeId, int start, int limit, InventoryOperation.SourceType sourceType) {

        Iterable<InventoryOperation> items = coll.find("{companyId:#,shopId: #, deleted:false, inventoryId:#,employeeId:#, sourceType:#}", companyId, shopId, assignedInventoryId, employeeId, sourceType).sort("{modified:1}").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false, inventoryId:#, employeeId:#, sourceType:#}", companyId, shopId, assignedInventoryId, employeeId, sourceType);

        SearchResult<InventoryOperation> inventorySearchResult = new SearchResult<>();
        inventorySearchResult.setSkip(start);
        inventorySearchResult.setLimit(limit);
        inventorySearchResult.setTotal(count);
        inventorySearchResult.setValues(Lists.newArrayList(items));
        return inventorySearchResult;
    }

    @Override
    public Iterable<InventoryOperation> getOperationLogsBySourceType(String companyId, String shopId, InventoryOperation.SourceType sourceType, long startDate, long endDate) {
        return coll.find("{ companyId:#,shopId:#, deleted:false, sourceType:#,created : {$gte:#,$lte:#}}", companyId, shopId, sourceType, startDate, endDate).sort("{created:1}").as(entityClazz);
    }

    @Override
    public Iterable<InventoryOperation> getAllLogActions(String companyId, String shopId, long startDate, long endDate) {
        return coll.find("{ companyId:#,shopId:#,created : {$gte:#,$lte:#}}", companyId, shopId, startDate, endDate).sort("{created:1}").as(entityClazz);
    }

    @Override
    public InventoryOperation getLastActionBySource(String companyId, String shopId, String sourceId, InventoryOperation.SourceType sourceType, InventoryOperation.SubSourceAction subSourceAction, String batchId) {
        final Iterable<InventoryOperation> inventoryActionsIterable = coll.find("{companyId:#, shopId:#, batchId:#, sourceId:#, sourceType:#, subSourceAction:#}", companyId, shopId, batchId, sourceId, sourceType, subSourceAction).sort("{created:-1}").as(entityClazz);
        final ArrayList<InventoryOperation> inventoryOperations = Lists.newArrayList(inventoryActionsIterable);
        if (!inventoryOperations.isEmpty()) {
            return inventoryOperations.get(0);
        }
        return null;
    }
}
