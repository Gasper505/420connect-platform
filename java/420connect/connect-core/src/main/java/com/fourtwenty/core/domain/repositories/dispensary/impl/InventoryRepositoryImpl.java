package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by mdo on 3/13/16.
 */
public class InventoryRepositoryImpl extends ShopBaseRepositoryImpl<Inventory> implements InventoryRepository {

    @Inject
    public InventoryRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Inventory.class, mongoManager);
    }

    @Override
    public Inventory getInventory(String companyId, String shopId, String name) {
        return coll.findOne("{companyId:#,shopId:#,deleted:false,name:#}", companyId, shopId, name).as(entityClazz);
    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }


    @Override
    public Iterable<Inventory> getInventoriesByStatus(String companyId, String shopId, List<ObjectId> inventories, boolean status) {
        return coll.find("{companyId:#, shopId:#, _id:{$in:#}, deleted:false, active:#}", companyId, shopId, inventories, status).as(entityClazz);
    }


    @Override
    public Inventory getInventoryByExternalId(String companyId, String shopId, String externalId) {
        return coll.findOne("{companyId:#,shopId:#,deleted:false,externalId:#}", companyId, shopId, externalId).as(entityClazz);
    }
}
