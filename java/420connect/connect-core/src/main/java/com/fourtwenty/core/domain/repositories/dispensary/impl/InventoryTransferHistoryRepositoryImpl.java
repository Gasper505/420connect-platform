package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryTransferHistoryRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Raja Dushyant Vashishtha
 */
public class InventoryTransferHistoryRepositoryImpl extends ShopBaseRepositoryImpl<InventoryTransferHistory> implements InventoryTransferHistoryRepository {

    @Inject
    public InventoryTransferHistoryRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(InventoryTransferHistory.class, mongoManager);
    }

    @Override
    public <E extends InventoryTransferHistory> SearchResult<E> findItemByModifiedDate(String companyId, String shopId, Long startDate, Long endDate, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, boolean status, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (status) {
            items = coll.find("{companyId:#,shopId:{$ne:#},toShopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#}}", companyId, shopId, shopId, startDate, endDate, statusList).sort(sortOptions).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:{$ne:#},toShopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#}}", companyId, shopId, shopId, startDate, endDate, statusList);

        } else {
            items = coll.find("{companyId:#,shopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#}}", companyId, shopId, startDate, endDate, statusList).sort(sortOptions).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#}}", companyId, shopId, startDate, endDate, statusList);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends InventoryTransferHistory> SearchResult<E> findItemByStatus(String companyId, String shopId, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, boolean status, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (status) {
            items = coll.find("{companyId:#,shopId:{$ne:#},toShopId:#,deleted:false,status:{$in:#}}", companyId, shopId, shopId, statusList).sort(sortOptions).skip(start).limit(limit).as(clazz);
            count = coll.count("{companyId:#,shopId:{$ne:#},toShopId:#,deleted:false,status:{$in:#}}", companyId, shopId, shopId, statusList);
        } else {
            items = coll.find("{companyId:#,shopId:#,deleted:false,status:{$in:#}}", companyId, shopId, statusList).sort(sortOptions).skip(start).limit(limit).as(clazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false,status:{$in:#}}", companyId, shopId, statusList);
        }


        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<InventoryTransferHistoryResult> findItemByModifiedDate(String companyId, String shopId, Long startDate, Long endDate) {
        Iterable<InventoryTransferHistoryResult> items = coll.find("{companyId:#,shopId:#,deleted:false,modified:{$lt:#,$gt:#}}", companyId, shopId, endDate, startDate).sort("{modified:-1}").as(InventoryTransferHistoryResult.class);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$lt:#,$gt:#}}", companyId, shopId, endDate, startDate);

        DateSearchResult<InventoryTransferHistoryResult> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setAfterDate(startDate);
        results.setBeforeDate(endDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends InventoryTransferHistory> E getTransferHistoryByNo(String companyId, String shopId, String transferNo, Class<E> clazz) {
        return coll.findOne("{companyId:#,shopId:#,transferNo:#}", companyId, shopId, transferNo).as(clazz);
    }

    @Override
    public <E extends InventoryTransferHistory> SearchResult<E> findItemByInventoryIdandStatus(String companyId, String shopId, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, String inventoryId, String term, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            items = coll.find("{companyId:#,shopId:#,deleted:false,status:{$in:#}, toInventoryId: #, transferNo:#}", companyId, shopId, statusList, inventoryId, pattern).sort(sortOptions).skip(start).limit(limit).as(clazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false,status:{$in:#}, toInventoryId: #, transferNo:#}", companyId, shopId, statusList, inventoryId, pattern);
        } else {
            items = coll.find("{companyId:#,shopId:#,deleted:false,status:{$in:#}, toInventoryId: #}", companyId, shopId, statusList, inventoryId).sort(sortOptions).skip(start).limit(limit).as(clazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false,status:{$in:#}, toInventoryId: #}", companyId, shopId, statusList, inventoryId);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends InventoryTransferHistory> SearchResult<E> findEmployeeItemByModifiedDate(String companyId, String shopId, Long startDate, Long endDate, String sortOptions, int start, int limit, List<InventoryTransferHistory.TransferStatus> statusList, String invenotryId, String term, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            items = coll.find("{companyId:#,shopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#},  toInventoryId: #, transferNo:#}", companyId, shopId, startDate, endDate, statusList, invenotryId, pattern).sort(sortOptions).skip(start).limit(limit).as(clazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#},  toInventoryId: #, transferNo:#}", companyId, shopId, startDate, endDate, statusList, invenotryId, pattern);
        } else {
            items = coll.find("{companyId:#,shopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#},  toInventoryId: #}", companyId, shopId, startDate, endDate, statusList, invenotryId).sort(sortOptions).skip(start).limit(limit).as(clazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false,modified: {$gt:#, $lt:#},status:{$in:#},  toInventoryId: #}", companyId, shopId, startDate, endDate, statusList, invenotryId);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
