package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.InventoryTransferKit;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryTransferKitRepository;

import javax.inject.Inject;

public class InventoryTransferKitRepositoryImpl extends ShopBaseRepositoryImpl<InventoryTransferKit> implements InventoryTransferKitRepository {

    @Inject
    public InventoryTransferKitRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(InventoryTransferKit.class, mongoManager);
    }

    @Override
    public InventoryTransferKit getInventoryKitByName(String companyId, String shopId, String kitName) {
        return coll.findOne("{companyId:#, shopId:#, deleted:false, kitName:#}", companyId, shopId, kitName).as(entityClazz);
    }
}
