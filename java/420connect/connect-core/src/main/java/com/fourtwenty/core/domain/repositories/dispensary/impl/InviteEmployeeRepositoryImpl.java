package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.InviteEmployee;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.InviteEmployeeRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

/**
 * Created by decipher on 22/11/17 11:59 AM
 * Raja (Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class InviteEmployeeRepositoryImpl extends CompanyBaseRepositoryImpl<InviteEmployee> implements InviteEmployeeRepository {

    @Inject
    public InviteEmployeeRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(InviteEmployee.class, mongoManager);
    }

    @Override
    public InviteEmployee getEmployeeByEmail(String email) {
        return coll.findOne("{email:#}", email).as(entityClazz);
    }

    @Override
    public SearchResult<InviteEmployee> getInviteEmployeeList(String companyId, int skip, int limit, String sortOptions) {
        SearchResult<InviteEmployee> searchResult = new SearchResult<>();
        Iterable<InviteEmployee> inviteEmployees = coll.find("{companyId:#,deleted:false}", companyId).skip(skip).limit(limit).sort(sortOptions).as(InviteEmployee.class);
        long count = coll.count("{deleted:false}");

        searchResult.setValues(Lists.newArrayList(inviteEmployees));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }
}
