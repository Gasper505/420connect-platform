package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MarketingJobLogRepository;
import com.google.inject.Inject;

public class MarketingJobLogRepositoryImpl extends ShopBaseRepositoryImpl<MarketingJobLog> implements MarketingJobLogRepository {

    @Inject
    public MarketingJobLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MarketingJobLog.class, mongoManager);
    }

    /*

    private String fromNumber;
    private String toNumber;
     */
    @Override
    public MarketingJobLog getLog(String companyId, String fromNumber, String toNumber) {
        return coll.findOne("{companyId:#,fromNumber:#,toNumber:#}", companyId, fromNumber, toNumber).orderBy("{modified:-1}").as(entityClazz);
    }
}
