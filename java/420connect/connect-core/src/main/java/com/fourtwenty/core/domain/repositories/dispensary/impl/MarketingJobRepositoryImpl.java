package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MarketingJobRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import java.util.List;

/**
 * Created by mdo on 7/6/17.
 */
public class MarketingJobRepositoryImpl extends ShopBaseRepositoryImpl<MarketingJob> implements MarketingJobRepository {

    @Inject
    public MarketingJobRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MarketingJob.class, mongoManager);
    }

    @Override
    public <T extends MarketingJob> SearchResult<T> findMarketingJobsByStatus(String companyId, String shopId,
                                                                              List<MarketingJob.MarketingJobStatus> statuses, int start, int limit,
                                                                              Class<T> clazz) {
        Iterable<T> items = coll.find("{companyId:#,shopId:#,deleted:false,status:{$in:#}}", companyId, shopId, statuses).sort("{modified:-1}").skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,status:{$in:#}}", companyId, shopId, statuses);

        SearchResult<T> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<MarketingJob> listAllPendingJobsByStatus(String companyId, String shopId, MarketingJob.MarketingJobStatus status) {
        return coll.find("{companyId:#,shopId:#,deleted:false,status:#}", companyId, shopId, status).as(entityClazz);
    }

    @Override
    public Iterable<MarketingJob> listMarketingJobsPending(long currentDate, MarketingJob.MarketingType marketingType) {
        Iterable<MarketingJob> items = coll.find("{active:true,status:#,reqSendDate:{$lte:#},type:#}",
                MarketingJob.MarketingJobStatus.Pending, currentDate, marketingType)
                .as(entityClazz);

        return items;
    }

    @Override
    public Iterable<MarketingJob> getMarketingJobsByType(MarketingJob.MarketingType marketingType) {
        Iterable<MarketingJob> items = coll.find("{active:true,status:#,type:#}",
                MarketingJob.MarketingJobStatus.Pending, marketingType)
                .as(entityClazz);
        return items;
    }

    @Override
    public List<MarketingJob> getMarketingJobsWithoutType() {
        Iterable<MarketingJob> items = coll.find("{type:{ $exists: false }}").as(entityClazz);
        return Lists.newArrayList(items);
    }

    @Override
    public Iterable<MarketingJob> findMarketingJobsByStatus(MarketingJob.MarketingJobType jobType, MarketingJob.MarketingJobStatus status) {
        return coll.find("{deleted:false,status:#,jobType:#}", status, jobType).as(entityClazz);
    }
}
