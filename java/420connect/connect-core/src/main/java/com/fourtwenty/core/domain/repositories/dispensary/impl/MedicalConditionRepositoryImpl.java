package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MedicalConditionRepository;

import javax.inject.Inject;

/**
 * Created by mdo on 11/9/15.
 */
public class MedicalConditionRepositoryImpl extends MongoBaseRepositoryImpl<MedicalCondition> implements MedicalConditionRepository {
    @Inject
    public MedicalConditionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MedicalCondition.class, mongoManager);
    }
}
