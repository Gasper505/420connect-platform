package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.customer.MemberActivity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MemberActivityRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class MemberActivityRepositoryImpl extends ShopBaseRepositoryImpl<MemberActivity> implements MemberActivityRepository {

    @Inject
    public MemberActivityRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MemberActivity.class, mongoManager);
    }

    @Override
    public SearchResult<MemberActivity> getMemberActivities(String companyId, String shopId, String targetId, int start, int limit) {
        Iterable<MemberActivity> items = coll.find("{ companyId:#,shopId:#,targetId:#}", companyId, shopId, targetId).sort("{ created : -1}").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{ companyId:#,shopId:#,targetId:#}", companyId, shopId, targetId);

        SearchResult<MemberActivity> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setLimit(limit);
        result.setSkip(start);

        return result;
    }
}
