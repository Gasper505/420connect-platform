package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.MemberGroupPrices;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupPricesRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 5/23/17.
 */
public class MemberGroupPricesRepositoryImpl extends ShopBaseRepositoryImpl<MemberGroupPrices> implements MemberGroupPricesRepository {

    @Inject
    public MemberGroupPricesRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MemberGroupPrices.class, mongoManager);
    }


    @Override
    public <E extends MemberGroupPrices> E getPricesForProductGroup(String companyId, String shopId, String productId, String memberGroupId, Class<E> clazz) {
        Iterable<E> iterable = coll.find("{companyId:#,shopId:#,productId:#,memberGroupId:#,deleted:false}", companyId, shopId,
                productId,
                memberGroupId).as(clazz);

        // return the first one (there should only be one)
        for (E item : iterable) {
            return item;
        }
        return null;
    }

    @Override
    public <E extends MemberGroupPrices> Iterable<E> getPricesForProduct(String companyId,
                                                                         String shopId,
                                                                         String productId,
                                                                         Class<E> clazz) {
        return coll.find("{companyId:#,shopId:#,productId:#,deleted:false}", companyId, shopId,
                productId).as(clazz);
    }

    @Override
    public HashMap<String, MemberGroupPrices> getPricesMapForProductGroup(String companyId, String shopId, List<String> productIds, String memberGroupId) {
        Iterable<MemberGroupPrices> iterable = coll.find("{companyId:#,shopId:#,productId:{$in:#},memberGroupId:#,deleted:false}", companyId, shopId,
                productIds,
                memberGroupId).as(entityClazz);
        HashMap<String, MemberGroupPrices> pricesHashMap = new HashMap<>();
        for (MemberGroupPrices prices : iterable) {
            pricesHashMap.put(prices.getProductId(), prices);
        }
        return pricesHashMap;
    }

    @Override
    public Iterable<MemberGroupPrices> getMemberGroupPricesByTemplate(String companyId, String shopId, String priceTemplateId) {
        return coll.find("{ companyId:#,shopId:#,deleted:false,pricingTemplateId:#}", companyId, shopId, priceTemplateId).as(entityClazz);
    }

    @Override
    public void updatePricesForTemplate(String companyId, String shopId, List<ObjectId> memberGroupPriceId, List<ProductPriceBreak> productPriceBreaks, List<ProductPriceRange> productPriceRanges) {
        coll.update("{ companyId:#,shopId:#,_id : {$in:#}}", companyId, shopId, memberGroupPriceId).multi().with("{ $set:{priceRanges:#,priceBreaks:#,modified:#}}", productPriceRanges, productPriceBreaks, DateTime.now().getMillis());
    }

    @Override
    public void updatePricingTemplate(String companyId, String shopId, List<ObjectId> groupPriceId, String pricingTemplateId) {
        coll.update("{ companyId:#, shopId:#, _id : {$in:#}}", companyId, shopId, groupPriceId).multi().with("{ $set:{pricingTemplateId:#,modified:#}}", pricingTemplateId, DateTime.now().getMillis());
    }

    @Override
    public Iterable<MemberGroupPrices> getPricesForProducts(String companyId, String shopId, List<String> productIds) {
        return coll.find("{ companyId:#,shopId:#,deleted:false,productId:{$in:#}}", companyId, shopId, productIds).as(entityClazz);
    }
}
