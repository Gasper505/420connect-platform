package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * Created by mdo on 10/25/15.
 */
public class MemberGroupRepositoryImpl extends ShopBaseRepositoryImpl<MemberGroup> implements MemberGroupRepository {
    @Inject
    public MemberGroupRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MemberGroup.class, mongoManager);
    }

    @Override
    public MemberGroup getMemberGroup(String companyId, String name) {
        return coll.findOne("{companyId:#,name:#}", companyId, name).as(entityClazz);
    }

    @Override
    public MemberGroup getMemberGroup(String companyId, String shopId, String name) {
        return coll.findOne("{companyId:#,shopId:#,name:#}", companyId, shopId, name).as(entityClazz);
    }

    @Override
    public MemberGroup getDefaultMemberGroup(String companyId) {
        return coll.findOne("{companyId:#,defaultGroup:#}", companyId, true).as(entityClazz);
    }


    @Override
    public <E extends MemberGroup> SearchResult<E> getActiveMemberGroups(String companyId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, active:true}", companyId).as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(0);
        results.setLimit(100);
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public <E extends MemberGroup> SearchResult<E> getActiveMemberGroupsShop(String companyId, String shopId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, active:true}", companyId, shopId).as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(0);
        results.setLimit(100);
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public void setMemberGroupFalse(String companyId) {
        coll.update("{companyId:#}", companyId).multi().with("{$set: {defaultGroup:#, modified:#}}", false, DateTime.now().getMillis());
    }

    @Override
    public void setMemberGroupFalse(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#}", companyId, shopId).multi().with("{$set: {defaultGroup:#, modified:#}}", false, DateTime.now().getMillis());
    }

    @Override
    public void removeById(String companyId, String entityId, String name) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#,name:#}}", DateTime.now().getMillis(), name + "-del-" + DateTime.now().getMillis());
    }
}
