package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByField;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByFieldExtended;
import com.fourtwenty.core.reporting.model.reportmodels.MemberPerformance;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberTagsByCompany;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by mdo on 9/27/15.
 */
public class MemberRepositoryImpl extends ShopBaseRepositoryImpl<Member> implements MemberRepository {

    @Inject
    public MemberRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Member.class, mongoManager);
    }


    @Override
    public Member getMemberWithConsumerId(String companyId, String consumerUserId) {
        return coll.findOne("{companyId:#,consumerUserId:#}", companyId, consumerUserId).as(entityClazz);
    }

    @Override
    public Member getMemberWithDriversLicense(String companyId, String driversLicense) {
        Pattern pattern = TextUtil.createPattern(driversLicense);
        Iterable<Member> members = coll.find("{companyId:#,identifications.licenseNumber:#,deleted:#,status:#}",
                companyId, pattern, false, Member.MembershipStatus.Active).as(entityClazz);
        for (Member member : members) {
            return member;
        }
        return null;
    }

    @Override
    public Iterable<Member> listWithOptions(List<String> companyIds, Member.MembershipStatus memberStatus, String projections) {
        return coll.find("{companyId:{$in:#},deleted:false,status:#,primaryPhone:{$ne:''}}", companyIds, memberStatus).projection(projections).as(entityClazz);

    }

    @Override
    public Iterable<Member> listWithOptionsTextOptIn(String companyId, Member.MembershipStatus memberStatus, String projections) {
        return coll.find("{companyId:#,deleted:false,textOptIn:true,status:#,primaryPhone:{$ne:''}}", companyId, memberStatus).projection(projections).as(entityClazz);
    }

    @Override
    public long countWithOptionsTextOptIn(String companyId, Member.MembershipStatus memberStatus, String projections) {
        return coll.count("{companyId:#,deleted:false,textOptIn:true,status:#,primaryPhone:{$ne:''}}", companyId, memberStatus);
    }

    @Override
    public long countWithOptionsTextOptIn(String companyId, String shopId, Member.MembershipStatus memberStatus, String projections) {
        return coll.count("{companyId:#,shopId:#,deleted:false,textOptIn:true,status:#,primaryPhone:{$ne:''}}", companyId, shopId, memberStatus);
    }

    @Override
    public Iterable<Member> listWithOptionsTextOptIn(String companyId, String shopId, Member.MembershipStatus memberStatus, String projections) {
        return coll.find("{companyId:#,shopId:#,deleted:false,textOptIn:true,status:#,primaryPhone:{$ne:''}}", companyId, shopId, memberStatus).projection(projections).as(entityClazz);
    }

    @Override
    public Iterable<Member> getMemberByEmail(String companyId, String memberEmail) {
        return coll.find("{companyId:#,email:#}", companyId, memberEmail).sort("{modified:-1}").as(entityClazz);
    }

    @Override
    public Iterable<Member> getMemberByMemberGroup(String companyId, String memberGroupId) {
        return coll.find("{companyId:#,memberGroupId:#}", companyId, memberGroupId).as(entityClazz);
    }

    @Override
    public Iterable<Member> getMemberByMemberGroupsTextOptIn(String companyId, List<String> memberGroupIds) {
        return coll.find("{companyId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},memberGroupId:{$in:#}}", companyId, Member.MembershipStatus.Active, memberGroupIds).as(entityClazz);
    }

    @Override
    public long getMemberByMemberGroupsCountTextOptIn(String companyId, List<String> memberGroupIds) {
        return coll.count("{companyId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},memberGroupId:{$in:#}}", companyId, Member.MembershipStatus.Active, memberGroupIds);
    }


    @Override
    public Iterable<Member> getMembersOlderThan(long dob) {
        return coll.find("{dob: {$gte: #}}", dob).as(entityClazz);
    }

    @Override
    public long countMemberByEmail(String companyId, String memberEmail) {
        return coll.count("{companyId:#,deleted:false,email:#}", companyId, memberEmail);
    }

    @Override
    public long countMemberByEmail(String companyId, String shopId, String memberEmail) {
        return coll.count("{companyId:#,shopId:#,deleted:false,email:#}", companyId, shopId, memberEmail);
    }

    @Override
    public Long getActiveMemberCount(String companyId, String shopId) {
        return coll.count("{companyId:#, shopId:#, status:#}", companyId, shopId, Member.MembershipStatus.Active);
    }

    @Override
    public SearchResult<Member> searchMemberships(String companyId, String term, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Member> items = coll.find("{companyId:#,searchText:#}", companyId, pattern)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,searchText:#}", companyId, pattern);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> searchMemberships(String companyId, String term, String projections, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Member> items = coll.find("{companyId:#,searchText:#}", companyId, pattern)
                .projection(projections)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,searchText:#}", companyId, pattern);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> searchMembershipsWithShop(String companyId, String shopId, String term, String projections, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Member> items = coll.find("{companyId:#,shopId:#,searchText:#}", companyId, shopId, pattern)
                .projection(projections)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,searchText:#}", companyId, shopId, pattern);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> searchMembershipsWithShop(String companyId, String shopId, String term, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Member> items = coll.find("{companyId:#,shopId:#,searchText:#}", companyId, shopId, pattern)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,searchText:#}", companyId, shopId, pattern);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> searchMemberships(String companyId, String term, String projections, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items = coll.find("{companyId:#,searchText:#}", companyId, pattern)
                .projection(projections)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(clazz);

        long count = coll.count("{companyId:#,searchText:#}", companyId, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> getMembersWithDoctorId(String companyId, String doctorId, int start, int limit) {
        Iterable<Member> items = coll.find("{companyId:#,recommendations.doctorId:#}", companyId, doctorId)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,recommendations.doctorId:#}", companyId, doctorId);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> getMembersWithDoctorId(String companyId, String doctorId, String term, int start, int limit) {

        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Member> items = coll.find("{companyId:#,recommendations.doctorId:#,searchText:#}", companyId, doctorId, pattern)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,recommendations.doctorId:#,searchText:#}", companyId, doctorId, pattern);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> getMembersWithDoctorIdWithShop(String companyId, String shopId, String doctorId, int start, int limit) {
        Iterable<Member> items = coll.find("{companyId:#,shopId:#,recommendations.doctorId:#}", companyId, shopId, doctorId)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,recommendations.doctorId:#}", companyId, shopId, doctorId);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Member> getMembersWithDoctorIdWithShop(String companyId, String shopId, String doctorId, String term, int start, int limit) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Member> items = coll.find("{companyId:#,shopId:#,recommendations.doctorId:#,searchText:#}", companyId, shopId, doctorId, pattern)
                .skip(start)
                .limit(limit)
                .sort("{firstName:1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,recommendations.doctorId:#,searchText:#}", companyId, shopId, doctorId, pattern);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<Member> getMembers(String companyId, List<ObjectId> memberIds) {
        return coll.find("{companyId:#,_id:{$in: #}}", companyId, memberIds).as(entityClazz);
    }

    @Override
    public Long getActiveMemberCount(String companyId) {
        return coll.count("{companyId:#,  status:#}", companyId, Member.MembershipStatus.Active);
    }


    @Override
    public void updateMemberGroup(String companyId, String memberGroupId, MemberGroup memberGroup) {
        coll.update("{companyId:#,memberGroupId:#}", companyId, memberGroupId).multi().with("{$set: {memberGroup:#}}", memberGroup);
    }

    @Override
    public void removeByIdSetState(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,status:#,modified:#}}", Member.MembershipStatus.Inactive,
                DateTime.now().getMillis());
    }

    @Override
    public Iterable<MemberByField> getMembershipsByGroup(String companyId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match: {companyId:#,deleted:false, status:#}}", companyId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return result;
    }

    @Override
    public Iterable<MemberByFieldExtended> getMembersByGroup(String companyId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<MemberByFieldExtended> result = coll.aggregate("{$match: {companyId:#,deleted:false, status:#}}", companyId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}, members:{ $push:{id:'$_id',created:'$created',consumerType:'$consumerType'}}}}")
                .and("{$project: {_id:1, count:1, members:1}}")
                .options(aggregationOptions)
                .as(MemberByFieldExtended.class);

        return result;
    }

    @Override
    public Iterable<MemberByField> getMembershipsByGroupEmailOptIn(String companyId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match: {companyId:#,deleted:false, emailOptIn:true,status:#}}", companyId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return result;
    }

    @Override
    public Iterable<MemberByField> getMembershipsByGroupTextOptIn(String companyId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();
        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match: {companyId:#,deleted:false, textOptIn:true,primaryPhone:{$ne:''},status:#}}", companyId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return result;
    }

    // count by with shopId


    @Override
    public Iterable<MemberByField> getMembershipsByGroup(String companyId, String shopId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();
        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match: {companyId:#,shopId:#,deleted:false, status:#}}", companyId, shopId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return result;
    }

    @Override
    public Iterable<MemberByField> getMembershipsByGroupTextOptIn(String companyId, String shopId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();
        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match: {companyId:#,shopId:#,deleted:false, textOptIn:true,primaryPhone:{$ne:''},status:#}}", companyId, shopId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return result;
    }

    @Override
    public Iterable<MemberByField> getMembershipsByGroupEmailOptIn(String companyId, String shopId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();
        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match: {companyId:#,shopId:#,deleted:false, emailOptIn:true,status:#}}", companyId, shopId, Member.MembershipStatus.Active)
                .and("{$group: {_id: '$memberGroupId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return result;
    }

    @Override
    public ArrayList<MemberByField> getMemberCountByDoctor(String companyId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match:{companyId:#,deleted:false}}", companyId)
                .and("{$unwind: '$recommendations'}")
                .and("{$group: {_id: '$recommendations.doctorId', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return Lists.newArrayList(result.iterator());
    }

    @Override
    public ArrayList<MemberByField> getMembershipsByCity(String companyId) {

        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<MemberByField> result = coll.aggregate("{$match:{companyId:#}}", companyId)
                .and("{$group: {_id: '$uppercaseCity', count: {$sum: 1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(MemberByField.class);

        return Lists.newArrayList(result.iterator());
    }

    @Override
    public Iterable<Member> getMembershipsExpiringSoon(String companyId, Long currentDate) {
        Long thirtyDays = (long) 60 * 60 * 24 * 30 * 1000; //60s * 60min * 24hrs * 30days * 1000ms
        Long targetDate = currentDate + thirtyDays;
        //Find by member expiration date
        Iterable<Member> result = coll.find("{companyId:#,  status:#, $or: [" +
                        " {expirationDate: {$lte: #}}," +
                        " {recommendations.expirationDate: {$lte: #}}," +
                        " {identifications.expirationDate: {$lte: #}}" +
                        "]}"
                , companyId, Member.MembershipStatus.Active, targetDate, targetDate, targetDate)
                .as(Member.class);
        return result;
    }

    @Override
    public Iterable<Member> getMembershipsExpiringSoon(String companyId, String shopId, Long currentDate) {
        Long thirtyDays = (long) 60 * 60 * 24 * 30 * 1000; //60s * 60min * 24hrs * 30days * 1000ms
        Long targetDate = currentDate + thirtyDays;
        //Find by member expiration date
        Iterable<Member> result = coll.find("{companyId:#,  shopId:#,status:#, $or: [" +
                        " {expirationDate: {$lte: #}}," +
                        " {recommendations.expirationDate: {$lte: #}}," +
                        " {identifications.expirationDate: {$lte: #}}" +
                        "]}"
                , companyId, shopId, Member.MembershipStatus.Active, targetDate, targetDate, targetDate)
                .as(Member.class);
        return result;
    }

    @Override
    public List<Member> getInactiveMembers(String companyId, Long currentDate) {
        Long thirtyDays = (long) 60 * 60 * 24 * 30 * 1000; //60s * 60min * 24hrs * 30days * 1000ms
        Long targetDate = currentDate - thirtyDays;
        //Find by member expiration date
        Iterable<Member> result = coll.find("{companyId:#, status:#, lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, targetDate)
                .as(Member.class);
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public Iterable<Member> getInactiveMembersWithDays(String companyId, long olderThanDate) {
        Iterable<Member> result = coll.find("{companyId:#, status:#, lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, olderThanDate)
                .as(Member.class);
        return result;
    }

    @Override
    public Iterable<Member> getInactiveMembersWithDaysTextOptIn(String companyId, long olderThanDate) {
        Iterable<Member> result = coll.find("{companyId:#, status:#,textOptIn:true,primaryPhone:{$ne:''}, lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, olderThanDate)
                .as(Member.class);
        return result;
    }

    @Override
    public long countInactiveMembersWithDaysTextOptIn(String companyId, long olderThanDate) {
        return coll.count("{companyId:#, status:#,textOptIn:true,primaryPhone:{$ne:''}, lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, olderThanDate);
    }

    @Override
    public long getInactiveMembersWithDaysCountTextOptIn(String companyId, long olderThanDate) {
        return coll.count("{companyId:#, status:#, textOptIn:true,primaryPhone:{$ne:''},lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, olderThanDate);
    }

    @Override
    public long getInactiveMembersWithDaysCount(String companyId, long olderThanDate) {
        return coll.count("{companyId:#, status:#, lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, olderThanDate);
    }

    @Override
    public void updateRecentBought(String companyId, String memberId, List<String> products) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(memberId))
                .with("{$set: {recentProducts:#,modified:#,lastVisitDate:#}}", products, DateTime.now().getMillis(), DateTime.now().getMillis());
    }

    @Override
    public void addLoyaltyPoints(String companyId, String memberId, double pointsEarned) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(memberId)).with("{$set: {modified:#},$inc:{loyaltyPoints:#}}", DateTime.now().getMillis(), pointsEarned);
    }

    @Override
    public void addLifetimePoints(String companyId, String memberId, double pointsEarned) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(memberId)).with("{$set: {modified:#},$inc:{lifetimePoints:#}}", DateTime.now().getMillis(), pointsEarned);
    }

    public Iterable<Member> getMembersWithoutStartDate() {
        return coll.find("{startDate:{$exists: false}}").as(entityClazz);
    }

    @Override
    public void setMemberGroupIfEmpty(String companyId, MemberGroup memberGroup) {
        coll.update("{companyId:#, memberGroupId: {$exists: false}}", companyId).multi().with("{$set: {memberGroupId:#, memberGroup:#}}", memberGroup.getId(), memberGroup);
    }

    @Override
    public long getNewMembersFromToday(String companyId, Long start, Long end) {
        return coll.count("{companyId:#,  created: {$gt: #, $lt:#}}", companyId, start, end);
    }

    @Override
    public Iterable<Member> getMembersWithStartDate(String companyId, long startDate, long endDate) {
        return coll.find("{companyId:#,  startDate:{$gt:#, $lt:#}}", companyId, startDate, endDate).as(entityClazz);
    }

    @Override
    public Iterable<Member> getMembersWithStartDate(String companyId, String shopId, long startDate, long endDate) {
        return coll.find("{companyId:#,shopId:#, startDate:{$gt:#, $lt:#}}", companyId, shopId, startDate, endDate).as(entityClazz);
    }

    @Override
    public List<Member> getMembers(String companyId) {
        Iterable<Member> result = coll.find("{companyId:#,deleted:false}}", companyId).as(entityClazz);
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public List<Member> getInactiveMembersSpecificDays(String companyId, Long noOfDays, Long currentDate) {
        Long lastVisit = (long) 60 * 60 * 24 * noOfDays * 1000; //60s * 60min * 24hrs * 30days * 1000ms
        Long targetDate = currentDate - lastVisit;
        //Find by member expiration date
        Iterable<Member> result = coll.find("{companyId:#, status:#, lastVisitDate: {$lte: #}}"
                , companyId, Member.MembershipStatus.Active, targetDate)
                .as(Member.class);
        return Lists.newArrayList(result.iterator());
    }


    @Override
    public long countActiveTextOptIn(String companyId, Member.MembershipStatus memberStatus) {
        return coll.count("{companyId:#,deleted:false,textOptIn:true,primaryPhone:{$ne:''},status:#}", companyId, memberStatus);
    }

    @Override
    public Member getMemberWithPhoneNumber(final String companyId, final String phoneNUmber) {
        return coll.findOne("{companyId:#,primaryPhone:#}", companyId, phoneNUmber).as(entityClazz);
    }

    @Override
    public Iterable<Member> getActiveAndPendingMembers(String companyId, Long start, Long end) {
        List<Member.MembershipStatus> membershipStatusList = new ArrayList<>();
        membershipStatusList.add(Member.MembershipStatus.Active);
        membershipStatusList.add(Member.MembershipStatus.Pending);

        return coll.find("{companyId:#,  status: {$in:#}, created: {$gt: #, $lt:#}}", companyId, membershipStatusList, start, end).as(Member.class);
    }

    @Override
    public void bulkUpdateTextOptIn(String companyId, List<ObjectId> memberIds, Boolean textOptInValue) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, memberIds).multi().with("{$set: {textOptIn: #, modified:#}}", textOptInValue, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateEmailOptIn(String companyId, List<ObjectId> memberIds, Boolean emailOptInValue) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, memberIds).multi().with("{$set: {emailOptIn: #, modified:#}}", emailOptInValue, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateMemberStatus(String companyId, List<ObjectId> memberIds, Member.MembershipStatus membershipStatus) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, memberIds).multi().with("{$set: {status: #, modified:#}}", membershipStatus, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateMemberType(String companyId, List<ObjectId> memberIds, Boolean medical, ConsumerType consumerType) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, memberIds).multi().with("{$set: {medical: #, consumerType: #, modified:#}}", medical, consumerType, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateMemberMarketingSource(String companyId, List<ObjectId> memberIds, String marketingSource) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, memberIds).multi().with("{$set: {marketingSource: #, modified:#}}", marketingSource, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateMemberGroup(String companyId, List<ObjectId> memberIds, MemberGroup memberGroup) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, memberIds).multi().with("{$set: {memberGroup: #, memberGroupId: #, modified:#}}", memberGroup, memberGroup.getId(), DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateMemberVerified(String companyId, List<ObjectId> memberIds, Boolean verified) {
        coll.update("{companyId:#, _id: {$in:#}, recommendations.verified:{$exists : #}}", companyId, memberIds, true).multi().with("{$set: {recommendations.$.verified: #, modified:#}}", verified, DateTime.now().getMillis());
    }

    @Override
    public void bulkAddNoteToMembers(String companyId, List<ObjectId> memberIds, Note note) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, memberIds).multi().with("{$push: {notes:#},$set:{modified:#}}", note, DateTime.now().getMillis());
    }

    @Override
    public Iterable<Member> getMembersWithNegativeLongForIdentification() {
        return coll.find("{identifications.expirationDate: {$lt: #}}", 0L).as(entityClazz);
    }

    @Override
    public void bulkDeleteMembers(String companyId, List<ObjectId> memberIds) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, memberIds).multi().with("{$set: {deleted:true,modified:#}}", DateTime.now().getMillis());

    }

    @Override
    public List<Member> getMembersByCareGiver(String companyId, String careGiverId) {
        Iterable<Member> result = coll.find("{companyId:#,careGivers:#}", companyId, careGiverId).as(Member.class);
        return Lists.newArrayList(result);
    }

    @Override
    public void bulkUpdateMemberLoyalty(String companyId, List<ObjectId> membersList, Boolean enableLoyalty) {
        coll.update("{companyId:#,_id: {$in:#}}", companyId, membersList).multi().with("{$set: {enableLoyalty: #, modified:#}}", enableLoyalty, DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, Member> getActiveMembers(String companyId, Member.MembershipStatus status) {
        Iterable<Member> members = coll.find("{companyId:#,deleted:false,textOptIn:true,primaryPhone:{$ne:''},status:#}", companyId, status).as(entityClazz);
        return asMap(members);
    }

    @Override
    public Iterable<Member> listByTextOptIn(String companyId, List<ObjectId> memberIds) {
        return coll.find("{companyId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},_id:{$in:#}}", companyId, Member.MembershipStatus.Active, memberIds).as(entityClazz);
    }

    @Override
    public long countByTextOptIn(String companyId, List<ObjectId> memberIds) {
        return coll.count("{companyId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},_id:{$in:#}}", companyId, Member.MembershipStatus.Active, memberIds);
    }

    @Override
    public long countByTextOptIn(String companyId, String shopId, List<ObjectId> memberIds) {
        return coll.count("{companyId:#,shopId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},_id:{$in:#}}", companyId, shopId, Member.MembershipStatus.Active, memberIds);
    }

    @Override
    public Iterable<Member> listByTextOptIn(String companyId, String shopId, List<ObjectId> memberIds) {
        return coll.find("{companyId:#,shopId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},_id:{$in:#}}", companyId, shopId, Member.MembershipStatus.Active, memberIds).as(entityClazz);
    }

    @Override
    public WriteResult updateCustomerRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("members").update(query, field);
    }

    @Override
    public List<Member> getMembersListWithoutQbRef(String companyId, long afterDate, long beforeDate) {
        Iterable<Member> result = coll.find("{companyId:#,modified:{$lt:#, $gt:#}, active:true,qbCustomerRef: {$exists: false} }"
                , companyId, beforeDate, afterDate).as(Member.class);
        return Lists.newArrayList(result);
    }

    @Override
    public List<String> getAllMemberIds(String companyId) {
        return Lists.newArrayList(listAsMap(companyId).keySet());
    }

    @Override
    public Iterable<Member> listActiveAndPendingMembers(String companyId) {
        List<Member.MembershipStatus> membershipStatusList = new ArrayList<>();
        membershipStatusList.add(Member.MembershipStatus.Active);
        membershipStatusList.add(Member.MembershipStatus.Pending);

        return coll.find("{companyId:#,  status: {$in:#}}", companyId, membershipStatusList).as(Member.class);
    }

    @Override
    public void updateMemberRef(String companyId, String id, String qbCustomerRef, String editSequence, String qbListId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(id)).with("{$set: {qbDesktopCustomerRef:#, editSequence:#, qbListId:#}}", qbCustomerRef, editSequence, qbListId);
    }

    @Override
    public Iterable<Member> listAllTextOptIn(List<String> companyId) {
        Iterable<Member> items = coll.find("{companyId:{$in:#},deleted:false,textOptIn:true,status:#,primaryPhone:{$ne:''}}", companyId, Member.MembershipStatus.Active).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<Member> listInActiveTextOptInAsMap(long lastVisitDate) {
        Iterable<Member> items = coll.find("{status:#,textOptIn:true,primaryPhone:{$ne:''}, lastVisitDate: {$lte: #}}"
                , Member.MembershipStatus.Active, lastVisitDate)
                .as(entityClazz);
        return (items);
    }

    @Override
    public Iterable<Member> listByTextOptIn(List<ObjectId> ids) {
        Iterable<Member> items = coll.find("{deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},_id:{$in:#}}", Member.MembershipStatus.Active, ids).as(entityClazz);
        return items;
    }

    @Override
    public Iterable<Member> getAllMemberByMemberGroupsTextOptIn(List<String> memberGroupIds) {
        Iterable<Member> items = coll.find("{deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},memberGroupId:{$in:#}}", Member.MembershipStatus.Active, memberGroupIds).as(entityClazz);
        return (items);
    }

    @Override
    public Iterable<MemberPerformance> getMembersForBestPerformance(String companyId) {

        Iterable<MemberPerformance> items = coll.find("{companyId:#}", companyId)
                .projection("{_id:1,firstName:1,lastName:1, primaryPhone:1,dob:1,sex:1,marketingSource:1, memberGroupId:1, created:1, consumerType:1, loyaltyPoints:1, address:1, lastVisitDate:1}")
                .as(MemberPerformance.class);

        return items;
    }

    @Override
    public HashMap<String, Member> findMembers(String companyId, String projection, ArrayList<ObjectId> objectIds) {
        Iterable<Member> items = coll.find("{companyId:#,_id:{$in:#}}", companyId, objectIds).projection(projection).as(entityClazz);
        return asMap(items);
    }

    @Override
    public Iterable<Member> getMembersByEmails(String companyId, Set<String> emailIds) {
        return coll.find("{companyId:#,deleted:false,email : {$in: #}}", companyId, emailIds).projection("{email:1}").as(entityClazz);
    }

    @Override
    public List<String> getAllMemberTags(String companyId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<MemberTagsByCompany> result = coll.aggregate("{$match:{companyId:#, deleted:false, tags.0: {$exists:true}}}", companyId)
                .and("{$unwind: '$tags'}")
                .and("{$group: {_id:'$companyId', tags:{$push:'$tags'}}}")
                .options(aggregationOptions)
                .as(MemberTagsByCompany.class);

        List<String> tags = new ArrayList<>();
        for (MemberTagsByCompany tagsByCompany : result) {
            tags = Lists.newArrayList(tagsByCompany.getTags() != null ? tagsByCompany.getTags() : new ArrayList<>());
            break;
        }
        return tags;
    }

    @Override
    public long countMembersTextOptInByTags(String companyId, List<String> memberTags) {
        return coll.count("{companyId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},tags:{$in:#}}", companyId, Member.MembershipStatus.Active, memberTags);
    }

    @Override
    public Iterable<Member> getMemberTextOptInByTags(String companyId, List<String> memberTags) {
        return coll.find("{companyId:#,deleted:false,status:#,textOptIn:true,primaryPhone:{$ne:''},tags:{$in:#}}", companyId, Member.MembershipStatus.Active, memberTags).as(entityClazz);
    }

    @Override
    public void updateConsumerUserId(String companyId, String consumerUserId, String memberId) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(memberId)).with("{$set:{consumerUserId:#,modified:#}}", consumerUserId, DateTime.now().getMillis());
    }

    @Override
    public Iterable<Member> listByShopWithOptions(String companyId, String shopId, String projections) {
        return coll.find("{companyId: #,shopId:#,deleted:false}", companyId, shopId).projection(projections).as(entityClazz);
    }

    @Override
    public void bulkRemoveMemberTags(String companyId, String shopId, List<String> memberTagsToRemove) {
        coll.update("{companyId:#,shopId:#, tags: {$in:#}}", companyId, shopId, memberTagsToRemove).multi().with("{$pull: {tags:{$in:#}},$set: {modified:#}}", memberTagsToRemove, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<Member> getMemberList(String companyId, long startDate, long endDate, int skip, int limit, String sortBy) {
        Iterable<Member> members = coll.find("{companyId:#, deleted:false, modified:{$lt:#, $gt:#}}", companyId, endDate, startDate)
                                    .sort(sortBy)
                                    .skip(skip)
                                    .limit(limit)
                                    .as(entityClazz);

        long count = coll.count("{companyId:#, deleted:false, modified:{$lt:#, $gt:#}}", companyId, endDate, startDate);

        SearchResult<Member> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(members));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);

        return results;
    }

    @Override
    public ArrayList<Member> getMembersByLicenceNo(String companyId, String licenceNumber, String sortBy) {

        Iterable<Member> items = coll.find("{companyId:#, deleted:false, identifications.licenseNumber:#}", companyId, licenceNumber).sort(sortBy).as(entityClazz);

        long count = coll.count("{companyId:#, deleted:false, identifications.licenseNumber:#}", companyId, licenceNumber);

        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Member> List<E> getMembersByLimitsWithoutQbDesktopRef(String companyId, int start, int limit, Class<E> clazz) {
        Iterable<E> members = coll.find("{companyId:#, deleted:false, qbMapping:{$exists: false}}", companyId)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public <E extends Member> List<E> getMembersByLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> members = coll.find("{companyId:#, deleted:false, qbMapping:{$exists: true}, qbMapping.shopId : {$ne:#}}", companyId, shopId)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public <E extends Member> List<E> getQBExistingMember(String companyId, String shopId, int start, int limit, long endTime, Class<E> clazz) {
        Iterable<E> members = coll.find("{companyId:#, deleted:false, qbMapping:{$exists:true}, qbMapping.shopId:#, qbMapping.qbDesktopRef:{$exists: true},modified:{$gt:#}, qbMapping.editSequence:{$exists: true}, qbMapping.qbListId:{$exists: true}}", companyId, shopId, endTime)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public void updateEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef) {
        coll.update("{companyId:#, qbMapping:{$exists:true}, qbMapping.shopId:#, qbMapping.qbListId:#}", companyId, shopId, qbListId).with("{$set: {qbMapping.$[].editSequence:#, qbMapping.$[].qbDesktopRef:#}}", editSequence, desktopRef);
    }

    @Override
    public <E extends Member> List<E> getMembersByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        Iterable<E> members =  coll.find("{companyId:#, deleted:false, qbMapping:{$exists:true}, qbMapping.shopId:#, qbMapping.qbDesktopRef:{$exists: false}, qbMapping.errorTime:{$exists:true}, modified:{$gt:#}}", companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public void updateMemberQbMapping(String companyID, String id, List<QBDataMapping> mapping) {
        coll.update("{companyId:#, _id:#}",companyID, new ObjectId(id)).with("{$set: {qbMapping:#}}", mapping);
    }

    @Override
    public void bulkUpdateMemberIdVerified(String companyId, List<ObjectId> ids, boolean verified) {
        coll.update("{companyId:#, _id: {$in:#}, identifications.verified:{$exists : #}}", companyId, ids, true).multi().with("{$set: {identifications.$.verified: #, modified:#}}", verified, DateTime.now().getMillis());
    }

    @Override
    public void updateMemberAddress(String companyId, String memberId, Address address) {
        coll.update("{companyId:#, _id:#, deleted: false}", companyId, new ObjectId(memberId)).multi().with("{$addToSet: {addresses:#}},$set: {modified: #}", address, DateTime.now().getMillis());
    }

    @Override
    public void hardQbMappingRemove(String companyId, String shopId) {
        coll.update("{companyId:#, qbMapping:{$exists:true}, qbMapping.shopId:#}", companyId, shopId).multi().with(" { $pull: { qbMapping: { shopId: # } } }", shopId);
    }

    @Override
    public long countEmptyEmailAndDobMember(String companyId, String shopId) {
        return coll.count("{companyId:#, shopId:#, deleted:false, $or: [{$or: [{email: {$exists: false }},{email: {$in: ['',null]}}]},{$or: [{dob: {$lte: 0 }},{dob: { $exists: false }},{dob: {$in: ['',null]}}]}]}", companyId, shopId);
    }

    @Override
    public <E extends Member> List<E> getEmptyEmailAndDobMember(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> members = coll.find("{companyId:#, shopId:#, deleted:false, $or: [{$or: [{email: {$exists: false }},{email: {$in: ['',null]}}]},{$or: [{dob: {$lte: 0 }},{dob: { $exists: false }},{dob: {$in: ['',null]}}]}]}", companyId, shopId)
                .sort("{created:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public long countMemberByDrivingLicense(String companyId, String licenseNumber) {
        return coll.count("{companyId:#, deleted:false, identifications.licenseNumber:#}", companyId, licenseNumber);
    }

    @Override
    public ArrayList<Member> getMembersByDrivingLicense(String companyId, List<String> licenseNumber) {
        Iterable<Member> result = coll.find("{companyId:#, deleted:false, identifications.licenseNumber:{$in:#}}", companyId, licenseNumber).sort("{created:-1}").as(entityClazz);
        return Lists.newArrayList(result);
    }

    @Override
    public Member getMemberByEmailAddress(String companyId, String memberEmail) {
        return coll.findOne("{companyId:#, email:#, deleted:false}", companyId, memberEmail).as(entityClazz);
    }
}
