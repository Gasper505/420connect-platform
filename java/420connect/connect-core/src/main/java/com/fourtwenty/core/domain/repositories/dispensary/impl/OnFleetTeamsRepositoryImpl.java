package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.OnFleetTeamsRepository;

import javax.inject.Inject;

public class OnFleetTeamsRepositoryImpl extends ShopBaseRepositoryImpl<OnFleetTeams> implements OnFleetTeamsRepository {

    @Inject
    public OnFleetTeamsRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(OnFleetTeams.class, mongoManager);
    }

    @Override
    public OnFleetTeams getByCompanyAndShop(String companyId, String shopId) {
        return coll.findOne("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
    }
}
