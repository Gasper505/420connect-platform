package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Organization;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.OrganizationRepository;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Lists;
import java.util.stream.Collectors;

public class OrganizationRepositoryImpl extends MongoBaseRepositoryImpl<Organization> implements OrganizationRepository {

    @Inject
    public OrganizationRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Organization.class, mongoManager);
    }

    @Override
    public List<Organization> getOrganizations() {
        Iterable<Organization> organizations = coll.find("{deleted:false}").as(entityClazz);
        return Lists.newArrayList(organizations.iterator());
    }

    @Override
    public List<Organization> getOrganizationsIn(List<String> organizationIds){
        List<ObjectId> objectIds = organizationIds.stream().map(e -> new ObjectId(e)).collect(Collectors.toList());

        Iterable<Organization> organizations = coll.find("{_id:{$in:#}}", objectIds).as(entityClazz);
        return Lists.newArrayList(organizations.iterator());
    }

    @Override
    public List<Organization> getOrganizationsByCompany(String companyId) {
        List<String> companyIds = new ArrayList<String>();
        companyIds.add(companyId);

        Iterable<Organization> organizations = coll.find("{companyIds: {$in:#}}", companyIds).as(entityClazz);
        return Lists.newArrayList(organizations.iterator());
    }
}
