package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.POActivityRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

/**
 * Created by decipher on 4/10/17 5:41 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class POActivityRepositoryImpl extends ShopBaseRepositoryImpl<POActivity> implements POActivityRepository {

    @Inject
    public POActivityRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(POActivity.class, mongoManager);
    }

    /**
     * This method gets list of PO activity by purchase order id
     *
     * @param companyId       : company Id
     * @param shopId          : shop id
     * @param skip            : skip
     * @param limit           : limit
     * @param sortOption      : sort options
     * @param purchaseOrderId : purchase order id
     */
    @Override
    public SearchResult<POActivity> findItemsByPurchaseOrderId(String companyId, String shopId, int skip, int limit, String sortOption, String purchaseOrderId) {
        SearchResult<POActivity> searchResult = new SearchResult<>();
        Iterable<POActivity> result = coll.find("{companyId:#,shopId:#,deleted:false,purchaseOrderId:#}", companyId, shopId, purchaseOrderId).limit(limit).sort(sortOption).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,purchaseOrderId:#}", companyId, shopId, purchaseOrderId);

        searchResult.setValues(Lists.newArrayList(result));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }
}
