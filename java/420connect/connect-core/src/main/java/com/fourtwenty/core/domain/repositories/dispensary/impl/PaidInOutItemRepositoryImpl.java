package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PaidInOutItemRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

/**
 * Created by mdo on 12/7/16.
 */
public class PaidInOutItemRepositoryImpl extends ShopBaseRepositoryImpl<PaidInOutItem> implements PaidInOutItemRepository {
    @Inject
    public PaidInOutItemRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PaidInOutItem.class, mongoManager);
    }


    @Override
    public Iterable<PaidInOutItem> getPaidInOuts(String companyId, String shopId, String cdSessionId) {
        Iterable<PaidInOutItem> items = coll.find("{companyId:#,shopId:#,cdSessionId:#,deleted:false}", companyId, shopId, cdSessionId).as(entityClazz);
        return items;
    }

    @Override
    public SearchResult<PaidInOutItem> findPaidInOuts(String companyId, String shopId, String cdSessionId, int start, int limit) {
        Iterable<PaidInOutItem> items = coll.find("{companyId:#,shopId:#,cdSessionId:#,deleted:false}", companyId, shopId, cdSessionId).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,cdSessionId:#,deleted:false}", companyId, shopId, cdSessionId);

        SearchResult<PaidInOutItem> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<PaidInOutItem> getPaidInOutsTerminal(String companyId, String shopId, String terminalId, int start, int limit) {
        Iterable<PaidInOutItem> items = coll.find("{companyId:#,shopId:#,terminalId:#}", companyId, shopId, terminalId).sort("{created:-1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,terminalId:#}", companyId, shopId, terminalId);

        DateSearchResult<PaidInOutItem> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }
}
