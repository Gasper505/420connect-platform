package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.PasswordReset;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PasswordResetRepository;
import com.google.inject.Inject;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by mdo on 8/3/16.
 */
public class PasswordResetRepositoryImpl extends MongoBaseRepositoryImpl<PasswordReset> implements PasswordResetRepository {

    @Inject
    public PasswordResetRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PasswordReset.class, mongoManager);
    }

    @Override
    public PasswordReset getPasswordReset(String resetCode) {
        return coll.findOne("{resetCode:#}", resetCode).as(entityClazz);
    }

    @Override
    public Iterable<PasswordReset> getPasswordResetForEmployee(String companyId, String employeeId) {
        return coll.find("{companyId:#,employeeId:#}", companyId, employeeId).as(entityClazz);
    }

    @Override
    public void setPasswordExpired(String companyId, String employeeId) {
        coll.update("{companyId:#,employeeId:#}", companyId, employeeId).multi().with("{$set: {expired:#, modified:#}}", true, DateTime.now().getMillis());
    }


    @Override
    public void setPasswordExpiredForEmployees(String companyId, List<String> consumerUserIds) {
        coll.update("{companyId:#, employeeId: {$in:#}}", companyId, consumerUserIds).multi().with("{$set: {expired:#, modified:#}}", true, DateTime.now().getMillis());

    }
}
