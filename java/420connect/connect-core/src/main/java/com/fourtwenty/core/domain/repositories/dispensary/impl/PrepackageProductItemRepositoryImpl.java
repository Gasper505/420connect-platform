package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.google.inject.Inject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/1/17.
 */
public class PrepackageProductItemRepositoryImpl extends ShopBaseRepositoryImpl<PrepackageProductItem> implements PrepackageProductItemRepository {

    @Inject
    public PrepackageProductItemRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PrepackageProductItem.class, mongoManager);
    }

    @Override
    public PrepackageProductItem getPrepackageProduct(String companyId, String shopId, String productId, String prepackageId, String batchId) {
        return coll.findOne("{companyId:#,shopId:#,productId:#,prepackageId:#,batchId:#}", companyId, shopId, productId, prepackageId, batchId).as(entityClazz);
    }

    @Override
    public Iterable<PrepackageProductItem> getPrepackagesForProduct(String companyId, String shopId, String productId) {
        return coll.find("{companyId:#,shopId:#,productId:#,deleted:false}", companyId, shopId, productId).as(entityClazz);
    }

    @Override
    public <E extends PrepackageProductItem> Iterable<E> getPrepackagesForProduct(String companyId, String shopId, String productId, Class<E> clazz) {
        return coll.find("{companyId:#,shopId:#,productId:#,deleted:false}", companyId, shopId, productId).as(clazz);
    }

    @Override
    public Iterable<PrepackageProductItem> getPrepackagesForPrepackageId(String companyId, String shopId, String prepackageId) {
        return coll.find("{companyId:#,shopId:#,prepackageId:#,deleted:false}", companyId, shopId, prepackageId).as(entityClazz);
    }

    @Override
    public Iterable<PrepackageProductItem> getPrepackagesForProducts(String companyId, String shopId, List<String> productIds) {
        Iterable<PrepackageProductItem> items = coll.find("{companyId:#,shopId:#,productId:{$in:#},deleted:false}", companyId, shopId, productIds).as(entityClazz);
        return items;
    }

    @Override
    public HashMap<String, PrepackageProductItem> getPrepackagesForProductsAsMap(String companyId, String shopId, List<String> productIds) {

        Iterable<PrepackageProductItem> items = coll.find("{companyId:#,shopId:#,productId:{$in:#},deleted:false}", companyId, shopId, productIds).as(entityClazz);
        return asMap(items);
    }

    @Override
    public PrepackageProductItem getPrepackageProductById(String prepackageItemId) {
        return coll.findOne("{_id:#}", new ObjectId(prepackageItemId)).as(entityClazz);
    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {quantity:0,deleted:true,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public <E extends PrepackageProductItem> Iterable<E> getPrepackagesForProductIds(String companyId, String shopId, List<String> productIds, Class<E> clazz) {
        return coll.find("{companyId:#,shopId:#,productId:{$in: #},deleted:false}", companyId, shopId, productIds).as(clazz);
    }

    @Override
    public HashMap<String, PrepackageProductItem> getPrepackagesForProductsByCompany(String companyId, List<String> productIds) {
        Iterable<PrepackageProductItem> items = coll.find("{companyId:#,productId:{$in:#},deleted:false}", companyId, productIds).as(entityClazz);
        return asMap(items);
    }

    @Override
    public Iterable<PrepackageProductItem> getPrepackagesForPrepackagesAndProduct(String companyId, String shopId, List<String> prepackageIds, String productId) {
        return coll.find("{companyId:#,shopId:#,prepackageId:{$in:#},deleted:false,productId:#}", companyId, shopId, prepackageIds, productId).as(entityClazz);
    }

    @Override
    public <E extends PrepackageProductItem> Iterable<E> list(String companyId, List<ObjectId> prepackageLineItemIds, Class<E> clazz) {
        return coll.find("{companyId:#,_id : {$in: #}}", companyId, prepackageLineItemIds).as(clazz);
    }

    @Override
    public void updateRunningQuantity(String companyId, String shopId, String entityId, Integer runningTotal) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(entityId)).with("{$set: {runningQuantity:#,modified:#}}", runningTotal, DateTime.now().getMillis());
    }
}
