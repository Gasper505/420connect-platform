package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/15/17.
 */
public class PrepackageRepositoryImpl extends ShopBaseRepositoryImpl<Prepackage> implements PrepackageRepository {
    @Inject
    public PrepackageRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Prepackage.class, mongoManager);
    }


    @Override
    public Prepackage getPrepackage(String companyId, String shopId, String productId, String name) {
        return coll.findOne("{companyId:#,shopId:#,productId:#,name:#}", companyId, shopId, productId, name).as(entityClazz);
    }


    @Override
    public SearchResult<Prepackage> getPrepackages(String companyId, String shopId, String productId) {
        SearchResult<Prepackage> result = getPrepackages(companyId, shopId, productId, entityClazz);
        return result;
    }

    @Override
    public HashMap<String, Prepackage> getPrepackagesForProductsAsMap(String companyId, String shopId, List<String> productIds) {
        Iterable<Prepackage> prepackages = coll.find("{companyId:#,shopId:#,productId:{$in: #}}",
                companyId, shopId, productIds).as(entityClazz);

        return asMap(prepackages);
    }

    @Override
    public <E extends Prepackage> SearchResult<E> getPrepackages(String companyId, String shopId, String productId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,productId:#,deleted:false}", companyId, shopId, productId).as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(0);
        results.setLimit(results.getValues().size());
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public Prepackage getPrepackageById(String prepackageId) {
        return coll.findOne("{_id:#}", new ObjectId(prepackageId)).as(entityClazz);
    }


    public void removeById(String companyId, String entityId, String name) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,modified:#,name:#}}", DateTime.now().getMillis(), name + "--del--" + DateTime.now().getMillis());
    }

    @Override
    public <E extends Prepackage> Iterable<E> getPrepackagesForProducts(String companyId, String shopId, List<String> productIds, Class<E> clazz) {

        return coll.find("{companyId:#, shopId:#, productId:{$in: #}, deleted:false}", companyId, shopId, productIds).as(clazz);

    }

    @Override
    public Iterable<Prepackage> getPrepackagesForProductsByCompany(String companyId, List<String> productIds) {
        return coll.find("{companyId:#, productId:{$in: #}, deleted:false}", companyId, productIds).as(entityClazz);
    }

    @Override
    public void updateRunningQuantity(String companyId, String shopId, String entityId, Integer runningTotal) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(entityId)).with("{$set: {runningQuantity:#,modified:#}}", runningTotal, DateTime.now().getMillis());
    }
}
