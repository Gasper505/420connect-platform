package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.PricingTemplate;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PricingTemplateRepository;

import javax.inject.Inject;

public class PricingTemplateRepositoryImpl extends ShopBaseRepositoryImpl<PricingTemplate> implements PricingTemplateRepository {

    @Inject
    public PricingTemplateRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PricingTemplate.class, mongoManager);
    }

    @Override
    public PricingTemplate getTemplateByName(String companyId, String shopId, String name) {
        return coll.findOne("{companyId:#, shopId:#, deleted:false, name:#}", companyId, shopId, name).as(entityClazz);
    }
}
