package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by mdo on 10/26/15.
 */
public class ProductBatchRepositoryImpl extends ShopBaseRepositoryImpl<ProductBatch> implements ProductBatchRepository {

    @Inject
    public ProductBatchRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ProductBatch.class, mongoManager);
    }

    @Override
    public <E extends ProductBatch> SearchResult<E> getBatches(String companyId, String shopId, String productId, int start, int limit, String sortOptions, Class<E> clazz) {
        Iterable<E> batches = coll.find("{companyId:#,shopId:#,productId:#,deleted:false}", companyId, shopId, productId).sort(sortOptions).skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,productId:#,deleted:false}", companyId, shopId, productId);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(batches));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ProductBatch> getBatches(String companyId, String shopId, int start, int limit) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#,shopId:#,archived:#}", companyId, shopId, false).skip(start).limit(limit).as(entityClazz);

        SearchResult<ProductBatch> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(batches));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public HashMap<String, ProductBatch> getBatchesForProductsMap(String companyId, String shopId, List<String> productIds) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#,shopId:#,productId:{$in: #},deleted:false}", companyId, shopId, productIds).as(entityClazz);
        return asMap(batches);
    }

    @Override
    public HashMap<String, ProductBatch> getBatchesForProductsMap(String companyId, List<String> productIds) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#,productId:{$in: #},deleted:false}", companyId, productIds).as(entityClazz);
        return asMap(batches);
    }

    @Override
    public Iterable<ProductBatch> getBatchesForDateBracket(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, purchasedDate: {$gt:#, $lt:#}}", companyId, shopId, startDate, endDate).sort("{created:-1}").as(entityClazz);

    }

    @Override
    public Iterable<ProductBatch> getBatchesLiveNonMetrcBatches(String companyId, String shopId, int start, int limit) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,archived:false,trackTraceSystem:#}",
                companyId, shopId, ProductBatch.TrackTraceSystem.MANUAL).sort("{purchasedDate:-1}").skip(start).limit(limit).as(entityClazz);
        return batches;
    }

    @Override
    public ProductBatch getLatestBatchForProduct(String companyId, String shopId, String productId) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,productId:#}",
                companyId, shopId, productId).sort("{purchasedDate:-1}").skip(0).limit(1).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        ProductBatch batch = null;

        if (batchList.size() > 0) {
            batch = batchList.get(0);
        }
        return batch;
    }

    @Override
    public ProductBatch getLatestBatchForProductActive(String companyId, String shopId, String productId) {
        ProductBatch batch = coll.findOne("{companyId:#, shopId:#,productId:#,deleted:false,archived:false}",
                companyId, shopId, productId).orderBy("{purchasedDate:-1}").as(entityClazz);
        return batch;
    }

    @Override
    public ProductBatch getBatchBySKU(String companyId, String shopId, String batchSKU) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,sku:#}",
                companyId, shopId, batchSKU).skip(0).limit(1).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        ProductBatch batch = null;

        if (batchList.size() > 0) {
            batch = batchList.get(0);
        }
        return batch;
    }

    @Override
    public ProductBatch getLatestBatchForProductWithMetrc(String companyId, String shopId, String productId) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,productId:#,trackTraceVerified:true,trackTraceSystem:#}",
                companyId, shopId, productId, ProductBatch.TrackTraceSystem.METRC).sort("{purchasedDate:-1}").skip(0).limit(1).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        ProductBatch batch = null;

        if (batchList.size() > 0) {
            batch = batchList.get(0);
        }
        return batch;
    }

    @Override
    public Iterable<ProductBatch> listProductBatchesSorted(String companyId, String shopId, String productId) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,productId:#}",
                companyId, shopId, productId).sort("{purchasedDate:-1}").skip(0).limit(1).as(entityClazz);
        return batches;
    }

    @Override
    public List<ProductBatch> getProductBatches(String companyId, String shopId) {
        Iterable<ProductBatch> result = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public Iterable<ProductBatch> getProductBatchesForLabel(String companyId, String shopId, String label, ProductBatch.TrackTraceSystem trackTraceSystem) {
        Iterable<ProductBatch> result = coll.find("{companyId:#,shopId:#,trackPackageLabel:#,trackTraceVerified:true,trackTraceSystem:#}",
                companyId, shopId, label, trackTraceSystem).as(entityClazz);
        return result;
    }

    @Override
    public long countProductBatchesForLabel(String companyId, String shopId, String label, ProductBatch.TrackTraceSystem trackTraceSystem) {
        return coll.count("{companyId:#,shopId:#,trackPackageLabel:#,trackTraceVerified:true,trackTraceSystem:#}",
                companyId, shopId, label, trackTraceSystem);
    }

    @Override
    public int setProductBatchArchived() {
        WriteResult writeResult = coll.update("{archived:{$exists:false}}").multi().with("{$set:{archived:false, modified:#}}", DateTime.now().getMillis());
        return writeResult.getN();
    }

    @Override
    public Map<String, ProductBatch> getLatestBatchForProductList(String companyId, String shopId, List<String> productIdList) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#,shopId:#,productId:{$in: #}}", companyId, shopId, productIdList).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        Map<String, ProductBatch> productBatchMap = new HashMap<>();

        for (ProductBatch batch : batchList) {
            ProductBatch productBatch = productBatchMap.get(batch.getProductId());

            if (productBatch == null) {
                productBatchMap.put(batch.getProductId(), batch);
            } else if (batch.getCreated() > productBatch.getCreated()) {
                productBatchMap.put(batch.getProductId(), batch);
            }
        }

        return productBatchMap;
    }

    @Override
    public SearchResult<ProductBatchResult> getAllProductBatches(String companyId, String shopId, String sortOption, int start, int limit) {
        Iterable<ProductBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,archived:false,voidStatus:false}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(ProductBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archived:false,voidStatus:false}", companyId, shopId);
        SearchResult<ProductBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ProductBatchResult> getAllArchivedProductBatches(String companyId, String shopId, String sortOption, int start, int limit) {
        Iterable<ProductBatchResult> items = coll.find("{companyId:#,shopId:#,archived:true,deleted:false}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(ProductBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,archived:true}", companyId, shopId);
        SearchResult<ProductBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ProductBatchResult> getAllProductBatchesByState(String companyId, String shopId, boolean state, String sortOption, int start, int limit) {
        Iterable<ProductBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,active:#,archived:false}", companyId, shopId, state).sort(sortOption).skip(start).limit(limit).as(ProductBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:#,archived:false}", companyId, shopId, state);
        SearchResult<ProductBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ProductBatch> SearchResult<E> getAllProductBatchesByStatus(String companyId, String shopId, ProductBatch.BatchStatus status, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,status:#,archived:false,voidStatus:false}", companyId, shopId, status).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,status:#,archived:false,voidStatus:false}", companyId, shopId, status);
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<ProductBatchResult> getAllScannedProductBatch(String companyId, String shopId, String metrcTagId, String productId, int start, int limit, String sortOption) {
        Iterable<ProductBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:false,trackPackageId:#,productId:#}", companyId, shopId, metrcTagId, productId).sort(sortOption).skip(start).limit(limit).as(ProductBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,trackPackageId:#,productId:#}", companyId, shopId, metrcTagId, productId);
        SearchResult<ProductBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ProductBatchResult> getAllDeletedProductBatch(String companyId, String shopId, String sortOption, int start, int limit) {
        Iterable<ProductBatchResult> items = coll.find("{companyId:#,shopId:#,deleted:true}", companyId, shopId).sort(sortOption).skip(start).limit(limit).as(ProductBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:true}", companyId, shopId);
        SearchResult<ProductBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkProductBatchDelete(String companyId, String shopId, List<ObjectId> batchIds) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, batchIds).multi().with("{$set: {deleted:true, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public void bulkProductBatchStatusUpdate(String companyId, String shopId, List<ObjectId> batchIds, ProductBatch.BatchStatus status) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, batchIds).multi().with("{$set: {status:#, modified:#}}", status, DateTime.now().getMillis());
    }

    @Override
    public void bulkProductBatchArchive(String companyId, String shopId, List<ObjectId> batchIds, boolean archive) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, batchIds).multi().with("{$set:{archive:#, modified:#}}", archive, DateTime.now().getMillis());
    }

    @Override
    public void bulkProductBatchVoidStatusUpdate(String companyId, String shopId, List<ObjectId> batchIds, Boolean voidStatus) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, batchIds).multi().with("{$set:{voidStatus:#, modified:#}}", voidStatus, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<ProductBatchResult> getAllProductBatchesByVoidStatus(String companyId, String shopId, boolean voidStatus, String sortOption, int start, int limit) {
        Iterable<ProductBatchResult> items = coll.find("{companyId:#,shopId:#,voidStatus:#,deleted:false,archived:false}", companyId, shopId, voidStatus).limit(limit).skip(start).sort(sortOption).as(ProductBatchResult.class);
        long count = coll.count("{companyId:#,shopId:#,voidStatus:#,deleted:false,archived:false}", companyId, shopId, voidStatus);
        SearchResult<ProductBatchResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<ProductBatch> getBatchesByTerm(String companyId, String shopId, int skip, int limit, String sortOptions, String searchTerm) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        Iterable<ProductBatch> batches = coll.find("{$and: [{companyId:#,shopId:#,deleted:false},{$or:[{productId:#},{status:#},{published:#},{metrcCategory:#}]}]}", companyId, shopId, pattern, pattern, pattern, pattern).sort(sortOptions).skip(skip).limit(limit).as(entityClazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false},{$or:[{productId:#},{status:#},{published:#},{metrcCategory:#}]}]}", companyId, shopId, pattern, pattern, pattern, pattern);

        SearchResult<ProductBatch> searchResult = new SearchResult<>();

        searchResult.setValues(Lists.newArrayList(batches));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ProductBatch> getBatchesByBatchNo(String companyId, String shopId, int skip, int limit, String sortOptions, Long batchNo) {

        Iterable<ProductBatch> batches = coll.find("{companyId:#,shopId:#,deleted:false,batchNo:#}", companyId, shopId, batchNo).sort(sortOptions).skip(skip).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,batchNo:#}", companyId, shopId, batchNo);

        SearchResult<ProductBatch> searchResult = new SearchResult<>();

        searchResult.setValues(Lists.newArrayList(batches));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ProductBatch> getBatchesByTermForStatus(String companyId, String shopId, int skip, int limit, String sortOptions, boolean state) {
        Iterable<ProductBatch> batches = coll.find("{$and: [{companyId:#,shopId:#,deleted:false},{$or:[{active:#},{voidStatus:#},{archived:#},{published:#}]}]}", companyId, shopId, state, state, state, state).sort(sortOptions).skip(skip).limit(limit).as(entityClazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false},{$or:[{active:#},{voidStatus:#},{archived:#},{published:#}]}]}", companyId, shopId, state, state, state, state);

        SearchResult<ProductBatch> searchResult = new SearchResult<>();

        searchResult.setValues(Lists.newArrayList(batches));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }


    @Override
    public ProductBatch getBatchByProductAndSku(String companyId, String shopId, String productId, String sku) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,productId:#,sku:#}", companyId, shopId, productId, sku).skip(0).limit(1).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);

        if (batchList.size() > 0) {
            return batchList.get(0);
        }
        return null;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> getAllProductBatchesByTermWithStatus(String companyId, String shopId, ProductBatch.BatchStatus status, String sortOption, int start, int limit, String searchTerm, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> invoices = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,status:#,archived:false,voidStatus:false},{$or:[{quantity:#},{sku:#},{status:#}]}]}", companyId, shopId, status, pattern, pattern, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,status:#,archived:false,voidStatus:false},{$or:[{quantity:#},{sku:#},{status:#}]}]}", companyId, shopId, status, pattern, pattern, pattern);
        searchResult.setValues(Lists.newArrayList(invoices));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public List<ProductBatch> listAllSorted(String sortOptions) {
        Iterable<ProductBatch> items = coll.find("{deleted:false}").sort(sortOptions).as(entityClazz);
        return Lists.newArrayList(items);
    }

    @Override
    public ProductBatch getProductBatchByVendor(String companyId, String shopId, String batchId, String vendorId) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#,_id:#,vendorId:#}", companyId, shopId, new ObjectId(batchId), vendorId).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        ProductBatch batch = null;
        if (batchList.size() > 0) {
            batch = batchList.get(0);
        }
        return batch;
    }

    @Override
    public void unArchiveBatch(String companyId, String shopId, String batchId) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(batchId)).with("{$set:{archived:false, modified:#}}", DateTime.now().getMillis());
    }


    @Override
    public SearchResult<ProductBatch> getBatchesProductByBatchStatus(String companyId, String shopId, String productId, int start, int limit, String sortOptions, ProductBatch.BatchStatus status) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#,shopId:#,productId:#,status:#,deleted:false}", companyId, shopId, productId, status).sort(sortOptions).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,productId:#,status:#,deleted:false}", companyId, shopId, productId, status);
        SearchResult<ProductBatch> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(batches));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public List<ProductBatch> getLimitedBatchViewAsMap(String companyId, String shopId) {
        Iterable<ProductBatch> result = coll.find("{companyId:#, shopId:#, deleted:false}}", companyId, shopId)
                .projection("{companyId:1, shopId:1, deleted:1, sku:1, vendorId:1, productId:1, brandId:1, status:1, quantity:1, created:1, modified:1, purchasedDate:1, poNumber:1, purchaseOrderId:1, receiveDate:1, trackHarvestBatch:1, liveQuantity:1, actualWeightPerUnit:1}")
                .as(entityClazz);
        return Lists.newArrayList(result);
    }

    @Override
    public void updateLiveQuantity(String companyId, String shopId, String batchId, double quantity) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(batchId)).with("{$set: { liveQuantity:#, modified:# }}", quantity, DateTime.now().getMillis());
    }

    @Override
    public void updateMetrcTag(String batchId, String metrcTag, ProductWeightTolerance.WeightKey trackWeight) {
        coll.update("{_id:#}", new ObjectId(batchId))
                .with("{$set: { trackTraceSystem:#,trackPackageLabel:#, trackWeight:#, modified:# }}",
                        ProductBatch.TrackTraceSystem.METRC, metrcTag, trackWeight,DateTime.now().getMillis());
    }

    @Override
    public void updateProductBatchSku(String companyId, String shopId, String batchId, String sku) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(batchId)).with("{$set: { sku:#, modified:# }}", sku, DateTime.now().getMillis());
    }

    @Override
    public <E extends ProductBatch> List<E> getBatchesForProductListAndBatchStatus(String companyId, String shopId, List<String> productIds, List<ProductBatch.BatchStatus> statuses, Class<E> clazz) {
        Iterable<E> batches = coll.find("{companyId:#, shopId:#, deleted:false, productId:{$in: #}, status:{$in: #}}", companyId, shopId, productIds, statuses).as(clazz);
        return Lists.newArrayList(batches);
    }

    @Override
    public Map<String, ProductBatch> getOldestBatchForProductList(String companyId, String shopId, ArrayList<String> productIdList) {
        Iterable<ProductBatch> batches = coll.find("{companyId:#, shopId:#, archived:false, productId:{$in: #}}", companyId, shopId, productIdList).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        Map<String, ProductBatch> productBatchMap = new HashMap<>();

        for (ProductBatch batch : batchList) {
            ProductBatch productBatch = productBatchMap.get(batch.getProductId());

            if (productBatch == null) {
                productBatchMap.put(batch.getProductId(), batch);
            } else if (batch.getCreated() < productBatch.getCreated()) {
                productBatchMap.put(batch.getProductId(), batch);
            }
        }
        return productBatchMap;
    }

    @Override
    public Map<String, ProductBatch> getBatchesByBatchesId(List<String> batchIdList) {
        List<ObjectId> objectsId= new ArrayList<>();
        for (String id : batchIdList){
            if(id != null && ObjectId.isValid(id)) {
                objectsId.add(new ObjectId(id));
            }
        }
        Iterable<ProductBatch> batches = coll.find("{_id:{$in: #}}", objectsId).as(entityClazz);
        List<ProductBatch> batchList = Lists.newArrayList(batches);
        Map<String, ProductBatch> productBatchMap = new HashMap<>();

        for (ProductBatch batch : batchList) {
            productBatchMap.put(batch.getId(),batch);
        }

        return productBatchMap;
    }
}
