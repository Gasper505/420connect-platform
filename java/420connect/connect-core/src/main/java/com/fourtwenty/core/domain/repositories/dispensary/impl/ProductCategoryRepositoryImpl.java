package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryUpdateRequest;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mdo on 10/14/15.
 */
public class ProductCategoryRepositoryImpl extends ShopBaseRepositoryImpl<ProductCategory> implements ProductCategoryRepository {

    @Inject
    public ProductCategoryRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ProductCategory.class, mongoManager);
    }

    @Override
    public void update(String companyId, String productCategoryId, ProductCategoryUpdateRequest request) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productCategoryId)).with("{$set: {name:#,cannabis:#,modified:#}}",
                request.getName(), request.isCannabis(), DateTime.now().getMillis());
    }


    @Override
    public ProductCategory getCategory(String companyId, String shopId, String name) {
        Iterator<ProductCategory> iters = coll.find("{companyId:#,shopId:#,name:#,deleted:false}", companyId, shopId, name).as(entityClazz);
        List<ProductCategory> categoryList = Lists.newArrayList(iters);

        if (categoryList.size() > 0) {
            return categoryList.get(0);
        }
        return null;
    }

    @Override
    public ProductCategory getCategoryByPhotoKey(String companyId, String photoKey) {
        return coll.findOne("{companyId:#,photo.key:#}", companyId, photoKey).as(entityClazz);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: false}, errorTime:{$exists:true}, modified:{$gt:#}}", companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: false}, errorTime:{$exists:false}, editSequence:{$exists: false}, qbListId:{$exists: false}}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutQbDesktopRefWithoutError(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: false}, qbErrored:false, editSequence:{$exists: false}, qbListId:{$exists: false}}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getQBExistProductCategories(String companyId, String shopId, int start, int limit, long startTime, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, modified:{$gt:#}}", companyId, shopId, startTime).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void updateProductCategoryQbErrorAndTime(String companyId, String shopId, String id, boolean qbErrored, long errorTime) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(id)).with("{$set: {qbErrored:#, errorTime:#}}", qbErrored, errorTime);
    }

    @Override
    public void updateProductCategoryRef(String companyId, String shopId, String id, String qbDesktopRef, String editSequence, String qbListId) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId,shopId, new ObjectId(id)).with("{$set: {qbDesktopRef:#, editSequence:#, qbListId:#}}", qbDesktopRef, editSequence, qbListId);
    }

    @Override
    public void updateEditSequenceAndRef(String companyId, String shopId, String id, String qbListId, String editSequence, String qbDesktopRef) {
        coll.update("{companyId:#,shopId:#, _id:#, qbListId:#}", companyId,shopId, new ObjectId(id), qbListId).with("{$set: {qbDesktopRef:#, editSequence:#}}", qbDesktopRef, editSequence);
    }

    @Override
    public void updateEditSequence(String companyId, String shopId, String qbListId, String editSequence) {
        coll.update("{companyId:#, shopId:#, qbListId:#}", companyId, shopId, qbListId).with("{$set: {editSequence:#}}", editSequence);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutSyncedStatus(String companyId, String shopId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: false}}", companyId, shopId).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutQuantitySynced(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, qbQuantitySynced : {$exists : false}, qbQuantityErrored : {$exists : false}}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimits(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, qbQuantitySynced : false, qbQuantityErrored : false}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesByLimitsWithQbQuantityError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: true}, qbQuantitySynced:{$exists:false}, qbQuantityErrored : true, qbQuantityErrorTime:{$exists:true}, modified:{$gt:#}}", companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimitsWithQuantitySynced(String companyId, String shopId, long dateTime, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, qbQuantityErrored: {$exists:false}, modified:{$gt:#}}", companyId, shopId, dateTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductNewCategoriesLimitsWithQuantitySynced(String companyId, String shopId, long dateTime, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, qbQuantityErrored: false, modified:{$gt:#}}", companyId, shopId, dateTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void updateProductCategoryQbQuantityErrorAndTime(String companyId, String id, boolean qbQuantityErrored, long errorQuantityTime) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(id)).with("{$set: {qbQuantityErrored:#, qbQuantityErrorTime:#}}", qbQuantityErrored, errorQuantityTime);
    }

    @Override
    public void updateProductCategoryQbQuantitySynced(String companyId, String id, boolean qbQuantitySynced) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(id)).with("{$set: {qbQuantitySynced:#}}", qbQuantitySynced);
    }

    @Override
    public <E extends ProductCategory> List<E> getProductCategoriesLimitsWithoutSyncedQuantityStatus(String companyId, String shopId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, qbQuantitySynced:{$exists: false}, qbQuantityErrored : false}", companyId, shopId).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void hardRemoveQuickBookDataInProductCategories(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {qbDesktopRef:1,editSequence:1, qbListId:1, qbErrored:1, errorTime:1, qbQuantitySynced:1,qbQuantityErrored:1,qbQuantityErrorTime:1}}");
    }

    @Override
    public void updateWmCategory(String companyId, String shopId, String categoryId, String wmCategory) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(categoryId)).multi().with("{$set: {wmCategory:#, modified:#}}", wmCategory, DateTime.now().getMillis());
    }
}
