package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductChangeLog;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ProductChangeLogRepository;
import com.fourtwenty.core.reporting.model.reportmodels.InventoryHistory;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import com.mongodb.AggregationOptions;
import org.jongo.Aggregate;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by mdo on 11/6/16.
 */
public class ProductChangeLogRepositoryImpl extends ShopBaseRepositoryImpl<ProductChangeLog> implements ProductChangeLogRepository {
    @Inject
    public ProductChangeLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ProductChangeLog.class, mongoManager);
    }


    @Override
    public Iterable<ProductChangeLog> getProductChangeLogsDates(String companyId, String shopId, String productId, long startDate, long endDate) {
        return coll.find("{companyId:#,shopId:#,productId:#, created: {$gte:#, $lte:#}}", companyId, shopId, productId, startDate, endDate)
                .sort("{created:-1}")
                .as(entityClazz);
    }

    @Override
    public Iterable<ProductChangeLog> getChangeLogsWithReferenceBegins(String companyId, String shopId, String refBegins, long afterDate, long beforeDate) {
        Pattern pattern = TextUtil.createPattern(refBegins);
        return coll.find("{companyId:#,shopId:#, created: {$gte:#, $lte:#}, reference:#}", companyId, shopId, afterDate, beforeDate, pattern)
                .sort("{created:-1}")
                .as(entityClazz);
    }

    @Override
    public Iterable<InventoryHistory> getTopGroupProductChangeLog(String companyId, String shopId, long beforeEndDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<InventoryHistory> results = coll.aggregate("{$match: {companyId:#,shopId:#,created:{$lte: #}}}", companyId, shopId, beforeEndDate)
                .and("{$sort: {created:-1}}")
                .and("{$group: {_id:'$productId', created: {$first: '$created'},  employeeId: {$first: '$employeeId'}, reference: {$first: '$reference'}, productQuantities: {$first: '$productQuantities'}}}")
                .options(aggregationOptions)
                .as(InventoryHistory.class);

        return results;
    }

    @Override
    public Iterable<InventoryHistory> getTopGroupProductChangeLog(String companyId, String shopId, long startDate, long beforeEndDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<InventoryHistory> results = coll.aggregate("{$match: {companyId:#,shopId:#,created:{$gte:#,$lte: #}}}", companyId, shopId, startDate, beforeEndDate)
                .and("{$sort: {created:-1}}")
                .and("{$group: {_id:'$productId', created: {$first: '$created'},  employeeId: {$first: '$employeeId'}, reference: {$first: '$reference'}, productQuantities: {$first: '$productQuantities'}}}")
                .options(aggregationOptions)
                .as(InventoryHistory.class);

        return results;
    }

    @Override
    public ProductChangeLog createNewChangeLog(String employeeId, Product product, List<ProductPrepackageQuantity> curPrepackages, String reference) {
        // Add change log
        ProductChangeLog changeLog = new ProductChangeLog();
        changeLog.prepare(product.getCompanyId());
        changeLog.setShopId(product.getShopId());
        changeLog.setProductId(product.getId());
        changeLog.setEmployeeId(employeeId);
        changeLog.setReference(reference);
        changeLog.setProductQuantities(product.getQuantities());
        changeLog.setPrepackageQuantities(curPrepackages);

        return changeLog;
    }

}
