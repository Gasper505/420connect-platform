package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/31/17.
 */
public class ProductPrepackageQuantityRepositoryImpl extends ShopBaseRepositoryImpl<ProductPrepackageQuantity> implements ProductPrepackageQuantityRepository {

    @Inject
    public ProductPrepackageQuantityRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ProductPrepackageQuantity.class, mongoManager);
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForProduct(String companyId, String shopId, String productId) {
        return coll.find("{companyId:#,shopId:#,productId:#}", companyId, shopId, productId).as(entityClazz);
    }

    @Override
    public List<ProductPrepackageQuantity> getQuantitiesForProductAsList(String companyId, String shopId, String productId) {
        return Lists.newArrayList(getQuantitiesForProduct(companyId, shopId, productId));
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForProducts(String companyId, String shopId, List<String> productIds) {
        return coll.find("{companyId:#,shopId:#,productId:{$in:#}}", companyId, shopId, productIds).as(entityClazz);
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackageItem(String companyId, String shopId, String prepackageItemId) {
        return coll.find("{companyId:#,shopId:#,prepackageItemId:#}", companyId, shopId, prepackageItemId).as(entityClazz);
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackage(String companyId, String shopId, String prepackageId) {
        return coll.find("{companyId:#,shopId:#,prepackageId:#}", companyId, shopId, prepackageId).as(entityClazz);
    }

    @Override
    public ProductPrepackageQuantity getQuantity(String companyId, String shopId, String inventoryId, String prepackageItemId) {
        return coll.findOne("{companyId:#,shopId:#,inventoryId:#,prepackageItemId:#}",
                companyId, shopId, inventoryId, prepackageItemId).as(entityClazz);
    }

    @Override
    public void resetAllProductPrepackageQuantities(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#,deleted:false}", companyId, shopId).multi().with("{$set: {quantity:0,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public List<ProductPrepackageQuantity> getProductPrepackageQuantities(String companyId, String shopId) {
        Iterable<ProductPrepackageQuantity> result = coll.find("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
        ;
        return Lists.newArrayList(result);
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForInventory(String companyId, String shopId, String inventoryId) {
        Iterable<ProductPrepackageQuantity> result = coll.find("{companyId:#,shopId:#,inventoryId:#}", companyId, shopId, inventoryId).as(entityClazz);
        ;
        return result;
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForInventoryWithQuantity(String companyId, String shopId, String inventoryId) {
        Iterable<ProductPrepackageQuantity> result = coll.find("{companyId:#,shopId:#,inventoryId:#, quantity: {$gt: 0}}", companyId, shopId, inventoryId).as(entityClazz);
        ;
        return result;
    }

    @Override
    public HashMap<String, Integer> getQuantitiesForInventoryWithQuantityProductMap(String companyId, String shopId, String inventoryId) {
        Iterable<ProductPrepackageQuantity> prepackageQuantities = getQuantitiesForInventoryWithQuantity(companyId, shopId, inventoryId);

        return this.createQuantityMap(prepackageQuantities);
    }

    @Override
    public HashMap<String, ProductPrepackageQuantity> getQuantitiesForInventoryAsMap(String companyId, String shopId, String inventoryId) {
        Iterable<ProductPrepackageQuantity> result = coll.find("{companyId:#,shopId:#,inventoryId:#}", companyId, shopId, inventoryId).as(entityClazz);
        ;
        return asMap(result);
    }

    @Override
    public HashMap<String, ProductPrepackageQuantity> getQuantitiesForProductsAsMap(String companyId, String shopId, List<String> productIds) {
        Iterable<ProductPrepackageQuantity> quantities = coll.find("{companyId:#,shopId:#,productId:{$in:#}}", companyId, shopId, productIds).as(entityClazz);
        return asMap(quantities);
    }

    @Override
    public HashMap<String, Integer> getQuantitiesForProductsWithQuantityProductMap(String companyId, String shopId, List<String> productIds) {
        Iterable<ProductPrepackageQuantity> prepackageQuantities = this.getQuantitiesForProductsWithQuantity(companyId, shopId, productIds);

        return this.createQuantityMap(prepackageQuantities);
    }

    private HashMap<String, Integer> createQuantityMap(Iterable<ProductPrepackageQuantity> prepackageQuantities) {
        HashMap<String, Integer> quantityMap = new HashMap<>();

        for (ProductPrepackageQuantity quantity : prepackageQuantities) {
            Integer amt = quantityMap.get(quantity.getProductId());
            if (amt == null) {
                amt = 0;
            }
            amt += quantity.getQuantity();
            quantityMap.put(quantity.getProductId(), amt);
        }

        return quantityMap;
    }

    private Iterable<ProductPrepackageQuantity> getQuantitiesForProductsWithQuantity(String companyId, String shopId, List<String> productIds) {
        Iterable<ProductPrepackageQuantity> result = coll.find("{companyId:#,shopId:#, productId:{$in:#}, quantity: {$gt: 0}}", companyId, shopId, productIds).as(entityClazz);
        ;
        return result;
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesWithProductAndPrepackage(String companyId, String shopId, List<String> prepackageIds, String productId) {
        return coll.find("{companyId:#,shopId:#,deleted:false,prepackageId:{$in:#},productId:#}", companyId, shopId, prepackageIds, productId).as(entityClazz);
    }

    @Override
    public HashMap<String, ProductPrepackageQuantity> getQuantitiesForPrepackageItems(String companyId, String shopId, String inventoryId, List<String> prepackageItemIdList) {
        Iterable<ProductPrepackageQuantity> quantities = coll.find("{companyId:#, shopId:#, inventoryId:#, prepackageItemId:{$in:#}}", companyId, shopId, inventoryId, prepackageItemIdList).as(entityClazz);

        HashMap<String, ProductPrepackageQuantity> quantityHashMap = new HashMap<>();
        for (ProductPrepackageQuantity quantity : quantities) {
            quantityHashMap.putIfAbsent(quantity.getPrepackageItemId(), quantity);
        }

        return quantityHashMap;
    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackageItemByInventory(String companyId, String shopId, String inventoryId, List<String> productIds) {
        return coll.find("{companyId:#,shopId:#,inventoryId : #, productId:{$in:#}}", companyId, shopId, inventoryId, productIds).as(entityClazz);

    }

    @Override
    public Iterable<ProductPrepackageQuantity> getQuantitiesForPrepackageItem(String companyId, String shopId, List<String> prepackageItemIds) {
        return coll.find("{companyId:#,shopId:#, prepackageItemId:{$in:#}}", companyId, shopId, prepackageItemIds).as(entityClazz);

    }


}
