package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.model.reportmodels.BestSellingBrands;
import com.fourtwenty.core.reporting.model.reportmodels.InventoryByStrain;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByProductCategory;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;
import org.jongo.MongoCursor;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by Stephen Schmidt on 10/6/2015.
 */
public class ProductRepositoryImpl extends ShopBaseRepositoryImpl<Product> implements ProductRepository {

    @Inject
    public ProductRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Product.class, mongoManager);
    }

    @Override
    public Iterable<Product> listItemsBreaksAfterCreated(long afterDate) {
        Iterable<Product> items = coll.find("{priceBreaks.0:{$exists:true},created:{$gte:#}}", afterDate).as(entityClazz);

        return items;
    }

    @Override
    public SearchResult<Product> findProductsByCategoryId(String companyId, String shopId, String categoryId, String sortOption, int start, int limit) {
        Iterable<Product> items = coll.find("{companyId:#,shopId:#,deleted:false,categoryId:#}", companyId, shopId, categoryId).sort(sortOption).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,categoryId:#}", companyId, shopId, categoryId);

        SearchResult<Product> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryId(String companyId, String shopId, String categoryId, String sortOption, int start, int limit, Class<E> eClass) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,categoryId:#}", companyId, shopId, categoryId).sort(sortOption).skip(start).limit(limit).as(eClass);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,categoryId:#}", companyId, shopId, categoryId);

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryId(String companyId, String shopId, String categoryId, List<String> tags, String sortOption, boolean active, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:#,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, active, categoryId, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:#,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, active, categoryId, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:#,categoryId:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, active, categoryId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:#,categoryId:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, active, categoryId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdStrain(String companyId, String shopId, String categoryId, String strain, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, strain, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, strain, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, strain).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, strain);
        }


        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdVendor(String companyId, String shopId, String categoryId, String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, vendorId, vendorId);
        }


        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdAndVendorId(String companyId, String shopId, String categoryId, String strain, String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, vendorId, vendorId);
        }


        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrain(String companyId, String shopId, String strain, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, strain, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, strain, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, strain).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, strain);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByVendorId(String companyId, String shopId, String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainAndVendor(String companyId, String shopId, String strain, String vendorId, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public Iterable<Product> findProductsByIds(String companyId, String shopId, List<ObjectId> productIds) {
        Iterable<Product> items = coll.find("{companyId:#,shopId:#,_id: {$in:#}}}", companyId, shopId, productIds).as(entityClazz);
        return items;
    }

    @Override
    public HashMap<String, Product> findProductsByIdsAsMap(String companyId, String shopId, List<ObjectId> productIds) {
        Iterable<Product> items = this.findProductsByIds(companyId, shopId, productIds);
        HashMap<String, Product> productHashMap = new HashMap<>();
        for (Product p : items) {
            productHashMap.put(p.getId(), p);
        }
        return productHashMap;
    }

    @Override
    public List<Product> getProducts(String companyId) {
        return Lists.newArrayList((Iterable<Product>) coll.find("{companyId:#,deleted:false}}", companyId).as(entityClazz));
    }

    @Override
    public Iterable<Product> getProductsForCategories(String companyId, List<String> categoryIds) {
        return coll.find("{companyId:#,deleted:false,categoryId:{$in:#}}", companyId, categoryIds).as(entityClazz);
    }

    @Override
    public Iterable<Product> getActiveProductsForCategories(String companyId, List<String> categoryIds) {
        return coll.find("{companyId:#,deleted:false,active:true,categoryId:{$in:#}}", companyId, categoryIds).as(entityClazz);
    }

    @Override
    public Iterable<Product> getProductsForCategory(String companyId, String categoryId) {
        return coll.find("{companyId:#,deleted:false,categoryId:#}", companyId, categoryId).as(entityClazz);
    }

    @Override
    public Iterable<Product> getProductsForCategoriesNewer(String companyId, String shopId, List<String> categoryIds, long afterDate) {
        return coll.find("{companyId:#,shopId:#,categoryId:{$in:#},modified:{$gte:#}}", companyId, shopId, categoryIds, afterDate).as(entityClazz);
    }

    @Override
    public Iterable<Product> getProductsForCategoriesOlderThan(String companyId, String shopId, List<String> categoryIds, long afterDate) {
        return coll.find("{companyId:#,shopId:#,categoryId:{$in:#},lastWMSyncTime:{lt:#}}", companyId, shopId, categoryIds, afterDate).as(entityClazz);
    }

    @Override
    public HashMap<String, Product> getProductsForCategoriesAsMap(String companyId, List<String> categoryIds) {
        return this.asMap(getProductsForCategories(companyId, categoryIds));
    }

    @Override
    public Iterable<Product> findProductsWithRanges(String companyId, String shopId) {
        return coll.find("{companyId:#,shopId:#,deleted:false,priceRanges.1: {$exists: true}}", companyId, shopId).as(entityClazz);
    }

    @Override
    public Iterable<InventoryByStrain> getInventoryByCannabisType(String companyId, String shopId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<InventoryByStrain> results = coll.aggregate("{$match: {companyId:#, shopId:#, deleted:false,active:#}}", companyId, shopId, true)
                .and("{$unwind: '$quantities'}")
                .and("{$group: {_id:'$flowerType', value: {$sum: {$multiply:['$quantities.quantity', '$unitPrice'] }}}}")
                .and("{$project: {_id:1, value:1}}")
                .options(aggregationOptions)
                .as(InventoryByStrain.class);
        return results;
    }

    @Override
    public long countProductsForCategory(String companyId, String categoryId) {
        return coll.count("{companyId:#,categoryId:#,deleted:false}", companyId, categoryId);
    }

    @Override
    public void updateCachedCategory(String companyId, String shopId, String categoryId, ProductCategory category) {
        coll.update("{companyId:#,shopId:#,deleted:false,categoryId:#}", companyId, shopId, categoryId)
                .multi()
                .with("{$set: {category:#,modified:#}}", category, DateTime.now().getMillis());
    }

    @Override
    public void updateCachedAssets(String companyId, String productId, List<CompanyAsset> assets) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {assets:#,modified:#}}", assets, DateTime.now().getMillis());
    }

    @Override
    public void updateProductSKU(String companyId, String productId, String sku) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {sku:#,modified:#}}", sku, DateTime.now().getMillis());
    }

    @Override
    public void updateProductTaxInfo(String companyId, String productId, TaxInfo taxInfo) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {customTaxInfo:#,modified:#}}", taxInfo, DateTime.now().getMillis());
    }

    @Override
    public void updateProductPriceRanges(String companyId, String productId, List<ProductPriceRange> priceRanges) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {priceRanges:#,modified:#,priceBreaks:[]}}", priceRanges, DateTime.now().getMillis());
    }

    @Override
    public void updateProductPriceBreaks(String companyId, String productId, List<ProductPriceBreak> priceBreaks) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {priceBreaks:#,modified:#,priceRanges:[]}}", priceBreaks, DateTime.now().getMillis());
    }

    @Override
    public void updateProductQuantities(String companyId, String productId, List<ProductQuantity> quantities) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {quantities:#,modified:#}}", quantities, DateTime.now().getMillis());
    }

    @Override
    public void updateInStockFlag(String companyId, String productId, boolean inStock) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {instock:#,modified:#}}", inStock, DateTime.now().getMillis());

    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public <E extends Product> SearchResult<E> findProductWithAvailableInventory(String companyId, String shopId, String inventoryId, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,quantities.inventoryId:#,quantities.quantity: {$gt: 0},deleted:false}", companyId, shopId, inventoryId)
                .skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,quantities.inventoryId:#,quantities.quantity: {$gt: 0},deleted:false}", companyId, shopId, inventoryId);

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(skip);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public long countProductWithAvailableInventory(String companyId, String inventoryId) {
        return coll.count("{companyId:#,quantities.inventoryId:#,quantities.quantity: {$gt: 0}}", companyId, inventoryId);
    }

    @Override
    public <E extends Product> SearchResult<E> getProductList(String companyId, String shopId, List<String> tags, String sortOptions, int skip, int limit, Class<E> clazz) {

        Iterable<E> items;
        long count;

        if (tags.size() > 0) {
            items = coll.find("{companyId:#, shopId:#, active:true,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}"
                    , companyId, shopId, tags).skip(skip).limit(limit).sort(sortOptions)
                    .as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, tags);
        } else {
            items = coll.find("{companyId:#, shopId:#, active:true,showInWidget:true,deleted:false,category.active:true}"
                    , companyId, shopId).skip(skip).limit(limit).sort(sortOptions)
                    .as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true}", companyId, shopId);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Product> SearchResult<E> getProductListInStock(String companyId, String shopId, List<String> tags, String sortOptions, int skip, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;

        if (tags.size() > 0) {
            items = coll.find("{companyId:#, shopId:#, active:true,showInWidget:true,deleted:false,instock:true,category.active:true,tags: {$in:#}}"
                    , companyId, shopId, tags).skip(skip).limit(limit).sort(sortOptions)
                    .as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,instock:true,category.active:true,tags: {$in:#}}", companyId, shopId, tags);
        } else {
            items = coll.find("{companyId:#, shopId:#, active:true,showInWidget:true,deleted:false,instock:true,category.active:true}"
                    , companyId, shopId).skip(skip).limit(limit).sort(sortOptions)
                    .as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,instock:true,category.active:true}", companyId, shopId);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public List<Product> getActiveProductList(String companyId, String shopId) {
        Iterable<Product> result = coll.find("{companyId:#, shopId:#, deleted:false ,active:true}"
                , companyId, shopId).as(Product.class);
        return Lists.newArrayList(result);
    }

    @Override
    public Product findProductWithAssetKey(String companyId, String assetKey) {
        Iterable<Product> iters = coll.find("{companyId:#,assets.key:#}", companyId, assetKey).as(entityClazz);
        for (Product product : iters) {
            return product;
        }
        return null;
    }

    // Search


    @Override
    public Iterable<Product> findProductsWithBreaks() {
        Iterable<Product> iters = coll.find("{priceBreaks.0: {$exists:true}}").as(entityClazz);
        return iters;
    }

    @Override
    public void resetAllProductsStock(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#,deleted:false}", companyId, shopId).multi().with("{$set: {quantities:[], modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdWithAvailableQuantity(String companyId, String shopId, String categoryId, String sortOption, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,categoryId:#,quantities.quantity: {$gt: 0}}", companyId, shopId, categoryId).sort(sortOption).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,categoryId:#,quantities.quantity: {$gt: 0}}", companyId, shopId, categoryId);

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(skip);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findItemsWithAvailableQuantity(String companyId, String shopId, String sortOptions, int skip, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,quantities.quantity: {$gt: 0}}", companyId, shopId).sort(sortOptions).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,quantities.quantity: {$gt: 0}}", companyId, shopId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findProductsByPattern(String companyId, String shopId, String sortOptions, String searchTerm, int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPatternWithSpecialChars(searchTerm);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,deleted:false}, {$or: [{name:#}]}]}", companyId, shopId, pattern).sort(sortOptions).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false}, {$or: [{name:#}]}]}", companyId, shopId, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Product> SearchResult<E> getProductsByVendorId(String companyId, String shopId, String vendorId, boolean active, Class<E> clazz) {
        Iterable<E> items = null;
        if (active == true) {
            items = coll.find("{companyId:#, shopId:#, deleted:false, active:#, $or: [{vendorId: #}, {$and: [{secondaryVendors : {$exists:true}}, {secondaryVendors:#}]}]}"
                    , companyId, shopId, active, vendorId, vendorId).sort("{name:1}")
                    .as(clazz);
        } else {
            items = coll.find("{companyId:#, shopId:#, deleted:false, $or: [{vendorId: #}, {$and: [{secondaryVendors : {$exists:true}}, {secondaryVendors:#}]}]}"
                    , companyId, shopId, vendorId, vendorId).sort("{name:1}")
                    .as(clazz);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(0);
        results.setLimit(0);
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public <E extends Product> SearchResult<E> findItemsSearch(String companyId, String shopId, String term, List<String> tags, String sortOptions,
                                                               int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, pattern, tags)
                    .skip(skip).limit(limit).sort(sortOptions).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, pattern, tags);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, pattern)
                    .skip(skip).limit(limit).sort(sortOptions).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, pattern);

        }
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdSearch(String companyId, String shopId, String categoryId,
                                                                              String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, pattern, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdStrainSearch(String companyId, String shopId, String categoryId, String strain, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, strain, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, strain, pattern, tags);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, strain, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, strain, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdVendorSearch(String companyId, String shopId, String categoryId, String vendorId, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, pattern, tags, vendorId, vendorId);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdStrainVendorSearch(String companyId, String shopId, String categoryId, String strain, String vendorId, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, pattern, tags, vendorId, vendorId);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainSearch(String companyId, String shopId, String strain, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, strain, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, strain, pattern, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, strain, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, strain, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByVendorIdSearch(String companyId, String shopId, String vendorId, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, pattern, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainVendorSearch(String companyId, String shopId, String strain, String vendorId, String term, List<String> tags, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, pattern, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public void bulkUpdateStatus(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean active) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {active:#, modified:#}}", active, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateVendor(String companyId, String shopId, List<ObjectId> productObjectIdList, String vendorId) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {vendorId:#, modified:#}}", vendorId, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateMixMatch(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean enableMixMatch) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {enableMixMatch:#, modified:#}}", enableMixMatch, DateTime.now().getMillis());
    }

    @Override
    public void bulkDeleteProduct(String companyId, String shopId, List<ObjectId> productObjectIdList) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {deleted:true, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateWeedMapStatus(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean weedMapsStatus) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {enableWeedmap:#, modified:#}}", weedMapsStatus, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateLowInventoryNotification(String companyId, String shopId, List<ObjectId> productObjectIdList, boolean lowInventoryNotification, BigDecimal lowThreshold) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {lowThreshold:#, lowInventoryNotification:#, modified:#}}", lowThreshold.doubleValue(), lowInventoryNotification, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateTaxInfo(String companyId, String shopId, List<ObjectId> productObjectIdList, TaxInfo.TaxType taxType, TaxInfo.TaxProcessingOrder taxOrder, TaxInfo customTaxInfo) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {taxType:#, taxOrder:#, customTaxInfo:#, modified:#}}", taxType, taxOrder, customTaxInfo, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateUnitPrice(String companyId, String shopId, List<ObjectId> productObjectIdList, BigDecimal unitPrice) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {unitPrice:#, modified:#}}", unitPrice.doubleValue(), DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateWeightTolerancePrice(String companyId, String shopId, List<ObjectId> productObjectIdList, String weightToleranceId, BigDecimal price) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#},priceRanges.weightToleranceId:#}", companyId, shopId, productObjectIdList, weightToleranceId).multi().with("{$set: {priceRanges.$.price: #, modified:#}}", price.doubleValue(), DateTime.now().getMillis());
    }


    @Override
    public void bulkUpdateCategory(String companyId, String shopId, List<ObjectId> productObjectIdList, ProductCategory category) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {categoryId:#, category:#,modified:#}}", category.getId(), category, DateTime.now().getMillis());

    }

    @Override
    public void bulkOperationType(String companyId, String shopId, List<ObjectId> productObjectIdList, Product.ProductSaleType productSaleType) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {productSaleType:#, modified:#}}", productSaleType, DateTime.now().getMillis());

    }

    @Override
    public MongoCursor<Product> getVendorIdListByCategoryId(String companyId, String shopId, String categoryId, String projection) {
        return coll.find("{companyId:#,shopId:#,categoryId:#}}", companyId, shopId, categoryId).projection(projection).as(entityClazz);
    }

    @Override
    public SearchResult<Product> getProducts(String companyId, String shopId, String sortOptions, int skip, int limit) {

        Iterable<Product> items = coll.find("{companyId:#, shopId:#, active:true,showInWidget:true,deleted:false,category.active:true}"
                , companyId, shopId).skip(skip).limit(limit).sort(sortOptions)
                .as(Product.class);

        long count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,category.active:true}", companyId, shopId);

        SearchResult<Product> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkUpdateProductTag(String companyId, String shopId, List<ObjectId> productObjectIdList, LinkedHashSet<String> tags) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {tags:#, modified:#}}", tags, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateProductTaxtable(String companyId, String shopId, List<ObjectId> productObjectIdList, List<CompoundTaxTable> taxTables, TaxInfo.TaxType taxType) {
        coll.update("{companyId:#,shopId:#,_id: {$in:#}}", companyId, shopId, productObjectIdList).multi().with("{$set: {taxTables:#, taxType:#, modified:#}}", taxTables, taxType, DateTime.now().getMillis());
    }

    @Override
    public void bulkRemoveProductTags(String companyId, String shopId, List<String> tagListToRemove) {
        coll.update("{companyId:#,shopId:#, tags: {$in:#}}", companyId, shopId, tagListToRemove).multi().with("{$pull: {tags:{$in:#}},$set: {modified:#}}", tagListToRemove, DateTime.now().getMillis());
    }

    @Override
    public void updateProductTaxTable(String companyId, String productId, List<CompoundTaxTable> taxTables) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(productId)).with("{$set: {taxTables:#, modified:#}}", taxTables, DateTime.now().getMillis());
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryIdAndTerm(String companyId, String shopId, String sortOptions, String categoryId, String term, int skip, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPatternWithSpecialChars(term);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,categoryId:#,deleted:false}, {$or: [{name:#}]}]}", companyId, shopId, categoryId, pattern).sort(sortOptions).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#,categoryId:#,deleted:false}, {$or: [{name:#}]}]}", companyId, shopId, categoryId, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkUpdateCannabisType(String companyId, String shopId, List<ObjectId> productObjectList, Product.CannabisType cannabisType) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, productObjectList).multi().with("{$set:{cannabisType:#,modified:#}}", cannabisType, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdatePriceIncludesExcise(String companyId, String shopId, List<ObjectId> productObjectList, Boolean priceIncludesExcise) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, productObjectList).multi().with("{$set:{priceIncludesExcise:#,priceIncludesALExcise:#,modified:#}}", priceIncludesExcise, priceIncludesExcise, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateBrandId(String companyId, String shopId, String brandId) {
        coll.update("{companyId:#,shopId:#, brandId:#}", companyId, shopId, brandId).multi().with("{$set:{brandId:#,modified:#}}", null, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateShowInWidget(String companyId, String shopId, List<ObjectId> productObjectList, boolean showInWidget) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, productObjectList).multi().with("{$set:{showInWidget:#,modified:#}}", showInWidget, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateBrandInProduct(String companyId, String shopId, List<ObjectId> productObjectList, String brandId) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, productObjectList).multi().with("{$set:{brandId:#,modified:#}}", brandId, DateTime.now().getMillis());
    }


    @Override
    @Deprecated
    public WriteResult updateItemRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("products").update(query, field);
    }

    @Override
    public Iterable<BestSellingBrands> getTopSellingBrands(String companyId, String shopId, List<ObjectId> productIds, int start, int limit) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<BestSellingBrands> brandResults = coll.aggregate("{$match: {companyId:#,shopId:#,_id:{$in:#},brandId : { $exists : true, $ne: null }}}", companyId, shopId, productIds)
                .and("{$group: {_id:'$brandId',count:{$sum:1}}}")
                .and("{$sort: {count:-1}}")
                .and("{$limit:#}", limit)
                .options(aggregationOptions)
                .as(BestSellingBrands.class);

        ArrayList<BestSellingBrands> brandList = Lists.newArrayList((Iterable<BestSellingBrands>) brandResults);

        return brandList;
    }

    @Override
    public Iterable<SalesByProductCategory> getSalesByCategory(String companyId, String shopId, List<ObjectId> productIds, int start, int limit) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByProductCategory> salesByCategoryResult = coll.aggregate("{$match: {companyId:#,shopId:#,_id:{$in:#},categoryId : { $exists : true, $ne: null }}}", companyId, shopId, productIds)
                .and("{$group: {_id:'$categoryId',count:{$sum:1}}}")
                .and("{$sort: {count:-1}}")
                .and("{$limit:#}", limit)
                .options(aggregationOptions)
                .as(SalesByProductCategory.class);

        ArrayList<SalesByProductCategory> salesByCategoryList = Lists.newArrayList((Iterable<SalesByProductCategory>) salesByCategoryResult);

        return salesByCategoryList;
    }

    @Override
    public DateSearchResult<Product> findItemsByDate(String companyId, String shopId, long afterDate, long beforeDate, String projection) {
        Iterable<Product> items = coll.find("{companyId:#,shopId:#,deleted:false,modified:{$lt:#,$gt:#}}", companyId, shopId, beforeDate, afterDate).projection(projection).sort("{modified:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$lt:#,$gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<Product> searchResult = new DateSearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setBeforeDate(beforeDate);
        searchResult.setAfterDate(afterDate);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ProductResult> getProductsByVendorBrand(String companyId, String shopId, String vendorId, List<String> brands, int start, int limit) {
        Iterable<ProductResult> items = coll.find("{$and : [{companyId:#,shopId:#,deleted:false,active:true},{$or : [{brandId:{$in:#}}, {$or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}]}]}", companyId, shopId, brands, vendorId, vendorId).sort("{modified:-1}").skip(start).limit(limit).as(ProductResult.class);
        long count = coll.count("{$and : [{companyId:#,shopId:#,deleted:false,active:true},{$or : [{brandId:{$in:#}}, {$or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}]}]}", companyId, shopId, brands, vendorId, vendorId);

        SearchResult<ProductResult> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        return searchResult;
    }

    public SearchResult<Product> findActiveProductsByCategoryId(String companyId, String shopId, String categoryId, String sort, int start, int limit, Boolean active) {
        Iterable<Product> items = coll.find("{companyId:#,shopId:#,deleted:false,categoryId:#, active:#}", companyId, shopId, categoryId, active).sort(sort).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,categoryId:#, active:#}", companyId, shopId, categoryId, active);

        SearchResult<Product> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public Product findProductBySku(String companyId, String shopId, String sku) {
        Iterable<Product> iters = coll.find("{companyId:#,shopId:#,deleted:false,sku:#}", companyId, shopId,sku).as(entityClazz);
        for (Product product : iters) {
            return product;
        }
        return null;
    }

    @Override
    public List<Product> getProductForQB(String companyId, String shopId) {
        Iterable<Product> result = coll.find("{companyId:#, shopId:# }"
                , companyId, shopId).as(Product.class);
        return Lists.newArrayList(result);
    }

    @Override
    public void bulkUpdateFlowerTypeInProduct(String companyId, String shopId, List<ObjectId> productObjectList, String flowerType) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, productObjectList).multi().with("{$set:{flowerType:#,modified:#}}", flowerType, DateTime.now().getMillis());
    }

    @Override
    public long getProductsByName(String companyId, String shopId, String name, String vendorId, String categoryId, String flowerType, Product.CannabisType cannabisType) {
        long result = coll.count("{companyId:#,shopId:#,deleted:false,vendorId:#,name:#,categoryId:#,flowerType:#,cannabisType:#}", companyId, shopId, vendorId, name, categoryId, flowerType, cannabisType);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByTagsAndCategory(String companyId, String shopId, String categoryId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, tags, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, tags, afterDate, beforeDate);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, afterDate, beforeDate);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByStrainAndVendor(String companyId, String shopId, String categoryId, String vendorId, String strain, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, tags, afterDate, beforeDate, vendorId, vendorId).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, tags, afterDate, beforeDate, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, strain, afterDate, beforeDate, vendorId, vendorId).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, vendorId, strain, afterDate, beforeDate, vendorId, vendorId);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByCategoryAndVendorId(String companyId, String shopId, String categoryId, String vendorId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, tags, afterDate, beforeDate, vendorId, vendorId).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, tags, afterDate, beforeDate, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, afterDate, beforeDate, vendorId, vendorId).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, afterDate, beforeDate, vendorId, vendorId);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByCategoryAndStrain(String companyId, String shopId, String categoryId, String strain, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, strain, tags, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#},modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, strain, tags, afterDate, beforeDate);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, strain, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,showInWidget:true,deleted:false,category.active:true,modified :{$gt:#,$lt:#}}", companyId, shopId, categoryId, strain, afterDate, beforeDate);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByTags(String companyId, String shopId, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, tags, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, tags, afterDate, beforeDate);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, afterDate, beforeDate);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByStrainAndVendor(String companyId, String shopId, String strain, String vendor, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, tags, afterDate, beforeDate, vendor, vendor).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, tags, afterDate, beforeDate, vendor, vendor);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, afterDate, beforeDate, vendor, vendor).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, afterDate, beforeDate, vendor, vendor);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByTagsAndVendor(String companyId, String shopId, String vendor, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, tags, afterDate, beforeDate, vendor, vendor).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, tags, afterDate, beforeDate, vendor, vendor);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, afterDate, beforeDate, vendor, vendor).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, afterDate, beforeDate, vendor, vendor);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }

    @Override
    public <E extends Product> DateSearchResult<E> findProductsByTagsAndStrain(String companyId, String shopId, String strain, LinkedHashSet<String> tags, long afterDate, long beforeDate, Class<E> clazz) {
        Iterable<E> items = null;
        long count = 0;
        if (!tags.isEmpty()) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, strain, tags, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,tags: {$in:#},modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, strain, tags, afterDate, beforeDate);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, strain, afterDate, beforeDate).sort("{name:1}").as(clazz);
            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,showInWidget:true,deleted:false,modified :{$gt:#,$lt:#},category.active:true}", companyId, shopId, strain, afterDate, beforeDate);
        }
        DateSearchResult<E> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }


    @Override
    public List<Product> getProductsListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<Product> result = coll.find("{companyId:#, shopId:#,modified:{$lt:#, $gt:#}, active:true,qbItemRef: {$exists: false} }"
                , companyId, shopId, beforeDate, afterDate).as(Product.class);
        return Lists.newArrayList(result);
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findItemsByState(String companyId, String shopId, String sortOptions, int skip, int limit, Class<E> clazz, boolean status) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,active:#,deleted:false}", companyId, shopId, status).sort(sortOptions).skip(skip).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,active:#,deleted:false}", companyId, shopId, status);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void updateProductRef(String companyId, String id, String qbDesktopRef, String editSequence, String qbListId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(id)).with("{$set: {qbDesktopItemRef:#, editSequence:#, qbListId:#}}", qbDesktopRef, editSequence, qbListId);
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findProductsByPatternAndState(String companyId, String shopId, String sortOptions, String term, int start, int limit, Class<E> clazz, boolean state) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,active:#,deleted:false}, {$or: [{name:#}]}]}", companyId, shopId, state, pattern).sort(sortOptions).skip(start).limit(limit).as(clazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#,active:#,deleted:false}, {$or: [{name:#}]}]}", companyId, shopId, state, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryIdAndState(String companyId, String shopId, String categoryId, String sortOption, int start, int limit, Class<E> eClass, boolean state) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false,categoryId:#,active: #}", companyId, shopId, categoryId, state).sort(sortOption).skip(start).limit(limit).as(eClass);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,categoryId:#,active:#}", companyId, shopId, categoryId, state);

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> findProductsByCategoryIdAndTermAndState(String companyId, String shopId, String sortOption, String categoryId, String term, int start, int limit, Class<E> clazz, boolean state) {
        Pattern pattern = TextUtil.createPatternWithSpecialChars(term);
        Iterable<E> items = coll.find("{$and: [{companyId:#,shopId:#,categoryId:#,deleted:false,active:#}, {$or: [{name:#}]}]}", companyId, shopId, categoryId, state, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#,categoryId:#,deleted:false,active:#}, {$or: [{name:#}]}]}", companyId, shopId, categoryId, state, pattern);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<Product> getAllProductByCompany(String companyId) {
        Iterable<Product> items = coll.find("{companyId:#}", companyId).as(entityClazz);

        return items;
    }

    @Override
    public void bulkUpdateWeightPerUnit(String companyId, String shopId, List<ObjectId> productId, Product.WeightPerUnit weightPerUnit, BigDecimal customGrams, Product.CustomGramType customGramType) {
        coll.update("{companyId:#,shopId:#,_id:{$in:#}}", companyId, shopId, productId).multi().with("{$set:{weightPerUnit:#,customWeight:#,customGramType:#,modified:#}}", weightPerUnit, customGrams.doubleValue(), customGramType, DateTime.now().getMillis());
    }

    @Override
    public void updateProductPricing(String companyId, String productId, List<ProductPriceRange> priceRanges, List<ProductPriceBreak> priceBreaks, String pricingTemplateId, BigDecimal unitPrice) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(productId)).with("{$set:{priceRanges:#, priceBreaks:#, pricingTemplateId:#, unitPrice:#, modified:#}}", priceRanges, priceBreaks, pricingTemplateId, unitPrice.doubleValue(), DateTime.now().getMillis());
    }

    @Override
    public void removeProductPricing(String companyId, String shopId, String pricingTemplateId) {
        coll.update("{companyId:#, shopId:#,pricingTemplateId:#}", companyId, shopId, pricingTemplateId).with("{$set:{pricingTemplateId:#, modified:#}}", StringUtils.EMPTY, DateTime.now().getMillis());
    }

    @Override
    public void resetPricingTemplateByProductCategory(String companyId, String shopId, String categoryId, List<ObjectId> productIds) {
        coll.update("{companyId:#, shopId:#, categoryId:#, _id : {$in:#}}", companyId, shopId, categoryId, productIds).multi().with("{$set:{pricingTemplateId:#, modified:#}}", StringUtils.EMPTY, DateTime.now().getMillis());
    }

    @Override
    public MongoCursor<Product> findProductsByPricingTemplateId(String companyId, String shopId, String pricingTemplateId) {
        return coll.find("{companyId:#, shopId:#, pricingTemplateId:#}", companyId, shopId, pricingTemplateId).as(entityClazz);
    }

    @Override
    public void updateProductPrice(String companyId, String shopId, Product product) {
        coll.update("{ companyId:#,shopId:#,_id : #}", companyId, shopId, new ObjectId(product.getId())).multi().with("{ $set:{priceRanges:#,priceBreaks:#,unitPrice:#,modified:#}}", product.getPriceRanges(), product.getPriceBreaks(), product.getUnitPrice().doubleValue(), DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, Product> getLimitedProductViewAsMap(String companyId, String shopId) {
        HashMap<String, Product> productMap = new HashMap<>();
        Iterable<Product> products = coll.find("{companyId:#, shopId:#, deleted:false}", companyId, shopId).projection("{_id:1, name:1}").as(entityClazz);
        for (Product product : products) {
            productMap.put(product.getId(), product);
        }
        return productMap;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdStrainBrandSearch(String companyId, String shopId, String categoryId, String strain, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, strain, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, strain, pattern, tags);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId, strain, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId, strain, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdStrainBrand(String companyId, String shopId, String categoryId, String strain, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, strain, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, strain, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId, strain).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId, strain);
        }


        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdVendorBrandSearch(String companyId, String shopId, String categoryId, String vendorId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, pattern, tags, vendorId, vendorId);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdVendorBrand(String companyId, String shopId, String categoryId, String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, vendorId, vendorId);
        }


        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdStrainVendorBrandSearch(String companyId, String shopId, String categoryId, String strain, String vendorId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, pattern, tags, vendorId, vendorId);

        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryIdAndVendorIdAndBrand(String companyId, String shopId, String categoryId, String strain, String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, categoryId, brandId, strain, vendorId, vendorId);
        }


        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainBrandSearch(String companyId, String shopId, String strain, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, strain, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, strain, pattern, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId, strain, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId, strain, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainBrand(String companyId, String shopId, String strain, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, strain, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, strain, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId, strain).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId, strain);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByVendorIdBrandIdSearch(String companyId, String shopId, String vendorId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, pattern, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByVendorIdBrand(String companyId, String shopId, String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainVendorBrandSearch(String companyId, String shopId, String strain, String vendorId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, brandId, pattern, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, brandId, pattern, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,flowerType:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, brandId, pattern, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,flowerType:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, strain, brandId, pattern, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsStrainAndVendorBrand(String companyId, String shopId, String strain, String vendorId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, strain, tags, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, strain, tags, vendorId, vendorId);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, strain, vendorId, vendorId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,flowerType:#,showInWidget:true,deleted:false,category.active:true, $or:[{vendorId:#},{$and:[{secondaryVendors:{$exists:true}},{secondaryVendors:#}]}]}", companyId, shopId, brandId, strain, vendorId, vendorId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryAndBrandSearch(String companyId, String shopId, String categoryId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, pattern, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByCategoryAndBrand(String companyId, String shopId, String categoryId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, categoryId, brandId, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,categoryId:#,brandId:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, categoryId, brandId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByBrandSearch(String companyId, String shopId, String term, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, pattern, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, pattern, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId, pattern).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,name:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId, pattern);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public <E extends Product> SearchResult<E> findProductsByBrand(String companyId, String shopId, List<String> tags, String brandId, String sortOption, int start, int limit, Class<E> clazz) {
        Iterable<E> items;
        long count;
        if (tags.size() > 0) {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, tags).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true,tags: {$in:#}}", companyId, shopId, brandId, tags);
        } else {
            items = coll.find("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId).sort(sortOption).skip(start).limit(limit).as(clazz);

            count = coll.count("{companyId:#,shopId:#,active:true,brandId:#,showInWidget:true,deleted:false,category.active:true}", companyId, shopId, brandId);
        }

        SearchResult<E> productSearchResult = new SearchResult<>();
        productSearchResult.setSkip(start);
        productSearchResult.setLimit(limit);
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));

        return productSearchResult;
    }

    @Override
    public void bulkRemoveBrand(String companyId, String shopId, List<String> brandIds) {
        coll.update("{companyId:#,shopId:#, brandId: {$in:#}}", companyId, shopId, brandIds).multi().with("{$set:{brandId:#,modified:#}}", null, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<Product> findItemsWithDate(String companyId, String shopId, long startDate, long endDate, int skip, int limit, String sortOptions) {
        if (startDate < 0) startDate = 0;
        if (endDate <= 0) endDate = DateTime.now().getMillis();

        Iterable<Product> items = coll.find("{companyId:#,shopId:#,deleted:false,modified:{$gt:#, $lt:#}}", companyId, shopId, startDate, endDate).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,modified:{$gt:#, $lt:#}}", companyId, shopId, startDate, endDate);

        SearchResult<Product> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }



    @Override
    public <E extends Product> List<E> getQBExistProducts(String companyId, String shopId, int start, int limit, long startTime, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopItemRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, modified:{$gt:#}}", companyId, shopId, startTime).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void updateEditSequenceAndRef(String companyId, String shopId, String id, String qbListId, String editSequence, String qbDesktopRef) {
        coll.update("{companyId:#, shopId:#, _id:#, qbListId:#}", companyId, shopId, new ObjectId(id), qbListId).with("{$set: {qbDesktopRef:#, editSequence:#}}", qbDesktopRef, editSequence);
    }

    @Override
    public void updateEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef) {
        coll.update("{companyId:#, shopId:#, qbListId:#}", companyId, shopId, qbListId).with("{$set: {editSequence:#, qbDesktopRef:#}}", editSequence, desktopRef);
    }

    @Override
    public <E extends Product> List<E> getProductsLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopItemRef:{$exists: false}, errorTime:{$exists:false}, editSequence:{$exists: false}, qbListId:{$exists: false}}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Product> List<E> getNewProductsLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopItemRef:{$exists: false}, qbErrored:false, editSequence:{$exists: false}, qbListId:{$exists: false}}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Product> List<E> getProductsByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopItemRef:{$exists: false}, errorTime:{$exists:true}, modified:{$gt:#}}", companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void updateProductQbErrorAndTime(String companyID, String id, boolean qbErrored, long errorTime, boolean qbQuantitySynced, boolean qbQuantityErrored) {
        coll.update("{companyId:#,_id:#}", companyID, new ObjectId(id)).with("{$set: {qbErrored:#, errorTime:#, qbQuantitySynced:#, qbQuantityErrored:#, qbQuantityErrorTime:#}}", qbErrored, errorTime, qbQuantitySynced, qbQuantityErrored, DateTime.now().getMillis());
    }

    @Override
    public void updateQBQuantitySyncedStatus(String companyId, String shopId, List<ObjectId> productIds, Boolean status) {
        coll.update("{companyId:#, shopId:#, _id: {$in:#}}", companyId, shopId, productIds).multi().with("{$set: {qbQuantitySynced:#}}", status);
    }

    @Override
    public <E extends Product> List<E> getQBExistProducts(String companyId, List<ObjectId> productIds, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#,_id : {$in: #}, deleted:false, qbDesktopItemRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}}", companyId, productIds).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Product> List<E> getProductsLimitsWithoutSyncedStatus(String companyId, String shopId, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, qbQuantitySynced:{$exists: false}}", companyId, shopId).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Product> List<E> getProductsLimitsWithoutQuantitySynced(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopItemRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true},qbQuantitySynced:{$exists: false}}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Product> List<E> getNewProductsLimitsWithoutQuantitySynced(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopItemRef:{$exists: true}, editSequence:{$exists: true}, qbListId:{$exists: true}, qbQuantitySynced:false}", companyId, shopId).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends Product> List<E> getProductsByLimitsWithQbQuantityError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopItemRef:{$exists: true}, qbQuantityErrored:{$exists:true}, qbQuantityErrorTime:{$exists:true}, modified:{$gt:#}}", companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public void updateProductQbQuantityErrorAndTime(String companyID, String id, boolean qbQuantityErrored, long errorQuantityTime) {
        coll.update("{companyId:#,_id:#}", companyID, new ObjectId(id)).with("{$set: {qbQuantityErrored:#, qbQuantityErrorTime:#}}", qbQuantityErrored, errorQuantityTime);
    }

    @Override
    public void updateProductQbQuantitySynced(String companyID, String id, boolean qbQuantitySynced) {
        coll.update("{companyId:#, _id:#}", companyID, new ObjectId(id)).with("{$set: {qbQuantitySynced:#}}", qbQuantitySynced);
    }

    @Override
    public void updateComplianceId(String companyID, String id, String complianceId) {
        coll.update("{companyId:#, _id:#}", companyID, new ObjectId(id)).with("{$set: {complianceId:#}}", complianceId);
    }

    @Override
    public void hardRemoveQuickBookDataInProducts(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {qbDesktopItemRef:1,editSequence:1, qbListId:1, qbErrored:1, errorTime:1, qbQuantitySynced:1,qbQuantityErrored:1,qbQuantityErrorTime:1}}");
    }

    @Override
    public SearchResult<Product> listAllByShopAndTerm(String companyId, String shopId,  String searchTerm) {
        Iterable<Product> items;
        long count;

        if(StringUtils.isNotBlank(searchTerm)) {
            Pattern pattern = TextUtil.createPattern(searchTerm);
            items=  coll.find("{companyId:#,shopId: #, deleted:false, name:#}", companyId, shopId, pattern).sort("{name:1}").as(entityClazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false, name:#}", companyId, shopId, pattern);
        } else {
            items=  coll.find("{companyId:#,shopId: #, deleted:false}", companyId, shopId).sort("{name:1}").as(entityClazz);
            count = coll.count("{companyId:#,shopId:#,deleted:false}", companyId, shopId);
        }

        SearchResult<Product> productSearchResult = new SearchResult<>();
        productSearchResult.setTotal(count);
        productSearchResult.setValues(Lists.newArrayList(items));
        return productSearchResult;
    }

    @Override
    public HashMap<String, Product> getProductsWithIdsAndTerm(String companyId, String shopId, List<ObjectId> productIds, String searchTerm) {
        Iterable<Product> items;
        if(StringUtils.isNotBlank(searchTerm)) {
            Pattern pattern = TextUtil.createPattern(searchTerm);
            items=  coll.find("{companyId:#,shopId: # ,deleted:false, _id: {$in:#}, name:#}", companyId, shopId, productIds, pattern).sort("{name:1}").as(entityClazz);
        } else {
            items=  coll.find("{companyId:#,shopId: #,  deleted:false, _id: {$in:#}}", companyId, shopId, productIds).sort("{name:1}").as(entityClazz);
        }

     return  asMap(items);
    }


    @Override
    public void updateWmThreshold(String companyId, List<ObjectId> productIds, Double wmThreshold) {
        coll.update("{companyId:#, _id:{$in:#}}", companyId, productIds).multi().with("{$set: {wmThreshold:#, modified:#}}", wmThreshold, DateTime.now().getMillis());
    }

    @Override
    public void updateLastWMSyncTime(String companyId, List<ObjectId> productIds, long lastWPSyncTime) {
        // no need to update last modified
        coll.update("{companyId:#, _id:{$in:#}}", companyId, productIds).multi().with("{$set: {lastWMSyncTime:#}}", lastWPSyncTime);
    }

    @Override
    public void updateBrandsInProduct(String companyId, String shopId, LinkedHashSet<String> brandIds, String vendorId) {
        coll.update("{companyId:#, shopId:#, brandId:{$in:#}, vendorId:#}", companyId, shopId, brandIds, vendorId).multi().with("{$set: {brandId:#, modified:#}}", StringUtils.EMPTY, DateTime.now().getMillis());
    }

    @Override
    public void updateLeaflySyncTimeAndStatus(String companyId, Set<ObjectId> productIds, long lastLeaflySyncTime, Boolean lastLeaflySyncStatus) {
        // no need to update last modified
        coll.update("{companyId:#, _id:{$in:#}}", companyId, productIds).multi().with("{$set: {lastLeaflySyncTime:#, lastLeaflySyncStatus:#}}", lastLeaflySyncTime, lastLeaflySyncStatus);
    }

    @Override
    public Iterable<Product> listAfterLastLeaflySync(String companyId, List<ObjectId> ids, long lastSync) {
        return coll.find("{companyId:#,_id : {$in: #},$or:[{lastLeaflySyncTime:{$gt:#}},{lastLeaflySyncStatus:false}]}", companyId, ids, lastSync).as(entityClazz);
    }

}

