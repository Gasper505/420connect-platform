package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.ProductVersion;
import com.fourtwenty.core.domain.repositories.dispensary.ProductVersionRepository;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import javax.inject.Inject;

public class ProductVersionRepositoryImpl extends ShopBaseRepositoryImpl<ProductVersion> implements ProductVersionRepository {

    @Inject
    public ProductVersionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ProductVersion.class, mongoManager);
    }

    @Override
    public ProductVersion findLastProductVersionByProductId(String companyId,String shopId, String productId){
        Iterable<ProductVersion> iters = coll.find("{companyId:#,shopId:#,deleted:false,productId:#}", companyId, shopId,productId).sort("{created:-1}").limit(1).as(entityClazz);
        for (ProductVersion productVerion : iters) {
            return productVerion;
        }
        return null;
    }
}