package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;

/**
 * Created by mdo on 10/25/15.
 */
public class ProductWeightToleranceRepositoryImpl extends CompanyBaseRepositoryImpl<ProductWeightTolerance> implements ProductWeightToleranceRepository {

    @Inject
    public ProductWeightToleranceRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ProductWeightTolerance.class, mongoManager);
    }


    @Override
    public ProductWeightTolerance getToleranceForWeight(String companyId, ProductWeightTolerance.WeightKey weightKey) {
        Iterable<ProductWeightTolerance> tolerances = coll.find("{companyId:#,weightKey:#}", companyId, weightKey).sort("{modified:-1}").as(entityClazz);
        for (ProductWeightTolerance tolerance : tolerances) {
            return tolerance;
        }
        return null;
    }

    @Override
    public HashMap<String, ProductWeightTolerance> getActiveTolerancesAsMap(String companyId) {
        Iterable<ProductWeightTolerance> tolerances = coll.find("{companyId:#,enabled:true}", companyId).as(entityClazz);
        return asMap(tolerances);
    }

    @Override
    public void enableWeightTolerance(String weightToleranceId, boolean state) {
        coll.update("{_id:#}", new ObjectId(weightToleranceId)).with("{$set:{enabled:#, modified:#}}", state, DateTime.now().getMillis());
    }

    @Override
    public ProductWeightTolerance getToleranceByName(String companyId, String name) {
        return coll.findOne("{companyId:#, deleted:false, name:#}", companyId, name).as(entityClazz);
    }
}
