package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.loyalty.PromoUsage;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PromoUsageRepository;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by mdo on 1/3/17.
 */
public class PromoUsageRepositoryImpl extends ShopBaseRepositoryImpl<PromoUsage> implements PromoUsageRepository {

    @Inject
    public PromoUsageRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PromoUsage.class, mongoManager);
    }

    @Override
    public long countForMember(String companyId, String shopId, String promoId, String memberId) {
        return coll.count("{companyId:#,shopId:#,promoId:#,memberId:#}", companyId, shopId, promoId, memberId);
    }

    @Override
    public long countForPromo(String companyid, String shopId, String promoId) {
        return coll.count("{companyId:#,shopId:#,promoId:#}", companyid, shopId, promoId);
    }

    @Override
    public Iterable<PromoUsage> listForPromo(String companyid, String shopId, String promoId) {
        return coll.find("{companyId:#,shopId:#,promoId:#}", companyid, shopId, promoId).as(entityClazz);
    }

    @Override
    public PromoUsage getUserFor(String companyId, String shopId, String promoId, String memberId, String transId) {
        return coll.findOne("{companyId:#,shopId:#,promoId:#,memberId:#,transId:#}", companyId, shopId, promoId, memberId, transId).as(entityClazz);
    }

    @Override
    public PromoUsage getPromoUsage(String companyId, String shopId, String promoId, long startDate, long endDate) {
        return coll.findOne("{companyId:#,shopId:#,promoId:#, created:{$gt:#, $lt:#}}", companyId, shopId, promoId, startDate, endDate).as(entityClazz);
    }

    @Override
    public List<PromoUsage> getPromoUsages(String companyId, String shopId, long startDate, long endDate) {
        Iterable<PromoUsage> result = coll.find("{companyId:#,shopId:#,created:{$gt:#, $lt:#}}", companyId, shopId, startDate, endDate).as(entityClazz);
        return Lists.newArrayList(result.iterator());
    }

}
