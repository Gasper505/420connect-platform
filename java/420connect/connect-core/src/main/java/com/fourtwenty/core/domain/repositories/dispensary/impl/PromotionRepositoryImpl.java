package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 1/3/17.
 */
public class PromotionRepositoryImpl extends ShopBaseRepositoryImpl<Promotion> implements PromotionRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(PromotionRepositoryImpl.class);

    @Inject
    public PromotionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Promotion.class, mongoManager);
    }

    @Override
    public Promotion getPromotionByPromoCode(String companyId, String shopId, String promocode) {
        if (StringUtils.isBlank(promocode)) {
            return null;
        }
        Iterable<Promotion> promotions = coll.find("{companyId:#,shopId:#,deleted:false,promoCodes:#}", companyId, shopId, promocode).as(entityClazz);
        List<Promotion> promotionList = Lists.newArrayList(promotions);
        if (!promotionList.isEmpty()) {
            return promotionList.get(0);
        }
        return null;
    }

    @Override
    public List<Promotion> getPromotionByPromoCode(String companyId, String shopId, Set<String> promoCodes) {
        if (promoCodes == null || promoCodes.isEmpty()) {
            return Lists.newArrayList();
        }
        Iterable<Promotion> promotions = coll.find("{companyId:#,shopId:#,promoCodes:{$in:#},deleted:false}", companyId, shopId, promoCodes).as(entityClazz);
        List<Promotion> promotionList = Lists.newArrayList(promotions);
        if (!promotionList.isEmpty()) {
            return promotionList;
        }
        return Lists.newArrayList();
    }

    @Override
    public Promotion getPromotionByName(String companyId, String shopId, String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Iterable<Promotion> promotions = coll.find("{companyId:#,shopId:#,name:#,deleted:false}", companyId, shopId, name).as(entityClazz);
        List<Promotion> promotionList = Lists.newArrayList(promotions);
        if (promotionList.size() > 0) {
            return promotionList.get(0);
        }
        return null;
    }

    @Override
    public List<Promotion> getPromotions(String companyId, String shopId) {
        Iterable<Promotion> promotions = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(promotions.iterator());
    }

    @Override
    @Deprecated
    public DBCursor getPromotionsCursor(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("promotions").find(query, field);
    }

    /**
     * This method check for promotion with name except provided promotion
     *
     * @param companyId   : company id
     * @param shopId      : shop id
     * @param promotionId : promotion id
     * @param name        : name
     */
    @Override
    public Promotion getPromotionByNameExceptThis(String companyId, String shopId, String promotionId, String name) {
        Iterable<Promotion> promotions = coll.find("{companyId:#,shopId:#,name:#, _id:{$ne:#}}", companyId, shopId, name, new ObjectId(promotionId)).as(entityClazz);
        List<Promotion> promotionList = Lists.newArrayList(promotions);
        if (promotionList.size() > 0) {
            return promotionList.get(0);
        }
        return null;
    }
}
