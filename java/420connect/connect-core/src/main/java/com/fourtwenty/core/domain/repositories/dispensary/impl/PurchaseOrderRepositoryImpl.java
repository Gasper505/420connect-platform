package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.apache.commons.collections.CollectionUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * Created by decipher on 3/10/17 3:41 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class PurchaseOrderRepositoryImpl extends ShopBaseRepositoryImpl<PurchaseOrder> implements PurchaseOrderRepository {

    @Inject
    public PurchaseOrderRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PurchaseOrder.class, mongoManager);
    }

    /**
     * This method gets purchase order by companyId and status of PO
     *
     * @param companyId : company Id
     * @param status    : status of PO
     */
    @Override
    public SearchResult<PurchaseOrderItemResult> listAllByStatus(String companyId, String shopId, int skip, int limit, String sortOptions, String status, PurchaseOrder.CustomerType customerType) {
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrderIterable = null;
        long count = 0;
        if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY == customerType) {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#, purchaseOrderStatus:#,archive:false,deleted:false,customerType:#}", companyId, shopId, status, customerType).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:#,archive:false,deleted:false,customerType:#}", companyId, shopId, status, customerType);
        } else {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#, purchaseOrderStatus:#,archive:false,deleted:false, customerType:{$ne: # }}", companyId, shopId, status, PurchaseOrder.CustomerType.CUSTOMER_COMPANY).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:#,archive:false,deleted:false, customerType:{$ne: # }}", companyId, shopId, status, PurchaseOrder.CustomerType.CUSTOMER_COMPANY);
        }

        searchResult.setValues(Lists.newArrayList(purchaseOrderIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(String companyId, String shopId, String sortOptions, int skip, int limit, PurchaseOrder.CustomerType customerType) {
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrderIterable = null;
        long count = 0;
        if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY == customerType) {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#, purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false,customerType:#}", companyId, shopId, customerType).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false,customerType:#}", companyId, shopId, customerType);
        } else {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#, purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false, customerType:{$ne: # }}", companyId, shopId, PurchaseOrder.CustomerType.CUSTOMER_COMPANY).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false, customerType:{$ne: # }}", companyId, shopId, PurchaseOrder.CustomerType.CUSTOMER_COMPANY);
        }

        searchResult.setValues(Lists.newArrayList(purchaseOrderIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public PurchaseOrderItemResult getPOById(String companyId, String id) {
        PurchaseOrderItemResult purchaseOrderItemResult = null;
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }

        purchaseOrderItemResult = coll.findOne("{companyId: #,_id:#}", companyId, new ObjectId(id)).as(PurchaseOrderItemResult.class);
        return purchaseOrderItemResult;
    }

    @Override
    public PurchaseOrder getBOByPOId(String companyId, String id) {
        PurchaseOrder purchaseOrder = null;
        if (id == null || !ObjectId.isValid(id)) {
            return null;
        }

        purchaseOrder = coll.findOne("{companyId: #,parentPOId:#}", companyId, id).as(entityClazz);

        return purchaseOrder;
    }

    @Override
    public void updatePurchaseOrderStatus(String poId, PurchaseOrder.PurchaseOrderStatus status) {
        coll.update("{_id:#}", new ObjectId(poId)).with("{$set:{purchaseOrderStatus:#}}", status);
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> listAllArchivedPO(String companyId, String shopId, PurchaseOrder.CustomerType customerType, int skip, int limit, String sortOptions) {
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrderIterable = coll.find("{companyId:#,shopId:#,customerType:#,archive:true,deleted:false}", companyId, shopId, customerType).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
        long count = coll.count("{companyId:#,shopId:#,customerType:#,archive:true,deleted:false}", companyId, shopId, customerType);

        searchResult.setValues(Lists.newArrayList(purchaseOrderIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public Iterable<PurchaseOrderItemResult> getBracketSalesByCustomerType(String companyId, String shopId, String customerType, int size) {
        return coll.find("{companyId:#,shopId:#, purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false,customerType:#}", companyId, shopId, customerType).sort("{modified: -1}").limit(size).as(PurchaseOrderItemResult.class);
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> listAllByTerm(String companyId, String shopId, int start, int limit, String sortOptions, String searchTerm, PurchaseOrder.CustomerType customerType, List<String> vendorIds) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrder = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,customerType:#},{$or:[{purchaseOrderStatus:#},{poPaymentOptions:#},{poType:#},{poNumber:#},{parentPOId:#},{vendorId:{$in:#}}]}]}", companyId, shopId, customerType, pattern, pattern, pattern, pattern, pattern, vendorIds).sort(sortOptions).skip(start).limit(limit).as(PurchaseOrderItemResult.class);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,customerType:#},{$or:[{purchaseOrderStatus:#},{poPaymentOptions:#},{poType:#},{poNumber:#},{parentPOId:#},{vendorId:{$in:#}}]}]}", companyId, shopId, customerType, pattern, pattern, pattern, pattern, pattern, vendorIds);
        searchResult.setValues(Lists.newArrayList(purchaseOrder));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> listAllByTermWithStatus(String companyId, String shopId, int start, int limit, String sortOptions, String searchTerm, PurchaseOrder.CustomerType customerType, String status,  List<String> vendorIds) {
        Pattern pattern = TextUtil.createPattern(searchTerm);
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrder = coll.find("{$and: [{companyId:#,shopId:#,deleted:false,customerType:#,purchaseOrderStatus:#,archive:false},{$or:[{purchaseOrderStatus:#},{poPaymentOptions:#},{poType:#},{poNumber:#},{parentPOId:#},{vendorId:{$in:#}}]}]}", companyId, shopId, customerType, status, pattern, pattern, pattern, pattern, pattern, vendorIds).sort(sortOptions).skip(start).limit(limit).as(PurchaseOrderItemResult.class);
        long count = coll.count("{$and: [{companyId:#,shopId:#,deleted:false,customerType:#,purchaseOrderStatus:#,archive:false},{$or:[{purchaseOrderStatus:#},{poPaymentOptions:#},{poType:#},{poNumber:#},{parentPOId:#},{vendorId:{$in:#}}]}]}", companyId, shopId, customerType, status, pattern, pattern, pattern, pattern, pattern, vendorIds);

        searchResult.setValues(Lists.newArrayList(purchaseOrder));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> listAllByVendorId(String companyId, String shopId, int start, int limit, String vendorId, String sortOptions) {
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrder = coll.find("{companyId:#,shopId:#,deleted:false,vendorId:#}", companyId, shopId, vendorId).sort(sortOptions).skip(start).limit(limit).as(PurchaseOrderItemResult.class);
        long count = coll.count("{companyId:#,shopId:#,deleted:false,vendorId:#}", companyId, shopId, vendorId);

        searchResult.setValues(Lists.newArrayList(purchaseOrder));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public PurchaseOrder getPOByPOId(String companyId, String shopId, String poId) {
        PurchaseOrder purchaseOrder = coll.findOne("{companyId:#,shopId:#,deleted:false,_id:#}", companyId, shopId, new ObjectId(poId)).as(entityClazz);
        return purchaseOrder;
    }

    @Override
    public Iterable<PurchaseOrder> getAllPurchaseOrderById(String companyId, String shopId, List<ObjectId> purchaseOrderIds) {
        return coll.find("{companyId:#,shopId:#,deleted:false,_id:{$in:#}}", companyId, shopId, purchaseOrderIds).as(entityClazz);
    }

    @Override
    @Deprecated
    public WriteResult updatePurchaseOrderRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("purchaseorders").update(query, field);
    }

    @Override
    public <E extends ShopBaseModel> DateSearchResult<E> findItemsWithDate(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz, String projection, List<PurchaseOrder.PurchaseOrderStatus> status) {

        Iterable<E> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#},purchaseOrderStatus:{$in:#}}", companyId, shopId, beforeDate, afterDate, status).projection(projection).sort("{modified:-1}").as(clazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$lt:#, $gt:#},purchaseOrderStatus:{$in:#}}", companyId, shopId, beforeDate, afterDate, status);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<PurchaseOrder> getPurchaseOrderByStatus(String companyId, String shopId, List<PurchaseOrder.PurchaseOrderStatus> status, PurchaseOrder.CustomerType customerType, long afterDate, long beforeDate) {
        return coll.find("{companyId:#, shopId:#, modified:{$lt:#, $gt:#}, purchaseOrderStatus:{$in:#}, customerType:#}", companyId, shopId, beforeDate, afterDate, status, customerType).sort("{modified:-1}").as(entityClazz);
    }

    @Override
    public Iterable<PurchaseOrder> getPurchaseOrderByStatusPurchaseDate(String companyId, String shopId, List<PurchaseOrder.PurchaseOrderStatus> status, PurchaseOrder.CustomerType customerType, long afterDate, long beforeDate) {
        return coll.find("{companyId:#, shopId:#, purchaseOrderDate:{$lt:#, $gt:#}, purchaseOrderStatus:{$in:#}, customerType:#}", companyId, shopId, beforeDate, afterDate, status, customerType).sort("{purchaseOrderDate:1}").as(entityClazz);
    }

    @Override
    public <E extends ShopBaseModel> SearchResult<E> listAllArchiveByTerm(String companyId, String shopId, PurchaseOrder.CustomerType customerType, int start, int limit, String sortOptions, String term, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> purchaseOrderIterable = coll.find("{$and:[{companyId:#,shopId:#,customerType:#,archive:true,deleted:false},{$or:[{purchaseOrderStatus:#},{poPaymentOptions:#},{poType:#},{poNumber:#},{parentPOId:#}]}]}", companyId, shopId, customerType, pattern, pattern, pattern, pattern, pattern).skip(start).limit(limit).sort(sortOptions).as(clazz);
        long count = coll.count("{$and:[{companyId:#,shopId:#,customerType:#,archive:true,deleted:false},{$or:[{purchaseOrderStatus:#},{poPaymentOptions:#},{poType:#},{poNumber:#},{parentPOId:#}]}]}", companyId, shopId, customerType, pattern, pattern, pattern, pattern, pattern);

        searchResult.setValues(Lists.newArrayList(purchaseOrderIterable));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<PurchaseOrder> getAllPurchaseOrderByProductId(String companyId, String shopId, String sortOptions, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<PurchaseOrder> items = coll.aggregate("{$match:{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel', 'Decline'] },archive:false,deleted:false}}", companyId, shopId)
                .and("{$match: {poProductRequestList : { $elemMatch: {productId: {$in:#}}}}}", productIds)
                .options(aggregationOptions)
                .as(entityClazz);
        SearchResult<PurchaseOrder> purchaseOrderSearchResult = new SearchResult<>();
        purchaseOrderSearchResult.setValues(Lists.newArrayList((Iterable<PurchaseOrder>) items));
        return purchaseOrderSearchResult;
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrdersListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<PurchaseOrder> result = coll.find("{companyId:#, shopId:#,modified:{$lt:#, $gt:#}, active:true,qbPurchaseOrderRef: {$exists: false} }"
                , companyId, shopId, beforeDate, afterDate).as(PurchaseOrder.class);
        return Lists.newArrayList(result);
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrderByDate(String companyId, String shopId, String sortOptions, int skip, int limit, PurchaseOrder.CustomerType customerType, long beforeDate, long afterDate) {
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrderIterable = null;
        long count = 0;
        if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY == customerType) {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#, purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false,customerType:#,modified:{$lt:#, $gt:#}}", companyId, shopId, customerType, afterDate, beforeDate).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false,customerType:#,modified:{$lt:#, $gt:#}}", companyId, shopId, customerType, afterDate, beforeDate);
        } else {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#, purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false, customerType:{$ne: # },modified:{$lt:#, $gt:#}}", companyId, shopId, PurchaseOrder.CustomerType.CUSTOMER_COMPANY, afterDate, beforeDate).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false, customerType:{$ne: # },modified:{$lt:#, $gt:#}}", companyId, shopId, PurchaseOrder.CustomerType.CUSTOMER_COMPANY, afterDate, beforeDate);
        }

        searchResult.setValues(Lists.newArrayList(purchaseOrderIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }


    @Override
    public Iterable<PurchaseOrder> getPurchasOrderByStatus(String companyId, String shopId) {
        return coll.find("{companyId:#, shopId:#, ,purchaseOrderStatus: { $nin: ['Closed', 'Cancel','Decline'] } }", companyId, shopId).as(PurchaseOrder.class);

    }

    @Override
    public SearchResult<PurchaseOrder> getPurchaseOrdersByProductId(String companyId, String shopId, String productId, int start, int limit, String sortOptions) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<PurchaseOrder> items = coll.aggregate("{$match:{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false}}", companyId, shopId)
                .and("{$match: {poProductRequestList.productId:#}}", productId)
                .options(aggregationOptions)
                .as(entityClazz);
        long count = 0;
        count = coll.count("{companyId:#,shopId:#,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel'] },archive:false,deleted:false,poProductRequestList.productId:#}", companyId, shopId, productId);

        SearchResult<PurchaseOrder> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList((Iterable<PurchaseOrder>) items));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends PurchaseOrder> E getPurchaseOrderByNo(String companyId, String shopId, String poNumber, Class<E> clazz) {
        return coll.findOne("{companyId: #, shopId:#, poNumber:#}", companyId, shopId, poNumber).as(clazz);
    }

    @Override
    public <E extends PurchaseOrder> SearchResult<E> getLimitedPurchaseOrderWithoutQbDesktopRef(String companyId, String shopId, long syncTime, int start, int limit, Class<E> clazz) {
        Iterable<E> items  = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopPurchaseOrderRef:{$exists: false}, qbErrored:false, modified:{$gt:#}}", companyId, shopId, syncTime)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public <E extends PurchaseOrder> SearchResult<E> getLimitedPurchaseOrderWithoutQbDesktopRef(String companyId, String shopId, long startDate, long endDate, Class<E> clazz) {
        Iterable<E> items  = coll.find("{companyId:#, shopId:#, deleted:false, modified: {$gt:#, $lt:#}, qbDesktopPurchaseOrderRef:{$exists: false}, qbErrored:false}", companyId, shopId, startDate, endDate)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        return results;
    }

    @Override
    public void updateQbDesktopPurchaseOrderRef(String companyId, String shopId, String poNumber, String editSequence, String listId) {
        coll.update("{companyId:#, shopId:#, poNumber:#}", companyId, shopId, poNumber).with("{$set:{qbDesktopPurchaseOrderRef:#, editSequence:#, qbListId:#, modified:#}}", poNumber, editSequence, listId, DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, PurchaseOrder> listAsMapByPoNumber(String companyId, List<String> poNumbers) {
        Iterable<PurchaseOrder> purchaseOrders = coll.find("{companyId:#, poNumber:{$in:#}}", companyId, poNumbers).as(entityClazz);

        HashMap<String, PurchaseOrder> poMap = new HashMap<>();

        for (PurchaseOrder order : purchaseOrders) {
            poMap.put(order.getPoNumber(), order);
        }

        return poMap;
    }

    @Override
    public void updatePurchaseOrderQbErrorAndTime(String companyId, String refNo, Boolean qbErrored, long errorTime) {
        coll.update("{companyId:#, poNumber:#}", companyId, refNo).with("{$set: {qbErrored:#, errorTime:#}}", qbErrored, errorTime);
    }

    @Override
    public void updateQbDesktopPurchaseOrderBillRef(String companyId, String shopId, String id, boolean billRef) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(id)).with("{$set: {qbDesktopBillRef:#}}",billRef);
    }

    @Override
    public <E extends PurchaseOrder> List<E> getPOByLimitsWithQBError(String companyId, String shopId, Long errorTime, Class<E> clazz) {
        Iterable<E> poList =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopPurchaseOrderRef:{$exists: false}, qbErrored:true, modified:{$gt:#}}",
                companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(poList);
    }

    @Override
    public <E extends PurchaseOrder> DateSearchResult<E> findItemsWithDateAndLimit(String companyId, String shopId, long afterDate, long beforeDate, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort("{modified:-1}").skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public void updateRecievedDate(String purchaseOrderId , long receivedDate) {
        coll.update("{_id:#}",new ObjectId(purchaseOrderId)).with("{$set: {receivedDate:#, modified:#}}", receivedDate, DateTime.now().getMillis());
    }

    @Override
    public <E extends PurchaseOrder> SearchResult<E> getPOByLimitsWithoutBillRef(String companyId, String shopId, long syncTime, int start, int limit, Class<E> clazz) {
        Iterable<E> items  = coll.find("{companyId:#, shopId:#, deleted:false, purchaseOrderStatus:{ $in : ['Closed']}, qbDesktopPurchaseOrderRef:{$exists: true}, qbDesktopBillRef : false, created:{$gt:#}}", companyId, shopId, syncTime)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public long countPOByAdjustmentId(String companyId, String shopId, String adjustmentId) {
        return coll.count("{companyId:#, shopId:#, deleted:false, adjustmentInfoList.adjustmentId:#}", companyId, shopId, adjustmentId);
    }

    @Override
    public <E extends PurchaseOrder> List<E> getPurchaseOrderByLimitsWithDateTime(String companyId, String shopId, long startDate, long endDate, Class<E> clazz) {
        Iterable<E> items =  coll.find("{companyId:#, shopId:#, deleted:false, purchaseOrderStatus:{ $in : ['Closed']}, poType:#, qbDesktopPurchaseOrderRef:{$exists:false}, modified:{$gt:#, $lt:#}}",
                companyId, shopId, PurchaseOrder.POType.Normal, startDate, endDate)
                .sort("{processedTime: -1}")
                .as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends PurchaseOrder> List<E> getQBExistPurchaseOrders(String companyId, String shopId, int start, int limit, long startTime, Class<E> clazz) {
        Iterable<E> pos = coll.find("{companyId:#,shopId:#,deleted:false, qbDesktopPurchaseOrderRef:{$exists: true}, editSequence:{$exists: true}, modified:{$gt:#}}", companyId, shopId, startTime).skip(start).limit(limit).as(clazz);
        return Lists.newArrayList(pos);
    }

    @Override
    public Map<String, PurchaseOrder> listAsMapByPONumber(String companyId, List<String> poNumbers) {
        Iterable<PurchaseOrder> pos = coll.find("{companyId:#,poNumber : {$in: #}}", companyId, poNumbers).as(entityClazz);
        return asMap(pos);
    }

    @Override
    public PurchaseOrder getPOByPONumber(String companyId, String poNumber) {
        return coll.findOne("{companyId:#,poNumber:#,deleted:false}", companyId, poNumber).as(entityClazz);
    }

    @Override
    public PurchaseOrder updatePurchaseOrder(String companyId, String id, PurchaseOrder purchaseOrder) {
        coll.update("{companyId:#,_id: #}", companyId, new ObjectId(id)).with(purchaseOrder);
        return purchaseOrder;
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrderWithSorting(String companyId, String shopId, String sortOptions, int skip, int limit, PurchaseOrder.CustomerType customerType) {
        SearchResult<PurchaseOrderItemResult> searchResult = new SearchResult<>();
        Iterable<PurchaseOrderItemResult> purchaseOrderIterable = null;
        long count = 0;
        if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY == customerType) {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#,archive:false,deleted:false,customerType:#}", companyId, shopId, customerType).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,archive:false,deleted:false,customerType:#}", companyId, shopId, customerType);
        } else {
            purchaseOrderIterable = coll.find("{companyId:#,shopId:#,archive:false,deleted:false, customerType:{$ne: # }}", companyId, shopId, PurchaseOrder.CustomerType.CUSTOMER_COMPANY).skip(skip).limit(limit).sort(sortOptions).as(PurchaseOrderItemResult.class);
            count = coll.count("{companyId:#,shopId:#,archive:false,deleted:false, customerType:{$ne: # }}", companyId, shopId, PurchaseOrder.CustomerType.CUSTOMER_COMPANY);
        }

        searchResult.setValues(Lists.newArrayList(purchaseOrderIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }


    @Override
    public Iterable<PurchaseOrder> getPurchaseOrderByStatusWithPODate(String companyId, String shopId, List<PurchaseOrder.PurchaseOrderStatus> status, PurchaseOrder.CustomerType customerType, long afterDate, long beforeDate) {
        return coll.find("{companyId:#, shopId:#, created:{$lt:#, $gt:#}, purchaseOrderStatus:{$in:#}, customerType:#}", companyId, shopId, beforeDate, afterDate, status, customerType).sort("{created: -1}").as(entityClazz);
    }


    @Override
    public long countPurchaseOrderByProductId(String companyId, String shopId, List<String> productIds) {
        return coll.count("{companyId:#, shopId:#, deleted:false,purchaseOrderStatus:{ $nin: ['Closed', 'Cancel', 'Decline'] },archive:false, poProductRequestList.productId:{$in:#}}", companyId, shopId, productIds);
    }
    @Override
    public void hardRemoveQuickBookDataInPOs(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {qbDesktopPurchaseOrderRef:1,editSequence:1, qbDesktopBillRef:1, qbErrored:1, errorTime:1}}");
    }
    @Override
    public void updatePurchaseOrderStatus(String companyId, String purchaseOrderId, PurchaseOrder.PurchaseOrderStatus status, PurchaseOrder.PurchaseOrderStatus purchaseOrderPreviousStatus) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(purchaseOrderId)).with("{$set: {purchaseOrderStatus:#, purchaseOrderPreviousStatus:#, modified:#}}", status, purchaseOrderPreviousStatus, DateTime.now().getMillis());
    }

    @Override
    public long countActivePurchaseOrderByVendor(String companyId, String vendorId) {
        ArrayList<PurchaseOrder.PurchaseOrderStatus> purchaseOrderStatuses = Lists.newArrayList(PurchaseOrder.PurchaseOrderStatus.Closed, PurchaseOrder.PurchaseOrderStatus.Cancel, PurchaseOrder.PurchaseOrderStatus.Decline);
        return coll.count("{companyId:#, vendorId:#, deleted:false, archive:false, purchaseOrderStatus:{$nin:#}}", companyId, vendorId, purchaseOrderStatuses);
    }

    @Override
    public <E extends BaseModel> List<E> getPurchaseOrderByStatus(String companyId, String shopId, PurchaseOrder.PurchaseOrderStatus status, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#, shopId:#, deleted:false, archive:false, purchaseOrderStatus: #}", companyId, shopId, status).sort("{modified:-1}").as(clazz);
        return Lists.newArrayList(items);
    }

    @Override
    public <E extends BaseModel> List<E> getPOByVendorAndProductStatus(String companyId, String shopId, List<String> vendorIds, List<String> productIds, PurchaseOrder.PurchaseOrderStatus status, Class<E> clazz) {
        Iterable<E> items;
        if (CollectionUtils.isNotEmpty(vendorIds) && CollectionUtils.isNotEmpty(productIds)) {
            items = coll.find("{companyId:#, shopId:#, deleted:false, archive:false, purchaseOrderStatus: #, vendorId:{$in:#}, poProductRequestList.productId:{$in:#}}", companyId, shopId, status, vendorIds, productIds).sort("{modified:-1}").as(clazz);
        } else if (CollectionUtils.isEmpty(productIds)) {
            items = coll.find("{companyId:#, shopId:#, deleted:false, archive:false, purchaseOrderStatus: #, vendorId:{$in:#}}", companyId, shopId, status, vendorIds).sort("{modified:-1}").as(clazz);
        } else {
            items = coll.find("{companyId:#, shopId:#, deleted:false, archive:false, purchaseOrderStatus: #, poProductRequestList.productId:{$in:#}}", companyId, shopId, status, productIds).sort("{modified:-1}").as(clazz);
        }
        return Lists.newArrayList(items);
    }
}
