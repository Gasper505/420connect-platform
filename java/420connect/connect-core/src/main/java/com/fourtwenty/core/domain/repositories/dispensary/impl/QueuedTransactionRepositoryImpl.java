package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.QueuedTransactionRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 4/30/16.
 */
public class QueuedTransactionRepositoryImpl extends ShopBaseRepositoryImpl<QueuedTransaction> implements QueuedTransactionRepository {

    @Inject
    public QueuedTransactionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(QueuedTransaction.class, mongoManager);
    }


    @Override
    public long getCountStatus(String companyId, String shopId, QueuedTransaction.QueueStatus queueStatus) {
        return coll.count("{companyId:#,shopId:#,status:#}", companyId, shopId, queueStatus);
    }

    @Override
    public Iterable<QueuedTransaction> getQueuedTransactions(QueuedTransaction.QueueStatus queueStatus) {
        return coll.find("{status:#}", queueStatus).as(entityClazz);
    }

    @Override
    public Iterable<QueuedTransaction> getQueuedTransactions(QueuedTransaction.QueueStatus queueStatus, long requestTime) {
        return coll.find("{status:#, requestTime: {$lt:#}}", queueStatus, requestTime).as(entityClazz);
    }

    @Override
    public long getQueuedTransactionsCount(QueuedTransaction.QueueStatus queueStatus) {
        return coll.count("{status:#}", queueStatus);
    }

    @Override
    public long getQueuedTransactionsCount(QueuedTransaction.QueueStatus queueStatus, long requestTime) {
        return coll.count("{status:#, requestTime: {$lt:#}}", queueStatus, requestTime);
    }

    @Override
    public void updateRequestTime(String queuedTransactionId) {
        coll.update(new ObjectId(queuedTransactionId)).with("{$set: {requestTime:#}}", DateTime.now().getMillis());
    }

    @Override
    public QueuedTransaction getRecentCompletedTransaction(String companyId, String shopId, String transactionId) {
        Iterable<QueuedTransaction> iters = coll.find("{companyId:#,shopId:#,transactionId:#,status:#}", companyId, shopId, transactionId, QueuedTransaction.QueueStatus.Completed).sort("{modified:-1}").as(entityClazz);
        for (QueuedTransaction job : iters) {
            return job;
        }
        return null;
    }

    @Override
    public QueuedTransaction getRecentQueuedTransaction(String companyId, String shopId, String transactionId) {
        Iterable<QueuedTransaction> iters = coll.find("{companyId:#,shopId:#,transactionId:#}", companyId, shopId, transactionId).sort("{requestTime:-1}").as(entityClazz);
        for (QueuedTransaction job : iters) {
            return job;
        }
        return null;
    }

    @Override
    public QueuedTransaction getRecentQueuedTransactionModified(String companyId, String shopId, String transactionId) {
        Iterable<QueuedTransaction> iters = coll.find("{companyId:#,shopId:#,transactionId:#}", companyId, shopId, transactionId).sort("{modified:-1}").as(entityClazz);
        for (QueuedTransaction job : iters) {
            return job;
        }
        return null;
    }

    @Override
    public List<QueuedTransaction> getQueuedTransactionByManifest(String companyId, String shopId, String invoiceId, String manifestId, String sortOptions) {
        Iterable<QueuedTransaction> result = coll.find("{companyId:#,shopId:#,invoiceId:#,shippingManifestId:#}", companyId, shopId, invoiceId, manifestId).sort(sortOptions).as(entityClazz);
        return Lists.newArrayList(result);
    }

    @Override
    public SearchResult<QueuedTransaction> getQueuedTransactionsBySourceId(String companyId, String shopId, String transactionId) {
        Iterable<QueuedTransaction> iters = coll.find("{companyId:#,shopId:#,transactionId:#}", companyId, shopId, transactionId).sort("{modified:-1}").as(entityClazz);
        long count = coll.count("{companyId:#, shopId:#, transactionId:#}", companyId, shopId, transactionId);

        SearchResult<QueuedTransaction> searchResult = new SearchResult<>();
        ArrayList<QueuedTransaction> queuedTransactions = Lists.newArrayList(iters);

        searchResult.setValues(queuedTransactions);
        searchResult.setSkip(0);
        searchResult.setLimit(queuedTransactions.size());
        searchResult.setTotal(count);
        return searchResult;
    }
}
