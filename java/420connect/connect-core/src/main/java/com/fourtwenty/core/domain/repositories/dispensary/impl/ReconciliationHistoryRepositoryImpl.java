package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.product.ReconciliationHistory;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ReconciliationHistoryRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ReconciliationHistoryResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;

public class ReconciliationHistoryRepositoryImpl extends ShopBaseRepositoryImpl<ReconciliationHistory> implements ReconciliationHistoryRepository {
    @Inject
    public ReconciliationHistoryRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ReconciliationHistory.class, mongoManager);
    }

    @Override
    public SearchResult<ReconciliationHistoryResult> getAllReconciliationHistory(String companyId, String shopId, String sortOptions, int start, int limit) {
        SearchResult<ReconciliationHistoryResult> searchResult = new SearchResult<>();
        Iterable<ReconciliationHistoryResult> reconciliationHistoryIterable = coll.find("{companyId:#,shopId:#}", companyId, shopId).skip(start).limit(limit).sort(sortOptions).as(ReconciliationHistoryResult.class);
        Long count = coll.count("{companyId:#,shopId:#}", companyId, shopId);

        searchResult.setValues(Lists.newArrayList(reconciliationHistoryIterable));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public <E extends ReconciliationHistory> E getReconciliationHistory(String companyId, String shopId, String historyId, Class<E> clazz) {
        if (historyId == null || !ObjectId.isValid(historyId)) {
            return null;
        }
        return coll.findOne("{companyId: #,shopId:#,_id:#}", companyId, shopId, new ObjectId(historyId)).as(clazz);
    }

    @Override
    public Iterable<ReconciliationHistory> getReconciliationHistoryByDateRange(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, created: {$gt:#, $lt:#}}",
                companyId, shopId, startDate, endDate)
                .sort("{created: -1}")
                .as(entityClazz);
    }

    @Override
    public <E extends ReconciliationHistory> E getReconciliationHistoryByNo(String companyId, String shopId, Long reconciliationNo, Class<E> clazz) {
        return coll.findOne("{companyId: #,shopId:#,requestNo:#}", companyId, shopId, reconciliationNo).as(clazz);
    }

    @Override
    public ReconciliationHistory getLastReconciliationHistory(String companyId, String shopId) {
        Iterable<ReconciliationHistory> histories = coll.find("{companyId:#, shopId:#}", companyId, shopId).sort("{created: -1}").limit(1).as(entityClazz);

        ArrayList<ReconciliationHistory> reconciliationHistories = Lists.newArrayList(histories);
        if (reconciliationHistories.size() > 0) {
            return reconciliationHistories.get(0);
        }
        return null;
    }
}
