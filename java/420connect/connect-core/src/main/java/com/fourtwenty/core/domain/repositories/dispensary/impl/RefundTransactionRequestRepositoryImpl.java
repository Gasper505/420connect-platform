package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.transaction.RefundTransactionRequest;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.RefundTransactionRequestRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 6/12/18.
 */
public class RefundTransactionRequestRepositoryImpl extends ShopBaseRepositoryImpl<RefundTransactionRequest> implements RefundTransactionRequestRepository {

    @Inject
    public RefundTransactionRequestRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(RefundTransactionRequest.class, mongoManager);
    }
}
