package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ReportTrackRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;

public class ReportTrackRepositoryImpl extends ShopBaseRepositoryImpl<ReportTrack> implements ReportTrackRepository {

    @Inject
    public ReportTrackRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ReportTrack.class, mongoManager);
    }

    @Override
    public ReportTrack getReportTrackByType(String companyId, String shopId, ReportType type) {
        return coll.findOne("{companyId:#, shopId:#, reportType:#}", companyId, shopId, type).as(entityClazz);
    }

    @Override
    public List<ReportTrack> getFrequentlyUsedReport(String companyId, String shopId, int limit, String sortOptions, List<ReportType> dashboardNames) {
        Iterable<ReportTrack> tracks = coll.find("{companyId:#,shopId:#,reportType:{$nin:#}}", companyId, shopId, dashboardNames).sort(sortOptions).limit(limit).as(entityClazz);
        return Lists.newArrayList(tracks);
    }
}
