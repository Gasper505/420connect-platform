package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 6/15/16.
 */
public class RoleRepositoryImpl extends CompanyBaseRepositoryImpl<Role> implements RoleRepository {
    @Inject
    public RoleRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Role.class, mongoManager);
    }

    @Override
    public Role getRoleByName(String companyId, String roleName) {
        return coll.findOne("{companyId:#,name:#}", companyId, roleName).as(entityClazz);
    }

    @Override
    public Iterable<Role> getRoleByName(String roleName) {
        return coll.find("{name:#}", roleName).as(entityClazz);
    }
}
