package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ShipmentBillRequestResult;
import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class ShipmentBillRepositoryImpl extends ShopBaseRepositoryImpl<ShipmentBill> implements ShipmentBillRepository {


    @Inject
    public ShipmentBillRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ShipmentBill.class, mongoManager);
    }


    @Override
    public ShipmentBill getShipmentBillById(final String shipmentId) {

        return coll.findOne("{_id:#}", shipmentId).as(entityClazz);
    }

    @Override
    public ShipmentBill getShipmentBillByPoId(String poId) {
        return coll.findOne("{purchaseOrderId:#}", poId).as(entityClazz);
    }

    @Override
    public SearchResult<ShipmentBillRequestResult> getAllShipmentBills(final String companyId, final String shopId, final String sortOptions, final int start, final int limit, PurchaseOrder.CustomerType customerType) {
        SearchResult<ShipmentBillRequestResult> searchResult = new SearchResult<>();
        Iterable<ShipmentBillRequestResult> shipmentBillResultIterable = null;
        long count = 0;
        if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY == customerType) {
            shipmentBillResultIterable = coll.find("{companyId:#,shopId:#,archive:false,deleted:false,customerType:#}", companyId, shopId, customerType).skip(start).limit(limit).sort(sortOptions).as(ShipmentBillRequestResult.class);
            count = coll.count("{companyId:#,shopId:#,archive:false,deleted:false,customerType:#}", companyId, shopId, customerType);
        } else {
            shipmentBillResultIterable = coll.find("{$and: [{companyId:#,shopId:#,archive:false,deleted:false}]}, {$or:[{customerType:{$exists : false}},{customerType:#}]}", companyId, shopId, customerType).skip(start).limit(limit).sort(sortOptions).as(ShipmentBillRequestResult.class);
            count = coll.count("{$and: [{companyId:#,shopId:#,archive:false,deleted:false}]}, {$or:[{customerType:{$exists : false}},{customerType:#}]}", companyId, shopId, customerType);
        }

        searchResult.setValues(Lists.newArrayList(shipmentBillResultIterable));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ShipmentBillRequestResult> getArchivedShipmentBills(String companyId, String shopId, String sortOption, int skip, int limit) {
        SearchResult<ShipmentBillRequestResult> searchResult = new SearchResult<>();
        Iterable<ShipmentBillRequestResult> requestResultIterable = coll.find("{companyId:#,shopId:#,archive:true,deleted:false}", companyId, shopId).skip(skip).limit(limit).sort(sortOption).as(ShipmentBillRequestResult.class);
        long count = coll.count("{companyId:#,shopId:#,archive:true,deleted:false}", companyId, shopId);

        searchResult.setValues(Lists.newArrayList(requestResultIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public Iterable<ShipmentBill> getShipmentBills(String companyId, String shopId, Long startDate, Long endDate, String sortOptions) {
        return coll.find("{companyId:#,shopId:#,created: {$gt: #, $lt:#}}", companyId, shopId, startDate, endDate).sort(sortOptions).as(ShipmentBill.class);
    }

    @Override
    public HashMap<String, ShipmentBill> listShipmentBillByPOAsMap(String companyId, String shopId, List<String> purchaseOrderIdList) {
        Iterable<ShipmentBill> result = coll.find("{companyId:#,shopId:#,archive:false,deleted:false,purchaseOrderId: { $in : # }}", companyId, shopId, purchaseOrderIdList).as(entityClazz);

        HashMap<String, ShipmentBill> resultMap = new HashMap<>();
        for (ShipmentBill shipmentBill : result) {
            String purchaseOrderId = shipmentBill.getPurchaseOrderId();
            if (StringUtils.isNotBlank(purchaseOrderId)) {
                resultMap.put(purchaseOrderId, shipmentBill);
            }
        }

        return resultMap;
    }

    @Override
    public WriteResult updateBillRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("shipmentbills").update(query, field);
    }

    @Override
    public List<ShipmentBill> getBillsListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<ShipmentBill> result = coll.find("{companyId:#, shopId:#,modified:{$lt:#, $gt:#}, active:true,qbBillRef: {$exists: false} }"
                , companyId, shopId, beforeDate, afterDate).as(ShipmentBill.class);
        return Lists.newArrayList(result);
    }

    @Override
    public void updatePaymentStatus(String shipmentBillId, ShipmentBill.PaymentStatus paymentStatus) {
        coll.update("{_id:#}", new ObjectId(shipmentBillId)).with("{$set: {paymentStatus:#, modified:#}}", paymentStatus, DateTime.now().getMillis());
    }

    @Override
    public <E extends ShipmentBill> SearchResult<E> getShipmentBillByPOWithoutQbRef(String companyId, String shopId, long syncTime, List<String> poIds, Class<E> clazz) {
        Iterable<E> bills = coll.find("{companyId:#,shopId:#,archive:false,deleted:false,purchaseOrderId: { $in : # }, created:{$gt:#}}", companyId, shopId, poIds, syncTime).as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(bills));
        return results;
    }

    @Override
    public void updateDesktopBillQbErrorAndTime(String companyId, String shopId, String refNo, Boolean qbErrored, long errorTime) {
        coll.update("{companyId:#, shopId:#, shipmentBillNumber:#}", companyId, shopId, refNo).with("{$set: {qbErrored:#, errorTime:#}}", qbErrored, errorTime);
    }

    @Override
    public void updateQbDesktopBillRef(String companyId, String shopId, String ref, String txnId) {
        coll.update("{companyId:#, shopId:#, shipmentBillNumber:#}", companyId, shopId, ref).with("{$set: {qbDesktopBillRef:#}}", txnId);
    }

    @Override
    public void updateReceivedDate(String shipmentBillId, long receivedDate) {
        coll.update("{_id:#}", new ObjectId(shipmentBillId)).with("{$set: {receivedDate:#, modified:#}}", receivedDate, DateTime.now().getMillis());
    }

    @Override
    public <E extends ShipmentBill> SearchResult<E> getBillByLimitsWithQBPaymentReceivedError(String companyId, String shopId, ShipmentBill.PaymentStatus paymentStatus, long syncTime, long lastSyncTime, Class<E> clazz) {
        Iterable<E> bills  = coll.find("{companyId:#, shopId:#, deleted:false, paymentStatus: #, qbDesktopBillRef:{$exists: true}, paymentHistory.qbPaymentReceived : false, paymentHistory.qbPaymentReceivedError: true, created:{$gt:#}, modified:{$gt:#}}", companyId, shopId, paymentStatus, syncTime, lastSyncTime)
                .sort("{modified:-1}")
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(bills));
        return results;
    }

    @Override
    public <E extends ShipmentBill> SearchResult<E> getBillByLimitsWithoutQBPaymentReceived(String companyId, String shopId, ShipmentBill.PaymentStatus paymentStatus, long syncTime, int start, int limit, Class<E> clazz) {
        Iterable<E> bills  = coll.find("{companyId:#, shopId:#, deleted:false, paymentStatus:#, qbDesktopBillRef:{$exists: true}, paymentHistory.qbPaymentReceived : false, paymentHistory.qbPaymentReceivedError: false, created:{$gt:#}}", companyId, shopId, paymentStatus, syncTime)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(bills));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public void updateQbDesktopPaymentReceivedRef(String companyId, String shopId, String id, boolean paymentReceivedRef, boolean qbPaymentReceivedError, long qbPaymentReceivedErroredTime) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(id)).with("{$set: {qbPaymentReceived:#, qbPaymentReceivedError:#, qbPaymentReceivedErrorTime:#}}",paymentReceivedRef, qbPaymentReceivedError, qbPaymentReceivedErroredTime);
    }

    @Override
    public void updateShipmentBillPaymentHistory(String companyID, String id, List<ShipmentBillPayment> paymentHistory) {
        coll.update("{companyId:#, _id:#}",companyID, new ObjectId(id)).with("{$set: {paymentHistory:#}}", paymentHistory);
    }

    @Override
    public <E extends ShipmentBill> List<E> getBillByIds(String companyId, String shopId, List<ObjectId> billIds, Class<E> clazz) {
        Iterable<E> bills = coll.find("{companyId:#, shopId:#, deleted : false, _id:{$in : #}}", companyId, shopId, billIds)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(bills);
    }

    @Override
    public void hardRemoveQuickBookDataInBills(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {qbDesktopBillRef:1, qbErrored:1, errorTime:1}}");
    }

    @Override
    public void hardRemoveQuickBookDataInBillPayments(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#, paymentHistory:{exists:true}}", companyId, shopId).multi().with("{$unset: {paymentHistory.$[].qbPaymentReceived:1, paymentHistory.$[].qbPaymentReceivedError:1, paymentHistory.$[].qbPaymentReceivedErrorTime:1}}");
    }
}
