package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.List;

public class ShipmentRepositoryImpl extends ShopBaseRepositoryImpl<Shipment> implements ShipmentRepository {

    @Inject
    public ShipmentRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Shipment.class, mongoManager);
    }

    /**
     * This method gets shipments by companyId and status
     */
    @Override
    public SearchResult<Shipment> listAllByStatus(String companyId, String shopId, int skip, int limit, String sortOptions, String status) {
        SearchResult<Shipment> searchResult = new SearchResult<>();
        Iterable<Shipment> shipmentIterable = coll.find("{companyId:#, shopId:#, shipmentStatus:#, deleted:false}", companyId, shopId, status).skip(skip).limit(limit).sort(sortOptions).as(Shipment.class);
        long count  = coll.count("{companyId:#, shopId:#, shipmentStatus:#, deleted:false}", companyId, shopId, status);
        searchResult.setValues(Lists.newArrayList(shipmentIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<Shipment> getAllShipment(String companyId, String shopId, int skip, int limit, String sortOptions) {
        SearchResult<Shipment> searchResult = new SearchResult<>();
        Iterable<Shipment> shipmentIterable = coll.find("{companyId:#, shopId:#, deleted:false}", companyId, shopId).skip(skip).limit(limit).sort(sortOptions).as(Shipment.class);
        long count  = coll.count("{companyId:#, shopId:#, deleted:false}", companyId, shopId);
        searchResult.setValues(Lists.newArrayList(shipmentIterable));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public Shipment getShipmentById(String companyId, String id) {
        Shipment shipment = null;
        if(id != null && ObjectId.isValid(id)) {
            shipment = coll.findOne("{companyId:#, _id:#}", companyId, new ObjectId(id)).as(Shipment.class);
        }

        return shipment;
    }

    @Override
    public List<Shipment> getAvailableShipment(String companyId, String shopId) {
        Iterable<Shipment> shipmentIterable = coll.find("{companyId:#, shopId:#, shipmentStatus:#, assigned:false, deleted:false}", companyId, shopId, Shipment.ShipmentStatus.Accepted).as(Shipment.class);
        return Lists.newArrayList(shipmentIterable);
    }

    @Override
    public Shipment getShipmentByShippingManifestId(String shippingManifestId){
        return coll.findOne("{shippingManifestId:#}", shippingManifestId).as(Shipment.class);
    }
}