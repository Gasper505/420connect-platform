package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Organization;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.OrganizationRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.CustomShopResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by mdo on 9/6/15.
 */
public class ShopRepositoryImpl extends CompanyBaseRepositoryImpl<Shop> implements ShopRepository {
    private static final int METERS_PER_MILE = 1610;

    @Inject
    public ShopRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Shop.class, mongoManager);
    }

    @Inject
    private OrganizationRepository organizationRepository;

    @Override
    public void setShopTimeZone(String timeZone) {
        coll.update("{timeZone: {$exists:false}}").multi().with("{$set: {timeZone:#, modified:#}}", timeZone, DateTime.now().getMillis());
    }

    @Override
    public void setShopShortIdentifier(String companyId, String shopId, String shortIdentifier) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set: {shortIdentifier:#,modified:#}}", shortIdentifier, DateTime.now().getMillis());
    }

    @Override
    public void updateLastMembersSyncDate(String companyId, String shopId) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set: {membersCountSyncDate:#,modified:#}}", DateTime.now().getMillis(), DateTime.now().getMillis());
    }

    @Override
    public void updateTerminalSaleOption(String companyId, String shopId, Shop.TerminalSalesOption salesOption) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set: {termSalesOption:#,modified:#}}", salesOption, DateTime.now().getMillis());
    }

    @Override
    public void updateSnapshoptTime(String companyId, String shopId) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set: {snapshopTime:#,modified:#}}", DateTime.now().getMillis(), DateTime.now().getMillis());
    }

    @Override
    public void updateNextSnapshoptTime(String companyId, String shopId, long nextSnapshotTime) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set: {nextSnapshotTime:#,modified:#}}", nextSnapshotTime, DateTime.now().getMillis());
    }

    @Override
    public void updateTwilioPhoneNumber(String companyId, String shopId, String twilioNumber) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set: {twilioNumber:#,modified:#}}", twilioNumber, DateTime.now().getMillis());

    }

    @Override
    public boolean existsWithIdentifier(String shortIdentifier) {
        return coll.count("{shortIdentifier:#}", shortIdentifier) > 0;
    }

    @Override
    public SearchResult<Shop> nearbyShops(long lat, long lng, int radius, int skip, int limit) {
        Iterable<Shop> results = coll.find("{$near:{$geometry:{type:#, coordinates:[#, #]}, $maxDistance:#, $minDistance:#}}",
                "Point", lng, lat, 0, radius * METERS_PER_MILE).skip(skip).limit(limit).as(entityClazz);
        long count = coll.count("{$near:{$geometry:{type:#, coordinates:[#, #]}, $maxDistance:#, $minDistance:#}}",
                "Point", lng, lat, 0, radius * METERS_PER_MILE);
        SearchResult<Shop> shops = new SearchResult<>();
        shops.setValues(Lists.newArrayList(results));
        shops.setLimit(limit);
        shops.setSkip(skip);
        shops.setTotal(count);
        return shops;
    }

    @Override
    public Shop getShopByPhoneNumber(final String phoneNumber) {
        return coll.findOne("{phoneNumber:#}", phoneNumber).as(entityClazz);
    }

    @Override
    public <E extends Shop> SearchResult<E> findShopsByAppTarget(String companyId, int skip, int limit, CompanyFeatures.AppTarget appTarget, Class<E> clazz) {
        Iterable<E> items;
        long count = 0;
        if (CompanyFeatures.AppTarget.Retail == appTarget) {
            items = coll.find("{companyId:#,deleted:false, $or: [{appTarget:#},{appTarget:null}]}", companyId, appTarget).skip(skip).limit(limit).as(clazz);
            count = coll.count("{companyId:#,deleted:false, $or: [{appTarget:#},{appTarget:null}]}", companyId, appTarget);
        } else {
            items = coll.find("{companyId:#,deleted:false, appTarget:#}", companyId, appTarget).skip(skip).limit(limit).as(clazz);
            count = coll.count("{companyId:#,deleted:false, appTarget:#}", companyId, appTarget);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void updateEnableTookan(String shopId, boolean enable) {
        coll.update("{_id:#}", new ObjectId(shopId)).with("{$set:{enableTookan:#,modified:#}}", enable, DateTime.now().getMillis());

    }

    @Override
    public Shop getShopByTwilioNumber(String number) {
        return coll.findOne("{ deleted : false, active : true,twilioNumber:#}", number).orderBy("{ modified:-1}").as(entityClazz);

    }

    @Override
    public void updateProductTags(String companyId, String shopId, LinkedHashSet<String> productsTag) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(shopId)).with("{$set:{productsTag:#,modified:#}}", productsTag, DateTime.now().getMillis());
    }

    @Override
    public void updateShopInfo(String companyId, String shopId, Map<String, Object> shopInfoMap) {

        Set<String> strings = shopInfoMap.keySet();
        final StringBuilder builder = new StringBuilder();
        strings.forEach(s -> builder.append(s).append(":#,"));

        String data = builder.toString();
        data = data.substring(0, data.length() - 1);


        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(shopId)).with("{$set:{" + data + "}}", shopInfoMap.values().toArray());
    }

    @Override
    public void updateOrderTags(String companyId, String shopId, Set<String> orderTags) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(shopId)).with("{$set:{orderTags:#,modified:#}}", orderTags, DateTime.now().getMillis());
    }

    @Override
    public void updateMarketingSources(String companyId, String shopId, LinkedHashSet<String> marketingSources) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(shopId)).with("{$set:{marketingSources:#,modified:#}}", marketingSources, DateTime.now().getMillis());

    }

    @Override
    public void updateMemberTags(String companyId, String shopId, Set<String> memberTags) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(shopId)).with("{$set:{membersTag:#,modified:#}}", memberTags, DateTime.now().getMillis());

    }

    @Override
    public SearchResult<CustomShopResult> getAllShops(int skip, int limit){
        Iterable<CustomShopResult> items = coll.find("{deleted:false}").skip(skip).limit(limit).as(CustomShopResult.class);

        long count = coll.count("{deleted:false}");

        SearchResult<CustomShopResult> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<ShopResult> getEmployeeShops(Employee employee) {
        List<String> companyIds = new ArrayList();
        companyIds.add(employee.getCompanyId());
        for(String organizationId : employee.getOrganizationIds()){
            Organization organization = organizationRepository.getById(organizationId);
            companyIds.addAll(organization.getCompanyIds());
        }

        return coll.find("{deleted:false, companyId:{$in:#}}", companyIds).as(ShopResult.class);
    }

    @Override
    public Iterable<ShopResult> getAllShopsInList(List<String> shopList){
        List<ObjectId> shopIds = new ArrayList<>();
        for (String shopId : shopList) {
            shopIds.add(new ObjectId(shopId));
        }
        return coll.find("{deleted:false, _id: {$in:#}}",shopIds).as(ShopResult.class);
    }
}
