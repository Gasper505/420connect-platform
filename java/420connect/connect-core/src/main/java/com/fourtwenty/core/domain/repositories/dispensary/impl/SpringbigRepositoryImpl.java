package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.SpringbigRepository;

import javax.inject.Inject;

public class SpringbigRepositoryImpl extends ShopBaseRepositoryImpl<SpringBigInfo> implements SpringbigRepository {

    @Inject
    public SpringbigRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(SpringBigInfo.class, mongoManager);
    }

    @Override
    public SpringBigInfo getInfoByShop(String companyId, String shopId) {
        return coll.findOne("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
    }
}
