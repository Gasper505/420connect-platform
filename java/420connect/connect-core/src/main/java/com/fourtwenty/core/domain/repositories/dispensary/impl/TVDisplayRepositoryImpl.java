package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.plugins.TVDisplay;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.TVDisplayRepository;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * Created by decipher on 14/10/17 2:45 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class TVDisplayRepositoryImpl extends ShopBaseRepositoryImpl<TVDisplay> implements TVDisplayRepository {

    @Inject
    public TVDisplayRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TVDisplay.class, mongoManager);
    }

    @Override
    public TVDisplay getTvDisplayByIndentifier(String uniqueIdentifier) {
        return coll.findOne("{identifier:#}", uniqueIdentifier).as(entityClazz);
    }

    @Override
    public boolean existsWithIdentifier(String uniqueIdentifier) {
        return coll.count("{identifier:#}", uniqueIdentifier) > 0;
    }

    /**
     * Fetch TVDisplay on basis of tv number and unique identifier
     *
     * @param uniqueIdentifier : unique identifier for tv display
     * @param tvNo             : tv number for tv display
     */
    @Override
    public TVDisplay getTVDisplayByIdentifierAndTVNo(String uniqueIdentifier, Long tvNo) {
        Iterable<TVDisplay> tvDisplays = coll.find("{identifier:#,tvNumber:#}", uniqueIdentifier, tvNo).as(entityClazz);

        ArrayList<TVDisplay> tvDisplayList = Lists.newArrayList(tvDisplays);
        TVDisplay tvDisplay = null;

        if (!tvDisplayList.isEmpty()) {
            tvDisplay = tvDisplayList.get(0);
        }

        return tvDisplay;
    }

    @Override
    public <E extends TVDisplay> E getTVDisplayByIdentifierAndTVNo(String uniqueIdentifier, Long tvNo, Class<E> clazz) {

        Iterable<E> tvDisplays = coll.find("{identifier:#,tvNumber:#}", uniqueIdentifier, tvNo).as(clazz);

        for (E display : tvDisplays) {
            return display; // return the first one
        }

        return null;
    }

    @Override
    public void removeAndUnpublish(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,published:false,modified:#}}", DateTime.now().getMillis());
    }
}
