package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalLocationRepository;
import com.fourtwenty.core.rest.dispensary.results.EmployeeRecentLocationResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.AggregationOptions;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 7/3/16.
 */
public class TerminalLocationRepositoryImpl extends ShopBaseRepositoryImpl<TerminalLocation> implements TerminalLocationRepository {

    @Inject
    public TerminalLocationRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TerminalLocation.class, mongoManager);
    }


    @Override
    public Iterable<TerminalLocation> getTermLocations(String companyId, String shopId, String terminalId, int limit) {
        return coll.find("{companyId:#,shopId:#,terminalId:#}", companyId, shopId, terminalId).sort("{modified:-1}").limit(limit).as(entityClazz);
    }

    @Override
    public Iterable<TerminalLocation> getTermLocationsAfter(String companyId, String shopId, Long startDate, String sortOption) {
        return coll.find("{companyId:#, shopId:#, deleted: false, created:{$gte:#}}", companyId, shopId, startDate).sort(sortOption).as(entityClazz);
    }

    @Override
    public Iterable<TerminalLocation> getTermLocationsWithDate(String companyId, String shopId, Long startDate, Long endDate, String sortOption) {
        return coll.find("{companyId:#, shopId:#, deleted: false, created:{$lt:#, $gt:#}}", companyId, shopId, endDate, startDate).sort(sortOption).as(entityClazz);

    }

    @Override
    public Iterable<TerminalLocation> getTermLocationsWithDate(String companyId, String shopId, Long startDate, Long endDate, String sortOption, String employeeId) {
        return coll.find("{companyId:#, shopId:#, employeeId:#,created:{$lt:#, $gt:#}}", companyId, shopId, endDate, startDate).sort(sortOption).as(entityClazz);
    }

    @Override
    public Iterable<TerminalLocation> listByShopSortAndEmployee(String companyId, String shopId, String sortOptions, int skip, int limit, String employeeId) {
        return coll.find("{companyId: #,shopId:#,employeeId:#,deleted:false}", companyId, shopId, employeeId).sort(sortOptions).as(entityClazz);
    }

    @Override
    public TerminalLocation getRecentLocation(String companyId, String shopId, String employeeId) {
        return coll.findOne("{companyId: #,shopId:#,employeeId:#}", companyId, shopId, employeeId).orderBy("{created:-1}").as(entityClazz);
    }

    @Override
    public Iterable<TerminalLocation> listAllBySort(String sortOptions) {
        return coll.find().sort(sortOptions).as(entityClazz);
    }

    @Override
    public List<TerminalLocation> getLocationByTransaction(String companyId, String shopId, String sortOptions, String employeeId, String terminalId, int start, int limit, Long startRouteDate, Long endRouteDate) {
        if (endRouteDate == null || endRouteDate == 0) {
            endRouteDate = DateTime.now().getMillis();
        }

        Iterable<TerminalLocation> locations = coll.find("{companyId:#,shopId:#,terminalId:#,deleted: false, employeeId:#, created:{$lt:#, $gt:#}}", companyId, shopId, terminalId, employeeId, endRouteDate, startRouteDate).sort(sortOptions).limit(limit).as(entityClazz);
        return Lists.newArrayList(locations);
    }

    @Override
    public HashMap<String, EmployeeRecentLocationResult> getEmployeesRecentLocation(String companyId, String shopId, List<String> employeeIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<EmployeeRecentLocationResult> terminalLocations = coll.aggregate("{$match: {companyId:#,shopId:#,employeeId:{$in:#}}}", companyId, shopId, employeeIds)
                .and("{$sort: {created:-1}}")
                .and("{$group: {_id:'$employeeId', loc: {$first: '$loc'} }}")
                .options(aggregationOptions)
                .as(EmployeeRecentLocationResult.class);


        HashMap<String, EmployeeRecentLocationResult> terminalLocationMap = new HashMap<>();

        for (EmployeeRecentLocationResult item : terminalLocations) {
            terminalLocationMap.put(item.getId(), item);
        }
        return terminalLocationMap;
    }

    @Override
    public HashMap<String, TerminalLocation> getRecentLocationByEmployee(String companyId, String shopId, List<String> employeeIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Employee> items = coll.aggregate("{ $match : {companyId: #,shopId:#,employeeId: { $in : # }} }", companyId, shopId, employeeIds)
                .and("{$sort : { created :-1 } }")
                .and("{ $group : {_id: '$employeeId' , recentLocation: { $first: '$$ROOT' }} }")
                .options(aggregationOptions)
                .as(Employee.class);

        HashMap<String, TerminalLocation> terminalLocationHashMap = new HashMap<>();

        for (Employee employee : items) {
            terminalLocationHashMap.put(employee.getId(), employee.getRecentLocation());
        }

        return terminalLocationHashMap;

    }
}
