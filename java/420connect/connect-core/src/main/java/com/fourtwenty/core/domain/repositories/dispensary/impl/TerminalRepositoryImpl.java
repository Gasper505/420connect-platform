package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalUpdateNameRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.restfb.util.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by mdo on 10/9/15.
 */
public class TerminalRepositoryImpl extends ShopBaseRepositoryImpl<Terminal> implements TerminalRepository {

    @Inject
    public TerminalRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Terminal.class, mongoManager);
    }

    @Override
    public Terminal getTerminalByDeviceId(String companyId, String deviceId) {
        return coll.findOne("{companyId:#,$or:[{deviceId:#},{$and:[{deviceDetail:{$exists:true}},{deviceDetail.deviceId:#}]}]}", companyId, deviceId, deviceId).as(entityClazz);
    }

    @Override
    public Terminal getTerminalByName(String companyId, String name) {
        return coll.findOne("{companyId:#, name:#}", companyId, name).as(entityClazz);
    }

    @Override
    public Boolean isNameAlreadyExist(String name) {
        long count = (int) coll.count("{name:#,deleted:false}", name);

        if (count > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void updateTerminalName(String companyId, String terminalId, TerminalUpdateNameRequest request) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(terminalId)).with("{$set: {name:#,modified:#,active:#,assignedInventoryId:#}}",
                request.getName(),
                DateTime.now().getMillis(),
                request.isActive(),
                request.getAssignedInventoryId());
    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public Terminal getTerminalById(String companyId, String terminalId) {
        if (StringUtils.isBlank(terminalId) || !ObjectId.isValid(terminalId)) {
            return null;
        }
        return coll.findOne("{companyId:#,_id:#,deleted:false}", companyId, new ObjectId(terminalId)).as(entityClazz);

    }

    @Override
    public SearchResult<Terminal> getAllTerminalsByState(String companyId, String shopId, boolean state, String sortOption, int start, int limit) {
        Iterable<Terminal> items = coll.find("{companyId:#,shopId:#,deleted:false,active:#}", companyId, shopId, state).sort(sortOption).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,deleted:false,active:#}", companyId, shopId, state);

        SearchResult<Terminal> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void updateCheckoutType(List<String> shopIds, Shop.ShopCheckoutType checkoutType) {
        coll.update("{ shopId: {$in:#} }", shopIds).multi().with("{ $set: {checkoutType : #, modified : #} }", checkoutType, DateTime.now().getMillis());
    }
}
