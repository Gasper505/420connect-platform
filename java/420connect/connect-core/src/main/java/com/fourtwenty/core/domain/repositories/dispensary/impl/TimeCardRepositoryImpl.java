package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.EmployeeSession;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.TimeCardRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * Created by mdo on 6/24/16.
 */
public class TimeCardRepositoryImpl extends ShopBaseRepositoryImpl<TimeCard> implements TimeCardRepository {
    @Inject
    public TimeCardRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TimeCard.class, mongoManager);
    }

    @Override
    public TimeCard getActiveTimeCard(String companyId, String shopId, String employeeId) {
        return coll.findOne("{companyId:#,shopId:#,employeeId:#,clockin:#}", companyId, shopId, employeeId, true).as(entityClazz);
    }

    @Override
    public Iterable<TimeCard> getActiveTimeCards(String companyId, String shopId) {
        return coll.find("{companyId:#,shopId:#,clockin:#}", companyId, shopId, true).sort("{clockInTime:1}").as(entityClazz);
    }

    @Override
    public Iterable<TimeCard> getAllTimeCards(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, clockInTime:{$gte: #}, clockOutTime:{$lte:#}}", companyId, shopId, startDate, endDate)
                .as(entityClazz);
    }

    @Override
    public void addTimeCardSession(String companyId, String timeCardId, EmployeeSession employeeSession) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(timeCardId)).with("{$push:{sessions:#},$set:{modified:#}}", employeeSession, DateTime.now().getMillis());
    }
}
