package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.model.reportmodels.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.MemberSalesDetail;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import joptsimple.internal.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by mdo on 1/22/16.
 */
public class TransactionRepositoryImpl extends ShopBaseRepositoryImpl<Transaction> implements TransactionRepository {
    private static final Log log = LogFactory.getLog(TransactionRepositoryImpl.class);

    @javax.inject.Inject
    public TransactionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Transaction.class, mongoManager);
    }


    @Override
    public void updatePointsEarned(String companyId, String transId, BigDecimal pointsEarned) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId)).with("{$set: {pointsEarned:#}}", pointsEarned.toString());
    }

    @Override
    public void updateMetrcId(String companyId, String transId, long metrcId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {metrcId:#,traceSubmitStatus:#, modified:#}}", metrcId, Transaction.TraceSubmissionStatus.Completed, DateTime.now().getMillis());
    }

    @Override
    public void updateTraceSubmitStatus(String companyId, String transId, Transaction.TraceSubmissionStatus traceSubmitStatus) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {traceSubmitStatus:#, modified:#}}", traceSubmitStatus, DateTime.now().getMillis());
    }


    @Override
    public void updateTraceSubmitStatusMessage(String companyId, String transId, Transaction.TraceSubmissionStatus traceSubmitStatus, String message) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {traceSubmitStatus:#, traceMessage:#,modified:#}}", traceSubmitStatus, message,DateTime.now().getMillis());
    }

    @Override
    public void markAsUnassigned(String companyId, String transId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {assigned:#, sellerId:#,assignedEmployeeId:#,modified:#}}", false, Strings.EMPTY, Strings.EMPTY, DateTime.now().getMillis());
    }

    @Override
    public void deactivate(String companyId, String transId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {active:#, modified:#}}", false, DateTime.now().getMillis());
    }


    @Override
    public void completeTransaction(String companyId, String transId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {active:#, status:#,deleteNote:#, modified:#}}", false, Transaction.TransactionStatus.Completed, null, DateTime.now().getMillis());
    }

    @Override
    public void completeTransaction(String companyId, String transId, String sellerId, String sellerTerminalId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {active:#, status:#,deleteNote:#, sellerId:#,sellerTerminalId:#, modified:#}}",
                        false, Transaction.TransactionStatus.Completed, null, sellerId,sellerTerminalId,DateTime.now().getMillis());
    }

    @Override
    public void cancelTransaction(String companyId, String transId, String sellerId, String sellerTerminalId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {active:#, status:#, sellerId:#,sellerTerminalId:#, modified:#}}",
                        false, Transaction.TransactionStatus.Completed, sellerId, sellerTerminalId, DateTime.now().getMillis());
    }
    @Override
    public void markTransactionAsPaid(String companyId, String transId, long paidDate, Cart.PaymentOption paymentOption) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {paid:#, processedTime:#, modified:#, cart.paymentOption :#}}", true, paidDate, DateTime.now().getMillis(), paymentOption);
    }

    @Override
    public void markTransactionAsUnpaid(String companyId, String transId, Cart.PaymentOption paymentOption) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {paid:#, modified:#, cart.paymentOption :#}}", false, DateTime.now().getMillis(), paymentOption);
    }

    @Override
    public void updateTransactionState(String companyId, String transId, Transaction.TransactionStatus status,  String sellerId, String sellerTerminalId) {

        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {status:#, sellerId:#,sellerTerminalId:#, modified:#}}",
                        status,sellerId,sellerTerminalId,DateTime.now().getMillis());
    }

    @Override
    public void startTransaction(String companyId, String transId, Transaction.TransactionStatus status, String sellerId, String sellerTerminalId) {

        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {status:#, sellerId:#,sellerTerminalId:#,startTime:#, modified:#}}",
                        status,sellerId,sellerTerminalId,DateTime.now().getMillis(),DateTime.now().getMillis());
    }

    @Override
    public void updateTransactionState(String companyId, String transId, Transaction.TransactionStatus status, String assignedEmployeeId, String sellerId, String sellerTerminalId) {

        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {status:#, sellerId:#,assignedEmployeeId:#,sellerTerminalId:#, modified:#}}",
                        status,sellerId,assignedEmployeeId,sellerTerminalId,DateTime.now().getMillis());
    }

    @Override
    public void startTransaction(String companyId, String transId, Transaction.TransactionStatus status, String assignedEmployeeId, String sellerId, String sellerTerminalId) {

        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {status:#, sellerId:#,assignedEmployeeId:#,sellerTerminalId:#,startTime:#, assigned:#, modified:#}}",
                        status,sellerId,assignedEmployeeId,sellerTerminalId,DateTime.now().getMillis(),true,DateTime.now().getMillis());
    }

    @Override
    public void updateOnfleetInfo(String companyId, String transId, int onfleetState, String onfleetId, String onfleetShortId, boolean createOnfleetFlag) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {state:#,shortId:#, onFleetTaskId:#, createOnfleetTask:#,modified:#}}",
                        onfleetState, onfleetShortId, onfleetId, createOnfleetFlag, DateTime.now().getMillis());
    }

    @Override
    public void updateOnfleetInfo(String companyId, String transId, int onfleetState, Transaction.OnFleetTaskStatus onFleetTaskStatus) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transId))
                .with("{$set: {state:#,onFleetTaskStatus:#, modified:#}}",
                        onfleetState, onFleetTaskStatus, DateTime.now().getMillis());
    }

    @Override
    public long getActiveTransactionCount(String shopId, String memberId) {
        return coll.count("{shopId:#,memberId:#,active:#}", shopId, memberId, true);
    }

    @Override
    public Iterable<Transaction> getAllTransactionsByType(Transaction.TransactionType transactionType) {
        return coll.find("{transType:#}", transactionType).as(entityClazz);
    }

    @Override
    public List<Transaction> getTransactions(String companyId) {
        return Lists.newArrayList((Iterable<Transaction>) coll.find("{companyId:#,deleted:false}}", companyId).as(entityClazz));
    }

    @Override
    public SearchResult<Transaction> getTransactionsForMember(String companyId, String shopId, String memberId, int skip, int limit) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, memberId:#,status: {$in:#}}", companyId, shopId, memberId, statuses)
                .sort("{processedTime:-1}")
                .skip(skip)
                .limit(limit)
                .as(entityClazz);
        long count = coll.count("{companyId:#, shopId:#, memberId:#,status: {$in:#}}", companyId, shopId, memberId, statuses);

        SearchResult<Transaction> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(results));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public DateSearchResult<Transaction> getCompletedTransactionsDates(String companyId, String shopId, long startDate, long endDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}}", companyId, shopId, startDate, endDate, statuses)
                .sort("{processedTime:-1}").as(entityClazz);

        DateSearchResult<Transaction> searchResult = new DateSearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setAfterDate(startDate);
        searchResult.setBeforeDate(endDate);
        searchResult.setTotal((long) searchResult.getValues().size());
        return searchResult;
    }

    @Override
    public DateSearchResult<Transaction> getCompletedTransactionsForTerminal(String companyId, String shopId, String terminalId, long startDate, long endDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#},status:{$in: #},sellerTerminalId:#}", companyId, shopId, startDate, endDate, statuses, terminalId)
                .sort("{processedTime:-1}").as(entityClazz);

        DateSearchResult<Transaction> searchResult = new DateSearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setAfterDate(startDate);
        searchResult.setBeforeDate(endDate);
        searchResult.setTotal((long) searchResult.getValues().size());
        return searchResult;
    }

    @Override
    public Iterable<Transaction> getCompletedTransactionsDatesList(String companyId, String shopId, long startDate, long endDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}}", companyId, shopId, startDate, endDate, statuses)
                .sort("{processedTime:-1}").as(entityClazz);
        return items;
    }

    @Override
    public Iterable<Transaction> getCompletedTransactionsForDateRange(String companyId, String shopId, long startDate, long endDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
        return coll.find("{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}}", companyId, shopId, startDate, endDate, statuses)
                .sort("{processedTime:-1}").as(entityClazz);
    }

    @Override
    public DateSearchResult<Transaction> getTransactionsForMemberDates(String companyId, String shopId, String memberId, long startDate, long endDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, memberId:#,processedTime:{$gte:#,$lte:#},status:{$in: #}}",
                companyId, shopId, memberId, startDate, endDate, statuses)
                .sort("{processedTime:-1}")
                .as(entityClazz);

        DateSearchResult<Transaction> searchResult = new DateSearchResult<>();
        searchResult.setValues(Lists.newArrayList(results));
        searchResult.setAfterDate(startDate);
        searchResult.setBeforeDate(endDate);
        searchResult.setTotal((long) searchResult.getValues().size());
        return searchResult;
    }

    @Override
    public DateSearchResult<Transaction> getActiveTransactions(String companyId, String shopId, boolean active, long afterDate, long beforeDate) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#, modified:{$lt:#, $gt:#}}", companyId, shopId, active, beforeDate, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, active:#, modified:{$lt:#, $gt:#}}", companyId, shopId, active, beforeDate, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Transaction> getSalesRefundsTransactions(String companyId, String shopId, long afterDate, long beforeDate) {
        List<Transaction.TransactionType> transactionTypes = new ArrayList<>();
        transactionTypes.add(Transaction.TransactionType.Sale);
        transactionTypes.add(Transaction.TransactionType.Refund);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, transType:{$in:#}, modified:{$lt:#, $gt:#}}", companyId, shopId,
                transactionTypes, beforeDate, afterDate).as(entityClazz);


        long count = coll.count("{companyId:#,shopId:#, transType:{$in:#}, modified:{$lt:#, $gt:#}}",
                companyId, shopId, transactionTypes, beforeDate, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Transaction> getSalesTransactions(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, transType:#, modified:{$lt:#, $gt:#}}", companyId, shopId,
                Transaction.TransactionType.Sale, beforeDate, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, transType:#, modified:{$lt:#, $gt:#}}", companyId, shopId, Transaction.TransactionType.Sale, beforeDate, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Transaction> getActiveTransactionsAssigned(String companyId, String shopId, String employeeId) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,active:#,assignedEmployeeId:#}", companyId, shopId, true, employeeId).sort("{modified:1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,active:#,assignedEmployeeId:#}", companyId, shopId, true, employeeId);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(0);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Transaction> getAllTransactionsAssigned(String companyId, String shopId, String employeeId, long afterDate, long beforeDate, int start, int limit) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#,modified:{$lt:#, $gt:#}}", companyId, shopId, employeeId, beforeDate, afterDate).skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,assignedEmployeeId:#, modified:{$lt:#, $gt:#}}", companyId, shopId, employeeId, beforeDate, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<Transaction> getAllActiveTransactions() {
        Iterable<Transaction> items = coll.find("{active:#}", true).sort("{priority:1}").as(entityClazz);
        return items;
    }

    @Override
    public Iterable<Transaction> getAllActiveTransactions(String companyId, String shopId) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#}", companyId, shopId, true).sort("{priority:1}").as(entityClazz);
        return items;
    }

    @Override
    public Iterable<Transaction> getAllActiveTransactionsForAssignedEmployee(String companyId, String shopId, String employeeId) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#,assignedEmployeeId:#}", companyId, shopId, true, employeeId).sort("{priority:1}").as(entityClazz);
        return items;
    }

    @Override
    public long getAllActiveTransactionsCount(String companyId, String shopId) {
        return coll.count("{companyId:#,shopId:#, active:#}", companyId, shopId, true);
    }

    @Override
    public SearchResult<Transaction> getActiveTransactions(String companyId, String shopId) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#}", companyId, shopId, true).sort("{priority:1}").as(entityClazz);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setTotal((long) results.getValues().size());
        return results;
    }


    @Override
    public SearchResult<Transaction> getActiveTransactionsForQueue(String companyId, String shopId, Transaction.QueueType queueType) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#, queueType:#}", companyId, shopId, true, queueType).sort("{priority:1}").as(entityClazz);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public SearchResult<Transaction> getCompletedTransactions(String companyId, String shopId, int start, int limit) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 500 || limit <= 0) {
            limit = 500;
        }


        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, status: {$in: #}}", companyId, shopId, statuses)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,status: {$in: #}}", companyId, shopId, statuses);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<Transaction> getCompletedTransactions(String companyId, String shopId, String employeeId, int start, int limit) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 200 || limit <= 0) {
            limit = 200;
        }


        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, status: {$in: #},$or:[{sellerId:#},{preparedBy:#}]}",
                companyId, shopId, statuses, employeeId, employeeId)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#, status: {$in: #},$or:[{sellerId:#},{preparedBy:#}]}",
                companyId, shopId, statuses, employeeId, employeeId);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<Transaction> getCompletedTransactionsForQueue(String companyId, String shopId, Transaction.QueueType queueType, int start, int limit) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 200 || limit <= 0) {
            limit = 200;
        }


        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Canceled,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, queueType:#,status: {$in: #}}", companyId, shopId, queueType, statuses)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#, queueType:#,status: {$in: #}}", companyId, shopId, queueType, statuses);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<Transaction> getCompletedTransactionsForQueue(String companyId, String shopId, Transaction.QueueType queueType, String employeeId, int start, int limit) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 200 || limit <= 0) {
            limit = 200;
        }


        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, queueType:#,status: {$in: #},$or:[{sellerId:#},{preparedBy:#}]}", companyId, shopId, queueType, statuses, employeeId, employeeId)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#, queueType:#,status: {$in: #},$or:[{sellerId:#},{preparedBy:#}]}", companyId, shopId, queueType, statuses, employeeId, employeeId);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public Transaction getTransactionByTransNo(String companyId, String shopId, String transNo) {
        return coll.findOne("{companyId:#,shopId:#, transNo:#}", companyId, shopId, transNo).as(entityClazz);
    }

    @Override
    public Transaction getTransactionByConsumerCartId(String companyId, String shopId, String consumerCartId) {
        return coll.findOne("{companyId:#,shopId:#, consumerCartId:#}", companyId, shopId, consumerCartId).as(entityClazz);
    }

    @Override
    public SearchResult<Transaction> findRecentTransactions(String companyId, String shopId, String transNo) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);

        SearchResult<Transaction> results = new SearchResult<>();
        if (transNo != null && ObjectId.isValid(transNo)) {
            // return one transaction instead
            Transaction transaction = coll.findOne(new ObjectId(transNo)).as(entityClazz);
            if (transaction != null) {
                results.getValues().add(transaction);
                results.setTotal(1l);
            }
        } else {
            Pattern pattern = TextUtil.createPattern(transNo);

            Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#, status: {$in: #},transNo:#}", companyId, shopId, false, statuses, pattern)
                    .sort("{processedTime:-1}").as(entityClazz);

            results.setValues(Lists.newArrayList(items));
        }
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public Iterable<Transaction> getTransactionByStatus(String companyId, String shopId, Transaction.TransactionStatus transactionStatus, long afterDate, long beforeDate) {
        return coll.find("{companyId:#, shopId:#,status:#,processedTime:{$gte:#, $lte:#}}",
                companyId, shopId, transactionStatus, afterDate, beforeDate).sort("{processedTime:-1}").as(entityClazz);
    }

    @Override
    public Iterable<Transaction> getTransactionByStatus(String companyId, String shopId, Transaction.TransactionStatus transactionStatus) {
        return coll.find("{companyId:#, shopId:#,status:#}",
                companyId, shopId, transactionStatus).sort("{processedTime:-1}").as(entityClazz);
    }




    @Override
    public Iterable<Transaction> getTransactionsWithoutOrderId() {
        return coll.find("{cart.items.1: {$exists:true}, cart.items.orderItemId: {$exists:false}}").as(entityClazz);
    }

    @Override
    public List<SalesByMember> getSalesByMember(String companyId, String shopId, long startDate, long endDate) {

        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);

        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByMember> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime:{$lt:#, $gt:#}, status: {$in:#}}}",
                companyId, shopId, endDate, startDate, statuses)
                .and("{$group: {_id: '$memberId', count: {$sum:1}, total: {$sum: {:1: [ {$and: [{$eq:['$transType', 'Refund']},{$eq:['$cart.refundOption', 'Retail']}]},{$multiply:['$cart.total', -1]},{$multiply:['$cart.total', 1]}]}}}}")
                .and("{$project: {_id:1, count:1, total:1}}")
                .and("{$sort: {total:-1}}")
                .options(aggregationOptions)
                .as(SalesByMember.class);

        return Lists.newArrayList(results.iterator());
    }

    @Override
    public List<SalesByMember> getTotalSalesByMember(String companyId, String shopId, String memberId) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);

        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByMember> results = coll.aggregate("{$match: {companyId:#, shopId:#, memberId:#, status: {$in:#}}}", companyId, shopId, memberId, statuses)
                .and("{$group: {_id: '$memberId', count: {$sum:1}, total: {$sum:'$cart.total'}}}")
                .and("{$project: {_id:1, count:1, total:1}}")
                .and("{$sort: {total:-1}}")
                .options(aggregationOptions)
                .as(SalesByMember.class);

        return Lists.newArrayList(results.iterator());
    }

    @Override
    public Iterable<NewMemberTransactions> getCompletedTransactionStatsByMemberIds(String companyId, String shopId, Iterable<String> newMemberIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<NewMemberTransactions> results = coll.aggregate("{$match: {companyId:#, shopId:#, memberId: {$in:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, newMemberIds, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$group: {_id: '$memberId', count: {$sum:1}}}")
                .and("{$project: {_id:1, count:1}}")
                .options(aggregationOptions)
                .as(NewMemberTransactions.class);

        return results;
    }

    @Override
    public List<Transaction> getRecentCompletedTransactions(String companyId, String shopId, int size) {
        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, active: false}", companyId, shopId)
                .sort("{processedTime: -1}")
                .limit(size)
                .as(entityClazz);

        return Lists.newArrayList(results.iterator());
    }

    @Override
    public List<Transaction> getRecentCompletedTransactionsSales(String companyId, String shopId, int size) {

        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);

        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, active: false, status: {$in: #}}",
                companyId, shopId, statuses)
                .sort("{processedTime: -1}")
                .limit(size)
                .as(entityClazz);

        return Lists.newArrayList(results.iterator());
    }

    @Override
    public Iterable<Transaction> getDailySales(String companyId, String shopId, Long startDate, Long endDate) {

        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, processedTime:{$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .as(entityClazz);

        return results;
    }

    @Override
    public long countTransactionsByMember(String companyId, String shopId, String memberId) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed);
        return coll.count("{companyId:#, shopId:#, memberId:#, status: {$in: #}}", companyId, shopId, memberId, statuses);
    }

    @Override
    public long getDailyTotalVisits(String companyId, String shopId, Long startDate, Long endDate) {

        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
        return coll.count("{companyId:#, shopId:#, status: {$in: #}, processedTime: {$gt: #, $lt:#}}", companyId, shopId, statuses, startDate, endDate);
    }

    @Override
    public Iterable<SalesByProduct> getSalesByCannabisType(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByProduct> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status:{$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$unwind: '$cart.items'}")
                .and("{$match: {cart.items.status:#}}", OrderItem.OrderItemStatus.Active)
                .and("{$group:{_id:'$cart.items.productId', sales:{$sum: '$cart.items.finalPrice'}}}")
                .and("{$project: {_id:1, sales:1}}")
                .options(aggregationOptions)
                .as(SalesByProduct.class);

        return results;
    }

    @Override
    public Iterable<BestSellingProduct> getDailyBestSellingProducts(String companyId, String shopId, Long startDate, Long endDate) {

        log.info("\nStatusTime: " + startDate + "\nEndTime: " + endDate);
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<BestSellingProduct> results =
                coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                        companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                        Transaction.TransactionStatus.RefundWithoutInventory)
                        .and("{$unwind: '$cart.items'}")
                        .and("{$match: {cart.items.status:#}}", OrderItem.OrderItemStatus.Active)
                        .and("{$group:{_id:'$cart.items.productId', sales:{$sum: '$cart.items.finalPrice'}}}")
                        .and("{$project: {_id:1, sales:1}}")
                        .and("{$sort: {sales:-1}}")
                        .options(aggregationOptions)
                        .as(BestSellingProduct.class);

        return results;
    }

    @Override
    public Iterable<Transaction> getBracketSales(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public Iterable<Transaction> getBracketSalesWithStatuses(String companyId, String shopId, Long startDate, Long endDate, List<Transaction.TransactionStatus> statuses) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:#}}",
                companyId, shopId, startDate, endDate, statuses)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public Iterable<Transaction> getBracketSales(String companyId, String shopId, long startDate, long endDate, int skip, int fetchRecordLimit) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .skip(skip).limit(fetchRecordLimit)
                .as(Transaction.class);
    }

    @Override
    public Iterable<Transaction> getBracketSalesForTerminalId(String companyId, String shopId, String terminalId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, sellerTerminalId:#, processedTime: {$gte:#, $lte:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, terminalId, startDate, endDate, Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public Iterable<Transaction> getBracketSalesWithUncompleteOrders(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#,#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Queued,
                Transaction.TransactionStatus.Hold,
                Transaction.TransactionStatus.InProgress)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public Iterable<SalesByProduct> getSalesByProduct(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByProduct> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$unwind: '$cart.items'}")
                .and("{$match: {cart.items.status:#}}", OrderItem.OrderItemStatus.Active)
                .and("{$group:{_id:'$cart.items.productId', sales:{$sum: '$cart.items.finalPrice'}," +
                        " tax:{$sum: {$multiply: ['$cart.tax', '$cart.items.quantity', '$cart.items.unitPrice']}}, units:{$sum: '$cart.items.quantity'} }}")
                .and("{$project: {_id:1, units:1, sales:1, tax:1}}")
                .options(aggregationOptions)
                .as(SalesByProduct.class);

        return results;
    }

    @Override
    public Iterable<SalesByPaymentType> getSalesByPaymentType(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByPaymentType> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$group: {_id:'$cart.paymentOption', sales: {$sum: '$cart.total'}, transactions:{$sum:1}}}")
                .and("{$project:{_id:1, sales:1, transactions:1}}")
                .options(aggregationOptions)
                .as(SalesByPaymentType.class);

        return results;
    }

    @Override
    public Iterable<SalesByPaymentTypeTerminal> getSalesByPaymentTypeTerminal(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByPaymentTypeTerminal> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$group: {_id:{terminalId: '$sellerTerminalId', paymentOption: '$cart.paymentOption'}, sales: {$sum: '$cart.total'}, transactions:{$sum:1}}}")
                .and("{$project:{_id:1, sales:1, transactions:1}}")
                .options(aggregationOptions)
                .as(SalesByPaymentTypeTerminal.class);

        return results;
    }

    @Override
    public Iterable<SalesByQueue> getSalesByQueue(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByQueue> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$group: {_id:'$queueType', sales: {$sum: '$cart.total'}, transactions:{$sum:1}}}")
                .and("{$project:{_id:1, sales:1, transactions:1}}")
                .options(aggregationOptions)
                .as(SalesByQueue.class);

        return results;
    }

    @Override
    public Iterable<SalesByEmployee> getSalesByEmployee(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<SalesByEmployee> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$group: {_id:'$sellerId', sales: {$sum: '$cart.total'}, transactions:{$sum:1}}}")
                .and("{$project:{_id:1, sales:1, transactions:1}}")
                .options(aggregationOptions)
                .as(SalesByEmployee.class);

        return results;
    }

    @Override
    public Iterable<Transaction> getTotalSales(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Transaction> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$sort: {processedTime: -1}}")
                .options(aggregationOptions)
                .as(Transaction.class);

        return results;
    }

    @Override
    public Iterable<Transaction> getRefundData(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Transaction> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.RefundWithInventory, Transaction.TransactionStatus.RefundWithoutInventory)
                .and("{$sort: {processedTime: -1}}")
                .options(aggregationOptions)
                .as(Transaction.class);
        return results;
    }

    @Override
    public Iterable<Transaction> getTransactionsWithDiscounts(String companyId, String shopId, Long startDate, Long endDate) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Transaction> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.RefundWithInventory, Transaction.TransactionStatus.RefundWithoutInventory
                , Transaction.TransactionStatus.Completed)
                .and("{$match:{cart.totalDiscount: {$not:{$eq:#}}}}", 0)
                .and("{$sort: {processedTime: -1}}")
                .options(aggregationOptions)
                .as(Transaction.class);

        return results;
    }

    @Override
    public Iterable<Transaction> getLosses(String companyId, String shopId, Long startDate, Long endDate) {

        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Transaction> results = coll.aggregate("{$match: {companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, transType:#}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionType.Adjustment)
                .and("{$sort: {processedTime: -1}}")
                .options(aggregationOptions)
                .as(Transaction.class);

        return results;
    }

    @Override
    public Iterable<Transaction> getTransfers(String companyId, String shopId, Long startDate, Long endDate) {
        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, transType:#}",
                companyId, shopId, startDate, endDate, Transaction.TransactionType.Transfer)
                .as(entityClazz);

        return results;
    }

    @Override
    public List<Transaction> getWalkinTransations(final String shopId, final Transaction.QueueType queueType) {
        Iterable<Transaction> result = coll.find("{shopId:#,queueType:#,active:#}", shopId, queueType, true).as(entityClazz);
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public Transaction getActiveTransactionsForShopAndMember(String shopId, String memberId) {
        Transaction result = coll.findOne("{shopId:#,memberId:#, active:#}", shopId, memberId, true).as(entityClazz);
        return result;
    }

    @Override
    public HashMap<String, Transaction> getTransactionsByAssignEmployeeId(String companyId, String shopId, String employeeId, String sortOptions, int skip, int limit) {
        List<Transaction.TransactionType> transactionTypeList = new ArrayList<>();
        transactionTypeList.add(Transaction.TransactionType.Sale);
        Iterable<Transaction> result = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#, transType: {$in: #}}", companyId, shopId, employeeId, transactionTypeList).sort(sortOptions).skip(skip).limit(limit).as(entityClazz);
        return asMap(result);
    }

    @Override
    public <E extends Transaction> SearchResult<E> getTransactionsByAssignEmployeeIdAndStatus(String companyId, String shopId, String employeeId, String sortOptions, int skip, int limit, Transaction.TransactionStatus status, Class<E> clazz) {
        SearchResult<E> results = new SearchResult<>();
        Iterable<E> items = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#,status:#}", companyId, shopId, employeeId, status).sort(sortOptions).skip(skip).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,assignedEmployeeId:#,status:#}", companyId, shopId, employeeId, status);

        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getTransactionsByTransactionTypes(List<Transaction.TransactionType> transactionTypeList, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 500 || limit <= 0) {
            limit = 500;
        }
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,transType: {$in: #},processedTime: {$gt:#, $lt:#}}", companyId, shopId, transactionTypeList, timeZoneStartDateMillis, timeZoneEndDateMillis)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,transType: {$in: #},processedTime: {$gt:#, $lt:#}}", companyId, shopId, transactionTypeList, timeZoneStartDateMillis, timeZoneEndDateMillis);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<Transaction> getTransactionsByTransactionStatus(List<Transaction.TransactionStatus> transactionStatusList, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 500 || limit <= 0) {
            limit = 500;
        }
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, status: {$in: #},processedTime: {$gt:#, $lt:#}}", companyId, shopId, transactionStatusList, timeZoneStartDateMillis, timeZoneEndDateMillis)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,status: {$in: #},processedTime: {$gt:#, $lt:#}}", companyId, shopId, transactionStatusList, timeZoneStartDateMillis, timeZoneEndDateMillis);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getTransactionsByTransactionTypes(List<Transaction.TransactionType> transactionTypeList, String employeeId, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 200 || limit <= 0) {
            limit = 200;
        }

        Iterable<Transaction> items = coll.find("{$or: [{companyId:#,shopId:#, transType: {$in: #}, sellerId:#},{companyId:#,shopId:#, transType: {$in: #}, preparedBy:#}]}",
                companyId, shopId, transactionTypeList, employeeId,
                companyId, shopId, transactionTypeList, employeeId)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{$or: [{companyId:#,shopId:#, transType: {$in: #}, sellerId:#},{companyId:#,shopId:#, transType: {$in: #}, preparedBy:#}]}",
                companyId, shopId, transactionTypeList, employeeId,
                companyId, shopId, transactionTypeList, employeeId);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getTransactionsByTransactionStatus(List<Transaction.TransactionStatus> transactionStatusList, String employeeId, String companyId, String shopId, int start, int limit, long timeZoneStartDateMillis, long timeZoneEndDateMillis) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 200 || limit <= 0) {
            limit = 200;
        }
        Iterable<Transaction> items = coll.find("{$or: [{companyId:#,shopId:#, status: {$in: #}, sellerId:#},{companyId:#,shopId:#, transType: {$in: #}, preparedBy:#}]}",
                companyId, shopId, transactionStatusList, employeeId,
                companyId, shopId, transactionStatusList, employeeId)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{$or: [{companyId:#,shopId:#, status: {$in: #}, sellerId:#},{companyId:#,shopId:#, transType: {$in: #}, preparedBy:#}]}",
                companyId, shopId, transactionStatusList, employeeId,
                companyId, shopId, transactionStatusList, employeeId);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getTransactionsByTransactionStatusAndMember(List<Transaction.TransactionStatus> transactionStatusList, String companyId, String shopId, int start, int limit, String memberId) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 500 || limit <= 0) {
            limit = 500;
        }
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,memberId:#,status: {$in: #}}", companyId, shopId, memberId, transactionStatusList)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,memberId:#,status: {$in: #}}", companyId, shopId, memberId, transactionStatusList);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getTransactionsByTransactionTypesAndMember(List<Transaction.TransactionType> transactionTypeList, String companyId, String shopId, int start, int limit, String memberId) {
        if (start < 0) {
            start = 0;
        }
        if (limit > 500 || limit <= 0) {
            limit = 500;
        }
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,memberId:#,transType: {$in: #}}", companyId, shopId, memberId, transactionTypeList)
                .skip(start)
                .limit(limit)
                .sort("{processedTime:-1}").as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,memberId:#,transType: {$in: #}}", companyId, shopId, memberId, transactionTypeList);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public HashMap<String, Transaction> getTransactionsByMileageCalculated(boolean isMileageCalculated) {
        List<Transaction.TransactionStatus> statusList = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory,
                Transaction.TransactionStatus.Void);
        Iterable<Transaction> result = coll.find("{isMileageCalculated: { $exists: # },status: {$in: #},deleted: false,}", isMileageCalculated, statusList).as(entityClazz);
        return asMap(result);
    }

    @Override
    public HashMap<String, Transaction> listTransactionByDate(String companyId, String shopId, String sortOption, Long startDate, Long endDate) {
        Iterable<Transaction> transactions = listByShopWithDateSort(companyId, shopId, sortOption, startDate, endDate);
        return asMap(transactions);
    }

    @Override
    public Transaction getTransactionByTaskId(String taskId) {
        return coll.findOne("{onFleetTaskId:#}", taskId).as(entityClazz);
    }

    @Override
    public SearchResult<Transaction> getActiveOnFleetTransactions(String companyId, String shopId) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#, onFleetTaskId:{$exists : true, $ne : ''}}", companyId, shopId, true).sort("{priority:1}").as(entityClazz);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public Iterable<Transaction> getBracketSalesByMemberId(String companyId, String memberId, Long startDate, Long endDate) {
        return coll.find("{companyId:#,  memberId: #, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, memberId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public Iterable<Transaction> getBracketSalesByMemberId(String companyId, String shopId, String memberId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, memberId: #, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, memberId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .as(Transaction.class);

    }

    @Override
    public Iterable<Transaction> getOnFleetTransactions(String companyId, String shopId) {
        return coll.find("{companyId:#,shopId:#, onFleetTaskId:{$exists : true, $ne : ''}}", companyId, shopId).sort("{priority:1}").as(entityClazz);
    }

    @Override
    public Iterable<Transaction> getBracketSalesByCompany(String companyId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}, transType:{$in:[#,#]}}",
                companyId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory, Transaction.TransactionType.Sale, Transaction.TransactionType.Refund)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    @Deprecated
    public WriteResult updateSalesReceiptRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("transactions").update(query, field);
    }


    @Override
    @Deprecated
    public WriteResult updateRefundReceipt(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("transactions").update(query, field);
    }

    @Override
    public void updateTransactionLockStatus(String companyId, String transactionId, Boolean lockStatus) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transactionId))
                .with("{$set: {locked:#, modified:#}}", lockStatus, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<Transaction> getAllTransactionByStatusAndType(String companyId, String shopId, Transaction.TransactionStatus transactionStatus, Transaction.TransactionType transactionType, long timeZoneStartDateMillis, long timeZoneEndDateMillis) {
        Iterable<Transaction> items = coll.find("{companyId:#, shopId:#, deleted:false, status:#, transType:#, processedTime: {$gt:#, $lt:#}}", companyId, shopId, transactionStatus, transactionType, timeZoneStartDateMillis, timeZoneEndDateMillis).as(entityClazz);
        long count = coll.count("{companyId:#, shopId:#, deleted:false, status:#, transType:#, processedTime: {$gt:#, $lt:#}}", companyId, shopId, transactionStatus, transactionType, timeZoneStartDateMillis, timeZoneEndDateMillis);

        SearchResult<Transaction> transactionSearchResult = new SearchResult<>();
        transactionSearchResult.setValues(Lists.newArrayList(items));
        transactionSearchResult.setTotal(count);

        return transactionSearchResult;
    }

    @Override
    public SearchResult<Transaction> getAllTransactionByStatusAndTypeLimit(String companyId, String shopId, List<Transaction.TransactionStatus> transactionStatuses,
                                                                      List<Transaction.TransactionType> transactionTypes, int skip, int limit) {
        Iterable<Transaction> items = coll.find("{companyId:#, shopId:#, deleted:false, status:{$in:#}, transType:{$in:#}}",
                companyId, shopId, transactionStatuses, transactionTypes).sort("{processedTime:1}").skip(skip).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#, shopId:#, deleted:false, status:{$in:#}, transType:{$in:#}}", companyId, shopId,
                transactionStatuses, transactionTypes);

        SearchResult<Transaction> transactionSearchResult = new SearchResult<>();
        transactionSearchResult.setValues(Lists.newArrayList(items));
        transactionSearchResult.setTotal(count);
        transactionSearchResult.setSkip(skip);
        transactionSearchResult.setLimit(limit);

        return transactionSearchResult;
    }

    @Override
    public SearchResult<Transaction> getAllTransactionByStatusAndType(String companyId, String shopId,
                                                                      List<Transaction.TransactionStatus> transactionStatuses,
                                                                      List<Transaction.TransactionType> transactionTypes,
                                                                      long timeZoneStartDateMillis, long timeZoneEndDateMillis, int skip, int limit) {
        Iterable<Transaction> items = coll.find("{companyId:#, shopId:#, deleted:false, status:{$in:#}, transType:{$in:#}, processedTime: {$gt:#, $lt:#}}",
                companyId, shopId,
                transactionStatuses, transactionTypes, timeZoneStartDateMillis, timeZoneEndDateMillis).sort("{processedTime:1}").skip(skip).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#, shopId:#, deleted:false, status:{$in:#}, transType:{$in:#}, processedTime: {$gt:#, $lt:#}}", companyId, shopId,
                transactionStatuses, transactionTypes, timeZoneStartDateMillis, timeZoneEndDateMillis);

        SearchResult<Transaction> transactionSearchResult = new SearchResult<>();
        transactionSearchResult.setValues(Lists.newArrayList(items));
        transactionSearchResult.setTotal(count);
        transactionSearchResult.setSkip(skip);
        transactionSearchResult.setLimit(limit);

        return transactionSearchResult;
    }

    @Override
    public SearchResult<Transaction> getTransactionByProduct(String companyId, String shopId, String sortOption, List<String> productIds) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Transaction> items = coll.aggregate("{$match:{companyId:#,shopId:#,deleted:false,active:true}}", companyId, shopId)
                .and("{$match: {cart.items : { $elemMatch: {productId: {$in:#}}}}}", productIds)
                .options(aggregationOptions)
                .as(entityClazz);
        SearchResult<Transaction> transactionSearchResult = new SearchResult<>();
        transactionSearchResult.setValues(Lists.newArrayList((Iterable<Transaction>) items));
        return transactionSearchResult;
    }

    @Override
    public Iterable<Transaction> getTransactionByType(String companyId, String shopId, Transaction.TransactionType transactionType) {
        return coll.find("{companyId:#, shopId:#,transType:#}",
                companyId, shopId, transactionType).as(entityClazz);
    }

    @Override
    public Iterable<Transaction> getTransactionByTypeAndStatus(String companyId, String shopId, Transaction.TransactionType transactionType, Transaction.TransactionStatus transactionStatus) {
        return coll.find("{companyId:#, shopId:#,transType:#, status:#}",
                companyId, shopId, transactionType, transactionStatus).as(entityClazz);
    }

    @Override
    public Iterable<Transaction> getBracketSalesQB(String companyId, String shopId) {
        return coll.find("{companyId:#, shopId:#, status: {$in:[#,#,#]}}",
                companyId, shopId, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public Iterable<Transaction> getDailySalesforQB(String companyId, String shopId, long startDate, long endDate) {

        Iterable<Transaction> results = coll.find("{companyId:#, shopId:#, processedTime:{$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .as(entityClazz);

        return results;
    }

    @Override
    public DateSearchResult<Transaction> getAllTransactionsAssigned(String companyId, String shopId, String employeeId, long afterDate) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#,modified:{$gte:#}}", companyId, shopId, employeeId, afterDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,assignedEmployeeId:#,modified:{$gte:#}}", companyId, shopId, employeeId, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public DateSearchResult<Transaction> getSalesRefundsTransactions(String companyId, String shopId, long afterDate) {
        List<Transaction.TransactionType> transactionTypes = new ArrayList<>();
        transactionTypes.add(Transaction.TransactionType.Sale);
        transactionTypes.add(Transaction.TransactionType.Refund);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, transType:{$in:#}, modified: {$gte:#}}", companyId, shopId,
                transactionTypes, afterDate).as(entityClazz);


        long count = coll.count("{companyId:#,shopId:#, transType:{$in:#}, modified: {$gte:#}}", companyId, shopId,
                transactionTypes, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(DateTime.now().getMillis());
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public Iterable<Transaction> getAllBracketSales(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory, Transaction.TransactionStatus.Canceled)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public List<Transaction> getSalesOrdersListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<Transaction> result = coll.find("{companyId:#, shopId:#, modified:{$lt:#, $gt:#},active:true,qbSalesReceiptRef: {$exists: false} }"
                , companyId, shopId, beforeDate, afterDate).as(Transaction.class);
        return Lists.newArrayList(result);
    }


    @Override
    public List<Transaction> getRefundReceiptsListWithoutQbRef(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<Transaction> result = coll.find("{companyId:#, shopId:#,modified:{$lt:#, $gt:#}, active:true,qbRefundReceiptRef: {$exists: false} }"
                , companyId, shopId, beforeDate, afterDate).as(Transaction.class);
        return Lists.newArrayList(result);
    }


    @Override
    @Deprecated
    public WriteResult updateJournalEntryRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("transactions").update(query, field);
    }

    @Override
    public void updateOnFleetTaskStatus(String companyId, String shopId, String transactionId, Transaction.OnFleetTaskStatus status) {
        coll.update("{companyId:#,shopId:#,_id:#}", companyId, shopId, new ObjectId(transactionId))
                .with("{$set: {onFleetTaskStatus:#, modified:#}}", status, DateTime.now().getMillis());
    }

    @Override
    public Iterable<Transaction> listByShopWithDateAndStatus(String companyId, String shopId, long afterDate, long beforeDate, Transaction.TransactionType transactionType, Transaction.TransactionStatus transactionStatus) {
        Iterable<Transaction> transactionIterable = coll.find("{companyId:#,shopId:#, modified:{$lt:#, $gt:#},transType:#,status:#}", companyId, shopId, beforeDate, afterDate, transactionType, transactionStatus).as(entityClazz);
        return transactionIterable;

    }

    @Override
    public Iterable<Transaction> getBracketSalesQB(String companyId, String shopId, long startDate, long endDate) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}}, deleted: false",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public SearchResult<Transaction> getActiveTransactionsByTerm(String companyId, String shopId, int start, int limit, String term, long afterDate, long beforeDate) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Transaction> items = coll.find("{$and: [{companyId:#,shopId:#, active:true,modified:{$gte:#,$lte:#}},{$or: [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, pattern).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#, active:true,modified:{$gte:#,$lte:#}},{$or: [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, pattern);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public SearchResult<Transaction> getAllAssignedTransactionByTerm(String companyId, String shopId, int start, int limit, String term, boolean isAssigned, long afterDate, long beforeDate) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Transaction> items = null;
        long count;

        if (isAssigned) {
            items = coll.find("{$and: [{companyId: #,shopId: #,active: true,deleted: false,modified:{$gte:#,$lte:#},$or: [{$and: [{assigned: {$exists: true}},{assigned:true}]},{$and :[{assignedEmployeeId: {$exists: true} },{ assignedEmployeeId: {$ne: ''} }]}]},{$or: [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, pattern).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);
            count = coll.count("{$and: [{companyId: #,shopId: #,active: true,deleted: false,modified:{$gte:#,$lte:#},$or: [{$and: [{assigned: {$exists: true}},{assigned:true}]},{$and :[{assignedEmployeeId: {$exists: true} },{ assignedEmployeeId: {$ne: ''} }]}]},{$or: [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, pattern);
        } else {
            items = coll.find("{$and: [{companyId: #,shopId: #,active: true,deleted: false,modified:{$gte:#,$lte:#},$and: [{$and: [{assigned: {$exists: true}},{assigned:false}]},{$or :[{assignedEmployeeId: {$exists: false} },{ assignedEmployeeId: {$eq: ''} }]}]},{$or: [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, pattern).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);
            count = coll.count("{$and: [{companyId: #,shopId: #,active: true,deleted: false,modified:{$gte:#,$lte:#},$and: [{$and: [{assigned: {$exists: true}},{assigned:false}]},{$or :[{assignedEmployeeId: {$exists: false} },{ assignedEmployeeId: {$eq: ''} }]}]},{$or: [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, pattern);
        }
        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getAllAssignedTransaction(String companyId, String shopId, int start, int limit, boolean isAssigned, long afterDate, long beforeDate) {
        Iterable<Transaction> items = null;
        long count;

        if (isAssigned) {
            items = coll.find("{companyId: #,shopId: #,active: true,modified:{$gte:#,$lte:#},deleted: false,$or: [{$and: [{assigned: {$exists: true}},{assigned:true}]},{$and :[{assignedEmployeeId: {$exists: true} },{ assignedEmployeeId: {$ne: ''} }]}]}", companyId, shopId, afterDate, beforeDate).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);
            count = coll.count("{companyId: #,shopId: #,active: true,modified:{$gte:#,$lte:#},deleted: false,$or: [{$and: [{assigned: {$exists: true}},{assigned:true}]},{$and :[{assignedEmployeeId: {$exists: true} },{ assignedEmployeeId: {$ne: ''} }]}]}", companyId, shopId, afterDate, beforeDate);
        } else {
            items = coll.find("{companyId: #,shopId: #,active: true,modified:{$gte:#,$lte:#},deleted: false,$and: [{$and: [{assigned: {$exists: true}},{assigned:false}]},{$or :[{assignedEmployeeId: {$exists: false} },{ assignedEmployeeId: {$eq: ''} }]}]}", companyId, shopId, afterDate, beforeDate).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);
            count = coll.count("{companyId: #,shopId: #,active: true,modified:{$gte:#,$lte:#},deleted: false,$and: [{$and: [{assigned: {$exists: true}},{assigned:false}]},{$or :[{assignedEmployeeId: {$exists: false} },{ assignedEmployeeId: {$eq: ''} }]}]}", companyId, shopId, afterDate, beforeDate);
        }

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public long countTransactionByStatus(String companyId, String shopId, List<Transaction.TransactionStatus> statuses, boolean isActive) {
        return coll.count("{ companyId: #,shopId:#,active : #,deleted : false,status :{ $in :#}}", companyId, shopId, isActive, statuses);
    }

    @Override
    public long countTransactionsByAssignStatus(String companyId, String shopId, boolean isAssigned) {
        if (isAssigned) {
            return coll.count("{companyId: #,shopId: #,active: true,deleted: false,$or: [{$and: [{assigned: {$exists: true}},{assigned:true}]},{$and :[{assignedEmployeeId: {$exists: true} },{ assignedEmployeeId: {$ne: ''} }]}]}", companyId, shopId);
        } else {
            return coll.count("{companyId: #,shopId: #,active: true,deleted: false,$and: [{$and: [{assigned: {$exists: true}},{assigned:false}]},{$or :[{assignedEmployeeId: {$exists: false} },{ assignedEmployeeId: {$eq: ''} }]}]}", companyId, shopId);
        }
    }

    @Override
    public SearchResult<Transaction> getCompletedTransactionsByTerm(String companyId, String shopId, String term, int start, int limit, long afterDate, long beforeDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Transaction> items = coll.find("{ $and : [{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}} , {$or : [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, statuses, pattern)
                .sort("{processedTime:-1}").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{ $and : [{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}} , {$or : [{transNo:#}]}]}", companyId, shopId, afterDate, beforeDate, statuses, pattern);

        SearchResult<Transaction> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        return searchResult;
    }

    @Override
    public SearchResult<Transaction> getCompletedTransactionsByDate(String companyId, String shopId, int start, int limit, long afterDate, long beforeDate) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed,
                Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}}", companyId, shopId, afterDate, beforeDate, statuses)
                .sort("{processedTime:-1}").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,processedTime:{$gte:#,$lte:#}, status: {$in: #}}", companyId, shopId, afterDate, beforeDate, statuses);

        SearchResult<Transaction> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        return searchResult;
    }

    @Override
    public SearchResult<Transaction> getActiveTransactionByDate(String companyId, String shopId, int start, int limit, long afterDate, long beforeDate) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, deleted : false,active:true, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).sort("{priority:-1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#, active:true,deleted:false, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<Transaction> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public MemberSalesDetail getSalesByMember(String companyId, String shopId, String memberId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();
        List<Transaction.TransactionStatus> status = Lists.newArrayList(Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory
                , Transaction.TransactionStatus.RefundWithoutInventory);
        Aggregate.ResultsIterator<MemberSalesDetail> results = coll.aggregate("{$match: { companyId: #, shopId: #, status: { $in:# }}}"
                , companyId, shopId, status)
                .and("{$unwind: '$cart.items'}")
                .and("{$group: {  _id:'$memberId',purchaseProducts:{$push:{productId:'$cart.items.productId',quantity:'$cart.items.quantity'}},totalVisit:{$sum:1},total:{$sum:{" +
                        " $cond : [ {$and:[{$eq:['$transType','Refund']},{$eq:['$cart.refundOption','Retail']}]}," +
                        " {$multiply:['$cart.total',-1]},{$multiply:['$cart.total',1]}]}}}}")
                .and("{ $match: { _id : #}}", memberId)
                .options(aggregationOptions)
                .as(MemberSalesDetail.class);

        List<MemberSalesDetail> list = Lists.newArrayList((Iterable<MemberSalesDetail>) results);
        if (!list.isEmpty())
            return list.get(0);
        return new MemberSalesDetail();

    }

    @Override
    public void updateTookanInfo(String companyId, String transactionId, String tookanJobId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transactionId)).with("{$set:{tookanTaskId:#,modified:#}}", tookanJobId, DateTime.now().getMillis());
    }

    @Override
    public Iterable<Transaction> getTransactionByTypes(String companyId, String shopId, Long startDate, Long endDate, List<Transaction.TransactionType> transactionTypeList) {
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, transType: {$in: #}}",
                companyId, shopId, startDate, endDate, transactionTypeList)
                .sort("{processedTime: -1}")
                .as(Transaction.class);
    }

    @Override
    public void updateTookanTransaction(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#,active:true,tookanTaskId:{$exists:true,$ne:''}}", companyId, shopId).multi().with("{$set:{tookanTaskId:#,createTookanTask:#,modified:#}}", StringUtils.EMPTY, false, DateTime.now().getMillis());
    }

    @Override
    public Iterable<MemberPerformanceBySale> getSalesByMemberPerformance(String companyId, String shopId, Long startDate, Long endDate) {

        Iterable<MemberPerformanceBySale> items = coll.find("{companyId:#, shopId:#, processedTime: {$gt:# , $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory)
                .projection("{memberId:1,cart.total:1, status:1}")
                .as(MemberPerformanceBySale.class);

        return items;
    }

    @Override
    public SearchResult<Transaction> getTransactionsByFulFillmentStepAndQueue(String companyId, String shopId, Transaction.FulfillmentStep fulfillmentStep, Transaction.QueueType queueType, int start, int limit) {
        Iterable<Transaction> items = coll.find("{companyId:#, shopId:#, active:#, queueType:#, fulfillmentStep:#}", companyId, shopId, Boolean.TRUE, queueType, fulfillmentStep)
                .projection("{memberId:1,cart:1, status:1, created:1, queueType:1, fulfillmentStep:1, modified:1, transNo :1, checkinTime:1}")
                .sort("{checkinTime:1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{companyId:#, shopId:#, active:#, queueType:#, fulfillmentStep:#}", companyId, shopId, Boolean.TRUE, queueType, fulfillmentStep);

        ArrayList<Transaction> transactions = Lists.newArrayList(items);
        SearchResult<Transaction> searchResult = new SearchResult<>();
        searchResult.setValues(transactions);
        searchResult.setTotal(count);
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        return searchResult;
    }

    @Override
    public long getBracketSalesCount(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.count("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:[#,#,#]}}",
                companyId, shopId, startDate, endDate, Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
    }

    @Override
    public void updateOrderTags(String companyId, String transactionId, Set<String> orderTags) {
        coll.update("{companyId:#, _id:#}", companyId, new ObjectId(transactionId)).with("{$set:{orderTags:#,modified:#}}", orderTags, DateTime.now().getMillis());
    }

    @Override
    public <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbDesktopRef(String companyId, String shopId, long syncTime, int start, int limit, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz) {
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, transType:#, status:#, qbDesktopSalesRef:{$exists: false}, qbErrored:false, processedTime:{$gt:#}}", companyId, shopId, type, status, syncTime)
                .sort("{processedTime:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(transactions));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbDesktopRef(String companyId, String shopId, long startDate, long endDate, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz) {
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, processedTime: {$gt:#, $lt:#}, transType:#, status:#, qbDesktopSalesRef:{$exists: false}}", companyId, shopId, startDate, endDate, type, status)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(transactions));
        return results;
    }

    @Override
    public void updateQbDesktopSalesRef(String companyId, String shopId, String transNo, String txnId) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyId, shopId, transNo).with("{$set:{qbDesktopSalesRef:#, txnId:#}}", transNo, txnId);
    }

    @Override
    public <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutJournalEntryRef(String companyId, String shopId, int start, int limit, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz) {
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, transType:#, status:#, qbDesktopJournalEntryRef:{$exists: false}}", companyId, shopId, type, status)
                .sort("{processedTime:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(transactions));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutJournalEntryRef(String companyId, String shopId, long startDate, long endDate, Transaction.TransactionType type, Transaction.TransactionStatus status, Class<E> clazz) {
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, processedTime: {$gt:#, $lt:#}, transType:#, status:#, qbDesktopJournalEntryRef:{$exists: false}}", companyId, shopId, startDate, endDate, type, status)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(transactions));
        return results;
    }

    @Override
    public void updateQbDesktopJournalEntryRef(String companyId, String shopId, String transNo) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyId, shopId, transNo).with("{$set:{qbDesktopJournalEntryRef:#}}", transNo);
    }

    @Override
    public <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbRefundReceipt(String companyId, String shopId, int start, int limit, long syncTime, Transaction.TransactionType type, Class<E> clazz) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, transType:#, status: {$in:#}, qbDesktopRefundReceipt:{$exists: false}, qbRefundErrored:false, processedTime:{$gt:#} }", companyId, shopId, type, statuses, syncTime)
                .sort("{processedTime:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(transactions));
        results.setSkip(start);
        results.setLimit(limit);
        return results;
    }

    @Override
    public <E extends Transaction> SearchResult<E> getLimitedTransactionWithoutQbRefundReceipt(String companyId, String shopId, long startDate, long endDate, Transaction.TransactionType type, Class<E> clazz) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory);
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, processedTime: {$gt:#, $lt:#}, transType:#, status: {$in:#}, qbDesktopRefundReceipt:{$exists: false}, qbRefundErrored:false}", companyId, shopId, startDate, endDate, type, statuses)
                .as(clazz);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(transactions));
        return results;
    }

    @Override
    public void updateQbDesktopQbRefundReceipt(String companyId, String shopId, String transNo, String txnId) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyId, shopId, transNo).with("{$set:{qbDesktopRefundReceipt:#, txnId:#}}", transNo, txnId);
    }

    @Override
    public <E extends Transaction> List<E> getTransactionsByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed);
        Iterable<E> transactions =  coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopSalesRef:{$exists: false}, qbErrored:true, modified:{$gt:#}, transType:#,status: {$in:#}}", companyId, shopId, errorTime, Transaction.TransactionType.Sale,statuses)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(transactions);
    }

    @Override
    public void updateTransactionQbErrorAndTime(String companyID, String shopId, String transNo, boolean qbErrored, long errorTime) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyID, shopId, transNo).with("{$set: {qbErrored:#, errorTime:#}}", qbErrored, errorTime);
    }

    @Override
    public void updateTransactionQbRefundErrorAndTime(String companyID, String shopId, String transNo, boolean qbRefundErrored, long errorRefundTime) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyID, shopId, transNo).with("{$set: {qbRefundErrored:#, errorRefundTime:#}}", qbRefundErrored, errorRefundTime);

    }

    @Override
    public void hardRemoveQuickBookDataInTransactions(String companyId, String shopId) {
        coll.update("{companyId:#, shopId:#}", companyId, shopId).multi().with("{$unset: {txnId:1, qbDesktopSalesRef:1, qbDesktopJournalEntryRef:1, qbDesktopRefundReceipt:1, qbErrored:1, errorTime:1, qbRefundErrored:1, errorRefundTime:1, refundPayment:1, refundPaymentErrored:1, refundPaymentErrorTime:1}}");
    }

    @Override
    public void updatePickupDate(String id, long completeAfter, long pickupDate) {
        coll.update("{_id:#}", new ObjectId(id)).with("{$set:{completeAfter:#,pickUpDate:#,modified:#}}", completeAfter,pickupDate, DateTime.now().getMillis());
    }

    @Override
    public void updateDeliveryDate(String id, long completeAfter, long deliveryDate) {
        coll.update("{_id:#}", new ObjectId(id)).with("{$set:{completeAfter:#,deliveryDate:#,modified:#}}",completeAfter, deliveryDate, DateTime.now().getMillis());
    }

    @Override
    public void updatePackedBy(String id, String packedBy, long packedDate) {
        coll.update("{_id:#}", new ObjectId(id)).with("{$set:{packedBy:#,packedDate:#,modified:#}}",packedBy, packedDate, DateTime.now().getMillis());
    }

    @Override
    public void updateDeliveryAddress(String id, Address address) {
        coll.update("{_id:#}", new ObjectId(id)).with("{$set:{deliveryAddress:#,modified:#}}", address, DateTime.now().getMillis());
    }

    @Override
    public long getAllActiveTransactionsCountForTerminal(String companyId, String shopId, String terminalId) {
        return coll.count("{companyId:#, shopId:#, active:#, sellerTerminalId:#}", companyId, shopId, true, terminalId);
    }

    @Override
    public SearchResult<Transaction> getActiveTransactionsForMember(String companyId, String shopId, String memberId) {
        Iterable<Transaction> items = coll.find("{companyId:#, shopId:#, memberId:#, active:#}", companyId, shopId, memberId, true).as(entityClazz);
        long count = coll.count("{companyId:#, shopId:#, memberId:#, active:#}", companyId, shopId, memberId, true);

        SearchResult<Transaction> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);

        return result;
    }
    @Override
    public void updateActiveOrderTags(String companyId, List<String> memberIds, List<String> tags, List<String> removeTags) {
        if (!removeTags.isEmpty()) {
            coll.update("{companyId:#, active:true, memberId : {$in:#}}", companyId, memberIds).multi().with("{$pullAll:{orderTags:#}, $set: {modified: #}}", removeTags, DateTime.now().getMillis());
        }
        if (!tags.isEmpty()) {
            coll.update("{companyId:#, active:true, memberId : {$in:#}}", companyId, memberIds).multi().with("{$addToSet:{orderTags:{$each:#}}, $set: {modified: #}}", tags, DateTime.now().getMillis());
        }
    }

    @Override
    public long countTransactionsByProductId(String companyId, String shopId, List<String> productIds) {
        return coll.count("{companyId: #, shopId: #, status :{ $in:['Queued','Hold','InProgress']}, cart.items.productId:{$in:#}}", companyId, shopId, productIds);
    }
    public void saveMtracTxn(String companyId, String shopId, String transactionId, String mtracTransaction) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(transactionId)).with("{$set:{mtracTxnId:#,modified:#}}", mtracTransaction, DateTime.now().getMillis());
    }

    @Override
    public void saveCloverTxn(String companyId, String shopId, String transactionId, String cloverTransaction) {
        coll.update("{companyId:#, shopId:#, _id:#}", companyId, shopId, new ObjectId(transactionId)).with("{$set:{cloverTxnId:#,modified:#}}", cloverTransaction, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<Transaction> getAllTransactions(String companyId, String shopId, int start, int limit, long afterDate, long beforeDate) {
        Iterable<Transaction> items = coll.find("{$and: [{companyId:#,shopId:#, deleted:false, modified:{$gte:#,$lte:#}}]}", companyId, shopId, afterDate, beforeDate).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#, deleted:false, modified:{$gte:#,$lte:#}}]}", companyId, shopId, afterDate, beforeDate);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }



    @Override
    public Iterable<Transaction> getBracketSalesByEmployee(String companyId, String shopId, String employeeId, Long startDate, Long endDate) {
        List<Transaction.TransactionStatus> status = Lists.newArrayList(Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory,
                Transaction.TransactionStatus.RefundWithoutInventory, Transaction.TransactionStatus.Canceled);
        return coll.find("{companyId:#, shopId:#, processedTime: {$gt:#, $lt:#}, status: {$in:#}, sellerId:#, deleted:#}",
                companyId, shopId, startDate, endDate, status, employeeId, false)
                .sort("{processedTime: -1}")
                .as(Transaction.class);

    }
    @Override
    public void updatePaymentOption(String companyId, String transactionId, Cart cart) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(transactionId))
                .with("{$set: {modified:#, cart:#, processedTime:#}}", DateTime.now().getMillis(), cart, DateTime.now().getMillis());
    }

    @Override
    public void updateAllDeliveryAddress(String companyId, String memberId, Address address) {
        coll.update("{companyId:#, memberId:#, active:true}", companyId, memberId).multi().with("{$set: {deliveryAddress:#, addressUpdate: true, modified:#}}", address, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<Transaction> getAllTransactionsAssignedByStatus(String companyId, String shopId, String employeeId, List<Transaction.TransactionStatus> statuses, int start, int limit, String searchTerm) {
        Iterable<Transaction> items;
        long count;
        if(StringUtils.isNotBlank(searchTerm)) {
            Pattern pattern = TextUtil.createPattern(searchTerm);
           items = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#, status: {$in:#}, transNo:#}", companyId, shopId, employeeId, statuses, pattern).skip(start).limit(limit).as(entityClazz);
           count = coll.count("{companyId:#,shopId:#,assignedEmployeeId:#, status: {$in:#}, transNo:#}", companyId, shopId, employeeId, statuses, pattern);
        } else {
             items = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#, status: {$in:#}}", companyId, shopId, employeeId, statuses).skip(start).limit(limit).as(entityClazz);
             count = coll.count("{companyId:#,shopId:#,assignedEmployeeId:#, status: {$in:#}}", companyId, shopId, employeeId, statuses);
        }
        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }


    @Override
    public <E extends Transaction> List<E> getRefundTransactionByLimitsWithQBRefundPaymentError(String companyId, String shopId, long startTime, long endTime, Class<E> clazz) {
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRefundReceipt:{$exists: true}, refundPayment:false, refundPaymentErrored:true, modified:{$gt:#, $lt:#}}", companyId, shopId, startTime, endTime)
                .sort("{processedTime:-1}")
                .as(clazz);
        return Lists.newArrayList(transactions);
    }

    @Override
    public <E extends Transaction> List<E> getLimitedTransactionWithoutRefundPayment(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> transactions  = coll.find("{companyId:#, shopId:#, deleted:false, qbDesktopRefundReceipt:{$exists: true}, refundPayment:false, refundPaymentErrored:false}", companyId, shopId)
                .sort("{processedTime:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(transactions);
    }

    @Override
    public void updateTransactionQbRefundPayment(String companyId, String shopId, String transNo, boolean refundPayment) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyId, shopId, transNo).with("{$set: {refundPayment:#}}", refundPayment);

    }

    @Override
    public void updateTransactionQbRefundPaymentErrorAndTime(String companyID, String shopId, String transNo, boolean refundPayment, boolean refundPaymentErrored, long refundPaymentErrorTime) {
        coll.update("{companyId:#, shopId:#, transNo:#}", companyID, shopId, transNo).with("{$set: {refundPayment:#, refundPaymentErrored:#, refundPaymentErrorTime:#}}", refundPayment, refundPaymentErrored, refundPaymentErrorTime);
    }

    @Override
    public SearchResult<Transaction> getAllEmployeeTransactionsWithStatus(String companyId, String shopId, String employeeId,  List<Transaction.TransactionStatus> statuses, long afterDate, long beforeDate) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#,assignedEmployeeId:#, status: {$in:#},modified:{$lt:#, $gt:#}}", companyId, shopId, employeeId, statuses, beforeDate, afterDate).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,assignedEmployeeId:#, status: {$in:#}, modified:{$lt:#, $gt:#}}", companyId, shopId, employeeId, statuses, beforeDate, afterDate);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setTotal(count);
        return results;
    }

    @Override
    public SearchResult<Transaction> getActiveTransactionsForQueue(String companyId, String shopId, String assignEmployeeId, Transaction.QueueType queueType) {
        Iterable<Transaction> items = coll.find("{companyId:#,shopId:#, active:#, assignedEmployeeId:#, queueType:#}", companyId, shopId, true, assignEmployeeId, queueType).sort("{priority:1}").as(entityClazz);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setTotal((long) results.getValues().size());
        return results;
    }

    @Override
    public SearchResult<Transaction> getActiveTransactionsByTerm(String companyId, String shopId, String employeeId, int start, int limit, String term, long afterDate, long beforeDate) {
        Pattern pattern = TextUtil.createPattern(term);
        Iterable<Transaction> items = coll.find("{$and: [{companyId:#,shopId:#, active:true, assignedEmployeeId:#, modified:{$gte:#,$lte:#}},{$or: [{transNo:#}]}]}", companyId, shopId, employeeId, afterDate, beforeDate, pattern).sort("{ priority:1}").skip(start).limit(limit).as(entityClazz);

        long count = coll.count("{$and: [{companyId:#,shopId:#, active:true, assignedEmployeeId:#, modified:{$gte:#,$lte:#}},{$or: [{transNo:#}]}]}", companyId, shopId, employeeId, afterDate, beforeDate, pattern);

        SearchResult<Transaction> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public long countTransactionByStatus(String companyId, String shopId, String employeeId, List<Transaction.TransactionStatus> status, boolean active) {
        return coll.count("{ companyId: #,shopId:#,active : #,deleted : false,status :{ $in :#}, assignedEmployeeId:#}", companyId, shopId, active, status, employeeId);
    }
}
