package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.UniqueSequenceRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * The ShareIdentifierRepositoryImpl object.
 * User: mdo
 * Date: 4/2/15
 * Time: 4:14 PM
 */
public class UniqueSequenceRepositoryImpl extends ShopBaseRepositoryImpl<UniqueSequence> implements UniqueSequenceRepository {

    @Inject
    public UniqueSequenceRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(UniqueSequence.class, mongoManager);
    }

    @Override
    public UniqueSequence getNewIdentifier(String companyId, String shopId, String name) {
        UniqueSequence sequence = coll.findAndModify("{companyId:#,shopId:#,name:#}", companyId, shopId, name)
                .with("{$set:{companyId:#,shopId:#,name:#,modified:#},$inc:{count:1}}", companyId, shopId, name, DateTime.now().getMillis())
                .upsert().returnNew().as(UniqueSequence.class);
        return sequence;
    }
}
