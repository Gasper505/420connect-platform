package com.fourtwenty.core.domain.repositories.dispensary.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.quickbook.QBDataMapping;
import com.fourtwenty.core.reporting.model.reportmodels.CustomerByCurrentMonth;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Aggregate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Stephen Schmidt on 10/12/2015.
 */
public class VendorRepositoryImpl extends CompanyBaseRepositoryImpl<Vendor> implements VendorRepository {

    @javax.inject.Inject
    public VendorRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Vendor.class, mongoManager);
    }

    @Override
    public void removeById(String companyId, String entityId) {
        coll.update("{companyId:#,_id:#}", companyId, new ObjectId(entityId)).with("{$set: {deleted:true,active:false,modified:#}}", DateTime.now().getMillis());
    }

    public long countVendorByVendorName(String companyId, String vendorName) {
        Pattern pattern = Pattern.compile("^" + vendorName + "$", Pattern.CASE_INSENSITIVE);
        return coll.count("{companyId:#,name:#,deleted:false}", companyId, pattern);
    }

    @Override
    public List<Vendor> getVendorList(String companyId, String shopId) {
        Iterable<Vendor> result = coll.find("{companyId:#, shopId:#}"
                , companyId, shopId).as(Vendor.class);
        return Lists.newArrayList(result.iterator());
    }

    @Override
    public Iterable<Vendor> getVendorsByBrand(String companyId, String brandId) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<Vendor> results = coll.aggregate("{$match: {companyId:#,brands:#}}", companyId, brandId)
                .options(aggregationOptions)
                .as(Vendor.class);
        return results;
    }

    @Override
    public <E extends Vendor> SearchResult<E> findItemsWithSort(String companyId, String sortOptions, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,deleted:false}", companyId).sort(sortOptions).skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,deleted:false}", companyId);

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(start);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public void bulkRemoveBrand(String companyId, List<String> brands) {
        coll.update("{companyId:#, brands: {$in:#}}", companyId, brands).multi().with("{$pull: {brands:{$in:#}},$set: {modified:#}}", brands, DateTime.now().getMillis());
    }

    @Override
    @Deprecated
    public WriteResult updateVendorRef(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("vendors").update(query, field);
    }

    @Override
    public void bulkUpdateStatus(String companyId, List<ObjectId> vendorIds, boolean active) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, vendorIds).multi().with("{$set: {active:#, modified:#}}", active, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateBackOrderEnabled(String companyId, List<ObjectId> vendorIds, boolean backOrderEnabled) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, vendorIds).multi().with("{$set: {backOrderEnabled:#, modified:#}}", backOrderEnabled, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateTax(String companyId, List<ObjectId> vendorIds, Vendor.ArmsLengthType armsLengthType) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, vendorIds).multi().with("{$set: {armsLengthType:#, modified:#}}", armsLengthType, DateTime.now().getMillis());
    }

    @Override
    public void bulkUpdateBrands(String companyId, List<ObjectId> vendorIds, LinkedHashSet<String> brands) {
        coll.update("{companyId:#, _id: {$in:#}}", companyId, vendorIds).multi().with("{$set: {brands:#, modified:#}}", brands, DateTime.now().getMillis());
    }

    @Override
    public <E extends Vendor> SearchResult<E> findItemsByVendorAndCompanyType(String companyId, String sort, int skip, int limit, Vendor.VendorType vendorType, List<Vendor.CompanyType> companyType, Class<E> clazz) {

        List<Vendor.VendorType> vendorTypes = new ArrayList<>();
        Vendor.VendorType reqVendorType = vendorType;
        if (vendorType == null) {
            reqVendorType = Vendor.VendorType.VENDOR;
        }
        vendorTypes.add(reqVendorType);
        vendorTypes.add(Vendor.VendorType.BOTH);

        Iterable<E> items = null;
        long count = 0;
        if (companyType != null && !companyType.isEmpty()) {
            if (Vendor.VendorType.BOTH == reqVendorType || vendorType == null) {
                items = coll.find("{companyId:#, deleted:false,companyLicenses.companyType: {$in :#}}", companyId, companyType).sort(sort).skip(skip).limit(limit).as(clazz);
                count = coll.count("{companyId:#, deleted:false,companyLicenses.companyType: {$in :#}}", companyId, companyType);
            } else if (Vendor.VendorType.VENDOR == reqVendorType) {
                items = coll.find("{companyId:#, deleted:false, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}],companyLicenses.companyType : {$in :#}}", companyId, vendorTypes, companyType).sort(sort).skip(skip).limit(limit).as(clazz);
                count = coll.count("{companyId:#, deleted:false, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}],companyLicenses.companyType : {$in :#}}", companyId, vendorTypes, companyType);
            } else {
                Vendor.CompanyType type = companyType.get(0);
                if (type != null) {
                    items = coll.find("{companyId:#, deleted:false, vendorType:{ $in:#},companyLicenses.companyType : {$in :#}}", companyId, vendorTypes, companyType).sort(sort).skip(skip).limit(limit).as(clazz);
                    count = coll.count("{companyId:#, deleted:false, vendorType:{ $in:#},companyLicenses.companyType : {$in :#}}", companyId, vendorTypes, companyType);
                } else {
                    items = coll.find("{companyId:#, deleted:false, vendorType:{ $in:#}}", companyId, vendorTypes).sort(sort).skip(skip).limit(limit).as(clazz);
                    count = coll.count("{companyId:#, deleted:false, vendorType:{ $in:#}}", companyId, vendorTypes);
                }
            }
        } else {
            if (Vendor.VendorType.BOTH == reqVendorType) {
                items = coll.find("{companyId:#, deleted:false}", companyId).sort(sort).skip(skip).limit(limit).as(clazz);
                count = coll.count("{companyId:#, deleted:false}", companyId);
            } else if (Vendor.VendorType.VENDOR == reqVendorType) {
                items = coll.find("{companyId:#, deleted:false, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}]}", companyId, vendorTypes).sort(sort).skip(skip).limit(limit).as(clazz);
                count = coll.count("{companyId:#, deleted:false, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}]}", companyId, vendorTypes);
            } else {
                items = coll.find("{companyId:#, deleted:false, vendorType:{ $in:#}}", companyId, vendorTypes).sort(sort).skip(skip).limit(limit).as(clazz);
                count = coll.count("{companyId:#, deleted:false, vendorType:{ $in:#}}", companyId, vendorTypes);
            }
        }
        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Vendor> SearchResult<E> findItemsByTerm(String companyId, String sort, int skip, int limit, Vendor.VendorType vendorType, String term, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        List<Vendor.VendorType> vendorTypes = new ArrayList<>();
        if (vendorType == null) {
            vendorType = Vendor.VendorType.VENDOR;
        }
        vendorTypes.add(vendorType);
        vendorTypes.add(Vendor.VendorType.BOTH);

        Iterable<E> items = null;
        long count = 0;
        if (Vendor.VendorType.BOTH == vendorType) {
            items = coll.find("{$and:[{companyId:#,deleted: false},{$or:[{$or:[{name:#},{$and:[{ dbaName:{$exists:true}},{dbaName:#}]}]},{companyLicenses.companyType:#},{companyLicenses.licenseType:#}]}]}", companyId, pattern, pattern, pattern,pattern).sort(sort).skip(skip).limit(limit).as(clazz);
            count = coll.count("{$and:[{companyId:#,deleted: false},{$or:[{$or:[{name:#},{$and:[{ dbaName:{$exists:true}},{dbaName:#}]}]},{companyLicenses.companyType:#},{companyLicenses.licenseType:#}]}]}", companyId, pattern, pattern, pattern,pattern);
        } else if (Vendor.VendorType.VENDOR == vendorType) {
            items = coll.find("{$and: [{companyId:#,deleted:false, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}]},{$or:[{ $or:[{name:#},{ $and: [{ dbaName:{$exists:true}},{dbaName:#}]}]},{companyLicenses.companyType:#},{companyLicenses.licenseType:#}]}]}", companyId, vendorTypes, pattern, pattern, pattern, pattern).sort(sort).skip(skip).limit(limit).as(clazz);
            count = coll.count("{$and: [{companyId:#,deleted:false, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}]},{$or:[{ $or:[{name:#},{ $and: [{ dbaName:{$exists:true}},{dbaName:#}]}]},{companyLicenses.companyType:#},{companyLicenses.licenseType:#}]}]}",
                    companyId, vendorTypes, pattern, pattern, pattern, pattern);
        } else {
            items = coll.find("{$and: [{companyId:#,deleted:false,vendorType:{ $in:#}},{$or:[{ $or:[{name:#},{ $and: [{ dbaName:{$exists:true}},{dbaName:#}]}]},{companyLicenses.companyType:#},{companyLicenses.licenseType:#}]}]}", companyId, vendorTypes, pattern, pattern, pattern, pattern).sort(sort).skip(skip).limit(limit).as(clazz);
            count = coll.count("{$and: [{companyId:#,deleted:false,vendorType:{ $in:#}},{$or:[{ $or:[{name:#},{ $and: [{ dbaName:{$exists:true}},{dbaName:#}]}]},{companyLicenses.companyType:#},{companyLicenses.licenseType:#}]}]}", companyId, vendorTypes, pattern, pattern, pattern, pattern);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public CustomerByCurrentMonth getCustomerByCurrentMonth(String companyId, long start, long end) {
        AggregationOptions aggregationOptions = AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();

        Aggregate.ResultsIterator<CustomerByCurrentMonth> customerByCurrentMonthResult = coll.aggregate("{$match: {companyId:# , created :{$gt:#,$lt:#} }}", companyId, start, end)
                .and("{$group: {_id:'$companyId',count:{$sum:1}}}")
                .options(aggregationOptions)
                .as(CustomerByCurrentMonth.class);

        ArrayList<CustomerByCurrentMonth> customerByCurrentMonthList = Lists.newArrayList((Iterable<CustomerByCurrentMonth>) customerByCurrentMonthResult);
        for (CustomerByCurrentMonth custByMonth : customerByCurrentMonthList) {
            return custByMonth;
        }

        return new CustomerByCurrentMonth();
    }

    @Override
    public <E extends CompanyBaseModel> SearchResult<E> findVendorsByTerm(String companyId, String shopId, String term, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        SearchResult<E> searchResult = new SearchResult<>();
        Iterable<E> vendors = coll.find("{$and: [{companyId:#,deleted:false},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]}]}]}", companyId, pattern,pattern).as(clazz);
        long count = coll.count("{$and: [{companyId:#,deleted:false},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]}]}]}", companyId, pattern,pattern);
        searchResult.setValues(Lists.newArrayList(vendors));
        searchResult.setSkip(0);
        searchResult.setLimit(Integer.MAX_VALUE);
        searchResult.setTotal(count);
        return searchResult;


    }

    @Override
    public <E extends CompanyBaseModel> SearchResult<E> findItemsByCompanyTypeAndTerm(String companyId, String sort, int skip, int limit, Vendor.VendorType vendorType, List<Vendor.CompanyType> companyType, String term, Class<E> clazz) {
        Pattern pattern = TextUtil.createPattern(term);
        List<Vendor.VendorType> vendorTypes = new ArrayList<>();
        if (vendorType == null) {
            vendorType = Vendor.VendorType.VENDOR;
        }
        vendorTypes.add(vendorType);
        vendorTypes.add(Vendor.VendorType.BOTH);

        Iterable<E> items = null;
        long count = 0;
        if (Vendor.VendorType.BOTH == vendorType) {
            items = coll.find("{$and: [{companyId:#,deleted:false,companyLicenses.companyType:{$in:#}},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]}, {companyLicenses.licenseType:#}]}]}", companyId, companyType, pattern, pattern,pattern).sort(sort).skip(skip).limit(limit).as(clazz);
            count = coll.count("{$and: [{companyId:#,deleted:false,companyLicenses.companyType:{$in:#}},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]},{companyLicenses.licenseType:#}]}]}", companyId, companyType, pattern, pattern,pattern);
        } else if (Vendor.VendorType.VENDOR == vendorType) {
            items = coll.find("{$and: [{companyId:#,deleted:false,companyLicenses.companyType:{$in:#}, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}]},{$or:[{name:#}, {$and:[{dbaName:{$exists:true}},{dbaName:#}]},{companyLicenses.licenseType:#}]}]}",
                    companyId, companyType, vendorTypes, pattern, pattern, pattern).sort(sort).skip(skip).limit(limit).as(clazz);
            count = coll.count("{$and: [{companyId:#,deleted:false,companyLicenses.companyType:{$in:#}, $or:[{vendorType:{$exists : false}},{vendorType:{ $in:#}}]},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]},{companyLicenses.licenseType:#}]}]}",
                    companyId, companyType, vendorTypes, pattern, pattern, pattern);
        } else {
            items = coll.find("{$and: [{companyId:#,deleted:false,companyLicenses.companyType:{$in:#}, vendorType:{ $in:#}},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]},{companyLicenses.licenseType:#}]}]}", companyId, companyType, vendorTypes, pattern, pattern, pattern).sort(sort).skip(skip).limit(limit).as(clazz);
            count = coll.count("{$and: [{companyId:#,deleted:false,companyLicenses.companyType:{$in:#}, vendorType:{ $in:#}},{$or:[{name:#},{$and:[{dbaName:{$exists:true}},{dbaName:#}]}, {companyLicenses.licenseType:#}]}]}", companyId, companyType, vendorTypes, pattern, pattern, pattern);
        }

        SearchResult<E> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public List<Vendor> getVendorListWithoutQbRef(String companyId, long afterDate, long beforeDate) {
        Iterable<Vendor> result = coll.find("{companyId:#,modified:{$lte:#, $gte:#},qbVendorRef: {$exists: false} }"
                , companyId, beforeDate, afterDate).as(Vendor.class);
        return Lists.newArrayList(result);
    }

    @Override
    public void updateVendorRef(String companyID, String id, String qbDesktopRef, String editSequence, String qbListId) {
        coll.update("{companyId:#,_id:#}", companyID, new ObjectId(id)).with("{$set: {qbDesktopRef:#, editSequence:#, qbListId:#}}", qbDesktopRef, editSequence, qbListId);
    }

    @Override
    public HashMap<String, Vendor> getLimitedVendorViewAsMap(String companyId) {
        HashMap<String, Vendor> vendorMap = new HashMap<>();
        Iterable<Vendor> vendors = coll.find("{companyId:#, deleted:false}", companyId).projection("{_id:1, name:1}").as(entityClazz);
        for (Vendor vendor : vendors) {
            vendorMap.put(vendor.getId(), vendor);
        }
        return vendorMap;
    }

    @Override
    public Iterable<Vendor> findItems(int start, int limit, String projection) {
        return coll.find().sort("{created:1}").skip(start).limit(limit).projection(projection).as(entityClazz);
    }


    @Override
    public void updateCompanyLicense(String id, List<CompanyLicense> companyLicenses) {
        coll.update("{_id:#}", new ObjectId(id)).with("{$set : {companyLicenses:#,modified:#}}", companyLicenses, DateTime.now().getMillis());
    }

    @Override
    public SearchResult<Vendor> findItemsWithDate(String companyId, long startTime, long endTime, int skip, int limit, String sortOptions) {
        if (startTime < 0) startTime = 0;
        if (endTime <= 0) endTime = DateTime.now().getMillis();

        Iterable<Vendor> items = coll.find("{companyId:#,deleted:false,modified:{$gt:#, $lt:#}}", companyId, startTime, endTime).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);

        long count = coll.count("{companyId:#,deleted:false,modified:{$gt:#, $lt:#}}", companyId, startTime, endTime);

        SearchResult<Vendor> results = new SearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setSkip(skip);
        results.setLimit(limit);
        results.setTotal(count);
        return results;
    }

    @Override
    public <E extends Vendor> List<E> getVendorsByLimits(String companyId, int start, int limit, Class<E> clazz) {
        Iterable<E> vendors = coll.find("{companyId:#, deleted:false, qbMapping:{$exists: false}}", companyId)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(vendors);
    }

    @Override
    public <E extends Vendor> List<E> getVendorsByLimits(String companyId, String shopId, int start, int limit, Class<E> clazz) {
        Iterable<E> vendors = coll.find("{companyId:#, deleted:false, qbMapping:{$exists: true}, qbMapping.shopId : {$ne:#}}", companyId, shopId)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(vendors);
    }

    @Override
    public <E extends Vendor> List<E> getVendorsByLimitsWithQBError(String companyId, String shopId, long errorTime, Class<E> clazz) {
        Iterable<E> vendors =  coll.find("{companyId:#, deleted:false, qbMapping:{$exists:true}, qbMapping.shopId:#, qbMapping.qbDesktopRef:{$exists: false}, qbMapping.errorTime:{$exists:true}, modified:{$gt:#}}", companyId, shopId, errorTime)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(vendors);
    }

    @Override
    public HashMap<String, Vendor> getNonDeletedVendors(String companyId, List<ObjectId> vendorIds) {
        HashMap<String, Vendor> vendorMap = new HashMap<>();
        Iterable<Vendor> vendors = coll.find("{companyId:#, deleted:false, _id:{ $in:# }}", companyId, vendorIds).as(entityClazz);
        for (Vendor vendor : vendors) {
            vendorMap.put(vendor.getId(), vendor);
        }
        return vendorMap;
    }

    @Override
    public <E extends Vendor> List<E> getQBExistVendors(String companyId, String shopId, int start, int limit, long endTime, Class<E> clazz) {
        Iterable<E> vendors = coll.find("{companyId:#, deleted:false, qbMapping:{$exists:true}, qbMapping.shopId:#, qbMapping.qbDesktopRef:{$exists: true},modified:{$gt:#}, qbMapping.editSequence:{$exists: true}, qbMapping.qbListId:{$exists: true}}", companyId, shopId, endTime)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(vendors);
    }

    @Override
    public void updateVendorEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef) {
        coll.update("{companyId:#, qbMapping:{$exists:true}, qbMapping.shopId:#, qbMapping.qbListId:#}", companyId, shopId, qbListId).with("{$set: {qbMapping.$[].editSequence:#, qbMapping.$[].qbDesktopRef:#}}", editSequence, desktopRef);
    }

    @Override
    public <E extends Vendor> List<E> getCustomerByLimitsWithQBError(String companyId, String shopId, Long errorTime, List<Vendor.VendorType> vendorTypes, Class<E> clazz) {
        Iterable<E> members =  coll.find("{companyId:#, deleted:false, qbCustomerMapping:{$exists:true}, qbCustomerMapping.shopId:#, qbCustomerMapping.qbDesktopRef:{$exists: false}, qbCustomerMapping.errorTime:{$exists:true}, modified:{$gt:#}, vendorType:{$in:#}}", companyId, shopId, errorTime, vendorTypes)
                .sort("{modified:-1}")
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public <E extends Vendor> List<E> getCustomerByLimitsWithoutQbDesktopRef(String companyId, int start, int limit, List<Vendor.VendorType> vendorTypes, Class<E> clazz) {
        Iterable<E> members = coll.find("{companyId:#, deleted:false, qbCustomerMapping:{$exists: false}, vendorType:{$in:#}}", companyId, vendorTypes)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public <E extends Vendor> List<E> getCustomerByLimitsWithoutQbDesktopRef(String companyId, String shopId, int start, int limit, List<Vendor.VendorType> vendorTypes, Class<E> clazz) {
        Iterable<E> vendors = coll.find("{companyId:#, deleted:false, qbCustomerMapping:{$exists: true}, qbCustomerMapping.shopId : {$ne:#}, vendorType:{$in:#}}", companyId, shopId, vendorTypes)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(vendors);
    }

    @Override
    public <E extends Vendor> List<E> getQBExistingCustomer(String companyId, String shopId, int start, int limit, long time, List<Vendor.VendorType> vendorTypes, Class<E> clazz) {
        Iterable<E> members = coll.find("{companyId:#, deleted:false, qbCustomerMapping:{$exists:true}, qbCustomerMapping.shopId:#, qbCustomerMapping.qbDesktopRef:{$exists: true},modified:{$gt:#}, qbCustomerMapping.editSequence:{$exists: true}, qbCustomerMapping.qbListId:{$exists: true}, vendorType:{$in:#}}", companyId, shopId, time, vendorTypes)
                .sort("{modified:-1}")
                .skip(start)
                .limit(limit)
                .as(clazz);
        return Lists.newArrayList(members);
    }

    @Override
    public void updateCustomerEditSequence(String companyId, String shopId, String qbListId, String editSequence, String desktopRef) {
        coll.update("{companyId:#, qbCustomerMapping:{$exists:true}, qbCustomerMapping.shopId:#, qbCustomerMapping.qbListId:#}", companyId, shopId, qbListId).with("{$set: {qbCustomerMapping.$[].editSequence:#, qbCustomerMapping.$[].qbDesktopRef:#}}", editSequence, desktopRef);
    }

    @Override
    public void updateVendorQbMapping(String companyID, String id, List<QBDataMapping> mapping) {
        coll.update("{companyId:#, _id:#}",companyID, new ObjectId(id)).with("{$set: {qbMapping:#}}", mapping);
    }

    @Override
    public void updateVendorQbCustomerMapping(String companyID, String id, List<QBDataMapping> mapping) {
        coll.update("{companyId:#, _id:#}",companyID, new ObjectId(id)).with("{$set: {qbCustomerMapping:#}}", mapping);
    }

    @Override
    public void hardQbMappingRemove(String companyId, String shopId) {
        coll.update("{companyId:#, qbMapping:{$exists:true}, qbMapping.shopId:#}", companyId, shopId).multi().with(" { $pull: { qbMapping: { shopId: # } } }", shopId);
    }

    @Override
    public void hardQbCustomerMappingRemove(String companyId, String shopId) {
        coll.update("{companyId:#, qbCustomerMapping:{$exists:true}, qbCustomerMapping.shopId:#}", companyId, shopId).multi().with(" { $pull: { qbCustomerMapping: { shopId: # } } }", shopId);

    }

    @Override
    public Vendor getDefaultVendor(String companyId) {
        return coll.findOne("{companyId:#, deleted:false, active:true, toDefault:true}", companyId).as(entityClazz);
    }
}
