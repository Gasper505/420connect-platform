package com.fourtwenty.core.domain.repositories.global;

import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

public interface ReportingInfoRepository extends BaseRepository<ReportingInfo> {
}
