package com.fourtwenty.core.domain.repositories.global;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.global.USState;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 3/26/18.
 */
public interface StateCannabisLimitRepository extends BaseRepository<StateCannabisLimit> {

    StateCannabisLimit getStateLimit(USState state, ConsumerType consumerType);

    StateCannabisLimit getStateCannabisLimit(ConsumerType consumerType, String state);
}
