package com.fourtwenty.core.domain.repositories.global.impl;

import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.domain.repositories.global.ReportingInfoRepository;
import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.google.inject.Inject;

public class ReportingInfoRepositoryImpl extends MongoBaseRepositoryImpl<ReportingInfo> implements ReportingInfoRepository {

    @Inject
    public ReportingInfoRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ReportingInfo.class, mongoManager);
    }
}
