package com.fourtwenty.core.domain.repositories.global.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.global.USState;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.global.StateCannabisLimitRepository;

import javax.inject.Inject;

/**
 * Created by mdo on 3/26/18.
 */
public class StateCannabisLimitRepositoryImpl extends MongoBaseRepositoryImpl<StateCannabisLimit> implements StateCannabisLimitRepository {

    @Inject
    public StateCannabisLimitRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(StateCannabisLimit.class, mongoManager);
    }

    @Override
    public StateCannabisLimit getStateLimit(USState state, ConsumerType consumerType) {
        return coll.findOne("{state:#,consumerType:#}", state, consumerType).as(entityClazz);
    }

    @Override
    public StateCannabisLimit getStateCannabisLimit(ConsumerType consumerType, String state) {
        return coll.findOne("{consumerType:#,state:#}", consumerType, state).as(entityClazz);
    }
}
