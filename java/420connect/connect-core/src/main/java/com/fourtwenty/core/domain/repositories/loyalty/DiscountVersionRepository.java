package com.fourtwenty.core.domain.repositories.loyalty;

import com.fourtwenty.core.domain.models.loyalty.*;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface DiscountVersionRepository extends MongoShopBaseRepository<DiscountVersion> {
    
    DiscountVersion findLastDiscountVersionByPromotionId(String companyId,String shopId, String promotionId);
    DiscountVersion findLastDiscountVersionByRewardId(String companyId,String shopId, String rewardId);
}
