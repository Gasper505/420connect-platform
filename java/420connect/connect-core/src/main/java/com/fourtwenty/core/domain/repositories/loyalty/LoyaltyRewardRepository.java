package com.fourtwenty.core.domain.repositories.loyalty;

import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import java.util.List;

/**
 * Created by mdo on 8/3/17.
 */
public interface LoyaltyRewardRepository extends MongoShopBaseRepository<LoyaltyReward> {
    boolean existsWithSlug(String companyId, String shopId, String slug);

    Iterable<LoyaltyReward> getPublishedRewards(String companyId, String shopId);

    LoyaltyReward getRewardByName(String companyId, String shopId, String name);

    Iterable<LoyaltyReward> getRewardByNameList(String companyId, String shopId, List<String> name);
}
