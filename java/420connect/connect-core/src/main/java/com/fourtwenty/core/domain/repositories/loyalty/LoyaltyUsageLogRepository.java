package com.fourtwenty.core.domain.repositories.loyalty;

import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 7/22/17.
 */
public interface LoyaltyUsageLogRepository extends MongoShopBaseRepository<LoyaltyActivityLog> {
    Iterable<LoyaltyActivityLog> getActivityLogsForMember(String companyId, String memberId, LoyaltyActivityLog.LoyaltyActivityType type);

    boolean hasEarnedForTransaction(String companyId, String shopId, String memberId, String transNo);

    boolean hasUsedForMemberReward(String companyId, String memberId, String rewardId);

    DateSearchResult<LoyaltyActivityLog> getUsageLogsForDate(String companyId, String shopId, long afterDate, long beforeDate);

    <E extends LoyaltyActivityLog> SearchResult<E> getUsageLogsForType(String companyId, String shopId,
                                                                       LoyaltyActivityLog.LoyaltyActivityType activityType,
                                                                       int start, int limit,
                                                                       Class<E> clazz);
}
