package com.fourtwenty.core.domain.repositories.loyalty;

import com.fourtwenty.core.domain.models.loyalty.StorewideSale;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;

import java.util.List;

/**
 * Created on 9/10/19.
 */
public interface StorewideSaleRepository extends MongoShopBaseRepository<StorewideSale> {
    StorewideSale getStorewideSaleByName(String companyId, String shopId, String name);

    List<StorewideSale> getStorewideSales(String companyId, String shopId);

    DBCursor getStorewideSalesCursor(BasicDBObject query, BasicDBObject field);

    StorewideSale getStorewideSaleByNameExceptThis(String companyId, String shopId, String StorewideSaleId, String name);

    List<StorewideSale> getActivatedEntireStoreSalesByRangeDate(String companyId, String shopId, long startDateMillies, long endDateMillies);

    int getStorewideSalesActiveCount(String companyId, String shopId);
    
    StorewideSale getActiveStorewideSale(String companyId, String shopId);
}
