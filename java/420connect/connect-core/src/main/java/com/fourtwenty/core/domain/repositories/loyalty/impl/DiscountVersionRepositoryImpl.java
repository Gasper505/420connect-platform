package com.fourtwenty.core.domain.repositories.loyalty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.loyalty.DiscountVersion;
import com.fourtwenty.core.domain.repositories.loyalty.DiscountVersionRepository;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import javax.inject.Inject;

public class DiscountVersionRepositoryImpl extends ShopBaseRepositoryImpl<DiscountVersion> implements DiscountVersionRepository {

    @Inject
    public DiscountVersionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(DiscountVersion.class, mongoManager);
    }

    @Override
    public DiscountVersion findLastDiscountVersionByPromotionId(String companyId,String shopId, String promotionId){
        Iterable<DiscountVersion> iters = coll.find("{companyId:#,shopId:#,deleted:false,promotionId:#}", companyId, shopId,promotionId).sort("{created:-1}").limit(1).as(entityClazz);
        for (DiscountVersion discountVersion : iters) {
            return discountVersion;
        }
        return null;
    }

    @Override
    public DiscountVersion findLastDiscountVersionByRewardId(String companyId,String shopId, String rewardId){
        Iterable<DiscountVersion> iters = coll.find("{companyId:#,shopId:#,deleted:false,rewardId:#}", companyId, shopId,rewardId).sort("{created:-1}").limit(1).as(entityClazz);
        for (DiscountVersion discountVersion : iters) {
            return discountVersion;
        }
        return null;
    }
}