package com.fourtwenty.core.domain.repositories.loyalty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.google.inject.Inject;
import java.util.List;

/**
 * Created by mdo on 8/3/17.
 */
public class LoyaltyRewardRepositoryImpl extends ShopBaseRepositoryImpl<LoyaltyReward> implements LoyaltyRewardRepository {

    @Inject
    public LoyaltyRewardRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(LoyaltyReward.class, mongoManager);
    }

    @Override
    public boolean existsWithSlug(String companyId, String shopId, String slug) {
        return coll.count("{companyId:#,shopId:#,slug:#}", companyId, shopId, slug) > 0;
    }

    @Override
    public Iterable<LoyaltyReward> getPublishedRewards(String companyId, String shopId) {
        return coll.find("{companyId:#,shopId:#,published:true,deleted:false}", companyId, shopId).sort("{points:1}").as(entityClazz);
    }

    @Override
    public LoyaltyReward getRewardByName(String companyId, String shopId, String name) {
        return coll.findOne("{ companyId:#, shopId:#, slug:# }", companyId, shopId, name).as(entityClazz);
    }

    @Override
    public Iterable<LoyaltyReward> getRewardByNameList(String companyId, String shopId, List<String> name) {
        return (Iterable<LoyaltyReward>)coll.find("{ companyId:#, shopId:#, slug:{$in:#} }", companyId, shopId, name).as(entityClazz);
    }
}
