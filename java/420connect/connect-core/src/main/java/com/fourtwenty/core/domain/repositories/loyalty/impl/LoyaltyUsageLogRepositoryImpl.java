package com.fourtwenty.core.domain.repositories.loyalty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyUsageLogRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

/**
 * Created by mdo on 7/22/17.
 */
public class LoyaltyUsageLogRepositoryImpl extends ShopBaseRepositoryImpl<LoyaltyActivityLog> implements LoyaltyUsageLogRepository {

    @Inject
    public LoyaltyUsageLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(LoyaltyActivityLog.class, mongoManager);
    }

    @Override
    public Iterable<LoyaltyActivityLog> getActivityLogsForMember(String companyId, String memberId, LoyaltyActivityLog.LoyaltyActivityType type) {
        return coll.find("{companyId:#,memberId:#,activityType:#}", companyId, memberId, type).as(entityClazz);
    }

    @Override
    public boolean hasEarnedForTransaction(String companyId, String shopId, String memberId, String transNo) {
        return coll.count("{companyId:#,shopId:#,memberId:#,activityType:#,transNo:#}", companyId, shopId, memberId, LoyaltyActivityLog.LoyaltyActivityType.Accrue, transNo) > 0;
    }

    @Override
    public boolean hasUsedForMemberReward(String companyId, String memberId, String rewardId) {
        return coll.count("{companyId:#,memberId:#,activityType:#,rewardId:#}", companyId, memberId, LoyaltyActivityLog.LoyaltyActivityType.Usage, rewardId) > 0;
    }

    @Override
    public DateSearchResult<LoyaltyActivityLog> getUsageLogsForDate(String companyId, String shopId, long afterDate, long beforeDate) {
        Iterable<LoyaltyActivityLog> logs = coll.find("{companyId:#,shopId:#,modified:{$gte:#,$lte:#}}", companyId, shopId, afterDate, beforeDate).as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,modified:{$gte:#,$lte:#}}", companyId, shopId, afterDate, beforeDate);

        DateSearchResult<LoyaltyActivityLog> result = new DateSearchResult<>();
        result.setValues(Lists.newArrayList(logs));
        result.setTotal(count);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }


    @Override
    public <E extends LoyaltyActivityLog> SearchResult<E> getUsageLogsForType(String companyId, String shopId, LoyaltyActivityLog.LoyaltyActivityType activityType, int start, int limit, Class<E> clazz) {
        Iterable<E> logs = coll.find("{companyId:#,shopId:#,activityType:#}", companyId, shopId, activityType).sort("{modified:-1}").skip(start).limit(limit).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,activityType:#}", companyId, shopId, activityType);

        SearchResult<E> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(logs));
        result.setTotal(count);
        result.setSkip(start);
        result.setLimit(limit);
        return result;
    }
}
