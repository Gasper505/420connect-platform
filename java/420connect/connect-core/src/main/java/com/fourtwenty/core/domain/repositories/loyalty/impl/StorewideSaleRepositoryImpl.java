package com.fourtwenty.core.domain.repositories.loyalty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.loyalty.StorewideSale;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.loyalty.StorewideSaleRepository;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created on 9/10/19.
 */
public class StorewideSaleRepositoryImpl extends ShopBaseRepositoryImpl<StorewideSale> implements StorewideSaleRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(StorewideSaleRepositoryImpl.class);

    @Inject
    public StorewideSaleRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(StorewideSale.class, mongoManager);
    }

    @Override
    public StorewideSale getStorewideSaleByName(String companyId, String shopId, String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Iterable<StorewideSale> storewideSales = coll.find("{companyId:#,shopId:#,name:#,deleted:false}", companyId, shopId, name).as(entityClazz);
        List<StorewideSale> storewideSaleList = Lists.newArrayList(storewideSales);
        if (storewideSaleList.size() > 0) {
            return storewideSaleList.get(0);
        }
        return null;
    }

    @Override
    public List<StorewideSale> getStorewideSales(String companyId, String shopId) {
        Iterable<StorewideSale> storewideSales = coll.find("{companyId:#,shopId:#,deleted:false}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(storewideSales.iterator());
    }

    @Override
    @Deprecated
    public DBCursor getStorewideSalesCursor(final BasicDBObject query, final BasicDBObject field) {
        return getDB().getCollection("StorewideSales").find(query, field);
    }

    /**
     * This method check for StorewideSale with name except provided StorewideSale
     *
     * @param companyId   : company id
     * @param shopId      : shop id
     * @param storewideSaleId : StorewideSale id
     * @param name        : name
     */
    @Override
    public StorewideSale getStorewideSaleByNameExceptThis(String companyId, String shopId, String storewideSaleId, String name) {
        Iterable<StorewideSale> storewideSales = coll.find("{companyId:#,shopId:#,name:#, _id:{$ne:#}}", companyId, shopId, name, new ObjectId(storewideSaleId)).as(entityClazz);
        List<StorewideSale> storewideSaleList = Lists.newArrayList(storewideSales);
        if (storewideSaleList.size() > 0) {
            return storewideSaleList.get(0);
        }
        return null;
    }

    /**
     * This method get the storewide sales which are applied to the entire store 
     *
     * @param companyId         : company id
     * @param shopId            : shop id
     * @param startDateMillis   : start date in milliseconds
     * @param endDateMillis     : end date in milliseconds
     */ 
     @Override
    public List<StorewideSale> getActivatedEntireStoreSalesByRangeDate(String companyId, String shopId, long startDateMillies, long endDateMillies) {
        Iterable<StorewideSale> storewideSales = coll.find("{$and:[{companyId:#,shopId:#,deleted:false,active:true,rules:{ $exists: true, $size: 0}},{$nor:[{startDate: {$gt:#}},{endDate:{$lt:#}}]}]}", companyId, shopId,endDateMillies, startDateMillies).as(entityClazz);
        return Lists.newArrayList(storewideSales.iterator());
    }

    @Override
    public int getStorewideSalesActiveCount(String companyId, String shopId){
        Iterable<StorewideSale> storewideSales = coll.find("{companyId:#,shopId:#,deleted:false,active:true}", companyId, shopId).as(entityClazz);
        return Lists.newArrayList(storewideSales.iterator()).size();
    }

    @Override
    public StorewideSale getActiveStorewideSale(String companyId, String shopId) {
        Iterable<StorewideSale> storewideSales = coll.find("{companyId:#,shopId:#,deleted:false,active:true}", companyId, shopId).as(entityClazz);
        List<StorewideSale> storewideSaleList = Lists.newArrayList(storewideSales);
        
        if (storewideSaleList.size() > 0) {
            return storewideSaleList.get(0);
        }
        return null;
    }
}
