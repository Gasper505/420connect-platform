package com.fourtwenty.core.domain.repositories.notification;

import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;
import java.util.List;

public interface NotificationInfoRepository extends MongoShopBaseRepository<NotificationInfo> {

    void deleteAll(String companyId, String shopId, NotificationInfo.NotificationType type);

    HashMap<String, NotificationInfo> listByShopAndTypeAsMap(NotificationInfo.NotificationType type);

    void updateLastSentTime(List<String> shopIds, long lastSent, NotificationInfo.NotificationType type);
    void updateNextNotificationTime(List<String> shopIds, long nextNotificationTime, NotificationInfo.NotificationType type);

    NotificationInfo getByShopAndType(String companyId, String shopId, NotificationInfo.NotificationType type);

    SearchResult<NotificationInfo> findItemsByType(String companyId, String shopId, NotificationInfo.NotificationType type, int start, int limit);

}
