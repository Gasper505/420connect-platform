package com.fourtwenty.core.domain.repositories.notification.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class NotificationInfoRepositoryImpl extends ShopBaseRepositoryImpl<NotificationInfo> implements NotificationInfoRepository {

    @Inject
    public NotificationInfoRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(NotificationInfo.class, mongoManager);
    }

    @Override
    public void deleteAll(String companyId, String shopId, NotificationInfo.NotificationType type) {
        coll.update("{companyId:#,shopId:#,deleted:false,notificationType:#}", companyId, shopId, type).multi().with("{$set: {deleted:true,active:false, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, NotificationInfo> listByShopAndTypeAsMap(NotificationInfo.NotificationType type) {
        Iterable<NotificationInfo> iterable = coll.find("{ deleted : false,notificationType:# }", type).as(entityClazz);
        HashMap<String, NotificationInfo> returnMap = new HashMap<>();
        for (NotificationInfo notificationInfo : iterable) {
            returnMap.put(notificationInfo.getShopId(), notificationInfo);
        }

        return returnMap;
    }

    @Override
    public void updateLastSentTime(List<String> shopIds, long lastSent, NotificationInfo.NotificationType type) {
        coll.update("{ shopId : { $in : #} , deleted:false , notificationType:# }", shopIds, type).multi().with("{ $set : { lastSent : #,modified:#}}", lastSent, DateTime.now().getMillis());
    }


    @Override
    public void updateNextNotificationTime(List<String> shopIds, long nextNotificationTime, NotificationInfo.NotificationType type) {
        coll.update("{ shopId : { $in : #} , deleted:false , notificationType:# }", shopIds, type).multi().with("{ $set : { nextNotificationTime : #,modified:#}}", nextNotificationTime, DateTime.now().getMillis());

    }

    @Override
    public NotificationInfo getByShopAndType(String companyId, String shopId, NotificationInfo.NotificationType type) {
        return coll.findOne("{companyId:#,shopId:#,deleted:false,notificationType:#}", companyId, shopId, type).as(entityClazz);
    }

    @Override
    public SearchResult<NotificationInfo> findItemsByType(String companyId, String shopId, NotificationInfo.NotificationType type, int start, int limit) {
        SearchResult<NotificationInfo> result = new SearchResult<>();
        Iterable<NotificationInfo> items = coll.find("{companyId:#,shopId:#,notificationType:#,deleted:false}", companyId, shopId, type).sort("{modified:-1}").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,notificationType:#,deleted:false}", companyId, shopId, type);

        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setSkip(start);
        result.setLimit(limit);

        return result;
    }
}
