package com.fourtwenty.core.domain.repositories.onprem;

import com.fourtwenty.core.domain.models.onprem.OnPremSyncDetails;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

import java.util.List;

public interface OnPremSyncDetailsRepository extends MongoCompanyBaseRepository<OnPremSyncDetails> {
    List<OnPremSyncDetails> syncDetailsList(String companyId, List<String> entityNames);
}
