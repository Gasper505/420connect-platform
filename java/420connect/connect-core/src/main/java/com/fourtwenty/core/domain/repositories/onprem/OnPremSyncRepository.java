package com.fourtwenty.core.domain.repositories.onprem;

import com.mongodb.DBCollection;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

import java.util.List;

public interface OnPremSyncRepository {
    <E> MongoCollection getCollection(Class<E> eClass);
    <E> DBCollection getMongoDBCollection(Class<E> eClass);


    <E> List<E> get(String companyId, Class<E> modelClass);
    <E> List<E> get(String companyId, Class<E> modelClass, long dateTime);

    <E> List<E> get(String companyId, String shopId, Class<E> modelClass);
    <E> List<E> get(String companyId, String shopId, Class<E> modelClass, long dateTime);

    <E> List<E> get(Class<E> modelClass);
    <E> List<E> get(Class<E> modelClass, long dateTime);

    //<E> List<Object> save(Object[] models, Class<E> modelClass);
    <E> List<E> save(List<E> models, Class<E> modelClass);
    <E> List<E> getIndexes(String companyId, Class<E> modelClass);
    <E> List<E> get(ObjectId _id, Class<E> modelClass);

    <E> List<Object> upsert(String entityId, E models, Class<E> modelClass);

}
