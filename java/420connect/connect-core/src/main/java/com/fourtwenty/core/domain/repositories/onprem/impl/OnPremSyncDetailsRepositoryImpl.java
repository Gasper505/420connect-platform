package com.fourtwenty.core.domain.repositories.onprem.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.onprem.OnPremSyncDetails;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncDetailsRepository;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

public class OnPremSyncDetailsRepositoryImpl extends CompanyBaseRepositoryImpl<OnPremSyncDetails> implements OnPremSyncDetailsRepository {

    @Inject
    public OnPremSyncDetailsRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(OnPremSyncDetails.class, mongoManager);
    }

    @Override
    public List<OnPremSyncDetails> syncDetailsList(String companyId, List<String> entityNames) {
        Iterator<OnPremSyncDetails> iterator = coll.find("{companyId : #, deleted: false, entity: {$in:#}}", companyId, entityNames).as(OnPremSyncDetails.class).iterator();
        return Lists.newArrayList(iterator);
    }

}
