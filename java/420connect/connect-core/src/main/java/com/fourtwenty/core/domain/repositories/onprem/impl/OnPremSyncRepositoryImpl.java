package com.fourtwenty.core.domain.repositories.onprem.impl;

import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.managed.MongoManager;
import com.google.common.collect.Lists;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

public final class OnPremSyncRepositoryImpl implements OnPremSyncRepository {

    private MongoManager mongoManager;

    @Inject
    public OnPremSyncRepositoryImpl(MongoManager mongoManager) {
        this.mongoManager = mongoManager;
    }

    @Override
    public <E> List<E> get(String companyId, Class<E> modelClass) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find("{companyId: #}", companyId).as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> get(String companyId, Class<E> modelClass, long dateTime) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find("{companyId: #, modified:{$gt:#}}", companyId, dateTime).as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> get(String companyId, String shopId, Class<E> modelClass) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find("{companyId: #, shopId: #}", companyId, shopId).as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> get(String companyId, String shopId, Class<E> modelClass, long dateTime) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find("{companyId: #, shopId: #, modified:{$gt:#}}", companyId, shopId,dateTime).as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> get(Class<E> modelClass, long dateTime) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find("{modified:{$gt:#}}", dateTime).as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> get(Class<E> modelClass) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find().as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> get(ObjectId _id, Class<E> modelClass) {
        MongoCollection coll = getCollection(modelClass);
        Iterator<E> iterator = coll.find("{_id: #}", _id).as(modelClass).iterator();
        return Lists.newArrayList(iterator);
    }

    @Override
    public <E> List<E> save(List<E> models, Class<E> modelClass) {
        if (models.size() > 0) {
            MongoCollection coll = getCollection(modelClass);
            coll.insert(models.toArray());
        }
        return Lists.newArrayList(models);
    }

    public <E> MongoCollection getCollection(Class<E> eClass) {
        CollectionName collectionName = eClass.getAnnotation(CollectionName.class);
        try {
            return mongoManager.getJongoCollection(collectionName.name());
        } catch (Exception e) {
            throw new BlazeOperationException("Couldn't obtain mongo collection");
        }
    }

    public <E> DBCollection getMongoDBCollection(Class<E> eClass) {
        CollectionName collectionName = eClass.getAnnotation(CollectionName.class);
        try {
            return mongoManager.getDBCollection(collectionName.name());
        } catch (Exception e) {
            throw new BlazeOperationException("Couldn't obtain mongo collection");
        }
    }

    @Override
    public List getIndexes(String companyId, Class modelClass) {
        MongoCollection coll = getCollection(modelClass);
        List<DBObject> indexInfo = coll.getDBCollection().getIndexInfo();
        return indexInfo;
    }

    @Override
    public <E> List<Object> upsert(String entityId, E models, Class<E> modelClass) {
        if (entityId == null || !ObjectId.isValid(entityId) || models == null) {
            throw new BlazeInvalidArgException("Upsert", "Entity or Model should not be null.");
        }
        MongoCollection coll = getCollection(modelClass);
        Object id = null;
        if (entityId != null) {
            id = coll.update(new ObjectId(entityId)).upsert().with(models).getUpsertedId();

        } else {
            id = coll.save(models).getUpsertedId();
        }

        if (id != null && id instanceof ObjectId) {
            ((BaseModel) models).setId(id.toString());
        }
        return Lists.newArrayList(models);
    }
}
