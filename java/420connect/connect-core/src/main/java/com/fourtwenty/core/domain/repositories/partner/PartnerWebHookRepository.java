package com.fourtwenty.core.domain.repositories.partner;

import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface PartnerWebHookRepository extends MongoShopBaseRepository<PartnerWebHook> {
    <T extends PartnerWebHook> SearchResult<T> findItemsByWebHookType(String companyId, PartnerWebHook.PartnerWebHookType webHookType, String sortOption, int start, int limit, Class<T> clazz);

    PartnerWebHook getByWebHookType(String companyId, String shopId, PartnerWebHook.PartnerWebHookType partnerWebHookType);

    SearchResult<PartnerWebHook> findItemsByWebHookTypeWithShop(String companyId, String shopId, PartnerWebHook.PartnerWebHookType webHookType, String sortOption, int start, int limit);
}
