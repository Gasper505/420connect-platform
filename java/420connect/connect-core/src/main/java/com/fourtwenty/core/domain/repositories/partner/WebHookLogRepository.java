package com.fourtwenty.core.domain.repositories.partner;

import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.partner.WebHookLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface WebHookLogRepository extends MongoShopBaseRepository<WebHookLog> {

    SearchResult<WebHookLog> findItemsByWebHookType(String companyId, String shopId, PartnerWebHook.PartnerWebHookType webHookType, int skip, int limit);
}
