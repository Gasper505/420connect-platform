package com.fourtwenty.core.domain.repositories.partner.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.partner.PartnerWebHookRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

public class PartnerWebHookRepositoryImpl extends ShopBaseRepositoryImpl<PartnerWebHook> implements PartnerWebHookRepository {
    @Inject
    public PartnerWebHookRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(PartnerWebHook.class, mongoManager);
    }

    @Override
    public <T extends PartnerWebHook> SearchResult<T> findItemsByWebHookType(String companyId, PartnerWebHook.PartnerWebHookType webHookType, String sortOption, int start, int limit, Class<T> clazz) {
        Iterable<T> items = coll.find("{companyId:#,webHookType:#}", companyId, webHookType).sort(sortOption).skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,webHookType:#}", companyId, webHookType);

        SearchResult<T> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        return searchResult;

    }

    @Override
    public PartnerWebHook getByWebHookType(String companyId, String shopId, PartnerWebHook.PartnerWebHookType partnerWebHookType) {
        return coll.findOne("{companyId:#, shopId:#, webHookType:#, deleted:false}", companyId, shopId, partnerWebHookType).as(entityClazz);
    }

    @Override
    public SearchResult<PartnerWebHook> findItemsByWebHookTypeWithShop(String companyId, String shopId, PartnerWebHook.PartnerWebHookType webHookType, String sortOption, int start, int limit) {
        Iterable<PartnerWebHook> items = coll.find("{companyId:#,shopId:#,webHookType:#}", companyId, shopId, webHookType).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,shopId:#,webHookType:#}", companyId, shopId, webHookType);

        SearchResult<PartnerWebHook> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        return searchResult;
    }
}
