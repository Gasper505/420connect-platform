package com.fourtwenty.core.domain.repositories.partner.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.partner.WebHookLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.partner.WebHookLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class WebHookLogRepositoryImpl extends ShopBaseRepositoryImpl<WebHookLog> implements WebHookLogRepository {

    @Inject
    public WebHookLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(WebHookLog.class, mongoManager);
    }

    @Override
    public SearchResult<WebHookLog> findItemsByWebHookType(String companyId, String shopId, PartnerWebHook.PartnerWebHookType webHookType, int skip, int limit) {
        Iterable<WebHookLog> items = coll.find("{ companyId : # , shopId : # , deleted : false , webHookType : # }", companyId, shopId, webHookType).sort("{ created : -1 }").skip(skip).limit(limit).as(entityClazz);
        long count = coll.count("{ companyId : # , shopId : # , deleted : false , webHookType : # }", companyId, shopId, webHookType);

        SearchResult<WebHookLog> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(items));
        searchResult.setTotal(count);
        searchResult.setLimit(limit);
        searchResult.setSkip(skip);

        return searchResult;
    }
}
