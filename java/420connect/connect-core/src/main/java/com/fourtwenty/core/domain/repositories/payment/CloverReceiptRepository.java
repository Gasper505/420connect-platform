package com.fourtwenty.core.domain.repositories.payment;

import com.fourtwenty.core.domain.models.receipt.CloverReceipt;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface CloverReceiptRepository extends MongoShopBaseRepository<CloverReceipt> {

    CloverReceipt getCloverTransaction(String companyId, String transactionId);

    Iterable<CloverReceipt> getCloverReceiptsTransaction(String transactionId);


}
