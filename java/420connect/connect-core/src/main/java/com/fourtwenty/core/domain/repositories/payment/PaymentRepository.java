package com.fourtwenty.core.domain.repositories.payment;

import com.fourtwenty.core.domain.models.payment.Payment;
import com.fourtwenty.core.domain.models.payment.PaymentComponent;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created on 23/10/17 11:47 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public interface PaymentRepository extends MongoShopBaseRepository<Payment> {
    Payment getPayment(String companyId, String shopId, String purchaseReferenceId, PaymentComponent paymentComponent);
}
