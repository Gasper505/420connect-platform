package com.fourtwenty.core.domain.repositories.payment;

import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;


public interface ShopPaymentOptionRepository extends MongoShopBaseRepository<ShopPaymentOption> {


}
