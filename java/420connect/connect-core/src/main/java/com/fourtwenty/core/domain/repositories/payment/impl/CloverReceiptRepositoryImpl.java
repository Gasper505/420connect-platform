package com.fourtwenty.core.domain.repositories.payment.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.receipt.CloverReceipt;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.payment.CloverReceiptRepository;

import javax.inject.Inject;

public class CloverReceiptRepositoryImpl extends ShopBaseRepositoryImpl<CloverReceipt> implements CloverReceiptRepository {

    @Inject
    public CloverReceiptRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(CloverReceipt.class, mongoManager);
    }

    @Override
    public CloverReceipt getCloverTransaction(String companyId, String transactionId) {
        return coll.findOne("{companyId:#,transactionId:#}",companyId,transactionId).as(entityClazz);
    }

    @Override
    public Iterable<CloverReceipt> getCloverReceiptsTransaction(String transactionId) {
        return coll.find("{transactionId:#}",transactionId).as(entityClazz);
    }
}
