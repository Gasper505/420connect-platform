package com.fourtwenty.core.domain.repositories.payment.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.payment.Payment;
import com.fourtwenty.core.domain.models.payment.PaymentComponent;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.payment.PaymentRepository;
import com.google.inject.Inject;

/**
 * Created on 23/10/17 11:48 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public class PaymentRepositoryImpl extends ShopBaseRepositoryImpl<Payment> implements PaymentRepository {

    @Inject
    public PaymentRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(Payment.class, mongoManager);
    }

    @Override
    public Payment getPayment(final String companyId, final String shopId, final String purchaseReferenceId, final PaymentComponent paymentComponent) {
        return this.coll.findOne("{companyId:#, shopId:#, purchaseReferenceId:#, paymentComponent:#}", companyId, shopId, purchaseReferenceId, paymentComponent).as(entityClazz);
    }

}
