package com.fourtwenty.core.domain.repositories.payment.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.payment.ShopPaymentOptionRepository;
import com.google.inject.Inject;


public class ShopPaymentOptionRepositoryImpl extends ShopBaseRepositoryImpl<ShopPaymentOption> implements ShopPaymentOptionRepository {

    @Inject
    public ShopPaymentOptionRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ShopPaymentOption.class, mongoManager);
    }

}
