package com.fourtwenty.core.domain.repositories.plugins;

import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

import java.util.List;

public interface BlazePluginProductRepository extends BaseRepository<BlazePluginProduct> {
    void deletePluginProducts(List<BlazePluginProduct> pluginProductList);

    BlazePluginProduct getPlugin(String pluginId);
}
