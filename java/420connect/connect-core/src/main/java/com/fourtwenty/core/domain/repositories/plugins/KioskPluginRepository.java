package com.fourtwenty.core.domain.repositories.plugins;

import com.fourtwenty.core.domain.models.plugins.KioskPluginCompanySetting;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

public interface KioskPluginRepository extends PluginBaseRepository<KioskPluginCompanySetting>, MongoCompanyBaseRepository<KioskPluginCompanySetting> {

}
