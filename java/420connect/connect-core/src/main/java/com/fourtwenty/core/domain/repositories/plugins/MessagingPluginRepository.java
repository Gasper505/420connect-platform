package com.fourtwenty.core.domain.repositories.plugins;

import com.fourtwenty.core.domain.models.plugins.MessagingPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginShopSetting;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

public interface MessagingPluginRepository extends PluginBaseRepository<MessagingPluginCompanySetting>, MongoCompanyBaseRepository<MessagingPluginCompanySetting> {

    MessagingPluginShopSetting getMessagingPluginShopSetting(String companyId, String shopId);
}
