package com.fourtwenty.core.domain.repositories.plugins;

public interface PluginBaseRepository<T> {
    T getCompanyPlugin(final String companyId);
}
