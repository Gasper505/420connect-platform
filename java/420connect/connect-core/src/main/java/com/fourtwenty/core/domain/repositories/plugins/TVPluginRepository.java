package com.fourtwenty.core.domain.repositories.plugins;

import com.fourtwenty.core.domain.models.plugins.TVPluginCompanySetting;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

public interface TVPluginRepository extends PluginBaseRepository<TVPluginCompanySetting>, MongoCompanyBaseRepository<TVPluginCompanySetting> {
}
