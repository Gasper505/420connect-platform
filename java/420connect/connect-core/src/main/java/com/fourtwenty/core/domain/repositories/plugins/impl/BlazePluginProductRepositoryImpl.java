package com.fourtwenty.core.domain.repositories.plugins.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.plugins.BlazePluginProductRepository;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.List;

public class BlazePluginProductRepositoryImpl extends MongoBaseRepositoryImpl<BlazePluginProduct> implements BlazePluginProductRepository {

    @Inject
    public BlazePluginProductRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(BlazePluginProduct.class, mongoManager);
    }

    @Override
    public void deletePluginProducts(final List<BlazePluginProduct> pluginProductList) {

        for (BlazePluginProduct product : pluginProductList) {
            this.removeById(product.getId());
        }
    }

    @Override
    public BlazePluginProduct getPlugin(final String pluginId) {
        return this.coll.findOne("{_id:#, deleted:false}", new ObjectId(pluginId)).as(BlazePluginProduct.class);
    }

}
