package com.fourtwenty.core.domain.repositories.plugins.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.plugins.KioskPluginCompanySetting;
import com.fourtwenty.core.domain.repositories.plugins.KioskPluginRepository;

import javax.inject.Inject;

public class KioskPluginRepositoryImpl extends PluginBaseRepositoryImpl<KioskPluginCompanySetting> implements KioskPluginRepository {

    @Inject
    public KioskPluginRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(KioskPluginCompanySetting.class, mongoManager);
    }
}
