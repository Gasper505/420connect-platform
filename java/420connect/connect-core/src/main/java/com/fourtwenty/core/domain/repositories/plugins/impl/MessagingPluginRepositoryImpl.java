package com.fourtwenty.core.domain.repositories.plugins.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginShopSetting;
import com.fourtwenty.core.domain.repositories.plugins.MessagingPluginRepository;

import javax.inject.Inject;
import java.util.List;

public class MessagingPluginRepositoryImpl extends PluginBaseRepositoryImpl<MessagingPluginCompanySetting> implements MessagingPluginRepository {

    @Inject
    public MessagingPluginRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MessagingPluginCompanySetting.class, mongoManager);
    }

    @Override
    public MessagingPluginShopSetting getMessagingPluginShopSetting(final String companyId, final String shopId) {
        final MessagingPluginCompanySetting pluginCompanySetting = this.coll.findOne("{companyId:#,messagingSetting.shopId:#}", companyId, shopId).as(entityClazz);
        if (pluginCompanySetting != null) {
            List<MessagingPluginShopSetting> pluginShopSettingList = pluginCompanySetting.getMessagingSetting();
            for (MessagingPluginShopSetting messagingPluginShopSetting : pluginShopSettingList) {
                if (shopId.equals(messagingPluginShopSetting.getShopId())) {
                    return messagingPluginShopSetting;
                }
            }
        }
        return null;
    }

}
