package com.fourtwenty.core.domain.repositories.plugins.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.plugins.PluginCompanySetting;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.plugins.PluginBaseRepository;

public abstract class PluginBaseRepositoryImpl<T extends PluginCompanySetting> extends CompanyBaseRepositoryImpl<T> implements PluginBaseRepository<T> {

    public PluginBaseRepositoryImpl(Class<T> clazz, MongoDb mongoManager) throws Exception {
        super(clazz, mongoManager);
    }

    @Override
    public T getCompanyPlugin(final String companyId) {
        return coll.findOne("{companyId:#,deleted:false}", companyId).as(entityClazz);
    }
}
