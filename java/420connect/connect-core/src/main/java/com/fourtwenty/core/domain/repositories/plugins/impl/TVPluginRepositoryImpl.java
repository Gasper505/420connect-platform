package com.fourtwenty.core.domain.repositories.plugins.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.plugins.TVPluginCompanySetting;
import com.fourtwenty.core.domain.repositories.plugins.TVPluginRepository;

import javax.inject.Inject;

public class TVPluginRepositoryImpl extends PluginBaseRepositoryImpl<TVPluginCompanySetting> implements TVPluginRepository {

    @Inject
    public TVPluginRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TVPluginCompanySetting.class, mongoManager);
    }

}
