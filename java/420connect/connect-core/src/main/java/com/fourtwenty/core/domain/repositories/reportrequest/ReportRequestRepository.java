package com.fourtwenty.core.domain.repositories.reportrequest;

import com.fourtwenty.core.domain.models.reportrequest.ReportRequest;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface ReportRequestRepository extends MongoShopBaseRepository<ReportRequest> {

    SearchResult<ReportRequest> listAllByEmployee(String companyId, String shopId, String employeeId, int start, int limit);
}
