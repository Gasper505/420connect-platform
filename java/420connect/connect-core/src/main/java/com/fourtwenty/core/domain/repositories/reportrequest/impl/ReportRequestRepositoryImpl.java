package com.fourtwenty.core.domain.repositories.reportrequest.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.reportrequest.ReportRequest;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.reportrequest.ReportRequestRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;

public class ReportRequestRepositoryImpl extends ShopBaseRepositoryImpl<ReportRequest> implements ReportRequestRepository {

    @Inject
    public ReportRequestRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ReportRequest.class, mongoManager);
    }

    @Override
    public SearchResult<ReportRequest> listAllByEmployee(String companyId, String shopId, String employeeId, int start, int limit) {
        Iterable<ReportRequest> items = coll.find("{ companyId:#, shopId:#, employeeId:#, deleted:false }", companyId, shopId, employeeId).sort("{ modified:-1 }").skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{ companyId:#, shopId:#, employeeId:#, deleted:false }", companyId, shopId, employeeId);

        SearchResult<ReportRequest> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setSkip(start);
        result.setLimit(limit);

        return result;
    }
}
