package com.fourtwenty.core.domain.repositories.store;


import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;


import java.util.List;

/**
 * Created by mdo on 5/10/17.
 */
public interface ConsumerCartRepository extends MongoShopBaseRepository<ConsumerCart> {
    ConsumerCart getCurrentInProgressCartWithSessionId(String companyId, String shopId, String sessionId);

    ConsumerCart getCurrentInProgressCart(String companyId, String shopId, String consumerId);

    <E extends ConsumerCart> Iterable<E> getPlacedConsumerCartsByStatus(String companyId, String shopId,
                                                                        List<ConsumerCart.ConsumerCartStatus> statuses, long startDate, long endDate,
                                                                        Class<E> clazz);

    // Orders are completed carts
    SearchResult<ConsumerCart> getCompletedOrders(String companyId,
                                                  String shopId,
                                                  String consumerId,
                                                  boolean completed,
                                                  int start,
                                                  int limit);

    SearchResult<ConsumerCart> getAllOrders(String companyId,
                                            String shopId,
                                            String consumerId,
                                            int start,
                                            int limit);

    <E extends ConsumerCartResult> DateSearchResult<E> fetchOrders(String companyId,
                                                String shopId,
                                                long afterDate,
                                                long beforeDate,
                                                Class<E> clazz);



    ConsumerCartResult getConsumerCart(String publicKey);

    Iterable<ConsumerCart> getConsumerCartsWithoutKey();

    Iterable<ConsumerCart> getBracketSaleByConsumerId(String companyId, String consumerId, Long startDate, Long endDate);

    Iterable<ConsumerCart> getBracketSaleByConsumerId(String companyId, String shopId, String consumerId, Long startDate, Long endDate);

    long countIncomingOrders(String companyId, String shopId, List<ConsumerCart.ConsumerCartStatus> statuses);

    Iterable<ConsumerCart> getBracketConsumerCart(String companyId, String shopId, Long startDate, Long endDate);

    Iterable<ConsumerCart> getBracketConsumerCartByStatus(String companyId, String shopId, ConsumerCart.ConsumerCartStatus status, Long startDate, Long endDate);

    <E extends ConsumerCart> SearchResult<E> getPlacedConsumerCartsByStatus(String companyId, String shopId,
                                                                        List<ConsumerCart.ConsumerCartStatus> statuses,
                                                                        int start, int limit, Class<E> clazz);
}
