package com.fourtwenty.core.domain.repositories.store;

import com.fourtwenty.core.domain.models.store.ConsumerPasswordReset;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;

/**
 * Created by mdo on 5/22/17.
 */
public interface ConsumerPasswordResetRepository extends BaseRepository<ConsumerPasswordReset> {

    ConsumerPasswordReset getPasswordReset(String resetCode);

    Iterable<ConsumerPasswordReset> getPasswordResetForEmployee(String consumerUserId);

    void setPasswordExpired(String consumerUserId);
}
