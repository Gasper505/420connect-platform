package com.fourtwenty.core.domain.repositories.store;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 12/19/16.
 */
public interface ConsumerUserRepository extends MongoCompanyBaseRepository<ConsumerUser> {
    <E extends ConsumerUser> E getConsumerByEmail(String email, Class<E> clazz);

    ConsumerUser getConsumerByCPN(String companyId, String cpn);

    ConsumerUser getConsumerUserBySignedContractId(String signedContractId);

    Iterable<ConsumerUser> getConsumerUnsetCPN();

    WriteResult updateCPN(String consumerUserId, String cpn);

    HashMap<String, ConsumerUser> listAsMap(List<ObjectId> ids);

    SearchResult<ConsumerUser> getConsumerUsersByCompanyId(String companyId, int skip, int limit);

    Iterable<ConsumerUser> getConsumerByEmails(String companyId, Set<String> emailIds);

    Iterable<ConsumerUser> list(int skip, int limit);

    <E extends ConsumerUser> E getConsumerByEmail(String companyId, String emailId, Class<E> clazz);

    SearchResult<ConsumerUser> findConsumersByAcceptance(String companyId, int start, int limit, Boolean acceptance, String sortOptions, String term);

    SearchResult<ConsumerUser> findCreatedConsumerByDate(String companyId, int start, int limit, long startDate, long endDate, String term);

    SearchResult<ConsumerUser> findCreatedConsumerByDateWithMarketingSource(String companyId, int start, int limit, long startDate, long endDate, String marketingSource, String term);

    SearchResult<ConsumerUser> findConsumersByAcceptanceWithMarketingSource(String companyId, int skip, int limit, String marketingSource, Boolean acceptance, String sortOptions, String term);

    SearchResult<ConsumerUser> findConsumersByIdAndAcceptanceWithMarketingSource(String companyId, int start, int limit, List<ObjectId> consumerUserIds, String marketingSource, boolean accepted, String sortOptions, String term);

    SearchResult<ConsumerUser> findConsumersByIdAndAcceptance(String companyId, int start, int limit, List<ObjectId> consumerUserIds, boolean accepted, String sortOptions, String term);

    SearchResult<ConsumerUser> findRejectedConsumers(String companyId, int start, int limit, Boolean accepted, String sortOptions, List<ObjectId> consumerUserIds, String term);

    SearchResult<ConsumerUser> findRejectedConsumersWithMarketingSource(String companyId, int start, int limit, String marketingSource, Boolean accepted, String sortOptions, List<ObjectId> consumerUserIds, String term);

    long countConsumerByDrivingLicense(String companyId, String licenseNumber);
}
