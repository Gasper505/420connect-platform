package com.fourtwenty.core.domain.repositories.store;

import com.fourtwenty.core.domain.models.store.StoreWidgetKey;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 4/17/17.
 */
public interface StoreWidgetKeyRepository extends MongoShopBaseRepository<StoreWidgetKey> {
    StoreWidgetKey getStoreKey(String key);

    StoreWidgetKey getStoreKeyByActiveShop(String companyId, String shopId);

    void deleteAll(String companyId, String shopId);

    SearchResult<StoreWidgetKey> getActiveKeys(String companyId);
}
