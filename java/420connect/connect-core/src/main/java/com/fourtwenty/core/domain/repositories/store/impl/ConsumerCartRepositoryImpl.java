package com.fourtwenty.core.domain.repositories.store.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by mdo on 5/10/17.
 */
public class ConsumerCartRepositoryImpl extends ShopBaseRepositoryImpl<ConsumerCart> implements ConsumerCartRepository {
    @Inject
    public ConsumerCartRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ConsumerCart.class, mongoManager);
    }

    @Override
    public ConsumerCart getCurrentInProgressCart(String companyId, String shopId, String consumerId) {
        return coll.findOne("{companyId:#,shopId:#,consumerId:#,cartStatus:#}", companyId, shopId, consumerId, ConsumerCart.ConsumerCartStatus.InProgress).as(entityClazz);
    }

    @Override
    public ConsumerCart getCurrentInProgressCartWithSessionId(String companyId, String shopId, String sessionId) {
        return coll.findOne("{companyId:#,shopId:#,sessionId:#,cartStatus:#}", companyId, shopId, sessionId, ConsumerCart.ConsumerCartStatus.InProgress).as(entityClazz);
    }

    @Override
    public <E extends ConsumerCart> Iterable<E> getPlacedConsumerCartsByStatus(String companyId, String shopId, List<ConsumerCart.ConsumerCartStatus> statuses, long startDate, long endDate, Class<E> clazz) {
        return coll.find("{companyId:#,shopId:#,cartStatus:{$in:#}},modified:{$lt:#, $gt:#}", companyId, shopId, statuses, endDate, startDate).sort("{orderPlacedTime:-1}").as(clazz);
    }

    @Override
    public SearchResult<ConsumerCart> getCompletedOrders(String companyId,
                                                         String shopId,
                                                         String consumerId,
                                                         boolean completed,
                                                         int start,
                                                         int limit) {
        Iterable<ConsumerCart> consumerOrders = coll.find("{companyId:#,shopId:#,consumerId:#,completed:#}", companyId, shopId, consumerId, completed)
                .sort("{orderPlacedTime:-1}")
                .skip(start)
                .limit(limit)
                .as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,consumerId:#,completed:#}", companyId, shopId, consumerId, completed);

        SearchResult<ConsumerCart> result = new SearchResult<>();
        result.setTotal(count);
        result.setValues(Lists.newArrayList(consumerOrders));
        result.setLimit(limit);
        result.setSkip(start);

        return result;
    }

    @Override
    public SearchResult<ConsumerCart> getAllOrders(String companyId,
                                                   String shopId,
                                                   String consumerId,
                                                   int start,
                                                   int limit) {
        Iterable<ConsumerCart> consumerOrders = coll.find("{companyId:#,shopId:#,consumerId:#,deleted:false}", companyId, shopId, consumerId)
                .sort("{orderPlacedTime:-1}")
                .skip(start)
                .limit(limit)
                .as(entityClazz);

        long count = coll.count("{companyId:#,shopId:#,consumerId:#,deleted:false}", companyId, shopId, consumerId);

        SearchResult<ConsumerCart> result = new SearchResult<>();
        result.setTotal(count);
        result.setValues(Lists.newArrayList(consumerOrders));
        result.setLimit(limit);
        result.setSkip(start);

        return result;
    }

    @Override
    public <E extends ConsumerCartResult> DateSearchResult<E> fetchOrders(String companyId, String shopId, long afterDate, long beforeDate, Class<E> clazz) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();

        Iterable<E> items = coll.find("{companyId:#,shopId:#,consumerId:{$exists:true},modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate).as(clazz);

        long count = coll.count("{companyId:#,shopId:#,consumerId:{$exists:true}, modified:{$lt:#, $gt:#}}", companyId, shopId, beforeDate, afterDate);

        DateSearchResult<E> results = new DateSearchResult<>();
        results.setValues(Lists.newArrayList(items));
        results.setBeforeDate(beforeDate);
        results.setAfterDate(afterDate);
        results.setTotal(count);
        return results;
    }

    @Override
    public ConsumerCartResult getConsumerCart(String publicKey) {
        Iterable<ConsumerCartResult> carts = coll.find("{publicKey:#}", publicKey).as(ConsumerCartResult.class);
        for (ConsumerCartResult result : carts) {
            return result;
        }
        return null;
    }

    @Override
    public Iterable<ConsumerCart> getConsumerCartsWithoutKey() {
        return coll.find("{completed:true,publicKey: {$exists:false}}").as(entityClazz);
    }

    @Override
    public Iterable<ConsumerCart> getBracketSaleByConsumerId(String companyId, String consumerId, Long startDate, Long endDate) {
        return coll.find("{companyId:#,consumerId:#,orderPlacedTime:{$gt:#, $lt:#},cartStatus:{$in:[#,#]}}", companyId, consumerId, startDate, endDate, ConsumerCart.ConsumerCartStatus.Placed, ConsumerCart.ConsumerCartStatus.Accepted)
                .sort("{orderPlacedTime:-1}")
                .as(entityClazz);
    }

    @Override
    public Iterable<ConsumerCart> getBracketSaleByConsumerId(String companyId, String shopId, String consumerId, Long startDate, Long endDate) {
        return coll.find("{companyId:#,shopId:#,consumerId:#,orderPlacedTime:{$gt:#, $lt:#},cartStatus:{$in:[#,#]}}", companyId, shopId, consumerId, startDate, endDate, ConsumerCart.ConsumerCartStatus.Placed, ConsumerCart.ConsumerCartStatus.Accepted)
                .sort("{orderPlacedTime:-1}")
                .as(entityClazz);
    }

    @Override
    public long countIncomingOrders(String companyId, String shopId, List<ConsumerCart.ConsumerCartStatus> statuses) {
        return coll.count("{companyId:#,shopId:#,cartStatus:{$in:#}}", companyId, shopId, statuses);
    }

    @Override
    public Iterable<ConsumerCart> getBracketConsumerCart(String companyId, String shopId, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, orderPlacedTime: {$gt:#, $lt:#}, cartStatus:{$ne:#}}",
                companyId, shopId, startDate, endDate, ConsumerCart.ConsumerCartStatus.InProgress)
                .sort("{processedTime: -1}")
                .as(entityClazz);
    }

    @Override
    public Iterable<ConsumerCart> getBracketConsumerCartByStatus(String companyId, String shopId, ConsumerCart.ConsumerCartStatus status, Long startDate, Long endDate) {
        return coll.find("{companyId:#, shopId:#, orderPlacedTime: {$gt:#, $lt:#}, cartStatus:#}",
                companyId, shopId, startDate, endDate, status)
                .sort("{processedTime: -1}")
                .as(entityClazz);
    }

    @Override
    public <E extends ConsumerCart> SearchResult<E> getPlacedConsumerCartsByStatus(String companyId, String shopId, List<ConsumerCart.ConsumerCartStatus> statuses, int start, int limit, Class<E> clazz) {
        Iterable<E> items = coll.find("{companyId:#,shopId:#,cartStatus:{$in:#}}", companyId, shopId, statuses).sort("{orderPlacedTime:-1}").skip(start).limit(limit).as(clazz);
        long count = coll.count("{companyId:#,shopId:#,cartStatus:{$in:#}}", companyId, shopId, statuses);

        SearchResult<E> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setSkip(start);
        result.setLimit(limit);

        return result;
    }

}
