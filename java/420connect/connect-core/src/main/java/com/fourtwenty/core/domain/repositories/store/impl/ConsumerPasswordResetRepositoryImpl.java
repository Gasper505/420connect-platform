package com.fourtwenty.core.domain.repositories.store.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.store.ConsumerPasswordReset;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.ConsumerPasswordResetRepository;
import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * Created by mdo on 5/22/17.
 */
public class ConsumerPasswordResetRepositoryImpl extends MongoBaseRepositoryImpl<ConsumerPasswordReset> implements ConsumerPasswordResetRepository {

    @Inject
    public ConsumerPasswordResetRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ConsumerPasswordReset.class, mongoManager);
    }

    @Override
    public ConsumerPasswordReset getPasswordReset(String resetCode) {
        return coll.findOne("{resetCode:#}", resetCode).as(entityClazz);
    }

    @Override
    public Iterable<ConsumerPasswordReset> getPasswordResetForEmployee(String consumerUserId) {
        return coll.find("{consumerUserId:#}", consumerUserId).as(entityClazz);
    }

    @Override
    public void setPasswordExpired(String consumerUserId) {
        coll.update("{consumerUserId:#}", consumerUserId).multi().with("{$set: {expired:#, modified:#}}", true, DateTime.now().getMillis());
    }
}
