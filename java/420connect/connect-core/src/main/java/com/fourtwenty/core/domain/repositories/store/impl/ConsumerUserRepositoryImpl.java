package com.fourtwenty.core.domain.repositories.store.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mongodb.WriteResult;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by mdo on 12/19/16.
 */
public class ConsumerUserRepositoryImpl extends CompanyBaseRepositoryImpl<ConsumerUser> implements ConsumerUserRepository {
    @Inject
    public ConsumerUserRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ConsumerUser.class, mongoManager);
    }

    @Override
    public <E extends ConsumerUser> E getConsumerByEmail(String email, Class<E> clazz) {
        return coll.findOne("{email:#}", email).as(clazz);
    }

    @Override
    public ConsumerUser getConsumerByCPN(String companyId, String cpn) {
        Iterable<ConsumerUser> consumerUsers = coll.find("{companyId:#, cpn:#}", companyId, cpn).sort("{modified:-1}").as(entityClazz);
        for (ConsumerUser consumerUser : consumerUsers) {
            return consumerUser; // return first consumer
        }
        return null;
    }

    @Override
    public ConsumerUser getConsumerUserBySignedContractId(String signedContractId) {
        if (signedContractId == null || !ObjectId.isValid(signedContractId)) {
            return null;
        }
        Iterable<ConsumerUser> consumerUsers = coll.find("{signedContracts._id:#}", new ObjectId(signedContractId)).as(entityClazz);
        for (ConsumerUser consumerUser : consumerUsers) {
            return consumerUser;
        }
        return null;
    }


    @Override
    public Iterable<ConsumerUser> getConsumerUnsetCPN() {
        return coll.find("{cpn: {$exists: false}}").as(entityClazz);
    }

    @Override
    public WriteResult updateCPN(String consumerUserId, String cpn) {
        return coll.update("{_id: #}", new ObjectId(consumerUserId)).with("{$set: {cpn:#, modified:#}}", cpn, DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, ConsumerUser> listAsMap(List<ObjectId> ids) {
        Iterable<ConsumerUser> consumerUsers = coll.find("{_id : {$in: #}}", ids).as(entityClazz);
        HashMap<String, ConsumerUser> map = new HashMap<>();
        for (ConsumerUser item : consumerUsers) {
            map.put(item.getId(), item);
        }
        return map;
    }


    @Override
    public SearchResult<ConsumerUser> getConsumerUsersByCompanyId(String companyId, int skip, int limit) {
        Iterable<ConsumerUser> consumerUsers = coll.find("{sourceCompanyId:#}", companyId).skip(skip)
                .limit(limit).as(entityClazz);

        long count = coll.count("{sourceCompanyId:#}", companyId);

        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public Iterable<ConsumerUser> getConsumerByEmails(String companyId, Set<String> emailIds) {
        return coll.find("{companyId:#,deleted:false,email : {$in: #}}", companyId, emailIds).projection("{email:1}").as(entityClazz);
    }

    @Override
    public Iterable<ConsumerUser> list(int skip, int limit) {
        return coll.find().sort("{created:-1}").skip(skip).limit(limit).as(entityClazz);
    }

    @Override
    public <E extends ConsumerUser> E getConsumerByEmail(String companyId, String emailId, Class<E> clazz) {
        return coll.findOne("{companyId:#, email:#}", companyId, emailId).as(clazz);
    }

    @Override
    public SearchResult<ConsumerUser> findConsumersByAcceptance(String companyId, int skip, int limit, Boolean acceptance, String sortOptions, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{companyId:#, accepted:#, $or:[{firstName:#},{lastName:#}]}", companyId, acceptance, pattern, pattern).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, accepted:#, $or:[{firstName:#},{lastName:#}]}", companyId, acceptance, pattern, pattern);
        } else {
            consumerUsers = coll.find("{companyId:#, accepted:#}", companyId, acceptance).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, accepted:#}", companyId, acceptance);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ConsumerUser> findCreatedConsumerByDate(String companyId, int start, int limit, long startDate, long endDate, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{companyId:#, created: {$gt:#, $lt:#},$or:[{firstName:#},{lastName:#}]}", companyId, startDate, endDate, pattern, pattern).skip(start).limit(limit).sort("{created:-1}").as(entityClazz);
            count = coll.count("{companyId:#, created: {$gt:#, $lt:#},$or:[{firstName:#},{lastName:#}]}", companyId, startDate, endDate, pattern, pattern);
        } else {
            consumerUsers = coll.find("{companyId:#, created: {$gt:#, $lt:#}}", companyId, startDate, endDate).skip(start).limit(limit).sort("{created:-1}").as(entityClazz);
            count = coll.count("{companyId:#, created: {$gt:#, $lt:#}}", companyId, startDate, endDate);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }


    @Override
    public SearchResult<ConsumerUser> findCreatedConsumerByDateWithMarketingSource(String companyId, int start, int limit, long startDate, long endDate, String marketingSource, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{companyId:#, created: {$gt:#, $lt:#}, marketingSource:#,$or:[{firstName:#},{lastName:#}]}", companyId, startDate, endDate, marketingSource, pattern, pattern).skip(start).limit(limit).sort("{created:-1}").as(entityClazz);
            count = coll.count("{companyId:#, created: {$gt:#, $lt:#}, marketingSource:#,$or:[{firstName:#},{lastName:#}]}", companyId, startDate, endDate, marketingSource, pattern, pattern);
        } else {
            consumerUsers = coll.find("{companyId:#, created: {$gt:#, $lt:#}, marketingSource:#}", companyId, startDate, endDate, marketingSource).skip(start).limit(limit).sort("{created:-1}").as(entityClazz);
            count = coll.count("{companyId:#, created: {$gt:#, $lt:#}, marketingSource:#}", companyId, startDate, endDate, marketingSource);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }


    @Override
    public SearchResult<ConsumerUser> findConsumersByAcceptanceWithMarketingSource(String companyId, int skip, int limit, String marketingSource, Boolean acceptance, String sortOptions, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{companyId:#, marketingSource:#, accepted:#, $or:[{firstName:#},{lastName:#}]}", companyId, marketingSource, acceptance, pattern, pattern).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, marketingSource:#, accepted:#, $or:[{firstName:#},{lastName:#}]}", companyId, marketingSource, acceptance, pattern, pattern);
        } else {
            consumerUsers = coll.find("{companyId:#, marketingSource:#, accepted:#}", companyId, marketingSource, acceptance).skip(skip).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, marketingSource:#, accepted:#}", companyId, marketingSource, acceptance);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(skip);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ConsumerUser> findConsumersByIdAndAcceptanceWithMarketingSource(String companyId, int start, int limit, List<ObjectId> consumerUserIds, String marketingSource, boolean accepted, String sortOptions, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{_id: {$in:#}, companyId:#, marketingSource:#, accepted:#,$or:[{firstName:#},{lastName:#}]}", consumerUserIds, companyId, marketingSource, accepted, pattern, pattern).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{_id: {$in:#}, companyId:#, marketingSource:#, accepted:#,$or:[{firstName:#},{lastName:#}]}", consumerUserIds, companyId, marketingSource, accepted, pattern, pattern);
        } else {
            consumerUsers = coll.find("{_id: {$in:#}, companyId:#, marketingSource:#, accepted:#}", consumerUserIds, companyId, marketingSource, accepted).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{_id: {$in:#}, companyId:#, marketingSource:#, accepted:#}", consumerUserIds, companyId, marketingSource, accepted);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ConsumerUser> findConsumersByIdAndAcceptance(String companyId, int start, int limit, List<ObjectId> consumerUserIds, boolean accepted, String sortOptions, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{_id: {$in:#}, companyId:#, accepted:#,$or:[{firstName:#},{lastName:#}]}", consumerUserIds, companyId, accepted, pattern, pattern).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{_id: {$in:#}, companyId:#, accepted:#,$or:[{firstName:#},{lastName:#}]}", consumerUserIds, companyId, accepted, pattern, pattern);
        } else {
            consumerUsers = coll.find("{_id: {$in:#}, companyId:#, accepted:#}", consumerUserIds, companyId, accepted).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{_id: {$in:#}, companyId:#, accepted:#}", consumerUserIds, companyId, accepted);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ConsumerUser> findRejectedConsumers(String companyId, int start, int limit, Boolean acceptance, String sortOptions, List<ObjectId> consumerUserIds, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{companyId:#, accepted:#, $and : [{$or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}, {$or:[{firstName:#},{lastName:#}]}]}", companyId, acceptance, consumerUserIds, pattern, pattern).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, accepted:#,$and : [{$or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}, {$or:[{firstName:#},{lastName:#}]}]}", companyId, acceptance, consumerUserIds, pattern, pattern);
        } else {
            consumerUsers = coll.find("{companyId:#, accepted:#, $or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}", companyId, acceptance, consumerUserIds).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, accepted:#, $or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}", companyId, acceptance, consumerUserIds);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }

    @Override
    public SearchResult<ConsumerUser> findRejectedConsumersWithMarketingSource(String companyId, int start, int limit, String marketingSource, Boolean accepted, String sortOptions, List<ObjectId> consumerUserIds, String term) {
        Iterable<ConsumerUser> consumerUsers = null;
        long count = 0;
        if (StringUtils.isNotBlank(term)) {
            Pattern pattern = TextUtil.createPattern(term);
            consumerUsers = coll.find("{companyId:#, accepted:#, marketingSource:#, $and : [{$or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}, {$or:[{firstName:#},{lastName:#}]}]}", companyId, accepted, marketingSource, consumerUserIds, pattern, pattern).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, accepted:#, marketingSource:#, $and : [{$or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}, {$or:[{firstName:#},{lastName:#}]}]}", companyId, accepted, marketingSource, consumerUserIds, pattern, pattern);
        } else {
            consumerUsers = coll.find("{companyId:#, accepted:#, marketingSource:#, $or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}", companyId, accepted, marketingSource, consumerUserIds).skip(start).limit(limit).sort(sortOptions).as(entityClazz);
            count = coll.count("{companyId:#, accepted:#, marketingSource:#, $or:[{rejectedDate: {$exists: true}}, {$and:[{acceptedDate: null},{_id:{$nin: #}}]}]}", companyId, accepted, marketingSource, consumerUserIds);
        }
        SearchResult<ConsumerUser> searchResult = new SearchResult<>();
        searchResult.setValues(Lists.newArrayList(consumerUsers));
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setTotal(count);
        return searchResult;
    }


    @Override
    public long countConsumerByDrivingLicense(String companyId, String licenseNumber) {
        return coll.count("{companyId:#, deleted:false, dlNo:#}", companyId, licenseNumber);
    }

}
