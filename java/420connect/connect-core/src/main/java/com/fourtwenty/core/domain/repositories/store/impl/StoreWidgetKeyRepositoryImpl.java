package com.fourtwenty.core.domain.repositories.store.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.store.StoreWidgetKey;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.store.StoreWidgetKeyRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * Created by mdo on 4/17/17.
 */
public class StoreWidgetKeyRepositoryImpl extends ShopBaseRepositoryImpl<StoreWidgetKey> implements StoreWidgetKeyRepository {

    @Inject
    public StoreWidgetKeyRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(StoreWidgetKey.class, mongoManager);
    }

    @Override
    public StoreWidgetKey getStoreKey(String key) {
        return coll.findOne("{key:#}", key).as(entityClazz);
    }

    @Override
    public StoreWidgetKey getStoreKeyByActiveShop(String companyId, String shopId) {
        return coll.findOne("{companyId:#,shopId:#,active:true}", companyId, shopId).as(entityClazz);
    }

    @Override
    public void deleteAll(String companyId, String shopId) {
        coll.update("{companyId:#,shopId:#}", companyId, shopId).multi().with("{$set: {deleted:true,active:false, modified:#}}", DateTime.now().getMillis());
    }

    @Override
    public SearchResult<StoreWidgetKey> getActiveKeys(String companyId) {
        Iterable<StoreWidgetKey> items = coll.find("{companyId:#, active:true}", companyId).as(entityClazz);

        SearchResult<StoreWidgetKey> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal((long) result.getValues().size());
        result.setSkip(0);
        result.setLimit(result.getTotal().intValue());

        return result;

    }
}
