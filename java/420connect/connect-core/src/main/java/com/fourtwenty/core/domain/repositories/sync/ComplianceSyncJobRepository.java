package com.fourtwenty.core.domain.repositories.sync;

import com.fourtwenty.core.domain.models.compliance.ComplianceSyncJob;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface ComplianceSyncJobRepository extends MongoShopBaseRepository<ComplianceSyncJob> {
    Iterable<ComplianceSyncJob> getQueuedJobs();


    void markQueueJobAsInProgress(String syncJobId);
    void markQueueJobAsComplete(String syncJobId, String errorMsg);
    void updateQueueJobStatus(String syncJobId, ComplianceSyncJob.ComplianceSyncJobStatus status);
}
