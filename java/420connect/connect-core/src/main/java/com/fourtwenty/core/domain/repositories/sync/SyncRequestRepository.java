package com.fourtwenty.core.domain.repositories.sync;

import com.fourtwenty.core.domain.models.sync.SyncRequest;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface SyncRequestRepository extends MongoShopBaseRepository<SyncRequest> {
}
