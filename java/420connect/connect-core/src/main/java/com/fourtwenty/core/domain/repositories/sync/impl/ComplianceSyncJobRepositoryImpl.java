package com.fourtwenty.core.domain.repositories.sync.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.compliance.ComplianceSyncJob;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.sync.ComplianceSyncJobRepository;
import com.google.inject.Inject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

public class ComplianceSyncJobRepositoryImpl extends ShopBaseRepositoryImpl<ComplianceSyncJob> implements ComplianceSyncJobRepository {

    @Inject
    public ComplianceSyncJobRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ComplianceSyncJob.class, mongoManager);
    }

    @Override
    public Iterable<ComplianceSyncJob> getQueuedJobs() {
        return coll.find("{status:#}",ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED).sort("{created:1}").as(entityClazz);
    }


    @Override
    public void markQueueJobAsInProgress(String syncJobId) {
        coll.update(new ObjectId(syncJobId)).with("{$set:{status:#,startTime:#,modified:#}}",
                ComplianceSyncJob.ComplianceSyncJobStatus.INPROGRESS,
                DateTime.now().getMillis(),
                DateTime.now().getMillis());
    }

    @Override
    public void markQueueJobAsComplete(String syncJobId, String errorMsg) {
        coll.update(new ObjectId(syncJobId)).with("{$set:{status:#,completeTime:#,modified:#, errorMsg : #}}",
                ComplianceSyncJob.ComplianceSyncJobStatus.COMPLETE,
                DateTime.now().getMillis(),
                DateTime.now().getMillis(), errorMsg);
    }

    @Override
    public void updateQueueJobStatus(String syncJobId, ComplianceSyncJob.ComplianceSyncJobStatus status) {
        coll.update(new ObjectId(syncJobId)).with("{$set:{status:#,modified:#}}",status, DateTime.now().getMillis());
    }
}

