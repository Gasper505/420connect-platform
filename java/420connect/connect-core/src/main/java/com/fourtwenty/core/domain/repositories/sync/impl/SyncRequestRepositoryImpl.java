package com.fourtwenty.core.domain.repositories.sync.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.sync.SyncRequest;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.sync.SyncRequestRepository;

import javax.inject.Inject;

public class SyncRequestRepositoryImpl extends ShopBaseRepositoryImpl<SyncRequest> implements SyncRequestRepository {

    @Inject
    public SyncRequestRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(SyncRequest.class, mongoManager);
    }
}
