package com.fourtwenty.core.domain.repositories.testsample;

import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.HashMap;
import java.util.List;

public interface TestSampleRepository extends MongoShopBaseRepository<TestSample> {
    SearchResult<TestSample> getAllTestSamplesByBatchId(final String companyId, final String incomingBatchId, final String sortOption, final int start, final int limit);

    long countItemsByProductBatch(final String companyId, final String shopId, final String batchId);

    HashMap<String, Long> countItemsByProductBatchAsMap(final String companyId, final String shopId, final List<String> batchIds);

    HashMap<String, List<TestSample>> getTestSampleByBatchAsMap(final String companyId, final String shopId, final List<String> batchIds);
}
