package com.fourtwenty.core.domain.repositories.testsample.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class TestSampleRepositoryImpl extends ShopBaseRepositoryImpl<TestSample> implements TestSampleRepository {

    @Inject
    public TestSampleRepositoryImpl(MongoDb mongoDb) throws Exception {
        super(TestSample.class, mongoDb);
    }

    @Override
    public SearchResult<TestSample> getAllTestSamplesByBatchId(String companyId, String incomingBatchId, String sortOption, int start, int limit) {
        Iterable<TestSample> items = coll.find("{companyId:#,batchId:#,deleted:false}", companyId, incomingBatchId).sort(sortOption).skip(start).limit(limit).as(entityClazz);
        long count = coll.count("{companyId:#,batchId:#,deleted:false}", companyId, incomingBatchId);
        SearchResult<TestSample> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(items));
        result.setTotal(count);
        result.setLimit(limit);
        result.setSkip(start);
        return result;
    }

    @Override
    public long countItemsByProductBatch(String companyId, String shopId, String batchId) {
        return coll.count("{companyId:#,shopId:#,batchId:#,}", companyId, shopId, batchId);
    }

    @Override
    public HashMap<String, Long> countItemsByProductBatchAsMap(String companyId, String shopId, List<String> batchIds) {
        Iterable<TestSample> items = coll.find("{ companyId: #, shopId : #, deleted : false, batchId : { $in:#},status: { $in: [#,#] } }", companyId, shopId, batchIds, TestSample.SampleStatus.IN_TESTING, TestSample.SampleStatus.PENDING).as(entityClazz);
        HashMap<String, Long> sampleCount = new HashMap<>();
        for (TestSample testSample : items) {
            long count = 1;
            if (sampleCount.containsKey(testSample.getBatchId())) {
                count += sampleCount.get(testSample.getBatchId());
            }
            sampleCount.put(testSample.getBatchId(), count);
        }
        return sampleCount;
    }

    @Override
    public HashMap<String, List<TestSample>> getTestSampleByBatchAsMap(String companyId, String shopId, List<String> batchIds) {
        Iterable<TestSample> items = coll.find("{ companyId: #, shopId : #, deleted : false, batchId : { $in:#}}", companyId, shopId, batchIds).as(entityClazz);
        HashMap<String, List<TestSample>> returnMap = new HashMap<>();
        for (TestSample testSample : items) {
            returnMap.putIfAbsent(testSample.getBatchId(), Lists.newArrayList());
            returnMap.get(testSample.getBatchId()).add(testSample);
        }
        return returnMap;
    }
}
