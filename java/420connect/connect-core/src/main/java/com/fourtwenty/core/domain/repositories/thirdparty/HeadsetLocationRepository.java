package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by Gaurav Saini on 10/7/17.
 */
public interface HeadsetLocationRepository extends MongoShopBaseRepository<HeadsetLocation> {
    Iterable<HeadsetLocation> getHeadsetLocations(String companyId);

    HeadsetLocation getHeadsetLocation(String companyId, String shopId);

    Iterable<HeadsetLocation> getAcceptedHeadsetLocations();
}
