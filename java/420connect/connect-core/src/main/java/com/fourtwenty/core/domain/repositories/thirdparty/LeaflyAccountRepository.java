package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;
import com.fourtwenty.core.thirdparty.leafly.result.LeaflyAccountResult;

public interface LeaflyAccountRepository extends MongoShopBaseRepository<LeaflyAccount> {
    LeaflyAccountResult getAccountByShop(String companyId, String shopId);
}
