package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface LeaflySyncJobRepository extends MongoShopBaseRepository<LeaflySyncJob> {

    void updateJobStatus(String jobId, LeaflySyncJob.LeaflySyncJobStatus status, String msg);

    void markQueueJobAsInProgress(String leaflySyncJobId);

    Iterable<LeaflySyncJob> getQueuedJobs();
}
