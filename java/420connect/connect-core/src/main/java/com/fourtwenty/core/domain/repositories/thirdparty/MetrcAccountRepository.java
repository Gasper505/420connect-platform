package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 8/25/17.
 */
public interface MetrcAccountRepository extends MongoCompanyBaseRepository<MetrcAccount> {
    MetrcAccount getMetrcAccount(String companyId, String stateCode);

    HashMap<String, MetrcAccount> getMetrcAccountWithStateCode(String companyId, List<String> stateCodes);
}
