package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface OnFleetErrorLogRepository extends MongoShopBaseRepository<OnFleetErrorLog> {
}
