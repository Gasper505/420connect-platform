package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.mongodb.WriteResult;

/**
 * Created by mdo on 2/10/17.
 */
public interface ThirdPartyAccountRepository extends MongoCompanyBaseRepository<ThirdPartyAccount> {
    public ThirdPartyAccount findByAccountType(String companyId, ThirdPartyAccount.ThirdPartyAccountType accountType);

    public ThirdPartyAccount addQuickbookDetails(BearerTokenResponse bearerTokenResponse, String companyId, String quickbook_companyId, String shopId);

    public ThirdPartyAccount updteQuickbookDetails(ThirdPartyAccount account, BearerTokenResponse bearerTokenResponse, String companyId, String quickbook_companyId, String shopId);

    public WriteResult deleteAccount(String companyId, String shopId, ThirdPartyAccount.ThirdPartyAccountType accountType);

    public ThirdPartyAccount findByAccountTypeByShopId(String companyId, String shopId, ThirdPartyAccount.ThirdPartyAccountType accountType);

}
