package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by mdo on 8/23/17.
 */
public interface ThirdPartyShopAccountRepository extends MongoShopBaseRepository<ThirdPartyShopAccount> {
    ThirdPartyShopAccount getTPShopAccount(String companyId, String shopId, ThirdPartyShopAccount.ThirdPartyShopAccountType accountType);

    void migrateIndex();

}
