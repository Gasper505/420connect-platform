package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

/**
 * Created by mdo on 4/12/17.
 */
public interface WeedmapAccountRepository extends MongoShopBaseRepository<WeedmapAccount> {
    WeedmapAccount getWeedmapAccount(String companyId, String shopId);

    <T extends WeedmapAccount> T getWeedmapAccount(String companyId, String shopId, Class<T> clazz);

    void updateSyncStatus(String companyId, String entityId, WeedmapAccount.WeedmapSyncStatus status, long lastSyncTime);

}
