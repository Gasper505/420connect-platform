package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;

import java.util.HashMap;
import java.util.List;

public interface WeedmapSyncItemRepository extends MongoCompanyBaseRepository<WmSyncItems> {

    HashMap<String, WmSyncItems> getSyncItemsByItemIdsAsMap(String companyId, String shopId, String wmListingId, List<String> itemIds);

    Iterable<WmSyncItems> getAllItemsAsMap(String companyId, String shopId, String wmListingId);

    WmSyncItems getItemByType(String companyId, String shopId, String wmListingId, WmSyncItems.WMItemType type);

    HashMap<String, HashMap<String, WmSyncItems>> getSyncItemsByItemIdsAsMap(String companyId, String shopId, List<String> itemIds);

    HashMap<String, WmSyncItems> getWmItemsByListingIdAsMap(String companyId, String shopId, List<String> wmListingIds, WmSyncItems.WMItemType type);

    HashMap<String, WmSyncItems> getItemsByItemIdsAsMap(String companyId, String shopId, List<String> itemIds);
}
