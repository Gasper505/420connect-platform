package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.ProductTagGroups;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.domain.mongo.MongoCompanyBaseRepository;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

public interface WmMappingRepository extends MongoCompanyBaseRepository<WmProductMapping> {

    Iterable<WmProductMapping> getMappingForAccount(String companyId, String shopId, String accountId);

    WmProductMapping getMappingForProduct(String companyId, String shopId, String productId);

    void updateTagsForCategory(String companyId, String shopId, String account, String categoryId, List<String> tags);

    HashMap<String, WmProductMapping> getMappingForProducts(String companyId, String shopId, List<String> productId);

    void deleteAccountProductMapping(String companyId, String shopId, String accountId);

    void updateTagGroups(List<ObjectId> ids, List<ProductTagGroups> productTags);
}
