package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.weedmap.WeedmapSyncJob;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface WmSyncJobRepository extends MongoShopBaseRepository<WeedmapSyncJob> {
    void updateJobStatus(String jobId, WeedmapSyncJob.WmSyncJobStatus status, String msg);

    void markQueueJobAsInProgress(String weedmapSyncJobId);

    Iterable<WeedmapSyncJob> getQueuedJobs();
}
