package com.fourtwenty.core.domain.repositories.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.domain.repositories.base.BaseRepository;
@Deprecated
public interface WmTagGroupRepository extends BaseRepository<WmTagGroups> {
}
