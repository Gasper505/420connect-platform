package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.HeadsetLocationRepository;

import javax.inject.Inject;

/**
 * Created by Gaurav Saini on 10/7/17.
 */
public class HeadsetLocationRepositoryImpl extends ShopBaseRepositoryImpl<HeadsetLocation> implements HeadsetLocationRepository {

    @Inject
    public HeadsetLocationRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(HeadsetLocation.class, mongoManager);
    }

    @Override
    public Iterable<HeadsetLocation> getHeadsetLocations(String companyId) {
        return coll.find("{companyId:#}", companyId).as(entityClazz);
    }

    @Override
    public HeadsetLocation getHeadsetLocation(String companyId, String shopId) {
        return coll.findOne("{companyId:#,shopId:#}", companyId, shopId).as(entityClazz);
    }

    @Override
    public Iterable<HeadsetLocation> getAcceptedHeadsetLocations() {
        return coll.find("{acceptedLiteVersion:true}").as(entityClazz);
    }
}
