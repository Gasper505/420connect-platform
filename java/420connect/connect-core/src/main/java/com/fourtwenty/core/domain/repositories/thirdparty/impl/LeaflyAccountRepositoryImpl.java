package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflyAccountRepository;
import com.fourtwenty.core.thirdparty.leafly.result.LeaflyAccountResult;
import com.google.inject.Inject;

public class LeaflyAccountRepositoryImpl extends ShopBaseRepositoryImpl<LeaflyAccount> implements LeaflyAccountRepository {

    @Inject
    public LeaflyAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(LeaflyAccount.class, mongoManager);
    }

    @Override
    public LeaflyAccountResult getAccountByShop(String companyId, String shopId) {
        return coll.findOne("{companyId:#, shopId:#, deleted:false}", companyId, shopId).as(LeaflyAccountResult.class);
    }
}
