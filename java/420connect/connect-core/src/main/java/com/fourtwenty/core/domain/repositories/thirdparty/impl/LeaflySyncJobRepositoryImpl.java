package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflySyncJobRepository;
import com.google.inject.Inject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

public class LeaflySyncJobRepositoryImpl extends ShopBaseRepositoryImpl<LeaflySyncJob> implements LeaflySyncJobRepository {

    @Inject
    public LeaflySyncJobRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(LeaflySyncJob.class, mongoManager);
    }

    @Override
    public void updateJobStatus(String jobId, LeaflySyncJob.LeaflySyncJobStatus status, String msg) {
        coll.update("{_id:#}", new ObjectId(jobId)).with("{$set : {errorMsg:#, completeTime:#, status:#} }", msg, DateTime.now().getMillis(), status);
    }

    @Override
    public void markQueueJobAsInProgress(String leaflySyncJobId) {
        coll.update("{_id:#}", new ObjectId(leaflySyncJobId)).with("{$set : {status:#} }", LeaflySyncJob.LeaflySyncJobStatus.InProgress);
    }

    @Override
    public Iterable<LeaflySyncJob> getQueuedJobs() {
        return coll.find("{status:#}", LeaflySyncJob.LeaflySyncJobStatus.Queued).sort("{created:1}").as(entityClazz);
    }
}
