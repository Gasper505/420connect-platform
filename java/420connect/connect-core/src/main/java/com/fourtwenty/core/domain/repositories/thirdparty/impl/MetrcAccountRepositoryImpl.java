package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 8/25/17.
 */
public class MetrcAccountRepositoryImpl extends CompanyBaseRepositoryImpl<MetrcAccount> implements MetrcAccountRepository {

    @Inject
    public MetrcAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(MetrcAccount.class, mongoManager);
    }

    @Override
    public MetrcAccount getMetrcAccount(String companyId, String stateCode) {
        return coll.findOne("{companyId:#, stateCode:#}", companyId, stateCode).as(entityClazz);
    }

    @Override
    public HashMap<String, MetrcAccount> getMetrcAccountWithStateCode(String companyId, List<String> stateCodes) {
        Iterable<MetrcAccount> metrcAccounts = coll.find("{companyId:#,deleted:false,stateCode:{$in:#}}", companyId, stateCodes).as(entityClazz);
        HashMap<String, MetrcAccount> stateCodeHashMap = new HashMap<>();
        for (MetrcAccount item : metrcAccounts) {
            stateCodeHashMap.put(item.getStateCode(), item);
        }
        return stateCodeHashMap;
    }
}
