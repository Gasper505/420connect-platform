package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.OnFleetErrorLogRepository;

import javax.inject.Inject;

public class OnFleetErrorLogRepositoryImpl extends ShopBaseRepositoryImpl<OnFleetErrorLog> implements OnFleetErrorLogRepository {

    @Inject
    public OnFleetErrorLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(OnFleetErrorLog.class, mongoManager);
    }
}
