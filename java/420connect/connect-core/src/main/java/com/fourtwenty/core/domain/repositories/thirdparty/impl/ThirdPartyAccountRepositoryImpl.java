package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.mongo.impl.CompanyBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.quickbook.BearerTokenResponse;
import com.google.inject.Inject;
import com.mongodb.WriteResult;

/**
 * Created by mdo on 2/10/17.
 */
public class ThirdPartyAccountRepositoryImpl extends CompanyBaseRepositoryImpl<ThirdPartyAccount> implements ThirdPartyAccountRepository {
    @Inject
    public ThirdPartyAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ThirdPartyAccount.class, mongoManager);
    }

    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;
    @Inject
    private ConnectConfiguration configuration;


    public ThirdPartyAccount addQuickbookDetails(BearerTokenResponse bearerTokenResponse, String companyId, String quickbook_companyId, String shopId) {
        ThirdPartyAccount account = new ThirdPartyAccount();
        account.setAccountType(ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
        account.setName(configuration.getQuickBookConfig().getName());
        account.setUsername(bearerTokenResponse.getRefreshToken());
        account.setPassword(bearerTokenResponse.getAccessToken());
        account.setCompanyId(companyId);
        account.setQuickbook_companyId(quickbook_companyId);
        account.setShopId(shopId);
        thirdPartyAccountRepository.save(account);
        return account;

    }

    public ThirdPartyAccount updteQuickbookDetails(ThirdPartyAccount account, BearerTokenResponse bearerTokenResponse, String companyId, String quickbook_companyId, String shopId) {
        if (bearerTokenResponse != null) {
            if (bearerTokenResponse.getAccessToken() != null) {
                account.setAccountType(ThirdPartyAccount.ThirdPartyAccountType.Quickbook);
                account.setName(configuration.getQuickBookConfig().getName());
                account.setUsername(bearerTokenResponse.getRefreshToken());
                account.setPassword(bearerTokenResponse.getAccessToken());
                account.setCompanyId(companyId);
                account.setQuickbook_companyId(quickbook_companyId);
                account.setShopId(shopId);
                thirdPartyAccountRepository.save(account);
                return account;
            }
        }
        return account;
    }

    @Override
    public ThirdPartyAccount findByAccountType(String companyId, ThirdPartyAccount.ThirdPartyAccountType accountType) {
        return coll.findOne("{companyId:#,accountType:#}", companyId, accountType).as(entityClazz);

    }

    @Override
    public WriteResult deleteAccount(String companyId, String shopId, ThirdPartyAccount.ThirdPartyAccountType accountType) {
        WriteResult result = coll.remove("{companyId:#,shopId:#,accountType:#}", companyId, shopId, accountType);
        return result;

    }

    @Override
    public ThirdPartyAccount findByAccountTypeByShopId(String companyId, String shopId, ThirdPartyAccount.ThirdPartyAccountType accountType) {
        return coll.findOne("{companyId:#,shopId:#,accountType:#}", companyId, shopId, accountType).as(entityClazz);
    }
}
