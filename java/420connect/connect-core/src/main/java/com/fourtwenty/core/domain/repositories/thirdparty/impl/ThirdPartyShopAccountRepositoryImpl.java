package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyShopAccountRepository;
import com.google.inject.Inject;

/**
 * Created by mdo on 8/23/17.
 */
public class ThirdPartyShopAccountRepositoryImpl extends ShopBaseRepositoryImpl<ThirdPartyShopAccount> implements ThirdPartyShopAccountRepository {

    @Inject
    public ThirdPartyShopAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(ThirdPartyShopAccount.class, mongoManager);
    }

    @Override
    public ThirdPartyShopAccount getTPShopAccount(String companyId, String shopId, ThirdPartyShopAccount.ThirdPartyShopAccountType accountType) {
        return coll.findOne("{companyId:#,shopId:#,accountType:#}", companyId, shopId, accountType).as(entityClazz);
    }

    @Override
    public void migrateIndex() {
        coll.dropIndex("{companyId:1,shop:1,accountType:1}");
    }
}
