package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapAccountRepository;
import org.bson.types.ObjectId;

import javax.inject.Inject;

/**
 * Created by mdo on 4/12/17.
 */
public class WeedmapAccountRepositoryImpl extends ShopBaseRepositoryImpl<WeedmapAccount> implements WeedmapAccountRepository {
    @Inject
    public WeedmapAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(WeedmapAccount.class, mongoManager);
    }

    @Override
    public WeedmapAccount getWeedmapAccount(String companyId, String shopId) {
        return getWeedmapAccount(companyId, shopId, WeedmapAccount.class);
    }

    @Override
    public <T extends WeedmapAccount> T getWeedmapAccount(String companyId, String shopId, Class<T> clazz) {
        return coll.findOne("{companyId:#,shopId:#,deleted:false}", companyId, shopId).as(clazz);
    }


    @Override
    public WeedmapAccount update(String companyId, String entityId, WeedmapAccount pojo) {
        return super.update(companyId, entityId, pojo);
    }

    @Override
    public void updateSyncStatus(String companyId, String entityId, WeedmapAccount.WeedmapSyncStatus syncStatus, long lastSyncTime) {
        coll.update(new ObjectId(entityId)).with("{$set:{syncStatus:#,menuLastSyncDate:#}}",syncStatus,lastSyncTime);
    }
}
