package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapSyncItemRepository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class WeedmapSyncItemRepositoryImpl extends ShopBaseRepositoryImpl<WmSyncItems> implements WeedmapSyncItemRepository {

    @Inject
    public WeedmapSyncItemRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(WmSyncItems.class, mongoManager);
    }

    @Override
    public HashMap<String, WmSyncItems> getSyncItemsByItemIdsAsMap(String companyId, String shopId, String weedmapKeyId, List<String> itemIds) {
        Iterable<WmSyncItems> items = coll.find("{companyId:#, shopId:#, wmListingId: #, itemId:{$in:#}}", companyId, shopId, weedmapKeyId, itemIds).sort("{modified:-1}").as(entityClazz);

        HashMap<String, WmSyncItems> returnMap = new HashMap<>();

        for (WmSyncItems syncItem : items) {
            if (syncItem.isVerified() && syncItem.getItemType() == WmSyncItems.WMItemType.BRANDS) {
                continue;
            }
            returnMap.putIfAbsent(syncItem.getItemId(), syncItem);
        }
        return returnMap;
    }

    @Override
    public Iterable<WmSyncItems> getAllItemsAsMap(String companyId, String shopId, String wmKeyId) {
        return coll.find("{companyId:#, shopId:#, wmListingId: #}", companyId, shopId, wmKeyId).as(entityClazz);
    }

    @Override
    public WmSyncItems getItemByType(String companyId, String shopId, String wmKeyId, WmSyncItems.WMItemType type) {
        return coll.findOne("{companyId:#, shopId:#, wmListingId:#, itemType:#}", companyId, shopId, wmKeyId, type).as(entityClazz);
    }

    @Override
    public HashMap<String, HashMap<String, WmSyncItems>> getSyncItemsByItemIdsAsMap(String companyId, String shopId, List<String> itemIds) {
        Iterable<WmSyncItems> items = coll.find("{companyId:#, shopId:#, itemId:{$in:#}}", companyId, shopId, itemIds).as(entityClazz);

        HashMap<String, HashMap<String, WmSyncItems>> returnMap = new HashMap<>();

        for (WmSyncItems syncItem : items) {
            returnMap.putIfAbsent(syncItem.getWmListingId(), new HashMap<>());
            HashMap<String, WmSyncItems> map = returnMap.get(syncItem.getWmListingId());
            map.put(syncItem.getItemId(), syncItem);

        }
        return returnMap;
    }

    @Override
    public HashMap<String, WmSyncItems> getWmItemsByListingIdAsMap(String companyId, String shopId, List<String> wmListingIds, WmSyncItems.WMItemType type) {
        Iterable<WmSyncItems> items = coll.find("{companyId:#, shopId:#, itemType:#, wmListingId: {$in:#}}", companyId, shopId, type.getWeedmapType(), wmListingIds).as(entityClazz);
        HashMap<String, WmSyncItems> map = new HashMap<>();
        for (WmSyncItems item : items) {
            map.putIfAbsent(item.getWmListingId(), item);
        }
        return map;
    }

    @Override
    public HashMap<String, WmSyncItems> getItemsByItemIdsAsMap(String companyId, String shopId, List<String> itemIds) {
        Iterable<WmSyncItems> items = coll.find("{companyId:#, shopId:#, itemId:{$in:#}}", companyId, shopId, itemIds).as(entityClazz);

        HashMap<String, WmSyncItems> returnMap = new HashMap<>();

        for (WmSyncItems syncItem : items) {
            returnMap.put(syncItem.getItemId(), syncItem);
        }
        return returnMap;
    }

}
