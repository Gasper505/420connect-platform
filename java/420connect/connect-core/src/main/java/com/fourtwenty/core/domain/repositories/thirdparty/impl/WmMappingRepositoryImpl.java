package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.ProductTagGroups;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.WmMappingRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class WmMappingRepositoryImpl extends ShopBaseRepositoryImpl<WmProductMapping> implements WmMappingRepository {

    @Inject
    public WmMappingRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(WmProductMapping.class, mongoManager);
    }

    @Override
    public Iterable<WmProductMapping> getMappingForAccount(String companyId, String shopId, String accountId) {
        return coll.find("{companyId:#, shopId:#, wmAccountId:#, deleted:false}", companyId, shopId, accountId).as(entityClazz);
    }

    @Override
    public WmProductMapping getMappingForProduct(String companyId, String shopId, String productId) {
        return coll.findOne("{companyId:#, shopId:#, productId:#, deleted: false}", companyId, shopId, productId).as(entityClazz);
    }

    @Override
    public void updateTagsForCategory(String companyId, String shopId, String accountId, String categoryId, List<String> tags) {
        coll.update("{companyId: #, shopId: #, categoryId: #, deleted: false, wmAccountId: #}", companyId, shopId, categoryId, accountId).multi().with("{$set:{wmTags:#, modified:#}}", tags, DateTime.now().getMillis());
    }

    @Override
    public HashMap<String, WmProductMapping> getMappingForProducts(String companyId, String shopId, List<String> productIds) {
        Iterable<WmProductMapping> items = coll.find("{companyId:#, shopId:#, productId:{$in:#}, deleted: false}", companyId, shopId, productIds).as(entityClazz);

        HashMap<String, WmProductMapping> returnMap = new HashMap<>();

        for (WmProductMapping mapping : items) {
            returnMap.put(mapping.getProductId(), mapping);
        }

        return returnMap;
    }

    @Override
    public void deleteAccountProductMapping(String companyId, String shopId, String accountId) {
        coll.update("{companyId: #, shopId: #, wmAccountId: #}", companyId, shopId, accountId).multi().with("{$set:{deleted:#, modified:#}}", true, DateTime.now().getMillis());
    }

    @Override
    public void updateTagGroups(List<ObjectId> ids, List<ProductTagGroups> productTags) {
        coll.update("{_id: {$in:#}}", ids).multi().with("{$set:{productTagGroups:#, modified:#}}", productTags, DateTime.now().getMillis());
    }
}
