package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.weedmap.WeedmapSyncJob;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.WmSyncJobRepository;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;

public class WmSyncJobRepositoryImpl extends ShopBaseRepositoryImpl<WeedmapSyncJob> implements WmSyncJobRepository {

    @Inject
    public WmSyncJobRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(WeedmapSyncJob.class, mongoManager);
    }

    @Override
    public void updateJobStatus(String jobId, WeedmapSyncJob.WmSyncJobStatus status, String msg) {
        coll.update("{_id:#}", new ObjectId(jobId)).with("{$set : {errorMsg:#, completeTime:#, status:#} }", msg, DateTime.now().getMillis(), status);
    }

    @Override
    public void markQueueJobAsInProgress(String weedmapSyncJobId) {
        coll.update("{_id:#}", new ObjectId(weedmapSyncJobId)).with("{$set : {status:#} }", WeedmapSyncJob.WmSyncJobStatus.InProgress);
    }

    @Override
    public Iterable<WeedmapSyncJob> getQueuedJobs() {
        return coll.find("{status:#}",WeedmapSyncJob.WmSyncJobStatus.Queued).sort("{created:1}").as(entityClazz);
    }
}
