package com.fourtwenty.core.domain.repositories.thirdparty.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.domain.mongo.impl.MongoBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.thirdparty.WmTagGroupRepository;

import javax.inject.Inject;

@Deprecated
public class WmTagGroupRepositoryImpl extends MongoBaseRepositoryImpl<WmTagGroups> implements WmTagGroupRepository {

    @Inject
    public WmTagGroupRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(WmTagGroups.class, mongoManager);
    }
}
