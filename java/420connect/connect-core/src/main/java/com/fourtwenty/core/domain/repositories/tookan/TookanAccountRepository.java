package com.fourtwenty.core.domain.repositories.tookan;

import com.fourtwenty.core.domain.models.tookan.TookanAccount;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface TookanAccountRepository extends MongoShopBaseRepository<TookanAccount> {

    TookanAccount getAccountByShop(String companyId, String shopId);
}
