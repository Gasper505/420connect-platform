package com.fourtwenty.core.domain.repositories.tookan;

import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface TookanErrorLogRepository extends MongoShopBaseRepository<TookanErrorLog> {
}
