package com.fourtwenty.core.domain.repositories.tookan;

import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.domain.mongo.MongoShopBaseRepository;

public interface TookanTeamRepository extends MongoShopBaseRepository<TookanTeams> {

    TookanTeams getTeamByShop(String companyId, String shopId);
}
