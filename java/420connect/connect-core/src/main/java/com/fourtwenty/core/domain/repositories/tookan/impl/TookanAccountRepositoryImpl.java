package com.fourtwenty.core.domain.repositories.tookan.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.tookan.TookanAccount;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.tookan.TookanAccountRepository;
import com.google.inject.Inject;

public class TookanAccountRepositoryImpl extends ShopBaseRepositoryImpl<TookanAccount> implements TookanAccountRepository {

    @Inject
    public TookanAccountRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TookanAccount.class, mongoManager);
    }

    @Override
    public TookanAccount getAccountByShop(String companyId, String shopId) {
        return coll.findOne("{ companyId:#,shopId:#,deleted:false}", companyId, shopId).as(entityClazz);
    }
}
