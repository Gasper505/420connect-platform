package com.fourtwenty.core.domain.repositories.tookan.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.tookan.TookanErrorLogRepository;
import com.google.inject.Inject;

public class TookanErrorLogRepositoryImpl extends ShopBaseRepositoryImpl<TookanErrorLog> implements TookanErrorLogRepository {

    @Inject
    public TookanErrorLogRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TookanErrorLog.class, mongoManager);
    }
}
