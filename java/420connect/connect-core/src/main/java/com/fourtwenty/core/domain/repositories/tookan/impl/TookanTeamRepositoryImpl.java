package com.fourtwenty.core.domain.repositories.tookan.impl;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.domain.mongo.impl.ShopBaseRepositoryImpl;
import com.fourtwenty.core.domain.repositories.tookan.TookanTeamRepository;
import com.google.inject.Inject;

public class TookanTeamRepositoryImpl extends ShopBaseRepositoryImpl<TookanTeams> implements TookanTeamRepository {

    @Inject
    public TookanTeamRepositoryImpl(MongoDb mongoManager) throws Exception {
        super(TookanTeams.class, mongoManager);
    }

    @Override
    public TookanTeams getTeamByShop(String companyId, String shopId) {
        return coll.findOne("{ companyId : # ,shopId : #, deleted : false }", companyId, shopId).as(entityClazz);
    }
}
