package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.util.NumberUtils;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalFourDigitsSerializer extends JsonSerializer<BigDecimal> {

    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {

        double dValue = NumberUtils.round(value, 4);
        gen.writeNumber(dValue);
    }
}
