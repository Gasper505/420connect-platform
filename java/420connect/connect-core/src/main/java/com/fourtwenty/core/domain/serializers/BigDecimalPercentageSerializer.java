package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.util.NumberUtils;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by mdo on 4/13/17.
 */
public class BigDecimalPercentageSerializer extends JsonSerializer<BigDecimal> {

    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {

        double dvalue = NumberUtils.round(value, 4);
        gen.writeNumber(dvalue);
        //gen.writeNumber(value.setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue());
    }
}
