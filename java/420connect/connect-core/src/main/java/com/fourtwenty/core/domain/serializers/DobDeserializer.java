package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Objects;

public class DobDeserializer extends JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Long date = p.getValueAsLong();
        if (!Objects.isNull(date)) {
            DateTime dateTime = new DateTime(date);
            if (dateTime.getYear() >= 1900) {
                return date;
            }
        }
        return null;
    }
}
