package com.fourtwenty.core.domain.serializers;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.DateTime;

import java.io.IOException;

public class DobSerializer extends JsonSerializer<Long> {

    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        long date = value;
        DateTime dateTime = new DateTime(date);
        if (dateTime.getYear() < 1900) {
            gen.writeNull();
        } else {
            gen.writeNumber(value);
        }
    }
}
