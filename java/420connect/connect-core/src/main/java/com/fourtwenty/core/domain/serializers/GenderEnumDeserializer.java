package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * Created by mdo on 11/7/17.
 */
public class GenderEnumDeserializer extends JsonDeserializer<BaseMember.Gender> {

    @Override
    public BaseMember.Gender deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (StringUtils.isNotBlank(p.getValueAsString())) {
            String value = p.getValueAsString();
            try {
                int intValue = Integer.parseInt(value);
                return BaseMember.Gender.toGender(intValue);
            } catch (Exception e) {
                // ignore
            }
            return BaseMember.Gender.toGender(value);
        } else {
            return BaseMember.Gender.toGender(p.getValueAsInt());
        }
    }
}
