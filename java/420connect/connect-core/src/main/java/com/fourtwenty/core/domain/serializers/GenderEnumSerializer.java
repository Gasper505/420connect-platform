package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.domain.models.customer.BaseMember;

import java.io.IOException;

/**
 * Created by mdo on 11/7/17.
 */
public class GenderEnumSerializer extends JsonSerializer<BaseMember.Gender> {

    @Override
    public void serialize(BaseMember.Gender value,
                          JsonGenerator gen,
                          SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeNumber(value.toValue());
    }
}
