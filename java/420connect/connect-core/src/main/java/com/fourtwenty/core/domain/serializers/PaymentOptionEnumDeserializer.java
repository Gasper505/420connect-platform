package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fourtwenty.core.domain.models.transaction.Cart;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;


/**
 * Created by mdo on 11/7/17.
 */
public class PaymentOptionEnumDeserializer extends JsonDeserializer<Cart.PaymentOption> {

    @Override
    public Cart.PaymentOption deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (StringUtils.isNotBlank(p.getValueAsString())) {
            String value = p.getValueAsString();
            return Cart.PaymentOption.valueOf(value);
        }
        return Cart.PaymentOption.None;
    }
}
