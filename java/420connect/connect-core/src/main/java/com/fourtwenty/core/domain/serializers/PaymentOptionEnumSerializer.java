package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.domain.models.transaction.Cart;

import java.io.IOException;

public class PaymentOptionEnumSerializer extends JsonSerializer<Cart.PaymentOption> {

    @Override
    public void serialize(Cart.PaymentOption value,
                          JsonGenerator gen,
                          SerializerProvider serializers) throws IOException, JsonProcessingException {

        gen.writeFieldName(value.toString());
    }

}
