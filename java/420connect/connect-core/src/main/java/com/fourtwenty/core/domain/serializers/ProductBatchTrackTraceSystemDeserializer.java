package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;


public class ProductBatchTrackTraceSystemDeserializer extends JsonDeserializer<ProductBatch.TrackTraceSystem> {

    @Override
    public ProductBatch.TrackTraceSystem deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (StringUtils.isNotBlank(p.getValueAsString())) {
            String value = p.getValueAsString().toUpperCase();
            return ProductBatch.TrackTraceSystem.valueOf(value);
        }

        return null;
    }
}
