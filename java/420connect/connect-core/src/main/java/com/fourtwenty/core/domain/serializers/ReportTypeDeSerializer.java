package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fourtwenty.core.reporting.ReportType;

import java.io.IOException;
import java.util.Objects;

public class ReportTypeDeSerializer extends JsonDeserializer<ReportType> {
    @Override
    public ReportType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String value = p.getValueAsString();
        if (!Objects.isNull(value) && value.equalsIgnoreCase("SWPG")) {
            return ReportType.SALES_BY_CONSUMER_TYPE;
        } else {
            return ReportType.valueOf(value);
        }
    }
}
