package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.reporting.ReportType;

import java.io.IOException;

public class ReportTypeSerializer extends JsonSerializer<ReportType> {
    @Override
    public void serialize(ReportType value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        if (value.toString().equalsIgnoreCase("SWPG")) {
            value = ReportType.SALES_BY_CONSUMER_TYPE;
        }
        gen.writeObject(value);
    }
}
