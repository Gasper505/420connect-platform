package com.fourtwenty.core.domain.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.util.NumberUtils;

import java.io.IOException;
import java.math.BigDecimal;

public class TruncateSixDigitSerialzer extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        double dvalue = NumberUtils.truncateDecimal(value.doubleValue(), 6);
        gen.writeNumber(dvalue);

    }
}
