package com.fourtwenty.core.engine;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 1/25/18.
 */
public interface PromoRuleValidation {
    PromoValidationResult validate(Promotion promotion,
                                   PromotionRule criteria,
                                   Cart workingCart,
                                   Shop shop,
                                   Member member,
                                   HashMap<String, Product> productHashMap,
                                   List<OrderItem> matchedItems);
}
