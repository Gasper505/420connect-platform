package com.fourtwenty.core.engine;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;


/**
 * Created by mdo on 1/25/18.
 */
public interface PromoValidation {
    PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member);
}
