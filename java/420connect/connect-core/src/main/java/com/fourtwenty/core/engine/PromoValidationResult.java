package com.fourtwenty.core.engine;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;

/**
 * Created by mdo on 1/25/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public final class PromoValidationResult {
    public enum PromoType {
        Reward,
        Promotion
    }

    private PromoType promoType = PromoType.Promotion;
    private boolean success = false;
    private String message;
    private String id;
    private List<OrderItem> matchedOrderItems;

    public PromoValidationResult(PromoType promoType, String id, boolean success, String message) {
        this.id = id;
        this.message = message;
        this.promoType = promoType;
        this.success = success;
    }

    public PromoValidationResult(PromoType promoType, String id, boolean success, String message, List<OrderItem> matchedOrderItems) {
        this.id = id;
        this.matchedOrderItems = matchedOrderItems;
        this.message = message;
        this.promoType = promoType;
        this.success = success;
    }

    public List<OrderItem> getMatchedOrderItems() {
        return matchedOrderItems;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public PromoType getPromoType() {
        return promoType;
    }

    public boolean isSuccess() {
        return success;
    }


    public static List<OrderItem> sortOrderItems(List<OrderItem> orderItems) {
        List<OrderItem> orderItems2 = Lists.newArrayList(orderItems);
        orderItems2.sort(new Comparator<OrderItem>() {
            @Override
            public int compare(OrderItem o1, OrderItem o2) {
                if (o1.getFinalPrice().doubleValue() > 0 && o2.getFinalPrice().doubleValue() > 0) {
                    if (o1.getUnitPrice().compareTo(o1.getFinalPrice()) > 0) {
                        // unit price is greater than final price
                        return 1;
                    } else {
                        int c0 = (o2.getUnitPrice().compareTo(o1.getUnitPrice()));
                        if (c0 == 0) {
                            int c1 = (o2.getFinalPrice().compareTo(o1.getFinalPrice()));

                            if (c1 == 0) {
                                return o2.getDiscount().compareTo(o1.getDiscount());
                            }
                            return c1;
                        }

                        return c0;
                    }
                } else {
                    int c1 = (o2.getFinalPrice().compareTo(o1.getFinalPrice()));

                    if (c1 == 0) {
                        return o2.getDiscount().compareTo(o1.getDiscount());
                    }
                    return c1;
                }

            }
        });
        return orderItems2;
    }
}
