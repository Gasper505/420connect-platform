package com.fourtwenty.core.engine;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.google.inject.Inject;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 1/25/18.
 */
public class PromotionEngine {
    @Inject
    ShopRepository shopRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    PromotionRepository promotionRepository;
    @Inject
    Set<PromoValidation> promoValidations;
    @Inject
    Set<PromoRuleValidation> promoRuleValidations;

    public List<SuggestedPromotion> getSuggestedPromotions(String companyId, String shopId, Cart cart, Member member) {
        List<SuggestedPromotion> suggestedPromos = new ArrayList<>();

        List<ObjectId> objectIds = new ArrayList();
        for (OrderItem orderItem : cart.getItems()) {
            objectIds.add(new ObjectId(orderItem.getProductId()));
        }

        if (objectIds.size() == 0) {
            return suggestedPromos;
        }


        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyId, objectIds);


        Shop shop = shopRepository.get(companyId, shopId);
        Iterable<Promotion> availablePromos = promotionRepository.listByShop(companyId, shopId);
        for (Promotion promotion : availablePromos) {
            if (promotion.getDiscountAmt() != null && promotion.getDiscountAmt().doubleValue() <= 0) {
                continue;
            }
            boolean passed = true;
            for (PromoValidation promoValidation : promoValidations) {
                PromoValidationResult result = promoValidation.validate(promotion, cart, shop, member);
                if (result.isSuccess() == false) {
                    passed = false;
                    break;
                }
            }
            // Now look at the individual criteria
            if (passed) {
                List<OrderItem> orderItems = cart.getItems();
                for (PromotionRule promotionRule : promotion.getRules()) {
                    for (PromoRuleValidation promoRuleValidation : promoRuleValidations) {
                        PromoValidationResult result = promoRuleValidation.validate(promotion, promotionRule, cart, shop, member, productHashMap, orderItems);
                        orderItems = result.getMatchedOrderItems();
                        if (result.isSuccess() == false) {
                            passed = false;
                            break;
                        }
                    }
                    if (!passed) {
                        break;
                    }
                }
            }

            if (passed) {
                suggestedPromos.add(new SuggestedPromotion(promotion.getId(), promotion.getName()));
            }

        }

        return suggestedPromos;
    }

    public static boolean checkProductQuantityRule(final PromotionRule criteria, OrderItem orderItem) {


        if (criteria.getRuleType() == PromotionRule.PromotionRuleType.Product
                || criteria.getRuleType() == PromotionRule.PromotionRuleType.ProductCategory) {
            // check min and max
            double minAmt = -1;
            double maxAmt = Double.MAX_VALUE;
            if (criteria.getMinAmt() != null) {
                minAmt = criteria.getMinAmt().doubleValue();
            }
            if (criteria.getMaxAmt() != null && criteria.getMaxAmt().doubleValue() > 0) {
                maxAmt = criteria.getMaxAmt().doubleValue();
            }

            // check if quantity matches with requirement
            BigDecimal quantity = orderItem.getQuantity();

            boolean met = quantity.doubleValue() >= minAmt
                    && quantity.doubleValue() <= maxAmt;

            return met;
        }

        return true;
    }
}
