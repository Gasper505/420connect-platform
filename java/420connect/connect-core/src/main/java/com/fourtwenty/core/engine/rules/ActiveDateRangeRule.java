package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import org.joda.time.DateTime;

/**
 * Created by mdo on 1/25/18.
 */
public class ActiveDateRangeRule implements PromoValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        long now = DateTime.now().getMillis();
        boolean success = true;
        String message = "";
        if (promotion.getStartDate() != null && promotion.getStartDate() != 0) {
            if (now < promotion.getStartDate()) {
                success = false;
                message = String.format("Promo '%s' has not started.", promotion.getName());
            }
        }

        if (promotion.getEndDate() != null && promotion.getEndDate() != 0) {
            if (now > promotion.getEndDate()) {
                success = false;
                message = String.format("Promo '%s' has expired.", promotion.getName());
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
