package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;

/**
 * Created by mdo on 1/25/18.
 */
public class ActiveStateRule implements PromoValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        boolean success = true;
        String message = "";
        // Check whether promotion is enabled or not
        if (promotion.isActive() == false) {
            success = false;
            message = String.format("Promo '%s' is no longer active.", promotion.getName());
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
