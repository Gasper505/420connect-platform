package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.engine.PromoRuleValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 1/25/18.
 */
public class CartPromoFirstTimeMemberRule implements PromoRuleValidation {
    @Inject
    TransactionRepository transactionRepository;

    @Override
    public PromoValidationResult validate(Promotion promotion, PromotionRule criteria,
                                          Cart workingCart, Shop shop, Member member,
                                          HashMap<String, Product> productHashMap, List<OrderItem> matchedItems) {
        boolean success = true;
        String message = "";

        PromotionRule.PromotionRuleType type = criteria.getRuleType();
        if (type == PromotionRule.PromotionRuleType.FirstTimeMember) {
            boolean cartPassed = checkPromoFirstTimeMember(shop, member);
            if (cartPassed == false) {
                success = false;
                message = String.format("Customer is not a first time member.", promotion.getName());
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message, matchedItems);
    }

    private boolean checkPromoFirstTimeMember(Shop shop, Member member) {
        if (member == null) {
            return true;
        }
        long totalVisits = transactionRepository.countTransactionsByMember(shop.getCompanyId(), shop.getId(), member.getId());
        return totalVisits == 0;
    }

}
