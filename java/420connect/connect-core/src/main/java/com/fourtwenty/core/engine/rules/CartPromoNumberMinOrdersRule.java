package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.engine.PromoRuleValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 1/25/18.
 */
public class CartPromoNumberMinOrdersRule implements PromoRuleValidation {
    @Inject
    TransactionRepository transactionRepository;

    @Override
    public PromoValidationResult validate(Promotion promotion, PromotionRule criteria, Cart workingCart,
                                          Shop shop, Member member,
                                          HashMap<String, Product> productHashMap,
                                          List<OrderItem> matchedItems) {
        boolean success = true;
        String message = "";

        PromotionRule.PromotionRuleType type = criteria.getRuleType();
        if (type == PromotionRule.PromotionRuleType.NumberMinOrders) {
            boolean cartPassed = checkPromoMinOrdersPrevious(shop, criteria, member);
            if (cartPassed == false) {
                success = false;
                message = String.format("Customer did not meet promotion minimum previous orders for promo '%s'.", promotion.getName());
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message, matchedItems);
    }

    private boolean checkPromoMinOrdersPrevious(final Shop shop, PromotionRule promotion, Member member) {
        if (member == null) {
            return true;
        }
        long totalVisits = transactionRepository.countTransactionsByMember(shop.getCompanyId(), shop.getId(), member.getId());

        if (promotion.getMinAmt() != null) {
            long minOrders = promotion.getMinAmt().longValue();
            return minOrders <= totalVisits;
        }

        return true;

    }

}
