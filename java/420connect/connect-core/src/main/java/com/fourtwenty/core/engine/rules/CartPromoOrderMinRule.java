package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.engine.PromoRuleValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.fourtwenty.core.util.TextUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 1/25/18.
 */
public class CartPromoOrderMinRule implements PromoRuleValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, PromotionRule criteria,
                                          Cart workingCart,
                                          Shop shop,
                                          Member member,
                                          HashMap<String, Product> productHashMap, List<OrderItem> matchedItems) {
        boolean success = true;
        String message = "";

        PromotionRule.PromotionRuleType type = criteria.getRuleType();
        if (type == PromotionRule.PromotionRuleType.CartOrderMin) {
            boolean cartPassed = checkPromoCartOrderMin(criteria, matchedItems);
            if (cartPassed == false) {
                success = false;
                message = String.format("Cart subtotal did not meet promotion minimum amount of '%s' for promo '%s'.",
                        TextUtil.toCurrency(criteria.getMinAmt().doubleValue(), shop.getDefaultCountry()),
                        promotion.getName());
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message, matchedItems);
    }

    private boolean checkPromoCartOrderMin(PromotionRule criteria, List<OrderItem> matchedItems) {
        if (criteria.getMinAmt() != null) {
            double amt = criteria.getMinAmt().intValue();

            // Check cost of cart without discounts
            double cost = 0.0;
            for (OrderItem orderItem : matchedItems) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                cost += orderItem.getCost().doubleValue();
            }
            return cost >= amt;
        }
        return true;
    }
}
