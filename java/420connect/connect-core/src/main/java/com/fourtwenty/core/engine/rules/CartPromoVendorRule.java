package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.engine.PromoRuleValidation;
import com.fourtwenty.core.engine.PromoValidationResult;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by mdo on 1/25/18.
 */
public class CartPromoVendorRule implements PromoRuleValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, PromotionRule criteria,
                                          Cart workingCart, Shop shop, Member member,
                                          HashMap<String, Product> productHashMap, List<OrderItem> matchedItems) {
        boolean success = true;
        String message = "";

        PromotionRule.PromotionRuleType type = criteria.getRuleType();
        if (type == PromotionRule.PromotionRuleType.ByVendor && (promotion.getPromotionType() == Promotion.PromotionType.Cart)) {
            boolean cartPassed = checkPromoCartVendor(criteria, matchedItems, productHashMap);
            if (cartPassed == false) {
                success = false;
                message = String.format("Vendor did not match for '%s'.", promotion.getName());
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message, matchedItems);
    }

    private boolean checkPromoCartVendor(PromotionRule criteria, List<OrderItem> matchedItems,
                                         final HashMap<String, Product> productHashMap) {
        for (OrderItem orderItem : matchedItems) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }
            Product product = productHashMap.get(orderItem.getProductId());
            if (criteria.getVendorIds() != null) {
                for (String vendorId : criteria.getVendorIds()) {
                    if (vendorId.equals(product.getVendorId())) {
                        return true;
                    }
                }
            }
            if (!Objects.isNull(criteria.getVendorId()) && criteria.getVendorId().equals(product.getVendorId())) {
                return true;
            }


        }
        return false;
    }
}
