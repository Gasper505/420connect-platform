package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.dispensary.PromoUsageRepository;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.google.inject.Inject;

/**
 * Created by mdo on 1/25/18.
 */
public class CustomerUsageRule implements PromoValidation {
    @Inject
    PromoUsageRepository promoUsageRepository;

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        boolean success = true;
        String message = "";

        if (promotion.isEnableLimitPerCustomer()) {
            int limitPerUser = promotion.getLimitPerCustomer();
            if (member != null) {
                long usageCount = promoUsageRepository.countForMember(shop.getCompanyId(), shop.getId(), promotion.getId(), member.getId());
                if (usageCount >= limitPerUser) {
                    success = false;
                    message = String.format("Member has reached max allowed for promo '%s'.", promotion.getName());
                }
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
