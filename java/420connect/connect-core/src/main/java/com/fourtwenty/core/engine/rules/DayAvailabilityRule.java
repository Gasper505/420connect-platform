package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;

/**
 * Created by mdo on 1/25/18.
 */
public class DayAvailabilityRule implements PromoValidation {
    private static final Log LOG = LogFactory.getLog(DayAvailabilityRule.class);

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        // Find current day based on timezone

        boolean success = true;
        String message = "";

        if (!isAvailableToday(promotion, shop)) {
            success = false;
            message = String.format("Promo '%s' is not available today.", promotion.getName());
        }

        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }

    private boolean isAvailableToday(final Promotion promotion, final Shop shop) {
        if (shop != null) {
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            DateTime dateTime = DateTime.now().toDateTime(DateTimeZone.forID("UTC"));
            int dayOfWeek = dateTime.plusMinutes(timeZoneOffset).withTimeAtStartOfDay().getDayOfWeek();
            long millis = dateTime.plusMinutes(timeZoneOffset).withTimeAtStartOfDay().getMillis();
            LOG.info("Day of Week: " + dayOfWeek + "   millis: " + millis + "  timezoneOffset: " + timeZoneOffset + "  timezone" + timeZone);
            switch (dayOfWeek) {
                case DateTimeConstants.MONDAY:
                    return promotion.isMon();
                case DateTimeConstants.TUESDAY:
                    return promotion.isTues();
                case DateTimeConstants.WEDNESDAY:
                    return promotion.isWed();
                case DateTimeConstants.THURSDAY:
                    return promotion.isThur();
                case DateTimeConstants.FRIDAY:
                    return promotion.isFri();
                case DateTimeConstants.SATURDAY:
                    return promotion.isSat();
                case DateTimeConstants.SUNDAY:
                    return promotion.isSun();
            }
        }

        return true;
    }
}
