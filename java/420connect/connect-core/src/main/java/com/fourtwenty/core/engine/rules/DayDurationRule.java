package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mdo on 1/25/18.
 */
public class DayDurationRule implements PromoValidation {
    private static final Log LOG = LogFactory.getLog(DayDurationRule.class);

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        boolean success = true;
        String message = "";
        // Check time of day
        if (promotion.isEnableDayDuration()) {
            long millis = DateUtil.timeAtStartWithTimeZone(shop == null ? null : shop.getTimeZone());
            LOG.info("Current day millis: " + millis);
            LOG.info("Day Start Time: " + promotion.getDayStartTime());
            LOG.info("Day End Time: " + promotion.getDayEndTime());
            if (promotion.getDayStartTime() != null) {
                if (promotion.getDayStartTime() > millis) {
                    success = false;
                    message = String.format("Promo '%s' is not active at this time.", promotion.getName());
                }
            }

            if (promotion.getDayEndTime() != null) {
                if (promotion.getDayEndTime() < millis) {
                    success = false;
                    message = String.format("Promo '%s' has ended for today.", promotion.getName());
                }
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
