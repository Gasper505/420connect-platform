package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;

/**
 * Created by mdo on 1/26/18.
 */
public class DeliveryFeeRule implements PromoValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        boolean success = true;
        String message = "";
        // Check time of day
        if (promotion.getPromotionType() == Promotion.PromotionType.DeliveryFee) {
            success = false;
            message = "Shop has no delivery fee.";
            if (workingCart.isEnableDeliveryFee()) {
                success = true;
                message = "";
            }

        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
