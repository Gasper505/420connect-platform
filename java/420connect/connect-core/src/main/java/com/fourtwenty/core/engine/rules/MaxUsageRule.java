package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.dispensary.PromoUsageRepository;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.google.inject.Inject;

/**
 * Created by mdo on 1/25/18.
 */
public class MaxUsageRule implements PromoValidation {
    @Inject
    PromoUsageRepository promoUsageRepository;


    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        boolean success = true;
        String message = "";
        // Check promotion.available
        if (promotion.isEnableMaxAvailable()) {
            long maxUsed = promoUsageRepository.countForPromo(shop.getCompanyId(), shop.getId(), promotion.getId());
            if (promotion.getMaxAvailable() <= maxUsed) {
                success = false;
                message = String.format("Promo '%s' is no longer available.", promotion.getName());
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
