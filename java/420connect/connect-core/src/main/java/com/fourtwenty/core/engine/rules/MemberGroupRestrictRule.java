package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.engine.PromoValidation;
import com.fourtwenty.core.engine.PromoValidationResult;

/**
 * Created by mdo on 1/25/18.
 */
public class MemberGroupRestrictRule implements PromoValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, Cart workingCart, Shop shop, Member member) {
        boolean success = true;
        String message = "";
        if (promotion.isRestrictMemberGroups()) {
            boolean isMemberIdExistsInMemberGroup = false;
            for (String s : promotion.getMemberGroupIds()) {
                if (s != null && s.equals(member.getMemberGroupId())) {
                    isMemberIdExistsInMemberGroup = true;
                    break;
                }
            }

            if (!isMemberIdExistsInMemberGroup) {
                success = false;
                message = "Member is not in the mandatory member groups.";
            }
        }
        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message);
    }
}
