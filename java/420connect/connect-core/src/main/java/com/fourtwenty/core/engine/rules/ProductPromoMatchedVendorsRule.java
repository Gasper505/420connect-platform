package com.fourtwenty.core.engine.rules;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.engine.PromoRuleValidation;
import com.fourtwenty.core.engine.PromoValidationResult;
import com.fourtwenty.core.engine.PromotionEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 1/25/18.
 */
public class ProductPromoMatchedVendorsRule implements PromoRuleValidation {

    @Override
    public PromoValidationResult validate(Promotion promotion, PromotionRule criteria, Cart workingCart,
                                          Shop shop, Member member, HashMap<String, Product> productHashMap, List<OrderItem> matchedItems) {
        boolean success = true;
        String message = "";

        List<OrderItem> matchedOrderItems = new ArrayList<>();
        List<OrderItem> orderItems = PromoValidationResult.sortOrderItems(matchedItems);
        if (promotion.getPromotionType() == Promotion.PromotionType.Product) {

            PromotionRule.PromotionRuleType type = criteria.getRuleType();
            if (type == PromotionRule.PromotionRuleType.ByVendor) {
                for (OrderItem orderItem : orderItems) {
                    if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                        continue;
                    }

                    boolean qtyTest = PromotionEngine.checkProductQuantityRule(criteria, orderItem);
                    if (qtyTest) {
                        Product product = productHashMap.get(orderItem.getProductId());
                        if (product != null) {
                            if (criteria.getVendorIds() != null && criteria.getVendorIds().size() > 0) {
                                for (String vendorId : criteria.getVendorIds()) {
                                    if (vendorId.equals(product.getVendorId())) {
                                        // apply the product based promotions
                                        matchedOrderItems.add(orderItem);
                                    }
                                }
                            } else if (criteria.getVendorId().equals(product.getVendorId())) {
                                // apply the product based promotions
                                matchedOrderItems.add(orderItem);
                            }
                        }
                    }

                }
                if (matchedOrderItems.size() == 0) {
                    success = false;
                    message = String.format("Product not found for promo '%s'", promotion.getName());
                }
            }
        }

        return new PromoValidationResult(PromoValidationResult.PromoType.Promotion,
                promotion.getId(), success, message, matchedOrderItems);
    }

}
