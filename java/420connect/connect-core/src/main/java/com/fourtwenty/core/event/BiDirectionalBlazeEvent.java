package com.fourtwenty.core.event;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class BiDirectionalBlazeEvent<T> extends BlazeEvent<T> {
    public static final int LONG_WAIT_TIME = 180000; // 3 minutes
    public static final int SHORT_WAIT_TIME = 20000; // 20 secs
    private RuntimeException exception = null;
    private T response = null;
    private CountDownLatch latch = new CountDownLatch(1);
    private int responseTimeout = SHORT_WAIT_TIME;

    public T getResponse() {
        return getResponse(responseTimeout);
    }

    public T getResponse(long timeoutMS) {
        try {
            latch.await(timeoutMS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (exception != null) {
            throw exception;
        }
        return response;
    }

    public void throwException(RuntimeException exception) {
        this.exception = exception;
        latch.countDown();
    }

    public RuntimeException getException() {
        return exception;
    }

    public void setResponseTimeout(int responseTimeout) {
        this.responseTimeout = responseTimeout;
    }

    public void setResponse(T response) {
        this.response = response;
        latch.countDown();
    }
}
