package com.fourtwenty.core.event;

public interface BlazeEventBus {
    void register(Object subscriber);

    void unregister(Object subscriber);

    <T> void post(BiDirectionalBlazeEvent<T> event);
}
