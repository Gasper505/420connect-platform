package com.fourtwenty.core.event;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.inject.Binder;
import org.reflections.Reflections;

import java.util.Set;
import java.util.concurrent.Executors;

public class BlazeEventBusImpl implements BlazeEventBus {
    private Binder binder;
    private EventBus eventBus = new AsyncEventBus(Executors.newCachedThreadPool());

    public void register(Object subscriber) {
        eventBus.register(subscriber);
    }

    public void unregister(Object subscriber) {
        eventBus.unregister(subscriber);
    }

    public <T> void post(BiDirectionalBlazeEvent<T> event) {
        eventBus.post(event);
    }

    public void autoRegisterSubscribers(Binder binder) {
        this.binder = binder;

        Reflections reflections = new Reflections("com.fourtwenty");
        Set<Class<? extends BlazeSubscriber>> classes = reflections.getSubTypesOf(BlazeSubscriber.class);

        for (Class clazz : classes) {
            try {
                Object listener = clazz.newInstance();
                binder.requestInjection(listener);
                register(listener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
