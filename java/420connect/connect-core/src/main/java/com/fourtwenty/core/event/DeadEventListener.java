package com.fourtwenty.core.event;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

public class DeadEventListener {

    @Subscribe
    public void subscribe(DeadEvent deadEvent) {
        if (BiDirectionalBlazeEvent.class.isAssignableFrom(deadEvent.getEvent().getClass())) {
            BiDirectionalBlazeEvent biDirectionalEvent = (BiDirectionalBlazeEvent) deadEvent.getEvent();
            biDirectionalEvent.setResponseTimeout(0);
        }
    }
}
