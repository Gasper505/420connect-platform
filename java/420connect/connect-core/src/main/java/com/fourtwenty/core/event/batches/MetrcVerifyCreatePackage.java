package com.fourtwenty.core.event.batches;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.rest.dispensary.requests.inventory.DerivedBatchAddRequest;

import java.math.BigDecimal;
import java.util.List;

public class MetrcVerifyCreatePackage extends BiDirectionalBlazeEvent<List<String>> {
    private String companyId;
    private String shopId;
    private String metrcTag;
    private Product product;
    private BigDecimal quantity = BigDecimal.ZERO;
    private String actualDate; // Format: //2015-12-15
    private DerivedBatchAddRequest derivedBatchAddRequest;
    private String sku;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getMetrcTag() {
        return metrcTag;
    }

    public void setMetrcTag(String metrcTag) {
        this.metrcTag = metrcTag;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getActualDate() {
        return actualDate;
    }

    public void setActualDate(String actualDate) {
        this.actualDate = actualDate;
    }

    public DerivedBatchAddRequest getDerivedBatchAddRequest() {
        return derivedBatchAddRequest;
    }

    public void setDerivedBatchAddRequest(DerivedBatchAddRequest derivedBatchAddRequest) {
        this.derivedBatchAddRequest = derivedBatchAddRequest;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
