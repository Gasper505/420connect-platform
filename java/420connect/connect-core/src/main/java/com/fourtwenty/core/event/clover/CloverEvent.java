package com.fourtwenty.core.event.clover;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.rest.paymentcard.clover.CloverRequest;

public class CloverEvent extends BiDirectionalBlazeEvent<Object> {
    private String shopId;
    private String companyId;
    private CloverRequest cloverRequest;
    private String terminalId;
    private String employeeId;


    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public CloverRequest getCloverRequest() {
        return cloverRequest;
    }

    public void setCloverRequest(CloverRequest cloverRequest) {
        this.cloverRequest = cloverRequest;
    }
    public void setPayload(String companyId, String shopId, CloverRequest cloverRequest) {
        this.cloverRequest = cloverRequest;
        this.companyId = companyId;
        this.shopId = shopId;
    }

}
