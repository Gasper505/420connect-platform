package com.fourtwenty.core.event.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceBatchPackagePair;
import com.fourtwenty.core.domain.models.compliance.ComplianceSyncJob;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.ArrayList;
import java.util.List;

public class CreateItemComplianceEvent extends BiDirectionalBlazeEvent<CreateItemComplianceEvent> {
    private String companyId;
    private String shopId;
    private List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();
    private String errorMsg;


    private ComplianceSyncJob complianceSyncJob;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public List<ComplianceBatchPackagePair> getPackageTags() {
        return packageTags;
    }

    public void setPackageTags(List<ComplianceBatchPackagePair> packageTags) {
        this.packageTags = packageTags;
    }


    public ComplianceSyncJob getComplianceSyncJob() {
        return complianceSyncJob;
    }

    public void setComplianceSyncJob(ComplianceSyncJob complianceSyncJob) {
        this.complianceSyncJob = complianceSyncJob;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


}
