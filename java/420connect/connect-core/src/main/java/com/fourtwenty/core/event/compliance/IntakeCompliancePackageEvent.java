package com.fourtwenty.core.event.compliance;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.compliance.CompliancePackage;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class IntakeCompliancePackageEvent extends BiDirectionalBlazeEvent<SyncComplianceResult> {
    private String companyId;
    private String shopId;
    private String employeeId;
    private String productId;
    private String batchId;
    private boolean createNew = false;
    private CompliancePackage compliancePackage;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal costPerUnit = new BigDecimal(0);

    private Long sellBy;
    private Long expirationDate;

    public Long getSellBy() {
        return sellBy;
    }

    public void setSellBy(Long sellBy) {
        this.sellBy = sellBy;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public boolean isCreateNew() {
        return createNew;
    }

    public void setCreateNew(boolean createNew) {
        this.createNew = createNew;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public CompliancePackage getCompliancePackage() {
        return compliancePackage;
    }

    public void setCompliancePackage(CompliancePackage compliancePackage) {
        this.compliancePackage = compliancePackage;
    }
}
