package com.fourtwenty.core.event.compliance;

import com.blaze.clients.metrcs.models.strains.MetrcStrainRequest;

import com.fourtwenty.core.domain.models.compliance.ComplianceSyncJob;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.ArrayList;
import java.util.List;

public class StrainComplianceEvent extends BiDirectionalBlazeEvent<StrainComplianceEvent> {
    private String companyId;
    private String shopId;
    private List<MetrcStrainRequest> metrcStr = new ArrayList<>();
    private ComplianceSyncJob complianceSyncJob;
    private String message;



    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public List<MetrcStrainRequest> getMetrcStr() {
        return metrcStr;
    }

    public void setMetrcStr(List<MetrcStrainRequest> metrcStr) {
        this.metrcStr = metrcStr;
    }

    public ComplianceSyncJob getComplianceSyncJob() {
        return complianceSyncJob;
    }

    public void setComplianceSyncJob(ComplianceSyncJob complianceSyncJob) {
        this.complianceSyncJob = complianceSyncJob;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
