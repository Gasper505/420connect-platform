package com.fourtwenty.core.event.compliance;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class SyncComplianceEvent extends BiDirectionalBlazeEvent<SyncComplianceResult> {
    private String companyId;
    private String shopId;


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
