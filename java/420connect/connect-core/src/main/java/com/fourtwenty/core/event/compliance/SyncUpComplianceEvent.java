package com.fourtwenty.core.event.compliance;

import com.fourtwenty.core.domain.models.compliance.ComplianceBatchPackagePair;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.ArrayList;
import java.util.List;

public class SyncUpComplianceEvent extends BiDirectionalBlazeEvent<SyncComplianceResult> {
    private String companyId;
    private String shopId;
    private List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();

    public List<ComplianceBatchPackagePair> getPackageTags() {
        return packageTags;
    }

    public void setPackageTags(List<ComplianceBatchPackagePair> packageTags) {
        this.packageTags = packageTags;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
