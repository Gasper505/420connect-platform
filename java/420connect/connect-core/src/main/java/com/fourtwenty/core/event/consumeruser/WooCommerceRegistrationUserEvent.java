package com.fourtwenty.core.event.consumeruser;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.List;

public class WooCommerceRegistrationUserEvent extends BiDirectionalBlazeEvent<Object> {

    private String companyId;
    private List<ConsumerUser> consumerUsers;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<ConsumerUser> getConsumerUsers() {
        return consumerUsers;
    }

    public void setConsumerUsers(List<ConsumerUser> consumerUsers) {
        this.consumerUsers = consumerUsers;
    }
}
