package com.fourtwenty.core.event.inventory;

import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;

import java.math.BigDecimal;

public class ComplianceBatch {
    public String label;
    public BigDecimal quantity;
    public String measurement;
    public String productName;
    public String categoryName;
    public String packagedDate;
    public String testingState;
    public String testingStateDate;
    public String lastModifiedDate;
    public String productBatchId;
    private ProductWeightTolerance.WeightKey blazeMeasurement = ProductWeightTolerance.WeightKey.GRAM;
    private BigDecimal blazeQuantity;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPackagedDate() {
        return packagedDate;
    }

    public void setPackagedDate(String packagedDate) {
        this.packagedDate = packagedDate;
    }

    public String getTestingState() {
        return testingState;
    }

    public void setTestingState(String testingState) {
        this.testingState = testingState;
    }

    public String getTestingStateDate() {
        return testingStateDate;
    }

    public void setTestingStateDate(String testingStateDate) {
        this.testingStateDate = testingStateDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getProductBatchId() {
        return productBatchId;
    }

    public void setProductBatchId(String productBatchId) {
        this.productBatchId = productBatchId;
    }

    public ProductWeightTolerance.WeightKey getBlazeMeasurement() {
        return blazeMeasurement;
    }

    public void setBlazeMeasurement(ProductWeightTolerance.WeightKey blazeMeasurement) {
        this.blazeMeasurement = blazeMeasurement;
    }

    public BigDecimal getBlazeQuantity() {
        return blazeQuantity;
    }

    public void setBlazeQuantity(BigDecimal blazeQuantity) {
        this.blazeQuantity = blazeQuantity;
    }
}
