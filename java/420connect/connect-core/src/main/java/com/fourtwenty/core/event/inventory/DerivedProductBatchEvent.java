package com.fourtwenty.core.event.inventory;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsResult;

import java.util.HashMap;

public class DerivedProductBatchEvent extends BiDirectionalBlazeEvent<AssignProductBatchDetailsResult> {
    private String companyId;
    private String shopId;
    private HashMap<String, Double> batchQuantityMap;
    private long salesDateTime;
    private ConsumerType consumerType;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public long getSalesDateTime() {
        return salesDateTime;
    }

    public void setSalesDateTime(long salesDateTime) {
        this.salesDateTime = salesDateTime;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public HashMap<String, Double> getBatchQuantityMap() {
        return batchQuantityMap;
    }

    public void setBatchQuantityMap(HashMap<String, Double> batchQuantityMap) {
        this.batchQuantityMap = batchQuantityMap;
    }
}
