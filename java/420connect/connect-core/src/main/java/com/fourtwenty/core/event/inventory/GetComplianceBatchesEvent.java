package com.fourtwenty.core.event.inventory;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class GetComplianceBatchesEvent extends BiDirectionalBlazeEvent<GetComplianceBatchesResult> {
    private String companyId;
    private String shopId;
    private boolean getActiveBatches;
    private boolean getOnHoldBatches;
    private boolean getInactiveBatches;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public boolean isGetActiveBatches() {
        return getActiveBatches;
    }

    public void setGetActiveBatches(boolean getActiveBatches) {
        this.getActiveBatches = getActiveBatches;
    }

    public boolean isGetOnHoldBatches() {
        return getOnHoldBatches;
    }

    public void setGetOnHoldBatches(boolean getOnHoldBatches) {
        this.getOnHoldBatches = getOnHoldBatches;
    }

    public boolean isGetInactiveBatches() {
        return getInactiveBatches;
    }

    public void setGetInactiveBatches(boolean getInactiveBatches) {
        this.getInactiveBatches = getInactiveBatches;
    }
}
