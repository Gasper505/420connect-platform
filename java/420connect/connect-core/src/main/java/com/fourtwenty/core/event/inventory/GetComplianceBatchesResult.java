package com.fourtwenty.core.event.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class GetComplianceBatchesResult {

    private List<ComplianceBatch> activeBatches;
    private List<ComplianceBatch> onHoldBatches;
    private List<ComplianceBatch> inactiveBatches;

    public List<ComplianceBatch> getActiveBatches() {
        return activeBatches;
    }

    public void setActiveBatches(List<ComplianceBatch> activeBatches) {
        this.activeBatches = activeBatches;
    }

    public List<ComplianceBatch> getOnHoldBatches() {
        return onHoldBatches;
    }

    public void setOnHoldBatches(List<ComplianceBatch> onHoldBatches) {
        this.onHoldBatches = onHoldBatches;
    }

    public List<ComplianceBatch> getInactiveBatches() {
        return inactiveBatches;
    }

    public void setInactiveBatches(List<ComplianceBatch> inactiveBatches) {
        this.inactiveBatches = inactiveBatches;
    }

    @JsonIgnore
    public List<ComplianceBatch> getAllBatches() {
        List<ComplianceBatch> list = new ArrayList<>();
        list.addAll(getActiveBatches());
        list.addAll(getOnHoldBatches());
        list.addAll(getInactiveBatches());
        return list;
    }
}
