package com.fourtwenty.core.event.inventory;

import com.fourtwenty.core.domain.models.product.ReconciliationHistory;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class InventoryReconciliationEvent extends BiDirectionalBlazeEvent<InventoryReconciliationResult> {
    private ReconciliationHistory reconciliationHistory;

    public ReconciliationHistory getReconciliationHistory() {
        return reconciliationHistory;
    }

    public void setPayload(ReconciliationHistory reconciliationHistory) {
        this.reconciliationHistory = reconciliationHistory;
    }
}
