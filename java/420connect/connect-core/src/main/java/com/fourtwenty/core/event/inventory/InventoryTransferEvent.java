package com.fourtwenty.core.event.inventory;

import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsResult;

public class InventoryTransferEvent extends BiDirectionalBlazeEvent<AssignProductBatchDetailsResult> {
    private InventoryTransferHistory inventoryTransferHistory;

    public InventoryTransferHistory getInventoryTransferHistory() {
        return inventoryTransferHistory;
    }

    public void setPayload(InventoryTransferHistory inventoryTransferHistory) {
        this.inventoryTransferHistory = inventoryTransferHistory;
    }
}
