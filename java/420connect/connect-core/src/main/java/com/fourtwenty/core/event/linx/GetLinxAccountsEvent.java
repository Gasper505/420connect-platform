package com.fourtwenty.core.event.linx;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class GetLinxAccountsEvent extends BiDirectionalBlazeEvent<GetLinxAccountsResult> {
    private String companyId;

    public void setPayload(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }
}
