package com.fourtwenty.core.event.linx;

import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;

import java.util.List;

public class GetLinxAccountsResult {
    private List<LinxAccount> linxAccounts;


    public GetLinxAccountsResult(List<LinxAccount> linxAccounts) {
        this.linxAccounts = linxAccounts;
    }

    public List<LinxAccount> getLinxAccounts() {
        return linxAccounts;
    }
}
