package com.fourtwenty.core.event.linx;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class IsLinxEnabledEvent extends BiDirectionalBlazeEvent<IsLinxEnabledResult> {

    private String companyId;
    private String shopId;

    public IsLinxEnabledEvent() {
    }

    public void setPayload(String companyId, String shopId) {
        this.companyId = companyId;
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getShopId() {
        return shopId;
    }
}
