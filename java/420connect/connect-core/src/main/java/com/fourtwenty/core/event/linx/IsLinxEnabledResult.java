package com.fourtwenty.core.event.linx;

public class IsLinxEnabledResult {
    private boolean isEnabled;


    public IsLinxEnabledResult(boolean isEnabled) {

        this.isEnabled = isEnabled;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

}
