package com.fourtwenty.core.event.linx;

import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.List;

public class SaveLinxAccountsEvent extends BiDirectionalBlazeEvent<SaveLinxAccountsResult> {
    private List<LinxAccount> linxAccounts;

    public void setPayload(List<LinxAccount> linxAccounts) {
        this.linxAccounts = linxAccounts;
    }

    public List<LinxAccount> getLinxAccounts() {
        return linxAccounts;
    }
}
