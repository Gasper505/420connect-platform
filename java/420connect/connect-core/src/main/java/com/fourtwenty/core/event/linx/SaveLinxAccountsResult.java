package com.fourtwenty.core.event.linx;

import com.fourtwenty.core.domain.models.thirdparty.LinxAccount;

import java.util.List;

public class SaveLinxAccountsResult {
    private List<LinxAccount> linxAccounts;


    public SaveLinxAccountsResult(List<LinxAccount> linxAccounts) {
        this.linxAccounts = linxAccounts;
    }

    public List<LinxAccount> getLinxAccounts() {
        return linxAccounts;
    }
}
