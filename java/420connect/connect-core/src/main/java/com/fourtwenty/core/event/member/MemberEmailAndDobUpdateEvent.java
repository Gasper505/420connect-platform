package com.fourtwenty.core.event.member;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class MemberEmailAndDobUpdateEvent extends BiDirectionalBlazeEvent<Boolean> {

    private String companyId;
    private String shopId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
