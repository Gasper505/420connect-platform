package com.fourtwenty.core.event.mtrac;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class MtracRefundTransactionEvent  extends BiDirectionalBlazeEvent<Object> {
    private String shopId;
    private String companyId;
    private Transaction transaction;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal partialAmount = new BigDecimal(0);

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }


    public BigDecimal getPartialAmount() {
        return partialAmount;
    }

    public void setPartialAmount(BigDecimal partialAmount) {
        this.partialAmount = partialAmount;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void settransaction(Transaction transaction) {
        this.transaction = transaction;
    }
    public void setPayload(String companyId, String shopId, Transaction transaction, BigDecimal partialAmount) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.transaction = transaction;
        this.partialAmount = partialAmount;
    }

}
