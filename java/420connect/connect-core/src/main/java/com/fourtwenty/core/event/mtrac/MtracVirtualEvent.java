package com.fourtwenty.core.event.mtrac;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.rest.paymentcard.mtrac.MtracTransactionRequest;

public class MtracVirtualEvent extends BiDirectionalBlazeEvent<Object> {
    private String shopId;
    private String companyId;
    private MtracTransactionRequest mtracTransactionRequest;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public MtracTransactionRequest getMtracTransactionRequest() {
        return mtracTransactionRequest;
    }

    public void setMtracTransactionRequest(MtracTransactionRequest mtracTransactionRequest) {
        this.mtracTransactionRequest = mtracTransactionRequest;
    }

    public void setPayload(String companyId, String shopId, MtracTransactionRequest request) {
        this.mtracTransactionRequest = request;
        this.companyId = companyId;
        this.shopId = shopId;
    }


}
