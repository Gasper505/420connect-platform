package com.fourtwenty.core.event.orders;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class IncomingOrderEvent extends BiDirectionalBlazeEvent<IncomingOrderEvent> {
    private ConsumerUser consumerUser;
    private ConsumerCart consumerCart;
    private Shop shop;

    public IncomingOrderEvent() {
    }

    public IncomingOrderEvent(ConsumerUser consumerUser, ConsumerCart consumerCart, Shop shop) {
        this.consumerUser = consumerUser;
        this.consumerCart = consumerCart;
        this.shop = shop;
    }

    public ConsumerUser getConsumerUser() {
        return consumerUser;
    }

    public void setConsumerUser(ConsumerUser consumerUser) {
        this.consumerUser = consumerUser;
    }

    public ConsumerCart getConsumerCart() {
        return consumerCart;
    }

    public void setConsumerCart(ConsumerCart consumerCart) {
        this.consumerCart = consumerCart;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
