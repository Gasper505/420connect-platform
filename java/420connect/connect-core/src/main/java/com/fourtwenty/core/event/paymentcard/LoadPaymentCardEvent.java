package com.fourtwenty.core.event.paymentcard;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.CreditCard;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.rest.paymentcard.LoadPaymentCardRequest;
import com.fourtwenty.core.security.IAuthToken;

import java.math.BigDecimal;

public class LoadPaymentCardEvent extends BiDirectionalBlazeEvent<LoadPaymentCardResult> {
    private IAuthToken token;
    private PaymentCardType type;
    private String paymentCardNumber;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    private BigDecimal amount;
    private Member member;
    private CreditCard creditCard;

    public LoadPaymentCardEvent() {
    }

    public IAuthToken getToken() {
        return token;
    }

    public PaymentCardType getType() {
        return type;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Member getMember() {
        return member;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }


    // Helper Mapping Method
    public void setPayload(IAuthToken token, LoadPaymentCardRequest request) {
        this.token = token;

        type = request.getType();
        paymentCardNumber = request.getPaymentCardNumber();
        amount = request.getAmount();
        creditCard = request.getCreditCard();
    }


}
