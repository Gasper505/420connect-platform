package com.fourtwenty.core.event.paymentcard;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadPaymentCardResult {
    private BigDecimal loadedTotal;
    private BigDecimal feeTotal;
    private BigDecimal chargedTotal;


    public LoadPaymentCardResult(BigDecimal loadedTotal, BigDecimal feeTotal, BigDecimal chargedTotal) {
        this.loadedTotal = loadedTotal;
        this.feeTotal = feeTotal;
        this.chargedTotal = chargedTotal;
    }

    public BigDecimal getLoadedTotal() {
        return loadedTotal;
    }

    public BigDecimal getFeeTotal() {
        return feeTotal;
    }

    public BigDecimal getChargedTotal() {
        return chargedTotal;
    }
}
