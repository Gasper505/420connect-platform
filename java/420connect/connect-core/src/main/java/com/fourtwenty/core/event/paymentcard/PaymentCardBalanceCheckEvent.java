package com.fourtwenty.core.event.paymentcard;

import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.rest.paymentcard.PaymentCardBalanceCheckRequest;
import com.fourtwenty.core.security.IAuthToken;

public class PaymentCardBalanceCheckEvent extends BiDirectionalBlazeEvent<PaymentCardBalanceCheckResult> {
    private IAuthToken token;
    private PaymentCardType type;
    private String paymentCardNumber;

    public IAuthToken getToken() {
        return token;
    }

    public PaymentCardType getType() {
        return type;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }


    // Helper Mapping Method
    public void setPayload(IAuthToken token, PaymentCardBalanceCheckRequest request) {
        this.token = token;

        type = request.getType();
        paymentCardNumber = request.getPaymentCardNumber();
    }
}
