package com.fourtwenty.core.event.paymentcard;


import java.math.BigDecimal;

public class PaymentCardBalanceCheckResult {
    private String status;
    private BigDecimal balance;

    public PaymentCardBalanceCheckResult(String status, BigDecimal balance) {
        this.status = status;
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
