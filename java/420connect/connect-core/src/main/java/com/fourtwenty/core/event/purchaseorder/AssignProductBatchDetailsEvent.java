package com.fourtwenty.core.event.purchaseorder;

import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.purchaseorder.TrackingPackages;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class AssignProductBatchDetailsEvent extends BiDirectionalBlazeEvent<AssignProductBatchDetailsResult> {

    private TrackingPackages trackingPackages;
    private ProductBatch productBatch;

    public void setPayload(TrackingPackages trackingPackages, ProductBatch productBatch) {
        this.trackingPackages = trackingPackages;
        this.productBatch = productBatch;
    }

    public TrackingPackages getTrackingPackages() {
        return trackingPackages;
    }

    public ProductBatch getProductBatch() {
        return productBatch;
    }
}
