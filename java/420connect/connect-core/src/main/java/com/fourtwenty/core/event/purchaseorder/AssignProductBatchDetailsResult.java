package com.fourtwenty.core.event.purchaseorder;

import java.math.BigDecimal;

public class AssignProductBatchDetailsResult {
    private BigDecimal quantity; // In Grams

    public AssignProductBatchDetailsResult(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }
}
