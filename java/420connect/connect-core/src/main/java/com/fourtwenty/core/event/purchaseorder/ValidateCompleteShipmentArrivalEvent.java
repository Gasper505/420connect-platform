package com.fourtwenty.core.event.purchaseorder;

import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.TrackingPackages;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.List;

public class ValidateCompleteShipmentArrivalEvent extends BiDirectionalBlazeEvent<ValidateCompleteShipmentArrivalResult> {

    private List<TrackingPackages> trackingPackagesList;
    private POProductRequest poProductRequest;
    private PurchaseOrder purchaseOrder;

    public void setPayload(POProductRequest poProductRequest) {
        this.poProductRequest = poProductRequest;
    }

    public POProductRequest getPoProductRequest() {
        return poProductRequest;
    }
}
