package com.fourtwenty.core.event.report;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesResult;
import com.fourtwenty.core.reporting.ReportType;

public class EmailReportEvent extends BiDirectionalBlazeEvent<GetComplianceBatchesResult> {

    private String email;
    private String shopId;
    private String companyId;
    private ReportType reportingInfoType;
    private Long startDate;
    private Long endDate;
    private int timezoneOffset;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public ReportType getReportingInfoType() {
        return reportingInfoType;
    }

    public void setReportingInfoType(ReportType reportingInfoType) {
        this.reportingInfoType = reportingInfoType;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public int getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(int timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }
}
