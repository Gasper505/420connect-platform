package com.fourtwenty.core.event.report;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsResult;

public class PreCalculateReportEvent extends BiDirectionalBlazeEvent<AssignProductBatchDetailsResult> {

    private long triggerTime;

    public long getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(long triggerTime) {
        this.triggerTime = triggerTime;
    }
}
