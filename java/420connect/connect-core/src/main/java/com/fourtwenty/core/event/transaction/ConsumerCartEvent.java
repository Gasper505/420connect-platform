package com.fourtwenty.core.event.transaction;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class ConsumerCartEvent extends BiDirectionalBlazeEvent {
    private Transaction transaction;
    private String shopId;
    private String companyId;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
