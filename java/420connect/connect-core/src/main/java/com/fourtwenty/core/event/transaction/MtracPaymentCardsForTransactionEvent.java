package com.fourtwenty.core.event.transaction;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class MtracPaymentCardsForTransactionEvent extends BiDirectionalBlazeEvent<ProcessLoyaltyCardsForTransactionResult> {
    private Transaction dbTransaction;
    private Transaction requestTransaction;

    public MtracPaymentCardsForTransactionEvent() {
    }

    public void setPayload(Transaction dbTransaction, Transaction requestTransaction) {
        this.dbTransaction = dbTransaction;
        this.requestTransaction = requestTransaction;
    }


    public Transaction getDbTransaction() {
        return dbTransaction;
    }

    public Transaction getRequestTransaction() {
        return requestTransaction;
    }
}
