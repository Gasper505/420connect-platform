package com.fourtwenty.core.event.transaction;

import com.fourtwenty.core.domain.models.transaction.Transaction;

public class ProcessLoyaltyCardsForTransactionResult {
    private Transaction transaction;

    public ProcessLoyaltyCardsForTransactionResult(Transaction transaction) {
        this.transaction = transaction;
    }

    public Transaction getTransaction() {
        return transaction;
    }
}
