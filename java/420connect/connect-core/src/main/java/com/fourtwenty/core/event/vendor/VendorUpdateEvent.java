package com.fourtwenty.core.event.vendor;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

public class VendorUpdateEvent extends BiDirectionalBlazeEvent<Boolean> {
    private String vendorId;
    private Vendor.VendorType dbVendorType;
    private Vendor.VendorType reqVendorType;
    private String companyId;

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public Vendor.VendorType getDbVendorType() {
        return dbVendorType;
    }

    public void setDbVendorType(Vendor.VendorType dbVendorType) {
        this.dbVendorType = dbVendorType;
    }

    public Vendor.VendorType getReqVendorType() {
        return reqVendorType;
    }

    public void setReqVendorType(Vendor.VendorType reqVendorType) {
        this.reqVendorType = reqVendorType;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
