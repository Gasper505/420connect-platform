package com.fourtwenty.core.event.weedmap;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapCategoryMapItem;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.List;

public class WeedMapDeleteEvent extends BiDirectionalBlazeEvent {
    private String companyId;
    private String shopId;
    private List<WeedmapCategoryMapItem> categoryMappings;
    private List<WmProductMapping> tagsMappings;
    private List<WeedmapApiKeyMap> apiKeyMaps;
    private List<Product> products;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public List<WeedmapCategoryMapItem> getCategoryMappings() {
        return categoryMappings;
    }

    public void setCategoryMappings(List<WeedmapCategoryMapItem> categoryMappings) {
        this.categoryMappings = categoryMappings;
    }

    public List<WeedmapApiKeyMap> getApiKeyMaps() {
        return apiKeyMaps;
    }

    public void setApiKeyMaps(List<WeedmapApiKeyMap> apiKeyMaps) {
        this.apiKeyMaps = apiKeyMaps;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<WmProductMapping> getTagsMappings() {
        return tagsMappings;
    }

    public void setTagsMappings(List<WmProductMapping> tagsMappings) {
        this.tagsMappings = tagsMappings;
    }
}
