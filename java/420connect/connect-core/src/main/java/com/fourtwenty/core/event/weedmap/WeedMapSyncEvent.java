package com.fourtwenty.core.event.weedmap;

import com.fourtwenty.core.event.BiDirectionalBlazeEvent;

import java.util.HashSet;
import java.util.Set;

public class WeedMapSyncEvent extends BiDirectionalBlazeEvent<WeedmapSyncResponse> {
    private String companyId;
    private String shopId;
    private Set<String> productIds = new HashSet<>();

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public Set<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(Set<String> productIds) {
        this.productIds = productIds;
    }
}
