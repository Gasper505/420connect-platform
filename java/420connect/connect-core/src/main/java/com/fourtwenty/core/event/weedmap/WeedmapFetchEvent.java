package com.fourtwenty.core.event.weedmap;

import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.event.BiDirectionalBlazeEvent;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;

public class WeedmapFetchEvent extends BiDirectionalBlazeEvent {
    private String companyId;
    private String shopId;
    private WeedmapAccount account;
    private WeedmapTokenType type;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public WeedmapAccount getAccount() {
        return account;
    }

    public void setAccount(WeedmapAccount account) {
        this.account = account;
    }

    public WeedmapTokenType getType() {
        return type;
    }

    public void setType(WeedmapTokenType type) {
        this.type = type;
    }
}
