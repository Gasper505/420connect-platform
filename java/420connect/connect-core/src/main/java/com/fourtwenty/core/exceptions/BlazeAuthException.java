package com.fourtwenty.core.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 8/28/15.
 */
public class BlazeAuthException extends BlazeException {
    public BlazeAuthException(String field, String message) {
        super(Response.Status.UNAUTHORIZED, field, message, BlazeAuthException.class);
    }

}
