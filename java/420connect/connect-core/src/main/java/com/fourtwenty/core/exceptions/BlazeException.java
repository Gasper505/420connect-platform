package com.fourtwenty.core.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 8/28/15.
 */
public class BlazeException extends RuntimeException {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private final String field;
    private final Class<?> clazz;
    private final Response.Status status;

    public BlazeException(Response.Status status, String field, String message, Class<?> clazz) {
        super(message);
        this.status = status;
        this.field = field;
        this.clazz = clazz;
    }

    public String getField() {
        return field;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public Response.Status getStatus() {
        return status;
    }
}

