package com.fourtwenty.core.exceptions;

import com.fourtwenty.core.util.JsonSerializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by mdo on 10/2/15.
 */
public class BlazeInvalidArgException extends BlazeException implements ExceptionMapper<BlazeInvalidArgException> {
    private static final Log LOG = LogFactory.getLog(BlazeInvalidArgException.class);

    public BlazeInvalidArgException(String field, String message) {
        super(Response.Status.BAD_REQUEST, field, message, BlazeInvalidArgException.class);
    }

    @Override
    public Response toResponse(BlazeInvalidArgException e) {

        String s = JsonSerializer.toJson(e);
        LOG.error(e.getMessage(), e);
        return Response.status(Response.Status.BAD_REQUEST).entity(s).header("Content-Type", "application/json").build();
    }
}
