package com.fourtwenty.core.exceptions;

public class BlazeOperationException extends BlazeInvalidArgException {
    public BlazeOperationException() {
        super("Operation", "Unknown Err");
    }

    public BlazeOperationException(String message) {
        super("Operation", message);
    }

    public BlazeOperationException(String message, Throwable cause) {
        super("Operation", message);
    }

    public BlazeOperationException(Throwable cause) {
        super("Operation", cause.getMessage());
    }

    public BlazeOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        //super ( message, cause, enableSuppression, writableStackTrace );

        super("Operation", message);
    }
}
