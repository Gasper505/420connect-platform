package com.fourtwenty.core.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 10/2/15.
 */
public class BlazeQuickPinExistException extends BlazeException {
    public BlazeQuickPinExistException(String field, String message) {
        super(Response.Status.BAD_REQUEST, field, message, BlazeQuickPinExistException.class);
    }
}
