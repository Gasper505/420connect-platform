package com.fourtwenty.core.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 2/2/17.
 */
public class DeveloperAuthException extends BlazeException {
    public DeveloperAuthException(String field, String message) {
        super(Response.Status.UNAUTHORIZED, field, message, DeveloperAuthException.class);
    }

}
