package com.fourtwenty.core.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 5/10/17.
 */
public class VersionErrorException extends BlazeException {

    public VersionErrorException(String field, String message) {
        super(Response.Status.BAD_REQUEST, field, message, VersionErrorException.class);
    }
}
