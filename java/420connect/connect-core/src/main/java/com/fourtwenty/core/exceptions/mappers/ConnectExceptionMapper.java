package com.fourtwenty.core.exceptions.mappers;

import com.fourtwenty.core.exceptions.BlazeError;
import com.fourtwenty.core.exceptions.BlazeException;
import com.fourtwenty.core.util.JsonSerializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by mdo on 8/16/16.
 */
@Provider
public class ConnectExceptionMapper implements ExceptionMapper<BlazeException> {
    private static final Log LOG = LogFactory.getLog(ConnectExceptionMapper.class);

    @Override
    public Response toResponse(BlazeException e) {

        BlazeError blazeError = new BlazeError(e.getField(), e.getMessage(), e.getClass().getCanonicalName());
        String s = JsonSerializer.toJson(blazeError);
        LOG.error(e.getMessage(), e);

        return Response.status(e.getStatus()).entity(s).header("Content-Type", "application/json").build();
    }
}
