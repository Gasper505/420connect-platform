package com.fourtwenty.core.exceptions.mappers;

import com.fourtwenty.core.exceptions.BlazeError;
import com.fourtwenty.core.util.JsonSerializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by mdo on 8/16/16.
 */
@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {
    private static final Log LOG = LogFactory.getLog(RuntimeExceptionMapper.class);

    @Override
    public Response toResponse(RuntimeException e) {
        System.out.println("error");
        StringBuilder msg = new StringBuilder(e.getMessage());

        BlazeError blazeError = new BlazeError("ERROR Parsing JSON", msg.toString(), e.getClass().getCanonicalName());

        String s = JsonSerializer.toJson(blazeError);
        LOG.error(e.getMessage(), e);
        System.out.println(e.getStackTrace());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(s).header("Content-Type", "application/json").build();

    }
}
