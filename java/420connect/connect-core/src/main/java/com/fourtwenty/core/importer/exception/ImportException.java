package com.fourtwenty.core.importer.exception;

import com.fourtwenty.core.exceptions.BlazeException;

import javax.ws.rs.core.Response;

/**
 * Created by Stephen Schmidt on 1/10/2016.
 */
public class ImportException extends BlazeException {

    public ImportException(String message) {
        super(Response.Status.BAD_REQUEST, "Import", message, ImportException.class);
    }
}
