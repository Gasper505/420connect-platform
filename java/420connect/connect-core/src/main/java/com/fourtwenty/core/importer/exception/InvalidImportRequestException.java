package com.fourtwenty.core.importer.exception;

/**
 * Created by Stephen Schmidt on 1/10/2016.
 */
public class InvalidImportRequestException extends ImportException {
    public InvalidImportRequestException(String message) {
        super(message);
    }
}
