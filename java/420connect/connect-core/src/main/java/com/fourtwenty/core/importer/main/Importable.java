package com.fourtwenty.core.importer.main;

/**
 * Created by Stephen Schmidt on 2/6/2016.
 */
public interface Importable {
    enum ImportSource {
        Meadow,
        Custom
    }
    String getImportId();
}
