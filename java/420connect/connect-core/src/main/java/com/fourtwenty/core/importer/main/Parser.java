package com.fourtwenty.core.importer.main;

/**
 * Created by Stephen Schmidt on 1/6/2016.
 */

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.importer.exception.ImportException;
import com.fourtwenty.core.importer.main.parsers.IDataParser;
import com.fourtwenty.core.importer.model.ParseResult;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Parser {
    private static final Log LOG = LogFactory.getLog(Parser.class);
    public enum ParseDataType {
        Vendor("Vendor Data"),
        Doctor("Doctor Data"),
        Product("Product Data"),
        Customer("BaseMember Data"),
        ProductBatch("ProductBatch Data"),
        ConsumerUser("ConsumerUser Data"),
        Promotion("Promotion Data");

        ParseDataType(String title) {
            this.title = title;
        }

        private String title;

        public String getTitle() {
            return title;
        }
    }


    public ParseResult parse(InputStream stream, IDataParser dataParser, Shop shop) {
        return parseEntitiesFromFile(stream, dataParser, shop);
    }

    private ParseResult parseEntitiesFromFile(InputStream inputStream, IDataParser dataParser, Shop shop) throws ImportException {
        CSVParser parser = createCSVParser(inputStream);

        ParseResult result = dataParser.parse(parser, shop);
        return result;
    }

    public CSVParser createCSVParser(InputStream inputStream) throws ImportException {
        CSVParser parser = null;
        try {
            Reader in = new InputStreamReader(inputStream);
            CSVFormat format = CSVFormat.EXCEL.withHeader().withSkipHeaderRecord(true);
            parser = new CSVParser(in, format);
        } catch (IOException e) {
            LOG.error(e);
            throw new BlazeInvalidArgException("Parser", "Could not create CSV Parser.");
        }
        return parser;
    }
}