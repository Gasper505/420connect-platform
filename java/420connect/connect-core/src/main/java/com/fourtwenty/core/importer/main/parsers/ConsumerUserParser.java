package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class ConsumerUserParser implements IDataParser<ConsumerUser> {

    private final StoreAuthToken token;
    public ConsumerUserParser(StoreAuthToken token) {
      this.token = token;
    }


    @Override
    public ParseResult<ConsumerUser> parse(CSVParser csvParser, Shop shop) {

        ParseResult<ConsumerUser> result = new ParseResult<ConsumerUser>(Parser.ParseDataType.ConsumerUser);

        for (CSVRecord csvRecord : csvParser) {
            ConsumerUser consumerUser = new ConsumerUser();
            consumerUser.setSourceCompanyId(token.getCompanyId());
            consumerUser.setEmail(csvRecord.get("Email"));
            consumerUser.setFirstName(csvRecord.get("FirstName"));
            consumerUser.setLastName(csvRecord.get("LastName"));
            consumerUser.setPrimaryPhone(csvRecord.get("PhoneNo"));
            result.addSuccess(consumerUser);
        }
        return result;
    }
}
