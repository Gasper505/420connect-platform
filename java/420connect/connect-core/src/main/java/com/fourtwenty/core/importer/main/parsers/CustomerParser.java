package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.importer.exception.ImportException;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.CustomerContainer;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.importer.util.ImporterUtil;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.services.mgmt.DoctorService;
import com.fourtwenty.core.services.mgmt.MemberService;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 3/24/16.
 */
public class CustomerParser implements IDataParser<CustomerContainer> {
    private final MemberService memberService;
    private final DoctorService doctorService;

    public CustomerParser(MemberService memberService, DoctorService doctorService) {
        this.memberService = memberService;
        this.doctorService = doctorService;
    }

    @Override
    public ParseResult<CustomerContainer> parse(CSVParser csvParser, Shop shop) {
        List<Doctor> doctors = doctorService.getAllDoctors();
        HashMap<String, Doctor> doctorMap = new HashMap<>();
        for (Doctor d : doctors) {
            if (d.getImportId() != null && !d.getImportId().isEmpty()) {
                doctorMap.put(d.getImportId(), d);
            }
        }

        ParseResult<CustomerContainer> result = new ParseResult<CustomerContainer>(Parser.ParseDataType.Customer);
        for (CSVRecord r : csvParser) {
            CustomerContainer entity = buildCustomerRequest(r, doctorMap, shop);

            if (entity != null) {
                result.addSuccess(entity);
            }
        }
        return result;
    }


    private CustomerContainer buildCustomerRequest(CSVRecord r, HashMap<String, Doctor> doctors, Shop shop) throws ImportException {
        MembershipAddRequest request = new MembershipAddRequest();
        request.setFirstName(r.get("First Name"));
        request.setLastName(r.get("Last Name"));
        request.setMiddleName(r.get("Middle Name"));

        Address a = new Address();
        a.setId(ObjectId.get().toString());
        a.setAddress(r.get("Street Address"));
        a.setCity(r.get("City"));
        a.setState(r.get("State"));
        a.setZipCode(r.get("Zip Code"));
        request.setAddress(a);

        Identification id = new Identification();
        id.setType(ImporterUtil.getIdType(r.get("Identification Type")));
        id.setLicenseNumber(r.get("Identification Number"));
        id.setState(r.get("Identification State"));

        Recommendation rec = new Recommendation();
        rec.setRecommendationNumber(r.get("Recommendation Number"));
        rec.setVerifyWebsite(r.get("Verification Website"));
        rec.setVerifyPhoneNumber(r.get("Verification Phone Number"));

        request.setPrimaryPhone(r.get("Primary Phone"));

        String cpn = request.getPrimaryPhone();
        if (StringUtils.isNotBlank(cpn)) {
            // remove first character


            cpn = AmazonServiceManager.cleanPhoneNumber(cpn,shop);
            if (cpn != null && (cpn.startsWith("+1"))) {
                String fixCPN = cpn.substring(2);
                request.setPrimaryPhone(fixCPN);
            }
        }

        request.setEmail(r.get("Email Address"));
        request.setMedical(ImporterUtil.parseBoolean(r.get("Medical?")));
        request.setDoctorImportId(r.get("Doctor ID"));

        Doctor doctor = doctors.get(request.getDoctorImportId());
        if (doctor != null) {
            rec.setDoctorId(doctor.getId());

        }
        try {
            request.setDob(ImporterUtil.getDate(r.get("Date of Birth"), shop.getTimeZone()));
        } catch (Exception e) {
        }

        try {
            request.setStartDate(ImporterUtil.getDate(r.get("Date Joined"), shop.getTimeZone()));
        } catch (Exception e) {

        }
        try {
            request.setMarketingSource(r.get("Marketing Source"));
        } catch (Exception e) {
            // ignore
        }

        // Enable by default
        request.setEnableLoyalty(true);
        try {
            request.setEnableLoyalty(ImporterUtil.parseBoolean(r.get("Enable Loyalty")));
        } catch (Exception e) {
            // ignore
        }
        try {
            String points = r.get("Loyalty Points");
            double pts = Double.parseDouble(points);
            request.setLoyaltyPoints(new BigDecimal(pts));
        } catch (Exception e) {
            // ignore
        }

        try {
            request.setTextOptIn(ImporterUtil.parseBoolean(r.get("Text Opt-In")));
        } catch (Exception e) {
        }

        try {
            request.setEmailOptIn(ImporterUtil.parseBoolean(r.get("Email Opt-In")));
        } catch (Exception e) {
        }

        // ID
        ArrayList<Identification> ids = new ArrayList<>(1);
        try {
            id.setExpirationDate(ImporterUtil.getDate(r.get("Expiration Date"), shop.getTimeZone()));
            request.setImportId((request.getFirstName() + request.getLastName() + request.getDob()));
            ids.add(id);
        } catch (IllegalArgumentException e) {
            System.out.print(e.getLocalizedMessage());
            System.out.println("Invalid or missing date in baseMember data - ID: " + request.getImportId());
        }
        request.setIdentifications(ids);

        // Rec
        ArrayList<Recommendation> recs = new ArrayList<>(1);
        try {
            rec.setExpirationDate(ImporterUtil.getDate(r.get("Recommendation Expiration Date"), shop.getTimeZone()));
            rec.setIssueDate(ImporterUtil.getDate(r.get("Recommendation Issue Date"), shop.getTimeZone()));
            rec.setState(r.get("Recommendation State"));
            ;
            //recommendation and identification successfully parsed, set them and move right along
            request.setImportId((request.getFirstName() + request.getLastName() + request.getDob()));
            recs.add(rec);
        } catch (IllegalArgumentException e) {
            System.out.print(e.getLocalizedMessage());
            System.out.println("Invalid or missing date in baseMember data - ID: " + request.getImportId());
        }
        String patientGroup = null;
        try {
            patientGroup = r.get("Member Group");
        } catch (Exception e) {
        }

        String memberNote = null;
        try {
            memberNote = r.get("Notes");
        } catch (Exception e) {
            // Ignore
        }

        try {
            String gender = r.get("Gender");
            BaseMember.Gender sex = BaseMember.Gender.OTHER;
            if (StringUtils.isNotBlank(gender)) {
                if (gender.equalsIgnoreCase("M")) {
                    sex = BaseMember.Gender.MALE;
                } else if (gender.equalsIgnoreCase("F")) {
                    sex = BaseMember.Gender.FEMALE;
                }
            }
            request.setSex(sex);
        } catch (Exception e) {

        }

        request.setRecommendations(recs);

        CustomerContainer container = new CustomerContainer();
        Member member = memberService.sanitize(request, doctors);
        String importId = member.getFirstName() + "_" + TextUtil.textOrEmpty(member.getMiddleName()) + "_" + member.getLastName() + "_" + member.getDob() + "_" + cpn;

        if (id != null && StringUtils.isNotBlank(id.getLicenseNumber())) {
            importId = id.getState() + "_" + id.getLicenseNumber().trim();
        }

        member.setImportId(importId.trim().toLowerCase());
        ConsumerType consumerType = ConsumerType.AdultUse;

        try {
            String consumerTypeStr = r.get("Consumer Type");
            if (consumerTypeStr.equalsIgnoreCase("Other")) {
                consumerType = ConsumerType.Other;
            } else if (consumerTypeStr.equalsIgnoreCase("Medicinal - Third Party")) {
                consumerType = ConsumerType.MedicinalThirdParty;
            } else if (consumerTypeStr.equalsIgnoreCase("Medicinal - MMIC")) {
                consumerType = ConsumerType.MedicinalState;
            }
        } catch (Exception e) {
            if (member.isMedical()) {
                consumerType = ConsumerType.MedicinalThirdParty;
            }
        }
        member.setConsumerType(consumerType);
        container.setPatientGroup(patientGroup);
        container.setMemberNote(memberNote);
        container.setMember(member);
        container.setDateJoined(request.getStartDate());
        return container;
    }
}

