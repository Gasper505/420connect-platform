package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.rest.dispensary.requests.customer.DoctorAddRequest;
import com.fourtwenty.core.services.mgmt.DoctorService;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.bson.types.ObjectId;

/**
 * Created by mdo on 3/24/16.
 */
public class DoctorParser implements IDataParser<Doctor> {

    private final DoctorService doctorService;

    public DoctorParser(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Override
    public ParseResult<Doctor> parse(CSVParser csvParser, Shop shop) {
        ParseResult<Doctor> result = new ParseResult<Doctor>(Parser.ParseDataType.Doctor);
        for (CSVRecord r : csvParser) {
            Doctor entity = buildDoctorRequest(r);
            if (entity != null) {
                result.addSuccess(entity);
            }
        }
        return result;

    }


    private Doctor buildDoctorRequest(CSVRecord r) {
        DoctorAddRequest request = new DoctorAddRequest();
        request.setImportId(r.get("Doctor ID"));
        request.setFirstName(r.get("First Name"));
        request.setLastName(r.get("Last Name"));
        request.setDegree(r.get("Degree"));
        request.setLicense(r.get("License Number"));
        Address a = new Address();
        a.setId(ObjectId.get().toString());
        a.setAddress(r.get("Address"));
        a.setCity(r.get("City"));
        a.setState(r.get("State"));
        a.setZipCode(r.get("Zip Code"));
        request.setAddress(a);
        request.setPhoneNumber(r.get("Phone"));
        request.setEmail(r.get("Email"));
        request.setFax(r.get("Fax"));
        request.setWebsite("Website");

        return doctorService.sanitize(request);
    }
}
