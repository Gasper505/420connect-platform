package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.importer.model.ParseResult;
import org.apache.commons.csv.CSVParser;

/**
 * Created by mdo on 3/24/16.
 */
public interface IDataParser<T extends Importable> {
    ParseResult<T> parse(CSVParser csvParser, Shop shop);
}
