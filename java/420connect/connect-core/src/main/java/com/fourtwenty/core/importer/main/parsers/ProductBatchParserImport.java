package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.ProductBatchQueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.importer.util.ImporterUtil;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import com.google.common.collect.Lists;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ProductBatchParserImport implements IDataParser<ProductBatchResult> {
    private static final Log LOG = LogFactory.getLog(ProductBatchParserImport.class);

    private final ProductService productService;
    private final InventoryRepository inventoryRepository;
    private final ProductBatchRepository productBatchRepository;
    private final InventoryService inventoryService;
    private final BatchQuantityService batchQuantityService;
    private final ConnectAuthToken token;
    private final ProductChangeLogRepository productChangeLogRepository;
    private final ProductRepository productRepository;
    private final RealtimeService realtimeService;
    private BarcodeService barcodeService;
    private MetrcAccountService metrcAccountService;
    private final ProductCategoryRepository productCategoryRepository;
    private final QueuedTransactionRepository queuedTransactionRepository;
    private final BackgroundJobService backgroundJobService;
    private final ShopRepository shopRepository;
    private BatchActivityLogRepository batchActivityLogRepository;
    private BarcodeItemRepository barcodeItemRepository;

    public ProductBatchParserImport(ProductService productService,
                                    InventoryRepository inventoryRepository,
                                    ProductBatchRepository productBatchRepository,
                                    InventoryService inventoryService,
                                    BatchQuantityService batchQuantityService,
                                    ConnectAuthToken token,
                                    ProductChangeLogRepository
                                            productChangeLogRepository,
                                    ProductRepository productRepository,
                                    RealtimeService realtimeService,
                                    BarcodeService barcodeService,
                                    MetrcAccountService metrcAccountService,
                                    ProductCategoryRepository productCategoryRepository,
                                    BackgroundJobService backgroundJobService,
                                    QueuedTransactionRepository queuedTransactionRepository,
                                    ShopRepository shopRepository,
                                    BatchActivityLogRepository batchActivityLogRepository, BarcodeItemRepository barcodeItemRepository) {
        this.productService = productService;
        this.productBatchRepository = productBatchRepository;
        this.inventoryRepository = inventoryRepository;
        this.inventoryService = inventoryService;
        this.batchQuantityService = batchQuantityService;
        this.token = token;
        this.productChangeLogRepository = productChangeLogRepository;
        this.productRepository = productRepository;
        this.realtimeService = realtimeService;
        this.barcodeService = barcodeService;
        this.metrcAccountService = metrcAccountService;
        this.productCategoryRepository = productCategoryRepository;
        this.queuedTransactionRepository = queuedTransactionRepository;
        this.backgroundJobService = backgroundJobService;
        this.shopRepository = shopRepository;
        this.batchActivityLogRepository = batchActivityLogRepository;
        this.barcodeItemRepository = barcodeItemRepository;
    }

    @Override
    public ParseResult<ProductBatchResult> parse(CSVParser csvParser, Shop shop) {

        ParseResult<ProductBatchResult> result = new ParseResult<>(Parser.ParseDataType.ProductBatch);
        result.setDataType(Parser.ParseDataType.ProductBatch);

        Map<String, Integer> headerMap = csvParser.getHeaderMap();
        List<String> inventoryList = new ArrayList<>();
        ArrayList<ObjectId> objectIds = new ArrayList<>();

        //List inventory header
        for (String header : headerMap.keySet()) {
            if (header.startsWith("Inv: ")) {
                inventoryList.add(header);
            }
        }

        //List product's id
        List<CSVRecord> csvRecordList = new ArrayList<>();
        for (CSVRecord csvRecord : csvParser) {
            String productId = csvRecord.isSet("Product Id") ? csvRecord.get("Product Id") : "";
            if (StringUtils.isNotBlank(productId)) {
                try {
                    objectIds.add(new ObjectId(productId));
                } catch (Exception e) {
                    throw new BlazeInvalidArgException("Product id", "Product id is not correct");
                }
            }
            csvRecordList.add(csvRecord);
        }

        Map<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), objectIds);
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

        Inventory quarantineInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.QUARANTINE);

        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        ProductBatchResult batchResult;
        Product product;
        HashMap<String, BigDecimal> inventoryQuantity;
        //Process product list
        for (CSVRecord csvRecord : csvRecordList) {
            boolean success = true;
            String productId = csvRecord.isSet("Product Id") ? csvRecord.get("Product Id") : "";

            double costPerUnit = getDoubleValue(csvRecord, "Cost Per Unit");
            String batchDate = csvRecord.isSet("Batch Date") ? csvRecord.get("Batch Date") : "";
            String productName = csvRecord.isSet("Product Name") ? csvRecord.get("Product Name") : "";
            String batchId = csvRecord.isSet("Batch ID") ? csvRecord.get("Batch ID") : "";
            String metrcPackageLabel = csvRecord.isSet("Metrc Package Label") ? csvRecord.get("Metrc Package Label") : "";


            batchResult = new ProductBatchResult();
            batchResult.setProductName(productName);
            batchResult.setProductId(productId);
            String batchSKU = csvRecord.isSet("Batch SKU") ? csvRecord.get("Batch SKU") : " ";

            batchResult.setBatchSKU(batchSKU);
            batchResult.setThc(getDoubleValue(csvRecord, "THC"));
            batchResult.setCbd(getDoubleValue(csvRecord, "CBD"));
            batchResult.setCbn(getDoubleValue(csvRecord, "CBN"));
            batchResult.setThca(getDoubleValue(csvRecord, "THCA"));
            batchResult.setCbda(getDoubleValue(csvRecord, "CBDA"));
            batchResult.setTrackPackageLabel(metrcPackageLabel);

            batchResult.setTrackHarvestBatch(batchId);

            if (batchDate.isEmpty()) {
                batchResult.setBatchDate(DateTime.now().getMillis());
            } else {
                try {
                    batchResult.setBatchDate(ImporterUtil.getDate(batchDate, shop.getTimeZone()));
                } catch (Exception e) {
                    success = false;
                    result.addFailed(batchResult, "Invalid batch date format (\'MM/dd/yyyy\')");
                }
            }

            inventoryQuantity = new HashMap<>();

            BigDecimal quantity = new BigDecimal(0);
            for (String inventoryHeader : inventoryList) {
                String inventoryName = inventoryHeader.replace("Inv: ", "");
                String value = csvRecord.isSet(inventoryHeader) ? csvRecord.get(inventoryHeader) : "";
                if (StringUtils.isNotBlank(value)) {
                    value = value.trim();
                    try {
                        if (Double.parseDouble(value) > 0) {
                            String inventoryId = getInventoryIdByName(inventoryMap, inventoryName);

                            if (shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) {
                                if (safeInventory == null && Inventory.SAFE.equalsIgnoreCase(inventoryMap.get(inventoryId).getName())) {
                                    safeInventory = inventoryMap.get(inventoryId);
                                }
                                if (quarantineInventory != null && quarantineInventory.getId().equalsIgnoreCase(inventoryId) && safeInventory != null) {
                                    inventoryId = safeInventory.getId();
                                }

                            }
                            double vValue = ImporterUtil.getDollarAmountDecimal(value).doubleValue();
                            quantity = BigDecimal.valueOf(quantity.doubleValue() + vValue);

                            inventoryQuantity.put(inventoryId, inventoryQuantity.getOrDefault(inventoryId, BigDecimal.ZERO).add(new BigDecimal(vValue)));
                        }
                    } catch (Exception e) {
                        result.addFailed(batchResult, "Invalid number of quantity for inventory " + inventoryName + " value: " + value);
                    }
                }
            }

            batchResult.setInventoryQuantity(inventoryQuantity);
            batchResult.setQuantity(quantity);
            batchResult.setCostPerUnit(new BigDecimal(costPerUnit));

            PotencyMG potencyMG = new PotencyMG();

            potencyMG.setThc(getDecimalValue(csvRecord, "Potency: THC"));
            potencyMG.setCbd(getDecimalValue(csvRecord, "Potency: CBD"));
            potencyMG.setCbn(getDecimalValue(csvRecord, "Potency: CBN"));
            potencyMG.setThca(getDecimalValue(csvRecord, "Potency: THCA"));
            potencyMG.setCbda(getDecimalValue(csvRecord, "Potency: CBDA"));

            batchResult.setPotencyAmount(potencyMG);


            product = productMap.get(productId);
            if (Objects.isNull(product)) {
                success = false;
                result.addFailed(batchResult, "Product not found");
            } else if (batchDate.isEmpty() && quantity.doubleValue() == 0) {
                success = false;
                result.addFailed(batchResult, "Batch Date is not present.");
                if(quantity.doubleValue() == 0) {
                    success = false;
                    result.addFailed(batchResult, "No quantity is present.");
                }
            }


            if (success) {
                result.addSuccess(batchResult);
            }
        }
        return result;
    }

    private double getDoubleValue(CSVRecord csvRecord, String key) {
        String record = csvRecord.isSet(key) ? csvRecord.get(key) : "";
        try {
            //ImporterUtil.getDecimalValue()
            return ImporterUtil.getDecimalValue(record);
        } catch (Exception e) {
            return 0d;
        }
    }

    private BigDecimal getDecimalValue(CSVRecord csvRecord, String key) {
        String record = csvRecord.isSet(key) ? csvRecord.get(key) : "";
        try {
            return new BigDecimal(record);
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    private void updateProductBatchInformation(Product dbProduct) {

        // Add change log
        ProductChangeLog changeLog = productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                dbProduct, null, "Add Batch");

        productRepository.update(token.getCompanyId(), dbProduct.getId(), dbProduct);
        productChangeLogRepository.save(changeLog);

        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    private void addNewBatchInventoryQuantity(Product product, String inventoryId, BigDecimal inventoryQuantity) {
        Inventory dbInventory = null;

        if (inventoryId != null) {
            dbInventory = inventoryRepository.get(token.getCompanyId(), inventoryId);
        }
        if (dbInventory == null) {
            dbInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        }

        if (dbInventory == null) {
            dbInventory = new Inventory();
            dbInventory.prepare(token.getCompanyId());
            dbInventory.setShopId(token.getShopId());
            dbInventory.setName(Inventory.SAFE);
            dbInventory.setType(Inventory.InventoryType.Storage);
            inventoryRepository.save(dbInventory);
        }

        ProductQuantity quantity = null;
        for (ProductQuantity q : product.getQuantities()) {
            if (q.getInventoryId().equalsIgnoreCase(dbInventory.getId())) {
                quantity = q;
                break;
            }
        }
        if (quantity == null) {
            quantity = new ProductQuantity();
            quantity.setId(ObjectId.get().toString());
            quantity.setCompanyId(token.getCompanyId());
            quantity.setShopId(token.getShopId());
            quantity.setInventoryId(dbInventory.getId());
            quantity.setQuantity(inventoryQuantity);
            product.getQuantities().add(quantity);

        } else {
            double newQuantity = quantity.getQuantity().doubleValue() + inventoryQuantity.doubleValue();
            quantity.setQuantity(BigDecimal.valueOf(newQuantity));
        }
    }

    private String getInventoryIdByName(HashMap<String, Inventory> inventoryMap, String inventoryName) {
        String id = "";

        for (Map.Entry<String, Inventory> entry : inventoryMap.entrySet()) {
            if (entry.getValue().getName().equals(inventoryName)) {
                id = entry.getKey();
                break;
            }
        }

        return id;
    }

    /* create new batch */
    public Map<String, Object> createBatch(Product dbProduct,
                                    ProductBatchResult batchResult) {

        Map<String, Object> batchMap = new HashMap<>();


        /* Check Exceptions first */
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("BatchAddRequest", "Product does not exist.");
        }

        ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), dbProduct.getCategoryId());
        if (productCategory == null) {
            throw new BlazeInvalidArgException("BatchAddRequest", "Category does not exist.");
        }

        Shop shop = shopRepository.getById(token.getShopId());

        String batchSKU = batchResult.getBatchSKU();
        //BigDecimal cost = batchResult.getCogs();
        BigDecimal costPerUnit = batchResult.getCostPerUnit();
        long purchasedDate = batchResult.getBatchDate();
        BigDecimal quantity = batchResult.getQuantity();

        ProductBatch productBatch = new ProductBatch();
        productBatch.prepare(token.getCompanyId());
        productBatch.setProductId(dbProduct.getId());
        productBatch.setShopId(token.getShopId());
        productBatch.setSku(batchSKU);
        productBatch.setVendorId(dbProduct.getVendorId());
        productBatch.setQuantity(new BigDecimal(0));
        productBatch.setThc(batchResult.getThc());
        productBatch.setCbn(batchResult.getCbn());
        productBatch.setCbd(batchResult.getCbd());
        productBatch.setCbda(batchResult.getCbda());
        productBatch.setThca(batchResult.getThca());
        productBatch.setTrackHarvestBatch(batchResult.getTrackHarvestBatch());

        productBatch.setPurchasedDate(purchasedDate);
        if (StringUtils.isNotBlank(batchResult.getTrackPackageLabel())) {
            productBatch.setTrackPackageLabel(batchResult.getTrackPackageLabel());
            productBatch.setTrackTraceSystem(ProductBatch.TrackTraceSystem.METRC);
        }

        productBatch.setSellBy(DateTime.now().getMillis());

        if (quantity.doubleValue() < 0) {
            productBatch.setQuantity(new BigDecimal(0));
        } else {
            productBatch.setQuantity(quantity);
        }
        if (costPerUnit == null) {
            costPerUnit = new BigDecimal(0);
        }

        productBatch.setStatus((shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) ? ProductBatch.BatchStatus.READY_FOR_SALE : ProductBatch.BatchStatus.RECEIVED);

        BarcodeItem item;
        try {


            if (productBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.MANUAL) {
                productBatch.setTrackPackageLabel("");
                productBatch.setTrackPackageId(null);
                productBatch.setTrackTraceVerified(false);
            }
            if (StringUtils.isNotBlank(productBatch.getTrackPackageLabel())) {
                MetrcVerifiedPackage metricsPackages = metrcAccountService.getMetrcVerifiedPackage(productBatch.getTrackPackageLabel());
                if (metricsPackages != null && metricsPackages.isRequired()) {
                    productBatch.setTrackPackageLabel(productBatch.getTrackPackageLabel());
                    productBatch.setTrackTraceSystem(productBatch.getTrackTraceSystem());
                    if (metricsPackages.getId() > 0) {
                        productBatch.setTrackPackageId(metricsPackages.getId());
                    } else {
                        productBatch.setTrackPackageId(null);
                    }
                    productBatch.setTrackTraceVerified(true);
                    productBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());

                    BigDecimal verifiedQty = metrcAccountService.convertToBatchQuantity(metricsPackages,productCategory.getUnitType(),dbProduct);
                    productBatch.setQuantity(verifiedQty);
                }
            }

            // Update sku if needed
            item = barcodeService.createBarcodeItemIfNeeded(token.getCompanyId(),dbProduct.getShopId(), dbProduct.getId(),
                    BarcodeItem.BarcodeEntityType.Batch,
                    productBatch.getId(), productBatch.getSku(), null, false);

            if (item != null) {
                productBatch.setSku(item.getBarcode());
                productBatch.setBatchNo(item.getNumber());
            }

            // specify cost per unit & final cost
            productBatch.setCost(costPerUnit.multiply(productBatch.getQuantity()));
            productBatch.setCostPerUnit(costPerUnit);
            productBatch.setPotencyAmount(batchResult.getPotencyAmount());

            batchMap.put("batch", productBatch);
        } catch (Exception e) {
            batchMap.put("errorMessage", e.getMessage());
            return batchMap;
        }
        return batchMap;
    }

    public void importProductBatch(List<ProductBatchResult> productBatchResult) {
        if (productBatchResult == null) {
            throw new BlazeInvalidArgException("Product Batch", "Please add information for import");
        }

        LOG.info("BEGINNING Batch import.");
        List<ObjectId> productIds = new ArrayList<>();
        for (ProductBatchResult batchResult : productBatchResult) {
            productIds.add(new ObjectId(batchResult.getProductId()));
        }

        Map<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);

        List<QueuedTransaction> queuedTransactionList = new ArrayList<>();
        List<ProductBatch> productBatches = new ArrayList<>();

        boolean createBatches = Boolean.TRUE;
        String errorMessage = StringUtils.EMPTY;

        for (ProductBatchResult batchResult : productBatchResult) {
            Product product = productMap.get(batchResult.getProductId());

            ProductBatch productBatch;
            Map<String, Object> batch = this.createBatch(product, batchResult);

            productBatch = (ProductBatch) batch.get("batch");

            if (productBatch == null) {
                createBatches = Boolean.FALSE;
                errorMessage = (String) batch.get("errorMessage");
                break;
            }
            batchResult.setId(productBatch.getId());
            batchResult.setProductId(product.getId());

            productBatches.add(productBatch);

            ConcurrentHashMap<String, BigDecimal> inventoryQuantities = new ConcurrentHashMap<>(batchResult.getInventoryQuantity());

            Iterator<Map.Entry<String, BigDecimal>> it = inventoryQuantities.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry<String, BigDecimal> entry = it.next();
                String inventoryId = entry.getKey();
                double quantity = entry.getValue().doubleValue();
                Inventory inventory = this.createInventory(inventoryId, productBatch.getShopId());
                if (!inventory.getId().equalsIgnoreCase(inventoryId)) {
                    double quant = inventoryQuantities.get(inventory.getId()) != null ? inventoryQuantities.get(inventory.getId()).doubleValue() : 0;
                    inventoryQuantities.putIfAbsent(inventory.getId(), BigDecimal.ZERO);
                    inventoryQuantities.put(inventory.getId(), new BigDecimal(quantity + quant));
                    it.remove();
                }
            }

            batchResult.setInventoryQuantity(new HashMap<>(inventoryQuantities));
        }

        if (!createBatches) {
            List<String> batchSkuList = new ArrayList<>();
            for (ProductBatch batch : productBatches) {
                batchSkuList.add(batch.getSku());
            }

            //If there is any error in batch creation then delete already created barcodes
            int count = barcodeItemRepository.removeByBarCodes(token.getCompanyId(), token.getShopId(), batchSkuList);
            LOG.info("Count of deleted barcode items : " + count);

            throw new BlazeInvalidArgException("Product Batch", errorMessage);
        }

        List<List<ProductBatchResult>> batchPartition = Lists.partition(productBatchResult, 50);

        HashMap<String, Map<String, BigDecimal>> batchMap = null;
        for (List<ProductBatchResult> batchResults : batchPartition) {
            batchMap = new HashMap<>();

            for (ProductBatchResult batchResult : batchResults) {
                batchMap.putIfAbsent(batchResult.getId(), new HashMap<>());
                Map<String, BigDecimal> batchQuantityMap = batchMap.get(batchResult.getId());
                batchQuantityMap.putAll(batchResult.getInventoryQuantity());
                batchMap.put(batchResult.getId(), batchQuantityMap);
            }

            queuedTransactionList.add(createQueuedTransactionJobForBatchQuantity(batchMap, ProductBatchQueuedTransaction.OperationType.Add,  null));
        }

        productBatchRepository.save(productBatches);
        queuedTransactionRepository.save(queuedTransactionList);

        List<BatchActivityLog> batchActivityLogs = new ArrayList<>();

        productBatches.forEach(productBatch -> {
            BatchActivityLog activityLog = new BatchActivityLog();
            activityLog.prepare(token.getCompanyId());
            activityLog.setShopId(token.getShopId());
            activityLog.setTargetId(productBatch.getId());
            activityLog.setActivityType(BatchActivityLog.ActivityType.NORMAL_LOG);
            activityLog.setEmployeeId(token.getActiveTopUser().getUserId());
            activityLog.setLog(productBatch.getSku() + " Batch imported");
            batchActivityLogs.add(activityLog);
        });

        batchActivityLogRepository.save(batchActivityLogs);

        for (QueuedTransaction queuedTransaction : queuedTransactionList) {
            backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
        }

        LOG.info("Batch import complete.");
    }

    private Inventory createInventory(String inventoryId, String shopId) {
        Inventory dbInventory = null;

        if (StringUtils.isNotBlank(inventoryId)) {
            dbInventory = inventoryRepository.get(token.getCompanyId(), inventoryId);
        }

        if (dbInventory == null) {
            dbInventory = inventoryRepository.getInventory(token.getCompanyId(), shopId, Inventory.SAFE);
        }

        if (dbInventory == null) {
            dbInventory = new Inventory();
            dbInventory.setCompanyId(token.getCompanyId());
            dbInventory.setShopId(shopId);
            dbInventory.setName(Inventory.SAFE);
            dbInventory.setType(Inventory.InventoryType.Storage);
            inventoryRepository.save(dbInventory);
        }
        return dbInventory;
    }

    /**
     * Create queue transaction job for add batch quantity.
     *
     * @param operationType
     * @param sourceChildId
     * @return
     */
    private QueuedTransaction createQueuedTransactionJobForBatchQuantity(HashMap<String, Map<String, BigDecimal>> batchQuantityMap, ProductBatchQueuedTransaction.OperationType operationType, String sourceChildId) {

        ProductBatchQueuedTransaction queuedTransaction = new ProductBatchQueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.BulkProductBatch);
        queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setOperationType(operationType);
        queuedTransaction.setBatchQuantityMap(batchQuantityMap);

        queuedTransaction.setSubSourceAction(InventoryOperation.SubSourceAction.AddBatch);

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);
        queuedTransaction.setSourceChildId(sourceChildId);

        return queuedTransaction;
    }
}
