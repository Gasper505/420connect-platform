package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.ImportProductAddRequest;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.importer.model.ProductContainer;
import com.fourtwenty.core.importer.util.ImporterUtil;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.fourtwenty.core.services.mgmt.VendorService;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by mdo on 3/24/16.
 */
public class ProductParser implements IDataParser<ProductContainer> {
    private static final Log LOG = LogFactory.getLog(ProductParser.class);
    private final ProductService productService;
    private final VendorService vendorService;

    public ProductParser(ProductService productService, VendorService vendorService) {
        this.productService = productService;
        this.vendorService = vendorService;
    }

    @Override
    public ParseResult<ProductContainer> parse(CSVParser csvParser, Shop shop) {
        List<Vendor> vendors = vendorService.getAllVendors();
        HashMap<String, Vendor> vendorMap = new HashMap<>();
        HashMap<String, Vendor> vendorByNameMap = new HashMap<>();
        for (Vendor d : vendors) {
            if (!StringUtils.isBlank(d.getImportId())) {
                vendorMap.put(d.getImportId(), d);
            }
            vendorByNameMap.put(d.getName().toLowerCase().trim(),d);
        }
        List<ProductWeightTolerance> tolerances = productService.getWeightTolerances();


        Map<String, Integer> headers = csvParser.getHeaderMap();

        List<String> inventoryHeaders = new ArrayList<>();
        for (String header : headers.keySet()) {
            if (header.startsWith("INV: ")) {
                inventoryHeaders.add(header);
            }
        }

        ParseResult<ProductContainer> result = new ParseResult<ProductContainer>(Parser.ParseDataType.Product);
        for (CSVRecord r : csvParser) {
            ProductContainer entity = buildProductRequest(r, vendorMap, vendorByNameMap,tolerances, inventoryHeaders, shop);

            if (entity != null) {
                result.addSuccess(entity);
            }

        }
        return result;
    }


    private ProductContainer buildProductRequest(CSVRecord r, HashMap<String, Vendor> vendorHashMap, HashMap<String, Vendor> vendorNameMap, List<ProductWeightTolerance> weightTolerances, List<String> inventoryHeaders, Shop shop) {
        ImportProductAddRequest request = new ImportProductAddRequest();
        request.setVendorImportId(r.get("Vendor ID"));
        request.setName(r.get("Item"));
        request.setUnitType(r.get("Measurement"));
        request.setFlowerType(r.get("Type"));
        request.setDescription(r.get("Description"));
        request.setGenetics(r.get("Genetics"));
        request.setSku(r.get("SKU"));
        request.setPercentTHC(ImporterUtil.getPercentValue(r.get("THC %")));
        request.setPercentCBD(ImporterUtil.getPercentValue(r.get("CBD %")));
        request.setPercentCBN(ImporterUtil.getPercentValue(r.get("CBN %")));
        request.setUnitPrice(ImporterUtil.getDollarAmountDecimal(r.get("Unit Price")));

        Long datePurchased = ImporterUtil.getDate(r.get("Date Purchased"), shop.getTimeZone());
        request.setDatePurchased(datePurchased == null ? DateTime.now().getMillis() : datePurchased);

        if (!r.get("Inventory Available").isEmpty()) {
            try {
                request.setInventoryAvailable(ImporterUtil.getDollarAmountDecimal(r.get("Inventory Available")));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }


        request.setCannabisType(Product.CannabisType.DEFAULT);
        try {
            String canType = r.get("Cannabis Type");
            Product.CannabisType cannabisType = Product.CannabisType.valueOf(canType);
            request.setCannabisType(cannabisType);
        } catch (Exception e) {
            // Ignore
        }

        Product product = productService.sanitize(request, vendorHashMap);
        product.setId(ObjectId.get().toString());
        ProductContainer productPackage = new ProductContainer();
        productPackage.setProduct(product);
        productPackage.setCategoryName(r.get("Category"));
        if (productPackage.getCategoryName() != null) {
            productPackage.setCategoryName(productPackage.getCategoryName().trim());
        }
        productPackage.setPercentTHC(request.getPercentTHC());
        productPackage.setPercentCBD(request.getPercentCBD());
        productPackage.setPercentCBN(request.getPercentCBN());
        productPackage.setInventoryAvailable(request.getInventoryAvailable());
        productPackage.setDatePurchased(request.getDatePurchased());
        productPackage.setImage1(r.get("Image 1"));
        productPackage.setImage2(r.get("Image 2"));
        productPackage.setImage3(r.get("Image 3"));
        productPackage.setImage4(r.get("Image 4"));
        productPackage.setImage5(r.get("Image 5"));
        productPackage.setCostPerUnit(ImporterUtil.getDollarAmountDecimal(r.get("Cost per Unit")));
        productPackage.setUnitType(r.get("Measurement"));
        productPackage.setBrand(r.get("Brand"));

        // Check if vendor doesn't exist
        if (StringUtils.isBlank(product.getVendorId())) {
            Vendor vendor = vendorNameMap.get(request.getVendorImportId().toLowerCase().trim());
            if (vendor != null) {
                product.setVendorId(vendor.getId());
                product.setVendor(vendor);
            }
        }


        boolean active = true;
        try {
            active = ImporterUtil.parseBoolean(r.get("Active"));
        } catch (Exception e) {

        }
        product.setActive(active);


        request.setImportId(request.getName() + productPackage.getBrand() +  request.getVendorImportId());
        if (StringUtils.isNotBlank(request.getSku())) {
            request.setImportId(request.getSku().trim());
        }
        // Handle tags
        try {
            String tags = r.get("Tag");
            if (StringUtils.isNotBlank(tags)) {
                String[] tagArr = tags.split(";");

                LinkedHashSet<String> tagSet = new LinkedHashSet<>();
                for (String t : tagArr) {
                    tagSet.add(t);
                }
                product.setTags(tagSet);
            }
        } catch (Exception e) {
            // Ignore
        }

        try {
            String pId = r.get("Product ID");
            productPackage.setProductId(pId);
        } catch (Exception e) {
        }

        // Handle Custom Weights



        List<ProductPriceRange> ranges = new ArrayList<>();
        List<ProductPriceBreak> priceBreaks = new ArrayList<>();
        if (!productPackage.getUnitType().equalsIgnoreCase(ProductCategory.UnitType.units.getType())) {
            for (ProductWeightTolerance tolerance : weightTolerances) {
                String value = r.get(tolerance.getName() + " Price");
                BigDecimal dValue = ImporterUtil.getDollarAmountDecimal(value);
                if (dValue != null) {
                    ProductPriceRange range = new ProductPriceRange();
                    range.setPrice(dValue);
                    range.setWeightToleranceId(tolerance.getId());
                    range.setWeightTolerance(tolerance);
                    range.setPriority(tolerance.getPriority());

                    range.setId(String.format("%s_%s", product.getId(), range.getWeightToleranceId()));
                    ranges.add(range);
                }
            }
        } else {
            // priceBreaks
            // Headers
            String weightPerUnit = r.get("Weight Per Unit");
            if (StringUtils.isNotBlank(weightPerUnit)) {
                try {
                    weightPerUnit = weightPerUnit.toUpperCase();
                    Product.WeightPerUnit weight = Enum.valueOf(Product.WeightPerUnit.class, weightPerUnit);
                    product.setWeightPerUnit(weight);
                } catch (Exception e) {
                }
                if (weightPerUnit != null &&
                        (weightPerUnit.equalsIgnoreCase("custom")
                                || weightPerUnit.equalsIgnoreCase("CUSTOM_GRAMS"))) {
                    product.setWeightPerUnit(Product.WeightPerUnit.CUSTOM_GRAMS);
                }
            }

            if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                String weightType = r.get("Custom Weight Type");
                if (StringUtils.isNotBlank(weightType)) {
                    if (weightType.equalsIgnoreCase("grams") || weightType.equalsIgnoreCase("g")) {
                        product.setCustomGramType(Product.CustomGramType.GRAM);
                    } else if (weightType.equalsIgnoreCase("milligrams") || weightType.equalsIgnoreCase("mg")) {
                        product.setCustomGramType(Product.CustomGramType.MILLIGRAM);
                    }
                    BigDecimal weightValue = ImporterUtil.getDollarAmountDecimal(r.get("Custom Weight Measurement"));
                    product.setCustomWeight(weightValue);
                }

            }


            if (product.getPriceBreaks() != null) {
                for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                    priceBreak.resetPrepare(product.getCompanyId());
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                            priceBreak.setActive(true);
                        }
                    } else {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                            priceBreak.setActive(true);
                        }
                    }
                }
            }
            priceBreaks = populatePriceBreaks(product,product.getPriceBreaks(),true,true,false);

        }
        product.setPriceRanges(ranges);
        product.setPriceBreaks(priceBreaks);


        List<String> keys = new ArrayList<>();
        r.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                keys.add(s);
                if (s.startsWith("INV: ")) {
                    LOG.info(s);
                }
            }
        });

        HashMap<String, BigDecimal> inventoryQuantities = new HashMap<>();
        for (String inventoryName : inventoryHeaders) {
            BigDecimal inventoryQty = ImporterUtil.getDollarAmountDecimal(r.get(inventoryName));
            String iName = inventoryName.replace("INV: ", "");
            inventoryQuantities.put(iName, inventoryQty);
        }

        productPackage.setInventoryQuantities(inventoryQuantities);

        return productPackage;
    }


    public List<ProductPriceBreak> populatePriceBreaks(final Product product,
                                                       List<ProductPriceBreak> priceBreaks,
                                                       boolean isDefault,
                                                       boolean reset, Boolean isPricingTemplate) {

        // Fill in the priceBreaks or priceRanges

        final ProductPriceBreak.PriceBreakType[] priceBreakTypes = ProductPriceBreak.PriceBreakType.values();

        // Remove bad price breaks
        priceBreaks.removeIf(new Predicate<ProductPriceBreak>() {
            @Override
            public boolean test(ProductPriceBreak productPriceBreak) {

                return (product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                        product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM ||
                        product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS)
                        && productPriceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit;
            }
        });

        if (reset) {
            for (ProductPriceBreak productPriceBreak : priceBreaks) {
                productPriceBreak.setPrice(new BigDecimal(0));
                productPriceBreak.setActive(false);
            }
        }

        for (ProductPriceBreak.PriceBreakType type : priceBreakTypes) {
            if (type == ProductPriceBreak.PriceBreakType.None) {
                continue;
            }

            if (type == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                        || product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                        product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    continue;
                }
            }

            // find the price break by type
            ProductPriceBreak priceBreak = null;
            for (ProductPriceBreak productPriceBreak : priceBreaks) {
                if (productPriceBreak.getPriceBreakType() == type) {
                    priceBreak = productPriceBreak;
                    break;
                }
            }
            // if none is found, create a new one
            if (priceBreak == null) {
                // Add a new one
                priceBreak = new ProductPriceBreak();
                priceBreak.prepare(product.getCompanyId());
                priceBreak.setPriceBreakType(type);
                priceBreak.setActive(false);


                // add price break to group
                priceBreaks.add(priceBreak);
            }

            // specify name and quantity
            if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                    product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                priceBreak.setName(type.eachName);
                priceBreak.setQuantity(type.fullGramValue);
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                priceBreak.setQuantity(type.fullGramValue);
                priceBreak.setName(type.gramName);
            } else {
                priceBreak.setName(type.gramName);
                priceBreak.setQuantity(type.halfGramValue);
            }

            // default cost
            if (isDefault) {
                if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM ||
                        product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                        if (isPricingTemplate) {
                            product.setUnitPrice(priceBreak.getPrice() == null ? BigDecimal.ZERO : priceBreak.getPrice());
                        } else {
                            priceBreak.setPrice(product.getUnitPrice());
                        }
                        priceBreak.setActive(true);
                    }
                } else {
                    if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                        if (isPricingTemplate) {
                            product.setUnitPrice(priceBreak.getPrice() == null ? BigDecimal.ZERO : priceBreak.getPrice());
                        } else {
                            priceBreak.setPrice(product.getUnitPrice());
                        }
                        priceBreak.setActive(true);
                    }
                }
            }

            if (reset) {
                priceBreak.setPrice(product.getUnitPrice().multiply(new BigDecimal(priceBreak.getQuantity())));
            }
        }

        // Sort price breaks
        priceBreaks.sort(new Comparator<ProductPriceBreak>() {
            @Override
            public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                return ((Integer) o1.getQuantity()).compareTo((Integer) o2.getQuantity());
            }
        });

        return priceBreaks;
    }

}
