package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

public class PromotionParser implements IDataParser<Promotion>  {

    private static final String PROMOTION = "Promotion";
    private static final String DISCOUNT_TYPES = "Discount Type is not valid.Only %s are allowed.";
    private static final String PROMOTION_TYPES = "Promotion Type is not valid.Only %s are allowed.";


    public PromotionParser() {}

    @Override
    public ParseResult<Promotion> parse(CSVParser csvParser, Shop shop) {
        ParseResult<Promotion> result = new ParseResult<Promotion>(Parser.ParseDataType.Promotion);
        for (CSVRecord r : csvParser) {
            Promotion entity = buildPromoRequest(r);
            if (entity != null) {
                result.addSuccess(entity);
            }
        }
        return result;
    }

    private Promotion buildPromoRequest(CSVRecord r) {
        Map<String, String> csvRecord = r.toMap();
        Promotion promotion = new Promotion();
        promotion.prepare();
        for (Map.Entry<String, String> entry : csvRecord.entrySet()) {
            String key = entry.getKey();
            switch (key) {
                case "name":
                    promotion.setName(entry.getValue());
                    break;
                case "promo_code":
                    promotion.setPromoCodes(new HashSet<>(Arrays.asList(entry.getValue().split("\\s*,\\s*"))));
                    break;
                case "promotion_type":
                    try {
                        promotion.setPromotionType(Promotion.PromotionType.valueOf(entry.getValue()));
                    } catch (Exception e) {
                        throw new BlazeInvalidArgException(PROMOTION, String.format(PROMOTION_TYPES, Lists.newArrayList(Promotion.PromotionType.values()).toString()));
                    }
                    break;
                case "discount_type":
                    try {
                        promotion.setDiscountType(OrderItem.DiscountType.valueOf(entry.getValue()));
                    } catch (Exception e) {
                        throw new BlazeInvalidArgException(PROMOTION, String.format(DISCOUNT_TYPES, Lists.newArrayList(OrderItem.DiscountType.values()).toString()));
                    }
                    break;
                case "amount":
                    try {
                        promotion.setDiscountAmt(BigDecimal.valueOf(Double.parseDouble(entry.getValue())));
                    } catch (Exception e) {
                        throw new BlazeInvalidArgException(PROMOTION, "Discount amount should be numeric value.");
                    }
                    break;
                case "description":
                    promotion.setPromoDesc(entry.getValue());
                    break;
                case "expiry_date":
                    if (StringUtils.isNotBlank(entry.getValue())) {
                        try {
                            DateTime date = DateUtil.parseStrDateToDateTimeForPattern(entry.getValue(), "yyyy-MM-dd");
                            promotion.setEndDate(date.getMillis());
                        } catch (Exception e) {
                            throw new BlazeInvalidArgException(PROMOTION, "Expiry Date should be in YYYY-MM-DD format.");
                        }
                    }
                    break;
                case "Enable Free Shipping":
                    //toDo: No field for enable free shipping
                    break;
                case "stackable":
                    promotion.setStackable(StringUtils.isNotBlank(entry.getValue()) && entry.getValue().equalsIgnoreCase("yes"));
                    break;
                case "usage_limit":
                    if (StringUtils.isNumeric(entry.getValue())) {
                        promotion.setMaxAvailable(Integer.parseInt(entry.getValue()));
                        if (promotion.getUsageCount() > 0) {
                            promotion.setEnableMaxAvailable(true);
                        }
                    }
                    break;
                case "usage_limit_per_user":
                    if (StringUtils.isNumeric(entry.getValue())) {
                        promotion.setLimitPerCustomer(Integer.parseInt(entry.getValue()));
                        if (promotion.getLimitPerCustomer() > 0) {
                            promotion.setEnableLimitPerCustomer(true);
                        }
                    }
                    break;
            }
        }
        return promotion;
    }
}
