package com.fourtwenty.core.importer.main.parsers;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.model.ParseResult;
import com.fourtwenty.core.importer.util.ImporterUtil;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.services.mgmt.VendorService;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 3/24/16.
 */
public class VendorParser implements IDataParser<Vendor> {

    private final VendorService vendorService;

    public VendorParser(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    @Override
    public ParseResult<Vendor> parse(CSVParser csvParser, Shop shop) {
        ParseResult<Vendor> result = new ParseResult<Vendor>(Parser.ParseDataType.Vendor);
        for (CSVRecord r : csvParser) {
            Vendor entity = buildVendorRequest(r);
            if (entity != null) {
                result.addSuccess(entity);
            }
        }
        return result;
    }


    private Vendor buildVendorRequest(CSVRecord csvRecord) {
        Map<String, String> r = csvRecord.toMap();

        VendorAddRequest request = new VendorAddRequest();
        Address address = new Address();
        List<Note> notes = new ArrayList<>();

        for (Map.Entry<String, String> entry : r.entrySet()) {
            String key = entry.getKey();
            switch (key) {
                case "Vendor ID":
                    request.setImportId(r.get(key));
                    break;
                case "Vendor Type":
                    try {
                        Vendor.VendorType vendorType = Vendor.VendorType.valueOf(r.get(key).trim().toUpperCase());
                        request.setVendorType(vendorType);
                    } catch (Exception e) {
                    }
                    break;
                case "Vendor Name":
                    request.setName(r.get(key));
                    break;
                case "Contact First":
                    request.setContactFirstName(r.get(key));
                    break;
                case "Contact Last Name":
                    request.setContactLastName(r.get(key));
                    break;
                case "Phone":
                    request.setPhone(r.get(key));
                    break;
                case "Email":
                    request.setEmail(r.get(key));
                    break;
                case "Active":
                    request.setActive(ImporterUtil.parseBoolean(r.get(key)));
                    break;
                case "Website":
                    request.setWebsite(r.get(key));
                    break;
                case "Street Address":
                    address.setAddress(r.get(key));
                    break;
                case "City":
                    address.setCity(r.get(key));
                    break;
                case "State":
                    address.setState(r.get(key));
                    break;
                case "Zip Code":
                    address.setZipCode(r.get(key));
                    break;
                case "License Number":
                    request.setLicenseNumber(r.get(key));
                    break;

                case "License Type":
                    Vendor.LicenceType licenceType = Vendor.LicenceType.BOTH;
                    try {
                        licenceType = Vendor.LicenceType.valueOf(r.get(key));
                    } catch (Exception e) {
                        // ignore
                    }
                    request.setLicenceType(licenceType);
                    break;
                case "Company Type":
                    Vendor.CompanyType companyType = Vendor.CompanyType.OTHER;
                    try {
                        companyType = Vendor.CompanyType.valueOf(r.get(key));
                    } catch (Exception e) {
                        // ignore
                    }
                    request.setCompanyType(companyType);
                    break;
                default:
                    if (key.startsWith("Note")) {
                        if (StringUtils.isBlank(r.get(key))) {
                            continue;
                        }
                        Note note = new Note();
                        note.setMessage(r.get(key));

                        notes.add(note);
                    }
                    break;
            }
        }
        request.setAddress(address);
        request.setNotes(notes);

        return vendorService.sanitize(request);
    }
}
