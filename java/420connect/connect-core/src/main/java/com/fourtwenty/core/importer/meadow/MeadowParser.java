package com.fourtwenty.core.importer.meadow;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.ProductBatchQueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.importer.meadow.models.*;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MeadowParser {
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    BarcodeItemRepository barcodeItemRepository;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    BatchActivityLogRepository batchActivityLogRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    BrandRepository brandRepository;


    public void importMeadowProducts(String companyid, String shopId, String userId, InputStream inputStream) {

        MeadowProductsContainer productsContainer = JsonSerializer.fromJson(inputStream, MeadowProductsContainer.class);

        Inventory safeInventory = inventoryRepository.getInventory(companyid,shopId, Inventory.SAFE);


        Iterable<Vendor> oldVendors = vendorRepository.list(companyid);
        Iterable<ProductCategory> categories = productCategoryRepository.listAllByShop(companyid,shopId);

        HashMap<String,Vendor> vendorsMap = new HashMap<>();
        HashMap<String,ProductCategory> productCategoryHashMap = new HashMap<>();

        oldVendors.forEach(vendor -> vendorsMap.put(vendor.getName(),vendor));
        categories.forEach(category -> productCategoryHashMap.put(category.getName(),category));

        Iterable<Brand> brands = brandRepository.list(companyid);
        HashMap<String,Brand> brandHashMap = new HashMap<>();
        for (Brand b : brands) {
            brandHashMap.put(b.getName(),b);
        }

        HashMap<String, BarcodeItem> barcodeItemHashMap = new HashMap<>();
        HashMap<String,String> productSKUs = new HashMap<>();
        HashMap<String,String> batchSKUs = new HashMap<>();


        List<Product> products = new ArrayList<>();
        List<ProductBatch> productBatches = new ArrayList<>();
        List<BarcodeItem> barcodeItems = new ArrayList<>();
        List<CompanyAsset> companyAssets = new ArrayList<>();
        List<Brand> newBrands = new ArrayList<>();
        List<QueuedTransaction> queuedTransactionList = new ArrayList<>();

        for (MeadowProduct meadowProduct : productsContainer.getData()) {
            Vendor vendor = createOrGetVendor(companyid,meadowProduct.getVendor(),vendorsMap);

            MeadowProductCategory meadowCategory = meadowProduct.getPrimaryCategory();
            if (meadowCategory == null) {
                continue;
            }
            boolean isCannabis = !meadowCategory.getCannabisType().equalsIgnoreCase("none");
            ProductCategory productCategory = createOrGetCategory(companyid,shopId,meadowCategory.getName().trim(),isCannabis,productCategoryHashMap);

            String brandId = null;
            if (StringUtils.isNotBlank(meadowProduct.getVendor())) {
                Brand brand = brandHashMap.get(meadowProduct.getVendor());
                if (brand == null) {
                    brand = new Brand();
                    brand.prepare(companyid);
                    brand.setName(meadowProduct.getVendor());
                    brand.setActive(true);
                    newBrands.add(brand);
                }
                brandId = brand.getId();
                vendor.getBrands().add(brand.getId());

                vendorRepository.update(companyid,vendor.getId(),vendor);
            }


            for (MeadowProductOption productOption : meadowProduct.getOptions()) {
                String productName = meadowProduct.getName();
                if (productOption.getName() != null) {
                    productName = productName + " " + productOption.getName();
                    productName = productName.trim();
                }





                Product product = new Product();
                product.prepare(companyid);
                product.setShopId(shopId);
                product.setName(productName);
                product.setSku(productOption.getUpc());
                product.setCategory(productCategory);
                product.setCategoryId(productCategory.getId());
                product.setVendorId(vendor.getId());
                product.setDescription(meadowProduct.getDescription());
                product.setFlowerType(meadowProduct.getStrainType());
                product.setActive(meadowProduct.isActive());
                product.setBrandId(brandId);
                product.setImportId(productOption.getId() + "");
                product.setImportSrc(Importable.ImportSource.Meadow.name());

                // Deal with skus

                String productSku = productOption.getUpc();
                if (productSku != null && productSKUs.containsKey(productSku)) {
                    productSku = null;
                }
                if (StringUtils.isBlank(productSku)) {

                    String ui = UUID.randomUUID().toString().substring(0,4);
                    productSku = ui + genBarcode("SESPE",product.getId(),product.getId(), BarcodeItem.BarcodeEntityType.Product);
                }
                productSKUs.put(productSku,productSku);

                product.setSku(productSku);

                BarcodeItem barcodeItem = new BarcodeItem();
                barcodeItem.prepare(companyid);
                barcodeItem.setShopId(shopId);
                barcodeItem.setProductId(product.getId());
                barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Product);
                barcodeItem.setEntityId(product.getId());
                barcodeItem.setBarcode(productSku);
                barcodeItems.add(barcodeItem);


                // Deal with CBD & THC
                String thc = meadowProduct.getPercentThc().replaceAll("mg","");
                String cbd = meadowProduct.getPercentCbd().replaceAll("mg","");
                BigDecimal thcValue = BigDecimal.ZERO;
                BigDecimal cbdValue = BigDecimal.ZERO;
                try {
                    thcValue = new BigDecimal(thc);
                } catch (Exception e) {
                    // ignore
                }
                try {
                    cbdValue = new BigDecimal(cbd);
                } catch (Exception e) {
                    // ignore
                }

                if (meadowProduct.getPercentThc().contains("mg") || meadowProduct.getPercentCbd().contains("mg")) {
                    // This is now potency
                    product.setPotency(true);

                    PotencyMG potencyMG = new PotencyMG();
                    potencyMG.setThc(thcValue);
                    potencyMG.setCbd(cbdValue);
                    product.setPotencyAmount(potencyMG);
                } else {
                    product.setThc(thcValue.doubleValue());
                    product.setCbd(cbdValue.doubleValue());
                }


                // Deal with weight per unit
                String optionName = "";
                if (productOption.getName() != null) {
                    optionName = productOption.getName().toLowerCase();
                }
                if (optionName.contains("eighth")) {
                    product.setWeightPerUnit(Product.WeightPerUnit.EIGHTH);
                } else if (optionName.contains("half gram")) {
                    product.setWeightPerUnit(Product.WeightPerUnit.HALF_GRAM);
                } else {
                    product.setWeightPerUnit(Product.WeightPerUnit.EACH);
                }


                // Set cannabis type
                if (meadowCategory.getCannabisType() != null) {
                    // options are non-concentrated or concentrated
                    if (meadowCategory.getCannabisType().equalsIgnoreCase("concentrated")) {
                        product.setCannabisType(Product.CannabisType.CONCENTRATE);
                    } else if (meadowCategory.getCannabisType().equalsIgnoreCase("non-concentrated")) {
                        product.setCannabisType(Product.CannabisType.NON_CONCENTRATE);
                    } else {
                        product.setCannabisType(Product.CannabisType.NON_CANNABIS);
                    }
                }

                // price
                long price = productOption.getPrice();
                double blazePrice = price / 100;
                product.setUnitPrice(new BigDecimal(blazePrice));

                List<ProductPriceBreak> productPriceBreaks = getPriceBreaks(product);
                product.setPriceBreaks(productPriceBreaks);

                products.add(product);



                // get inventory count
                double inventoryQty = 0;
                for (MeadowLocationInventory locationInventory : productOption.getLocationInventory()) {
                    inventoryQty += locationInventory.getMaxQuantity();
                }

                // only create batches if inventory qty > 0
                if (NumberUtils.round(inventoryQty,2) > 0) {
                    // Deal with Inventory Quantity
                    double cogs = 0;
                    if (productOption.getDefaultCostPerUnit() != null) {
                        try {
                            cogs = Double.parseDouble(productOption.getDefaultCostPerUnit());
                        } catch (Exception e) {
                            cogs = 0;
                        }
                    }


                    BigDecimal inventoryAmt = new BigDecimal(inventoryQty);
                    // Create corresponding batch
                    ProductBatch productBatch = new ProductBatch();
                    productBatch.prepare(companyid);
                    productBatch.setShopId(shopId);
                    productBatch.setProductId(product.getId());
                    productBatch.setVendorId(vendor.getId());
                    productBatch.setSku(productOption.getUpc());
                    productBatch.setQuantity(inventoryAmt);
                    productBatch.setCostPerUnit(new BigDecimal(cogs));
                    productBatch.setCost(new BigDecimal(cogs * inventoryQty));
                    String batchSKU = productOption.getUpc();
                    if (batchSKU != null && batchSKUs.containsKey(batchSKU)) {
                        batchSKU = null;
                    }
                    if (StringUtils.isBlank(productSku)) {
                        batchSKU = genBarcode("SESPE", product.getId(), productBatch.getId(), BarcodeItem.BarcodeEntityType.Batch);
                    }
                    productBatch.setSku(batchSKU);

                    BarcodeItem batchBarcodeItem = new BarcodeItem();
                    batchBarcodeItem.prepare(companyid);
                    batchBarcodeItem.setShopId(shopId);
                    batchBarcodeItem.setProductId(product.getId());
                    batchBarcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Batch);
                    batchBarcodeItem.setEntityId(productBatch.getId());
                    batchBarcodeItem.setBarcode(productSku);
                    barcodeItems.add(batchBarcodeItem);


                    // Deal with Photos
                    // Only get first photo
                    List<CompanyAsset> pAssets = convertAssets(companyid, meadowProduct);
                    product.setAssets(pAssets);
                    companyAssets.addAll(pAssets);


                    productBatches.add(productBatch);


                    // create the queue job
                    queuedTransactionList.add(createQueuedTransactionJobForBatchQuantity(companyid, shopId, userId, productBatch,
                            safeInventory.getId(), ProductBatchQueuedTransaction.OperationType.Add, inventoryAmt, null));
                }
            }
        }


        // Save Products
        productRepository.save(products);
        batchRepository.save(productBatches);
        barcodeItemRepository.save(barcodeItems);
        companyAssetRepository.save(companyAssets);
        brandRepository.save(newBrands);


        // Now create execute the queue transaction
        queuedTransactionRepository.save(queuedTransactionList);

        List<BatchActivityLog> batchActivityLogs = new ArrayList<>();

        productBatches.forEach(productBatch -> {
            BatchActivityLog activityLog = new BatchActivityLog();
            activityLog.prepare(companyid);
            activityLog.setShopId(shopId);
            activityLog.setTargetId(productBatch.getId());
            activityLog.setActivityType(BatchActivityLog.ActivityType.NORMAL_LOG);
            activityLog.setEmployeeId(userId);
            activityLog.setLog(productBatch.getSku() + " Batch imported");
            batchActivityLogs.add(activityLog);
        });

        batchActivityLogRepository.save(batchActivityLogs);

        for (QueuedTransaction queuedTransaction : queuedTransactionList) {
            backgroundJobService.addTransactionJob(companyid, shopId, queuedTransaction.getId());
        }
    }

    private List<CompanyAsset> convertAssets(String companyId, MeadowProduct meadowProduct) {
        List<CompanyAsset> companyAssets = new ArrayList<>();
        for (MeadowPhoto meadowPhoto : meadowProduct.getPhotos()) {
            CompanyAsset companyAsset = new CompanyAsset();
            companyAsset.prepare(companyId);
            companyAsset.setOrigURL(meadowPhoto.getFullPath());
            companyAsset.setSecured(false);
            companyAsset.setActive(true);
            companyAsset.setKey(UUID.randomUUID().toString().toLowerCase());
            companyAsset.setType(Asset.AssetType.Photo);
            if (meadowPhoto.getSizes().size() > 0) {
                String size640 = meadowPhoto.getSizes().get("640");


                // now calculate
                // small - 200
                // medium - 600
                // large = 800
                // large2 = 1600
                String small = size640.replaceAll("h_640", "h_200").replaceAll("w_640", "w_200");
                String medium = size640.replaceAll("h_640", "h_500").replaceAll("w_640", "w_500");
                String large = size640.replaceAll("h_640", "h_800").replaceAll("w_640", "w_800");
                String large2 = size640.replaceAll("h_640", "h_1600").replaceAll("w_640", "w_1600");

                companyAsset.setThumbURL(small);
                companyAsset.setMediumURL(medium);
                companyAsset.setLargeURL(large);
                companyAsset.setLargeX2URL(large2);
                companyAsset.setPublicURL(large);
            }
            companyAssets.add(companyAsset);
        }
        return companyAssets;
    }


    private String genBarcode(String companyName, String productId, String entityId, BarcodeItem.BarcodeEntityType entityType) {
        String substr = companyName.substring(0, 1);
        String idstr = productId.substring(entityId.length() - 3, productId.length()).toUpperCase();
        /// 3 digits name -  product-category -- product-type -- product name -- sku number
        /// 2 - 3 - 3 - 4
        /// AB-AB3-PRO-10
        String key = String.format("%s-%s", idstr, entityType.getCode()).toUpperCase();

        String sku = String.format("%s%s%s%d", substr, idstr, entityType.getCode(), 1);

        return sku.toUpperCase();
    }

    private List<ProductPriceBreak> getPriceBreaks(Product product) {
        final ProductPriceBreak.PriceBreakType[] priceBreakTypes = ProductPriceBreak.PriceBreakType.values();

        List<ProductPriceBreak> priceBreaks = new ArrayList<>();
        ProductPriceBreak prevPriceBreak = null;
        for (ProductPriceBreak.PriceBreakType type : priceBreakTypes) {
            if (type == ProductPriceBreak.PriceBreakType.None) {
                continue;
            }

            if (type == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                        || product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                    continue;
                }
            }


            ProductPriceBreak priceBreak = new ProductPriceBreak();
            priceBreak.prepare(product.getCompanyId());
            priceBreak.setPriceBreakType(type);
            priceBreak.setActive(false);

            // add price break to group
            priceBreaks.add(priceBreak);

            // specify name and quantity
            if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                priceBreak.setName(type.eachName);
                priceBreak.setQuantity(type.fullGramValue);
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                priceBreak.setQuantity(type.fullGramValue);
                priceBreak.setName(type.gramName);
            } else {
                priceBreak.setName(type.gramName);
                priceBreak.setQuantity(type.halfGramValue);
            }


            BigDecimal price = null;

            // set the price
            if (price == null || price.doubleValue() == 0) {
                if (prevPriceBreak != null) {
                    double unitCost = prevPriceBreak.getPrice().doubleValue() / prevPriceBreak.getQuantity();
                    priceBreak.setPrice(prevPriceBreak.getPrice().add(new BigDecimal(unitCost)));

                    if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                        int multiplier = priceBreak.getQuantity() - prevPriceBreak.getQuantity();

                        priceBreak.setPrice(prevPriceBreak.getPrice().add(new BigDecimal(unitCost * multiplier)));
                    }
                } else {
                    priceBreak.setPrice(product.getUnitPrice().multiply(new BigDecimal(priceBreak.getQuantity())));
                }
            } else {
                priceBreak.setPrice(price);
                priceBreak.setActive(true);
            }
            if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                    || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                    priceBreak.setPrice(product.getUnitPrice());
                    priceBreak.setActive(true);
                }
            } else {
                if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                    priceBreak.setPrice(product.getUnitPrice());
                    priceBreak.setActive(true);
                }
            }

            prevPriceBreak = priceBreak;

        }
        return priceBreaks;
    }


    private Vendor createOrGetVendor(String companyId, String vendorName, HashMap<String,Vendor> vendorHashMap) {
        Vendor vendor = vendorHashMap.get(vendorName);
        if (vendor == null) {
            vendor = new Vendor();
            vendor.prepare(companyId);
            vendor.setName(vendorName);
            vendor.setActive(true);
            vendor.setCompanyType(Vendor.CompanyType.DISTRIBUTOR);
            vendor.setVendorType(Vendor.VendorType.VENDOR);
            vendorHashMap.put(vendor.getName(),vendor);
            vendorRepository.save(vendor);
        }

        return vendor;
    }

    private ProductCategory createOrGetCategory(String companyId, String shopId,
                                                String categoryName, boolean cannabis,
                                                HashMap<String,ProductCategory> categoryHashMap) {
        ProductCategory category = categoryHashMap.get(categoryName);
        if (category == null) {
            category = new ProductCategory();
            category.prepare(companyId);
            category.setShopId(shopId);
            category.setName(categoryName);
            category.setActive(true);
            category.setUnitType(ProductCategory.UnitType.units);
            category.setCannabis(cannabis);
            categoryHashMap.put(category.getName(),category);
            productCategoryRepository.save(category);
        }

        return category;
    }


    /**
     * Create queue transaction job for add batch quantity.
     *
     * @param productBatch
     * @param inventoryId
     * @param operationType
     * @param quantity
     * @param sourceChildId
     * @return
     */
    private QueuedTransaction createQueuedTransactionJobForBatchQuantity(final String companyId,
                                                                         final String shopId,
                                                                         String userId, ProductBatch productBatch,
                                                                         String inventoryId,
                                                                         ProductBatchQueuedTransaction.OperationType operationType,
                                                                         BigDecimal quantity, String sourceChildId) {



        ProductBatchQueuedTransaction queuedTransaction = new ProductBatchQueuedTransaction();
        queuedTransaction.setShopId(shopId);
        queuedTransaction.setCompanyId(companyId);
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.ProductBatch);
        queuedTransaction.setSellerId(userId);
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(productBatch.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setInventoryId(inventoryId);
        queuedTransaction.setOperationType(operationType);
        queuedTransaction.setQuantity(quantity);

        queuedTransaction.setSubSourceAction(InventoryOperation.SubSourceAction.AddBatch);

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);
        queuedTransaction.setSourceChildId(sourceChildId);

        return queuedTransaction;
    }
}
