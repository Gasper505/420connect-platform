package com.fourtwenty.core.importer.meadow.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MeadowPhoto {
    private long id;
    private long productId;
    private String path;
    private String createdAt;
    private String cloudinaryId;
    private String format;
    private String fullPath;
    private String cloudinaryBase;
    private HashMap<String,String> sizes = new HashMap<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCloudinaryId() {
        return cloudinaryId;
    }

    public void setCloudinaryId(String cloudinaryId) {
        this.cloudinaryId = cloudinaryId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getCloudinaryBase() {
        return cloudinaryBase;
    }

    public void setCloudinaryBase(String cloudinaryBase) {
        this.cloudinaryBase = cloudinaryBase;
    }

    public HashMap<String, String> getSizes() {
        return sizes;
    }

    public void setSizes(HashMap<String, String> sizes) {
        this.sizes = sizes;
    }
}