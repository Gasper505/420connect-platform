package com.fourtwenty.core.importer.meadow.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeadowProduct {
    private long id;
    private long organizationId;
    private String name;
    private String unit;
    private String strainType;
    private String strainName = null;
    private String percentThc;
    private String percentCbd;
    private String description;
    private String createdAt;
    @JsonProperty("isActive")
    private boolean active;
    private boolean featured;
    private String archivedAt = null;
    private String leaflyCategory = null;
    private String vendor;
    private String inventoryType;
    private String unitPlural;
    private String defaultCostPerUnit = null;
    private String movingAverageCostPerUnit = null;
    ArrayList<MeadowProductOption> options = new ArrayList<MeadowProductOption>();
    ArrayList<Object> bulk = new ArrayList<Object>();
    ArrayList<MeadowPhoto> photos = new ArrayList<MeadowPhoto>();
    MeadowProductCategory primaryCategory;
    ArrayList<MeadowLocationInventory> locationInventory = new ArrayList<MeadowLocationInventory>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStrainType() {
        return strainType;
    }

    public void setStrainType(String strainType) {
        this.strainType = strainType;
    }

    public String getStrainName() {
        return strainName;
    }

    public void setStrainName(String strainName) {
        this.strainName = strainName;
    }

    public String getPercentThc() {
        return percentThc;
    }

    public void setPercentThc(String percentThc) {
        this.percentThc = percentThc;
    }

    public String getPercentCbd() {
        return percentCbd;
    }

    public void setPercentCbd(String percentCbd) {
        this.percentCbd = percentCbd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getArchivedAt() {
        return archivedAt;
    }

    public void setArchivedAt(String archivedAt) {
        this.archivedAt = archivedAt;
    }

    public String getLeaflyCategory() {
        return leaflyCategory;
    }

    public void setLeaflyCategory(String leaflyCategory) {
        this.leaflyCategory = leaflyCategory;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(String inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getUnitPlural() {
        return unitPlural;
    }

    public void setUnitPlural(String unitPlural) {
        this.unitPlural = unitPlural;
    }

    public String getDefaultCostPerUnit() {
        return defaultCostPerUnit;
    }

    public void setDefaultCostPerUnit(String defaultCostPerUnit) {
        this.defaultCostPerUnit = defaultCostPerUnit;
    }

    public String getMovingAverageCostPerUnit() {
        return movingAverageCostPerUnit;
    }

    public void setMovingAverageCostPerUnit(String movingAverageCostPerUnit) {
        this.movingAverageCostPerUnit = movingAverageCostPerUnit;
    }

    public ArrayList<MeadowProductOption> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<MeadowProductOption> options) {
        this.options = options;
    }

    public ArrayList<Object> getBulk() {
        return bulk;
    }

    public void setBulk(ArrayList<Object> bulk) {
        this.bulk = bulk;
    }

    public ArrayList<MeadowPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<MeadowPhoto> photos) {
        this.photos = photos;
    }

    public MeadowProductCategory getPrimaryCategory() {
        return primaryCategory;
    }

    public void setPrimaryCategory(MeadowProductCategory primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    public ArrayList<MeadowLocationInventory> getLocationInventory() {
        return locationInventory;
    }

    public void setLocationInventory(ArrayList<MeadowLocationInventory> locationInventory) {
        this.locationInventory = locationInventory;
    }
}