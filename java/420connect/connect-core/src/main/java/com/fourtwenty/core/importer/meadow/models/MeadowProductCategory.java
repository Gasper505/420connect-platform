package com.fourtwenty.core.importer.meadow.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeadowProductCategory {
    private long id;
    private String name;
    private long order;
    private String cannabisType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long order) {
        this.order = order;
    }

    public String getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(String cannabisType) {
        this.cannabisType = cannabisType;
    }
}