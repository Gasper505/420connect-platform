package com.fourtwenty.core.importer.meadow.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MeadowProductOption {
    private long id;
    private long productId;
    private String name;
    private float amount;
    private long price;
    private String createdAt;
    private String upc;
    private String content;
    private String defaultCostPerUnit;
    private String movingAverageCostPerUnit;
    ArrayList<MeadowLocationInventory> locationInventory = new ArrayList<MeadowLocationInventory>();
    ArrayList<Object> inventoryRules = new ArrayList<Object>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDefaultCostPerUnit() {
        return defaultCostPerUnit;
    }

    public void setDefaultCostPerUnit(String defaultCostPerUnit) {
        this.defaultCostPerUnit = defaultCostPerUnit;
    }

    public String getMovingAverageCostPerUnit() {
        return movingAverageCostPerUnit;
    }

    public void setMovingAverageCostPerUnit(String movingAverageCostPerUnit) {
        this.movingAverageCostPerUnit = movingAverageCostPerUnit;
    }

    public ArrayList<MeadowLocationInventory> getLocationInventory() {
        return locationInventory;
    }

    public void setLocationInventory(ArrayList<MeadowLocationInventory> locationInventory) {
        this.locationInventory = locationInventory;
    }

    public ArrayList<Object> getInventoryRules() {
        return inventoryRules;
    }

    public void setInventoryRules(ArrayList<Object> inventoryRules) {
        this.inventoryRules = inventoryRules;
    }
}