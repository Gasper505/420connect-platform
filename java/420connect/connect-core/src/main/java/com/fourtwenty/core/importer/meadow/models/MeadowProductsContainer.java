package com.fourtwenty.core.importer.meadow.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeadowProductsContainer {
    private List<MeadowProduct> data = new ArrayList<MeadowProduct>();


    public List<MeadowProduct> getData() {
        return data;
    }

    public void setData(List<MeadowProduct> data) {
        this.data = data;
    }
}
