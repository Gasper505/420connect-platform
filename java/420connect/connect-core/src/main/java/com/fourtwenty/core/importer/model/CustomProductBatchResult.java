package com.fourtwenty.core.importer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomProductBatchResult{

    private  ProductBatchResult productBatchResult;
    private Product product;
    private ProductCategory productCategory;

    public CustomProductBatchResult(ProductBatchResult productBatchResult, Product product, ProductCategory productCategory) {
        this.product = product;
        this.productBatchResult = productBatchResult;
        this.productCategory = productCategory;
    }

    public ProductBatchResult getProductBatchResult() {
        return productBatchResult;
    }

    public void setProductBatchResult(ProductBatchResult productBatchResult) {
        this.productBatchResult = productBatchResult;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
