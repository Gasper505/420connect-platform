package com.fourtwenty.core.importer.model;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.importer.main.Importable;

/**
 * Created by mdo on 3/24/16.
 */
public class CustomerContainer implements Importable {
    private Member member;
    private Long dateJoined;
    private String patientGroup;
    private String memberNote;

    public String getMemberNote() {
        return memberNote;
    }

    public void setMemberNote(String memberNote) {
        this.memberNote = memberNote;
    }

    public String getPatientGroup() {
        return patientGroup;
    }

    public void setPatientGroup(String patientGroup) {
        this.patientGroup = patientGroup;
    }

    @Override
    public String getImportId() {
        return null;
    }

    public Long getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Long dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
