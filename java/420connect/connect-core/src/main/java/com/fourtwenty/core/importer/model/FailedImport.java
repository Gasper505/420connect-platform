package com.fourtwenty.core.importer.model;

import com.fourtwenty.core.importer.main.Importable;

/**
 * Created by Stephen Schmidt on 1/14/2016.
 */
public class FailedImport {
    private Importable entity;
    private String reason;

    public FailedImport(Importable request, String reason) {
        this.entity = request;
        this.reason = reason;
    }

    public Importable getEntity() {
        return entity;
    }

    public void setEntity(Importable entity) {
        this.entity = entity;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
