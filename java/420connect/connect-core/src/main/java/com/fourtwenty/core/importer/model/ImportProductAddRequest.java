package com.fourtwenty.core.importer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;

import java.math.BigDecimal;

/**
 * Created by mdo on 3/24/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportProductAddRequest extends ProductAddRequest {
    private BigDecimal inventoryAvailable = new BigDecimal(0);
    private Float percentTHC;
    private Float percentCBD;
    private Float percentCBN;
    private long datePurchased;
    private String unitType;

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public long getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(long datePurchased) {
        this.datePurchased = datePurchased;
    }

    public BigDecimal getInventoryAvailable() {
        return inventoryAvailable;
    }

    public void setInventoryAvailable(BigDecimal inventoryAvailable) {
        this.inventoryAvailable = inventoryAvailable;
    }

    public Float getPercentCBD() {
        return percentCBD;
    }

    public void setPercentCBD(Float percentCBD) {
        this.percentCBD = percentCBD;
    }

    public Float getPercentCBN() {
        return percentCBN;
    }

    public void setPercentCBN(Float percentCBN) {
        this.percentCBN = percentCBN;
    }

    public Float getPercentTHC() {
        return percentTHC;
    }

    public void setPercentTHC(Float percentTHC) {
        this.percentTHC = percentTHC;
    }
}
