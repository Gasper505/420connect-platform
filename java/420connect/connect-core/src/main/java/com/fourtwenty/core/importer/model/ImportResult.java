package com.fourtwenty.core.importer.model;

import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.importer.main.Parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephen Schmidt on 1/10/2016.
 */
public class ImportResult<T extends Importable> {
    private Parser.ParseDataType dataType;
    private int successes, failures, updates;
    private List<FailedImport> failedImports = new ArrayList<FailedImport>();
    private List<T> successfulImports = new ArrayList<T>();
    private List<T> successfulUpdates = new ArrayList<>();

    public ImportResult(Parser.ParseDataType dataType) {
        this.dataType = dataType;
        successes = 0;
        failures = 0;
        updates = 0;
    }


    public void addSuccess(T record) {
        successfulImports.add(record);
        successes++;
    }

    public void addFailed(T record, String reason) {
        failedImports.add(new FailedImport(record, reason));
        failures++;
    }

    public void addUpdated(T record) {
        successfulUpdates.add(record);
        updates++;
    }

    public Parser.ParseDataType getDataType() {
        return dataType;
    }

    public void setDataType(Parser.ParseDataType dataType) {
        this.dataType = dataType;
    }

    public boolean isSuccess() {
        return failedImports.isEmpty();
    }

    public List<FailedImport> getFailedImports() {
        return failedImports;
    }

    public void setFailedImports(List<FailedImport> failedImports) {
        this.failedImports = failedImports;
    }

    public List<T> getSuccessfulImports() {
        return successfulImports;
    }

    public void setSuccessfulImports(List<T> successfulImports) {
        this.successfulImports = successfulImports;
    }

    public int getSuccesses() {
        return successes;
    }

    public int getFailures() {
        return failures;
    }

    public int getUpdates() {
        return updates;
    }
}
