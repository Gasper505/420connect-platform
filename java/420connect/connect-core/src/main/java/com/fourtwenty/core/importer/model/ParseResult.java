package com.fourtwenty.core.importer.model;

import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.importer.main.Parser;

/**
 * Created by Stephen Schmidt on 1/26/2016.
 */
public class ParseResult<T extends Importable> extends ImportResult<T> {

    public ParseResult(Parser.ParseDataType dataType) {
        super(dataType);
    }
}
