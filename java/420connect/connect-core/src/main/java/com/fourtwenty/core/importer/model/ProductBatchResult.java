package com.fourtwenty.core.importer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.BatchActivityLog;
import com.fourtwenty.core.domain.models.product.DerivedProductBatchLog;
import com.fourtwenty.core.domain.models.product.PotencyMG;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductBatchLabel;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.importer.main.Importable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatchResult extends ProductBatch implements Importable {

    private String productName;
    private String batchSKU;
    private long batchDate;
    private BigDecimal costPerUnit;
    private BigDecimal quantity;

    private double thc;
    private double cbn;
    private double cbd;
    private double cbda;
    private double thca;

    //<InventoryId, Quantity>
    private HashMap<String, BigDecimal> inventoryQuantity = new HashMap<>();

    private PotencyMG potencyAmount;

    private String brandName;
    private String vendorName;
    private String productType;
    private ProductBatchLabel productBatchLabel;
    private Map<String, BigDecimal> batchQuantityMap = new HashMap<>();

    private List<BatchActivityLog> batchActivityLog;
    private DerivedProductBatchLog derivedBatchLog;
    private List<TestSample> testSamples = new ArrayList<>();

    private String trackPackageLabel;

    @Override
    public String getTrackPackageLabel() {
        return trackPackageLabel;
    }

    @Override
    public void setTrackPackageLabel(String trackPackageLabel) {
        this.trackPackageLabel = trackPackageLabel;
    }

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public double getCbd() {
        return cbd;
    }

    public void setCbd(double cbd) {
        this.cbd = cbd;
    }

    public double getCbda() {
        return cbda;
    }

    public void setCbda(double cbda) {
        this.cbda = cbda;
    }

    public double getCbn() {
        return cbn;
    }

    public void setCbn(double cbn) {
        this.cbn = cbn;
    }

    public double getThc() {
        return thc;
    }

    public void setThc(double thc) {
        this.thc = thc;
    }

    public double getThca() {
        return thca;
    }

    public void setThca(double thca) {
        this.thca = thca;
    }

    public String getBatchSKU() {
        return batchSKU;
    }

    public void setBatchSKU(String batchSKU) {
        this.batchSKU = batchSKU;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getBatchDate() {
        return batchDate;
    }

    public void setBatchDate(long batchDate) {
        this.batchDate = batchDate;
    }


    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public HashMap<String, BigDecimal> getInventoryQuantity() {
        return inventoryQuantity;
    }

    public void setInventoryQuantity(HashMap<String, BigDecimal> inventoryQuantity) {
        this.inventoryQuantity = inventoryQuantity;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    @Override
    public String getImportId() {
        return this.getProductId();
    }

    @Override
    public ProductBatchLabel getProductBatchLabel() {
        return productBatchLabel;
    }

    @Override
    public void setProductBatchLabel(ProductBatchLabel productBatchLabel) {
        this.productBatchLabel = productBatchLabel;
    }

    public PotencyMG getPotencyAmount() {
        return potencyAmount;
    }

    public void setPotencyAmount(PotencyMG potencyAmount) {
        this.potencyAmount = potencyAmount;
    }

    public List<BatchActivityLog> getBatchActivityLog() {
        return batchActivityLog;
    }

    public void setBatchActivityLog(List<BatchActivityLog> batchActivityLog) {
        this.batchActivityLog = batchActivityLog;
    }

    public Map<String, BigDecimal> getBatchQuantityMap() {
        return batchQuantityMap;
    }

    public void setBatchQuantityMap(Map<String, BigDecimal> batchQuantityMap) {
        this.batchQuantityMap = batchQuantityMap;
    }

    public DerivedProductBatchLog getDerivedBatchLog() {
        return derivedBatchLog;
    }

    public void setDerivedBatchLog(DerivedProductBatchLog derivedBatchLog) {
        this.derivedBatchLog = derivedBatchLog;
    }

    public List<TestSample> getTestSamples() {
        return testSamples;
    }

    public void setTestSamples(List<TestSample> testSamples) {
        this.testSamples = testSamples;
    }
}
