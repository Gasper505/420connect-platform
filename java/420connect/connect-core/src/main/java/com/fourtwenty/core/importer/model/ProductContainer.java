package com.fourtwenty.core.importer.model;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.importer.main.Importable;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by mdo on 3/24/16.
 */
public class ProductContainer implements Importable {
    private String productId;
    private Product product;
    private BigDecimal inventoryAvailable = new BigDecimal(0);
    private Float percentTHC;
    private Float percentCBD;
    private Float percentCBN;
    private long datePurchased;
    private String image1;
    private String image2;
    private String image3;
    private String image4;
    private String image5;
    private String categoryName;
    private String unitType;
    private String vendorImportId;
    private BigDecimal costPerUnit = new BigDecimal(0);

    private HashMap<String, BigDecimal> inventoryQuantities = new HashMap<>();
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }


    public HashMap<String, BigDecimal> getInventoryQuantities() {
        return inventoryQuantities;
    }

    public void setInventoryQuantities(HashMap<String, BigDecimal> inventoryQuantities) {
        this.inventoryQuantities = inventoryQuantities;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVendorImportId() {
        return vendorImportId;
    }

    public void setVendorImportId(String vendorImportId) {
        this.vendorImportId = vendorImportId;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public long getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(long datePurchased) {
        this.datePurchased = datePurchased;
    }

    @Override
    public String getImportId() {
        return null;
    }

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public BigDecimal getInventoryAvailable() {
        return inventoryAvailable;
    }

    public void setInventoryAvailable(BigDecimal inventoryAvailable) {
        this.inventoryAvailable = inventoryAvailable;
    }

    public Float getPercentCBD() {
        if (percentCBD == null) return 0f;
        return percentCBD;
    }

    public void setPercentCBD(Float percentCBD) {
        this.percentCBD = percentCBD;
    }

    public Float getPercentCBN() {
        if (percentCBN == null) return 0f;
        return percentCBN;
    }

    public void setPercentCBN(Float percentCBN) {
        this.percentCBN = percentCBN;
    }

    public Float getPercentTHC() {
        if (percentTHC == null) return 0f;
        return percentTHC;
    }

    public void setPercentTHC(Float percentTHC) {
        this.percentTHC = percentTHC;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }


}
