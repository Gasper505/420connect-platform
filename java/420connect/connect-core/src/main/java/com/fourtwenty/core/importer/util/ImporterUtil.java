package com.fourtwenty.core.importer.util;

import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Stephen Schmidt on 1/10/2016.
 */
public class ImporterUtil {

    public static boolean parseBoolean(String input) {
        String s = input.toLowerCase();
        return ("yes".equalsIgnoreCase(s) || "true".equalsIgnoreCase(s) || "y".equalsIgnoreCase(s)) ? true : false;
    }


    public static Float getPercentValue(String percentString) {
        if (percentString == null || percentString.trim().isEmpty())
            return null;
        int l = percentString.length() - 1;
        try {
            if (percentString.charAt(l) == '%') {
                return Float.parseFloat(percentString.substring(0, l));
            } else {
                return Float.parseFloat(percentString);
            }
        } catch (NumberFormatException e) {
            return -1f;
        }
    }

    public static Float getDollarValue(String dollarString) {
        if (dollarString == null || dollarString.trim().isEmpty()) {
            return null;
        }
        try {
            dollarString = dollarString.replaceAll(",","");
            if (dollarString.charAt(0) == '$') {
                return Float.parseFloat(dollarString.substring(1));
            } else {
                return Float.parseFloat(dollarString);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0f;
        }
    }

    public static double getDecimalValue(String decimalString) {
        if (decimalString == null || decimalString.trim().isEmpty()) {
            return 0d;
        }
        try {
            decimalString = decimalString.replaceAll(",","");
            if (decimalString.charAt(0) == '$') {
                return Double.parseDouble(decimalString.substring(1));
            } else {
                return Double.parseDouble(decimalString);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0d;
        }
    }

    public static BigDecimal getDollarAmountDecimal(String dollarString) {
        Float value = getDollarValue(dollarString);
        if (value == null) {
            value = 0f;
        }
        return BigDecimal.valueOf(value);
    }

    public static Long getDate(String dateString) throws IllegalArgumentException {
        if (dateString == null || dateString.trim().isEmpty()) {
            return null;
        }

        String[] parts = dateString.split("/");
        StringBuilder sb = new StringBuilder();
        if (parts.length == 3) {
            if (parts[0].length() < 2) {
                sb.append("0");
                sb.append(parts[0]);
            } else {
                sb.append(parts[0]);
            }
            sb.append("/");

            if (parts[1].length() < 2) {
                sb.append("0");
                sb.append(parts[1]);
            } else {
                sb.append(parts[1]);
            }
            sb.append("/");

            if (parts[2].length() == 2) {
                // Check date
                try {
                    int year = Integer.valueOf(parts[2]);
                    if (year > 30) {
                        sb.append("19");
                    } else {
                        sb.append("20");
                    }
                } catch (Exception e) {

                    sb.append("20");
                }
                sb.append(parts[2]);
            } else {
                sb.append(parts[2]);
            }
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        Long date = formatter.parseDateTime(sb.toString()).getMillis();
        return date;
    }

    public static Long getDate(String dateString, String timeZone) throws IllegalArgumentException {
        if (dateString == null || dateString.trim().isEmpty()) {
            return null;
        }

        String[] parts = dateString.split("/");
        StringBuilder sb = new StringBuilder();
        if (parts.length == 3) {
            if (parts[0].length() < 2) {
                sb.append("0");
                sb.append(parts[0]);
            } else {
                sb.append(parts[0]);
            }
            sb.append("/");

            if (parts[1].length() < 2) {
                sb.append("0");
                sb.append(parts[1]);
            } else {
                sb.append(parts[1]);
            }
            sb.append("/");

            if (parts[2].length() == 2) {
                // Check date
                try {
                    int year = Integer.valueOf(parts[2]);
                    if (year > 30) {
                        sb.append("19");
                    } else {
                        sb.append("20");
                    }
                } catch (Exception e) {

                    sb.append("20");
                }
                sb.append(parts[2]);
            } else {
                sb.append(parts[2]);
            }
        }

        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            sdf.setTimeZone(TimeZone.getTimeZone(name));
            Date newDate = sdf.parse(sb.toString());

            long date = newDate.getTime();
            return date;
            /*DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
            formatter.set
            Long date = formatter.parseDateTime(sb.toString()).getMillis();


            DateTimeZone tz = DateTimeZone.forID(name);
            DateTime jodatime = new DateTime(date, tz);
            return jodatime.getMillis();*/
        } catch (Exception e) {
            return null;
        }
    }

    public static Identification.IdentificationType getIdType(String idType) {
        return Identification.IdentificationType.getTypeByValue(idType);
    }
}
