package com.fourtwenty.core.jobs;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CompanyLicenseExpirationNotification {

    static final Logger LOGGER = LoggerFactory.getLogger(CompanyLicenseExpirationNotification.class);

    private static final String DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String FILE_HEADERS = "S.No., Vendor Name, License No., Company Type, License Type, Licence Expiration Date";
    private static final String emailTitle = "Company License Expiration Notification";
    private static final String fileName = "Vendors (Company License Expiration).csv";

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private NotificationInfoRepository notificationInfoRepository;

    public void validateCompanyLicence() {

        HashMap<String, Company> companyMap = companyRepository.listNonDeletedActiveAsMap();
        HashMap<String, Shop> shopMap = shopRepository.listNonDeletedActiveAsMap();
        HashMap<String, NotificationInfo> notificationInfoHashMap = notificationInfoRepository.listByShopAndTypeAsMap(NotificationInfo.NotificationType.Company_Licence_Expiration);


        Map<String, StringBuilder> visitedCompanyMap = new HashMap<>();
        Set<String> visitedShopIds = new HashSet<>();
        for (Map.Entry<String, Shop> entry : shopMap.entrySet()) {
            Shop shop = entry.getValue();

            //skip the shop if not active
            if (!shop.isActive() || CompanyFeatures.AppTarget.Distribution != shop.getAppTarget()) {
                continue;
            }

            NotificationInfo notificationInfo = notificationInfoHashMap.get(shop.getId());

            //If notification info isn't available for the shop
            if (notificationInfo != null && !notificationInfo.isActive()) {
                continue;
            }

            String emailBody = this.prepareEmailBody(shop, companyMap.get(shop.getCompanyId()));

            if (visitedCompanyMap.containsKey(shop.getCompanyId()) && visitedCompanyMap.get(shop.getCompanyId()) != null) {
                this.sendMail(shop, visitedCompanyMap, visitedCompanyMap.get(shop.getCompanyId()), emailBody);
                continue;
            }

            if (notificationInfo != null) {
                visitedShopIds.add(shop.getId());
            }

            Iterable<Vendor> vendors = vendorRepository.list(shop.getCompanyId());

            DateTime currentTime = DateUtil.nowUTC().withTimeAtStartOfDay();
            String timeZone = DateTimeZone.UTC.toString();

            if (StringUtils.isNotBlank(shop.getTimeZone())) {
                timeZone = shop.getTimeZone();
                currentTime = DateUtil.nowWithTimeZone(timeZone).withTimeAtStartOfDay();
            }

            DateTime sevenDaysTime = currentTime.plusDays(7).withTimeAtStartOfDay();

            Map<String, Object> vendorDataMap = this.prepareVendorList(vendors, currentTime.getMillis(), sevenDaysTime, notificationInfo, timeZone);

            Boolean dataAvailable = (Boolean) vendorDataMap.get("dataAvailable");

            //If vendor data is available then only send email
            if (dataAvailable) {
                StringBuilder data = (StringBuilder) vendorDataMap.get("data");
                if (StringUtils.isNotBlank(data)) {
                    StringBuilder str = new StringBuilder(FILE_HEADERS);
                    data = str.append(NEW_LINE_SEPARATOR).append(data);
                    this.sendMail(shop, visitedCompanyMap, data, emailBody);
                }
            }

        }

        if (!visitedShopIds.isEmpty()) {
            notificationInfoRepository.updateLastSentTime(Lists.newArrayList(visitedShopIds), DateTime.now().getMillis(), NotificationInfo.NotificationType.Company_Licence_Expiration);
        }

    }

    private void sendMail(Shop shop, Map<String, StringBuilder> visitedCompanyMap, StringBuilder data, String emailBody) {

        visitedCompanyMap.put(shop.getCompanyId(), data);

        byte[] bytes = data.toString().getBytes();
        InputStream stream = new ByteArrayInputStream(bytes);

        HashMap<String, InputStream> attachmentData = new HashMap<>();
        attachmentData.put(fileName, stream);

        HashMap<String, HashMap<String, InputStream>> attachmentMap = new HashMap<>();
        attachmentMap.put("attachment", attachmentData);

        amazonServiceManager.sendMultiPartEmail("support@blaze.me", shop.getEmailAdress(), emailTitle, emailBody, null, null, attachmentMap, "Content-Disposition", "", "");
    }

    private Map<String, Object> prepareVendorList(Iterable<Vendor> vendors, long currentTime, DateTime sevenDaysTime, NotificationInfo notificationInfo, String timeZone) {

        Map<String, Object> vendorDataMap = new HashMap<>();

        StringBuilder builder = new StringBuilder();

        int sNo = 0;
        DateTime expirationDate;
        for (Vendor vendor : vendors) {
            if (!CollectionUtils.isEmpty(vendor.getCompanyLicenses())) {
                for (CompanyLicense companyLicense : vendor.getCompanyLicenses()) {
                    expirationDate = getLicenseExpirationDate(companyLicense.getLicenseExpirationDate(), timeZone);
                    sNo = prepareCompanyLicense(builder, sNo, vendor, companyLicense.getLicenseNumber(), companyLicense.getLicenseType(), companyLicense.getCompanyType(), expirationDate, sevenDaysTime, currentTime, notificationInfo);
                }
            } else {
                expirationDate = getLicenseExpirationDate(vendor.getLicenseExpirationDate(), timeZone);
                sNo = prepareCompanyLicense(builder, sNo, vendor, vendor.getLicenseNumber(), vendor.getLicenceType(), vendor.getCompanyType(), expirationDate, sevenDaysTime, currentTime, notificationInfo);
            }
        }

        vendorDataMap.put("dataAvailable", Boolean.FALSE);
        if (sNo > 0) {
            vendorDataMap.put("dataAvailable", Boolean.TRUE);
            vendorDataMap.put("data", builder);
        }

        return vendorDataMap;
    }

    private String prepareEmailBody(Shop shop, Company company) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/companyLicenseNotification.html");

        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        String emailBody = writer.toString();

        String logoURL = company != null && company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";

        emailBody = emailBody.replaceAll("==logo==", logoURL);
        emailBody = emailBody.replaceAll("==preferredEmailColor==", company != null && StringUtils.isNotBlank(company.getPreferredEmailColor()) ? company.getPreferredEmailColor() : "#1cc4e8 !important");
        emailBody = emailBody.replaceAll("==shopName==", shop.getName());

        return emailBody;
    }

    private DateTime getLicenseExpirationDate(long date, String timeZone) {
        return DateUtil.toDateTime(date, timeZone).withTimeAtStartOfDay();
    }

    private int prepareCompanyLicense(StringBuilder builder, int sNo, Vendor vendor, String licenseNo, Vendor.LicenceType licenceType, Vendor.CompanyType companyType, DateTime expirationDate, DateTime sevenDaysTime, long currentTime, NotificationInfo notificationInfo) {
        if (Days.daysBetween(expirationDate, sevenDaysTime).getDays() == 0 ||
                (notificationInfo != null && notificationInfo.getLastSent() == 0 && expirationDate.getMillis() < currentTime)) {
            builder.append(++sNo).append(DELIMITER);
            builder.append(vendor.getName()).append(DELIMITER);
            builder.append(TextUtil.textOrEmpty(licenseNo)).append(DELIMITER);
            builder.append(TextUtil.textOrEmpty(companyType == null ? "-" : companyType.toString())).append(DELIMITER);
            builder.append(TextUtil.textOrEmpty(licenceType == null ? "-" : licenceType.toString())).append(DELIMITER);
            builder.append(TextUtil.toDate(expirationDate.getMillis())).append(DELIMITER);

            builder.append(NEW_LINE_SEPARATOR);
        }

        return sNo;
    }
}
