package com.fourtwenty.core.jobs;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.SwitchableApplication;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class ConsumerOrderNotification {

    static final Logger LOGGER = LoggerFactory.getLogger(ConsumerOrderNotification.class);
    private static final String ONLINE_WIDGET_SOURCE = "OnlineWidget";

    @Inject
    private NotificationInfoRepository notificationInfoRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private ConnectConfiguration configuration;

    /**
     * Method to process new incoming orders and send notifications to provided email address and phone number
     *
     * @param status   : status
     * @param shopId   : shop id
     * @param timeZone : timezone
     */
    public void processConsumerOrderNotification(boolean status, String shopId, String timeZone) {
        HashMap<String, NotificationInfo> notificationInfoMap = notificationInfoRepository.listByShopAndTypeAsMap(NotificationInfo.NotificationType.Incoming_Order);

        HashMap<String, Shop> shopHashMap = new HashMap<>();

        if (StringUtils.isNotBlank(shopId)) {
            Shop shop = shopRepository.getById(shopId);
            shopHashMap.put(shop.getId(), shop);
        } else {
            shopHashMap = shopRepository.listNonDeletedAsMap();
        }

        List<ConsumerCart> consumerOrders = getConsumerOrders(status, timeZone, Lists.newArrayList(shopHashMap.values()), notificationInfoMap);

        Set<ObjectId> consumerUserIds = new HashSet<>();
        Set<String> updateShopIds = new HashSet<>();

        for (ConsumerCart cart : consumerOrders) {
            if (StringUtils.isNotBlank(cart.getConsumerId()) && ObjectId.isValid(cart.getConsumerId())) {
                consumerUserIds.add(new ObjectId(cart.getConsumerId()));
            }
        }

        HashMap<String, ConsumerUser> consumerUserMap = consumerUserRepository.listAsMap(Lists.newArrayList(consumerUserIds));

        HashMap<String, StringBuilder> shopEmails = new HashMap<>();
        HashMap<String, Integer> shopOrderCount = new HashMap<>();


        prepareNotifications(consumerOrders, consumerUserMap, shopHashMap, shopOrderCount, shopEmails);

        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/consumerOrderNotification.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder emailStream = new StringBuilder(writer.toString());

        if (StringUtils.isBlank(emailStream)) {
            LOGGER.info("No resource found for consumer notification");
            return;
        }

        List<String> notificationPhones;
        List<String> notificationEmails;

        for (String key : shopEmails.keySet()) {
            Shop shop = shopHashMap.get(key);

            if (shop == null) {
                continue;

            }
            NotificationInfo notificationInfo = notificationInfoMap.get(shop.getId());

            if (notificationInfo == null || !notificationInfo.isActive()) {
                continue;
            }

            notificationEmails = notificationPhones = new ArrayList<>();

            if (notificationInfo != null && StringUtils.isNotBlank(notificationInfo.getDefaultNumber())) {
                notificationPhones = Arrays.asList(notificationInfo.getDefaultNumber().split("\\s*,\\s*"));
            }

            if (notificationInfo != null && StringUtils.isNotBlank(notificationInfo.getDefaultEmail())) {
                notificationEmails = Arrays.asList(notificationInfo.getDefaultEmail().split("\\s*,\\s*"));
            }

            if (shopOrderCount.containsKey(shop.getId())) {
                int orderCount = shopOrderCount.get(shop.getId());
                if (orderCount == 0) {
                    continue;
                }
                int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(StringUtils.isBlank(timeZone) ? shop.getTimeZone() : timeZone);
                DateTime dateTime = DateUtil.nowUTC().withTimeAtStartOfDay().minusMinutes(timeZoneOffset);

                String message = "You have " + orderCount + "new orders for " + DateUtil.toDateTimeFormatted(dateTime.getMillis(), (StringUtils.isBlank(timeZone) ? shop.getTimeZone() : timeZone));

                LOGGER.info("New orders found for shop : " + shop.getName() + " , order count : " + orderCount + " , To number : " + notificationPhones);

                amazonServiceManager.sendSMSMessage(notificationPhones, message, shop);

                updateShopIds.add(shop.getId());
            }

            if (shopEmails.containsKey(shop.getId())) {
                String emailBody = emailStream.toString();
                emailBody = emailBody.replaceAll(Pattern.quote("{{orderBody}}"), shopEmails.get(shop.getId()).toString());

                LOGGER.info("Sending notification mail for shop : " + shop.getName() + " , To Mail Address : " + notificationEmails);

                int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(StringUtils.isBlank(timeZone) ? shop.getTimeZone() : timeZone);
                DateTime dateTime = DateUtil.nowUTC().withTimeAtStartOfDay().minusMinutes(timeZoneOffset);

                emailBody = emailBody.replaceAll(Pattern.quote("{{notificationDate}}"), DateUtil.toDateFormatted(dateTime.getMillis(), shop.getTimeZone()));

                for (String email : notificationEmails) {
                    amazonServiceManager.sendEmail("support@blaze.me", email, "Consumer Order Notification", emailBody, null, null, "Blaze Support");
                }

                updateShopIds.add(shop.getId());
            }
        }

        if (status && !updateShopIds.isEmpty()) {
            notificationInfoRepository.updateLastSentTime(Lists.newArrayList(updateShopIds), DateTime.now().getMillis(), NotificationInfo.NotificationType.Incoming_Order);
        }

    }

    /**
     * Private method to prepare notifications
     *
     * @param consumerCarts   : list of new incoming orders
     * @param consumerUserMap : consumer user hash map
     * @param shopHashMap     : shop hash map
     * @param shopOrderCount  : order count by shop map
     * @param shopEmails      : order details by shop
     */
    private void prepareNotifications(List<ConsumerCart> consumerCarts, HashMap<String, ConsumerUser> consumerUserMap, HashMap<String, Shop> shopHashMap, HashMap<String, Integer> shopOrderCount, HashMap<String, StringBuilder> shopEmails) {

        for (ConsumerCart consumerCart : consumerCarts) {
            ConsumerUser consumerUser = consumerUserMap.get(consumerCart.getConsumerId());
            Shop shop = shopHashMap.get(consumerCart.getShopId());

            if (consumerUser == null || shop == null) {
                continue;
            }

            int orderCount = shopOrderCount.getOrDefault(consumerCart.getShopId(), 0);
            StringBuilder emailBody = shopEmails.getOrDefault(consumerCart.getShopId(), new StringBuilder());

            if (ONLINE_WIDGET_SOURCE.equalsIgnoreCase(consumerCart.getSource()) && consumerUser.getNotificationType() != ConsumerUser.ConsumerOrderNotificationType.None) {
                orderCount++; //count consumer orders for notification sms.

                boolean isMember = false;
                if (consumerUser.isAccepted()) {
                    isMember = Boolean.TRUE;
                }
                /*for (ConsumerMemberStatus memberStatus : consumerUser.getMemberStatuses()) {
                    if (shop.getId().equalsIgnoreCase(memberStatus.getShopId()) && memberStatus.isAccepted()) {
                        isMember = true;
                        break;
                    }
                }*/

                emailBody.append(prepareOrdersRow(consumerCart, consumerUser, shop, isMember));

                shopEmails.put(shop.getId(), emailBody);
                shopOrderCount.put(shop.getId(), orderCount);

            }

        }
    }

    /**
     * Private method to prepare incoming order rows
     *
     * @param consumerCart : consumer cart
     * @param consumerUser : consumer user
     * @param shop         : shop
     * @param isNewMember  : is new member
     * @return : returns order detail row for mail
     */
    private String prepareOrdersRow(ConsumerCart consumerCart, ConsumerUser consumerUser, Shop shop, boolean isNewMember) {

        StringBuilder consumerName = new StringBuilder();
        consumerName.append(consumerUser.getFirstName()).append(StringUtils.isNotBlank(consumerUser.getLastName()) ? " " + consumerUser.getLastName() : "");

        SwitchableApplication applicationConfig = configuration.getBlazeApplicationsConfig().getRedirectURL("Retail");
        String actionUrl = ((applicationConfig != null) ? applicationConfig.getAppLink() : "") + "/pos?activeTab=incomingOrders&shopId=" + shop.getId();

        return "<tr>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + consumerName + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + (StringUtils.isNotBlank(consumerUser.getPrimaryPhone()) ? consumerUser.getPrimaryPhone() : "-") + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + (consumerUser.getAddress() != null ? consumerUser.getAddress().getAddressString() : "-") + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + consumerUser.getConsumerType().getDisplayName() + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + consumerCart.getCartStatus().name() + "</td>\n" +
                "<td width=\"180\" height=\"30\" style=\"padding:3px 3px;\">" + consumerCart.getOrderNo() + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + DateUtil.toDateTimeFormatted(consumerCart.getOrderPlacedTime(), shop.getTimeZone()) + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + (consumerCart.getCart() != null ? TextUtil.toEscapeCurrency(consumerCart.getCart().getTotal().doubleValue(), shop.getDefaultCountry()) : "-") + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + consumerCart.getSource() + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + (isNewMember ? Member.MembershipStatus.Pending : Member.MembershipStatus.Active) + "</td>\n" +
                "<td width=\"250\" height=\"30\"><div><a href='" + actionUrl + "' class='button button--green'>Review Order</a></div></td>\n" +
                "</tr>";
    }

    /**
     * Private method to get list of all new incoming orders for shops if notification info is enabled
     *
     * @param status                  : status
     * @param requestTimeZone         : request timezone
     * @param shops                   : shops
     * @param notificationInfoHashMap : notification info map
     * @return : returns list of consumer carts
     */
    private List<ConsumerCart> getConsumerOrders(boolean status, String requestTimeZone, List<Shop> shops, HashMap<String, NotificationInfo> notificationInfoHashMap) {
        List<ConsumerCart> consumerCarts = new ArrayList<>();

        for (Shop shop : shops) {

            DateTime startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
            DateTime endDateTime = DateUtil.nowUTC();

            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(StringUtils.isBlank(requestTimeZone) ? shop.getTimeZone() : requestTimeZone);

            long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

            NotificationInfo notificationInfo = notificationInfoHashMap.get(shop.getId());
            if (notificationInfo != null && !notificationInfo.isActive()) {
                continue;
            }

            if (status && notificationInfo != null) {
                /*
                  run only when call from quartz job
                  or previously send
                */
                DateTime lastSendTime = new DateTime(notificationInfo.getLastSent());
                if (lastSendTime.plusDays(1).isAfter(timeZoneStartDateMillis)) {
                    continue;
                }
                timeZoneStartDateMillis = lastSendTime.getMillis();
            }

            consumerCarts.addAll(Lists.newArrayList(consumerCartRepository.getBracketConsumerCartByStatus(shop.getCompanyId(), shop.getId(), ConsumerCart.ConsumerCartStatus.Placed, timeZoneStartDateMillis, timeZoneEndDateMillis)));
        }

        return consumerCarts;
    }

}
