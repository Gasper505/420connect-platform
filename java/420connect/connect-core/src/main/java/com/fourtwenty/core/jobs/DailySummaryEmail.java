package com.fourtwenty.core.jobs;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.reporting.gather.impl.transaction.DailySummaryGatherer;
import com.fourtwenty.core.reporting.model.DailySummary;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;

public class DailySummaryEmail {
    static final Logger LOGGER = LoggerFactory.getLogger(DailySummaryEmail.class);

    private static final String TOTAL_INVOICED = "totalInvoiced";
    private static final String SALES = "sales";
    private static final String REFUNDS = "refunds";
    private static final String Count = "count";
    @Inject
    DailySummaryGatherer dailySummaryGatherer;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private NotificationInfoRepository notificationInfoRepository;
    @Inject
    private CompanyRepository companyRepository;

    public DailySummaryEmail() {

    }

    public void dataForDailySummary(boolean backgroundJob, List<String> requestEmail, String requestShopId, String timeZone, long summaryDate) {
        HashMap<String, Shop> shopHashMap = new HashMap<>();

        if (StringUtils.isBlank(requestShopId)) {
            shopHashMap = shopRepository.listNonDeletedActiveAsMap();
        } else {
            Shop shop = shopRepository.getById(requestShopId);
            shopHashMap.put(requestShopId, shop);
        }
        HashMap<String, Company> companyHashMap = companyRepository.listAsMap();


        HashMap<String, NotificationInfo> notificationInfoHashMap = notificationInfoRepository.listByShopAndTypeAsMap(NotificationInfo.NotificationType.Daily_Summary);
        Set<String> updateShopIds = new HashSet<>();

        List<String> toEmails = new ArrayList<>();

        for (String shopId : shopHashMap.keySet()) {

            Shop shop = shopHashMap.get(shopId);
            if (shop == null || !shop.isActive()) {
                continue;
            }

            if (StringUtils.isBlank(timeZone)) {
                timeZone = shop.getTimeZone();
            }

            Company company = companyHashMap.get(shop.getCompanyId());
            if (company == null || company.isActive() == false || company.isDeleted()) {
                continue;
            }

            try {
                LOGGER.info(String.format("Shop: %s (%s)", shop.getName(),shop.getId()));

                int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);
                //DateTime nowUTC = DateUtil.nowUTC().withTimeAtStartOfDay();

                DateTime shopStartOfDay = DateUtil.nowWithTimeZone(timeZone).minusDays(1).withTimeAtStartOfDay();
                long timeZoneStartDateMillis = shopStartOfDay.getMillis();
                DateTime jodaEndDate = shopStartOfDay.plusDays(1).minusSeconds(1);
                long timeZoneEndDateMillis = jodaEndDate.getMillis();

                LOGGER.info(String.format("Today: %d, Start: %d, end: %d -- timezone: %d", DateUtil.nowUTC().getMillis(), timeZoneStartDateMillis, timeZoneEndDateMillis, timeZoneOffset));

                NotificationInfo notificationInfo = notificationInfoHashMap.get(shop.getId());


                //continue if null or not enable for shop
                if (notificationInfo == null || !notificationInfo.isActive()) {
                    LOGGER.info("Not active... continuing next shop.");
                    continue;
                }

                /* If email provided in request use those else use from notification info else use shop's email*/
                toEmails = requestEmail;

                long endDateMillis = DateUtil.nowUTC().getMillis();
                if (!backgroundJob && notificationInfo != null && summaryDate > 0) {
                    DateTime startDateTime = new DateTime(summaryDate);
                    timeZoneStartDateMillis = startDateTime.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
                    DateTime endDate = startDateTime.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
                    endDateMillis = endDate.minusMinutes(timeZoneOffset).getMillis();
                }


                LOGGER.info("Time: " + DateUtil.nowUTC() + " " + DateUtil.nowUTC().getMillis());
                if (backgroundJob && notificationInfo != null) {

                    // last snap shot taken time
                    long lastNotificationTime = notificationInfo.getLastSent();
                    long nextNotificationTime = notificationInfo.getNextNotificationTime();

                    String lastNotificationStr = DateUtil.toDateFormatted(lastNotificationTime, shop.getTimeZone());
                    String nextNotificationTimeStr = DateUtil.toDateFormatted(nextNotificationTime, shop.getTimeZone());

                    // Job takes snapshot at 2:00AM else continue
                    DateTime newNotificationTime = DateUtil.nowWithTimeZone(shop.getTimeZone()).withTimeAtStartOfDay().plusDays(1).plusHours(2);
                    DateTime currentTime = DateUtil.nowWithTimeZone(shop.getTimeZone());
                    String newRequiredTimeStr = DateUtil.toDateFormatted(newNotificationTime.getMillis(), shop.getTimeZone());


                    LOGGER.info("Time: " + DateUtil.nowUTC() + " " + DateUtil.nowUTC().getMillis());
                    LOGGER.info("Next Notification Time: " + nextNotificationTimeStr + " - " + nextNotificationTime);
                    LOGGER.info("Shop: " + shop.getName() + " - " + nextNotificationTime);
                    LOGGER.info("Last Notification Date: " + lastNotificationStr + ", current date: " + newRequiredTimeStr + " hasNotified: " + lastNotificationStr.equalsIgnoreCase(newRequiredTimeStr));
                    LOGGER.info("Required Time " + newNotificationTime + " - " + newNotificationTime.getMillis());
                    LOGGER.info("Current Time " + currentTime + " - " + currentTime.getMillis());

                    /*
                      run only when call from quartz job
                      or previously send
                    */
                    if (currentTime.getMillis() < nextNotificationTime) {
                        // Not yet time
                        LOGGER.info("Not yet time, skipping...");
                        continue;
                    }
                    //timeZoneStartDateMillis = lastSendTime;
                    toEmails = new ArrayList<>();
                    if (StringUtils.isNotBlank(notificationInfo.getDefaultEmail())) {
                        toEmails = Arrays.asList(notificationInfo.getDefaultEmail().split(","));
                    }

                    // Update next notification time
                    notificationInfoRepository.updateNextNotificationTime(Lists.newArrayList(shop.getId()),newNotificationTime.getMillis(),notificationInfo.getNotificationType());
                }


                if (toEmails != null && !toEmails.isEmpty()) {


                    HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(shop.getCompanyId());
                    Iterable<Transaction> transactions = transactionRepository.getAllBracketSales(shop.getCompanyId(), shop.getId(), timeZoneStartDateMillis, endDateMillis);

                    HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(shop.getCompanyId());
                    HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(shop.getCompanyId(), shop.getId());
                    HashMap<String, PrepackageProductItem> prepackageProductItemMap = prepackageProductItemRepository.listAsMap(shop.getCompanyId(), shop.getId());
                    HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(shop.getCompanyId());
                    HashMap<String, ProductBatch> recentBatchMap = getProductBatchByProductId(productBatchHashMap);


                    String emailBody = emailDailySummaryDetailsBody(shop, timeZoneStartDateMillis, transactions, prepackageHashMap, prepackageProductItemMap, toleranceHashMap, recentBatchMap, productBatchHashMap, employeeHashMap);

                    String emailTitle = String.format("Daily Summary Notification - %s - %s", shop.getName(), TextUtil.toDate(timeZoneStartDateMillis));

                    for (String email : toEmails) {
                        LOGGER.info("Daily summary email : " + email);
                        amazonServiceManager.sendEmail("support@blaze.me", email, emailTitle, emailBody, null, null, "Blaze Support");
                    }

                    updateShopIds.add(shop.getId());
                    LOGGER.info("Notifying...");
                }
            } catch (Exception e) {
                LOGGER.error("Error on daily summary : " + shopId, e);
            }

        }
        if (backgroundJob && !updateShopIds.isEmpty()) {
            notificationInfoRepository.updateLastSentTime(Lists.newArrayList(updateShopIds), DateTime.now().getMillis(), NotificationInfo.NotificationType.Daily_Summary);
        }
    }

    private String emailDailySummaryDetailsBody(Shop shop,
                                                long timeZoneStartDateMillis,
                                                Iterable<Transaction> transactions,
                                                HashMap<String, Prepackage> prepackageHashMap,
                                                HashMap<String, PrepackageProductItem> prepackageProductItemMap,
                                                HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                HashMap<String, ProductBatch> recentBatchMap,
                                                HashMap<String, ProductBatch> productBatchHashMap,
                                                Map<String, Employee> employeeHashMap) {

        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/daily_summary.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        String emailBody = writer.toString();

        int totalRefundCount = 0;
        int totalSalesCount = 0;
        int totalAllVisits = 0;
        double cashSales = 0.0;
        double cashlessSales = 0.0;
        double checkSales = 0.0;
        double creditSales = 0.0;
        double storeCreditSales = 0.0;
        double linxSales = 0.0;
        double totalDiscounts = 0.0;
        double splitSales = 0.0;
        double cogs = 0.0;
        double subTotalSales = 0.0;
        double totalTax = 0.0;

        double items = 0;
        double itemsRefund = 0;
        double totalRecTax = 0.0;
        double totalMedicalTax = 0.0;

        double totalRefundReceipts = 0.0;
        double totalSalesReceipts = 0.0;

        double deliveryFees = 0;
        double creditFees = 0;


        // Build unique set
        HashSet<Cart.PaymentOption> customPaymentOptions = new HashSet<>();
        for (Transaction transaction : transactions) {
            switch (transaction.getCart().getPaymentOption()) {
                case Cash:
                case Check:
                case Credit:
                case StoreCredit:
                case Split:
                    // Skip these, they're already explicitly defined.
                    break;
                default:
                    // Everything else is considered custom
                    customPaymentOptions.add(transaction.getCart().getPaymentOption());
            }
        }


        HashMap<Long, DailySummary> days = new HashMap<>();
        ArrayList<DailySummary> summaries = dailySummaryGatherer.getDailySummaries(days, new ArrayList<Member>(), transactions, productBatchHashMap,
                prepackageHashMap, prepackageProductItemMap, toleranceHashMap, recentBatchMap, customPaymentOptions, true);

        StringBuilder section = new StringBuilder();
        if (summaries.size() > 0) {
            DailySummary today = summaries.get(0);

            Map<String, DailySummary.EmployeeStats> employeeStatsMap = today.employeeStatsMap;

            for (String sellerId : employeeStatsMap.keySet()) {
                Employee employee = employeeHashMap.get(sellerId);
                String employeeName = null;
                if (employee != null) {
                    employeeName = employee.getFirstName().concat(" ").concat(employee.getLastName());
                } else {
                    employeeName = "Unknown Employee";
                }
                Double totalInvoiceValue = 0.0;
                Double salesValue = 0.0;
                Double refundsValue = 0.0;
                int salesCount = 0;

                int refundCount = 0;

                DailySummary.EmployeeStats employeeStats = employeeStatsMap.get(sellerId);

                salesValue = employeeStats.sales;
                salesCount = employeeStats.salesCount;
                refundsValue = employeeStats.refunds;
                refundCount = employeeStats.refundCount;

                section.append(getEmployeeInformationSection(shop, employeeName, salesValue, refundsValue, salesCount, refundCount));

            }

            totalRefundCount = today.refundCount;
            totalSalesCount = today.salesCount;
            totalAllVisits = today.totalAllVisits;
            cashSales = today.cashSales;
            checkSales = today.checkSales;
            creditSales = today.creditSales;
            storeCreditSales = today.storeCreditSales;
            linxSales = 0.0;
            if (today.customPaymentTotals.containsKey(Cart.PaymentOption.Linx)) {
                linxSales = today.customPaymentTotals.get(Cart.PaymentOption.Linx).doubleValue();
            }
            cashlessSales = today.cashlessAtmSales;

            totalDiscounts = today.totalDiscounts;
            splitSales = today.splitSales;
            cogs = today.cogs;
            subTotalSales = today.subTotalSales;
            totalTax = today.totalTax;

            items = today.salesItems;
            itemsRefund = today.refundItems;
            totalRecTax = today.totalRecTax;
            totalMedicalTax = today.totalMedTax;
            //totalTax = totalRecTax + totalMedicalTax;

            totalRefundReceipts = today.totalRefunds;
            totalSalesReceipts = today.gross;

            deliveryFees = today.deliveryFees;
            creditFees = today.creditFees;

        }


        emailBody = emailBody.replaceAll("==date==", TextUtil.toDate(timeZoneStartDateMillis));
        emailBody = emailBody.replaceAll("==shopname==", shop.getName());
        emailBody = emailBody.replaceAll("==address==", shop.getAddress().getAddressString());

        emailBody = emailBody.replaceAll("==employeeSales==", Matcher.quoteReplacement(section.toString()));
        emailBody = emailBody.replace("==subTotal==", TextUtil.toCurrency(subTotalSales, shop.getDefaultCountry()));

        double negDiscounts = totalDiscounts * -1;
        emailBody = emailBody.replace("==totalDiscount1==", TextUtil.toCurrency(negDiscounts, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==totalTax==", TextUtil.toCurrency(totalTax, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==fees==",  TextUtil.toCurrency(deliveryFees + creditFees,  shop.getDefaultCountry()));
        emailBody = emailBody.replace("==totalInvoiced==", TextUtil.toCurrency(totalSalesReceipts, shop.getDefaultCountry()));

        emailBody = emailBody.replace("==totalDiscount==", TextUtil.toCurrency(totalDiscounts, shop.getDefaultCountry()));

        emailBody = emailBody.replace("==deliveryFees==", TextUtil.toCurrency(deliveryFees, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==creditFees==", TextUtil.toCurrency(creditFees, shop.getDefaultCountry()));

        emailBody = emailBody.replace("==totalRefund==", TextUtil.toCurrency(totalRefundReceipts, shop.getDefaultCountry()));


        emailBody = emailBody.replace("==totalCost==", TextUtil.toCurrency(cogs, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==grossProfit==", TextUtil.toCurrency((totalSalesReceipts - cogs), shop.getDefaultCountry()));

        emailBody = emailBody.replace("==cash==", TextUtil.toCurrency(cashSales, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==credits==", TextUtil.toCurrency(creditSales, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==checks==", TextUtil.toCurrency(checkSales, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==storeCredits==", TextUtil.toCurrency(storeCreditSales, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==cashlessAtm==", TextUtil.toCurrency(cashlessSales, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==linx==", TextUtil.toCurrency(linxSales, shop.getDefaultCountry()));

        emailBody = emailBody.replace("==splits==", TextUtil.toCurrency(splitSales, shop.getDefaultCountry()));

        emailBody = emailBody.replace("==recSalesTax==", TextUtil.toCurrency(totalRecTax, shop.getDefaultCountry()));
        emailBody = emailBody.replace("==medicinalSalesTax==", TextUtil.toCurrency(totalMedicalTax, shop.getDefaultCountry()));


        emailBody = emailBody.replaceAll("==invoiceSold==", Matcher.quoteReplacement(String.valueOf(totalSalesCount)));
        emailBody = emailBody.replaceAll("==invoiceRefunded==", Matcher.quoteReplacement(String.valueOf(totalRefundCount)));
        emailBody = emailBody.replaceAll("==netSold==", Matcher.quoteReplacement(String.valueOf(totalSalesCount - totalRefundCount)));

        emailBody = emailBody.replaceAll("==itemSold==", Matcher.quoteReplacement(String.valueOf(items)));
        emailBody = emailBody.replaceAll("==itemRefunded==", Matcher.quoteReplacement(String.valueOf(itemsRefund)));
        emailBody = emailBody.replaceAll("==netItemSold==", Matcher.quoteReplacement(String.valueOf(items - itemsRefund)));


        return emailBody;
    }


    private String getEmployeeInformationSection(Shop shop, String employee, double sale, double refunds, int salesCount, int refundCount) {
        double avgReceipts = 0d;
        if (salesCount > 0) {
            avgReceipts = sale / salesCount;
        }


        return "<tr>\n" +
                "<td width=\"25%\" height=\"30\" style=\"text-align:left;\">" + employee + "</td>\n" +
                "<td width=\"25%\" height=\"30\" style=\"text-align:right;\">" + TextUtil.toCurrency(sale, shop.getDefaultCountry()) + " (" + String.valueOf(salesCount) + ")" + "</td>\n" +
                "<td width=\"25%\" height=\"30\" style=\"text-align:right;\">" + TextUtil.toCurrency(avgReceipts, shop.getDefaultCountry()) + "</td>\n" +
                "<td width=\"25%\" height=\"30\" style=\"text-align:right;\">" + TextUtil.toCurrency(refunds, shop.getDefaultCountry()) + " (" + String.valueOf(refundCount) + ")" + "</td>\n" +
                "</tr>";
    }

    private HashMap<String, ProductBatch> getProductBatchByProductId(HashMap<String, ProductBatch> productBatchHashMap) {
        HashMap<String, ProductBatch> returnMap = new HashMap<>();

        for (String key : productBatchHashMap.keySet()) {
            ProductBatch batch = productBatchHashMap.get(key);
            if (returnMap.containsKey(batch.getProductId())) {
                ProductBatch mapBatch = returnMap.get(batch.getProductId());
                DateTime batchDateTime = new DateTime(batch.getCreated());
                if (batchDateTime.isBefore(mapBatch.getCreated())) {
                    batch = mapBatch;
                }
            }
            returnMap.put(batch.getProductId(), batch);
        }

        return returnMap;
    }

    private Double calculateTotalCogs(Transaction transaction, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, HashMap<String, ProductBatch> recentBatchMap) {
        Double totalCogs = 0.0;
        if (transaction.getCart() == null || transaction.getCart().getItems() == null) {
            return totalCogs;
        }
        for (OrderItem item : transaction.getCart().getItems()) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(item.getPrepackageItemId());
            boolean calculate = false;
            if (prepackageProductItem != null) {
                ProductBatch batch = productBatchMap.get(prepackageProductItem.getBatchId());
                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                if (batch != null && prepackage != null) {
                    BigDecimal unitValue = prepackage.getUnitValue();
                    if (unitValue == null || unitValue.doubleValue() == 0) {
                        ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                        unitValue = weightTolerance.getUnitValue();
                    }
                    double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                    double unitPrice = batch.getFinalUnitCost().doubleValue();
                    totalCogs += unitPrice * unitsSold;
                    calculate = true;
                }
            } else if (item.getQuantityLogs() != null && !item.getQuantityLogs().isEmpty()) {
                for (QuantityLog log : item.getQuantityLogs()) {
                    ProductBatch batch = productBatchMap.get(log.getBatchId());
                    if (batch != null) {
                        double unitsSold = item.getQuantity().doubleValue();
                        double unitPrice = batch.getFinalUnitCost().doubleValue();
                        totalCogs += unitPrice * unitsSold;
                        calculate = true;
                    }
                }
            }
            if (!calculate) {
                ProductBatch batch = recentBatchMap.get(item.getProductId());
                BigDecimal unitPrice = (batch == null) ? BigDecimal.ZERO : batch.getFinalUnitCost();
                totalCogs += item.getQuantity().multiply(unitPrice).doubleValue();
            }
        }
        return totalCogs;
    }

}

