package com.fourtwenty.core.jobs;

import net.greghaines.jesque.Job;
import net.greghaines.jesque.worker.Worker;
import net.greghaines.jesque.worker.WorkerEvent;
import net.greghaines.jesque.worker.WorkerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mdo on 5/1/16.
 */

public class EchoListener implements WorkerListener {
    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(EchoListener.class);

    @Override
    public void onEvent(WorkerEvent event, Worker worker, String queue, Job job, Object runner,
                        Object result, Throwable ex) {
        LOG.info("onEvent ==>> queue={}, event={}", new Object[]{queue, event});
    }

}
