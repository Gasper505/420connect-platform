package com.fourtwenty.core.jobs;

import com.google.inject.Injector;
import net.greghaines.jesque.Job;
import net.greghaines.jesque.utils.ReflectionUtils;
import net.greghaines.jesque.worker.MapBasedJobFactory;
import net.greghaines.jesque.worker.UnpermittedJobException;

import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by mdo on 5/1/16.
 */
public class GuiceJobFactory extends MapBasedJobFactory {
    private final Injector injector;

    public GuiceJobFactory(Map<String, ? extends Class<?>> jobTypes, Injector injector) {
        super(jobTypes);
        this.injector = injector;
    }


    @Override
    public Object materializeJob(Job job) throws Exception {

        final String className = job.getClassName();
        final Class<?> clazz = this.getJobTypes().get(className);
        if (clazz == null) {
            throw new UnpermittedJobException(className);
        }
        // A bit redundant since we check when the job type is added...
        if (!Runnable.class.isAssignableFrom(clazz) && !Callable.class.isAssignableFrom(clazz)) {
            throw new ClassCastException("jobs must be a Runnable or a Callable: " + clazz.getName() + " - " + job);
        }

        // Create object from Guice
        Object jobObj = injector.getInstance(clazz);
        // Invoke setters
        Object obj = ReflectionUtils.invokeSetters(jobObj, job.getVars());
        return obj;
    }
}
