package com.fourtwenty.core.jobs;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;

public class LowInventoryNotification {
    static final Logger LOGGER = LoggerFactory.getLogger(LowInventoryNotification.class);


    private static final String CATEGORY_NAME = "categoryName";
    private static final String PRODUCT_NAME = "productName";
    private static final String VENDOR_NAME = "vendorName";
    private static final String PRODUCT_QUANTITY = "productQty";
    private static final String LOW_THRESHOLD = "lowThreshold";
    private static final String PREPACKAGE_QTY = "prepackageQty";
    private static final String PREPACKAGE_NAME = "prepackageName";
    private static final String TOTAL_QUANTITY = "totalQuantity";
    private static final String BRAND_NAME = "brandName";

    @Inject
    private AppRepository appRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    @Inject
    private NotificationInfoRepository notificationInfoRepository;
    @Inject
    private BrandRepository brandRepository;

    public LowInventoryNotification() {

    }

    public void dataForLowInventoryNotification(String shopId, boolean nightlyJob, List<String> email) {

        // For last sent email
        HashMap<String, NotificationInfo> notificationInfoHashMap = notificationInfoRepository.listByShopAndTypeAsMap(NotificationInfo.NotificationType.Low_Inventory);

        HashMap<String, Company> companyHashMap = new HashMap<>();
        HashMap<String, Shop> shopHashMap = new HashMap<>();
        HashMap<String, Product> productHashMap = new HashMap<>();
        HashMap<String, ProductCategory> categoryHashMap = new HashMap<>();
        HashMap<String, Vendor> vendorHashMap = new HashMap<>();
        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        HashMap<String, ProductWeightTolerance> toleranceHashMap = new HashMap<>();
        HashMap<String, ProductPrepackageQuantity> productPrepackageQuantityHashMap = new HashMap<>();

        List<String> toEmails = new ArrayList<>();

        if (shopId == null) {
            return;
        }

        /* If for only one shop*/
        Shop shop = shopRepository.getById(shopId);
        if (shop == null || shop.isActive() == false) {
            return;
        }
        Company company = companyRepository.getById(shop.getCompanyId());

        if (company == null || company.isDeleted() || company.isActive() == false) {
            return;
        }


        NotificationInfo notificationInfo = notificationInfoHashMap.get(shop.getId());

        //continue if null or not enable for shop or previously send

        /* If email provided in request use those else use from notification info else use shop's email*/
        toEmails = email;
        if ((toEmails == null || toEmails.isEmpty()) && notificationInfo != null && StringUtils.isNotBlank(notificationInfo.getDefaultEmail())) {
            toEmails = Arrays.asList(notificationInfo.getDefaultEmail().split("\\s*,\\s*"));
        }

        DateTime newNotificationTime = null;
        if (nightlyJob && notificationInfo != null) {
            if (notificationInfo.isActive() == false) {
                return;
            }
            /*
             run only when call from quartz job
            */
            DateTime dateTime = new DateTime(notificationInfo.getLastSent());
            if (dateTime.plusDays(1).isAfterNow()) {
                return;
            }
            // last snap shot taken time
            long lastNotificationTime = notificationInfo.getLastSent();
            long nextNotificationTime = notificationInfo.getNextNotificationTime();

            String lastNotificationStr = DateUtil.toDateFormatted(lastNotificationTime, shop.getTimeZone());
            String nextNotificationTimeStr = DateUtil.toDateFormatted(nextNotificationTime, shop.getTimeZone());

            // Job takes snapshot at 2:00AM else continue
            newNotificationTime = DateUtil.nowWithTimeZone(shop.getTimeZone()).withTimeAtStartOfDay().plusDays(1).plusHours(3);
            DateTime currentTime = DateUtil.nowWithTimeZone(shop.getTimeZone());
            String newRequiredTimeStr = DateUtil.toDateFormatted(newNotificationTime.getMillis(), shop.getTimeZone());


                    /*
                      run only when call from quartz job
                      or previously send
                    */
            if (currentTime.getMillis() < nextNotificationTime) {
                // Not yet time
                LOGGER.info("Not yet time, skipping...");
                return;
            }

            if (StringUtils.isNotBlank(notificationInfo.getDefaultEmail())) {
                toEmails = Arrays.asList(notificationInfo.getDefaultEmail().split("\\s*,\\s*"));
            }
        }




        shopHashMap.put(shop.getId(), shop);
        companyHashMap.put(shop.getCompanyId(), company);
        productHashMap = productRepository.listAsMap(company.getId(), shopId);
        categoryHashMap = categoryRepository.listAsMap(company.getId(), shopId);
        vendorHashMap = vendorRepository.listAsMap(company.getId());
        prepackageHashMap = prepackageRepository.listAsMap(company.getId(), shopId);
        toleranceHashMap = weightToleranceRepository.listAsMap(company.getId());
        productPrepackageQuantityHashMap = productPrepackageQuantityRepository.listAsMap(company.getId(), shopId);
        HashMap<String, Brand>  productBrandMap = brandRepository.listAsMap(company.getId());

        HashMap<String, String> productsDataMap;
        List<HashMap<String, String>> productsList;
        HashMap<String, List<HashMap<String, String>>> shopProductsMap = new HashMap<>();

        HashMap<String, HashMap<String, BigDecimal>> prepackageProductQuantityMap = groupPrepackageByProduct(toleranceHashMap, prepackageHashMap, productPrepackageQuantityHashMap);
        for (String productKey : productHashMap.keySet()) {
            Product product = productHashMap.get(productKey);

            if (product == null || product.isDeleted() || !product.isActive()) {
                continue;
            }

            ProductCategory category = categoryHashMap.get(product.getCategoryId());
            if (category == null || !category.isActive()) {
                continue;
            }




            HashMap<String, BigDecimal> prePackageQuantityMap = prepackageProductQuantityMap.get(productKey);


            productsDataMap = new HashMap<>();
            BigDecimal productQuantity = new BigDecimal("0.0");
            BigDecimal prePackageQty = new BigDecimal("0.0");
            StringBuilder prepackageName = new StringBuilder();
            List<ProductQuantity> productQuantities = new ArrayList<>();

            if (product.getQuantities() != null) {
                productQuantities = product.getQuantities();
                for (ProductQuantity productQty : productQuantities) {
                    productQuantity = productQuantity.add(productQty.getQuantity());
                }
            }

            if (prePackageQuantityMap != null && !prePackageQuantityMap.isEmpty()) {
                for (String prepackageKey : prePackageQuantityMap.keySet()) {
                    Prepackage prepackage = prepackageHashMap.get(prepackageKey);
                    if (prepackage == null || !prepackage.isActive()) {
                        continue;
                    }
                    BigDecimal qty = prePackageQuantityMap.get(prepackageKey);
                    prePackageQty = prePackageQty.add(qty);
                    if (StringUtils.isNotBlank(prepackageName)) {
                        prepackageName.append(",");
                    }
                    prepackageName.append(prepackage.getName()).append(" ( ").append(TextUtil.formatToTwoDecimalPoints(qty)).append(" ").append(category.getUnitType()).append(")");
                }
            }
            boolean isAddInMap = false;
            BigDecimal totalQuantity = productQuantity.add(prePackageQty);
            BigDecimal lowThresHoldValue = BigDecimal.ZERO;

            if (product.isLowInventoryNotification() && product.getLowThreshold() != null) {
                if (product.getLowThreshold().doubleValue() > totalQuantity.doubleValue()) {
                    isAddInMap = true;
                    lowThresHoldValue = product.getLowThreshold();
                }
            } else if (category.getLowThreshold() != null && category.getLowThreshold().longValue() > totalQuantity.longValue()) {
                isAddInMap = true;
                lowThresHoldValue = category.getLowThreshold();
            }
            if (isAddInMap) {
                productsDataMap = arrangeProductsDataMap(category, product, vendorHashMap, productQuantity, productsDataMap, prePackageQty, prepackageName.toString(), lowThresHoldValue, totalQuantity, productBrandMap);
                productsList = new ArrayList<>();
                if (shopProductsMap.containsKey(shop.getId())) {
                    productsList = shopProductsMap.get(shop.getId());
                }
                productsList.add(productsDataMap);
                shopProductsMap.put(shop.getId(), productsList);
            }

        }


        Set<String> shopKeysList = shopProductsMap.keySet();
        for (String key : shopKeysList) {
            sendEmail(key, shopProductsMap, companyHashMap, shopHashMap, nightlyJob, toEmails);
        }

        if (nightlyJob && !shopKeysList.isEmpty()) {
            notificationInfoRepository.updateLastSentTime(Lists.newArrayList(shopKeysList), DateTime.now().getMillis(), NotificationInfo.NotificationType.Low_Inventory);
            if (newNotificationTime != null) {
                // Update next notification time
                notificationInfoRepository.updateNextNotificationTime(Lists.newArrayList(shopKeysList), newNotificationTime.getMillis(), NotificationInfo.NotificationType.Low_Inventory);
            }

        }

    }

    private void sendEmail(String key, HashMap<String, List<HashMap<String, String>>> shopProductsMap,
                           HashMap<String, Company> companyHashMap, HashMap<String, Shop> shopHashMap,
                           boolean nightlyJob, List<String> requestToEmail) {
        String emailBody;
        List<HashMap<String, String>> products = shopProductsMap.get(key);
        Shop shop = shopHashMap.get(key);
        Company company = companyHashMap.get(shop.getCompanyId());
        emailBody = emailLowInventoryBody(shop, products, company, nightlyJob);
        if (requestToEmail != null && requestToEmail.size() > 0) {
            for (String email : requestToEmail) {
                amazonServiceManager.sendEmail("support@blaze.me", email, "Low Inventory Notification", emailBody, null, null, "Blaze Support");
            }
        } else {
            List<String> emailList = shop.getEmailList();
            for (String email : emailList) {
                amazonServiceManager.sendEmail("support@blaze.me", email, "Low Inventory Notification", emailBody, null, null, "Blaze Support");
            }
            amazonServiceManager.sendEmail("support@blaze.me", shop.getEmailAdress(), "Low Inventory Notification", emailBody, null, null, "Blaze Support");
        }
    }

    private HashMap<String, String> arrangeProductsDataMap(ProductCategory category, Product product, HashMap<String, Vendor> vendorHashMap, BigDecimal productQuantity, HashMap<String, String> productsDataMap, BigDecimal prepackageQty, String prepackageName, BigDecimal lowThresholdValue, BigDecimal totalQuantity, HashMap<String, Brand> productBrandMap) {
        productsDataMap.put(CATEGORY_NAME, category.getName());
        productsDataMap.put(PRODUCT_NAME, product.getName());
        if (vendorHashMap.containsKey(product.getVendorId()) && vendorHashMap.get(product.getVendorId()) != null) {
            productsDataMap.put(VENDOR_NAME, vendorHashMap.get(product.getVendorId()).getName());
        } else {
            productsDataMap.put(VENDOR_NAME, "");
        }
        productsDataMap.put(PRODUCT_QUANTITY, TextUtil.formatToTwoDecimalPoints(productQuantity) + " " + category.getUnitType());
        productsDataMap.put(LOW_THRESHOLD, TextUtil.formatToTwoDecimalPoints(lowThresholdValue) + " " + category.getUnitType());
        productsDataMap.put(PREPACKAGE_QTY, TextUtil.formatToTwoDecimalPoints(prepackageQty) + " " + category.getUnitType());
        productsDataMap.put(PREPACKAGE_NAME, prepackageName);
        productsDataMap.put(TOTAL_QUANTITY, TextUtil.formatToTwoDecimalPoints(totalQuantity) + " " + category.getUnitType());
        productsDataMap.put(BRAND_NAME,  StringUtils.isNotBlank(product.getBrandId()) && (productBrandMap.get(product.getBrandId()) != null) &&  StringUtils.isNotBlank(productBrandMap.get(product.getBrandId()).getName())  ? productBrandMap.get(product.getBrandId()).getName() : "N/A");
        return productsDataMap;
    }

    private String emailLowInventoryBody(Shop shop, List<HashMap<String, String>> productsList, Company company, boolean nightlyJob) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/lowInventory.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        String emailBody = writer.toString();
        StringBuilder section = new StringBuilder();
        for (HashMap<String, String> productsMap : productsList) {
            String categoryName = productsMap.get(CATEGORY_NAME);
            String productName = productsMap.get(PRODUCT_NAME);
            String vendorName = productsMap.get(VENDOR_NAME);
            String productQuantity = productsMap.get(PRODUCT_QUANTITY);
            String lowThreshold = productsMap.get(LOW_THRESHOLD);
            String prepackageName = productsMap.get(PREPACKAGE_NAME);
            String prepackageQuantity = productsMap.get(PREPACKAGE_QTY);
            String totalQuantity = productsMap.get(TOTAL_QUANTITY);
            String brandName = productsMap.get(BRAND_NAME);

            section.append(getProductInformationSection(vendorName, categoryName, productName, totalQuantity, lowThreshold, prepackageName, productQuantity, prepackageQuantity, brandName));

        }
        emailBody = emailBody.replaceAll("==productInfo==", Matcher.quoteReplacement(section.toString()));

        emailBody = emailBody.replaceAll("==shopName==", shop.getName());
        emailBody = emailBody.replaceAll("==companyEmail==", company.getEmail());
        emailBody = emailBody.replaceAll("==companyPhone==", company.getPhoneNumber());
        emailBody = emailBody.replaceAll("==companyName==", company.getName());
        emailBody = emailBody.replaceAll("==preferredEmailColor==", company.getPreferredEmailColor());

        String logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        emailBody = emailBody.replaceAll("==logo==", logoURL);

        return emailBody;
    }


    private String getProductInformationSection(String vendorName, String categoryName, String productName, String totalInventory, String lowThreshold, String prepackageName, String productQuantity, String prepackageQuantity, String brandName) {
        return "<tr>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + categoryName + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + productName + "</td>\n" +
                "<td width=\"85\" height=\"30\" style=\"padding:3px 3px;\">" + vendorName + "</td>\n" +
                "<td width=\"85\" height=\"30\" style=\"padding:3px 3px;\">" + brandName + "</td>\n" +
                "<td width=\"130\" height=\"30\" style=\"padding:3px 3px;\">" + prepackageName + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + productQuantity + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + prepackageQuantity + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + lowThreshold + "</td>\n" +
                "<td width=\"150\" height=\"30\" style=\"padding:3px 3px;\">" + totalInventory + "</td>\n" +
                "</tr>";
    }

    private HashMap<String, HashMap<String, BigDecimal>> groupPrepackageByProduct(HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                                                  HashMap<String, Prepackage> prepackageHashMap,
                                                                                  HashMap<String, ProductPrepackageQuantity> productPrepackageQuantityHashMap) {
        HashMap<String, HashMap<String, BigDecimal>> productPrepackageQuantityMap = new HashMap<>();

        for (String quantityKey : productPrepackageQuantityHashMap.keySet()) {
            ProductPrepackageQuantity productPrepackageQuantity = productPrepackageQuantityHashMap.get(quantityKey);
            Prepackage prepackage = prepackageHashMap.get(productPrepackageQuantity.getPrepackageId());
            if (prepackage == null) {
                continue;
            }

            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
            if (weightTolerance == null) {
                continue;
            }

            HashMap<String, BigDecimal> prepackageQuantityMap = new HashMap<>();
            if (productPrepackageQuantityMap.containsKey(productPrepackageQuantity.getProductId())) {
                prepackageQuantityMap = productPrepackageQuantityMap.get(productPrepackageQuantity.getProductId());
            }

            Integer quantity = productPrepackageQuantity.getQuantity();

            ProductWeightTolerance.WeightKey weightKey = ProductWeightTolerance.WeightKey.getKey(weightTolerance.getName());
            BigDecimal quantityInGrams = weightKey.weightValue.multiply(new BigDecimal(quantity));

            if (prepackageQuantityMap.containsKey(prepackage.getId())) {
                quantityInGrams = quantityInGrams.add(prepackageQuantityMap.get(prepackage.getId()));
            }
            prepackageQuantityMap.put(prepackage.getId(), quantityInGrams);
            productPrepackageQuantityMap.put(productPrepackageQuantity.getProductId(), prepackageQuantityMap);
        }
        return productPrepackageQuantityMap;
    }

}
