package com.fourtwenty.core.jobs;

import com.fourtwenty.core.domain.models.compliance.ComplianceSyncJob;
import com.fourtwenty.core.domain.repositories.sync.ComplianceSyncJobRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.compliance.AssignTagComplianceEvent;
import com.fourtwenty.core.event.compliance.CreateItemComplianceEvent;
import com.fourtwenty.core.event.compliance.StrainComplianceEvent;
import com.fourtwenty.core.event.compliance.SyncComplianceEvent;
import com.fourtwenty.core.event.compliance.SyncUpComplianceEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class QueuedComplianceSyncJob implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(QueuedComplianceSyncJob.class);
    private String complianceSyncJobId;
    private String companyId;
    private String shopId;
    private String errorMsg;

    @Inject
    private ComplianceSyncJobRepository syncJobRepository;
    @Inject
    private BlazeEventBus eventBus;

    @Override
    public void run() {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(shopId)) {

            ComplianceSyncJob syncJob = syncJobRepository.getById(complianceSyncJobId);
            syncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.ERROR);
            syncJobRepository.update(companyId, complianceSyncJobId, syncJob);
            return;
        }

        ComplianceSyncJob syncJob = syncJobRepository.getById(complianceSyncJobId);
        if (syncJob != null && syncJob.getStatus() == ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED) {
            syncJobRepository.markQueueJobAsInProgress(complianceSyncJobId);
            // process the job accordingly
            if (syncJob.getJobType() == ComplianceSyncJob.ComplianceSyncJobType.CREATE_EXT_PACKAGES) {
                AssignTagComplianceEvent event = new AssignTagComplianceEvent();
                event.setCompanyId(companyId);
                event.setShopId(shopId);
                event.setComplianceSyncJob(syncJob);
                event.setPackageTags(syncJob.getPackages());

                eventBus.post(event);
                event.getResponse();
            } else if (syncJob.getJobType() == ComplianceSyncJob.ComplianceSyncJobType.SYNC) {
                SyncComplianceEvent complianceEvent = new SyncComplianceEvent();
                complianceEvent.setCompanyId(companyId);
                complianceEvent.setShopId(shopId);

                eventBus.post(complianceEvent);
                //complianceEvent.setResponseTimeout(BiDirectionalBlazeEvent.LONG_WAIT_TIME);
                complianceEvent.getResponse();
            } else if (syncJob.getJobType() == ComplianceSyncJob.ComplianceSyncJobType.SYNC_UP) {
                SyncUpComplianceEvent complianceEvent = new SyncUpComplianceEvent();
                complianceEvent.setCompanyId(companyId);
                complianceEvent.setShopId(shopId);
                complianceEvent.setPackageTags(syncJob.getPackages());

                eventBus.post(complianceEvent);
                //complianceEvent.setResponseTimeout(BiDirectionalBlazeEvent.LONG_WAIT_TIME);
                complianceEvent.getResponse();

            } else if (syncJob.getJobType() == ComplianceSyncJob.ComplianceSyncJobType.CREATE_ITEMS) {
                CreateItemComplianceEvent complianceEvent = new CreateItemComplianceEvent();
                complianceEvent.setCompanyId(companyId);
                complianceEvent.setShopId(shopId);
                complianceEvent.setPackageTags(syncJob.getPackages());
                complianceEvent.setComplianceSyncJob(syncJob);

                eventBus.post(complianceEvent);
                //complianceEvent.setResponseTimeout(BiDirectionalBlazeEvent.LONG_WAIT_TIME);
                complianceEvent.getResponse();
                errorMsg =  complianceEvent.getResponse().getErrorMsg();

            } else if (syncJob.getJobType() == ComplianceSyncJob.ComplianceSyncJobType.CREATE_STRAINS) {
                StrainComplianceEvent complianceEvent = new StrainComplianceEvent();
                complianceEvent.setCompanyId(companyId);
                complianceEvent.setShopId(shopId);
                complianceEvent.setComplianceSyncJob(syncJob);
                complianceEvent.setMetrcStr(syncJob.getStrains());

                eventBus.post(complianceEvent);
                //complianceEvent.setResponseTimeout(BiDirectionalBlazeEvent.LONG_WAIT_TIME);
                complianceEvent.getResponse();
                errorMsg =  complianceEvent.getResponse().getMessage();
            }

            syncJobRepository.markQueueJobAsComplete(complianceSyncJobId, errorMsg);
        }
    }


    public String getComplianceSyncJobId() {
        return complianceSyncJobId;
    }

    public void setComplianceSyncJobId(String complianceSyncJobId) {
        this.complianceSyncJobId = complianceSyncJobId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
