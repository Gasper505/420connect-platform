package com.fourtwenty.core.jobs;

/**
 * Created by mdo on 6/20/17.
 */
public enum QueuedJobType {
    TransactionJob("transaction_jobs", "TransactionJob"),
    ScheduledJob("quartz_scheduled_jobs", "ScheduledJob");

    QueuedJobType(String queueName, String jobName) {
        this.queueName = queueName;
        this.jobName = jobName;
    }

    public String queueName;
    public String jobName;
}
