package com.fourtwenty.core.jobs;

import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflySyncJobRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.leafly.LeaflySyncEvent;
import com.fourtwenty.core.event.leafly.LeaflySyncResponse;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.services.thirdparty.impl.LeaflyConstants;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class QueuedLeaflySyncJob implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueuedLeaflySyncJob.class);
    private String leaflySyncJobId;
    private String companyId;
    private String shopId;

    @Inject
    private LeaflySyncJobRepository leaflySyncJobRepository;
    @Inject
    private BackgroundTaskManager backgroundTaskManager;
    @Inject
    private BlazeEventBus blazeEventBus;

    @Override
    public void run() {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(shopId)) {
            LOGGER.error(LeaflyConstants.BLANK_COMPANY_SHOP);
            leaflySyncJobRepository.updateJobStatus(leaflySyncJobId, LeaflySyncJob.LeaflySyncJobStatus.Error, LeaflyConstants.BLANK_COMPANY_SHOP);
            return;
        }

        LeaflySyncJob syncJob = leaflySyncJobRepository.getById(leaflySyncJobId);
        if (syncJob != null && syncJob.getStatus() == LeaflySyncJob.LeaflySyncJobStatus.Queued) {
            LeaflySyncJob.LeaflySyncJobStatus status;
            String errorMsg = "";
            leaflySyncJobRepository.markQueueJobAsInProgress(leaflySyncJobId);
            switch (syncJob.getJobType()) {
                case NIGHTLY_BASIS:
                    LeaflySyncResponse response = backgroundTaskManager.syncLeafly(companyId, shopId, syncJob);
                    boolean sync = response.isSyncStatus();
                    errorMsg = response.getErrorMsg();
                    status = sync ? LeaflySyncJob.LeaflySyncJobStatus.Completed : LeaflySyncJob.LeaflySyncJobStatus.Error;
                    break;
                case INDIVIDUAL:
                    if (CollectionUtils.isEmpty(syncJob.getProductIds())) {
                        LOGGER.info(LeaflyConstants.EMPTY_SYNC_PRODUT_LIST);
                        errorMsg = LeaflyConstants.EMPTY_SYNC_PRODUT_LIST;
                        status = LeaflySyncJob.LeaflySyncJobStatus.Error;
                    } else {
                        LeaflySyncEvent event = new LeaflySyncEvent();
                        event.setShopId(shopId);
                        event.setCompanyId(companyId);
                        event.setProductIds(Sets.newHashSet(syncJob.getProductIds()));
                        blazeEventBus.post(event);
                        response = event.getResponse(30000);
                        errorMsg = response.getErrorMsg();
                        sync = response.isSyncStatus();
                        status = sync ? LeaflySyncJob.LeaflySyncJobStatus.Completed : LeaflySyncJob.LeaflySyncJobStatus.Error;
                    }
                    break;
                case RESET:
                    if (StringUtils.isBlank(syncJob.getAccountId())) {
                        LOGGER.info(LeaflyConstants.BLANK_JOB_ACCOUNT_ID);
                        errorMsg = LeaflyConstants.BLANK_JOB_ACCOUNT_ID;
                        status = LeaflySyncJob.LeaflySyncJobStatus.Error;
                    } else {
                        response = backgroundTaskManager.resetLeafly(companyId, shopId, syncJob.getAccountId());
                        errorMsg = response.getErrorMsg();
                        sync = response.isSyncStatus();
                        status = sync ? LeaflySyncJob.LeaflySyncJobStatus.Completed : LeaflySyncJob.LeaflySyncJobStatus.Error;
                    }
                    break;
                default:
                    errorMsg = LeaflyConstants.SYNC_JOB_TYPE_NOT_FOUND;
                    status = LeaflySyncJob.LeaflySyncJobStatus.Error;
                    break;
            }
            leaflySyncJobRepository.updateJobStatus(syncJob.getId(), status, errorMsg);
        }

    }

    public String getLeaflySyncJobId() {
        return leaflySyncJobId;
    }

    public void setLeaflySyncJobId(String leaflySyncJobId) {
        this.leaflySyncJobId = leaflySyncJobId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
