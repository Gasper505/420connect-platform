package com.fourtwenty.core.jobs;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.CustomerInfo;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.inventory.DerivedProductBatchEvent;
import com.fourtwenty.core.event.inventory.InventoryReconciliationEvent;
import com.fourtwenty.core.event.mtrac.MtracRefundTransactionEvent;
import com.fourtwenty.core.lifecycle.TransactionDidCompleteSaleReceiver;
import com.fourtwenty.core.lifecycle.TransactionDidRefundReceiver;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.managed.InventoryActionManager;
import com.fourtwenty.core.managed.SpringBigManager;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundOrderItemRequest;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.mgmt.LoyaltyActivityService;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 8/28/17.
 */
public class QueuedTransactionJob implements Runnable {
    public enum InventoryProcessStatus {
        Success,
        PreviouslyCompleted,
        Error
    }

    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(QueuedTransactionJob.class);
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private LoyaltyActivityService activityService;
    @Inject
    Set<TransactionDidCompleteSaleReceiver> completeReceivers;
    @Inject
    Set<TransactionDidRefundReceiver> refundReceivers;
    @Inject
    BackgroundTaskManager taskManager;
    @Inject
    BatchQuantityService batchQuantityService;
    @Inject
    CartService cartService;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    RefundTransactionRequestRepository refundTransactionRequestRepository;
    @Inject
    UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    private SpringBigManager springBigManager;
    @Inject
    private SpringbigRepository springbigRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    CashDrawerProcessorService cashDrawerProcessorService;
    @Inject
    private InventoryActionManager inventoryActionManager;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private InventoryTransferHistoryRepository inventoryTransferHistoryRepository;
    @Inject
    private ReconciliationHistoryRepository reconciliationHistoryRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private DerivedProductBatchLogRepository derivedProductBatchLogRepository;
    private String queueTransactionId;
    private String companyId;
    private String shopId;

    public QueuedTransactionJob() {
    }


    @Override
    public void run() {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(shopId)) {

            QueuedTransaction dbQueuedTransaction = queuedTransactionRepository.getById(queueTransactionId);
            dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
            queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            return;
        }
        LOG.info("Processing QueuedTransaction: " + queueTransactionId);

        QueuedTransaction dbQueuedTransaction = queuedTransactionRepository.get(companyId, queueTransactionId);

        if (dbQueuedTransaction != null && (QueuedTransaction.QueuedTransactionType.SalesTrans == dbQueuedTransaction.getQueuedTransType() ||
                QueuedTransaction.QueuedTransactionType.ShippingManifest == dbQueuedTransaction.getQueuedTransType() ||
                QueuedTransaction.QueuedTransactionType.RefundRequest == dbQueuedTransaction.getQueuedTransType() ||
                QueuedTransaction.QueuedTransactionType.RevertShippingManifest == dbQueuedTransaction.getQueuedTransType() ||
                QueuedTransaction.QueuedTransactionType.RejectShippingManifest == dbQueuedTransaction.getQueuedTransType())
                && dbQueuedTransaction.getStatus() == QueuedTransaction.QueueStatus.Pending) {

            try {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                if (dbQueuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.RefundRequest) {
                    processRefundRequest(dbQueuedTransaction);
                } else if (dbQueuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.RevertShippingManifest ||
                        dbQueuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.RejectShippingManifest) {
                    processRevertManifest(dbQueuedTransaction);
                } else {
                    /*Transaction dbTransaction = transactionRepository.get(companyId, dbQueuedTransaction.getTransactionId());
                    if (dbTransaction.isActive() || (dbTransaction.isActive() == false && dbQueuedTransaction.getPendingStatus() != Transaction.TransactionStatus.Hold)) {
                        processSaleRefundTransaction(dbQueuedTransaction, dbTransaction, false);
                    } else if (dbTransaction.isActive() == false && dbTransaction.getStatus() == Transaction.TransactionStatus.Hold) {
                        // if it's in-active, and we're on hold, then determine the status of the most recently completed transaction
                        QueuedTransaction completedQT = queuedTransactionRepository.getRecentCompletedTransaction(dbTransaction.getCompanyId(), dbQueuedTransaction.getShopId(),
                                dbTransaction.getId());
                        if (completedQT != null) {
                            if (completedQT.getPendingStatus() == Transaction.TransactionStatus.Completed
                                    && completedQT.getStatus() == QueuedTransaction.QueueStatus.Completed) {
                                // set this transaction as completed
                                // need to set sellerId and sellerTerminalId
                                transactionRepository.completeTransaction(companyId, dbTransaction.getId(),completedQT.getSellerId(),completedQT.getTerminalId());
                                realtimeService.sendRealTimeEvent(dbTransaction.getShopId(),
                                        RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
                            }
                        }
                    }*/
                    // check previous one
                    Transaction dbTransaction = transactionRepository.get(companyId, dbQueuedTransaction.getTransactionId());
                    QueuedTransaction completedQT = queuedTransactionRepository.getRecentCompletedTransaction(dbTransaction.getCompanyId(), dbQueuedTransaction.getShopId(),
                            dbTransaction.getId());
                    boolean canRun = true;
                    if (completedQT != null) {
                        if (completedQT.getPendingStatus() == Transaction.TransactionStatus.Completed) {
                            canRun = false;
                            dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                            transactionRepository.completeTransaction(companyId, dbTransaction.getId(),completedQT.getSellerId(),completedQT.getTerminalId());
                            realtimeService.sendRealTimeEvent(dbTransaction.getShopId(),
                                    RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
                        } else if (completedQT.getPendingStatus() == Transaction.TransactionStatus.Canceled) {
                            dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);

                            transactionRepository.cancelTransaction(companyId, dbTransaction.getId(),completedQT.getSellerId(),completedQT.getTerminalId());
                            realtimeService.sendRealTimeEvent(dbTransaction.getShopId(),
                                    RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
                        }
                    }

                    if (canRun) {
                        processSaleRefundTransaction(dbQueuedTransaction, dbTransaction, false);
                    }
                }
                queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            }
        } else if (dbQueuedTransaction != null && dbQueuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.PurchaseOrder
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {

            try {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);

                PurchaseOrder purchaseOrder = purchaseOrderRepository.get(companyId, dbQueuedTransaction.getTransactionId());
                if (purchaseOrder != null && !PurchaseOrder.PurchaseOrderStatus.Closed.equals(purchaseOrder.getPurchaseOrderStatus())) {
                    this.processPurchaseOrder(dbQueuedTransaction, purchaseOrder);
                }
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
            }
            queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.InventoryReconciliation == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {

            try {
                ReconciliationHistory reconciliationHistory = reconciliationHistoryRepository.get(companyId, dbQueuedTransaction.getTransactionId());


                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                if (reconciliationHistory != null) {
                    this.processReconciliationHistory(dbQueuedTransaction, reconciliationHistory);
                }
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
            }
            queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);

        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.InventoryTransfer == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {
            try {
                InventoryTransferHistory inventoryTransferHistory = inventoryTransferHistoryRepository.get(this.companyId, dbQueuedTransaction.getTransactionId());

                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                if (inventoryTransferHistory != null && !InventoryTransferHistory.TransferStatus.ACCEPTED.equals(inventoryTransferHistory.getStatus())) {
                    this.processInventoryTransfer(dbQueuedTransaction, inventoryTransferHistory);
                }
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
            }
            queuedTransactionRepository.update(this.companyId, dbQueuedTransaction.getId(), dbQueuedTransaction);

        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.ProductBatch == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {

            try {
                ProductBatch productBatch = productBatchRepository.get(companyId, dbQueuedTransaction.getTransactionId());

                ProductBatchQueuedTransaction batchQueuedTransaction = queuedTransactionRepository.get(companyId, dbQueuedTransaction.getId(), ProductBatchQueuedTransaction.class);
                if (productBatch != null && batchQueuedTransaction != null) {
                    this.processProductBatchInventory(batchQueuedTransaction, productBatch);
                    batchQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                    queuedTransactionRepository.update(this.companyId, batchQueuedTransaction.getId(), batchQueuedTransaction);
                } else if (batchQueuedTransaction == null) {
                    dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                    queuedTransactionRepository.update(this.companyId, dbQueuedTransaction.getId(), dbQueuedTransaction);
                }
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                queuedTransactionRepository.update(this.companyId, queueTransactionId, dbQueuedTransaction);
            }

        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.Prepackage == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {
            try {
                PrepackageProductItem prepackageProductItem = prepackageProductItemRepository.get(companyId, dbQueuedTransaction.getTransactionId());

                PrepackageProductItemQueuedTransaction prepackageQueuedTransaction = queuedTransactionRepository.get(companyId, dbQueuedTransaction.getId(), PrepackageProductItemQueuedTransaction.class);
                if (prepackageQueuedTransaction != null) {
                    this.processPrepackage(prepackageQueuedTransaction, prepackageProductItem);
                    prepackageQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                    queuedTransactionRepository.update(this.companyId, prepackageQueuedTransaction.getId(), prepackageQueuedTransaction);
                } else if (prepackageQueuedTransaction == null) {
                    dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                    queuedTransactionRepository.update(this.companyId, dbQueuedTransaction.getId(), dbQueuedTransaction);
                }
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                queuedTransactionRepository.update(this.companyId, queueTransactionId, dbQueuedTransaction);
            }

        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.StockReset == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {

            try {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                this.processStockReset(dbQueuedTransaction);
                queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            }

        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.Product == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {

            try {

                ConvertToProductQueuedTransaction queuedTransaction = queuedTransactionRepository.get(companyId, dbQueuedTransaction.getId(), ConvertToProductQueuedTransaction.class);
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                this.processProductInventory(queuedTransaction);
                queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
            }

        } else if (dbQueuedTransaction != null && QueuedTransaction.QueuedTransactionType.BulkProductBatch == dbQueuedTransaction.getQueuedTransType()
                && QueuedTransaction.QueueStatus.Pending == dbQueuedTransaction.getStatus()) {

            try {

                ProductBatchQueuedTransaction batchQueuedTransaction = queuedTransactionRepository.get(companyId, dbQueuedTransaction.getId(), ProductBatchQueuedTransaction.class);
                if (batchQueuedTransaction.getBatchQuantityMap() != null && batchQueuedTransaction.getBatchQuantityMap().size() > 0) {
                    this.processBulkProductBatchInventory(batchQueuedTransaction);
                    batchQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Completed);
                    queuedTransactionRepository.update(this.companyId, batchQueuedTransaction.getId(), batchQueuedTransaction);
                } else {
                    dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                    queuedTransactionRepository.update(this.companyId, dbQueuedTransaction.getId(), dbQueuedTransaction);
                }

            } catch (Exception e) {
                dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
                queuedTransactionRepository.update(this.companyId, queueTransactionId, dbQueuedTransaction);
            }

        } else if (dbQueuedTransaction != null && dbQueuedTransaction.getStatus() == QueuedTransaction.QueueStatus.Completed) {
            // ignore
            LOG.info("Queued transaction was previously completed.");
        } else if (dbQueuedTransaction != null) {
            dbQueuedTransaction.setStatus(QueuedTransaction.QueueStatus.Error);
            queuedTransactionRepository.update(companyId, queueTransactionId, dbQueuedTransaction);
        }
    }

    private void processSaleRefundTransaction(QueuedTransaction queuedTransaction,
                                              Transaction dbTransaction,
                                              boolean refund) {


        InventoryProcessStatus processStatus = processInventoryUpdates(queuedTransaction, dbTransaction, refund);

        if (processStatus == InventoryProcessStatus.Success) {
            // UPDATE TRANSACTION WITH NEW CART
            DateTime processedTime = DateUtil.getCurrentUTCTime();
            dbTransaction.setProcessedTime(processedTime.getMillis());
            dbTransaction.setCompletedTime(processedTime.getMillis());

            dbTransaction.setMetrcSaleTime(processedTime.toString());
            dbTransaction.setCart(queuedTransaction.getCart());

            if (Transaction.TransactionStatus.RefundWithInventory == dbTransaction.getStatus() || Transaction.TransactionStatus.RefundWithoutInventory == dbTransaction.getStatus()) {
                dbTransaction.setTransactionRefundStatus(Transaction.TransactionRefundStatus.Complete_Refund);
            }

            dbTransaction.setStatus(queuedTransaction.getPendingStatus());
            if (dbTransaction.getStatus() == Transaction.TransactionStatus.Completed) {
                dbTransaction.setActive(false);
                dbTransaction.setSellerId(queuedTransaction.getSellerId());
                dbTransaction.setSellerTerminalId(queuedTransaction.getTerminalId());
            }

            // persist transaction
            transactionRepository.update(companyId, dbTransaction.getId(), dbTransaction);

            if (dbTransaction.getCart().getPaymentOption() == Cart.PaymentOption.Mtrac) {
                try {
                    MtracRefundTransactionEvent event = new MtracRefundTransactionEvent();
                    event.setPayload(dbTransaction.getCompanyId(), dbTransaction.getShopId(), dbTransaction, dbTransaction.getCart().getRequestRefundAmt());
                    blazeEventBus.post(event);
                    event.setResponseTimeout(30000);
                } catch (Exception e) {
                    LOG.warn("Couldn't process MTrac payment for refund transaction : " + dbTransaction.getId(), e);
                }
            }

            // POST TRANSACTION PROCESS
            processPostTransaction(queuedTransaction, dbTransaction);
        }
    }

    private void processPostTransaction(final QueuedTransaction queuedTransaction,
                                        final Transaction dbTransaction) {
        LOG.info("Completed Processing QueuedTransaction: " + queueTransactionId);
        // Send a real time update to all shop to update the inventory
        if (this.getShopId() != null) {
            realtimeService.sendRealTimeEvent(this.getShopId().toString(),
                    RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
            realtimeService.sendRealTimeEvent(this.getShopId().toString(),
                    RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
        }


        if ((queuedTransaction.getPendingStatus() == Transaction.TransactionStatus.Completed ||
                queuedTransaction.getPendingStatus() == Transaction.TransactionStatus.RefundWithInventory ||
                queuedTransaction.getPendingStatus() == Transaction.TransactionStatus.RefundWithoutInventory)
                && (dbTransaction.getTransType() == Transaction.TransactionType.Sale ||
                dbTransaction.getTransType() == Transaction.TransactionType.Refund)) {
            SpringBigInfo springBigInfo = springbigRepository.getInfoByShop(this.getCompanyId(), this.getShopId());
            if (springBigInfo != null && springBigInfo.isActive()) {
                Member member = memberRepository.get(this.getCompanyId(), dbTransaction.getMemberId());
                Shop shop = shopRepository.get(this.getCompanyId(), this.getShopId());
                springBigManager.createVisit(springBigInfo, dbTransaction, member, shop);
            }

        }
        // PROCESS CURRENT CASH DRAWERS
        try {
            processCurrentCashDrawer(dbTransaction.getCompanyId(),
                    dbTransaction.getShopId(),
                    dbTransaction.getSellerTerminalId());
        } catch (Exception e) {
            LOG.error("Cannot processed cash drawer.", e);
        }


        // NOTIFY ALL ASSOCIATE RECEIVERS
        if (dbTransaction.getStatus() == Transaction.TransactionStatus.Completed) {
            if (dbTransaction.getTransType() == Transaction.TransactionType.Sale
                    || dbTransaction.getTransType() == Transaction.TransactionType.ShippingManifest) {
                Kryo kyro = new Kryo();
                for (TransactionDidCompleteSaleReceiver receiver : completeReceivers) {
                    Transaction copyTrans = kyro.copy(dbTransaction);
                    taskManager.runTransactionReceiverBG(copyTrans, receiver);
                }
            }
        } else if (dbTransaction.getStatus() == Transaction.TransactionStatus.RefundWithInventory
                || dbTransaction.getStatus() == Transaction.TransactionStatus.RefundWithoutInventory) {
            if (dbTransaction.getTransType() == Transaction.TransactionType.Refund) {
                Kryo kyro = new Kryo();
                for (TransactionDidRefundReceiver receiver : refundReceivers) {
                    Transaction copyTrans = kyro.copy(dbTransaction);
                    taskManager.runTransactionReceiverBG(copyTrans, receiver);
                }
            } else if (dbTransaction.getTransType() == Transaction.TransactionType.Exchange) {
                Kryo kyro = new Kryo();
                for (TransactionDidRefundReceiver receiver : refundReceivers) {
                    Transaction copyTrans = kyro.copy(dbTransaction);
                    taskManager.runTransactionReceiverBG(copyTrans, receiver);
                }
            }
        }

        try {
            if (dbTransaction.getTransType() == Transaction.TransactionType.Adjustment && StringUtils.isNotBlank(queuedTransaction.getReconcilliationHistoryId())) {
                // Compliance integration
                ReconciliationHistory reconciliationHistory = reconciliationHistoryRepository.getById(queuedTransaction.getReconcilliationHistoryId());
                if (reconciliationHistory != null) {
                    List<ReconciliationHistoryList> reconciliationHistoryLists = new ArrayList<>();
                    if (!reconciliationHistory.isBatchReconcile() && dbTransaction.getCart() != null && dbTransaction.getCart().getItems() != null && !dbTransaction.getCart().getItems().isEmpty()) {
                        HashMap<String, List<QuantityLog>> quantityLogsMap = new HashMap<>();
                        for (OrderItem orderItem : dbTransaction.getCart().getItems()) {
                            if (orderItem.getQuantityLogs() != null && !orderItem.getQuantityLogs().isEmpty()) {
                                List<QuantityLog> quantityLogs = quantityLogsMap.getOrDefault(orderItem.getProductId(), new ArrayList<>());
                                quantityLogs.addAll(orderItem.getQuantityLogs());
                                quantityLogsMap.put(orderItem.getProductId(), quantityLogs);
                            }
                        }

                        for (ReconciliationHistoryList reconciliationHistoryList : reconciliationHistory.getReconciliations()) {
                            if (quantityLogsMap.containsKey(reconciliationHistoryList.getProductId())) {
                                for (QuantityLog quantityLog : quantityLogsMap.get(reconciliationHistoryList.getProductId())) {
                                    Kryo kryo = new Kryo();
                                    ReconciliationHistoryList reconciliationHistoryCopy = kryo.copy(reconciliationHistoryList);
                                    reconciliationHistoryCopy.setInventoryId(quantityLog.getInventoryId());
                                    BigDecimal quantity = reconciliationHistoryCopy.getOldQuantity().subtract(quantityLog.getQuantity());
                                    reconciliationHistoryCopy.setNewQuantity(quantity);
                                    reconciliationHistoryCopy.setBatchId(quantityLog.getBatchId());
                                    reconciliationHistoryCopy.setPrePackageItemId(quantityLog.getPrepackageItemId());
                                    reconciliationHistoryLists.add(reconciliationHistoryCopy);
                                }
                            }
                        }

                        /* update reconciliation history just for updating metrc batch quantity
                            update reconciliation history to set reconciliation history list and
                            is reconcile by batch to true
                            these details are not saved in database.
                         */
                        reconciliationHistory.setReconciliations(reconciliationHistoryLists);
                        reconciliationHistory.setBatchReconcile(true);
                    }

                    InventoryReconciliationEvent inventoryReconciliationEvent = new InventoryReconciliationEvent();
                    inventoryReconciliationEvent.setPayload(reconciliationHistory);
                    blazeEventBus.post(inventoryReconciliationEvent);
                }
            }
        } catch (Exception e) {
            LOG.error("Error in inventory reconcilliation event", e);
        }

        processDerivedBatchesForMetrc(dbTransaction, queuedTransaction);
    }

    private InventoryProcessStatus processInventoryUpdates(QueuedTransaction queuedTransaction,
                                                           Transaction dbTransaction,
                                                           final boolean isRefund) {
        Terminal terminal = terminalRepository.get(this.getCompanyId(), queuedTransaction.getTerminalId());
        Inventory safeInventory = inventoryRepository.getInventory(this.getCompanyId(), this.getShopId(), Inventory.SAFE);
        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAllAsMap(this.getCompanyId(), this.getShopId());
        //Shop shop = shopRepository.get(this.getCompanyId(), this.getShopId());
        final String shopId = this.getShopId();

        Set<ObjectId> objectIds = new HashSet<>();
        Set<String> productIds = new HashSet<>();

        // Add old quantity back to inventory
        // Only do this if incoming is not refunded transaction
        List<ProductChangeLog> productChangeLogs = new ArrayList<>();

        // get the IDs of the current incoming cart

        QueuedTransaction recentCompletedJob = queuedTransactionRepository.getRecentCompletedTransaction(this.getCompanyId(), this.getShopId(), dbTransaction.getId());

        // get the IDs of the old cart
        Cart oldJobCart = dbTransaction.getCart();
        if (recentCompletedJob != null && recentCompletedJob.getCart() != null) {
            oldJobCart = recentCompletedJob.getCart();

            if (recentCompletedJob.getStatus() == QueuedTransaction.QueueStatus.Completed
                    && (recentCompletedJob.getPendingStatus() == Transaction.TransactionStatus.Completed)) {
                // previously completed so just complete it altogether
                if (dbTransaction.getTransType() == Transaction.TransactionType.Sale && recentCompletedJob.getPendingStatus() == Transaction.TransactionStatus.Completed) {
                    // mark item as completed with the seller & terminal info
                    transactionRepository.completeTransaction(dbTransaction.getCompanyId(),
                            dbTransaction.getId(),recentCompletedJob.getSellerId(),recentCompletedJob.getTerminalId());
                    queuedTransaction.setPendingStatus(Transaction.TransactionStatus.Completed);
                }
                LOG.info("Real transaction was previously processed as completed or canceled.");
                return InventoryProcessStatus.PreviouslyCompleted;
            }
        }

        /*for (OrderItem item : oldJobCart.getItems()) {
            objectIds.add(new ObjectId((item.getProductId())));
            productIds.add(item.getProductId());
        }

        List<String> pids = Lists.newArrayList(productIds);

        // Find all related products & quantities
        HashMap<String,Product> productHashMap = productRepository.findProductsByIdsAsMap(this.getCompanyId(),
                this.getShopId(),
                Lists.newArrayList(objectIds));
        Iterable<ProductPrepackageQuantity> prepackageQuantitiesIter = prepackageQuantityRepository.getQuantitiesForProducts(this.getCompanyId(),this.getShopId(),pids);
        //List<ProductPrepackageQuantity> prepackageQuantities = Lists.newArrayList(prepackageQuantitiesIter);
        HashMap<String,List<ProductPrepackageQuantity>> productToPrepackageQuantitiesMap = new HashMap<>();
        for (ProductPrepackageQuantity ppQty : prepackageQuantitiesIter) {
            List<ProductPrepackageQuantity> prepackageQuantityList = productToPrepackageQuantitiesMap.get(ppQty.getProductId());

            if (prepackageQuantityList == null) {
                prepackageQuantityList = new ArrayList<>();
                productToPrepackageQuantitiesMap.put(ppQty.getProductId(),prepackageQuantityList);
            }
            prepackageQuantityList.add(ppQty);
        }


        // Any prepackage quantity in here need to be persisted
        Set<ProductPrepackageQuantity> dirtyPrepackageQuantities = new HashSet<>();
        Set<Product> dirtyProducts = new HashSet<>();
        */

        if (isRefund == false) {
            // Check Inventory again
        }

        inventoryActionManager.process(queuedTransaction, dbTransaction);

        return InventoryProcessStatus.Success;
    }


    // Process Refund Requests
    private void processRefundRequest(QueuedTransaction queuedTransaction) {
        // set appropriate orders in transaction to refunded
        HashMap<String, PromotionReq> promoMap = new HashMap<>();
        HashMap<String, BigDecimal> refundAmtMap = new HashMap<>();


        RefundTransactionRequest request = refundTransactionRequestRepository.get(this.getCompanyId(),
                queuedTransaction.getTransactionId());
        if (request == null) {
            return;
        }
        Transaction dbTrans = transactionRepository.get(this.getCompanyId(), request.getTransactionId());
        if (dbTrans == null) {
            return;
        }

        if (dbTrans.isActive()) {
            return;
        }

        Shop shop = shopRepository.get(this.getCompanyId(), this.getShopId());


        Transaction refundTransaction = cartService.createRefundTransaction(shop, dbTrans, request, queuedTransaction.getTerminalId(), queuedTransaction.getSellerId());

        // update refund status in db transaction
        if (refundTransaction.getCart().getItems().size() > 0) {
            updateRefundStatus(dbTrans);
        }

        Transaction transaction = transactionRepository.save(refundTransaction);


        // IF REFUNDWITH INVENTORY, THEN PROCEED TO INVENTORY UPDATES
        processRefundInventory(refundTransaction.getStatus(), request, queuedTransaction, transaction);
    }


    //Update Database transaction refund status
    private void updateRefundStatus(Transaction dbTrans) {
        boolean status = false;
        if (dbTrans.getCart().getItems().size() > 0) {
            for (OrderItem orderItem : dbTrans.getCart().getItems()) {
                BigDecimal origQuantity = orderItem.getOrigQuantity();
                BigDecimal totalRefundQty = orderItem.getTotalRefundQty() != null ? orderItem.getTotalRefundQty() : new BigDecimal(0);
                for (RefundOrderItemRequest itemRequest : dbTrans.getCart().getRefundOrderItemRequests()) {
                    BigDecimal refundedTotalQuantity = itemRequest.getQuantity();
                    if (itemRequest.getOrderItemId().equalsIgnoreCase(orderItem.getOrderItemId())) {
                        if (origQuantity.doubleValue() > refundedTotalQuantity.doubleValue()) {
                            status = true;
                        }
                    } else {
                        if (origQuantity.doubleValue() > totalRefundQty.doubleValue()) {
                            status = true;
                        }
                    }
                }
            }
        }

        if (status) {
            dbTrans.setTransactionRefundStatus(Transaction.TransactionRefundStatus.Partial_Refund);
        } else {
            dbTrans.setTransactionRefundStatus(Transaction.TransactionRefundStatus.Complete_Refund);
        }
        transactionRepository.update(companyId, dbTrans.getId(), dbTrans);
    }


    private void processRefundInventory(Transaction.TransactionStatus newStatus,
                                        RefundTransactionRequest request,
                                        QueuedTransaction queuedTransaction,
                                        Transaction transaction) {
        Kryo kryo = new Kryo();
        if (newStatus == Transaction.TransactionStatus.RefundWithInventory) {
            Transaction newRefundTrans = kryo.copy(transaction);
            newRefundTrans.getCart().setCashReceived(new BigDecimal(0));
            newRefundTrans.getCart().setChangeDue(new BigDecimal(0));

            // marked all items in here as refunded or exchanged
            for (OrderItem orderItem : newRefundTrans.getCart().getItems()) {
                if (request.getRefundType().equals(RefundTransactionRequest.RefundType.Exchange)) {
                    orderItem.setStatus(OrderItem.OrderItemStatus.Exchange);
                } else {
                    orderItem.setStatus(OrderItem.OrderItemStatus.Refunded);
                }
            }
            queuedTransaction.setCart(newRefundTrans.getCart());
            processSaleRefundTransaction(queuedTransaction, transaction, true);
        }

    }


    // HELPER METHODS

    void processCurrentCashDrawer(final String companyId, final String shopId, final String terminalId) {
        CashDrawerSession dbLogResult = cashDrawerSessionRepository.getLatestDrawerForTerminal(companyId,
                shopId,
                terminalId);

        if (dbLogResult == null || dbLogResult.getStatus() == CashDrawerSession.CashDrawerLogStatus.Closed) {
            return;
        }

        Shop shop = shopRepository.get(companyId, shopId);
        cashDrawerProcessorService.processCurrentCashDrawer(shop, dbLogResult, false);

        cashDrawerSessionRepository.update(companyId, dbLogResult.getId(), dbLogResult);
        realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);

    }

    /**
     * This method processes purchase order for inventory updates
     *
     * @param dbQueuedTransaction : QueuedTransaction object
     * @param purchaseOrder       : PurchaseOrder object
     */
    private void processPurchaseOrder(QueuedTransaction dbQueuedTransaction, PurchaseOrder purchaseOrder) {

        inventoryActionManager.process(dbQueuedTransaction, purchaseOrder);
    }

    private void processInventoryTransfer(QueuedTransaction dbQueuedTransaction, InventoryTransferHistory history) {
        inventoryActionManager.process(dbQueuedTransaction, history);
        if (history.isCompleteTransfer()) {
            history.setStatus(InventoryTransferHistory.TransferStatus.ACCEPTED);
            history.setProcessing(Boolean.FALSE);
        }
        inventoryTransferHistoryRepository.update(this.companyId, history.getId(), history);
    }

    /**
     * This method process reconciliation history for inventory updates
     *
     * @param dbQueuedTransaction   : QueuedTransaction object
     * @param reconciliationHistory : reconciliation history object
     */
    private void processReconciliationHistory(QueuedTransaction dbQueuedTransaction, ReconciliationHistory reconciliationHistory) {
        inventoryActionManager.process(dbQueuedTransaction, reconciliationHistory);
    }

    /**
     * This method process add/update batch quantity
     *
     * @param dbQueuedTransaction : QueuedTransaction object
     * @param productBatch        : Product batch
     */
    private void processProductBatchInventory(ProductBatchQueuedTransaction dbQueuedTransaction, ProductBatch productBatch) {
        inventoryActionManager.process(dbQueuedTransaction, productBatch);
    }

    private void processBulkProductBatchInventory(ProductBatchQueuedTransaction batchQueuedTransaction) {
        inventoryActionManager.process(batchQueuedTransaction, null);
    }

    /**
     * This method process inventory for prepackage
     *
     * @param dbQueuedTransaction   : QueuedTransaction object
     * @param prepackageProductItem : prepackage product item
     */
    private void processPrepackage(PrepackageProductItemQueuedTransaction dbQueuedTransaction, PrepackageProductItem prepackageProductItem) {
        if (dbQueuedTransaction.getOperationType() != ProductBatchQueuedTransaction.OperationType.DeletePrepackage && prepackageProductItem == null) {
            return;
        }
        inventoryActionManager.process(dbQueuedTransaction, prepackageProductItem);
    }

    /**
     * This method process inventory for stock reset
     *
     * @param dbQueuedTransaction : QueuedTransaction object
     */
    private void processStockReset(QueuedTransaction dbQueuedTransaction) {
        inventoryActionManager.process(dbQueuedTransaction, null);
    }

    private void processProductInventory(ConvertToProductQueuedTransaction dbQueuedTransaction) {
        inventoryActionManager.process(dbQueuedTransaction, null);
    }

    private void processDerivedBatchesForMetrc(Transaction transaction, QueuedTransaction queuedTransaction) {
        if (queuedTransaction.getStatus() != QueuedTransaction.QueueStatus.Completed) {
            return;
        }

        if (transaction.getCart() == null || CollectionUtils.isEmpty(transaction.getCart().getItems())) {
            return;
        }
        List<ObjectId> batchIds = new ArrayList<>();
        HashMap<String, Double> productQuantityLogsMap = new HashMap<>();
        HashMap<String, Double> batchQuantityMap = new HashMap<>();

        for (OrderItem item : transaction.getCart().getItems()) {
            if (CollectionUtils.isEmpty(item.getQuantityLogs())) {
                continue;
            }
            for (QuantityLog quantityLog : item.getQuantityLogs()) {
                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                    batchIds.add(new ObjectId(quantityLog.getBatchId()));
                }
            }
            productQuantityLogsMap.put(item.getProductId(), item.getQuantity().doubleValue() + productQuantityLogsMap.getOrDefault(item.getProductId(), 0D));
            batchQuantityMap.put(item.getBatchId(), item.getQuantity().doubleValue() + productQuantityLogsMap.getOrDefault(item.getBatchId(), 0D));
        }

        List<ObjectId> derivedLogIds = new ArrayList<>();
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(transaction.getCompanyId(), batchIds);


        for (ProductBatch productBatch : productBatchHashMap.values()) {
            if (StringUtils.isNotBlank(productBatch.getDerivedLogId())) {
                derivedLogIds.add(new ObjectId(productBatch.getDerivedLogId()));
            }
        }


        Iterable<DerivedProductBatchLog> derivedProductBatchLogs = derivedProductBatchLogRepository.list(transaction.getCompanyId(), derivedLogIds);

        for (DerivedProductBatchLog derivedProductBatchLog : derivedProductBatchLogs) {
            double soldQuantity = productQuantityLogsMap.getOrDefault(derivedProductBatchLog.getDerivedProductId(), 0D);
            if (soldQuantity == 0) {
                continue;
            }
            for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : derivedProductBatchLog.getProductMap().entrySet()) {
                for (DerivedProductBatchInfo info : entry.getValue()) {
                    double infoQuantity = info.getQuantity().doubleValue() * soldQuantity;
                    batchQuantityMap.put(info.getBatchId(), infoQuantity + batchQuantityMap.getOrDefault(info.getBatchId(), 0d));
                }
            }
        }

        if (!batchQuantityMap.isEmpty()) {
            DerivedProductBatchEvent event = new DerivedProductBatchEvent();
            event.setCompanyId(queuedTransaction.getCompanyId());
            event.setShopId(queuedTransaction.getShopId());
            event.setBatchQuantityMap(batchQuantityMap);
            event.setSalesDateTime(transaction.getProcessedTime());
            event.setConsumerType(transaction.getCart().getConsumerType());
            blazeEventBus.post(event);
        }
    }

    private CustomerInfo getMemberGroup(final String memberId) {
        Member member = memberRepository.get(this.getCompanyId(), memberId);
        CustomerInfo customerInfo = new CustomerInfo();
        if (member != null) {
            MemberGroup group = memberGroupRepository.get(this.getCompanyId(), member.getMemberGroupId());
            customerInfo.setMemberGroup(group);
            customerInfo.setConsumerType(member.getConsumerType());
        }
        customerInfo.setPatient(member);
        return customerInfo;
    }

    /**
     * Private method to process revert/reject of shipping manifest
     *
     * @param queuedTransaction : queueTransaction
     */
    private void processRevertManifest(QueuedTransaction queuedTransaction) {
        Shop shop = shopRepository.get(this.getCompanyId(), this.getShopId());


        Transaction transaction = transactionRepository.getByShopAndId(shop.getCompanyId(), shop.getId(), queuedTransaction.getTransactionId());

        if (transaction == null) {
            LOG.info("Transaction not found for queue transaction:" + queueTransactionId);
            return;
        }
        // update refund status in db transaction
        if (transaction.getCart().getItems().size() > 0) {
            updateRefundStatus(transaction);
        }

        // IF REFUNDWITH INVENTORY, THEN PROCEED TO INVENTORY UPDATES
        Kryo kryo = new Kryo();
        if (transaction.getStatus() == Transaction.TransactionStatus.RefundWithInventory) {
            Transaction newRefundTrans = kryo.copy(transaction);
            newRefundTrans.getCart().setCashReceived(new BigDecimal(0));
            newRefundTrans.getCart().setChangeDue(new BigDecimal(0));

            // marked all items in here as refunded or exchanged
            for (OrderItem orderItem : newRefundTrans.getCart().getItems()) {
                orderItem.setStatus(OrderItem.OrderItemStatus.Refunded);
            }
            queuedTransaction.setCart(newRefundTrans.getCart());
            processSaleRefundTransaction(queuedTransaction, transaction, true);
        }
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getQueueTransactionId() {
        return queueTransactionId;
    }

    public void setQueueTransactionId(String queueTransactionId) {
        this.queueTransactionId = queueTransactionId;
    }
}

