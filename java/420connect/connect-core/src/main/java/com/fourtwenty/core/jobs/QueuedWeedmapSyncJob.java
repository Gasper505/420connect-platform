package com.fourtwenty.core.jobs;

import com.fourtwenty.core.domain.models.thirdparty.weedmap.WeedmapSyncJob;
import com.fourtwenty.core.domain.repositories.thirdparty.WmSyncJobRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.weedmap.WeedMapSyncEvent;
import com.fourtwenty.core.event.weedmap.WeedmapSyncResponse;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class QueuedWeedmapSyncJob implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(QueuedComplianceSyncJob.class);
    private String weedmapSyncJobId;
    private String companyId;
    private String shopId;

    @Inject
    private WmSyncJobRepository wmSyncJobRepository;
    @Inject
    private BackgroundTaskManager backgroundTaskManager;
    @Inject
    private BlazeEventBus blazeEventBus;

    @Override
    public void run() {
        if (StringUtils.isBlank(companyId) || StringUtils.isBlank(shopId)) {
            LOG.error("Company or Shop cannot be blank.");
            wmSyncJobRepository.updateJobStatus(weedmapSyncJobId, WeedmapSyncJob.WmSyncJobStatus.Error, "Company or Shop cannot be blank.");
            return;
        }

        WeedmapSyncJob syncJob = wmSyncJobRepository.getById(weedmapSyncJobId);
        if (syncJob != null && syncJob.getStatus() == WeedmapSyncJob.WmSyncJobStatus.Queued) {
            WeedmapSyncJob.WmSyncJobStatus status;
            String errorMsg = "";
            wmSyncJobRepository.markQueueJobAsInProgress(weedmapSyncJobId);
            switch (syncJob.getJobType()) {
                case MANUAL:
                    WeedmapSyncResponse response = backgroundTaskManager.syncWeedmap(companyId, shopId);
                    boolean sync = response.isSyncStatus();
                    errorMsg = response.getErrorMsg();
                    status = sync ? WeedmapSyncJob.WmSyncJobStatus.Completed : WeedmapSyncJob.WmSyncJobStatus.Error;
                    break;
                case INDIVIDUAL:
                    if (CollectionUtils.isEmpty(syncJob.getProductIds())) {
                        LOG.info("Sync products list is empty.");
                        errorMsg = "Sync products list is empty.";
                        status = WeedmapSyncJob.WmSyncJobStatus.Error;
                    } else {
                        WeedMapSyncEvent event = new WeedMapSyncEvent();
                        event.setShopId(shopId);
                        event.setCompanyId(companyId);
                        event.setProductIds(Sets.newHashSet(syncJob.getProductIds()));
                        blazeEventBus.post(event);
                        response = event.getResponse(30000);
                        errorMsg = response.getErrorMsg();
                        sync = response.isSyncStatus();
                        status = sync ? WeedmapSyncJob.WmSyncJobStatus.Completed : WeedmapSyncJob.WmSyncJobStatus.Error;
                    }
                    break;
                case RESET:
                    if (StringUtils.isBlank(syncJob.getAccountId())) {
                        LOG.info("Account id is blank in job.");
                        errorMsg = "Account id is blank in job.";
                        status = WeedmapSyncJob.WmSyncJobStatus.Error;
                    } else {
                        response = backgroundTaskManager.resetWeedmap(companyId, shopId, syncJob.getAccountId());
                        errorMsg = response.getErrorMsg();
                        sync = response.isSyncStatus();
                        status = sync ? WeedmapSyncJob.WmSyncJobStatus.Completed : WeedmapSyncJob.WmSyncJobStatus.Error;
                    }
                    break;
                default:
                    errorMsg = "Sync Job type not found";
                    status = WeedmapSyncJob.WmSyncJobStatus.Error;
                    break;
            }
            wmSyncJobRepository.updateJobStatus(syncJob.getId(), status, errorMsg);
        }
    }

    public String getWeedmapSyncJobId() {
        return weedmapSyncJobId;
    }

    public void setWeedmapSyncJobId(String weedmapSyncJobId) {
        this.weedmapSyncJobId = weedmapSyncJobId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
