package com.fourtwenty.core.jobs;

import com.fourtwenty.core.managed.TransactionJobQueueManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by mdo on 5/1/16.
 */
public class TransactionJob implements Runnable {
    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(TransactionJob.class);

    @Inject
    TransactionJobQueueManager jobQueueManager;

    private String queueTransactionId;
    private String companyId;
    private String shopId;

    public TransactionJob() {
    }


    @Override
    public void run() {
        jobQueueManager.runTransactionJob(companyId, shopId, queueTransactionId);
    }


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getQueueTransactionId() {
        return queueTransactionId;
    }

    public void setQueueTransactionId(String queueTransactionId) {
        this.queueTransactionId = queueTransactionId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}

