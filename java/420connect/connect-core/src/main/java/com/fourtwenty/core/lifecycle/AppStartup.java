package com.fourtwenty.core.lifecycle;

/**
 * Created by mdo on 9/5/15.
 */
public interface AppStartup {
    void run();
}
