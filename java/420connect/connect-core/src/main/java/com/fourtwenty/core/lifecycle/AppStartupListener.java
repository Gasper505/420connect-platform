package com.fourtwenty.core.lifecycle;

import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.google.inject.Injector;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import javax.inject.Inject;
import javax.ws.rs.ext.Provider;
import java.util.Set;

/**
 * Created by mdo on 9/7/15.
 */
@Provider
public class AppStartupListener implements ApplicationEventListener {
    @Inject
    Migration migration;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    Injector injector;
    @Inject
    Set<AppStartup> appStartupSet;


    @Override
    public void onEvent(ApplicationEvent event) {
        ApplicationEvent.Type type = event.getType();

        System.out.println("event type: " + type.name());
        if (type == ApplicationEvent.Type.INITIALIZATION_FINISHED) {
            /*if (appStartup != null) {
                appStartup.run();
            }*/
            if (appStartupSet != null) {
                for (AppStartup appStartup : appStartupSet) {
                    appStartup.run();
                }
            }

            if (migration != null) {
                migration.migrate();
            }
        }
    }

    @Override
    public RequestEventListener onRequest(RequestEvent requestEvent) {
        return null;
    }
}
