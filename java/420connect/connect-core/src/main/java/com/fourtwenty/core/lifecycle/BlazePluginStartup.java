package com.fourtwenty.core.lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.models.plugins.BlazePluginData;
import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.repositories.plugins.BlazePluginProductRepository;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class BlazePluginStartup implements AppStartup {
    private static final Logger logger = LoggerFactory.getLogger(BlazePluginStartup.class);

    private Environment environment;
    private BlazePluginProductRepository blazePluginProductRepository;

    @Inject
    public BlazePluginStartup(Environment environment, BlazePluginProductRepository blazePluginProductRepository) {
        this.environment = environment;
        this.blazePluginProductRepository = blazePluginProductRepository;
    }

    @Override
    public void run() {
        final ObjectMapper objectMapper = environment.getObjectMapper();
        final InputStream stream = BlazePluginStartup.class.getResourceAsStream("/plugin_products.json");
        try {
            final BlazePluginData blazePluginData = objectMapper.readValue(stream, BlazePluginData.class);
            List<BlazePluginProduct> persistedPluginProducts = blazePluginProductRepository.listNonDeleted();
            final List<BlazePluginProduct> pluginProducts = new ArrayList<>();
            pluginProducts.addAll(blazePluginData.getMessagingProducts());
            pluginProducts.addAll(blazePluginData.getConsumerKioskProducts());
            pluginProducts.addAll(blazePluginData.getTvProducts());

            for (BlazePluginProduct pluginProduct : pluginProducts) {
                if (persistedPluginProducts.contains(pluginProduct)) {
                    blazePluginProductRepository.update(persistedPluginProducts.get(persistedPluginProducts.indexOf(pluginProduct)).getId(), pluginProduct);
                    persistedPluginProducts.remove(pluginProduct);
                } else {
                    pluginProduct.prepare();
                    blazePluginProductRepository.save(pluginProduct);
                }
            }

            // Clean up for deleted plugin products
            blazePluginProductRepository.deletePluginProducts(persistedPluginProducts);

        } catch (Exception ex) {
            logger.warn("Error while updating plugin products [Error : {}]", ex.getMessage());
            throw new BlazeOperationException(ex);
        }
    }
}
