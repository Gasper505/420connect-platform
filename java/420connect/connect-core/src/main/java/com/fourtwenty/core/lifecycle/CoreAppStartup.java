package com.fourtwenty.core.lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.lifecycle.model.StartupData;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.mgmt.DefaultDataService;
import com.fourtwenty.core.util.SecurityUtil;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashSet;

/**
 * Created by mdo on 9/5/15.
 */
public class CoreAppStartup implements AppStartup {
    private static final Logger LOG = LoggerFactory.getLogger(CoreAppStartup.class);
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    Environment environment;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    DefaultDataService defaultDataService;
    @Inject
    RoleRepository roleRepository;
    @Inject
    AppRepository appRepository;
    @Inject
    ConnectProductRepository connectProductRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    ConnectConfiguration connectConfiguration;


    // public static QBSyncService qbSyncService=QBSyncService.getInstance();


    @Override
    public void run() {
        Bootstrap<ConnectConfiguration> bootstrap;
        LOG.info("CoreAppStartup is starting up..");

        if (connectConfiguration.getPlatformModeConfiguration() != null
                && connectConfiguration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem) {
            LOG.info("On-Prem mode detected and hence default data will not be available in OnPrem mode");
        } else {
            App app = appRepository.getAppByName("Connect App");
            if (app == null) {
                app = new App();
                app.setName("Connect App");
                app.setVersion(1);
                appRepository.save(app);
            }


            final ObjectMapper objectMapper = environment.getObjectMapper();
            InputStream stream = CoreAppStartup.class.getResourceAsStream("/testdata2.json");

            try {
                StartupData data = objectMapper.readValue(stream, StartupData.class);
                HashMap<String, MemberGroup> memberGroupHashMap = new HashMap<>();
                if (companyRepository.count() == 0) {
                    for (Company company : data.getCompanyList()) {

                        if (company.getAddress() != null) {
                            company.getAddress().prepare();
                        }

                        companyRepository.upsert(company.getId(), company);
                        defaultDataService.initializeDefaultCompanyData(company.getId(), null,null);
                    }
                }

                if (shopRepository.count() == 0) {
                    for (Shop shop : data.getShops()) {
                        if (shop.getAddress() != null) {
                            shop.getAddress().prepare();
                        }
                        shop.setAppTarget(CompanyFeatures.AppTarget.Retail);
                        shopRepository.upsert(shop.getId(), shop);
                        defaultDataService.initializeDefaultShopData(shop, memberGroupHashMap, true, null);
                    }
                }

                if (employeeRepository.count() == 0) {

                    LinkedHashSet<CompanyFeatures.AppTarget> appTargets = new LinkedHashSet<>();
                    appTargets.add(CompanyFeatures.AppTarget.Retail);
                    appTargets.add(CompanyFeatures.AppTarget.AuthenticationApp);

                    Iterable<Role> roles = roleRepository.list();
                    for (Employee employee : data.getEmployees()) {
                        for (Role role : roles) {
                            if (role.getCompanyId().equalsIgnoreCase(employee.getCompanyId())
                                    && role.getName().equalsIgnoreCase("Admin")) {
                                employee.setRoleId(role.getId());
                                employee.setAppAccessList(appTargets);
                                break;
                            }
                        }
                        employee.setPassword(securityUtil.encryptPassword(employee.getPassword()));
                    }
                    employeeRepository.save(data.getEmployees());
                }


                if (memberRepository.count() == 0) {
                    for (Member member : data.getMembers()) {
                        if (member.getAddress() != null) {
                            member.getAddress().prepare();
                        }
                        MemberGroup memberGroup = memberGroupHashMap.get(member.getCompanyId());
                        if (memberGroup != null) {
                            member.setMemberGroupId(memberGroup.getId());
                            member.setMemberGroup(memberGroup);
                        }
                    }
                    memberRepository.save(data.getMembers());
                }


            /*Iterable<Member> badMembers = memberRepository.getMembersWithoutStartDate();
            for (Member member : badMembers) {
                if (member.getStartDate() == null || member.getStartDate() == 0) {
                    member.setStartDate(member.getCreated());
                    memberRepository.update(member.getId(), member);
                }
            }*/


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        republishQueuedJobs();
    }

    private void republishQueuedJobs() {
        if (connectConfiguration.getProcessorConfig() != null && connectConfiguration.getProcessorConfig().isCanRepublish()) {
            LOG.info("Re-publishing ENABLED");
            Iterable<QueuedTransaction> queuedTransactions = queuedTransactionRepository.getQueuedTransactions(QueuedTransaction.QueueStatus.Pending);
            for (QueuedTransaction qt : queuedTransactions) {
                LOG.info("Re-publishing: " + qt.getId());
                backgroundJobService.addTransactionJob(qt.getCompanyId(), qt.getShopId(), qt.getId());
            }
        } else {
            LOG.info("Re-publishing DISABLED");
        }
    }

}
