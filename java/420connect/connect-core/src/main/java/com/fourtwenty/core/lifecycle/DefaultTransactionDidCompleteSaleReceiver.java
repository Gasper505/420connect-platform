package com.fourtwenty.core.lifecycle;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.services.mgmt.LoyaltyActivityService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mdo on 9/21/17.
 */
public class DefaultTransactionDidCompleteSaleReceiver implements TransactionDidCompleteSaleReceiver {
    @Inject
    CompanyRepository companyRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    LoyaltyActivityService activityService;
    @Inject
    ProductRepository productRepository;

    private static final Log LOG = LogFactory.getLog(DefaultTransactionDidCompleteSaleReceiver.class);

    @Override
    public void run(Transaction transaction) {
        Company company = companyRepository.getById(transaction.getCompanyId());
        LOG.info(String.format("Sale transaction did complete for: %s,  company: %s", transaction.getTransNo(), company.getName()));


        HashSet<ObjectId> productIds = new HashSet<>();
        for (OrderItem orderItem : transaction.getCart().getItems()) {
            productIds.add(new ObjectId(orderItem.getProductId()));
        }

        Iterable<Product> productList = productRepository.list(company.getId(), Lists.newArrayList(productIds));

        List<String> recentProducts = new ArrayList<>();
        for (Product p : productList) {
            recentProducts.add(p.getName());
        }

        if (recentProducts.size() > 0) {
            LOG.info(String.format("Updating recent member products: %s", transaction.getMemberId()));
            memberRepository.updateRecentBought(company.getId(), transaction.getMemberId(), recentProducts);
        }

        // Update points
        LoyaltyActivityLog loyaltyActivityLog = activityService.addLoyaltyPoints(company.getId(), transaction);
        if (loyaltyActivityLog != null) {
            transaction.setPointsEarned(loyaltyActivityLog.getAmount());
            transactionRepository.updatePointsEarned(company.getId(), transaction.getId(), loyaltyActivityLog.getAmount());
        }
    }
}
