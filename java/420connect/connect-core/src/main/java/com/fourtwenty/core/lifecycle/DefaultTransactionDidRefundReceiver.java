package com.fourtwenty.core.lifecycle;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mdo on 11/20/17.
 */
public class DefaultTransactionDidRefundReceiver implements TransactionDidRefundReceiver {
    @Inject
    CompanyRepository companyRepository;

    private static final Log LOG = LogFactory.getLog(DefaultTransactionDidCompleteSaleReceiver.class);

    @Override
    public void run(Transaction transaction) {
        Company company = companyRepository.getById(transaction.getCompanyId());
        LOG.info(String.format("Sale transaction did refunded for: %s,  company: %s", transaction.getTransNo(), company.getName()));
    }
}
