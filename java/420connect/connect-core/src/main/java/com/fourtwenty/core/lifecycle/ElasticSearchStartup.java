package com.fourtwenty.core.lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.request.AWSMappingRequest;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSMappingResponse;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchCommunicatorService;
import com.google.inject.Injector;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ElasticSearchStartup implements AppStartup {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchStartup.class);

    @Inject
    private Injector injector;

    @Inject
    private ElasticSearchCommunicatorService elasticSearchCommunicatorService;

    @Inject
    MemberRepository memberRepository;

    @Override
    public void run() {
        createElasticSearchIndexes();
    }

    private void createElasticSearchIndexes() {
        final Reflections reflections = new Reflections("com.fourtwenty.core");
        final ObjectMapper objectMapper = new ObjectMapper();

        Set<Class<? extends ElasticSearchCapable>> classes =
                reflections.getSubTypesOf(ElasticSearchCapable.class);
        for (Class<?> clazz : classes) {

            // Pull Index mappings from ElasticSearchCapable class
            ElasticSearchIndex elasticSearchIndex = null;
            try {
                ElasticSearchCapable definition = (ElasticSearchCapable) injector.getInstance(clazz);
                elasticSearchIndex = definition.getElasticSearchIndex();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (elasticSearchIndex == null)
                continue;

            boolean canUpdate = false;
            AWSMappingResponse awsMappingResponse = null;
            try {
                awsMappingResponse = elasticSearchCommunicatorService.getIndexMappings(elasticSearchIndex.getIndex(), elasticSearchIndex.getType());
                awsMappingResponse.load();
                canUpdate = true;
            } catch (Exception e) {
                LOGGER.debug("", e);
                LOGGER.info("Elastic Index {} not found", elasticSearchIndex.getIndex());
            }

            final AWSMappingRequest mappingRequest = new AWSMappingRequest();
            mappingRequest.setProperties(elasticSearchIndex.getProperties());
            final Map<String, Object> mappings = new HashMap<>();
            mappings.put(elasticSearchIndex.getType(), mappingRequest);

            /*
            try {
                elasticSearchCommunicatorService.deleteIndex(elasticSearchIndex.getIndex());
                canUpdate = false;
            } catch (Exception e) {
                LOGGER.error("Error deleting index : " + elasticSearchIndex.getIndex(),e.getMessage());
            }*/
            if (canUpdate) {
                String indexJson = null;
                try {
                    // Existing mappings can not be reverted, so only new mappings will be updated
                    if (awsMappingResponse.getProperties() != null) {
                        final AWSMappingResponse efAwsMappingResponse = awsMappingResponse;
                        Map<String, FieldIndex> filteredMappings = mappingRequest.getProperties().entrySet().stream().filter(key -> !efAwsMappingResponse.getProperties().keySet().contains(key.getKey())).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

                        LOGGER.info("Updating index: ", filteredMappings);
                        if (!filteredMappings.isEmpty()) {
                            mappingRequest.setProperties(filteredMappings);
                            indexJson = objectMapper.writeValueAsString(mappingRequest);

                            LOGGER.info("updating indexing", indexJson);
                            if (StringUtils.isNotBlank(indexJson)) {
                                elasticSearchCommunicatorService.updateIndex(elasticSearchIndex.getIndex(), elasticSearchIndex.getType(), indexJson);
                            }
                        }
                    } else {
                        LOGGER.info("current mappings are not available for index {}, skipping", elasticSearchIndex.getIndex());
                    }
                } catch (Exception e) {
                    //throw new BlazeOperationException("Cannot update elastic mapping for index : " + elasticSearchIndex.getIndex());
                    LOGGER.error("Cannot create elastic mapping for index : " + elasticSearchIndex.getIndex(), e.getMessage());
                    LOGGER.error(indexJson);
                }
            } else {
                final Map<String, Object> mappingsWithRootNode = new HashMap<>();
                mappingsWithRootNode.put("mappings", mappings);
                String indexJson = null;
                try {
                    indexJson = objectMapper.writeValueAsString(mappingsWithRootNode);
                    LOGGER.info("creating indexing", indexJson);
                    elasticSearchCommunicatorService.createIndex(elasticSearchIndex.getIndex(), indexJson);
                } catch (Exception e) {
                    LOGGER.error("Cannot create elastic mapping for index : " + elasticSearchIndex.getIndex(), e.getMessage());
                    LOGGER.error(indexJson);
                }
            }

        }
    }

}
