package com.fourtwenty.core.lifecycle;

/**
 * Created by mdo on 7/12/16.
 */
public interface Migration {
    void migrate();
}
