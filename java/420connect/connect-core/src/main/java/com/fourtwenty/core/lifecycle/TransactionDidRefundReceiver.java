package com.fourtwenty.core.lifecycle;

/**
 * Created by mdo on 11/20/17.
 * <p>
 * Any class that implements "TransactionCompleteReceiver" will be executed when a Transaction is completed.
 * <p>
 * In order for this to work, you must bind your implementation class using Guice.
 */
public interface TransactionDidRefundReceiver extends TransactionReceiver {
}
