package com.fourtwenty.core.lifecycle;

import com.fourtwenty.core.domain.models.transaction.Transaction;

/**
 * Created by mdo on 11/20/17.
 */
public interface TransactionReceiver {
    void run(final Transaction transaction);
}
