package com.fourtwenty.core.lifecycle;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.google.inject.Inject;
import com.twilio.Twilio;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TwilioStartup implements AppStartup {
    private static final Log LOG = LogFactory.getLog(TwilioStartup.class);
    @Inject
    private ConnectConfiguration configuration;

    @Override
    public void run() {
        if (this.configuration.getTwilioConfiguration() != null) {
            try {
                Twilio.init(this.configuration.getTwilioConfiguration().getAccountSid(), this.configuration.getTwilioConfiguration().getAuthToken());
            } catch (Exception e) {
                LOG.error("Error in twilio Initialization ", e);
            }

        }
    }
}
