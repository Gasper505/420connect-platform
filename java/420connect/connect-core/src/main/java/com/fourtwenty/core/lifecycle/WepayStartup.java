package com.fourtwenty.core.lifecycle;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.google.inject.Inject;
import com.wepay.WePay;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 12/9/17 12:48 PM by Raja Dushyant Vashishtha
 * Sr. Software Engineer
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */

public class WepayStartup implements AppStartup {
    private static final Log LOG = LogFactory.getLog(WepayStartup.class);
    @Inject
    private ConnectConfiguration configuration;

    @Override
    public void run() {
        //Wepay Initialization
        if (this.configuration.getWepayConfiguration() != null) {
            try {
                WePay.initialize(this.configuration.getWepayConfiguration().getClientId(),
                        this.configuration.getWepayConfiguration().getClientSecret(),
                        this.configuration.getWepayConfiguration().getStageEnabled());
            } catch (Exception e) {
                LOG.error("Issue initiating WePay", e);
            }
        }
    }
}
