package com.fourtwenty.core.lifecycle.metrc;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.lifecycle.TransactionDidCompleteSaleReceiver;
import com.fourtwenty.core.services.compliance.metrc.MetrcProcessingService;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MetrcTransactionDidCompleteSaleReceiverImpl implements TransactionDidCompleteSaleReceiver {
    private static final Log LOG = LogFactory.getLog(MetrcTransactionDidCompleteSaleReceiverImpl.class);
    @Inject
    MetrcProcessingService metrcProcessingService;


    @Override
    public void run(Transaction transaction) {
        metrcProcessingService.submitUpdateReceipt(transaction, false);
    }


}
