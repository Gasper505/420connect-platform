package com.fourtwenty.core.lifecycle.metrc;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.lifecycle.TransactionDidRefundReceiver;
import com.fourtwenty.core.services.compliance.metrc.MetrcProcessingService;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mdo on 2/22/18.
 */
public class MetrcTransactionDidRefundReceiverImpl implements TransactionDidRefundReceiver {
    private static final Log LOG = LogFactory.getLog(MetrcTransactionDidRefundReceiverImpl.class);
    @Inject
    MetrcProcessingService metrcProcessingService;


    @Override
    public void run(Transaction transaction) {
        if (transaction.getMetrcId() != null) {
            metrcProcessingService.submitUpdateReceipt(transaction, false);
        } else {
            LOG.info("This transaction was not previously submitted to METRC.");
        }
    }
}
