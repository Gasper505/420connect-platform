package com.fourtwenty.core.lifecycle.model;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;

/**
 * Created by mdo on 4/26/16.
 */
public class CategoryInfo {
    private String name;
    private boolean cannabis;
    private String imageURL;
    private String unitType;
    private CompanyFeatures.AppTarget appTarget;

    public boolean isCannabis() {
        return cannabis;
    }

    public void setCannabis(boolean cannabis) {
        this.cannabis = cannabis;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }
}
