package com.fourtwenty.core.lifecycle.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;

import java.util.List;

/**
 * Created by mdo on 10/25/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultData {
    @JsonProperty("productTolerances") // Shop
    private List<ProductWeightTolerance> productTolerances;
    @JsonProperty("taxInfos") // Shop
    private List<TaxInfo> taxInfos;
    @JsonProperty("productCategories") // Shop
    private List<CategoryInfo> productCategories;
    @JsonProperty("memberGroups") // Company
    private List<MemberGroup> memberGroups;
    @JsonProperty("inventories")
    private List<Inventory> inventories;
    @JsonProperty("roles")
    private List<Role> roles;

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }

    public List<ProductWeightTolerance> getProductTolerances() {
        return productTolerances;
    }

    public void setProductTolerances(List<ProductWeightTolerance> productTolerances) {
        this.productTolerances = productTolerances;
    }

    public List<TaxInfo> getTaxInfos() {
        return taxInfos;
    }

    public void setTaxInfos(List<TaxInfo> taxInfos) {
        this.taxInfos = taxInfos;
    }

    public List<CategoryInfo> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(List<CategoryInfo> productCategories) {
        this.productCategories = productCategories;
    }

    public List<MemberGroup> getMemberGroups() {
        return memberGroups;
    }

    public void setMemberGroups(List<MemberGroup> memberGroups) {
        this.memberGroups = memberGroups;
    }
}
