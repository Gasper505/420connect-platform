package com.fourtwenty.core.lifecycle.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 8/9/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GlobalData {
    @JsonProperty("connectProducts") // Shop
    private List<ConnectProduct> connectProducts = new ArrayList<>();

    public List<ConnectProduct> getConnectProducts() {
        return connectProducts;
    }

    public void setConnectProducts(List<ConnectProduct> connectProducts) {
        this.connectProducts = connectProducts;
    }
}
