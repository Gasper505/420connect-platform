package com.fourtwenty.core.lifecycle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class IndexData {
    @JsonProperty("collectionName")
    private String collectionName;
    @JsonProperty("indexes")
    private List<String> indexes;
    @JsonProperty("uniqueIndexes")
    private List<String> uniqueIndexes;
    @JsonProperty("create")
    private boolean create;
    @JsonProperty("deleteIndexes")
    private List<String> deleteIndexes;

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public List<String> getIndexes() {
        return indexes;
    }

    public void setIndexes(List<String> indexes) {
        this.indexes = indexes;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public List<String> getUniqueIndexes() {
        return uniqueIndexes;
    }

    public void setUniqueIndexes(List<String> uniqueIndexes) {
        this.uniqueIndexes = uniqueIndexes;
    }

    public List<String> getDeleteIndexes() {
        return deleteIndexes;
    }

    public void setDeleteIndexes(List<String> deleteIndexes) {
        this.deleteIndexes = deleteIndexes;
    }
}
