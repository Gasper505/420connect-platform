package com.fourtwenty.core.lifecycle.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IndexesData {
    @JsonProperty("indices")
    private List<IndexData> indexesData;
    private boolean create;

    public List<IndexData> getIndexesData() {
        return indexesData;
    }

    public void setIndexesData(List<IndexData> indexesData) {
        this.indexesData = indexesData;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }
}
