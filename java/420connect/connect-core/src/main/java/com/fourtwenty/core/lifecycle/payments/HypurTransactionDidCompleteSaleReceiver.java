package com.fourtwenty.core.lifecycle.payments;

import com.blaze.clients.hypur.models.HypurPayment;
import com.blaze.clients.hypur.models.HypurRefund;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyShopAccountRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.lifecycle.TransactionDidCompleteSaleReceiver;
import com.fourtwenty.core.services.thirdparty.HypurService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * Created by mdo on 9/24/17.
 */
public class HypurTransactionDidCompleteSaleReceiver implements TransactionDidCompleteSaleReceiver {
    private static final Log LOG = LogFactory.getLog(HypurTransactionDidCompleteSaleReceiver.class);

    @Inject
    HypurService hypurService;
    @Inject
    ThirdPartyShopAccountRepository thirdPartyShopAccountRepository;
    @Inject
    TerminalRepository terminalRepository;


    @Override
    public void run(final Transaction transaction) {
        if (transaction.getCart() != null && transaction.getCart().getPaymentOption() == Cart.PaymentOption.Hypur) {
            // Only run this if it's a Hypur transaction
            LOG.info("Hypur payment option found!");
            ThirdPartyShopAccount thirdPartyShopAccount = thirdPartyShopAccountRepository.getTPShopAccount(transaction.getCompanyId(),
                    transaction.getShopId(), ThirdPartyShopAccount.ThirdPartyShopAccountType.Hypur);

            if (!thirdPartyShopAccount.isEnabled()) {
                throw new BlazeInvalidArgException("Hypur", "Account is not Enabled");
            }
            Note note = transaction.getNote();
            if (transaction.getTransType() == Transaction.TransactionType.Sale) {
                HypurPayment hypurPayment = new HypurPayment();
                hypurPayment.setEmployeeId(Integer.valueOf(transaction.getMemberId()));
                hypurPayment.setMerchantId(Integer.valueOf(thirdPartyShopAccount.getId()));
                hypurPayment.setCheckInISN(Integer.valueOf(transaction.getHypurCheckinISN()));
                hypurPayment.setFromEntityISN(Integer.valueOf(thirdPartyShopAccount.getEntityISN()));                                            //TODO: set value
                hypurPayment.setPAC(transaction.getHypurPin());
                hypurPayment.setAmount(transaction.getCart().getTotal().doubleValue());
                if (note != null) {
                    hypurPayment.setNote(note.getMessage());
                } else {
                    hypurPayment.setNote("");
                }
                Terminal terminal = terminalRepository.get(transaction.getCompanyId(), transaction.getTerminalId());

                hypurPayment.setDeviceOS(terminal.getDeviceVersion());
                hypurPayment.setTransactionTypeId(9);

                hypurService.makeHypurPayment(hypurPayment);

            } else if (transaction.getTransType() == Transaction.TransactionType.Refund) {
                HypurRefund hypurRefund = new HypurRefund();

                hypurRefund.setTransactionId(Integer.valueOf(transaction.getId()));
                hypurRefund.setAmount(transaction.getCart().getTotal().doubleValue());
//                hypurRefund.setInvoiceNumber();                                     //Not Required
                if (note != null) {
                    hypurRefund.setNote(note.getMessage());
                } else {
                    hypurRefund.setNote("");
                }

                hypurService.refundHypurPayment(hypurRefund);
            }
        }
    }
}
