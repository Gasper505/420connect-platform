package com.fourtwenty.core.lifecycle.refunds;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.lifecycle.TransactionDidRefundReceiver;
import com.fourtwenty.core.services.mgmt.LoyaltyActivityService;
import com.google.inject.Inject;

/**
 * Created by mdo on 11/21/17.
 */
public class RewardsTransactionRefundReceiver implements TransactionDidRefundReceiver {

    @Inject
    LoyaltyActivityService activityService;

    @Override
    public void run(Transaction transaction) {
        activityService.refundLoyaltyPoints(transaction.getCompanyId(), transaction);
    }
}
