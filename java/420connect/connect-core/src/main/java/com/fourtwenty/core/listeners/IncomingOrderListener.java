package com.fourtwenty.core.listeners;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.SwitchableApplication;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.orders.IncomingOrderEvent;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;

public class IncomingOrderListener implements BlazeSubscriber {
    private static final Log LOG = LogFactory.getLog(IncomingOrderListener.class);

    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    NotificationInfoRepository notificationInfoRepository;
    @Inject
    ConnectConfiguration configuration;


    @Subscribe
    public void subscribe(IncomingOrderEvent event) {

        if (event.getShop() != null) {
            ConsumerCart consumerCart = event.getConsumerCart();
            Shop shop = event.getShop();
            ConsumerUser consumerUser = event.getConsumerUser();

            String body = null;
            String title = null;
            if (consumerUser.isAccepted() && StringUtils.isNotBlank(consumerUser.getMemberId())) {
                body = getNewOrderReceipt(consumerCart, consumerUser, shop);
                title = "You have a new online order!";
            } else {
                body = getNewMemberReceipt(consumerCart, consumerUser, shop);
                title = "New Member Notification - Action Required";
            }

            /*amazonServiceManager.sendEmail("support@blaze.me", shop.getEmailAdress(), title, body, null, null, "Blaze Support");

            NotificationInfo notificationInfo = notificationInfoRepository.getByShopAndType(shop.getCompanyId(),shop.getId(),NotificationInfo.NotificationType.Incoming_Order);
            if (notificationInfo != null && StringUtils.isNotBlank(notificationInfo.getDefaultNumber())) {

            }*/

            NotificationInfo incomingOrderNotifiction =  notificationInfoRepository.getByShopAndType(shop.getCompanyId(), shop.getId(), NotificationInfo.NotificationType.Incoming_Order);

            if (incomingOrderNotifiction != null && incomingOrderNotifiction.isActive()) {
                LOG.info("Notifying store of orders..");
                if (StringUtils.isNotBlank(incomingOrderNotifiction.getDefaultEmail())) {
                    String[] emails = incomingOrderNotifiction.getDefaultEmail().split(",");
                    LOG.info("Sending notification to: " + incomingOrderNotifiction.getDefaultEmail());
                    for (String email : emails) {
                        try {
                            amazonServiceManager.sendEmail("support@blaze.me", email, title, body, null, null, "Blaze Support");
                        } catch (Exception e) {
                            LOG.error("Could not notify order",e);
                        }
                    }
                }

                if (StringUtils.isNotBlank(incomingOrderNotifiction.getDefaultNumber())) {
                    String[] phone = incomingOrderNotifiction.getDefaultNumber().split(",");

                    LOG.info("Sending notification to: " + incomingOrderNotifiction.getDefaultNumber());
                    amazonServiceManager.sendSMSMessage(Lists.newArrayList(phone),
                            String.format("Receive a new '%s' order totaling $%.2f.", consumerCart.getPickupType(), consumerCart.getCart().getTotal().doubleValue()),shop);
                }
            }
        }
    }

    //Helper
    private String getNewMemberReceipt(final ConsumerCart consumerCart, final ConsumerUser consumerUser, final Shop shop) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/new_member_alert.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        SwitchableApplication applicationConfig = configuration.getBlazeApplicationsConfig().getRedirectURL("Retail");

        String theString = writer.toString();

        theString = theString.replaceAll(Pattern.quote("{{shop_name}}"), shop.getName());

        theString = theString.replaceAll(Pattern.quote("{{member_name}}"), consumerUser.getFirstName() + " " + consumerUser.getLastName());
        theString = theString.replaceAll(Pattern.quote("{{member_phone}}"), consumerUser.getPrimaryPhone());
        theString = theString.replaceAll(Pattern.quote("{{member_address}}"), consumerUser.getAddress() != null ? consumerUser.getAddress().getAddressString() : "");
        theString = theString.replaceAll(Pattern.quote("{{consumer_type}}"), consumerUser.getConsumerType().getDisplayName());


        if (consumerCart != null) {
            theString = theString.replaceAll(Pattern.quote("{{order_status}}"), consumerCart.getCartStatus().name());
            theString = theString.replaceAll(Pattern.quote("{{order_placed_time}}"),
                    DateUtil.toDateTimeFormatted(consumerCart.getOrderPlacedTime(), shop.getTimeZone()));
            theString = theString.replaceAll(Pattern.quote("{{total_order_amount}}"),
                    TextUtil.toEscapeCurrency(consumerCart.getCart().getTotal().doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll(Pattern.quote("{{orderSource}}"), consumerCart.getSource());
        } else {
            theString = theString.replaceAll(Pattern.quote("{{order_status}}"), "");
            theString = theString.replaceAll(Pattern.quote("{{order_placed_time}}"), "");
            theString = theString.replaceAll(Pattern.quote("{{total_order_amount}}"), "");
            theString = theString.replaceAll(Pattern.quote("{{orderSource}}"), "");
        }

        theString = theString.replaceAll(Pattern.quote("{{action_url}}"), ((applicationConfig != null) ? applicationConfig.getAppLink() : "") + "/pos?activeTab=incomingOrders&shopId=" + shop.getId());

        return theString;
    }

    private String getNewOrderReceipt(final ConsumerCart consumerCart, final ConsumerUser consumerUser, final Shop shop) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/new_order_alert.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        SwitchableApplication applicationConfig = configuration.getBlazeApplicationsConfig().getRedirectURL("Retail");

        String theString = writer.toString();

        theString = theString.replaceAll(Pattern.quote("{{shop_name}}"), shop.getName());
        theString = theString.replaceAll(Pattern.quote("{{member_name}}"), consumerUser.getFirstName() + " " + consumerUser.getLastName());
        theString = theString.replaceAll(Pattern.quote("{{member_phone}}"), consumerUser.getPrimaryPhone());
        theString = theString.replaceAll(Pattern.quote("{{member_address}}"), consumerUser.getAddress() != null ? consumerUser.getAddress().getAddressString() : "");
        theString = theString.replaceAll(Pattern.quote("{{consumer_type}}"), consumerUser.getConsumerType().getDisplayName());

        theString = theString.replaceAll(Pattern.quote("{{order_status}}"), consumerCart.getCartStatus().name());
        theString = theString.replaceAll(Pattern.quote("{{order_placed_time}}"),
                DateUtil.toDateTimeFormatted(consumerCart.getOrderPlacedTime(), shop.getTimeZone()));
        theString = theString.replaceAll(Pattern.quote("{{total_order_amount}}"),
                TextUtil.toEscapeCurrency(consumerCart.getCart().getTotal().doubleValue(), shop.getDefaultCountry()));
        theString = theString.replaceAll(Pattern.quote("{{action_url}}"), ((applicationConfig != null) ? applicationConfig.getAppLink() : "") + "/pos?activeTab=incomingOrders&shopId=" + shop.getId());

        if (shop != null && !StringUtils.isBlank(shop.getEmailMessage())) {
            theString = theString.replaceAll("==shopMessage==", shop.getEmailMessage());
        } else {
            theString = theString.replaceAll("==shopMessage==", "");
        }

        theString = theString.replaceAll(Pattern.quote("{{orderSource}}"), consumerCart.getSource());
        theString = theString.replaceAll(Pattern.quote("{{showSource}}"), StringUtils.isNotBlank(consumerCart.getSource()) ? "" : "display:none;");
        return theString;
    }

}
