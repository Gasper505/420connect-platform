package com.fourtwenty.core.listeners;

import com.blaze.clients.metrcs.models.items.MetricsItems;
import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.compliance.ComplianceItem;
import com.fourtwenty.core.domain.models.compliance.CompliancePackage;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.CultivationTaxResult;
import com.fourtwenty.core.domain.models.transaction.ProductBatchQueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceItemRepository;
import com.fourtwenty.core.domain.repositories.compliance.CompliancePackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.event.BlazeSubscriber;
import com.fourtwenty.core.event.compliance.IntakeCompliancePackageEvent;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.QrCodeUtil;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

public class InventoryServiceListener implements BlazeSubscriber {
    private static final Log LOG = LogFactory.getLog(InventoryServiceListener.class);

    @Inject
    ProductRepository productRepository;
    @Inject
    ComplianceItemRepository complianceItemRepository;
    @Inject
    CompliancePackageRepository compliancePackageRepository;
    @Inject
    BrandRepository brandRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    TaxCalulationService taxCalulationService;
    @Inject
    AmazonS3Service amazonS3Service;
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    BarcodeService barcodeService;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ElasticSearchManager elasticSearchManager;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    BatchActivityLogRepository batchActivityLogRepository;


    @Subscribe
    public void onAddBatchRequest(final IntakeCompliancePackageEvent intakeRequest) {
        final String companyId = intakeRequest.getCompanyId();
        final String shopId = intakeRequest.getShopId();
        final Shop shop = shopRepository.get(companyId,shopId);

        CompliancePackage compliancePackage = intakeRequest.getCompliancePackage();
        Product dbProduct = productRepository.get(companyId,intakeRequest.getProductId());
        ProductCategory productCategory = categoryRepository.get(dbProduct.getCompanyId(),dbProduct.getCategoryId());
        Vendor vendor = vendorRepository.get(dbProduct.getCompanyId(),dbProduct.getVendorId());

        ProductBatch productBatch = new ProductBatch();
        productBatch.prepare(companyId);
        productBatch.setProductId(intakeRequest.getProductId());
        productBatch.setShopId(shopId);
        productBatch.setSku(compliancePackage.getData().getLabel());
        if (StringUtils.isNotBlank(compliancePackage.getData().getLabel())) {
            productBatch.setSku(compliancePackage.getData().getLabel());
        }

        ComplianceItem complianceItem = complianceItemRepository.getComplianceItemByKey(companyId,shopId,compliancePackage.getData().getProductName());

        if (complianceItem != null && complianceItem.getData() != null) {
            MetricsItems metricsItem = complianceItem.getData();
            productBatch.setThc(NumberUtils.round(metricsItem.getUnitThcPercent()));
            productBatch.setCbd(NumberUtils.round(metricsItem.getUnitCbdPercent()));
            productBatch.setCbn(0);
            productBatch.setCbda(0);
            productBatch.setThca(0);

            if (metricsItem.getUnitThcContent() != null && metricsItem.getUnitThcContent().doubleValue() > 0) {
                PotencyMG potencyMG = new PotencyMG();
                potencyMG.setThc(metricsItem.getUnitThcContent());
                potencyMG.setCbd(metricsItem.getUnitCbdContent());

                productBatch.setPotencyAmount(potencyMG);
            }
        }
        // receiveDate
        // PackagedDate
        DateTime packageDate = DateTime.now();
        if (StringUtils.isNotBlank(compliancePackage.getData().getPackagedDate())) {
            packageDate = DateUtil.parseStrDateToDateTimeForPattern(compliancePackage.getData().getPackagedDate(),"yyyy-MM-dd");
            productBatch.setPurchasedDate(packageDate.getMillis());
            productBatch.setTrackHarvestDate(compliancePackage.getData().getPackagedDate());
        }

        productBatch.setVendorId(dbProduct.getVendorId());
        productBatch.setCost(intakeRequest.getCostPerUnit().multiply(compliancePackage.getBlazeQuantity())); //NumberUtils.round(request.getSentCost(),2));
        productBatch.setCostPerUnit(intakeRequest.getCostPerUnit());
        productBatch.setQuantity(compliancePackage.getBlazeQuantity()); //NumberUtils.round(request.getQuantity(),2));
        productBatch.setTrackHarvestBatch(compliancePackage.getData().getLabel());
        productBatch.setReferenceNote(null);
        productBatch.setFlowerSourceType(null);
        productBatch.setExpirationDate(intakeRequest.getExpirationDate() != null ? intakeRequest.getExpirationDate() : 0);
        productBatch.setSellBy(intakeRequest.getSellBy() != null ? intakeRequest.getSellBy() : 0);

        if (StringUtils.isNotBlank(dbProduct.getBrandId())) {
            Brand brand = brandRepository.get(companyId, dbProduct.getBrandId());
            if (brand != null) {
                productBatch.setBrandId(dbProduct.getBrandId());
            }
        }
        productBatch.setReceiveDate(packageDate.getMillis());


        productBatch.setStatus(ProductBatch.BatchStatus.READY_FOR_SALE);
        if (productBatch.getQuantity() == null || productBatch.getQuantity().doubleValue() < 0) {
            productBatch.setQuantity(new BigDecimal(0));
        }


        CompanyLicense companyLicense = null;

        companyLicense = new CompanyLicense();
        companyLicense.setCompanyType(Vendor.CompanyType.RETAILER);
        Vendor.ArmsLengthType armsLengthType = (vendor != null) ? vendor.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;

        //OZ tax calculation
        if ((shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) && (Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && (PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT.equals(productBatch.getFlowerSourceType()))) {
            CultivationTaxResult totalCultivationTax = null;
            totalCultivationTax = taxCalulationService.calculateOzTax(companyId, shopId, dbProduct, productCategory, productBatch.getQuantity().doubleValue(), productBatch);
            productBatch.setTotalCultivationTax(totalCultivationTax.getTotalCultivationTax());
        } else {
            if (productCategory != null) {
                HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(companyId, shopId, productBatch.getQuantity(), productBatch.getCostPerUnit(), armsLengthType, dbProduct.isCannabisProduct(productCategory.isCannabis()));
                productBatch.setPerUnitExciseTax(taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO));
                productBatch.setTotalExciseTax(taxInfo.getOrDefault("totalExciseTax", BigDecimal.ZERO));
            }
        }

        // Update sku if needed
        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(dbProduct.getCompanyId(),dbProduct.getShopId(), dbProduct.getId(),
                BarcodeItem.BarcodeEntityType.Batch,
                productBatch.getId(), productBatch.getSku(), null, false);
        if (item != null) {
            productBatch.setSku(item.getBarcode());
            productBatch.setBatchNo(item.getNumber());
        }

        productBatch.setTrackTraceSystem(ProductBatch.TrackTraceSystem.METRC);
        productBatch.setTrackPackageLabel(compliancePackage.getData().getLabel());


        if (StringUtils.isNotBlank(productBatch.getSku())) {
            CompanyAsset companyAsset = new CompanyAsset();
            File file = null;
            InputStream inputStream = QrCodeUtil.getQrCode(productBatch.getSku());
            try {
                file = QrCodeUtil.stream2file(inputStream, ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            String keyName = productBatch.getId() + "-" + productBatch.getSku();
            UploadFileResult uploadFileResult = amazonS3Service.uploadFilePublic(file, keyName, ".png");
            companyAsset.prepare(companyId);
            companyAsset.setKey(uploadFileResult.getKey());
            companyAsset.setPublicURL(uploadFileResult.getUrl());
            companyAsset.setType(Asset.AssetType.Photo);
            companyAsset.setActive(true);
            companyAsset.setSecured(false);
            productBatch.setBatchQRAsset(companyAsset);
        }
        productBatch.setMetrcTagId(compliancePackage.getData().getLabel());
        productBatch.setPrepaidTax(false);


        ProductBatch dbBatch = productBatchRepository.save(productBatch);

        Brand dbBrand = brandRepository.get(dbBatch.getCompanyId(), dbBatch.getBrandId());
        if (!Objects.isNull(dbBrand)) {
            dbBatch.setBrandName(dbBrand.getName());
        } else {
            dbBatch.setBrandName("");
        }
        dbBatch.setProductName(dbProduct.getName());
        dbBatch.setVendorName(vendor.getName());
        compliancePackageRepository.setComplianceProductBatch(dbBatch.getCompanyId(),compliancePackage.getId(),dbProduct.getId(),dbBatch.getId());


        //batchActivityLogService.addBatchActivityLog(productBatch.getId(), intakeRequest.getEmployeeId(), productBatch.getSku() + " Batch added");

//        addNewBatchInventoryQuantity(productBatch, dbProduct, null, request.getTargetInventory());
        this.createQueuedTransactionJobForBatchQuantity(intakeRequest,
                productBatch, Inventory.InventoryType.Storage, ProductBatchQueuedTransaction.OperationType.Add, productBatch.getQuantity(), "",
                InventoryOperation.SubSourceAction.AddBatch, new HashMap<>(), dbProduct.getProductType());
        elasticSearchManager.createOrUpdateIndexedDocument(dbBatch);
    }


    private void createQueuedTransactionJobForBatchQuantity(final IntakeCompliancePackageEvent intakeRequest,
                                                            ProductBatch productBatch,
                                                            Inventory.InventoryType inventoryType,
                                                            ProductBatchQueuedTransaction.OperationType operationType,
                                                            BigDecimal quantity, String sourceChildId,
                                                            InventoryOperation.SubSourceAction subSourceAction,
                                                            HashMap<String, Double> bundleProductQuantity, Product.ProductType productType) {
        Inventory dbInventory = null;

        Shop shop = shopRepository.get(productBatch.getCompanyId(), productBatch.getShopId());
        if ((shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) && (productBatch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE)) {
            inventoryType = Inventory.InventoryType.Quarantine;
        } else {
            inventoryType = Inventory.InventoryType.Storage;
        }

        if (Inventory.InventoryType.Quarantine == inventoryType) {
            dbInventory = inventoryRepository.getInventory(productBatch.getCompanyId(), productBatch.getShopId(), Inventory.QUARANTINE);
        } else {
            dbInventory = inventoryRepository.getInventory(productBatch.getCompanyId(), productBatch.getShopId(), Inventory.SAFE);
        }

        if (dbInventory == null) {
            dbInventory = new Inventory();
            dbInventory.prepare(productBatch.getCompanyId());
            dbInventory.setShopId(productBatch.getShopId());
            if (Inventory.InventoryType.Quarantine == inventoryType) {
                dbInventory.setType(Inventory.InventoryType.Quarantine);
                dbInventory.setName(Inventory.QUARANTINE);
            } else {
                dbInventory.setName(Inventory.SAFE);
                dbInventory.setType(Inventory.InventoryType.Storage);
            }
            inventoryRepository.save(dbInventory);
        }


        ProductBatchQueuedTransaction queuedTransaction = new ProductBatchQueuedTransaction();
        queuedTransaction.setShopId(productBatch.getShopId());
        queuedTransaction.setCompanyId(productBatch.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.ProductBatch);
        queuedTransaction.setSellerId(intakeRequest.getEmployeeId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(productBatch.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setInventoryId(dbInventory.getId());
        queuedTransaction.setOperationType(operationType);
        queuedTransaction.setQuantity(quantity);
        queuedTransaction.setBundleQuantityMap(bundleProductQuantity);
        queuedTransaction.setProductType(productType);


        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);
        queuedTransaction.setSourceChildId(sourceChildId);
        queuedTransaction.setSubSourceAction(subSourceAction);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(productBatch.getCompanyId(), productBatch.getShopId(), queuedTransaction.getId());


    }

    private void addBatchLog(IntakeCompliancePackageEvent event, String batchId, String employeeId, String log) {
        BatchActivityLog batchActivityLog = new BatchActivityLog();
        batchActivityLog.prepare(event.getCompanyId());
        batchActivityLog.setShopId(event.getShopId());
        batchActivityLog.setEmployeeId(event.getEmployeeId());
        batchActivityLog.setTargetId(batchId);
        batchActivityLog.setLog(log);
        batchActivityLog.setActivityType(BatchActivityLog.ActivityType.NORMAL_LOG);
        batchActivityLogRepository.save(batchActivityLog);
    }
}
