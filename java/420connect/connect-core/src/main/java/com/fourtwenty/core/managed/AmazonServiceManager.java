package com.fourtwenty.core.managed;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceAsync;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.MailFromDomainNotVerifiedException;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.MessageRejectedException;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.AuthorizationErrorException;
import com.amazonaws.services.sns.model.CheckIfPhoneNumberIsOptedOutRequest;
import com.amazonaws.services.sns.model.CheckIfPhoneNumberIsOptedOutResult;
import com.amazonaws.services.sns.model.EndpointDisabledException;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PlatformApplicationDisabledException;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.S3Config;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.BouncedEmail;
import com.fourtwenty.core.domain.models.company.BouncedNumber;
import com.fourtwenty.core.domain.models.company.InvalidPhone;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.dispensary.BouncedEmailRepository;
import com.fourtwenty.core.domain.repositories.dispensary.BouncedNumberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InvalidPhoneRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.features.TwilioMessage;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.services.thirdparty.SendgridSerive;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.inject.Singleton;
import com.sendgrid.Response;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.routines.EmailValidator;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.MimetypesFileTypeMap;
import javax.inject.Inject;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * The AmazonServiceManager object.
 * User: mdo
 * Date: 3/16/14
 * Time: 2:31 AM
 */
@Singleton
public class AmazonServiceManager implements Managed {
    private static final Log LOG = LogFactory.getLog(AmazonServiceManager.class);
    private final BouncedEmailRepository bouncedEmailRepository;
    private final InvalidPhoneRepository invalidPhoneRepository;
    private final TwilioMessageService twilioMessageService;
    private final SendgridSerive sendgridSerive;
    private final AmazonServiceFactory awsFactory;
    private final AppRepository appRepository;
    ExecutorService executorService;
    @Inject
    private ConnectConfiguration config;
    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BouncedNumberRepository bouncedNumberRepository;
    @Inject
    public AmazonServiceManager(ConnectConfiguration aConfig,
                                BouncedEmailRepository bouncedEmailRepository,
                                InvalidPhoneRepository invalidPhoneRepository,
                                TwilioMessageService twilioMessageService,
                                SendgridSerive sendgridSerive, AppRepository appRepository) {
        config = aConfig;
        awsFactory = new AmazonServiceFactory(aConfig);
        this.bouncedEmailRepository = bouncedEmailRepository;
        this.invalidPhoneRepository = invalidPhoneRepository;
        this.twilioMessageService = twilioMessageService;
        this.sendgridSerive = sendgridSerive;
        this.appRepository = appRepository;
    }

    private static void sendDirectSMSMessage(List<String> phoneNumbers, String message,
                                             AmazonSNS snsClient, ConnectConfiguration connectConfiguration,
                                             InvalidPhoneRepository invalidPhoneRepository, Shop shop) {
        for (String phone : phoneNumbers) {
            String phoneNumber = cleanPhoneNumber(phone, shop);
            if (StringUtils.isNotBlank(phoneNumber)) {
                sendDirectSMSMessage(snsClient, phoneNumber, message, connectConfiguration, invalidPhoneRepository);
            } else {
                LOG.info("Formatted phone is blank for phone: " + phone);
            }
        }
    }

    public static String stripCharacters(String primaryPhone) {
        String clean = primaryPhone.replaceAll("[^0-9]", "");
        return clean;
    }

    public static String cleanPhoneNumber(String phoneNumber, Shop shop) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            String country = "US";
            if (shop != null && shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getCountry())) {
                country = shop.getAddress().getCountry();
            }
            if (phoneNumber == null) {
                return phoneNumber;
            }

            Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(phoneNumber, country);
            String formattedNumber = phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
            LOG.info(String.format("Unformatted: %s, cleaned: %s", phoneNumber, formattedNumber));
            return formattedNumber;
        } catch (NumberParseException e) {
            LOG.error("NumberParseException was thrown: " + e.toString(), e);
        }
        return null;
    }

    public static String cleanPhoneNumberCountry(String phoneNumber, String country) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            if (StringUtils.isBlank(country)) {
                country = "US";
            }
            Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(phoneNumber, country);
            String formattedNumber = phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
            LOG.info("Formatted phone number: " + formattedNumber);
            return formattedNumber;
        } catch (NumberParseException e) {
            LOG.error("NumberParseException was thrown: " + e.toString(), e);
        }
        return null;
    }

    private static String sendDirectSMSMessage(AmazonSNS snsClient, String phoneNumber,
                                               String message, ConnectConfiguration connectConfiguration,
                                               InvalidPhoneRepository invalidPhoneRepository) {
        try {

            boolean invalid = invalidPhoneRepository.isInvalidPhone(phoneNumber);
            if (invalid) {
                LOG.info("Phone number is invalid: " + phoneNumber);
                return null;
            }
            LOG.info("Sending SMS to: " + phoneNumber);
            Map<String, MessageAttributeValue> smsAttributes =
                    new HashMap<String, MessageAttributeValue>();

            try {
                CheckIfPhoneNumberIsOptedOutRequest optQutRequest = new CheckIfPhoneNumberIsOptedOutRequest().withPhoneNumber(phoneNumber);
                CheckIfPhoneNumberIsOptedOutResult optedOutResult = snsClient.checkIfPhoneNumberIsOptedOut(optQutRequest);
                if (optedOutResult.isOptedOut()) {
                    // ignore
                    LOG.error(phoneNumber + " has opted out.");

                    InvalidPhone invalidPhone = new InvalidPhone();
                    invalidPhone.prepare();
                    invalidPhone.setPhoneNumber(phoneNumber);
                    invalidPhone.setValid(true);
                    invalidPhone.setOptOut(true);
                    invalidPhoneRepository.save(invalidPhone);

                    return null;
                }
            } catch (Exception e) {
                // ignore dups
            }

            //<set SMS attributes>
            smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                    .withStringValue(connectConfiguration.getAppName()) //The sender ID shown on the device.
                    .withDataType("String"));
            PublishResult result = snsClient.publish(new PublishRequest()
                    .withMessage(message)
                    .withPhoneNumber(phoneNumber)
                    .withMessageAttributes(smsAttributes));

            return result.getMessageId();
        } catch (AuthorizationErrorException e) {
            // ignore?
            LOG.error("Error sending SMS to: " + phoneNumber, e);
        } catch (PlatformApplicationDisabledException e) {
            // ignore?
            LOG.error("Error sending SMS to: " + phoneNumber, e);
        } catch (EndpointDisabledException e) {
            // ignore?
            LOG.error("Error sending SMS to: " + phoneNumber, e);
        } catch (Exception e) {
            // something wrong with the phone number
            InvalidPhone invalidPhone = new InvalidPhone();
            invalidPhone.prepare();
            invalidPhone.setPhoneNumber(phoneNumber);
            invalidPhone.setValid(false);
            invalidPhoneRepository.save(invalidPhone);
            LOG.error("Error sending SMS to: " + phoneNumber, e);
        }
        return "";
    }

    @Override
    public void start() throws Exception {
        LOG.debug("Starting AmazonServiceManager");
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(8);
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping AmazonServiceManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void sendEmail(final String supportEmail, final String toEmail,
                          final String aSubject, final String aBody,
                          final String bccEmail, final String ccEmail,
                          final String fromName) {
        sendEmail(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, ccEmail, fromName);

    }

    public void sendEmail(final String supportEmail, final String toEmail,
                          final String aSubject, final String aBody,
                          final String bccEmail, final String ccEmail, final String replyToEmail,
                          final String fromName) {

        // check if this email is invalid
        boolean bounced = bouncedEmailRepository.isBounced(toEmail);
        if (bounced) {
            LOG.error("Email was previously bounced: " + toEmail);
            return;
        }

        boolean allowLocal = true;
        boolean valid = EmailValidator.getInstance(allowLocal).isValid(toEmail);
        if (!valid) {
            LOG.error("Email is not valid.");
            return;
        }

        final App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (App.EmailSender.SES.equals(app.getEmailSender())) {
            sendEmailViaSES(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, replyToEmail, fromName);
        } else {
            sendEmailViaSendgrid(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, replyToEmail, fromName);
        }

    }

    private void sendEmailViaSendgrid(final String supportEmail, final String toEmail, final String aSubject,
                                      final String aBody, final String bccEmail, final String ccEmail, final String replyToEmail, final String fromName) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = sendgridSerive.sendEmail(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, replyToEmail, fromName);
                    if (response != null) {
                        LOG.info(String.format("Email sent: %s, status: %d", toEmail, response.getStatusCode()));
                    }
                } catch (IOException ex) {
                    LOG.error("[sendgrid]The email was not sent.");
                    LOG.error("[sendgrid]Error message: " + ex.getMessage());
                } catch (BlazeOperationException ex) {
                    LOG.error("[sendgrid]The email was not sent");
                    LOG.error("[sendgrid]Error message: " + ex.getMessage());
                }
            }
        });
    }

    private void sendMultipartEmailViaSendgrid(final String supportEmail, final String toEmail, final String aSubject,
                                               final String aBody, final String bccEmail, final String ccEmail,
                                               final HashMap<String, HashMap<String, InputStream>> streamMap, final String contentType, final String fileName, final String fromName) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = sendgridSerive.sendMultiPartEmail(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, streamMap, contentType, fileName, fromName);
                } catch (IOException ex) {
                    LOG.error("[sendgrid]The email was not sent.");
                    LOG.error("[sendgrid]Error message: " + ex.getMessage());
                } catch (BlazeOperationException ex) {
                    LOG.error("[sendgrid]The email was not sent");
                    LOG.error("[sendgrid]Error message: " + ex.getMessage());
                }
            }
        });
    }

    private void sendEmailViaSES(final String supportEmail, final String toEmail,
                                 final String aSubject, final String aBody,
                                 final String bccEmail, final String ccEmail,
                                 final String replyToEmail, final String fromName) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                // Construct an object to contain the recipient address.
                Destination destination = new Destination().withToAddresses(new String[]{toEmail});

                List<String> replyAddresses = new ArrayList<String>();
                if (StringUtils.isNotBlank(bccEmail)) {
                    List<String> bccEmails = Arrays.asList(bccEmail.split("\\s*,\\s*"));
                    destination.setBccAddresses(bccEmails);
                }

                if (StringUtils.isNoneBlank(ccEmail)) {
                    List<String> toCcAddress = new ArrayList<>();
                    toCcAddress.add(ccEmail);
                    destination.setCcAddresses(toCcAddress);
                }
                if (StringUtils.isNotBlank(replyToEmail)) {
                    replyAddresses.add(replyToEmail);
                }

                // Create the subject and body of the message.
                Content subject = new Content().withData(aSubject);
                Content textBody = new Content().withData(aBody);
                Body body = new Body().withHtml(textBody);

                // Create a message with the specified subject and body.
                Message message = new Message().withSubject(subject).withBody(body);

                // Assemble the email.
                String fromEmail = supportEmail;
                if (StringUtils.isNotBlank(fromName)) {
                    fromEmail = String.format("%s <%s>", fromName, supportEmail);
                }
                SendEmailRequest request = new SendEmailRequest().withSource(fromEmail).withReplyToAddresses(replyAddresses).withDestination(destination).withMessage(message);

                try {
                    System.out.println("Attempting to send an email through Amazon SES by using the AWS SDK for Java...");
                    AmazonSimpleEmailServiceAsync client = awsFactory.getSESClient();
                    //Region REGION = Region.getRegion(Regions.US_EAST_1);
                    //client.setRegion(REGION);

                    // Send the email.
                    SendEmailResult result = client.sendEmail(request);
                    LOG.info("Email sent: " + toEmail);
                } catch (MessageRejectedException ex) {
                    // Error because of bad email
                    String msg = ex.getMessage();

                    LOG.error("The email was not sent.");
                    LOG.error("Error message: " + ex.getMessage());
                    BouncedEmail bouncedEmail = new BouncedEmail();
                    bouncedEmail.prepare();
                    bouncedEmail.setEmail(toEmail);
                    bouncedEmailRepository.save(bouncedEmail);

                } catch (MailFromDomainNotVerifiedException ex) {
                    // error because of sandbox
                    String msg = ex.getMessage();
                    LOG.error("The email was not sent.");
                    LOG.error("Error message: " + ex.getMessage());
                } catch (Exception ex) {
                    LOG.error("The email was not sent.");
                    LOG.error("Error message: " + ex.getMessage());
                }
            }
        });
    }

	/*private void sendSMSMessageToTwilio(final List<String> phoneNumbers, final String message, final Shop shop){
		for(String phone : phoneNumbers){
			String phoneNumber = cleanPhoneNumber ( phone,shop);
			if(BLStringUtils.isNotBlank ( phone )){
				App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

				String fromTwilioNo = app.getTwilioPhoneNumber();
				LOG.error("Send using Shop number: " + app.isSendShopSms());
				if (app.isSendShopSms()) {
					if (BLStringUtils.isNotBlank(shop.getTwilioNumber())) {
						fromTwilioNo = shop.getTwilioNumber();
					} else {
						try {
							String country = shop.getAddress().getCountry();
							String state = shop.getAddress().getState() != null ? shop.getAddress().getState() : "California";
							fromTwilioNo = twilioMessageService.findAndCreatePhoneNumber(country, state);
							shop.setTwilioNumber(fromTwilioNo);
							shopRepository.update(shop.getCompanyId(), shop.getId(), shop);
							LOG.info("New Shop Phone: " + fromTwilioNo + " for shop: " + shop.getName());
						} catch (Exception e) {
							LOG.error("Error creating twilio number for shop: " + shop.getName());
						}
					}
				}


				if (BLStringUtils.isBlank(fromTwilioNo)) {
					fromTwilioNo = app.getTwilioPhoneNumber();
				}


				final TwilioMessage twilioMessage = new TwilioMessage ();
				twilioMessage.setFrom (fromTwilioNo);
				twilioMessage.setTo ( phoneNumber );
				twilioMessage.setBody (String.format("%s: %s",shop.getName(),message));
				twilioMessage.setCountry ( shop.getAddress ().getCountry () );
				twilioMessage.setState ( shop.getAddress ().getState () );
				twilioMessageService.sendMessage ( twilioMessage );
			}
		}
	}*/

    private void sendMultipartEmailViaSES(final String supportEmail, final String toEmail, final String aSubject,
                                          final String aBody, final String bccEmail, final String ccEmail,
                                          final HashMap<String, HashMap<String, InputStream>> streamMap, final String contentType, final String fromName) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                S3Config amazonS3Config = config.getAmazonS3Config();

                String defaultJavaCharset = MimeUtility.getDefaultJavaCharset();

                AWSCredentials credentials = new BasicAWSCredentials(amazonS3Config.getApiKey(), amazonS3Config.getApiSecret());

                AmazonSimpleEmailServiceAsync client = awsFactory.getSESClient();
                Properties props = new Properties();
                props.setProperty("mail.transport.protocol", "aws");
                props.setProperty("mail.aws.user", credentials.getAWSAccessKeyId());
                props.setProperty("mail.aws.password", credentials.getAWSSecretKey());


                try {
                    Session session = Session.getDefaultInstance(props);
                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(supportEmail));
                    message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(toEmail));
                    message.setSubject(aSubject, "");

                    if (!StringUtils.isEmpty(bccEmail)) {
                        List<String> bccEmails = Arrays.asList(bccEmail.split("\\s*,\\s*"));
                        for (String email : bccEmails) {
                            if (!email.equalsIgnoreCase(toEmail)) {
                                message.addRecipient(MimeMessage.RecipientType.BCC, new InternetAddress(email));
                            }
                        }
                    }

                    MimeMultipart mimeMultipart = new MimeMultipart("alternative");

                    MimeBodyPart wrap = new MimeBodyPart();

                    MimeBodyPart textPart = new MimeBodyPart();

                    textPart.setContent(MimeUtility
                            .encodeText(aBody, defaultJavaCharset, "B"), "text/plain; charset=UTF-8");
                    textPart.setHeader("Content-Transfer-Encoding", "base64");

                    MimeBodyPart htmlPart = new MimeBodyPart();

                    htmlPart.setContent(MimeUtility
                            .encodeText(aBody, defaultJavaCharset, "B"), "text/html; charset=UTF-8");
                    htmlPart.setHeader("Content-Transfer-Encoding", "base64");

                    if (StringUtils.isNotBlank(ccEmail)) {
                        htmlPart.setHeader("reply-to", ccEmail);
                    }
                    mimeMultipart.addBodyPart(textPart);
                    mimeMultipart.addBodyPart(htmlPart);

                    wrap.setContent(mimeMultipart);

                    MimeMultipart msg = new MimeMultipart("mixed");

                    message.setContent(msg);

                    msg.addBodyPart(wrap);
                    message.setContent(msg, "text/html");

                    if (streamMap != null && !streamMap.isEmpty()) {
                        HashMap<String, InputStream> attachmentMap = streamMap.get("attachment");
                        HashMap<String, InputStream> inlineAttachmentMap = streamMap.get("inlineAttachment");
                        if (attachmentMap != null) {
                            for (String key : attachmentMap.keySet()) {
                                InputStream stream = attachmentMap.get(key);
                                MimeBodyPart bodyPart = new MimeBodyPart();
                                DataSource source = new ByteArrayDataSource(stream, new MimetypesFileTypeMap().getContentType(contentType));
                                bodyPart.setDataHandler(new DataHandler(source));
                                bodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
                                bodyPart.setFileName(key);
                                msg.addBodyPart(bodyPart);
                            }
                        }
                        if (inlineAttachmentMap != null) {
                            for (String key : inlineAttachmentMap.keySet()) {
                                InputStream stream = inlineAttachmentMap.get(key);
                                MimeBodyPart bodyPart = new MimeBodyPart();
                                InternetHeaders header = new InternetHeaders();
                                bodyPart = new MimeBodyPart(header, IOUtils.toByteArray(stream));
                                bodyPart.setHeader("Content-ID", "<" + key + ">");
                                bodyPart.setDisposition(MimeBodyPart.INLINE);
                                bodyPart.setFileName(key);
                                msg.addBodyPart(bodyPart);
                            }
                        }
                    }
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    message.writeTo(outputStream);

                    RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
                    SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);


                    rawEmailRequest.setDestinations(Arrays.asList(toEmail));
                    String fromEmail = supportEmail;
                    if (StringUtils.isNotBlank(fromName)) {
                        fromEmail = String.format("%s <%s>", fromName, supportEmail);
                    }

                    rawEmailRequest.setSource(fromEmail);


                    client.sendRawEmail(rawEmailRequest);

                    LOG.info("Email is sent: " + toEmail);
                } catch (Exception ex) {
                    LOG.error("The email is not sent.");
                    LOG.error("Error message: " + ex.getMessage());
                }
            }
        });
    }

    public void sendSMSMessage(final List<String> phoneNumbers, final String message, final Shop shop) {
        executorService.execute(() -> {
            //final AmazonSNS snsClient = awsFactory.getSNSClient();
            App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
            if (app == null || app.isEnableSMS()) {
                filterUnBounceNumber(phoneNumbers, shop);
                if (phoneNumbers.isEmpty()) {
                    return;
                }
                String fromNumber = getFromNumber(app, shop, false);
                //sendDirectSMSMessage(phoneNumbers, String.format("%s: %s", shop.getName(), message), snsClient, config, invalidPhoneRepository, shop);
                sendSMSMessageToTwilio(phoneNumbers, String.format("%s", message), shop, fromNumber, app);
            } else {
                LOG.info("SMS is disabled..");
            }
        });
    }

    public boolean sendMarketingSMSMessage(final List<String> toNumbers, final String message, final Shop shop, final String fromNumber, final App app) {
        if (app == null || app.isEnableSMS() == false) {
            return false;
        }

        if (toNumbers.isEmpty()) {
            return false;
        }
        // Just return if we're faking twilio
        if (app.isFakeTwilio()) {
            return true;
        }

        executorService.execute(() -> {
            //sendDirectSMSMessage(phoneNumbers, String.format("%s: %s", shop.getName(), message), snsClient, config, invalidPhoneRepository, shop);
            sendSMSMessageToTwilio(toNumbers, String.format("%s", message), shop, fromNumber, app);
        });

        return true;
    }

    private void sendSMSMessageToTwilio(final List<String> toNumbers, final String message, final Shop shop, final String fromNumber, final App app) {
        for (String phone : toNumbers) {
            String phoneNumber = cleanPhoneNumber(phone, shop);
            LOG.info(String.format("Sending SMS to: %s: %s", phone, message));
            if (StringUtils.isNotBlank(phone)) {
                final TwilioMessage twilioMessage = new TwilioMessage();
                twilioMessage.setFrom(fromNumber);
                twilioMessage.setTo(phoneNumber);
                twilioMessage.setBody(String.format("%s: %s", shop.getName(), message));
                twilioMessage.setCountry(shop.getAddress().getCountry());
                twilioMessage.setState(shop.getAddress().getState());
                twilioMessageService.sendMessage(twilioMessage);
            }
        }
    }

    public String getFromNumber(final App app, final Shop shop, final boolean createShopNumber) {
        String fromNumber = app.getTwilioPhoneNumber();
        LOG.error("Send using Shop number: " + app.isSendShopSms());
        if (app.isSendShopSms()) {
            if (StringUtils.isNotBlank(shop.getTwilioNumber())) {
                fromNumber = shop.getTwilioNumber();
            } else {
                if (createShopNumber) {
                    try {
                        String country = shop.getAddress().getCountry();
                        String state = shop.getAddress().getState() != null ? shop.getAddress().getState() : "California";
                        fromNumber = twilioMessageService.findAndCreatePhoneNumber(shop.getName(), country, state);
                        shop.setTwilioNumber(fromNumber);
                        shopRepository.updateTwilioPhoneNumber(shop.getCompanyId(), shop.getId(), fromNumber);
                        LOG.info("New Shop Phone: " + fromNumber + " for shop: " + shop.getName());
                    } catch (Exception e) {
                        LOG.error("Error creating twilio number for shop: " + shop.getName(), e);
                    }
                }
            }
        }

        if (StringUtils.isBlank(fromNumber)) {
            fromNumber = app.getTwilioPhoneNumber();
        }
        return fromNumber;
    }

    public void sendMultiPartEmail(final String supportEmail, final String toEmail, final String aSubject,
                                   final String aBody, final String bccEmail, final String ccEmail,
                                   final HashMap<String, HashMap<String, InputStream>> stream, final String contentType, String fileName, final String fromName) {
        boolean bounced = bouncedEmailRepository.isBounced(toEmail);
        if (bounced) {
            LOG.error("Email was previously bounched: " + toEmail);
            return;
        }
        final App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (App.EmailSender.SES.equals(app.getEmailSender())) {
            sendMultipartEmailViaSES(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, stream, contentType, fromName);
        } else {
            sendMultipartEmailViaSendgrid(supportEmail, toEmail, aSubject, aBody, bccEmail, ccEmail, stream, contentType, fileName, fromName);
        }

    }

    private void filterUnBounceNumber(List<String> phoneNumbers, Shop shop) {
        Iterable<BouncedNumber> bouncedNumbers = bouncedNumberRepository.getDetailsByNumber(phoneNumbers);
        for (BouncedNumber bouncedNumber : bouncedNumbers) {
            phoneNumbers.remove(cleanPhoneNumber(bouncedNumber.getNumber(), shop));
        }
    }

    public interface ThirdPartyCallBack {
        void callback();
    }
}
