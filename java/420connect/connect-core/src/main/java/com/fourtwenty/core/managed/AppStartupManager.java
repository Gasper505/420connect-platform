package com.fourtwenty.core.managed;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;

/**
 * Created by mdo on 9/6/15.
 */
@Singleton
@Deprecated
public class AppStartupManager implements Managed {
    private final AppStartup appStartup;
    private final ConnectConfiguration connectConfiguration;
    private final Environment environment;


    public AppStartupManager() {
        this.appStartup = null;
        this.connectConfiguration = null;
        this.environment = null;
    }

    public AppStartupManager(AppStartup appStartup, ConnectConfiguration config, Environment environment) {
        this.appStartup = appStartup;
        this.connectConfiguration = config;
        this.environment = environment;
    }

    @Override
    public void start() throws Exception {
        //appStartup.run(environment.getObjectMapper());

        // Find current queued transactions and add it to be processed
    }

    @Override
    public void stop() throws Exception {

    }
}
