package com.fourtwenty.core.managed;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.google.inject.Inject;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BGComplianceTaskManager implements Managed {
    private static final Log LOG = LogFactory.getLog(BGComplianceTaskManager.class);

    ExecutorService executorService;
    @Inject
    private ConnectConfiguration config;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;

    @Inject
    public BGComplianceTaskManager() {
    }

    @Override
    public void start() throws Exception {
        LOG.debug("Starting BackgroundTaskManager");
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(2);
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping BackgroundTaskManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }



    public void runTaskInBackground(final Task task) {
        try {
            this.start();
        } catch (Exception e) {
        }
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    task.execute(null, null);
                } catch (Exception e) {
                    LOG.error("Error executing task: " + task.getName(), e);
                }
            }
        });
    }
}
