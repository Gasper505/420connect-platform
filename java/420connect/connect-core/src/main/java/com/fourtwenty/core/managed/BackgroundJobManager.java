package com.fourtwenty.core.managed;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.google.inject.Injector;
import io.dropwizard.lifecycle.Managed;
import net.greghaines.jesque.worker.Worker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * Created by mdo on 4/30/16.
 */
public class BackgroundJobManager implements Managed {
    static final Log LOG = LogFactory.getLog(BackgroundJobManager.class);
    Worker worker;
    Thread workerThread;
    @Inject
    Injector injector;
    @Inject
    ConnectConfiguration connectConfiguration;

    @Override
    public void start() throws Exception {
        /*LOG.info("Redis connection: " + connectConfiguration.getRedisHost() + " port: " + connectConfiguration.getRedisPort());
        int port = 6379;
        try {
            port = Integer.valueOf(connectConfiguration.getRedisPort());
        } catch (Exception e) {
            // ignore
        }

        try {
            final Config config = new ConfigBuilder()
                    .withHost(connectConfiguration.getRedisHost())
                    .withPort(port)
                    .withDatabase(0)
                    .build();

            Map<String, Class<?>> map = new HashMap<>();
            map.put(QueuedJobType.TransactionJob.jobName, TransactionJob.class);

            Worker worker = new WorkerImpl(config,
                    Arrays.asList(QueuedJobType.TransactionJob.queueName), new GuiceJobFactory(map, injector));

            workerThread = new Thread(worker);
            workerThread.start();
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
        }*/
    }

    @Override
    public void stop() throws Exception {
        if (worker != null) {
            worker.end(true);
            try {
                workerThread.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
