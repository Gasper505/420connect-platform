package com.fourtwenty.core.managed;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.domain.models.thirdparty.ProductTagGroups;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapCategoryMapItem;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyCategoryMapItem;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyVariantUnitMap;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflyAccountRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapAccountRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapSyncItemRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WmMappingRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WmTagGroupRepository;
import com.fourtwenty.core.event.leafly.LeaflySyncResponse;
import com.fourtwenty.core.event.weedmap.WeedmapSyncResponse;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.lifecycle.TransactionReceiver;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenRequest;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenResponse;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;
import com.fourtwenty.core.services.thirdparty.impl.LeaflyConstants;
import com.fourtwenty.core.thirdparty.leafly.model.Compound;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyCategory;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyMenuItem;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyMenuItemRequest;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyResult;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyVariantUnit;
import com.fourtwenty.core.thirdparty.leafly.model.Variant;
import com.fourtwenty.core.thirdparty.leafly.services.LeaflyAPIService;
import com.fourtwenty.core.thirdparty.weedmap.WmSyncMenuDetails;
import com.fourtwenty.core.thirdparty.weedmap.models.*;
import com.fourtwenty.core.thirdparty.weedmap.result.WeedmapAccountResult;
import com.fourtwenty.core.thirdparty.weedmap.services.WeedmapAPIService;
import com.fourtwenty.core.util.MeasurementConversionUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mdo.pusher.RestErrorException;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mdo on 1/9/17.
 */
@Singleton
public class BackgroundTaskManager implements Managed {
    private static final Log LOG = LogFactory.getLog(BackgroundTaskManager.class);
    private final ProductRepository productRepository;
    private final ProductChangeLogRepository productChangeLogRepository;
    private final ShopRepository shopRepository;
    private final ProductPrepackageQuantityRepository quantityRepository;
    private final WeedmapAccountRepository weedmapAccountRepository;
    private final ProductCategoryRepository categoryRepository;
    private final WeedmapAPIService weedmapAPIService;
    private final ProductWeightToleranceRepository toleranceRepository;

    ExecutorService executorService;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private WeedmapSyncItemRepository weedmapSyncItemRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    @Inject
    private WmTagGroupRepository wmTagGroupRepository;
    @Inject
    private WmMappingRepository wmMappingRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private AppRepository appRepository;
    @Inject
    private LeaflyAPIService leaflyAPIService;
    @Inject
    private LeaflyAccountRepository leaflyAccountRepository;

    @Inject
    public BackgroundTaskManager(ProductRepository productRepository,
                                 ProductChangeLogRepository productChangeLogRepository,
                                 ProductPrepackageQuantityRepository quantityRepository,
                                 ShopRepository shopRepository,
                                 ProductCategoryRepository categoryRepository,
                                 WeedmapAccountRepository weedmapAccountRepository,
                                 WeedmapAPIService weedmapAPIService,
                                 ProductWeightToleranceRepository toleranceRepository,
                                 LeaflyAPIService leaflyAPIService,
                                 LeaflyAccountRepository leaflyAccountRepository) {
        this.productRepository = productRepository;
        this.productChangeLogRepository = productChangeLogRepository;
        this.shopRepository = shopRepository;
        this.quantityRepository = quantityRepository;
        this.categoryRepository = categoryRepository;
        this.weedmapAccountRepository = weedmapAccountRepository;
        this.weedmapAPIService = weedmapAPIService;
        this.toleranceRepository = toleranceRepository;
        this.leaflyAPIService = leaflyAPIService;
        this.leaflyAccountRepository = leaflyAccountRepository;
    }

    @Override
    public void start() throws Exception {
        LOG.debug("Starting BackgroundTaskManager");
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping BackgroundTaskManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void takeInventorySnapshot(final String companyId, final String shopId, final String employeeId, final String message, final boolean force) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (companyId == null || shopId == null) {
                    return;
                }
                Shop shop = shopRepository.get(companyId, shopId);
                if (shop != null) {

                    // 10 minutes before day end
                    long startOfDay = DateTime.now().withTimeAtStartOfDay().getMillis();
                    if (force || shop.getSnapshopTime() == null || shop.getSnapshopTime() < startOfDay) {
                        Iterable<ProductPrepackageQuantity> prepackageQuantities = quantityRepository.listAllByShop(companyId, shopId);
                        HashMap<String, Prepackage> prepackagesMap = prepackageRepository.listAsMap(companyId, shopId);
                        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(companyId, shopId);
                        Iterable<Product> products = productRepository.listByShop(companyId, shopId);
                        HashMap<String, List<ProductPrepackageQuantity>> prepackageMap = new HashMap<String, List<ProductPrepackageQuantity>>();
                        HashMap<String, HashMap<String, PrepackageQuantityLog>> productPrepackageQuantityLogs = new HashMap<>();

                        List<String> productIds = new ArrayList<>();
                        for (Product p : products) {
                            productIds.add(p.getId());
                        }

                        for (ProductPrepackageQuantity quantity : prepackageQuantities) {
                            List<ProductPrepackageQuantity> items = prepackageMap.get(quantity.getProductId());
                            if (items == null) {
                                items = new ArrayList<ProductPrepackageQuantity>();
                                prepackageMap.put(quantity.getProductId(), items);
                            }
                            items.add(quantity);

                            if (!message.equalsIgnoreCase("Inventory Snapshot Job")) {
                                // continue if request is not from quartz job
                                continue;
                            }
                            Prepackage prepackage = prepackagesMap.get(quantity.getPrepackageId());
                            PrepackageProductItem prepackageProductItem = productItemHashMap.get(quantity.getPrepackageItemId());

                            if (prepackage == null || prepackageProductItem == null) {
                                continue;
                            }
                            productPrepackageQuantityLogs.putIfAbsent(quantity.getProductId(), new HashMap<>());

                            HashMap<String, PrepackageQuantityLog> prepackageQuantityLogMap = productPrepackageQuantityLogs.get(quantity.getProductId());

                            String key = quantity.getPrepackageId() + "_" + quantity.getInventoryId() + "_" + quantity.getPrepackageItemId() + "_" + prepackageProductItem.getBatchId();
                            prepackageQuantityLogMap.putIfAbsent(key, new PrepackageQuantityLog());

                            PrepackageQuantityLog prepackageQuantityLog = prepackageQuantityLogMap.get(key);
                            prepackageQuantityLog.setBatchId(prepackageProductItem.getBatchId());
                            prepackageQuantityLog.setInventoryId(quantity.getInventoryId());
                            prepackageQuantityLog.setPrepackageItemId(prepackageProductItem.getId());
                            prepackageQuantityLog.setPrepackageId(prepackage.getId());
                            prepackageQuantityLog.setToleranceId(prepackage.getToleranceId());
                            prepackageQuantityLog.setQuantity(quantity.getQuantity() + prepackageQuantityLog.getQuantity());

                        }

                        HashMap<String, List<BatchQuantity>> batchQuantityMap = batchQuantityRepository.getBatchQuantityForProduct(companyId, shopId, productIds);

                        // Do the snapshot
                        long snapshotTime = DateTime.now().getMillis();
                        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
                        for (Product p : products) {
                            if (message.equalsIgnoreCase("Inventory Snapshot Job")) {
                                List<BatchQuantity> batchQuantities = batchQuantityMap.getOrDefault(p.getId(), new ArrayList<>());
                                HashMap<String, BatchQuantityLogs> batchQuantityLogs = new HashMap<>();
                                for (BatchQuantity batchQuantity : batchQuantities) {
                                    String key = batchQuantity.getBatchId() + "_" + batchQuantity.getInventoryId();
                                    batchQuantityLogs.putIfAbsent(key, new BatchQuantityLogs());

                                    BatchQuantityLogs batchQuantityLog = batchQuantityLogs.get(key);
                                    batchQuantityLog.setBatchId(batchQuantity.getBatchId());
                                    batchQuantityLog.setInventoryId(batchQuantity.getInventoryId());
                                    batchQuantityLog.setQuantity(batchQuantity.getQuantity().add(batchQuantityLog.getQuantity()));
                                }

                                HashMap<String, PrepackageQuantityLog> prepackageQuantityList = productPrepackageQuantityLogs.getOrDefault(p.getId(), new HashMap<>());

                                productChangeLogs.add(createProductChangeLog(employeeId, p, message, snapshotTime, prepackageMap, Lists.newArrayList(prepackageQuantityList.values()), Lists.newArrayList(batchQuantityLogs.values())));

                            } else {
                                productChangeLogs.add(createProductChangeLog(employeeId, p, message, snapshotTime, prepackageMap, null, null));
                            }
                        }
                        if (productChangeLogs.size() > 0) {
                            productChangeLogRepository.save(productChangeLogs);
                        }


                        // Update snapshot time
                        shopRepository.updateSnapshoptTime(companyId, shop.getId());
                    }
                }


            }
        });

    }

    public void takeInventorySnapshot(final String companyId, final String shopId) {
        takeInventorySnapshot(companyId, shopId, "", "Inventory Snapshot", false);
    }


    private ProductChangeLog createProductChangeLog(final String employeeId,
                                                    final Product product,
                                                    final String reference,
                                                    long snapshotTime,
                                                    final HashMap<String, List<ProductPrepackageQuantity>> prepackageMap,
                                                    List<PrepackageQuantityLog> prepackageQuantityLogs,
                                                    List<BatchQuantityLogs> batchQuantityLogs) {

        // Add change log
        ProductChangeLog changeLog = new ProductChangeLog();
        changeLog.setCreated(snapshotTime);
        changeLog.setModified(snapshotTime);
        changeLog.prepare(product.getCompanyId());
        changeLog.setShopId(product.getShopId());
        changeLog.setProductId(product.getId());
        changeLog.setEmployeeId(employeeId);
        changeLog.setReference(reference);
        changeLog.setProductQuantities(product.getQuantities());
        changeLog.setPrepackageQuantities(prepackageMap.get(product.getId()));
        changeLog.setBatchQuantities(batchQuantityLogs);
        changeLog.setPrepackageQuantityLogs(prepackageQuantityLogs);
        return changeLog;
    }


    public void runTaskInBackground(final Task task) {
        try {
            this.start();
        } catch (Exception e) {
        }
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    task.execute(null, null);
                } catch (Exception e) {
                    LOG.error("Error executing task: " + task.getName(), e);
                }
            }
        });
    }

    public void runTransactionReceiverBG(final Transaction transaction, final TransactionReceiver receiver) {
        try {
            this.start();
        } catch (Exception e) {
        }
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    receiver.run(transaction);
                } catch (Exception e) {
                    //LOG.error("Error executing event: " + e.getMessage());
                    LOG.error("Error executing receiving transactions", e);
                }
            }
        });
    }


    /**
     * Public method to sync weedmap items
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     */
    public WeedmapSyncResponse syncWeedmap(final String companyId, final String shopId) {
        return doSyncWeedmap(companyId, shopId);
    }

    /**
     * Public method to reset weedmap items
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param accountId : accountId
     */
    public WeedmapSyncResponse resetWeedmap(final String companyId, final String shopId, String accountId) {
        return doResetWeedmap(companyId, shopId, accountId);
    }

    /**
     * Private method to reset weedmap method
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     */
    private WeedmapSyncResponse doResetWeedmap(final String companyId, final String shopId, final String accountId) {
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        WeedmapAccountResult dbAccount = getWeedmapAccount(companyId, shopId, accountId);
        if (dbAccount == null) {
            LOG.info(String.format("Weedmap account not found for companyId: %s, shopId: %s", companyId, shopId));
            response.setErrorMsg(String.format("Weedmap account not found for companyId: %s, shopId: %s", companyId, shopId));
            return response;
        }

        for (WeedmapApiKeyMap keyMap : dbAccount.getApiKeyList()) {
            if (keyMap.getVersion() == WeedmapAccount.Version.V2 && keyMap.getStatus() == WeedmapAccount.AuthStatus.Authorized) {
                response = _resetV2Weedmap(companyId, shopId, keyMap, dbAccount);
            } else {
                response = _resetV1Weedmap(keyMap.getApiKey());
            }
            if (!response.isSyncStatus()) {
                return response;
            }
        }
        return response;
    }

    /**
     * Private method to reset weedmap for V1
     *
     * @param apiKey : apiKey
     */
    private WeedmapSyncResponse _resetV1Weedmap(String apiKey) {
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        try {
            WeedmapResult weedmapResult = weedmapAPIService.getWeedmapMenu(apiKey);
            int deleted = 0;
            int failed = 0;
            for (WeedmapMenuItem menuItem : weedmapResult.getData()) {
                try {
                    weedmapAPIService.deleteWeedmapMenuItem(apiKey, menuItem.getItemId());
                    deleted++;
                } catch (Exception e) {
                    LOG.error("Fail to menu sync", e);
                    failed++;
                }
            }
            LOG.info("Completed deleting weedmap menu items: " + deleted + " out of: " + weedmapResult.getData().size());
            LOG.info("Failed deleting weedmap menu items: " + failed);
            response.setErrorMsg("Success");
            response.setSyncStatus(true);
        } catch (Exception e) {
            LOG.error("Fail to menu sync", e);
            response.setErrorMsg(e.getMessage());
            response.setSyncStatus(false);
        }
        return response;
    }

    /**
     * Private method to reset weedmap for V2
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param apiKey    : apiKeyMap
     */
    private WeedmapSyncResponse _resetV2Weedmap(String companyId, String shopId, WeedmapApiKeyMap apiKey, WeedmapAccountResult dbAccount) {
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        try {
            if (!canAccessV2Weedmap(companyId)) {
                LOG.info(String.format("Skip weedmap..company %s does not have access to v2 weedmap", companyId));
                response.setErrorMsg(String.format("Skip weedmap..company %s does not have access to v2 weedmap", companyId));
                response.setSyncStatus(false);
                return response;
            }

            Iterable<WmSyncItems> items = weedmapSyncItemRepository.getAllItemsAsMap(companyId, shopId, apiKey.getListingWmId());
            WmSyncItems organizationItem = weedmapSyncItemRepository.getItemByType(companyId, shopId, apiKey.getListingWmId(), WmSyncItems.WMItemType.ORGANIZATIONS);

            if (organizationItem == null) {
                LOG.info(String.format("Organization information not found for companyId:%s shopId:%s", companyId, shopId));
                wmMappingRepository.deleteAccountProductMapping(companyId, shopId, dbAccount.getId());
                response.setErrorMsg(String.format("Organization information not found for companyId:%s shopId:%s", companyId, shopId));
                response.setSyncStatus(false);
                return response;
            }

            int productDeleted = 0, otherItems = 0;
            int failed = 0;
            int total = 0;
            List<ObjectId> deletedItems = new ArrayList<>();
            for (WmSyncItems item : items) {
                total++;
                try {
                    boolean isDelete = deleteWmV2Items(companyId, shopId, apiKey, organizationItem.getWmItemId(), item, dbAccount.getWeedmapConfig());
                    if (isDelete) {
                        if (item.getItemType() == WmSyncItems.WMItemType.PRODUCTS) {
                            productDeleted++;
                        } else {
                            otherItems++;
                        }
                        deletedItems.add(new ObjectId(item.getId()));
                    }
                } catch (Exception e) {
                    LOG.error("Fail to menu sync", e);
                    failed++;
                }
            }
            if (!deletedItems.isEmpty()) {
                weedmapSyncItemRepository.hardRemoveById(deletedItems);
            }
            wmMappingRepository.deleteAccountProductMapping(companyId, shopId, dbAccount.getId());
            LOG.info("Completed deleting weedmap menu items: Products : " + productDeleted + " Brands: " + otherItems + " out of: " + total);
            LOG.info("Failed deleting weedmap menu items: " + failed);
            response.setErrorMsg("Success");
            response.setSyncStatus(true);
        } catch (Exception e) {
            LOG.error("Fail to menu sync", e);
            response.setErrorMsg(String.format("Organization information not found for companyId:%s shopId:%s", companyId, shopId));
            response.setSyncStatus(false);
        }
        return response;
    }

    /**
     * Public method to sync weedmap from sync menu
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     */
    private WeedmapSyncResponse doSyncWeedmap(final String companyId, final String shopId) {
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        WeedmapAccountResult dbAccount = getWeedmapAccount(companyId, shopId, null);
        if (dbAccount == null) {
            LOG.info(String.format("Weedmap account not found for companyId: %s, shopId: %s", companyId, shopId));
            response.setErrorMsg(String.format("Weedmap account not found for companyId: %s, shopId: %s", companyId, shopId));
            return response;
        }
        List<WmProductMapping> tagsMappings = Lists.newArrayList(wmMappingRepository.getMappingForAccount(companyId, shopId, dbAccount.getId()));
        // Get category ids
        Set<ObjectId> categoryIds = new HashSet<>();
        HashMap<String, WmProductMapping> categoryMappingMap = new HashMap<>();

        for (WeedmapCategoryMapItem mapItem : dbAccount.getCategoryMapping()) {
            if (StringUtils.isNotBlank(mapItem.getCategoryId())) {
                categoryIds.add(new ObjectId(mapItem.getCategoryId()));
            }
        }

        if (!CollectionUtils.isNullOrEmpty(tagsMappings)) {
            for (WmProductMapping tagsMapping : tagsMappings) {
                if (StringUtils.isNotBlank(tagsMapping.getCategoryId())) {
                    categoryIds.add(new ObjectId(tagsMapping.getCategoryId()));
                    categoryMappingMap.put(tagsMapping.getProductId(), tagsMapping);
                }
                categoryMappingMap.put(tagsMapping.getProductId(), tagsMapping);
            }
        }

        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAsMap(companyId, Lists.newArrayList(categoryIds));
        HashMap<String, ProductWeightTolerance> toleranceHashMap = toleranceRepository.listAsMap(companyId);
        Iterable<Inventory> inventories = inventoryRepository.listAllByShop(companyId, shopId);

        HashMap<String, Set<String>> regionInventoriesMap = new HashMap<>();
        for (Inventory inventory : inventories) {
            if (inventory.getRegionIds() != null && !inventory.getRegionIds().isEmpty()) {
                for (String regionId : inventory.getRegionIds()) {
                    Set<String> inventoryIds = regionInventoriesMap.getOrDefault(regionId, new HashSet<>());
                    inventoryIds.add(inventory.getId());
                    regionInventoriesMap.put(regionId, inventoryIds);
                }
            }
        }


        try {
            Brand defaultBrand = getDefaultBrand(companyId);

            List<String> categoryIdList = Lists.newArrayList(categoryHashMap.keySet());
            // Get Multiple API Keys for syncing products
            List<WeedmapApiKeyMap> apiKeyList = dbAccount.getApiKeyList();
            for (WeedmapApiKeyMap weedmapApiKeyMap : apiKeyList) {
                if (!weedmapApiKeyMap.isEnabled()) {
                    LOG.info(String.format("%s is not enabled for weedmap %s sync", weedmapApiKeyMap.getName(), weedmapApiKeyMap.getVersion()));
                    continue;
                }

                long afterDate = weedmapApiKeyMap.getMenuLastSyncDate() == null ? 0 : weedmapApiKeyMap.getMenuLastSyncDate();
                Iterable<Product> products = productRepository.getProductsForCategoriesNewer(companyId, shopId, categoryIdList, afterDate);

                List<ObjectId> brandIds = new ArrayList<>();
                for (Product product : products) {
                    if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                        brandIds.add(new ObjectId(product.getBrandId()));
                    }
                }

                HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(companyId, brandIds);
                brandHashMap.putIfAbsent(defaultBrand.getId(), defaultBrand);

                if (weedmapApiKeyMap.getVersion() == WeedmapAccount.Version.V2) {
                    syncVersion2Weedmap(companyId, shopId, defaultBrand, Lists.newArrayList(products), categoryHashMap, toleranceHashMap, dbAccount, weedmapApiKeyMap, categoryMappingMap, tagsMappings, regionInventoriesMap, brandHashMap);
                } else {
                    syncVersion1Weedmap(companyId, shopId, products, categoryHashMap, toleranceHashMap, dbAccount, weedmapApiKeyMap, regionInventoriesMap, brandHashMap);
                }
                weedmapApiKeyMap.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Completed);
                weedmapApiKeyMap.setMenuLastSyncDate(DateTime.now().getMillis());
            }
            dbAccount.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Completed);
            dbAccount.setMenuLastSyncDate(DateTime.now().getMillis());
            //weedmapAccountRepository.update(companyId, dbAccount.getId(), dbAccount);
            weedmapAccountRepository.updateSyncStatus(companyId,dbAccount.getId(), WeedmapAccount.WeedmapSyncStatus.Completed,DateTime.now().getMillis());
            response.setErrorMsg("Success");
            response.setSyncStatus(true);
        } catch (Exception e) {
            LOG.error("Fail to menu sync", e);
            response.setErrorMsg(e.getMessage());
        }
        return response;

    }

    /**
     * Private method to get weedmap category
     *
     * @param product  : product
     * @param category : category
     * @param account  : account
     * @return : return WeedmapCategory
     */
    private WeedmapCategory getWeedmapCategory(Product product, ProductCategory category, WeedmapAccount account) {

        WeedmapCategoryMapItem mapItem = null;
        for (WeedmapCategoryMapItem item : account.getCategoryMapping()) {
            if (item.getCategoryId().equalsIgnoreCase(category.getId())) {

                if (StringUtils.isNotBlank(product.getFlowerType())) {
                    // if the weedmap category is a flower type, then check the flower type
                    if (item.getWeedmapCategory().name().equalsIgnoreCase("Indica")
                            || item.getWeedmapCategory().name().equalsIgnoreCase("Sativa")
                            || item.getWeedmapCategory().name().equalsIgnoreCase("Hybrid")) {
                        if (item.getWeedmapCategory().name().equalsIgnoreCase(product.getFlowerType())) {
                            mapItem = item;
                            break;
                        }
                    } else {
                        mapItem = item;
                        break;
                    }
                } else if (StringUtils.isNotBlank(item.getFlowerType())) {
                    // match flower type
                    if (item.getFlowerType().equalsIgnoreCase(product.getFlowerType())) {
                        mapItem = item;
                        break;
                    }
                } else {
                    mapItem = item;
                    break;
                }
            }
        }

        if (mapItem != null) {
            // find weedmap category with matching id
            return mapItem.getWeedmapCategory();
        }
        // if weedmap category == null, then we do not sync this product
        return null;
    }

    /**
     * Overload method for sync weed map
     *
     * @param companyId          : company id
     * @param shopId             : shop Id
     * @param apiKeyList         : weed map api key list
     * @param products           : product list
     */

    public WeedmapSyncResponse syncWeedmap(final String companyId, final String shopId,
                                           final List<WeedmapApiKeyMap> apiKeyList,
                                           final List<Product> products) {
        return doSyncWeedmapWithProducts(companyId, shopId, apiKeyList, products);
    }

    /**
     * Overload method for sync weed map
     *
     * @param companyId          : company id
     * @param shopId             : shop Id
     * @param apiKeyList         : weed map api key list
     * @param products           : product list
     */

    private WeedmapSyncResponse doSyncWeedmapWithProducts(String companyId, String shopId, List<WeedmapApiKeyMap> apiKeyList, List<Product> products) {
        WeedmapAccountResult dbAccount = getWeedmapAccount(companyId, shopId, null);
        WeedmapSyncResponse response = new WeedmapSyncResponse();
        if (dbAccount == null) {
            LOG.info(String.format("Weedmap account not found for companyId: %s, shopId: %s", companyId, shopId));
            response.setErrorMsg(String.format("Weedmap account not found for companyId: %s, shopId: %s", companyId, shopId));
            response.setSyncStatus(false);
            return response;
        }
        List<WmProductMapping> tagsMappings = Lists.newArrayList(wmMappingRepository.getMappingForAccount(companyId, shopId, dbAccount.getId()));
        // Get category ids
        Set<ObjectId> categoryIds = new HashSet<>();
        HashMap<String, WmProductMapping> categoryMappingMap = new HashMap<>();

        for (WeedmapCategoryMapItem mapItem : dbAccount.getCategoryMapping()) {
            if (StringUtils.isNotBlank(mapItem.getCategoryId())) {
                categoryIds.add(new ObjectId(mapItem.getCategoryId()));
            }
        }

        if (!CollectionUtils.isNullOrEmpty(tagsMappings)) {
            for (WmProductMapping tagsMapping : tagsMappings) {
                if (StringUtils.isNotBlank(tagsMapping.getCategoryId())) {
                    categoryIds.add(new ObjectId(tagsMapping.getCategoryId()));
                }
                categoryMappingMap.put(tagsMapping.getProductId(), tagsMapping);
            }
        }

        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAsMap(companyId, Lists.newArrayList(categoryIds));
        HashMap<String, ProductWeightTolerance> toleranceHashMap = toleranceRepository.listAsMap(companyId);
        Iterable<Inventory> inventories = inventoryRepository.listAllByShop(companyId, shopId);

        HashMap<String, Set<String>> regionInventoriesMap = new HashMap<>();
        for (Inventory inventory : inventories) {
            if (inventory.getRegionIds() != null && !inventory.getRegionIds().isEmpty()) {
                for (String regionId : inventory.getRegionIds()) {
                    Set<String> inventoryIds = regionInventoriesMap.getOrDefault(regionId, new HashSet<>());
                    inventoryIds.add(inventory.getId());
                    regionInventoriesMap.put(regionId, inventoryIds);
                }
            }
        }

        List<ObjectId> brandIds = new ArrayList<>();
        for (Product product : products) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(companyId, brandIds);

        try {
            Brand defaultBrand = getDefaultBrand(companyId);

            brandHashMap.putIfAbsent(defaultBrand.getId(), defaultBrand);

            // Get Multiple API Keys for syncing products
            for (WeedmapApiKeyMap weedmapApiKeyMap : apiKeyList) {

                if (weedmapApiKeyMap.getVersion() == WeedmapAccount.Version.V2) {
                    syncVersion2Weedmap(companyId, shopId, defaultBrand, Lists.newArrayList(products), categoryHashMap, toleranceHashMap, dbAccount, weedmapApiKeyMap, categoryMappingMap, tagsMappings, regionInventoriesMap, brandHashMap);
                } else {

                    syncVersion1Weedmap(companyId, shopId, products, categoryHashMap, toleranceHashMap, dbAccount, weedmapApiKeyMap, regionInventoriesMap, brandHashMap);
                }
                weedmapApiKeyMap.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Completed);
                weedmapApiKeyMap.setMenuLastSyncDate(DateTime.now().getMillis());
            }
            response.setErrorMsg("Success");
            response.setSyncStatus(true);

            // This is an individual sync so we do not need to update this
            //dbAccount.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Completed);
            //dbAccount.setMenuLastSyncDate(DateTime.now().getMillis());
            //weedmapAccountRepository.update(companyId, dbAccount.getId(), dbAccount);
        } catch (Exception e) {
            LOG.error("Fail to menu sync", e);
            response.setErrorMsg(e.getMessage());
            response.setSyncStatus(false);
        }


        return response;
    }

    /**
     * Private method to process weedmap prices with product
     *
     * @param product          : product
     * @param category         : product category
     * @param toleranceHashMap : product weight tolerances
     * @param prices           : weed map prices
     * @param weedmapCategory  : weedmap category
     */
    private void processProductPrices(Product product, ProductCategory category, HashMap<String, ProductWeightTolerance> toleranceHashMap, WeedmapPrices prices, WeedmapCategory weedmapCategory) {
        if (category.getUnitType() == ProductCategory.UnitType.grams) {
            for (ProductPriceRange range : product.getPriceRanges()) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(range.getWeightToleranceId());
                if (tolerance != null) {
                    if (tolerance.isEnabled() && NumberUtils.round(range.getPrice(), 2) > 0) {
                        switch (tolerance.getWeightKey()) {
                            case HALF_GRAM:
                                prices.setPriceHalfGram(range.getPrice());
                                break;
                            case GRAM:
                                prices.setPriceGram(range.getPrice());
                                break;
                            case TWO_GRAMS:
                                prices.setPriceTwoGrams(range.getPrice());
                                break;
                            case ONE_EIGHTTH:
                                prices.setPriceEighth(range.getPrice());
                                break;
                            case QUARTER:
                                prices.setPriceQuarter(range.getPrice());
                                break;
                            case HALF:
                                prices.setPriceHalfOunce(range.getPrice());
                                break;
                            case OUNCE:
                                prices.setPriceOunce(range.getPrice());
                                break;
                            default:
                                break;

                        }
                    }
                }
            }
        } else {
            // UNIT BASED PRODUCTS
            /*if (weedmapCategory.title.startsWith("Concentrate")
                    || weedmapCategory.title.startsWith("Wax")) {
                int gramValue = 1;
                int twoGramsValue = 2;
                if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                    prices.setPriceGram(product.getUnitPrice());
                } else {
                    prices.setPriceHalfGram(product.getUnitPrice());
                    gramValue = 2;
                    twoGramsValue = 4;
                }

                for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                    if (priceBreak.getQuantity() == gramValue) {
                        prices.setPriceGram(priceBreak.getPrice());
                    } else if (priceBreak.getQuantity() == twoGramsValue) {
                        prices.setPriceTwoGrams(priceBreak.getPrice());
                    } else {
                        prices.getPricesOther().put(priceBreak.getName(), priceBreak.getPrice());
                    }
                }
            } else if (category.getName().startsWith("Flower")
                        || weedmapCategory.title.startsWith("Concentrate")
                        || weedmapCategory.title.startsWith("Wax")) {*/
            // defaults to 8th
            if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                    || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                prices.setPriceUnit(product.getUnitPrice());
            }

            if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                prices.setPriceHalfGram(product.getUnitPrice());
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                prices.setPriceGram(product.getUnitPrice());
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                prices.setPriceEighth(product.getUnitPrice());
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                prices.setPriceQuarter(product.getUnitPrice());
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_OUNCE) {
                prices.setPriceHalfOunce(product.getUnitPrice());
            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.OUNCE) {
                prices.setPriceOunce(product.getUnitPrice());
            }


            for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                if (!priceBreak.isActive()) {
                    continue;
                }
                if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                    if (priceBreak.getQuantity() == 2) {
                        prices.setPriceGram(priceBreak.getPrice());
                    } else if (priceBreak.getQuantity() == 4) {
                        prices.setPriceTwoGrams(priceBreak.getPrice());
                    }
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                    if (priceBreak.getQuantity() == 2) {
                        prices.setPriceTwoGrams(priceBreak.getPrice());
                    }
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                    if (priceBreak.getQuantity() == 2) {
                        prices.setPriceQuarter(priceBreak.getPrice());
                    } else if (priceBreak.getQuantity() == 4) {
                        prices.setPriceHalfOunce(priceBreak.getPrice());
                    } else if (priceBreak.getQuantity() == 8) {
                        prices.setPriceOunce(priceBreak.getPrice());
                    }
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                    if (priceBreak.getQuantity() == 2) {
                        prices.setPriceHalfOunce(priceBreak.getPrice());
                    } else if (priceBreak.getQuantity() == 4) {
                        prices.setPriceOunce(priceBreak.getPrice());
                    }
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_OUNCE) {
                    if (priceBreak.getQuantity() == 2) {
                        prices.setPriceOunce(priceBreak.getPrice());
                    }
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH) {
                    prices.getPricesOther().put(priceBreak.getName(), priceBreak.getPrice());
                }

            }
        }
    }

    /**
     * Private method get quantity
     *
     * @param product              : product
     * @param prepackageQuantities : prepackage quantities
     * @return : quantity
     */
    private double getProductQuantity(Product product, Iterable<ProductPrepackageQuantity> prepackageQuantities) {
        double quantity = 0;
        if (product.getQuantities() == null || product.getQuantities().isEmpty()) {
            return quantity;
        }

        for (ProductPrepackageQuantity productPrepackageQuantity : prepackageQuantities) {
            if (product.getId().equals(productPrepackageQuantity.getProductId())) {
                quantity += productPrepackageQuantity.getQuantity();
            }
        }
        for (ProductQuantity productQuantity : product.getQuantities()) {
            quantity += productQuantity.getQuantity().doubleValue();
        }

        return quantity;
    }

    private double getProductQuantity(Product product, Set<String> inventoryIds, Iterable<ProductPrepackageQuantity> prepackageQuantities) {
        double quantity = 0;
        if (product.getQuantities() == null || product.getQuantities().isEmpty()) {
            return quantity;
        }

        for (ProductPrepackageQuantity productPrepackageQuantity : prepackageQuantities) {
            if (product.getId().equals(productPrepackageQuantity.getProductId()) && inventoryIds.contains(productPrepackageQuantity.getInventoryId())) {
                quantity += productPrepackageQuantity.getQuantity();
            }
        }
        for (ProductQuantity productQuantity : product.getQuantities()) {
            if (inventoryIds.contains(productQuantity.getInventoryId())) {
                quantity += productQuantity.getQuantity().doubleValue();
            }
        }

        return quantity;
    }

    /**
     * Public method to authorize weedmap token.
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param account     : account
     * @param type        : type
     * @param isQuartzJob : isQuartzJob
     * @return : weedmapAccount
     */
    public WeedmapAccount authenticateWeedmapToken(String companyId, String shopId, WeedmapAccount account, WeedmapTokenType type, boolean isQuartzJob, WeedmapConfig config) {
        if (config == null) {
            LOG.info(String.format("Configuration not found for weedmaps..companyId: %s, shopId: %s", companyId, shopId));
        }
        for (WeedmapApiKeyMap weedmapApiKeyMap : account.getApiKeyList()) {
            if (weedmapApiKeyMap.getVersion() != WeedmapAccount.Version.V2) {
                continue;
            }

            WeedmapTokenRequest request = new WeedmapTokenRequest();
            request.setClientSecret(config.getSecret());
            request.setClientId(config.getClientId());
            request.setCode(type.getCode());
            request.setRedirectUri(config.getRedirectURL());
            request.setGrantType(type.getType().getValue());

            if (type.getType() == WeedmapTokenType.TokenType.REFRESHTOKEN) {
                request.setRefreshToken(weedmapApiKeyMap.getRefreshToken());
            }

            WeedmapTokenResponse weedmapTokenResponse = weedmapAPIService.authenticateWeedmap(account.getCompanyId(), account.getShopId(), request, config);
            if (weedmapTokenResponse != null) {
                weedmapApiKeyMap.setRefreshToken(weedmapTokenResponse.getRefreshToken());
                weedmapApiKeyMap.setToken(weedmapTokenResponse.getAccessToken());
                weedmapApiKeyMap.setStatus(WeedmapAccount.AuthStatus.Authorized);
                DateTime expireTime = new DateTime(weedmapTokenResponse.getCreatedAt()).plus(weedmapTokenResponse.getExpiresIn());
                weedmapApiKeyMap.setExpiresIn(expireTime.getMillis() * 1000);
            } else {
                LOG.error("Error in weed mapa authenticate");
            }

        }

        return isQuartzJob ? weedmapAccountRepository.update(account.getId(), account) : account;
    }

    public LeaflyAccount authenticateLeaflyAccount(String companyId, String shopId, LeaflyAccount account, String clientKey) {
        Boolean status = leaflyAPIService.getMenuIntegrationStatus(IntegrationSetting.Environment.Development, account.getClientKey(), account.getApiKey());
        if (!status) {
            LOG.info(String.format("Configuration not found for Leafly..companyId: %s, shopId: %s", companyId, shopId));
            throw new BlazeAuthException("LeaflyAccount", "Error in leafly account authentication");
        }
        return account;
    }

    /**
     * Public method to create catalog/organization.
     *
     * @param companyId           : companyId
     * @param shopId              : shopId
     * @param apiKeyMap           : apiKeyMap
     * @param syncList            : syncList
     * @param weedmapSyncItemsMap : weedmapSyncItemMap
     * @param weedmapConfig       : config
     * @param syncMenuItems       : syncMenuItems
     * @return
     */
    public WmSyncItems createWmOrganization(String companyId, String shopId, WeedmapApiKeyMap apiKeyMap, List<WmSyncItems> syncList, HashMap<String, WmSyncItems> weedmapSyncItemsMap, WeedmapConfig weedmapConfig, boolean syncMenuItems) {
        //WmSyncItems catalogSyncItem = weedmapSyncItemRepository.getSyncItemByType(companyId, shopId, WmSyncItems.WMItemType.CATALOG);
        WmSyncItems organizationItem = weedmapSyncItemsMap.get(shopId);
        String catalogId = "";

        if (organizationItem == null) {
            try {
                WmSyncRequest request = WmRequestGenerator.catalogRequest(apiKeyMap.getListingWmId().trim());

                // check if catalog exists
                catalogId = weedmapAPIService.getCatalogId(companyId, shopId, apiKeyMap, weedmapConfig);
                WmSyncRequest response = null;
                if (StringUtils.isNotBlank(catalogId)) {
                    response = weedmapAPIService.getCatalogById(companyId, shopId, catalogId, apiKeyMap, weedmapConfig);
                } else {
                    response = weedmapAPIService.createWeedmapCatalog(companyId, shopId, apiKeyMap, request, weedmapConfig);
                }
                if (response != null) {
                    catalogId = response.getId();
                    HashMap<WmSyncItems.WMItemType, String> wmLinkedItems = new HashMap<>();
                    wmLinkedItems.put(WmSyncItems.WMItemType.CATALOG, catalogId);
                    organizationItem = createSyncItem(companyId, shopId, apiKeyMap.getListingWmId(), response.getOrganizationId(), shopId, "", WmSyncItems.WMItemType.ORGANIZATIONS, wmLinkedItems, false);
                    syncList.add(organizationItem);
                    weedmapSyncItemsMap.put(shopId, organizationItem);
                } else {
                    LOG.info("No Response from weedmap catalog creation.");
                }
            } catch (Exception e) {
                LOG.info("Error Response from weedmap catalog creation." + e.getMessage());
            }
            if (organizationItem != null && syncMenuItems) {
                syncWmMenuItems(companyId, shopId, organizationItem, weedmapSyncItemsMap, weedmapConfig, apiKeyMap, syncList);
            }
        }
        return organizationItem;
    }

    /**
     * Private method to create syncItem object
     *
     * @param companyId     : companyId
     * @param shopId        : shopId
     * @param wmListingId   : wmListingId
     * @param wmItemId      : wmItemId
     * @param itemId        : itemId
     * @param wmsin         : wmSin
     * @param itemType      : itemType
     * @param wmLinkedItems : wmLinkedItems
     * @param isVerified    : isVerified
     * @return : syncItemObject
     */
    private WmSyncItems createSyncItem(String companyId, String shopId, String wmListingId, String wmItemId, String itemId, String wmsin, WmSyncItems.WMItemType itemType, HashMap<WmSyncItems.WMItemType, String> wmLinkedItems, boolean isVerified) {
        return createSyncItem(companyId, shopId, wmListingId, wmItemId, itemId, wmsin, itemType, wmLinkedItems, isVerified, new ArrayList<>());
    }

    /**
     * Private method to sync version 2 weedmap when called from event
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param defaultBrand      : defaultBrand
     * @param categoryHashMap   : categoryMap
     * @param toleranceHashMap  : toleranceMap
     * @param dbAccount         : dbAccount
     * @param apiKeyMap         : apiKeyMap
     * @param productMappingMap : categoryTagMapping
     * @param tagsMappings      : tagsMapping
     * @param brandHashMap      : brandHashMap
     */
    private void syncVersion2Weedmap(String companyId, String shopId, Brand defaultBrand,
                                     Iterable<Product> products,
                                     HashMap<String, ProductCategory> categoryHashMap,
                                     HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                     WeedmapAccountResult dbAccount,
                                     WeedmapApiKeyMap apiKeyMap,
                                     HashMap<String, WmProductMapping> productMappingMap,
                                     List<WmProductMapping> tagsMappings,
                                     HashMap<String, Set<String>> regionInventoriesMap, HashMap<String, Brand> brandHashMap) {
        try {

            if (apiKeyMap.getVersion() != WeedmapAccount.Version.V2) {
                LOG.info("Skip weedmap api key..record not available for Version 2");
                return;
            }

            if (StringUtils.isBlank(apiKeyMap.getToken())) {
                LOG.info(String.format("Skip weedmap..for %s need to authorize api map first.", apiKeyMap.getId()));
                return;
            }

            if (productMappingMap.isEmpty()) {
                LOG.info(String.format("Skip weedmap..for %s no product mapping found for tags.", apiKeyMap.getId()));
                return;
            }

            if (!canAccessV2Weedmap(companyId)) {
                LOG.info(String.format("Skip weedmap..company %s does not have access to v2 weedmap", companyId));
                return;
            }

            DateTime expiredTime = new DateTime(apiKeyMap.getExpiresIn());

            if (expiredTime.isBeforeNow()) {
                LOG.info(String.format("Weedmap token expired for api map %s", apiKeyMap.getId()));
                WeedmapTokenType type = new WeedmapTokenType();
                type.setType(WeedmapTokenType.TokenType.REFRESHTOKEN);
                authenticateWeedmapToken(companyId, shopId, dbAccount, type, false, dbAccount.getWeedmapConfig());
            }

            List<String> productIds = new ArrayList<>();

            List<ObjectId> productObjIds = new ArrayList<>();
            for (Product product : products) {
                productIds.add(product.getId());
                productObjIds.add(new ObjectId(product.getId()));
            }

            Shop shop = shopRepository.getById(shopId);

            List<String> itemIds = Lists.newArrayList(productIds);
            itemIds.addAll(brandHashMap.keySet());
            itemIds.add(shopId);

            List<WmSyncItems> syncItemList = new ArrayList<>();
            List<WmSyncItems> updateItemList = new ArrayList<>();
            List<ObjectId> deletedItems = new ArrayList<>();

            HashMap<String, WmSyncItems> dbSyncItemsMap = weedmapSyncItemRepository.getSyncItemsByItemIdsAsMap(companyId, shopId, apiKeyMap.getListingWmId(), itemIds);
            HashMap<String, WmSyncItems> wmSyncItemsMap = new HashMap<>(dbSyncItemsMap);

            WmSyncItems organizationItem = createWmOrganization(companyId, shopId, apiKeyMap, syncItemList, wmSyncItemsMap, dbAccount.getWeedmapConfig(), true);
            if (organizationItem == null) {
                LOG.info(String.format("Organization id not found for companyId: %s shopId: %s weedmap api: %s", companyId, shopId, apiKeyMap.getId()));
                return;
            }

            //syncWmMenuItems(companyId, shopId, organizationItem, wmSyncItemsMap, dbAccount.getWeedmapConfig(), apiKeyMap, new ArrayList<>());

            String organizationId = organizationItem.getWmItemId();
            String catalogId = "";
            if (organizationItem.getWmLinkedItems().containsKey(WmSyncItems.WMItemType.CATALOG)) {
                catalogId = organizationItem.getWmLinkedItems().getOrDefault(WmSyncItems.WMItemType.CATALOG, "");
            }

            if (StringUtils.isBlank(organizationId) || StringUtils.isBlank(catalogId)) {
                LOG.info(String.format("Organization id not found for companyId: %s shopId: %s weedmap api: %s", companyId, shopId, apiKeyMap.getId()));
                return;
            }

            HashMap<String, String> tagsMapping = getWmTagMapping(companyId, shopId, organizationId, dbAccount, apiKeyMap, tagsMappings);

            if (tagsMapping == null || tagsMapping.isEmpty()) {
                LOG.info(String.format("Tags not found for companyId: %s shopId: %s weedmap api: %s", companyId, shopId, apiKeyMap.getId()));
                return;
            }

            WmAttributeResponse attributeResponse = weedmapAPIService.getVariantAttributes(companyId, shopId, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());

            Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(companyId, shopId, productIds);

            int added = 0;
            int updated = 0;
            int failed = 0;
            int imagesFound = 0;
            int deleted = 0;
            int total = 0;
            int totalSkipped = 0;
            for (Product product : products) {
                total++;

                if (!productMappingMap.containsKey(product.getId())) {
                    LOG.info(String.format("Product not mapped with account for product %s", product.getName()));
                    failed++;
                    continue;
                }

                WmSyncItems productSyncItem = wmSyncItemsMap.get(product.getId());

                WmProductMapping productMapping = productMappingMap.get(product.getId());

                boolean published = true;

                double quantity = getProductQuantity(product, prepackageQuantities);
                quantity = NumberUtils.round(quantity,2);

                if (apiKeyMap.getRegion() != null && regionInventoriesMap.containsKey(apiKeyMap.getRegion().getId())) {
                    double regionQty = getProductQuantity(product, regionInventoriesMap.get(apiKeyMap.getRegion().getId()), prepackageQuantities);
                    if ((product.getWmThreshold() != null && regionQty < product.getWmThreshold().doubleValue()) || regionQty <= 0) {
                        published = false;
                    }
                }

                if (!product.isActive() || (product.getWmThreshold() != null && quantity < product.getWmThreshold().doubleValue()) || quantity <= 0) {
                    // unpublish
                    published = false;
                }

                // if product is disabled for weedmaps or product is deleted, remove from weedmaps completely
                if (!product.isEnableWeedmap() || product.isDeleted()) {
                    // Check to see if this product was set to false
                    totalSkipped++;
                    if (productSyncItem != null) {
                        deleteWmV2Items(companyId, shopId, apiKeyMap, organizationId, productSyncItem, dbAccount.getWeedmapConfig());
                        deleted++;
                        deletedItems.add(new ObjectId(productSyncItem.getId()));
                    }
                    continue;
                }
                if (!published) {
                    // this item is supposed to be unpublished
                    // if it does not exist, then just skip
                    if (productSyncItem == null) {
                        totalSkipped++;
                        continue;
                    }
                }

                Brand brand = brandHashMap.getOrDefault(product.getBrandId(), defaultBrand);

                ThirdPartyProduct thirdPartyProduct = productMapping.getThirdPartyProduct();
                String thirdPartyBrandId = thirdPartyProduct != null ? thirdPartyProduct.getThirdPartyBrandId() : "";
                String thirdPartyProductId = thirdPartyProduct != null ? thirdPartyProduct.getSourceProductId() : "";

                WmSyncItems brandSyncItem = wmSyncItemsMap.get(brand.getId());
                String lastSyncBrand = productSyncItem != null ? productSyncItem.getWmLinkedItems().getOrDefault(WmSyncItems.WMItemType.BRANDS, "") : "";

                LOG.info(String.format("Product %s found %s | Brand Found: %s", product.getName(), productSyncItem != null , brandSyncItem != null));

                if (brandSyncItem != null && StringUtils.isNotBlank(lastSyncBrand) && !brandSyncItem.getWmItemId().equalsIgnoreCase(lastSyncBrand)) {
                    wmSyncItemsMap.remove(brand.getId());
                    if (productSyncItem != null && !productSyncItem.isVerified()) {
                        deletedItems.add(new ObjectId(brandSyncItem.getId()));
                    }
                    brandSyncItem = null;
                }

                if (brandSyncItem == null) {
                    syncBrands(companyId, shopId, apiKeyMap, brand, organizationId, syncItemList, wmSyncItemsMap, dbAccount.getWeedmapConfig(), thirdPartyBrandId);
                    brandSyncItem = wmSyncItemsMap.get(brand.getId());
                }

                if (brandSyncItem == null) {
                    LOG.info(String.format("Brand %s not synched to weedmap for company %s and shop %s", brand.getName(), companyId, shopId));
                    totalSkipped++;
                    continue;
                }

                //check if brand updated for product
                if (productSyncItem != null) {
                    String wmBrandId = productSyncItem.getWmLinkedItems() != null ? productSyncItem.getWmLinkedItems().get(WmSyncItems.WMItemType.BRANDS) : "";
                    boolean isDelete = (brandSyncItem != null && !brandSyncItem.getWmItemId().equalsIgnoreCase(lastSyncBrand))
                            || (productSyncItem.isVerified() && StringUtils.isBlank(thirdPartyProductId))
                            || (StringUtils.isNotBlank(thirdPartyProductId) && !productSyncItem.getWmItemId().equalsIgnoreCase(thirdPartyProductId));

                    if (isDelete) {
                        LOG.info(String.format("Brand is updated for product: %s, delete already linked items and product and add new", brand.getName()));
                        deleteWmV2Items(companyId, shopId, apiKeyMap, organizationId, productSyncItem, dbAccount.getWeedmapConfig());
                        wmSyncItemsMap.remove(wmBrandId);
                        wmSyncItemsMap.remove(product.getId());
                        WmSyncItems wmSyncItems = syncItemList.stream().filter(syncItem -> product.getId().equalsIgnoreCase(syncItem.getItemId())).findFirst().orElse(null);
                        if (wmSyncItems != null) {
                            syncItemList.remove(wmSyncItems);
                        }
                        deletedItems.add(new ObjectId(productSyncItem.getId()));
                        productSyncItem = null;
                    }
                }

                boolean isSync;
                if (productSyncItem == null) {
                    isSync = syncProducts(companyId, shop, apiKeyMap, product, organizationId, catalogId, syncItemList, updateItemList, wmSyncItemsMap, brandSyncItem, null, categoryHashMap.get(product.getCategoryId()), toleranceHashMap, tagsMapping, dbAccount, attributeResponse, productMapping, BigDecimal.valueOf(quantity), published, brand);

                    if (isSync) {
                        added++;
                    } else {
                        failed++;
                    }
                } else {
                    isSync = syncProducts(companyId, shop, apiKeyMap, product, organizationId, catalogId, syncItemList, updateItemList, wmSyncItemsMap, brandSyncItem, productSyncItem, categoryHashMap.get(product.getCategoryId()), toleranceHashMap, tagsMapping, dbAccount, attributeResponse, productMapping, BigDecimal.valueOf(quantity), published, brand);
                    if (isSync) {
                        updated++;
                        wmSyncItemsMap.put(product.getId(), productSyncItem);
                    } else {
                        failed++;
                        deletedItems.add(new ObjectId(productSyncItem.getId()));
                    }
                }
                productSyncItem = wmSyncItemsMap.get(product.getId());

                if (isSync && productSyncItem != null && productSyncItem.getWmLinkedItems().containsKey(WmSyncItems.WMItemType.PRODUCT_IMAGE) && !productSyncItem.isVerified()) {
                    imagesFound++;
                }
            }

            LOG.info("Added new menu items: " + added + " out of: " + total);
            LOG.info("Updated new menu items: " + updated + " out of: " + total);
            LOG.info("Skipped menu items: " + totalSkipped + " out of: " + total);
            LOG.info("Failed menu items: " + failed + " out of: " + total);
            LOG.info("Deleted menu items: " + deleted + " out of: " + total);
            LOG.info("Image found and synched: " + imagesFound + " out of: " + total);
            // set sync completed

            if (!deletedItems.isEmpty()) {
                weedmapSyncItemRepository.hardRemoveById(deletedItems);
            }
            if (!syncItemList.isEmpty()) {
                weedmapSyncItemRepository.save(syncItemList);
            }
            if (!updateItemList.isEmpty()) {
                updateItemList.forEach(weedmapSyncItems -> {
                    weedmapSyncItemRepository.update(companyId, weedmapSyncItems.getId(), weedmapSyncItems);
                });
            }

            productRepository.updateLastWMSyncTime(companyId, productObjIds, DateTime.now().getMillis());
        } catch (Exception e) {
            LOG.error("Fail to menu sync for Version 2: " + apiKeyMap.getName() + " token: " + apiKeyMap.getToken(), e);
            //dbAccount.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Failed);
           // dbAccount.setMenuLastSyncDate(DateTime.now().getMillis());
            //weedmapAccountRepository.update(companyId, dbAccount.getId(), dbAccount);

            weedmapAccountRepository.updateSyncStatus(companyId,dbAccount.getId(), WeedmapAccount.WeedmapSyncStatus.Failed,DateTime.now().getMillis());

        }
    }

    /**
     * Private method to delete version 2 menu items
     *
     * @param companyId        : companyId
     * @param shopId           : shopId
     * @param apiKeyMap        : apiKeyMap
     * @param organizationId   : organizationId
     * @param productSyncItem  : productSyncItem
     * @param weedmapConfig
     * @return true/false
     */
    private boolean deleteWmV2Items(String companyId, String shopId, WeedmapApiKeyMap apiKeyMap, String organizationId, WmSyncItems productSyncItem, WeedmapConfig weedmapConfig) {
        if (productSyncItem.getItemType() != WmSyncItems.WMItemType.PRODUCTS) {
            return true;
        }
        if (productSyncItem.getWmLinkedItems() != null && !productSyncItem.getWmLinkedItems().isEmpty()) {
            List<WmSyncItems.WMItemType> types = Lists.newArrayList(WmSyncItems.WMItemType.CATALOG_ITEMS, WmSyncItems.WMItemType.VARIANT_OPTIONS, WmSyncItems.WMItemType.PRODUCT_VARIANTS, WmSyncItems.WMItemType.PRODUCT_IMAGE, WmSyncItems.WMItemType.LINKED_TAGS, WmSyncItems.WMItemType.BATCHES);
            for (WmSyncItems.WMItemType linkedItems : types) {
                if (productSyncItem.getWmLinkedItems().containsKey(linkedItems)) {
                    if (linkedItems == WmSyncItems.WMItemType.BRANDS) {
                        continue;
                    }
                    if (linkedItems == WmSyncItems.WMItemType.LINKED_TAGS) {
                        List<String> list = Arrays.asList(productSyncItem.getWmLinkedItems().get(linkedItems).split(","));
                        LinkTagsRequest linkTagsRequest = WmRequestGenerator.linkTagsRequest(list);
                        weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, linkedItems, productSyncItem.getWmItemId(), linkTagsRequest, weedmapConfig);

                    } else if (linkedItems == WmSyncItems.WMItemType.PRODUCT_IMAGE || linkedItems == WmSyncItems.WMItemType.PRODUCT_VARIANTS || linkedItems == WmSyncItems.WMItemType.VARIANT_OPTIONS || linkedItems == WmSyncItems.WMItemType.CATALOG_ITEMS) {
                        String optionIdStr = productSyncItem.getWmLinkedItems().get(linkedItems);
                        Arrays.asList(optionIdStr.split(",")).stream().forEach(id -> {
                            weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, linkedItems, id, null, weedmapConfig);
                        });

                    } else {

                        weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, linkedItems, productSyncItem.getWmLinkedItems().get(linkedItems), null, weedmapConfig);
                    }
                }
            }
        }
        if (!productSyncItem.isVerified()) {
            return weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.PRODUCTS, productSyncItem.getWmItemId(), null, weedmapConfig);
        }
        return true;
    }

    /**
     * Private method to get wm tags mapping
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param dbAccount     : tagMapping
     * @param apiKeyMap      : apiKeyMap
     * @param tagsMappings
     * @return: map for tags and mappingId
     */
    private HashMap<String, String> getWmTagMapping(String companyId, String shopId, String organizationId, WeedmapAccountResult dbAccount, WeedmapApiKeyMap apiKeyMap, List<WmProductMapping> tagsMappings) {
        if (CollectionUtils.isNullOrEmpty(tagsMappings)) {
            return new HashMap<>();
        }

        WmTagsResponse response = weedmapAPIService.getWeedmapTags(companyId, shopId, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());
        if (response == null) {
            return new HashMap<>();
        }

        HashSet<String> tags = new HashSet<>();
        for (WmProductMapping mapping : tagsMappings) {
            for (String tag : mapping.getWmTags()) {
                if (StringUtils.isBlank(tag)) {
                    continue;
                }
                WmProductMapping.Tags value = WmProductMapping.Tags.toTag(tag);
                tags.add(value.name);
            }
        }

        HashMap<String, String> returnMap = new HashMap<>();
        for (WmDataRequest request : response.getData()) {
            String tag = request.getWeedmapAttributes().getName();
            if (tags.contains(tag)) {
                returnMap.putIfAbsent(tag, request.getId());
            }
            returnMap.putIfAbsent(tag, request.getId());
            if (tag.equalsIgnoreCase(WmProductMapping.Tags.INDICA.name) || tag.equalsIgnoreCase(WmProductMapping.Tags.SATIVA.name) || tag.equalsIgnoreCase(WmProductMapping.Tags.HYBRID.name) || tag.equalsIgnoreCase(WmProductMapping.Tags.CBD.name)) {
                returnMap.put(tag, request.getId());
            }
        }

        return returnMap;
    }

    /**
     * Private method to sync products
     *
     * @param companyId         : companyId
     * @param shop            : shopId
     * @param apiKeyMap         : apiKeyMap
     * @param product           : product
     * @param organizationId    : organizationId
     * @param catalogId         : catalogId
     * @param syncList          : syncList
     * @param updateItemList    : updateItemList
     * @param wmSyncItemMap     : wmSyncItemsMap
     * @param brandSyncItem     : brandSyncItem
     * @param productSyncItem   : productSyncItem
     * @param category          : category
     * @param toleranceMap      : toleranceMap
     * @param tagsMappingMap    : tagsMappingMap
     * @param dbAccount         : account
     * @param attributeResponse : attributeResponse
     * @param productMapping    : product mapping
     * @param currentQuantity   : currentQuantity
     * @param brand             : brand
     * @return : true/false
     */
    private boolean syncProducts(String companyId, Shop shop, WeedmapApiKeyMap apiKeyMap, Product product, String organizationId, String catalogId, List<WmSyncItems> syncList, List<WmSyncItems> updateItemList, HashMap<String, WmSyncItems> wmSyncItemMap, WmSyncItems brandSyncItem, WmSyncItems productSyncItem, ProductCategory category, HashMap<String, ProductWeightTolerance> toleranceMap, HashMap<String, String> tagsMappingMap, WeedmapAccountResult dbAccount, WmAttributeResponse attributeResponse, WmProductMapping productMapping, BigDecimal currentQuantity, boolean published, Brand brand) {

        if (category == null) {
            LOG.info(String.format("Fail to sync product %s , category not found..", product.getName()));
            return false;
        }
        String id = null;
        String wmsin = null;
        WmSyncResponse response;
        HashMap<WmSyncItems.WMItemType, String> wmLinkedItems = new HashMap<>();
        String wmBrandId = brandSyncItem.getWmItemId();
        List<WmSyncMenuDetails.WmProductVariantDetails> variantDetailsList = new ArrayList<>();

        if (productSyncItem != null) {
            id = productSyncItem.getWmItemId();
            wmsin = productSyncItem.getWmsin();
            wmLinkedItems = productSyncItem.getWmLinkedItems();
            variantDetailsList = productSyncItem.getVariantDetails();
        }

        boolean isSync = false;
        if (productMapping.getThirdPartyProduct() != null && StringUtils.isNotBlank(productMapping.getThirdPartyProduct().getSourceProductId())) {
            ThirdPartyProduct thirdPartyProduct = productMapping.getThirdPartyProduct();
            wmLinkedItems.putIfAbsent(WmSyncItems.WMItemType.BRANDS, wmBrandId);

            isSync = syncVerifiedProductLinkItems(companyId, shop.getId(), organizationId, catalogId, apiKeyMap, product, category, thirdPartyProduct, toleranceMap, wmLinkedItems, dbAccount, currentQuantity, published, variantDetailsList);
            if (isSync && productSyncItem == null) {
                WmSyncItems syncItem = createSyncItem(companyId, shop.getId(), apiKeyMap.getListingWmId(), thirdPartyProduct.getSourceProductId(), product.getId(), "", WmSyncItems.WMItemType.PRODUCTS, wmLinkedItems, true, variantDetailsList);
                syncList.add(syncItem);
                wmSyncItemMap.put(product.getId(), syncItem);
            } else if (isSync) {
                updateItemList.add(productSyncItem);
            }
        } else {
            String productName = product.getName();
            if (shop.isAppendBrandNameWM() && brand != null && !brand.isDefault() && StringUtils.isNotBlank(brand.getName())) {
                productName = String.format("%s | %s", brand.getName(), productName);
            }
            WmSyncRequest request = WmRequestGenerator.productRequest(organizationId, wmBrandId, productName, product.getDescription(), id, wmsin);
            response = weedmapAPIService.syncItems(companyId, shop.getId(), organizationId, apiKeyMap, request, WmSyncResponse.class, dbAccount.getWeedmapConfig());

            if (response != null) {
                isSync = syncProductLinkedItems(companyId, shop.getId(), organizationId, catalogId, apiKeyMap, product, category, response.getId(), toleranceMap, wmLinkedItems, tagsMappingMap, dbAccount, attributeResponse, productMapping, currentQuantity, published, variantDetailsList);
            }
            wmLinkedItems.putIfAbsent(WmSyncItems.WMItemType.BRANDS, wmBrandId);

            if (!isSync && response != null) {
                WmSyncItems item = new WmSyncItems();
                item.setWmLinkedItems(wmLinkedItems);
                item.setItemType(WmSyncItems.WMItemType.PRODUCTS);
                item.setWmItemId(response.getId());
                deleteWmV2Items(companyId, shop.getId(), apiKeyMap, organizationId, item, dbAccount.getWeedmapConfig());
                return isSync;
            }
            if (response != null && productSyncItem == null && isSync) {
                WmSyncItems syncItem = createSyncItem(companyId, shop.getId(), apiKeyMap.getListingWmId(), response.getId(), product.getId(), response.getWmData().getWeedmapAttributes().getWmsin(), WmSyncItems.WMItemType.PRODUCTS, wmLinkedItems, false, variantDetailsList);
                syncList.add(syncItem);
                wmSyncItemMap.put(product.getId(), syncItem);
            } else if (response != null && isSync) {
                productSyncItem.setVariantDetails(variantDetailsList);
                updateItemList.add(productSyncItem);
            }
        }
        return isSync;
    }


    /**
     * Private method to sync product's linked items for non verified products
     *
     * @param companyId          : companyId
     * @param shopId             : shopId
     * @param organizationId     : organizationId
     * @param catalogId          : catalogId
     * @param apiKeyMap          : apiKeyMap
     * @param product            : product
     * @param category           : category
     * @param wmProductId        : wmProductId
     * @param toleranceMap       : toleranceMap
     * @param wmLinkedItems      : wmLinkedItems
     * @param tagsMappingMap     : tagsMappingMap
     * @param dbAccount          : dbAccount
     * @param attributeResponse  : attributeResponse
     * @param tagsMapping        : tagMapping
     * @param currentQuantity    : currentQuantity
     * @param variantDetailsList
     * @return : true/false
     */
    private boolean syncProductLinkedItems(String companyId, String shopId, String organizationId, String catalogId, WeedmapApiKeyMap apiKeyMap, Product product, ProductCategory category, String wmProductId, HashMap<String, ProductWeightTolerance> toleranceMap, HashMap<WmSyncItems.WMItemType, String> wmLinkedItems, HashMap<String, String> tagsMappingMap, WeedmapAccountResult dbAccount, WmAttributeResponse attributeResponse, WmProductMapping tagsMapping, BigDecimal currentQuantity, boolean published, List<WmSyncMenuDetails.WmProductVariantDetails> variantDetailsList) {
        // Verify tags first
        if (variantDetailsList.isEmpty() && !wmLinkedItems.isEmpty()) {
            WmSyncMenuDetails.WmSyncProductDetails wmCustomProduct = weedmapAPIService.getWmCustomProduct(companyId, shopId, organizationId, dbAccount.getWeedmapConfig(), apiKeyMap, wmProductId);
            wmLinkedItems.putAll(wmCustomProduct.getLinkedItems());
            variantDetailsList.addAll(wmCustomProduct.getWeightVariantDetailsMap().values());
        }

        boolean lastSyncCategory = wmLinkedItems.containsKey(WmSyncItems.WMItemType.VARIANT_OPTIONS); // last synced with grams(true) or units(false)
        boolean newCategory = category.getUnitType() == ProductCategory.UnitType.grams;

        if (lastSyncCategory != newCategory && !wmLinkedItems.isEmpty()) {
            //only delete variant items when category is changed.
            deleteVariantItems(companyId, shopId, wmLinkedItems, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());
            variantDetailsList.clear();
        }

        Set<String> tagsList = new HashSet<>();
        if (tagsMapping != null) {
            for (String tag : tagsMapping.getWmTags()) {
                if (StringUtils.isBlank(tag)) {
                    continue;
                }
                WmProductMapping.Tags value = WmProductMapping.Tags.toTag(tag);
                if (tagsMappingMap.containsKey(value.name)) {
                    tagsList.add(tagsMappingMap.get(value.name));
                }
            }
        }

        if (StringUtils.isNotBlank(product.getFlowerType())) {
            // if the weedmap category is a flower type, then check the flower type
            String flowerType = product.getFlowerType().trim();
            try {
                WmProductMapping.Tags value = WmProductMapping.Tags.toTag(flowerType);
                tagsList.add(tagsMappingMap.get(value.name));
            } catch (Exception e) {
                String lowertype = flowerType.toLowerCase();
                if (lowertype.startsWith("indica")) {
                    tagsList.add(tagsMappingMap.get(WmProductMapping.Tags.INDICA.name));
                } else if (lowertype.startsWith("sativa")) {
                    tagsList.add(tagsMappingMap.get(WmProductMapping.Tags.SATIVA.name));
                } else {
                    tagsList.add(tagsMappingMap.get(WmProductMapping.Tags.HYBRID.name));
                }
            }
        }

        if (product.getCannabisType() == Product.CannabisType.CBD || category.getCannabisType() == Product.CannabisType.CBD) {
            WmProductMapping.Tags value = WmProductMapping.Tags.toTag(product.getCannabisType().name());
            if (tagsMappingMap.containsKey(value.name)) {
                tagsList.add(tagsMappingMap.get(value.name));
            }
        }

        if (tagsMapping != null && !CollectionUtils.isNullOrEmpty(tagsMapping.getProductTagGroups())) {
            for (ProductTagGroups productTagGroups : tagsMapping.getProductTagGroups()) {
                tagsList.add(productTagGroups.getWmTagGroup());
                tagsList.add(productTagGroups.getWmDiscoveryTag());
            }
        }

        /*if (StringUtils.isBlank(product.getFlowerType())) {
            tagsList.add(tagsMappingMap.get(WmProductMapping.Tags.INDICA.name));
        }*/

        if (tagsList.isEmpty()) {
            LOG.info(String.format("No tags found for category: %s", category.getName()));
            return false;
        }

        tagsList.remove(null);
        LinkTagsRequest linkTagsRequest = WmRequestGenerator.linkTagsRequest(Lists.newArrayList(tagsList));
        boolean isValid = weedmapAPIService.validateTags(companyId, shopId, organizationId, linkTagsRequest, apiKeyMap, dbAccount.getWeedmapConfig());
        if (!isValid) {
            LOG.info("Tags are not valid for product: " + tagsList);
            //return isValid;
        }

        WmSyncResponse response = null;

        // Sync images
        List<String> imageIds = new ArrayList<>();
        if (wmLinkedItems.containsKey(WmSyncItems.WMItemType.PRODUCT_IMAGE)) {
            String imageIdStr = wmLinkedItems.get(WmSyncItems.WMItemType.PRODUCT_IMAGE);
            Arrays.asList(imageIdStr.split(",")).forEach(id -> {
                weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.PRODUCT_IMAGE, id, null, dbAccount.getWeedmapConfig());
            });
            wmLinkedItems.remove(WmSyncItems.WMItemType.PRODUCT_IMAGE);
        }
        if (product.getAssets() != null) {
            for (CompanyAsset asset : product.getAssets()) {
                String imageUrl = "";

                if (StringUtils.isNotBlank(asset.getLargeURL())) {
                    imageUrl = asset.getLargeURL();
                } else if (StringUtils.isNotBlank(asset.getMediumURL())) {
                    imageUrl = asset.getMediumURL();
                } else if (StringUtils.isNotBlank(asset.getThumbURL())) {
                    imageUrl = asset.getThumbURL();
                } else if (StringUtils.isNotBlank(asset.getPublicURL())) {
                    imageUrl = asset.getPublicURL();
                }
                WmSyncRequest request;
                if (StringUtils.isNotBlank(imageUrl)) {
                    request = WmRequestGenerator.imageRequest(wmProductId, imageUrl, imageIds.isEmpty(), wmLinkedItems.get(WmSyncItems.WMItemType.PRODUCT_IMAGE));
                    response = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, request, WmSyncResponse.class, dbAccount.getWeedmapConfig());

                    if (response != null) {
                        imageIds.add(response.getId());
                    }
                }
            }
            if (!imageIds.isEmpty()) {
                wmLinkedItems.put(WmSyncItems.WMItemType.PRODUCT_IMAGE, StringUtils.join(imageIds, ","));
            }
        }
        BigDecimal cbdAmount = BigDecimal.ZERO;
        BigDecimal thcAmount = BigDecimal.ZERO;
        String potencyUnitType = "percent";
        if (product.getCbd() > 0) {
            cbdAmount = BigDecimal.valueOf(product.getCbd());
        }

        if (product.getThc() > 0) {
            thcAmount = BigDecimal.valueOf(product.getThc());
        }

        if (product.isPotency() && product.getPotencyAmount() != null) {
            potencyUnitType = "milligrams";
            thcAmount = product.getPotencyAmount().getThc();
            cbdAmount = product.getPotencyAmount().getCbd();
        }


        WmSyncRequest batchRequest = WmRequestGenerator.createBatchRequest(wmProductId, organizationId, thcAmount, cbdAmount, potencyUnitType, wmLinkedItems.get(WmSyncItems.WMItemType.BATCHES));
        response = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, batchRequest, WmSyncResponse.class, dbAccount.getWeedmapConfig());

        if (response != null) {
            wmLinkedItems.put(WmSyncItems.WMItemType.BATCHES, response.getId());
        }

        HashMap<String, BigDecimal> priceRangeMap = new HashMap<>();
        if (category.getUnitType() == ProductCategory.UnitType.grams) {
            getProductPricesMap(product, category, toleranceMap, priceRangeMap);
        } else {
            priceRangeMap.put("Each", product.getUnitPrice());
        }

        List<String> optionIds = new ArrayList<>();
        HashMap<String, WmSyncRequest> variantRequestMap = new HashMap<>();

        LinkedHashMap<String, WmSyncMenuDetails.WmProductVariantDetails> weightVariantMap = new LinkedHashMap<>();
        variantDetailsList.forEach(wmProductVariantDetails -> {
            weightVariantMap.putIfAbsent(wmProductVariantDetails.getWeightKey(), wmProductVariantDetails);
        });

        /*HashMap<WmSyncItems.WMItemType, String> deleteWmLinkedItems = new HashMap<>();
        weightVariantMap.values().forEach(wmProductVariantDetails -> {
            String weightKey = wmProductVariantDetails.getWeightKey();
            if (!priceRangeMap.containsKey(weightKey)) {
                String catalogItemId = deleteWmLinkedItems.getOrDefault(WmSyncItems.WMItemType.CATALOG_ITEMS, "");
                String variantId = deleteWmLinkedItems.getOrDefault(WmSyncItems.WMItemType.PRODUCT_VARIANTS, "");
                String optionId = deleteWmLinkedItems.getOrDefault(WmSyncItems.WMItemType.VARIANT_OPTIONS, "");

                if (StringUtils.isNotBlank(catalogItemId)) {
                    catalogItemId = catalogItemId + ",";
                }
                if (StringUtils.isNotBlank(variantId)) {
                    variantId = variantId + ",";
                }
                if (StringUtils.isNotBlank(optionId)) {
                    optionId = optionId + ",";
                }

                catalogItemId += wmProductVariantDetails.getCatalogId();
                variantId += wmProductVariantDetails.getVariantId();
                if (StringUtils.isNotBlank(wmProductVariantDetails.getOptionId())) {
                    optionId += wmProductVariantDetails.getOptionId();
                    deleteWmLinkedItems.put(WmSyncItems.WMItemType.VARIANT_OPTIONS, optionId);
                }

                deleteWmLinkedItems.put(WmSyncItems.WMItemType.CATALOG_ITEMS, catalogItemId);
                deleteWmLinkedItems.put(WmSyncItems.WMItemType.PRODUCT_VARIANTS, variantId);
            }
        });
        if (!deleteWmLinkedItems.isEmpty()) {
            deleteVariantItems(companyId, shopId, deleteWmLinkedItems, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());
        }*/
        if (category.getUnitType() == ProductCategory.UnitType.grams) {
            //deleteVariantItems(companyId, shopId, wmLinkedItems, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());
            /* Create new options*/
            priceRangeMap.entrySet().forEach(entry -> {
                BigDecimal price = entry.getValue();
                String key = entry.getKey();
                WmSyncMenuDetails.WmProductVariantDetails variantDetails = weightVariantMap.get(key);

                if (attributeResponse != null && attributeResponse.getVariantValues() != null && attributeResponse.getVariantValues().containsKey(key)) {
                    String attributeId = attributeResponse.getId();
                    String valueId = attributeResponse.getVariantValues().get(key);
                    // Sync Variants
                    String sku = product.getSku() + "-" + key;
                    BigDecimal unitPrice = price.multiply(BigDecimal.valueOf(100));
                    boolean soldBy = true;

                    WmSyncRequest req = WmRequestGenerator.productVariantRequest(organizationId, wmProductId, unitPrice, sku, soldBy, variantDetails != null ? variantDetails.getVariantId() : null);

                    WmSyncResponse res = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, req, WmSyncResponse.class, dbAccount.getWeedmapConfig());

                    if (res == null) {
                        return;
                    }
                    String variantId = res.getId();
                    variantRequestMap.put(res.getId(), req);

                    if (variantDetails == null || StringUtils.isBlank(variantDetails.getOptionId())) {
                        req = WmRequestGenerator.createOptionRequest(variantId, attributeId, valueId);
                        res = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, req, WmSyncResponse.class, dbAccount.getWeedmapConfig());
                        if (res == null) {
                            return;
                        }
                        optionIds.add(res.getId());
                        variantDetails = new WmSyncMenuDetails.WmProductVariantDetails();
                        variantDetails.setVariantId(variantId);
                        variantDetails.setOptionId(res.getId());
                        variantDetails.setWeightKey(key);
                        weightVariantMap.putIfAbsent(variantDetails.getWeightKey(), variantDetails);
                    } else {
                        optionIds.add(variantDetails.getOptionId());
                    }
                }
            });

            if (optionIds.isEmpty() || variantRequestMap.isEmpty()) {
                LOG.info(String.format("Variant, Options not created at weedmap for company : %s shop: %s", companyId, shopId));
                return false;
            }
        } else if (category.getUnitType() == ProductCategory.UnitType.units) {
            //deleteVariantItems(companyId, shopId, wmLinkedItems, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());
            String sku = product.getSku() + "-" + "Each";
            BigDecimal unitPrice = product.getUnitPrice().multiply(BigDecimal.valueOf(100));
            boolean soldBy = false;
            WmSyncMenuDetails.WmProductVariantDetails variantDetails = weightVariantMap.get("Each");
            WmSyncRequest req = WmRequestGenerator.productVariantRequest(organizationId, wmProductId, unitPrice, sku, soldBy, variantDetails != null ? variantDetails.getVariantId() : null);

            WmSyncResponse res = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, req, WmSyncResponse.class, dbAccount.getWeedmapConfig());

            if (res == null) {
                LOG.info(String.format("Variant, Options not created at weedmap for company : %s shop: %s", companyId, shopId));
                return false;
            }
            variantRequestMap.put(res.getId(), req);
            if (variantDetails == null) {
                variantDetails = new WmSyncMenuDetails.WmProductVariantDetails();
            }
            variantDetails.setVariantId(res.getId());
            variantDetails.setWeightKey("Each");
            weightVariantMap.putIfAbsent("Each", variantDetails);
            if (StringUtils.isNotBlank(variantDetails.getOptionId())) {
                weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.VARIANT_OPTIONS, variantDetails.getVariantId(), null, dbAccount.getWeedmapConfig());
            }
        }

        if (!variantRequestMap.isEmpty()) {
            wmLinkedItems.put(WmSyncItems.WMItemType.PRODUCT_VARIANTS, StringUtils.join(variantRequestMap.keySet(), ","));
        }

        if (!optionIds.isEmpty()) {
            wmLinkedItems.put(WmSyncItems.WMItemType.VARIANT_OPTIONS, StringUtils.join(optionIds, ","));
        }

        if (wmLinkedItems.containsKey(WmSyncItems.WMItemType.LINKED_TAGS)) {
            List<String> list = Arrays.asList(wmLinkedItems.get(WmSyncItems.WMItemType.LINKED_TAGS).split(","));
            LinkTagsRequest deleteTagRequest = WmRequestGenerator.linkTagsRequest(list);
            weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.LINKED_TAGS, wmProductId, deleteTagRequest, dbAccount.getWeedmapConfig());
        }
        boolean isTags = weedmapAPIService.linkProductTags(companyId, shopId, organizationId, apiKeyMap, linkTagsRequest, wmProductId, dbAccount.getWeedmapConfig());

        if (!isTags) {
            LOG.info(String.format("Tags not linked at weedmap for category: %s company : %s shop: %s", category.getName(), companyId, shopId));
            return false;
        }

        wmLinkedItems.put(WmSyncItems.WMItemType.LINKED_TAGS, StringUtils.join(tagsList, ","));

        List<String> catelogIds = new ArrayList<>();
        List<String> catalogInventoryIds = new ArrayList<>();
        variantRequestMap.entrySet().forEach(entry -> {
            String vId = entry.getKey();
            Long price = entry.getValue().getWmData().getWeedmapAttributes().getMsrp();
            price = (price == null) ? 0 : price;
            WmSyncMenuDetails.WmProductVariantDetails variantDetails = weightVariantMap.values().stream().filter(wmProductVariantDetails -> vId.equalsIgnoreCase(wmProductVariantDetails.getVariantId())).findFirst().orElse(null);

            WmSyncRequest req = WmRequestGenerator.catalogItemRequest(catalogId, vId, published, true, BigDecimal.valueOf(price), variantDetails != null ? variantDetails.getCatalogId() : null);
            WmSyncResponse res = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, req, WmSyncResponse.class, dbAccount.getWeedmapConfig());

            if (res == null) {
                return;
            }

            if (variantDetails != null) {
                variantDetails.setCatalogId(res.getId());
            }

            WmSyncRequest inventoryRequest = WmRequestGenerator.createInventoryRequest(organizationId, currentQuantity);
            WmSyncResponse inventoryResponse = weedmapAPIService.syncInventory(companyId, shopId, organizationId, res.getId(), apiKeyMap, inventoryRequest, WmSyncResponse.class, dbAccount.getWeedmapConfig());

            if (inventoryResponse == null) {
                return;
            }

            catelogIds.add(res.getId());
            catalogInventoryIds.add(res.getId());

        });

        if (catelogIds.isEmpty()) {
            LOG.info(String.format("Catelog items not created: %s company : %s shop: %s", category.getName(), companyId, shopId));
            return false;
        }
        wmLinkedItems.put(WmSyncItems.WMItemType.CATALOG_ITEMS, StringUtils.join(catelogIds, ","));

        if (!catalogInventoryIds.isEmpty()) {
            wmLinkedItems.put(WmSyncItems.WMItemType.INVENTORY, StringUtils.join(catalogInventoryIds, ","));
        }

        variantDetailsList.clear();
        variantDetailsList.addAll(Lists.newArrayList(weightVariantMap.values()));
        return true;

    }

    /**
     * Private method to delete variant related items for prices
     * @param companyId        :  companyId
     * @param shopId           :  shopId
     * @param wmLinkedItems    :  wmLinkedItems
     * @param organizationId   :  organizationId
     * @param apiKeyMap        :  apiKeyMap
     * @param weedmapConfig
     */
    private void deleteVariantItems(String companyId, String shopId, HashMap<WmSyncItems.WMItemType, String> wmLinkedItems, String organizationId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        /* Remove old variants*/
        /*if (wmLinkedItems.containsKey(WmSyncItems.WMItemType.INVENTORY)) {
            String optionIdStr = wmLinkedItems.get(WmSyncItems.WMItemType.INVENTORY);
            Arrays.asList(optionIdStr.split(",")).forEach(id -> {
                weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.INVENTORY, id, null, weedmapConfig);
            });
            wmLinkedItems.remove(WmSyncItems.WMItemType.INVENTORY);
        }*/
        if (wmLinkedItems.containsKey(WmSyncItems.WMItemType.CATALOG_ITEMS)) {
            String optionIdStr = wmLinkedItems.get(WmSyncItems.WMItemType.CATALOG_ITEMS);
            Arrays.asList(optionIdStr.split(",")).forEach(id -> {
                weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.CATALOG_ITEMS, id, null, weedmapConfig);
            });
            wmLinkedItems.remove(WmSyncItems.WMItemType.CATALOG_ITEMS);
        }
        if (wmLinkedItems.containsKey(WmSyncItems.WMItemType.VARIANT_OPTIONS)) {
            String optionIdStr = wmLinkedItems.get(WmSyncItems.WMItemType.VARIANT_OPTIONS);
            Arrays.asList(optionIdStr.split(",")).forEach(id -> {
                weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.VARIANT_OPTIONS, id, null, weedmapConfig);
            });
            wmLinkedItems.remove(WmSyncItems.WMItemType.VARIANT_OPTIONS);
        }
        if (wmLinkedItems.containsKey(WmSyncItems.WMItemType.PRODUCT_VARIANTS)) {
            String optionIdStr = wmLinkedItems.get(WmSyncItems.WMItemType.PRODUCT_VARIANTS);
            Arrays.asList(optionIdStr.split(",")).forEach(id -> {
                weedmapAPIService.deleteWmItem(companyId, shopId, organizationId, apiKeyMap, WmSyncItems.WMItemType.PRODUCT_VARIANTS, id, null, weedmapConfig);
            });
            wmLinkedItems.remove(WmSyncItems.WMItemType.PRODUCT_VARIANTS);
        }
    }

    /**
     * @param companyId           :  companyId
     * @param shopId              :  shopId
     * @param apiKeyMap           :  apiKeyMap
     * @param brand               :  brand
     * @param organizationId      :  organizationId
     * @param syncList            :  syncList
     * @param weedmapSyncItemsMap :  weedmapSyncItemsMap
     * @param thirdPartyBrandId   :  third party brand
     */
    private void syncBrands(String companyId, String shopId, WeedmapApiKeyMap apiKeyMap, Brand brand, String organizationId, List<WmSyncItems> syncList, HashMap<String, WmSyncItems> weedmapSyncItemsMap, WeedmapConfig weedmapConfig, String thirdPartyBrandId) {
        if (StringUtils.isNotBlank(thirdPartyBrandId)) {
            WmSyncItems syncItem = createSyncItem(companyId, shopId, apiKeyMap.getListingWmId(), thirdPartyBrandId, brand.getId(), "", WmSyncItems.WMItemType.BRANDS, null, true);
            //syncList.add(syncItem);
            weedmapSyncItemsMap.put(brand.getId(), syncItem);
        } else {
            WmSyncRequest request = WmRequestGenerator.brandRequest(organizationId, brand.getName());
            WmSyncResponse response = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, request, WmSyncResponse.class, weedmapConfig);
            if (response != null) {
                WmSyncItems syncItem = createSyncItem(companyId, shopId, apiKeyMap.getListingWmId(), response.getWmData().getId(), brand.getId(), "", WmSyncItems.WMItemType.BRANDS, null, false);
                syncList.add(syncItem);
                weedmapSyncItemsMap.put(brand.getId(), syncItem);
            }
        }
    }

    /**
     * Private method to create default brand
     *
     * @param companyId : companyId
     * @return : brand
     */
    private Brand getDefaultBrand(String companyId) {
        Brand brand = brandRepository.getDefaultBrand(companyId);
        if (brand == null) {
            Company company = companyRepository.getById(companyId);
            brand = new Brand();
            brand.prepare(companyId);
            brand.setActive(true);
            brand.setName(company.getName() + "-Default");
            brand.setDefault(true);
            brandRepository.save(brand);
        }
        return brand;
    }

    /**
     * @param companyId            :  companyId
     * @param shopId               :  shopId
     * @param categoryHashMap      :  categoryHashMap
     * @param toleranceHashMap     :  toleranceHashMap
     * @param dbAccount            :  dbAccount
     * @param weedmapApiKeyMap     :  weedmapApiKeyMap
     * @param brandHashMap
     */
    private void syncVersion1Weedmap(String companyId, String shopId, Iterable<Product> products,
                                     HashMap<String, ProductCategory> categoryHashMap,
                                     HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                     WeedmapAccount dbAccount,
                                     WeedmapApiKeyMap weedmapApiKeyMap,
                                     HashMap<String, Set<String>> regionInventoriesMap, HashMap<String, Brand> brandHashMap) {
        try {
            HashMap<String, WeedmapMenuItem> currentMenuItems = new HashMap<>();
            WeedmapResult weedmapResult = weedmapAPIService.getWeedmapMenu(weedmapApiKeyMap.getApiKey());

            LOG.info("Found previous weedmap menu items: " + weedmapResult.getData().size());
            for (WeedmapMenuItem menuItem : weedmapResult.getData()) {
                currentMenuItems.put(menuItem.getItemId(), menuItem);
            }


            long afterDate = weedmapApiKeyMap.getMenuLastSyncDate() == null ? 0 : weedmapApiKeyMap.getMenuLastSyncDate();

            Shop shop = shopRepository.getById(shopId);

            List<String> productIds = new ArrayList<>();
            List<ObjectId> productObjIds = new ArrayList<>();
            for (Product product : products) {
                productIds.add(product.getId());
                productObjIds.add(new ObjectId(product.getId()));
            }

            Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(companyId, shopId, productIds);

            int added = 0;
            int updated = 0;
            int failed = 0;
            int imagesFound = 0;
            int deleted = 0;
            int total = 0;
            int totalSkipped = 0;
            int unmapped = 0;
            for (Product product : products) {
                total++;

                boolean published = true;

                double quantity = getProductQuantity(product, prepackageQuantities);
                quantity = NumberUtils.round(quantity,2);

                if (weedmapApiKeyMap.getRegion() != null && regionInventoriesMap.containsKey(weedmapApiKeyMap.getRegion().getId())) {
                    double regionQty = getProductQuantity(product, regionInventoriesMap.get(weedmapApiKeyMap.getRegion().getId()), prepackageQuantities);
                    if ((product.getWmThreshold() != null && regionQty < product.getWmThreshold().doubleValue()) || regionQty <= 0) {
                        published = false;
                    }
                }

                if (product.isActive() == false || (product.getWmThreshold() != null && quantity < product.getWmThreshold().doubleValue()) || quantity <= 0) {
                    // unpublish
                    published = false;
                }


                // if product is disabled for weedmaps or product is deleted, remove from weedmaps completely
                if (!product.isEnableWeedmap() || product.isDeleted()) {
                    // Check to see if this product was set to false
                    totalSkipped++;
                    if (currentMenuItems.containsKey(product.getId())) {
                        weedmapAPIService.deleteWeedmapMenuItem(weedmapApiKeyMap.getApiKey(), product.getId());
                        deleted++;
                    }
                    continue;
                }
                if (published == false) {
                    // this item is supposed to be unpublished
                    // if it does not exist, then just skip
                    if (!currentMenuItems.containsKey(product.getId())) {
                        totalSkipped++;
                        continue;
                    }
                }


                try {
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());

                    WeedmapCategory weedmapCategory = getWeedmapCategory(product, category, dbAccount);
                    if (weedmapCategory != null) {
                        // we should be able to sync now
                        Brand brand = brandHashMap.get(product.getBrandId());

                        String productName = product.getName();
                        if (shop.isAppendBrandNameWM() && brand != null && !brand.isDefault() && StringUtils.isNotBlank(brand.getName())) {
                            productName = String.format("%s | %s", brand.getName(), productName);
                        }
                        WeedmapItemRequest request = new WeedmapItemRequest();
                        request.setItemId(product.getId());
                        request.setName(productName);
                        request.setBody(product.getDescription());
                        request.setCategoryId(weedmapCategory.id);
                        request.setCategoryName(weedmapCategory.title);
                        request.setPublished(published);

                        if (product.getThc() > 0) {
                            request.setThc(String.format("%.2f", product.getThc() / 100));
                            request.setLabTested(true);
                        }
                        if (product.getCbd() > 0) {
                            request.setCbd(String.format("%.2f", product.getCbd() / 100));
                            request.setLabTested(true);
                        }
                        if (product.getCbn() > 0) {
                            request.setCbn(String.format("%.2f", product.getCbn() / 100));
                            request.setLabTested(true);
                        }

                        if (product.getAssets() != null) {
                            for (CompanyAsset asset : product.getAssets()) {
                                if (StringUtils.isNotBlank(asset.getLargeURL())) {
                                    request.setImageURL(asset.getLargeURL());
                                    LOG.info("Found large image: " + asset.getLargeURL());
                                    imagesFound++;
                                    break;
                                } else if (StringUtils.isNotBlank(asset.getPublicURL())) {
                                    request.setImageURL(asset.getPublicURL());
                                    LOG.info("Found image: " + asset.getPublicURL());
                                    imagesFound++;
                                    break;
                                }
                            }
                        }
                        // configure prices
                        WeedmapPrices prices = new WeedmapPrices();
                        request.setPrices(prices);
                        processProductPrices(product, category, toleranceHashMap, prices, weedmapCategory);

                        // Now sync
                        try {
                            String apiKey = weedmapApiKeyMap.getApiKey();
                            if (currentMenuItems.containsKey(product.getId())) {
                                // Do an update instead
                                weedmapAPIService.updateWeedmapMenuItem(apiKey, product.getId(), request);
                                updated++;
                            } else {
                                weedmapAPIService.addWeedmapMenuItem(apiKey, request);
                                added++;
                            }
                        } catch (RestErrorException e) {
                            // fail to sync
                            LOG.error("Fail to sync: " + product.getName() + "\n" + e.getErrorResponse(), e);
                            failed++;
                        } catch (Exception e) {
                            // fail to sync
                            LOG.error("Fail to sync: " + product.getName(), e);
                            failed++;
                        }
                    } else {
                        LOG.info("Fail to sync: " + product.getName() + " --- UNMAPPED!");
                        unmapped++;
                    }
                } catch (Exception e) {
                    LOG.error("Fail to sync: " + product.getName(), e);
                    failed++;
                }

            }

            LOG.info("Added new menu items: " + added + " out of: " + total);
            LOG.info("Updated new menu items: " + updated + " out of: " + total);
            LOG.info("Skipped menu items: " + totalSkipped + " out of: " + total);
            LOG.info("Failed menu items: " + failed + " out of: " + total);
            LOG.info("Deleted menu items: " + deleted + " out of: " + total);
            LOG.info("Images found: " + imagesFound + " out of: " + total);
            LOG.info("Unmapped found: " + unmapped + " out of: " + total);
            // set sync completed

            productRepository.updateLastWMSyncTime(companyId,productObjIds,DateTime.now().getMillis());
        } catch (Exception e) {
            LOG.error("Fail to menu sync: " + weedmapApiKeyMap.getName() + " key: " + weedmapApiKeyMap.getApiKey(), e);
            dbAccount.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Failed);
            dbAccount.setMenuLastSyncDate(DateTime.now().getMillis());
            //weedmapAccountRepository.update(companyId, dbAccount.getId(), dbAccount);

            weedmapAccountRepository.updateSyncStatus(companyId,dbAccount.getId(), WeedmapAccount.WeedmapSyncStatus.Failed,DateTime.now().getMillis());

        }
    }



    private WeedmapAccountResult getWeedmapAccount(String companyId, String shopId, String accountId) {
        WeedmapAccountResult result;
        if (StringUtils.isNotBlank(accountId)) {
            result = weedmapAccountRepository.get(companyId, accountId, WeedmapAccountResult.class);
        } else {
            result = weedmapAccountRepository.getWeedmapAccount(companyId, shopId, WeedmapAccountResult.class);
        }
        if (result == null) {
            return null;
        }

        WeedmapConfig weedmapConfig = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);
        result.setWeedmapConfig(weedmapConfig);
        return result;
    }

    /**
     * Private method to get already synced wm items
     * @param companyId           : companyId
     * @param shopId              : shopId
     * @param organizationItem    : organizationItem
     * @param weedmapSyncItemsMap : wmSyncItemsmap
     * @param weedmapConfig       : weedmapconfig
     * @param apiKeyMap           : apiKeymap
     * @param syncList            : syncList
     */
    private void syncWmMenuItems(String companyId, String shopId, WmSyncItems organizationItem, HashMap<String, WmSyncItems> weedmapSyncItemsMap, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, List<WmSyncItems> syncList) {
        HashMap<String, WmSyncMenuDetails> details = weedmapAPIService.getWeedmapMenu(companyId, shopId, organizationItem.getWmItemId(), weedmapConfig, apiKeyMap);

        Iterable<Brand> brands = brandRepository.listWithOptions(companyId, "{name:1}");

        HashMap<String, Brand> brandNameMap = new HashMap<>();
        HashMap<String, Product> productBySkuMap = new HashMap<>();
        HashMap<String, Product> productByIdMap = new HashMap<>();
        brands.forEach(brand -> {
            if (StringUtils.isNotBlank(brand.getName())) {
                brandNameMap.put(brand.getName(), brand);
            }
        });

        Iterable<Product> products = productRepository.findItems(companyId, shopId, "", "{sku:1, name:1}", 0, Integer.MAX_VALUE).getValues();
        products.forEach(product -> {
            productBySkuMap.put(product.getSku(), product);
            productByIdMap.put(product.getId(), product);
        });

        for (Map.Entry<String, WmSyncMenuDetails> entry : details.entrySet()) {
            WmSyncMenuDetails wmSyncMenuDetails = entry.getValue();
            Brand brand = brandNameMap.get(entry.getKey());
            if (brand == null) {
                continue;
            }
            WmSyncItems wmSyncItems = weedmapSyncItemsMap.get(brand.getId());
            String wmBrandId = "";
            if (wmSyncItems == null) {
                WmSyncItems syncItem = createSyncItem(companyId, shopId, apiKeyMap.getListingWmId(), wmSyncMenuDetails.getId(), brand.getId(), null, WmSyncItems.WMItemType.BRANDS, null, false);
                weedmapSyncItemsMap.put(syncItem.getItemId(), syncItem);
                syncList.add(syncItem);
                wmBrandId = syncItem.getWmItemId();
            }

            for (Map.Entry<String, WmSyncMenuDetails.WmSyncProductDetails> detailEntry : wmSyncMenuDetails.getProductDetail().entrySet()) {
                WmSyncMenuDetails.WmSyncProductDetails wmSyncProductDetails = detailEntry.getValue();
                Product product = null;
                if (StringUtils.isNotBlank(wmSyncProductDetails.getBlazeId())) {
                    product = productByIdMap.get(wmSyncProductDetails.getBlazeId());
                }

                if (product == null) {
                    product = productBySkuMap.get(wmSyncProductDetails.getSku());
                }

                if (product == null) {
                    continue;
                }
                if (StringUtils.isBlank(wmBrandId)) {
                    continue;
                }

                WmSyncItems productSyncItem = weedmapSyncItemsMap.get(product.getId());
                if (productSyncItem == null) {
                    productSyncItem = createSyncItem(companyId, shopId, apiKeyMap.getListingWmId(), wmSyncProductDetails.getId(), product.getId(), wmSyncProductDetails.getWmsin(), WmSyncItems.WMItemType.PRODUCTS, wmSyncProductDetails.getLinkedItems(), false, Lists.newArrayList(wmSyncProductDetails.getWeightVariantDetailsMap().values()));
                    weedmapSyncItemsMap.put(product.getId(), productSyncItem);
                    syncList.add(productSyncItem);
                }
            }
        }

    }

    private WmSyncItems createSyncItem(String companyId, String shopId, String wmListingId, String wmItemId, String itemId, String wmsin, WmSyncItems.WMItemType itemType, HashMap<WmSyncItems.WMItemType, String> wmLinkedItems, boolean isVerified, List<WmSyncMenuDetails.WmProductVariantDetails> variantDetails) {
        WmSyncItems syncItems = new WmSyncItems();
        syncItems.prepare(companyId);
        syncItems.setShopId(shopId);
        syncItems.setWmItemId(wmItemId);
        syncItems.setWmsin(wmsin);
        syncItems.setItemId(itemId);
        syncItems.setItemType(itemType);
        syncItems.setWmListingId(wmListingId);
        syncItems.setWmLinkedItems(wmLinkedItems);
        syncItems.setVerified(isVerified);
        syncItems.setVariantDetails(variantDetails);
        return syncItems;
    }

    private boolean syncVerifiedProductLinkItems(String companyId, String shopId, String organizationId, String catalogId, WeedmapApiKeyMap apiKeyMap, Product product, ProductCategory category, ThirdPartyProduct thirdPartyProduct, HashMap<String, ProductWeightTolerance> toleranceMap, HashMap<WmSyncItems.WMItemType, String> wmLinkedItems, WeedmapAccountResult dbAccount, BigDecimal currentQuantity, boolean published, List<WmSyncMenuDetails.WmProductVariantDetails> variantDetailsList) {
        if (CollectionUtils.isNullOrEmpty(thirdPartyProduct.getVariantDetail())) {
            LOG.info(String.format("No variant found for third party product : %s for company : %s shop : %s", product.getName(), companyId, shopId));
            return false;
        }
        LinkedHashMap<String, WmSyncMenuDetails.WmProductVariantDetails> variantCatalogMap = new LinkedHashMap<>();

        if (wmLinkedItems.isEmpty() || !wmLinkedItems.containsKey(WmSyncItems.WMItemType.CATALOG_ITEMS) || variantDetailsList.isEmpty()) {
            WmSyncMenuDetails.WmSyncProductDetails wmVerifiedProducts = weedmapAPIService.getWmVerifiedProducts(companyId, shopId, organizationId, dbAccount.getWeedmapConfig(), apiKeyMap, thirdPartyProduct.getSourceProductId());
            wmLinkedItems.putIfAbsent(WmSyncItems.WMItemType.CATALOG_ITEMS, wmVerifiedProducts.getLinkedItems().get(WmSyncItems.WMItemType.CATALOG_ITEMS));
            variantDetailsList.addAll(wmVerifiedProducts.getWeightVariantDetailsMap().values());
        }

        //deleteVariantItems(companyId, shopId, wmLinkedItems, organizationId, apiKeyMap, dbAccount.getWeedmapConfig());
        HashMap<String, BigDecimal> priceRangeMap = new HashMap<>();
        getProductPricesMap(product, category, toleranceMap, priceRangeMap);

        variantDetailsList.forEach(wmProductVariantDetails -> {
            if (StringUtils.isBlank(wmProductVariantDetails.getCatalogId())) {
                return;
            }
            variantCatalogMap.putIfAbsent(wmProductVariantDetails.getVariantId(), wmProductVariantDetails);
        });

        HashMap<String, ThirdPartyProduct.ThirdPartyVariant> variantOptionMap = new HashMap<>();
        for (ThirdPartyProduct.ThirdPartyVariant variant : thirdPartyProduct.getVariantDetail()) {
            String sourceValue = StringUtils.isBlank(variant.getSourceValueId()) ? "" : variant.getSourceValueId();
            variantOptionMap.put(sourceValue, variant);
        }

        List<String> catelogIds = new ArrayList<>();
        List<String> catalogInventoryIds = new ArrayList<>();

        BigDecimal unitPrice = product.getUnitPrice();
        for (Map.Entry<String, ThirdPartyProduct.ThirdPartyVariant> entry : variantOptionMap.entrySet()) {
            String weightName = entry.getKey();
            weightName = (StringUtils.isBlank(weightName)) ? WmAttributeResponse.VariantAttributes.GRAM.weightName : weightName;
            ThirdPartyProduct.ThirdPartyVariant variant = entry.getValue();
            BigDecimal price = priceRangeMap.getOrDefault(weightName, BigDecimal.ZERO);

            if (price.doubleValue() == 0) {
                ProductWeightTolerance.WeightKey key = WmAttributeResponse.VariantAttributes.getCorrespondingKey(WmAttributeResponse.VariantAttributes.getKey(weightName));
                BigDecimal weightValue = (key != null) ? key.weightValue : BigDecimal.ONE;
                if (key == ProductWeightTolerance.WeightKey.OUNCE) {
                    weightValue = BigDecimal.valueOf(MeasurementConversionUtil.ounceQuantityCalculation(category, product, 1));
                }
                price = weightValue.multiply(unitPrice);
            }

            price = BigDecimal.valueOf(price.multiply(BigDecimal.valueOf(100), new MathContext(3)).longValue());

            WmSyncMenuDetails.WmProductVariantDetails variantDetail = variantCatalogMap.get(variant.getSourceVariantId());
            WmSyncRequest req = WmRequestGenerator.catalogItemRequest(catalogId, variant.getSourceVariantId(), published, true, price, variantDetail != null ? variantDetail.getCatalogId() : null);
            WmSyncResponse res = weedmapAPIService.syncItems(companyId, shopId, organizationId, apiKeyMap, req, WmSyncResponse.class, dbAccount.getWeedmapConfig());

            if (res == null) {
                continue;
            }

            req = WmRequestGenerator.createInventoryRequest(organizationId, currentQuantity);
            WmSyncResponse inventoryResponse = weedmapAPIService.syncInventory(companyId, shopId, organizationId, res.getId(), apiKeyMap, req, WmSyncResponse.class, dbAccount.getWeedmapConfig());

            if (inventoryResponse == null) {
                continue;
            }

            catelogIds.add(res.getId());
            catalogInventoryIds.add(inventoryResponse.getId());

            if (variantDetail == null) {
                variantDetail = new WmSyncMenuDetails.WmProductVariantDetails();
            }
            variantDetail.setVariantId(variant.getSourceVariantId());
            variantDetail.setCatalogId(res.getId());
            variantDetail.setWeightKey(weightName);
            variantCatalogMap.put(variant.getSourceVariantId(), variantDetail);
        }

        if (catelogIds.isEmpty()) {
            LOG.info(String.format("Catelog items not created: %s company : %s shop: %s", category.getName(), companyId, shopId));
            return false;
        }
        wmLinkedItems.put(WmSyncItems.WMItemType.CATALOG_ITEMS, StringUtils.join(catelogIds, ","));
        if (!catalogInventoryIds.isEmpty()) {
            wmLinkedItems.put(WmSyncItems.WMItemType.INVENTORY, StringUtils.join(catalogInventoryIds, ","));
        }

        variantDetailsList.clear();
        variantDetailsList.addAll(variantCatalogMap.values());
        return true;
    }

    private void getProductPricesMap(Product product, ProductCategory category, HashMap<String, ProductWeightTolerance> toleranceMap, HashMap<String, BigDecimal> priceRangeMap) {
        if (category.getUnitType() == ProductCategory.UnitType.grams && !CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
            product.getPriceRanges().forEach(priceRange -> {
                ProductWeightTolerance tolerance = toleranceMap.get(priceRange.getWeightToleranceId());
                if (tolerance == null) {
                    return;
                }
                WmAttributeResponse.VariantAttributes correspondingKey = WmAttributeResponse.VariantAttributes.getCorrespondingKey(tolerance.getWeightKey());
                if (correspondingKey == null) {
                    return;
                }
                priceRangeMap.put(correspondingKey.weightName, priceRange.getPrice());
            });
        } else {
            priceRangeMap.put(WmAttributeResponse.VariantAttributes.GRAM.weightName, product.getUnitPrice());
        }
    }



    private boolean canAccessV2Weedmap(String companyId) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app != null) {
            if (app.isEnableV2WeedmapSync()) {
                return true;
            }

            // check beta
            if (app.getWeedmapsBetaTesters() != null) {
                return app.getWeedmapsBetaTesters().getOrDefault(companyId, false);
            }
        }
        return false;
    }

    /**
     * Public method to sync leafly items
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @return LeaflySyncResponse object.
     */
    public LeaflySyncResponse syncLeafly(String companyId, String shopId, LeaflySyncJob syncJob) {
        LeaflySyncResponse response = new LeaflySyncResponse();
        LeaflyAccount account = leaflyAccountRepository.getAccountByShop(companyId, shopId);
        if (account == null) {
            response.setErrorMsg(LeaflyConstants.ACCOUNT_DOES_NOT_EXIST);
            response.setSyncStatus(Boolean.FALSE);
            LOG.info(String.format(LeaflyConstants.ACCOUNT_DOES_NOT_EXIST + " for companyId: %s, shopId: %s", companyId, shopId));
            return response;
        }
        long currentTime = DateTime.now().getMillis();
        //Update leafly account with In Progress details.
        account.setMenuSyncRequestDate(currentTime);
        account.setSyncStatus(LeaflyAccount.LeaflySyncStatus.InProgress);
        leaflyAccountRepository.update(companyId, account.getId(), account);

        Set<ObjectId> productIdsSet = new HashSet<>();
        for (String productId : syncJob.getProductIds()) {
            productIdsSet.add(new ObjectId(productId));
        }
        Iterable<Product> products;
        //get products based on account key status.
        if (account.getKeyUpdate()) {
            products = productRepository.list(companyId, Lists.newArrayList(productIdsSet));
        } else {
            products = productRepository.listAfterLastLeaflySync(companyId, Lists.newArrayList(productIdsSet), account.getLastSync());
        }

        Set<ObjectId> failProducts = new HashSet<>();
        Set<ObjectId> successProducts = new HashSet<>();
        if (StringUtils.isNotEmpty(account.getApiKey())) {
            //Fail and Success product Ids set.
            for (Product product : products) {
                response = syncLeaflyItem(account, product);
                if (response.isSyncStatus()) {
                    successProducts.add(new ObjectId(product.getId()));
                } else {
                    failProducts.add(new ObjectId(product.getId()));
                }
            }
            //updating success and failed products time and status
            if (!successProducts.isEmpty()) {
                productRepository.updateLeaflySyncTimeAndStatus(companyId, successProducts, currentTime, Boolean.TRUE);
            }
            if (!failProducts.isEmpty()) {
                productRepository.updateLeaflySyncTimeAndStatus(companyId, failProducts, currentTime, Boolean.FALSE);
            }
            response.setSyncStatus(Boolean.TRUE);
            account.setLastSync(DateTime.now().getMillis());
            account.setSyncStatus(LeaflyAccount.LeaflySyncStatus.Completed);
            account.setKeyUpdate(Boolean.FALSE);
            leaflyAccountRepository.update(companyId, account.getId(), account);
        } else {
            response.setSyncStatus(false);
            response.setErrorMsg(LeaflyConstants.EMPTY_API_KEY);
        }
        return response;
    }

    /**
     * This method is used to sync single leafly item.
     *
     * @param account Leafly account
     * @param product Single product
     * @return LeaflySyncResponse object.
     */
    private LeaflySyncResponse syncLeaflyItem(LeaflyAccount account, Product product) {
        LeaflyMenuItem request = new LeaflyMenuItem();
        List<Compound> compounds = new ArrayList<>();
        List<Variant> variants = new ArrayList<>();
        Compound compound = new Compound();
        Variant variant = new Variant();
        compound.setType(Compound.CompoundTypes.CBD);
        compound.setContent(product.getCbd());
        compound.setUnit(Compound.CompoundUnits.PERCENT);
        compounds.add(compound);
        compound = new Compound();
        compound.setType(Compound.CompoundTypes.THC);
        compound.setContent(product.getThc());
        compounds.add(compound);
        compound.setUnit(Compound.CompoundUnits.PERCENT);
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForProduct(account.getCompanyId(), account.getShopId(), product.getId());
        variant.setPrice(product.getUnitPrice() == null ? LeaflyConstants.DOUBLEZERO : product.getUnitPrice().doubleValue());
        variant.setPriceIncludesTax(product.isPriceIncludesExcise());
        variant.setTaxRate(LeaflyConstants.DOUBLEZERO);
        variant.setAmount(product.getSalesPrice() == null ? (product.getUnitPrice() == null ? LeaflyConstants.DOUBLEZERO : product.getUnitPrice().doubleValue()) : product.getSalesPrice().doubleValue());
        for (LeaflyVariantUnitMap mapItem : account.getVariantUnitMapping()) {
            if (StringUtils.isNotBlank(mapItem.getVariantUnit()) && mapItem.getVariantUnit().equalsIgnoreCase(product.getWeightPerUnit().toString())) {
                variant.setUnit(mapItem.getLeaflyVariantUnit().toString());
                break;
            } else {
                variant.setUnit(LeaflyVariantUnit.OZ.toString());
            }
        }
        variant.setInventoryLevel(getProductQuantity(product, prepackageQuantities));
        variant.setBatchId(StringUtils.EMPTY);
        variant.setParentBatchId(StringUtils.EMPTY);
        variant.setSku(product.getSku());
        variants.add(variant);
        for (LeaflyCategoryMapItem mapItem : account.getCategoryMapping()) {
            if (StringUtils.isNotBlank(mapItem.getFlowerType()) && mapItem.getFlowerType().equalsIgnoreCase(product.getFlowerType())) {
                request.setType(mapItem.getLeaflyCategory().toString());
                break;
            } else {
                request.setType(LeaflyCategory.Other.toString());
            }
        }
        request.setId(product.getId());
        request.setName(product.getName());
        request.setBrand(product.getBrandId());
        request.setCompounds(compounds);
        request.setVariants(variants);
        request.setDescription(product.getDescription());
        request.setAvailableForPickup(Boolean.FALSE);

        LeaflyMenuItemRequest leaflyMenuItemRequest = new LeaflyMenuItemRequest().setItems(Lists.newArrayList(request));
        LeaflyResult leaflyResult = new LeaflyResult();
        //API call for Leafly Item Update
        try {
            leaflyResult = leaflyAPIService.upsertMenuItem(IntegrationSetting.Environment.Development, account.getClientKey(), account.getApiKey(), leaflyMenuItemRequest);
            if (leaflyResult.getMessage().trim().equalsIgnoreCase(LeaflyConstants.OK)) {
                leaflyResult.setErrorMsg(LeaflyConstants.SUCCESS);
                leaflyResult.setSyncStatus(true);
            } else {
                leaflyResult.setErrorMsg(LeaflyConstants.FAIL);
                leaflyResult.setSyncStatus(false);
            }
        } catch (InterruptedException | ExecutionException | IOException e) {
            LOG.info(e.getMessage(), e);
            leaflyResult.setErrorMsg(e.getMessage());
            leaflyResult.setSyncStatus(false);
        }
        return leaflyResult;
    }

    /**
     * Public method to reset leafly items
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     */
    public LeaflySyncResponse resetLeafly(String companyId, String shopId, String accountId) {
        LeaflyAccount account = leaflyAccountRepository.getAccountByShop(companyId, shopId);
        List<Product> productList = productRepository.getActiveProductList(companyId, shopId);
        LeaflySyncResponse leaflyResult = new LeaflyResult();
        List<String> productIds = new ArrayList<>();
        for (Product product : productList) {
            productIds.add(product.getId());
        }
        Boolean resetResponse = Boolean.FALSE;
        try {
            resetResponse = leaflyAPIService.deleteMenuItems(IntegrationSetting.Environment.Development, account.getClientKey(), account.getApiKey(), productIds.stream().toArray(String[]::new));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (resetResponse) {
            leaflyResult.setSyncStatus(Boolean.TRUE);
            leaflyResult.setErrorMsg(LeaflyConstants.SUCCESS);
        } else {
            leaflyResult.setSyncStatus(Boolean.FALSE);
            leaflyResult.setErrorMsg(LeaflyConstants.FAIL);
        }
        return leaflyResult;
    }
}
