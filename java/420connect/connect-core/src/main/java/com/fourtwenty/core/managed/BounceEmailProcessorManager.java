package com.fourtwenty.core.managed;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.config.BounceEmailProcessorConfig;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.BouncedEmail;
import com.fourtwenty.core.domain.repositories.dispensary.BouncedEmailRepository;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.JsonSerializer;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BounceEmailProcessorManager implements Managed {

    private static final Logger LOGGER = LoggerFactory.getLogger(BounceEmailProcessorManager.class);

    @Inject
    private BouncedEmailRepository bouncedEmailRepository;

    @Inject
    private ConnectConfiguration config;

    private ScheduledExecutorService ses;

    @Inject
    private AmazonServiceFactory amazonServiceFactory;

    @Override
    public void start() throws Exception {
        ses = Executors.newScheduledThreadPool(1);
        final BounceEmailProcessorConfig bounceEmailProcessorConfig = config.getBounceEmailProcessorConfig();
        if (bounceEmailProcessorConfig != null && bounceEmailProcessorConfig.isEnabled()) {
            processBounceEmails(bounceEmailProcessorConfig);
        } else {
            LOGGER.info("Bounce email processing is disabled");
        }
    }

    private void processBounceEmails(final BounceEmailProcessorConfig bounceEmailProcessorConfig) {
        //Must be standard queue
        final String queueName = config.getEnv() + "-" + bounceEmailProcessorConfig.getQueueName();

        // Run it every minute
        ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    final AmazonSQS sqsClient = amazonServiceFactory.getSQSClient();

                    // Queue should be already created and configured manually
                    GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(queueName);
                    final GetQueueUrlResult queueUrlResult = sqsClient.getQueueUrl(getQueueUrlRequest);
                    if (queueUrlResult != null) {
                        String queueUrl = queueUrlResult.getQueueUrl();

                        final ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
                        final List<Message> messages = sqsClient.receiveMessage(receiveMessageRequest).getMessages();
                        final List<BouncedEmail> bouncedEmails = new ArrayList<>();

                        for (Message message : messages) {
                            final BounceEmailMessageBody emailMessageBody = JsonSerializer.fromJson(message.getBody(), BounceEmailMessageBody.class);

                            if (emailMessageBody != null
                                    && "Bounce".equals(emailMessageBody.notificationType)
                                    && emailMessageBody.getBounceInformation() != null
                                    && emailMessageBody.getBounceInformation().getBouncedRecipients() != null
                                    ) {

                                for (BouncedRecipients bouncedRecipients : emailMessageBody.getBounceInformation().getBouncedRecipients()) {
                                    BouncedEmail bouncedEmail = new BouncedEmail();
                                    bouncedEmail.prepare();
                                    bouncedEmail.setEmail(bouncedRecipients.getEmailAddress());
                                    bouncedEmails.add(bouncedEmail);
                                }
                            }
                            sqsClient.deleteMessage(queueUrl, message.getReceiptHandle());
                        }

                        if (bouncedEmails.size() > 0) {
                            bouncedEmailRepository.save(bouncedEmails);
                        }

                    }

                } catch (Exception e) {
                    LOGGER.error("Error in Bounce email SQS", e);
                }
            }
        }, 0, 5, TimeUnit.MINUTES);
    }


    @Override
    public void stop() throws Exception {
        if (ses != null)
            ses.shutdown();
    }

    // Model Classes
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BounceEmailMessageBody {
        private String notificationType;
        @JsonProperty("bounce")
        private BounceInformation bounceInformation;

        public String getNotificationType() {
            return notificationType;
        }

        public void setNotificationType(String notificationType) {
            this.notificationType = notificationType;
        }

        public BounceInformation getBounceInformation() {
            return bounceInformation;
        }

        public void setBounceInformation(BounceInformation bounceInformation) {
            this.bounceInformation = bounceInformation;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BounceInformation {
        private String bounceType;
        private String bounceSubType;
        private List<BouncedRecipients> bouncedRecipients;

        public String getBounceType() {
            return bounceType;
        }

        public void setBounceType(String bounceType) {
            this.bounceType = bounceType;
        }

        public String getBounceSubType() {
            return bounceSubType;
        }

        public void setBounceSubType(String bounceSubType) {
            this.bounceSubType = bounceSubType;
        }

        public List<BouncedRecipients> getBouncedRecipients() {
            return bouncedRecipients;
        }

        public void setBouncedRecipients(List<BouncedRecipients> bouncedRecipients) {
            this.bouncedRecipients = bouncedRecipients;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BouncedRecipients {
        private String emailAddress;

        public String getEmailAddress() {
            return emailAddress;
        }

        public void setEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }
    }
}
