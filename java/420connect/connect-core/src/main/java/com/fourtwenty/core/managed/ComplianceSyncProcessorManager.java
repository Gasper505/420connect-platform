package com.fourtwenty.core.managed;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.*;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.compliance.ComplianceSyncJob;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.sync.ComplianceSyncJobRepository;
import com.fourtwenty.core.jobs.QueuedComplianceSyncJob;
import com.fourtwenty.core.services.thirdparty.models.ComplianceSQSMessageRequest;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Singleton
public class ComplianceSyncProcessorManager implements Managed {
    public static final String QUEUE_NAME = "COMPLIANCE_SYNC_QUEUE";
    static final Log LOG = LogFactory.getLog(ComplianceSyncProcessorManager.class);
    @Inject
    ComplianceSyncJobRepository complianceSyncJobRepository;
    @Inject
    ConnectConfiguration config;
    private ScheduledExecutorService ses;
    @Inject
    Injector injector;
    @Inject
    private AmazonServiceFactory amazonServiceFactory;
    @Inject
    private AppRepository appRepository;

    @Override
    public void start() throws Exception {
        ses = Executors.newScheduledThreadPool(1);
        final TransProcessorConfig processorConfig = config.getProcessorConfig();
        if (processorConfig != null
                && processorConfig.isEnabled()) {
            if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.Local) {
                runLocalTransProcessor();
            } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
                runSQSTransProcessor(processorConfig);
            }
        } else {
            LOG.info("Transaction Processing is not enabled to process sales.");
        }
    }

    @Override
    public void stop() throws Exception {
        if (ses != null) {
            ses.shutdown();
        }
    }

    private void runLocalTransProcessor() {
        LOG.info("LOCAL Transaction Processing is enabled.");
        ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // do some work
                Iterable<ComplianceSyncJob> syncJobs = complianceSyncJobRepository.getQueuedJobs();



            }
        }, 0, 1, TimeUnit.SECONDS);  // ex	ecute every x seconds
    }

    private void runSQSTransProcessor(final TransProcessorConfig processorConfig) {
        LOG.info("SQSQueue Transaction Processing is enabled.");
        String env = config.getEnv().name();
        if (config.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();

                computername = computername.replace(".", "");
                env = config.getEnv().name() + "-" + computername;

            } catch (UnknownHostException e) {
            }
        }

        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app != null) {
            int numOfQueues = 1;
            // need to have at least 1 queue

            for (int i = 0; i < numOfQueues; i++) {


                String queueNumStr = "";
                if (i > 0) {
                    queueNumStr = "_" + i;
                }

                //final String queueName =  env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
                final String multiQueueName = env + "-" + QUEUE_NAME + queueNumStr + ".fifo";
                LOG.info("SQSName: " + multiQueueName);

                ComplianceSyncProcessorManager.QueueRunner queueRunner = new ComplianceSyncProcessorManager.QueueRunner(multiQueueName);
                ses.scheduleAtFixedRate(queueRunner,0,2,TimeUnit.SECONDS);
            }

        }

    }

    protected class QueueRunner implements Runnable {
        final String queueName;
        public QueueRunner(String myQueueName) {
            queueName = myQueueName;
        }

        @Override
        public void run() {
            try {
                LOG.info(String.format("Queue: %s",queueName));
                AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
                // do some work
                Map<String, String> attributes = new HashMap<String, String>();
                // A FIFO queue must have the FifoQueue attribute set to True
                attributes.put("FifoQueue", "true");
                // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
                attributes.put("ContentBasedDeduplication", "true");
                // The FIFO queue name must end with the .fifo suffix

                GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
                String myQueueUrl = null;
                try {
                    GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
                    if (queueUrlResult != null) {
                        myQueueUrl = queueUrlResult.getQueueUrl();
                    }
                } catch (Exception e) {
                    LOG.info("Queue does not exist.");
                }
                if (myQueueUrl == null) {
                    LOG.info("Queue does not exist. Creating new Queue: " + queueName);
                    CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
                    myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
                }


                // Receive messages
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
                // Uncomment the following to provide the ReceiveRequestDeduplicationId
                //receiveMessageRequest.setReceiveRequestAttemptId("1");
                List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();

                //LOG.info("Receiving messages from " + queueName + ", size: " + messages.size());
                for (Message message : messages) {
                    LOG.info("  Message");
                    LOG.info("    MessageId:     " + message.getMessageId());
                    LOG.info("    ReceiptHandle: " + message.getReceiptHandle());
                    LOG.info("    MD5OfBody:     " + message.getMD5OfBody());
                    LOG.info("    Body:          " + message.getBody());

                    ComplianceSQSMessageRequest request = JsonSerializer.fromJson(message.getBody(), ComplianceSQSMessageRequest.class);

                    QueuedComplianceSyncJob complianceSyncJob = injector.getInstance(QueuedComplianceSyncJob.class);
                    complianceSyncJob.setCompanyId(request.getCompanyId());
                    complianceSyncJob.setShopId(request.getShopId());
                    complianceSyncJob.setComplianceSyncJobId(request.getQueuedComplianceJobId());

                    try {
                        LOG.info("Executing QueuedTransaction: " + request.getQueuedComplianceJobId());
                        complianceSyncJob.run();
                        LOG.info(String.format("Deleting Message from SQS Queue: %s - %s",queueName,request.getQueuedComplianceJobId()));
                        amazonSQS.deleteMessage(myQueueUrl, message.getReceiptHandle());
                    } catch (Exception e) {
                        LOG.error("Error processing queued transaction job", e);
                    }
                }
            } catch (Exception e) {
                LOG.error("Error executing SQS", e);
            }
        }
    }
}
