package com.fourtwenty.core.managed;


import com.fourtwenty.core.domain.models.company.BouncedNumber;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.repositories.dispensary.BouncedNumberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ConversationRepository;
import com.fourtwenty.core.rest.features.TwilioMessage;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class ConversationJobManager implements Managed {
    private static final Logger logger = LoggerFactory.getLogger(ConversationJobManager.class);

    ExecutorService executorService;

    private TwilioMessageService twilioMessageService;
    private ConversationRepository conversationRepository;
    private BouncedNumberRepository bouncedNumberRepository;

    @Inject
    public ConversationJobManager(TwilioMessageService twilioMessageService, ConversationRepository conversationRepository, BouncedNumberRepository bouncedNumberRepository) {
        this.twilioMessageService = twilioMessageService;
        this.conversationRepository = conversationRepository;
        this.bouncedNumberRepository = bouncedNumberRepository;
    }

    @Override
    public void start() throws Exception {
        logger.debug("Starting ConversationJobManager");
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        logger.debug("Stopping ConversationJobManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void sendMessageExecutor(final Conversation conversation, final TwilioMessage twilioMessage) {

        BouncedNumber bouncedNumber = bouncedNumberRepository.getBouncedNumber(twilioMessage.getTo());
        if (bouncedNumber != null && bouncedNumber.isDeleted() == false) {
            logger.error("This number is previously bounced : " + twilioMessage.getTo());
            conversation.setStatus(Conversation.Status.FAILED);
            return;
        }

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final String sid = twilioMessageService.sendMessage(twilioMessage);
                    conversation.setSid(sid);
                    conversation.setStatus(Conversation.Status.PENDING);
                    conversationRepository.update(conversation.getId(), conversation);
                } catch (Exception ex) {
                    conversation.setStatus(Conversation.Status.FAILED);
                    conversationRepository.update(conversation.getId(), conversation);
                    logger.debug("Error while sending message via twilio", ex);
                    logger.warn("Unable to send message [Response from Twilio : {}]", ex.getMessage());
                    recordBouncedNumber(twilioMessage.getTo(), ex.getMessage());
                }
            }
        });
    }

    /**
     * Save bounce number
     *
     * @param number : bounced number
     */
    private void recordBouncedNumber(String number, String reason) {
        BouncedNumber bouncedNumber = new BouncedNumber();
        bouncedNumber.prepare();
        bouncedNumber.setNumber(number);
        bouncedNumber.setReason(reason);

        bouncedNumberRepository.save(bouncedNumber);
    }


}
