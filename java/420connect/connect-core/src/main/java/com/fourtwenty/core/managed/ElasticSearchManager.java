package com.fourtwenty.core.managed;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductCustomResult;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchIndexingService;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Singleton
public class ElasticSearchManager implements Managed {
    private static final int THREAD_COUNT = 40;
    private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchManager.class);

    private ExecutorService executorService;
    private Injector injector;
    private ElasticSearchIndexingService elasticSearchIndexingService;
    private AppRepository appRepository;
    private List<CompletableFuture<Object>> futures = new ArrayList<>();

    @Inject
    public ElasticSearchManager(Injector injector, ElasticSearchIndexingService elasticSearchIndexingService, AppRepository appRepository) {
        this.injector = injector;
        this.elasticSearchIndexingService = elasticSearchIndexingService;
        this.appRepository = appRepository;
    }

    @Override
    public void start() throws Exception {
        LOG.debug("Starting ElasticSearchManager");
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping ElasticSearchManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void waitUntilDone() {
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).join();
    }


    private boolean isElasticSearchEnabledForObj(Object obj) {
        if (obj == null) return false;

        Class clazz = obj.getClass();
        return isElasticSearchEnabledForObj(clazz);
    }

    private boolean isElasticSearchEnabledForObj(Class clazz) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

        boolean enableElasticSearch =
                (clazz == Member.class && app.isEnableMemberElasticSearch()) ||
                        (clazz == Transaction.class && app.isEnableTransactionElasticSearch()) ||
                        (clazz == ProductCustomResult.class && app.isEnableProductElasticSearch()) ||
                        (clazz == ProductBatch.class && app.isEnableProductBatchElasticSearch());

        return enableElasticSearch;
    }


    public <E extends CompanyBaseModel & ElasticSearchCapable> void createOrUpdateIndexedDocument(E obj) {

        if (isElasticSearchEnabledForObj(obj)) {
            JSONObject jsonObject = obj.toElasticSearchObject();
            if (jsonObject != null) {
                futures.add(CompletableFuture.supplyAsync(() -> {
                    return elasticSearchIndexingService.createOrUpdateIndexedDocument(obj, jsonObject);
                }, executorService));
            }
        }
    }

    public <E extends CompanyBaseModel & ElasticSearchCapable> void createOrUpdateIndexedDocuments(List<E> objs, int batchSize) {
        if (objs == null || objs.isEmpty() || objs.get(0) == null || !isElasticSearchEnabledForObj(objs.get(0))) return;
        Class clazz = objs.get(0).getClass();

        long batches = (long) Math.ceil((double) objs.size() / (double) batchSize);
        LOG.info("Batches size: " + batches);
        for (long i = 0; i < batches; i++) {
            List<ElasticSearchCapable> batchObjs = objs
                    .stream()
                    .skip(i * batchSize)
                    .limit(batchSize)
                    .collect(Collectors.toList());

            LOG.info("Batch size: " + batchObjs.size());
            futures.add(CompletableFuture.supplyAsync(() -> {
                return elasticSearchIndexingService.createOrUpdateIndexedDocuments(batchObjs);
            }, executorService));
        }
    }

    public <E extends CompanyBaseModel> void deleteIndexedDocument(String id, Class<E> clazz) {

        try {
            if (isElasticSearchEnabledForObj(clazz)) {
                futures.add(CompletableFuture.supplyAsync(() -> {
                    return elasticSearchIndexingService.deleteIndexedDocument(id, clazz);
                }, executorService));

            }
        } catch (Exception e) {
            LOG.error("Erring deleting object",e);
        }
    }

    public <E extends CompanyBaseModel> void deleteIndexedDocumentsFor(String companyId, Class<E> clazz) {
        try {
            if (isElasticSearchEnabledForObj(clazz)) {
                futures.add(CompletableFuture.supplyAsync(() -> {
                    return elasticSearchIndexingService.deleteIndexedDocumentsFor(companyId, clazz);
                }, executorService));

            }
        } catch (Exception e) {
            LOG.error("Erring deleting object",e);
        }
    }

    public <E extends CompanyBaseModel> void deleteIndexedDocumentsFor(List<String> ids, Class<E> clazz) {
        if (isElasticSearchEnabledForObj(clazz)) {
            futures.add(CompletableFuture.supplyAsync(() -> {
                return elasticSearchIndexingService.deleteIndexedDocumentsFor(ids, clazz);
            }, executorService));

        }
    }


}
