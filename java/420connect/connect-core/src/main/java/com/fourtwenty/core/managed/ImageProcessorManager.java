package com.fourtwenty.core.managed;

import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mdo on 10/25/17.
 */
@Singleton
public class ImageProcessorManager implements Managed {
    static final Log LOG = LogFactory.getLog(ImageProcessorManager.class);

    ExecutorService executorService;

    @Override
    public void start() throws Exception {
        LOG.debug("Starting BackgroundTaskManager");
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(4);
        }
    }

    @Override
    public void stop() throws Exception {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void processPublicImage(File file) {

    }
}
