package com.fourtwenty.core.managed;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.models.thirdparty.weedmap.WeedmapSyncJob;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.WmSyncJobRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.inventory.InventoryReconciliationEvent;
import com.fourtwenty.core.event.inventory.InventoryTransferEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchBundleItems;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.inventory.InventoryOperationService;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.*;

/**
 * Purpose : To handle inventory management at a single place,
 * for now I don't see any use for making it managed. let's have it in todo
 * TODO: Think on where this manager fits later on.
 */
@Singleton
public class InventoryActionManager implements Managed {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryActionManager.class);

    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private InventoryTransferHistoryRepository transferHistoryRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private BatchQuantityService batchQuantityService;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private BarcodeItemRepository barcodeItemRepository;
    @Inject
    private CompanyUniqueSequenceRepository sequenceRepository;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    InventoryOperationService inventoryOperationService;
    @Inject
    PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    ReconciliationHistoryRepository reconciliationHistoryRepository;
    @Inject
    private InventoryTransferHistoryRepository inventoryTransferHistoryRepository;
    @Inject
    private ProductPrepackageQuantityRepository quantityRepository;
    @Inject
    private DerivedProductBatchLogRepository derivedLogRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private ShipmentBillRepository shipmentBillRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private WmSyncJobRepository wmSyncJobRepository;
    @Inject
    private BackgroundJobService backgroundJobService;

    @Override
    public void start() throws Exception {
        LOGGER.info("Inventory action manager started");
    }

    @Override
    public void stop() throws Exception {
        LOGGER.info("Inventory action manager stopped");
    }

    /**
     * Entry point for inventory action manager and responsible for invoking all inventory actions
     *
     * @param queuedTransaction queued transaction that we received from FIFO
     * @param transactionable   main db object which holds the data
     */
    public synchronized void process(QueuedTransaction queuedTransaction, Transactionable transactionable) {
        switch (queuedTransaction.getQueuedTransType()) {
            case SalesTrans:
            case ShippingManifest:
                processTransactionNew(queuedTransaction, (Transaction) transactionable); //fixed
                break;
            case RefundRequest:
            case RevertShippingManifest:
            case RejectShippingManifest:
                processRefundTransaction(queuedTransaction, (Transaction) transactionable); //fixed
                break;
            case PurchaseOrder:
                processPurchaseOrder((PurchaseOrder) transactionable, queuedTransaction);
                break;
            case InventoryTransfer:
                processTransferNew((InventoryTransferHistory) transactionable, queuedTransaction); // Fixed
                break;
            case InventoryReconciliation:
                processReconciliationNew((ReconciliationHistory) transactionable); // Fixed
                break;
            case ProductBatch:
                processBatches((ProductBatch) transactionable, queuedTransaction); //fixed
                break;
            case Prepackage:
                if (transactionable != null) {
                    processPrepackage(queuedTransaction, (PrepackageProductItem) transactionable);
                } else {
                    processPrepackage(queuedTransaction, null);
                }
                break;
            case StockReset:
                processStockReset(queuedTransaction);
                break;
            case Product:
                processProduct(queuedTransaction);
                break;
            case BulkProductBatch:
                processBulkProductBatch(queuedTransaction);
                break;
            default:
                throw new BlazeInvalidArgException("Transaction type", "This type of queue processing is not yet defined");
        }
    }


    private void processTransactionNew(QueuedTransaction queuedTransaction, Transaction dbTransaction) {
        final String companyId = queuedTransaction.getCompanyId();
        final String shopId = queuedTransaction.getShopId();

        final Terminal terminal = terminalRepository.get(queuedTransaction.getCompanyId(), queuedTransaction.getTerminalId());
        final Inventory safeInventory = inventoryRepository.getInventory(queuedTransaction.getCompanyId(), queuedTransaction.getShopId(), Inventory.SAFE);

        InventoryOperation.SourceType sourceType = InventoryOperation.SourceType.Transaction;
        if (queuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.ShippingManifest) {
            sourceType = InventoryOperation.SourceType.ShippingManifest;
        } else if (queuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.RefundRequest) {
            sourceType = InventoryOperation.SourceType.RefundTransaction;
        }


        //1.  Revert old quantities
        InventoryOperationService.InventoryOperationResult operations = inventoryOperationService.revertRecent(companyId, shopId, sourceType, dbTransaction.getId());


        // Now sync accordingly
        ////If queued transaction pending status is cancel then process only for put back

        Set<String> affectedProducts = new HashSet<>();
        for (InventoryOperation operation : operations.operations) {
            affectedProducts.add(operation.getProductId());
        }


        if (Transaction.TransactionStatus.Canceled == queuedTransaction.getPendingStatus()) {
            //3. Sync all products
            syncInventoryOperations(shopId, operations, String.format("Cancel Trans #%s", dbTransaction.getTransNo()), true);
            return;
        } else {
            syncInventoryOperations(shopId, operations, String.format("PutBack Trans #%s", dbTransaction.getTransNo()), false,false);
        }

        InventoryOperationService.InventoryOperationResult takeOutOps = new InventoryOperationService.InventoryOperationResult();
        //2. Deduct new one
        // Take items out of inventory
        Cart newCart = queuedTransaction.getCart();
        String requestId = this.createSourceRequestId();

        List<String> productIds = new ArrayList<>();
        for (OrderItem newLineItme : newCart.getItems()) {
            productIds.add(newLineItme.getProductId());
            affectedProducts.add(newLineItme.getProductId());
        }

        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(companyId, shopId, productIds);

        if (queuedTransaction.getPendingStatus() != Transaction.TransactionStatus.Completed
            && queuedTransaction.getPendingStatus() != Transaction.TransactionStatus.RefundWithInventory) {
            // if we're not completing or we're not refunding, check to see if it's unassigned. If it's unassigned, return
            if (dbTransaction.isAssigned() == false && Transaction.TransactionType.ShippingManifest != dbTransaction.getTransType() && !dbTransaction.getTransType().equals(Transaction.TransactionType.Adjustment) && dbTransaction.getTransType() != Transaction.TransactionType.ReturnToVendor) {
                // just return since it is not marked as unassigned
                // sync weedmaps manually
                syncWeedMap(affectedProducts,shopId, queuedTransaction.getSellerId());
                return;
            }
        }

        for (OrderItem newLineItme : newCart.getItems()) {
            // take items out of inventory
            InventoryOperationService.InventoryOperationResult adjustResult2 = takeItemsOutOfInventory(
                    companyId,
                    shopId,
                    queuedTransaction,
                    newLineItme,
                    dbTransaction,
                    terminal,
                    safeInventory, sourceType,
                    requestId,
                    prepackageProductItemHashMap);

            takeOutOps.operations.addAll(adjustResult2.operations);
        }

        //3. Sync all products
        if (queuedTransaction.getPendingStatus() == Transaction.TransactionStatus.Completed) {
            syncInventoryOperations(shopId, takeOutOps, String.format("Complete Trans #%s", dbTransaction.getTransNo()), true,false);
        } else {
            syncInventoryOperations(shopId, takeOutOps, String.format("Hold Trans #%s", dbTransaction.getTransNo()), true,false);
        }

        // sync weedmaps manually
        syncWeedMap(affectedProducts,shopId, queuedTransaction.getSellerId());
    }

    private InventoryOperationService.InventoryOperationResult takeItemsOutOfInventory(String companyId,
                                                                                       String shopId,
                                                                                       QueuedTransaction newQueueTransaction,
                                                                                       OrderItem orderItem,
                                                                                       Transaction dbTransaction,
                                                                                       Terminal terminal,
                                                                                       Inventory safeInventory,
                                                                                       InventoryOperation.SourceType sourceType, String requestId,
                                                                                       HashMap<String, PrepackageProductItem> prepackageProductItemHashMap
    ) {
        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();
        if (newQueueTransaction.getPendingStatus() == Transaction.TransactionStatus.Canceled) {
            return result;
        }

        if (dbTransaction.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment) {
            if (!orderItem.isFulfilled()) {
                return result;
            }
        }

        // clear previous quantity logs as we are now taking new sets
        orderItem.getQuantityLogs().clear();

        // use safe inventory as default
        // use orderItem first
        String assignedInventory = StringUtils.isNotBlank(orderItem.getInventoryId()) ? orderItem.getInventoryId() : null;
        if (assignedInventory == null) {
            assignedInventory = ((terminal != null) ? terminal.getAssignedInventoryId() : safeInventory.getId());
        }
        if (StringUtils.isNotBlank(dbTransaction.getOverrideInventoryId())) {
            assignedInventory = dbTransaction.getOverrideInventoryId();
        }

        // Calculate quantity
        if (StringUtils.isNotEmpty(orderItem.getPrepackageItemId())
                && ObjectId.isValid(orderItem.getPrepackageItemId())) {

            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
            String batchId = orderItem.getBatchId();
            if (prepackageProductItem != null) {
                batchId = prepackageProductItem.getBatchId();
            }
            // Find the quantity we want to deduct this from
            orderItem.addQuantityLog(assignedInventory, orderItem.getPrepackageItemId(), batchId, orderItem.getQuantity().intValue());


        } else {
            // try to deduct accordingly
            List<QuantityLog> quantityLogs = batchQuantityService.fakeSubtractBatchQuantity(companyId,
                    shopId,
                    orderItem.getProductId(),
                    assignedInventory,
                    orderItem.getBatchId(),
                    orderItem.getQuantity(),
                    orderItem.getOrderItemId());

            orderItem.addQuantityLogs(quantityLogs);

        }

        // now try to adjust accordingly here
        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
            BigDecimal takeQty = quantityLog.getQuantity().multiply(new BigDecimal(-1));

            String batchId = quantityLog.getBatchId();
            String prepackageItemId = quantityLog.getPrepackageItemId();

            InventoryOperationService.InventoryOperationResult result1 = inventoryOperationService.adjust(companyId,
                    shopId, orderItem.getProductId(), prepackageItemId,
                    batchId,
                    assignedInventory,
                    takeQty,
                    newQueueTransaction.getSellerId(),
                    sourceType,
                    dbTransaction.getId(),
                    orderItem.getId(), requestId,
                    InventoryOperation.SubSourceAction.Sale);

            result.operations.addAll(result1.operations);
        }

        return result;
    }


    /**
     * Process inventory updates for refund transaction
     *
     * @param queuedTransaction queued transaction that we received from FIFO
     * @param transaction       Transaction object which holds the data
     */
    private void processRefundTransaction(QueuedTransaction queuedTransaction, Transaction transaction) {

        final Terminal terminal = terminalRepository.get(queuedTransaction.getCompanyId(), queuedTransaction.getTerminalId());
        final Inventory safeInventory = inventoryRepository.getInventory(queuedTransaction.getCompanyId(), queuedTransaction.getShopId(), Inventory.SAFE);
        final String companyId = queuedTransaction.getCompanyId();
        final String shopId = queuedTransaction.getShopId();

        // use safe inventory as default
        String assignedInventory = terminal != null ? terminal.getAssignedInventoryId() : safeInventory.getId();
        if (StringUtils.isNotBlank(transaction.getOverrideInventoryId())) {
            assignedInventory = transaction.getOverrideInventoryId();
        }

        String targetInventoryId = assignedInventory;
        if (queuedTransaction.isReassignTransfer() && StringUtils.isNotBlank(queuedTransaction.getNewTransferInventoryId())) {
            targetInventoryId = queuedTransaction.getNewTransferInventoryId();
        }

        InventoryOperation.SourceType sourceType = InventoryOperation.SourceType.RefundTransaction;
        String source = "Refund Trans";
        if (queuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.RevertShippingManifest) {
            sourceType = InventoryOperation.SourceType.RevertShippingManifest;
            source = "Revert Trans";
        } else if (queuedTransaction.getQueuedTransType() == QueuedTransaction.QueuedTransactionType.RejectShippingManifest) {
            sourceType = InventoryOperation.SourceType.RejectShippingManifest;
            source = "Reject Trans";
        }

        String requestId = this.createSourceRequestId();

        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();

        //1. Adjust - put back for refunds
        for (OrderItem orderItem : transaction.getCart().getItems()) {
            String inventoryId = targetInventoryId;
            if (sourceType == InventoryOperation.SourceType.RevertShippingManifest && StringUtils.isNotBlank(orderItem.getInventoryId())) {
                inventoryId = orderItem.getInventoryId();
            }
            //Removing this TO_DO as it has been handled before request has been sent here
           /* //NEED TO HANDLE PARTIAL REFUNDS
            //anyway we can use the inventory_operations instead of quantityLogs?*/

           if (sourceType == InventoryOperation.SourceType.RefundTransaction && StringUtils.isNotBlank(orderItem.getParentId())) {
               InventoryOperation recentOp = inventoryOperationService.getRecentOperation(companyId,shopId,transaction.getParentTransactionId(),orderItem.getParentId());

               BigDecimal qtyToRefund = orderItem.getQuantity();
               if (recentOp != null) {
                   // get all ops
                   Iterable<InventoryOperation> ops = inventoryOperationService.getAllOperationsFor(companyId,shopId,orderItem.getParentId(),recentOp.getRequestId());
                   // Group by batchId
                   HashMap<String,BigDecimal> totalMap = new HashMap<>();
                   List<InventoryOperation> operations = Lists.newArrayList(ops);
                   for (InventoryOperation qtyOp : operations) {
                       BigDecimal quantity = totalMap.getOrDefault(qtyOp.getBatchId(),BigDecimal.ZERO);
                       quantity = quantity.add(qtyOp.getQuantity().negate());
                       totalMap.put(qtyOp.getBatchId(),quantity);
                   }

                   for (String batchId : totalMap.keySet()) {
                       BigDecimal amt = totalMap.get(batchId);
                       if (amt.doubleValue() <= 0) {
                           continue;
                       }

                       // add logic here
                       BigDecimal leftOver = amt.subtract(qtyToRefund);
                       BigDecimal qtyToDeduct = qtyToRefund;
                       if (leftOver.doubleValue() < 0) {

                           qtyToRefund = leftOver.abs();
                           qtyToDeduct = amt;

                           amt = BigDecimal.ZERO;
                           totalMap.put(batchId,amt);
                       } else {
                           amt = amt.subtract(qtyToDeduct);
                           totalMap.put(batchId,amt);
                           qtyToRefund = BigDecimal.ZERO;
                       }



                       InventoryOperationService.InventoryOperationResult operationResult =
                               inventoryOperationService.adjust(companyId,
                                       shopId, orderItem.getProductId(), orderItem.getPrepackageItemId(), batchId,
                                       inventoryId,qtyToDeduct, queuedTransaction.getSellerId(),
                                       sourceType, transaction.getId(), orderItem.getParentId(), recentOp.getRequestId(), InventoryOperation.SubSourceAction.Refund);

                       result.operations.addAll(operationResult.operations);
                   }

               }
               if (qtyToRefund.doubleValue() > 0) {
                   // somehow there are items left to refund
                   QuantityLog firstLog = null;
                   for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                       firstLog = quantityLog;
                       break;
                   }

                   InventoryOperationService.InventoryOperationResult operationResult =
                           inventoryOperationService.adjust(companyId,
                                   shopId, orderItem.getProductId(), firstLog.getPrepackageItemId(), firstLog.getBatchId(),
                                   inventoryId, qtyToRefund, queuedTransaction.getSellerId(),
                                   sourceType, transaction.getId(), orderItem.getParentId(), recentOp.getRequestId(), InventoryOperation.SubSourceAction.Refund);

                   result.operations.addAll(operationResult.operations);
               }

           } else {
               for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                   InventoryOperationService.InventoryOperationResult operationResult =
                           inventoryOperationService.adjust(companyId,
                                   shopId, orderItem.getProductId(), quantityLog.getPrepackageItemId(), quantityLog.getBatchId(),
                                   inventoryId, quantityLog.getQuantity(), queuedTransaction.getSellerId(),
                                   sourceType, transaction.getId(), orderItem.getId(), requestId, InventoryOperation.SubSourceAction.Refund);

                   result.operations.addAll(operationResult.operations);
               }
           }
        }

        //2. Sync
        syncInventoryOperations(shopId, result, String.format("%s #%s", source, transaction.getTransNo()), true);

    }

    private void processTransferNew(InventoryTransferHistory history, QueuedTransaction queuedTransaction) {
        String companyId = history.getCompanyId();
        String fromShopId = history.getFromShopId();
        String toShopId = history.getToShopId();
        String fromInventoryId = history.getFromInventoryId();
        String toInventoryId = history.getToInventoryId();


        // completed and processed
        if (history.getProcessedTime() != null && history.getProcessedTime() > 0) {
            return;
        }

        //1. Revert previous changes
        InventoryOperationService.InventoryOperationResult operations = inventoryOperationService.revertRecent(companyId, fromShopId,
                InventoryOperation.SourceType.Transfer, history.getId());

        syncInventoryOperations(fromShopId, operations, String.format("Revert Transfer: %s", history.getTransferNo()), true);

        if (history.getStatus() == InventoryTransferHistory.TransferStatus.DECLINED) {
            // Update procesed time
            history.setProcessedTime(DateTime.now().getMillis());
            history.setProcessing(Boolean.FALSE);
            transferHistoryRepository.update(companyId, history.getId(), history);
            return;
        }

        if (history.getTransferLogs() == null || history.getTransferLogs().size() == 0) {
            return;
        }

        // create new transfer request
        final String requestId = this.createSourceRequestId();

        //2. Deduct
        InventoryOperationService.InventoryOperationResult fromTransferOps = new InventoryOperationService.InventoryOperationResult();
        List<ObjectId> productIds = new ArrayList<>();
        for (InventoryTransferLog log : history.getTransferLogs()) {
            productIds.add(new ObjectId(log.getProductId()));
            BigDecimal deductAmt = log.getTransferAmount().multiply(new BigDecimal(-1));
            InventoryOperationService.InventoryOperationResult myResult;
            if (StringUtils.isNotBlank(log.getFromBatchId()) || StringUtils.isNotBlank(log.getPrepackageItemId())) {
                // deduct known batch
                 myResult =
                        inventoryOperationService.adjust(companyId, fromShopId,
                                log.getProductId(), log.getPrepackageItemId(), log.getFromBatchId(),
                                fromInventoryId,
                                deductAmt,
                                queuedTransaction.getSellerId(),
                                InventoryOperation.SourceType.Transfer, history.getId(),
                                null, requestId, InventoryOperation.SubSourceAction.Transfer);

                fromTransferOps.operations.addAll(myResult.operations);
            } else {
                // deduct unknown
                myResult =
                        deductUnknownFromInventory(companyId,
                                fromShopId, log.getProductId(), fromInventoryId, deductAmt,
                                queuedTransaction.getSellerId(), requestId, InventoryOperation.SubSourceAction.Transfer, InventoryOperation.SourceType.Transfer, history.getId());

                fromTransferOps.operations.addAll(myResult.operations);
            }
            //<batchId, quantity>
            Map<String, BigDecimal> batchQuantityMap = new HashMap<>();

            myResult.operations.forEach(inventoryOperation -> {
                batchQuantityMap.put(inventoryOperation.getBatchId(), inventoryOperation.getQuantity().negate().add(batchQuantityMap.getOrDefault(inventoryOperation.getBatchId(), BigDecimal.ZERO)));
            });
            log.setBatchQuantityMap(batchQuantityMap);
        }

        // sync from operations
        syncInventoryOperations(fromShopId, fromTransferOps, String.format("Prepare Transfer: %s", history.getTransferNo()), true);

        //3. Transfer
        if (history.isCompleteTransfer()) {

            InventoryOperationService.InventoryOperationResult toTransferOps =
                    processAcceptTransfer(companyId, fromShopId, toShopId, fromTransferOps, queuedTransaction,
                            toInventoryId, history, requestId, productIds);

            // Update and sync
            history.setProcessedTime(DateTime.now().getMillis());
            transferHistoryRepository.update(companyId, history.getId(), history);
            syncInventoryOperations(toShopId, toTransferOps, String.format("Complete Transfer: %s", history.getTransferNo()), true);

        }

        try {
            this.updateTransferHistoryData(history, companyId, fromShopId, toShopId);
        } catch (Exception ex) {
            LOGGER.error("InventoryTransferHistory :" + ex.getMessage());
        }

        // Compliance integration
        InventoryTransferEvent inventoryTransferEvent = new InventoryTransferEvent();
        inventoryTransferEvent.setPayload(history);
        try {
            blazeEventBus.post(inventoryTransferEvent);
        } catch (Exception e) {
            LOGGER.error("InventoryTransferEvent:" + e.getMessage());
        }
    }

    private InventoryOperationService.InventoryOperationResult processAcceptTransfer(String companyId, String fromShopId,
                                                                                     String toShopId,
                                                                                     InventoryOperationService.InventoryOperationResult fromTransferOps,
                                                                                     QueuedTransaction queuedTransaction,
                                                                                     String toInventoryId,
                                                                                     InventoryTransferHistory history,
                                                                                     String requestId,
                                                                                     List<ObjectId> productIds) {
        InventoryOperationService.InventoryOperationResult toTransferOps = new InventoryOperationService.InventoryOperationResult();


        if (toShopId == null || fromShopId.equalsIgnoreCase(toShopId)) {
            toShopId = fromShopId;
            for (InventoryOperation operation : fromTransferOps.operations) {

                // Now do the the transfer
                BigDecimal transferAmt = new BigDecimal(Math.abs(operation.getQuantity().doubleValue()));
                InventoryOperationService.InventoryOperationResult myResult =
                        inventoryOperationService.adjust(companyId, toShopId,
                                operation.getProductId(), operation.getPrepackageItemId(), operation.getBatchId(),
                                toInventoryId, transferAmt, queuedTransaction.getSellerId(),
                                InventoryOperation.SourceType.Transfer, history.getId(),
                                null, requestId, InventoryOperation.SubSourceAction.Transfer);

                toTransferOps.operations.addAll(myResult.operations);
            }

        } else {
            boolean isNew = false;
            HashMap<String, Product> otherProductHashMap = new HashMap<>();
            // We are now transferring to another shop, so we need to something special here
            final HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(companyId);

            // Check if inventory is being transferred to another shop
            // Find products of shop in which inventory needs to be transfer
            Iterable<Product> otherShopProducts = productRepository.listAllByShop(companyId, toShopId);
            // Create product map with name_vendorId as key
            for (Product product : otherShopProducts) {
                product.setCategory(productCategoryHashMap.get(product.getCategoryId()));
                String key = product.getCompanyLinkId();
                otherProductHashMap.put(key, product);
            }

            HashMap<String, Product> currentProductMap = productRepository.listAsMap(companyId, productIds);

            for (InventoryOperation operation : fromTransferOps.operations) {
                // As we are transfering to another shop, we need to fetch the other shop's info to transfer
                Product fromProduct = currentProductMap.get(operation.getProductId());
                if (fromProduct == null) {
                    continue;
                }

                fromProduct.setCategory(productCategoryHashMap.get(fromProduct.getCategoryId()));
                String key = fromProduct.getCompanyLinkId();
                Product toProduct = otherProductHashMap.get(key);


                // If toProduct doesn't exist, just continue
                if (toProduct == null) {
                    continue;
                }

                // get the batch info from this shop so we can transfer
                ProductBatch fromProductBatch = null;
                Prepackage toPrepackage = null;
                if (StringUtils.isNotBlank(operation.getPrepackageItemId())) {
                    PrepackageProductItem fromPrepackageProductItem = prepackageProductItemRepository.get(companyId, operation.getPrepackageItemId());
                    if (fromPrepackageProductItem != null) {
                        Prepackage fromPrepackage = prepackageRepository.get(companyId, fromPrepackageProductItem.getPrepackageId());

                        // Find matching prepackage with same name
                        toPrepackage = prepackageRepository.getPrepackage(companyId,
                                toShopId, toProduct.getId(), fromPrepackage.getName());

                        if (toPrepackage == null) {
                            // Create one with similar attribute
                            toPrepackage = (new Kryo()).copy(fromPrepackage);
                            toPrepackage.resetPrepare(companyId);
                            toPrepackage.setProductId(toProduct.getId());
                            toPrepackage.setShopId(toProduct.getShopId());
                            toPrepackage.setActive(true);

                            prepackageRepository.save(toPrepackage);
                        }
                        fromProductBatch = productBatchRepository.get(companyId, fromPrepackageProductItem.getBatchId());

                    }

                } else {
                    fromProductBatch = productBatchRepository.get(companyId, operation.getBatchId());
                }

                // if from batch doesn't exist, just continue as well
                if (fromProductBatch == null) {
                    continue;
                }
                ProductBatch toProductBatch = productBatchRepository.getBatchBySKU(companyId, toShopId, fromProductBatch.getSku());

                if (toProductBatch == null) {
                    isNew = true;
                    toProductBatch = (new Kryo()).copy(fromProductBatch);
                    toProductBatch.resetPrepare(toProduct.getCompanyId());
                    toProductBatch.setProductId(toProduct.getId());
                    toProductBatch.setShopId(toProduct.getShopId());
                    toProductBatch.setQuantity(new BigDecimal(0));
                    productBatchRepository.save(toProductBatch);


                    // Update sku if needed
                    BarcodeItem item = createBarcodeItemIfNeeded(companyId, toProduct.getShopId(), toProduct.getId(), BarcodeItem.BarcodeEntityType.Batch,
                            toProductBatch.getId(), fromProductBatch.getSku(), null, true);
                    if (item != null) {
                        toProductBatch.setSku(item.getBarcode());
                        toProductBatch.setBatchNo(item.getNumber());
                    }
                    productBatchRepository.save(toProductBatch);
                }


                String toBatchId = toProductBatch.getId();
                String toPrepackageItemId = null;

                if (toPrepackage != null) {
                    // find the prepackage if exist
                    PrepackageProductItem prepackageProductItem =
                            prepackageProductItemRepository.getPrepackageProduct(companyId, toShopId, toProduct.getId(), toPrepackage.getId(), toProductBatch.getId());
                    if (prepackageProductItem == null) {
                        prepackageProductItem = new PrepackageProductItem();
                        prepackageProductItem.prepare(companyId);
                        prepackageProductItem.setShopId(toShopId);
                        prepackageProductItem.setProductId(toProduct.getId());
                        prepackageProductItem.setBatchId(toProductBatch.getId());
                        prepackageProductItem.setPrepackageId(toPrepackage.getId());
                        prepackageProductItem.setActive(true);
                        prepackageProductItem.setBatchSKU(toProductBatch.getSku());
                        prepackageProductItem.setSku(toShopId);

                        BarcodeItem item = createBarcodeItemIfNeeded(companyId, toProduct.getShopId(), toProduct.getId(),
                                BarcodeItem.BarcodeEntityType.Prepackage,
                                prepackageProductItem.getId(), prepackageProductItem.getSku(), null, true);
                        if (item != null) {
                            prepackageProductItem.setSku(item.getBarcode());
                        }
                        prepackageProductItemRepository.save(prepackageProductItem);
                    }

                    toPrepackageItemId = prepackageProductItem.getId();
                }


                // Now do the the transfer
                BigDecimal transferAmt = new BigDecimal(Math.abs(operation.getQuantity().doubleValue()));
                InventoryOperationService.InventoryOperationResult myResult =
                        inventoryOperationService.adjust(companyId, toShopId,
                                toProduct.getId(), toPrepackageItemId, toBatchId,
                                toInventoryId, transferAmt, queuedTransaction.getSellerId(),
                                InventoryOperation.SourceType.Transfer, history.getId(),
                                null, requestId, InventoryOperation.SubSourceAction.Transfer);

                toTransferOps.operations.addAll(myResult.operations);

                if (isNew) {
                    prepareUpdateBatchDetails(toProduct, transferAmt, toProductBatch);
                }
            }

        }
        return toTransferOps;
    }


    /**
     * Process inventory updates for adding batch quantity for product batch's product request
     *
     * @param purchaseOrder     : Purchase Order object which holds the data
     * @param queuedTransaction : Queued transaction job's object
     */
    private void processPurchaseOrder(PurchaseOrder purchaseOrder, QueuedTransaction queuedTransaction) {
        final String companyId = purchaseOrder.getCompanyId();
        final String shopId = purchaseOrder.getShopId();

        //If PO does not have any product request then exit
        if (purchaseOrder.getPoProductRequestList() == null || purchaseOrder.getPoProductRequestList().size() == 0) {
            return;
        }


        //If shop has receiving inventory then select by default inventory other go for 'Safe' inventory of shop
        Inventory inventory = null;
        Shop shop = shopRepository.get(companyId, shopId);

        // If it's retail and already completed, then just returned
        if (CompanyFeatures.AppTarget.Retail == shop.getAppTarget() && purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
            return;
        }
        // If there's previously an operation for the PO, then it implies that we have already received this
        InventoryOperation prevOps = inventoryOperationService.getRecentOperation(companyId,shopId,purchaseOrder.getId());
        if (CompanyFeatures.AppTarget.Retail == shop.getAppTarget() && prevOps != null) {
            return;
        }

        if (StringUtils.isNotBlank(shop.getReceivingInventoryId()) && CompanyFeatures.AppTarget.Retail == shop.getAppTarget()) {
            inventory = inventoryRepository.get(companyId, shop.getReceivingInventoryId());
        }

        String targetInventoryType = Inventory.SAFE;
        if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
            targetInventoryType = Inventory.QUARANTINE;
        }

        if (inventory == null) {
            inventory = inventoryRepository.getInventory(companyId, shopId, targetInventoryType);
        }

        //If no inventory found then create one for SAFE
        if (inventory == null && CompanyFeatures.AppTarget.Retail == shop.getAppTarget()) {
            inventory = new Inventory();
            inventory.setCompanyId(companyId);
            inventory.setShopId(shopId);
            inventory.setName(Inventory.SAFE);
            inventory.setType(Inventory.InventoryType.Storage);

            inventoryRepository.save(inventory);
        }

        String requestId = this.createSourceRequestId();

        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();
        for (POProductRequest request : purchaseOrder.getPoProductRequestList()) {
            if (request.getBatchQuantityMap() != null && !request.getBatchQuantityMap().isEmpty()) {
                //Process update batches quantity created for product request
                for (Map.Entry<String, BigDecimal> entry : request.getBatchQuantityMap().entrySet()) {

                    if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
                        if (ProductBatch.BatchStatus.READY_FOR_SALE != request.getReceiveBatchStatus()) {
                            targetInventoryType = Inventory.QUARANTINE;
                        } else {
                            targetInventoryType = Inventory.SAFE;
                        }

                        if (inventory == null || !targetInventoryType.equalsIgnoreCase(inventory.getType().toString())) {
                            inventory = inventoryRepository.getInventory(companyId, shopId, targetInventoryType);
                        }

                        if (inventory == null) {
                            inventory = new Inventory();
                            inventory.setCompanyId(companyId);
                            inventory.setShopId(shopId);
                            if (Inventory.QUARANTINE.equalsIgnoreCase(targetInventoryType)) {
                                inventory.setName(Inventory.QUARANTINE);
                                inventory.setType(Inventory.InventoryType.Quarantine);
                            } else {
                                inventory.setName(Inventory.SAFE);
                                inventory.setType(Inventory.InventoryType.Storage);
                            }

                            inventoryRepository.save(inventory);
                        }
                    }

                    InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId, shopId, request.getProductId(), null, entry.getKey(),
                            inventory.getId(), entry.getValue(), queuedTransaction.getSellerId(),
                            InventoryOperation.SourceType.PurchaseOrder, purchaseOrder.getId(), null,
                            requestId, InventoryOperation.SubSourceAction.AddBatch);

                    result.operations.addAll(operationResult.operations);
                }
            }
        }

        syncInventoryOperations(shopId, result, InventoryOperation.SourceType.PurchaseOrder.name(), true);

        boolean closePo = false;
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
            closePo = true;
        } else if (StringUtils.isNotBlank(purchaseOrder.getShipmentBillId())) {
            ShipmentBill shipmentBill = shipmentBillRepository.getById(purchaseOrder.getShipmentBillId());
            if (shipmentBill != null && shipmentBill.getPaymentStatus() == ShipmentBill.PaymentStatus.PAID) {
                closePo = true;
            }
        }
        // close PO
        if (closePo) {
            //purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Closed);
            purchaseOrderRepository.updatePurchaseOrderStatus(purchaseOrder.getId(), PurchaseOrder.PurchaseOrderStatus.Closed);
        }
    }

    // ProductQuantities -> BatchQuantities (Update, add new batch if necessary) -> InventoryOperations
    private void processReconciliationNew(ReconciliationHistory reconciliationHistory) {
        final String companyId = reconciliationHistory.getCompanyId();
        final String shopId = reconciliationHistory.getShopId();

        String requestId = this.createSourceRequestId();
        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();
        for (ReconciliationHistoryList reconciliation : reconciliationHistory.getReconciliations()) {
            //If report loss then don't process because transaction should have already created for this
            if (reconciliation.isReportLoss()) {
                continue;
            }

            InventoryOperationService.InventoryOperationResult result1 = null;
            if (StringUtils.isNotBlank(reconciliation.getBatchId())
                    || StringUtils.isNotBlank(reconciliation.getPrePackageItemId())) {
                InventoryOperation lastOperation = inventoryOperationService.getRecentOperation(companyId,
                        shopId,
                        reconciliation.getProductId(),
                        reconciliation.getBatchId(),
                        reconciliation.getInventoryId(),
                        reconciliation.getPrePackageItemId());
                // find how much to reconcile
                BigDecimal amtToReconcile = reconciliation.getNewQuantity();
                if (lastOperation != null) {
                    amtToReconcile = amtToReconcile.subtract(lastOperation.getRunningTotal());
                }

                // apply the inventory operation
                result1 = inventoryOperationService.adjust(companyId,
                        shopId, reconciliation.getProductId(), reconciliation.getPrePackageItemId(),
                        reconciliation.getBatchId(), reconciliation.getInventoryId(), amtToReconcile,
                        reconciliationHistory.getEmployeeId(),
                        InventoryOperation.SourceType.Reconciliation, reconciliationHistory.getId(),
                        null, requestId, InventoryOperation.SubSourceAction.Reconcile);

            } else {
                // find out much to reconcile since we don't know the batch info
                BigDecimal totalInventory = batchQuantityRepository.getBatchQuantitiesForInventoryTotal(companyId, shopId, reconciliation.getProductId(), reconciliation.getInventoryId());
                BigDecimal amtToReconcile = reconciliation.getNewQuantity();
                if (totalInventory != null) {
                    amtToReconcile = amtToReconcile.subtract(totalInventory);
                }

                if (amtToReconcile.doubleValue() > 0) {
                    // we are adding, so add it to the newest batch (create new product batch if needed)
                    result1 = addUnknownFromInventory(companyId, shopId, reconciliation.getProductId(), reconciliation.getInventoryId(),
                            amtToReconcile, reconciliationHistory.getEmployeeId(), requestId, InventoryOperation.SubSourceAction.Reconcile,
                            InventoryOperation.SourceType.Reconciliation, reconciliationHistory.getId());
                } else {
                    // we're now deducting here instead, however, since it's unknown, we need to deduct one at a time

                    // now deducting
                    result1 = deductUnknownFromInventory(companyId, shopId, reconciliation.getProductId(), reconciliation.getInventoryId(),
                            amtToReconcile, reconciliationHistory.getEmployeeId(), requestId, InventoryOperation.SubSourceAction.Reconcile,
                            InventoryOperation.SourceType.Reconciliation, reconciliationHistory.getId());
                }
            }

            // update operations
            if (result1 != null) {
                result.operations.addAll(result1.operations);
                reconciliation.setOperations(result1.operations);
            }
        }

        //TODO: THIS SHOULD BE DONE OUTSIDE OF THE INVENTORY OPERATIONS
        if (result.operations.size() > 0) {
            // update reconciliation
            reconciliationHistoryRepository.update(reconciliationHistory.getCompanyId(), reconciliationHistory.getId(), reconciliationHistory);
        }


        // SYNC INVENTORY
        syncInventoryOperations(shopId, result, String.format("Inventory Reconciliation: #%s", reconciliationHistory.getRequestNo()), true);

        // Compliance integration
        InventoryReconciliationEvent inventoryReconciliationEvent = new InventoryReconciliationEvent();
        inventoryReconciliationEvent.setPayload(reconciliationHistory);
        blazeEventBus.post(inventoryReconciliationEvent);
    }

    private InventoryOperationService.InventoryOperationResult addUnknownFromInventory(String companyId, String shopId,
                                                                                       String productId, String inventoryId,
                                                                                       BigDecimal addAmt,
                                                                                       String employeeId, String requestId,
                                                                                       InventoryOperation.SubSourceAction subSourceAction,
                                                                                       InventoryOperation.SourceType sourceType,
                                                                                       String sourceId) {

        // Since this is unknown, add it to the latest batch. If batch doesn't exist, create a new one
        ProductBatch latestBatch = productBatchRepository.getLatestBatchForProductActive(companyId, shopId, productId);
        if (latestBatch == null) {
            // create a new batch, if latest unarchive batch doesn't exist
            latestBatch = new ProductBatch();
            latestBatch.prepare(companyId);
            latestBatch.setShopId(shopId);
            latestBatch.setQuantity(addAmt);
            latestBatch.setProductId(productId);
            latestBatch.setPurchasedDate(DateTime.now().getMillis());
            latestBatch.setSellBy(DateTime.now().plusMonths(6).getMillis());

            BarcodeItem barcodeItem = this.createBarcodeItem(companyId, shopId, productId, BarcodeItem.BarcodeEntityType.Batch, productId);
            latestBatch.setSku(barcodeItem.getBarcode());
            latestBatch = productBatchRepository.save(latestBatch);

        }

        InventoryOperationService.InventoryOperationResult result =
                inventoryOperationService.adjust(companyId, shopId, productId,
                        null, latestBatch.getId(), inventoryId, addAmt, employeeId, sourceType, sourceId, null, requestId, subSourceAction);
        return result;
    }

    private InventoryOperationService.InventoryOperationResult deductUnknownFromInventory(String companyId,
                                                                                          String shopId,
                                                                                          String productId,
                                                                                          String inventoryId,
                                                                                          BigDecimal quantityToSubtract,
                                                                                          String employeeId,
                                                                                          String requestId,
                                                                                          InventoryOperation.SubSourceAction subSourceAction,
                                                                                          InventoryOperation.SourceType sourceType,
                                                                                          String sourceId) {
        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();

        // deduct from oldest first
        Iterable<BatchQuantity> sortedBatches = batchQuantityRepository.getBatchQuantitiesForInventorySorted(companyId, shopId, productId, inventoryId, "{batchPurchaseDate:1}");

        double curQty = Math.abs(quantityToSubtract.doubleValue());
        for (BatchQuantity batchQuantity : sortedBatches) {
            if (batchQuantity.getQuantity().doubleValue() > 0 && curQty > 0) {
                double leftOver = batchQuantity.getQuantity().doubleValue() - curQty;

                double deductAmt = curQty;
                if (leftOver >= 0) {
                    // this means there were enough amt in the batch
                    curQty = 0;
                } else {
                    // less, so batchQuantity didn't have enough, let's continue to loop
                    deductAmt = curQty + leftOver; // (left over is negative

                    curQty = NumberUtils.round(Math.abs(leftOver), 2);
                }

                deductAmt = Math.abs(deductAmt);

                InventoryOperationService.InventoryOperationResult result1 =
                        inventoryOperationService.adjust(companyId, shopId, productId,
                                null, batchQuantity.getBatchId(), inventoryId, new BigDecimal(deductAmt * -1),
                                employeeId, sourceType, sourceId,
                                null, requestId, subSourceAction);

                // aggregate operations
                result.operations.addAll(result1.operations);

            }
        }
        return result;
    }


    /**
     * This method process inventory action for product batch
     *
     * @param productBatch      : product batch object
     * @param queuedTransaction : queued transaction job
     */
    private void processBatches(ProductBatch productBatch, QueuedTransaction queuedTransaction) {

        ProductBatchQueuedTransaction batchQueuedTransaction = (ProductBatchQueuedTransaction) queuedTransaction;
        final String companyId = productBatch.getCompanyId();
        final String shopId = productBatch.getShopId();

        if (Product.ProductType.DERIVED == batchQueuedTransaction.getProductType()) {
            processDerivedProductBatch(batchQueuedTransaction, companyId, shopId);
        } else if (Product.ProductType.BUNDLE == batchQueuedTransaction.getProductType()) {
            processBundleProductBatch(batchQueuedTransaction, companyId, shopId);
        } else {
            String requestId = this.createSourceRequestId();

            InventoryOperation.SubSourceAction subSourceAction = batchQueuedTransaction.getSubSourceAction();


            // apply action
            InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId,
                    shopId, productBatch.getProductId(), null, productBatch.getId(),
                    queuedTransaction.getInventoryId(), batchQueuedTransaction.getQuantity(), queuedTransaction.getSellerId(),
                    InventoryOperation.SourceType.ProductBatch,
                    productBatch.getId(), batchQueuedTransaction.getSourceChildId(), requestId, subSourceAction);

            syncInventoryOperations(shopId, operationResult, subSourceAction.name(), true);
        }
    }

    private void processBulkProductBatch(QueuedTransaction queuedTransaction) {
        final String companyId = queuedTransaction.getCompanyId();
        final String shopId = queuedTransaction.getShopId();

        ProductBatchQueuedTransaction batchQueuedTransaction = (ProductBatchQueuedTransaction) queuedTransaction;

        HashMap<String, Map<String, BigDecimal>> batchQuantityMap = batchQueuedTransaction.getBatchQuantityMap();
        Set<String> batchIdList = batchQuantityMap.keySet();

        List<ObjectId> batchIds = new ArrayList<>();
        for (String id : batchIdList) {
            batchIds.add(new ObjectId(id));
        }

        HashMap<String, ProductBatch> productBatchMap = productBatchRepository.listAsMap(companyId, batchIds);
        InventoryOperation.SubSourceAction subSourceAction = batchQueuedTransaction.getSubSourceAction();

        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();
        for (Map.Entry<String, Map<String, BigDecimal>> entry : batchQuantityMap.entrySet()) {
            ProductBatch batch = productBatchMap.get(entry.getKey());
            if (batch == null) {
                continue;
            }

            for (Map.Entry<String, BigDecimal> quantityEntry : entry.getValue().entrySet()) {
                String requestId = this.createSourceRequestId();


                InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId,
                        shopId, batch.getProductId(), null, batch.getId(),
                        quantityEntry.getKey(), quantityEntry.getValue(), queuedTransaction.getSellerId(),
                        InventoryOperation.SourceType.ProductBatch,
                        batch.getId(), batchQueuedTransaction.getSourceChildId(), requestId, subSourceAction);

                result.operations.addAll(operationResult.operations);
            }
        }

        syncInventoryOperations(shopId, result, subSourceAction.name(), true);

    }

    /**
     * This method creates UUID for inventory action
     */
    private String createSourceRequestId() {
        return UUID.randomUUID().toString();
    }

    /**
     * This method process stock reset
     *
     * @param queuedTransaction : queued transaction job
     */
    private void processStockReset(QueuedTransaction queuedTransaction) {
        final String companyId = queuedTransaction.getCompanyId();
        final String shopId = queuedTransaction.getShopId();
        final String employeeId = queuedTransaction.getSellerId();
        String requestId = createSourceRequestId();

        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.listAllByShop(companyId, shopId);

        List<InventoryOperation> actionList = new ArrayList<>();

        for (BatchQuantity batchQuantity : batchQuantities) {
            if (batchQuantity.getQuantity().doubleValue() <= 0 && StringUtils.isBlank(batchQuantity.getBatchId())) {
                continue;
            }
            InventoryOperation inventoryOperation = this.createInventoryActionForStockReset(companyId, shopId, batchQuantity.getQuantity().negate(), batchQuantity.getInventoryId(), batchQuantity.getBatchId(), batchQuantity.getProductId(),
                    employeeId, requestId, Boolean.FALSE);

            actionList.add(inventoryOperation);
        }

        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.listAllByShop(companyId, shopId);

        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
            if (prepackageQuantity.getQuantity() <= 0) {
                continue;
            }
            InventoryOperation inventoryOperation = this.createInventoryActionForStockReset(companyId, shopId, new BigDecimal(prepackageQuantity.getQuantity()).negate(), prepackageQuantity.getInventoryId(), prepackageQuantity.getPrepackageItemId(), prepackageQuantity.getProductId(),
                    employeeId, requestId, Boolean.TRUE);

            actionList.add(inventoryOperation);
        }

        List<List<InventoryOperation>> partition = Lists.partition(actionList, 1000);

        for (List<InventoryOperation> actions : partition) {
            inventoryActionRepository.save(actions);
        }

        //Delete batch quantities
        batchQuantityRepository.removeAllProductBatchQuantities(companyId, shopId);
        //Delete prepackage quantities
        prepackageQuantityRepository.resetAllProductPrepackageQuantities(companyId, shopId);
        //Delete products stocks
        productRepository.resetAllProductsStock(companyId, shopId);

        realtimeService.sendRealTimeEvent(companyId, RealtimeService.RealtimeEventType.ProductsUpdateEvent, null);
    }

    /**
     * This method process inventory for products
     *
     * @param requestQueuedTransaction : queued transaction job
     */
    private void processProduct(QueuedTransaction requestQueuedTransaction) {

        ConvertToProductQueuedTransaction queuedTransaction = (ConvertToProductQueuedTransaction) requestQueuedTransaction;

        final String companyId = queuedTransaction.getCompanyId();
        final String shopId = queuedTransaction.getShopId();
        final String employeeId = queuedTransaction.getSellerId();
        final String requestId = this.createSourceRequestId();

        Product product = productRepository.get(companyId, queuedTransaction.getTransactionId());

        InventoryOperationService.InventoryOperationResult inventoryOperationResult = deductUnknownFromInventory(companyId, shopId, product.getId(),
                queuedTransaction.getInventoryId(), queuedTransaction.getQuantity().negate(), employeeId,
                requestId, InventoryOperation.SubSourceAction.ConvertToProduct,
                InventoryOperation.SourceType.Product, product.getId());

        syncInventoryOperations(shopId, inventoryOperationResult, InventoryOperation.SubSourceAction.ConvertToProduct.name(), true);

    }

    /**
     * This method process inventory for derived products
     * @param queuedTransaction : queued transaction job
     * @param companyId
     * @param shopId
     */
    private void processDerivedProductBatch(ProductBatchQueuedTransaction queuedTransaction, String companyId, String shopId) {

        final String sellerId = queuedTransaction.getSellerId();

        String requestId = this.createSourceRequestId();
        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();

        ProductBatch productBatch = productBatchRepository.get(companyId, queuedTransaction.getTransactionId());
        if (StringUtils.isBlank(productBatch.getDerivedLogId())) {
            LOGGER.error("Derived log not found for batch " + productBatch.getId());
            return;
        }

        Product product = productRepository.get(companyId, productBatch.getProductId());
        if (Product.ProductType.DERIVED != product.getProductType()) {
            LOGGER.error("Product is not derived type so can't process here " + product.getId());
            return;
        }

        DerivedProductBatchLog derivedProductBatchLog = derivedLogRepository.get(companyId, productBatch.getDerivedLogId());
        if (derivedProductBatchLog == null || derivedProductBatchLog.getProductMap() == null) {
            LOGGER.error("Derived log not found for batch " + productBatch.getId());
            return;
        }

        InventoryOperation.SubSourceAction subSourceAction;
        if (ProductBatchQueuedTransaction.OperationType.Add == queuedTransaction.getOperationType()) {
            subSourceAction = InventoryOperation.SubSourceAction.AddBatch;
            //Deduct quantity from products
            this.processDerivedProduct(companyId, shopId, product, requestId, subSourceAction, queuedTransaction, sellerId, result);

            syncInventoryOperations(shopId, result, subSourceAction.name(), Boolean.TRUE);

            //Add quantity for created batch
            InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId, shopId,
                    product.getId(), null,
                    productBatch.getId(), queuedTransaction.getInventoryId(), queuedTransaction.getQuantity(),
                    sellerId, InventoryOperation.SourceType.DerivedProduct,
                    product.getId(), null, requestId, InventoryOperation.SubSourceAction.AddBatch);

            syncInventoryOperations(shopId, operationResult, subSourceAction.name(), Boolean.TRUE);
        } else if (ProductBatchQueuedTransaction.OperationType.Update == queuedTransaction.getOperationType()) {
            subSourceAction = InventoryOperation.SubSourceAction.UpdateBatch;

            if (queuedTransaction.isInventoryUpdate()) {
                InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.revertRecentForDerivedBatch(companyId, shopId,
                    product.getId(), null,
                    productBatch.getId(), queuedTransaction.getPreviousInventoryId(), queuedTransaction.getPreviousQuantity(),
                    sellerId, InventoryOperation.SourceType.DerivedProduct,
                    product.getId(), null, requestId, InventoryOperation.SubSourceAction.AddBatch);

                syncInventoryOperations(shopId, operationResult, subSourceAction.name(), Boolean.TRUE);

                operationResult = inventoryOperationService.adjust(companyId, shopId, product.getId(), null, productBatch.getId(), queuedTransaction.getInventoryId(), queuedTransaction.getPreviousQuantity(),
                    sellerId, InventoryOperation.SourceType.DerivedProduct,
                    product.getId(), null, requestId, InventoryOperation.SubSourceAction.AddBatch);

                syncInventoryOperations(shopId, operationResult, subSourceAction.name(), Boolean.TRUE);
            }
            this.processDerivedProduct(companyId, shopId, product, requestId, subSourceAction, queuedTransaction, sellerId, result);

            syncInventoryOperations(shopId, result, subSourceAction.name(), Boolean.TRUE);

            if (queuedTransaction.isUpdateBatchQuantity()) {
                InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId, shopId,
                        product.getId(), null,
                        productBatch.getId(), queuedTransaction.getInventoryId(), queuedTransaction.getQuantity(),
                        sellerId, InventoryOperation.SourceType.DerivedProduct,
                        product.getId(), null, requestId, InventoryOperation.SubSourceAction.AddBatch);

                syncInventoryOperations(shopId, operationResult, subSourceAction.name(), Boolean.TRUE);
            }
        }
    }

    private void processDerivedProduct(String companyId, String shopId, Product product, String requestId, InventoryOperation.SubSourceAction subSourceAction,
                                       ProductBatchQueuedTransaction queuedTransaction, String sellerId, InventoryOperationService.InventoryOperationResult result) {
        for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : queuedTransaction.getProductMap().entrySet()) {
            for (DerivedProductBatchInfo info : entry.getValue()) {
                String inventoryId = queuedTransaction.getInventoryId();
                if (StringUtils.isNotBlank(info.getInventoryId())) {
                    inventoryId = info.getInventoryId();
                } else if (StringUtils.isNotBlank(queuedTransaction.getDerivedInventoryId())) {
                    inventoryId = queuedTransaction.getDerivedInventoryId();
                }
                InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId, shopId,
                        entry.getKey(), null,
                        info.getBatchId(), inventoryId, info.getQuantity(),
                        sellerId, InventoryOperation.SourceType.DerivedProduct,
                        product.getId(), null, requestId, subSourceAction);

                result.operations.addAll(operationResult.operations);
            }
        }
    }

    /**
     * This method creates object for reset stock
     *
     * @param companyId    : company id
     * @param shopId       : shop id
     * @param quantity     : quantity
     * @param inventoryId  : inventory id
     * @param targetId     : target id (Prepackage item id or product batch id)
     * @param productId    : product id
     * @param employeeId   : employee id
     * @param requestId    : request id
     * @param byPrepackage : by prepackage or by product
     */
    private InventoryOperation createInventoryActionForStockReset(String companyId, String shopId, BigDecimal quantity, String inventoryId, String targetId, String productId,
                                                                  String employeeId, String requestId, Boolean byPrepackage) {
        InventoryOperation inventoryOperation = new InventoryOperation();
        inventoryOperation.prepare(companyId);
        inventoryOperation.setShopId(shopId);
        inventoryOperation.setQuantity(quantity);
        if (byPrepackage) {
            inventoryOperation.setPrepackageItemId(targetId);
        } else {
            inventoryOperation.setBatchId(targetId);
        }
        inventoryOperation.setInventoryId(inventoryId);
        inventoryOperation.setProductId(productId);
        inventoryOperation.setEmployeeId(employeeId);
        inventoryOperation.setSourceId(productId);
        inventoryOperation.setSourceType(InventoryOperation.SourceType.StockReset);
        inventoryOperation.setAction(InventoryOperation.Operation.Adjust);
        inventoryOperation.setRequestId(requestId);

        return inventoryOperation;
    }

    /**
     * This method process inventory action for prepackage
     *
     * @param prepackageProductItem : prepackage product line item's object
     * @param queuedTransaction     : queued transaction object
     */
    private void processPrepackage(QueuedTransaction queuedTransaction, PrepackageProductItem prepackageProductItem) {
        PrepackageProductItemQueuedTransaction prepackageQueuedTransaction = (PrepackageProductItemQueuedTransaction) queuedTransaction;

        final String companyId = queuedTransaction.getCompanyId();
        final String shopId = queuedTransaction.getShopId();

        Prepackage dbPrepackage = null;

        List<ObjectId> productIds = new ArrayList<>();
        if (prepackageProductItem != null) {
            productIds.add(new ObjectId(prepackageProductItem.getProductId()));
        } else if (StringUtils.isNotBlank(prepackageQueuedTransaction.getPrepackageId())) {
            dbPrepackage = prepackageRepository.get(companyId, prepackageQueuedTransaction.getPrepackageId());
            if (dbPrepackage == null) {
                return;
            }
            productIds.add(new ObjectId(dbPrepackage.getProductId()));
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, productIds);

        InventoryOperation.SubSourceAction subSourceAction;
        String requestId = this.createSourceRequestId();
        if (ProductBatchQueuedTransaction.OperationType.Add == prepackageQueuedTransaction.getOperationType()) {

            subSourceAction = InventoryOperation.SubSourceAction.AddPrepackage;

            //Add inventory in prepackage product quantity
            InventoryOperationService.InventoryOperationResult prepackageOperationResult = inventoryOperationService.adjust(companyId, shopId, prepackageProductItem.getProductId(),
                    prepackageProductItem.getId(), prepackageProductItem.getBatchId(),
                    prepackageQueuedTransaction.getInventoryId(), prepackageQueuedTransaction.getPrepackageItemQuantity(),
                    prepackageQueuedTransaction.getSellerId(), InventoryOperation.SourceType.Prepackage,
                    prepackageProductItem.getId(), null,
                    requestId, subSourceAction);

            syncInventoryOperations(shopId, prepackageOperationResult, subSourceAction.name(), true);

            //Deduct quantity from product and product batch
            InventoryOperationService.InventoryOperationResult batchOperationResult = inventoryOperationService.adjust(companyId, shopId, prepackageProductItem.getProductId(),
                    null, prepackageProductItem.getBatchId(),
                    prepackageQueuedTransaction.getInventoryId(), prepackageQueuedTransaction.getProductQuantity().negate(),
                    prepackageQueuedTransaction.getSellerId(), InventoryOperation.SourceType.Prepackage,
                    prepackageProductItem.getId(), null,
                    requestId, subSourceAction);

            syncInventoryOperations(shopId, batchOperationResult, subSourceAction.name(), true);

        } else if (ProductBatchQueuedTransaction.OperationType.ReducePrepackage == prepackageQueuedTransaction.getOperationType()) {

            subSourceAction = InventoryOperation.SubSourceAction.ReducePrepackage;

            //Deduct quantity from prepackage
            InventoryOperationService.InventoryOperationResult prepackageOperationResult = inventoryOperationService.adjust(companyId, shopId, prepackageProductItem.getProductId(),
                    prepackageProductItem.getId(), prepackageProductItem.getBatchId(),
                    prepackageQueuedTransaction.getInventoryId(), prepackageQueuedTransaction.getPrepackageItemQuantity().negate(),
                    prepackageQueuedTransaction.getSellerId(), InventoryOperation.SourceType.Prepackage,
                    prepackageProductItem.getId(), null,
                    requestId, subSourceAction);

            syncInventoryOperations(shopId, prepackageOperationResult, subSourceAction.name(), true);

            //Add quantity in product and product batch
            InventoryOperationService.InventoryOperationResult batchOperationResult = inventoryOperationService.adjust(companyId, shopId, prepackageProductItem.getProductId(),
                    null, prepackageProductItem.getBatchId(),
                    prepackageQueuedTransaction.getInventoryId(), prepackageQueuedTransaction.getProductQuantity(),
                    prepackageQueuedTransaction.getSellerId(), InventoryOperation.SourceType.Prepackage,
                    prepackageProductItem.getId(), null,
                    requestId, subSourceAction);

            syncInventoryOperations(shopId, batchOperationResult, subSourceAction.name(), true);

        } else if (ProductBatchQueuedTransaction.OperationType.DeletePrepackageItem == prepackageQueuedTransaction.getOperationType()) {

            if (prepackageProductItem == null) {
                return;
            }

            dbPrepackage = prepackageRepository.get(companyId, prepackageProductItem.getPrepackageId());

            if (dbPrepackage == null) {
                return;
            }

            Product product = productRepository.get(companyId, prepackageProductItem.getProductId());

            BigDecimal unitValue = dbPrepackage.getUnitValue();
            if (!dbPrepackage.isCustomWeight()) {
                ProductWeightTolerance weightTolerance = weightToleranceRepository.get(companyId, dbPrepackage.getToleranceId());
                if (weightTolerance != null) {
                    unitValue = weightTolerance.getUnitValue();
                }
            }

            Iterable<ProductPrepackageQuantity> quantities = prepackageQuantityRepository.getQuantitiesForPrepackageItem(companyId, shopId, prepackageProductItem.getId());

            for (ProductPrepackageQuantity quantity : quantities) {

                for (ProductQuantity productQuantity : product.getQuantities()) {

                    if (productQuantity.getInventoryId().equalsIgnoreCase(quantity.getInventoryId())) {
                        this.updateInventoryForPrepackageProductItem(companyId, shopId, prepackageQueuedTransaction, productQuantity, quantity, unitValue,
                                product, requestId, prepackageProductItem, InventoryOperation.SourceType.Prepackage, InventoryOperation.SubSourceAction.DeletePrepackageItem);
                    }
                }
                // delete the prepackages quantity
                prepackageQuantityRepository.removeById(companyId, quantity.getId());
            }

            // update the prepackages
            prepackageProductItemRepository.removeById(companyId, prepackageProductItem.getId());

        } else if (ProductBatchQueuedTransaction.OperationType.DeletePrepackage == prepackageQueuedTransaction.getOperationType()) {

            if (dbPrepackage == null) {
                return;
            }

            Product product = productMap.get(dbPrepackage.getProductId());
            if (product == null) {
                return;
            }

            BigDecimal unitValue = dbPrepackage.getUnitValue();
            if (!dbPrepackage.isCustomWeight()) {
                ProductWeightTolerance weightTolerance = weightToleranceRepository.get(companyId, dbPrepackage.getToleranceId());
                if (weightTolerance != null) {
                    unitValue = weightTolerance.getUnitValue();
                }
            }

            Iterable<PrepackageProductItem> items = prepackageProductItemRepository.getPrepackagesForPrepackageId(companyId, shopId, dbPrepackage.getId());
            Map<String, PrepackageProductItem> prepackageProductItemMap = new HashMap<>();
            for (PrepackageProductItem item : items) {
                prepackageProductItemMap.put(item.getId(), item);
            }

            Iterable<ProductPrepackageQuantity> quantities = prepackageQuantityRepository.getQuantitiesForPrepackage(companyId, shopId, dbPrepackage.getId());
            for (ProductPrepackageQuantity quantity : quantities) {

                for (ProductQuantity productQuantity : product.getQuantities()) {
                    PrepackageProductItem item = prepackageProductItemMap.get(quantity.getPrepackageItemId());
                    if (item == null) {
                        continue;
                    }

                    this.updateInventoryForPrepackageProductItem(companyId, shopId, prepackageQueuedTransaction, productQuantity, quantity, unitValue,
                            product, requestId, item, InventoryOperation.SourceType.Prepackage, InventoryOperation.SubSourceAction.DeletePrepackage);
                }

                for (PrepackageProductItem item : items) {
                    // delete the prepackages
                    prepackageProductItemRepository.removeById(companyId, item.getId());
                }
                // delete the prepackages quantity
                prepackageQuantityRepository.removeById(companyId, quantity.getId());
            }
        }
    }

    /**
     * This method add/update inventory for prepackage item
     *
     * @param companyId                   : company id
     * @param shopId                      : shop id
     * @param prepackageQueuedTransaction : prepackage queued transaction
     * @param productQuantity             : ProductQuantity
     * @param quantity                    : ProductPrepackageQuantity
     * @param unitValue                   : unit value of prepackage
     * @param product                     : product
     * @param requestId                   : request id
     * @param item                        : prepackage Product item
     * @param sourceType                  : inventory operation source type
     * @param subSourceAction             : inventory operation sub source type
     */
    private void updateInventoryForPrepackageProductItem(String companyId, String shopId, PrepackageProductItemQueuedTransaction prepackageQueuedTransaction, ProductQuantity productQuantity,
                                                         ProductPrepackageQuantity quantity, BigDecimal unitValue, Product product, String requestId, PrepackageProductItem item,
                                                         InventoryOperation.SourceType sourceType, InventoryOperation.SubSourceAction subSourceAction) {
        if (productQuantity.getInventoryId().equalsIgnoreCase(quantity.getInventoryId())) {

            double totalValue = quantity.getQuantity() * unitValue.doubleValue();

            InventoryOperationService.InventoryOperationResult prepackageOperationResult = inventoryOperationService.adjust(companyId, shopId, product.getId(),
                    item.getId(), item.getBatchId(),
                    quantity.getInventoryId(), new BigDecimal(quantity.getQuantity()).negate(),
                    prepackageQueuedTransaction.getSellerId(), sourceType,
                    item.getId(), null,
                    requestId, subSourceAction);

            syncInventoryOperations(shopId, prepackageOperationResult, subSourceAction.name(), true);

            InventoryOperationService.InventoryOperationResult batchOperationResult = inventoryOperationService.adjust(companyId, shopId, item.getProductId(),
                    null, item.getBatchId(),
                    quantity.getInventoryId(), new BigDecimal(totalValue),
                    prepackageQueuedTransaction.getSellerId(), sourceType,
                    item.getId(), null,
                    requestId, subSourceAction);

            syncInventoryOperations(shopId, batchOperationResult, subSourceAction.name(), true);
        }
    }


    /**
     * create an barcode for given parameters
     *
     * @param companyId  companyId
     * @param shopId     shopId
     * @param productId  productId
     * @param entityType entityType
     * @param entityId   entityId
     * @return item
     */
    private BarcodeItem createBarcodeItem(String companyId, String shopId, String productId, final BarcodeItem.BarcodeEntityType entityType, final String entityId) {
        BarcodeItem item = new BarcodeItem();
        item.prepare(companyId);
        item.setShopId(shopId);
        item.setEntityType(entityType);
        item.setEntityId(entityId);
        item.setProductId(productId);

        Company company = companyRepository.getById(companyId);
        String substr = "";
        if (company.getName().length() >= 2) {
            substr = company.getName().substring(0, 1);
        }
        String idstr = productId.substring(entityId.length() - 3, productId.length()).toUpperCase();
        /// 3 digits name -  product-category -- product-type -- product name -- sku number
        /// 2 - 3 - 3 - 4
        /// AB-AB3-PRO-10
        String key = String.format("%s-%s", idstr, entityType.getCode()).toUpperCase();
        CompanyUniqueSequence uniqueSequence = sequenceRepository.getNewIdentifier(companyId, key);

        String sku = String.format("%s%s%s%d", substr, idstr, entityType.getCode(), uniqueSequence.getCount());
        item.setNumber((int) uniqueSequence.getCount());
        item.setBarcode(sku);
        barcodeItemRepository.save(item);

        return item;
    }

    /**
     * This method created bar code if needed
     *
     * @param companyId    : company id
     * @param shopId       : shop id
     * @param productId    : product id for which bar code needs to generate
     * @param entityType   : bar code entity type
     * @param entityId     : entity id
     * @param currentKey   : current key
     * @param oldKey       : old key
     * @param transferShop : weather it's transfer to another shop
     */
    private BarcodeItem createBarcodeItemIfNeeded(final String companyId, final String shopId, String productId,
                                                  BarcodeItem.BarcodeEntityType entityType,
                                                  String entityId, String currentKey,
                                                  String oldKey,
                                                  boolean transferShop) {

        BarcodeItem item = null;
        if (oldKey != null) {
            BarcodeItem oldItem = barcodeItemRepository.getBarcode(companyId, shopId, entityType, oldKey);
            if (oldItem != null) {
                if (StringUtils.isNotBlank(oldKey) && currentKey.equalsIgnoreCase(oldKey) && oldItem.getEntityId().equalsIgnoreCase(entityId)) {
                    return oldItem; // same key so just return it. we don't have to create a new one
                }

                // delete old key so it can be re-used
                oldItem.setDeleted(true);
                oldItem.setBarcode(String.format("%s-del-%d", oldItem.getBarcode(), DateTime.now().getMillis()));
                barcodeItemRepository.update(companyId, oldItem.getId(), oldItem);
            }

        }
        //If current key matched with deleted barcode
        BarcodeItem deletedItem = barcodeItemRepository.getDeletedBarcode(companyId, shopId, entityType, currentKey);
        if (deletedItem != null) {
            deletedItem.setDeleted(false);
            if (!deletedItem.getProductId().equals(productId)) {
                LOGGER.info(String.format("Updating productId for %s to %s", currentKey, productId));
                deletedItem.setProductId(productId);
            }
            if (!deletedItem.getEntityId().equals(entityId)) {
                LOGGER.info(String.format("Updating entityId for %s to %s", currentKey, entityId));
                deletedItem.setEntityId(entityId);
            }
            LOGGER.info("Found old barcode -- reusing: " + currentKey);
            barcodeItemRepository.update(companyId, deletedItem.getId(), deletedItem);
            LOGGER.info("Found old barcode -- reusing: success" + currentKey);
            return deletedItem;
        }

        // test new key
        item = barcodeItemRepository.getBarcode(companyId, shopId, entityType, currentKey);
        if (item != null) {
            // If not the same entityType, then throw an exception stating that it's being used by another product/batch
            if (!item.getEntityId().equalsIgnoreCase(entityId)) {
                String productName = "";
                Product otherProduct = productRepository.get(companyId, item.getProductId());

                if (otherProduct != null) {
                    productName = otherProduct.getName();
                }
                if (transferShop
                        && entityType == BarcodeItem.BarcodeEntityType.Batch
                        && item.getProductId().equalsIgnoreCase(productId)) {
                    // ignore if you're transferring to another shop and the barcode already exists for this same product
                } else {
                    throw new BlazeInvalidArgException("Barcode",
                            String.format("SKU '%s' is currently used by another %s for product '%s'.", currentKey, entityType.name(), productName));
                }
            }

            if (!item.getProductId().equalsIgnoreCase(productId)) {
                item.setProductId(productId);
                barcodeItemRepository.update(companyId, item.getId(), item);
            }
            return item;
        }
        if (StringUtils.isBlank(currentKey)) {
            return createBarcodeItem(companyId, shopId, productId, entityType, entityId);
        }
        currentKey = currentKey.toUpperCase();
        CompanyUniqueSequence uniqueSequence = sequenceRepository.getNewIdentifier(companyId, currentKey);

        item = new BarcodeItem();
        item.prepare(companyId);
        item.setShopId(shopId);
        item.setEntityType(entityType);
        item.setEntityId(entityId);
        item.setBarcode(currentKey);
        item.setProductId(productId);
        item.setNumber((int) uniqueSequence.getCount());

        // Save new barcode with the SKU
        barcodeItemRepository.save(item);
        return item;
    }


    /**
     *
     * @param shopId
     * @param result
     * @param note
     * @param notify
     */
    private void syncInventoryOperations(String shopId, InventoryOperationService.InventoryOperationResult result, String note, boolean notify) {
        syncInventoryOperations(shopId,result,note,notify,notify);
    }
    // 1. Operate on inventory options
    // 2. Sync batch quantities
    // 3. Sync product quantities
    // 4. sync prepackage quantities
    // sync weedmap
    private void syncInventoryOperations(String shopId, InventoryOperationService.InventoryOperationResult result, String note, boolean notify, boolean syncWeedmap) {
        HashMap<String, String> visited = new HashMap<>();
        List<ProductChangeLog> changeLogs = new ArrayList<>();
        String employeeId = "";
        if (StringUtils.isNotBlank(note)) {
            LOGGER.info(String.format("shop: %s : %s", shopId, note));
        }
        Set<String> productIdSet = new HashSet<>();
        for (InventoryOperation operation : result.operations) {
            if (StringUtils.isNotBlank(operation.getPrepackageItemId())) {
                LOGGER.info(String.format("%s: PACK: (p: %s, b: %s, i: %s) => %s %.2f:  runningTotal: %.2f",
                        operation.getSubSourceAction(),
                        operation.getProductId(), operation.getBatchId(), operation.getInventoryId(), operation.getAction(), operation.getQuantity().doubleValue(), operation.getRunningTotal().doubleValue()));
            } else {
                LOGGER.info(String.format("%s: Batch: (p: %s, b: %s, i: %s) => %s %.2f:  runningTotal: %.2f",
                        operation.getSubSourceAction(),
                        operation.getProductId(), operation.getBatchId(), operation.getInventoryId(), operation.getAction(), operation.getQuantity().doubleValue(), operation.getRunningTotal().doubleValue()));
            }


            String key = String.format("%s_%s_%s_%s", operation.getProductId(), operation.getInventoryId(), operation.getBatchId(), operation.getPrepackageItemId());
            if (!visited.containsKey(key)) {
                if (StringUtils.isNotBlank(operation.getPrepackageItemId())) {
                    syncPrepackageInventory(operation.getCompanyId(), operation.getShopId(), operation.getProductId(), operation.getBatchId(), operation.getInventoryId(), operation.getPrepackageItemId());
                } else {
                    syncRawInventory(operation.getCompanyId(), operation.getShopId(), operation.getProductId(), operation.getBatchId(), operation.getInventoryId());
                }
                visited.put(key, key);


                // update product change logs


                Product product = productRepository.get(operation.getCompanyId(), operation.getProductId());
                List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(operation.getCompanyId(), shopId, operation.getProductId());

                ProductChangeLog changeLog = new ProductChangeLog();
                changeLog.prepare(operation.getCompanyId());
                changeLog.setShopId(operation.getShopId());
                changeLog.setProductId(operation.getProductId());
                changeLog.setEmployeeId(operation.getEmployeeId());
                changeLog.setReference(note);
                changeLog.setProductQuantities(product.getQuantities());
                changeLog.setPrepackageQuantities(productPrepackageQuantities);
                changeLogs.add(changeLog);
                employeeId = operation.getEmployeeId();
            }

            productIdSet.add(operation.getProductId());
        }
        productChangeLogRepository.save(changeLogs);

        // Notify that inventory has been updated
        if (notify && result.operations.size() > 0) {
            realtimeService.sendRealTimeEvent(shopId,
                    RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        }
        if (syncWeedmap) {
            syncWeedMap(productIdSet, shopId, employeeId);
        }
    }


    // Propagation to other parts
    private void syncRawInventory(String companyId, String shopId, String productId, String batchId, String inventoryId) {
        // get recent inventory
        Product product = productRepository.get(companyId, productId);
        if (product != null) {
            InventoryOperation inventoryOperation = inventoryOperationService.getRecentOperation(companyId, shopId, productId, batchId, inventoryId, null);
            if (inventoryOperation != null) {

                //1. Update Batch Quantities
                BatchQuantity batchQuantity = batchQuantityRepository.getBatchQuantityForInventory(companyId, shopId, productId, inventoryId, batchId);
                if (batchQuantity == null) {
                    // add new BatchQuantity// batch doesn't exist so let's just add a new batch quantity
                    batchQuantity = new BatchQuantity();
                    batchQuantity.prepare(companyId);
                    batchQuantity.setShopId(shopId);
                    batchQuantity.setBatchId(batchId);
                    batchQuantity.setProductId(productId);
                    batchQuantity.setQuantity(inventoryOperation.getRunningTotal());
                    batchQuantity.setInventoryId(inventoryId);

                    batchQuantityRepository.save(batchQuantity);
                } else {
                    batchQuantity.setQuantity(inventoryOperation.getRunningTotal());
                    batchQuantityRepository.update(companyId, batchQuantity.getId(), batchQuantity);
                }

                //2. Now update product quantities
                BigDecimal totalInventory = batchQuantityRepository.getBatchQuantitiesForInventoryTotal(companyId, shopId, productId, inventoryId);
                ProductQuantity targetPQ = null;
                for (ProductQuantity productQuantity : product.getQuantities()) {
                    if (productQuantity.getInventoryId().equalsIgnoreCase(inventoryId)) {
                        targetPQ = productQuantity;
                        break;
                    }
                }
                // fix to remove duplicate inventoryId
                product.getQuantities().removeIf(productQuantity -> productQuantity.getInventoryId().equalsIgnoreCase(inventoryId));

                if (targetPQ == null) {
                    // create one for this inventory
                    targetPQ = new ProductQuantity();
                    targetPQ.prepare(companyId);
                    targetPQ.setShopId(shopId);
                    targetPQ.setInventoryId(inventoryId);
                }
                product.getQuantities().add(targetPQ);

                // update total inventory
                targetPQ.setQuantity(totalInventory);

                productRepository.updateProductQuantities(companyId, product.getId(), product.getQuantities());

                updateBatchQuantityInfo(companyId, shopId, batchId);

                // update instock flag
                updateInStock(companyId,shopId,product);
            }
        }

    }

    private void updateInStock(String companyId, String shopId, Product product) {
        if (product == null) {
            return;
        }

        boolean instock = calculateInStock(companyId,shopId,product);
        productRepository.updateInStockFlag(companyId,product.getId(),instock);
    }

    private boolean calculateInStock(String companyId, String shopId,Product product) {
        for (ProductQuantity productQuantity : product.getQuantities()) {
            if (productQuantity.getQuantity() != null && productQuantity.getQuantity().doubleValue() > 0) {
                return true;
            }
        }

        Iterable<ProductPrepackageQuantity> preQuantities = prepackageQuantityRepository.getQuantitiesForProduct(companyId,shopId,product.getId());

        BigDecimal quantity = new BigDecimal(0);
        for (ProductPrepackageQuantity prepackageQuantity : preQuantities) {
            if (!prepackageQuantity.isDeleted() && prepackageQuantity.getQuantity() > 0) {
                return true;
            }
        }
        return false;
    }

    private void syncPrepackageInventory(String companyId, String shopId, String productId, String batchId, String inventoryId, String prepackageItemId) {
        // get recent inventory
        Product product = productRepository.get(companyId, productId);
        PrepackageProductItem prepackageProductItem = prepackageProductItemRepository.get(companyId, prepackageItemId);
        if (product != null && prepackageProductItem != null && product.getId().equalsIgnoreCase(prepackageProductItem.getProductId())) {
            InventoryOperation inventoryOperation = inventoryOperationService.getRecentOperation(companyId, shopId, productId, batchId, inventoryId, prepackageItemId);
            if (inventoryOperation != null) {

                ProductPrepackageQuantity productPrepackageQuantity = prepackageQuantityRepository.getQuantity(companyId, shopId, inventoryId, prepackageItemId);

                if (productPrepackageQuantity == null) {
                    productPrepackageQuantity = new ProductPrepackageQuantity();
                    productPrepackageQuantity.prepare(companyId);
                    productPrepackageQuantity.setShopId(shopId);
                    productPrepackageQuantity.setPrepackageId(prepackageProductItem.getPrepackageId());
                    productPrepackageQuantity.setPrepackageItemId(prepackageProductItem.getId());
                    productPrepackageQuantity.setInventoryId(inventoryId);
                    productPrepackageQuantity.setProductId(productId);
                    productPrepackageQuantity.setQuantity(inventoryOperation.getRunningTotal().intValue());
                    prepackageQuantityRepository.save(productPrepackageQuantity);
                } else {
                    productPrepackageQuantity.setQuantity(inventoryOperation.getRunningTotal().intValue());
                    prepackageQuantityRepository.update(companyId, productPrepackageQuantity.getId(), productPrepackageQuantity);
                }
                updatePrepackageQuantity(companyId, shopId, prepackageItemId, prepackageProductItem.getPrepackageId());


                // update instock flag
                updateInStock(companyId,shopId,product);
            }
        }

    }

    private void updatePrepackageQuantity(String companyId, String shopId, String prepackageItemId, String prepackageId) {
        Iterable<PrepackageProductItem> prepackageProductItems = prepackageProductItemRepository.getPrepackagesForPrepackageId(companyId, shopId, prepackageId);

        List<String> prepackageItemIds = new ArrayList<>();

        for (PrepackageProductItem prepackageProductItem : prepackageProductItems) {
            prepackageItemIds.add(prepackageProductItem.getId());
        }

        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForPrepackageItem(companyId, shopId, prepackageItemIds);

        HashMap<String, Integer> prepackageItemQuantityMap = new HashMap<>();
        int prepackageRunningTotal = 0;

        for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
            Integer quantity = prepackageItemQuantityMap.getOrDefault(prepackageQuantity.getPrepackageItemId(), 0);
            prepackageItemQuantityMap.put(prepackageQuantity.getPrepackageItemId(), quantity + prepackageQuantity.getQuantity());

            prepackageRunningTotal += prepackageQuantity.getQuantity();

        }

        prepackageProductItemRepository.updateRunningQuantity(companyId, shopId, prepackageItemId, prepackageItemQuantityMap.getOrDefault(prepackageItemId, 0));
        prepackageRepository.updateRunningQuantity(companyId, shopId, prepackageId, prepackageRunningTotal);

    }

    private void syncWeedMap(Set<String> productIdSet, String shopId, String employeeId) {
        if (productIdSet.isEmpty()) {
            LOGGER.info("Product Ids list is empty.");
            return;
        }
        if (StringUtils.isBlank(employeeId)) {
            LOGGER.info("Employee is blank.");
            return;
        }

        Shop shop = shopRepository.getById(shopId);
        if (shop == null) {
            return;
        }
        WeedmapSyncJob job = new WeedmapSyncJob();
        job.prepare(shop.getCompanyId());
        job.setShopId(shopId);
        job.setEmployeeId(employeeId);
        job.setStartTime(DateTime.now().getMillis());
        job.setRequestTime(DateTime.now().getMillis());
        job.setJobType(WeedmapSyncJob.WmSyncType.INDIVIDUAL);
        job.setStatus(WeedmapSyncJob.WmSyncJobStatus.Queued);
        job.setProductIds(Lists.newArrayList(productIdSet));
        wmSyncJobRepository.save(job);
        backgroundJobService.addWeedmapSyncJob(shop.getCompanyId(), shopId, job.getId());
    }

    /**
     * Private method to update inventory transfer logs for initial and final inventory
     *
     * @param history    : inventory history
     * @param companyId  : company id
     * @param fromShopId : from shop id
     * @param toShopId   : to shop id
     */
    private void updateTransferHistoryData(InventoryTransferHistory history, String companyId, String fromShopId, String toShopId) {
        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyId, fromShopId);
        HashMap<String, Product> otherProductHashMap = productRepository.listAsMap(companyId, toShopId);
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(companyId, toShopId);

        for (InventoryTransferLog inventoryTransferLog : history.getTransferLogs()) {
            Product fromProduct = productHashMap.get(inventoryTransferLog.getProductId());

            Map<String, Product> otherShopProduct = new HashMap<>();
            for (String productId : otherProductHashMap.keySet()) {
                Product product = otherProductHashMap.get(productId);
                String key = product.getCompanyLinkId();
                otherShopProduct.put(key, product);
            }

            // Check for same product is Another shop
            Product otherProduct = null;
            if (!fromShopId.equalsIgnoreCase(toShopId)) {
                String key = fromProduct.getCompanyLinkId();
                otherProduct = otherShopProduct.get(key);
            }

            Map<String, ProductBatch> fromProductBatchMap = new HashMap<>();
            for (String batch : batchHashMap.keySet()) {
                ProductBatch productBatch = batchHashMap.get(batch);
                fromProductBatchMap.putIfAbsent(productBatch.getProductId(), productBatch);
            }


            // TODO : For prepackages
            if (StringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) &&
                    ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {

                ProductPrepackageQuantity fromPreQuantity = quantityRepository.getQuantity(companyId,
                        fromProduct.getShopId(),
                        history.getFromInventoryId(),
                        inventoryTransferLog.getPrepackageItemId());
                if (fromPreQuantity == null) {
                    throw new BlazeInvalidArgException("PrepackageProduct", "Prepackage Line Item does not exist.");
                }

                ProductPrepackageQuantity toPreQuantity = null;
                if (otherProduct == null) {
                    toPreQuantity = quantityRepository.getQuantity(companyId,
                            fromProduct.getShopId(),
                            history.getToInventoryId(),
                            inventoryTransferLog.getPrepackageItemId());
                } else {
                    // Do this if toProduct is not null
                    Prepackage fromPrepackage = prepackageRepository.get(companyId, fromPreQuantity.getPrepackageId());
                    if (fromPrepackage == null) {
                        throw new BlazeInvalidArgException("InventoryTransfer", "Prepackage not found for product: " + fromProduct.getName());
                    }
                    PrepackageProductItem fromPrepackageProductItem = prepackageProductItemRepository.get(companyId, inventoryTransferLog.getPrepackageItemId());
                    if (fromPrepackageProductItem == null) {
                        throw new BlazeInvalidArgException("InventoryTransfer", "Prepackage Item not found for product: " + fromProduct.getName());
                    }

                    // Find matching prepackage with same name
                    Prepackage toPrepackage = prepackageRepository.getPrepackage(companyId,
                            otherProduct.getShopId(), otherProduct.getId(), fromPrepackage.getName());

                    if (toPrepackage == null) {
                        throw new BlazeInvalidArgException("InventoryTransfer", "Prepackage is not found for product" + otherProduct.getName());
                    }
                    // Get to Prepackage
                    PrepackageProductItem toPrepackageProductItem = prepackageProductItemRepository.getPrepackageProduct(companyId,
                            otherProduct.getShopId(),
                            otherProduct.getId(),
                            toPrepackage.getId(),
                            fromPrepackageProductItem.getBatchId());

                    if (toPrepackageProductItem == null) {
                        throw new BlazeInvalidArgException("InventoryTransfer", "Prepackage Product Item not found for product: " + otherProduct.getName());
                    }

                    if (!fromProduct.getShopId().equalsIgnoreCase(otherProduct.getShopId())) {
                        ProductBatch fromProductBatch = fromProductBatchMap.get(fromProduct.getId());

                        if (fromProductBatch == null && StringUtils.isNotBlank(toPrepackageProductItem.getBatchId()) && !toPrepackageProductItem.getBatchId().equals(inventoryTransferLog.getFromBatchId())) {
                            fromProductBatch = productBatchRepository.get(companyId, toPrepackageProductItem.getBatchId());
                        }

                        ProductBatch toBatch = null;
                        if (fromProductBatch != null && StringUtils.isNotBlank(fromProductBatch.getSku())) {
                            toBatch = productBatchRepository.getBatchBySKU(companyId, otherProduct.getShopId(), fromProductBatch.getSku());
                        }
                        inventoryTransferLog.setToBatchId(toBatch.getId());
                    }

                    // Now get the toQuantity
                    toPreQuantity = quantityRepository.getQuantity(companyId,
                            otherProduct.getShopId(),
                            history.getToInventoryId(),
                            toPrepackageProductItem.getId());

                }

                // update transfer log quantity
                int transferAmount = inventoryTransferLog.getTransferAmount().intValue();
                inventoryTransferLog.setOrigFromQty(BigDecimal.valueOf(fromPreQuantity.getQuantity() + transferAmount));
                inventoryTransferLog.setFinalFromQty(BigDecimal.valueOf(fromPreQuantity.getQuantity()));
                if (history.isCompleteTransfer()) {
                    inventoryTransferLog.setOrigToQty(BigDecimal.valueOf(toPreQuantity.getQuantity() - transferAmount));
                    inventoryTransferLog.setFinalInventory(BigDecimal.valueOf(toPreQuantity == null ? transferAmount : toPreQuantity.getQuantity()));
                } else {
                    inventoryTransferLog.setOrigToQty((toPreQuantity == null) ? BigDecimal.ZERO : BigDecimal.valueOf(toPreQuantity.getQuantity()));
                    inventoryTransferLog.setFinalInventory(BigDecimal.valueOf((toPreQuantity == null) ? transferAmount : toPreQuantity.getQuantity() + transferAmount));
                }
            } else {
                ProductQuantity fromQuantity = null;
                ProductQuantity toQuantity = null;
                for (ProductQuantity dbQuantity : fromProduct.getQuantities()) {
                    if (dbQuantity.getInventoryId().equalsIgnoreCase(history.getFromInventoryId())) {
                        fromQuantity = dbQuantity;
                    } else if (dbQuantity.getInventoryId().equalsIgnoreCase(history.getToInventoryId())) {
                        toQuantity = dbQuantity;
                    }
                }

                // If toProduct is not null, then find the toQuantity
                if (otherProduct != null) {
                    toQuantity = null;
                    for (ProductQuantity dbQuantity : otherProduct.getQuantities()) {
                        if (dbQuantity.getInventoryId().equalsIgnoreCase(history.getToInventoryId())) {
                            toQuantity = dbQuantity;
                            break;
                        }
                    }
                }


                // update transfer log quantity
                inventoryTransferLog.setOrigFromQty(fromQuantity.getQuantity().add(inventoryTransferLog.getTransferAmount()));
                inventoryTransferLog.setFinalFromQty(fromQuantity.getQuantity());
                if (history.isCompleteTransfer()) {
                    inventoryTransferLog.setOrigToQty((toQuantity == null) ? BigDecimal.ZERO : toQuantity.getQuantity().subtract(inventoryTransferLog.getTransferAmount()));
                    inventoryTransferLog.setFinalInventory((toQuantity == null) ? inventoryTransferLog.getTransferAmount() : toQuantity.getQuantity());
                } else {
                    inventoryTransferLog.setOrigToQty((toQuantity == null) ? BigDecimal.ZERO : toQuantity.getQuantity());
                    inventoryTransferLog.setFinalInventory((toQuantity == null) ? inventoryTransferLog.getTransferAmount() : toQuantity.getQuantity().add(inventoryTransferLog.getTransferAmount()));
                }

                if (otherProduct != null) {
                    if (!fromProduct.getShopId().equalsIgnoreCase(otherProduct.getShopId())) {
                        ProductBatch fromProductBatch = fromProductBatchMap.get(fromProduct.getId());

                        ProductBatch toBatch = null;
                        if (fromProductBatch != null && StringUtils.isNotBlank(fromProductBatch.getSku())) {
                            toBatch = productBatchRepository.getBatchBySKU(companyId, otherProduct.getShopId(), fromProductBatch.getSku());
                        }
                        if (toBatch != null) {
                            inventoryTransferLog.setToBatchId(toBatch.getId());
                        }
                    }
                }
            }
        }
        if (history.isCompleteTransfer()) {
            inventoryTransferHistoryRepository.update(companyId, history.getId(), history);
        }
    }


    private void updateBatchQuantityInfo(String companyId, String shopId, String batchId) {
        ProductBatch productBatch = productBatchRepository.getByShopAndId(companyId, shopId, batchId);
        if (productBatch == null) {
            return;
        }

        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.getBatchQuantityByBatch(companyId, shopId, batchId);

        BigDecimal liveQuantity = new BigDecimal(0);
        for (BatchQuantity batchQuantity : batchQuantities) {
            liveQuantity = liveQuantity.add(batchQuantity.getQuantity());
        }

        productBatchRepository.updateLiveQuantity(companyId, shopId, batchId, liveQuantity.doubleValue());
        productBatch.setLiveQuantity(liveQuantity);
        elasticSearchManager.createOrUpdateIndexedDocument(productBatch);
    }


    /**
     * This method process inventory for bundled products
     * @param queuedTransaction : queued transaction job
     * @param companyId
     * @param shopId
     */
    private void processBundleProductBatch(ProductBatchQueuedTransaction queuedTransaction, String companyId, String shopId) {

        final String sellerId = queuedTransaction.getSellerId();

        String requestId = this.createSourceRequestId();
        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();

        ProductBatch productBatch = productBatchRepository.get(companyId, queuedTransaction.getTransactionId());
        if (queuedTransaction.getBundleQuantityMap() == null || queuedTransaction.getBundleQuantityMap().isEmpty()) {
            LOGGER.error("Bundle product quantities not found " + productBatch.getId());
            return;
        }

        Product product = productRepository.get(companyId, productBatch.getProductId());
        if (Product.ProductType.BUNDLE != product.getProductType()) {
            LOGGER.error("Product is not bundle type so can't process here " + product.getId());
            return;
        }

        InventoryOperation.SubSourceAction subSourceAction;
        List<BatchBundleItems> batchBundleItems = new ArrayList<>();
        if (ProductBatchQueuedTransaction.OperationType.Add == queuedTransaction.getOperationType()) {
            subSourceAction = InventoryOperation.SubSourceAction.AddBatch;
            batchBundleItems = processBundleItems(companyId, shopId, product, requestId, subSourceAction, queuedTransaction, sellerId, result);
            //Add quantity for created batch
            InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId, shopId,
                    product.getId(), null,
                    productBatch.getId(), queuedTransaction.getInventoryId(), queuedTransaction.getQuantity(),
                    sellerId, InventoryOperation.SourceType.ProductBatch,
                    productBatch.getId(), null, requestId, InventoryOperation.SubSourceAction.AddBatch);

            syncInventoryOperations(shopId, operationResult, subSourceAction.name(), Boolean.TRUE);
        } else if (ProductBatchQueuedTransaction.OperationType.Update == queuedTransaction.getOperationType()) {
            subSourceAction = InventoryOperation.SubSourceAction.UpdateBatch;

            batchBundleItems = processBundleItems(companyId, shopId, product, requestId, subSourceAction, queuedTransaction, sellerId, result);

            InventoryOperationService.InventoryOperationResult operationResult = inventoryOperationService.adjust(companyId, shopId,
                    product.getId(), null,
                    productBatch.getId(), queuedTransaction.getInventoryId(), queuedTransaction.getQuantity(),
                    sellerId, InventoryOperation.SourceType.ProductBatch,
                    productBatch.getId(), null, requestId, InventoryOperation.SubSourceAction.UpdateBatch);

            syncInventoryOperations(shopId, operationResult, subSourceAction.name(), Boolean.TRUE);
        }

        if (!batchBundleItems.isEmpty()) {
            productBatch.setBundleItems(batchBundleItems);
            productBatchRepository.update(productBatch.getId(), productBatch);
        }
    }

    private List<BatchBundleItems> processBundleItems(String companyId, String shopId, Product product, String requestId, InventoryOperation.SubSourceAction subSourceAction,
                                                                 ProductBatchQueuedTransaction queuedTransaction, String sellerId, InventoryOperationService.InventoryOperationResult result) {

        //Deduct quantity from products
        result = inventoryOperationService.revertRecent(companyId, shopId, InventoryOperation.SourceType.BundleProduct, queuedTransaction.getTransactionId());

        syncInventoryOperations(shopId, result, subSourceAction.name(), Boolean.TRUE);

        InventoryOperationService.InventoryOperationResult bundleOpResult = new InventoryOperationService.InventoryOperationResult();
        for (Map.Entry<String, Double> entry : queuedTransaction.getBundleQuantityMap().entrySet()) {
            String key[] = entry.getKey().split("_");
            String productId = key[0];
            String batchId = key.length >= 2 && StringUtils.isNotBlank(key[1]) ? key[1] : "";
            String inventoryId = key.length >= 3 && StringUtils.isNotBlank(key[2]) ? key[2] : queuedTransaction.getInventoryId();

            if (entry.getValue() > 0) {
                if (StringUtils.isBlank(batchId)) {
                    // deduct unknown
                    InventoryOperationService.InventoryOperationResult myResult =
                            deductUnknownFromInventory(companyId,
                                    shopId, productId, queuedTransaction.getInventoryId(), BigDecimal.valueOf(entry.getValue()),
                                    sellerId, requestId, subSourceAction, InventoryOperation.SourceType.BundleProduct, queuedTransaction.getTransactionId());
                    result.operations.addAll(myResult.operations);
                    bundleOpResult.operations.addAll(myResult.operations);
                } else {
                    InventoryOperationService.InventoryOperationResult myResult = inventoryOperationService.adjust(companyId,
                            shopId, productId, null, batchId, inventoryId, BigDecimal.valueOf(entry.getValue()).negate(),
                            sellerId, InventoryOperation.SourceType.BundleProduct, queuedTransaction.getTransactionId(), null, requestId, subSourceAction);
                    result.operations.addAll(myResult.operations);
                }
            } else {
                if (StringUtils.isBlank(batchId)) {
                    InventoryOperationService.InventoryOperationResult myResult = addUnknownFromInventory(companyId, shopId, productId, queuedTransaction.getInventoryId(),
                            BigDecimal.valueOf(entry.getValue()), sellerId, requestId, subSourceAction,
                            InventoryOperation.SourceType.BundleProduct, queuedTransaction.getTransactionId());
                    result.operations.addAll(myResult.operations);
                    bundleOpResult.operations.addAll(myResult.operations);
                } else {
                    InventoryOperationService.InventoryOperationResult myResult = inventoryOperationService.adjust(companyId,
                            shopId, productId, null, batchId, inventoryId, BigDecimal.valueOf(entry.getValue()).negate(),
                            sellerId, InventoryOperation.SourceType.BundleProduct, queuedTransaction.getTransactionId(), null, requestId, subSourceAction);
                    result.operations.addAll(myResult.operations);
                }

            }
        }
        syncInventoryOperations(shopId, result, subSourceAction.name(), Boolean.TRUE);

        HashMap<String, BatchBundleItems> batchBundleItemsMap = new HashMap<>();
        bundleOpResult.operations.forEach(inventoryOperation -> {
            batchBundleItemsMap.putIfAbsent(inventoryOperation.getProductId(), new BatchBundleItems());
            BatchBundleItems batchBundleItems = batchBundleItemsMap.get(inventoryOperation.getProductId());
            batchBundleItems.setProductId(inventoryOperation.getProductId());

            BatchBundleItems.BatchItems batchItem = null;
            for (BatchBundleItems.BatchItems batchItems : batchBundleItems.getBatchItems()) {
                if ((StringUtils.isNotBlank(batchItems.getBatchId()) && batchItems.getBatchId().equalsIgnoreCase(inventoryOperation.getBatchId()))
                    && (StringUtils.isNotBlank(batchItems.getInventoryId()) && batchItems.getInventoryId().equalsIgnoreCase(inventoryOperation.getInventoryId()))) {
                    batchItem = batchItems;
                }
            }
            if (batchItem == null) {
                batchItem = new BatchBundleItems.BatchItems();
                batchItem.setBatchId(inventoryOperation.getBatchId());
                batchItem.setInventoryId(inventoryOperation.getInventoryId());
                batchItem.setQuantity(inventoryOperation.getQuantity().negate());
                batchBundleItems.getBatchItems().add(batchItem);
            } else {
                batchItem.setQuantity(batchItem.getQuantity().negate().add(inventoryOperation.getQuantity()));
            }
        });

        return Lists.newArrayList(batchBundleItemsMap.values());
    }


    private void prepareUpdateBatchDetails(Product product, BigDecimal quantity, ProductBatch productBatch) {
        double totalCost = productBatch.getCostPerUnit().doubleValue() * quantity.doubleValue();
        productBatch.setQuantity(quantity);
        productBatch.setCost(BigDecimal.valueOf(totalCost));


        Shop shop = shopRepository.getById(productBatch.getShopId());
        Vendor vendor = vendorRepository.getById(product.getVendorId());
        ProductCategory productCategory = productCategoryRepository.getById(product.getCategoryId());

        CompanyLicense companyLicense = null;
        if (vendor != null && CollectionUtils.isNotEmpty(vendor.getCompanyLicenses())) {
            companyLicense = vendor.getCompanyLicense(StringUtils.isNotBlank(productBatch.getLicenseId()) ? productBatch.getLicenseId() : "");
        } else {
            companyLicense = new CompanyLicense();
            companyLicense.setCompanyType(Vendor.CompanyType.RETAILER);
        }


        //OZ tax calculation
        if ((shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) && (Vendor.CompanyType.CULTIVATOR == companyLicense.getCompanyType() || Vendor.CompanyType.MANUFACTURER == companyLicense.getCompanyType()) && (PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == productBatch.getFlowerSourceType())) {
            CultivationTaxResult totalCultivationTax = taxCalulationService.calculateOzTax(productBatch.getCompanyId(), productBatch.getShopId(), product, productCategory, productBatch.getQuantity().doubleValue(), productBatch);
            productBatch.setTotalCultivationTax(totalCultivationTax.getTotalCultivationTax());
        } else {
            if (productCategory != null) {
                if ((product.getCannabisType() == Product.CannabisType.DEFAULT && productCategory.isCannabis())
                        || (product.getCannabisType() != Product.CannabisType.CBD
                        && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                        && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                    Vendor.ArmsLengthType armsLength = productBatch.getArmsLengthType();
                    if (armsLength == null) {
                        armsLength = (vendor != null) ? vendor.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;
                    }
                    HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(productBatch.getCompanyId(), productBatch.getShopId(), productBatch.getQuantity(), productBatch.getCostPerUnit(), armsLength, product.isCannabisProduct(productCategory != null && productCategory.isCannabis()));
                    productBatch.setTotalExciseTax(taxInfo.getOrDefault("totalExciseTax", BigDecimal.ZERO));
                    productBatch.setPerUnitExciseTax(taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO));
                }
            }
        }

        productBatchRepository.update(productBatch.getCompanyId(), productBatch.getId(), productBatch);
    }
}
