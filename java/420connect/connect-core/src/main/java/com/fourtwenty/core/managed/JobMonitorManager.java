package com.fourtwenty.core.managed;

import com.fourtwenty.core.domain.repositories.dispensary.QueuedTransactionRepository;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.thirdparty.AmazonSQSFifoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by mdo on 7/28/16.
 */
@Singleton
public class JobMonitorManager implements Managed {
    static final Log LOG = LogFactory.getLog(JobMonitorManager.class);
    private final BackgroundJobService jobService;
    private final QueuedTransactionRepository queuedTransactionRepository;
    private ScheduledExecutorService ses;
    @Inject
    AmazonSQSFifoService amazonSQSFifoService;

    public JobMonitorManager() {
        this.jobService = null;
        this.queuedTransactionRepository = null;
    }

    @Inject
    public JobMonitorManager(BackgroundJobService jobService, QueuedTransactionRepository queuedTransactionRepository) {
        this.jobService = jobService;
        this.queuedTransactionRepository = queuedTransactionRepository;
    }

    @Override
    public void start() throws Exception {
        ses = Executors.newScheduledThreadPool(8);
        /*ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // do some work
                long time = DateTime.now().minusMinutes(5).getMillis();
                long count = queuedTransactionRepository.getQueuedTransactionsCount(QueuedTransaction.QueueStatus.Pending,time);

                LOG.info("Checking queued transactions: " + count);
                if (count > 0) {
                    Iterable<QueuedTransaction> transactions = queuedTransactionRepository.getQueuedTransactions(QueuedTransaction.QueueStatus.Pending,time);
                    for (QueuedTransaction queuedTransaction : transactions) {
                        queuedTransactionRepository.updateRequestTime(queuedTransaction.getId());
                        //jobService.addTransactionJob(queuedTransaction.getCompanyId(),queuedTransaction.getShopId(),queuedTransaction.getId());
                        amazonSQSFifoService.publishQueuedTransaction(queuedTransaction.getCompanyId(),
                                queuedTransaction.getShopId(),
                                queuedTransaction.getId());
                    }
                }
            }
        }, 0, 30, TimeUnit.SECONDS);  // ex	ecute every x seconds
        */
    }

    @Override
    public void stop() throws Exception {
        if (ses != null) {
            ses.shutdown();
        }
    }
}
