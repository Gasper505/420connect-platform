package com.fourtwenty.core.managed;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflySyncJobRepository;
import com.fourtwenty.core.jobs.QueuedLeaflySyncJob;
import com.fourtwenty.core.services.thirdparty.models.LeaflySQSMessageRequest;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Singleton
public class LeaflySyncProcessorManager implements Managed {
    public static final String QUEUE_NAME = "LEAFLY_SYNC_QUEUE";

    private static final Log LOG = LogFactory.getLog(LeaflySyncProcessorManager.class);

    private ScheduledExecutorService scheduledExecutorService;
    @Inject
    private LeaflySyncJobRepository leaflySyncJobRepository;
    @Inject
    private ConnectConfiguration config;
    @Inject
    private Injector injector;
    @Inject
    private AmazonServiceFactory amazonServiceFactory;
    @Inject
    private AppRepository appRepository;
    private long initialDelay;

    @Override
    public void start() throws Exception {
       /* ZonedDateTime now = ZonedDateTime.now(ZoneId.of("America/Los_Angeles"));
        ZonedDateTime nextRun = now.withHour(12).withMinute(0).withSecond(0);
        if(now.compareTo(nextRun) > 0) {
            nextRun = nextRun.plusDays(1);
        }
        this.initialDelay = Duration.between(now, nextRun).getSeconds();
*/
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        final TransProcessorConfig processorConfig = config.getProcessorConfig();
        if (processorConfig == null || !processorConfig.isEnabled()) {
            LOG.info("Leafly Queue Processing is not enabled to process sync.");
        } else {
            if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.Local) {
                runLocalTransProcessor();
            } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
                runSQSLeaflyProcessor(processorConfig);
            }
        }
    }

    @Override
    public void stop() throws Exception {
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
        }
    }

    private void runLocalTransProcessor() {
        LOG.info("LOCAL Transaction Processing is enabled.");
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            Iterable<LeaflySyncJob> syncJobs = leaflySyncJobRepository.getQueuedJobs();

            for (LeaflySyncJob leaflySyncJob : syncJobs) {
                QueuedLeaflySyncJob queuedLeaflySyncJob = injector.getInstance(QueuedLeaflySyncJob.class);
                queuedLeaflySyncJob.setCompanyId(leaflySyncJob.getCompanyId());
                queuedLeaflySyncJob.setShopId(leaflySyncJob.getShopId());
                queuedLeaflySyncJob.setLeaflySyncJobId(leaflySyncJob.getId());

                try {
                    LOG.info("Executing Leafly Job: " + leaflySyncJob.getId());
                    queuedLeaflySyncJob.run();
                } catch (Exception e) {
                    LOG.error("Error processing queued Leafly job", e);
                }
            }

        }, 0, 1, TimeUnit.SECONDS);  // execute every x seconds
        //this.initialDelay, TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);  // execute every x seconds
    }

    private void runSQSLeaflyProcessor(final TransProcessorConfig processorConfig) {
        LOG.info("SQSQueue Leafly Sync Processing is enabled.");
        String env = config.getEnv().name();
        if (config.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();

                computername = computername.replace(".", "");
                env = String.format("%s-%s", config.getEnv().name(), computername);

            } catch (UnknownHostException e) {
            }
        }

        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app == null) {
            return;
        }
        int numOfQueues = 2;
        // need to have at least 1 queue
        if (app.getNumOfQueues() > 0) {
            numOfQueues = app.getNumOfQueues();
        }

        for (int i = 0; i < numOfQueues; i++) {
            String queueNumStr = (i > 0) ? "_" + i : "";


            //final String queueName =  env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
            final String multiQueueName = env + "-" + QUEUE_NAME + queueNumStr + ".fifo";
            LOG.info("SQSName: " + multiQueueName);

            LeaflySyncProcessorManager.QueueRunner queueRunner = new LeaflySyncProcessorManager.QueueRunner(multiQueueName);
            scheduledExecutorService.scheduleAtFixedRate(queueRunner, 0, 1, TimeUnit.SECONDS);
            //scheduledExecutorService.scheduleAtFixedRate(queueRunner, this.initialDelay, TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);
        }
    }

    protected class QueueRunner implements Runnable {
        final String queueName;
        public QueueRunner(String myQueueName) {
            queueName = myQueueName;
        }

        @Override
        public void run() {
            try {
                LOG.info(String.format("Queue: %s",queueName));

                AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();

                // do some work
                Map<String, String> attributes = new HashMap<String, String>();
                // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
                attributes.put("ContentBasedDeduplication", "true");
                // A FIFO queue must have the FifoQueue attribute set to True
                attributes.put("FifoQueue", "true");
                // The FIFO queue name must end with the .fifo suffix

                GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
                String myQueueUrl = null;
                try {
                    GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
                    if (queueUrlResult != null) {
                        myQueueUrl = queueUrlResult.getQueueUrl();
                    }
                } catch (Exception e) {
                    LOG.info("Queue does not exist.");
                }

                if (myQueueUrl == null) {
                    LOG.info("Queue does not exist. Creating new Queue: " + queueName);
                    CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
                    myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
                }


                // Receive messages
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
                // Uncomment the following to provide the ReceiveRequestDeduplicationId
                //receiveMessageRequest.setReceiveRequestAttemptId("1");
                List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();

                //LOG.info("Receiving messages from " + queueName + ", size: " + messages.size());
                for (Message message : messages) {
                    LOG.info("  Message");
                    LOG.info("    ReceiptHandle: " + message.getReceiptHandle());
                    LOG.info("    MessageId:     " + message.getMessageId());
                    LOG.info("    MD5OfBody:     " + message.getMD5OfBody());
                    LOG.info("    Body:          " + message.getBody());

                    LeaflySQSMessageRequest request = JsonSerializer.fromJson(message.getBody(), LeaflySQSMessageRequest.class);
                    if (request == null) {
                        continue;
                    }
                    QueuedLeaflySyncJob leaflySyncJob = injector.getInstance(QueuedLeaflySyncJob.class);
                    leaflySyncJob.setCompanyId(request.getCompanyId());
                    leaflySyncJob.setShopId(request.getShopId());
                    leaflySyncJob.setLeaflySyncJobId(request.getQueuedLeaflyJobId());

                    try {
                        LOG.info("Executing Queued Leafly sync job: " + request.getQueuedLeaflyJobId());
                        leaflySyncJob.run();
                        LOG.info(String.format("Deleting Message from SQS Queue: %s - %s", queueName, request.getQueuedLeaflyJobId()));
                        amazonSQS.deleteMessage(myQueueUrl, message.getReceiptHandle());
                    } catch (Exception e) {
                        LOG.error("Error processing queued Leafly sync job", e);
                    }
                }
            } catch (Exception e) {
                LOG.error("Error executing SQS", e);
            }
        }
    }
}
