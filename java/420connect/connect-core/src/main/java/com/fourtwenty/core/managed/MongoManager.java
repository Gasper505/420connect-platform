package com.fourtwenty.core.managed;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.db.MongoDb;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mongodb.*;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.JacksonMapper;

@Singleton
public class MongoManager implements Managed, MongoDb {
    @Inject
    private ConnectConfiguration config;
    private static MongoClientURI clientURI;
    private static Jongo jongo;

    private static MongoClient client;
    static final Log LOG = LogFactory.getLog(MongoManager.class);

    public MongoManager() {
        LOG.info("Initiating MongoManager");
    }

    public MongoManager(ConnectConfiguration config) {
        this.config = config;
    }

    public void start() throws Exception {
        // Only start if client isn't started
        if (client != null) return;
        LOG.info("Starting MongoManager");

        String mongoUri = config.getMongoConfiguration().getUri();
        clientURI = new MongoClientURI(mongoUri);
        client = new MongoClient(clientURI);

        DB db = client.getDB(clientURI.getDatabase());
        db.setWriteConcern(WriteConcern.ACKNOWLEDGED);
        jongo = new Jongo(db,
                new JacksonMapper.Builder()
                        .registerModule(new JodaModule())
                        .enable(MapperFeature.AUTO_DETECT_GETTERS)
                        .build());
    }


    public void stop() throws Exception {
        LOG.info("Stopping MongoManager - DB");

        if (client != null) {
            client.close();
            client = null;
        }
    }

    public MongoCollection getJongoCollection(String collectionName) throws Exception {
        //LOG.debug("Accessing collection: " + collectionName);
        start();
        return jongo.getCollection(collectionName);
    }

    public DBCollection getDBCollection(String collectionName) throws Exception {
        //LOG.debug("Accessing collection: " + collectionName);
        start();

        DB db = client.getDB(clientURI.getDatabase());

        return db.getCollection(collectionName);
    }

    public Jongo getJongo() throws Exception {
        start();
        return jongo;
    }

    public DB getDB() throws Exception {
        start();
        return client.getDB(clientURI.getDatabase());
    }

}
