package com.fourtwenty.core.managed;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.EmployeeOnFleetInfo;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeamInfo;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.OnFleetTeamsRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.*;
import com.fourtwenty.core.thirdparty.onfleet.models.response.CompletionDetails;
import com.fourtwenty.core.thirdparty.onfleet.models.response.OnFleetTaskResponse;
import com.fourtwenty.core.thirdparty.onfleet.models.response.TeamResult;
import com.fourtwenty.core.thirdparty.onfleet.models.response.WorkerResult;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetAPIService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class OnFleetManager implements Managed {

    private static final Logger LOG = LoggerFactory.getLogger(OnFleetManager.class);

    private static final String ERROR_UPDATE_WORKER = "Error while update worker at OnFleet";
    private static final String ONFLEET_WORKER = "Worker";
    private static final String ONFLEET_TASK = "Task";
    private static final String ERROR_CREATE_WORKER_FOR_TASK = "Error while creating worker at OnFleet in transaction process";
    private static final String ERROR_DELETE_WORKER_FOR_TASK = "Error while delete worker at OnFleet in transaction process";
    private static final String ERROR_GET_WORKER_FOR_TASK = "Error while getting worker at OnFleet in transaction process";
    private static final String ERROR_DELETE_FAIL_TASK = "Error while deleting transaction at Onfleet in transaction remove process";
    private static final String TASK_NOT_FOUND = "Task not found at Onfleet in transaction remove process";
    private static final String ERROR_COMPLETE_TASK = "Error in completing task in transaction remove process";
    private static final String ONFLEET_TASK_NOT_FOUND = "Task not found at Onfleet for this transaction";

    ExecutorService executorService;
    @Inject
    private ConnectConfiguration config;
    private OnFleetAPIService onFleetAPIService;
    private OnFleetTeamsRepository onFleetTeamsRepository;
    private AmazonServiceManager amazonServiceManager;
    private TransactionRepository transactionRepository;
    private EmployeeRepository employeeRepository;
    private MemberGroupRepository memberGroupRepository;
    private ProductRepository productRepository;

    @Inject
    public OnFleetManager(OnFleetAPIService onFleetAPIService,
                          OnFleetTeamsRepository onFleetTeamsRepository,
                          AmazonServiceManager amazonServiceManager,
                          TransactionRepository transactionRepository,
                          EmployeeRepository employeeRepository,
                          MemberGroupRepository memberGroupRepository,
                          ProductRepository productRepository) {
        this.onFleetAPIService = onFleetAPIService;
        this.onFleetTeamsRepository = onFleetTeamsRepository;
        this.amazonServiceManager = amazonServiceManager;
        this.transactionRepository = transactionRepository;
        this.employeeRepository = employeeRepository;
        this.memberGroupRepository = memberGroupRepository;
        this.productRepository = productRepository;
    }

    @Override
    public void start() throws Exception {
        LOG.debug("Starting OnFleetManager");
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping OnFleetManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void synchronizeTeam(final String apiKey, String companyId, Shop shop, OnFleetTeams onFleetTeams) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    String teams = onFleetAPIService.getAllTeams(apiKey, companyId, shop.getId());
                    List<TeamResult> teamResults = mapper.readValue(teams, new TypeReference<List<TeamResult>>() {
                    });
                    if (teamResults != null) {
                        synchronizeTeamWithShop(teamResults, companyId, shop, onFleetTeams);
                    }
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                    onFleetAPIService.addOnFleetError("Error in synchronize team", "Error in synchronize team", "500",
                            "", OnFleetErrorLog.ErrorType.Team, "Synchronize team", apiKey, companyId, shop.getId(), "");
                }
            }
        });
    }

    private void synchronizeTeamWithShop(List<TeamResult> teamResults, String companyId, Shop shop, OnFleetTeams onFleetTeams) {
        onFleetTeams.setSyncDate(DateTime.now().getMillis());

        List<OnFleetTeamInfo> teamInfoList = onFleetTeams.getOnFleetTeamInfoList();
        if (teamInfoList == null) {
            teamInfoList = new ArrayList<>();
        }
        for (TeamResult teamResult : teamResults) {
            if (shop.getHubId().equalsIgnoreCase(teamResult.getHub())) {
                OnFleetTeamInfo teamInfo = null;
                for (OnFleetTeamInfo onFleetTeamInfo : teamInfoList) {
                    if (teamResult.getId().equalsIgnoreCase(onFleetTeamInfo.getTeamId())) {
                        teamInfo = onFleetTeamInfo;
                        break;
                    }
                }

                if (teamInfo == null) {
                    teamInfo = new OnFleetTeamInfo();
                    teamInfo.prepare();
                    teamInfo.setTeamId(teamResult.getId());
                    teamInfoList.add(teamInfo);
                }
                teamInfo.setName(teamResult.getName());
                teamInfo.setHub(teamResult.getHub());
            }
        }
        onFleetTeams.setOnFleetTeamInfoList(teamInfoList);

        onFleetTeamsRepository.update(companyId, onFleetTeams.getId(), onFleetTeams);
    }

    public void createOnFleetTask(Shop shop, Member member, Transaction transaction, final String employeeWorkerId, String companyId, Employee employee, String onFleetTeamId, EmployeeOnFleetInfo employeeOnFleetInfo, boolean reAssign) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {

                if (reAssign && StringUtils.isNotBlank(transaction.getOnFleetTaskId())) {
                    try {
                        onFleetAPIService.deleteTask(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId());
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        onFleetAPIService.addOnFleetError(ERROR_DELETE_WORKER_FOR_TASK, e.getMessage(), "400",
                                ERROR_DELETE_WORKER_FOR_TASK, OnFleetErrorLog.ErrorType.Worker, ONFLEET_TASK, shop.getOnFleetApiKey(), companyId, shop.getId(), transaction.getId());
                    }
                }

                //Check if worker exist with provided id or not
                String workerId = null;
                WorkerResult worker = null;
                if (StringUtils.isNotBlank(employeeWorkerId)) {
                    try {
                        worker = onFleetAPIService.getWorkerById(shop.getOnFleetApiKey(), employeeWorkerId);
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        onFleetAPIService.addOnFleetError(ERROR_GET_WORKER_FOR_TASK, e.getMessage(), "400",
                                "Worker does not exist with already assigned id", OnFleetErrorLog.ErrorType.Worker, ONFLEET_WORKER, shop.getOnFleetApiKey(), companyId, shop.getId(), transaction.getId());
                    }
                }
                if (worker != null) {
                    workerId = worker.getId();
                }

                if (StringUtils.isNotBlank(onFleetTeamId)) {
                    WorkerResult workerResult = updateWorkerTeam(companyId, shop, employee, onFleetTeamId, employeeOnFleetInfo, transaction.getId(), worker);

                    if (workerResult != null) {
                        workerId = workerResult.getId();
                    }
                }

                OnFleetTaskRequest onFleetTaskRequest = new OnFleetTaskRequest();

                boolean createTask = createOnFleetTaskRequest(companyId, shop, employee, member, transaction, workerId, onFleetTaskRequest);

                if (createTask) {
                    OnFleetTaskResponse onFleetTask = onFleetAPIService.createOnFleetTask(companyId, shop.getId(), shop.getOnFleetApiKey(), onFleetTaskRequest, transaction);

                    LOG.info(String.format("Onfleet: %s", onFleetTask.getError()));
                    updateTransaction(companyId, onFleetTask, transaction);
                }
            }
        });
    }

    /**
     * This method update employee's OnFleet team if requested is not same not
     *
     * @param companyId           : company id
     * @param shop                : shop
     * @param employee            : employee's reference
     * @param teamId              : onFleet team id
     * @param employeeOnFleetInfo : employee's onfleet info
     * @param transactionId       : transaction id
     * @param worker              : worker
     */
    private WorkerResult updateWorkerTeam(String companyId, Shop shop, Employee employee, String teamId, EmployeeOnFleetInfo employeeOnFleetInfo, String transactionId, WorkerResult worker) {

        OnFleetTeams onFleetTeams = onFleetTeamsRepository.getByCompanyAndShop(companyId, shop.getId());
        String onFleetTeamId = "";
        if (onFleetTeams != null) {
            for (OnFleetTeamInfo teamInfo : onFleetTeams.getOnFleetTeamInfoList()) {
                if (teamInfo.getId().equalsIgnoreCase(teamId)) {
                    onFleetTeamId = teamInfo.getTeamId();
                }
            }
        }

        if (employeeOnFleetInfo != null && worker != null && !employeeOnFleetInfo.getOnFleetTeamList().iterator().next().equalsIgnoreCase(onFleetTeamId)) {
            //If employee's onfleet info is not null and employee's team is different then current one then update team for worker and employee

            LinkedHashSet<String> teamList = new LinkedHashSet<>();
            teamList.add(onFleetTeamId);

            UpdateWorkerRequest updateWorkerRequest = new UpdateWorkerRequest();
            updateWorkerRequest.setName(employee.getFirstName() + " " + employee.getLastName());
            updateWorkerRequest.setTeams(teamList);

            try {
                WorkerResult workerResult = onFleetAPIService.updateWorker(shop.getOnFleetApiKey(), employeeOnFleetInfo.getOnFleetWorkerId(), updateWorkerRequest);
                employeeOnFleetInfo.setOnFleetTeamList(teamList);

                List<EmployeeOnFleetInfo> onFleetInfoList = employee.getEmployeeOnFleetInfoList();
                onFleetInfoList.add(employeeOnFleetInfo);

                employee.setEmployeeOnFleetInfoList(onFleetInfoList);
                employeeRepository.update(companyId, employee.getId(), employee);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                onFleetAPIService.addOnFleetError(ERROR_UPDATE_WORKER, e.getMessage(), "400",
                        e.getMessage(), OnFleetErrorLog.ErrorType.Worker, ONFLEET_WORKER, shop.getOnFleetApiKey(), companyId, shop.getId(), transactionId);
            }

        } else if (employeeOnFleetInfo == null || worker == null) {
            //If employee's onfleet info does not present then create worker at OnFleet and update in system

            return createWorkerForEmployee(companyId, shop, employee, transactionId, onFleetTeamId);

        }

        return null;

    }

    private WorkerResult createWorkerForEmployee(String companyId, Shop shop, Employee employee, String transactionId, String onFleetTeamId) {
        String phoneNumber = amazonServiceManager.cleanPhoneNumber(employee.getPhoneNumber(), shop);
        if (StringUtils.isBlank(phoneNumber)) {
            onFleetAPIService.addOnFleetError(ERROR_CREATE_WORKER_FOR_TASK, "Invalid phone number", "400",
                    "Employee does not valid phone number", OnFleetErrorLog.ErrorType.Worker, ONFLEET_WORKER, shop.getOnFleetApiKey(), companyId, shop.getId(), transactionId);
            return null;
        }

        WorkerRequest workerRequest = new WorkerRequest();
        workerRequest.setName(employee.getFirstName() + " " + employee.getLastName());
        workerRequest.setPhone(phoneNumber);
        workerRequest.setTeams(Lists.newArrayList(onFleetTeamId));

        WorkerVehicleRequest vehicle = new WorkerVehicleRequest();
        workerRequest.setVehicle(vehicle);

        WorkerResult worker = null;
        try {
            worker = onFleetAPIService.createWorker(shop.getOnFleetApiKey(), workerRequest);

            if (worker != null) {

                List<EmployeeOnFleetInfo> employeeOnFleetInfoList = employee.getEmployeeOnFleetInfoList();

                EmployeeOnFleetInfo employeeOnfFleetInfo = null;
                if (employeeOnFleetInfoList != null) {
                    for (EmployeeOnFleetInfo onFleetInfo : employeeOnFleetInfoList) {
                        if (onFleetInfo.getShopId().equalsIgnoreCase(shop.getId())) {
                            employeeOnfFleetInfo = onFleetInfo;
                            break;
                        }
                    }
                }

                if (employeeOnfFleetInfo == null) {
                    employeeOnfFleetInfo = new EmployeeOnFleetInfo();
                }

                LinkedHashSet<String> teamList = new LinkedHashSet<>();
                teamList.add(onFleetTeamId);

                employeeOnfFleetInfo.setOnFleetWorkerId(worker.getId());
                employeeOnfFleetInfo.setOnFleetTeamList(teamList);
                employeeOnfFleetInfo.setShopId(shop.getId());

                List<EmployeeOnFleetInfo> onFleetInfoList = employee.getEmployeeOnFleetInfoList();
                if (onFleetInfoList == null) {
                    onFleetInfoList = new ArrayList<>();
                }

                onFleetInfoList.add(employeeOnfFleetInfo);

                employee.setEmployeeOnFleetInfoList(onFleetInfoList);

                employeeRepository.update(companyId, employee.getId(), employee);

                return worker;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            onFleetAPIService.addOnFleetError(ERROR_CREATE_WORKER_FOR_TASK, e.getMessage(), "400",
                    e.getMessage(), OnFleetErrorLog.ErrorType.Worker, ONFLEET_WORKER, shop.getOnFleetApiKey(), companyId, shop.getId(), transactionId);
        }

        return worker;
    }

    private boolean createOnFleetTaskRequest(String companyId, Shop shop, Employee employee, Member member, Transaction transaction, String workerId, OnFleetTaskRequest onFleetTaskRequest) {
        DestinationRequest destinationRequest = new DestinationRequest();
        OnFleetAddress onFleetAddress = new OnFleetAddress();
        StringBuilder address = new StringBuilder();

        // Use transaction delivery address
        Address memberAddress = transaction.getDeliveryAddress();

        if (memberAddress == null || !memberAddress.isValid()) {
            memberAddress = member.getAddress();
        }

        if (memberAddress == null) {
            onFleetAPIService.addOnFleetError("Error while creating task at OnFleet", "Address not found", "400",
                    "Member does not have address", OnFleetErrorLog.ErrorType.Task, "Create task", shop.getOnFleetApiKey(), companyId, shop.getId(), transaction.getId());
            return false;
        }

        if (StringUtils.isNotBlank(memberAddress.getAddress())) {
            //onFleetAddress.setStreet(memberAddress.getAddress());
            address.append(memberAddress.getAddress());
            address.append(", ");
        }

        if (StringUtils.isNotBlank(memberAddress.getCity())) {
            //onFleetAddress.setCity(memberAddress.getCity());
            address.append(memberAddress.getCity());
            address.append(", ");
        }

        if (StringUtils.isNotBlank(memberAddress.getZipCode())) {
            //onFleetAddress.setPostalCode(memberAddress.getZipCode());
            address.append(memberAddress.getZipCode());
            address.append(", ");
        }

        if (StringUtils.isNotBlank(memberAddress.getState())) {
            //onFleetAddress.setState(memberAddress.getState());
            address.append(memberAddress.getState());
            address.append(", ");
        }

        if (StringUtils.isNotBlank(memberAddress.getCountry())) {
            //onFleetAddress.setCountry(memberAddress.getCountry());
            address.append(memberAddress.getCountry());
        }

        onFleetAddress.setUnparsed(address.toString());


        destinationRequest.setAddress(onFleetAddress);

        String phoneNumber = "";
        if (StringUtils.isNotBlank(member.getPrimaryPhone())) {
            //
            phoneNumber = amazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop).replaceAll("[^0-9]", "");
            phoneNumber = amazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
        }


        RecipientsRequest recipientsRequest = new RecipientsRequest();
        recipientsRequest.setName(member.getFirstName() + " " + member.getLastName());
        recipientsRequest.setPhone(phoneNumber);

        MemberGroup memberGroup = memberGroupRepository.get(companyId, member.getMemberGroupId());


        StringBuilder memberNoteSB = new StringBuilder();
        if (memberGroup != null) {
            memberNoteSB.append(String.format("\nMember Group: %s", memberGroup.getName()));
        }
        for (Note note : member.getNotes()) {
            if (note.isEnableOnFleet() && !note.isDeleted()) {
                memberNoteSB.append("\n");
                memberNoteSB.append(TextUtil.textOrEmpty(note.getMessage()));
            }
        }
        recipientsRequest.setNotes(memberNoteSB.toString());

        if (StringUtils.isNotBlank(transaction.getCreatedById())) {
            Employee createdEmployee = employeeRepository.get(companyId, transaction.getCreatedById());
            if (createdEmployee != null) {
                address.append(String.format("\nCreated By: %s %s.", TextUtil.textOrEmpty(createdEmployee.getFirstName()), TextUtil.textOrEmpty(createdEmployee.getLastName())));
            }
        }

        if (employee != null) {
            address.append(String.format("\nAssigned To: %s %s.", TextUtil.textOrEmpty(employee.getFirstName()), TextUtil.textOrEmpty(employee.getLastName())));
        }

        //If unassigned order is enabled then don't assign container
        if (!shop.isEnableUnAssignedOnfleetOrder()) {
            OnFleetContainer container = new OnFleetContainer();
            if (StringUtils.isNotBlank(workerId)) {
                container.setType(OnFleetContainer.TaskType.WORKER);
                container.setWorker(workerId);
            } else {
                container.setType(OnFleetContainer.TaskType.ORGANIZATION);
                container.setWorker(null);
            }

            onFleetTaskRequest.setContainer(container);
        }

        onFleetTaskRequest.setRecipients(Lists.newArrayList(recipientsRequest));
        onFleetTaskRequest.setDestination(destinationRequest);
        if (StringUtils.isNotBlank(transaction.getMemo())) {
            address.append("\n\n");
            address.append(TextUtil.textOrEmpty(transaction.getMemo()));
        }

        String orderNotes = generateOrderNotes(transaction);
        if (StringUtils.isNotBlank(orderNotes)) {
            address.append(orderNotes);
        }

        onFleetTaskRequest.setNotes(address.toString());

        if (transaction.getCompleteAfter() != null && transaction.getCompleteAfter() > 0) {
            onFleetTaskRequest.setCompleteAfter(transaction.getCompleteAfter());
        }

        long nowUTC = DateUtil.nowUTC().plusSeconds(30).getMillis();
        if (transaction.getQueueType() == Transaction.QueueType.Delivery && transaction.getDeliveryDate() > 0) {
            if (transaction.getDeliveryDate() < nowUTC) {
                onFleetTaskRequest.setCompleteBefore(nowUTC);
            } else {
                onFleetTaskRequest.setCompleteBefore(transaction.getDeliveryDate());
            }
        } else if (transaction.getPickUpDate() != null && transaction.getPickUpDate() > 0) {
            if (transaction.getPickUpDate() < nowUTC) {
                onFleetTaskRequest.setCompleteBefore(nowUTC);
            } else {
                onFleetTaskRequest.setCompleteBefore(transaction.getPickUpDate());
            }
        }

        //Complete After
        //Complete Before
        return true;
    }


    private String generateOrderNotes(Transaction dbTrans) {
        // loop through and create the order notes
        if (productRepository == null) {
            return "";
        }
        StringBuilder orders = new StringBuilder();
        orders.append("\n\n-------------------\n");
        orders.append("Order Details");
        orders.append("\n-------------------\n\n");

        // Get the products
        List<ObjectId> productIds = new ArrayList<ObjectId>();
        for (OrderItem o : dbTrans.getCart().getItems()) {
            productIds.add(new ObjectId(o.getProductId()));
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(dbTrans.getCompanyId(), productIds);
        for (OrderItem o : dbTrans.getCart().getItems()) {
            Product p = productHashMap.get(o.getProductId());
            if (p != null) {
                orders.append(String.format("%.2f x %s\n", o.getQuantity().doubleValue(), p.getName()));
            }
        }

        if (dbTrans.getCart().getPaymentOption() != Cart.PaymentOption.None) {
            orders.append(String.format("Payment Option: %s\n", dbTrans.getCart().getPaymentOption()));
        }
        orders.append(String.format("Total: %s\n", TextUtil.toCurrency(dbTrans.getCart().getTotal().doubleValue(), null)));
        orders.append("-------------------\n");
        return orders.toString();
    }


    private void updateTransaction(String companyId, OnFleetTaskResponse taskResponse, Transaction trans) {

        if (taskResponse != null && !taskResponse.isNotCreated()) {
            /*trans.setOnFleetTaskId(taskResponse.getId());
            trans.setState(taskResponse.getState());
            trans.setShortId(taskResponse.getShortId());
            trans.setCreateOnfleetTask(true);

            transactionRepository.update(companyId, trans.getId(), trans);*/
            transactionRepository.updateOnfleetInfo(companyId, trans.getId(), taskResponse.getState(), taskResponse.getId(), taskResponse.getShortId(), true);
        }

    }

    public void completeTask(Shop shop, Transaction transaction, StatusRequest request) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {

                try {

                    CompletionDetails completionDetails = new CompletionDetails();
                    completionDetails.setSuccess(request.isStatus());
                    completionDetails.setTime(DateTime.now().getMillis());
                    completionDetails.setNotes("Completed from Blaze");
                    CompleteTask completeTask = new CompleteTask();
                    completeTask.setCompletionDetails(completionDetails);

                    Response response = onFleetAPIService.completeTask(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId(), completeTask);
                    if (response != null && response.getStatus() == 200) {
                        transactionRepository.updateOnFleetTaskStatus(transaction.getCompanyId(), transaction.getShopId(), transaction.getId(), Transaction.OnFleetTaskStatus.TASK_COMPLETED);
                    }

                } catch (Exception e) {
                    LOG.error("Error while completing task at onfleet for transaction :" + transaction.getId());
                }

            }
        });
    }

    /**
     * Method to update onfleet task when schedule time updated
     *
     * @param companyId           : companyId
     * @param shop                : shop
     * @param transaction         : transaction
     * @param employeeWorkerId    : employeeWorkerId
     * @param employee            : employee
     * @param employeeOnFleetInfo : employeeOnfleetInfo
     * @param member              : member
     */
    public void updateOnfleetTaskSchedule(String companyId, Shop shop, Transaction transaction, String employeeWorkerId, Employee employee, EmployeeOnFleetInfo employeeOnFleetInfo, Member member) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {

                String onFleetTeamId = "";
                if (StringUtils.isNotBlank(transaction.getOnFleetTaskId())) {
                    try {
                        OnFleetTask onFleetTask = onFleetAPIService.getOnFleetTaskById(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId());
                        if (onFleetTask != null) {
                            onFleetTeamId = onFleetTask.getWorker();
                        }
                        onFleetAPIService.deleteTask(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId());
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        onFleetAPIService.addOnFleetError(ERROR_DELETE_WORKER_FOR_TASK, e.getMessage(), "400",
                                ERROR_DELETE_WORKER_FOR_TASK, OnFleetErrorLog.ErrorType.Worker, ONFLEET_TASK, shop.getOnFleetApiKey(), companyId, shop.getId(), transaction.getId());
                        return;
                    }
                }

                //Check if worker exist with provided id or not
                String workerId = null;
                WorkerResult worker = null;
                if (StringUtils.isNotBlank(employeeWorkerId)) {
                    try {
                        worker = onFleetAPIService.getWorkerById(shop.getOnFleetApiKey(), employeeWorkerId);
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        onFleetAPIService.addOnFleetError(ERROR_GET_WORKER_FOR_TASK, e.getMessage(), "400",
                                "Worker does not exist with already assigned id", OnFleetErrorLog.ErrorType.Worker, ONFLEET_WORKER, shop.getOnFleetApiKey(), companyId, shop.getId(), transaction.getId());
                    }
                }
                if (worker != null) {
                    workerId = worker.getId();
                }

                if (StringUtils.isNotBlank(onFleetTeamId)) {
                    WorkerResult workerResult = updateWorkerTeam(companyId, shop, employee, onFleetTeamId, employeeOnFleetInfo, transaction.getId(), worker);

                    if (workerResult != null) {
                        workerId = workerResult.getId();
                    }
                }

                OnFleetTaskRequest onFleetTaskRequest = new OnFleetTaskRequest();

                boolean createTask = createOnFleetTaskRequest(companyId, shop, employee, member, transaction, workerId, onFleetTaskRequest);

                if (createTask) {
                    OnFleetTaskResponse onFleetTask = onFleetAPIService.createOnFleetTask(companyId, shop.getId(), shop.getOnFleetApiKey(), onFleetTaskRequest, transaction);

                    LOG.info(String.format("Onfleet: %s", onFleetTask.getError()));
                    updateTransaction(companyId, onFleetTask, transaction);
                }
            }
        });
    }

    /**
     * Delete task at onfleet
     * Check if task in started, if started then mark as failed otherwise delete transaction
     * @param shop: shop instance
     * @param transaction: transaction
     */
    public void deleteOrMarkFailedTaskAtOnfleet(Shop shop, Transaction transaction) {
        try {

            OnFleetTask onFleetTaskById = onFleetAPIService.getOnFleetTaskById(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId());

            if (onFleetTaskById != null) {

                if (onFleetTaskById.getState() == 2) {
                    StatusRequest request = new StatusRequest();
                    request.setStatus(Boolean.FALSE);
                    this.completeTask(shop, transaction, request);
                } else {
                    onFleetAPIService.deleteTask(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId());
                }

            } else {
                LOG.error(TASK_NOT_FOUND);
                onFleetAPIService.addOnFleetError(ERROR_DELETE_FAIL_TASK, ONFLEET_TASK_NOT_FOUND, "400",
                        ERROR_DELETE_FAIL_TASK, OnFleetErrorLog.ErrorType.Task, ONFLEET_TASK, shop.getOnFleetApiKey(), shop.getCompanyId(), shop.getId(), transaction.getId());
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            onFleetAPIService.addOnFleetError(ERROR_DELETE_WORKER_FOR_TASK, e.getMessage(), "400",
                    ERROR_DELETE_FAIL_TASK, OnFleetErrorLog.ErrorType.Task, ONFLEET_TASK, shop.getOnFleetApiKey(), shop.getCompanyId(), shop.getId(), transaction.getId());
        }
    }

    public void completeTaskAtOnfleet(Shop shop, Transaction transaction, StatusRequest request) {
        try {

            OnFleetTask onFleetTaskById = onFleetAPIService.getOnFleetTaskById(shop.getOnFleetApiKey(), transaction.getOnFleetTaskId());

            if (onFleetTaskById != null) {

                if (onFleetTaskById.getState() != 2) {
                    onFleetAPIService.addOnFleetError(ERROR_COMPLETE_TASK, "Transaction is not yet started so can't be marked as completed", "400",
                            ERROR_COMPLETE_TASK, OnFleetErrorLog.ErrorType.Task, ONFLEET_TASK, shop.getOnFleetApiKey(), shop.getCompanyId(), shop.getId(), transaction.getId());
                } else {
                    this.completeTask(shop, transaction, request);
                }

            } else {
                LOG.error(TASK_NOT_FOUND);
                onFleetAPIService.addOnFleetError(ERROR_COMPLETE_TASK, ONFLEET_TASK_NOT_FOUND, "400",
                        ERROR_COMPLETE_TASK, OnFleetErrorLog.ErrorType.Task, ONFLEET_TASK, shop.getOnFleetApiKey(), shop.getCompanyId(), shop.getId(), transaction.getId());
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            onFleetAPIService.addOnFleetError(ERROR_COMPLETE_TASK, e.getMessage(), "400",
                    ERROR_COMPLETE_TASK, OnFleetErrorLog.ErrorType.Task, ONFLEET_TASK, shop.getOnFleetApiKey(), shop.getCompanyId(), shop.getId(), transaction.getId());
        }
    }
}
