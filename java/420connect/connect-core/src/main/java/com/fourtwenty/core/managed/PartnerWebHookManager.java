package com.fourtwenty.core.managed;

import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.partner.WebHookLog;
import com.fourtwenty.core.domain.repositories.partner.PartnerWebHookRepository;
import com.fourtwenty.core.domain.repositories.partner.WebHookLogRepository;
import com.fourtwenty.core.rest.partners.request.models.PartnerWebHookResponse;
import com.fourtwenty.core.rest.store.webhooks.*;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SSLUtilities;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.mdo.pusher.SimpleRestUtil;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.message.internal.MessageBodyProviderNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class PartnerWebHookManager implements Managed {
    private static final Logger LOG = LoggerFactory.getLogger(PartnerWebHookManager.class);

    private ExecutorService executorService;
    @Inject
    private Injector injector;

    private PartnerWebHookRepository partnerWebHookRepository;
    private WebHookLogRepository webHookLogRepository;

    /**
     * Starts the object. Called <i>before</i> the application becomes available.
     *
     * @throws Exception if something goes wrong; this will halt the application startup.
     */
    @Override
    public void start() throws Exception {
        LOG.debug("Starting Partner Web Hook Task");

        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(10);
        }

        if (partnerWebHookRepository == null) {
            partnerWebHookRepository = injector.getInstance(PartnerWebHookRepository.class);
        }
        if (webHookLogRepository == null) {
            webHookLogRepository = injector.getInstance(WebHookLogRepository.class);
        }

    }

    /**
     * Stops the object. Called <i>after</i> the application is no longer accepting requests.
     *
     * @throws Exception if something goes wrong.
     */
    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping PARTNER Web Hook Task");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void newConsumerOrderHook(final String companyId, final String shopId, final ConsumerOrderData consumerOrderData) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {

                final PartnerWebHook webHook = partnerWebHookRepository.getByWebHookType(companyId, shopId, PartnerWebHook.PartnerWebHookType.NEW_CONSUMER_ORDER);

                if (webHook != null && StringUtils.isNotBlank(webHook.getUrl())) {
                    updateWebHook(webHook);
                }
                LOG.info("New Customer Order Web Hook");


                if (webHook != null && webHook.getWebHookUrls().size() > 0) {
                    final String json = JsonSerializer.toJson(consumerOrderData);
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
                    List<WebHookLog> logList = new ArrayList<>();
                    for (String webHookUrl : webHook.getWebHookUrls()) {
                        LOG.info("Webhook url : " + webHookUrl + "\n Order data : " + json);
                        try {
                            SimpleRestUtil.postWithSsl(webHookUrl, json, PartnerWebHookResponse.class, headers);
                        } catch (MessageBodyProviderNotFoundException me) {
                            LOG.info("Web Hook completed but got no response error(can be ignored) for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "Web Hook completed but got no response error(can be ignored) because of: " + me.getMessage()));
                        } catch (Exception e) {
                            LOG.error("New Order Web Hook failed for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl, e);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "New Order Web Hook failed because of: " + e.getMessage()));
                        }
                    }

                    if (!logList.isEmpty()) {
                        webHookLogRepository.save(logList);
                    }
                }
            }
        });
    }


    public void updateConsumerOrderHook(final String companyId, final String shopId, final ConsumerOrderData consumerOrderData) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {

                final PartnerWebHook webHook = partnerWebHookRepository.getByWebHookType(companyId, shopId, PartnerWebHook.PartnerWebHookType.UPDATE_CONSUMER_ORDER);

                if (webHook != null && StringUtils.isNotBlank(webHook.getUrl())) {
                    updateWebHook(webHook);
                }
                LOG.info(":: Update Customer Order Web Hook ::");


                if (webHook != null && webHook.getWebHookUrls().size() > 0) {
                    final String json = JsonSerializer.toJson(consumerOrderData);
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
                    List<WebHookLog> logList = new ArrayList<>();
                    for (String webHookUrl : webHook.getWebHookUrls()) {
                        LOG.info("Webhook url : " + webHookUrl + "\n Order data : " + json);
                        try {
                            SimpleRestUtil.postWithSsl(webHookUrl, json, PartnerWebHookResponse.class, headers);
                        } catch (MessageBodyProviderNotFoundException me) {
                            LOG.info("Web Hook completed but got no response error(can be ignored) for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "Web Hook completed but got no response error(can be ignored) because of: " + me.getMessage()));
                        } catch (Exception e) {
                            LOG.error("Update Order Web Hook failed for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl, e);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "Update Order Web Hook failed because of: " + e.getMessage()));
                        }
                    }

                    if (!logList.isEmpty()) {
                        webHookLogRepository.save(logList);
                    }
                }
            }
        });
    }

    public void newConsumerUserHook(final String companyId, final String shopId, final ConsumerUserData consumerUserData) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {

                final PartnerWebHook webHook = partnerWebHookRepository.getByWebHookType(companyId, shopId, PartnerWebHook.PartnerWebHookType.NEW_CONSUMER_SIGNUP);

                if (webHook != null && StringUtils.isNotBlank(webHook.getUrl())) {
                    updateWebHook(webHook);
                }
                LOG.info(":: New Customer User Web Hook ::");


                if (webHook != null && webHook.getWebHookUrls().size() > 0) {
                    final String json = JsonSerializer.toJson(consumerUserData);
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
                    List<WebHookLog> logList = new ArrayList<>();
                    for (String webHookUrl : webHook.getWebHookUrls()) {
                        LOG.info("New Customer Order ::" + "\n" + "Webhook url : " + webHookUrl + "\n Data : " + json);
                        try {
                            SimpleRestUtil.postWithSsl(webHookUrl, json, PartnerWebHookResponse.class, headers);

                        } catch (MessageBodyProviderNotFoundException me) {
                            LOG.info("Web Hook completed but got no response error(can be ignored) for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "Web Hook completed but got no response error(can be ignored) because of: " + me.getMessage()));
                        } catch (Exception e) {
                            LOG.error("New Consumer User Web Hook failed for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl, e);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "New Consumer User Web Hook failed because of: " + e.getMessage()));
                        }
                    }

                    if (!logList.isEmpty()) {
                        webHookLogRepository.save(logList);
                    }
                }
            }
        });
    }

    public void memberWebHook(final String companyId, final String shopId, final MemberData memberData, final PartnerWebHook.PartnerWebHookType partnerWebHookType) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {

                final PartnerWebHook webHook = partnerWebHookRepository.getByWebHookType(companyId, shopId, partnerWebHookType);

                if (webHook != null && StringUtils.isNotBlank(webHook.getUrl())) {
                    updateWebHook(webHook);
                }

                LOG.info("Member Web Hook");

                if (webHook != null && webHook.getWebHookUrls().size() > 0) {
                    final String json = JsonSerializer.toJson(memberData);
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
                    List<WebHookLog> logList = new ArrayList<>();
                    for (String webHookUrl : webHook.getWebHookUrls()) {
                        LOG.info("Webhook url : " + webHookUrl + "\n Member data : " + json);
                        try {
                            SimpleRestUtil.postWithSsl(webHookUrl, json, PartnerWebHookResponse.class, headers);

                        } catch (MessageBodyProviderNotFoundException me) {
                            LOG.info("Web Hook completed but got no response error(can be ignored) for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "Web Hook completed but got no response error(can be ignored) because of: " + me.getMessage()));
                        } catch (Exception e) {
                            LOG.error("Member Web Hook failed for shop: " + shopId + ",companyId: " + companyId + " url: " + webHookUrl, e);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, webHook, "Member Web Hook failed because of: " + e.getMessage()));
                        }
                    }

                    if (!logList.isEmpty()) {
                        webHookLogRepository.save(logList);
                    }
                }
            }
        });
    }


    public void completeTransactionWebHook(final String companyId, final String shopId, final TransactionData transactionData) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                PartnerWebHook dbWebHook = partnerWebHookRepository.getByWebHookType(companyId, shopId, PartnerWebHook.PartnerWebHookType.COMPLETE_TRANSACTION);

                if (dbWebHook != null && StringUtils.isNotBlank(dbWebHook.getUrl())) {
                    updateWebHook(dbWebHook);
                }

                LOG.info("Complete transaction Web Hook");

                if (dbWebHook != null && dbWebHook.getWebHookUrls().size() > 0) {
                    final String jsonTransaction = JsonSerializer.toJson(transactionData);
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
                    List<WebHookLog> logList = new ArrayList<>();
                    for (String webHookUrl : dbWebHook.getWebHookUrls()) {
                        LOG.info("Webhook url : " + webHookUrl + "\n Order data : " + jsonTransaction);
                        try {
                            SimpleRestUtil.postWithSsl(webHookUrl, jsonTransaction, PartnerWebHookResponse.class, headers);

                        } catch (MessageBodyProviderNotFoundException ex) {
                            LOG.info("Web Hook completed but got no response error(can be ignored) for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, dbWebHook, "Web Hook completed but got no response error(can be ignored) because of: " + ex.getMessage()));
                        } catch (Exception e) {
                            LOG.error("Complete Transaction Web Hook failed for shop: " + shopId + ", companyId: " + companyId + " url: " + webHookUrl, e);
                            logList.add(createWebHookErrorLog(companyId, shopId, webHookUrl, dbWebHook, "Complete Transaction Web Hook failed because of: " + e.getMessage()));
                        }
                    }

                    if (!logList.isEmpty()) {
                        webHookLogRepository.save(logList);
                    }
                }
            }
        });
    }

    public PartnerWebHook updateWebHook(PartnerWebHook partnerWebHook) {
        List<String> urls = new ArrayList<>();
        if (partnerWebHook.getWebHookUrls() == null || partnerWebHook.getWebHookUrls().size() == 0) {
            urls.add(partnerWebHook.getUrl());
            partnerWebHook.setWebHookUrls(urls);
        }
        return partnerWebHook;
    }

    public void updateProductWebHook(final String companyId, final String shopId, final ProductData product, final PartnerWebHook.PartnerWebHookType webHookType) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {

                final PartnerWebHook webHook = partnerWebHookRepository.getByWebHookType(companyId, shopId, webHookType);

                if (webHook != null && StringUtils.isNotBlank(webHook.getUrl())) {
                    updateWebHook(webHook);
                }
                LOG.info("Update product Web Hook");

                if (webHook != null && !webHook.getWebHookUrls().isEmpty()) {
                    final String requestJson = JsonSerializer.toJson(product);
                    final MultivaluedMap<String, Object> header = new MultivaluedHashMap<>();
                    header.putSingle("Content-Type", MediaType.APPLICATION_JSON);
                    List<WebHookLog> logList = new ArrayList<>();
                    for (String url : webHook.getWebHookUrls()) {
                        LOG.info("Webhook url : " + url + "\n Order data : " + requestJson);
                        try {
                            SimpleRestUtil.postWithSsl(url, requestJson, PartnerWebHookResponse.class, header);
                        } catch (MessageBodyProviderNotFoundException ex) {
                            LOG.info("Web hook completed but got no response error(can be ignored) for shop: " + shopId + ", company: " + companyId + " url: " + url);
                            logList.add(createWebHookErrorLog(companyId, shopId, url, webHook, "Web Hook completed but got no response error(can be ignored) because of: " + ex.getMessage()));
                        } catch (Exception e) {
                            LOG.error("Product web hook failed for shop: " + shopId + ", company: " + companyId + " url: " + url);
                            logList.add(createWebHookErrorLog(companyId, shopId, url, webHook, "Product web hook failed because of: " + e.getMessage()));
                        }
                    }

                    if (!logList.isEmpty()) {
                        webHookLogRepository.save(logList);
                    }
                }
            }
        });
    }

    /**
     * Private method for listing webhook error logs
     *
     * @param companyId
     * @param shopId
     * @param url
     * @param webHook
     * @param errorMessage
     */
    private WebHookLog createWebHookErrorLog(String companyId, String shopId, String url, PartnerWebHook webHook, String errorMessage) {
        WebHookLog errorLog = new WebHookLog();
        errorLog.prepare(companyId);
        errorLog.setShopId(shopId);
        errorLog.setMessage(errorMessage);
        errorLog.setWebHookId(webHook.getId());
        errorLog.setWebHookType(webHook.getWebHookType());
        errorLog.setUrl(url);

        return errorLog;
    }

    private boolean checkSslURL(String url) {
        String pattern = "^(https)://.*$";
        return url.matches(pattern);
    }

}
