package com.fourtwenty.core.managed;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.FcmConfig;
import com.fourtwenty.core.config.PusherConfig;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.services.notification.FcmService;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.util.ConnectSerializerModule;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import com.mdo.pusher.Pusher;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The PushNotificationManager object.
 * User: mdo
 * Date: 1/8/14
 * Time: 2:52 AM
 */
@Singleton
public class PushNotificationManager implements Managed {
    private static final int MAX_MESSAGE_SIZE = 200; // 200 bytes
    private static final Logger logUtil = LoggerFactory.getLogger(PushNotificationManager.class);
    ApnsService apnsService = null;
    ConnectConfiguration config;
    ExecutorService executorService;
    Pusher pusher;
    private String myIP;
    @Inject
    private FcmService fcmService;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    private FcmConfig fcmConfig;

    public PushNotificationManager() {
        logUtil.info("Initiating Push and Fcm NotificationManager");
    }

    @Inject
    public PushNotificationManager(ConnectConfiguration config) {
        logUtil.info("Initiating PushNotificationManager");
        this.config = config;
        PusherConfig pconfig = config.getPusherConfig();
        pusher = new Pusher(pconfig.getAppId(), pconfig.getAuthKey(), pconfig.getAuthSecret());
        pusher.registerSerializer(new ConnectSerializerModule());
    }

    @Override
    public void start() throws Exception {
        logUtil.info("Starting PushNotificationManager");
        executorService = Executors.newFixedThreadPool(8);

        if (apnsService == null) {
            try {
                String path = config.getApplePushCertPath();
                String password = config.getApplePushCertPassword();
                String name = config.getApplePushCertName();

                if (StringUtils.isNotEmpty(name)) {
                    URL url = PushNotificationManager.class.getResource(name);
                    path = url.getPath();
                }

                ApnsServiceBuilder builder = APNS.newService()
                        .withCert(path, password);

                if (config.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
                    apnsService = builder.withNoErrorDetection().withSandboxDestination().asBatched().asPool(10)
                            .build();
                } else {
                    apnsService = builder.withNoErrorDetection().withProductionDestination().asBatched().asPool(10)
                            .build();
                }

                logUtil.error("Starting APNS Service...");
            } catch (Exception e) {
                logUtil.error("Error starting apns", e);
            }
        }
        if (fcmConfig == null) {
            logUtil.info("Starting Fcm PushNotificationManager");
            try {
                fcmConfig = integrationSettingRepository.getFcmConfig(IntegrationSetting.Environment.Production);
                logUtil.error("Starting Fcm Service...");
            } catch (Exception e) {
                logUtil.error("Error starting fcm", e);
            }
        }
    }


    @Override
    public void stop() throws Exception {
        logUtil.info("Stopping Push and Fcm NotificationManager");
        executorService.shutdown();
        if (apnsService != null) {
            apnsService.stop();
        }
    }


    private static String getIp() throws Exception {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            String ip = in.readLine();
            return ip;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void sendPusherMessage(final String channelName, final String eventName, final Object data) {
        executorService.execute(new PusherWorker(channelName, eventName, data));
    }

    public class PusherWorker implements Runnable {
        final String channelName;
        final String eventName;
        final Object data;

        public PusherWorker(String channelName, String eventName, Object data) {
            this.channelName = channelName;
            this.eventName = eventName;
            this.data = data;
        }

        @Override
        public void run() {
            pusher.trigger(getChannel(channelName), eventName, data);
        }
    }


    private String getChannel(String shopId) {
        return String.format("%s-%s", config.getPusherConfig().getChannelPrefix(), shopId);
    }

    public void sendPushNotification(final String message,
                                     final RealtimeService.RealtimeEventType eventType,
                                     final Object data,
                                     final List<Terminal> devices,
                                     final int badgeCount,
                                     final FcmPayload.Type notificationType,
                                     final String sourceId,
                                     final String transNo,
                                     final String title,
                                     final FcmPayload.SubType subType) {
        // max payload is 256 bytes
        // truncating alert message down to 200 bytes
        String newMessage = message;
        if (message.length() > MAX_MESSAGE_SIZE) {
            newMessage = message.substring(0, MAX_MESSAGE_SIZE);
            newMessage += "...";
        }
        Runnable worker = new APNSWorker(newMessage, badgeCount, devices, false);
        executorService.execute(worker);

        // fcm Notification
        if (notificationType != FcmPayload.Type.ONLINE_ORDER) {
            Runnable fcmWorker = new FcmWorker(newMessage, devices, badgeCount, false, notificationType, sourceId, transNo, title, subType, null);
            executorService.execute(fcmWorker);
        }
    }

    public void sendPushNotification(final String message,
                                     final List<Terminal> devices,
                                     final int badgeCount,
                                     final FcmPayload.Type notificationType,
                                     final String sourceId,
                                     final String transNo,
                                     final String title,
                                     final FcmPayload.SubType subType,
                                     final Conversation conversation) {
        // max payload is 256 bytes
        // truncating alert message down to 200 bytes
        String newMessage = message;
        if (message.length() > MAX_MESSAGE_SIZE) {
            newMessage = message.substring(0, MAX_MESSAGE_SIZE);
            newMessage += "...";
        }
        Runnable worker = new APNSWorker(newMessage, badgeCount, devices, false);
        executorService.execute(worker);

        // fcm Notification
        if (notificationType != FcmPayload.Type.ONLINE_ORDER) {
            Runnable fcmWorker = new FcmWorker(newMessage, devices, badgeCount, false, notificationType, sourceId, transNo, title, subType, conversation);
            executorService.execute(fcmWorker);
        }
    }

    public void sendRemotePushNotification(final String message, Terminal terminal, FcmPayload.Type notificationType, String sourceId, String title, FcmPayload.SubType subType) {

        Runnable worker = new APNSWorker(message, 0, Lists.newArrayList(terminal), true);
        executorService.execute(worker);
       // Fcm Remote Notification
        Runnable fcmWorker = new FcmWorker(message, Lists.newArrayList(terminal), 0, true, notificationType, sourceId, "", title, subType, null);
        executorService.execute(fcmWorker);
    }

    public <E> void sendPushNotification(final String message,
                                         final List<Terminal> devices,
                                         final int badgeCount,
                                         final FcmPayload.Type notificationType,
                                         final String sourceId,
                                         final String transNo,
                                         final String title,
                                         final FcmPayload.SubType subType) {
        sendPushNotification(message, devices,badgeCount, notificationType, sourceId, transNo, title, subType, null);

    }
    public class APNSWorker implements Runnable {

        private final String message;
        private final List<Terminal> devices;
        private final int badgeCount;
        private final boolean remote;

        public APNSWorker(String message, int badgeCount, List<Terminal> devices, boolean remote) {
            this.message = message;
            this.devices = devices;
            this.badgeCount = badgeCount;
            this.remote = remote;
        }

        @Override
        public void run() {
            processCommand();
        }

        private void processCommand() {
            try {
                String apnsPayload = null;

                if (remote) {
                    apnsPayload = APNS.newPayload()
                            .alertBody(message)
                            .forNewsstand()
                            .build();
                } else {
                    apnsPayload = APNS.newPayload()
                            .alertBody(message)
                            .sound("default")
                            .badge(badgeCount)
                            .build();
                }

                for (Terminal device : devices) {
                    Terminal.DeviceDetail deviceDetail = null;
                    if (CollectionUtils.isNotEmpty(device.getDeviceDetails())) {
                        deviceDetail = device.getDeviceDetails().stream().filter(detail -> detail.getAppType() == ConnectAuthToken.ConnectAppType.Blaze).findFirst().orElse(null);
                    }
                    String token = (deviceDetail == null) ? device.getDeviceToken() : deviceDetail.getDeviceToken();
                    String name = (deviceDetail == null) ? device.getDeviceName() : deviceDetail.getDeviceName();
                    String deviceType = (deviceDetail == null) ? device.getDeviceType() : deviceDetail.getDeviceType().name();

                    if (token != null) {
                        if (deviceType != null && deviceType.equalsIgnoreCase("iOS")) {
                            logUtil.info("APNS Payload: " + apnsPayload);
                            ApnsNotification result = apnsService.push(token, apnsPayload);
                            if (result != null) {
                                logUtil.info(String.format("iOS message sent to: %s -- name: %s, identifier: %s", token, name, result.getIdentifier()));
                            } else {
                                logUtil.info(String.format("FAILED: iOS message sent to: %s -- name: %s", token, name));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logUtil.error("Error sending push notificaton", e);
            } finally {
                //apnsService.stop();
            }
        }
    }

    public class FcmWorker implements Runnable {
        private final String message;
        private final List<Terminal> devices;
        private final FcmPayload.Type notificationType;
        private String sourceId;
        private String transNo;
        private String title;
        private FcmPayload.SubType subType;
        private Conversation conversation;

        public FcmWorker(String message, List<Terminal> devices, int badgeCount, boolean remote, FcmPayload.Type notificationType, String sourceId, String transNo, String title, FcmPayload.SubType subType, Conversation conversation) {
            this.message = message;
            this.devices = devices;
            this.notificationType = notificationType;
            this.sourceId = sourceId;
            this.transNo = transNo;
            this.title = title;
            this.subType = subType;
            this.conversation = conversation;
        }


        @Override
        public void run() {
            processCommand();
        }

        private void processCommand() {
            try {

                if (fcmConfig == null) {
                    return;
                }
                for (Terminal device : devices) {
                    Terminal.DeviceDetail deviceDetail = null;
                    if (CollectionUtils.isNotEmpty(device.getDeviceDetails())) {
                        deviceDetail = device.getDeviceDetails().stream().filter(Terminal.DeviceDetail::isActive).findFirst().orElse(null);
                    }
                    String token = (deviceDetail == null) ? device.getDeviceToken() : deviceDetail.getDeviceToken();
                    String name = (deviceDetail == null) ? device.getDeviceName() : deviceDetail.getDeviceName();
                    logUtil.info(String.format("Fcm Message token %s  and deviceName : %s", token, device.getDeviceName()));
                    if (StringUtils.isNotBlank(token)) {
                        String publish = fcmService.send(fcmConfig, new FcmPayload(message, sourceId, notificationType, token, transNo, title, subType, conversation));
                        if (publish != null) {
                            logUtil.info(String.format("Fcm Message sent to: %s -- name: %s, notificationType : %s, identifier: %s,Transaction:%s, Transaction Number : %s, Title :%s, SubType:%s, conversation: %s", token, name, notificationType, publish, sourceId, transNo, title, subType, conversation));
                        } else {
                            logUtil.info(String.format("FAILED: Message sent to: %s -- name: %s, notificationType : %s, Transaction:%s, Transaction Number :%s, Title :%s, SubType:%s, conversation : %s", token, name, notificationType, sourceId, transNo, title, subType, conversation));
                        }
                    }
                }
            } catch (Exception e) {
                logUtil.error("Error sending fcm push notification", e);
            }
        }


    }

}
