package com.fourtwenty.core.managed;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.models.reportrequest.ReportRequest;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ReportTrackRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.GathererManager;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.FormatProcessor;
import com.fourtwenty.core.security.tokens.AssetAccessToken;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.services.reportrequest.ReportRequestService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Injector;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

@Singleton
public class ReportManager implements Managed {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportManager.class);

    private static final String REPORT_HTML_RESOURCE = "/reportRequest.html";

    private ExecutorService executorService;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ReportRequestService reportRequestService;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private SecurityUtil securityUtil;
    @Inject
    private ConnectConfiguration connectConfiguration;
    @Inject
    private FormatProcessor formatProcessor;
    @Inject
    private ReportTrackRepository reportTrackRepository;
    @Inject
    Injector injector;
    @Inject
    private CompanyAssetRepository assetRepository;

    @Override
    public void start() throws Exception {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public void generateReport(ReportFilter filter, GathererReport report, ReportType type) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ReportRequest reportRequest = reportRequestService.addReportRequest(filter.getCompanyId(), filter.getShopId(), filter.getCurrentEmployeeId(), filter.getType(), null, ReportRequest.RequestStatus.InProgress, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
                Employee employee = employeeRepository.get(filter.getCompanyId(), filter.getCurrentEmployeeId());

                Report reportData = formatProcessor.process(FormatProcessor.ReportFormat.CSV, report);

                if (reportData != null) {
                    addOrUpdateTrackCount(type, filter);
                }
                if (employee != null && StringUtils.isNotBlank(employee.getEmail()) && report != null) {
                    LOGGER.info(String.format("%s report has been requested by employee : %s", filter.getType(), employee.getFirstName()));
                    postReportProcess((InputStream) reportData.getData(), filter, employee, reportRequest.getId());
                }
            }
        });
    }

    /**
     * Private method to process after report generation to send email and generate report request
     *
     * @param reportData : report date
     * @param filter     : report filter
     * @param employee   : employee
     * @param fileName   : fileName
     * @param extension  : file extension
     */
    private void postReportProcess(StringBuilder reportData, ReportFilter filter, Employee employee, String fileName, String extension, String requestId) {
        byte[] bytes = reportData.toString().getBytes();
        InputStream stream = new ByteArrayInputStream(bytes);

        postReportProcess(stream, filter, employee, requestId);
    }

    private String getActionUrl(String keyName, String fileName, String companyId, String shopId, String employeeId) {
        String assetToken = getNewEncryptedAssetToken(companyId, shopId, employeeId);
        try {
            assetToken = URLEncoder.encode(assetToken, "UTF-8");
        } catch (Exception e) {

        }
        String url = String.format("%s/api/v1/mgmt/assets/%s?reportRequest=true&reportName=%s&assetToken=%s", connectConfiguration.getAppApiUrl(), keyName, fileName, assetToken);
        return url;
    }

    private String getNewEncryptedAssetToken(String companyId, String shopId, String userId) {
        AssetAccessToken token = new AssetAccessToken();
        token.setCompanyId(companyId);
        token.setShopId(shopId);
        token.setUserId(userId);
        token.setSignedDate(DateTime.now().getMillis());

        return securityUtil.createAssetAccessToken(token);
    }

    private void postReportProcess(InputStream stream, ReportFilter filter, Employee employee, String requestId) {
        ReportRequest reportRequest = reportRequestService.generateReportRequest(filter.getCompanyId(), filter.getShopId(), filter.getCurrentEmployeeId(), requestId, stream, filter.getType().name(), ".csv");

        if (reportRequest == null || reportRequest.getAsset() == null) {
            LOGGER.info("Error! in generating report request");
        }

        StringWriter writer = new StringWriter();
        try {
            InputStream inputStream = ShopServiceImpl.class.getResourceAsStream(REPORT_HTML_RESOURCE);
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in report email", ex);
            return;
        }

        CompanyAsset asset = reportRequest.getAsset();
        StringBuilder employeeName = new StringBuilder();
        employeeName.append(employee.getFirstName().trim())
                .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName().trim() : "");

        String url = getActionUrl(asset.getKey(), asset.getName(), reportRequest.getCompanyId(), reportRequest.getShopId(), employee.getId());
        String emailBody = writer.toString();
        emailBody = emailBody.replaceAll(Pattern.quote("{{name}}"), employeeName.toString());
        emailBody = emailBody.replaceAll(Pattern.quote("{{action_url}}"), url);
        emailBody = emailBody.replaceAll(Pattern.quote("{{reportName}}"), filter.getType().name().toLowerCase().replaceAll("_", " "));

        String subject = StringUtils.capitalize((filter.getType().name().toLowerCase().replaceAll("_", " ")).concat(" report"));

        amazonServiceManager.sendMultiPartEmail("support@blaze.me", employee.getEmail(), subject, emailBody, null, null, null, "Content-Disposition", "", "");
    }

    /**
     * This method gather data from gatherer and generate report
     * @param type : report type {@link ReportType}
     * @param filter : report filter {@link ReportFilter}
     */
    public void generateReport(ReportType type, ReportFilter filter) {
        GathererReport report = this.getReport(type, filter);

        if (report != null) {
            generateReport(filter, report, type);
        }
    }

    private void addOrUpdateTrackCount(ReportType type, ReportFilter filter) {
        ReportTrack reportTrack = reportTrackRepository.getReportTrackByType(filter.getCompanyId(), filter.getShopId(), type);

        if (reportTrack == null) {
            reportTrack = new ReportTrack();

            reportTrack.prepare(filter.getCompanyId());
            reportTrack.setShopId(filter.getShopId());
            reportTrack.setCount(0);
        }

        reportTrack.setReportType(type);
        reportTrack.setCount(reportTrack.getCount() + 1);

        reportTrackRepository.upsert(reportTrack.getId(), reportTrack);
    }

    public GathererReport getReport(ReportType type, ReportFilter filter) {
        GathererManager gathererManager = injector.getInstance(GathererManager.class);
        Gatherer gatherer = gathererManager.getGathererForReportType(type);
        return gatherer.gather(filter);
    }

    public void uploadSendBulkOrderMail(String companyId, String shopId, Employee employee, CompanyAsset asset) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                String name = "Bulk Dispatch Orders";
                LOGGER.info(String.format("%s has been requested by employee : %s", name, employee.getFirstName()));
                StringWriter writer = new StringWriter();
                try {
                    IOUtils.copy(ShopServiceImpl.class.getResourceAsStream(REPORT_HTML_RESOURCE), writer, "UTF-8");
                } catch (IOException ex) {
                    LOGGER.error("Error! in report email", ex);
                    return;
                }

                StringBuilder employeeName = new StringBuilder();
                employeeName.append(employee.getFirstName().trim())
                        .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName().trim() : "");

                String url = getActionUrl(asset.getKey(), asset.getName(), companyId, shopId, employee.getId());
                String emailBody = writer.toString();
                emailBody = emailBody.replaceAll(Pattern.quote("{{name}}"), employeeName.toString());
                emailBody = emailBody.replaceAll(Pattern.quote("{{action_url}}"), url);
                emailBody = emailBody.replaceAll(Pattern.quote("{{reportName}}"), name);

                String subject = StringUtils.capitalize(name);

                amazonServiceManager.sendMultiPartEmail("support@blaze.me", employee.getEmail(), subject, emailBody, null, null, null, "Content-Disposition", "", "");
            }
        });
    }
}

