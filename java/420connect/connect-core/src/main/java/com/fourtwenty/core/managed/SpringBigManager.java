package com.fourtwenty.core.managed;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.thirdparty.springbig.models.*;
import com.fourtwenty.core.thirdparty.springbig.services.SpringBigAPIService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class SpringBigManager implements Managed {

    private static final Logger LOG = LoggerFactory.getLogger(SpringBigManager.class);
    private static final String POS_TYPE = "BLAZE";

    private SpringBigAPIService springBigAPIService;
    private ProductBatchRepository productBatchRepository;
    private ProductRepository productRepository;
    private BrandRepository brandRepository;

    @Inject
    public SpringBigManager(SpringBigAPIService springBigAPIService, ProductBatchRepository productBatchRepository, ProductRepository productRepository, BrandRepository brandRepository) {
        this.springBigAPIService = springBigAPIService;
        this.productBatchRepository = productBatchRepository;
        this.productRepository = productRepository;
        this.brandRepository = brandRepository;
    }

    ExecutorService executorService;

    @Override
    public void start() throws Exception {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    /**
     * This method initiate creation of visit at springbig
     *
     * @param transaction : transaction's object
     * @param member      : member object
     * @param shop        : Shop object
     * @implNote : When a sale is made then we need perform these steps
     * 1. Check if member have a springbig account using member's phone number
     * 2. If member does not exist then create a springbig member
     * 3. If member is created then create sale
     */
    public void createVisit(final SpringBigInfo springBigInfo, Transaction transaction, Member member, Shop shop) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    if (member == null || transaction == null) {
                        return;
                    }

                    //Check if member have a springbig account
                    SpringBigMemberRequest springBigMemberRequest = null;
                    SpringMemberResponse springBigMember = null;

                    springBigMember = springBigAPIService.getMemberByByMemberId(springBigInfo, member.getId());

                    if (springBigMember == null && StringUtils.isNotBlank(member.getPrimaryPhone())) {
                        String cpn = getMemberPhoneNumber(member.getPrimaryPhone());
                        springBigMember = springBigAPIService.getMemberByPhoneNumber(springBigInfo, cpn);
                    }

                    SpringBigMemberUpdateRequest memberUpdateRequest = null;
                    //If member found then check if it have pos_user id or not, if not then update member accordingly
                    if (springBigMember != null
                            && springBigMember.getMember() != null
                            && StringUtils.isNotBlank(member.getPrimaryPhone()) && !member.getPrimaryPhone().equalsIgnoreCase(springBigMember.getMember().getPhoneNumber())) {

                        //TODO: ONLY UPDATE IF springBigMember info is older than member's info
                        memberUpdateRequest = prepareUpdateMemberData(member, shop);

                        SpringMemberResponse updatedMember = springBigAPIService.updateMember(springBigInfo, memberUpdateRequest, member.getId());
                        if (updatedMember != null) {
                            springBigMember = updatedMember;
                        }
                    }

                    //If member not found then create new member at springbig
                    if (springBigMember == null) {
                        springBigMemberRequest = prepareMemberData(member, shop);

                        springBigMember = springBigAPIService.createMember(springBigInfo, springBigMemberRequest);
                    }

                    //If member is available then create visit of member at springbig
                    if (springBigMember != null
                            && springBigMember.getMember() != null
                            && StringUtils.isNotBlank(springBigMember.getMember().getPosUser())) {
                        SpringBigVisitRequest visitData = prepareVisitData(transaction, shop);

                        if (visitData != null) {
                            SpringBigVisitResponse visit = springBigAPIService.createVisit(springBigInfo, visitData);

                            if (visit != null) {
                                LOG.info("Springbig visit has been added successfully for member {} and transaction number {}", member.getId(), transaction.getTransNo());
                            } else {
                                LOG.info("Springbig visit could not be created for member {} and transaction number {}", member.getId(), transaction.getTransNo());
                            }
                        }
                    } else {
                        LOG.error("Springbig visit could not be created because member was not registered for transaction number : {} ", transaction.getTransNo());
                    }

                } catch (Exception e) {
                    LOG.error("Error with springbig");
                }
            }
        });
    }

    private static String getMemberPhoneNumber(String primaryPhone) {
        if (StringUtils.isNotBlank(primaryPhone)) {
            String clean = primaryPhone.replaceAll("[^0-9]", "");
            if (clean.length() >= 11) {
                // there are 11 digits
                if (clean.startsWith("1")) {
                    return clean.substring(1);
                }
            }
            return clean;
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * Prepare member's data for springbig
     *
     * @param member : Platform's member object
     * @param shop
     */
    private SpringBigMemberRequest prepareMemberData(Member member, Shop shop) {
        SpringBigMemberRequest memberRequest = new SpringBigMemberRequest();
        SpringBigMember bigMember = new SpringBigMember();
        memberRequest.setMember(bigMember);

        bigMember.setFirstName(member.getFirstName());
        bigMember.setLastName(member.getLastName());
        bigMember.setPosUser(member.getId());

        String phoneNumber = this.getMemberPhoneNumber(member.getPrimaryPhone());
        bigMember.setPhoneNumber(phoneNumber);
        bigMember.setEmail(member.getEmail());

        Address address = member.getAddress();
        if (address != null) {
            bigMember.setAddress1(TextUtil.textOrEmpty(address.getAddress()));
            bigMember.setCity(TextUtil.textOrEmpty(address.getCity()));
            bigMember.setZip(TextUtil.textOrEmpty(address.getZipCode()));
            bigMember.setRegion(TextUtil.textOrEmpty(address.getState()));
            bigMember.setStateId(TextUtil.textOrEmpty(address.getState()));
            bigMember.setAddress2("");


        }

        bigMember.setCreated(DateUtil.toDateFormatted(member.getCreated(), shop.getTimeZone(), "yyyy-MM-dd HH:mm:ss"));
        bigMember.setPosType(POS_TYPE);

        return memberRequest;
    }


    /**
     * Prepare information for creating member's visit at springbig
     *
     * @param transaction : transaction object
     * @param shop        : Shop object
     */
    private SpringBigVisitRequest prepareVisitData(Transaction transaction, Shop shop) {
        SpringBigVisitRequest visitRequest = null;
        int factor = 1;

        Cart cart = transaction.getCart();
        if (cart != null) {
            if (Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Retail == cart.getRefundOption()) {
                factor = -1;
            }
            visitRequest = new SpringBigVisitRequest();

            VisitRequest visit = new VisitRequest();
            visitRequest.setVisit(visit);

            visit.setPosId(transaction.getId());
            visit.setPosUser(transaction.getMemberId());
            visit.setTransactionTotal(new BigDecimal(cart.getTotal().doubleValue() * factor));
            visit.setPosType(POS_TYPE);
            visit.setSendNotification(Boolean.TRUE.toString());
            visit.setLocation(shop.getId());

            String processedDate = DateUtil.toDateFormattedURC(transaction.getProcessedTime(), "yyyy-MM-dd'T'HH:mm:ss.SSS"); //DateUtil.toDateFormatted(transaction.getProcessedTime(), shop.getTimeZone(), "yyyyMMdd");

            visit.setTransactionDate(processedDate);

            List<VisitDetail> springBigVisitDetails = new ArrayList<>();

            List<ObjectId> batchIds = new ArrayList<>();
            List<ObjectId> productIds = new ArrayList<>();
            int totalFinalCost = 0;
            for (OrderItem orderItem : cart.getItems()) {
                if (StringUtils.isNotBlank(orderItem.getProductId()) && ObjectId.isValid(orderItem.getProductId())) {
                    productIds.add(new ObjectId(orderItem.getProductId()));
                }
                if (StringUtils.isNotBlank(orderItem.getBatchId()) && ObjectId.isValid(orderItem.getBatchId())) {
                    batchIds.add(new ObjectId(orderItem.getBatchId()));
                }
                if (orderItem.getStatus() != OrderItem.OrderItemStatus.Refunded) {
                    totalFinalCost += (int) (orderItem.getFinalPrice().doubleValue() * 100);
                }
            }

            HashMap<String, Product> productMap = productRepository.listAsMap(transaction.getCompanyId(), productIds);
            HashMap<String, ProductBatch> batchMap = productBatchRepository.listAsMap(transaction.getCompanyId(), batchIds);

            List<ObjectId> brandIds = new ArrayList<>();
            for (Map.Entry<String, Product> entry : productMap.entrySet()) {
                Product product = entry.getValue();
                if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                    brandIds.add(new ObjectId(product.getBrandId()));
                }
            }

            HashMap<String, Brand> brandMap = brandRepository.listAsMap(transaction.getCompanyId(), brandIds);

            for (OrderItem orderItem : cart.getItems()) {

                int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                double targetDiscount = cart.getCalcCartDiscount().doubleValue() * ratio;

                VisitDetail visitDetail = new VisitDetail();

                Product product = productMap.get(orderItem.getProductId());
                ProductBatch productBatch = batchMap.get(orderItem.getBatchId());

                Brand brand = null;
                if (product != null && StringUtils.isNotBlank(product.getBrandId())) {
                    brand = brandMap.get(product.getBrandId());
                }

                visitDetail.setSku(productBatch != null ? productBatch.getSku() : "");
                visitDetail.setPrice(new BigDecimal(orderItem.getUnitPrice().doubleValue() * factor));
                visitDetail.setQuantity(orderItem.getQuantity().intValue() * factor);
                visitDetail.setCategory(product != null ? (product.getCategory() != null ? product.getCategory().getName() : "") : "");
                visitDetail.setBrand(brand != null ? brand.getName() : "");
                visitDetail.setName(product != null ? product.getName() : "");
                visitDetail.setDiscount(String.valueOf(targetDiscount));

                springBigVisitDetails.add(visitDetail);
            }

            visit.setVisitDetails(springBigVisitDetails);
        }

        return visitRequest;
    }

    /**
     * This method prepare data of member info that needs to update at springbig
     *
     * @param member : member object
     * @return : Update information
     */
    private SpringBigMemberUpdateRequest prepareUpdateMemberData(Member member, Shop shop) {
        SpringBigMemberUpdateRequest updateRequest = new SpringBigMemberUpdateRequest();

        String cpn = getMemberPhoneNumber(member.getPrimaryPhone());

        MemberUpdateRequest memberUpdateRequest = new MemberUpdateRequest();
        memberUpdateRequest.setPosType(POS_TYPE);
        memberUpdateRequest.setPosUser(member.getId());
        memberUpdateRequest.setPhoneNumber(cpn);

        updateRequest.setMember(memberUpdateRequest);

        return updateRequest;
    }
}
