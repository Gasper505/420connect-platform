package com.fourtwenty.core.managed;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.tookan.TookanTeamRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentDeleteRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.*;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanAgentInfo;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskInfo;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskResult;
import com.fourtwenty.core.thirdparty.tookan.service.TookanApiService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class TookanApiManager implements Managed {

    private static final Logger LOG = LoggerFactory.getLogger(TookanApiManager.class);
    private static final int DEFAULT_DELIVERY_TIME = 120; // 120 minutes = 2 hrs
    private static final int DEFAULT_DELIVERY_TIME_CUTOFF = 120; // 120 minutes = 2 hrs
    private static final String TOOKAN_TEAM = "Tookan Team";
    private static final String TOOKAN_TEAM_NOT_FOUND = "Tookan Team is not found.";
    private static final String TOOKAN_TASK = "Tookan Task";


    ExecutorService executorService;

    private TookanTeamRepository tookanTeamRepository;
    private ShopRepository shopRepository;
    private TookanApiService tookanApiService;
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private EmployeeRepository employeeRepository;
    private MemberRepository memberRepository;

    @Inject
    public TookanApiManager(TookanTeamRepository tookanTeamRepository,
                            ShopRepository shopRepository,
                            TookanApiService tookanApiService,
                            TransactionRepository transactionRepository,
                            ProductRepository productRepository,
                            EmployeeRepository employeeRepository,
                            MemberRepository memberRepository) {
        this.tookanTeamRepository = tookanTeamRepository;
        this.shopRepository = shopRepository;
        this.tookanApiService = tookanApiService;
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.employeeRepository = employeeRepository;
        this.memberRepository = memberRepository;
    }

    @Override
    public void start() throws Exception {
        LOG.debug("Starting Tookan Manager");
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stop Tookan Manager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    /**
     * Method to create tookan task for transaction
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param transaction : transaction
     * @param member      : member details
     * @param employee    : employee
     * @param teamId      : tookan team id
     * @param agentId     : agent id
     */
    public void createTookanTask(String companyId, String shopId, String timeZone, String apiKey, Transaction transaction, Member member, Employee employee, String teamId, String agentId, boolean isReassign) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (transaction == null) {
                    throw new BlazeInvalidArgException("Tookan Task", "Transaction is not found");
                }
                if (isReassign && StringUtils.isNotBlank(transaction.getTookanTaskId())) {
                    boolean isDeleted = tookanApiService.deleteTookanTask(companyId, shopId, apiKey, transaction.getTookanTaskId());
                    if (isDeleted) {

                        LOG.info("Task deleted successfully from tookan Task Id:" + transaction.getTookanTaskId());
                    }

                }
                Shop shop = shopRepository.getById(shopId);
                if (shop == null) {
                    throw new BlazeInvalidArgException("Shop", "Shop is not found");
                }
                boolean isPickup = transaction.getQueueType() != Transaction.QueueType.Delivery;


                if (isPickup) {
                    if (shop.getAddress() == null || StringUtils.isBlank(shop.getAddress().getAddressString())) {
                        throw new BlazeInvalidArgException("Shop", "Shop address is not found");
                    }
                }

                TookanTeams tookanTeams = tookanTeamRepository.getTeamByShop(companyId, shopId);
                if (tookanTeams == null || tookanTeams.getTookanTeamInfo() == null || tookanTeams.getTookanTeamInfo().isEmpty()) {
                    throw new BlazeInvalidArgException(TOOKAN_TEAM, TOOKAN_TEAM_NOT_FOUND);
                }

                TookanTeamInfo teamInfo = tookanApiService.getTookanTeamInfo(companyId, shopId, tookanTeams, teamId);

                EmployeeTookanInfo employeeTookanInfo = tookanApiService.getEmployeeTookanInfo(companyId, employee, shopId);

                if (teamInfo == null) {
                    tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                            (isPickup ? TookanErrorLog.SubErrorType.PickUp_Task : TookanErrorLog.SubErrorType.Delivery_Task),
                            "Error in task creation : team :" + teamId + " is not found."
                            , 400,
                            apiKey, "Error in task creation : team :" + teamId + " is not found.", transaction.getId());

                    LOG.error("Error in task creation : team :" + teamId + " is not found.");
                    return;
                }

                if (employeeTookanInfo == null) {
                    tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                            (isPickup ? TookanErrorLog.SubErrorType.PickUp_Task : TookanErrorLog.SubErrorType.Delivery_Task),
                            "Error in task creation: team :" + teamId + " agent:" + agentId + " is not found"
                            , 400,
                            apiKey, "Error in task creation: team :" + teamId + " agent:" + agentId + " is not found", transaction.getId());

                    LOG.error("Error in task creation: team :" + teamId + " agent:" + agentId + " is not found");
                    return;
                }

                boolean isAgentAvailable = verifyTookanAgent(companyId, shopId, apiKey, employeeTookanInfo);
                if (!isAgentAvailable) {
                    tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                            (isPickup ? TookanErrorLog.SubErrorType.PickUp_Task : TookanErrorLog.SubErrorType.Delivery_Task),
                            "Error in task creation: team :" + teamId + " agent:" + agentId + " agent is not available at tookan"
                            , 400,
                            apiKey, "Error in task creation: team :" + teamId + " agent:" + agentId + " is not available", transaction.getId());

                    LOG.error("Error in task creation: team :" + teamId + " agent:" + agentId + " is not available");
                    return;
                }
                String requestBody;
                if (isPickup) {
                    requestBody = createPickUpTask(timeZone, transaction, shop, apiKey, teamInfo, employeeTookanInfo);

                } else {
                    requestBody = createDeliveryTask(timeZone, transaction, shop, apiKey, member, teamInfo, employeeTookanInfo);
                }

                TookanTaskInfo taskInfo = tookanApiService.createTask(companyId, shopId, apiKey, isPickup, requestBody);
                if (taskInfo != null) {
                    transaction.setTookanTaskId(taskInfo.getJobId().toString());
                    transactionRepository.updateTookanInfo(companyId, transaction.getId(), String.valueOf(taskInfo.getJobId()));
                }
            }
        });
    }

    /**
     * Private method to create pick up task request
     *
     * @param transaction        : transaction for which task to be created
     * @param shop               : shop
     * @param apiKey             : tookan account api Key
     * @param teamInfo           : team id
     * @param employeeTookanInfo : agent id
     * @return : return task request body
     */

    private String createPickUpTask(String timeZone, Transaction transaction, Shop shop, String apiKey, TookanTeamInfo teamInfo, EmployeeTookanInfo employeeTookanInfo) {
        long pickupDate = (transaction.getPickUpDate() == null || transaction.getPickUpDate() == 0) ? DateTime.now().plusHours(6).getMillis() : transaction.getPickUpDate();
        PickUpTaskRequest request = new PickUpTaskRequest();
        request.setFleetId((employeeTookanInfo != null ? employeeTookanInfo.getTookanAgentId() : ""));
        request.setHasDelivery(0L);
        request.setJobPickupAddress(getAddressString(shop.getAddress()));
        request.setJobPickupEmail(shop.getEmailAdress());
        request.setJobPickupName(shop.getName());
        request.setTeamId(String.valueOf((teamInfo != null) ? teamInfo.getTeamId() : ""));
        request.setHasPickup(1L);
        request.setLayoutType(0L);
        request.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
        request.setOrderId(transaction.getId());
        request.setTrackingLink(1L);
        request.setJobPickupDatetime(DateUtil.toDateTimeFormatted(pickupDate, transaction.getTimeZone(), "yyyy-MM-dd HH:00:00"));
        request.setJobPickupPhone(AmazonServiceManager.cleanPhoneNumber(shop.getPhoneNumber(), shop));
        request.setApiKey(apiKey);
        request.setAutoAssigned(true);
        request.setJobDescription(generateTookanTaskDescription(transaction));
        return JsonSerializer.toJson(request);
    }

    /**
     * Private method to create delivery task.
     *
     * @param transaction        : transaction
     * @param shop               : shop
     * @param apiKey             : tookan account api key
     * @param member             : member
     * @param teamInfo           : tookan team info
     * @param employeeTookanInfo : employee tookan info
     * @return : return task request body
     */
    private String createDeliveryTask(String timeZone, Transaction transaction, Shop shop, String apiKey, Member member, TookanTeamInfo teamInfo, EmployeeTookanInfo employeeTookanInfo) {
        if (member == null) {
            throw new BlazeInvalidArgException(TOOKAN_TASK, "Member is not found.");
        }
        Address memberAddress = transaction.getDeliveryAddress();

        if (memberAddress == null || !memberAddress.isValid()) {
            memberAddress = member.getAddress();
        }

        if (memberAddress == null || StringUtils.isBlank(memberAddress.getAddressString())) {
            tookanApiService.addTookanLog(transaction.getCompanyId(), transaction.getShopId(), TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Delivery_Task,
                    "Member address is not found"
                    , 400,
                    apiKey, "Member address is not found", transaction.getId());

            throw new BlazeInvalidArgException(TOOKAN_TASK, "Shop address is not found");
        }
        if (StringUtils.isBlank(member.getPrimaryPhone())) {
            tookanApiService.addTookanLog(transaction.getCompanyId(), transaction.getShopId(), TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Delivery_Task,
                    "Member phone is not found"
                    , 400,
                    apiKey, "Member phone is not found", transaction.getId());

            throw new BlazeInvalidArgException(TOOKAN_TASK, "Member phone is not found");
        }
        StringBuilder memberName = new StringBuilder();
        memberName.append(member.getFirstName())
                .append((StringUtils.isNotBlank(member.getMiddleName()) ? " " + member.getMiddleName() : " "))
                .append((StringUtils.isNotBlank(member.getLastName()) ? " " + member.getLastName() : " "));
        int eta = transaction.getEta();
        if (eta <= DEFAULT_DELIVERY_TIME_CUTOFF) {
            eta = DEFAULT_DELIVERY_TIME; // 2 hrs
        }
        long deliveryDate = (transaction.getDeliveryDate() == 0) ? DateTime.now().plusMinutes(eta).getMillis() : transaction.getDeliveryDate();

        DeliveryTaskRequest request = new DeliveryTaskRequest();
        request.setFleetId(employeeTookanInfo != null ? employeeTookanInfo.getTookanAgentId() : "");
        request.setHasDelivery(1L);
        request.setCustomerAddress(getAddressString(memberAddress));
        request.setCustomerEmail(member.getEmail());
        request.setCustomerPhone(AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop));
        request.setCustomerUsername(memberName.toString());
        request.setTeamId(String.valueOf((teamInfo != null ? teamInfo.getTeamId() : "")));
        request.setHasPickup(0L);
        request.setLayoutType(0L);
        request.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
        request.setOrderId(transaction.getId());
        request.setTrackingLink(1L);
        request.setJobDeliveryDatetime(DateUtil.toDateTimeFormatted(deliveryDate, timeZone, "yyyy-MM-dd HH:00:00"));
        request.setApiKey(apiKey);
        request.setAutoAssigned(true);
        request.setJobDescription(generateTookanTaskDescription(transaction));
        return JsonSerializer.toJson(request);
    }

    /**
     * Private method to generate custom fields for tookan task.
     *
     * @param dbTrans : transaction
     * @return : return custom field array
     */
    private List<TookanCustomFields> generateCustomFields(Transaction dbTrans) {

        List<TookanCustomFields> customFieldsList = new ArrayList<>();

        StringBuilder orders = new StringBuilder();
        orders.append("-------------------\n\n");

        // Get the products
        List<ObjectId> productIds = new ArrayList<>();
        for (OrderItem o : dbTrans.getCart().getItems()) {
            productIds.add(new ObjectId(o.getProductId()));
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(dbTrans.getCompanyId(), productIds);
        for (OrderItem o : dbTrans.getCart().getItems()) {
            Product p = productHashMap.get(o.getProductId());
            if (p != null) {
                orders.append(String.format("%f x %s\n", o.getQuantity(), p.getName()));
            }
        }
        orders.append("-------------------\n");
        customFieldsList.add(new TookanCustomFields("Order_Detail", orders.toString()));

        if (dbTrans.getCart().getPaymentOption() != Cart.PaymentOption.None) {
            customFieldsList.add(new TookanCustomFields("Payment_Type", dbTrans.getCart().getPaymentOption().toString()));
        }

        customFieldsList.add(new TookanCustomFields("Total", TextUtil.toCurrency(dbTrans.getCart().getTotal().doubleValue(), null)));

        return customFieldsList;
    }

    /**
     * Private method to generate description for tookan task.
     *
     * @param dbTrans : transaction
     * @return : return task description
     */
    private String generateTookanTaskDescription(Transaction dbTrans) {
        StringBuilder orders = new StringBuilder();
        orders.append("-------------------\n\n");
        if (dbTrans.getCart() != null && dbTrans.getCart().getItems() != null && !dbTrans.getCart().getItems().isEmpty()) {
            orders.append("Transaction Details:\n");
            orders.append(String.format("Transaction #: %s\n", dbTrans.getTransNo()));

            // Get the products
            List<ObjectId> productIds = new ArrayList<>();
            for (OrderItem o : dbTrans.getCart().getItems()) {
                productIds.add(new ObjectId(o.getProductId()));
            }

            HashMap<String, Product> productHashMap = productRepository.listAsMap(dbTrans.getCompanyId(), productIds);
            for (OrderItem o : dbTrans.getCart().getItems()) {
                Product p = productHashMap.get(o.getProductId());
                if (p != null) {
                    orders.append(String.format("%.2f x %s\n", o.getQuantity(), p.getName()));
                }
            }

            if (dbTrans.getCart().getPaymentOption() != Cart.PaymentOption.None) {
                orders.append(String.format("Payment Option: %s\n", dbTrans.getCart().getPaymentOption()));
            }
            orders.append(String.format("Total: %s\n", TextUtil.toCurrency(dbTrans.getCart().getTotal().doubleValue(), null)));
        }
        if (StringUtils.isNotBlank(dbTrans.getMemo())) {
            orders.append(String.format("Note: %s\n", dbTrans.getMemo()));
        }
        orders.append("\n-------------------\n");

        return orders.toString();
    }

    /**
     * This method updates task and task status for tookan task
     *
     * @param companyId   : company id
     * @param shopId      : shop id
     * @param apiKey      : api key
     * @param timeZone    : timezone
     * @param transaction : transaction
     * @param status      : status
     */
    public void updateTaskStatus(String companyId, String shopId, String apiKey, String timeZone, Transaction transaction, TookanTaskResult.TookanTaskStatus status) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (transaction == null) {
                    throw new BlazeInvalidArgException("Tookan Task", "Transaction is not found");
                }

                Shop shop = shopRepository.getById(shopId);
                if (shop == null) {
                    throw new BlazeInvalidArgException("Shop", "Shop is not found");
                }
                DeleteTaskRequest getRequest = new DeleteTaskRequest();
                getRequest.setJobId(transaction.getTookanTaskId());
                getRequest.setApiKey(apiKey);
                TookanTaskInfo dbTaskInfo = tookanApiService.getTaskDetails(companyId, shopId, apiKey, getRequest);
                if (dbTaskInfo != null) {
                    if (status == TookanTaskResult.TookanTaskStatus.SUCCESSFUL || status == null) {
                        updateTookanTask(companyId, shopId, apiKey, timeZone, transaction, status, dbTaskInfo);
                    }
                    if (status != null) {
                        TookanTaskStatusRequest request = new TookanTaskStatusRequest();
                        request.setApiKey(apiKey);
                        request.setJobId(transaction.getTookanTaskId());
                        request.setJobStatus(String.valueOf(status.code));
                        tookanApiService.updateTaskStatus(companyId, shopId, apiKey, request);
                    }
                }

            }
        });
    }


    /**
     * This method updates task and task status for tookan task
     *
     * @param companyId   : company id
     * @param shopId      : shop id
     * @param apiKey      : api key
     * @param timeZone    : timezone
     * @param transaction : transaction
     * @param status      : status
     * @param taskInfo    : tookan task info
     */
    private void updateTookanTask(String companyId, String shopId, String apiKey, String timeZone, Transaction transaction, TookanTaskResult.TookanTaskStatus status, TookanTaskInfo taskInfo) {
        if (transaction == null) {
            throw new BlazeInvalidArgException("Tookan Task", "Transaction is not found");
        }

        Shop shop = shopRepository.getById(shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found");
        }

        boolean isPickup = transaction.getQueueType() == Transaction.QueueType.WalkIn;

        if (shop.getAddress() == null || StringUtils.isBlank(shop.getAddress().getAddressString())) {
            tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Update_Task,
                    "Error in task update task : shop is not found."
                    , 400,
                    apiKey, "Error in task update task : shop is not found.", transaction.getId());

            LOG.error("Error in task task update : shop is not found.");
            throw new BlazeInvalidArgException("Shop", "Shop address is not found");
        }

        Employee employee = employeeRepository.getById(transaction.getAssignedEmployeeId());
        if (employee == null) {
            tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Update_Task,
                    "Error in task update task : employee is not found.",
                    400,
                    apiKey, "Error in task update task : employee is not found.", transaction.getId());

            LOG.error("Error in task task update : employee is not found.");
            throw new BlazeInvalidArgException("Employee", "Employee is not found");
        }

        Member member = memberRepository.getById(transaction.getMemberId());
        if (member == null) {
            tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Update_Task,
                    "Error in task update task : member is not found."
                    , 400,
                    apiKey, "Error in task update task : member is not found.", transaction.getId());

            LOG.error("Error in task task update : member is not found.");
            throw new BlazeInvalidArgException("Member", "Member is not found");
        }

        TookanTeams tookanTeams = tookanTeamRepository.getTeamByShop(companyId, shopId);
        if (tookanTeams == null || tookanTeams.getTookanTeamInfo() == null || tookanTeams.getTookanTeamInfo().isEmpty()) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, TOOKAN_TEAM_NOT_FOUND);
        }

        TookanTeamInfo teamInfo = tookanApiService.getTookanTeamInfo(companyId, shopId, tookanTeams, String.valueOf(taskInfo.getTeamId()), true);

        EmployeeTookanInfo employeeTookanInfo = tookanApiService.getEmployeeTookanInfo(companyId, employee, shopId);

        if (teamInfo == null) {
            tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Update_Task,
                    "Error in task update task : team :" + taskInfo.getTeamId() + " is not found."
                    , 400,
                    apiKey, "Error in task update task : team :" + taskInfo.getTeamId() + " is not found.", transaction.getId());

            LOG.error("Error in task creation : team :" + taskInfo.getTeamId() + " is not found.");
            return;
        }

        if (employeeTookanInfo == null) {
            tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                    TookanErrorLog.SubErrorType.Update_Task,
                    "Error in task task update: team :" + taskInfo.getTeamId() + " agent:" + taskInfo.getAgentId() + " is not found"
                    , 400,
                    apiKey, "Error in task task update: team :" + taskInfo.getTeamId() + " agent:" + taskInfo.getAgentId() + " is not found", transaction.getId());

            LOG.error("Error in task task update : team :" + taskInfo.getTeamId() + " agent:" + taskInfo.getAgentId() + " is not found");
            return;
        }

        boolean isAgentAvailable = verifyTookanAgent(companyId, shopId, apiKey, employeeTookanInfo);
        if (!isAgentAvailable) {
            tookanApiService.addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK,
                    (isPickup ? TookanErrorLog.SubErrorType.PickUp_Task : TookanErrorLog.SubErrorType.Delivery_Task),
                    "Error in task update: team :" + taskInfo.getTeamId() + " agent:" + taskInfo.getAgentId() + " is not available"
                    , 400,
                    apiKey, "Error in task update: team :" + taskInfo.getTeamId() + " agent:" + taskInfo.getAgentId() + " is not available", transaction.getId());

            LOG.error("Error in task update: team :" + taskInfo.getTeamId() + " agent:" + taskInfo.getAgentId() + " is not available");
            return;
        }

        String requestBody;
        if (isPickup) {
            requestBody = createPickUpTaskUpdate(timeZone, transaction, shop, apiKey, taskInfo, status);

        } else {
            requestBody = createDeliveryTaskUpdate(timeZone, transaction, shop, apiKey, taskInfo, status);
        }

        tookanApiService.updateTaskDetails(companyId, shopId, apiKey, requestBody);
    }

    /**
     * Private method to create updates request for pickup task
     *
     * @param timeZone    : timezone
     * @param transaction : transaction details
     * @param shop        : shop
     * @param apiKey      : apikey
     * @param taskInfo    : tookan task info
     * @param status      : status
     * @return : return request string
     */

    private String createPickUpTaskUpdate(String timeZone, Transaction transaction, Shop shop, String apiKey, TookanTaskInfo taskInfo, TookanTaskResult.TookanTaskStatus status) {
        PickUpTaskUpdateRequest request = new PickUpTaskUpdateRequest();
        long pickupDate = (transaction.getPickUpDate() == null || transaction.getPickUpDate() == 0 || status != null) ? DateTime.now().plusHours(6).getMillis() : transaction.getPickUpDate();

        request.setFleetId(String.valueOf(taskInfo.getAgentId()));
        request.setHasDelivery(0L);
        request.setJobPickupAddress(taskInfo.getJobPickupAddress());
        request.setJobPickupEmail(taskInfo.getCustomerEmail());
        request.setJobPickupName(taskInfo.getCustomerUserName());
        request.setTeamId(String.valueOf(taskInfo.getTeamId()));
        request.setHasPickup(1L);
        request.setLayoutType(0L);
        request.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
        request.setOrderId(transaction.getId());
        request.setTrackingLink(1L);
        request.setJobPickupDatetime(DateUtil.toDateTimeFormatted(pickupDate, timeZone, "yyyy-MM-dd HH:00:00"));
        request.setJobPickupPhone(AmazonServiceManager.cleanPhoneNumber(shop.getPhoneNumber(), shop));
        request.setApiKey(apiKey);
        request.setAutoAssigned(true);
        request.setJobDescription(generateTookanTaskDescription(transaction));
        request.setJobStatus(taskInfo.getJobStatus());
        request.setJobId(String.valueOf(taskInfo.getJobId()));
        return JsonSerializer.toJson(request);
    }

    /**
     * Private method to get update request body for delivery task
     *
     * @param timeZone    : timezone
     * @param transaction : transaction details
     * @param shop        : shop details
     * @param apiKey      : api key
     * @param taskInfo    : tookan task info
     * @param status      : status
     * @return : return tookan task update request string
     */
    private String createDeliveryTaskUpdate(String timeZone, Transaction transaction, Shop shop, String apiKey, TookanTaskInfo taskInfo, TookanTaskResult.TookanTaskStatus status) {
        int eta = transaction.getEta();
        if (eta <= DEFAULT_DELIVERY_TIME_CUTOFF) {
            eta = DEFAULT_DELIVERY_TIME; // 2 hrs
        }
        long deliveryDate = (transaction.getDeliveryDate() == 0) ? DateTime.now().plusMinutes(eta).getMillis() : transaction.getDeliveryDate();

        DeliveryTaskUpdateRequest request = new DeliveryTaskUpdateRequest();
        request.setFleetId(String.valueOf(taskInfo.getAgentId()));
        request.setHasDelivery(1L);
        request.setCustomerAddress(taskInfo.getJobAddress());
        request.setCustomerEmail(taskInfo.getCustomerEmail());
        request.setCustomerPhone(AmazonServiceManager.cleanPhoneNumber(taskInfo.getCustomerPhone(), shop));
        request.setCustomerUsername(taskInfo.getCustomerUserName());
        request.setTeamId(String.valueOf(taskInfo.getTeamId()));
        request.setHasPickup(0L);
        request.setLayoutType(0L);
        request.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
        request.setOrderId(transaction.getId());
        request.setTrackingLink(1L);
        request.setJobDeliveryDatetime(DateUtil.toDateTimeFormatted(deliveryDate, timeZone, "yyyy-MM-dd HH:00:00"));
        request.setApiKey(apiKey);
        request.setAutoAssigned(true);
        request.setJobDescription(generateTookanTaskDescription(transaction));
        request.setJobStatus(taskInfo.getJobStatus());
        request.setJobId(String.valueOf(taskInfo.getJobId().toString()));
        return JsonSerializer.toJson(request);
    }

    /**
     * Private method to get address string for member or shop address
     *
     * @param address : address object
     * @return : address string
     */
    private String getAddressString(Address address) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(address.getAddress())) {
            sb.append(address.getAddress());
        }
        if (StringUtils.isNotBlank(address.getCity())) {
            sb.append(StringUtils.isBlank(sb) ? "" : ",");
            sb.append(address.getCity());
        }
        if (StringUtils.isNotBlank(address.getState())) {
            sb.append(StringUtils.isBlank(sb) ? "" : ",");
            sb.append(address.getState());
        }
        if (StringUtils.isNotBlank(address.getZipCode())) {
            sb.append(StringUtils.isBlank(sb) ? "" : ",");
            sb.append(address.getZipCode());
        }
        return sb.toString();
    }

    /**
     * Private method to check if agent is deleted at tookan
     *
     * @param companyId          : company id
     * @param shopId             : shop id
     * @param apiKey             : api key
     * @param employeeTookanInfo : employee tookan info
     * @return : true if available else false
     */

    private boolean verifyTookanAgent(String companyId, String shopId, String apiKey, EmployeeTookanInfo employeeTookanInfo) {
        AgentDeleteRequest request = new AgentDeleteRequest();
        request.setApiKey(apiKey);
        request.setFleetId(employeeTookanInfo.getTookanAgentId());
        TookanAgentInfo agentInfo = tookanApiService.getAgentById(companyId, shopId, apiKey, request);
        if (agentInfo != null && agentInfo.getIsDeleted() != null && agentInfo.getIsDeleted() == 0) {
            return true;
        }
        return false;
    }

    public void createTookanTaskWithoutEmployee(String companyId, String shopId, String timeZone, String apiKey, Transaction transaction, Member member, boolean isReassign) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (transaction == null) {
                    throw new BlazeInvalidArgException("Tookan Task", "Transaction is not found");
                }
                if (isReassign && StringUtils.isNotBlank(transaction.getTookanTaskId())) {
                    boolean isDeleted = tookanApiService.deleteTookanTask(companyId, shopId, apiKey, transaction.getTookanTaskId());
                    if (isDeleted) {

                        LOG.info("Task deleted successfully from tookan Task Id:" + transaction.getTookanTaskId());
                    }

                }
                Shop shop = shopRepository.getById(shopId);
                if (shop == null) {
                    throw new BlazeInvalidArgException("Shop", "Shop is not found");
                }
                if (shop.getAddress() == null || StringUtils.isBlank(shop.getAddress().getAddressString())) {
                    throw new BlazeInvalidArgException("Shop", "Shop address is not found");
                }
                boolean isPickup = transaction.getQueueType() == Transaction.QueueType.WalkIn;

                String requestBody;
                if (isPickup) {
                    requestBody = createPickUpTask(timeZone, transaction, shop, apiKey, null, null);

                } else {
                    requestBody = createDeliveryTask(timeZone, transaction, shop, apiKey, member, null, null);
                }

                TookanTaskInfo taskInfo = tookanApiService.createTask(companyId, shopId, apiKey, isPickup, requestBody);
                if (taskInfo != null) {
                    transaction.setTookanTaskId(taskInfo.getJobId().toString());
                    transactionRepository.updateTookanInfo(companyId, transaction.getId(), String.valueOf(taskInfo.getJobId()));
                }
            }
        });
    }
}
