package com.fourtwenty.core.managed;

import com.fourtwenty.core.domain.repositories.dispensary.QueuedTransactionRepository;
import com.fourtwenty.core.jobs.QueuedTransactionJob;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mdo on 8/28/17.
 */
@Singleton
public class TransactionJobQueueManager implements Managed {
    static final Log LOG = LogFactory.getLog(TransactionJobQueueManager.class);

    ExecutorService executorService;
    @Inject
    Injector injector;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;

    @Override
    public void start() throws Exception {
        LOG.debug("Starting TransactionJobQueueManager");
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public void stop() throws Exception {
        LOG.debug("Stopping TransactionJobQueueManager");
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    public synchronized void runTransactionJob(final String companyId, final String shopId, final String queuedTransactionId) {

        QueuedTransactionJob queuedTransactionJob = injector.getInstance(QueuedTransactionJob.class);
        queuedTransactionJob.setCompanyId(companyId);
        queuedTransactionJob.setShopId(shopId);
        queuedTransactionJob.setQueueTransactionId(queuedTransactionId);

        if (executorService != null) {
            executorService.submit(queuedTransactionJob);
        }
    }
}
