package com.fourtwenty.core.managed;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.dispensary.QueuedTransactionRepository;
import com.fourtwenty.core.jobs.QueuedTransactionJob;
import com.fourtwenty.core.services.thirdparty.RabbitMQFifoService;
import com.fourtwenty.core.services.thirdparty.models.TransSQSMessageRequest;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 10/12/17.
 */
@Singleton
public class TransactionProcessorManager implements Managed {
    static final Log LOG = LogFactory.getLog(TransactionProcessorManager.class);

    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    ConnectConfiguration config;
    private ScheduledExecutorService ses;
    @Inject
    Injector injector;
    @Inject
    private AmazonServiceFactory amazonServiceFactory;
    @Inject
    private AppRepository appRepository;
    @Inject
    private RabbitMQFifoService rabbitMQFifoService;

    @Override
    public void start() throws Exception {
        ses = Executors.newScheduledThreadPool(1);
        final TransProcessorConfig processorConfig = config.getProcessorConfig();
        if (processorConfig != null
                && processorConfig.isEnabled()) {
            if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.Local) {
                runLocalTransProcessor();
            } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
                runSQSTransProcessor(processorConfig);
            } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.RabbitMQ) {
                runRabbitMQTransProcessor(processorConfig);
            }
        } else {
            LOG.info("Transaction Processing is not enabled to process sales.");
        }
    }

    private void runLocalTransProcessor() {
        LOG.info("LOCAL Transaction Processing is enabled.");
        ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // do some work
                Iterable<QueuedTransaction> queuedTransactions = queuedTransactionRepository.getQueuedTransactions(QueuedTransaction.QueueStatus.Pending);

                for (QueuedTransaction queuedTransaction : queuedTransactions) {
                    QueuedTransactionJob queuedTransactionJob = injector.getInstance(QueuedTransactionJob.class);
                    queuedTransactionJob.setCompanyId(queuedTransaction.getCompanyId());
                    queuedTransactionJob.setShopId(queuedTransaction.getShopId());
                    queuedTransactionJob.setQueueTransactionId(queuedTransaction.getId());

                    try {
                        LOG.info("Executing QueuedTransaction: " + queuedTransaction.getId());
                        queuedTransactionJob.run();
                    } catch (Exception e) {
                        LOG.error("Error processing queued transaction job", e);
                    }
                }

            }
        }, 0, 1, TimeUnit.SECONDS);  // ex	ecute every x seconds
    }

    private void runSQSTransProcessor(final TransProcessorConfig processorConfig) {
        LOG.info("SQSQueue Transaction Processing is enabled.");
        final String queueName = getQueueName(processorConfig);
        LOG.info("SQSName: " + queueName);
        String env = config.getEnv().name();
        /*ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
                    // do some work
                    Map<String, String> attributes = new HashMap<String, String>();
                    // A FIFO queue must have the FifoQueue attribute set to True
                    attributes.put("FifoQueue", "true");
                    // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
                    attributes.put("ContentBasedDeduplication", "true");
                    // The FIFO queue name must end with the .fifo suffix

                    GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
                    String myQueueUrl = null;
                    try {
                        GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
                        if (queueUrlResult != null) {
                            myQueueUrl = queueUrlResult.getQueueUrl();
                        }
                    } catch (Exception e) {
                        LOG.info("Queue does not exist.");
                    }
                    if (myQueueUrl == null) {
                        LOG.info("Queue does not exist. Creating new Queue: " + queueName);
                        CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
                        myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
                    }


                    // Receive messages
                    ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
                    // Uncomment the following to provide the ReceiveRequestDeduplicationId
                    //receiveMessageRequest.setReceiveRequestAttemptId("1");
                    List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();

                    //LOG.info("Receiving messages from " + queueName + ", size: " + messages.size());
                    for (Message message : messages) {
                        LOG.info("  Message");
                        LOG.info("    MessageId:     " + message.getMessageId());
                        LOG.info("    ReceiptHandle: " + message.getReceiptHandle());
                        LOG.info("    MD5OfBody:     " + message.getMD5OfBody());
                        LOG.info("    Body:          " + message.getBody());

                        TransSQSMessageRequest request = JsonSerializer.fromJson(message.getBody(), TransSQSMessageRequest.class);

                        QueuedTransactionJob queuedTransactionJob = injector.getInstance(QueuedTransactionJob.class);
                        queuedTransactionJob.setCompanyId(request.getCompanyId());
                        queuedTransactionJob.setShopId(request.getShopId());
                        queuedTransactionJob.setQueueTransactionId(request.getQueueTransactionId());

                        try {
                            LOG.info("Executing QueuedTransaction: " + request.getQueueTransactionId());
                            queuedTransactionJob.run();
                            LOG.info("Deleting Message from SQS Queue: " + request.getQueueTransactionId());
                            amazonSQS.deleteMessage(myQueueUrl, message.getReceiptHandle());
                        } catch (Exception e) {
                            LOG.error("Error processing queued transaction job", e);
                        }
                    }
                } catch (Exception e) {
                    LOG.error("Error executing SQS", e);
                }

            }
        }, 0, 2, TimeUnit.SECONDS);  // ex	ecute every x seconds
        */

        initiateQueueRunner(queueName, env, processorConfig);

    }

    private void runRabbitMQTransProcessor(final TransProcessorConfig processorConfig) {
        LOG.info("RabbitMQ Queue Transaction Processing is enabled.");
        final String queueName = getQueueName(processorConfig);
        String env = config.getEnv().name();
        initiateQueueRunner(queueName, env, processorConfig);
    }

    private String getIP() {
        InetAddress ip;
        String hostname;
        try {
            ip = InetAddress.getLocalHost();
            String hostAddress = ip.getHostAddress();
            hostname = ip.getHostName();
            System.out.println("Your current IP address : " + ip);
            System.out.println("Your current Hostname : " + hostname);

            return hostAddress.replaceAll("\\.","-");
        } catch (UnknownHostException e) {

            e.printStackTrace();
        }
        return "NORMAL";
    }

    private void initiateQueueRunner(String queueName, String env, TransProcessorConfig processorConfig) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        String multiQueueName = "";
        HashMap<String,Boolean> queueServers = new HashMap<>();
        if (app.getQueueServers() != null) {
            queueServers = app.getQueueServers();
        }
        queueServers.put(getIP(),true);
        appRepository.update(app.getId(),app);

        if (app != null) {
            int numOfQueues = 1;
            // need to have at least 1 queue
            if (app != null && app.getNumOfQueues() > 0) {
                numOfQueues = app.getNumOfQueues();
            }

            if (config.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
                try {
                    String computername = InetAddress.getLocalHost().getHostName();
                    computername = computername.replace(".", "");
                    env = config.getEnv().name() + "-" + computername;
                } catch (UnknownHostException e) {
                }
            }

            for (int i = 0; i < numOfQueues; i++) {


                String queueNumStr = "";
                if (i > 0) {
                    queueNumStr = "_" + i;
                }

                //final String queueName =  env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
                multiQueueName = env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
                Runnable queueRunner = null;
                if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
                    LOG.info("SQSName: " + multiQueueName);
                    queueRunner = new SQSQueueRunner(multiQueueName);
                } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.RabbitMQ) {
                    LOG.info("RabbitMQ QueueName : " + multiQueueName);
                    queueRunner = new RabbitMQQueueRunner(multiQueueName, rabbitMQFifoService);
                } else {
                    continue;
                }
                ses.scheduleAtFixedRate(queueRunner,0,2,TimeUnit.SECONDS);
            }

        }
    }

    private String getQueueName(final TransProcessorConfig processorConfig) {
        LOG.info("Queue Transaction Processing is enabled.");
        String env = config.getEnv().name();
        if (config.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();

                computername = computername.replace(".", "");
                env = config.getEnv().name() + "-" + computername;

            } catch (UnknownHostException e) {
            }
        }
        return env + "-" + processorConfig.getQueueName() + ".fifo";
    }

    @Override
    public void stop() throws Exception {
        if (ses != null) {
            ses.shutdown();
        }
    }

    protected class SQSQueueRunner implements Runnable {
        final String queueName;
        public SQSQueueRunner(String myQueueName) {
            queueName = myQueueName;
        }

        @Override
        public void run() {
            try {
                LOG.info(String.format("Queue: %s",queueName));
                AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
                // do some work
                Map<String, String> attributes = new HashMap<String, String>();
                // A FIFO queue must have the FifoQueue attribute set to True
                attributes.put("FifoQueue", "true");
                // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
                attributes.put("ContentBasedDeduplication", "true");
                // The FIFO queue name must end with the .fifo suffix

                GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
                String myQueueUrl = null;
                try {
                    GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
                    if (queueUrlResult != null) {
                        myQueueUrl = queueUrlResult.getQueueUrl();
                    }
                } catch (Exception e) {
                    LOG.info("Queue does not exist.");
                }
                if (myQueueUrl == null) {
                    LOG.info("Queue does not exist. Creating new Queue: " + queueName);
                    CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
                    myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
                }


                // Receive messages
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
                // Uncomment the following to provide the ReceiveRequestDeduplicationId
                //receiveMessageRequest.setReceiveRequestAttemptId("1");
                List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();

                //LOG.info("Receiving messages from " + queueName + ", size: " + messages.size());
                for (Message message : messages) {
                    LOG.info("  Message");
                    LOG.info("    MessageId:     " + message.getMessageId());
                    LOG.info("    ReceiptHandle: " + message.getReceiptHandle());
                    LOG.info("    MD5OfBody:     " + message.getMD5OfBody());
                    LOG.info("    Body:          " + message.getBody());

                    TransSQSMessageRequest request = JsonSerializer.fromJson(message.getBody(), TransSQSMessageRequest.class);

                    QueuedTransactionJob queuedTransactionJob = injector.getInstance(QueuedTransactionJob.class);
                    queuedTransactionJob.setCompanyId(request.getCompanyId());
                    queuedTransactionJob.setShopId(request.getShopId());
                    queuedTransactionJob.setQueueTransactionId(request.getQueueTransactionId());

                    try {
                        LOG.info("Executing QueuedTransaction: " + request.getQueueTransactionId());
                        queuedTransactionJob.run();
                        LOG.info(String.format("Deleting Message from SQS Queue: %s - %s",queueName,request.getQueueTransactionId()));
                        amazonSQS.deleteMessage(myQueueUrl, message.getReceiptHandle());
                    } catch (Exception e) {
                        LOG.error("Error processing queued transaction job", e);
                    }
                }
            } catch (Exception e) {
                LOG.error("Error executing SQS", e);
            }
        }
    }


    protected class RabbitMQQueueRunner implements Runnable {
        final String queueName;
        final RabbitMQFifoService fifoService;
        public RabbitMQQueueRunner(String myQueueName, RabbitMQFifoService fifoService) {
            queueName = myQueueName;
            this.fifoService = fifoService;
        }

        @Override
        public void run() {
            try {
                LOG.info(String.format("Queue: %s",queueName));

                fifoService.processMessageFromQueue(queueName, (channel, consumerTag, envelope, properties, body) -> {
                    LOG.info("Got the message in queue");
                    String messageBody = new String(body, StandardCharsets.UTF_8);
                    TransSQSMessageRequest request = JsonSerializer.fromJson(messageBody, TransSQSMessageRequest.class);

                    if (request == null) {
                        // Let's ack the message
                        try {
                            channel.basicAck(envelope.getDeliveryTag(), false);
                        } catch (IOException e) {
                        }
                    }

                    QueuedTransactionJob queuedTransactionJob = injector.getInstance(QueuedTransactionJob.class);
                    queuedTransactionJob.setCompanyId(request.getCompanyId());
                    queuedTransactionJob.setShopId(request.getShopId());
                    queuedTransactionJob.setQueueTransactionId(request.getQueueTransactionId());
                    try {
                        LOG.info("Executing QueuedTransaction: " + request.getQueueTransactionId());
                        queuedTransactionJob.run();
                        LOG.info(String.format("Acking Message from RabbitMQ Queue: %s - %s",queueName,request.getQueueTransactionId()));
                        // Setting multiple to false so that we acknowledge 1 at a time
                        try {
                            channel.basicAck(envelope.getDeliveryTag(), false);
                        } catch (IOException e) {
                            LOG.warn(String.format("couldn't ack Message from RabbitMQ Queue: %s - %s",queueName,request.getQueueTransactionId()));
                        }
                    } catch (Exception e) {
                        LOG.error("Error processing queued transaction job", e);
                    }
                });


            } catch (Exception e) {
                LOG.error("Error executing RabbitMQ", e);
            }
        }
    }

}
