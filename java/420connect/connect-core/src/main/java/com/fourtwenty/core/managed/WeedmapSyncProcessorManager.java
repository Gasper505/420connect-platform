package com.fourtwenty.core.managed;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.thirdparty.weedmap.WeedmapSyncJob;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WmSyncJobRepository;
import com.fourtwenty.core.jobs.QueuedWeedmapSyncJob;
import com.fourtwenty.core.services.thirdparty.models.WeedmapSQSMessageRequest;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Singleton
public class WeedmapSyncProcessorManager implements Managed {
    public static final String QUEUE_NAME = "WEEDMAP_SYNC_QUEUE";

    static final Log LOG = LogFactory.getLog(WeedmapSyncProcessorManager.class);

    private ScheduledExecutorService scheduledExecutorService;
    @Inject
    private WmSyncJobRepository wmSyncJobRepository;
    @Inject
    private ConnectConfiguration config;
    @Inject
    private Injector injector;
    @Inject
    private AmazonServiceFactory amazonServiceFactory;
    @Inject
    private AppRepository appRepository;

    @Override
    public void start() throws Exception {
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        final TransProcessorConfig processorConfig = config.getProcessorConfig();
        if (processorConfig == null || !processorConfig.isEnabled()) {
            LOG.info("Weedmap Queue Processing is not enabled to process sync.");
        } else {
            if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.Local) {
                runLocalTransProcessor();
            } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
                runSQSWeedmapProcessor(processorConfig);
            }
        }
    }

    @Override
    public void stop() throws Exception {
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
        }
    }

    private void runLocalTransProcessor() {
        LOG.info("LOCAL Transaction Processing is enabled.");
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // do some work
                Iterable<WeedmapSyncJob> syncJobs = wmSyncJobRepository.getQueuedJobs();

                for (WeedmapSyncJob weedmapSyncJob : syncJobs) {
                    QueuedWeedmapSyncJob queuedWeedmapSyncJob = injector.getInstance(QueuedWeedmapSyncJob.class);
                    queuedWeedmapSyncJob.setCompanyId(weedmapSyncJob.getCompanyId());
                    queuedWeedmapSyncJob.setShopId(weedmapSyncJob.getShopId());
                    queuedWeedmapSyncJob.setWeedmapSyncJobId(weedmapSyncJob.getId());

                    try {
                        LOG.info("Executing Weedmap Job: " + weedmapSyncJob.getId());
                        queuedWeedmapSyncJob.run();
                    } catch (Exception e) {
                        LOG.error("Error processing queued weedmap job", e);
                    }
                }

            }
        }, 0, 1, TimeUnit.SECONDS);  // ex	ecute every x seconds
    }

    private void runSQSWeedmapProcessor(final TransProcessorConfig processorConfig) {
        LOG.info("SQSQueue Weedmap Sync Processing is enabled.");
        String env = config.getEnv().name();
        if (config.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();

                computername = computername.replace(".", "");
                env = String.format("%s-%s", config.getEnv().name(), computername);

            } catch (UnknownHostException e) {
            }
        }

        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app == null) {
            return;
        }
        int numOfQueues = 2;
        // need to have at least 1 queue
        if (app != null && app.getNumOfQueues() > 0) {
            numOfQueues = app.getNumOfQueues();
        }

        for (int i = 0; i < numOfQueues; i++) {
            String queueNumStr = (i > 0) ? "_" + i : "";


            //final String queueName =  env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
            final String multiQueueName = env + "-" + QUEUE_NAME + queueNumStr + ".fifo";
            LOG.info("SQSName: " + multiQueueName);

            WeedmapSyncProcessorManager.QueueRunner queueRunner = new WeedmapSyncProcessorManager.QueueRunner(multiQueueName);
            scheduledExecutorService.scheduleAtFixedRate(queueRunner,0,2,TimeUnit.SECONDS);
        }



    }

    protected class QueueRunner implements Runnable {
        final String queueName;
        public QueueRunner(String myQueueName) {
            queueName = myQueueName;
        }

        @Override
        public void run() {
            try {
                LOG.info(String.format("Queue: %s",queueName));

                AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();

                // do some work
                Map<String, String> attributes = new HashMap<String, String>();
                // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
                attributes.put("ContentBasedDeduplication", "true");
                // A FIFO queue must have the FifoQueue attribute set to True
                attributes.put("FifoQueue", "true");
                // The FIFO queue name must end with the .fifo suffix

                GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
                String myQueueUrl = null;
                try {
                    GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
                    if (queueUrlResult != null) {
                        myQueueUrl = queueUrlResult.getQueueUrl();
                    }
                } catch (Exception e) {
                    LOG.info("Queue does not exist.");
                }

                if (myQueueUrl == null) {
                    LOG.info("Queue does not exist. Creating new Queue: " + queueName);
                    CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
                    myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
                }


                // Receive messages
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
                // Uncomment the following to provide the ReceiveRequestDeduplicationId
                //receiveMessageRequest.setReceiveRequestAttemptId("1");
                List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();

                //LOG.info("Receiving messages from " + queueName + ", size: " + messages.size());
                for (Message message : messages) {
                    LOG.info("  Message");
                    LOG.info("    ReceiptHandle: " + message.getReceiptHandle());
                    LOG.info("    MessageId:     " + message.getMessageId());
                    LOG.info("    MD5OfBody:     " + message.getMD5OfBody());
                    LOG.info("    Body:          " + message.getBody());

                    WeedmapSQSMessageRequest request = JsonSerializer.fromJson(message.getBody(), WeedmapSQSMessageRequest.class);
                    if (request == null) {
                        continue;
                    }
                    QueuedWeedmapSyncJob weedmapSyncJob = injector.getInstance(QueuedWeedmapSyncJob.class);
                    weedmapSyncJob.setCompanyId(request.getCompanyId());
                    weedmapSyncJob.setShopId(request.getShopId());
                    weedmapSyncJob.setWeedmapSyncJobId(request.getQueuedWeedmapJobId());

                    try {
                        LOG.info("Executing Queued weedmap sync job: " + request.getQueuedWeedmapJobId());
                        weedmapSyncJob.run();
                        LOG.info(String.format("Deleting Message from SQS Queue: %s - %s", queueName, request.getQueuedWeedmapJobId()));
                        amazonSQS.deleteMessage(myQueueUrl, message.getReceiptHandle());
                    } catch (Exception e) {
                        LOG.error("Error processing queued weedmap sync job", e);
                    }
                }
            } catch (Exception e) {
                LOG.error("Error executing SQS", e);
            }
        }
    }

}
