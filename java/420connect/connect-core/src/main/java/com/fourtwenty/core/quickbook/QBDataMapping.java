package com.fourtwenty.core.quickbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QBDataMapping {
    private String shopId;
    private String qbDesktopRef;
    private String editSequence;
    private String qbListId;
    private boolean qbErrored;
    private long errorTime;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getQbDesktopRef() {
        return qbDesktopRef;
    }

    public void setQbDesktopRef(String qbDesktopRef) {
        this.qbDesktopRef = qbDesktopRef;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getQbListId() {
        return qbListId;
    }

    public void setQbListId(String qbListId) {
        this.qbListId = qbListId;
    }

    public boolean isQbErrored() {
        return qbErrored;
    }

    public void setQbErrored(boolean qbErrored) {
        this.qbErrored = qbErrored;
    }

    public long getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(long errorTime) {
        this.errorTime = errorTime;
    }
}
