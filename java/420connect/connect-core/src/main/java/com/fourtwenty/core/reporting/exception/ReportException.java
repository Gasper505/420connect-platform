package com.fourtwenty.core.reporting.exception;

/**
 * Created by Stephen Schmidt on 4/9/2016.
 */
public class ReportException extends Exception {
    public ReportException(String messsage) {
        super(messsage);
    }
}
