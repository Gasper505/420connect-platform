package com.fourtwenty.core.reporting.gather;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.impl.transaction.TaxSummaryGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CompanySalesByBrand implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = new String[]{"Brand", "Shop", "# of Transactions", "COGS", "Retail Value", "Discounts", "After Tax Discount", "Sales", "Excise Tax", "City Tax", "County Tax", "State Tax", "Total Tax", "Delivery Fee", "Gross Receipts"};


    public CompanySalesByBrand() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // 0
                GathererReport.FieldType.STRING, // 1
                GathererReport.FieldType.STRING, // 2
                GathererReport.FieldType.NUMBER, // 3
                GathererReport.FieldType.CURRENCY, // 4
                GathererReport.FieldType.CURRENCY, // 5
                GathererReport.FieldType.CURRENCY, // 6
                GathererReport.FieldType.CURRENCY, // 7
                GathererReport.FieldType.CURRENCY, // 8
                GathererReport.FieldType.CURRENCY, // 9
                GathererReport.FieldType.CURRENCY, // 10
                GathererReport.FieldType.CURRENCY, // 11
                GathererReport.FieldType.CURRENCY, // 12
                GathererReport.FieldType.CURRENCY, // 13
                GathererReport.FieldType.CURRENCY, // 14
                GathererReport.FieldType.CURRENCY, // 15
                GathererReport.FieldType.CURRENCY, // 16
                GathererReport.FieldType.CURRENCY, // 17
                GathererReport.FieldType.CURRENCY, // 18
                GathererReport.FieldType.CURRENCY, // 19
                GathererReport.FieldType.CURRENCY, // 20
                GathererReport.FieldType.CURRENCY, // 21
                GathererReport.FieldType.PERCENTAGE// 22
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Company Sales By Brand Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        List<ObjectId> brandIds = new ArrayList<>();
        //Get transactions by company
        Iterable<Transaction> transactionResults = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<Transaction> transactions = new ArrayList<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        LinkedHashSet<String> productIdList = new LinkedHashSet<>();
        for (Transaction transaction : transactionResults) {
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank((orderItem.getProductId())) && ObjectId.isValid(orderItem.getProductId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                        productIdList.add(orderItem.getProductId());
                    }
                }
            }
            transactions.add(transaction);
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsByCompany(filter.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductBatch> batchHashMap = batchRepository.getBatchesForProductsMap(filter.getCompanyId(), Lists.newArrayList(productIdList));
        Iterable<Prepackage> prepackagesForProducts = prepackageRepository.getPrepackagesForProductsByCompany(filter.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        for (Prepackage prepackages : prepackagesForProducts) {
            prepackageHashMap.putIfAbsent(prepackages.getId(), prepackages);
        }

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Map.Entry<String, Product> entry : productMap.entrySet()) {
            Product product = entry.getValue();
            List<Product> items = productsByCompanyLinkId.get(product.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            productsByCompanyLinkId.put(product.getCompanyLinkId(), items);

            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }
        HashMap<String, Brand> brandMap = brandRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(brandIds));

        List<ObjectId> shopIds = new ArrayList<>();

        //Create map on basis of product name (<Product NAME, SalesByProduct>)
        HashMap<String, CompanySalesByBrand.SalesByBrand> salesMap = new HashMap<>();

        //Process transactions for calculation of taxes in respect to products
        int factor = 1;
        for (Transaction transaction : transactions) {
            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Cart cart = transaction.getCart();
            if (cart == null) {
                continue;
            }
            List<String> visited = new ArrayList<>();

            int totalFinalCost = 0;
            for (OrderItem item : transaction.getCart().getItems()) {
                totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
            }

            double avgDeliveryFee = 0;
            if (transaction.getCart().getItems().size() > 0) {
                avgDeliveryFee = transaction.getCart().getDeliveryFee().doubleValue() / transaction.getCart().getItems().size();
            }

            double avgAfterTaxDiscount = 0;
            if (transaction.getCart().getAppliedAfterTaxDiscount() != null && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                avgAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() / transaction.getCart().getItems().size();
            }

            if (cart != null && transaction.getCart().getItems() != null) {
                HashMap<String, Double> discountHasMap = new HashMap<>();
                taxSummaryGatherer.calculateOrderItemTax(cart, cart.getTaxResult(), discountHasMap, transaction);

                String shopId = transaction.getShopId();
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    Product product = productMap.get(orderItem.getProductId());
                    if (product == null) {
                        continue;
                    }
                    Brand brand = brandMap.get(product.getBrandId());

                    if (transaction.getTransType() == Transaction.TransactionType.Sale
                            || (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded)) {

                        String brandId = Objects.isNull(brand) ? "N/A" : brand.getId();
                        salesMap.putIfAbsent(brandId, new CompanySalesByBrand.SalesByBrand());
                        CompanySalesByBrand.SalesByBrand salesByBrand = salesMap.get(brandId);
                        salesByBrand.name = Objects.isNull(brand) ? "" : brand.getName();
                        salesMap.put(brandId, salesByBrand);


                        if (StringUtils.isNotBlank(shopId) && ObjectId.isValid(shopId)) {
                            shopIds.add(new ObjectId(shopId));
                        }

                        salesByBrand.txByShop.putIfAbsent(shopId, new BrandSaleByShop());
                        BrandSaleByShop brandSaleByShop = salesByBrand.txByShop.get(shopId);
                        brandSaleByShop.txNo.add(transaction.getTransNo());

                        // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                        if (!visited.contains(product.getId())) {
                            brandSaleByShop.transCount++;
                            visited.add(product.getId());
                        }
                        int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                        double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                        double itemDiscount = orderItem.getCost().subtract(orderItem.getFinalPrice()).doubleValue();
                        double targetCartDiscount = transaction.getCart().getCalcCartDiscount().doubleValue() * ratio;
                        double targetCreditCardFee = transaction.getCart().getCreditCardFee().doubleValue() * ratio;

                        double sales = NumberUtils.round(orderItem.getFinalPrice().doubleValue() - targetCartDiscount, 5);

                        if (orderItem.getTaxResult() != null) {
                            brandSaleByShop.cityTax += orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor;
                            brandSaleByShop.countyTax += orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                            brandSaleByShop.stateTax += orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor;
                            brandSaleByShop.federalTax += orderItem.getTaxResult().getTotalFedTax().doubleValue() * factor;

                            brandSaleByShop.exciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                            brandSaleByShop.preALExciseTax += orderItem.getTaxResult().getOrderItemPreALExciseTax().doubleValue() * factor;
                            brandSaleByShop.preNALExciseTax += orderItem.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;
                            brandSaleByShop.postALExciseTax += orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                            brandSaleByShop.postNALExciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                        }
                        brandSaleByShop.retailValue += orderItem.getCost().doubleValue() * factor;
                        brandSaleByShop.discounts += itemDiscount * factor;
                        brandSaleByShop.deliveryFee += avgDeliveryFee * factor;
                        brandSaleByShop.sales += sales * factor;
                        brandSaleByShop.afterTaxDiscount += avgAfterTaxDiscount * factor;
                        brandSaleByShop.creditCardFee += targetCreditCardFee * factor;

                        // calculate cost of goods
                        boolean calculated = false;
                        double cogs = 0;
                        if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                            // if prepackage is set, use prepackage
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null && targetBatch != null) {
                                    calculated = true;

                                    BigDecimal unitValue = prepackage.getUnitValue();
                                    if (unitValue == null || unitValue.doubleValue() == 0) {
                                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                        unitValue = weightTolerance.getUnitValue();
                                    }
                                    // calculate the total quantity based on the prepackage value
                                    cogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                }
                            }
                        } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                            // otherwise, use quantity logs
                            for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                    ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                    if (targetBatch != null) {
                                        calculated = true;
                                        cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                    }
                                }
                            }
                        }

                        if (!calculated) {
                            double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                            cogs = unitCost * orderItem.getQuantity().doubleValue();
                        }

                        brandSaleByShop.cogs += cogs * factor;
                    }
                }
            }
        }

        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(shopIds));


        for (Map.Entry<String, CompanySalesByBrand.SalesByBrand> entry : salesMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            CompanySalesByBrand.SalesByBrand salesByBrand = entry.getValue();

            data.put(attrs[0], salesByBrand.name);

            int count = 0;
            for (Map.Entry<String, BrandSaleByShop> shopEntry : salesByBrand.txByShop.entrySet()) {
                Shop shop = shopMap.get(shopEntry.getKey());
                BrandSaleByShop saleByShop = shopEntry.getValue();
                if (count > 0) {
                    data = new HashMap<>();
                    data.put(attrs[0], "");

                }
                if (shop != null) {
                    count = 1;

                    data.put(attrs[0], salesByBrand.name);
                    data.put(attrs[1], shop != null ? shop.getName() : "");
                    data.put(attrs[2], saleByShop.transCount);
                    data.put(attrs[3], new DollarAmount(saleByShop.cogs));
                    data.put(attrs[4], new DollarAmount(saleByShop.sales));
                    data.put(attrs[5], new DollarAmount(saleByShop.discounts));
                    data.put(attrs[6], new DollarAmount(saleByShop.afterTaxDiscount));
                    data.put(attrs[7], new DollarAmount(saleByShop.sales));
                    data.put(attrs[8], new DollarAmount(saleByShop.exciseTax));
                    data.put(attrs[9], new DollarAmount(saleByShop.cityTax));
                    data.put(attrs[10], new DollarAmount(saleByShop.countyTax));
                    data.put(attrs[11], new DollarAmount(saleByShop.stateTax));

                    double totalTax = saleByShop.cityTax + saleByShop.countyTax + saleByShop.stateTax + saleByShop.federalTax + saleByShop.postALExciseTax + saleByShop.postNALExciseTax;
                    data.put(attrs[12], new DollarAmount(totalTax));
                    data.put(attrs[13], new DollarAmount(saleByShop.deliveryFee));

                    double gross = saleByShop.sales + saleByShop.deliveryFee + totalTax + saleByShop.creditCardFee - saleByShop.afterTaxDiscount;
                    data.put(attrs[14], new DollarAmount(gross));

                    report.add(data);
                }
            }
        }

        return report;
    }

    private class SalesByBrand {
        private String name = StringUtils.EMPTY;
        //<shopId, List of transaction>
        private HashMap<String, BrandSaleByShop> txByShop = new HashMap<>();

        public SalesByBrand() {
        }
    }

    private class BrandSaleByShop {
        HashSet<String> txNo = new HashSet<>();
        private double cogs = 0d;
        private int transCount = 0;
        private double retailValue = 0d;
        private double discounts = 0d;
        private double afterTaxDiscount = 0d;
        private double sales = 0d;
        private double exciseTax = 0d;
        private double cityTax = 0d;
        private double countyTax = 0d;
        private double stateTax = 0d;
        private double federalTax = 0d;
        private double deliveryFee = 0d;
        private double creditCardFee = 0d;
        private double preALExciseTax = 0d;
        private double preNALExciseTax = 0d;
        private double postALExciseTax = 0d;
        private double postNALExciseTax = 0d;

        public BrandSaleByShop() {
        }
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}
