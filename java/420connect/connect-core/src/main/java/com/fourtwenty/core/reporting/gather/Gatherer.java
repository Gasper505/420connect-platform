package com.fourtwenty.core.reporting.gather;

import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public interface Gatherer {
    GathererReport gather(ReportFilter filter);
}
