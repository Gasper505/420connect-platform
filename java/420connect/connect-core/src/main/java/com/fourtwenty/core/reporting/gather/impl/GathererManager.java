package com.fourtwenty.core.reporting.gather.impl;

import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.CompanySalesByBrand;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.admin.StorePerformanceReport;
import com.fourtwenty.core.reporting.gather.impl.admin.TimeClockGatherer;
import com.fourtwenty.core.reporting.gather.impl.company.*;
import com.fourtwenty.core.reporting.gather.impl.dashboard.*;
import com.fourtwenty.core.reporting.gather.impl.employee.EmployeeActivityGatherer;
import com.fourtwenty.core.reporting.gather.impl.employee.EmployeeBySalesByProductGatherer;
import com.fourtwenty.core.reporting.gather.impl.employee.MileageReportByEmployeeGatherer;
import com.fourtwenty.core.reporting.gather.impl.employee.SalesByEmployeeGatherer;
import com.fourtwenty.core.reporting.gather.impl.export.BatchExportGatherer;
import com.fourtwenty.core.reporting.gather.impl.export.ExportMemberGatherer;
import com.fourtwenty.core.reporting.gather.impl.export.ProductsExportGatherer;
import com.fourtwenty.core.reporting.gather.impl.export.VendorsExportGatherer;
import com.fourtwenty.core.reporting.gather.impl.inventory.*;
import com.fourtwenty.core.reporting.gather.impl.marketing.MarketingReportGatherer;
import com.fourtwenty.core.reporting.gather.impl.member.*;
import com.fourtwenty.core.reporting.gather.impl.promotion.PromotionGatherer;
import com.fourtwenty.core.reporting.gather.impl.promotion.PromotionsActivityGatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.*;
import com.fourtwenty.core.services.mgmt.DeliveryService;
import com.fourtwenty.core.services.mgmt.MemberService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.fourtwenty.core.services.mgmt.TimeCardService;
import com.google.inject.Injector;

import javax.inject.Inject;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public class GathererManager {
    //Repos
    @Inject
    protected Injector injector;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private DoctorRepository doctorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private TimeCardRepository timeCardRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private MemberGroupRepository groupRepository;
    @Inject
    private AuditLogRepository auditLogRepository;
    @Inject
    private ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private CashDrawerSessionRepository cashDrawerRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PromoUsageRepository promoUsageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ShipmentBillRepository shipmentBillRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private TerminalLocationRepository terminalLocationRepository;
    @Inject
    private ReconciliationHistoryRepository reconciliationHistoryRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductWeightToleranceRepository productWeightToleranceRepository;

    //Services
    @Inject
    private ProductService productService;
    @Inject
    private TimeCardService timeCardService;
    @Inject
    private MemberService memberService;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private DeliveryService deliveryService;
//    @Inject
//    private QBSyncReportsDetail syncReportsDetail;
//    @Inject
//    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
//    @Inject

    Gatherer g;

    public GathererManager() {
    }

    public Gatherer getGathererForReportType(ReportType type) {
        g = null;
        switch (type) {

            //Transaction Reports
            case SALES_BY_CONSUMER_TYPE:
                g = injector.getInstance(SalesByConsumerTypeGatherer.class);
                break;
            case TOTAL_SALES:
                g = injector.getInstance(TotalSalesOldGatherer.class);
                break;
            case TOTAL_SALES_OLD:
                // Total sales report as per old mechanism
                g = injector.getInstance(TotalSalesOldGatherer.class);
                break;
            case TOTAL_SALES_BY_COMPANY:
                g = injector.getInstance(TotalSalesByCompanyGatherer.class);
                break;
            case COMPANY_SALES_REPORT:
                g = injector.getInstance(CompanySalesGatherer.class);
                break;
            case TOTAL_SALES_WITH_UNCOMPLETE_ORDERS:
                g = injector.getInstance(TotalSalesWithUncompleteOrdersGatherer.class);
                break;
            case ALL_SALES:
                g = injector.getInstance(AllSalesGatherer.class);
                break;
            case TOTAL_SALES_PRODUCTS:
                g = injector.getInstance(TotalSalesProductsGatherer.class);
                break;
            case TOTAL_SALES_PRODUCTS_OLD:
                g = injector.getInstance(TotalSalesProductsOldGatherer.class);
                break;
            case TOTAL_SALES_DETAIL_BY_COMPANY:
                g = injector.getInstance(TotalSalesDetailByCompanyGatherer.class);
                break;
            case COMPLETED_SALES_DETAILS_REPORT:
                g = injector.getInstance(AllSalesDetailsGatherer.class);
                break;
            case SALES_BY_DAY:
                g = new SalesByDayGatherer(transactionRepository, productRepository);
                break;
            case SALES_BY_HOUR:
                g = new SalesByHourGatherer(transactionRepository, memberRepository);
                break;
            case SALES_BY_HOUR_OLD:
                g = new SalesByHourGatherer(transactionRepository, memberRepository);
                break;
            case DAILY_SUMMARY:
                g = injector.getInstance(DailySummaryGatherer.class);
                break;
            case SALES_BY_MEMBERSHIP_GROUP:
                g = new SalesByMembershipGroupGatherer(transactionRepository, memberRepository, groupRepository);
                break;
            case PRODUCT_SALES_BY_CANNABIS_TYPE:
                g = new ProductSalesByCannabisTypeGatherer(transactionRepository, productRepository);
                break;
            case SALES_BY_PRODUCT_CATEGORY:
                g = injector.getInstance(SalesByProductCategoryGatherer.class);
                break;
            case SALES_BY_PRODUCT:
                g = injector.getInstance(SalesByProductGatherer.class);
                break;
            case SALES_BY_PAYMENT_TYPE:
                g = injector.getInstance(SalesByPaymentTypeGatherer.class);
                break;
            case SALES_BY_TERMINAL:
                //TODO: sales by terminal
                break;
            case SALES_BY_QUEUE:
                g = new SalesByQueueGatherer(transactionRepository);
                break;
            case AVERAGE_WAIT_PER_QUEUE:
                //TODO: average wait per queue
                break;
            case AVERAGE_TRANSACTION_TIME:
                g = new AverageTransactionTimeGatherer(transactionRepository);
                break;
            case SALES_BY_CANNABIS_TYPE:
                g = new SalesByCannabisTypeGatherer(transactionRepository, productRepository);
                break;
            case PROFIT_LOSS_REPORT:
                g = new ProfitLossGatherer(transactionRepository, productRepository, batchRepository, prepackageRepository, prepackageProductItemRepository, productWeightToleranceRepository);
                break;
            case PRODUCT_PERFORMANCE:
                break;
            case REFUND_HISTORY:
                g = new RefundHistoryGatherer(transactionRepository, productService, employeeRepository, memberRepository);
                break;
            case DISCOUNTS_USED:
                g = injector.getInstance(DiscountGatherer.class);
                break;
            case SALES_BY_CITY:
                g = new SalesByCityGatherer(transactionRepository, memberRepository, productRepository);
                break;
            case SALES_BY_VENDOR:
                g = new SalesByVendorGatherer(transactionRepository, productRepository, vendorRepository);
                break;
            case TAX_SUMMARY:
                g = new TaxSummaryGatherer(transactionRepository, memberRepository, productRepository, categoryRepository);
                break;
            case PRODUCTS_BY_VENDOR:
                g = injector.getInstance(ProductsByVendorGatherer.class);
                break;
            case INVENTORY_VALUATION:
                //g = injector.getInstance(InventoryGatherer.class);
                g = injector.getInstance(InventoryValuationGatherer.class);
                break;
            case INVENTORY_TRANSFER_LOG:
                g = injector.getInstance(InventoryTransferGatherer.class);
                break;
            case INVENTORY_DISTRIBUTION:
                g = injector.getInstance(InventoryDistributionReport.class);
                break;
            case SHELVES_INVENTORY_VALUATION:
                //TODO: Shelves inventory valuation
                break;
            case INVENTORY_BY_CANNABIS_TYPE:
                g = new InventoryByStrainGatherer(productRepository, batchRepository);
                break;
            case LOW_STOCK_INVENTORY:
                //TODO: Low stock inventory
                break;
            case SALES_BY_EMPLOYEE:
                g = injector.getInstance(SalesByEmployeeGatherer.class);
                break;
            case LOSS_REPORT:
                g = injector.getInstance(LossGatherer.class);
                break;
            case CASH_DRAWER:
                g = new CashDrawerGatherer(terminalRepository, cashDrawerRepository);
                break;
            case PAIDINOUT_ACTIVITY:
                g = injector.getInstance(PaidInOutActivityGatherer.class);
                break;
            case INCOMING_ORDER:
                g = injector.getInstance(IncomingOrderGatherer.class);
                break;

            //Inventory reports
            case INVENTORY_PROCUREMENT:
                g = new InventoryProcurementGatherer(batchRepository, vendorRepository, productRepository, categoryRepository, purchaseOrderRepository);
                break;
            case INVENTORY_HISTORY_LOG:
                g = new InventoryHistoryGatherer(inventoryRepository, productRepository, productChangeLogRepository, categoryRepository);
                break;
            case INVENTORY_PRODUCT_HISTORY_LOG:
                g = injector.getInstance(ProductChangeHistoryGatherer.class);
                break;
            case SINGLE_INVENTORY:
                g = injector.getInstance(SingleInventoryGatherer.class); //new SingleInventoryGatherer(inventoryRepository, transactionRepository, productRepository, categoryRepository, prepackageRepository, prepackageQuantityRepository, prepackageProductItemRepository);
                break;
            case INVENTORY_HOLD_LOG:
                g = injector.getInstance(InventoryOnHoldGatherer.class);
                break;
            case INVENTORY_MOVEMENT:
                g = new InventoryMovementGatherer(transactionRepository, employeeRepository,
                        terminalRepository, memberRepository, productRepository, productChangeLogRepository, inventoryRepository);
                break;
            case INVENTORY_SNAPSHOT_REPORT:
                g = new InventorySnapshotGatherer(employeeRepository, productChangeLogRepository, inventoryRepository, productRepository, categoryRepository, prepackageRepository, batchRepository);
                break;
            case LOW_INVENTORY:
                g = injector.getInstance(LowInventoryGatherer.class);
                break;
            case PRODUCT_SELL_BY_EXPIRE:
                g = new ProductSellByGatherer(productRepository, batchRepository, categoryRepository);
                break;
            case INVENTORY_AGING:
                g = injector.getInstance(InventoryAgingGatherer.class);
                break;
            case SELL_THROUGH_REPORT:
                g = injector.getInstance(SellThroughGatherer.class);
                break;
            case MOVING_PRODUCT_BY_PACKAGEDDATE:
                g = injector.getInstance(FastestMovingProductByPackedDate.class);
                break;
            case MOVING_PRODUCT_BY_HARVESTDATE:
                g = injector.getInstance(FastestMovingProductByHarvestDate.class);
                break;
            //New Inventory Report 19/12/2017
            case INVENTORY_RECONCILIATION_HISTORY:
                g = new ReconciliationHistoryGatherer(terminalRepository, productRepository, categoryRepository, reconciliationHistoryRepository, prepackageRepository, prepackageProductItemRepository, employeeRepository, inventoryRepository, batchRepository, productWeightToleranceRepository, shopRepository);
                break;
            //Enhancement : inventory valuation by category 21/12/2017
            case INVENTORY_VALUATION_BY_CATEGORY:
                //g = new InventoryByCategoryGatherer(productRepository, categoryRepository, inventoryRepository, batchRepository, prepackageRepository, prepackageQuantityRepository);

                g = injector.getInstance(InventoryValuationByCategoryGatherer.class);
                break;
            //Membership Reports
            case TOTAL_ACTIVE_MEMBERS:
                g = new ActiveMembersGatherer(memberRepository);
                break;
            case MEMBERSHIP_BY_GROUP:
                g = new MembershipsByGroupGatherer(memberRepository, groupRepository, transactionRepository);
                break;
            case MEMBERSHIPS_BY_DOCTORS:
                g = new MembershipsByDoctorGatherer(memberRepository, doctorRepository);
                break;
            case EXPORT_MEMBER_AGREEMENTS:
                //TODO: export member agreements
                break;
            case MEMBERS_EXPIRING_SOON:
                g = new MembershipsExpiringSoonGatherer(memberRepository);
                break;
            case BEST_PERFORMING_MEMBERS:
                g = injector.getInstance(BestPerformingMembersGatherer.class);
                break;
            case NEW_MEMBERS:
                g = injector.getInstance(NewMembersGatherer.class);
                break;
            case MEMBERS_BY_CITY:
                g = new MembershipsByCityGatherer(memberRepository);
                break;
            case INACTIVE_MEMBERS:
                g = new InactiveMembersGatherer(memberRepository, transactionRepository);
                break;
            case EXPORT_MEMBERS:
                g = injector.getInstance(ExportMemberGatherer.class);
                break;
            case CUSTOMER_BOUGHT_PRODUCT_ON_DAILY_BASIS:
                g = new CustomerBoughtOnDailyBasis(memberRepository, transactionRepository, productRepository, prepackageRepository, prepackageProductItemRepository);
                break;
            case MEMBER_PERFORMANCE:
                g = injector.getInstance(MemberPerformanceGatherer.class);
                break;

            // Promotion Report
            case PROMOTION_USED:
                g = injector.getInstance(PromotionGatherer.class);
                break;
            // Promotion Report
            case PROMOTIONS_ACTIVITY:
                g = injector.getInstance(PromotionsActivityGatherer.class);
                break;

            //Admin Reports
            case STORE_PERFORMANCE:
                g = new StorePerformanceReport(transactionRepository, memberRepository, shopRepository);
                break;
            case CASH_DRAWER_REPORTS:
                //TODO: Cash drawer report
                break;
            case TIME_CLOCK_REPORTS:
                g = new TimeClockGatherer(timeCardRepository, employeeRepository, terminalRepository);
                break;
            case EMPLOYEE_ACTIVITY_REPORT:
                //TODO: employee activity report
                break;
            case PRODUCTS_EXPORT:
                g = injector.getInstance(ProductsExportGatherer.class);
                break;
            case VENDORS_EXPORT:
                g = injector.getInstance(VendorsExportGatherer.class);
                break;
            case PRODUCT_BATCH_EXPORT:
                g = injector.getInstance(BatchExportGatherer.class);
                break;
            //Dashboard
            case RECENT_TRANSACTIONS_DASHBOARD:
                g = new RecentTransactionsGatherer(transactionRepository, memberRepository);
                break;
            case CLOCKED_IN_EMPLOYEES_DASHBOARD:
                g = new ClockedInEmployeesGatherer(employeeRepository, roleRepository, timeCardService);
                break;
            case SALES_BY_CANNABIS_TYPE_DASHBOARD:
                g = new SalesByCannabisTypeGatherer(transactionRepository, productRepository);
                break;
            case DAILY_TOTAL_SALES_DASHBOARD:
                g = new DailySalesTotalGatherer(transactionRepository);
                break;
            case DAILY_NEW_MEMBERS_DASHBOARD:
                g = new DailyNewMembersGatherer(memberRepository);
                break;
            case DAILY_TOTAL_VISITS_DASHBOARD:
                g = new DailyTotalVisitsGatherer(transactionRepository);
                break;
            case DAILY_BEST_SELLING_PRODUCT_DASHBOARD:
                g = new DailyBestSellingProductGatherer(transactionRepository, productRepository);
                break;
            case SALES_BY_BRACKET_DASHBOARD:
                g = new SalesByDateBracketGatherer(transactionRepository);
                break;

            //Employee Reports
            case EMPLOYEE_ACTIVITY:
                g = new EmployeeActivityGatherer(auditLogRepository, employeeRepository, terminalRepository);
                break;
            case EMPLOYEE_BY_SALES_BY_PRODUCT:
                g = new EmployeeBySalesByProductGatherer(employeeRepository, transactionRepository, productRepository);
                break;

            case MARKETING:
                g = new MarketingReportGatherer(memberRepository);
                break;

            //Purchase Order Shipment Bill
            case SHIPMENT_BILL:
                g = injector.getInstance(ShipmentBillGatherer.class);
                break;
            case RETURN_TO_VENDOR:
                g = injector.getInstance(ReturnToVendorGatherer.class);
                break;

            //Mileage Report
            case MILEAGE_REPORT_BY_TRANSACTION:
                g = new MileageReportByTransactionGatherer(transactionRepository, terminalLocationRepository, deliveryService);
                break;
            case MILEAGE_REPORT_BY_EMPLOYEE:
                g = new MileageReportByEmployeeGatherer(employeeRepository, terminalLocationRepository, deliveryService);
                break;

            //Company Wide Report
            case COMPANY_SALES_BY_PRODUCT_CATEGORY:
                g = injector.getInstance(CompanySalesByProductCategoryGatherer.class);
                break;
            case COMPANY_TAX_SUMMARY:
                g = injector.getInstance(CompanyTaxSummaryGatherer.class);
                break;
            case SALES_BY_COMPANY:
                g = injector.getInstance(SalesByCompanyGatherer.class);
                break;
            case INVENTORY_BY_COMPANY:
                g = injector.getInstance(InventoryByCompany.class);
                break;
            case COMPANY_SALES_BY_PRODUCT:
                g = injector.getInstance(CompanySalesByProduct.class);
                break;
            case INVENTORY_ACTION:
                g = injector.getInstance(InventoryActionGatherer.class);
                break;
            case JOURNAL_ENTRY:
                g = injector.getInstance(JournalEntryGatherer.class);
                break;
            case PURCHASE_ORDER_BY_CATEGORY:
                g = injector.getInstance(PurchaseOrderByCategoryGatherer.class);
                break;
            case COMPANY_SALES_BY_BRAND:
                g = injector.getInstance(CompanySalesByBrand.class);
                break;
            case INVENTORY_LOG_VALUATION:
                g = injector.getInstance(InventoryValuationGatherer.class);
                break;
        }
        return g;
    }

}
