package com.fourtwenty.core.reporting.gather.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

import static com.google.common.collect.Iterables.isEmpty;

public class InventoryActionGatherer implements Gatherer {

    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ReconciliationHistoryRepository reconciliationHistoryRepository;
    @Inject
    private InventoryTransferHistoryRepository inventoryTransferHistoryRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{
            "Date",
            "Source Type",
            "Source",
            "Action",
            "Product",
            "Batch",
            "Prepackage",
            "Inventory",
            "Employee",
            "Quantity",
            "Running Total"
    };

    public InventoryActionGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory Action Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<InventoryOperation> inventoryActions = null;
        if (filter.getSourceType() == InventoryOperation.SourceType.None) {
            if ((StringUtils.isBlank(filter.getProductId()) && StringUtils.isBlank(filter.getBatchId())) || (StringUtils.isNotBlank(filter.getProductId()) && StringUtils.isBlank(filter.getBatchId()))) {
                inventoryActions = inventoryActionRepository.getAllActions(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getProductId());
            }
            if (StringUtils.isBlank(filter.getProductId()) && StringUtils.isNotBlank(filter.getBatchId())) {
                inventoryActions = inventoryActionRepository.getAllActionsWithBatch(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getBatchId());
            }
            if (StringUtils.isNotBlank(filter.getProductId()) && StringUtils.isNotBlank(filter.getBatchId())) {
                inventoryActions = inventoryActionRepository.getActionByProductWithBatch(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getProductId(), filter.getBatchId());
            }
        } else {
            if ((StringUtils.isBlank(filter.getProductId()) && StringUtils.isBlank(filter.getBatchId())) || (StringUtils.isNotBlank(filter.getProductId()) && StringUtils.isBlank(filter.getBatchId()))) {
                inventoryActions = inventoryActionRepository.getActionsBySource(filter.getCompanyId(), filter.getShopId(), filter.getSourceType(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getProductId());
            }
            if (StringUtils.isBlank(filter.getProductId()) && StringUtils.isNotBlank(filter.getBatchId())) {
                inventoryActions = inventoryActionRepository.getAllActionsBySourceWithBatch(filter.getCompanyId(), filter.getShopId(), filter.getSourceType(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getBatchId());
            }
            if (StringUtils.isNotBlank(filter.getProductId()) && StringUtils.isNotBlank(filter.getBatchId())) {
                inventoryActions = inventoryActionRepository.getBySourceWithProductAndBatch(filter.getCompanyId(), filter.getShopId(), filter.getSourceType(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), filter.getProductId(), filter.getBatchId());
            }
        }
        if (inventoryActions == null || isEmpty(Lists.newArrayList(inventoryActions))) {
            return  report;
        }
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        HashMap<String, Transaction> transactionHashMap;
        HashMap<String, InventoryTransferHistory> transferHistoryHashMap;
        HashMap<String, ReconciliationHistory> reconciliationHistoryHashMap;
        HashMap<String, PurchaseOrder> purchaseOrderHashMap;

        List<ObjectId> transactionIds = new ArrayList<>();
        List<ObjectId> transferIds = new ArrayList<>();
        List<ObjectId> reconciliationIds = new ArrayList<>();
        List<ObjectId> purchaseOrderIds = new ArrayList<>();

        for (InventoryOperation inventoryOperation : inventoryActions) {
            if (StringUtils.isBlank(inventoryOperation.getSourceId()) || !ObjectId.isValid(inventoryOperation.getSourceId())) {
                continue;
            }
            switch (inventoryOperation.getSourceType()) {
                case Transaction:
                case RefundTransaction:
                case ShippingManifest:
                    transactionIds.add(new ObjectId(inventoryOperation.getSourceId()));
                    break;
                case Reconciliation:
                    reconciliationIds.add(new ObjectId(inventoryOperation.getSourceId()));
                    break;
                case Transfer:
                    transferIds.add(new ObjectId(inventoryOperation.getSourceId()));
                    break;
                case PurchaseOrder:
                    purchaseOrderIds.add(new ObjectId(inventoryOperation.getSourceId()));
                    break;
            }
        }

        transactionHashMap = transactionRepository.listAsMap(filter.getCompanyId(), transactionIds);
        transferHistoryHashMap = inventoryTransferHistoryRepository.listAsMap(filter.getCompanyId(), transferIds);
        reconciliationHistoryHashMap = reconciliationHistoryRepository.listAsMap(filter.getCompanyId(), reconciliationIds);
        purchaseOrderHashMap = purchaseOrderRepository.listAsMap(filter.getCompanyId(), purchaseOrderIds);

        Product product;
        Prepackage prepackage;
        ProductBatch batch;
        Inventory inventory;
        Employee employee;
        PrepackageProductItem prepackageProductItem;
        StringBuilder employeeName = new StringBuilder();
        StringBuilder sourceDetail = new StringBuilder();
        Transaction transaction;
        ReconciliationHistory reconciliationHistory;
        InventoryTransferHistory transferHistory;
        PurchaseOrder purchaseOrder;
        double quantity = 0;
        double runningTotal = 0;
        ProductCategory.UnitType unitType = ProductCategory.UnitType.units;

        for (InventoryOperation inventoryOperation : inventoryActions) {
            employeeName = new StringBuilder();
            sourceDetail = new StringBuilder();
            HashMap<String, Object> data = new HashMap<>();

            product = productHashMap.get(inventoryOperation.getProductId());
            batch = productBatchHashMap.get(inventoryOperation.getBatchId());
            inventory = inventoryHashMap.get(inventoryOperation.getInventoryId());
            employee = employeeHashMap.get(inventoryOperation.getEmployeeId());
            prepackageProductItem = prepackageProductItemHashMap.get(inventoryOperation.getPrepackageItemId());
            prepackage = (prepackageProductItem != null) ? prepackageHashMap.get(prepackageProductItem.getPrepackageId()) : null;
            quantity = NumberUtils.round(inventoryOperation.getQuantity().doubleValue(), 2);
            runningTotal = NumberUtils.round(inventoryOperation.getRunningTotal().doubleValue(), 2);

            if (product != null && product.getCategory() != null) {
                unitType = product.getCategory().getUnitType();
            } else if (product != null && productCategoryHashMap.containsKey(product.getCategoryId())) {
                unitType = productCategoryHashMap.get(product.getCategoryId()).getUnitType();
            }

            if (employee != null) {
                employeeName.append(StringUtils.isNotBlank(employee.getFirstName()) ? employee.getFirstName() : "")
                        .append(StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : "");

            }
            if (StringUtils.isNotBlank(inventoryOperation.getPrepackageItemId())) {
                unitType = ProductCategory.UnitType.units;
            }
            switch (inventoryOperation.getSourceType()) {
                case Transaction:
                case RefundTransaction:
                case ShippingManifest:
                    transaction = transactionHashMap.get(inventoryOperation.getSourceId());
                    sourceDetail.append("Trans # ").append((transaction == null) ? "N/A" : transaction.getTransNo());
                    break;
                case Reconciliation:
                    reconciliationHistory = reconciliationHistoryHashMap.get(inventoryOperation.getSourceId());
                    sourceDetail.append("Rec # ").append((reconciliationHistory == null) ? "N/A" : reconciliationHistory.getRequestNo());
                    break;
                case Transfer:
                    transferHistory = transferHistoryHashMap.get(inventoryOperation.getSourceId());
                    sourceDetail.append("Transfer # ").append((transferHistory == null) ? "N/A" : transferHistory.getTransferNo());
                    break;
                case ProductBatch:
                    batch = productBatchHashMap.get(inventoryOperation.getSourceId());
                    if (inventoryOperation.getSubSourceAction() != null) {
                        if (InventoryOperation.SubSourceAction.AddBatch == inventoryOperation.getSubSourceAction()) {
                            sourceDetail.append("Add Batch : ").append((batch == null) ? "N/A" : (StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "N/A"));
                        } else if (InventoryOperation.SubSourceAction.AddPrepackage == inventoryOperation.getSubSourceAction()) {
                            sourceDetail.append("Update Batch : ").append((batch == null) ? "N/A" : (StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "N/A"));
                        }
                    } else {
                        sourceDetail.append("Batch : ").append((batch == null) ? "N/A" : (StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "N/A"));
                    }

                    break;
                case Prepackage:
                    prepackageProductItem = prepackageProductItemHashMap.get(inventoryOperation.getSourceId());
                    prepackage = (prepackageProductItem == null) ? null : prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (inventoryOperation.getSubSourceAction() != null) {

                        if (InventoryOperation.SubSourceAction.AddPrepackage == inventoryOperation.getSubSourceAction()) {
                            sourceDetail.append("Add Prepackage : ").append((prepackage == null) ? "N/A" : (StringUtils.isBlank(prepackage.getName()) ? "N/A" : prepackage.getName()));
                        } else if (InventoryOperation.SubSourceAction.ReducePrepackage == inventoryOperation.getSubSourceAction()) {
                            sourceDetail.append("Reduce Prepackage : ").append((prepackage == null) ? "N/A" : (StringUtils.isBlank(prepackage.getName()) ? "N/A" : prepackage.getName()));
                        } else if (InventoryOperation.SubSourceAction.DeletePrepackageItem == inventoryOperation.getSubSourceAction()) {
                            sourceDetail.append("Delete Prepackage line item : ").append((prepackage == null) ? "N/A" : (StringUtils.isBlank(prepackage.getName()) ? "N/A" : prepackage.getName()));
                        } else if (InventoryOperation.SubSourceAction.DeletePrepackage == inventoryOperation.getSubSourceAction()) {
                            sourceDetail.append("Delete Prepackage : ").append((prepackage == null) ? "N/A" : (StringUtils.isBlank(prepackage.getName()) ? "N/A" : prepackage.getName()));
                        }

                    } else {
                        sourceDetail.append("Prepackage : ").append((prepackage == null) ? "N/A" : (StringUtils.isBlank(prepackage.getName()) ? "N/A" : prepackage.getName()));
                    }
                    break;
                case PurchaseOrder:
                    purchaseOrder = purchaseOrderHashMap.get(inventoryOperation.getSourceId());
                    sourceDetail.append("PO # ").append((purchaseOrder == null) ? "N/A" : purchaseOrder.getPoNumber());
                    break;
                case System:
                    if (StringUtils.isNotBlank(inventoryOperation.getPrepackageItemId())) {
                        prepackageProductItem = prepackageProductItemHashMap.get(inventoryOperation.getSourceId());
                        prepackage = (prepackageProductItem != null) ? prepackageHashMap.get(prepackageProductItem.getPrepackageId()) : null;
                        sourceDetail.append("System(Prepackage) : ").append((prepackage == null) ? "N/A" : (StringUtils.isNotBlank(prepackage.getName()) ? prepackage.getName() : "N/A"));
                    } else {
                        batch = productBatchHashMap.get(inventoryOperation.getSourceId());
                        sourceDetail.append("System(Batch) : ").append((batch == null) ? "N/A" : (StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "N/A"));

                    }
                    break;
            }


            data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(inventoryOperation.getCreated(), filter.getTimezoneOffset()));
            data.put(attrs[1], inventoryOperation.getSourceType());
            data.put(attrs[2], sourceDetail);
            data.put(attrs[3], inventoryOperation.getAction());
            data.put(attrs[4], (product == null) ? "N/A" : (StringUtils.isNotBlank(product.getName()) ? product.getName() : "N/A"));
            data.put(attrs[5], (batch == null) ? "N/A" : (StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "N/A"));
            data.put(attrs[6], (prepackage == null) ? "N/A" : (StringUtils.isBlank(prepackage.getName()) ? "N/A" : prepackage.getName()));
            data.put(attrs[7], (inventory == null) ? "N/A" : (StringUtils.isNotBlank(inventory.getName()) ? inventory.getName() : "N/A"));
            data.put(attrs[8], StringUtils.isNotBlank(employeeName) ? employeeName : "N/A");
            data.put(attrs[9], quantity + " " + unitType.getType());
            data.put(attrs[10], runningTotal + " " + unitType.getType());


            report.add(data);
        }

        return report;
    }
}
