package com.fourtwenty.core.reporting.gather.impl.admin;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephen on 9/12/16.
 */
public class StorePerformanceReport implements Gatherer {
    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private ShopRepository shopRepository;
    private String[] attrs = new String[]{"Store", "Gross Receipts", "Total Transactions", "Total New Members", "Total Members"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public StorePerformanceReport(TransactionRepository transactionRepository, MemberRepository memberRepository, ShopRepository shopRepository) {
        this.transactionRepository = transactionRepository;
        this.memberRepository = memberRepository;
        this.shopRepository = shopRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER, GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Store Performance", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Long totalMembers = memberRepository.getActiveMemberCount(filter.getCompanyId());
        Long newMembers = memberRepository.getNewMembersFromToday(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        long transactionCount = 0;
        Double totalSales = 0d;

        for (Transaction t : transactions) {
            int factor = 1;
            if (Transaction.TransactionType.Refund.equals(t.getTransType()) && Cart.RefundOption.Retail.equals(t.getCart().getRefundOption())) {
                factor = -1;
            }
            transactionCount++;
            totalSales += t.getCart().getTotal().doubleValue() * factor;
        }
        HashMap<String, Object> data = new HashMap<>();
        data.put(attrs[0], shop.getName());
        data.put(attrs[1], new DollarAmount(totalSales));
        data.put(attrs[2], transactionCount);
        data.put(attrs[3], newMembers);
        data.put(attrs[4], totalMembers);
        report.add(data);
        return report;
    }
}
