package com.fourtwenty.core.reporting.gather.impl.admin;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.EmployeeSession;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TimeCardRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 7/11/2016.
 */
public class TimeClockGatherer implements Gatherer {
    private TimeCardRepository timeCardRepository;
    private EmployeeRepository employeeRepository;
    private TerminalRepository terminalRepository;
    private String[] attrs = new String[]{"Date", "Employee", "Clock In", "Terminal In", "Clock Out", "Terminal Out", "Time Clocked In", "iPad Sessions"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TimeClockGatherer(TimeCardRepository timeCardRepository,
                             EmployeeRepository employeeRepository,
                             TerminalRepository terminalRepository) {
        this.timeCardRepository = timeCardRepository;
        this.employeeRepository = employeeRepository;
        this.terminalRepository = terminalRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Time Clock Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        int timezoneOffset = 0;
        if (filter.getMap().containsKey("timezoneOffset")) {
            String offset = filter.getMap().get("timezoneOffset");
            timezoneOffset = Integer.parseInt(offset);
        }
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());

        Iterable<TimeCard> results = timeCardRepository.getAllTimeCards(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        for (TimeCard tc : results) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.dateStringWithOffset(tc.getCreated(), filter.getTimezoneOffset()));
            Employee emp = employeeMap.get(tc.getEmployeeId());
            if (emp != null) {
                data.put(attrs[1], emp.getFirstName() + " " + emp.getLastName());
            }
            data.put(attrs[2], ProcessorUtil.timeStampWithOffset(tc.getClockInTime(), filter.getTimezoneOffset()));

            String terminalName = "";
            if (!tc.getSessions().isEmpty()) {
                EmployeeSession firstSession = tc.getSessions().get(0);
                Terminal clockInTerminal = terminalMap.get(firstSession.getTerminalId());
                if (clockInTerminal != null) {
                    terminalName = clockInTerminal.getName();
                }
            }

            data.put(attrs[3], terminalName);

            data.put(attrs[4], ProcessorUtil.timeStampWithOffset(tc.getClockOutTime(), filter.getTimezoneOffset())); //clock out time
            int sessionCount = tc.getSessions().size();
            if (sessionCount > 1) {
                Terminal clockOutTerminal = terminalMap.get(tc.getSessions().get(sessionCount - 1).getTerminalId());
                data.put(attrs[5], clockOutTerminal
                        .getName()); //last session terminal name
            } else {
                data.put(attrs[5], data.get(attrs[3])); //if only 1 session, use the clock in terminal
            }

            //compute time worked
            final Period period = new Period(new DateTime(tc.getClockInTime()), new DateTime(tc.getClockOutTime()));
            System.out.print(period.getDays() + " days, ");
            System.out.print(period.getHours() + " hours, ");
            System.out.print(period.getMinutes() + " minutes, ");
            System.out.print(period.getSeconds() + " seconds.");
            StringBuilder diff = new StringBuilder();
            if (period.getYears() > 0) {
                diff.append(period.getYears() + "y");
            }
            if (period.getMonths() > 0) {
                diff.append(period.getMonths() + "m");
            }
            if (period.getDays() > 0) {
                diff.append(period.getDays() + "d");
            }
            diff.append(period.getHours() + "h");
            diff.append(period.getMinutes() + "m");

            long milliseconds = (tc.getClockOutTime() - tc.getClockInTime());
            int seconds = (int) (milliseconds / 1000) % 60;
            int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
            int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
            data.put(attrs[6], diff.toString()); //put in hours and minutes and seconds worked
            data.put(attrs[7], sessionCount); //add session count
            report.add(data);
        }

        return report;
    }
}
