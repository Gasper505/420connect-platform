package com.fourtwenty.core.reporting.gather.impl.company;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.TaxSummaryGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class CompanySalesByProduct implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = new String[]{"Product", "Brand", "Shop", "# of Transactions", "COGS", "Retail Value", "Discounts", "After Tax Discount", "Sales", "Excise Tax", "City Tax", "County Tax", "State Tax", "Total Tax", "Delivery Fee", "Gross Receipts"};

    public CompanySalesByProduct() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Company Sales By Product Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        //Get transactions by company
        Iterable<Transaction> transactionResults = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<Transaction> transactions = new ArrayList<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        LinkedHashSet<String> productIdList = new LinkedHashSet<>();
        for (Transaction transaction : transactionResults) {
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(orderItem.getProductId()) && ObjectId.isValid(orderItem.getProductId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                        productIdList.add(orderItem.getProductId());
                    }
                }
            }
            transactions.add(transaction);
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsByCompany(filter.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductBatch> batchHashMap = batchRepository.getBatchesForProductsMap(filter.getCompanyId(), Lists.newArrayList(productIdList));
        Iterable<Prepackage> prepackagesForProducts = prepackageRepository.getPrepackagesForProductsByCompany(filter.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        for (Prepackage prepackages : prepackagesForProducts) {
            prepackageHashMap.putIfAbsent(prepackages.getId(), prepackages);
        }

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Map.Entry<String, Product> entry : productMap.entrySet()) {
            Product product = entry.getValue();
            List<Product> items = productsByCompanyLinkId.get(product.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            productsByCompanyLinkId.put(product.getCompanyLinkId(), items);
        }

        List<ObjectId> brandIds = new ArrayList<>();
        List<ObjectId> shopIds = new ArrayList<>();

        //Create map on basis of product name (<Product NAME, SalesByProduct>)
        HashMap<String, SalesByProduct> salesMap = new HashMap<>();

        //Process transactions for calculation of taxes in respect to products
        int factor = 1;
        for (Transaction transaction : transactions) {
            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Cart cart = transaction.getCart();
            if (cart == null) {
                continue;
            }
            List<String> visited = new ArrayList<>();

            int totalFinalCost = 0;
            for (OrderItem item : transaction.getCart().getItems()) {
                totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
            }

            double avgDeliveryFee = 0;
            if (transaction.getCart().getItems().size() > 0) {
                avgDeliveryFee = transaction.getCart().getDeliveryFee().doubleValue() / transaction.getCart().getItems().size();
            }

            double avgAfterTaxDiscount = 0;
            if (transaction.getCart().getAppliedAfterTaxDiscount() != null && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                avgAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() / transaction.getCart().getItems().size();
            }

            if (cart != null && transaction.getCart().getItems() != null) {
                HashMap<String, Double> discountHasMap = new HashMap<>();
                taxSummaryGatherer.calculateOrderItemTax(cart, cart.getTaxResult(), discountHasMap, transaction);

                String shopId = transaction.getShopId();
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    Product product = productMap.get(orderItem.getProductId());

                    if (product != null && transaction.getTransType() == Transaction.TransactionType.Sale
                            || (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded)) {

                        String productName = product.getName().trim();
                        SalesByProduct salesByProduct = null;
                        if (salesMap.containsKey(productName)) {
                            salesByProduct = salesMap.get(productName);
                        } else {
                            salesByProduct = new SalesByProduct();
                            salesByProduct.name = productName;
                            salesByProduct.brand = product.getBrandId();

                            salesMap.put(productName, salesByProduct);
                        }

                        if (StringUtils.isNotBlank(shopId) && ObjectId.isValid(shopId)) {
                            shopIds.add(new ObjectId(shopId));
                        }

                        salesByProduct.txByShop.putIfAbsent(shopId, new ProductSaleByShop());
                        ProductSaleByShop productSaleByShop = salesByProduct.txByShop.get(shopId);
                        productSaleByShop.txNo.add(transaction.getTransNo());

                        // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                        if (!visited.contains(product.getId())) {
                            productSaleByShop.transCount++;
                            visited.add(product.getId());
                        }

                        if (StringUtils.isNotBlank(salesByProduct.brand) && ObjectId.isValid(salesByProduct.brand)) {
                            brandIds.add(new ObjectId(salesByProduct.brand));
                        }
                        int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                        double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                        double itemDiscount = orderItem.getCost().subtract(orderItem.getFinalPrice()).doubleValue();
                        double targetCartDiscount = transaction.getCart().getCalcCartDiscount().doubleValue() * ratio;
                        double targetCreditCardFee = transaction.getCart().getCreditCardFee().doubleValue() * ratio;

                        double sales = NumberUtils.round(orderItem.getFinalPrice().doubleValue() - targetCartDiscount, 5);

                        if (orderItem.getTaxResult() != null) {
                            productSaleByShop.cityTax += orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor;
                            productSaleByShop.countyTax += orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                            productSaleByShop.stateTax += orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor;
                            productSaleByShop.federalTax += orderItem.getTaxResult().getTotalFedTax().doubleValue() * factor;

                            productSaleByShop.exciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                            productSaleByShop.preALExciseTax += orderItem.getTaxResult().getOrderItemPreALExciseTax().doubleValue() * factor;
                            productSaleByShop.preNALExciseTax += orderItem.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;
                            productSaleByShop.postALExciseTax += orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                            productSaleByShop.postNALExciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                        }
                        productSaleByShop.retailValue += orderItem.getCost().doubleValue() * factor;
                        productSaleByShop.discounts += itemDiscount * factor;
                        productSaleByShop.deliveryFee += avgDeliveryFee * factor;
                        productSaleByShop.sales += sales * factor;
                        productSaleByShop.afterTaxDiscount += avgAfterTaxDiscount * factor;
                        productSaleByShop.creditCardFee += targetCreditCardFee * factor;

                        // calculate cost of goods
                        boolean calculated = false;
                        double cogs = 0;
                        if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                            // if prepackage is set, use prepackage
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null && targetBatch != null) {
                                    calculated = true;

                                    BigDecimal unitValue = prepackage.getUnitValue();
                                    if (unitValue == null || unitValue.doubleValue() == 0) {
                                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                        unitValue = weightTolerance.getUnitValue();
                                    }
                                    // calculate the total quantity based on the prepackage value
                                    cogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                }
                            }
                        } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                            // otherwise, use quantity logs
                            for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                                if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                    ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                    if (targetBatch != null) {
                                        calculated = true;
                                        cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                    }
                                }
                            }
                        }

                        if (!calculated) {
                            double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                            cogs = unitCost * orderItem.getQuantity().doubleValue();
                        }

                        productSaleByShop.cogs += cogs * factor;
                    }
                }
            }
        }

        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(shopIds));
        HashMap<String, Brand> brandMap = brandRepository.listAsMap(filter.getCompanyId());

        for (Map.Entry<String, SalesByProduct> entry : salesMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            SalesByProduct salesByProduct = entry.getValue();

            Brand brand = brandMap.get(salesByProduct.brand);

            data.put(attrs[0], salesByProduct.name);
            data.put(attrs[1], brand != null ? brand.getName() : "");

            int count = 0;
            for (Map.Entry<String, ProductSaleByShop> shopEntry : salesByProduct.txByShop.entrySet()) {
                Shop shop = shopMap.get(shopEntry.getKey());
                ProductSaleByShop saleByShop = shopEntry.getValue();
                if (count > 0) {
                    data = new HashMap<>();

                    data.put(attrs[0], "");
                    data.put(attrs[1], "");
                }
                if (shop != null) {
                    count = 1;

                    data.put(attrs[0], salesByProduct.name);
                    data.put(attrs[1], brand != null ? brand.getName() : "");
                    data.put(attrs[2], shop != null ? shop.getName() : "");
                    data.put(attrs[3], saleByShop.transCount);
                    data.put(attrs[4], new DollarAmount(saleByShop.cogs));
                    data.put(attrs[5], new DollarAmount(saleByShop.sales));
                    data.put(attrs[6], new DollarAmount(saleByShop.discounts));
                    data.put(attrs[7], new DollarAmount(saleByShop.afterTaxDiscount));
                    data.put(attrs[8], new DollarAmount(saleByShop.sales));
                    data.put(attrs[9], new DollarAmount(saleByShop.exciseTax));
                    data.put(attrs[10], new DollarAmount(saleByShop.cityTax));
                    data.put(attrs[11], new DollarAmount(saleByShop.countyTax));
                    data.put(attrs[12], new DollarAmount(saleByShop.stateTax));

                    double totalTax = saleByShop.cityTax + saleByShop.countyTax + saleByShop.stateTax + saleByShop.federalTax + saleByShop.postALExciseTax + saleByShop.postNALExciseTax;
                    data.put(attrs[13], new DollarAmount(totalTax));
                    data.put(attrs[14], new DollarAmount(saleByShop.deliveryFee));

                    double gross = saleByShop.sales + saleByShop.deliveryFee + totalTax + saleByShop.creditCardFee - saleByShop.afterTaxDiscount;
                    data.put(attrs[15], new DollarAmount(gross));

                    report.add(data);
                }
            }
        }

        return report;
    }

    private class SalesByProduct {
        private String name = StringUtils.EMPTY;
        private String brand = StringUtils.EMPTY;
        //<shopId, List of transaction>
        private HashMap<String, ProductSaleByShop> txByShop = new HashMap<>();

        public SalesByProduct() {
        }
    }

    private class ProductSaleByShop {
        HashSet<String> txNo = new HashSet<>();
        private double cogs = 0d;
        private int transCount = 0;
        private double retailValue = 0d;
        private double discounts = 0d;
        private double afterTaxDiscount = 0d;
        private double sales = 0d;
        private double exciseTax = 0d;
        private double cityTax = 0d;
        private double countyTax = 0d;
        private double stateTax = 0d;
        private double federalTax = 0d;
        private double deliveryFee = 0d;
        private double creditCardFee = 0d;
        private double preALExciseTax = 0d;
        private double preNALExciseTax = 0d;
        private double postALExciseTax = 0d;
        private double postNALExciseTax = 0d;

        public ProductSaleByShop() {
        }
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}
