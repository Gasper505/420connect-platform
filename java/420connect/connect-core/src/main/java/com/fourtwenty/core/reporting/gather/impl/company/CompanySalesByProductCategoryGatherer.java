package com.fourtwenty.core.reporting.gather.impl.company;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.TaxSummaryGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class CompanySalesByProductCategoryGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = new String[]{"Product Category", "Shop", "# of Transactions", "COGS", "Retail Value", "Discounts", "After Tax Discount", "Sales", "Excise Tax", "City Tax", "County Tax", "State Tax", "Total Tax", "Delivery Fee", "Gross Receipts"};

    public CompanySalesByProductCategoryGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Company Sales By Product Category Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        //Get transactions by company
        Iterable<Transaction> transactionResults = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<Transaction> transactions = new ArrayList<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        LinkedHashSet<String> productIdList = new LinkedHashSet<>();
        for (Transaction transaction : transactionResults) {
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                        productIdList.add(orderItem.getProductId());
                    }
                }
            }
            transactions.add(transaction);
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAsMap(filter.getCompanyId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsByCompany(filter.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductBatch> batchHashMap = batchRepository.getBatchesForProductsMap(filter.getCompanyId(), Lists.newArrayList(productIdList));
        Iterable<Prepackage> prepackagesForProducts = prepackageRepository.getPrepackagesForProductsByCompany(filter.getCompanyId(), Lists.newArrayList(productIdList));
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        return prepareCompanySalesByCategory(filter, transactionResults, productMap, categoryMap, batchHashMap, weightToleranceHashMap, prepackagesForProducts, prepackageProductItemHashMap);
    }

    public GathererReport prepareCompanySalesByCategory(ReportFilter filter, Iterable<Transaction> transactions, HashMap<String , Product> productMap, HashMap<String, ProductCategory> categoryMap, HashMap<String, ProductBatch> batchHashMap,  HashMap<String, ProductWeightTolerance> weightToleranceHashMap,  Iterable<Prepackage> prepackagesForProducts, HashMap<String, PrepackageProductItem> prepackageProductItemHashMap) {

        GathererReport report = new GathererReport(filter, "Company Sales By Product Category Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        List<ObjectId> shopIds = new ArrayList<>();


        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Map.Entry<String, Product> entry : productMap.entrySet()) {
            Product product = entry.getValue();
            List<Product> items = productsByCompanyLinkId.get(product.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            productsByCompanyLinkId.put(product.getCompanyLinkId(), items);
        }

        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        for (Prepackage prepackages : prepackagesForProducts) {
            prepackageHashMap.putIfAbsent(prepackages.getId(), prepackages);
        }

        //Create map on basis of product category name (<Product Category NAME, ProductCategorySale>)
        HashMap<String, SalesByProductCategory> salesMap = new HashMap<>();

        for (Transaction transaction : transactions) {

            Cart cart = transaction.getCart();
            if (cart == null) {
                continue;
            }

            if (transaction.getCart().getItems() != null) {
                taxSummaryGatherer.calculateOrderItemTax(cart, cart.getTaxResult(), new HashMap<String, Double>(), transaction);
            }
            List<String> visited = new ArrayList<>();

            int totalFinalCost = 0;
            for (OrderItem item : transaction.getCart().getItems()) {
                totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
            }

            double avgDeliveryFee = 0;
            if (transaction.getCart().getItems().size() > 0) {
                avgDeliveryFee = transaction.getCart().getDeliveryFee().doubleValue() / transaction.getCart().getItems().size();
            }

//            double avgAfterTaxDiscount = 0;
//            if (transaction.getCart().getAppliedAfterTaxDiscount() != null && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
//                avgAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() / transaction.getCart().getItems().size();
//            }

            String shopId = transaction.getShopId();
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                Product product = productMap.get(orderItem.getProductId());
                ProductCategory productCategory = product != null ? categoryMap.get(product.getCategoryId()) : null;

                if (product != null && productCategory != null && transaction.getTransType() == Transaction.TransactionType.Sale
                        || (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded)) {
                    String categoryName = productCategory.getName().trim();

                    int factor = 1;
                    if (Transaction.TransactionType.Refund == transaction.getTransType() && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                        factor = -1;
                    }

                    SalesByProductCategory salesByProductCategory = null;
                    if (salesMap.containsKey(categoryName)) {
                        salesByProductCategory = salesMap.get(categoryName);
                    } else {
                        salesByProductCategory = new SalesByProductCategory();
                        salesByProductCategory.name = categoryName;

                        salesMap.put(categoryName, salesByProductCategory);
                    }

                    if (StringUtils.isNotBlank(shopId) && ObjectId.isValid(shopId)) {
                        shopIds.add(new ObjectId(shopId));
                    }

                    int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                    double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                    double itemDiscount = orderItem.getCost().subtract(orderItem.getFinalPrice()).doubleValue();
                    double targetCartDiscount = transaction.getCart().getCalcCartDiscount().doubleValue() * ratio;
                    double targetCreditCardFees = NumberUtils.round(transaction.getCart().getCreditCardFee().doubleValue() * ratio, 6);
                    double sales = NumberUtils.round(orderItem.getFinalPrice().doubleValue() - targetCartDiscount, 5);
                    double avgAfterTaxDiscount = NumberUtils.round(transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio, 6);

                    salesByProductCategory.txByShop.putIfAbsent(shopId, new CategorySaleByShop());
                    CategorySaleByShop categorySaleByShop = salesByProductCategory.txByShop.get(shopId);
                    categorySaleByShop.txNo.add(transaction.getTransNo());

                    if (orderItem.getTaxResult() != null) {
                        categorySaleByShop.cityTax += (orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor);
                        categorySaleByShop.countyTax += (orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor);
                        categorySaleByShop.stateTax += (orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor);
                        categorySaleByShop.federalTax += orderItem.getTaxResult().getTotalFedTax().doubleValue() * factor;

                        categorySaleByShop.exciseTax += (orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor);

                        categorySaleByShop.preALExciseTax += orderItem.getTaxResult().getOrderItemPreALExciseTax().doubleValue() * factor;
                        categorySaleByShop.preNALExciseTax += orderItem.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;
                        categorySaleByShop.postALExciseTax += orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                        categorySaleByShop.postNALExciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;
                    }
                    categorySaleByShop.retailValue += (orderItem.getCost().doubleValue() * factor);
                    categorySaleByShop.discounts += (targetCartDiscount * factor);
                    categorySaleByShop.deliveryFee += (avgDeliveryFee * factor);
                    categorySaleByShop.sales += (sales * factor);
                    categorySaleByShop.afterTaxDiscount += (avgAfterTaxDiscount * factor);
                    categorySaleByShop.creditCardFee += (targetCreditCardFees * factor);


                    // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                    if (!visited.contains(product.getCategoryId())) {
                        categorySaleByShop.transCount++;
                        visited.add(product.getCategoryId());
                    }

                    // calculate cost of goods
                    boolean calculated = false;
                    double cogs = 0;
                    if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        // if prepackage is set, use prepackage
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null && targetBatch != null) {
                                calculated = true;

                                BigDecimal unitValue = prepackage.getUnitValue();
                                if (unitValue == null || unitValue.doubleValue() == 0) {
                                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                    unitValue = weightTolerance.getUnitValue();
                                }
                                // calculate the total quantity based on the prepackage value
                                cogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                            }
                        }
                    } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                }
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                        cogs = unitCost * orderItem.getQuantity().doubleValue();
                    }

                    categorySaleByShop.cogs += (cogs * factor);
                }
            }
        }

        //Get shop's for requested company
        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(shopIds));

        for (Map.Entry<String, SalesByProductCategory> categoryEntry : salesMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            SalesByProductCategory sales = categoryEntry.getValue();

            data.put(attrs[0], sales.name);

            int count = 0;
            for (Map.Entry<String, CategorySaleByShop> shopEntry : sales.txByShop.entrySet()) {
                Shop shop = shopMap.get(shopEntry.getKey());
                CategorySaleByShop saleByShop = shopEntry.getValue();
                if (count > 0) {
                    data = new HashMap<>();

                    data.put(attrs[0], "");
                }
                if (shop != null) {
                    count = 1;

                    data.put(attrs[0], sales.name);
                    data.put(attrs[1], shop != null ? shop.getName() : "");
                    data.put(attrs[2], saleByShop.transCount);
                    data.put(attrs[3], new DollarAmount(saleByShop.cogs));
                    data.put(attrs[4], new DollarAmount(saleByShop.retailValue));
                    data.put(attrs[5], new DollarAmount(saleByShop.discounts));
                    data.put(attrs[6], new DollarAmount(saleByShop.afterTaxDiscount));
                    data.put(attrs[7], new DollarAmount(saleByShop.sales));
                    data.put(attrs[8], new DollarAmount(saleByShop.exciseTax));
                    data.put(attrs[9], new DollarAmount(saleByShop.cityTax));
                    data.put(attrs[10], new DollarAmount(saleByShop.countyTax));
                    data.put(attrs[11], new DollarAmount(saleByShop.stateTax));

                    double totalTax = saleByShop.cityTax + saleByShop.countyTax + saleByShop.stateTax + saleByShop.federalTax + saleByShop.postNALExciseTax + saleByShop.postALExciseTax;
                    data.put(attrs[12], new DollarAmount(totalTax));
                    data.put(attrs[13], new DollarAmount(saleByShop.deliveryFee));

                    double gross = saleByShop.sales + saleByShop.deliveryFee + totalTax + saleByShop.creditCardFee - saleByShop.afterTaxDiscount;
                    data.put(attrs[14], new DollarAmount(gross));

                    report.add(data);
                }
            }

        }


        return report;
    }

    private class SalesByProductCategory {
        private String name = StringUtils.EMPTY;
        //<shopId, List of transaction>
        private HashMap<String, CategorySaleByShop> txByShop = new HashMap<>();

        public SalesByProductCategory() {
        }
    }

    private class CategorySaleByShop {
        HashSet<String> txNo = new HashSet<>();
        private double cogs = 0d;
        private int transCount = 0;
        private double retailValue = 0d;
        private double discounts = 0d;
        private double afterTaxDiscount = 0d;
        private double sales = 0d;
        private double exciseTax = 0d;
        private double cityTax = 0d;
        private double countyTax = 0d;
        private double stateTax = 0d;
        private double federalTax = 0d;
        private double deliveryFee = 0d;
        private double creditCardFee = 0d;
        private double preALExciseTax = 0d;
        private double preNALExciseTax = 0d;
        private double postALExciseTax = 0d;
        private double postNALExciseTax = 0d;

        public CategorySaleByShop() {
        }
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

}
