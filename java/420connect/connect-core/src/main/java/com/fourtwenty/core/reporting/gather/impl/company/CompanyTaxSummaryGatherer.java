package com.fourtwenty.core.reporting.gather.impl.company;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.TaxSummaryGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

public class CompanyTaxSummaryGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{"Shop", "Non-Cannabis Sale", "Cannabis Sale", "City Tax", "County Tax", "State Tax", "Excise Tax", "Total Tax", "Gross Receipts"};

    public CompanyTaxSummaryGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Company Tax Summary Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        //Get shop's for requested company
        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId());

        //Get transactions by company
        Iterable<Transaction> transactionResults = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<Transaction> transactions = new ArrayList<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        for (Transaction transaction : transactionResults) {
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
            transactions.add(transaction);
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAsMap(filter.getCompanyId());

        //<shopId, CompanyTaxSummary>
        int factor = 1;
        HashMap<String, ShopTaxSummary> taxSummaryMap = new HashMap<>();

        for (Transaction transaction : transactions) {
            factor = 1;
            if (Transaction.TransactionType.Refund == transaction.getTransType() && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            ShopTaxSummary taxSummary = null;
            if (taxSummaryMap.get(transaction.getShopId()) != null) {
                taxSummary = taxSummaryMap.get(transaction.getShopId());
            } else {
                taxSummary = new ShopTaxSummary();
                taxSummary.shopId = transaction.getShopId();

                Shop shop = shopMap.get(transaction.getShopId());
                if (shop != null) {
                    taxSummary.shopName = shop.getName();
                }
                taxSummaryMap.putIfAbsent(transaction.getShopId(), taxSummary);
            }

            Cart cart = transaction.getCart();
            if (cart != null) {
                taxSummaryGatherer.calculateOrderItemTax(cart, cart.getTaxResult(), new HashMap<String, Double>(), transaction);

                if (transaction.getCart().getItems() != null) {
                    for (OrderItem orderItem : transaction.getCart().getItems()) {
                        Product product = productMap.get(orderItem.getProductId());

                        if (product != null && categoryMap.get(product.getCategoryId()) != null && transaction.getTransType() == Transaction.TransactionType.Sale
                                || (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail
                                && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded)) {

                            ProductCategory category = categoryMap.get(product.getCategoryId());

                            if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                                    || (product.getCannabisType() != Product.CannabisType.CBD
                                    && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                                    && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                                taxSummary.cannabisSale += orderItem.getCost().doubleValue() * factor;
                            } else {
                                taxSummary.nonCannabisSale += orderItem.getCost().doubleValue() * factor;
                            }

                            if (orderItem.getTaxResult() != null) {

                                taxSummary.cityTax += orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor;
                                taxSummary.countyTax += orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                                taxSummary.stateTax += orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor;
                                taxSummary.exciseTax += (orderItem.getExciseTax().doubleValue() == 0 ? orderItem.getTaxResult().getTotalExciseTax().doubleValue() : orderItem.getExciseTax().doubleValue()) * factor;

                            }
                        }
                    }
                }
            }
        }

        for (Map.Entry<String, ShopTaxSummary> entry : taxSummaryMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            ShopTaxSummary summary = entry.getValue();

            data.put(attrs[0], summary.shopName);
            data.put(attrs[1], new DollarAmount(summary.nonCannabisSale));
            data.put(attrs[2], new DollarAmount(summary.cannabisSale));
            data.put(attrs[3], new DollarAmount(summary.cityTax));
            data.put(attrs[4], new DollarAmount(summary.countyTax));
            data.put(attrs[5], new DollarAmount(summary.stateTax));
            data.put(attrs[6], new DollarAmount(summary.exciseTax));

            double totalTax = summary.cityTax + summary.countyTax + summary.stateTax + summary.exciseTax;
            data.put(attrs[7], new DollarAmount(NumberUtils.round(totalTax, 2)));

            double gross = NumberUtils.round(summary.cannabisSale, 2) + NumberUtils.round(summary.nonCannabisSale, 2) +
                    NumberUtils.round(totalTax, 2);
            data.put(attrs[8], new DollarAmount(gross));

            report.add(data);
        }


        return report;
    }

    private class ShopTaxSummary {
        private String shopId = StringUtils.EMPTY;
        private String shopName = StringUtils.EMPTY;
        private double nonCannabisSale = 0d;
        private double cannabisSale = 0d;
        private double cityTax = 0d;
        private double countyTax = 0d;
        private double stateTax = 0d;
        private double exciseTax = 0d;
        private double totalTax = 0d;

        public ShopTaxSummary() {
        }
    }

}
