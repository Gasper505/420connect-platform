package com.fourtwenty.core.reporting.gather.impl.company;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

public class InventoryByCompany implements Gatherer {

    @Inject
    private ProductRepository productRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ShopRepository shopRepository;

    private List<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{"Product", "Brand", "Category", "Vendor", "Price Per Unit", "Cost Per Unit", "Type", "Company Total Inventory"};

    public InventoryByCompany() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Product> products = productRepository.list(filter.getCompanyId());

        LinkedHashSet<ObjectId> shopIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> brandIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> categoryIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        //<product name, InventorySummary>
        Map<String, InventorySummary> summaryMap = new HashMap<>();

        for (Product product : products) {
            if (!product.isActive() || product.isDeleted()) {
                continue;
            }

            String name = product.getName().trim();

            String productId = product.getId();
            InventorySummary inventorySummary = null;
            if (summaryMap.containsKey(name)) {
                inventorySummary = summaryMap.get(name);
            } else {
                inventorySummary = new InventorySummary();
                inventorySummary.productId = productId;
                inventorySummary.productName = product.getName();

                if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                    brandIds.add(new ObjectId(product.getBrandId()));
                }
                if (StringUtils.isNotBlank(product.getVendorId()) && ObjectId.isValid(product.getVendorId())) {
                    vendorIds.add(new ObjectId(product.getVendorId()));
                }
                if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                    categoryIds.add(new ObjectId(product.getCategoryId()));
                }
                if (StringUtils.isNotBlank(product.getShopId()) && ObjectId.isValid(product.getShopId())) {
                    shopIds.add(new ObjectId(product.getShopId()));
                }

                inventorySummary.brand = product.getBrandId();
                inventorySummary.category = product.getCategoryId();
                inventorySummary.vendor = product.getVendorId();

                summaryMap.put(productId, inventorySummary);
            }

            inventorySummary.pricePerUnit = product.getUnitPrice().doubleValue();
            inventorySummary.saleType = product.getProductSaleType();

            double quantity = 0d;
            for (ProductQuantity productQuantity : product.getQuantities()) {
                quantity += productQuantity.getQuantity().doubleValue();
            }

            Map<String, Double> inventoryMap = inventorySummary.shopInventoryMap;
            if (inventoryMap.containsKey(product.getShopId())) {
                Double pQuantity = inventoryMap.get(product.getShopId());
                inventoryMap.put(product.getShopId(), quantity + pQuantity);
            } else {
                inventoryMap.put(product.getShopId(), quantity);
            }

        }

        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Brand> brandMap = brandRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Vendor> vendorMap = vendorRepository.listAllAsMap(filter.getCompanyId());

        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(shopIds));

        for (Map.Entry<String, Shop> entry : shopMap.entrySet()) {
            Shop shop = entry.getValue();
            if (shop != null) {
                reportHeaders.add(shop.getName() + " Total Inventory");
                fieldTypes.put(shop.getName() + " Total Inventory", GathererReport.FieldType.NUMBER);
            }
        }

        reportHeaders.add("Company Total Inventory Valuation");
        fieldTypes.put("Company Total Inventory Valuation", GathererReport.FieldType.CURRENCY);

        for (Map.Entry<String, Shop> entry : shopMap.entrySet()) {
            Shop shop = entry.getValue();

            if (shop != null) {
                reportHeaders.add(shop.getName() + " Total Inventory Valuation ");
                fieldTypes.put(shop.getName() + " Total Inventory Valuation ", GathererReport.FieldType.CURRENCY);
            }
        }

        GathererReport report = new GathererReport(filter, "Company Sales Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        for (Map.Entry<String, InventorySummary> entry : summaryMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            InventorySummary summary = entry.getValue();

            data.put(attrs[0], summary.productName);

            Brand brand = brandMap.get(summary.brand);
            ProductCategory category = categoryMap.get(summary.category);
            Vendor vendor = vendorMap.get(summary.vendor);

            data.put(attrs[1], brand != null ? brand.getName() : "");
            data.put(attrs[2], category != null ? category.getName() : "");
            data.put(attrs[3], vendor != null ? vendor.getName() : "");
            data.put(attrs[4], new DollarAmount(summary.pricePerUnit));
            data.put(attrs[5], new DollarAmount(summary.pricePerUnit));
            data.put(attrs[6], summary.saleType);

            double companyTotalInventory = summary.shopInventoryMap.values().stream().mapToDouble(Double::doubleValue).sum();
            data.put(attrs[7], companyTotalInventory);

            double companyTotal = 0d;


            for (Map.Entry<String, Shop> shopMapEntry : shopMap.entrySet()) { //All shops for products
                Shop shop = shopMap.get(shopMapEntry.getKey());
                boolean available = false;
                for (Map.Entry<String, Double> shopEntry : summary.shopInventoryMap.entrySet()) { //current product's shop
                    if (shop != null && shop.getId().equalsIgnoreCase(shopEntry.getKey())) {
                        double inventoryValuation = shopEntry.getValue() * summary.pricePerUnit;
                        companyTotal += inventoryValuation;
                        data.put(shop.getName() + " Total Inventory", shopEntry.getValue());

                        data.put(shop.getName() + " Total Inventory Valuation", new DollarAmount(inventoryValuation));
                        available = true;
                    }
                }
                if (!available) {
                    data.put(shop.getName() + " Total Inventory", 0);

                    data.put(shop.getName() + " Total Inventory Valuation", new DollarAmount(0d));
                }
            }
            data.put("Company Total Inventory Valuation ", new DollarAmount(companyTotal));

            report.add(data);
        }


        return report;
    }

    private class InventorySummary {

        private String productId = StringUtils.EMPTY;
        private String productName = StringUtils.EMPTY;
        private String brand = StringUtils.EMPTY;
        private String category = StringUtils.EMPTY;
        private String vendor = StringUtils.EMPTY;
        private double pricePerUnit = 0d;
        private double costPerUnit = 0d;
        private Product.ProductSaleType saleType;
        private Map<String, Double> shopInventoryMap = new HashMap<>();

        public InventorySummary() {
        }
    }
}
