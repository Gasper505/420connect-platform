package com.fourtwenty.core.reporting.gather.impl.company;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.gather.impl.transaction.TaxSummaryGatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class SalesByCompanyGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;
    @Inject
    private MemberRepository memberRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{"Shop", "Recreational Number of Transactions", "Recreational Sales", "Medicinal Number of Transactions", "Medicinal Sales", "Total Sales", "Non-Cannabis Sale", "Cannabis Sale", "Total Discounts", "After Tax Discount", "City Tax", "County Tax", "State Tax", "Total Tax", "Delivery Fee", "Gross Receipts"};

    public SalesByCompanyGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.WHOLE_NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.WHOLE_NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Company Sales Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        //Get transaction for company
        Iterable<Transaction> results = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        //Process transaction for getting product, member's id list
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
            transactions.add(transaction);
        }

        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ShopSales> shopSalesMap = new HashMap<>();
        //Process transaction
        for (Transaction transaction : transactions) {

            ShopSales shopSales = null;
            if (shopSalesMap.containsKey(transaction.getShopId())) {
                shopSales = shopSalesMap.get(transaction.getShopId());
            } else {
                shopSales = new ShopSales();
                shopSales.shopId = transaction.getShopId();

                Shop shop = shopMap.get(transaction.getShopId());
                if (shop != null) {
                    shopSales.shopName = shop.getName();
                }
                shopSalesMap.put(transaction.getShopId(), shopSales);
            }

            Cart cart = transaction.getCart();

            int factor = 1;
            if (Transaction.TransactionType.Refund == transaction.getTransType() && cart.getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            if (cart != null) {
                //Re-calculate taxes if order item's tax result is null
                taxSummaryGatherer.calculateOrderItemTax(cart, cart.getTaxResult(), new HashMap<String, Double>(), transaction);

                BigDecimal sale = cart.getSubTotal();
                //Calculate medicinal/recreational sale and respective number of transactions
                ConsumerType consumerType = cart.getFinalConsumerTye();

                if (consumerType.equals(ConsumerType.AdultUse)) {
                    shopSales.recreationalSale += (sale.doubleValue() * factor);
                    shopSales.recreationalNoOfTx += 1;
                } else {
                    shopSales.medicinalSale += (sale.doubleValue() * factor);
                    shopSales.medicinalNoOfTx += 1;
                }

                shopSales.totalDiscounts += (cart.getCalcCartDiscount().doubleValue() * factor);
                shopSales.deliveryFee += (cart.getDeliveryFee().doubleValue() * factor);
                shopSales.afterTaxDiscount += (cart.getAppliedAfterTaxDiscount().doubleValue() * factor);

                if (transaction.getCart().getItems() != null) {
                    for (OrderItem orderItem : transaction.getCart().getItems()) {
                        Product product = productMap.get(orderItem.getProductId());

                        if (transaction.getTransType() == Transaction.TransactionType.Sale
                                || (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail
                                && orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) && product != null && categoryMap.get(product.getCategoryId()) != null) {

                            //Calculate cannabis/non-cannabis sale
                            ProductCategory category = categoryMap.get(product.getCategoryId());
                            if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                                    || (product.getCannabisType() != Product.CannabisType.CBD
                                    && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                                    && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                                shopSales.cannabisSale += (orderItem.getCost().doubleValue() * factor);
                            } else {
                                shopSales.nonCannabisSale += (orderItem.getCost().doubleValue() * factor);
                            }

                            //Process taxes
                            if (orderItem.getTaxResult() != null) {
                                shopSales.cityTax += (orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor);
                                shopSales.countyTax += (orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor);
                                shopSales.stateTax += (orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor);
                            }
                        }
                    }
                }

            }
        }

        for (Map.Entry<String, ShopSales> entry : shopSalesMap.entrySet()) {
            HashMap<String, Object> data = new HashMap<>();
            ShopSales shopSales = entry.getValue();

            double totalSale = NumberUtils.round(shopSales.cannabisSale, 2) + NumberUtils.round(shopSales.nonCannabisSale, 2);
            double totalTax = NumberUtils.round(shopSales.cityTax, 2) + NumberUtils.round(shopSales.countyTax, 2) + NumberUtils.round(shopSales.stateTax, 2);

            double grossReceipts = totalSale + totalTax - shopSales.totalDiscounts;

            data.put(attrs[0], shopSales.shopName);
            data.put(attrs[1], shopSales.recreationalNoOfTx);
            data.put(attrs[2], new DollarAmount(shopSales.recreationalSale));
            data.put(attrs[3], shopSales.medicinalNoOfTx);
            data.put(attrs[4], new DollarAmount(shopSales.medicinalSale));
            data.put(attrs[5], new DollarAmount(totalSale));
            data.put(attrs[6], new DollarAmount(shopSales.nonCannabisSale));
            data.put(attrs[7], new DollarAmount(shopSales.cannabisSale));
            data.put(attrs[8], new DollarAmount(shopSales.totalDiscounts));
            data.put(attrs[9], new DollarAmount(shopSales.afterTaxDiscount));
            data.put(attrs[10], new DollarAmount(shopSales.cityTax));
            data.put(attrs[11], new DollarAmount(shopSales.countyTax));
            data.put(attrs[12], new DollarAmount(shopSales.stateTax));
            data.put(attrs[13], new DollarAmount(totalTax));
            data.put(attrs[14], new DollarAmount(shopSales.deliveryFee));
            data.put(attrs[15], new DollarAmount(grossReceipts));

            report.add(data);
        }


        return report;
    }

    private class ShopSales {
        private String shopId;
        private String shopName;
        private int recreationalNoOfTx;
        private double recreationalSale;
        private int medicinalNoOfTx;
        private double medicinalSale;
        private double cannabisSale;
        private double nonCannabisSale;
        private double totalDiscounts;
        private double cityTax;
        private double countyTax;
        private double stateTax;
        private double deliveryFee;
        private double afterTaxDiscount;

        public ShopSales() {
        }
    }
}
