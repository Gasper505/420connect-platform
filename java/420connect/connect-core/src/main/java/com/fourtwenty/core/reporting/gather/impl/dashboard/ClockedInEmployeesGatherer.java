package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.TimeCardCompositeResult;
import com.fourtwenty.core.services.mgmt.TimeCardService;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 5/22/2016.
 */
public class ClockedInEmployeesGatherer implements Gatherer {
    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final TimeCardService timeCardService;
    private String[] attrs = new String[]{"First Name", "Last Name", "Position Title"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ClockedInEmployeesGatherer(EmployeeRepository repository, RoleRepository roleRepository, TimeCardService timeCardService) {
        this.employeeRepository = repository;
        this.roleRepository = roleRepository;
        this.timeCardService = timeCardService;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Clocked-In Employees Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
        report.setReportFieldTypes(fieldTypes);
        SearchResult<TimeCardCompositeResult> compositeResultSearchResult = timeCardService.getActiveTimeCards();

        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(filter.getCompanyId());

        for (TimeCardCompositeResult result : compositeResultSearchResult.getValues()) {
            Employee cie = result.getEmployee();
            HashMap<String, Object> data = new HashMap(reportHeaders.size());

            Role role = roleHashMap.get(cie.getRoleId());
            String roleName = "";
            if (role != null) {
                roleName = role.getName();
            }
            data.put(attrs[0], cie.getFirstName());
            data.put(attrs[1], cie.getLastName());
            data.put(attrs[2], roleName);
            report.add(data);
        }
        return report;
    }
}
