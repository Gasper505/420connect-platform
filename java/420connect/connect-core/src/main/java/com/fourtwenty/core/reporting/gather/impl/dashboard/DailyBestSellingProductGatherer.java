package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.BestSellingProduct;

import java.util.*;

/**
 * Created by Stephen Schmidt on 5/30/2016.
 */
public class DailyBestSellingProductGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private String[] attrs = new String[]{"Product Name", "Total Sales"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DailyBestSellingProductGatherer(TransactionRepository transactionRepository, ProductRepository productRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Daily Best Selling Product Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());


        Iterable<BestSellingProduct> bsp = transactionRepository.getDailyBestSellingProducts(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Object> data;
        int size = filter.getMap().get("size") != null ? Integer.parseInt(filter.getMap().get("size")) : 1;
        if (bsp != null && bsp.iterator().hasNext()) {
            Iterator<BestSellingProduct> it = bsp.iterator();
            for (int i = 0; i < size && it.hasNext(); i++) {
                data = new HashMap<>();
                BestSellingProduct p = it.next();
                Product product = productMap.get(p.getId());
                String productName = product != null ? product.getName() : "";
                data.put(attrs[0], productName);
                data.put(attrs[1], p.getSales());
                report.add(data);
            }
        }

        return report;
    }
}
