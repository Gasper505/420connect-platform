package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 5/25/2016.
 */
public class DailySalesTotalGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Date", "Today", "% Difference", "Yesterday"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DailySalesTotalGatherer(TransactionRepository repository) {
        this.transactionRepository = repository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.DATE,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.PERCENTAGE,
                GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Daily Sales Total Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);


        DateTime yesterdayStart = new DateTime(filter.getTimeZoneStartDateMillis()).minusDays(1).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset());
        DateTime yesterdayEnd = new DateTime(filter.getTimeZoneStartDateMillis()).minusSeconds(1);

        Iterable<Transaction> todaysResults = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Iterable<Transaction> yesterdaysResults = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), yesterdayStart.getMillis(), yesterdayEnd.getMillis());

        double todayTotal = 0.0;
        int factor = 1;
        for (Transaction t : todaysResults) {
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            double total = t.getCart().getTotal().doubleValue() * factor;
            if (t.getTransType() == Transaction.TransactionType.Refund
                    && t.getCart().getRefundOption() == Cart.RefundOption.Void
                    && t.getCart().getSubTotal().doubleValue() == 0) {
                total = 0;
            }

            todayTotal += total;
        }
        double yestTotal = 0.0;
        for (Transaction t : yesterdaysResults) {
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            double total = t.getCart().getTotal().doubleValue() * factor;
            if (t.getTransType() == Transaction.TransactionType.Refund
                    && t.getCart().getRefundOption() == Cart.RefundOption.Void
                    && t.getCart().getSubTotal().doubleValue() == 0) {
                total = 0;
            }

            yestTotal += total;
        }
        HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
        data.put(attrs[0], ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()));
        String total1 = new DecimalFormat("#.##").format(todayTotal);
        data.put(attrs[1], total1);
        String total2 = new DecimalFormat("#.##").format(yestTotal);
        data.put(attrs[3], total2);
        if (yestTotal != 0) {
            String prefix = "";
            Double pcnt = (todayTotal / yestTotal) * 100 - 100;
            if (pcnt > 0) {
                prefix = "+";
            }
            String pctDiff = new DecimalFormat("#.##").format(pcnt);
            data.put(attrs[2], prefix + pctDiff);
        } else {
            data.put(attrs[2], "");
        }


        report.add(data);

        return report;
    }


}
