package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 5/26/2016.
 */
public class DailyTotalVisitsGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Date", "Today", "% Difference", "Yesterday"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DailyTotalVisitsGatherer(TransactionRepository repository) {
        this.transactionRepository = repository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.DATE, GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.PERCENTAGE, GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Daily Total Visits Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        DateTime today = DateTime.now();
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(today.getMillis(), filter.getTimezoneOffset()));


        Long count = transactionRepository.getDailyTotalVisits(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        DateTime yesterdayStart = new DateTime(filter.getTimeZoneStartDateMillis()).minusDays(1).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset());
        DateTime yesterdayEnd = new DateTime(filter.getTimeZoneStartDateMillis()).minusSeconds(1);


        Long yesterdayCount = transactionRepository.getDailyTotalVisits(filter.getCompanyId(), filter.getShopId(), yesterdayStart.getMillis(), yesterdayEnd.getMillis());

        String differenceStr = "";
        if (yesterdayCount != 0) {
            String prefix = "";
            Double pcnt = count / (double) yesterdayCount * 100 - 100;
            if (pcnt > 0) {
                prefix = "+";
            }
            String pctDiff = new DecimalFormat("#.##").format(pcnt);
            differenceStr = prefix + pctDiff;
        } else {
            differenceStr = "+" + count;
        }


        HashMap<String, Object> data = new HashMap<>(reportHeaders.size());

        data.put(attrs[0], ProcessorUtil.dateStringWithOffset(today.getMillis(), filter.getTimezoneOffset()));
        data.put(attrs[1], count);
        data.put(attrs[2], differenceStr); // difference
        data.put(attrs[3], yesterdayCount);
        report.add(data);

        return report;
    }
}
