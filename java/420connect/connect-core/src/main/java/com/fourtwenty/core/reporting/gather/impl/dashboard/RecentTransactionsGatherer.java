package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * Created by Stephen Schmidt on 5/22/2016.
 */
public class RecentTransactionsGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private String[] attrs = new String[]{"Time", "Transaction Number", "First Name", "Last Name", "Items", "Total", "Queue", "Id"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public RecentTransactionsGatherer(TransactionRepository repository, MemberRepository memberRepository) {
        this.transactionRepository = repository;
        this.memberRepository = memberRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.DATE, GathererReport.FieldType.NUMBER, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Recent Transactions", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        Map<String, String> filterMap = filter.getMap();
        Integer size = 5;
        if (filterMap.get("size") != null) {
            size = Integer.parseInt(filterMap.get("size"));
        }

        List<Transaction> results = transactionRepository.getRecentCompletedTransactionsSales(filter.getCompanyId(), filter.getShopId(), size);
        // let's do the filter
        List<ObjectId> objectIds = new ArrayList<>();
        for (Transaction transaction : results) {
            if (transaction.getMemberId() != null && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }

        }

        Iterable<Member> members = memberRepository.findItemsIn(filter.getCompanyId(), objectIds, "{firstName:1,lastName:1,status:1,memberGroup:1}");
        HashMap<String, Member> memberHashMap = new HashMap<>();
        for (Member m : members) {
            memberHashMap.put(m.getId(), m);
        }

        int factor = 1;
        for (Transaction t : results) {
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            data.put(attrs[0], ProcessorUtil.timeStampWithOffset(t.getProcessedTime(), filter.getTimezoneOffset()));
            data.put(attrs[1], t.getTransNo());
            Member m = memberHashMap.get(t.getMemberId());
            if (m != null) {
                data.put(attrs[2], m.getFirstName());
                data.put(attrs[3], m.getLastName());
            } else {
                data.put(attrs[2], t.getTransType().toString());
                data.put(attrs[3], "");
            }
            data.put(attrs[4], t.getCart().getItems().size() * factor);
            data.put(attrs[5], NumberUtils.round(t.getCart().getTotal(), 2) * factor);
            data.put(attrs[6], t.getQueueType().toString());
            data.put(attrs[7], t.getId());
            report.add(data);
        }
        return report;
    }
}
