package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 5/30/2016.
 */
public class SalesByCannabisTypeGatherer implements Gatherer {
    private ProductRepository productRepository;
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Type", "Sales"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByCannabisTypeGatherer(TransactionRepository transactionRepository, ProductRepository productRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        Map<String, Product> products = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        return prepareSalesByCannabisType(filter, transactions, products);
    }

    public GathererReport prepareSalesByCannabisType(ReportFilter filter, Iterable<Transaction> transactions, Map<String, Product> products) {
        GathererReport report = new GathererReport(filter, "Sales By Cannabis Type", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Double[] strainSales = new Double[]{0d, 0d, 0d, 0d}; //sativa, indica, hybrid, CBD
        String[] strainNames = new String[]{"Sativa", "Indica", "Hybrid", "CBD"};
        int factor = 1;
        for (Transaction t : transactions) {
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            double avgCartDiscount = 0;
            if (t.getCart().getItems().size() > 0) {
                avgCartDiscount = t.getCart().getCalcCartDiscount().doubleValue() / t.getCart().getItems().size();
            }

            double avgDeliveryFee = 0;
            if (t.getCart().getItems().size() > 0) {
                avgDeliveryFee = t.getCart().getDeliveryFee().doubleValue() / t.getCart().getItems().size();
            }

            for (OrderItem item : t.getCart().getItems()) {
                if (t.getTransType() == Transaction.TransactionType.Sale
                        || (t.getTransType() == Transaction.TransactionType.Refund && t.getCart().getRefundOption() == Cart.RefundOption.Retail
                        && item.getStatus() == OrderItem.OrderItemStatus.Refunded)) {

                    Product product = products.get(item.getProductId());
                    if (product == null) {
                        continue;
                    }
                    String type = product.getFlowerType();
                    if (type != null && !type.isEmpty()) {
                        switch (type.toLowerCase()) {
                            case "sativa":
                                strainSales[0] += item.getFinalPrice().doubleValue() * factor;
                                break;
                            case "indica":
                                strainSales[1] += item.getFinalPrice().doubleValue() * factor;
                                break;
                            case "hybrid":
                                strainSales[2] += item.getFinalPrice().doubleValue() * factor;
                                break;
                            case "cbd":
                                strainSales[3] += item.getFinalPrice().doubleValue() * factor;
                                break;
                            default:
                                System.out.println("flower type '" + type + "' not recognized");
                        }
                    }
                }
            }
        }
        double totalSales = strainSales[0] + strainSales[1] + strainSales[2];
        for (int i = 0; i < strainSales.length; i++) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            data.put(attrs[0], strainNames[i]);

            Percentage percent = new Percentage(totalSales == 0 ? 0 : strainSales[i] / totalSales);
            data.put(attrs[1], percent);

            report.add(data);
        }
        return report;
    }
}
