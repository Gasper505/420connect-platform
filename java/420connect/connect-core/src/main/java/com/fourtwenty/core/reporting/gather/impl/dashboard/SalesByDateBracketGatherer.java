package com.fourtwenty.core.reporting.gather.impl.dashboard;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 6/6/2016.
 */
public class SalesByDateBracketGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Day", "Total Sales", "Total Transactions", "Average Sale/Transaction"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByDateBracketGatherer(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.DATE, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        String bracketName = filter.getMap().get("bracket") != null ? filter.getMap().get("bracket").toLowerCase() : "none";
        DateTime d = DateTime.now(); //new datetime based on start date

        GathererReport report = new GathererReport(filter, "Sales By Date Bracket Report", reportHeaders);

        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(),
                filter.getShopId(), filter.getTimeZoneStartDateMillis(),
                filter.getTimeZoneEndDateMillis());

        //Not doing this as an aggregation currently because if we want to break the data up differently in the future
        //it should be done in java here. O(N^2) efficiency is acceptable in the time being
        HashMap<Long, Double> salesByDayMap = new HashMap<>();
        HashMap<Long, Integer> countMap = new HashMap<>();

        //THe purpose of this loop is to iterate through each transactions' order items and sum the non-refunded items
        // to get a total dollar amount of that transaction and add it to a hashmap which maps a running total of $
        // to a map that is keyed by that days starting millis. This allows us to condense all transactions and group them
        // by the day by using the start of the day as a key. We also keep track of the number of transactions for that day
        // so we can compute the average transaction amount as well as hang on to the number of transactions for that day.
        int factor = 1;
        for (Transaction t : results) {
            long key = 0;
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            if (DateUtil.getMonthsBetweenTwoDates(filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis()) >= 2) {
                DateTime dt = new DateTime(t.getProcessedTime()).plusMinutes(filter.getTimezoneOffset());
                int dayOfWeek = dt.getDayOfWeek();
                //System.out.println("Day: " +dayOfWeek);
                key = dt.minusDays(dayOfWeek).withTimeAtStartOfDay().plusDays(1).getMillis();
            } else {
                DateTime dt = new DateTime(t.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset());
                key = dt.getMillis();
            }
            //use as key in map to group sales by day\\

            double transTotal = t.getCart().getTotal().doubleValue() * factor;
            if (t.getTransType() == Transaction.TransactionType.Refund
                    && t.getCart().getRefundOption() == Cart.RefundOption.Void
                    && t.getCart().getSubTotal().doubleValue() == 0) {
                transTotal = 0;
            }

            if (salesByDayMap.get(key) == null) { //if this is a new day, add to the map
                salesByDayMap.put(key, transTotal);
                countMap.put(key, 1); //transaction count
            } else { //if this day exists already in the map, add the sum to the daily total
                salesByDayMap.put(key, salesByDayMap.get(key) + transTotal);
                countMap.put(key, countMap.get(key) + 1); //increment daily transaction count
            }
        }

        //In this loop we go through the map we just created and add the data for each day into the report data map.
        for (Long day : salesByDayMap.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], day);
            double sales = salesByDayMap.get(day);
            double roundedSales = NumberUtils.round(sales, 2);
            data.put(attrs[1], roundedSales);
            int count = countMap.get(day);
            data.put(attrs[2], count);
            data.put(attrs[3], NumberUtils.round(sales / count, 2));
            report.add(data);
        }
        return report;
    }
}
