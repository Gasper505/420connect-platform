package com.fourtwenty.core.reporting.gather.impl.employee;

import com.fourtwenty.core.domain.models.company.AuditLog;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.repositories.dispensary.AuditLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by stephen on 9/14/16.
 */
public class EmployeeActivityGatherer implements Gatherer {
    private AuditLogRepository auditRepository;
    private EmployeeRepository employeeRepository;
    private TerminalRepository terminalRepository;
    private String[] attrs = new String[]{"Time", "Employee", "Action", "Category", "Terminal"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public EmployeeActivityGatherer(AuditLogRepository auditRepository, EmployeeRepository employeeRepository, TerminalRepository terminalRepository) {
        this.auditRepository = auditRepository;
        this.employeeRepository = employeeRepository;
        this.terminalRepository = terminalRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Employee Activity", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<AuditLog> logs = auditRepository.getLogsByDateBracket(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Employee> employees = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Terminal> terminals = terminalRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        for (AuditLog log : logs) {
            HashMap<String, Object> data = new HashMap<>();
            Employee emp = employees.get(log.getEmployeeId());
            if (emp != null) {

                data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(log.getModified(), filter.getTimezoneOffset()));
                data.put(attrs[1], emp.getFirstName() + emp.getLastName());
                data.put(attrs[2], log.getAction());
                data.put(attrs[3], log.getCategory());
                Terminal t = terminals.get(log.getTerminalId());
                if (t != null) {
                    data.put(attrs[4], t.getName());
                } else {
                    data.put(attrs[4], log.getTerminalId());
                }
            }
            report.add(data);
        }

        return report;
    }
}
