package com.fourtwenty.core.reporting.gather.impl.employee;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by decipher on 25/11/17 12:19 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class EmployeeBySalesByProductGatherer implements Gatherer {

    private EmployeeRepository employeeRepository;
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;

    private String[] attrs = new String[]{"Product", "Employee", "Quantity"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public EmployeeBySalesByProductGatherer(EmployeeRepository employeeRepository, TransactionRepository transactionRepository, ProductRepository productRepository) {
        this.employeeRepository = employeeRepository;
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Employee By Sales By Product", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        String productName = "";
        String unitType = "";
        HashMap<String, Object> data = null;
        Map<String, Map<String, BigDecimal>> employeeProductMap = new HashMap<>();
        for (Transaction transaction : transactions) {
            if (!transaction.getSellerId().isEmpty()) {
                Employee employee = employeeMap.get(transaction.getSellerId());

                List<OrderItem> orderItems = transaction.getCart().getItems();

                if (!orderItems.isEmpty()) {
                    for (OrderItem orderItem : orderItems) {
                        Product product = productMap.get(orderItem.getProductId());
                        if (employee != null && product != null) {
                            employeeProductMap.putIfAbsent(employee.getId(), new HashMap<>());
                            Map<String, BigDecimal> productQuantityMap = employeeProductMap.get(employee.getId());
                            productQuantityMap.putIfAbsent(product.getId(), BigDecimal.ZERO);
                            BigDecimal quantity = productQuantityMap.get(product.getId());
                            quantity = quantity.add(orderItem.getQuantity());
                            productQuantityMap.put(product.getId(), quantity);
                            employeeProductMap.put(employee.getId(), productQuantityMap);
                        }
                    }
                }
            }
        }

        for (String empId : employeeProductMap.keySet()) {
            Employee employee = employeeMap.get(empId);
            if (employee == null) {
                continue;
            }

            String employeeName = employee.getFirstName() + " " + employee.getLastName();
            Map<String, BigDecimal> productQuantityMap = employeeProductMap.get(empId);
            for (String pId : productQuantityMap.keySet()) {
                Product product = productMap.get(pId);
                if (product == null) {
                    continue;
                }

                data = new HashMap<>();
                productName = product.getName();
                unitType = product.getCategory().getUnitType().getType();

                data.put(attrs[0], productName);
                data.put(attrs[1], employeeName);
                data.put(attrs[2], NumberUtils.truncateDecimal(productQuantityMap.get(pId).doubleValue(), 2) + " " + unitType);
                report.add(data);
            }
        }


        return report;
    }
}
