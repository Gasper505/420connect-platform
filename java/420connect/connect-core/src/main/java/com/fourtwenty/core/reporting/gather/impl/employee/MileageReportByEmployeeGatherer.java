package com.fourtwenty.core.reporting.gather.impl.employee;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalLocationRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.services.mgmt.DeliveryService;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class MileageReportByEmployeeGatherer implements Gatherer {
    private String[] attrs = new String[]{"Employee Name", "Mileage"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private EmployeeRepository employeeRepository;
    private TerminalLocationRepository terminalLocationRepository;
    private DeliveryService deliveryService;

    public MileageReportByEmployeeGatherer(EmployeeRepository employeeRepository, TerminalLocationRepository terminalLocationRepository, DeliveryService deliveryService) {
        this.employeeRepository = employeeRepository;
        this.terminalLocationRepository = terminalLocationRepository;
        this.deliveryService = deliveryService;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(filter.getCompanyId());
        Iterable<TerminalLocation> terminalLocationList = terminalLocationRepository.getTermLocationsWithDate(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{created:1}");
        return prepareMileageReportByEmployee(filter, terminalLocationList, employeeHashMap);
    }

    public GathererReport prepareMileageReportByEmployee(ReportFilter filter, Iterable<TerminalLocation> terminalLocationList, HashMap<String, Employee> employeeHashMap) {
        GathererReport report = new GathererReport(filter, "Mileage Report By Employee", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, EmployeeInfo> distanceHashMap = new HashMap<>();

        NumberFormat formatter = new DecimalFormat("#.##");
        Set<String> employeeKeys = employeeHashMap.keySet();
        EmployeeInfo employeeInfo;
        for (String employeeKey : employeeKeys) {
            Employee employee = employeeHashMap.get(employeeKey);
            Double distance = 0D;

            List<TerminalLocation> locations = new ArrayList<>();

            for (TerminalLocation terminalLocation : terminalLocationList) {
                if (terminalLocation.getEmployeeId() != null) {
                    if (employee.getId().equals(terminalLocation.getEmployeeId())) {
                        locations.add(terminalLocation);
                    }
                }
                distance = deliveryService.calculateDistance(locations);
            }
            if (distance == 0) {
                continue;
            }
            employeeInfo = new EmployeeInfo(employee.getFirstName() + " " + employee.getLastName(), formatter.format(distance) + " miles");
            distanceHashMap.put(employee.getId(), employeeInfo);
        }

        Set<String> distanceKeys = distanceHashMap.keySet();
        for (String distanceKey : distanceKeys) {
            employeeInfo = distanceHashMap.get(distanceKey);
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], employeeInfo.getName());
            data.put(attrs[1], employeeInfo.getDistance());

            report.add(data);
        }

        return report;
    }

    private class EmployeeInfo {
        String name;
        String distance;

        public EmployeeInfo(String name, String distance) {
            this.name = name;
            this.distance = distance;
        }

        public String getName() {
            return name;
        }

        public String getDistance() {
            return distance;
        }
    }
}
