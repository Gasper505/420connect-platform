package com.fourtwenty.core.reporting.gather.impl.employee;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.loyalty.PromoUsage;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromoUsageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Stephen Schmidt on 7/9/2016.
 */
public class SalesByEmployeeGatherer implements Gatherer {
    static final Log LOGGGER = LogFactory.getLog(SalesByEmployeeGatherer.class);
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private PromoUsageRepository promoUsageRepository;

    private String[] attrs = new String[]{"First Name",
            "Last Name",
            "Gross Receipts",
            "Transactions",
            "Average Transaction",
            "Total Discount",
            "After Tax Discount",
            "Total promotions",
            "Number of promotions used",
            "Total sales by cash",
            "Total sales by credit",
            "Total sales by check",
            "Total sales by CashlessAtm"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByEmployeeGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Employee", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);


        List<EmployeeSale> employeeSaleList = prepareSalesReport(filter);

        for (EmployeeSale employeeSale : employeeSaleList) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], employeeSale.firstName);
            data.put(attrs[1], employeeSale.lastName);
            data.put(attrs[2], new DollarAmount(employeeSale.totalSales));
            data.put(attrs[3], employeeSale.numTransactions);

            data.put(attrs[4], new DollarAmount(employeeSale.totalSales / employeeSale.numTransactions));
            data.put(attrs[5], new DollarAmount(employeeSale.totalDiscounts));
            data.put(attrs[6], new DollarAmount(employeeSale.afterTaxDiscount));
            data.put(attrs[7], new DollarAmount(employeeSale.totalPromos));
            data.put(attrs[8], employeeSale.promoCount);
            data.put(attrs[9], new DollarAmount(employeeSale.totalCashSales));
            data.put(attrs[10], new DollarAmount(employeeSale.totalCreditSales));
            data.put(attrs[11], new DollarAmount(employeeSale.totalCheckSales));
            data.put(attrs[12], new DollarAmount(employeeSale.totalCashlessSales));
            report.add(data);
        }

        return report;
    }

    /**
     * Public method to prepare sales by employee report
     * @param filter : report filter
     * @return
     */
    public List<EmployeeSale> prepareSalesReport(ReportFilter filter) {
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Iterable<PromoUsage> promoUsages = promoUsageRepository.listByShopWithDates(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        HashMap<String, EmployeeSale> employeeSaleHashMap = new HashMap<>();

        HashMap<String, List<PromoUsage>> promoUsageByTrans = new HashMap<>();

        for (PromoUsage promoUsage : promoUsages) {
            List<PromoUsage> items = promoUsageByTrans.get(promoUsage.getTransId());
            if (items == null) {
                items = new ArrayList<>();
                promoUsageByTrans.put(promoUsage.getTransId(), items);
            }
            items.add(promoUsage);
        }

        for (Transaction transaction : transactions) {

            int factor = 1;
            if (Transaction.TransactionType.Refund == transaction.getTransType() && (transaction.getCart() == null || Cart.RefundOption.Void == transaction.getCart().getRefundOption())) {
                continue;
            }
            if (Transaction.TransactionType.Refund.equals(transaction.getTransType()) && Cart.RefundOption.Retail.equals(transaction.getCart().getRefundOption())) {
                factor = -1;
            }
            Employee emp = employeeMap.get(transaction.getSellerId());
            if (emp == null) {
                continue;
            }
            EmployeeSale empSale = employeeSaleHashMap.get(transaction.getSellerId());
            if (empSale == null) {
                empSale = new EmployeeSale();
                employeeSaleHashMap.put(emp.getId(), empSale);
            }

            BigDecimal totalPromoValue = BigDecimal.ZERO;
            double totalSalesByCash = 0d, totalSalesByCredit = 0d, totalSalesByCheck = 0d, totalCashlessSales = 0d;
            int promoCount = 0;


            LinkedHashSet<PromotionReqLog> promotionReqLogs = transaction.getCart().getPromotionReqLogs();

            List<PromoUsage> promoUsageList = promoUsageByTrans.get(transaction.getId());
            if (promoUsageList != null) {
                for (PromoUsage promoUsage : promoUsageList) {
                    promoCount += promoUsage.getCount();
                }
            }


            for (PromotionReqLog promotionReqLog : promotionReqLogs) {
                BigDecimal amount = promotionReqLog.getAmount();
                totalPromoValue = totalPromoValue.add(amount);
            }


            if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Cash || transaction.getCart().getPaymentOption() == Cart.PaymentOption.Check) {
                if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Cash) {
                    totalSalesByCash += transaction.getCart().getTotal().doubleValue() * factor;
                } else {
                    totalSalesByCheck += transaction.getCart().getTotal().doubleValue() * factor;
                }
            } else if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.CashlessATM) {
                totalCashlessSales += transaction.getCart().getTotal().doubleValue() * factor;
            } else if (transaction.getCart().getSplitPayment() != null && transaction.getCart().getPaymentOption() == Cart.PaymentOption.Split) {
                double checks = transaction.getCart().getSplitPayment().getCheckAmt().doubleValue() * factor;
                double credits = transaction.getCart().getSplitPayment().getCreditDebitAmt().doubleValue() * factor;

                double totalCashValue = transaction.getCart().getTotal().doubleValue() - checks - credits;

                totalSalesByCash += totalCashValue * factor;
                totalSalesByCheck += checks * factor;
                totalSalesByCredit += credits * factor;
            } else {
                totalSalesByCredit += transaction.getCart().getTotal().doubleValue() * factor;
            }

            double afterTaxDiscount = 0d;
            if (transaction.getCart().getAppliedAfterTaxDiscount() != null && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount().doubleValue();
            }


            empSale.firstName = emp.getFirstName();
            empSale.lastName = emp.getLastName();
            empSale.numTransactions += 1;
            empSale.totalSales += transaction.getCart().getTotal().doubleValue() * factor;
            empSale.promoCount += promoCount;
            empSale.totalDiscounts += transaction.getCart().getTotalDiscount().doubleValue() * factor;
            empSale.afterTaxDiscount += afterTaxDiscount * factor;
            empSale.totalPromos += totalPromoValue.doubleValue() * factor;
            empSale.totalCashSales += totalSalesByCash;
            empSale.totalCheckSales += totalSalesByCheck;
            empSale.totalCreditSales += totalSalesByCredit;
            empSale.totalCashlessSales += totalCashlessSales;
        }

        List<EmployeeSale> employeeSaleList = Lists.newArrayList(employeeSaleHashMap.values());
        employeeSaleList.sort(new Comparator<EmployeeSale>() {
            @Override
            public int compare(EmployeeSale o1, EmployeeSale o2) {
                return o1.firstName.compareTo(o2.firstName);
            }
        });

        return employeeSaleList;
    }

    public static final class EmployeeSale {
        public String firstName;
        public String lastName;
        public double totalSales = 0;
        public int numTransactions = 0;
        public double avgSales = 0;
        public double totalDiscounts = 0;
        public double afterTaxDiscount = 0d;
        public double totalPromos = 0;
        public double promoCount = 0;
        public double totalCashSales = 0;
        public double totalCheckSales = 0;
        public double totalCreditSales = 0;
        public double totalCashlessSales = 0;

    }
}
