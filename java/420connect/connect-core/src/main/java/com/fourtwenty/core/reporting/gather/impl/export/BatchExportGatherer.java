package com.fourtwenty.core.reporting.gather.impl.export;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class BatchExportGatherer implements Gatherer {
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private InventoryRepository inventoryRepository;

    private String[] attrs = new String[]{
            "Shop Name", //1
            "Vendor Name", //2
            "Arms Length Type", //3
            "Brand", //4
            "Product ID", //5
            "Product Name", //6
            "Category", //7
            "Measurement Type", //8
            "Weight Type", //9
            "Weight", //10
            "Status", //11
            "Archived?", //12
            "Cannabis Type",//13
            "Medicinal?", //14
            "Product SKU",//15
            "Internal ID",//16
            "Unique #",//17
            "Batch ID",//18
            "Tracking System", //19
            "Metrc Package Label",//20
            "Purchased Qty", //21
            "Current Qty", //22
            "Purchased Cost", //23
            "Cost per Unit", //24
            "Purchased Date", //25
            "Sell By Date", //26
            "Expiration Date", //27
            "IsCannabis", //27

    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public BatchExportGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Shop Name 1
                GathererReport.FieldType.STRING, //  Vendor Name 2
                GathererReport.FieldType.STRING, // Arms Length Type 3
                GathererReport.FieldType.STRING, // Brand 4
                GathererReport.FieldType.STRING, // Product Id 5
                GathererReport.FieldType.STRING, // Product Name 6
                GathererReport.FieldType.STRING, // Category 7
                GathererReport.FieldType.STRING, //  Measurement Type 9
                GathererReport.FieldType.STRING, // Weight Type 9
                GathererReport.FieldType.STRING, // Weight 10
                GathererReport.FieldType.STRING,  // Status 11
                GathererReport.FieldType.STRING, // Archived 12
                GathererReport.FieldType.STRING, // Cannabis Type 13
                GathererReport.FieldType.STRING, // Medicinal 14
                GathererReport.FieldType.STRING, // Product SKU 15
                GathererReport.FieldType.STRING, // Internal ID 16
                GathererReport.FieldType.STRING, // Unique # 17
                GathererReport.FieldType.STRING, // Batch ID 18
                GathererReport.FieldType.STRING, // Tracking System 19
                GathererReport.FieldType.STRING, // Metrc Package Label 20
                GathererReport.FieldType.NUMBER, // Purchased Qty 21
                GathererReport.FieldType.NUMBER, // Current Qty 22
                GathererReport.FieldType.NUMBER, // Purchased Cost 23
                GathererReport.FieldType.NUMBER, // Cost per Unit 24
                GathererReport.FieldType.STRING, // Purchased Date 25
                GathererReport.FieldType.STRING, // Sell By Date 26
                GathererReport.FieldType.STRING, // Expiration Date 27
                GathererReport.FieldType.STRING, // IsCannabis 27

        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        Set<ObjectId> vendorIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> productCategoryIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        List<String> batchIds = new ArrayList<>();

        for (ProductBatch productBatch : productBatchHashMap.values()) {
            if (StringUtils.isNotBlank(productBatch.getVendorId()) && ObjectId.isValid(productBatch.getVendorId())) {
                vendorIds.add(new ObjectId(productBatch.getVendorId()));
            }
            if (StringUtils.isNotBlank(productBatch.getBrandId()) && ObjectId.isValid(productBatch.getBrandId())) {
                brandIds.add(new ObjectId(productBatch.getBrandId()));
            }
            if (StringUtils.isNotBlank(productBatch.getProductId()) && ObjectId.isValid(productBatch.getProductId())) {
                productIds.add(new ObjectId(productBatch.getProductId()));
            }
            if (StringUtils.isNotBlank(productBatch.getId()) && ObjectId.isValid(productBatch.getId())) {
                batchIds.add(productBatch.getId());
            }

        }
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productCategoryIds));
        HashMap<String, List<BatchQuantity>> productBatchQuantities = batchQuantityRepository.getAllBatchQuantities(filter.getCompanyId(), filter.getShopId(), batchIds);
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        List<Inventory> sortedInventory = Lists.newArrayList(inventoryMap.values());
        Collections.sort(sortedInventory, new Comparator<Inventory>() {
            @Override
            public int compare(Inventory o1, Inventory o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        // create inventory columns
        for (Inventory inventory : inventoryMap.values()) {
            if (inventory.isActive() && !inventory.isDeleted()) {
                reportHeaders.add("INV: "+inventory.getName());
                fieldTypes.put("INV:" + inventory.getName(), GathererReport.FieldType.NUMBER);
            }
        }



        GathererReport report = new GathererReport(filter, "Batch Export", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);


        for (ProductBatch productBatch : productBatchHashMap.values()) {
            HashMap<String, Object> data = new HashMap<>();

            int i = 0;
            if (productBatch.isDeleted()) {
                continue;
            }
            if (productBatch.isArchived()) {
                continue;
            }

            for (Inventory inventory : inventoryMap.values()) {
                if (inventory.isActive() && !inventory.isDeleted()) {
                    data.put("INV: " + inventory.getName(), BigDecimal.ZERO);
                }
            }


            BigDecimal liveQuantity = BigDecimal.ZERO;
            List<BatchQuantity> batchQuantities = productBatchQuantities.getOrDefault(productBatch.getId(), new ArrayList<>());
            if (!batchQuantities.isEmpty()) {
                for (BatchQuantity batchQuantity : batchQuantities) {
                    Inventory inventory = inventoryMap.get(batchQuantity.getInventoryId());
                    if (inventory == null || inventory.isDeleted() || !inventory.isActive()) {
                        continue;
                    }
                    liveQuantity = liveQuantity.add(batchQuantity.getQuantity());


                    if (liveQuantity.doubleValue() > 0) {
                        data.put("INV: " + inventory.getName(), batchQuantity.getQuantity());
                    }
                }
            }




            String nAvail = "N/A";
            if (liveQuantity.doubleValue() > 0) {

                // set inventory to 0 if it does not have a value
                for (Inventory inventory : inventoryMap.values()) {
                    if (inventory.isActive() && !inventory.isDeleted()) {
                        String key = "INV: " + inventory.getName();
                        if (!data.containsKey(key)) {
                            data.put(key, BigDecimal.ZERO);
                        }
                    }
                }


                Vendor vendor = vendorHashMap.get(productBatch.getVendorId());
                Product product = productHashMap.get(productBatch.getProductId());

                if (product.isDeleted()) {
                    continue;
                }

                ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                Brand brand = brandHashMap.get(product.getBrandId());

                String status = "Inactive";
                String archived = "No";

                data.put(attrs[i++], shop != null && StringUtils.isNotBlank(shop.getName()) ? shop.getName() : nAvail);
                data.put(attrs[i++], (vendor != null && StringUtils.isNotBlank(vendor.getName())) ? vendor.getName() : nAvail);
                data.put(attrs[i++], (vendor != null) ? vendor.getArmsLengthType().toString() : nAvail);
                data.put(attrs[i++], (brand != null && StringUtils.isNotBlank(brand.getName())) ? brand.getName() : " ");
                data.put(attrs[i++], StringUtils.isNotBlank(productBatch.getProductId()) ? product.getId() : nAvail);
                data.put(attrs[i++], StringUtils.isNotBlank(product.getName()) ? product.getName() : nAvail);

                if (productCategory != null) {
                    data.put(attrs[i++], StringUtils.isNotBlank(productCategory.getName()) ? productCategory.getName() : nAvail);
                    data.put(attrs[i++], productCategory.getUnitType());
                }
                data.put(attrs[i++], product.getWeightPerUnit());

                String weight;
                if (Product.WeightPerUnit.CUSTOM_GRAMS == product.getWeightPerUnit()) {
                    weight = product.getCustomWeight() + " " + product.getCustomGramType().getType();
                } else {
                    weight = product.getWeightPerUnit().getWeightPerUnit();
                }

                data.put(attrs[i++], weight);

                if (product.isActive()) {
                    status = "Active";
                }
                data.put(attrs[i++], status);

                if (productBatch.isArchived()) {
                    archived = "Yes";
                }

                data.put(attrs[i++], archived);

                data.put(attrs[i++], product.getCannabisType());
                data.put(attrs[i++], product.getProductSaleType());
                data.put(attrs[i++], product.getSku());

                data.put(attrs[i++], productBatch.getId());
                data.put(attrs[i++], productBatch.getSku());

                data.put(attrs[i++], StringUtils.isNotBlank(productBatch.getTrackHarvestBatch()) ? productBatch.getTrackHarvestBatch() : nAvail);
                data.put(attrs[i++], productBatch.getTrackTraceSystem());
                data.put(attrs[i++], StringUtils.isNotBlank(productBatch.getTrackPackageLabel()) ? productBatch.getTrackPackageLabel() : nAvail);
                data.put(attrs[i++], productBatch.getQuantity());
                //live Qty
                data.put(attrs[i++], liveQuantity);
                data.put(attrs[i++], productBatch.getCost());
                data.put(attrs[i++], productBatch.getCostPerUnit());

                data.put(attrs[i++], productBatch.getPurchasedDate() > 0 ? ProcessorUtil.dateStringWithOffset(productBatch.getPurchasedDate(),filter.getTimezoneOffset()) : nAvail);
                data.put(attrs[i++], productBatch.getSellBy() > 0 ? ProcessorUtil.dateStringWithOffset(productBatch.getSellBy(),filter.getTimezoneOffset()) : nAvail);
                data.put(attrs[i++], productBatch.getExpirationDate() > 0 ? ProcessorUtil.dateStringWithOffset(productBatch.getExpirationDate(),filter.getTimezoneOffset()) : nAvail);

                String cannabis = "No";
                if (productCategory.isCannabis()) {
                    cannabis = "Yes";
                }
                if (product.getCannabisType() == Product.CannabisType.NON_CANNABIS) {
                    cannabis = "No";
                }
                data.put(attrs[i++],cannabis);

                report.add(data);
            }

        }
        return report;
    }
}
