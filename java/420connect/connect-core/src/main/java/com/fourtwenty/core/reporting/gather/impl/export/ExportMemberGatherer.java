package com.fourtwenty.core.reporting.gather.impl.export;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ExportMemberGatherer implements Gatherer {

    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    CompanyRepository companyRepository;
    private String[] attrs = new String[]{
            "Member Id",
            "First Name",
            "Last Name",
            "Street Address",
            "City",
            "State",
            "Zip Code",
            "Gender",
            "Member Group",
            "Identification Type",
            "Identification Number",
            "Identification State",
            "Expiration Date",
            "Date of Birth",
            "Date Joined",
            "Primary Phone",
            "Secondary Phone",
            "Email Address",
            "Medical?",
            "Recommendation Number",
            "Recommendation Expiration Date",
            "Recommendation Issue Date",
            "Verification Website",
            "Recommendation State",
            "Verification Phone Number",
            "Doctor ID",
            "Asset #1",
            "Asset #2",
            "Asset #3",
            "Marketing Source",
            "Enable Loyalty",
            "Loyalty Points",
            "Text Opt-In",
            "Email Opt-In",
            "Consumer Type",
            "Last Visit Date",
            "Source Shop"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();


    public ExportMemberGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Export Members", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Member> activeMembers;
        HashMap<String, MemberGroup> memberGroupMap;
        if (filter.getCompanyMembersShareOption() == Company.CompanyMembersShareOption.Isolated) {
            activeMembers = memberRepository.listByShopWithOptions(filter.getCompanyId(), filter.getShopId(), "{ id:1, shopId:1, firstName:1, lastName:1, address: 1, sex:1, memberGroupId:1, identifications:1, recommendations:1, dob:1, primaryPhone:1, email:1, medical:1, marketingSource:1, enableLoyalty:1, textOptIn:1, emailOptIn:1, consumerType:1, lastVisitDate:1, loyaltyPoints:1, startDate:1 }");
            memberGroupMap = memberGroupRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        } else {
            activeMembers = memberRepository.listWithOptions(filter.getCompanyId(), "{ id:1, shopId:1, firstName:1, lastName:1, address: 1, sex:1, memberGroupId:1, identifications:1, recommendations:1, dob:1, primaryPhone:1, email:1, medical:1, marketingSource:1, enableLoyalty:1, textOptIn:1, emailOptIn:1, consumerType:1, lastVisitDate:1, loyaltyPoints:1, startDate:1 }");
            memberGroupMap = memberGroupRepository.listAllAsMap(filter.getCompanyId());
        }

        if (activeMembers == null) {
            return report;
        }

        HashSet<String> addedMembers = new HashSet<>();
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        HashMap<String,Shop> shopHashMap = shopRepository.listAsMap(filter.getCompanyId());

        for (Member activeMember : activeMembers) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());


            data.put(attrs[0], activeMember.getId());
            data.put(attrs[1], activeMember.getFirstName());
            data.put(attrs[2], activeMember.getLastName());
            if (activeMember.getAddress() != null) {
                data.put(attrs[3], activeMember.getAddress().getAddress());
                data.put(attrs[4], activeMember.getAddress().getCity());
                data.put(attrs[5], activeMember.getAddress().getState());
                data.put(attrs[6], activeMember.getAddress().getZipCode());
            } else {
                data.put(attrs[3], "");
                data.put(attrs[4], "");
                data.put(attrs[5], "");
                data.put(attrs[6], "");
            }
            data.put(attrs[7], activeMember.getSex());

            MemberGroup memberGroup = memberGroupMap.get(activeMember.getMemberGroupId());
            if (memberGroup != null) {
                data.put(attrs[8], memberGroup.getName());
            } else {
                data.put(attrs[8], "");
            }

            List<Identification> identifications = activeMember.getIdentifications();

            identifications.sort(new Comparator<Identification>() {
                @Override
                public int compare(Identification o1, Identification o2) {
                    long exp1 = o1.getExpirationDate() != null ? o1.getExpirationDate() : 0;
                    long exp2 = o2.getExpirationDate() != null ? o2.getExpirationDate() : 0;
                    return ((Long) exp2).compareTo(exp1);
                }
            });

            String expiration = "";
            if (identifications.isEmpty()) {
                data.put(attrs[9], "");
                data.put(attrs[10], "");
                data.put(attrs[11], "");
            } else {
                Identification identification = identifications.get(0);

                data.put(attrs[9], identification.getType());
                data.put(attrs[10], identification.getLicenseNumber());
                data.put(attrs[11], identification.getState());
                if (identification.getExpirationDate() != null) {
                    expiration = DateUtil.toDateFormatted(identification.getExpirationDate());
                }
            }

            data.put(attrs[12], expiration);
            data.put(attrs[13], DateUtil.toDateFormatted(activeMember.getDob()));
            data.put(attrs[14], DateUtil.toDateFormatted(activeMember.getStartDate(), shop.getTimeZone()));
            data.put(attrs[15], activeMember.getPrimaryPhone());
            data.put(attrs[16], activeMember.getPrimaryPhone());
            data.put(attrs[17], activeMember.getEmail());
            data.put(attrs[18], activeMember.isMedical());

            List<Recommendation> recommendations = activeMember.getRecommendations();

            recommendations.sort(new Comparator<Recommendation>() {
                @Override
                public int compare(Recommendation o1, Recommendation o2) {
                    long exp1 = o1.getExpirationDate() != null ? o1.getExpirationDate() : 0;
                    long exp2 = o2.getExpirationDate() != null ? o2.getExpirationDate() : 0;
                    return ((Long) exp2).compareTo(exp1);
                    //return o2.getExpirationDate().compareTo(o1.getExpirationDate());
                }
            });

            if (recommendations.isEmpty()) {
                data.put(attrs[19], "");
                data.put(attrs[20], "");
                data.put(attrs[21], "");
                data.put(attrs[22], "");
                data.put(attrs[23], "");
                data.put(attrs[24], "");
                data.put(attrs[25], "");
            } else {
                Recommendation recommendation = recommendations.get(0);

                data.put(attrs[19], recommendation.getRecommendationNumber());
                data.put(attrs[20], recommendation.getExpirationDate() == null || recommendation.getExpirationDate() == 0 ? "" : DateUtil.toDateFormatted(recommendation.getExpirationDate()));
                data.put(attrs[21], recommendation.getIssueDate() == null || recommendation.getIssueDate() == 0 ? "" : DateUtil.toDateFormatted(recommendation.getIssueDate()));
                data.put(attrs[22], recommendation.getVerifyWebsite());
                data.put(attrs[23], recommendation.getState());
                data.put(attrs[24], recommendation.getVerifyPhoneNumber());
                data.put(attrs[25], recommendation.getDoctorId());
            }

            data.put(attrs[26], "");
            data.put(attrs[27], "");
            data.put(attrs[28], "");
            data.put(attrs[29], activeMember.getMarketingSource());
            data.put(attrs[30], activeMember.isEnableLoyalty());
            data.put(attrs[31], activeMember.getLoyaltyPoints());
            data.put(attrs[32], activeMember.isTextOptIn());
            data.put(attrs[33], activeMember.isEmailOptIn());
            data.put(attrs[34], activeMember.getConsumerType());
            String lastVisitDate = "";
            if (activeMember.getLastVisitDate() != null) {
                lastVisitDate = DateUtil.toDateFormatted(activeMember.getLastVisitDate(), shop.getTimeZone());
            }
            data.put(attrs[35], lastVisitDate);

            if (!addedMembers.contains(activeMember.getId())) {
                report.add(data);
                addedMembers.add(activeMember.getId());
            }


            Shop sourceShop = shopHashMap.get(activeMember.getShopId());
            String sourceshopName = "";
            if (sourceShop != null) {
                sourceshopName = sourceShop.getName();
            }
            data.put(attrs[36],sourceshopName);
        }




        return report;
    }
}
