package com.fourtwenty.core.reporting.gather.impl.export;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 1/31/18.
 */
public class ProductsExportGatherer implements Gatherer {
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    BrandRepository brandRepository;

    private String[] attrs = new String[]{
            "SKU",
            "Item",
            "Category",
            "Cannabis",
            "Measurement",
            "Cost per Unit",
            "Unit Price",
            "Gram Price",
            ".5 Gram Price",
            "2 Grams Price",
            "3 Grams Price",
            "1/8th Price",
            "1/4th Price",
            "1/2 Price",
            "1 Oz Price",
            "Type",
            "Description",
            "THC %",
            "CBD %",
            "CBN %",
            "Inventory Available",
            "Date Purchase",
            "Vendor ID",
            "Vendor",
            "Genetics",
            "Image 1",
            "Image 2",
            "Image 3",
            "Image 4",
            "Image 5",
            "Product ID",
            "Brand",
            "Cannabis Type",
            "Weight Per Unit",
            "Custom Weight Measurement",
            "Custom Weight Type",
            "Active",
            "Tag"

    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ProductsExportGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // 0
                GathererReport.FieldType.STRING, // 1
                GathererReport.FieldType.STRING, // 2
                GathererReport.FieldType.STRING, // 3
                GathererReport.FieldType.STRING, // 4
                GathererReport.FieldType.CURRENCY, // cost 5
                GathererReport.FieldType.CURRENCY, // unitprice 6
                GathererReport.FieldType.CURRENCY, // gram 7
                GathererReport.FieldType.CURRENCY, // half garm 8
                GathererReport.FieldType.CURRENCY,  // 2 grams 9
                GathererReport.FieldType.CURRENCY, // 3 grams 10
                GathererReport.FieldType.CURRENCY, // eigth 11
                GathererReport.FieldType.CURRENCY, // fourth 12
                GathererReport.FieldType.CURRENCY, // half 13
                GathererReport.FieldType.CURRENCY, // ounce 14
                GathererReport.FieldType.STRING, // type 15
                GathererReport.FieldType.STRING, // description 16
                GathererReport.FieldType.NUMBER, // thc 17
                GathererReport.FieldType.NUMBER, // cbd 18
                GathererReport.FieldType.NUMBER, // cbn 19
                GathererReport.FieldType.NUMBER, // inventory avail 20
                GathererReport.FieldType.STRING, // po date 21
                GathererReport.FieldType.STRING, // vendor id 22
                GathererReport.FieldType.STRING, // vendor name 23
                GathererReport.FieldType.STRING, // genetics 24
                GathererReport.FieldType.STRING, // image 1 25
                GathererReport.FieldType.STRING, // image 2 26
                GathererReport.FieldType.STRING, // image 3 27
                GathererReport.FieldType.STRING, // image 4 28
                GathererReport.FieldType.STRING, // image 5 29
                GathererReport.FieldType.STRING, // Product ID30
                GathererReport.FieldType.STRING, // Brand 31
                GathererReport.FieldType.STRING, // Cannabis Type 32
                GathererReport.FieldType.STRING, // Weight Per Unit33
                GathererReport.FieldType.STRING, // Custom Weight Measurement 34
                GathererReport.FieldType.STRING, // Custom Weight Type34
                GathererReport.FieldType.STRING, // Active Type34
                GathererReport.FieldType.STRING // Tags Type34


        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Product> products = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(filter.getCompanyId());

        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        List<Inventory> inventoryList = Lists.newArrayList(inventoryRepository.listAllByShop(filter.getCompanyId(), filter.getShopId()));

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        inventoryList.sort(new Comparator<Inventory>() {
            @Override
            public int compare(Inventory o1, Inventory o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        for (Inventory inventory : inventoryList) {
            if (inventory.isActive()) {
                reportHeaders.add("INV: " + inventory.getName());
                fieldTypes.put("INV: " + inventory.getName(), GathererReport.FieldType.NUMBER);
            }
        }

        GathererReport report = new GathererReport(filter, "Product Export", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        for (Product product : products) {
            String sku = product.getSku();
            String name = product.getName();
            String categoryName = "";
            String measurement = "units";
            ProductCategory category = productCategoryHashMap.get(product.getCategoryId());


            String datePurchase = "";
            double costPerUnit = 0;
            ProductBatch recentBatch = recentBatchMap.get(product.getId());
            if (recentBatch != null) {
                costPerUnit = recentBatch.getFinalUnitCost().doubleValue();
                datePurchase = DateUtil.toDateFormatted(recentBatch.getPurchasedDate(), shop.getTimeZone());
            }
            BigDecimal unitPrice = product.getUnitPrice();
            double gramPrice = 0;
            double halfGramPrice = 0;
            double twoGramsPrice = 0;
            double threeGramPrice = 0;
            double eigthPrice = 0;
            double fourthPrice = 0;
            double halfthPrice = 0;
            double ouncePrice = 0;
            if (category != null) {
                categoryName = category.getName();
                measurement = category.getUnitType().name();

                if (category.getUnitType() == ProductCategory.UnitType.grams) {
                    if (product.getPriceRanges() != null) {
                        for (ProductPriceRange pr : product.getPriceRanges()) {
                            ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(pr.getWeightToleranceId());
                            if (weightTolerance != null) {
                                switch (weightTolerance.getWeightKey()) {
                                    case GRAM:
                                        gramPrice = pr.getPrice().doubleValue();
                                        break;
                                    case HALF_GRAM:
                                        halfGramPrice = pr.getPrice().doubleValue();
                                        break;
                                    case TWO_GRAMS:
                                        twoGramsPrice = pr.getPrice().doubleValue();
                                        break;
                                    case THREE_GRAMS:
                                        threeGramPrice = pr.getPrice().doubleValue();
                                        break;
                                    case ONE_EIGHTTH:
                                        eigthPrice = pr.getPrice().doubleValue();
                                        break;
                                    case QUARTER:
                                        fourthPrice = pr.getPrice().doubleValue();
                                        break;
                                    case HALF:
                                        halfthPrice = pr.getPrice().doubleValue();
                                        break;
                                    case OUNCE:
                                        ouncePrice = pr.getPrice().doubleValue();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            String type = product.getFlowerType();
            String desc = product.getDescription() != null ? product.getDescription().replace(",", " ") : "";
            double thc = product.getThc();
            double cbd = product.getCbd();
            double cbn = product.getCbn();
            String genetics = product.getGenetics();

            String vendorId = "";
            String vendorName = "";
            Vendor vendor = vendorHashMap.get(product.getVendorId());
            if (vendor != null) {
                vendorId = vendor.getId();
                vendorName = vendor.getName();
            }
            String imageURL1 = "";
            String imageURL2 = "";
            String imageURL3 = "";
            String imageURL4 = "";
            String imageURL5 = "";
            for (int i = 0; i < product.getAssets().size(); i++) {
                CompanyAsset asset = product.getAssets().get(i);
                if (asset != null) {
                    switch (i) {
                        case 0:
                            imageURL1 = asset.getPublicURL();
                            break;
                        case 1:
                            imageURL2 = asset.getPublicURL();
                            break;
                        case 2:
                            imageURL3 = asset.getPublicURL();
                            break;
                        case 3:
                            imageURL4 = asset.getPublicURL();
                            break;
                        case 4:
                            imageURL5 = asset.getPublicURL();
                    }
                }
            }

            String brandName = "";
            Brand brand = brandHashMap.get(product.getBrandId());
            if (brand != null) {
                brandName = brand.getName();
            }
            String mg = "mg";
            if (product.getCustomGramType() == Product.CustomGramType.GRAM) {
                mg = "g";
            }


            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], sku);
            data.put(attrs[1], name);
            data.put(attrs[2], categoryName);
            data.put(attrs[3], (category != null && category.isCannabis()) ? "Yes" : "No");
            data.put(attrs[4], measurement);
            data.put(attrs[5], new DollarAmount(costPerUnit));
            data.put(attrs[6], new DollarAmount(unitPrice));
            data.put(attrs[7], new DollarAmount(gramPrice));
            data.put(attrs[8], new DollarAmount(halfGramPrice));
            data.put(attrs[9], new DollarAmount(twoGramsPrice));
            data.put(attrs[10], new DollarAmount(threeGramPrice));
            data.put(attrs[11], new DollarAmount(eigthPrice));
            data.put(attrs[12], new DollarAmount(fourthPrice));
            data.put(attrs[13], new DollarAmount(halfthPrice));
            data.put(attrs[14], new DollarAmount(ouncePrice));
            data.put(attrs[15], type);
            data.put(attrs[16], desc);
            data.put(attrs[17], NumberUtils.round(thc, 2));
            data.put(attrs[18], NumberUtils.round(cbd, 2));
            data.put(attrs[19], NumberUtils.round(cbn, 2));
            data.put(attrs[20], 0);
            data.put(attrs[21], datePurchase);
            data.put(attrs[22], vendorId);
            data.put(attrs[23], vendorName);
            data.put(attrs[24], "");
            data.put(attrs[25], imageURL1);
            data.put(attrs[26], imageURL2);
            data.put(attrs[27], imageURL3);
            data.put(attrs[28], imageURL4);
            data.put(attrs[29], imageURL5);
            data.put(attrs[30], product.getId());
            data.put(attrs[31], brandName);
            data.put(attrs[32], product.getCannabisType().name());
            data.put(attrs[33], product.getWeightPerUnit().name());
            data.put(attrs[34], (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS ? NumberUtils.round(product.getCustomWeight(),2) : ""));
            data.put(attrs[35], (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS ? mg : ""));
            data.put(attrs[36], (product.isActive() ? "Yes" : "No"));

            String tagArray = "";
            if (product.getTags() != null) {
                String[] arr = new String[product.getTags().size()];
                product.getTags().toArray(arr);
                tagArray = String.join(";",arr);
            }

            data.put(attrs[37], tagArray);

            for (Inventory inventory : inventoryList) {
                if (inventory.isActive()) {
                    double amt = 0;
                    for (ProductQuantity productQuantity : product.getQuantities()) {
                        if (inventory.getId().equalsIgnoreCase(productQuantity.getInventoryId())) {
                            amt = productQuantity.getQuantity().doubleValue();
                        }
                    }

                    data.put("INV: " + inventory.getName(), NumberUtils.round(amt, 2));
                }
            }


            report.add(data);
        }

        return report;
    }
}
