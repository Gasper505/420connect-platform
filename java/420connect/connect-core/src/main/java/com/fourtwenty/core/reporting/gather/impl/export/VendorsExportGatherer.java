package com.fourtwenty.core.reporting.gather.impl.export;


import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class VendorsExportGatherer implements Gatherer {

    @Inject
    private VendorRepository vendorRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{"Vendor Id", "Vendor Name", "Contact First Name", "Contact Last Name", "Phone", "Email", "Active", "Website", "Street Address", "City", "State", "Zip Code", "License Number"};

    public VendorsExportGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Vendor Id
                GathererReport.FieldType.STRING, // Vendor Name
                GathererReport.FieldType.STRING, // Contact First Name
                GathererReport.FieldType.STRING, // Contact Last Name
                GathererReport.FieldType.STRING, // Phone
                GathererReport.FieldType.STRING, // Email
                GathererReport.FieldType.STRING, // Active
                GathererReport.FieldType.STRING, // Website
                GathererReport.FieldType.STRING, // Street Address
                GathererReport.FieldType.STRING, // City
                GathererReport.FieldType.STRING, // State
                GathererReport.FieldType.STRING, // Zip code
                GathererReport.FieldType.STRING, // Licence Number
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Vendor Export", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Iterable<Vendor> vendors = vendorRepository.listAll(filter.getCompanyId());

        if (vendors == null || Iterables.isEmpty(vendors)) {
            return report;
        }

        for (Vendor vendor : vendors) {
            int i = 0;
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getId()) ? vendor.getId() : "N/A");
            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getName()) ? vendor.getName() : "N/A");
            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getFirstName()) ? vendor.getFirstName() : "N/A");
            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getLastName()) ? vendor.getLastName() : "N/A");
            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getPhone()) ? vendor.getPhone() : "N/A");
            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getEmail()) ? vendor.getEmail() : "N/A");
            data.put(attrs[i++], vendor.getActive()); //Active

            data.put(attrs[i++], StringUtils.isNotBlank(vendor.getWebsite()) ? vendor.getWebsite() : "N/A");

            Address address = vendor.getAddress();
            if (address != null) {
                data.put(attrs[i++], address.getAddress() == null ? "N/A" : address.getAddress());
                data.put(attrs[i++], address.getCity() == null ? "N/A" : address.getCity());
                data.put(attrs[i++], address.getState() == null ? "N/A" : address.getState());
                data.put(attrs[i++], address.getZipCode() == null ? "N/A" : address.getZipCode());
            } else {
                data.put(attrs[i++], "N/A");
                data.put(attrs[i++], "N/A");
                data.put(attrs[i++], "N/A");
                data.put(attrs[i++], "N/A");
            }

            data.put(attrs[i], StringUtils.isNotBlank(vendor.getLicenseNumber()) ? vendor.getLicenseNumber() : "N/A");
            report.add(data);
        }
        return report;
    }
}



