package com.fourtwenty.core.reporting.gather.impl.inventory;


import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FastestMovingProductByHarvestDate implements Gatherer {
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private TransactionRepository transactionRepository;

    private static final String BATCH_NOT_FOUND = "Batch Not Found";
    private static final String PRODUCT_NOT_FOUND = "Product Not Found";
    private List<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attributes = new String[]{"Product Name", "Product SKU", "Category Name", "Batch SKU", "Harvest Date", "Purchase Date", "Duration", "Product Status", "Duration In Days"};

    public FastestMovingProductByHarvestDate() {
        Collections.addAll(reportHeaders, attributes);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };

        for (int i = 0; i < attributes.length; i++) {
            fieldTypes.put(attributes[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Fastest Moving Product By Harvest Date Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        Iterable<InventoryOperation> bracketOperations = inventoryActionRepository.getOperationLogsBySourceType(filter.getCompanyId(), filter.getShopId(), filter.getSourceType(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<ObjectId> products = new HashSet<>();
        Set<ObjectId> batches = new HashSet<>();
        Set<ObjectId> sourceIds = new HashSet<>();

        bracketOperations.forEach(inventoryOperation -> {
            batches.add(new ObjectId(inventoryOperation.getBatchId()));
            products.add(new ObjectId(inventoryOperation.getProductId()));
            sourceIds.add(new ObjectId(inventoryOperation.getSourceId()));
        });
        HashMap<String, ProductBatch> batchesForProductsMap = productBatchRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(batches));
        HashMap<String, Product> productsByIds = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(products));
        HashMap<String, Transaction> transactions = transactionRepository.listAsMap(filter.getCompanyId(), sourceIds == null ? new ArrayList<>() : Lists.newArrayList(sourceIds));

        HashMap<String, Long> productWithDuration = new HashMap<>();

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MMM-dd");
        productsByIds.forEach((productId, product) -> {
            HashMap<String, Object> data = new HashMap<>();
            ProductCategory productCategory;
            int count = 0;
            for (ProductBatch productBatch : batchesForProductsMap.values()) {
                Transaction transaction = null;
                long trackHarvestDate = 0;
                String harvestDate;
                try {
                    harvestDate = StringUtils.isBlank(productBatch.getTrackHarvestDate()) ? "0" : productBatch.getTrackHarvestDate();
                    if (harvestDate.equals("0")) {
                        trackHarvestDate = productBatch.getPurchasedDate();
                    } else {
                        Date d = f.parse(harvestDate);
                        trackHarvestDate = d.getTime();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long totalDuration = 0;
                if (productBatch.getProductId().equals(productId)) {
                    for (InventoryOperation inventoryOperation : bracketOperations) {
                        if (Objects.nonNull(transactions) && inventoryOperation.getBatchId().equals(productBatch.getId())) {
                            transaction = transactions.get(inventoryOperation.getSourceId());
                            if (Objects.nonNull(transaction)) {
                                if (!productWithDuration.isEmpty() && productWithDuration.containsKey(productId)) {
                                    count++;
                                    totalDuration = (transaction.getCompletedTime() - trackHarvestDate);
                                    productWithDuration.replace(productId, (productWithDuration.get(productId) + totalDuration));
                                    data.put(attributes[7], "Sold");
                                } else if (productWithDuration.isEmpty()) {
                                    count++;
                                    totalDuration = (transaction.getCompletedTime() - trackHarvestDate);
                                    productWithDuration.put(productId, totalDuration);
                                    data.put(attributes[7], "Sold");
                                } else if (!productWithDuration.isEmpty() && !productWithDuration.containsKey(productId)) {
                                    count++;
                                    totalDuration = (transaction.getCompletedTime() - trackHarvestDate);
                                    productWithDuration.put(productId, totalDuration);
                                    data.put(attributes[7], "Sold");
                                }
                            } else {
                                productWithDuration.put(productId, 0l);
                                data.put(attributes[7], "Not Sold");
                            }
                        }
                    }
                    data.put(attributes[3], productBatch.getSku());
                    data.put(attributes[4], trackHarvestDate);
                    data.put(attributes[5], productBatch.getPurchasedDate());
                }
            }
            if (Objects.nonNull(productWithDuration) && count > 0) {
                productWithDuration.replace(productId, productWithDuration.get(productId) / count);
            }

            productCategory = productCategoryHashMap.get(product.getCategoryId());
            data.put(attributes[0], product.getName());
            data.put(attributes[1], product.getSku());
            data.put(attributes[2], productCategory == null && StringUtils.isBlank(productCategory.getName()) ? "N/A" : productCategory.getName());

            data.put(attributes[6], Objects.nonNull(productWithDuration.get(productId)) ? productWithDuration.get(productId) : 0l);
            data.put(attributes[8], formatTime(Objects.nonNull(productWithDuration.get(productId)) ? productWithDuration.get(productId) : 0l));
            report.add(data);
        });
        report.getData().sort(new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
                Long duration1 = (Long) map1.getOrDefault("Duration", 0l);
                Long duration2 = (Long) map2.getOrDefault("Duration", 0l);
                if (filter.getMap().containsKey("moveType") && filter.getMap().get("moveType").equalsIgnoreCase("slowest")) {
                    return duration2.compareTo(duration1);
                } else {
                    return duration1.compareTo(duration2);
                }
            }
        });
        return report;
    }


    public String formatTime(Long millis) {
        long minMillis = 1000 * 60;
        long hourMillis = minMillis * 60;
        long dayMillis = hourMillis * 24;
        String formatted;
        if (millis >= dayMillis) { //input millis is greater than 1 day of time
            String days = Long.toString(millis / dayMillis);
            long hrs = millis % dayMillis;
            String hours = Long.toString(hrs / hourMillis);
            long mns = hrs % hourMillis;
            String minutes = Long.toString(mns / minMillis);
            long secs = mns % minMillis;
            String seconds = Long.toString(secs / 1000);
            formatted = days + "d, " + hours + "h, " + minutes + "m, " + seconds + "s";
        } else if (millis >= hourMillis) { //input millis is greater than 1 hour of time
            String hours = Long.toString(millis / hourMillis);
            long mns = millis % hourMillis;
            String minutes = Long.toString(mns / minMillis);
            long secs = mns % minMillis;
            String seconds = Long.toString(secs / 1000);
            formatted = hours + "h, " + minutes + "m, " + seconds + "s";
        } else if (millis >= minMillis) { //input millis is greater than 1 minute of time
            String minutes = Long.toString(millis / minMillis);
            long mins = millis % minMillis;
            String seconds = Long.toString(mins / 1000);
            formatted = minutes + "m, " + seconds + "s";
        } else { //input millis are less that 1 minute of time
            formatted = Long.toString(millis / 1000) + "s";
        }
        return formatted;
    }

}