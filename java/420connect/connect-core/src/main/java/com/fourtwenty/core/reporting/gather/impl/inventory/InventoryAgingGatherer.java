package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InventoryAgingGatherer implements Gatherer {

    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;

    private List<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = new String[]{"Product Id", "Product Name", "Product Category", "30 Days Units Sold", "0-30 Days Quantity", "0-30 Days Valuation",
            "31-60 Days Quantity", "31-60 Days Valuation", "61-90 Days Quantity", "61-90 Days Valuation", "90 Days + Quantity", "90 Days + Valuation"};

    public InventoryAgingGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Product Aging Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        DateTime currentTime = DateTime.now();
        DateTime thirtyDays = currentTime.minusDays(30);
        DateTime sixtyDays = currentTime.minusDays(60);
        DateTime ninetyDays = currentTime.minusDays(90);
        DateTime startTime = new DateTime(1536560098000L); //As inventory operations were started around 13 September 2018 so setting this data for fetching prior operations

        String projection = "{productId:1, quantity:1, sourceType:1}";
        Iterable<InventoryOperation> thirtyDaysOperations = inventoryActionRepository.getBracketOperations(filter.getCompanyId(), filter.getShopId(), thirtyDays.getMillis(), currentTime.getMillis(), projection);
        Iterable<InventoryOperation> sixtyDaysOperations = inventoryActionRepository.getBracketOperations(filter.getCompanyId(), filter.getShopId(), sixtyDays.getMillis(), thirtyDays.plusDays(1).getMillis(), projection);
        Iterable<InventoryOperation> ninetyDaysOperations = inventoryActionRepository.getBracketOperations(filter.getCompanyId(), filter.getShopId(), ninetyDays.getMillis(), sixtyDays.plusDays(1).getMillis(), projection);

        Iterable<InventoryOperation> ninetyPlusDaysOperations = inventoryActionRepository.getBracketOperations(filter.getCompanyId(), filter.getShopId(), startTime.getMillis(), ninetyDays.plusDays(1).getMillis(), projection);

        Map<String, Double> thirtyProductQuantityMap = new HashMap<>();
        Map<String, Double> soldQuantityMap = new HashMap<>();
        for (InventoryOperation operation : thirtyDaysOperations) {
            if (InventoryOperation.SourceType.Transaction == operation.getSourceType()) {
                soldQuantityMap.putIfAbsent(operation.getProductId(), 0d);
                Double quantity = soldQuantityMap.get(operation.getProductId());
                Double oprQuantity = operation.getQuantity().doubleValue();
                quantity += oprQuantity;

                soldQuantityMap.put(operation.getProductId(), quantity);
            }

            calculateQuantity(thirtyProductQuantityMap, operation);
        }

        Map<String, Double> sixtyProductQuantityMap = new HashMap<>();
        for (InventoryOperation operation : sixtyDaysOperations) {
            calculateQuantity(sixtyProductQuantityMap, operation);
        }

        Map<String, Double> ninetyProductQuantityMap = new HashMap<>();
        for (InventoryOperation operation : ninetyDaysOperations) {
            calculateQuantity(ninetyProductQuantityMap, operation);
        }

        Map<String, Double> ninetyPlusProductQuantityMap = new HashMap<>();
        for (InventoryOperation operation : ninetyPlusDaysOperations) {
            calculateQuantity(ninetyPlusProductQuantityMap, operation);
        }

        Set<String> productIds = new LinkedHashSet<>();
        productIds.addAll(thirtyProductQuantityMap.keySet());
        productIds.addAll(sixtyProductQuantityMap.keySet());
        productIds.addAll(ninetyProductQuantityMap.keySet());
        productIds.addAll(ninetyPlusProductQuantityMap.keySet());

        Map<String, ProductBatch> productBatchMap = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productIds));

        String notAvail = "N/A";
        for (Map.Entry<String, Product> entry : productMap.entrySet()) {

            Product product = entry.getValue();
            if (product == null) {
                continue;
            }
            ProductCategory category = productCategoryMap.get(product.getCategoryId());
            ProductBatch batch = productBatchMap.get(product.getId());

            double soldQuantity = soldQuantityMap.get(entry.getKey()) != null ? soldQuantityMap.get(entry.getKey()) : 0D;
            double thirtyDayQuantity = thirtyProductQuantityMap.get(entry.getKey()) != null ? thirtyProductQuantityMap.get(entry.getKey()) : 0D;
            double sixtyDayQuantity = sixtyProductQuantityMap.get(entry.getKey()) != null ? sixtyProductQuantityMap.get(entry.getKey()) : 0D;
            double ninetyDayQuantity = ninetyProductQuantityMap.get(entry.getKey()) != null ? ninetyProductQuantityMap.get(entry.getKey()) : 0D;
            double ninetyPlusDayQuantity = ninetyPlusProductQuantityMap.get(entry.getKey()) != null ? ninetyPlusProductQuantityMap.get(entry.getKey()) : 0D;

            double finalCost = (batch != null ? batch.getFinalUnitCost().doubleValue() : 0D);

            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[0], entry.getKey());
            data.put(attrs[1], product.getName());
            data.put(attrs[2], category != null ? category.getName() : notAvail);
            data.put(attrs[3], Math.abs(soldQuantity));

            double quantity = thirtyDayQuantity + sixtyDayQuantity + ninetyDayQuantity + ninetyPlusDayQuantity;
            data.put(attrs[4], quantity);
            data.put(attrs[5], Math.abs(quantity * finalCost));

            quantity = sixtyDayQuantity + ninetyDayQuantity + ninetyPlusDayQuantity;
            data.put(attrs[6], quantity);
            data.put(attrs[7], Math.abs(quantity * finalCost));

            quantity = ninetyDayQuantity + ninetyPlusDayQuantity;
            data.put(attrs[8], quantity);
            data.put(attrs[9], Math.abs(quantity * finalCost));

            quantity = ninetyPlusDayQuantity;
            data.put(attrs[10], quantity);
            data.put(attrs[11], Math.abs(quantity * finalCost));

            report.add(data);
        }


        return report;
    }

    private void calculateQuantity(Map<String, Double> productQuantityMap, InventoryOperation operation) {
        productQuantityMap.putIfAbsent(operation.getProductId(), 0d);
        Double quantity = productQuantityMap.get(operation.getProductId());
        Double oprQuantity = operation.getQuantity().doubleValue();

        quantity += oprQuantity;

        productQuantityMap.put(operation.getProductId(), quantity);
    }
}
