package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.*;

/**
 * This class is responsible to show the report of inventory on product category basis.
 */
public class InventoryByCategoryGatherer implements Gatherer {
    private static final String INVENTORY_VALUATION_BY_CATEGORY_REPORT = "Inventory By Category Report";
    private static final String[] REPORT_ATTRS = new String[]{
            "Product Category",
            "Total Production Quantity",
            "Total Production COGS",
            "Total Prepackages",
            "Total Prepackages COGS",
            "Margin %"
    };
    private ProductRepository productRepository;
    private ProductCategoryRepository productCategoryRepository;
    private InventoryRepository inventoryRepository;
    private ProductBatchRepository batchRepository;
    private PrepackageRepository prepackageRepository;
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    private HashMap<String, ProductCategory> productCategoryMap;
    private HashMap<String, Product> productHashMap;
    private HashMap<String, ProductBatch> productBatchHashMap;
    private Iterable<Prepackage> prepackageList;
    private List<ProductPrepackageQuantity> productPrepackageQuantities;
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private double productQuantity;
    private double productCOGS;
    private int totalQuantity;
    private double cogs;
    private double margin;
    private double retailValue;
    private StringBuilder stringProductionCOGS;
    private StringBuilder stringPrepackage;
    private HashMap<String, Integer> prepackageInfo;
    private HashMap<String, Double> prepackageCogsInfo;

    /**
     * Constructor
     *
     * @param productRepository            : for product information.
     * @param productCategoryRepository    : for Product category information.
     * @param inventoryRepository          : for inventory information.
     * @param batchRepository              : for product batch information.
     * @param prepackageRepository         : for prepackage information.
     * @param prepackageQuantityRepository : for prepackage quantity.
     */
    public InventoryByCategoryGatherer(ProductRepository productRepository, ProductCategoryRepository productCategoryRepository, InventoryRepository inventoryRepository, ProductBatchRepository batchRepository, PrepackageRepository prepackageRepository, ProductPrepackageQuantityRepository prepackageQuantityRepository) {
        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.inventoryRepository = inventoryRepository;
        this.batchRepository = batchRepository;
        this.prepackageRepository = prepackageRepository;
        this.prepackageQuantityRepository = prepackageQuantityRepository;

        Collections.addAll(reportHeaders, REPORT_ATTRS);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.PERCENTAGE
        };
        for (int i = 0; i < REPORT_ATTRS.length; i++) {
            fieldTypes.put(REPORT_ATTRS[i], types[i]);
        }
    }

    /**
     * Overridden Method
     *
     * @param filter :  for report information like companyId, shopId, token etc.
     * @return report   :  return object contains reports data with headers.
     */
    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report;
        Iterable<Inventory> inventories;
        HashMap<String, ProductBatch> updatedBatches;
        HashMap<String, String> inventoryMap;
        HashMap<String, Object> data = null;


        report = new GathererReport(filter, INVENTORY_VALUATION_BY_CATEGORY_REPORT, reportHeaders);
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
        report.setReportFieldTypes(fieldTypes);
        productHashMap = productRepository.listAsMap(filter.getCompanyId());
        productCategoryMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        inventories = inventoryRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        prepackageList = prepackageRepository.findItems(filter.getCompanyId(), filter.getShopId(), 0, Integer.MAX_VALUE).getValues();
        productPrepackageQuantities = prepackageQuantityRepository.getProductPrepackageQuantities(filter.getCompanyId(), filter.getShopId());
        productBatchHashMap = batchRepository.listAsMap(filter.getCompanyId());
        inventoryMap = new HashMap<>();

        /* Get the products on Category basis*/
        HashMap<String, ArrayList<Product>> productListOnCategoryBasis = getCategoryBasisProducts(filter.getShopId());

        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        int numOfProducts = 0;
        for (Product p : productHashMap.values()) {
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
            if (p.getShopId().equalsIgnoreCase(filter.getShopId())) {
                numOfProducts++;
            }
        }


        /* Retrieve inventory ID and NAME.*/
        inventories.forEach((Inventory inventory) -> {
            if (inventory.isActive()) {
                inventoryMap.put(inventory.getId(), inventory.getName());
            }
        });

        /* Retrieve product batches. */
        updatedBatches = getProductBatch();

        for (Map.Entry<String, ArrayList<Product>> entry : productListOnCategoryBasis.entrySet()) {
            String categoryName;
            data = new HashMap<>();
            ProductCategory category = productCategoryMap.get(entry.getKey());
            categoryName = category.getName() == null ? StringUtils.EMPTY : category.getName();
            productQuantity = productCOGS = 0.0;
            stringPrepackage = new StringBuilder();
            stringProductionCOGS = new StringBuilder();
            prepackageInfo = new HashMap<>();
            prepackageCogsInfo = new HashMap<>();
            totalQuantity = 0;
            cogs = 0;
            retailValue = 0.0;
            margin = 0.0;

            for (Product product : entry.getValue()) {
                calculateProductionQuantitiesAndCogs(product, inventoryMap, updatedBatches, productsByCompanyLinkId);
                calculatePrepackageAndProductionCogs(product, updatedBatches, productsByCompanyLinkId);
            }

            if (retailValue > 0) {
                margin = productCOGS / retailValue;
            }

            //Extract prepackage name and total quantity.
            prepackageInfo.forEach((key, value) -> stringPrepackage.append(String.format("%s: %d ,", key, value)));

            //Extract COGS value
            prepackageCogsInfo.forEach((key, value) -> stringProductionCOGS.append(String.format("%s: %.02f ,", key, value)));

            data.put(REPORT_ATTRS[0], categoryName);
            data.put(REPORT_ATTRS[1], productQuantity);
            data.put(REPORT_ATTRS[2], new DollarAmount(productCOGS));
            data.put(REPORT_ATTRS[3], stringPrepackage);
            data.put(REPORT_ATTRS[4], stringProductionCOGS);
            data.put(REPORT_ATTRS[5], new Percentage(margin));

            report.add(data);
        }
        return report;
    }

    /**
     * Category basis products.
     *
     * @return product list on category basis.
     */
    private HashMap<String, ArrayList<Product>> getCategoryBasisProducts(String shopId) {
        HashMap<String, ArrayList<Product>> temp = new HashMap<>();
        ;

        for (Product product : productHashMap.values()) {
            if (product.getShopId().equalsIgnoreCase(shopId)) {
                ProductCategory productCategory = productCategoryMap.get(product.getCategoryId());
                if (productCategory != null) {
                    ArrayList<Product> productList = temp.get(productCategory.getId());
                    if (productList == null) {
                        productList = new ArrayList<>();
                    }
                    productList.add(product);
                    temp.put(productCategory.getId(), productList);
                }
            }
        }
        return temp;
    }

    /**
     * Filter the product batches.
     *
     * @return product batch information.
     */
    private HashMap<String, ProductBatch> getProductBatch() {
        HashMap<String, ProductBatch> temp = new HashMap<>();
        productBatchHashMap.values().forEach((ProductBatch productBatch) -> {
            Product product = productHashMap.get(productBatch.getProductId());
            if (product != null) {
                if (temp.containsKey(product.getId())) {
                    ProductBatch previousBatch = temp.get(product.getId());
                    if (productBatch.getCreated() > previousBatch.getCreated()) {
                        temp.put(product.getId(), productBatch);
                    }
                } else {
                    temp.put(product.getId(), productBatch);
                }
            }
        });
        return temp;
    }

    /**
     * Calculate cost.
     *
     * @return calculated cost.
     */
    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    /**
     * Calculate production quantities and COGS.
     *
     * @param product      : for product information.
     * @param inventoryMap : for inventory information.
     * @param batches      : for product batch information.
     */
    private void calculateProductionQuantitiesAndCogs(Product product, HashMap<String, String> inventoryMap, HashMap<String, ProductBatch> batches, HashMap<String, List<Product>> productByCompanyLink) {
        for (ProductQuantity pq : product.getQuantities()) {
            String id = pq.getInventoryId(); //hold the id for the inventory
            if (inventoryMap.keySet().contains(id)) {
                double quantity = pq.getQuantity().doubleValue();
                productQuantity += quantity; //update product total with this inventory value
                double cogs = quantity * getUnitCost(product, batches, productByCompanyLink); //compute cogs for this inventory
                productCOGS += cogs; //update product cogs with this inv. value
            }
        }
        retailValue += productQuantity * product.getUnitPrice().doubleValue();
    }

    /**
     * Calculate prepackage and production COGS
     *
     * @param product : for product information.
     * @param batches : for product batch information.
     */
    private void calculatePrepackageAndProductionCogs(Product product, HashMap<String, ProductBatch> batches, HashMap<String, List<Product>> productByCompanyLink) {
        for (Prepackage prepackage : prepackageList) {
            if (prepackage.getProductId().equals(product.getId())) {
                for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
                    if (prepackageQuantity.getPrepackageId().equals(prepackage.getId())) {
                        totalQuantity += prepackageQuantity.getQuantity();

                        //Calculate cogs for prepackage of product
                        cogs = prepackage.getUnitValue().doubleValue() * getUnitCost(product, batches, productByCompanyLink); //compute cogs for this inventory
                    }
                }
                prepackageInfoUpdate(prepackage, totalQuantity);
                prepackageInfoUpdate(prepackage, cogs);
            }
        }
    }

    /**
     * Calculate COGS
     *
     * @param quantity : having product quantity information.
     * @param batch    : having product batch information.
     * @return calculation
     */
    private double calcCogs(final double quantity, final ProductBatch batch) {

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    /**
     * @param prepackage : for prepackage information.
     * @param value      :values
     */
    private void prepackageInfoUpdate(Prepackage prepackage, int value) {
        if (prepackageInfo.keySet().contains(prepackage.getName())) {
            int totalQ = prepackageInfo.get(prepackage.getName());
            int newQuantities = totalQ + value;
            prepackageInfo.replace(prepackage.getName(), totalQ, newQuantities);
        } else {
            prepackageInfo.put(prepackage.getName(), value);
        }
    }

    /**
     * Overloaded Method
     *
     * @param prepackage : for prepackage information.
     * @param value      :values
     */
    private void prepackageInfoUpdate(Prepackage prepackage, double value) {
        if (prepackageCogsInfo.keySet().contains(prepackage.getName())) {
            double oldCogs = prepackageCogsInfo.get(prepackage.getName());
            double newCogs = oldCogs + value;
            prepackageCogsInfo.replace(prepackage.getName(), oldCogs, newCogs);
        } else {
            prepackageCogsInfo.put(prepackage.getName(), value);
        }
    }
}
