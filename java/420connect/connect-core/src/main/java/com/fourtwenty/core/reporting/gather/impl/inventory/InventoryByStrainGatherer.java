package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;

import java.util.*;

/**
 * Created by Stephen Schmidt on 6/21/2016.
 */
public class InventoryByStrainGatherer implements Gatherer {
    private ProductRepository productRepository;
    private ProductBatchRepository batchRepository;
    private String[] attrs = new String[]{"Cannabis Type", "Inventory (COGS)"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryByStrainGatherer(ProductRepository repository, ProductBatchRepository batchRepository) {
        this.productRepository = repository;
        this.batchRepository = batchRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory By Cannabis Type", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, Product> results = productRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        Collection<ProductBatch> batches = batchRepository.listAsMap(filter.getCompanyId(), filter.getShopId()).values();
        HashMap<String, ProductBatch> batchMap = new HashMap<>();
        for (ProductBatch batch : batches) {
            batchMap.put(batch.getProductId(), batch);
        }

        HashMap<String, Double> totalQuantities = new HashMap<>();
        Collection<Product> products = results.values();
        for (Product p : products) { //gather the total units of the product accross all inventories
            List<ProductQuantity> quants = p.getQuantities();
            double units = 0;
            for (ProductQuantity q : quants) {
                units += q.getQuantity().doubleValue();
            }
            totalQuantities.put(p.getId(), units); //map of productIds mapped to the quantity of that product
        }


        HashMap<String, Double> cogsByStrain = new HashMap<>();
        for (Product p : results.values()) {
            if (p.getFlowerType() != null) { //only value active products
                ProductBatch b = batchMap.get(p.getId());
                if (b != null) {
                    String flowerType = p.getFlowerType().toLowerCase();
                    if (!p.getFlowerType().isEmpty() && (flowerType.equals("sativa")
                            || flowerType.equals("indica")
                            || flowerType.equals("hybrid")
                            || flowerType.equals("cdb"))) {
                        double unitCost = b.getFinalUnitCost().doubleValue();
                        //If this strain is in our map already, add the current product cogs to the map, else just do a simple put.
                        if (cogsByStrain.containsKey(flowerType)) {
                            double newCogs = cogsByStrain.get(flowerType) + totalQuantities.get(p.getId()) * unitCost;
                            cogsByStrain.put(flowerType, newCogs);
                        } else {
                            cogsByStrain.put(flowerType, totalQuantities.get(p.getId()) * unitCost);
                        }
                    }
                }
                //cogs by strain map complete, load into report data map


            }
        }
        for (String key : cogsByStrain.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], key);
            data.put(attrs[1], new DollarAmount(cogsByStrain.get(key)));
            report.add(data);
        }
        return report;
    }
}
