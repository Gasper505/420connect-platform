package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 12/10/17.
 */
public class InventoryDistributionReport implements Gatherer {
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductPrepackageQuantityRepository prepackageQuantityRepository;

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryDistributionReport() {
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        reportHeaders.add("Product");
        fieldTypes.put("Product", GathererReport.FieldType.STRING);
        reportHeaders.add("Category");
        fieldTypes.put("Category", GathererReport.FieldType.STRING);
        reportHeaders.add("Unit Type");
        fieldTypes.put("Unit Type", GathererReport.FieldType.STRING);
        reportHeaders.add("Total");
        fieldTypes.put("Total", GathererReport.FieldType.NUMBER);

        Iterable<Inventory> inventoryIterable = inventoryRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        List<Inventory> inventories = Lists.newArrayList(inventoryIterable);
        inventories.sort(new Comparator<Inventory>() {
            @Override
            public int compare(Inventory o1, Inventory o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (Inventory inventory : inventories) {
            if (inventory.isActive()) {
                reportHeaders.add(inventory.getName());
                fieldTypes.put(inventory.getName(), GathererReport.FieldType.NUMBER);
            }
        }

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Prepackage> prepackages = prepackageRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());


        Iterable<Product> products = null;

        if (ReportFilter.ALL_CATEGORIES.equals(filter.getCategoryId())) {
            products = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        } else {
            products = productRepository.getProductsForCategory(filter.getCompanyId(), filter.getCategoryId());
        }

        // create the data structure
        HashMap<String, List<Prepackage>> productToPrepackages = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            List<Prepackage> prepackageList = productToPrepackages.get(prepackage.getProductId());

            if (prepackageList == null) {
                prepackageList = new ArrayList<>();
            }
            prepackageList.add(prepackage);
            productToPrepackages.put(prepackage.getProductId(), prepackageList);
        }

        // structure for prepackageQuantities
        HashMap<String, List<ProductPrepackageQuantity>> prepackagesToQuantities = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
            List<ProductPrepackageQuantity> items = prepackagesToQuantities.get(prepackageQuantity.getPrepackageId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(prepackageQuantity);
            prepackagesToQuantities.put(prepackageQuantity.getPrepackageId(), items);
        }

        // Sort the products
        List<Product> orderedProducts = Lists.newArrayList(products);
        orderedProducts.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });


        // Now create the report
        GathererReport report = new GathererReport(filter, "Inventory Distribution Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        for (Product product : products) {
            String productName = product.getName();
            String categoryName = "Unknown";
            String unitType = "units";
            if (StringUtils.isNotBlank(product.getCategoryId())) {
                ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
                if (category != null) {
                    categoryName = category.getName();
                    unitType = category.getUnitType().toString();
                }
            }

            // now to specify total
            HashMap<String, Object> mainRow = createRow(productName, categoryName, product.getQuantities(), inventories, unitType);
            report.add(mainRow);


            List<Prepackage> prepackageList = productToPrepackages.get(product.getId());
            if (prepackageList != null && prepackageList.size() > 0) {

                for (Prepackage prepackage : prepackageList) {
                    if (prepackage.isActive() && !prepackage.isDeleted()) {
                        String productWithPrepackage = String.format("%s (%s)", productName, prepackage.getName());
                        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackagesToQuantities.get(prepackage.getId());

                        HashMap<String, Object> row = createRowPrepackages(productWithPrepackage,
                                categoryName, productPrepackageQuantities,
                                inventories, unitType);
                        report.add(row);
                    }

                }

            }
        }


        return report;
    }

    private HashMap<String, Object> createRow(String productName, String categoryName,
                                              List<ProductQuantity> quantities,
                                              List<Inventory> inventories, String unitType) {
        HashMap<String, Object> row = new HashMap<>();
        row.put("Product", productName);
        row.put("Category", categoryName);
        row.put("Unit Type", unitType);

        double total = 0;
        HashMap<String, BigDecimal> quantityHashMap = new HashMap<>();
        if (quantities != null) {
            for (ProductQuantity quantity : quantities) {

                BigDecimal oldQty = quantityHashMap.get(quantity.getInventoryId());
                if (oldQty == null) {
                    oldQty = new BigDecimal(0);
                }
                oldQty = oldQty.add(quantity.getQuantity());
                quantityHashMap.put(quantity.getInventoryId(), oldQty);
                total += quantity.getQuantity().doubleValue();
            }
        }
        row.put("Total", NumberUtils.round(total, 2));

        for (Inventory inventory : inventories) {
            if (inventory.isActive() && !inventory.isDeleted()) {
                String inventoryName = inventory.getName();
                BigDecimal quantity = quantityHashMap.get(inventory.getId());
                if (quantity == null) {
                    quantity = new BigDecimal(0);
                }
                row.put(inventoryName, NumberUtils.round(quantity, 2));
            }
        }
        return row;
    }

    private HashMap<String, Object> createRowPrepackages(String productName, String categoryName,
                                                         List<ProductPrepackageQuantity> prepackageQuantities,
                                                         List<Inventory> inventories, String unitType) {
        HashMap<String, Object> row = new HashMap<>();
        row.put("Product", productName);
        row.put("Category", categoryName);
        row.put("Unit Type", unitType);

        if (productName.equalsIgnoreCase("Purple Haze (1/8th)")) {
            System.out.println("stop");
        }

        HashMap<String, Integer> quantityHashMap = new HashMap<>();
        int total = 0;
        if (prepackageQuantities != null) {
            for (ProductPrepackageQuantity quantity : prepackageQuantities) {
                Integer oldQty = quantityHashMap.get(quantity.getInventoryId());
                if (oldQty == null) {
                    oldQty = 0;
                }
                oldQty += quantity.getQuantity();

                quantityHashMap.put(quantity.getInventoryId(), oldQty);
                total += quantity.getQuantity();
            }
        }
        row.put("Total", NumberUtils.round(total, 2));

        for (Inventory inventory : inventories) {
            if (inventory.isActive() && !inventory.isDeleted()) {
                String inventoryName = inventory.getName();
                Integer quantity = quantityHashMap.get(inventory.getId());
                if (quantity == null) {
                    quantity = 0;
                }
                row.put(inventoryName, NumberUtils.round(quantity, 2));
            }
        }
        return row;
    }
}
