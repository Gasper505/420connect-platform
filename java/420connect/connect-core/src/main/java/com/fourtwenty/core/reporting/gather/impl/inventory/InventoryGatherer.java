package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

/**
 * Created by Stephen Schmidt on 5/14/2016.
 * <p>
 * THIS CLASS SUCKS BAD AND DOESNT FOLLOW ANY OF THE OTHER GATHERER CONVENTIONS. I PLAN ON REFACTORING THIS REPORT IN
 * THE NEAR FUTURE. DON'T JUDGE ME BASED ON THIS CODE PLEASE. THANKS.
 */
public class InventoryGatherer implements Gatherer {
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private HashSet<String> activeInventoryIds = new HashSet<>();

    public InventoryGatherer() {

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        //add the first three columns to the headers
        reportHeaders.add("Product Name");
        fieldTypes.put("Product Name", GathererReport.FieldType.STRING);
        reportHeaders.add("Product Category");
        fieldTypes.put("Product Category", GathererReport.FieldType.STRING);
        reportHeaders.add("Flower Type");
        fieldTypes.put("Flower Type", GathererReport.FieldType.STRING);
        reportHeaders.add("Status");
        fieldTypes.put("Status", GathererReport.FieldType.STRING);
        reportHeaders.add("Unit Cost");
        fieldTypes.put("Unit Cost", GathererReport.FieldType.CURRENCY);
        reportHeaders.add("Retail Price");
        fieldTypes.put("Retail Price", GathererReport.FieldType.CURRENCY);

        reportHeaders.add("Total Prod. Quantity");
        fieldTypes.put("Total Prod. Quantity", GathererReport.FieldType.NUMBER);
        reportHeaders.add("Total Prod. COGS");
        fieldTypes.put("Total Prod. COGS", GathererReport.FieldType.CURRENCY);
        reportHeaders.add("Pre-Packages");
        fieldTypes.put("Pre-Packages", GathererReport.FieldType.STRING);
        reportHeaders.add("Pre-Packages COGS");
        fieldTypes.put("Pre-Packages COGS", GathererReport.FieldType.STRING);

        HashMap<String, String> inventoryMap = new HashMap<>();
        Iterable<Inventory> inventories = inventoryRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        //Build the headers and stuff for this report
        for (Inventory i : inventories) {
            if (i.isActive()) {
                activeInventoryIds.add(i.getId());
                inventoryMap.put(i.getId(), i.getName()); //build a map where key=inventoryID  & value=inventory name
                reportHeaders.add(i.getName() + " Quantity"); //add a column for this inventory's quantity
                fieldTypes.put(i.getName() + " Quantity", GathererReport.FieldType.NUMBER);
                reportHeaders.add(i.getName() + " COGS"); //add a column for this inventory's COGS
                fieldTypes.put(i.getName() + " COGS", GathererReport.FieldType.CURRENCY);
            }
        }

        reportHeaders.add("Margin %");
        fieldTypes.put("Margin %", GathererReport.FieldType.PERCENTAGE);

        GathererReport report = new GathererReport(filter, "Comprehensive Inventory Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Product> productList = productRepository.findItems(filter.getCompanyId(), filter.getShopId(), 0, Integer.MAX_VALUE).getValues();
        HashMap<String, ProductBatch> newestBatches = new HashMap<>();
        HashMap<String, ProductBatch> batchMap = batchRepository.listAsMap(filter.getCompanyId());
        Iterable<Prepackage> prepackageList = prepackageRepository.findItems(filter.getCompanyId(), filter.getShopId(), 0, Integer.MAX_VALUE).getValues();
        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getProductPrepackageQuantities(filter.getCompanyId(), filter.getShopId());
        ProductWeightTolerance gramWeightTolerance = weightToleranceRepository.getToleranceForWeight(filter.getCompanyId(), ProductWeightTolerance.WeightKey.GRAM);

        Iterable<Product> allProducts = productRepository.list(filter.getCompanyId());
        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, Product> allProductMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : allProducts) {
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);

            allProductMap.put(p.getId(), p);
            if (p.getShopId().equalsIgnoreCase(filter.getShopId())) {
                productMap.put(p.getId(), p);
            }
        }


        for (ProductBatch batch : batchMap.values()) {
            ProductBatch oldBatch = newestBatches.get(batch.getProductId());
            if (oldBatch == null) {
                newestBatches.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                newestBatches.put(batch.getProductId(), batch);
            }
        }
        StringBuilder sb;
        StringBuilder cogsBuilder;
        InventoryReport inventoryReport = new InventoryReport(report, inventoryMap, newestBatches); //construct a new inventory report object to handle the logic
        for (Product p : productList) {
            sb = new StringBuilder();
            ProductCategory productCategory = productCategoryMap.get(p.getCategoryId());
            if (productCategory == null || productCategory.isDeleted()) {
                continue;
            }
            cogsBuilder = new StringBuilder();
            HashMap<String, Object> row = new HashMap<>();

            if (p.getShopId().equalsIgnoreCase(filter.getShopId())) {
                inventoryReport.add(p, reportHeaders, row, productsByCompanyLinkId, productCategoryMap, gramWeightTolerance);
                // add prepackage
                for (Prepackage prepackage : prepackageList) {
                    if (prepackage.getProductId().equals(p.getId())) {
                        int totalQuantity = 0;
                        double cogs = 0;
                        for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
                            if (prepackageQuantity.getPrepackageId().equals(prepackage.getId())) {
                                totalQuantity += prepackageQuantity.getQuantity();

                                //Calculate cogs for prepackage of product

                                double unitCost = getUnitCost(p, newestBatches, productsByCompanyLinkId);
                                cogs = unitCost * prepackage.getUnitValue().doubleValue();
                            }
                        }

                        sb.append(String.format("%s: %d ,", prepackage.getName(), totalQuantity));
                        cogsBuilder.append(String.format("%s: %.02f ,", prepackage.getName(), cogs));
                    }
                }
                row.put("Pre-Packages", sb);
                row.put("Pre-Packages COGS", cogsBuilder);
                row.put("Status", (p.isActive()) ? "Active" : "InActive");
            }
        }


        return inventoryReport.getReport();
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {
        if (batch == null) {
            return 0;
        }
        double cost = batch.getFinalUnitCost().doubleValue();

        double total = quantity * cost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    private class InventoryReport {
        private double totalCOGS = 0.0; //total COGS for all the shop inventory
        private HashMap<String, Double> cogsByInventoryId = new HashMap<>();
        GathererReport report;
        HashMap<String, String> inventoryMap;
        HashMap<String, ProductBatch> batchMap;

        public InventoryReport(GathererReport report, HashMap<String, String> inventories, HashMap<String, ProductBatch> batchMap) {
            this.report = report;
            this.batchMap = batchMap;
            inventoryMap = inventories;
            for (String inv : inventories.keySet()) {
                cogsByInventoryId.put(inv, 0.0);
            }
        }

        private void add(Product product, ArrayList<String> headers, HashMap<String, Object> row,
                         HashMap<String, List<Product>> productByCompanyLink, HashMap<String, ProductCategory> productCategoryHashMap,
                         ProductWeightTolerance gramWeightTolerance) {

            for (String header : headers) {
                row.put(header, 0L);
            }
            row.put("Product Name", product.getName()); //add product name to row
            String strain = "N/A";
            if (product.getFlowerType() != null && !product.getFlowerType().equals("")) {
                strain = product.getFlowerType();
            }
            ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
            String productCategoryName = category == null ? "" : category.getName();
            row.put("Product Category", productCategoryName);
            row.put("Flower Type", strain);


            double productQuantity, productCOGS;
            productQuantity = productCOGS = 0.0; //initialize product quant and cogs to 0
            double cost = getUnitCost(product, batchMap, productByCompanyLink);
            row.put("Unit Cost", new DollarAmount(cost));

            row.put("Retail Price", new DollarAmount(product.getUnitPrice()));
            if (product.getPriceRanges() != null && product.getPriceRanges().size() > 0) {
                if (gramWeightTolerance != null) {
                    for (ProductPriceRange pr : product.getPriceRanges()) {
                        if (pr.getWeightToleranceId().equals(gramWeightTolerance.getId())) {
                            row.put("Retail Price", new DollarAmount(pr.getPrice()));
                            break;
                        }
                    }
                } else {
                    ProductPriceRange pr = product.getPriceRanges().get(0);
                    if (pr != null) {
                        row.put("Retail Price", new DollarAmount(pr.getPrice()));
                    }
                }

            } else if (product.getPriceBreaks() != null && product.getPriceBreaks().size() > 0) {
                ProductPriceBreak priceBreak = product.getPriceBreaks().get(0);
                if (priceBreak != null) {
                    row.put("Retail Price", new DollarAmount(priceBreak.getPrice()));
                }
            }

            for (ProductQuantity pq : product.getQuantities()) {
                String id = pq.getInventoryId(); //hold the id for the inventory
                if (activeInventoryIds.contains(id)) {
                    double quantity = pq.getQuantity().doubleValue();
                    productQuantity += quantity; //update product total with this inventory value
                    row.put(inventoryMap.get(id) + " Quantity", quantity); //add this quantity to the row
                    double cogs = quantity * cost; //compute cogs for this inventory
                    productCOGS += cogs; //update product cogs with this inv. value
                    row.put(inventoryMap.get(id) + " COGS", new DollarAmount(cogs)); //add this product inventory cogs to the row
                    cogsByInventoryId.put(id, cogsByInventoryId.get(id) + cogs); //update COGS by inventory
                    totalCOGS += productCOGS; //update total shop inventory COGS
                }
            }

            double retailValue = productQuantity * product.getUnitPrice().doubleValue();
            double margin = 0.0;
            if (retailValue > 0) {
                margin = NumberUtils.round(productCOGS / retailValue, 4);
            }

            //Row Totals
            row.put("Total Prod. Quantity", productQuantity);
            row.put("Total Prod. COGS", new DollarAmount(productCOGS)); //add product totals to row
            row.put("Margin %", new Percentage(margin));
            report.add(row);
        }

        private void addTotalRow() {
            HashMap<String, Object> totalRow = new HashMap<>();
            totalRow.put("Product Name", "Totals");
            totalRow.put("Total Prod. Quantity", "");
            totalRow.put("Total Prod. COGS", new DollarAmount(totalCOGS));
            for (String id : inventoryMap.keySet()) {
                totalRow.put(inventoryMap.get(id) + " Quantity", "");
                totalRow.put(inventoryMap.get(id) + " COGS", new DollarAmount(cogsByInventoryId.get(id)));
            }
            report.add(totalRow);
        }

        private GathererReport getReport() {
            return this.report;
        }

    }
}
