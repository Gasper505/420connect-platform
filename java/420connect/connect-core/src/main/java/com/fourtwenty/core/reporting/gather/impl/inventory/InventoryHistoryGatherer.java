package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductChangeLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.InventoryHistory;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stephen on 11/7/16.
 */
public class InventoryHistoryGatherer implements Gatherer {
    private static final String AVAILABLE_AFTER_DATE = "11/08/2016";

    private final InventoryRepository inventoryRepository;
    private final ProductRepository productRepository;
    private final ProductChangeLogRepository productChangeLogRepository;
    private final ProductCategoryRepository productCategoryRepository;
    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryHistoryGatherer(InventoryRepository inventoryRepository,
                                    ProductRepository productRepository,
                                    ProductChangeLogRepository productChangeLogRepository,
                                    ProductCategoryRepository productCategoryRepository) {
        this.inventoryRepository = inventoryRepository;
        this.productRepository = productRepository;
        this.productChangeLogRepository = productChangeLogRepository;
        this.productCategoryRepository = productCategoryRepository;

        reportHeaders.add("Product Name");
        fieldTypes.put("Product Name", GathererReport.FieldType.STRING);
        reportHeaders.add("Category");
        fieldTypes.put("Category", GathererReport.FieldType.STRING);
        reportHeaders.add("Status");
        fieldTypes.put("Status", GathererReport.FieldType.STRING);
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        DateTime jodaAvailableDate = formatter.parseDateTime(AVAILABLE_AFTER_DATE).withTimeAtStartOfDay().minusSeconds(1);
        long availableDateTime = jodaAvailableDate.getMillis();

        Iterable<Inventory> inventoryIterable = inventoryRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());

        List<Inventory> inventories = Lists.newArrayList(inventoryIterable);
        for (Inventory inventory : inventories) { //Loop to set up report headers and fieldtypes
            if (!inventory.isActive()) {
                continue;
            }
            String name = inventory.getName();
            reportHeaders.add(name + ": Today");
            fieldTypes.put(name + ": Today", GathererReport.FieldType.STRING);
            reportHeaders.add(name + ": Previous Day");
            fieldTypes.put(name + ": Previous Day", GathererReport.FieldType.STRING);
            reportHeaders.add(name + ": Difference");
            fieldTypes.put(name + ": Difference", GathererReport.FieldType.NUMBER);
        }

        GathererReport report = new GathererReport(filter, "Inventory History", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);

        // Get current list of products
        //HashMap<String,Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(),filter.getShopId());
        Iterable<Product> products = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());

        //Target Dates
        long targetStartDate = filter.getTimeZoneStartDateMillis();
        long targetEndDate = filter.getTimeZoneEndDateMillis();

        // Previous Day
        long prevStartDate = targetStartDate - ReportFilter.DAY_IN_MILLIS;
        long prevEndDate = targetEndDate - ReportFilter.DAY_IN_MILLIS;


        long lastMonthEndDate = new DateTime(prevEndDate).minusMonths(1).getMillis();

        Iterable<InventoryHistory> targetHistory = productChangeLogRepository.getTopGroupProductChangeLog(filter.getCompanyId(),
                filter.getShopId(),
                lastMonthEndDate,
                targetEndDate);

        HashMap<String, InventoryHistory> targetHistoryMap = new HashMap<>();
        for (InventoryHistory history : targetHistory) {
            targetHistoryMap.put(history.getProductId(), history);
        }

        Iterable<InventoryHistory> prevHistory = productChangeLogRepository.getTopGroupProductChangeLog(filter.getCompanyId(),
                filter.getShopId(),
                lastMonthEndDate,
                prevEndDate);
        HashMap<String, InventoryHistory> prevHistoryMap = new HashMap<>();
        for (InventoryHistory history : prevHistory) {
            prevHistoryMap.put(history.getProductId(), history);
        }

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        // Sort Products
        List<Product> productList = Lists.newArrayList(products);
        productList.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (Product product : productList) {

            HashMap<String, Object> data = new HashMap<>();

            data.put("Product Name", product.getName());
            InventoryHistory dayHistory = targetHistoryMap.get(product.getId());

            // Category
            ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
            if (category != null) {
                data.put("Category", category.getName());
            } else {
                data.put("Category", "");
            }
            data.put("Status", (product.isActive() ? "Active" : "InActive"));

            // Inventory
            for (Inventory inventory : inventories) {
                if (!inventory.isActive()) {
                    continue;
                }
                String todayHeader = String.format("%s: Today", inventory.getName());
                String yesterHeader = String.format("%s: Previous Day", inventory.getName());
                String diffHeader = String.format("%s: Difference", inventory.getName());

                double dayQuantity = -1;
                double diffQuantity = 0;

                InventoryHistory yesterHistory = null;
                if (dayHistory != null) {
                    for (ProductQuantity productQuantity : dayHistory.getProductQuantities()) {
                        if (productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                            dayQuantity = productQuantity.getQuantity().doubleValue();
                            break;
                        }
                    }
                    yesterHistory = prevHistoryMap.get(product.getId());
                }
                // If yesterday doesn't have a value, then it has the same quantity
                double yesterQuantity = -1;
                if (yesterHistory != null) {
                    for (ProductQuantity productQuantity : yesterHistory.getProductQuantities()) {
                        if (productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                            yesterQuantity = productQuantity.getQuantity().doubleValue();
                            break;
                        }
                    }
                }
                if (yesterQuantity > 0) {
                    diffQuantity = dayQuantity - yesterQuantity;
                }

                Object defaultValue = "N/A";
                if (targetEndDate >= availableDateTime) {
                    defaultValue = 0;
                }

                data.put(todayHeader, dayQuantity > -1 ? NumberUtils.round(dayQuantity, 2) : defaultValue);
                data.put(yesterHeader, yesterQuantity > -1 ? NumberUtils.round(yesterQuantity, 2) : defaultValue);
                data.put(diffHeader, NumberUtils.round(diffQuantity, 2));

            }

            // Add Row
            report.add(data);
        }

        report.setReportFieldTypes(fieldTypes);
        return report;
    }
}
