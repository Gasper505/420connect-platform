package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ExciseTaxInfoRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.model.reportmodels.RateAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InventoryLogValuationGatherer implements Gatherer {
    private static final String[] REPORT_ATTRS = new String[]{
            "Product Name",
            "Category",
            "Cannabis",
            "Cannabis Type",
            "Flower Type",
            "Status",
            "Retail Price",
            "Weight",
            "Avg Unit Cost",
            "Avg. Excise Cost",
            "Unit+Excise Cost",
            "Avg Profit",
            "Avg Margin %",
            "Avg Markup %",
            "Available Quantity",
            "Total COGS",
            "Total Excise",
            "COGS + Excise",

    };
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();


    public InventoryLogValuationGatherer() {
        Collections.addAll(reportHeaders, REPORT_ATTRS);

        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, //0 product name
                GathererReport.FieldType.STRING, //1 category
                GathererReport.FieldType.STRING, //2 cannabis
                GathererReport.FieldType.STRING, // 3 cannabis type
                GathererReport.FieldType.STRING, // 4 flower type
                GathererReport.FieldType.STRING, // 5 status
                GathererReport.FieldType.CURRENCY, //6 Retail Price
                GathererReport.FieldType.STRING, //7 weight
                GathererReport.FieldType.CURRENCY, //8 Avg Unit Cost
                GathererReport.FieldType.CURRENCY, //9 Excise Tax
                GathererReport.FieldType.CURRENCY, // 10 Unit + Excise Tax
                GathererReport.FieldType.CURRENCY, // 11 profit
                GathererReport.FieldType.PERCENTAGE, // 12 margin
                GathererReport.FieldType.PERCENTAGE, // 13 markup
                GathererReport.FieldType.NUMBER, // 14 total qty
                GathererReport.FieldType.CURRENCY, //15 cogs
                GathererReport.FieldType.CURRENCY, //16 excise cost
                GathererReport.FieldType.CURRENCY, //17 excise+cogs

        };
        for (int i = 0; i < fields.length; i++) {
            fieldTypes.put(REPORT_ATTRS[i], fields[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {

        final Iterable<InventoryOperation> inventoryOperations = inventoryActionRepository.getAllLogActions(filter.getCompanyId(), filter.getShopId(), filter.getStartDateMillis(), filter.getTimeZoneEndDateMillis());

        //Build the headers and stuff for this report
        GathererReport report = new GathererReport(filter, "Inventory Log Valuation", reportHeaders);
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
        report.setReportFieldTypes(fieldTypes);


        HashMap<String, List<InventoryOperation>> holdInventoryLogsMap = new HashMap<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> batchIds = new HashSet<>();
        Set<ObjectId> prepackagesItemIds = new HashSet<>();
        Set<ObjectId> vendorIds = new HashSet<>();
        Set<ObjectId> categoryIds = new HashSet<>();
        Set<ObjectId> inventoryIds = new HashSet<>();
        // Get Products, Batch and prepackages fro inventory logs
        for (InventoryOperation inventoryOperation : inventoryOperations) {
            if (StringUtils.isNotBlank(inventoryOperation.getProductId()) && ObjectId.isValid(inventoryOperation.getProductId())) {
                productIds.add(new ObjectId(inventoryOperation.getProductId()));
            }
            if (StringUtils.isNotBlank(inventoryOperation.getBatchId()) && ObjectId.isValid(inventoryOperation.getBatchId())) {
                batchIds.add(new ObjectId(inventoryOperation.getBatchId()));
            }

            if (StringUtils.isNotBlank(inventoryOperation.getPrepackageItemId()) && ObjectId.isValid(inventoryOperation.getPrepackageItemId())) {
                prepackagesItemIds.add(new ObjectId(inventoryOperation.getPrepackageItemId()));
            }

            if (StringUtils.isNotBlank(inventoryOperation.getInventoryId()) && ObjectId.isValid(inventoryOperation.getInventoryId())) {
                inventoryIds.add(new ObjectId(inventoryOperation.getInventoryId()));
            }
        }


        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductBatch> batchMap = batchRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(batchIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(prepackagesItemIds));


        // Get all vendors and categories form products
        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getVendorId()) && ObjectId.isValid(product.getVendorId())) {
                vendorIds.add(new ObjectId(product.getVendorId()));
            }

            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }
        for (ProductBatch batch : batchMap.values()) {
            if (StringUtils.isNotBlank(batch.getVendorId()) && ObjectId.isValid(batch.getVendorId())) {
                vendorIds.add(new ObjectId(batch.getVendorId()));
            }
        }


        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds));

        Iterable<Prepackage> prepackages = prepackageRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.listAllByShopActive(filter.getCompanyId(), filter.getShopId());
        ProductWeightTolerance gramWeightTolerance = weightToleranceRepository.getToleranceForWeight(filter.getCompanyId(), ProductWeightTolerance.WeightKey.GRAM);
        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.listByShop(filter.getCompanyId(), filter.getShopId());


        // Get all shop inventories

        HashMap<String, String> inventoryMap = new HashMap<>();
        HashSet<String> activeInventoryIds = new HashSet<>();
        HashMap<String, Inventory> inventories = inventoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(inventoryIds));
        List<Inventory> inventoryList = Lists.newArrayList(inventories.values());
        //Build the headers and stuff for this report

        if (filter.isToday()) {
            reportHeaders.add("All Hold COGS"); //add a column for this inventory's COGS
            reportHeaders.add("All Holds"); //add a column for this inventory's COGS

            fieldTypes.put("All Hold COGS", GathererReport.FieldType.CURRENCY);
            fieldTypes.put("All Holds", GathererReport.FieldType.CURRENCY);


        }

        for (Inventory i : inventoryList) {
            if (i.isActive()) {
                inventoryMap.put(i.getId(), i.getName()); //build a map where key=inventoryID  & value=inventory name
                activeInventoryIds.add(i.getId());

                reportHeaders.add(i.getName() + " Quantity"); //add a column for this inventory's quantity
                reportHeaders.add(i.getName() + " COGS"); //add a column for this inventory's COGS

                fieldTypes.put(i.getName() + " Quantity", GathererReport.FieldType.NUMBER);
                fieldTypes.put(i.getName() + " COGS", GathererReport.FieldType.CURRENCY);

                if (filter.isToday()) {
                    reportHeaders.add(i.getName() + " Holds"); //add a column for this inventory's COGS
                    reportHeaders.add(i.getName() + " Hold COGS"); //add a column for this inventory's COGS

                    fieldTypes.put(i.getName() + " Holds", GathererReport.FieldType.NUMBER);
                    fieldTypes.put(i.getName() + " Hold COGS", GathererReport.FieldType.CURRENCY);
                }

            }
        }
        // Shop Excise Tax
        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }
        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);
        ExciseTaxInfo nonCannabisTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country, ExciseTaxInfo.CannabisTaxType.NonCannabis);


        // get all batch quantities from product and inventory
        HashMap<String, List<BatchQuantity>> productInventoryToQty = new HashMap<>();
        for (BatchQuantity batchQuantity : batchQuantities) {
            String mapkey = String.format("%s_%s", batchQuantity.getProductId(), batchQuantity.getInventoryId());
            List<BatchQuantity> batchquantities = productInventoryToQty.getOrDefault(mapkey, new ArrayList<>());

            batchquantities.add(batchQuantity);
            productInventoryToQty.put(mapkey, batchquantities);
        }


        // product -> list of prepackages
        HashMap<String, List<Prepackage>> productToPrepackages = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            String mapkey = prepackage.getProductId();
            List<Prepackage> prepackageList = productToPrepackages.getOrDefault(mapkey, new ArrayList<>());

            prepackageList.add(prepackage);
            productToPrepackages.put(mapkey, prepackageList);
        }


        // prepackage -> quantities
        HashMap<String, List<ProductPrepackageQuantity>> prepackageToQuantities = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
            String key = String.format("%s_%s", prepackageQuantity.getPrepackageId(), prepackageQuantity.getInventoryId());
            List<ProductPrepackageQuantity> prepackagesQuantities = prepackageToQuantities.getOrDefault(key, new ArrayList<>());

            prepackagesQuantities.add(prepackageQuantity);
            prepackageToQuantities.put(key, prepackagesQuantities);
        }


        //Map all quantities  according to inventory action log
        for (InventoryOperation inventoryOperation : inventoryOperations) {
            String key = String.format("%s_%s", inventoryOperation.getProductId(), inventoryOperation.getInventoryId());
            if (StringUtils.isNotBlank(inventoryOperation.getPrepackageItemId())) {
                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(inventoryOperation.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    key = String.format("%s_%s", prepackageProductItem.getPrepackageId(), inventoryOperation.getInventoryId());
                }
            }

            List<InventoryOperation> logs = holdInventoryLogsMap.getOrDefault(key, new ArrayList<>());
            logs.add(inventoryOperation);
            holdInventoryLogsMap.put(key, logs);
        }


        // prepare report data

        for (Product product : productHashMap.values()) {
            String categoryName = "";
            ProductCategory category = productCategoryMap.get(product.getCategoryId());
            String cannabis = "No";
            boolean isCannabis = false;
            if (category != null) {

                if (category.isCannabis()) {
                    isCannabis = true;
                    cannabis = "Yes";
                }
                categoryName = category.getName();
            }

            // Calculate qty for raw unit value
            HashMap<String, Object> row = new HashMap<>();
            row.put("Product Name", product.getName());
            row.put("Category", categoryName);
            row.put("Cannabis", cannabis);
            row.put("Cannabis Type", (product.getCannabisType() != null && StringUtils.isNotBlank(product.getCannabisType().getDisplayName())) ? product.getCannabisType().getDisplayName() : Product.CannabisType.DEFAULT.getDisplayName());
            row.put("Flower Type", TextUtil.textOrEmpty(product.getFlowerType()));
            row.put("Status", (product.isActive()) ? "Active" : "Inactive");
            if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                row.put("Weight", String.format("%.1f %s", product.getCustomWeight().doubleValue(), (product.getCustomGramType() == Product.CustomGramType.GRAM ? "g" : "mg")));
            } else {
                row.put("Weight", product.getWeightPerUnit().name());
            }

            BigDecimal retailPrice = new BigDecimal(0);
            if (category == null || category.getUnitType() == ProductCategory.UnitType.units) {
                retailPrice = product.getUnitPrice();
            } else {
                for (ProductPriceRange pr : product.getPriceRanges()) {
                    if (pr.getWeightToleranceId().equalsIgnoreCase(gramWeightTolerance.getId())) {
                        retailPrice = pr.getPrice();
                        break;
                    }
                }
            }
            if (retailPrice == null) {
                retailPrice = new BigDecimal(0);
            }

            row.put("Retail Price", new DollarAmount(retailPrice));

            BigDecimal pTotalQty = new BigDecimal(0);
            BigDecimal pTotalCost = new BigDecimal(0);

            BigDecimal pTotalExcise = new BigDecimal(0);

            BigDecimal pHoldQty = new BigDecimal(0);
            BigDecimal pHoldCost = new BigDecimal(0);
            BigDecimal pHoldExcise = new BigDecimal(0);

            BigDecimal totalBatchCogs = new BigDecimal(0);
            for (Inventory inventory : inventoryList) {
                if (inventory.isActive()) {
                    String key = String.format("%s_%s", product.getId(), inventory.getId());
                    List<BatchQuantity> quantities = productInventoryToQty.get(key);

                    BigDecimal totalQty = new BigDecimal(0);
                    BigDecimal totalCost = new BigDecimal(0);
                    BigDecimal totalExcise = new BigDecimal(0);

                    if (quantities != null) {

                        for (BatchQuantity batchQuantity : quantities) {

                            String vendorId = product.getVendorId();
                            BigDecimal batchCogs = new BigDecimal(0);

                            ProductBatch batch = batchMap.get(batchQuantity.getBatchId());

                            if (batch != null) {
                                batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                if (StringUtils.isNotBlank(batch.getVendorId())) {
                                    vendorId = batch.getVendorId();
                                }
                                totalBatchCogs = totalBatchCogs.add(batchCogs);
                            }

                            totalQty = totalQty.add(batchQuantity.getQuantity());
                            BigDecimal value = batchQuantity.getQuantity().multiply(batchCogs);
                            totalCost = totalCost.add(value);

                            // calculate excise tax
                            if (isCannabis) {
                                BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);
                                totalExcise = totalExcise.add(batchQuantity.getQuantity().multiply(unitExcise));
                            } else {
                                BigDecimal unitExcise = getUnitExcise(nonCannabisTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);
                                totalExcise = totalExcise.add(batchQuantity.getQuantity().multiply(unitExcise));
                            }
                        }
                    }

                    totalQty.setScale(2, RoundingMode.HALF_EVEN);
                    totalCost.setScale(2, RoundingMode.HALF_EVEN);
                    totalExcise.setScale(2, RoundingMode.HALF_EVEN);

                    row.put(inventory.getName() + " Quantity", totalQty);
                    row.put(inventory.getName() + " COGS", new DollarAmount(totalCost));

                    // sum
                    pTotalQty = pTotalQty.add(totalQty);
                    pTotalCost = pTotalCost.add(totalCost);
                    pTotalExcise = pTotalExcise.add(totalExcise);


                    // Holds
                    BigDecimal holdQty = new BigDecimal(0);
                    BigDecimal holdCost = new BigDecimal(0);
                    BigDecimal holdExcise = new BigDecimal(0);
                    List<InventoryOperation> holds = holdInventoryLogsMap.get(key);
                    if (holds != null) {
                        for (InventoryOperation log : holds) {
                            BigDecimal batchCogs = new BigDecimal(0);
                            String vendorId = product.getVendorId();

                            holdQty = holdQty.add(log.getQuantity());
                            ProductBatch batch = batchMap.get(log.getBatchId());

                            if (batch != null) {
                                if (StringUtils.isNotBlank(batch.getVendorId())) {
                                    vendorId = batch.getVendorId();
                                }
                                batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                            }

                            BigDecimal value = log.getQuantity().multiply(batchCogs);
                            holdCost = holdCost.add(value);

                            // calculate excise tax
                            if (isCannabis) {
                                BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);
                                holdExcise = holdExcise.add(log.getQuantity().multiply(unitExcise));
                            } else {
                                BigDecimal unitExcise = getUnitExcise(nonCannabisTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);
                                holdExcise = holdExcise.add(log.getQuantity().multiply(unitExcise));
                            }

                        }
                    }

                    holdQty.setScale(2, RoundingMode.HALF_EVEN);
                    holdCost.setScale(2, RoundingMode.HALF_EVEN);
                    holdExcise.setScale(2, RoundingMode.HALF_EVEN);

                    row.put(inventory.getName() + " Holds", holdQty);
                    row.put(inventory.getName() + " Hold COGS", new DollarAmount(holdCost));

                    // sum
                    pHoldQty = pHoldQty.add(holdQty);
                    pHoldCost = pHoldCost.add(holdCost);
                    pHoldExcise = pHoldExcise.add(holdExcise);

                }
            }


            pTotalQty.setScale(2, RoundingMode.HALF_EVEN);
            pTotalCost.setScale(2, RoundingMode.HALF_EVEN);
            pTotalExcise.setScale(2, RoundingMode.HALF_EVEN);

            BigDecimal avgUnitCost = pTotalQty.doubleValue() > 0 ? pTotalCost.divide(pTotalQty, 6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal avgExciseCost = pTotalQty.doubleValue() > 0 ? pTotalExcise.divide(pTotalQty, 6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal totalUnitCost = avgUnitCost.add(avgExciseCost);

            row.put("Avg. Unit Cost", new RateAmount(avgUnitCost.setScale(6, RoundingMode.HALF_EVEN)));
            row.put("Avg. Excise Cost", new RateAmount(avgExciseCost.setScale(6, RoundingMode.HALF_EVEN)));
            row.put("Unit+Excise Cost", new RateAmount(totalUnitCost.setScale(6, RoundingMode.HALF_EVEN)));

            row.put("Total Available", pTotalQty);
            row.put("Total Available COGS", new DollarAmount(pTotalCost));
            row.put("Total Available Excise", new DollarAmount(pTotalExcise));
            row.put("Total Available COGS+Excise", new DollarAmount(pTotalCost.add(pTotalExcise)));


            row.put("All Holds", pHoldQty);
            row.put("All Hold COGS", new DollarAmount(pHoldCost));


            BigDecimal grossProfit = retailPrice.subtract(totalUnitCost);
            BigDecimal margin = (retailPrice.doubleValue() > 0) ? grossProfit.divide(retailPrice, 2, RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal markup = (totalUnitCost.doubleValue() > 0) ? grossProfit.divide(totalUnitCost, 2, RoundingMode.HALF_EVEN) : new BigDecimal(0);

            row.put("Profit", new DollarAmount(grossProfit));
            row.put("Margin %", new Percentage(margin));
            row.put("Markup %", new Percentage(markup));

            report.add(row);


            // calculate quantity for prepackages
            List<Prepackage> prepackageList = productToPrepackages.get(product.getId());
            if (prepackageList != null) {
                for (Prepackage prepackage : prepackageList) {
                    for (Inventory inventory : inventoryList) {
                        if (inventory.isActive()) {
                            String key = String.format("%s_%s", prepackage.getId(), inventory.getId());
                            List<ProductPrepackageQuantity> quantities = prepackageToQuantities.get(key);
                            List<InventoryOperation> holds = holdInventoryLogsMap.get(key);

                            BigDecimal totalQty = new BigDecimal(0);
                            BigDecimal totalCost = new BigDecimal(0);
                            BigDecimal totalExcise = new BigDecimal(0);
                            // Holds
                            BigDecimal holdQty = new BigDecimal(0);
                            BigDecimal holdCost = new BigDecimal(0);
                            BigDecimal holdExcise = new BigDecimal(0);
                            if (holds != null) {
                                for (InventoryOperation log : holds) {

                                    // Calculate qty for raw unit value
                                    row = new HashMap<>();
                                    row.put("Product Name", String.format("%s (%s)", product.getName(), prepackage.getName()));
                                    row.put("Category", categoryName);
                                    row.put("Cannabis", cannabis);

                                    row.put("Cannabis Type", (product.getCannabisType() != null && StringUtils.isNotBlank(product.getCannabisType().getDisplayName())) ? product.getCannabisType().getDisplayName() : Product.CannabisType.DEFAULT.getDisplayName());
                                    row.put("Flower Type", TextUtil.textOrEmpty(product.getFlowerType()));
                                    row.put("Status", (product.isActive()) ? "Active" : "Inactive");

                                    BigDecimal unitValue = prepackage.getUnitValue().setScale(2, RoundingMode.HALF_EVEN);

                                    row.put("Weight", unitValue + "g");
                                    row.put("Retail Price", new DollarAmount(prepackage.getPrice()));


                                    pTotalQty = new BigDecimal(0);
                                    pTotalCost = new BigDecimal(0);
                                    pTotalExcise = new BigDecimal(0);
                                    pHoldQty = new BigDecimal(0);
                                    pHoldCost = new BigDecimal(0);
                                    pHoldExcise = new BigDecimal(0);

                                    totalBatchCogs = new BigDecimal(0);


                                    if (quantities != null) {
                                        for (ProductPrepackageQuantity prepackQuantity : quantities) {
                                            BigDecimal batchCogs = new BigDecimal(0);
                                            BigDecimal qty = new BigDecimal(prepackQuantity.getQuantity());
                                            totalQty = totalQty.add(qty);

                                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(prepackQuantity.getPrepackageItemId());

                                            if (prepackageProductItem != null) {
                                                ProductBatch batch = batchMap.get(prepackageProductItem.getBatchId());
                                                if (isCannabis) {
                                                    String vendorId = product.getVendorId();

                                                    if (StringUtils.isNotBlank(batch.getVendorId())) {
                                                        vendorId = batch.getVendorId();
                                                    }

                                                    BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);
                                                    totalExcise = totalExcise.add(qty.multiply(unitExcise));
                                                }

                                                if (batch != null) {
                                                    batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                                    totalBatchCogs = totalBatchCogs.add(batchCogs);
                                                }

                                            }

                                            BigDecimal value = qty.multiply(batchCogs);
                                            totalCost = totalCost.add(value);
                                        }
                                    }

                                    totalQty.setScale(0, RoundingMode.HALF_EVEN);
                                    totalCost.setScale(2, RoundingMode.HALF_EVEN);
                                    totalExcise.setScale(2, RoundingMode.HALF_EVEN);

                                    row.put(inventory.getName() + " Quantity", totalQty);
                                    row.put(inventory.getName() + " COGS", new DollarAmount(totalCost));

                                    // sum
                                    pTotalQty = pTotalQty.add(totalQty);
                                    pTotalCost = pTotalCost.add(totalCost);
                                    pTotalExcise = pTotalExcise.add(totalExcise);

                                    holdQty = holdQty.add(log.getQuantity());

                                    BigDecimal batchCogs = new BigDecimal(0);

                                    ProductBatch batch = batchMap.get(log.getBatchId());

                                    if (batch != null) {
                                        batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                    }

                                    BigDecimal value = log.getQuantity().multiply(batchCogs);
                                    holdCost = holdCost.add(value);

                                    if (isCannabis) {
                                        String vendorId = product.getVendorId();
                                        if (StringUtils.isNotBlank(batch.getVendorId())) {
                                            vendorId = batch.getVendorId();
                                        }
                                        BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);
                                        holdExcise = holdExcise.add(log.getQuantity().multiply(unitExcise));
                                    }


                                    holdQty.setScale(2, RoundingMode.HALF_EVEN);
                                    holdCost.setScale(2, RoundingMode.HALF_EVEN);
                                    holdExcise.setScale(2, RoundingMode.HALF_EVEN);

                                    row.put(inventory.getName() + " Holds", holdQty);
                                    row.put(inventory.getName() + " Hold COGS", new DollarAmount(holdCost));

                                    // sum
                                    pHoldQty = pHoldQty.add(holdQty);
                                    pHoldCost = pHoldCost.add(holdCost);
                                    pHoldExcise = pHoldExcise.add(holdExcise);


                                    pTotalQty.setScale(2, RoundingMode.HALF_EVEN);
                                    pTotalCost.setScale(2, RoundingMode.HALF_EVEN);

                                    avgUnitCost = pTotalQty.doubleValue() > 0 ? pTotalCost.divide(pTotalQty, 6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
                                    avgExciseCost = pTotalQty.doubleValue() > 0 ? pTotalExcise.divide(pTotalQty, 6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
                                    totalUnitCost = avgUnitCost.add(avgExciseCost);

                                    row.put("Avg. Unit Cost", new RateAmount(avgUnitCost.setScale(6, RoundingMode.HALF_EVEN)));
                                    row.put("Avg. Excise Cost", new RateAmount(avgExciseCost.setScale(6, RoundingMode.HALF_EVEN)));
                                    row.put("Unit+Excise Cost", new RateAmount(totalUnitCost.setScale(6, RoundingMode.HALF_EVEN)));

                                    row.put("Total Available", pTotalQty);
                                    row.put("Total Available COGS", new DollarAmount(pTotalCost));
                                    row.put("Total Available Excise", new DollarAmount(pTotalExcise));
                                    row.put("Total Available COGS+Excise", new DollarAmount(pTotalCost.add(pTotalExcise)));


                                    row.put("All Holds", pHoldQty);
                                    row.put("All Hold COGS", new DollarAmount(pHoldCost));


                                    grossProfit = prepackage.getPrice().subtract(totalUnitCost);
                                    margin = (prepackage.getPrice().doubleValue() > 0) ? grossProfit.divide(prepackage.getPrice(), 2, RoundingMode.HALF_EVEN) : new BigDecimal(0);
                                    markup = (totalUnitCost.doubleValue() > 0) ? grossProfit.divide(totalUnitCost, 2, RoundingMode.HALF_EVEN) : new BigDecimal(0);

                                    row.put("Profit", new DollarAmount(grossProfit));
                                    row.put("Margin %", new Percentage(margin));
                                    row.put("Markup %", new Percentage(markup));
                                    report.add(row);
                                }
                            }
                        }
                    }
                }
            }
        }
        return report;
    }

    private BigDecimal getUnitExcise(ExciseTaxInfo exciseTaxInfo,
                                     ProductBatch batch,
                                     BigDecimal batchCogs,
                                     String vendorId, HashMap<String, Vendor> vendorHashMap,
                                     BigDecimal retailPrice) {
        // Calculate Excise
        if (batch != null && batch.getPerUnitExciseTax() != null && batch.getPerUnitExciseTax().doubleValue() > 0) {

            return batch.getPerUnitExciseTax();
        } else {
            Vendor.ArmsLengthType armsLengthType = batch != null ? batch.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;
            if (armsLengthType == null && StringUtils.isNotBlank(vendorId)) {
                Vendor vendor = vendorHashMap.get(vendorId);
                if (vendor != null && vendor.getArmsLengthType() != null) {
                    armsLengthType = vendor.getArmsLengthType();
                }
            }

            BigDecimal exciseTax;
            if (armsLengthType == null || armsLengthType == Vendor.ArmsLengthType.ARMS_LENGTH) {
                // calculate based on cogs

                HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(exciseTaxInfo, new BigDecimal(1), batchCogs);
                exciseTax = taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO);
            } else {
                // calculate based on retail price per unit
                exciseTax = retailPrice.multiply(new BigDecimal(.15d));
            }

            return exciseTax;
        }
    }


}
