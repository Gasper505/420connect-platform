package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.InventoryHistory;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferRequest;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 12/29/16.
 */
public class InventoryMovementGatherer implements Gatherer {
    private final TransactionRepository transactionRepository;
    private final EmployeeRepository employeeRepository;
    private final TerminalRepository terminalRepository;
    private final MemberRepository memberRepository;
    private final ProductRepository productRepository;
    private final ProductChangeLogRepository productChangeLogRepository;
    private final InventoryRepository inventoryRepository;

    private static final String[] attrs = new String[]{"Date",
            "Action", "Employee"};

    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryMovementGatherer(TransactionRepository transactionRepository,
                                     EmployeeRepository employeeRepository,
                                     TerminalRepository terminalRepository,
                                     MemberRepository memberRepository,
                                     ProductRepository productRepository,
                                     ProductChangeLogRepository productChangeLogRepository,
                                     InventoryRepository inventoryRepository) {
        this.memberRepository = memberRepository;
        this.employeeRepository = employeeRepository;
        this.transactionRepository = transactionRepository;
        this.terminalRepository = terminalRepository;
        this.productRepository = productRepository;
        this.productChangeLogRepository = productChangeLogRepository;
        this.inventoryRepository = inventoryRepository;
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        //build past sales per product
        String inventoryId;
        if (filter.getMap().containsKey("inventoryId")) {
            inventoryId = filter.getMap().get("inventoryId");
        } else {
            return null;
        }
        Inventory inventory = inventoryRepository.get(filter.getCompanyId(), inventoryId);

        if (inventory == null) {
            return null;
        }


        final Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsDatesList(filter.getCompanyId(),
                filter.getShopId(),
                filter.getTimeZoneStartDateMillis(),
                filter.getTimeZoneEndDateMillis());


        List<ObjectId> memberIds = new ArrayList<>();
        HashMap<String, ObjectId> productIds = new HashMap<>();


        List<Transaction> validTransaction = new ArrayList<>();
        for (Transaction ts : transactions) {
            boolean hasInventory = false;
            if (ts.getStatus() == Transaction.TransactionStatus.RefundWithoutInventory) {
                // Ignore refund without inventory
                continue;
            }
            // check transfer
            if (ts.getTransferRequest() != null) {
                for (InventoryTransferRequest tr : ts.getTransferRequest().getTransfers()) {
                    if (tr.getFromInventoryId().equalsIgnoreCase(inventoryId) ||
                            tr.getToInventoryId().equalsIgnoreCase(inventoryId)) {
                        hasInventory = true;
                        break;
                    }
                }
            }

            for (OrderItem orderItem : ts.getCart().getItems()) {
                // Only check if quantity logs exist
                for (QuantityLog log : orderItem.getQuantityLogs()) {
                    if (log.getInventoryId().equalsIgnoreCase(inventoryId)) {
                        // exist
                        if (log.getQuantity().doubleValue() > 0) {
                            hasInventory = true;
                        }
                        break;
                    }
                }

                if (hasInventory) {
                    productIds.put(orderItem.getProductId(), new ObjectId(orderItem.getProductId()));
                }
            }

            if (hasInventory) {
                validTransaction.add(ts);
            }


            // add memberIds
            if (ts.getMemberId() != null && ObjectId.isValid(ts.getMemberId())) {
                memberIds.add(new ObjectId(ts.getMemberId()));
            }
        }


        final HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        // All associated products in these transactions
        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(filter.getCompanyId(), filter.getShopId(),
                Lists.newArrayList(productIds.values()));


        // Create product columns
        List<Product> products = Lists.newArrayList(productHashMap.values());
        // sort name
        products.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        // Create columns and field types
        List<String> headers = Lists.newArrayList("Date", "Action", "Employee");
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < fields.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
        for (Product product : products) {
            headers.add(product.getName());
            fieldTypes.put(product.getName(), GathererReport.FieldType.NUMBER);
        }

        // Create Reports
        GathererReport report = new GathererReport(filter, "Inventory Movement", headers);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        // Let's add the data
        // Add first row
        long targetStartDate = filter.getTimeZoneStartDateMillis();
        long lastMonthEndDate = new DateTime(targetStartDate).minusMonths(1).getMillis();
        if (validTransaction.size() > 0) {
            validTransaction.sort(new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getProcessedTime().compareTo(o2.getProcessedTime());
                }
            });
            targetStartDate = validTransaction.get(0).getProcessedTime() - 1000;
        }

        Iterable<InventoryHistory> targetHistory = productChangeLogRepository.getTopGroupProductChangeLog(filter.getCompanyId(),
                filter.getShopId(),
                lastMonthEndDate,
                targetStartDate);
        HashMap<String, InventoryHistory> targetHistoryMap = new HashMap<>();
        for (InventoryHistory history : targetHistory) {
            targetHistoryMap.put(history.getProductId(), history);
        }

        HashMap<String, Object> firstRow = new HashMap<>();
        firstRow.put("Date", ProcessorUtil.timeStampWithOffsetLong(targetStartDate, filter.getTimezoneOffset()));
        firstRow.put("Action", "Start Inventory");
        firstRow.put("Employee", "System");
        for (Product p : products) {
            InventoryHistory inventoryHistory = targetHistoryMap.get(p.getId());
            BigDecimal amount = new BigDecimal(0);
            if (inventoryHistory != null) {
                for (ProductQuantity productQuantity : inventoryHistory.getProductQuantities()) {
                    if (productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                        amount = productQuantity.getQuantity();
                        break;
                    }
                }
            }
            firstRow.put(p.getName(), amount);
        }
        report.add(firstRow);


        // Add the detail transaction info / movement
        for (Transaction transaction : validTransaction) {
            // Add meta data
            HashMap<String, Object> row = new HashMap<>();
            row.put("Date", ProcessorUtil.timeStampWithOffsetLong(transaction.getProcessedTime(), filter.getTimezoneOffset()));
            row.put("Action", String.format("Trans #%s - %s", transaction.getTransNo(), transaction.getTransType()));

            Employee employee = employeeMap.get(transaction.getSellerId());
            if (employee != null) {
                row.put("Employee", employee.getFirstName() + " " + employee.getLastName());
            } else {
                row.put("Employee", "");
            }

            // Now add default data for products
            for (Product p : products) {
                row.put(p.getName(), 0d);
            }

            // If transfer
            if (transaction.getTransType() == Transaction.TransactionType.Transfer) {

                // Look at transfer request
                if (transaction.getTransferRequest() != null) {
                    for (InventoryTransferRequest tr : transaction.getTransferRequest().getTransfers()) {
                        if (tr.getFromInventoryId().equalsIgnoreCase(inventoryId) ||
                                tr.getToInventoryId().equalsIgnoreCase(inventoryId)) {
                            Product product = productHashMap.get(tr.getProductId());
                            if (product != null) {
                                Double currentValue = (Double) row.get(product.getName());
                                if (currentValue == null) {
                                    currentValue = 0d;
                                }

                                if (tr.getFromInventoryId().equalsIgnoreCase(inventoryId)) {
                                    currentValue -= tr.getTransferAmount().doubleValue();
                                } else {
                                    currentValue += tr.getTransferAmount().doubleValue();
                                }

                                row.put(product.getName(), NumberUtils.round(currentValue, 2));
                            }

                        }
                    }
                }

            } else {
                // else look at the cart
                for (OrderItem orderItem : transaction.getCart().getItems()) {

                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product != null) {
                        for (QuantityLog log : orderItem.getQuantityLogs()) {
                            Double currentValue = (Double) row.get(product.getName());
                            if (currentValue == null) {
                                currentValue = 0d;
                            }
                            if (log.getInventoryId().equalsIgnoreCase(inventoryId)) {
                                // If refund then add
                                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                                    if (transaction.getStatus() == Transaction.TransactionStatus.RefundWithInventory) {
                                        currentValue += log.getQuantity().doubleValue();
                                    }
                                } else {
                                    // else then go ahead and minus
                                    currentValue -= log.getQuantity().doubleValue();
                                }
                            }

                            row.put(product.getName(), NumberUtils.round(currentValue, 2));
                        }
                    }
                }
            }
            // add row to report
            report.add(row);
        }


        return report;
    }
}
