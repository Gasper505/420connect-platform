package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 12/29/16.
 */
public class InventoryOnHoldGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private InventoryRepository inventoryRepository;

    private static final String[] attrs = new String[] {
            "Date",
            "Product",
            "Category",
            "Employee",
            "Transaction No.",
            "Member",
            "Terminal",
            "Inventory",
            "On Hold Quantity"
    };

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryOnHoldGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER};
        for (int i = 0; i < fields.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory On Hold", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);


        final HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        final HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        final HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        final HashMap<String, Product> productHashMap = productRepository.listAllAsMap(filter.getCompanyId());
        final HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(filter.getCompanyId());
        final HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(filter.getCompanyId());

        final Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactions(filter.getCompanyId(),
                filter.getShopId());

        List<ObjectId> memberIds = new ArrayList<>();
        List<ObjectId> prepackageItemIds = new ArrayList<>();
        for (Transaction ts : transactions) {
            if (ts.getMemberId() != null && ObjectId.isValid(ts.getMemberId())) {
                memberIds.add(new ObjectId(ts.getMemberId()));
            }
            for (OrderItem orderItem : ts.getCart().getItems()) {
                if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                    prepackageItemIds.add(new ObjectId(orderItem.getPrepackageItemId()));
                }
            }

        }

        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(filter.getCompanyId(), memberIds);
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap =
                prepackageProductItemRepository.findItemsInAsMap(filter.getCompanyId(), filter.getShopId(), prepackageItemIds);

        for (Transaction ts : transactions) {
            if (ts.isPreparingFulfillment()) {
                // skip any transactions that's currently preparing
                continue;
            }
            String employeeName = "";
            String terminalName = "";
            Terminal terminal = terminalHashMap.get(ts.getSellerTerminalId());
            if (terminal != null) {
                terminalName = terminal.getName();
            }

            Employee employee = employeeMap.get(ts.getSellerId());
            employeeName = employee == null ? "" : employee.getFirstName() + " " + employee.getLastName();


            Member member = memberHashMap.get(ts.getMemberId());
            String memberName = member == null ? "" : member.getFirstName() + " " + member.getLastName();


            for (OrderItem item : ts.getCart().getItems()) {
                if (ts.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment) {
                    if (!item.isFulfilled()) {
                        // has not been fulfilled so continue
                        continue;
                    }
                }
                if (item.getQuantityLogs() == null) {
                    continue;
                }
                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                    HashMap<String, Object> data = new HashMap<>();
                    String productName = "";
                    String categoryName = "";
                    Product product = productHashMap.get(item.getProductId());
                    if (product != null) {
                        productName = product.getName();
                        ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
                        categoryName = category.getName();

                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(quantityLog.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null) {
                                productName = String.format("%s (%s)", productName, prepackage.getName());
                            }
                        }
                    }

                    Inventory inventory = inventoryHashMap.get(quantityLog.getInventoryId());
                    String inventoryName = "";
                    if (inventory != null) {
                        inventoryName = inventory.getName();
                    }

                    data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(ts.getProcessedTime(), filter.getTimezoneOffset()));
                    data.put(attrs[1], productName);
                    data.put(attrs[2], categoryName);
                    data.put(attrs[3], employeeName);
                    data.put(attrs[4], ts.getTransNo());

                    data.put(attrs[5], memberName);
                    data.put(attrs[6], terminalName);
                    data.put(attrs[7], inventoryName);
                    data.put(attrs[8], NumberUtils.round(quantityLog.getQuantity().doubleValue(), 2));
                    report.add(data);
                }
            }

        }
        return report;

    }
}
