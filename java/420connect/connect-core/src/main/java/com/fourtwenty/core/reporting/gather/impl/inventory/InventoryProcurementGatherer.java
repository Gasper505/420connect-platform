package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephen on 9/14/16.
 */
public class InventoryProcurementGatherer implements Gatherer {
    private ProductBatchRepository batchRepository;
    private VendorRepository vendorRepository;
    private ProductRepository productRepository;
    private ProductCategoryRepository categoryRepository;
    private PurchaseOrderRepository purchaseOrderRepository;
    private String[] attrs = new String[]{"Purchase Time", "Vendor Name", "Product Name", "Product Sku", "Product Category",
            "Batch Sku", "Quantity Purchased", "Unit Type", "Purchase Amount", "Cost Per Unit", "Excise Tax", "Discount", "Fees", "Final Cost", "PO Number", "Reference Number"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryProcurementGatherer(ProductBatchRepository batchRepository, VendorRepository vendorRepository, ProductRepository productRepository,
                                        ProductCategoryRepository categoryRepository, PurchaseOrderRepository purchaseOrderRepository) {
        this.batchRepository = batchRepository;
        this.vendorRepository = vendorRepository;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < fields.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory Procurement", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        Map<String, Vendor> vendorMap = vendorRepository.listAllAsMap(filter.getCompanyId());
        Map<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Map<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductBatch> batches = batchRepository.getBatchesForDateBracket(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Map<String, PurchaseOrder> purchaseOrderMap = purchaseOrderRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        for (ProductBatch b : batches) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.timeStampWithOffset(b.getPurchasedDate(), filter.getTimezoneOffset()));

            Vendor v = vendorMap.get(b.getVendorId());
            if (v != null) {
                data.put(attrs[1], v.getName());
            } else {
                data.put(attrs[1], "");
            }

            Product p = productMap.get(b.getProductId());
            if (p != null) {
                data.put(attrs[2], p.getName());
                data.put(attrs[3], p.getSku());
                ProductCategory cat = categoryMap.get(p.getCategoryId());
                if (cat != null) {
                    data.put(attrs[4], cat.getName());
                    data.put(attrs[7], cat.getUnitType());
                } else {
                    data.put(attrs[4], "");
                    data.put(attrs[7], "");
                }
            } else {
                data.put(attrs[2], "");
                data.put(attrs[3], "");
                data.put(attrs[4], "");
            }

            data.put(attrs[5], StringUtils.isNotBlank(b.getSku()) ? b.getSku() : "");
            data.put(attrs[6], b.getQuantity());

            PurchaseOrder purchaseOrder = purchaseOrderMap.get(b.getPurchaseOrderId());
            BigDecimal cost = new BigDecimal(0);
            BigDecimal discount = new BigDecimal(0);
            BigDecimal fees = new BigDecimal(0);

            if (b.getCost() != null) {
                cost = b.getCost();
            }
            if (purchaseOrder != null && CollectionUtils.isNotEmpty(purchaseOrder.getPoProductRequestList())) {
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    if (p != null && !p.getId().equals(poProductRequest.getProductId())) {
                        continue;
                    }
                    BigDecimal quantity = poProductRequest.getRequestQuantity();
                    if (poProductRequest.getBatchQuantityMap() != null && poProductRequest.getBatchQuantityMap().containsKey(b.getId())) {
                        quantity = poProductRequest.getBatchQuantityMap().get(b.getId());
                    }

                    cost = quantity.multiply(poProductRequest.getUnitPrice());

                    BigDecimal productCost = poProductRequest.getRequestQuantity().multiply(poProductRequest.getUnitPrice());

                    BigDecimal propLineItemDiscount = BigDecimal.ZERO;
                    if (productCost.doubleValue() != 0) {
                        propLineItemDiscount = BigDecimal.valueOf((poProductRequest.getDiscount().doubleValue() * cost.doubleValue()) / productCost.doubleValue());
                    }

                    BigDecimal propPoDiscount = BigDecimal.ZERO;

                    if (purchaseOrder.getDiscount().doubleValue() != 0 && purchaseOrder.getTotalCost().doubleValue() != 0) {
                        propPoDiscount = BigDecimal.valueOf((purchaseOrder.getDiscount().doubleValue() * cost.doubleValue()) / purchaseOrder.getTotalCost().doubleValue());
                    }

                    discount = propLineItemDiscount.add(propPoDiscount);

                    if (purchaseOrder.getFees().doubleValue() != 0 && purchaseOrder.getTotalCost().doubleValue() != 0) {
                        fees = BigDecimal.valueOf((purchaseOrder.getFees().doubleValue() * cost.doubleValue()) / purchaseOrder.getTotalCost().doubleValue());
                    }
                }
            }

            data.put(attrs[8], new DollarAmount(cost));
            data.put(attrs[9], new DollarAmount(b.getFinalUnitCost()));
            data.put(attrs[10], new DollarAmount(b.getTotalExciseTax() != null ? b.getTotalExciseTax() : new BigDecimal(0)));
            data.put(attrs[11], new DollarAmount(discount));
            data.put(attrs[12], new DollarAmount(fees));
            data.put(attrs[13], new DollarAmount(b.getCost() != null ? b.getCost() : BigDecimal.ZERO));

            if (purchaseOrder != null) {
                data.put(attrs[14], purchaseOrder.getPoNumber());
            } else {
                data.put(attrs[14], "");
            }

            data.put(attrs[15], (purchaseOrder == null || StringUtils.isBlank(purchaseOrder.getReference())) ? "" : purchaseOrder.getReference());

            report.add(data);
        }

        return report;
    }
}
