package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 4/11/17.
 */
public class InventorySnapshotGatherer implements Gatherer {

    private final ProductChangeLogRepository productChangeLogRepository;
    private final InventoryRepository inventoryRepository;
    private final ProductRepository productRepository;
    private final EmployeeRepository employeeRepository;
    private final PrepackageRepository prepackageRepository;
    private final ProductCategoryRepository categoryRepository;
    private final ProductBatchRepository productBatchRepository;

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();


    public InventorySnapshotGatherer(final EmployeeRepository employeeRepository,
                                     final ProductChangeLogRepository productChangeLogRepository,
                                     final InventoryRepository inventoryRepository,
                                     final ProductRepository productRepository,
                                     final ProductCategoryRepository categoryRepository,
                                     final PrepackageRepository prepackageRepository,
                                     final ProductBatchRepository productBatchRepository) {
        this.employeeRepository = employeeRepository;
        this.productChangeLogRepository = productChangeLogRepository;
        this.inventoryRepository = inventoryRepository;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.prepackageRepository = prepackageRepository;
        this.productBatchRepository = productBatchRepository;
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        reportHeaders.add("Timestamp");
        fieldTypes.put("Timestamp", GathererReport.FieldType.STRING);
        reportHeaders.add("Initiated By");
        fieldTypes.put("Initiated By", GathererReport.FieldType.STRING);
        reportHeaders.add("Reference");
        fieldTypes.put("Reference", GathererReport.FieldType.STRING);
        reportHeaders.add("Product");
        fieldTypes.put("Product", GathererReport.FieldType.STRING);
        reportHeaders.add("Category");
        fieldTypes.put("Category", GathererReport.FieldType.STRING);
        reportHeaders.add("Total");
        fieldTypes.put("Total", GathererReport.FieldType.STRING);
        reportHeaders.add("Total Prepackages");
        fieldTypes.put("Total Prepackages", GathererReport.FieldType.STRING);
        reportHeaders.add("Cogs");
        fieldTypes.put("Cogs", GathererReport.FieldType.CURRENCY);


        Iterable<Inventory> inventoryIterable = inventoryRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        List<Inventory> inventories = Lists.newArrayList(inventoryIterable);
        inventories.sort(new Comparator<Inventory>() {
            @Override
            public int compare(Inventory o1, Inventory o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (Inventory inventory : inventories) {
            if (inventory.isActive()) {
                reportHeaders.add(inventory.getName());
                fieldTypes.put(inventory.getName(), GathererReport.FieldType.NUMBER);

                reportHeaders.add(inventory.getName() + " (Prepackages)");
                fieldTypes.put(inventory.getName(), GathererReport.FieldType.STRING);
            }
        }


        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Product> productHashMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        HashMap<String, ProductBatch> batchByProductIdMap = getProductBatchByProductId(filter.getCompanyId(), filter.getShopId());

        GathererReport report = new GathererReport(filter, "Inventory Snapshot History", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.DATE_BRACKET);

        Iterable<ProductChangeLog> changeLogs = productChangeLogRepository.getChangeLogsWithReferenceBegins(filter.getCompanyId(),
                filter.getShopId(),
                "Inventory Snapshot",
                filter.getTimeZoneStartDateMillis(),
                filter.getTimeZoneEndDateMillis());

        // Sort by date and productName
        for (ProductChangeLog changeLog : changeLogs) {

            HashMap<String, Object> data = new HashMap<>();
            String timestamp = ProcessorUtil.timeStampWithOffset(changeLog.getCreated(), filter.getTimezoneOffset());

            data.put("Timestamp", timestamp);


            Employee employee = employeeHashMap.get(changeLog.getEmployeeId());
            if (employee != null) {
                data.put("Initiated By", employee.getFirstName() + " " + employee.getLastName());
            } else {
                data.put("Initiated By", "System");
            }

            data.put("Reference", changeLog.getReference());

            // Get the product
            String productName = "";
            String categoryName = "";
            Product product = productHashMap.get(changeLog.getProductId());
            if (product != null) {
                productName = product.getName().replaceAll(",", " ");
                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                if (category != null) {
                    categoryName = category.getName();
                }
            }
            data.put("Product", productName);
            data.put("Category", categoryName);

            double total = 0.0;
            HashMap<String, Integer> totalQuantityMap = new HashMap<>(); // prepackages
            for (Inventory inventory : inventories) {
                if (inventory.isActive()) {
                    double quantity = 0;
                    for (ProductQuantity productQuantity : changeLog.getProductQuantities()) {
                        if (productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                            quantity = productQuantity.getQuantity().doubleValue();
                            break;
                        }
                    }
                    total += quantity;
                    data.put(inventory.getName(), NumberUtils.round(quantity, 2));


                    HashMap<String, Integer> localQuantityMap = new HashMap<>(); // prepackages
                    for (ProductPrepackageQuantity prepackageQuantity : changeLog.getPrepackageQuantities()) {
                        if (prepackageQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                            Integer totalQty = totalQuantityMap.get(prepackageQuantity.getPrepackageId());
                            Integer localQty = localQuantityMap.get(prepackageQuantity.getPrepackageId());
                            if (totalQty == null || totalQty == 0) {
                                totalQty = 0;
                            }
                            if (localQty == null) {
                                localQty = 0;
                            }
                            localQty += prepackageQuantity.getQuantity();
                            totalQty += prepackageQuantity.getQuantity();

                            totalQuantityMap.put(prepackageQuantity.getPrepackageId(), totalQty);
                            localQuantityMap.put(prepackageQuantity.getPrepackageId(), localQty);

                        }
                    }

                    StringBuilder sb = new StringBuilder();
                    for (String prepackageId : localQuantityMap.keySet()) {
                        Integer qty = localQuantityMap.get(prepackageId);
                        Prepackage prepackage = prepackageHashMap.get(prepackageId);
                        if (prepackage != null) {
                            sb.append(String.format("%s: %d<br/>", prepackage.getName(), qty));
                        }
                    }
                    data.put(inventory.getName() + " (Prepackages)", sb.toString());
                }
            }
            data.put("Total", NumberUtils.round(total, 2));

            StringBuilder sb = new StringBuilder();
            for (String prepackageId : totalQuantityMap.keySet()) {
                Integer qty = totalQuantityMap.get(prepackageId);
                Prepackage prepackage = prepackageHashMap.get(prepackageId);
                if (prepackage != null) {
                    sb.append(String.format("%s: %d<br/>", prepackage.getName(), qty));
                }
            }

            data.put("Total Prepackages", sb.toString());

            ProductBatch productBatch = batchByProductIdMap.get(changeLog.getProductId());

            double cogs = 0;
            if (productBatch != null && productBatch.getFinalUnitCost() != null) {
                cogs = productBatch.getFinalUnitCost().doubleValue() * total;
            }

            data.put("Cogs", NumberUtils.round(cogs, 2));

            report.add(data);
        }


        return report;
    }

    /**
     * This is private method to get latest product batch by product id
     *
     * @return
     */
    private HashMap<String, ProductBatch> getProductBatchByProductId(String companyId, String shopId) {
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(companyId, shopId);

        HashMap<String, ProductBatch> returnMap = new HashMap<>();

        for (String key : productBatchHashMap.keySet()) {
            ProductBatch batch = productBatchHashMap.get(key);
            if (returnMap.containsKey(batch.getProductId())) {
                ProductBatch mapBatch = returnMap.get(batch.getProductId());
                DateTime batchDateTime = new DateTime(batch.getCreated());
                if (batchDateTime.isBefore(mapBatch.getCreated())) {
                    batch = mapBatch;
                }
            }
            returnMap.put(batch.getProductId(), batch);
        }

        return returnMap;
    }
}
