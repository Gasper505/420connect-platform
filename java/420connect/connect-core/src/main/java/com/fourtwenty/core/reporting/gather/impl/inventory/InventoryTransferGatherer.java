package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkInventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferRequest;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephen on 7/31/16.
 */
public class InventoryTransferGatherer implements Gatherer {
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private PrepackageRepository prepackageRepository;

    private String[] attrs = new String[]{"Date", "Employee", "Product", "Origin Shop", "Origin Inventory", "Destination Shop", "Destination", "Amount"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryTransferGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Inventory Transfer Log", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        Iterable<Transaction> transfers = transactionRepository.getTransfers(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Map<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        Map<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId());
        Map<String, Inventory> inventoryMap = inventoryRepository.listAsMap(filter.getCompanyId());
        Map<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId());

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId());

        for (Transaction t : transfers) {
            BulkInventoryTransferRequest transferRequest = t.getTransferRequest();
            if (transferRequest != null) {
                for (InventoryTransferRequest tr : transferRequest.getTransfers()) {
                    HashMap<String, Object> data = new HashMap<>();
                    data.put(attrs[0], DateUtil.toDateTimeFormatted(t.getProcessedTime(), filter.getTimezone(), "MMM dd yyyy hh:mm a"));
                    Employee seller = employeeMap.get(t.getSellerId());
                    data.put(attrs[1], seller.getFirstName() + " " + seller.getLastName());
                    Product product = productMap.get(tr.getProductId());
                    Inventory origin = inventoryMap.get(tr.getFromInventoryId());
                    Inventory destination = inventoryMap.get(tr.getToInventoryId());

                    String productName = product != null ? product.getName() : "";

                    if (StringUtils.isNotBlank(tr.getPrepackageItemId())) {
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(tr.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null) {
                                productName = String.format("%s (%s)", productName, prepackage.getName());
                            }
                        }
                    }

                    data.put(attrs[2], productName);


                    String origName = "";
                    String origShop = "";
                    if (origin != null) {
                        origName = origin.getName();
                        Shop shop = shopMap.get(origin.getShopId());
                        if (shop != null) {
                            origShop = shop.getName();
                        }
                    }

                    String dsName = "";
                    String dsShop = "";
                    if (destination != null) {
                        dsName = destination.getName();
                        Shop shop = shopMap.get(destination.getShopId());
                        if (shop != null) {
                            dsShop = shop.getName();
                        }
                    }


                    data.put(attrs[3], origShop);
                    data.put(attrs[4], origName);
                    data.put(attrs[5], dsShop);
                    data.put(attrs[6], dsName);
                    data.put(attrs[7], tr.getTransferAmount());
                    report.add(data);
                }
            }
        }

        return report;
    }
}
