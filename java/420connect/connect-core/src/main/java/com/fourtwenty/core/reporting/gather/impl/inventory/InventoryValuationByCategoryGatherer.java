package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.model.reportmodels.RateAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class InventoryValuationByCategoryGatherer implements Gatherer {
    private static final String INVENTORY_VALUATION_BY_CATEGORY_REPORT = "Inventory Valuation By Category Report";
    private static final String[] REPORT_ATTRS = new String[]{
            "Product Category",
            "Avg Unit Cost",
            "Avg. Excise Cost",
            "Unit+Excise Cost",
            "Avg Retail Price",
            "Avg Profit",
            "Avg Margin %",
            "Avg Markup %",
            "Total Available Quantity",
            "Total Available COGS",
            "Total Available Excise",
            "Total Available COGS+Excise",
            "Total Avail Prepackages",
            "Total Avail Prepackages COGS",
    };
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;


    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public InventoryValuationByCategoryGatherer() {
        Collections.addAll(reportHeaders, REPORT_ATTRS);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, //name
                GathererReport.FieldType.CURRENCY, // unit cost
                GathererReport.FieldType.CURRENCY, // excise cost
                GathererReport.FieldType.CURRENCY, // excise+unit cost
                GathererReport.FieldType.CURRENCY, // retail price
                GathererReport.FieldType.CURRENCY, // profit
                GathererReport.FieldType.PERCENTAGE, // margin
                GathererReport.FieldType.PERCENTAGE, // markup
                GathererReport.FieldType.NUMBER, // qty
                GathererReport.FieldType.CURRENCY, // cogs
                GathererReport.FieldType.CURRENCY, // excise cost
                GathererReport.FieldType.CURRENCY, // excise+unit cost
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
        };
        for (int i = 0; i < REPORT_ATTRS.length; i++) {
            fieldTypes.put(REPORT_ATTRS[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {


        if (filter.isToday()) {
            reportHeaders.add("All Holds"); //add a column for this inventory's COGS
            fieldTypes.put("All Holds", GathererReport.FieldType.NUMBER);


            reportHeaders.add("All Hold COGS"); //add a column for this inventory's COGS
            fieldTypes.put("All Hold COGS", GathererReport.FieldType.CURRENCY);


            reportHeaders.add("All Prepack Holds"); //add a column for this inventory's COGS
            fieldTypes.put("All Prepack Holds", GathererReport.FieldType.STRING);


            reportHeaders.add("All Prepack Hold COGS"); //add a column for this inventory's COGS
            fieldTypes.put("All Prepack Hold COGS", GathererReport.FieldType.STRING);
        }
        GathererReport report = new GathererReport(filter, INVENTORY_VALUATION_BY_CATEGORY_REPORT, reportHeaders);
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
        report.setReportFieldTypes(fieldTypes);


        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> batchMap = batchRepository.listAsMap(filter.getCompanyId());
        Iterable<Prepackage> prepackages = prepackageRepository.listAllByShopActive(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        ProductWeightTolerance gramWeightTolerance = weightToleranceRepository.getToleranceForWeight(filter.getCompanyId(), ProductWeightTolerance.WeightKey.GRAM);
        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.listByShop(filter.getCompanyId(),filter.getShopId());
        Iterable<Product> productList = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        HashMap<String,PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(filter.getCompanyId(),filter.getShopId());
        HashMap<String,Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());

        Shop shop = shopRepository.get(filter.getCompanyId(),filter.getShopId());

        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }

        ExciseTaxInfo exciseTaxInfo =  exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);


        HashMap<String, List<BatchQuantity>> productInventoryToQty = new HashMap<>();
        for (BatchQuantity batchQuantity : batchQuantities) {
            String key = String.format("%s",batchQuantity.getProductId());
            List<BatchQuantity> quantities = productInventoryToQty.getOrDefault(key,new ArrayList<>());

            quantities.add(batchQuantity);
            productInventoryToQty.put(key,quantities);
        }

        // product -> list of prepackages
        HashMap<String,List<Prepackage>> productToPrepackages = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            String key = prepackage.getProductId();
            List<Prepackage> prepackageList = productToPrepackages.getOrDefault(key,new ArrayList<>());
            prepackageList.add(prepackage);
            productToPrepackages.put(key,prepackageList);
        }

        // prepackage -> quantities
        HashMap<String,List<ProductPrepackageQuantity>> prepackageToQuantities = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
            String key = String.format("%s",prepackageQuantity.getPrepackageId());
            List<ProductPrepackageQuantity> prepackagesQuantities = prepackageToQuantities.getOrDefault(key,new ArrayList<>());
            prepackagesQuantities.add(prepackageQuantity);
            prepackageToQuantities.put(key,prepackagesQuantities);
        }


        // on hold
        HashMap<String,List<QuantityLog>> holdLogsMap = new HashMap<>();
        if (filter.isToday()) {
            final Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactions(filter.getCompanyId(),
                    filter.getShopId());

            // on hold
            for (Transaction ts : transactions) {
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if (orderItem.getQuantityLogs() != null) {
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            String key = orderItem.getProductId();
                            if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                                if (prepackageProductItem != null) {
                                    key = prepackageProductItem.getPrepackageId();
                                }
                            }
                            List<QuantityLog> logs = holdLogsMap.getOrDefault(key, new ArrayList<>());
                            logs.add(quantityLog);
                            holdLogsMap.put(key, logs);
                        }
                    }
                }
            }
        }


        HashMap<String,InventoryValuationCategory> categoryValulationMap = new HashMap<>();
        for (Product product : productList) {


            ProductCategory category = productCategoryMap.get(product.getCategoryId());
            if (category == null) {
                continue;
            }


            InventoryValuationCategory valuationCategory = categoryValulationMap.getOrDefault(category.getId(),new InventoryValuationCategory());
            categoryValulationMap.put(category.getId(),valuationCategory);


            // Calculate Retail Price
            BigDecimal retailPrice = new BigDecimal(0);
            if (category.getUnitType() == ProductCategory.UnitType.units) {
                retailPrice = product.getUnitPrice();
            } else {
                for (ProductPriceRange pr : product.getPriceRanges()) {
                    if (pr.getWeightToleranceId().equalsIgnoreCase(gramWeightTolerance.getId())) {
                        retailPrice = pr.getPrice();
                        break;
                    }
                }
            }


            String key = product.getId();
            List<BatchQuantity> quantities = productInventoryToQty.get(key);

            BigDecimal totalQty = new BigDecimal(0);
            BigDecimal totalCost = new BigDecimal(0);
            BigDecimal totalExcise = new BigDecimal(0);

            BigDecimal totalBatchCogs = new BigDecimal(0);

            if (quantities != null) {
                for (BatchQuantity batchQuantity : quantities) {
                    totalQty = totalQty.add(batchQuantity.getQuantity());

                    BigDecimal batchCogs = new BigDecimal(0);

                    ProductBatch batch = batchMap.get(batchQuantity.getBatchId());

                    String vendorId = product.getVendorId();

                    if (batch != null) {
                        batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                        totalBatchCogs = totalBatchCogs.add(batchCogs);

                        if (StringUtils.isNotBlank(batch.getVendorId())) {
                            vendorId = batch.getVendorId();
                        }
                    }

                    BigDecimal value = batchQuantity.getQuantity().multiply(batchCogs);
                    totalCost = totalCost.add(value);


                    if (category.isCannabis()) {
                        BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                        totalExcise = totalExcise.add(batchQuantity.getQuantity().multiply(unitExcise));
                    }
                }
            }

            totalQty.setScale(2, RoundingMode.HALF_EVEN);
            totalCost.setScale(2, RoundingMode.HALF_EVEN);
            totalExcise.setScale(2, RoundingMode.HALF_EVEN);


            BigDecimal avgUnitCost = totalQty.doubleValue() > 0 ? totalCost.divide(totalQty,2,RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal avgExciseCost = totalQty.doubleValue() > 0 ? totalExcise.divide(totalQty,6,RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal totalUnitCost = avgUnitCost.add(avgExciseCost);


            // Holds
            BigDecimal holdQty = new BigDecimal(0);
            BigDecimal holdCost = new BigDecimal(0);
            BigDecimal holdExcise = new BigDecimal(0);
            List<QuantityLog> holds = holdLogsMap.get(key);
            if (holds != null) {
                for (QuantityLog log : holds) {
                    holdQty = holdQty.add(log.getQuantity());

                    BigDecimal batchCogs = new BigDecimal(0);

                    ProductBatch batch = batchMap.get(log.getBatchId());
                    if (batch != null) {
                        batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                    }

                    BigDecimal value = log.getQuantity().multiply(batchCogs);

                    holdCost = holdCost.add(value);


                    String vendorId = product.getVendorId();
                    if (batch != null && StringUtils.isNotBlank(batch.getVendorId())) {
                        vendorId = batch.getVendorId();
                    }
                    if (category.isCannabis()) {
                        BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                        holdExcise = holdExcise.add(log.getQuantity().multiply(unitExcise));
                    }
                }
            }

            holdQty.setScale(2, RoundingMode.HALF_EVEN);
            holdCost.setScale(2, RoundingMode.HALF_EVEN);
            holdExcise.setScale(2, RoundingMode.HALF_EVEN);






            valuationCategory.sumRetailPrice = valuationCategory.sumRetailPrice.add(retailPrice);
            valuationCategory.sumCOGs = valuationCategory.sumCOGs.add(avgUnitCost);
            valuationCategory.totalCOGs = valuationCategory.totalCOGs.add(totalCost);
            valuationCategory.totalExcise = valuationCategory.totalExcise.add(totalExcise);



            valuationCategory.totalQuantity = valuationCategory.totalQuantity.add(totalQty);
            valuationCategory.productPrepackCount++;
            valuationCategory.categoryName = category.getName();

            valuationCategory.totalHolds = valuationCategory.totalHolds.add(holdQty);
            valuationCategory.totalHoldCOGs = valuationCategory.totalHoldCOGs.add(holdCost);





            // calculate quantity for prepackages
            List<Prepackage> prepackageList = productToPrepackages.get(product.getId());
            if (prepackageList != null) {
                for (Prepackage prepackage : prepackageList) {

                    totalBatchCogs = new BigDecimal(0);

                    key = String.format("%s", prepackage.getId());
                    List<ProductPrepackageQuantity> prepackageQuantities = prepackageToQuantities.get(key);

                    totalQty = new BigDecimal(0);
                    totalCost = new BigDecimal(0);
                    totalExcise = new BigDecimal(0);
                    if (prepackageQuantities != null) {
                        for (ProductPrepackageQuantity prepackQuantity : prepackageQuantities) {
                            BigDecimal qty = new BigDecimal(prepackQuantity.getQuantity());
                            totalQty = totalQty.add(qty);

                            BigDecimal batchCogs = new BigDecimal(0);

                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(prepackQuantity.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                ProductBatch batch = batchMap.get(prepackageProductItem.getBatchId());
                                if (batch != null) {
                                    batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                    totalBatchCogs = totalBatchCogs.add(batchCogs);


                                    if (category.isCannabis()) {
                                        String vendorId = product.getVendorId();
                                        if (StringUtils.isNotBlank(batch.getVendorId())) {
                                            vendorId = batch.getVendorId();
                                        }
                                        BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                                        totalExcise = totalExcise.add(qty.multiply(unitExcise));
                                    }
                                }
                            }

                            BigDecimal value = qty.multiply(batchCogs);

                            totalCost = totalCost.add(value);
                        }
                    }

                    totalQty.setScale(0, RoundingMode.HALF_EVEN);
                    totalCost.setScale(2, RoundingMode.HALF_EVEN);
                    totalExcise.setScale(2, RoundingMode.HALF_EVEN);


                    avgUnitCost = totalQty.doubleValue() > 0 ? totalCost.divide(totalQty,2,RoundingMode.HALF_EVEN) : new BigDecimal(0);
                    avgExciseCost = totalQty.doubleValue() > 0 ? totalExcise.divide(totalQty,6,RoundingMode.HALF_EVEN) : new BigDecimal(0);


                    // Holds
                    holdQty = new BigDecimal(0);
                    holdCost = new BigDecimal(0);
                    holds = holdLogsMap.get(key);
                    if (holds != null) {
                        for (QuantityLog log : holds) {
                            holdQty = holdQty.add(log.getQuantity());

                            BigDecimal batchCogs = new BigDecimal(0);

                            ProductBatch batch = batchMap.get(log.getBatchId());
                            if (batch != null) {
                                batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                            }

                            BigDecimal value = log.getQuantity().multiply(batchCogs);

                            holdCost = holdCost.add(value);
                        }
                    }

                    holdQty.setScale(2, RoundingMode.HALF_EVEN);
                    holdCost.setScale(2, RoundingMode.HALF_EVEN);





                    valuationCategory.sumRetailPrice = valuationCategory.sumRetailPrice.add(prepackage.getPrice());
                    valuationCategory.sumCOGs = valuationCategory.sumCOGs.add(avgUnitCost);
                    valuationCategory.totalCOGs = valuationCategory.totalCOGs.add(totalCost);
                    valuationCategory.totalExcise = valuationCategory.totalExcise.add(totalExcise);
                    valuationCategory.productPrepackCount++;


                    // add prepackages info
                    Integer prepackQty = valuationCategory.prepackageInfo.getOrDefault(prepackage.getName(),new Integer(0));
                    prepackQty += totalQty.intValue();
                    valuationCategory.prepackageInfo.put(prepackage.getName(),prepackQty);


                    BigDecimal prepackCOGs = valuationCategory.prepackageCogsInfo.getOrDefault(prepackage.getName(),new BigDecimal(0));
                    prepackCOGs = prepackCOGs.add(totalCost);
                    valuationCategory.prepackageCogsInfo.put(prepackage.getName(),prepackCOGs);


                    // add hold info
                    // add prepackages info
                    Integer holdPrepackQty = valuationCategory.holdPepackageInfo.getOrDefault(prepackage.getName(),new Integer(0));
                    holdPrepackQty += holdQty.intValue();
                    valuationCategory.holdPepackageInfo.put(prepackage.getName(),holdPrepackQty);


                    BigDecimal holdPrepackCOGs = valuationCategory.holdPrepackageCogsInfo.getOrDefault(prepackage.getName(),new BigDecimal(0));
                    holdPrepackCOGs = holdPrepackCOGs.add(holdCost);
                    valuationCategory.holdPrepackageCogsInfo.put(prepackage.getName(),holdPrepackCOGs);


                }
            }
        }

        List<InventoryValuationCategory> sortedCategories = Lists.newArrayList(categoryValulationMap.values());

        Collections.sort(sortedCategories, Comparator.comparing(o -> o.categoryName));

        for (InventoryValuationCategory valuationCategory : sortedCategories) {

            StringBuilder stringPrepackage = new StringBuilder();
            StringBuilder stringProductionCOGS = new StringBuilder();

            valuationCategory.prepackageInfo.forEach((key, value) -> stringPrepackage.append(String.format("%s: %d ,", key, value)));

            //Extract COGS value
            valuationCategory.prepackageCogsInfo.forEach((key, value) -> stringProductionCOGS.append(String.format("%s: %.02f ,", key, value)));


            StringBuilder holdStringPrepackage = new StringBuilder();
            StringBuilder holdStringProductionCOGS = new StringBuilder();

            valuationCategory.holdPepackageInfo.forEach((key, value) -> holdStringPrepackage.append(String.format("%s: %d ,", key, value)));

            //Extract COGS value
            valuationCategory.holdPrepackageCogsInfo.forEach((key, value) -> holdStringProductionCOGS.append(String.format("%s: %.02f ,", key, value)));


            HashMap<String,Object> data = new HashMap<>();
            data.put(REPORT_ATTRS[0], valuationCategory.categoryName);
            data.put(REPORT_ATTRS[1], new RateAmount(valuationCategory.getAvgCOGs()));
            data.put(REPORT_ATTRS[2], new RateAmount(valuationCategory.getAvgExcise()));
            data.put(REPORT_ATTRS[3], new RateAmount(valuationCategory.getAvgCOGsExcise()));
            data.put(REPORT_ATTRS[4], new DollarAmount(valuationCategory.getAvgRetailPrice()));
            data.put(REPORT_ATTRS[5], new DollarAmount(valuationCategory.getAvgProfit()));
            data.put(REPORT_ATTRS[6], new Percentage(valuationCategory.getMarginPercentage()));
            data.put(REPORT_ATTRS[7], new Percentage(valuationCategory.getMarkupPercentage()));
            data.put(REPORT_ATTRS[8], valuationCategory.totalQuantity);
            data.put(REPORT_ATTRS[9], new DollarAmount(valuationCategory.totalCOGs));
            data.put(REPORT_ATTRS[10], new DollarAmount(valuationCategory.totalExcise));
            data.put(REPORT_ATTRS[11], new DollarAmount(valuationCategory.getTotalExciseCOGs()));
            data.put(REPORT_ATTRS[12], stringPrepackage);
            data.put(REPORT_ATTRS[13], stringProductionCOGS);


            data.put("All Holds", valuationCategory.totalHolds);
            data.put("All Hold COGS", new DollarAmount(valuationCategory.totalHoldCOGs));
            data.put("All Prepack Holds", holdStringPrepackage);
            data.put("All Prepack Hold COGS", holdStringProductionCOGS);



            report.add(data);
        }

        return report;
    }

    public class InventoryValuationCategory {
        String categoryName;
        BigDecimal totalQuantity = new BigDecimal(0);
        BigDecimal totalCOGs = new BigDecimal(0);
        BigDecimal totalExcise = new BigDecimal(0);

        BigDecimal totalHolds = new BigDecimal(0);
        BigDecimal totalHoldCOGs = new BigDecimal(0);
        BigDecimal totalHoldExcise = new BigDecimal(0);
        BigDecimal totalHoldCOGsExcise = new BigDecimal(0);

        HashMap<String, Integer> prepackageInfo = new HashMap<>();
        HashMap<String, BigDecimal> prepackageCogsInfo = new HashMap<>();

        HashMap<String, Integer> holdPepackageInfo = new HashMap<>();
        HashMap<String, BigDecimal> holdPrepackageCogsInfo = new HashMap<>();

        BigDecimal sumRetailPrice = new BigDecimal(0);
        BigDecimal sumCOGs = new BigDecimal(0);
        int productPrepackCount = 0;

        public BigDecimal getAvgRetailPrice() {
            return productPrepackCount > 0 ? sumRetailPrice.divide(new BigDecimal(productPrepackCount),6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
        }


        public BigDecimal getAvgCOGs() {
            return totalQuantity.doubleValue() > 0 ? totalCOGs.divide(totalQuantity,6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
        }

        public BigDecimal getAvgExcise() {
            return totalQuantity.doubleValue() > 0 ? totalExcise.divide(totalQuantity,6, RoundingMode.HALF_EVEN) : new BigDecimal(0);
        }

        public BigDecimal getAvgCOGsExcise() {
            return getAvgCOGs().add(getAvgExcise());
        }

        public BigDecimal getAvgProfit() {
            return getAvgRetailPrice().subtract(getAvgCOGsExcise());
        }

        public BigDecimal getTotalExciseCOGs() {
            return totalExcise.add(totalCOGs);
        }

        public BigDecimal getMarginPercentage() {
            BigDecimal avgRetailPrice = getAvgRetailPrice();
            if (avgRetailPrice.doubleValue() <= 0) {
                return new BigDecimal(0);
            }
            BigDecimal avgProfit = getAvgProfit();
            return avgProfit.divide(avgRetailPrice,2,RoundingMode.HALF_EVEN);
        }

        public BigDecimal getMarkupPercentage() {
            BigDecimal avgCOGs = getAvgCOGsExcise();
            if (avgCOGs.doubleValue() <= 0) {
                return new BigDecimal(0);
            }
            BigDecimal avgProfit = getAvgProfit();
            return avgProfit.divide(avgCOGs,2,RoundingMode.HALF_EVEN);
        }
    }

    private BigDecimal getUnitExcise(ExciseTaxInfo exciseTaxInfo,
                                     ProductBatch batch,
                                     BigDecimal batchCogs,
                                     String vendorId,HashMap<String,Vendor> vendorHashMap,
                                     BigDecimal retailPrice) {
        // Calculate Excise
        if (batch != null && batch.getPerUnitExciseTax() != null && batch.getPerUnitExciseTax().doubleValue() > 0) {
            return batch.getPerUnitExciseTax();
        } else {
            Vendor.ArmsLengthType armsLengthType = batch != null ? batch.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;
            if (armsLengthType == null && StringUtils.isNotBlank(vendorId)) {
                Vendor vendor = vendorHashMap.get(vendorId);
                if (vendor != null && vendor.getArmsLengthType() != null) {
                    armsLengthType = vendor.getArmsLengthType();
                }
            }

            BigDecimal exciseTax = new BigDecimal(0);
            if (armsLengthType == null || armsLengthType == Vendor.ArmsLengthType.ARMS_LENGTH) {
                // calculate based on cogs

                HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(exciseTaxInfo, new BigDecimal(1), batchCogs);
                exciseTax = taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO);
            } else {
                // calculate based on retail price per unit
                exciseTax = retailPrice.multiply(new BigDecimal(.15d));
            }

            return exciseTax;
        }
    }
}
