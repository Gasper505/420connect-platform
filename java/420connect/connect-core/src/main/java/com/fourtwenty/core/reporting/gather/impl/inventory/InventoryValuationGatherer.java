package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.model.reportmodels.RateAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class InventoryValuationGatherer implements Gatherer {
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private HashSet<String> activeInventoryIds = new HashSet<>();


    private static final String[] attrs = new String[] {
            "Product Name",
            "Category",
            "Cannabis",
            "Cannabis Type",
            "Flower Type",
            "Status",
            "Weight",
            "Avg. Unit Cost",
            "Avg. Excise Cost",
            "Unit+Excise Cost",
            "Retail Price",
            "Profit",
            "Margin %",
            "Markup %",
            "Total Available",
            "Total Available COGS",
            "Total Available Excise",
            "Total Available COGS+Excise"
    };

    public InventoryValuationGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, //name
                GathererReport.FieldType.STRING, //category
                GathererReport.FieldType.STRING, //cannabis
                GathererReport.FieldType.STRING, // cannabis type
                GathererReport.FieldType.STRING, // flower type
                GathererReport.FieldType.STRING, // status
                GathererReport.FieldType.STRING, // weight
                GathererReport.FieldType.CURRENCY, // unit cost
                GathererReport.FieldType.CURRENCY, // Avg. Excise Cost
                GathererReport.FieldType.CURRENCY, // Total Unit Cost
                GathererReport.FieldType.CURRENCY, // retail value
                GathererReport.FieldType.CURRENCY, // profit
                GathererReport.FieldType.PERCENTAGE, // margin
                GathererReport.FieldType.PERCENTAGE, // margin
                GathererReport.FieldType.NUMBER, // total qty
                GathererReport.FieldType.CURRENCY,// total cogs
                GathererReport.FieldType.CURRENCY,// total excise
                GathererReport.FieldType.CURRENCY// total cogs+excise

        };
        for (int i = 0; i < fields.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        HashMap<String, String> inventoryMap = new HashMap<>();
        Iterable<Inventory> inventories = inventoryRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        List<Inventory> inventoryList = Lists.newArrayList(inventories);
        //Build the headers and stuff for this report

        if (filter.isToday()) {
            reportHeaders.add("All Holds"); //add a column for this inventory's COGS
            fieldTypes.put("All Holds", GathererReport.FieldType.CURRENCY);


            reportHeaders.add("All Hold COGS"); //add a column for this inventory's COGS
            fieldTypes.put("All Hold COGS", GathererReport.FieldType.CURRENCY);
        }

        for (Inventory i : inventoryList) {
            if (i.isActive()) {
                activeInventoryIds.add(i.getId());
                inventoryMap.put(i.getId(), i.getName()); //build a map where key=inventoryID  & value=inventory name
                reportHeaders.add(i.getName() + " Quantity"); //add a column for this inventory's quantity
                fieldTypes.put(i.getName() + " Quantity", GathererReport.FieldType.NUMBER);
                reportHeaders.add(i.getName() + " COGS"); //add a column for this inventory's COGS
                fieldTypes.put(i.getName() + " COGS", GathererReport.FieldType.CURRENCY);

                if (filter.isToday()) {
                    reportHeaders.add(i.getName() + " Holds"); //add a column for this inventory's COGS
                    fieldTypes.put(i.getName() + " Holds", GathererReport.FieldType.NUMBER);


                    reportHeaders.add(i.getName() + " Hold COGS"); //add a column for this inventory's COGS
                    fieldTypes.put(i.getName() + " Hold COGS", GathererReport.FieldType.CURRENCY);
                }
            }
        }



        GathererReport report = new GathererReport(filter, "Inventory Valuation Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
        report.setReportFieldTypes(fieldTypes);



        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> batchMap = batchRepository.listAsMap(filter.getCompanyId());
        Iterable<Prepackage> prepackages = prepackageRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.listAllByShopActive(filter.getCompanyId(), filter.getShopId());
        ProductWeightTolerance gramWeightTolerance = weightToleranceRepository.getToleranceForWeight(filter.getCompanyId(), ProductWeightTolerance.WeightKey.GRAM);
        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.listByShop(filter.getCompanyId(),filter.getShopId());
        Iterable<Product> productList = productRepository.listByShop(filter.getCompanyId(), filter.getShopId());
        HashMap<String,PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(filter.getCompanyId(),filter.getShopId());
        HashMap<String,Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId());

        Shop shop = shopRepository.get(filter.getCompanyId(),filter.getShopId());

        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }

        ExciseTaxInfo exciseTaxInfo =  exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);


        HashMap<String,List<BatchQuantity>> productInventoryToQty = new HashMap<>();
        for (BatchQuantity batchQuantity : batchQuantities) {
            String key = String.format("%s_%s",batchQuantity.getProductId(),batchQuantity.getInventoryId());
            List<BatchQuantity> quantities = productInventoryToQty.getOrDefault(key,new ArrayList<>());

            quantities.add(batchQuantity);
            productInventoryToQty.put(key,quantities);
        }

        // product -> list of prepackages
        HashMap<String,List<Prepackage>> productToPrepackages = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            String key = prepackage.getProductId();
            List<Prepackage> prepackageList = productToPrepackages.getOrDefault(key,new ArrayList<>());
            prepackageList.add(prepackage);
            productToPrepackages.put(key,prepackageList);
        }

        // prepackage -> quantities
        HashMap<String,List<ProductPrepackageQuantity>> prepackageToQuantities = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
            String key = String.format("%s_%s",prepackageQuantity.getPrepackageId(),prepackageQuantity.getInventoryId());
            List<ProductPrepackageQuantity> prepackagesQuantities = prepackageToQuantities.getOrDefault(key,new ArrayList<>());
            prepackagesQuantities.add(prepackageQuantity);
            prepackageToQuantities.put(key,prepackagesQuantities);
        }


        HashMap<String,List<QuantityLog>> holdLogsMap = new HashMap<>();
        if (filter.isToday()) {
            final Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactions(filter.getCompanyId(),
                    filter.getShopId());

            // on hold
            for (Transaction ts : transactions) {
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if (orderItem.getQuantityLogs() != null) {
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            String key = String.format("%s_%s", orderItem.getProductId(), quantityLog.getInventoryId());
                            if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                                if (prepackageProductItem != null) {
                                    key = String.format("%s_%s", prepackageProductItem.getPrepackageId(), quantityLog.getInventoryId());
                                }
                            }
                            List<QuantityLog> logs = holdLogsMap.getOrDefault(key, new ArrayList<>());
                            logs.add(quantityLog);
                            holdLogsMap.put(key, logs);
                        }
                    }
                }
            }
        }


        for (Product product : productList) {

            String categoryName = "";
            ProductCategory category = productCategoryMap.get(product.getCategoryId());
            String cannabis = "No";
            boolean isCannabis = false;
            if (category != null) {
                categoryName = category.getName();
                if (category.isCannabis()) {
                    cannabis = "Yes";
                    isCannabis = true;
                }
            }

            // Calculate qty for raw unit value
            HashMap<String, Object> row = new HashMap<>();
            row.put("Product Name",product.getName());
            row.put("Category",categoryName);
            row.put("Cannabis", cannabis);
            row.put("Cannabis Type", (product.getCannabisType() != null && StringUtils.isNotBlank(product.getCannabisType().getDisplayName())) ? product.getCannabisType().getDisplayName() : Product.CannabisType.DEFAULT.getDisplayName());
            row.put("Flower Type", TextUtil.textOrEmpty(product.getFlowerType()));
            row.put("Status", (product.isActive()) ? "Active" : "Inactive");
            if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                row.put("Weight", String.format("%.1f %s",product.getCustomWeight().doubleValue(), (product.getCustomGramType() == Product.CustomGramType.GRAM ? "g" : "mg")));
            } else {
                row.put("Weight", product.getWeightPerUnit().name());
            }

            BigDecimal retailPrice = new BigDecimal(0);
            if (category == null || category.getUnitType() == ProductCategory.UnitType.units) {
                retailPrice = product.getUnitPrice();
            } else {
                for (ProductPriceRange pr : product.getPriceRanges()) {
                    if (pr.getWeightToleranceId().equalsIgnoreCase(gramWeightTolerance.getId())) {
                        retailPrice = pr.getPrice();
                        break;
                    }
                }
            }
            if (retailPrice == null) {
                retailPrice = new BigDecimal(0);
            }

            row.put("Retail Price",new DollarAmount(retailPrice));

            BigDecimal pTotalQty = new BigDecimal(0);
            BigDecimal pTotalCost = new BigDecimal(0);

            BigDecimal pTotalExcise = new BigDecimal(0);

            BigDecimal pHoldQty = new BigDecimal(0);
            BigDecimal pHoldCost = new BigDecimal(0);
            BigDecimal pHoldExcise = new BigDecimal(0);

            BigDecimal totalBatchCogs = new BigDecimal(0);
            for (Inventory inventory : inventoryList) {
                if (inventory.isActive()) {
                    String key = String.format("%s_%s",product.getId(),inventory.getId());
                    List<BatchQuantity> quantities = productInventoryToQty.get(key);

                    BigDecimal totalQty = new BigDecimal(0);
                    BigDecimal totalCost = new BigDecimal(0);
                    BigDecimal totalExcise = new BigDecimal(0);
                    if (quantities != null) {
                        for (BatchQuantity batchQuantity : quantities) {
                            totalQty = totalQty.add(batchQuantity.getQuantity());
                            BigDecimal batchCogs = new BigDecimal(0);

                            ProductBatch batch = batchMap.get(batchQuantity.getBatchId());
                            String vendorId = product.getVendorId();
                            if (batch != null) {
                                batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                totalBatchCogs = totalBatchCogs.add(batchCogs);
                                if (StringUtils.isNotBlank(batch.getVendorId())) {
                                    vendorId = batch.getVendorId();
                                }
                            }

                            BigDecimal value = batchQuantity.getQuantity().multiply(batchCogs);

                            totalCost = totalCost.add(value);

                            // calculate excise tax
                            if (isCannabis) {
                                BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                                totalExcise = totalExcise.add(batchQuantity.getQuantity().multiply(unitExcise));
                            }
                        }
                    }

                    totalQty.setScale(2, RoundingMode.HALF_EVEN);
                    totalCost.setScale(2, RoundingMode.HALF_EVEN);
                    totalExcise.setScale(2, RoundingMode.HALF_EVEN);

                    row.put(inventory.getName() + " Quantity",totalQty);
                    row.put(inventory.getName() + " COGS",new DollarAmount(totalCost));

                    // sum
                    pTotalQty = pTotalQty.add(totalQty);
                    pTotalCost = pTotalCost.add(totalCost);
                    pTotalExcise = pTotalExcise.add(totalExcise);




                    // Holds
                    BigDecimal holdQty = new BigDecimal(0);
                    BigDecimal holdCost = new BigDecimal(0);
                    BigDecimal holdExcise = new BigDecimal(0);
                    List<QuantityLog> holds = holdLogsMap.get(key);
                    if (holds != null) {
                        for (QuantityLog log : holds) {
                            holdQty = holdQty.add(log.getQuantity());

                            BigDecimal batchCogs = new BigDecimal(0);

                            ProductBatch batch = batchMap.get(log.getBatchId());
                            String vendorId = product.getVendorId();
                            if (batch != null) {
                                batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                if (StringUtils.isNotBlank(batch.getVendorId())) {
                                    vendorId = batch.getVendorId();
                                }
                            }

                            BigDecimal value = log.getQuantity().multiply(batchCogs);

                            holdCost = holdCost.add(value);



                            if (isCannabis) {
                                BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                                holdExcise = holdExcise.add(log.getQuantity().multiply(unitExcise));
                            }

                        }
                    }

                    holdQty.setScale(2, RoundingMode.HALF_EVEN);
                    holdCost.setScale(2, RoundingMode.HALF_EVEN);
                    holdExcise.setScale(2, RoundingMode.HALF_EVEN);

                    row.put(inventory.getName() + " Holds",holdQty);
                    row.put(inventory.getName() + " Hold COGS",new DollarAmount(holdCost));

                    // sum
                    pHoldQty = pHoldQty.add(holdQty);
                    pHoldCost = pHoldCost.add(holdCost);
                    pHoldExcise = pHoldExcise.add(holdExcise);

                }
            }


            pTotalQty.setScale(2, RoundingMode.HALF_EVEN);
            pTotalCost.setScale(2, RoundingMode.HALF_EVEN);
            pTotalExcise.setScale(2, RoundingMode.HALF_EVEN);

            BigDecimal avgUnitCost = pTotalQty.doubleValue() > 0 ? pTotalCost.divide(pTotalQty,6,RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal avgExciseCost = pTotalQty.doubleValue() > 0 ? pTotalExcise.divide(pTotalQty,6,RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal totalUnitCost = avgUnitCost.add(avgExciseCost);

            row.put("Avg. Unit Cost", new RateAmount(avgUnitCost.setScale(6,RoundingMode.HALF_EVEN)));
            row.put("Avg. Excise Cost", new RateAmount(avgExciseCost.setScale(6,RoundingMode.HALF_EVEN)));
            row.put("Unit+Excise Cost", new RateAmount(totalUnitCost.setScale(6,RoundingMode.HALF_EVEN)));

            row.put("Total Available",pTotalQty);
            row.put("Total Available COGS",new DollarAmount(pTotalCost));
            row.put("Total Available Excise",new DollarAmount(pTotalExcise));
            row.put("Total Available COGS+Excise",new DollarAmount(pTotalCost.add(pTotalExcise)));


            row.put("All Holds",pHoldQty);
            row.put("All Hold COGS",new DollarAmount(pHoldCost));




            BigDecimal grossProfit = retailPrice.subtract(totalUnitCost);
            BigDecimal margin = (retailPrice.doubleValue() > 0) ? grossProfit.divide(retailPrice,2,RoundingMode.HALF_EVEN) : new BigDecimal(0);
            BigDecimal markup = (totalUnitCost.doubleValue() > 0) ? grossProfit.divide(totalUnitCost,2,RoundingMode.HALF_EVEN) : new BigDecimal(0);

            row.put("Profit",new DollarAmount(grossProfit));
            row.put("Margin %",new Percentage(margin));
            row.put("Markup %",new Percentage(markup));

            report.add(row);


            // calculate quantity for prepackages
            List<Prepackage> prepackageList = productToPrepackages.get(product.getId());
            if (prepackageList != null) {
                for (Prepackage prepackage : prepackageList) {
                    // Calculate qty for raw unit value
                    row = new HashMap<>();
                    row.put("Product Name", String.format("%s (%s)", product.getName(), prepackage.getName()));
                    row.put("Category", categoryName);
                    row.put("Cannabis", cannabis);

                    row.put("Cannabis Type", (product.getCannabisType() != null && StringUtils.isNotBlank(product.getCannabisType().getDisplayName())) ? product.getCannabisType().getDisplayName() : Product.CannabisType.DEFAULT.getDisplayName());
                    row.put("Flower Type", TextUtil.textOrEmpty(product.getFlowerType()));
                    row.put("Status", (product.isActive()) ? "Active" : "Inactive");

                    BigDecimal unitValue = prepackage.getUnitValue().setScale(2, RoundingMode.HALF_EVEN);

                    row.put("Weight", unitValue + "g");
                    row.put("Retail Price",new DollarAmount(prepackage.getPrice()));


                    pTotalQty = new BigDecimal(0);
                    pTotalCost = new BigDecimal(0);
                    pTotalExcise = new BigDecimal(0);
                    pHoldQty = new BigDecimal(0);
                    pHoldCost = new BigDecimal(0);
                    pHoldExcise = new BigDecimal(0);

                    totalBatchCogs = new BigDecimal(0);

                    for (Inventory inventory : inventoryList) {
                        if (inventory.isActive()) {
                            String key = String.format("%s_%s", prepackage.getId(), inventory.getId());
                            List<ProductPrepackageQuantity> quantities = prepackageToQuantities.get(key);

                            BigDecimal totalQty = new BigDecimal(0);
                            BigDecimal totalCost = new BigDecimal(0);
                            BigDecimal totalExcise = new BigDecimal(0);
                            if (quantities != null) {
                                for (ProductPrepackageQuantity prepackQuantity : quantities) {
                                    BigDecimal qty = new BigDecimal(prepackQuantity.getQuantity());
                                    totalQty = totalQty.add(qty);

                                    BigDecimal batchCogs = new BigDecimal(0);

                                    PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(prepackQuantity.getPrepackageItemId());
                                    if (prepackageProductItem != null) {
                                        ProductBatch batch = batchMap.get(prepackageProductItem.getBatchId());
                                        if (batch != null) {
                                            batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                            totalBatchCogs = totalBatchCogs.add(batchCogs);
                                        }


                                        if (isCannabis) {
                                            String vendorId = product.getVendorId();
                                            if (StringUtils.isNotBlank(batch.getVendorId())) {
                                                vendorId = batch.getVendorId();
                                            }
                                            BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                                            totalExcise = totalExcise.add(qty.multiply(unitExcise));
                                        }
                                    }

                                    BigDecimal value = qty.multiply(batchCogs);

                                    totalCost = totalCost.add(value);
                                }
                            }

                            totalQty.setScale(0, RoundingMode.HALF_EVEN);
                            totalCost.setScale(2, RoundingMode.HALF_EVEN);
                            totalExcise.setScale(2, RoundingMode.HALF_EVEN);

                            row.put(inventory.getName() + " Quantity", totalQty);
                            row.put(inventory.getName() + " COGS", new DollarAmount(totalCost));

                            // sum
                            pTotalQty = pTotalQty.add(totalQty);
                            pTotalCost = pTotalCost.add(totalCost);
                            pTotalExcise = pTotalExcise.add(totalExcise);




                            // Holds
                            BigDecimal holdQty = new BigDecimal(0);
                            BigDecimal holdCost = new BigDecimal(0);
                            BigDecimal holdExcise = new BigDecimal(0);
                            List<QuantityLog> holds = holdLogsMap.get(key);
                            if (holds != null) {
                                for (QuantityLog log : holds) {
                                    holdQty = holdQty.add(log.getQuantity());

                                    BigDecimal batchCogs = new BigDecimal(0);

                                    ProductBatch batch = batchMap.get(log.getBatchId());
                                    if (batch != null) {
                                        batchCogs = batch.getCostPerUnit() != null ? batch.getCostPerUnit() : new BigDecimal(0);
                                    }

                                    BigDecimal value = log.getQuantity().multiply(batchCogs);

                                    holdCost = holdCost.add(value);



                                    if (isCannabis) {
                                        String vendorId = product.getVendorId();
                                        if (StringUtils.isNotBlank(batch.getVendorId())) {
                                            vendorId = batch.getVendorId();
                                        }
                                        BigDecimal unitExcise = getUnitExcise(exciseTaxInfo, batch, batchCogs, vendorId, vendorHashMap, retailPrice);

                                        holdExcise = holdExcise.add(log.getQuantity().multiply(unitExcise));
                                    }

                                }
                            }

                            holdQty.setScale(2, RoundingMode.HALF_EVEN);
                            holdCost.setScale(2, RoundingMode.HALF_EVEN);
                            holdExcise.setScale(2, RoundingMode.HALF_EVEN);

                            row.put(inventory.getName() + " Holds",holdQty);
                            row.put(inventory.getName() + " Hold COGS",new DollarAmount(holdCost));

                            // sum
                            pHoldQty = pHoldQty.add(holdQty);
                            pHoldCost = pHoldCost.add(holdCost);
                            pHoldExcise = pHoldExcise.add(holdExcise);
                        }
                    }


                    pTotalQty.setScale(2, RoundingMode.HALF_EVEN);
                    pTotalCost.setScale(2, RoundingMode.HALF_EVEN);

                    avgUnitCost = pTotalQty.doubleValue() > 0 ? pTotalCost.divide(pTotalQty,6,RoundingMode.HALF_EVEN) : new BigDecimal(0);
                    avgExciseCost = pTotalQty.doubleValue() > 0 ? pTotalExcise.divide(pTotalQty,6,RoundingMode.HALF_EVEN) : new BigDecimal(0);
                    totalUnitCost = avgUnitCost.add(avgExciseCost);

                    row.put("Avg. Unit Cost", new RateAmount(avgUnitCost.setScale(6,RoundingMode.HALF_EVEN)));
                    row.put("Avg. Excise Cost", new RateAmount(avgExciseCost.setScale(6,RoundingMode.HALF_EVEN)));
                    row.put("Unit+Excise Cost", new RateAmount(totalUnitCost.setScale(6,RoundingMode.HALF_EVEN)));

                    row.put("Total Available", pTotalQty);
                    row.put("Total Available COGS", new DollarAmount(pTotalCost));
                    row.put("Total Available Excise",new DollarAmount(pTotalExcise));
                    row.put("Total Available COGS+Excise",new DollarAmount(pTotalCost.add(pTotalExcise)));


                    row.put("All Holds",pHoldQty);
                    row.put("All Hold COGS",new DollarAmount(pHoldCost));


                    grossProfit = prepackage.getPrice().subtract(totalUnitCost);
                    margin = (prepackage.getPrice().doubleValue() > 0) ? grossProfit.divide(prepackage.getPrice(),2,RoundingMode.HALF_EVEN) : new BigDecimal(0);
                    markup = (totalUnitCost.doubleValue() > 0) ? grossProfit.divide(totalUnitCost,2,RoundingMode.HALF_EVEN) : new BigDecimal(0);

                    row.put("Profit",new DollarAmount(grossProfit));
                    row.put("Margin %",new Percentage(margin));
                    row.put("Markup %",new Percentage(markup));

                    report.add(row);
                }
            }
        }
        return report;
    }

    private BigDecimal getUnitExcise(ExciseTaxInfo exciseTaxInfo,
                                     ProductBatch batch,
                                     BigDecimal batchCogs,
                                     String vendorId,HashMap<String,Vendor> vendorHashMap,
                                     BigDecimal retailPrice) {
        // Calculate Excise
        if (batch != null && batch.getPerUnitExciseTax() != null && batch.getPerUnitExciseTax().doubleValue() > 0) {
            return batch.getPerUnitExciseTax();
        } else {
            Vendor.ArmsLengthType armsLengthType = batch != null ? batch.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;
            if (armsLengthType == null && StringUtils.isNotBlank(vendorId)) {
                Vendor vendor = vendorHashMap.get(vendorId);
                if (vendor != null && vendor.getArmsLengthType() != null) {
                    armsLengthType = vendor.getArmsLengthType();
                }
            }

            BigDecimal exciseTax = new BigDecimal(0);
            if (armsLengthType == null || armsLengthType == Vendor.ArmsLengthType.ARMS_LENGTH) {
                // calculate based on cogs

                HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(exciseTaxInfo, new BigDecimal(1), batchCogs);
                exciseTax = taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO);
            } else {
                // calculate based on retail price per unit
                exciseTax = retailPrice.multiply(new BigDecimal(.15d));
            }

            return exciseTax;
        }
    }
}
