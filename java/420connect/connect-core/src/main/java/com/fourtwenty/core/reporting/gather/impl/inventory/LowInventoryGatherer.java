package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.google.inject.Inject;

import java.util.*;

public class LowInventoryGatherer implements Gatherer {
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;


    private String[] attrs = new String[]{
            "Product Name",
            "Category",
            "Vendor",
            "Quantity Remaining",
            "Low Status",
            "Unit Type"
    };

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public LowInventoryGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        HashMap<String, ProductCategory> productCategoryMap = categoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        List<Product> activeProductList = productRepository.getActiveProductList(filter.getCompanyId(), filter.getShopId());
        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getProductPrepackageQuantities(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackagesMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        return prepareLowInventory(filter, productCategoryMap, activeProductList, productPrepackageQuantities, prepackagesMap);
    }
    public GathererReport prepareLowInventory(ReportFilter filter, HashMap<String, ProductCategory> productCategoryMap, List<Product> activeProductList, List<ProductPrepackageQuantity> productPrepackageQuantities, HashMap<String, Prepackage> prepackagesMap) {
        GathererReport report = new GathererReport(filter, "Current Low Inventory", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        for (Product product : activeProductList) {
            if (!product.isLowInventoryNotification()) {
                continue; // if enable is false, skip it
            }
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());

            HashMap<String, List<ProductPrepackageQuantity>> productToPrepackageQuantities = new HashMap<>();
            productToPrepackageQuantities.put(product.getId(), productPrepackageQuantities);

            ProductCategory category = productCategoryMap.get(product.getCategoryId());

            data.put(attrs[0], product.getName());
            data.put(attrs[1], category.getName());
            Vendor vendor = product.getVendor();
            if (vendor != null) {
                data.put(attrs[2], vendor.getName().isEmpty() ? "" : vendor.getName());
            } else {
                data.put(attrs[2], "");
            }

            double totalQuantity = 0;
            if (category.getUnitType() == ProductCategory.UnitType.units) {

                List<ProductQuantity> productQuantities = product.getQuantities();
                if (productQuantities.size() == 0) {
                    data.put(attrs[3], 0);
                    data.put(attrs[4], "YES");
                }

                for (ProductQuantity productQuantity : productQuantities) {
                    totalQuantity += productQuantity.getQuantity().doubleValue();
                    if (product.getLowThreshold() != null && totalQuantity <= product.getLowThreshold().doubleValue()) {
                        data.put(attrs[4], "YES");
                    } else {
                        data.put(attrs[4], "NO");
                    }
                }
                data.put(attrs[3], totalQuantity);
            } else {

                List<ProductPrepackageQuantity> prepackageQuantities = productToPrepackageQuantities.get(product.getId());

                if (prepackageQuantities.size() == 0) {
                    data.put(attrs[3], 0);
                    data.put(attrs[4], "YES");
                }
                double otherQuantities = 0;
                for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
                    totalQuantity += prepackageQuantity.getQuantity();
                    Prepackage prepackage = prepackagesMap.get(prepackageQuantity.getId());
                    if (prepackage != null) {
                        double preQuantity = prepackageQuantity.getQuantity() * prepackage.getUnitValue().doubleValue();
                        otherQuantities += preQuantity;
                    }
                }
                totalQuantity += otherQuantities;
                data.put(attrs[3], totalQuantity);

                if (product.getLowThreshold() != null && totalQuantity <= product.getLowThreshold().doubleValue()) {
                    data.put(attrs[4], "YES");
                } else {
                    data.put(attrs[4], "NO");
                }
            }
            data.put(attrs[5], category.getUnitType());
            report.add(data);
        }
        if (filter.getType() == ReportType.LOW_INVENTORY_DASHBOARD) {
            List<HashMap<String, Object>> list = new ArrayList<>();
            report.getData().forEach(dateMap -> {
                if (dateMap.getOrDefault(attrs[4], "").toString().equalsIgnoreCase("YES")) {
                    list.add(dateMap);
                }
            });
            report.setData(list);
        }

        return report;
    }
}
