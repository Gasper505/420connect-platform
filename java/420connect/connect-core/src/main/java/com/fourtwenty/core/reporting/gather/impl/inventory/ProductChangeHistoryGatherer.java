package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stephen on 11/7/16.
 */
public class ProductChangeHistoryGatherer implements Gatherer {

    @Inject
    private ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private PrepackageRepository prepackageRepository;

    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ProductChangeHistoryGatherer() {
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        //set up the headers and field types

        reportHeaders.add("Timestamp");
        fieldTypes.put("Timestamp", GathererReport.FieldType.STRING);
        reportHeaders.add("Initiated");
        fieldTypes.put("Initiated", GathererReport.FieldType.STRING);
        reportHeaders.add("Reference");
        fieldTypes.put("Reference", GathererReport.FieldType.STRING);
        reportHeaders.add("Total");
        fieldTypes.put("Total", GathererReport.FieldType.NUMBER);


        String productId = filter.getMap().get("productId");
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(filter.getCompanyId(),
                filter.getShopId(), Lists.newArrayList(productId));

        Iterable<Inventory> inventoryIterable = inventoryRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        List<Inventory> inventories = Lists.newArrayList(inventoryIterable);
        inventories.sort(new Comparator<Inventory>() {
            @Override
            public int compare(Inventory o1, Inventory o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        if (prepackageHashMap.size() > 0) {
            reportHeaders.add("Total PACKs");
            fieldTypes.put("Total PACKs", GathererReport.FieldType.STRING);
        }

        for (Inventory inventory : inventories) {
            if (inventory.isActive()) {
                reportHeaders.add(inventory.getName());
                fieldTypes.put(inventory.getName(), GathererReport.FieldType.NUMBER);


                if (prepackageHashMap.size() > 0) {
                    reportHeaders.add(inventory.getName() + " PACKs");
                    fieldTypes.put(inventory.getName() + " PACKs", GathererReport.FieldType.STRING);
                }
            }
        }
        GathererReport report = new GathererReport(filter, "Product Change History", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        Product product = productRepository.get(filter.getCompanyId(), productId);

        if (product == null) {
            return report;
        }


        HashMap<String, Inventory> inventoryMap = new HashMap<>();

        //grab our data for our product below
        Iterable<ProductChangeLog> changeLogs = productChangeLogRepository.getProductChangeLogsDates(filter.getCompanyId(),
                filter.getShopId(), productId, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        // Get Employees
        List<ObjectId> employeeIds = new ArrayList<>();
        for (ProductChangeLog changeLog : changeLogs) {
            if (changeLog.getEmployeeId() != null && ObjectId.isValid(changeLog.getEmployeeId())) {
                employeeIds.add(new ObjectId(changeLog.getEmployeeId()));
            }
        }
        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(filter.getCompanyId(), employeeIds);


        for (ProductChangeLog changeLog : changeLogs) {
            HashMap<String, Object> data = new HashMap<>();
            String timestamp = ProcessorUtil.timeStampWithOffsetLong(changeLog.getCreated(), filter.getTimezoneOffset());

            data.put("Timestamp", timestamp);

            Employee employee = employeeHashMap.get(changeLog.getEmployeeId());
            if (employee != null) {
                data.put("Initiated", employee.getFirstName() + " " + employee.getLastName());
            } else {
                data.put("Initiated", "System");
            }

            data.put("Reference", changeLog.getReference());

            double total = 0.0;
            HashMap<String, Integer> totalPacksMap = new HashMap<>();
            for (Inventory inventory : inventories) {
                if (inventory.isActive()) {
                    double quantity = 0;
                    for (ProductQuantity productQuantity : changeLog.getProductQuantities()) {
                        if (productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                            quantity = productQuantity.getQuantity().doubleValue();
                            break;
                        }
                    }
                    HashMap<String, Integer> prepackageQuantitiesMap = new HashMap<>();
                    for (ProductPrepackageQuantity prepackageQuantity : changeLog.getPrepackageQuantities()) {
                        if (prepackageQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageQuantity.getPrepackageId());
                            if (prepackage != null) {
                                Integer value = prepackageQuantitiesMap.getOrDefault(prepackage.getName(), 0);
                                value += prepackageQuantity.getQuantity();
                                prepackageQuantitiesMap.put(prepackage.getName(), value);


                                Integer totalPack = totalPacksMap.getOrDefault(prepackage.getName(), 0);
                                totalPack += prepackageQuantity.getQuantity();
                                totalPacksMap.put(prepackage.getName(), totalPack);

                            }
                        }
                    }

                    StringBuilder packs = new StringBuilder();
                    for (String key : prepackageQuantitiesMap.keySet()) {
                        if (packs.length() > 0) {
                            packs.append(",\n");
                        }
                        packs.append(String.format("%s: %s", key, prepackageQuantitiesMap.get(key)));
                    }

                    StringBuilder totalPacks = new StringBuilder();
                    for (String key : totalPacksMap.keySet()) {
                        if (totalPacks.length() > 0) {
                            totalPacks.append(",\n");
                        }
                        totalPacks.append(String.format("%s: %s", key, totalPacksMap.get(key)));
                    }


                    total += quantity;
                    data.put(inventory.getName(), NumberUtils.round(quantity, 2));
                    data.put(inventory.getName() + " PACKs", packs);
                    data.put("Total PACKs", totalPacks);

                }
            }
            data.put("Total", NumberUtils.round(total, 2));


            report.add(data);
        }

        return report;
    }
}
