package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByProduct;
import com.fourtwenty.core.util.NumberUtils;

import java.util.*;

/**
 * Created by Gaurav Saini on 17/5/17.
 */
public class ProductGatherer implements Gatherer {
    private ProductRepository productRepository;
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Product/Service Name", "Product Description", "SKU", "Type", "Total Quantity Sold", "Total Sales"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ProductGatherer(ProductRepository repository, TransactionRepository transactionRepository) {
        this.productRepository = repository;
        this.transactionRepository = transactionRepository;

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Product Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        HashSet<String> addedProducts = new HashSet<>();
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        Iterable<SalesByProduct> results = transactionRepository.getSalesByProduct(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        for (SalesByProduct sbp : results) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            Product product = productMap.get(sbp.getId());
            if (product != null) {
                data.put(attrs[0], product.getName());
                data.put(attrs[1], product.getDescription());
                data.put(attrs[2], product.getSku());
                data.put(attrs[3], product.getFlowerType());
                double unitsSold = sbp.getUnitsSold() == null ? 0 : sbp.getUnitsSold();
                data.put(attrs[4], NumberUtils.round(unitsSold, 2));
                data.put(attrs[5], sbp.getSales());

                if (!addedProducts.contains(product.getId())) {
                    report.add(data);
                    addedProducts.add(product.getId());
                }
            }
        }
        return report;
    }
}