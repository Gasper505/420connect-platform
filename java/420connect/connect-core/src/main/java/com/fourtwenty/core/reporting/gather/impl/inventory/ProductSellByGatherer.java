package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class ProductSellByGatherer implements Gatherer {

    private ProductRepository productRepository;
    private String[] attrs = new String[]{"Product Category", "Product Name", "Status", "Batch Id", "Sell By Date", "Qty remaining"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private ProductBatchRepository productBatchRepository;
    private ProductCategoryRepository productCategoryRepository;

    public ProductSellByGatherer(ProductRepository productRepository, ProductBatchRepository productBatchRepository, ProductCategoryRepository productCategoryRepository) {
        this.productRepository = productRepository;
        this.productBatchRepository = productBatchRepository;
        this.productCategoryRepository = productCategoryRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Product Expiring", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Product> products = productRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        List<String> productIds = new ArrayList<>();
        for (Product dbProduct : products) {
            productIds.add(dbProduct.getId());
        }

        List<String> productIdList = Lists.newArrayList(productIds);

        Map<String, ProductBatch> latestBatchForProductList = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), productIdList);
        ProductCategory category;
        HashSet<String> addedProducts = new HashSet<>();
        for (Product product : products) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());

            category = productCategoryHashMap.get(product.getCategoryId());

            data.put(attrs[0], (category != null && StringUtils.isNotBlank(category.getName())) ? category.getName() : "N/A");

            data.put(attrs[1], product.getName());
            data.put(attrs[2], product.isActive() ? "Active" : "InActive");

            ProductBatch productBatch = latestBatchForProductList.get(product.getId());

            if (productBatch != null) {
                data.put(attrs[3], productBatch.getSku());
                data.put(attrs[4], productBatch.getSellBy() == 0 ? "N/A" : ProcessorUtil.dateString(productBatch.getSellBy()));
                data.put(attrs[5], productBatch.getQuantity());
            } else {
                data.put(attrs[3], "No Batch");
                data.put(attrs[4], "N/A");
                data.put(attrs[5], "0");
            }

            if (!addedProducts.contains(product.getId())) {
                report.add(data);
                addedProducts.add(product.getId());
            }
        }
        return report;
    }
}
