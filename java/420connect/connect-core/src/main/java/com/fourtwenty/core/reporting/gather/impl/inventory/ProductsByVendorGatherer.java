package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Stephen Schmidt on 7/13/2016.
 */
public class ProductsByVendorGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ProductPrepackageQuantityRepository prepackageQuantityRepository;

    private String[] attrs = new String[]{"Vendor", "Contact First Name", "Contact Surname", "Product", "Quantity Sold", "Quantity In Stock", "Sales"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ProductsByVendorGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER, GathererReport.FieldType.NUMBER, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Products By Vendor", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);


        Map<String, Product> products = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        Map<String, Vendor> vendors = vendorRepository.listAllAsMap(filter.getCompanyId());

        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());


        // structure for prepackageQuantities
        HashMap<String, List<ProductPrepackageQuantity>> productToQuantities = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
            List<ProductPrepackageQuantity> items = productToQuantities.get(prepackageQuantity.getProductId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(prepackageQuantity);
            productToQuantities.put(prepackageQuantity.getProductId(), items);
        }


        Iterable<Transaction> completedTransactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        HashMap<String, ProductSaleInfo> salesMap = new HashMap<>();
        for (Transaction ts : completedTransactions) {

            double total = 0.0;
            for (OrderItem orderItem : ts.getCart().getItems()) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                Product product = products.get(orderItem.getProductId());
                if (product != null) {
                    if (product.isDiscountable()) {
                        total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                    }
                }
            }

            for (OrderItem item : ts.getCart().getItems()) {
                if (item.getStatus() != OrderItem.OrderItemStatus.Refunded) {

                    double finalCost = item.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;
                    double propDiscount = NumberUtils.round(ts.getCart().getCalcCartDiscount().doubleValue() * ratio, 6);

                    double propDelivery = NumberUtils.round(ts.getCart().getDeliveryFee().doubleValue() * ratio, 6);


                    ProductSaleInfo saleInfo = salesMap.get(item.getProductId());
                    if (saleInfo == null) {
                        saleInfo = new ProductSaleInfo();
                        salesMap.put(item.getProductId(), saleInfo);
                    }

                    PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());

                    double qtySold = item.getQuantity().doubleValue();
                    if (prepackageProductItem != null) {

                        Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                        if (prepackage != null) {
                            BigDecimal unitValue = prepackage.getUnitValue();
                            if (unitValue == null || unitValue.doubleValue() == 0) {
                                ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                                unitValue = weightTolerance.getUnitValue();
                            }
                            // calculate the total quantity based on the prepackage value
                            qtySold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        }
                    }

                    saleInfo.qty += qtySold;
                    saleInfo.sales += item.getFinalPrice().doubleValue() - propDiscount + propDelivery;
                }
            }
        }

        for (Product p : products.values()) {
            HashMap<String, Object> data = new HashMap<>();
            Vendor v = vendors.get(p.getVendorId());
            String vendorName, vendorFN, vendorLN;
            if (v != null) {
                vendorName = v.getName();
                vendorFN = v.getFirstName();
                vendorLN = v.getLastName();
            } else {
                vendorName = vendorFN = vendorLN = "Error";
            }
            data.put(attrs[0], vendorName);
            data.put(attrs[1], vendorFN);
            data.put(attrs[2], vendorLN);
            data.put(attrs[3], p.getName());

            ProductSaleInfo saleInfo = salesMap.get(p.getId());

            double totalSales = 0;
            double qtySold = 0;
            if (saleInfo != null) {
                totalSales += saleInfo.sales;
                qtySold += saleInfo.qty;
            }

            data.put(attrs[4], NumberUtils.round(qtySold, 2));


            double quantity = 0.0;
            for (ProductQuantity quant : p.getQuantities()) {
                quantity += quant.getQuantity().doubleValue();
            }

            // now look at the prepackages
            List<ProductPrepackageQuantity> prepackageQuantityList = productToQuantities.get(p.getId());
            if (prepackageQuantityList != null) {
                for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantityList) {
                    Prepackage prepackage = prepackageHashMap.get(prepackageQuantity.getPrepackageId());
                    if (prepackage != null && prepackage.isActive()) {

                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = (weightTolerance == null) ? BigDecimal.ZERO : weightTolerance.getUnitValue();
                        }

                        quantity += prepackageQuantity.getQuantity() * unitValue.doubleValue();
                    }
                }
            }


            data.put(attrs[5], quantity);
            data.put(attrs[6], NumberUtils.round(totalSales, 2));
            report.add(data);
        }


        return report;
    }

    class ProductSaleInfo {
        public double sales = 0;
        public double qty = 0;
        public String name = "";
    }
}
