package com.fourtwenty.core.reporting.gather.impl.inventory;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class PurchaseOrderByCategoryGatherer implements Gatherer {

    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;




    private String[] attrs = new String[]{"PO Date", "Vendor Name", "Product Category ", "COGS", "Excise Tax", "PO #"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public PurchaseOrderByCategoryGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }
    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Purchase Order by Category", reportHeaders);
        report.setReportFieldTypes(fieldTypes);


        List<PurchaseOrder.PurchaseOrderStatus> statusList = new ArrayList<>();
        statusList.add(PurchaseOrder.PurchaseOrderStatus.Approved);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.Closed);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
        statusList.add(PurchaseOrder.PurchaseOrderStatus.WaitingShipment);


        Iterable<PurchaseOrder> poByStatus = purchaseOrderRepository.getPurchaseOrderByStatusWithPODate(filter.getCompanyId(), filter.getShopId(), statusList, PurchaseOrder.CustomerType.VENDOR, filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        Set<ObjectId> productCategoryIds = new HashSet<>();
        List<ObjectId> vendorIds = new ArrayList<>();
        HashMap<String, PoPrductCategory> results = new HashMap<>();



        for (PurchaseOrder purchaseOrder : poByStatus) {
            if (CollectionUtils.isNotEmpty(purchaseOrder.getPoProductRequestList())) {
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    if (StringUtils.isNotBlank(poProductRequest.getProductId()) && ObjectId.isValid(poProductRequest.getProductId())) {
                        productIds.add(new ObjectId(poProductRequest.getProductId()));

                    }
                }
            }
            if (StringUtils.isNotBlank(purchaseOrder.getVendorId()) && ObjectId.isValid(purchaseOrder.getVendorId())) {
                vendorIds.add(new ObjectId(purchaseOrder.getVendorId()));
            }
        }

        if (productIds.isEmpty()) {
            return report;
        }
        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(),  Lists.newArrayList(productIds));
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(filter.getCompanyId(), vendorIds);
        Map<String, ProductBatch> recentBatchMap = productBatchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));


        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productCategoryIds));

        for (PurchaseOrder purchaseOrder : poByStatus) {
            if (CollectionUtils.isEmpty(purchaseOrder.getPoProductRequestList())) {
                continue;
            }

            for (POProductRequest productRequest : purchaseOrder.getPoProductRequestList()) {
                Product product = productHashMap.get(productRequest.getProductId());
                if (product == null) {
                    continue;
                }

                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                Vendor vendor = vendorHashMap.get(purchaseOrder.getVendorId());
                if (category == null) {
                    continue;
                }

                String key = category.getName() + "_" + purchaseOrder.getPoNumber();
                PoPrductCategory poPrductCategory = results.get(key);

                if (poPrductCategory == null) {
                    poPrductCategory = new PoPrductCategory();
                    results.put(key, poPrductCategory);
                }
                poPrductCategory.purchaseOrderDate = purchaseOrder.getPurchaseOrderDate() == 0 ? purchaseOrder.getCreated() : purchaseOrder.getPurchaseOrderDate();
                poPrductCategory.vendorName = vendor != null && (StringUtils.isNotBlank(vendor.getName()))  ? vendor.getName() : TextUtil.textOrEmpty("N/A");
                poPrductCategory.name =  StringUtils.isNotBlank(category.getName()) ? category.getName() : "N/A";
                BigDecimal  cogs = new BigDecimal(0);
                if (!Objects.isNull(productRequest.getBatchQuantityMap()) && productRequest.getBatchQuantityMap().size() > 0) {
                    for (Map.Entry<String, BigDecimal> entry : productRequest.getBatchQuantityMap().entrySet()) {
                        ProductBatch batch = batchHashMap.get(entry.getKey());
                        if (batch != null) {
                            cogs = cogs.add(batch.getFinalUnitCost().multiply(entry.getValue()));
                        } else {
                            BigDecimal unitCost = getUnitCost(product, recentBatchMap);
                            cogs = cogs.add(entry.getValue().multiply(unitCost));
                        }
                    }
                } else {
                    BigDecimal unitCost = getUnitCost(product, recentBatchMap);
                    cogs = cogs.add(productRequest.getRequestQuantity().multiply(unitCost));
                }
                poPrductCategory.cogs = poPrductCategory.cogs.add(cogs);
                BigDecimal exciseTax = new BigDecimal(0);
                exciseTax = exciseTax.add(productRequest.getTotalExciseTax().compareTo(BigDecimal.ZERO)  > 0 ? productRequest.getTotalExciseTax() : BigDecimal.ZERO);
                poPrductCategory.exciseTax = poPrductCategory.exciseTax.add(exciseTax);
                poPrductCategory.poNumber = purchaseOrder.getPoNumber();
            }

        }

        for (String key : results.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            PoPrductCategory poPrductCategory = results.get(key);
            int i = 0;
            data.put(attrs[i], DateUtil.toDateFormatted(poPrductCategory.purchaseOrderDate));
            data.put(attrs[++i], poPrductCategory.vendorName);
            data.put(attrs[++i], poPrductCategory.name);
            data.put(attrs[++i], NumberUtils.round(poPrductCategory.cogs, 2));
            data.put(attrs[++i], NumberUtils.round(poPrductCategory.exciseTax, 2));
            data.put(attrs[++i], poPrductCategory.poNumber);
            report.add(data);
        }
        return report;
    }

    private BigDecimal getUnitCost(Product p, Map<String, ProductBatch> recentBatchMap) {
        ProductBatch batch = recentBatchMap.get(p.getId());
        BigDecimal unitCost = BigDecimal.ZERO;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost();
        }
        return unitCost;
    }

    public static final class PoPrductCategory {
        public String name;
        private String poNumber;
        private String vendorName;
        private long purchaseOrderDate;
        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal cogs = new BigDecimal(0);

        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal exciseTax = new BigDecimal(0);
    }

}