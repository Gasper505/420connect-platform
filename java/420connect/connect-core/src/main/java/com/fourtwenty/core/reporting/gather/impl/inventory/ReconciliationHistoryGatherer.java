package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * Class is Responsible to get the reconciliation history data for reconciliation history report
 */
public class ReconciliationHistoryGatherer implements Gatherer {
    private static final String RECONCILIATION_HISTORY_REPORT = "Reconciliation History Report";

    private TerminalRepository terminalRepository;
    private ProductRepository productRepository;
    private ProductCategoryRepository productCategoryRepository;
    private ReconciliationHistoryRepository reconciliationHistoryRepository;
    private PrepackageRepository prepackageRepository;
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private EmployeeRepository employeeRepository;
    private InventoryRepository inventoryRepository;
    private ProductBatchRepository batchRepository;
    private ProductWeightToleranceRepository weightToleranceRepository;
    private ShopRepository shopRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{
            "Timestamp", // 0
            "Reconciliation No", // 1
            "Employee Name", // 2
            "Inventory Name", // 3
            "Product Name", // 4
            "Product sku", //5
            "Product Category", // 6
            "Prepackage Name", // 7
            "Batch sku", // 8
            "Old Quantity", // 9
            "New Quantity", // 10
            "Difference", // 11
            "Report Loss", // 12
            "Unit Cost", //13
            "Cogs", // 14
            "Reason"// 15
    };

    /**
     * Constructor
     *
     * @param terminalRepository              : For Terminal Information.
     * @param productRepository               : For Product information.
     * @param productCategoryRepository       : For product category information.
     * @param reconciliationHistoryRepository : For reconciliation history data information.
     * @param prepackageRepository            : For prepackage information.
     * @param prepackageProductItemRepository : For prepackage Item information.
     * @param inventoryRepository
     */
    public ReconciliationHistoryGatherer(TerminalRepository terminalRepository,
                                         ProductRepository productRepository,
                                         ProductCategoryRepository productCategoryRepository,
                                         ReconciliationHistoryRepository reconciliationHistoryRepository,
                                         PrepackageRepository prepackageRepository,
                                         PrepackageProductItemRepository prepackageProductItemRepository,
                                         EmployeeRepository employeeRepository, InventoryRepository inventoryRepository,
                                         ProductBatchRepository batchRepository, ProductWeightToleranceRepository weightToleranceRepository, ShopRepository shopRepository) {
        this.terminalRepository = terminalRepository;
        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.reconciliationHistoryRepository = reconciliationHistoryRepository;
        this.prepackageRepository = prepackageRepository;
        this.prepackageProductItemRepository = prepackageProductItemRepository;
        this.employeeRepository = employeeRepository;
        this.inventoryRepository = inventoryRepository;
        this.batchRepository = batchRepository;
        this.weightToleranceRepository = weightToleranceRepository;
        this.shopRepository = shopRepository;


        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.DATE, // 0
                GathererReport.FieldType.STRING, // 1
                GathererReport.FieldType.STRING, // 2
                GathererReport.FieldType.STRING, // 3
                GathererReport.FieldType.STRING, // 4
                GathererReport.FieldType.STRING, // 5
                GathererReport.FieldType.STRING, // 6
                GathererReport.FieldType.STRING, // 7
                GathererReport.FieldType.STRING, // 8
                GathererReport.FieldType.BIGDECIMAL, // 9
                GathererReport.FieldType.BIGDECIMAL, // 10
                GathererReport.FieldType.BIGDECIMAL, // 11
                GathererReport.FieldType.STRING, // 12
                GathererReport.FieldType.BIGDECIMAL, //13
                GathererReport.FieldType.CURRENCY, // 14
                GathererReport.FieldType.STRING // 15
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    /**
     * Overridden Method
     *
     * @param filter : Report filter having the information of companyId, ShopId, start limit, End Limit etc.
     * @return report   : Having the information related to report which want to display on web.
     */
    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report;
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        if (shop.getAppTarget() != CompanyFeatures.AppTarget.Retail) {
            fieldTypes.remove(attrs[12]);
            reportHeaders.remove(12);
            attrs = ArrayUtils.remove(attrs, 12);
        }
        if (StringUtils.isNotBlank(filter.getReconciliationId())) {
            ArrayList<String> attributesList = new ArrayList<>(Arrays.asList(attrs));
            attrs = attributesList.toArray(new String[attributesList.size()]);
            reportHeaders = new ArrayList<>();
            Collections.addAll(reportHeaders, attrs);
            report = new GathererReport(filter, RECONCILIATION_HISTORY_REPORT, reportHeaders);
        } else {
            report = new GathererReport(filter, RECONCILIATION_HISTORY_REPORT, reportHeaders);
        }
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(filter.getCompanyId());

        HashMap<String, Terminal> terminalMap = terminalRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Product> productHashMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> batchHashMap = batchRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (String productId : productHashMap.keySet()) {
            Product product = productHashMap.get(productId);
            List<Product> items = productsByCompanyLinkId.get(product.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(product);
            productsByCompanyLinkId.put(product.getCompanyLinkId(), items);
        }


        if (StringUtils.isBlank(filter.getReconciliationId())) {

            Iterable<ReconciliationHistory> history = reconciliationHistoryRepository.getReconciliationHistoryByDateRange(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

            for (ReconciliationHistory reconciliationHistory : history) {
                if (!Objects.isNull(reconciliationHistory)) {
                    prepareReconciliationHistoryDetails(filter, report, reconciliationHistory, employeeHashMap, terminalMap, productHashMap, productCategoryHashMap, prepackageHashMap, prepackageProductItemHashMap, Boolean.FALSE, inventoryMap,
                            batchHashMap, weightToleranceHashMap, recentBatchMap, productsByCompanyLinkId, shop);
                }
            }

        } else {
            ReconciliationHistory reconciliationHistory = reconciliationHistoryRepository.get(filter.getCompanyId(), filter.getReconciliationId());
            if (!Objects.isNull(reconciliationHistory)) {
                prepareReconciliationHistoryDetails(filter, report, reconciliationHistory, employeeHashMap, terminalMap, productHashMap, productCategoryHashMap,
                        prepackageHashMap, prepackageProductItemHashMap, Boolean.TRUE, inventoryMap, batchHashMap, weightToleranceHashMap, recentBatchMap, productsByCompanyLinkId, shop);
            }
        }
        return report;
    }

    private void prepareReconciliationHistoryDetails(ReportFilter filter, GathererReport report, ReconciliationHistory reconciliationHistory, HashMap<String, Employee> employeeHashMap, HashMap<String, Terminal> terminalMap, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> productCategoryHashMap,
                                                     HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemHashMap, boolean isSingleReconciliation, HashMap<String, Inventory> inventoryMap, HashMap<String, ProductBatch> batchHashMap,
                                                     HashMap<String, ProductWeightTolerance> weightToleranceHashMap, HashMap<String, ProductBatch> recentBatchMap, HashMap<String, List<Product>> productsByCompanyLinkId, Shop shop) {
        Employee employee = employeeHashMap.get(reconciliationHistory.getEmployeeId());
        final String employeeName = employee != null ? TextUtil.textOrEmpty(employee.getFirstName()) + " " + TextUtil.textOrEmpty(employee.getLastName()) : "";

        reconciliationHistory.getReconciliations().forEach((ReconciliationHistoryList reconciliation) -> {

            String productName = StringUtils.EMPTY;
            String productSku = StringUtils.EMPTY;
            String categoryName = StringUtils.EMPTY;
            String prePackageName = StringUtils.EMPTY;
            StringBuilder batchSku = new StringBuilder();

            ProductCategory category;
            boolean calculated = false;
            double cogs = 0;
            double unitCost = 0;
            double difference = reconciliation.getNewQuantity().doubleValue() - reconciliation.getOldQuantity().doubleValue();

            Terminal terminal = terminalMap.get(reconciliation.getTerminalId());
            Inventory inventory = inventoryMap.get(reconciliation.getInventoryId());
            Product product = productHashMap.get(reconciliation.getProductId());

            if (product == null) {
                return;
            }
            productName = product.getName();
            productSku = product.getSku();

            category = productCategoryHashMap.get(product.getCategoryId());
            if (!Objects.isNull(category)) {
                categoryName = category.getName();
            }

            String reason = "";
            if (reconciliation.getReconciliationReason() != null) {
                if (reconciliation.getReconciliationReason().equals(ReconciliationHistoryList.ReconciliationReason.OTHER)) {
                    if (reconciliation.getNote() != null) {
                        reason = reconciliation.getNote().getMessage();
                    }
                } else {
                    reason = reconciliation.getReconciliationReason().toString();
                }
            }

            if (reconciliation.getOperations() != null && reconciliation.getOperations().size() > 0) {
                // NEW SOLUTION
                // Use Inventory Operations
                for (InventoryOperation operation : reconciliation.getOperations()) {
                    ProductBatch targetBatch = batchHashMap.get(operation.getBatchId());
                    if (targetBatch != null) {
                        calculated = true;
                        batchSku.append(targetBatch.getSku());
                        BigDecimal unitCost2 = targetBatch == null ? new BigDecimal(0) : targetBatch.getFinalUnitCost();
                        unitCost = unitCost2.doubleValue();
                    }

                    // Handle prepackages
                    if (StringUtils.isNotBlank(operation.getPrepackageItemId())) {
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(operation.getPrepackageItemId());
                        if (!Objects.isNull(prepackageProductItem)) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            prePackageName = prepackage.getName() + " " + prepackageProductItem.getBatchSKU();
                        }
                    }
                }
            } else {
                // HANDLE OLD SOLUTION
                // Otherwise, if inventory operations doesn't exist, just do the usual
                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(reconciliation.getPrePackageItemId());
                if (!Objects.isNull(prepackageProductItem)) {
                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    prePackageName = prepackage.getName() + " " + prepackageProductItem.getBatchSKU();
                    ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        batchSku.append(targetBatch.getSku());
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        BigDecimal unitCost2 = targetBatch == null ? new BigDecimal(0) : targetBatch.getFinalUnitCost();
                        unitCost = unitCost2.doubleValue();
                        cogs = calcCOGS(difference * unitValue.doubleValue(), targetBatch);
                    }
                } else {
                    if (!Objects.isNull(product)) {
                        if (StringUtils.isNotBlank(reconciliation.getBatchId())) {
                            ProductBatch targetBatch = batchHashMap.get(reconciliation.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                batchSku.append(targetBatch.getSku());
                                BigDecimal unitCost2 = targetBatch == null ? new BigDecimal(0) : targetBatch.getFinalUnitCost();
                                //cogs = calcCOGS(difference, targetBatch);
                                unitCost = unitCost2.doubleValue();
                            }
                        } else {
                            Map<String, Double> batchQuantityInfoMap = reconciliationHistory.getBatchQuantityInfoMap();
                            if (batchQuantityInfoMap != null) {
                                for (String batchId : batchQuantityInfoMap.keySet()) {
                                    ProductBatch productBatch = batchHashMap.get(batchId);
                                    if (productBatch != null && productBatch.getProductId().equals(product.getId())) {
                                        calculated = true;
                                        BigDecimal unitCost2 = productBatch == null ? new BigDecimal(0) : productBatch.getFinalUnitCost();
                                        unitCost = unitCost2.doubleValue();
                                        //cogs = calcCOGS(batchQuantityInfoMap.get(batchId), productBatch);
                                    }
                                }
                            }
                        }
                    }


                }
            }

            if (!calculated) {
                unitCost = getUnitCost(productHashMap.get(reconciliation.getProductId()), recentBatchMap, productsByCompanyLinkId);
            }
            if (cogs == 0) {
                cogs = unitCost * difference;
            }

            if (cogs < 0 && difference > 0) {
                System.out.println("here");
            }
            setReportDetails(filter, report, reconciliationHistory, reconciliation, employeeName, productName, productSku, categoryName, inventory, prePackageName, batchSku.toString(), difference, cogs, reason, new BigDecimal(unitCost), shop);
        });
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    private void setReportDetails(ReportFilter filter, GathererReport report, ReconciliationHistory reconciliationHistory, ReconciliationHistoryList reconciliation,
                                  String employeeName, String productName, String productSku, String categoryName,
                                  Inventory inventory, String prePackageName, String batchSku, double difference, double cogs, String reason, BigDecimal unitCost, Shop shop) {
        HashMap data = new HashMap();
        String createDate = ProcessorUtil.timeStampWithOffset(reconciliationHistory.getCreated(), filter.getTimezoneOffset());
        data.put(attrs[0], createDate);
        data.put(attrs[1], reconciliationHistory.getRequestNo());
        data.put(attrs[2], employeeName);
        data.put(attrs[3], Objects.isNull(inventory) ? StringUtils.EMPTY : inventory.getName());
        data.put(attrs[4], productName);
        data.put(attrs[5], productSku);
        data.put(attrs[6], categoryName);
        data.put(attrs[7], prePackageName);
        data.put(attrs[8], batchSku);
        data.put(attrs[9], NumberUtils.round(reconciliation.getOldQuantity(), 4));
        data.put(attrs[10], NumberUtils.round(reconciliation.getNewQuantity(), 4));
        data.put(attrs[11], NumberUtils.round(difference, 2));
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
            data.put(attrs[12], reconciliation.isReportLoss() ? "Yes" : "No");
            data.put(attrs[13], unitCost);
            data.put(attrs[14], NumberUtils.round(cogs, 2));
            data.put(attrs[15], reason);
        } else {
            data.put(attrs[12], unitCost);
            data.put(attrs[13], NumberUtils.round(cogs, 2));
            data.put(attrs[14], reason);
        }

        report.add(data);
    }
}
