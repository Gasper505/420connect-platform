package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

public class ReturnToVendorGatherer implements Gatherer {

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    private String[] attrs = new String[]{
            "Date",
            "Vendor",
            "Product Category",
            "Product",
            "Batch SKU",
            "Quantity"
    };

    public ReturnToVendorGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Return To Vendor Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        List<Transaction.TransactionType> types = new ArrayList<>();
        types.add(Transaction.TransactionType.ReturnToVendor);

        Iterable<Transaction> results = transactionRepository.getTransactionByTypes(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), types);

        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> batchIds = new HashSet<>();

        for (Transaction transaction : results) {
            if (transaction.getCart() != null && !CollectionUtils.isNullOrEmpty(transaction.getCart().getItems())) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));

                        if (StringUtils.isNotBlank(item.getBatchId()) && ObjectId.isValid(item.getBatchId())) {
                            batchIds.add(new ObjectId(item.getBatchId()));
                        }
                    }
                }
            }
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductBatch> productBatchMap = productBatchRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(batchIds));

        Set<ObjectId> productCategoryIds = new HashSet<>();
        Set<ObjectId> vendorIds = new HashSet<>();
        for (Map.Entry<String, Product> entry : productMap.entrySet()) {
            if (StringUtils.isNotBlank(entry.getValue().getCategoryId()) && ObjectId.isValid(entry.getValue().getCategoryId())) {
                productCategoryIds.add(new ObjectId(entry.getValue().getCategoryId()));
            }
            if (StringUtils.isNotBlank(entry.getValue().getVendorId()) && ObjectId.isValid(entry.getValue().getVendorId())) {
                vendorIds.add(new ObjectId(entry.getValue().getVendorId()));
            }
        }

        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productCategoryIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds));

        HashMap<String, Object> rowMap;
        String notAvailable = "N/A";
        for (Transaction transaction : results) {
            if (transaction.getCart() != null && !CollectionUtils.isNullOrEmpty(transaction.getCart().getItems())) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    Product product = productMap.get(item.getProductId());

                    if (product == null) {
                        continue;
                    }

                    Vendor vendor = vendorMap.get(product.getVendorId());
                    ProductCategory category = productCategoryMap.get(product.getCategoryId());
                    ProductBatch productBatch = productBatchMap.get(item.getBatchId());

                    rowMap = new HashMap<>();
                    rowMap.put(attrs[0], ProcessorUtil.dateString(transaction.getCreated()));
                    rowMap.put(attrs[1], vendor != null ? vendor.getName() : notAvailable);
                    rowMap.put(attrs[2], category != null ? category.getName() : notAvailable);
                    rowMap.put(attrs[3], product.getName());
                    rowMap.put(attrs[4], productBatch != null ? (StringUtils.isNotBlank(productBatch.getSku()) ? productBatch.getSku() : notAvailable) : notAvailable);
                    rowMap.put(attrs[5], item.getQuantity());

                    report.add(rowMap);
                }
            }
        }

        return report;
    }
}
