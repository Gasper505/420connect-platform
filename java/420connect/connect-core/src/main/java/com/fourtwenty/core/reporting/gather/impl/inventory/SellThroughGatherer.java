package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SellThroughGatherer implements Gatherer {

    @Inject
    private ProductRepository productRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private List<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = new String[]{"Product Id", "Product Name", "Category", "Avg Qty Sold Per Day", "Days Remaining in Inventory", "Currenty Quanity On Hand"};

    public SellThroughGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Product Sell Through Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Transaction> sales = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        List<ObjectId> productIds = new ArrayList<>();
        for (Transaction transaction : sales) {
            if (transaction.getCart() != null && transaction.getCart().getItems().size() > 0) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));
                    }
                }
            }
        }

        HashMap<String, Product> productMap = productRepository.findProductsByIdsAsMap(filter.getCompanyId(), filter.getShopId(), productIds);
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        int daysCount = DateUtil.getDaysBetweenTwoDates(filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<String> visitedProduct = new HashSet<>();
        HashMap<String, CategorySale> results = new HashMap<>();
        for (Transaction transaction : sales) {

            if (Transaction.TransactionType.Refund == transaction.getTransType()) {
                continue;
            }

            Cart cart = transaction.getCart();
            if (cart == null || cart.getItems() == null || cart.getItems().size() == 0) {
                continue;
            }

            for (OrderItem item : cart.getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == cart.getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }

                if (!ReportFilter.ALL_CATEGORIES.equals(filter.getCategoryId()) && !filter.getCategoryId().equals(product.getCategoryId())) {
                    continue;
                }

                ProductCategory category = productCategoryMap.get(product.getCategoryId());

                CategorySale categorySale = results.get(product.getId());
                if (categorySale == null) {
                    categorySale = new CategorySale();
                    results.put(product.getId(), categorySale);
                }

                categorySale.productId = product.getId();
                categorySale.productName = product.getName();
                categorySale.categoryName = category.getName();
                categorySale.quantity = categorySale.quantity + item.getQuantity().doubleValue();

                if (!visitedProduct.contains(product.getId()) && product.getQuantities() != null &&
                        !product.getQuantities().isEmpty()) {
                    double currentQuantity = 0d;
                    for (ProductQuantity productQuantity : product.getQuantities()) {
                        if (productQuantity.getQuantity() != null) {
                            currentQuantity += productQuantity.getQuantity().doubleValue();
                        }
                    }

                    categorySale.currentQuantity = currentQuantity;
                }

                visitedProduct.add(product.getId());
            }
        }

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        for (String productId : results.keySet()) {
            HashMap<String, Object> data = new HashMap<>();

            CategorySale sale = results.get(productId);

            if (daysCount == 0) {
                daysCount = 1;
            }

            double avgSoldQuantity = Double.parseDouble(decimalFormat.format(sale.quantity / daysCount));

            double remainingDays = 0d;
            if (avgSoldQuantity != 0) {
                remainingDays = sale.currentQuantity / avgSoldQuantity;
            }

            data.put(attrs[0], sale.productId);
            data.put(attrs[1], sale.productName);
            data.put(attrs[2], sale.categoryName);
            data.put(attrs[3], avgSoldQuantity);
            data.put(attrs[4], TextUtil.formatToTwoDecimalPoints(Math.ceil(remainingDays * 4) / 4) + " Days");
            data.put(attrs[5], sale.currentQuantity);

            report.add(data);
        }


        return report;
    }

    public static final class CategorySale {
        String productId;
        String productName;
        String categoryName;
        double quantity = 0d;
        double currentQuantity = 0d;
    }
}
