package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentBillRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by decipher on 18/11/17 2:44 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class ShipmentBillGatherer implements Gatherer {
    private String[] attrs = new String[]{"Date", "Vendor", "PO Number", "Shipment Bill NO", "Total Cost", "Paid Amount", "Unpaid Amount", "Status"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ShipmentBillRepository shipmentBillRepository;

    public ShipmentBillGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Shipment Bill Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAllAsMap(filter.getCompanyId());

        Iterable<ShipmentBill> result = shipmentBillRepository.getShipmentBills(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), "{created:-1}");

        HashMap<String, Object> rowMap = null;
        if (result != null) {
            for (ShipmentBill shipmentBill : result) {
                rowMap = new HashMap<>();

                Vendor vendor = vendorHashMap.get(shipmentBill.getVendorId());
                String vendorName = "";
                if (vendor != null) {
                    vendorName = vendor.getName();
                }

                String poNumber[] = shipmentBill.getShipmentBillNumber().split("-SB");
                rowMap.put(attrs[0], ProcessorUtil.timeStampWithOffset(shipmentBill.getCreated(), filter.getTimezoneOffset()));
                rowMap.put(attrs[1], vendorName);
                rowMap.put(attrs[2], poNumber[0]);
                rowMap.put(attrs[3], shipmentBill.getShipmentBillNumber());
                rowMap.put(attrs[4], shipmentBill.getTotalCost());
                rowMap.put(attrs[5], shipmentBill.getAmountPaid());
                if (shipmentBill.getAmountPaid() != null) {
                    rowMap.put(attrs[6], new DollarAmount(shipmentBill.getTotalCost().doubleValue() - shipmentBill.getAmountPaid().doubleValue()));
                } else {
                    rowMap.put(attrs[6], new DollarAmount(shipmentBill.getTotalCost().doubleValue()));
                }
                rowMap.put(attrs[7], shipmentBill.getPaymentStatus());

                //rowMap.put(attrs[8], shipmentBill.getCompletedDate() != null ? ProcessorUtil.timeStampWithOffsetLong(shipmentBill.getCompletedDate(),filter.getTimezoneOffset()) : "");

                report.add(rowMap);
            }

        } else {
            rowMap = new HashMap<>();
            rowMap.put(attrs[0], "No Data Available");
            rowMap.put(attrs[1], "");
            rowMap.put(attrs[2], "");
            rowMap.put(attrs[3], "");
            rowMap.put(attrs[4], "");
            rowMap.put(attrs[5], "");
            rowMap.put(attrs[6], "");
            report.add(rowMap);
        }

        return report;
    }
}
