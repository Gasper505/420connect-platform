package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stephen on 12/7/16.
 */
public class SingleInventoryGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;


    private ArrayList<String> reportHeaders = new ArrayList<>();
    private String[] attrs = new String[]{"Product", "Category",
            "Current Quantity", "Current COGs",
            "Sold Quantity", "Sold COGs",
            "Current Prepackages", "Current PACK COGs",
            "Sold Prepackages", "Sold PACK COGs"};
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SingleInventoryGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Single Inventory", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        //build past sales per product
        String inventoryId = filter.getInventoryId();
        if (inventoryId == null) {
            return null;
        }

        Inventory inventory = inventoryRepository.get(filter.getCompanyId(), inventoryId);

        String inventoryName = "ALL";
        if (inventory != null) {
            inventoryName = inventory.getName();
        }

        report.setReportPostfix(filter.getStartDate() + " - " + filter.getEndDate() + " - " + inventoryName);

        HashMap<String, Product> allProducts = productRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAllAsMap(filter.getCompanyId());

        Iterable<Prepackage> prepackages = prepackageRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        Iterable<ProductPrepackageQuantity> prepackageQuantities = null;

        if (filter.isAllInventories()) {
            prepackageQuantities = prepackageQuantityRepository.listAllByShop(filter.getCompanyId(), filter.getShopId());
        } else {
            prepackageQuantities = prepackageQuantityRepository.getQuantitiesForInventory(filter.getCompanyId(),
                    filter.getShopId(), inventoryId);
        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAllAsMap(filter.getCompanyId());

        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());


        // create structure relationship for prepackages->products
        HashMap<String, List<Prepackage>> productToPrepackages = new HashMap<>();
        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            List<Prepackage> prepackageList = productToPrepackages.get(prepackage.getProductId());
            if (prepackageList == null) {
                prepackageList = new ArrayList<>();
                productToPrepackages.put(prepackage.getProductId(), prepackageList);
            }
            prepackageList.add(prepackage);
            prepackageHashMap.put(prepackage.getId(), prepackage);
        }

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        // create the structure relationship for quantities
        HashMap<String, AmountWithCogs> prepackageToQuantity = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
            // calculate cogs
            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(prepackageQuantity.getPrepackageItemId());
            if (prepackageProductItem != null) {

                AmountWithCogs amountWithCogs = prepackageToQuantity.get(prepackageQuantity.getPrepackageId());
                if (amountWithCogs == null) {
                    amountWithCogs = new AmountWithCogs();
                }

                amountWithCogs.amount += prepackageQuantity.getQuantity();


                ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                if (prepackage != null && targetBatch != null) {
                    BigDecimal unitValue = prepackage.getUnitValue();
                    if (unitValue == null || unitValue.doubleValue() == 0) {
                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                        unitValue = (weightTolerance == null) ? BigDecimal.ZERO : weightTolerance.getUnitValue();
                    }
                    // calculate the total quantity based on the prepackage value
                    amountWithCogs.cogs += calcCOGS(prepackageQuantity.getQuantity() * unitValue.doubleValue(), targetBatch);
                }

                prepackageToQuantity.put(prepackageQuantity.getPrepackageId(), amountWithCogs);
            }
        }


        HashMap<String, AmountWithCogs> quantitySoldByProduct = new HashMap<>();
        HashMap<String, AmountWithCogs> prepackagesSold = new HashMap<>();


        // Calculate Sold, etc.
        for (Transaction t : transactions) {
            if (filter.getEmployeeId().equalsIgnoreCase(ReportFilter.ALL_EMPLOYEES)
                    || filter.getEmployeeId().equalsIgnoreCase(t.getSellerId())) {

                if (t.getStatus() != Transaction.TransactionStatus.Void) {
                    for (OrderItem item : t.getCart().getItems()) {
                        if (item.getStatus() == OrderItem.OrderItemStatus.Active) {


                            //
                            for (QuantityLog quantityLog : item.getQuantityLogs()) {
                                if (filter.isAllInventories() || quantityLog.getInventoryId().equalsIgnoreCase(inventoryId)) {
                                    if (quantityLog.getPrepackageItemId() != null) {
                                        // selling prepackages
                                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(quantityLog.getPrepackageItemId());

                                        if (prepackageProductItem != null) {
                                            String key = item.getProductId() + "_" + prepackageProductItem.getPrepackageId();


                                            AmountWithCogs t1 = prepackagesSold.get(key);
                                            if (t1 == null) {
                                                t1 = new AmountWithCogs();
                                            }

                                            t1.amount += quantityLog.getQuantity().intValue();


                                            ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                            if (prepackage != null && targetBatch != null) {
                                                BigDecimal unitValue = prepackage.getUnitValue();
                                                if (unitValue == null || unitValue.doubleValue() == 0) {
                                                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                                    unitValue = weightTolerance.getUnitValue();
                                                }
                                                // calculate the total quantity based on the prepackage value
                                                t1.cogs += calcCOGS(item.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                                            }


                                            prepackagesSold.put(key, t1);
                                        }
                                    } else {
                                        // selling non-prepackages
                                        AmountWithCogs t1 = quantitySoldByProduct.get(item.getProductId());
                                        if (t1 == null) {
                                            t1 = new AmountWithCogs();
                                        }

                                        t1.amount += quantityLog.getQuantity().doubleValue();


                                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                            ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                            if (targetBatch != null) {
                                                t1.cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                            }
                                        } else {
                                            // get most recent batch and use that to calculate
                                            ProductBatch latestBatch = recentBatchMap.get(item.getProductId());
                                            if (latestBatch != null) {
                                                t1.cogs += calcCOGS(item.getQuantity().doubleValue(), latestBatch);
                                            }

                                        }


                                        quantitySoldByProduct.put(item.getProductId(), t1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for (Product p : allProducts.values()) {
            ProductCategory category = categoryHashMap.get(p.getCategoryId());
            String unit = category != null && category.getUnitType() == ProductCategory.UnitType.units ? "e" : "g";

            double quantity = 0;
            double qtyCOGs = 0;
            for (ProductQuantity q : p.getQuantities()) {
                if (filter.isAllInventories() || (q.getInventoryId() != null && q.getInventoryId().equals(inventory.getId()))) {
                    quantity += q.getQuantity().doubleValue();
                }
            }
            // get most recent batch and use that to calculate
            ProductBatch latestBatch = recentBatchMap.get(p.getId());
            if (latestBatch != null) {
                qtyCOGs += calcCOGS(quantity, latestBatch);
            }

            HashMap<String, Object> data = new HashMap<>();


            AmountWithCogs qSoldCog = quantitySoldByProduct.get(p.getId());

            Double quantSold = 0d;
            double soldQuantCOGs = 0d;
            if (qSoldCog != null) {
                quantSold = qSoldCog.amount;
                soldQuantCOGs = qSoldCog.cogs;
            }


            // show inventories
            List<Prepackage> prepackageList = productToPrepackages.get(p.getId());

            StringBuffer currentPacks = new StringBuffer();
            StringBuffer soldPacks = new StringBuffer();
            double curPACKCOGs = 0;
            double soldPackCOGs = 0;
            if (prepackageList != null) {
                for (int i = 0; i < prepackageList.size(); i++) {
                    Prepackage prepackage = prepackageList.get(i);
                    AmountWithCogs amountWithCogs = prepackageToQuantity.get(prepackage.getId());
                    if (amountWithCogs != null && amountWithCogs.amount > 0) {
                        if (currentPacks.length() > 0) {
                            currentPacks.append(", ");
                        }
                        currentPacks.append(String.format("%d of %s", (int) amountWithCogs.amount, prepackage.getName()));
                        curPACKCOGs += amountWithCogs.cogs;
                    }

                    String key = p.getId() + "_" + prepackage.getId();
                    // Sold
                    AmountWithCogs pSoldCog = prepackagesSold.get(key);
                    if (pSoldCog != null && pSoldCog.amount > 0) {
                        if (soldPacks.length() > 0) {
                            soldPacks.append(", ");
                        }
                        soldPacks.append(String.format("%d of %s", (int) pSoldCog.amount, prepackage.getName()));
                        soldPackCOGs += pSoldCog.cogs;
                    }

                }
            }


            data.put(attrs[0], p.getName());
            data.put(attrs[1], p.getCategory() != null ? p.getCategory().getName() : "");
            data.put(attrs[2], NumberUtils.round(quantity, 2) + unit);
            data.put(attrs[3], NumberUtils.round(qtyCOGs, 2)); // cur COGs
            data.put(attrs[4], (quantSold == null ? 0.0d : NumberUtils.round(quantSold, 2)) + unit);
            data.put(attrs[5], NumberUtils.round(soldQuantCOGs, 2));

            data.put(attrs[6], currentPacks.toString());
            data.put(attrs[7], NumberUtils.round(curPACKCOGs, 2)); // cur COGs

            data.put(attrs[8], soldPacks.toString());
            data.put(attrs[9], NumberUtils.round(soldPackCOGs, 2)); // cur COGs


            report.add(data);

        }


        return report;
    }


    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap) {
        ProductBatch batch = batchMap.get(p.getId());
        double unitCost = 0;
        if (batch != null) {
            return batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    class AmountWithCogs {
        double amount = 0;
        double cogs = 0;
    }
}
