package com.fourtwenty.core.reporting.gather.impl.inventory;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

import java.util.*;

/**
 * Created by Gaurav Saini on 17/5/17.
 */
public class VendorGatherer implements Gatherer {

    private VendorRepository vendorRepository;
    private ShopRepository shopRepository;
    private String[] attrs = new String[]{"Name", "Company", "Email", "Phone", "Mobile", "Fax",
            "Website", "Street", "City", "State ", "ZIP", "Country", "Opening Balance", "Date", "Tax ID"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public VendorGatherer(VendorRepository repository, ShopRepository shopRepository) {
        this.vendorRepository = repository;
        this.shopRepository = shopRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Vendor Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        HashSet<String> addedVendors = new HashSet<>();

        Shop shop = shopRepository.getById(filter.getShopId());
        List<Vendor> results = vendorRepository.getVendorList(filter.getCompanyId(), filter.getShopId());
        for (Vendor vendor : results) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());

            data.put(attrs[0], vendor.getFirstName() + vendor.getLastName());
            data.put(attrs[1], vendor.getName());
            data.put(attrs[2], vendor.getEmail());
            data.put(attrs[3], vendor.getPhone());
            data.put(attrs[4], vendor.getPhone());
            data.put(attrs[5], vendor.getFax());
            data.put(attrs[6], vendor.getWebsite());

            Address address = vendor.getAddress();
            if (address != null) {
                data.put(attrs[7], address.getAddress() == null ? "" : address.getAddress());
                data.put(attrs[8], address.getCity() == null ? "" : address.getCity());
                data.put(attrs[9], address.getState() == null ? "" : address.getState());
                data.put(attrs[10], address.getZipCode() == null ? "" : address.getZipCode());
            } else {
                data.put(attrs[7], "");
                data.put(attrs[8], "");
                data.put(attrs[9], "");
                data.put(attrs[10], "");
            }

            data.put(attrs[11], shop.getDefaultCountry());
            data.put(attrs[12], "");
            data.put(attrs[13], "");
            data.put(attrs[14], "");

            if (!addedVendors.contains(vendor.getId())) {
                report.add(data);
                addedVendors.add(vendor.getId());
            }
        }
        return report;
    }
}
