package com.fourtwenty.core.reporting.gather.impl.marketing;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created on 14/11/17 6:09 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class MarketingReportGatherer implements Gatherer {

    private MemberRepository memberRepository;
    private String[] attrs = new String[]{
            "First Name",
            "Last Name",
            "Membership Group",
            "Marketing Source",
            "Email",
            "Loyalty Points"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public MarketingReportGatherer(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }

    }


    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Marketing Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        final Iterable<Member> memberList = memberRepository.getActiveAndPendingMembers(filter.getCompanyId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        for (Member member : memberList) {
            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[0], member.getFirstName());
            data.put(attrs[1], member.getLastName());
            data.put(attrs[2], Objects.isNull(member.getMemberGroup()) ? StringUtils.EMPTY : member.getMemberGroup().getName());
            data.put(attrs[3], StringUtils.isBlank(member.getMarketingSource()) ? StringUtils.EMPTY : member.getMarketingSource());
            data.put(attrs[4], member.getEmail());
            data.put(attrs[5], member.getLoyaltyPoints());

            report.add(data);
        }

        return report;
    }
}
