package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 4/13/2016.
 */
public class ActiveMembersGatherer implements Gatherer {
    private String[] attrs = new String[]{"Active Memberships"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private MemberRepository memberRepository;
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ActiveMembersGatherer(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Active Member Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        long count = memberRepository.getActiveMemberCount(filter.getCompanyId());
        HashMap<String, Object> data = new HashMap<>(attrs.length);
        data.put(attrs[0], count);
        report.add(data);
        return report;
    }
}
