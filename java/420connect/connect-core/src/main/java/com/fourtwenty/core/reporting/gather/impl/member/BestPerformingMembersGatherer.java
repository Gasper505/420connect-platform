package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.DoctorRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByMember;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;

import java.util.*;

/**
 * Created by Stephen Schmidt on 5/10/2016.
 */
public class BestPerformingMembersGatherer implements Gatherer {
    private String[] attrs = new String[]{"Member Name", "Marketing Source", "Date Joined", "Loyalty Points", "Doctor", "Gross Receipts", "Total Transactions"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    @Inject
    private MemberRepository memberRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    ShopRepository shopRepository;

    public BestPerformingMembersGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Best Performing Members", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, Member> memberMap = memberRepository.listAllAsMap(filter.getCompanyId());

        List<SalesByMember> results = transactionRepository.getSalesByMember(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, Doctor> doctorHashMap = doctorRepository.listAsMap(filter.getCompanyId());
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());

        for (SalesByMember sbm : results) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            Member member = memberMap.get(sbm.getId());

            if (member != null) {
                String memberName = member.getFirstName() + " " + member.getLastName();
                Long startDate = member.getStartDate();
                String marketingSrc = member.getMarketingSource();
                String doctorName = "";
                if (member.getRecommendations() != null && member.getRecommendations().size() > 0) {
                    String docId = member.getRecommendations().get(0).getDoctorId();
                    Doctor doctor = doctorHashMap.get(docId);
                    if (doctor != null) {
                        doctorName = TextUtil.textOrEmpty(doctor.getFirstName()) + " " + TextUtil.textOrEmpty(doctor.getLastName());
                    }
                }

                data.put(attrs[0], memberName);
                data.put(attrs[1], TextUtil.textOrEmpty(marketingSrc));
                data.put(attrs[2], DateUtil.toDateFormatted(member.getStartDate(), shop.getTimeZone()));
                data.put(attrs[3], member.getLoyaltyPoints());
                data.put(attrs[4], doctorName);
                data.put(attrs[5], new DollarAmount(sbm.getTotal()));
                data.put(attrs[6], sbm.getCount());
                report.add(data);
            }
        }
        if (results.isEmpty()) {
            HashMap<String, Object> empty = new HashMap<>(reportHeaders.size());
            empty.put(attrs[0], "No Data Available");
            empty.put(attrs[1], "");
            empty.put(attrs[2], "");
            empty.put(attrs[3], "");
            report.add(empty);
        }

        return report;
    }
}
