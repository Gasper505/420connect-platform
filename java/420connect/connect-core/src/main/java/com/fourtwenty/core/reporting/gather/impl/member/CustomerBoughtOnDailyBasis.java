package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.*;

public class CustomerBoughtOnDailyBasis implements Gatherer {

    private String[] attrs = new String[]{"Product Name", "Category", "Cannabis", "Quantity Bought"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private MemberRepository memberRepository;
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private PrepackageRepository prepackageRepository;
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();


    public CustomerBoughtOnDailyBasis(MemberRepository repository, TransactionRepository transactionRepository, ProductRepository productRepository, PrepackageRepository prepackageRepository, PrepackageProductItemRepository prepackageProductItemRepository) {
        this.memberRepository = repository;
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.prepackageRepository = prepackageRepository;
        this.prepackageProductItemRepository = prepackageProductItemRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.BOOLEAN,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Customer Bought Products on Daily Basis Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);

        if (StringUtils.isNotBlank(filter.getMemberId())) {
            Member member = memberRepository.get(filter.getCompanyId(), filter.getMemberId());

            if (member == null) {
                throw new BlazeInvalidArgException("Member", "Member is not found");
            }
            Iterable<Transaction> results = transactionRepository.getBracketSalesByMemberId(filter.getCompanyId(), filter.getShopId(), filter.getMemberId(),
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

            List<ObjectId> productIds = new ArrayList<>();
            for (Transaction transaction : results) {
                if (transaction.getMemberId().equals(member.getId())) {
                    Cart cart = transaction.getCart();
                    for (OrderItem orderItem : cart.getItems()) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }

            HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
            HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());


            HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), productIds);

            HashMap<String, ProductDailyBasis> productDailyBasisHashMap = new HashMap<>();

            HashSet<String> productDailyBasisKeys = new HashSet<>();

            HashSet<String> quantityMapKeys = new HashSet<>();

            ProductDailyBasis productDailyBasis;

            for (Transaction transaction : results) {
                if (transaction.getMemberId().equals(member.getId())) {
                    Cart cart = transaction.getCart();
                    List<OrderItem> items = cart.getItems();

                    for (OrderItem item : items) {
                        if (item.getProductId() != null) {

                            if (productDailyBasisHashMap.containsKey(item.getProductId())) {
                                productDailyBasis = productDailyBasisHashMap.get(item.getProductId());

                                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                                if (prepackageProductItem != null) {
                                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());

                                    if (productDailyBasis.quantityMap.containsKey(prepackage.getId())) {
                                        BigDecimal quantity = productDailyBasis.quantityMap.get(prepackage.getId());
                                        productDailyBasis.quantityMap.put(prepackage.getId(), quantity.add(item.getQuantity()));
                                    } else {
                                        productDailyBasis.quantityMap.put(prepackage.getId(), item.getQuantity());
                                        quantityMapKeys.add(prepackage.getId());
                                    }
                                } else {
                                    productDailyBasis.quantity = productDailyBasis.quantity.add(item.getQuantity());
                                }
                            } else {
                                productDailyBasis = new ProductDailyBasis();
                                Product product = productHashMap.get(item.getProductId());
                                productDailyBasis.productName = product.getName();
                                productDailyBasis.categoryName = product.getCategory().getName();

                                if ((product.getCannabisType() == Product.CannabisType.DEFAULT && product.getCategory().isCannabis())
                                        || (product.getCannabisType() != Product.CannabisType.CBD
                                        && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                                        && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                                    productDailyBasis.cannabis = "Yes";
                                } else {
                                    productDailyBasis.cannabis = "No";
                                }

                                if (item.getPrepackageItemId() != null) {
                                    PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                    productDailyBasis.quantityMap.put(prepackage.getId(), item.getQuantity());
                                    quantityMapKeys.add(prepackage.getId());

                                } else {
                                    productDailyBasis.quantity = item.getQuantity();
                                }
                                productDailyBasisHashMap.put(item.getProductId(), productDailyBasis);
                                productDailyBasisKeys.add(product.getId());
                            }
                        }
                    }
                }
            }

            for (String productDailyBasisKey : productDailyBasisKeys) {
                HashMap<String, Object> data = new HashMap<>();
                ProductDailyBasis productDailyBasisDetails = productDailyBasisHashMap.get(productDailyBasisKey);
                data.put(attrs[0], productDailyBasisDetails.productName);
                data.put(attrs[1], productDailyBasisDetails.categoryName);
                data.put(attrs[2], productDailyBasisDetails.cannabis);

                Map<String, BigDecimal> quantityMap = productDailyBasisDetails.quantityMap;
                String value = StringUtils.EMPTY;

                if (productDailyBasisDetails.quantity.doubleValue() > 0) {
                    double val = productDailyBasisDetails.quantity.doubleValue();

                    value = Double.toString(val).concat(" grams, ");
                    for (String quantityMapKey : quantityMapKeys) {
                        Prepackage prepackage = prepackageHashMap.get(quantityMapKey);
                        if (quantityMap.containsKey(quantityMapKey)) {
                            double v = quantityMap.get(quantityMapKey).doubleValue();
                            value = value.concat(Double.toString(v).concat(" ").concat(prepackage.getName() + ", "));
                        }
                    }
                    data.put(attrs[3], value.contains(",") ? value.substring(0, value.lastIndexOf(",")) : value);

                } else {
                    for (String quantityMapKey : quantityMapKeys) {
                        Prepackage prepackage = prepackageHashMap.get(quantityMapKey);
                        if (quantityMap.containsKey(quantityMapKey)) {
                            double v = quantityMap.get(quantityMapKey).doubleValue();
                            value = value.concat(Double.toString(v).concat(" ").concat(prepackage.getName() + ", "));
                        }
                    }
                    data.put(attrs[3], value.contains(",") ? value.substring(0, value.lastIndexOf(",")) : value);
                }
                report.add(data);
            }
        }
        return report;
    }

    private class ProductDailyBasis {
        String productName;
        String categoryName;
        String cannabis;
        //<prepackgeId, quantity>
        Map<String, BigDecimal> quantityMap = new HashMap<>();
        BigDecimal quantity = new BigDecimal(0);

        public ProductDailyBasis() {

        }
    }
}
