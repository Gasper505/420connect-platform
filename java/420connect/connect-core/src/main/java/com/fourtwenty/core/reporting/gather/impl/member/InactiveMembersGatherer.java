package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByMember;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;

import java.util.*;

/**
 * Created by Gaurav Saini on 8/5/17.
 */
public class InactiveMembersGatherer implements Gatherer {
    private MemberRepository membershipRepository;
    private String[] attrs = new String[]{"First Name", "Last Name", "Cell Phone", "Email", "Last Visit", "Rec. Exp. Date", "Total Amount Spent"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private TransactionRepository transactionRepository;

    public InactiveMembersGatherer(MemberRepository repository, TransactionRepository transactionRepository) {
        this.membershipRepository = repository;
        this.transactionRepository = transactionRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    public GathererReport gather(ReportFilter filter) {
        DateTime dateTime = DateTime.now();
        GathererReport report = new GathererReport(filter, "Inactive Members", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        Long date = dateTime.getMillis();

        List<Member> results = membershipRepository.getInactiveMembers(filter.getCompanyId(), date);
        HashSet<String> addedMembers = new HashSet<>();
        for (Member member : results) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());

            data.put(attrs[0], member.getFirstName());
            data.put(attrs[1], member.getLastName());
            data.put(attrs[2], member.getPrimaryPhone());
            data.put(attrs[3], member.getEmail());
            data.put(attrs[4], ProcessorUtil.timeStampWithOffset(member.getLastVisitDate(), filter.getTimezoneOffset()));

            Long recDate = null;
            if (!member.getRecommendations().isEmpty()) {
                recDate = member.getRecommendations().get(0).getExpirationDate();
                if (recDate != null && recDate == 0) {
                    recDate = null;
                }
            }

            data.put(attrs[5], ProcessorUtil.dateStringWithOffset(recDate, filter.getTimezoneOffset()));


            List<SalesByMember> transactionResults = transactionRepository.getTotalSalesByMember(filter.getCompanyId(), filter.getShopId(), member.getId());

            if (transactionResults.size() > 0) {
                SalesByMember salesByMember = transactionResults.get(0);
                data.put(attrs[6], salesByMember.getTotal());
            } else {
                data.put(attrs[6], 0f);
            }

            if (!addedMembers.contains(member.getId())) {
                report.add(data);
                addedMembers.add(member.getId());
            }
        }
        return report;
    }
}