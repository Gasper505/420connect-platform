package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.MemberPerformance;
import com.fourtwenty.core.reporting.model.reportmodels.MemberPerformanceBySale;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;

import javax.inject.Inject;
import java.util.*;

public class MemberPerformanceGatherer implements Gatherer {

    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private TransactionRepository transactionRepository;

    private String[] attrs = new String[]{
            "MemberId",
            "Member Name",
            "Member Phone",
            "Marketing Src",
            "Member Group",
            "Date Joined",
            "Consumer Type",
            "Loyalty Points",
            "State",
            "Zip Code",
            "Last Visited",
            "# of Visits",
            "# of Sales",
            "# of Refunds",
            "Gross Sales Receipts",
            "Gross Refund Receipts",
            "Avg Sales Receipts",
            "Avg Refund Receipts"
    };

    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public MemberPerformanceGatherer() {
        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, //Member ID  0
                GathererReport.FieldType.STRING, //Member Name  1
                GathererReport.FieldType.STRING, //Member Phone    1
                GathererReport.FieldType.STRING, //Marketing Src    1
                GathererReport.FieldType.STRING, //Member Group 2
                GathererReport.FieldType.STRING, //Date Joined  3
                GathererReport.FieldType.STRING, //Consumer Type    4
                GathererReport.FieldType.NUMBER, //Loyalty Points   5
                GathererReport.FieldType.STRING, //State    6
                GathererReport.FieldType.STRING, //Zip Code 7
                GathererReport.FieldType.STRING, //Last Visited 8
                GathererReport.FieldType.NUMBER, //# of Visits  9
                GathererReport.FieldType.NUMBER, //# of Sales   10
                GathererReport.FieldType.NUMBER, //# of Refunds 11
                GathererReport.FieldType.CURRENCY, //# Gross Sales Receipts 12
                GathererReport.FieldType.CURRENCY, //# Gross Refund Receipts    13
                GathererReport.FieldType.CURRENCY, //# Avg Sales Receipts   14
                GathererReport.FieldType.CURRENCY, //# Avg Refund Receipt   15
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        GathererReport report = new GathererReport(filter, "Member Performing Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<MemberPerformance> members = memberRepository.getMembersForBestPerformance(filter.getCompanyId());

        Map<String, MemberPerformance> memberMap = new HashMap<>();
        for (MemberPerformance member : members) {
            memberMap.put(member.getId(), member);
        }

        Iterable<MemberPerformanceBySale> memberPerformanceResult = transactionRepository.getSalesByMemberPerformance(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, MemberGroup> memberGroupMap = memberGroupRepository.listAllAsMap(filter.getCompanyId());


        // New solution
        Map<String, MemberPerfInfo> memberPerfInfoMap = new HashMap<>();
        for (MemberPerformanceBySale sale : memberPerformanceResult) {
            MemberPerformance member = memberMap.get(sale.getMemberId());
            if (member == null) {
                continue;
            }


            MemberPerfInfo memberPerfInfo = memberPerfInfoMap.get(sale.getMemberId());
            if (memberPerfInfo == null) {
                memberPerfInfo = new MemberPerfInfo();
                memberPerfInfo.memberId = member.getId();
                memberPerfInfo.member = member;
                memberPerfInfoMap.put(sale.getMemberId(), memberPerfInfo);
            }


            memberPerfInfo.visit++;

            if (Transaction.TransactionStatus.Completed == sale.getStatus()) {
                memberPerfInfo.salesCount += 1;
                memberPerfInfo.sales += sale.getCart().getTotal() != null ? sale.getCart().getTotal().doubleValue() : 0d;
            }

            if (Transaction.TransactionStatus.RefundWithoutInventory == sale.getStatus() ||
                    Transaction.TransactionStatus.RefundWithInventory == sale.getStatus()) {
                memberPerfInfo.refundCount += 1;
                memberPerfInfo.refundAmount += sale.getCart().getTotal() != null ? sale.getCart().getTotal().doubleValue() : 0d;
            }
        }

        List<MemberPerfInfo> resultList = Lists.newArrayList(memberPerfInfoMap.values());
        resultList.sort((o1, o2) -> o1.member.getFirstName().compareTo(o2.member.getFirstName()));


        String notAvailable = "N/A";
        for (MemberPerfInfo info : resultList) {
            HashMap rowMap = new HashMap<>();
            MemberGroup memberGroup = memberGroupMap.get(info.member.getMemberGroupId());

            rowMap.put(attrs[0], info.member.getId());
            rowMap.put(attrs[1], info.member.getFirstName() + " " + info.member.getLastName());
            rowMap.put(attrs[2], TextUtil.textOrEmpty(info.member.getPrimaryPhone()));
            rowMap.put(attrs[3], StringUtils.isBlank(info.member.getMarketingSource()) ? notAvailable : info.member.getMarketingSource());
            rowMap.put(attrs[4], memberGroup != null ? memberGroup.getName() : notAvailable);
            rowMap.put(attrs[5], ProcessorUtil.dateStringWithOffset(info.member.getCreated(), filter.getTimezoneOffset()));
            rowMap.put(attrs[6], info.member.getConsumerType());
            rowMap.put(attrs[7], NumberUtils.round(info.member.getLoyaltyPoints(), 2));
            rowMap.put(attrs[8], info.member.getAddress() != null ? (StringUtils.isNotBlank(info.member.getAddress().getState()) ? info.member.getAddress().getState() : notAvailable) : notAvailable);
            rowMap.put(attrs[9], info.member.getAddress() != null ? (StringUtils.isNotBlank(info.member.getAddress().getZipCode()) ? info.member.getAddress().getZipCode() : notAvailable) : notAvailable);
            rowMap.put(attrs[10], (info.member.getLastVisitDate() == null || info.member.getLastVisitDate() == 0) ? notAvailable : ProcessorUtil.dateStringWithOffset(info.member.getLastVisitDate(), filter.getTimezoneOffset()));
            rowMap.put(attrs[11], info.visit);
            rowMap.put(attrs[12], info.salesCount);
            rowMap.put(attrs[13], info.refundCount);
            rowMap.put(attrs[14], new DollarAmount(NumberUtils.round(info.sales, 2)));
            rowMap.put(attrs[15], new DollarAmount(NumberUtils.round(info.refundAmount, 2)));
            rowMap.put(attrs[16], NumberUtils.round(info.getAvgSaleReceipt(), 2));
            rowMap.put(attrs[17], NumberUtils.round(info.getAvgRefundReceipt(), 2));

            report.add(rowMap);
        }


        return report;
    }

    class MemberPerfInfo {
        String memberId = "";
        MemberPerformance member;
        int visit = 0;
        int salesCount = 0;
        int refundCount = 0;
        double sales = 0;
        double refundAmount = 0d;

        public double getAvgRefundReceipt() {
            if (refundCount > 0) {
                return refundAmount / refundCount;
            }
            return 0;
        }

        public double getAvgSaleReceipt() {
            if (salesCount > 0) {
                return sales / salesCount;
            }
            return 0;
        }

    }

}
