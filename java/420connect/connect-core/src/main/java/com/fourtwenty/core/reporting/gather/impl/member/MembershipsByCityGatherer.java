package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByField;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 5/2/2016.
 */
public class MembershipsByCityGatherer implements Gatherer {
    private MemberRepository memberRepository;
    private String[] attrs = new String[]{"City", "Memberships"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public MembershipsByCityGatherer(MemberRepository repository) {
        this.memberRepository = repository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Memberships By City", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        ArrayList<MemberByField> results = memberRepository.getMembershipsByCity(filter.getCompanyId());

        if (results == null) {
            HashMap<String, Object> empty = new HashMap<>(1);
            empty.put(attrs[0], "The report returned no results");
            report.add(empty);
            return report;
        }
        for (MemberByField m : results) {
            HashMap<String, Object> data = new HashMap<>(attrs.length);
            data.put(attrs[0], m.get_id());
            data.put(attrs[1], m.getCount());
            report.add(data);
        }
        return report;

    }
}
