package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.repositories.dispensary.DoctorRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByField;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 4/27/2016.
 */
public class MembershipsByDoctorGatherer implements Gatherer {
    private MemberRepository memberRepository;
    private DoctorRepository doctorRepository;
    private String[] attrs = new String[]{"Doctor", "Memberships"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public MembershipsByDoctorGatherer(MemberRepository memberRepository, DoctorRepository doctorRepository) {
        this.memberRepository = memberRepository;
        this.doctorRepository = doctorRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Memberships By Doctor", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        Iterable<Doctor> doctors = doctorRepository.listAll(filter.getCompanyId());
        HashMap<String, String> doctorIdMap = new HashMap<>();
        for (Doctor d : doctors) {
            doctorIdMap.put(d.getId(), d.getFirstName() + " " + d.getLastName());
        }
        ArrayList<MemberByField> results = memberRepository.getMemberCountByDoctor(filter.getCompanyId());
        if (results == null) {
            HashMap<String, Object> empty = new HashMap<>(1);
            empty.put(attrs[0], "The report returned no results");
            empty.put(attrs[1], "");
            report.add(empty);
            return report;
        }
        for (MemberByField m : results) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            data.put(attrs[0], doctorIdMap.get(m.get_id()));
            data.put(attrs[1], m.getCount());
            report.add(data);
        }
        return report;
    }
}
