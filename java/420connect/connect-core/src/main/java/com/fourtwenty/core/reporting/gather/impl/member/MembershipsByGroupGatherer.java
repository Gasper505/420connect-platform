package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByFieldExtended;
import com.fourtwenty.core.reporting.model.reportmodels.MemberReportView;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Stephen Schmidt on 4/26/2016.
 */
public class MembershipsByGroupGatherer implements Gatherer {
    private String[] attrs = new String[]{"Date", "Member Group", "Revenue", "Deliveries", "Walk-Ins", "Specials", "New Members", "Returning Members", "Adult Use", "MMIC", "Third-Party", "Total Members"};
    private List<String> reportHeaders = new ArrayList<>(attrs.length);
    private MemberGroupRepository groupRepository;
    private MemberRepository memberRepository;
    private TransactionRepository transactionRepository;
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public MembershipsByGroupGatherer(MemberRepository repository, MemberGroupRepository groupRepository, TransactionRepository transactionRepository) {
        this.memberRepository = repository;
        this.groupRepository = groupRepository;
        this.transactionRepository = transactionRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[] {
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.NUMBER
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Membership By Group Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        Iterable<MemberByFieldExtended> results = memberRepository.getMembersByGroup(filter.getCompanyId());
        HashMap<String, MemberGroup> groupMap;
        if (filter.getCompanyMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            groupMap = groupRepository.listAllAsMap(filter.getCompanyId());
        } else {
            groupMap = groupRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        }
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, List<Transaction>> memberToTransactions = new HashMap<>();
        for (Transaction transaction : transactions) {
            List<Transaction> transactionList = memberToTransactions.get(transaction.getMemberId());
            if (transactionList == null) {
                transactionList = new ArrayList<>();
                memberToTransactions.put(transaction.getMemberId(), transactionList);
            }
            transactionList.add(transaction);
        }

        for (MemberByFieldExtended memberByField : results) {
            if (memberByField != null && memberByField.get_id() != null && memberByField.getMembers() != null) {
                // iterate members in memberByField
                BigDecimal revenue = new BigDecimal(0);
                int deliveries = 0;
                int walkIns = 0;
                int specials = 0;
                int newMembers = 0;
                int returnedMembers = 0;
                int adultUse = 0;
                int mmic = 0;
                int thirdParty = 0;
                for (MemberReportView member : memberByField.getMembers()) {

                    // iterate transactions to calculate revenue & deliveries
                    List<Transaction> memberTransactions = memberToTransactions.get(member.getId().toString());
                    if (memberTransactions != null) {
                        for (Transaction transaction : memberTransactions) {
                            if (transaction.getMemberId().equalsIgnoreCase(member.getId().toString())) {
                                Cart cart = transaction.getCart();
                                if (cart != null) {
                                    revenue = revenue.add(cart.getTotal());
                                }
                                if (transaction.getQueueType() == Transaction.QueueType.Delivery) {
                                    deliveries += 1;
                                } else if (transaction.getQueueType() == Transaction.QueueType.WalkIn) {
                                    walkIns += 1;
                                } else if (transaction.getQueueType() == Transaction.QueueType.Special) {
                                    specials += 1;
                                }
                            }
                        }

                        if (memberTransactions.size() > 0) {
                            // calculate new members
                            if (member.getCreated() >= filter.getTimeZoneStartDateMillis() && member.getCreated() <= filter.getTimeZoneEndDateMillis()) {
                                newMembers += 1;
                            }
                            // calculate returned members
                            if (member.getCreated() < filter.getTimeZoneStartDateMillis()) {
                                returnedMembers += 1;
                            }
                        }
                    }
                    if (member.getConsumerType() == ConsumerType.AdultUse) {
                        adultUse++;
                    } else if (member.getConsumerType() == ConsumerType.MedicinalState) {
                        mmic++;
                    } else if (member.getConsumerType() == ConsumerType.MedicinalThirdParty) {
                        thirdParty++;
                    }


                }

                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], ProcessorUtil.dateStringWithOffset(groupMap.get(memberByField.get_id()).getCreated(), filter.getTimezoneOffset()));
                data.put(attrs[1], groupMap.get(memberByField.get_id()).getName());
                data.put(attrs[2], new DollarAmount(revenue.doubleValue()));
                data.put(attrs[3], deliveries);
                data.put(attrs[4], walkIns);
                data.put(attrs[5], specials);
                data.put(attrs[6], newMembers);
                data.put(attrs[7], returnedMembers);
                data.put(attrs[8], adultUse);
                data.put(attrs[9], mmic);
                data.put(attrs[10], thirdParty);
                data.put(attrs[11], memberByField.getCount());
                report.add(data);
            }
        }
        return report;
    }
}
