package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;

import java.util.*;

/**
 * Created by Stephen Schmidt on 5/7/2016.
 */
public class MembershipsExpiringSoonGatherer implements Gatherer {
    private MemberRepository membershipRepository;
    private String[] attrs = new String[]{"First Name", "Last Name", "ID Exp. Date", "Rec. Exp. Date"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public MembershipsExpiringSoonGatherer(MemberRepository repository) {
        this.membershipRepository = repository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING, GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        DateTime d = DateTime.now();
        GathererReport report = new GathererReport(filter, "Members Expiring Soon", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);
        Long date = d.getMillis();
        Iterable<Member> results = null; //

        if (filter.getCompanyMembersShareOption() == Company.CompanyMembersShareOption.Isolated) {
            results = membershipRepository.getMembershipsExpiringSoon(filter.getCompanyId(), filter.getShopId(), date);
        } else {
            results = membershipRepository.getMembershipsExpiringSoon(filter.getCompanyId(), date);
        }

        HashSet<String> addedMembers = new HashSet<>();
        for (Member m : results) {

            long recDate = 0;
            long identDate = 0;
            if (m.getRecommendations().size() > 0) {
                Recommendation rec = m.getRecommendations().get(0);
                recDate = rec.getExpirationDate() != null ? rec.getExpirationDate() : 0;
            }

            if (m.getIdentifications().size() > 0) {
                Identification dl = m.getIdentifications().get(0);
                identDate = dl.getExpirationDate() != null ? dl.getExpirationDate() : 0;
            }

            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            data.put(attrs[0], m.getFirstName());
            data.put(attrs[1], m.getLastName());

            if (!m.getIdentifications().isEmpty() && identDate > 0) {
                data.put(attrs[2], ProcessorUtil.dateString(identDate));
            } else {
                data.put(attrs[2], "No Date");
            }
            if (!m.getRecommendations().isEmpty() && recDate > 0) {
                data.put(attrs[3], ProcessorUtil.dateString(recDate));
            } else {
                data.put(attrs[3], "No Date");
            }
            //add the report row if we haven't already added said member to the list.
            if (!addedMembers.contains(m.getId())) {
                report.add(data);
                addedMembers.add(m.getId());
            }

        }

        return report;
    }
}
