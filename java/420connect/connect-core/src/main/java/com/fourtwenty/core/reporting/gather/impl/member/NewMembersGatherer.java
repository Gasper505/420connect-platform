package com.fourtwenty.core.reporting.gather.impl.member;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.NewMemberTransactions;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Stephen Schmidt on 5/12/2016.
 */
public class NewMembersGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;

    private String[] attrs = new String[]{"Member Id", "First Name", "Last Name", "Phone Number", "Email", "Member Group", "Status", "Marketing source", "Date Joined", "Total Transactions",
    "Text Opt In", "Email Opt In"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public NewMembersGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "New Members Report", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, MemberGroup> memberGroupHashMap;
        Iterable<Member> members = null;

        if (filter.getCompanyMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            memberGroupHashMap = memberGroupRepository.listAllAsMap(filter.getCompanyId());
            members = memberRepository.getMembersWithStartDate(filter.getCompanyId(),
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        } else {
            memberGroupHashMap = memberGroupRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
            members = memberRepository.getMembersWithStartDate(filter.getCompanyId(), filter.getShopId(),
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        }


        List<Member> memberList = Lists.newArrayList(members); // List for later
        Set<String> memberIds = new HashSet<>(); //List for the query on the transaction repository
        for (Member member : members) {
            memberIds.add(member.getId());
        }

        //Build a map of the transactions for each member ID
        Map<String, NewMemberTransactions> nmtMap = new HashMap<>();
        Iterable<NewMemberTransactions> results = transactionRepository.getCompletedTransactionStatsByMemberIds(filter.getCompanyId(), filter.getShopId(), memberIds);
        for (NewMemberTransactions nmt : results) {
            nmtMap.put(nmt.getId(), nmt);
        }

        for (Member m : memberList) {
            //make sure members are active and not pending/inactive
            if (m.getStatus() != Member.MembershipStatus.Inactive) {
                HashMap<String, Object> rowMap = new HashMap<>();
                String fn = m.getFirstName() != null ? m.getFirstName() : "";
                String ln = m.getLastName() != null ? m.getLastName() : "";
                MemberGroup memberGroup = memberGroupHashMap.get(m.getMemberGroupId());
                String memberGroupName = "";
                if (memberGroup != null) {
                    memberGroupName = memberGroup.getName();
                }
                String source = m.getMarketingSource() != null ? m.getMarketingSource() : "";

                rowMap.put(attrs[0], m.getId());
                rowMap.put(attrs[1], fn);
                rowMap.put(attrs[2], ln);
                rowMap.put(attrs[3], TextUtil.textOrEmpty(m.getPrimaryPhone()));
                rowMap.put(attrs[4], TextUtil.textOrEmpty(m.getEmail()));
                rowMap.put(attrs[5], memberGroupName);
                rowMap.put(attrs[6], m.getStatus());
                rowMap.put(attrs[7], source);
                rowMap.put(attrs[8], new DateTime(m.getStartDate()).minusMinutes(filter.getTimezoneOffset()).toString().substring(0, 10));
                if (nmtMap.containsKey(m.getId())) {
                    rowMap.put(attrs[9], nmtMap.get(m.getId()).getCount());
                } else {
                    rowMap.put(attrs[9], 0);
                }
                rowMap.put(attrs[10], m.isTextOptIn());
                rowMap.put(attrs[11], m.isEmailOptIn());
                report.add(rowMap);
            }
        }
        if (memberList.isEmpty()) {
            HashMap<String, Object> rowMap = new HashMap<>();
            rowMap.put(attrs[0], "No Data Available");
            rowMap.put(attrs[1], "");
            rowMap.put(attrs[2], "");
            rowMap.put(attrs[3], "");
            rowMap.put(attrs[4], "");
            rowMap.put(attrs[5], "");
            rowMap.put(attrs[6], "");
            rowMap.put(attrs[7], "");
            rowMap.put(attrs[8], "");
            rowMap.put(attrs[9], "");
            rowMap.put(attrs[10], "");
            rowMap.put(attrs[11], "");
            report.add(rowMap);
        }

        return report;
    }
}
