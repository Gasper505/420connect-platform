package com.fourtwenty.core.reporting.gather.impl.member;


import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.*;

public class SalesByMembershipGroupGatherer implements Gatherer {
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private String[] attrs = new String[]{"Member Group", "Gross Receipts"};
    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private MemberGroupRepository groupRepository;

    public SalesByMembershipGroupGatherer(TransactionRepository transactionRepository, MemberRepository memberRepository, MemberGroupRepository groupRepository) {
        this.transactionRepository = transactionRepository;
        this.memberRepository = memberRepository;
        this.groupRepository = groupRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Membership Group", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, MemberGroup> groupHashMap;
        if (filter.getCompanyMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            groupHashMap = groupRepository.listAsMap(filter.getCompanyId());
        } else {
            groupHashMap = groupRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        }
        HashMap<String, Double> salesByGroup = new HashMap<>();


        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));

        int factor = 1;
        for (Transaction transaction : transactions) {

            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Member member = memberHashMap.get(transaction.getMemberId());

            double totalSales = transaction.getCart().getTotal() == null ? 0D : transaction.getCart().getTotal().doubleValue();

            if (transaction.getTransType() == Transaction.TransactionType.Refund
                    && transaction.getCart().getRefundOption() == Cart.RefundOption.Void
                    && transaction.getCart().getSubTotal().doubleValue() == 0) {
                totalSales = 0;
            }
            totalSales = totalSales * factor;

            if (member != null && groupHashMap.containsKey(member.getMemberGroupId())) {
                MemberGroup memberGroup = groupHashMap.get(member.getMemberGroupId());

                if (salesByGroup.get(memberGroup.getId()) != null) {
                    totalSales += salesByGroup.get(memberGroup.getId());
                }

                salesByGroup.put(memberGroup.getId(), totalSales);

            }
        }

        Set<String> groupKeys = salesByGroup.keySet();
        for (String key : groupKeys) {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], groupHashMap.get(key).getName());
            data.put(attrs[1], new DollarAmount(salesByGroup.get(key)));
            report.add(data);
        }

        return report;
    }
}
