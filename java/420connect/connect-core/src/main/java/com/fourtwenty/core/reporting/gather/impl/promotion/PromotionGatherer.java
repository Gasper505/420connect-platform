package com.fourtwenty.core.reporting.gather.impl.promotion;

import com.fourtwenty.core.domain.models.loyalty.PromoUsage;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.PromoUsageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Gaurav Saini on 8/11/17.
 */
public class PromotionGatherer implements Gatherer {

    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private PromoUsageRepository promoUsageRepository;
    @Inject
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Promo Name", "Promo Code", "Promotion Type", "Usage Count", "Total Promotion Value"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public PromotionGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY};

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Promotion", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);

        Iterable<PromoUsage> promoUsages = promoUsageRepository.listByShopWithDates(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        LinkedHashSet<ObjectId> promoIds = new LinkedHashSet<>();
        Map<String, Integer> countMap = new HashMap<>();

        List<ObjectId> transIds = new ArrayList<>();
        for (PromoUsage promoUsage : promoUsages) {
            promoIds.add(new ObjectId(promoUsage.getPromoId()));

            // add up the promoUsage for this timeframe
            Integer count = countMap.get(promoUsage.getPromoId());
            if (count == null || count == 0) {
                count = promoUsage.getCount();
            } else {
                count += promoUsage.getCount();
            }
            countMap.put(promoUsage.getPromoId(), count);

            /// Add the transId to the list to be searched
            if (StringUtils.isNotBlank(promoUsage.getTransId()) && ObjectId.isValid(promoUsage.getTransId())) {
                transIds.add(new ObjectId(promoUsage.getTransId()));
            }
        }

        // Retrieve all related transactions from the database based on the transId.
        HashMap<String, Transaction> transactionHashMap = transactionRepository.listAsMap(filter.getCompanyId(), transIds);

        // Store the transaction into promoId -> List<Transactions>
        HashMap<String, List<Transaction>> transListToPromo = new HashMap<>();
        for (PromoUsage promoUsage : promoUsages) {
            List<Transaction> items = transListToPromo.get(promoUsage.getPromoId());
            if (items == null) {
                items = new ArrayList<>();
                transListToPromo.put(promoUsage.getPromoId(), items);
            }
            Transaction transaction = transactionHashMap.get(promoUsage.getTransId());
            if (transaction != null) {
                items.add(transaction);
            }

        }

        Iterable<Promotion> promotions = promotionRepository.findItemsIn(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(promoIds));
        for (Promotion promotion : promotions) {
            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
            data.put(attrs[0], promotion.getName());
            data.put(attrs[1], promotion.getPromoCodes() != null && !promotion.getPromoCodes().isEmpty() ? StringUtils.join(promotion.getPromoCodes(), ',') : "");
            data.put(attrs[2], promotion.getPromotionType().name());
            data.put(attrs[3], countMap.get(promotion.getId()));

            BigDecimal totalPromoValue = BigDecimal.ZERO;
            List<Transaction> transactions = transListToPromo.get(promotion.getId());
            for (Transaction transaction : transactions) {
                LinkedHashSet<PromotionReqLog> promotionReqLogs = transaction.getCart().getPromotionReqLogs();
                boolean found = false;
                for (PromotionReqLog promotionReqLog : promotionReqLogs) {
                    // Only add up matching promotions. Ignore reward type requests
                    if (promotionReqLog.getPromotionId() != null && promotionReqLog.getPromotionId().equalsIgnoreCase(promotion.getId())) {
                        BigDecimal amount = promotionReqLog.getAmount();
                        totalPromoValue = totalPromoValue.add(amount);
                        found = true;
                    }
                }

                if (promotion.getPromotionType() == Promotion.PromotionType.Product && !found) {
                    // if not found, then let's just calculate this value
                    if (promotion.getDiscountType() == OrderItem.DiscountType.Cash) {
                        BigDecimal amount = promotion.getDiscountAmt();
                        totalPromoValue = totalPromoValue.add(amount);
                    } else {
                        // it's percentage based, so let's just calculate it
                        double totalProductDiscount = 0d;
                        for (OrderItem orderItem : transaction.getCart().getItems()) {
                            boolean oFound = false;
                            for (PromotionReq req : orderItem.getPromotionReqs()) {
                                if (req.getPromotionId().equals(promotion.getId())) {
                                    oFound = true;
                                    break;
                                }
                            }

                            if (oFound) {
                                // let's just calculate it
                                double discount = orderItem.getCost().doubleValue() * promotion.getDiscountAmt().doubleValue() / 100;
                                if (promotion.getMaxCashValue().doubleValue() > 0 && discount > promotion.getMaxCashValue().doubleValue()) {
                                    discount = promotion.getMaxCashValue().doubleValue();
                                }

                                totalProductDiscount += discount;
                            }
                        }

                        totalPromoValue = totalPromoValue.add(BigDecimal.valueOf(totalProductDiscount));
                    }
                }

            }
            data.put(attrs[4], new DollarAmount(NumberUtils.round(totalPromoValue.doubleValue(), 2)));
            report.add(data);
        }
        return report;
    }
}