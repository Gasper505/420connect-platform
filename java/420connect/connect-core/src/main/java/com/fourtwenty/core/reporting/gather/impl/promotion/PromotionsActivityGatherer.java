package com.fourtwenty.core.reporting.gather.impl.promotion;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * Created by mdo on 12/11/17.
 */
public class PromotionsActivityGatherer implements Gatherer {
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    PromotionRepository promotionRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    LoyaltyRewardRepository loyaltyRewardRepository;
    @Inject
    ConsumerCartRepository consumerCartRepository;

    private String[] attrs = new String[]{"Date", "Transaction No", "Promo/Reward Name", "Type", "Code Used", "Cash Value", "Employee", "Member"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public PromotionsActivityGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[] {
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Promotion/Reward Activity", reportHeaders);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fieldTypes);


        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        List<ObjectId> memberIds = new ArrayList<>();
        List<ObjectId> consumerCartIds = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (StringUtils.isNotBlank(transaction.getConsumerCartId())
                    && ObjectId.isValid(transaction.getConsumerCartId())) {
                consumerCartIds.add(new ObjectId(transaction.getConsumerCartId()));
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), memberIds);
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Promotion> promotionHashMap = promotionRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, LoyaltyReward> loyaltyRewardHashMap = loyaltyRewardRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ConsumerCart> cartHashMap = consumerCartRepository.listAsMap(filter.getCompanyId(), consumerCartIds);


        for (Transaction transaction : transactions) {
            if (transaction.getCart().getPromotionReqLogs().size() > 0) {
                Member member = memberHashMap.get(transaction.getMemberId());
                Employee employee = employeeHashMap.get(transaction.getSellerId());

                ConsumerCart consumerCart = cartHashMap.get(transaction.getConsumerCartId());

                for (PromotionReqLog reqLog : transaction.getCart().getPromotionReqLogs()) {
                    String promoName = null;
                    String type = "";
                    String promoCode = "";
                    if (StringUtils.isNotBlank(reqLog.getPromotionId())) {
                        //Promotions
                        Promotion promotion = promotionHashMap.get(reqLog.getPromotionId());
                        if (promotion == null) {
                            promotion = promotionHashMap.get(reqLog.getCompanyId()); // DO THIS FOR PREVIOUS BUG WHERE COMPANYID WAS SET AS PROMOID
                        }
                        if (promotion != null) {
                            promoName = promotion.getName();
                            type = "Promotion";


                            if (consumerCart != null && consumerCart.getCart() != null) {
                                String cCode = consumerCart.getCart().getPromoCode();
                                if (StringUtils.isNotBlank(cCode)) {
                                    if (promotion.getPromoCodes().contains(cCode.trim())) {
                                        promoCode = cCode.trim();
                                    }
                                }
                            }
                        }

                    } else if (StringUtils.isNotBlank(reqLog.getRewardId())) {
                        // Rewards
                        LoyaltyReward reward = loyaltyRewardHashMap.get(reqLog.getRewardId());
                        if (reward != null) {
                            promoName = reward.getName();
                            type = "Reward";
                        }
                    }

                    if (promoName != null) {
                        // promotion was found so now we can do something

                        HashMap<String, Object> data = new HashMap<>();
                        data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(transaction.getProcessedTime(), filter.getTimezoneOffset()));
                        data.put(attrs[1], transaction.getTransNo());
                        data.put(attrs[2], promoName);
                        data.put(attrs[3], type);
                        data.put(attrs[4], promoCode);
                        data.put(attrs[5], new DollarAmount(reqLog.getAmount()));
                        data.put(attrs[6], TextUtil.textOrEmpty(employee.getFirstName()) + " " + TextUtil.textOrEmpty(employee.getLastName()));
                        data.put(attrs[7], TextUtil.textOrEmpty(member.getFirstName()) + " " + TextUtil.textOrEmpty(member.getLastName()));


                        report.add(data);
                    }
                }
            }
        }

        return report;
    }
}
