//package com.fourtwenty.core.reporting.gather.impl.quickbook;
//
//import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
//import com.fourtwenty.quickbook.repositories.QuickbookSyncDetailsRepository;
//import com.fourtwenty.core.reporting.gather.Gatherer;
//import com.fourtwenty.core.reporting.model.GathererReport;
//import com.fourtwenty.core.reporting.model.ReportFilter;
//import com.fourtwenty.core.reporting.processing.ProcessorUtil;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.Map;
//
//public class QBSyncReportsDetail implements Gatherer {
//    private QuickbookSyncDetailsRepository quickbookSyncDetailsRepository;
//    private String[] attrs = new String[]{"Last Sync Date", "Quickbook Entity", "Total Record", "Total Success", "Total Fail"};
//    private ArrayList<String> reportHeaders = new ArrayList<>();
//    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
//
//
//    QBSyncReportsDetail() {
//    }
//
//    public QBSyncReportsDetail(QuickbookSyncDetailsRepository repository) {
//        this.quickbookSyncDetailsRepository = repository;
//        Collections.addAll(reportHeaders, attrs);
//        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
//                GathererReport.FieldType.DATE,
//                GathererReport.FieldType.STRING,
//                GathererReport.FieldType.NUMBER,
//                GathererReport.FieldType.NUMBER,
//                GathererReport.FieldType.NUMBER};
//        for (int i = 0; i < attrs.length; i++) {
//            fieldTypes.put(attrs[i], types[i]);
//        }
//
//    }
//
//    @Override
//    public GathererReport gather(ReportFilter filter) {
//        GathererReport report = new GathererReport(filter, "Quickbook Sync Report", reportHeaders);
//        report.setReportPostfix(GathererReport.TIMESTAMP);
//        report.setReportFieldTypes(fieldTypes);
//        Iterable<QuickbookSyncDetails> quickbookSyncDetailsIterable = quickbookSyncDetailsRepository.listByShopWithDateQb(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
//        for (QuickbookSyncDetails quickbookSyncDetails : quickbookSyncDetailsIterable) {
//            HashMap<String, Object> data = new HashMap<>(reportHeaders.size());
//            data.put(attrs[0], ProcessorUtil.dateStringWithOffset((Long) quickbookSyncDetails.getEndTime(), filter.getTimezoneOffset()));
//            data.put(attrs[1], quickbookSyncDetails.getQuickbookEntityType().toString());
//            data.put(attrs[2], quickbookSyncDetails.getTotalRecords());
//            data.put(attrs[3], quickbookSyncDetails.getTotal_Sync());
//            data.put(attrs[4], quickbookSyncDetails.getTotal_fail());
//            report.add(data);
//        }
//
//
//        return report;
//    }
//}