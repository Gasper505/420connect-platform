package com.fourtwenty.core.reporting.gather.impl.sidebar;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephen on 11/6/16.
 */
public class TodaysProfitGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private ProductBatchRepository batchRepository;
    private String[] attrs = new String[]{"Profit"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TodaysProfitGatherer(TransactionRepository transactionRepository, ProductRepository productRepository, ProductBatchRepository batchRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.batchRepository = batchRepository;
        reportHeaders.add("Profit");
        fieldTypes.put("Profit", GathererReport.FieldType.CURRENCY);
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Today's Profit", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.TIMESTAMP);
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        return report;
    }
}
