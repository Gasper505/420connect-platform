package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.managed.ReportManager;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.reportrequest.ReportRequestService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

public class AllSalesDetailsGatherer implements Gatherer {
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    CartService cartService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ReportManager reportManager;
    @Inject
    private ReportRequestService reportRequestService;

    private String[] attrs = new String[]{
            "Date", //0
            "Trans No.", //1
            "Created Date", //2
            "Trans Type", //3
            "Trans Status", //4
            "Product Name", //5
            "Product Category", //6
            "Brand Name", //7
            "Vendor", //8
            "Member", //9
            "Consumer Tax Type", //10
            "Cannabis", //11
            "Quantity Sold", //12
            "Batch", //13
            "COGs", //14
            "Retail Value", //15
            "Product Discounts", //16
            "Subtotal", //17
            "Cart Discounts", //18
            "Final Subtotal", //19
            "Pre ALExcise Tax", //20
            "Pre NALExcise Tax", //21

            "Pre City Tax", //22
            "Pre County Tax", //23
            "Pre State Tax", //24
            "Pre Federal Tax", //25

            "Post ALExcise Tax", //26
            "Post NALExcise Tax", //27
            "City Tax", //28
            "County Tax", //29
            "State Tax", //30
            "Federal Tax", //31
            "Total Tax", //32
            "After Tax Discount", //33
            "Delivery Fees", //34
            "Credit Card Fees", //35
            "Gross Receipt", //36
            "Employee", //37
            "Terminal", //38
            "Payment Type", //39
            "Promotion(s)", //40
            "Marketing Source", //41
            "Member Group", // 42
            "Zip Code", // 43
            "Member State", //44
            "Date Joined", //45
            "Gender", //46
            "DOB", //47
            "Age", //48
            "Loyalty Points" //49

    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public AllSalesDetailsGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Date 0
                GathererReport.FieldType.STRING, // Trans no 1
                GathererReport.FieldType.STRING, // Created date 2
                GathererReport.FieldType.STRING, // Type 3
                GathererReport.FieldType.STRING, // Status 4
                GathererReport.FieldType.STRING, // Product name 5
                GathererReport.FieldType.STRING, // Product category 6
                GathererReport.FieldType.STRING, // Brand name 7
                GathererReport.FieldType.STRING, // Vendor 8
                GathererReport.FieldType.STRING, // Member 9
                GathererReport.FieldType.STRING, // Consumer tax type 10
                GathererReport.FieldType.STRING, // Cannabis 11
                GathererReport.FieldType.STRING, // Quantity sold 12
                GathererReport.FieldType.STRING, // Batch 13
                GathererReport.FieldType.CURRENCY, // COGs 14
                GathererReport.FieldType.CURRENCY, // Cost 15
                GathererReport.FieldType.CURRENCY, // Product discounts 16
                GathererReport.FieldType.CURRENCY, // Subtotal 17
                GathererReport.FieldType.CURRENCY, // Cart discounts 18
                GathererReport.FieldType.CURRENCY, // Final subtotal 19
                GathererReport.FieldType.CURRENCY, // Pre AL excise tax 20
                GathererReport.FieldType.CURRENCY, // Pre NAL excise tax 21
                GathererReport.FieldType.CURRENCY, // CityTax 22

                GathererReport.FieldType.CURRENCY, // CountyTax 23
                GathererReport.FieldType.CURRENCY, // StateTax 24
                GathererReport.FieldType.CURRENCY, // FederalTax 25
                GathererReport.FieldType.CURRENCY, // Post AL excise tax 26

                GathererReport.FieldType.CURRENCY, // Post NAL excise tax 27
                GathererReport.FieldType.CURRENCY, // CityTax 28
                GathererReport.FieldType.CURRENCY, // CountyTax 29
                GathererReport.FieldType.CURRENCY, // StateTax 30
                GathererReport.FieldType.CURRENCY, // Federal 31
                GathererReport.FieldType.CURRENCY, // Post taxes 32
                GathererReport.FieldType.CURRENCY, // After tax discounts 33
                GathererReport.FieldType.CURRENCY, // Delivery fees 34
                GathererReport.FieldType.CURRENCY, // Credit Card fees 35
                GathererReport.FieldType.NUMBER, // Total sale 36
                GathererReport.FieldType.STRING, // Employee 37
                GathererReport.FieldType.STRING, // Term 38
                GathererReport.FieldType.STRING, // Payment type 39
                GathererReport.FieldType.STRING, // Promotions 40
                GathererReport.FieldType.STRING, // Marketing src 41
                GathererReport.FieldType.STRING, // Member Group 42
                GathererReport.FieldType.STRING, // Zip 43
                GathererReport.FieldType.STRING, // State 44
                GathererReport.FieldType.STRING, // Joined Date 45
                GathererReport.FieldType.STRING, // Gender 46
                GathererReport.FieldType.STRING, // DOB 47
                GathererReport.FieldType.STRING, // AGE 48
                GathererReport.FieldType.STRING  // Loyalty Points 49
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Completed Sales Detail Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        List<Transaction.TransactionStatus> statuses = new ArrayList<>();
        statuses.add(Transaction.TransactionStatus.Queued);
        statuses.add(Transaction.TransactionStatus.Hold);
        statuses.add(Transaction.TransactionStatus.InProgress);
        statuses.add(Transaction.TransactionStatus.Completed);
        statuses.add(Transaction.TransactionStatus.RefundWithInventory);
        statuses.add(Transaction.TransactionStatus.RefundWithoutInventory);

        Iterable<Transaction> results = transactionRepository.getBracketSalesWithStatuses(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), statuses);
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Promotion> promotionMap = promotionRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, LoyaltyReward> rewardMap = rewardRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Product> products = productRepository.getAllProductByCompany(filter.getCompanyId());
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, MemberGroup> memberGroupHashMap = memberGroupRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }


        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));

        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());

        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }
        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);


        long now = DateTime.now().getMillis();
        int factor = 1;
        for (Transaction ts : transactions) {
            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart() != null && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Member member = memberHashMap.get(ts.getMemberId());

            if (member == null) {
                continue;
            }

            Cart cart = ts.getCart();
            if (cart == null) {
                continue;
            }

            String consumerTaxType = cart.getFinalConsumerTye().getDisplayName();
            /*if (cart.getTaxTable() != null) {
                consumerTaxType = cart.getTaxTable().getName();
            }*/

            Employee emp = employeeMap.get(ts.getSellerId());
            String employeeName = "";
            if (emp != null) {
                employeeName = emp.getFirstName() + " " + emp.getLastName();
            }

            Terminal terminal = terminalMap.get(ts.getSellerTerminalId());
            String terminalName = "";
            if (terminal != null) {
                terminalName = terminal.getName();
            }

            //Getting promotions applied for each transaction
            LinkedHashSet<PromotionReq> promos = cart.getPromotionReqs();
            StringBuilder sb = new StringBuilder();
            Iterator<PromotionReq> it = promos.iterator();

            while (it.hasNext()) {
                PromotionReq promotionReq = it.next();
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                    if (promotion != null) {
                        sb.append(promotion.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                } else {
                    LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                    if (reward != null) {
                        sb.append(reward.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                }
            }


            String processedDate = ProcessorUtil.timeStampWithOffsetLong(ts.getProcessedTime(), filter.getTimezoneOffset());
            String createdDate = ProcessorUtil.timeStampWithOffsetLong(ts.getCreated(), filter.getTimezoneOffset());

            Cart.PaymentOption paymentOption = cart.getPaymentOption();
            String transNo = ts.getTransNo();


            // Calculate discounts proportionally so we can apply taxes

            HashMap<String, Double> propRatioMap = new HashMap<>();

            if (ts.getCart().getItems().size() > 0) {
                double total = 0.0;

                // Sum up the total
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {
                        if (product.isDiscountable()) {
                            total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                        }
                    }
                }

                // Calculate the ratio to be applied
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {

                        propRatioMap.put(orderItem.getId(), 1d); // 100 %
                        if (product.isDiscountable() && total > 0) {
                            double finalCost = orderItem.getFinalPrice().doubleValue();
                            double ratio = finalCost / total;

                            propRatioMap.put(orderItem.getId(), ratio);
                        }
                    }

                }
            }

            // Now do real calculation
            for (OrderItem item : cart.getItems()) {

                String productId = item.getProductId();
                Product product = productMap.get(productId);
                if (product == null) {
                    continue;
                }


                boolean cannabis = false;
                double cityTax = 0;
                double countyTax = 0;
                double stateTax = 0;
                double exciseTax = 0;
                double federalTax = 0;


                double preCityTax = 0;
                double preCountyTax = 0;
                double preStateTax = 0;
                double preFederalTax = 0;


                Double ratio = propRatioMap.get(item.getId());
                if (ratio == null) {
                    ratio = 1d;
                }

                Double propCartDiscount = ts.getCart().getCalcCartDiscount() != null ? ts.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propDeliveryFee = ts.getCart().getDeliveryFee() != null ? ts.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                Double propCCFee = ts.getCart().getCreditCardFee() != null ? ts.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                Double propAfterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount() != null ? ts.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;

                double preALExciseTax = 0;
                double preNALExciseTax = 0;
                double postALExciseTax = 0;
                double postNALExciseTax = 0;

                double totalTax = 0;
                double subTotalAfterdiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                if (item.getTaxResult() != null) {
                    preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                    preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                    postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                    postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        cityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                        stateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                        countyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                        federalTax = item.getTaxResult().getTotalFedTax().doubleValue();
                        exciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                        totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                    }


                    preCityTax = ts.getCart().getTaxResult().getTotalCityPreTax().doubleValue();
                    preCountyTax = ts.getCart().getTaxResult().getTotalCountyPreTax().doubleValue();
                    preStateTax = ts.getCart().getTaxResult().getTotalStatePreTax().doubleValue();
                    preFederalTax = ts.getCart().getTaxResult().getTotalFedPreTax().doubleValue();

                }
                if (totalTax == 0) {
                    if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                        TaxInfo taxInfo = item.getTaxInfo();
                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            cityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                            stateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                            federalTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();
                            totalTax = cityTax + stateTax + federalTax;
                        } else {
                            preCityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                            preStateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                            preFederalTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();
                        }
                    }
                }

                if (item.getExciseTax().doubleValue() > 0) {
                    exciseTax = item.getExciseTax().doubleValue();
                }

                if (totalTax == 0) {
                    totalTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                }

                totalTax += postALExciseTax + postNALExciseTax;

                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                Brand brand = brandHashMap.get(product.getBrandId());
                ProductCategory.UnitType unitType = category.getUnitType();
                if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                        || (product.getCannabisType() != Product.CannabisType.CBD
                        && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                        && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                    cannabis = true;
                }

                double finalAfterTax = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4)
                        + NumberUtils.round(propDeliveryFee, 4) + NumberUtils.round(totalTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);

                Vendor vendor = vendorHashMap.get(product.getVendorId());

                StringBuilder batchLogs = new StringBuilder();
                Double cogs = 0d;
                boolean calculated = false;
                String productName = product.getName();
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        cogs += calcCOGS(unitsSold, targetBatch, factor);

                        batchLogs.append(targetBatch.getSku());

                        productName = String.format("%s (%s)", product.getName(), prepackage.getName());
                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch, factor);
                            }
                        }
                        ProductBatch batch = allBatchMap.get(quantityLog.getBatchId());
                        if (batch != null) {
                            batch = getRecentBatch(product, recentBatchMap, productsByCompanyLinkId);
                        }
                        if (batch != null) {
                            batchLogs.append(batch.getSku() + ",");
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    cogs = unitCost * item.getQuantity().doubleValue() * factor;
                }


                String unitTypeStr = (category != null && category.getUnitType() == ProductCategory.UnitType.grams) ? "g" : "ea";

                double itemCogs = cogs;
                double itemCost = item.getCost().doubleValue() * factor;
                double itemDiscount = item.getCalcDiscount().doubleValue() * factor;
                double itemfinalPrice = item.getFinalPrice().doubleValue() * factor;
                double itemCartDiscount = propCartDiscount * factor;
                double finalSubtotal = itemfinalPrice - itemCartDiscount;
                double itemAfterTaxDiscount = propAfterTaxDiscount * factor;
                double itemPreALExciseTax = preALExciseTax * factor;
                double itemPreNalExciseTax = preNALExciseTax * factor;

                double itemPreCityTax = preCityTax * factor;
                double itemPreCountyTax = preCountyTax * factor;
                double itemPreStateTax = preStateTax * factor;
                double itemPreFederalTax = preFederalTax * factor;

                double itemPOSTALExciseTax = postALExciseTax * factor;
                double itemPOSTNALExciseTax = postNALExciseTax * factor;
                double itemCityTax = cityTax * factor;
                double itemCountyTax = countyTax * factor;
                double itemStateTax = stateTax * factor;
                double itemFederalTAx = federalTax * factor;
                double itemTotalTAx = totalTax * factor;
                double itemDeliveryFee = propDeliveryFee * factor;
                double itemPropCCFee = propCCFee * factor;
                double itemGrossReceipt = finalAfterTax * factor;

                if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    // zero out since this is the old refund
                    itemCogs = 0d;
                    itemCost = 0d;
                    itemDiscount = 0d;
                    itemfinalPrice = 0d;
                    itemCartDiscount = 0d;
                    itemAfterTaxDiscount = 0d;
                    itemPreALExciseTax = 0d;
                    itemPreNalExciseTax = 0d;
                    itemPOSTALExciseTax = 0d;
                    itemPOSTNALExciseTax = 0d;
                    itemCityTax = 0d;
                    itemCountyTax = 0d;
                    itemStateTax = 0d;
                    itemFederalTAx = 0d;
                    itemTotalTAx = 0d;
                    itemDeliveryFee = 0d;
                    itemPropCCFee = 0d;
                    itemGrossReceipt = 0d;
                }

                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], processedDate); // Date
                data.put(attrs[1], transNo); // Trans no
                data.put(attrs[2], createdDate); // Created date
                data.put(attrs[3], ts.getTransType()); // Type
                data.put(attrs[4], ts.getStatus()); // Status
                data.put(attrs[5], productName); // Product name
                data.put(attrs[6], category.getName()); // Product category
                data.put(attrs[7], (brand == null || StringUtils.isBlank(brand.getName())) ? "" : brand.getName()); // Brand name
                data.put(attrs[8], vendor != null ? vendor.getName() : ""); // vendor
                data.put(attrs[9], member.getFirstName() + " " + member.getLastName()); // Member
                data.put(attrs[10], consumerTaxType); // Consumer tax type
                data.put(attrs[11], cannabis ? "Yes" : "No"); // Cannabis

                data.put(attrs[12], item.getQuantity().doubleValue() + " " + unitTypeStr); // Quantity sold
                data.put(attrs[13], batchLogs.toString()); // Batch
                data.put(attrs[14], new DollarAmount(itemCogs)); // Cogs
                data.put(attrs[15], new DollarAmount(itemCost)); // Cost
                data.put(attrs[16], new DollarAmount(itemDiscount)); // Product discounts
                data.put(attrs[17], new DollarAmount(itemfinalPrice)); // Sub total
                data.put(attrs[18], new DollarAmount(itemCartDiscount)); // Cart discounts
                data.put(attrs[19], new DollarAmount(finalSubtotal)); // Final sub total

                data.put(attrs[20], new DollarAmount(itemPreALExciseTax)); // Pre AL excise tax
                data.put(attrs[21], new DollarAmount(itemPreNalExciseTax)); // Pre NAL excise tax

                data.put(attrs[22], new DollarAmount(itemPreCityTax)); // CityTax
                data.put(attrs[23], new DollarAmount(itemPreCountyTax)); // CountyTax
                data.put(attrs[24], new DollarAmount(itemPreStateTax)); // StateTax
                data.put(attrs[25], new DollarAmount(itemPreFederalTax)); // FederalTax

                data.put(attrs[26], new DollarAmount(itemPOSTALExciseTax)); // Post AL excise tax
                data.put(attrs[27], new DollarAmount(itemPOSTNALExciseTax)); // Post NAL excise tax

                data.put(attrs[28], new DollarAmount(itemCityTax)); // CityTax
                data.put(attrs[29], new DollarAmount(itemCountyTax)); // CountyTax
                data.put(attrs[30], new DollarAmount(itemStateTax)); // StateTax
                data.put(attrs[31], new DollarAmount(itemFederalTAx)); // FederalTax
                data.put(attrs[32], new DollarAmount(itemTotalTAx)); // Post taxes
                data.put(attrs[33], new DollarAmount(itemAfterTaxDiscount)); // After tax discounts
                data.put(attrs[34], new DollarAmount(itemDeliveryFee)); // Delivery fees
                data.put(attrs[35], new DollarAmount(itemPropCCFee)); // Credit Card fees
                data.put(attrs[36], new DollarAmount(itemGrossReceipt)); // Total sale

                data.put(attrs[37], employeeName); // Employee
                data.put(attrs[38], terminalName); // Term
                data.put(attrs[39], paymentOption); // Payment type
                data.put(attrs[40], sb.toString()); // Promotions
                data.put(attrs[41], TextUtil.textOrEmpty(member.getMarketingSource())); // Marketing src

                String memberGroup = "";
                if (memberGroupHashMap.containsKey(member.getMemberGroupId())) {
                    memberGroup = memberGroupHashMap.get(member.getMemberGroupId()).getName();
                }
/*

                GathererReport.FieldType.STRING, // Joined Date 29
                GathererReport.FieldType.STRING, // Gender 30
                GathererReport.FieldType.STRING, //DOB 31
                GathererReport.FieldType.STRING //Loyalty Points 32
 */
                String zipCode = "";
                String memberState = "";
                if (member.getAddress() != null) {
                    zipCode = member.getAddress().getZipCode();
                    memberState = member.getAddress().getState();
                }

                data.put(attrs[42], memberGroup); // Member Group
                data.put(attrs[43], TextUtil.textOrEmpty(zipCode)); // zip
                data.put(attrs[44], TextUtil.textOrEmpty(memberState)); // state
                data.put(attrs[45], DateUtil.toDateFormatted(member.getStartDate(), shop.getTimeZone())); // Joined Date
                data.put(attrs[46], member.getSex().name()); //Gender
                data.put(attrs[47], DateUtil.toDateFormatted(member.getDob())); // DOB
                data.put(attrs[48], member.getDob() != null ? DateUtil.getYearsBetweenTwoDates(member.getDob(), now) : ""); // AGE
                data.put(attrs[49], NumberUtils.round(member.getLoyaltyPoints(), 2)); // Loyalty Points

                report.add(data);
            }
        }
        return report;
    }

    public double calcCOGS(final double quantity, final ProductBatch batch, int factor) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost * factor;
        return NumberUtils.round(total, 2);
    }


    public double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public ProductBatch getRecentBatch(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }
        return batch;
    }
}
