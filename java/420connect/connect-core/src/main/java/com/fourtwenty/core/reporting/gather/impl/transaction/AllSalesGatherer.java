package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.*;

public class AllSalesGatherer implements Gatherer {
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;
    @Inject
    private MetrcService metrcService;

    private String[] attrs = new String[]{
            "Date", //0
            "Member", //1
            "Created Date", //2
            "Transaction No.", //3
            "Trans Type", //4
            "Trans Status", //5
            "Consumer Tax Type", //6
            "COGs", //7
            "Retail Value", //8
            "Discounts", //9
            "Pre AL Excise Tax", //10
            "Pre NAL Excise Tax", //11
            "Pre City Tax", //12
            "Pre County Tax", //13
            "Pre State Tax", //14
            "Post AL Excise Tax", //15
            "Post NAL Excise Tax", //16
            "City Tax", //17
            "County Tax", //18
            "State Tax", //19
            "Total Tax", //20
            "Delivery Fees", //21
            "Credit/Debit Card Fees", //22
            "After Tax Discount", //23
            "Gross Receipt", //24
            "Employee", //25
            "Terminal", //26
            "Payment Type", //27
            "Promotion(s)", //28
            "Marketing Source", //29
            "Tags" //30
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public AllSalesGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Date 0
                GathererReport.FieldType.STRING, // Member 1
                GathererReport.FieldType.STRING, // Created date 2
                GathererReport.FieldType.STRING, // TransNo 3
                GathererReport.FieldType.STRING, // Type 4
                GathererReport.FieldType.STRING, // Status 5
                GathererReport.FieldType.STRING, // Consumer tax type 6
                GathererReport.FieldType.CURRENCY, // Cogs 7
                GathererReport.FieldType.CURRENCY, // Subtotal 8
                GathererReport.FieldType.CURRENCY, // Discounts 9
                GathererReport.FieldType.CURRENCY, // Pre AL excise tax 10
                GathererReport.FieldType.CURRENCY, // Pre NAL excise tax 11
                GathererReport.FieldType.CURRENCY, // Pre city tax 12
                GathererReport.FieldType.CURRENCY, // Pre county tax 13
                GathererReport.FieldType.CURRENCY, // Pre state tax 14
                GathererReport.FieldType.CURRENCY, // Post AL excise tax 15
                GathererReport.FieldType.CURRENCY, // Post NAL excise tax 16
                GathererReport.FieldType.CURRENCY, // City tax 17
                GathererReport.FieldType.CURRENCY, // County tax 18
                GathererReport.FieldType.CURRENCY, // State tax 19
                GathererReport.FieldType.CURRENCY, // Post taxes 20
                GathererReport.FieldType.CURRENCY, // Delivery fees 21
                GathererReport.FieldType.CURRENCY, // Credit card fees 22
                GathererReport.FieldType.CURRENCY, // After tax discount 23
                GathererReport.FieldType.NUMBER, // Total sale 24
                GathererReport.FieldType.STRING, // Employee 25
                GathererReport.FieldType.STRING, // Term 26
                GathererReport.FieldType.STRING, // Payment type 27
                GathererReport.FieldType.STRING, // Promotions 28
                GathererReport.FieldType.STRING, // Marketing src 29
                GathererReport.FieldType.STRING}; // Tags 30
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        List<Transaction.TransactionStatus> statuses = new ArrayList<>();
        statuses.add(Transaction.TransactionStatus.Queued);
        statuses.add(Transaction.TransactionStatus.Hold);
        statuses.add(Transaction.TransactionStatus.InProgress);
        statuses.add(Transaction.TransactionStatus.Completed);
        statuses.add(Transaction.TransactionStatus.RefundWithInventory);
        statuses.add(Transaction.TransactionStatus.RefundWithoutInventory);

        Iterable<Transaction> results = transactionRepository.getBracketSalesWithStatuses(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis(), statuses);
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Promotion> promotionMap = promotionRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, LoyaltyReward> rewardMap = rewardRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Product> products = productRepository.list(filter.getCompanyId());
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }


        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));

        boolean enableMetrc = false;
        String stateCode = metrcService.getShopStateCode(filter.getCompanyId(), filter.getShopId(), false);
        if (stateCode != null) {
            MetrcAccount metrcAccount = metrcAccountRepository.getMetrcAccount(filter.getCompanyId(), stateCode);
            if (metrcAccount != null) {
                for (MetrcFacilityAccount metrcFacilityAccount : metrcAccount.getFacilities()) {
                    if (metrcFacilityAccount.getShopId() != null && metrcFacilityAccount.getShopId().equalsIgnoreCase(filter.getShopId())) {
                        if (metrcFacilityAccount.isEnabled()) {
                            enableMetrc = true;
                            fieldTypes.put("MetrcId", GathererReport.FieldType.STRING);
                            reportHeaders.add("MetrcId");
                        }
                    }
                }
            }
        }

        GathererReport report = null;
        StringBuilder builder = null;
        report = new GathererReport(filter, "All Sales Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        String nAvail = "N/A";
        int factor = 1;
        for (Transaction ts : transactions) {
            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart() != null && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Member member = memberHashMap.get(ts.getMemberId());

            if (member == null) {
                continue;
            }

            String consumerTaxType = ts.getCart().getFinalConsumerTye().getDisplayName();
            /*
            if (ts.getCart().getTaxTable() != null) {
                consumerTaxType = ts.getCart().getTaxTable().getName();
            }*/

            double units = 0.0;
            double totalPreTax = 0;
            double totalPostTax = 0.0;

            totalPostTax = ts.getCart().getTotalCalcTax().doubleValue() - ts.getCart().getTotalPreCalcTax().doubleValue();
            if (totalPostTax < 0) {
                totalPostTax = 0;
            }
            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;

            double preCityTax = 0;
            double preCountyTax = 0;
            double preStateTax = 0;

            double federalTax = 0;
            double exciseTax = 0;
            double preALExciseTax = 0d;
            double preNALExciseTax = 0d;
            double postALExciseTax = 0d;
            double postNALExciseTax = 0d;
            double afterTaxDiscount = 0d;
            if (ts.getCart().getTaxResult() != null) {
                exciseTax = ts.getCart().getTaxResult().getTotalExciseTax().doubleValue();
                postALExciseTax = ts.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = ts.getCart().getTaxResult().getTotalExciseTax().doubleValue();
                preALExciseTax = ts.getCart().getTaxResult().getTotalALExciseTax().doubleValue();
                preNALExciseTax = ts.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue();
                cityTax = ts.getCart().getTaxResult().getTotalCityTax().doubleValue();
                countyTax = ts.getCart().getTaxResult().getTotalCountyTax().doubleValue();
                stateTax = ts.getCart().getTaxResult().getTotalStateTax().doubleValue();
                //totalPostTax = ts.getCart().getTaxResult().getTotalPostCalcTax().doubleValue();

                preCityTax = ts.getCart().getTaxResult().getTotalCityPreTax().doubleValue();
                preCountyTax = ts.getCart().getTaxResult().getTotalCountyPreTax().doubleValue();
                preStateTax = ts.getCart().getTaxResult().getTotalStatePreTax().doubleValue();
            }

            if (ts.getCart().getAppliedAfterTaxDiscount() != null && ts.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount().doubleValue();
            }

            double creditCardFees = 0;
            if (ts.getCart().getCreditCardFee().doubleValue() > 0) {
                creditCardFees = ts.getCart().getCreditCardFee().doubleValue();
            }

            double deliveryFees = 0;
            if (ts.getCart().getDeliveryFee().doubleValue() > 0) {
                deliveryFees = ts.getCart().getDeliveryFee().doubleValue();
            }


            //Getting promotions applied for each transaction
            LinkedHashSet<PromotionReq> promos = ts.getCart().getPromotionReqs();
            StringBuilder sb = new StringBuilder();
            Iterator<PromotionReq> it = promos.iterator();

            while (it.hasNext()) {
                PromotionReq promotionReq = it.next();
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                    if (promotion != null) {
                        sb.append(promotion.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                } else {
                    LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                    if (reward != null) {
                        sb.append(reward.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                }
            }

            double transCogs = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

//                if (!(ts.getTransType() == Transaction.TransactionType.Sale
//                        || (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail
//                        && item.getStatus() == OrderItem.OrderItemStatus.Refunded))) {
//                    continue;
//                }

                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                boolean calculated = false;
                double itemCogs = 0;
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        itemCogs += calcCOGS(unitsSold, targetBatch);

                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                itemCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                            }
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    itemCogs = unitCost * item.getQuantity().doubleValue();
                }
                transCogs += itemCogs;
            }


            Employee emp = employeeMap.get(ts.getSellerId());
            String employeeName = "";
            if (emp != null) {
                employeeName = emp.getFirstName() + " " + emp.getLastName();
            }
            Terminal t = terminalMap.get(ts.getSellerTerminalId());
            String terminalName = "";
            if (t != null) {
                terminalName = t.getName();
            }

            double total = ts.getCart().getTotal().doubleValue() * factor;
            if (ts.getTransType() == Transaction.TransactionType.Refund
                    && ts.getCart().getRefundOption() == Cart.RefundOption.Void
                    && ts.getCart().getSubTotal().doubleValue() == 0) {
                creditCardFees = 0;
                deliveryFees = 0;
                afterTaxDiscount = 0;
                total = 0;
            }

            String memberName = member.getFirstName() + " " + member.getLastName();

            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(ts.getProcessedTime(), filter.getTimezoneOffset()));
            data.put(attrs[1], memberName);
            data.put(attrs[2], ProcessorUtil.timeStampWithOffsetLong(ts.getCreated(), filter.getTimezoneOffset()));
            data.put(attrs[3], ts.getTransNo());
            data.put(attrs[4], ts.getTransType());
            data.put(attrs[5], ts.getStatus());
            data.put(attrs[6], consumerTaxType);
            data.put(attrs[7], new DollarAmount(transCogs * factor)); //cogs
            data.put(attrs[8], new DollarAmount(ts.getCart().getSubTotal().doubleValue() * factor));
            data.put(attrs[9], new DollarAmount(ts.getCart().getTotalDiscount().doubleValue() * factor));

            data.put(attrs[10], new DollarAmount(preALExciseTax * factor));
            data.put(attrs[11], new DollarAmount(preNALExciseTax * factor));

            data.put(attrs[12], new DollarAmount(preCityTax * factor));
            data.put(attrs[13], new DollarAmount(preCountyTax * factor));
            data.put(attrs[14], new DollarAmount(preStateTax * factor));

            data.put(attrs[15], new DollarAmount(postALExciseTax * factor));
            data.put(attrs[16], new DollarAmount(postNALExciseTax * factor));
            data.put(attrs[17], new DollarAmount(cityTax * factor));
            data.put(attrs[18], new DollarAmount(countyTax * factor));
            data.put(attrs[19], new DollarAmount(stateTax * factor));
            data.put(attrs[20], new DollarAmount(totalPostTax * factor));
            data.put(attrs[21], new DollarAmount(deliveryFees * factor));
            data.put(attrs[22], new DollarAmount(creditCardFees * factor));
            data.put(attrs[23], new DollarAmount(afterTaxDiscount * factor));
            data.put(attrs[24], new DollarAmount(total));
            data.put(attrs[25], employeeName);
            data.put(attrs[26], terminalName);
            data.put(attrs[27], ts.getCart().getPaymentOption());
            data.put(attrs[28], sb.toString());
            data.put(attrs[29], TextUtil.textOrEmpty(member.getMarketingSource()));
            StringBuilder tagBuilder = new StringBuilder();
            if (ts.getOrderTags() != null) {
                ts.getOrderTags().forEach((tag) -> tagBuilder.append(tag + ";"));
            }
            data.put(attrs[30],tagBuilder.toString());


            if (enableMetrc) {
                String metrcId = "Not Submitted";
                if (ts.getMetrcId() != null) {
                    metrcId = String.valueOf(ts.getMetrcId());
                } else if (ts.getTraceSubmitStatus() != Transaction.TraceSubmissionStatus.None) {
                    metrcId = ts.getTraceSubmitStatus().name();
                    if (ts.getTraceSubmitStatus() == Transaction.TraceSubmissionStatus.SubmissionPending) {
                        metrcId = Transaction.TraceSubmissionStatus.SubmissionWaitingID.name();
                    }
                }
                data.put("MetrcId", metrcId);
            }

            report.add(data);


        }

        return report;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}
