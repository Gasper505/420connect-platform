package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

import javax.inject.Inject;
import java.util.*;

/**
 * Created by stephen on 9/11/16.
 */
public class AverageTransactionTimeGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Queue", "Total Transaction Time", "Transaction Count", "Avg Checkout Time"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    @Inject
    public AverageTransactionTimeGatherer(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Average Transaction Time", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        LinkedHashMap<String, AvgTime> avgMap = new LinkedHashMap<>();
        AvgTime overallTime = new AvgTime();
        overallTime.queueName = "(Overall)";


        for (Transaction t : results) {
            if (t.getTransType() == Transaction.TransactionType.Refund) {
                continue;
            }
            AvgTime avgTime = avgMap.get(t.getQueueType().name());
            if (avgTime == null) {
                avgTime = new AvgTime();
                avgTime.queueName = t.getQueueType().name();
                avgMap.put(t.getQueueType().name(), avgTime);
            }
            long endTime = (t.getEndTime() == null) ? 0 : t.getEndTime();
            long startTime = (t.getStartTime() == null) ? 0 : t.getStartTime();

            if (endTime == 0 && t.getProcessedTime() != null) {
                endTime = t.getProcessedTime();
            }

            if (startTime == 0 && t.getCreated() != null) {
                startTime = t.getCreated();
            }


            long queueEndTime = (t.getEndTime() == null) ? 0 : t.getEndTime();
            long queueCreateTime = t.getCreated();

            if (queueEndTime == 0 && t.getProcessedTime() != null) {
                queueEndTime = t.getProcessedTime();
            }


            long time = endTime - startTime;
            long queueDuration = queueEndTime - queueCreateTime;
            if (queueDuration < 0) {
                queueDuration = 0;
            }

            avgTime.count++;
            avgTime.totalTime += time;
            avgTime.totalQueueTime += queueDuration;

            overallTime.count++;
            overallTime.totalTime += time;
            overallTime.totalQueueTime += queueDuration;
        }

        avgMap.put(overallTime.queueName, overallTime);


        for (String avgTimeKey : avgMap.keySet()) {
            AvgTime avgTime = avgMap.get(avgTimeKey);
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], avgTime.queueName);
            data.put(attrs[1], avgTime.getAvgTime() < 0 ? "-" + formatTime(Math.abs(avgTime.totalTime)) : formatTime(avgTime.totalTime));

            data.put(attrs[2], avgTime.count);
            data.put(attrs[3], avgTime.getAvgTime() < 0 ? "-" + formatTime(Math.abs(avgTime.getAvgTime())) : formatTime(avgTime.getAvgTime()));
            //data.put(attrs[4], formatTime(avgTime.getAvgQueueTime()));
            report.add(data);
        }

        return report;
    }

    public String formatTime(Long millis) {
        long minMillis = 1000 * 60;
        long hourMillis = minMillis * 60;
        long dayMillis = hourMillis * 24;
        String formatted;
        if (millis >= dayMillis) { //input millis is greater than 1 day of time
            String days = Long.toString(millis / dayMillis);
            long hrs = millis % dayMillis;
            String hours = Long.toString(hrs / hourMillis);
            long mns = hrs % hourMillis;
            String minutes = Long.toString(mns / minMillis);
            long secs = mns % minMillis;
            String seconds = Long.toString(secs / 1000);
            formatted = days + "d, " + hours + "h, " + minutes + "m, " + seconds + "s";
        } else if (millis >= hourMillis) { //input millis is greater than 1 hour of time
            String hours = Long.toString(millis / hourMillis);
            long mns = millis % hourMillis;
            String minutes = Long.toString(mns / minMillis);
            long secs = mns % minMillis;
            String seconds = Long.toString(secs / 1000);
            formatted = hours + "h, " + minutes + "m, " + seconds + "s";
        } else if (millis >= minMillis) { //input millis is greater than 1 minute of time
            String minutes = Long.toString(millis / minMillis);
            long mins = millis % minMillis;
            String seconds = Long.toString(mins / 1000);
            formatted = minutes + "m, " + seconds + "s";
        } else { //input millis are less that 1 minute of time
            formatted = Long.toString(millis / 1000) + "s";
        }
        return formatted;
    }


    class AvgTime {
        String queueName = "";
        long count = 0;
        long totalQueueTime = 0;
        long totalTime = 0;

        long getAvgTime() {
            if (count > 0) {
                return totalTime / count;
            }
            return 0;
        }

        long getAvgQueueTime() {
            if (count > 0) {
                return totalQueueTime / count;
            }
            return 0;
        }
    }
}
