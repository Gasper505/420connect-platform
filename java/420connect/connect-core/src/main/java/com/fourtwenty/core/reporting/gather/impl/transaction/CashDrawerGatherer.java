package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.dispensary.CashDrawerSessionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;


/**
 * Created by Stephen Schmidt on 12/21/2016.
 */
public class CashDrawerGatherer implements Gatherer {
    private CashDrawerSessionRepository cashDrawerRepository;
    private TerminalRepository terminalRepository;
    private HashMap<Long, CashDrawerSession> cashDrawerByDayMap = new HashMap<>();

    private HashMap<String, GathererReport.FieldType> fields = new LinkedHashMap<>();
    private List<String> fieldHeaders = new ArrayList<>();

    public CashDrawerGatherer(TerminalRepository terminalRepository, CashDrawerSessionRepository cashRepository) {
        this.terminalRepository = terminalRepository;
        this.cashDrawerRepository = cashRepository;
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        HashMap<Long, ArrayList<CashDrawerSession>> sessionsByDay = new HashMap<>();
        HashMap<String, Terminal> terminalMap = terminalRepository.listAsMap(filter.getCompanyId(), filter.getShopId());


        //get data
        Iterable<CashDrawerSession> items = cashDrawerRepository.getCashDrawerSessionsForShop(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        ArrayList<CashDrawerSession> sessions = Lists.newArrayList(items);
        Collections.sort(sessions);

        // Build unique set
        HashSet<Cart.PaymentOption> customSalesTotalKeys = new HashSet<>();
        for (CashDrawerSession session : sessions) {
            if (session.getCustomSalesTotals() != null) {
                customSalesTotalKeys.addAll(session.getCustomSalesTotals().keySet());
            }
        }

        // Build unique set
        HashSet<Cart.PaymentOption> customRefundTotalKeys = new HashSet<>();
        for (CashDrawerSession session : sessions) {
            if (session.getCustomSalesTotals() != null) {
                customRefundTotalKeys.addAll(session.getCustomSalesTotals().keySet());
            }
        }


        //set fields
        fields.put("Date", GathererReport.FieldType.STRING);
        fields.put("Terminal", GathererReport.FieldType.STRING);
        fields.put("Status", GathererReport.FieldType.STRING);
        fields.put("Start Time", GathererReport.FieldType.STRING);
        fields.put("End Time", GathererReport.FieldType.STRING);
        fields.put("Starting Cash", GathererReport.FieldType.CURRENCY);
        fields.put("Cash Sales", GathererReport.FieldType.CURRENCY);
        fields.put("Check Sales", GathererReport.FieldType.CURRENCY);
        fields.put("Credit Sales", GathererReport.FieldType.CURRENCY);
        fields.put("Store Credit Sales", GathererReport.FieldType.CURRENCY);
        fields.put("CashlessAtm Sales", GathererReport.FieldType.CURRENCY);
        customSalesTotalKeys.forEach(k -> fields.put(String.format("%s Sales", k.toString()), GathererReport.FieldType.CURRENCY));
        fields.put("Cash Refunds", GathererReport.FieldType.CURRENCY);
        fields.put("Store Credit Refunds", GathererReport.FieldType.CURRENCY);
        fields.put("Cashless Atm Refunds", GathererReport.FieldType.CURRENCY);
        fields.put("Credit Refunds", GathererReport.FieldType.CURRENCY);
        customRefundTotalKeys.forEach(k -> fields.put(String.format("%s Refunds", k.toString()), GathererReport.FieldType.CURRENCY));
        fields.put("Paid In", GathererReport.FieldType.CURRENCY);
        fields.put("Paid Out", GathererReport.FieldType.CURRENCY);
        fields.put("Cash Drop", GathererReport.FieldType.CURRENCY);
        fields.put("Expected in Drawer", GathererReport.FieldType.CURRENCY);
        fields.put("Actual in Drawer", GathererReport.FieldType.CURRENCY);
        fieldHeaders = Lists.newArrayList(fields.keySet());

        GathererReport gathererReport = new GathererReport(filter, "Cash Drawer", fieldHeaders);
        gathererReport.setReportPostfix(GathererReport.DATE_BRACKET);
        gathererReport.setReportFieldTypes(fields);

        for (CashDrawerSession session : sessions) {
            HashMap<String, Object> data = new HashMap<>();

            int i = 0;

            long day = new DateTime(session.getStartTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset()).getMillis();
            data.put(fieldHeaders.get(i), ProcessorUtil.dateString(day));

            i++;
            Terminal t = terminalMap.get(session.getTerminalId());
            if (t != null) {
                data.put(fieldHeaders.get(i), t.getName());
            } else {
                data.put(fieldHeaders.get(i), "N/A");
            }

            data.put(fieldHeaders.get(++i), session.getStatus().toString());
            data.put(fieldHeaders.get(++i), ProcessorUtil.timeStampWithOffset(session.getStartTime(), filter.getTimezoneOffset()));

            i++;
            if (session.getStatus() == CashDrawerSession.CashDrawerLogStatus.Closed) {
                data.put(fieldHeaders.get(i), ProcessorUtil.timeStampWithOffset(session.getEndTime(), filter.getTimezoneOffset()));
            } else {
                data.put(fieldHeaders.get(i), "Not Closed");
            }

            data.put(fieldHeaders.get(++i), new DollarAmount(session.getStartCash()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getCashSales()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getCheckSales()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getCreditSales()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getStoreCreditSales()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getTotalCashlessAtmSales()));
            for (Cart.PaymentOption paymentOption : customSalesTotalKeys) {
                BigDecimal value = session.getCustomSalesTotals() != null ?
                        session.getCustomSalesTotals().getOrDefault(paymentOption, new BigDecimal(0)) :
                        new BigDecimal(0);
                data.put(fieldHeaders.get(++i), new DollarAmount(value));
            }
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getCashRefunds()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getStoreCreditRefunds()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getTotalCashlessAtmRefunds()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getCreditRefunds()));
            for (Cart.PaymentOption paymentOption : customRefundTotalKeys) {
                BigDecimal value = session.getCustomRefundTotals() != null ?
                        session.getCustomRefundTotals().getOrDefault(paymentOption, new BigDecimal(0)) :
                        new BigDecimal(0);
                data.put(fieldHeaders.get(++i), new DollarAmount(value));
            }
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getTotalCashIn()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getTotalCashOut()));
            //Add cash drop
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getTotalCashDrop()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getExpectedCash()));
            data.put(fieldHeaders.get(++i), new DollarAmount(session.getActualCash()));
            gathererReport.add(data);
        }
        return gathererReport;
    }

}
