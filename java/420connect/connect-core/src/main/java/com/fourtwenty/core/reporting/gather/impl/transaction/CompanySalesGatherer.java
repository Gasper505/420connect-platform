package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.managed.ReportManager;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.reportrequest.ReportRequestService;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CompanySalesGatherer implements Gatherer {

    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    CartService cartService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository; 
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ReportManager reportManager;
    @Inject
    private ReportRequestService reportRequestService;

    private String[] attrs = new String[]{
            "Shop", //0
            "Date", //1
            "Trans No.", //2
            "Trans Id", //3
            "Trans Type", //4
            "Payment Option", //5
            "Product Name", //6
            "Product Category", //7
            "Brand Name", //8
            "Quantity Sold", //9
            "Subtotal", //10
            "ALExcise Tax", //11
            "Pre City Tax", //12
            "Pre State Tax", //13
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public CompanySalesGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Shop 0
                GathererReport.FieldType.STRING, // Date 1
                GathererReport.FieldType.STRING, // Trans No 2
                GathererReport.FieldType.STRING, // Trans Id 3
                GathererReport.FieldType.STRING, // Trans Type 4
                GathererReport.FieldType.STRING, // Payment Option 5
                GathererReport.FieldType.STRING, // Product Name 6
                GathererReport.FieldType.STRING, // Product Category 7
                GathererReport.FieldType.STRING, // Brand Name 8
                GathererReport.FieldType.STRING, // Quantity Sold 9
                GathererReport.FieldType.CURRENCY, // Subtotal 10
                GathererReport.FieldType.CURRENCY, // ALExcise Tax 11
                GathererReport.FieldType.CURRENCY, // Pre City Tax 12
                GathererReport.FieldType.CURRENCY, // Pre State Tax 13
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Company Sales Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);


        Iterable<Transaction> transactions = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId());
        Iterable<Product> products = productRepository.getAllProductByCompany(filter.getCompanyId());
        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(filter.getCompanyId());

        HashMap<String, Product> productMap = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
        }

        int factor = 1;
        for (Transaction ts : transactions) {
            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && 
                (ts.getStatus() == Transaction.TransactionStatus.RefundWithInventory || ts.getStatus() == Transaction.TransactionStatus.RefundWithoutInventory)) {
                factor = -1;
            }

            Cart cart = ts.getCart();
            if (cart == null) {
                continue;
            }

            String processedDate = ProcessorUtil.timeStampWithOffsetLong(ts.getProcessedTime(), filter.getTimezoneOffset());
            Cart.PaymentOption paymentOption = cart.getPaymentOption();
            String transNo = ts.getTransNo();

            for (OrderItem item : cart.getItems()) {

                String productId = item.getProductId();
                Product product = productMap.get(productId);
                if (product == null) {
                    continue;
                }

                double itemCityTax = 0;;
                double itemStateTax = 0;
                double itemAlExciseTax = 0;

                if (item.getTaxResult() != null) {
                    itemAlExciseTax = item.getTaxResult().getTotalALExciseTax().doubleValue();
                    itemCityTax = item.getTaxResult().getTotalCityPreTax().doubleValue();
                    itemStateTax = item.getTaxResult().getTotalStatePreTax().doubleValue();
                }
                
                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                Brand brand = brandHashMap.get(product.getBrandId());
                ProductCategory.UnitType unitType = category.getUnitType();

                String productName = product.getName();
            
                String unitTypeStr = (category != null && category.getUnitType() == ProductCategory.UnitType.grams) ? "g" : "ea";
             
                double itemfinalPrice = item.getFinalPrice().doubleValue() * factor;
                itemAlExciseTax = itemAlExciseTax * factor;
                itemCityTax = itemCityTax * factor;
                itemStateTax = itemStateTax * factor;

                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], shopMap.get(ts.getShopId()).getName());
                data.put(attrs[1], processedDate);
                data.put(attrs[2], transNo);
                data.put(attrs[3], ts.getId());
                data.put(attrs[4], ts.getTransType());
                data.put(attrs[5], paymentOption);
                data.put(attrs[6], productName);
                data.put(attrs[7], category.getName());
                data.put(attrs[8], (brand == null || StringUtils.isBlank(brand.getName())) ? "" : brand.getName());
                data.put(attrs[9], item.getQuantity().doubleValue() + " " + unitTypeStr);
                data.put(attrs[10], new DollarAmount(itemfinalPrice));
                data.put(attrs[11], new DollarAmount(itemAlExciseTax));
                data.put(attrs[12], new DollarAmount(itemCityTax));
                data.put(attrs[13], new DollarAmount(itemStateTax));
              
                report.add(data);
            }
        }
        return report;
    }
}
