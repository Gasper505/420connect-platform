package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.SplitPayment;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.DailySummary;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class DailySummaryGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;

    private ArrayList<String> reportHeaders;
    private Map<String, GathererReport.FieldType> fields = new LinkedHashMap<>();
    private ReportFilter filter;

    public DailySummaryGatherer() {
    }

    @Override
    public GathererReport gather(ReportFilter filter) {


        Iterable<Transaction> transactions = transactionRepository.getAllBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        // Build unique set
        HashSet<Cart.PaymentOption> customPaymentOptions = new HashSet<>();
        for (Transaction transaction : transactions) {
            switch (transaction.getCart().getPaymentOption()) {
                case Cash:
                case Check:
                case Credit:
                case StoreCredit:
                case Split:
                case CashlessATM:
                    // Skip these, they're already explicitly defined.
                    break;
                default:
                    // Everything else is considered custom
                    customPaymentOptions.add(transaction.getCart().getPaymentOption());
            }
        }

        fields.put("Date", GathererReport.FieldType.STRING);
        fields.put("New Members", GathererReport.FieldType.NUMBER);
        fields.put("Unique Members", GathererReport.FieldType.NUMBER);
        fields.put("Total Visits", GathererReport.FieldType.NUMBER);
        fields.put("# Trans", GathererReport.FieldType.NUMBER);
        fields.put("# Refunds", GathererReport.FieldType.NUMBER);

        fields.put("# Cash", GathererReport.FieldType.NUMBER);
        fields.put("# Check", GathererReport.FieldType.NUMBER);
        fields.put("# Credit", GathererReport.FieldType.NUMBER);
        fields.put("# Store Credit", GathererReport.FieldType.NUMBER);
        fields.put("# Split", GathererReport.FieldType.NUMBER);
        fields.put("# Cashless", GathererReport.FieldType.NUMBER);

        fields.put("Gross Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Cash Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Check Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Credit Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Store Credit Receipts", GathererReport.FieldType.CURRENCY);
        customPaymentOptions.forEach(k -> fields.put(String.format("%s Receipts", k.toString()), GathererReport.FieldType.CURRENCY));
        fields.put("Split Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Cashless Receipts", GathererReport.FieldType.CURRENCY);


        fields.put("Avg Gross Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Avg Cash Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Avg Check Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Avg Credit Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Avg Store Credit Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Avg Split Receipts", GathererReport.FieldType.CURRENCY);
        fields.put("Avg Cashless Receipts", GathererReport.FieldType.CURRENCY);


        fields.put("Total Discounts", GathererReport.FieldType.CURRENCY);
        fields.put("After Tax Discount", GathererReport.FieldType.CURRENCY);
        fields.put("Total Refunds Amt", GathererReport.FieldType.CURRENCY);
        fields.put("Total COGS", GathererReport.FieldType.CURRENCY);

        fields.put("Total Sales", GathererReport.FieldType.CURRENCY);

        fields.put("Rec Sales", GathererReport.FieldType.CURRENCY);
        fields.put("MMIC Sales", GathererReport.FieldType.CURRENCY);
        fields.put("Third Party Sales", GathererReport.FieldType.CURRENCY);


        fields.put("Delivery Fees", GathererReport.FieldType.CURRENCY);
        fields.put("Credit Card Fees", GathererReport.FieldType.CURRENCY);

        fields.put("Pre AL excise Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Pre NAL excise Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Pre County Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Pre State Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Pre City Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Pre Fedral Tax", GathererReport.FieldType.CURRENCY);

        fields.put("Post AL Excise Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Post NAL Excise Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Post County Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Post State Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Post City Tax", GathererReport.FieldType.CURRENCY);
        fields.put("Post Fedral Tax", GathererReport.FieldType.CURRENCY);

        fields.put("Total Tax", GathererReport.FieldType.CURRENCY);

        reportHeaders = Lists.newArrayList(fields.keySet());

        GathererReport report = new GathererReport(filter, "Daily Summary", reportHeaders);
        this.filter = filter;
        report.setReportPostfix(GathererReport.TIMESTAMP);
        report.setReportFieldTypes(fields);

        HashMap<Long, DailySummary> days = new HashMap<>();

        Iterable<Member> newMembers = memberRepository.getMembersWithStartDate(filter.getCompanyId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, ProductBatch> recentBatchMap = getProductBatchByProductId(productBatchHashMap);



        ArrayList<DailySummary> summaries = getDailySummaries(days, newMembers, transactions, productBatchHashMap, prepackageHashMap, productItemHashMap, toleranceHashMap, recentBatchMap, customPaymentOptions, false);
        Collections.sort(summaries);
        for (DailySummary ds : summaries) {
            report.add(ds.getData(reportHeaders));
        }

        return report;
    }

    public ArrayList<DailySummary> getDailySummaries(HashMap<Long, DailySummary> days,
                                                     Iterable<Member> newMembers,
                                                     Iterable<Transaction> transactions,
                                                     HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageHashMap,
                                                     HashMap<String, PrepackageProductItem> prepackageProductItemMap,
                                                     HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                     HashMap<String, ProductBatch> recentBatchMap,
                                                     HashSet<Cart.PaymentOption> customPaymentOptions,
                                                     boolean calculateItems) {
        ArrayList<DailySummary> list = new ArrayList<>();
        for (Transaction sale : transactions) {

            Long key = getKey(sale);
            DailySummary ds;
            if (days.containsKey(key)) {
                ds = days.get(key);
            } else {
                ds = new DailySummary(key, customPaymentOptions);
            }
            int factor = 1;
            if (Transaction.TransactionType.Refund.equals(sale.getTransType()) && Cart.RefundOption.Retail.equals(sale.getCart().getRefundOption())) {
                factor = -1;
            }

            if (Transaction.TransactionStatus.Canceled != sale.getStatus() && sale.getCart().getItems().size() > 0) {
                Cart.PaymentOption payment = sale.getCart().getPaymentOption();


                double total = NumberUtils.round(sale.getCart().getTotal().doubleValue() * factor, 2);
                double creditCardFees = 0d;
                double deliveryFees = 0d;
                double afterTaxDiscount = 0d;
                double totalDiscounts = 0d;
                double changeDue = 0d;


                if (sale.getCart().getTotalDiscount().doubleValue() != 0) {
                    totalDiscounts += NumberUtils.round(sale.getCart().getTotalDiscount().doubleValue() * factor, 2);
                }

                if (sale.getCart().getAppliedAfterTaxDiscount() != null && sale.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                    afterTaxDiscount = sale.getCart().getAppliedAfterTaxDiscount().doubleValue() * factor;
                }

                if (sale.getCart().getCreditCardFee() != null && sale.getCart().getCreditCardFee().doubleValue() > 0) {
                    creditCardFees += sale.getCart().getCreditCardFee().doubleValue() * factor;
                }

                if (sale.getCart().getDeliveryFee() != null && sale.getCart().getDeliveryFee().doubleValue() > 0) {
                    deliveryFees += sale.getCart().getDeliveryFee().doubleValue() * factor;
                }

                if (sale.getCart().getChangeDue() != null && sale.getCart().getChangeDue().doubleValue() > 0) {
                    changeDue += sale.getCart().getChangeDue().doubleValue();
                }

                double preALExciseTax = 0;
                double preNALExciseTax = 0;
                double preCountyTax = 0;
                double preStateTax = 0;
                double preCityTax = 0;
                double preFedralTax = 0;

                double postALExciseTax = 0;
                double postNALExciseTax = 0;
                double postCountyTax = 0;
                double postStateTax = 0;
                double postCityTax = 0;
                double postFedralTax = 0;

                double postTotalTax = 0;
                double preTotalTax = 0;

                if (sale.getCart().getTaxResult().getTotalALExciseTax() != null) {
                    preALExciseTax += sale.getCart().getTaxResult().getTotalALExciseTax().doubleValue() * factor;
                }
                if (sale.getCart().getTaxResult().getTotalNALPreExciseTax() != null) {
                    preNALExciseTax = sale.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;
                }

                if (sale.getCart().getTaxResult().getTotalCountyPreTax() != null) {
                    preCountyTax = sale.getCart().getTaxResult().getTotalCountyPreTax().doubleValue() * factor;
                }
                if (sale.getCart().getTaxResult().getTotalStatePreTax() != null) {
                    preStateTax = sale.getCart().getTaxResult().getTotalStatePreTax().doubleValue() * factor;
                }
                if (sale.getCart().getTaxResult().getTotalCityPreTax() != null) {
                    preCityTax = sale.getCart().getTaxResult().getTotalCityPreTax().doubleValue() * factor;
                }

                if (sale.getCart().getTaxResult().getTotalFedPreTax() != null) {
                    preFedralTax = sale.getCart().getTaxResult().getTotalFedPreTax().doubleValue() * factor;
                }

                if (sale.getCart().getTaxResult().getTotalALPostExciseTax() != null) {
                    postALExciseTax = sale.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;

                }
                if (sale.getCart().getTaxResult().getTotalExciseTax() != null) {
                    postNALExciseTax = sale.getCart().getTaxResult().getTotalExciseTax().doubleValue() * factor;

                }

                if (sale.getCart().getTaxResult().getTotalCountyTax() != null) {
                    postCountyTax = sale.getCart().getTaxResult().getTotalCountyTax().doubleValue() * factor;
                }

                if (sale.getCart().getTaxResult().getTotalStateTax() != null) {
                    postStateTax = sale.getCart().getTaxResult().getTotalStateTax().doubleValue() * factor;
                }
                if (sale.getCart().getTaxResult().getTotalCityTax() != null) {
                    postCityTax = sale.getCart().getTaxResult().getTotalCityTax().doubleValue() * factor;

                }
                if (sale.getCart().getTaxResult().getTotalFedTax() != null) {
                    postFedralTax = sale.getCart().getTaxResult().getTotalFedTax().doubleValue() * factor;
                }

                if (sale.getCart().getTaxResult().getTotalPostCalcTax() != null) {
                    postTotalTax = sale.getCart().getTaxResult().getTotalPostCalcTax().doubleValue() * factor;
                }
                if (sale.getCart().getTaxResult().getTotalPreCalcTax() != null) {
                    preTotalTax = sale.getCart().getTaxResult().getTotalPreCalcTax().doubleValue() * factor;
                }

                if (sale.getTransType() == Transaction.TransactionType.Refund
                        && sale.getCart().getRefundOption() == Cart.RefundOption.Void
                        && sale.getCart().getSubTotal().doubleValue() == 0) {
                    creditCardFees = 0;
                    deliveryFees = 0;
                    afterTaxDiscount = 0;
                    total = 0;
                }

                updateSalesTotals(ds, total, payment, sale.getCart().getSplitPayment());

                if (sale.getTransType().equals(Transaction.TransactionType.Refund)) {
                    ds.totalRefunds += sale.getCart().getRefundAmount().doubleValue() * factor;
                    ds.refunds++;
                }
                ds.gross += total;
                ds.totalVisits++;

                ds.creditFees += NumberUtils.round(creditCardFees, 2);
                ds.deliveryFees += NumberUtils.round(deliveryFees, 2);
                ds.afterTaxDiscount += NumberUtils.round(afterTaxDiscount, 2);
                ds.totalDiscounts += NumberUtils.round(totalDiscounts, 2);

                ds.preALExciseTax += NumberUtils.round(preALExciseTax, 2);
                ds.preNAlExciseTax += NumberUtils.round(preNALExciseTax, 2);
                ds.preCountyTax += NumberUtils.round(preCountyTax, 2);
                ds.preStateTax += NumberUtils.round(preStateTax, 2);
                ds.preCityTax += NumberUtils.round(preCityTax, 2);
                ds.preFedralTax += NumberUtils.round(preFedralTax, 2);

                ds.postALExciseTax += NumberUtils.round(postALExciseTax, 2);
                ds.postNALExciseTax += NumberUtils.round(postNALExciseTax, 2);
                ds.postCountyTax += NumberUtils.round(postCountyTax, 2);
                ds.postStateTax += NumberUtils.round(postStateTax, 2);
                ds.postCityTax += NumberUtils.round(postCityTax, 2);
                ds.postFedralTax += NumberUtils.round(postFedralTax, 2);

                ds.postTotalTax += NumberUtils.round(postTotalTax, 2);
                ds.preTotalTax += NumberUtils.round(preTotalTax, 2);
                double totalPreTx = ds.preTotalTax + ds.preALExciseTax + ds.preNAlExciseTax;
                ds.preTotalTax = NumberUtils.roundToNearestCent(totalPreTx);

                if (calculateItems) {
                    double totalQty = 0;
                    for (OrderItem orderItem : sale.getCart().getItems()) {
                        totalQty += orderItem.getQuantity().doubleValue();
                    }
                    if (sale.getTransType() == Transaction.TransactionType.Refund) {
                        ds.refundItems += totalQty;
                        ds.refundCount++;
                    } else {
                        ds.salesItems += totalQty;
                        ds.salesCount++;
                    }
                }
            }

            if (Transaction.TransactionStatus.Canceled == sale.getStatus() || Transaction.TransactionStatus.Completed == sale.getStatus()) {
                ds.totalAllVisits++;
            }
            if (Transaction.TransactionStatus.Canceled != sale.getStatus()) {
                ds.cogs += calculateTotalCogs(sale, productBatchMap, prepackageHashMap, prepackageProductItemMap, toleranceHashMap, recentBatchMap, factor);
                double sales = (sale.getCart() == null) ? 0.0 : sale.getCart().getSubTotal().doubleValue() * factor;

                sales = NumberUtils.round(sales, 2);
                ds.subTotalSales += sales;

                if (sale.getCart() != null) {
                    double tax = 0;
                    if (sale.getCart().getTaxResult() != null) {
                        tax = sale.getCart().getTaxResult().getTotalPostCalcTax().doubleValue() * factor;
                    } else {
                        tax = (sale.getCart().getTotalCalcTax().doubleValue() - sale.getCart().getTotalPreCalcTax().doubleValue()) * factor;
                    }
                    tax = NumberUtils.round(tax, 2);
                    ds.totalTax += tax;
                    switch (sale.getCart().getFinalConsumerTye()) {
                        case AdultUse:
                            ds.recSales += sales;
                            ds.totalRecTax += tax;
                            break;
                        case MedicinalState:
                            ds.mmicSales += sales;
                            ds.totalMedTax += tax;
                            break;
                        case MedicinalThirdParty:
                            ds.tpSales += sales;
                            ds.totalMedTax += tax;
                            break;
                        default:
                            break;
                    }
                }
            }
            ds.memberList.add(sale.getMemberId());
            days.put(key, ds);



            // calculate summary
            DailySummary.EmployeeStats employeeStats = ds.employeeStatsMap.get(sale.getSellerId());
            if (employeeStats == null) {
                employeeStats = new DailySummary.EmployeeStats();
                employeeStats.sellerId = sale.getSellerId();
                ds.employeeStatsMap.put(sale.getSellerId(), employeeStats);
            }

            if (Transaction.TransactionStatus.Completed == sale.getStatus()) {
                employeeStats.sales += NumberUtils.round(sale.getCart().getTotal().doubleValue(), 2);
                employeeStats.salesCount += 1;
            } else if (sale.getStatus() == Transaction.TransactionStatus.RefundWithInventory
                    || sale.getStatus() == Transaction.TransactionStatus.RefundWithoutInventory) {
                employeeStats.refunds += NumberUtils.round(sale.getCart().getTotal().doubleValue(), 2);
                employeeStats.refundCount += 1;
            }
        }

        for (Member m : newMembers) {
            Long key = getKey(m);
            DailySummary ds;
            if (days.containsKey(key)) {
                ds = days.get(key);
            } else {
                ds = new DailySummary(key, customPaymentOptions);
            }
            ds.newMembers++;
            ds.memberList.remove(m.getId());
            days.put(key, ds);
        }
        for (DailySummary ds : days.values()) {
           // Remove employee from daily summary employee list who has salesCount = 0 and refundCount = 0
            HashMap<String, DailySummary.EmployeeStats> employeeStatsMap = new HashMap<>();
            for (DailySummary.EmployeeStats employeeStats : ds.employeeStatsMap.values()) {
                if (employeeStats.salesCount == 0 && employeeStats.refundCount == 0) {
                    continue;
                }
                employeeStatsMap.put(employeeStats.sellerId, employeeStats);
            }

            ds.employeeStatsMap = employeeStatsMap;
            list.add(ds);
        }
        return list;
    }

    private void updateSalesTotals(DailySummary ds, Double total, Cart.PaymentOption payment, SplitPayment splitPayment) {
        total = NumberUtils.round(total, 2);
        switch (payment) {
            case Cash:
                ds.cashSales += total;
                ds.cashCount++;
                break;
            case Credit:
                ds.creditSales += total;
                ds.creditcount++;
                break;
            case Check:
                ds.checkSales += total;
                ds.checkCount++;
                break;
            case StoreCredit:
                ds.storeCreditSales += total;
                ds.storeCreditCount++;
                break;
            case Split:
                ds.splitSales += total;
                ds.splitCount++;
                if (splitPayment != null) {
                    ds.cashSales += splitPayment.getCashAmt().doubleValue();
                    ds.creditSales += splitPayment.getCreditDebitAmt().doubleValue();
                    ds.checkSales += splitPayment.getCheckAmt().doubleValue();
                    ds.storeCreditSales += splitPayment.getStoreCreditAmt().doubleValue();
                    ds.cashlessAtmSales += splitPayment.getCashlessAmt().doubleValue();

                    BigDecimal splitChanged = splitPayment.getTotalSplits().subtract(BigDecimal.valueOf(total));

                    if (splitChanged.compareTo(new BigDecimal(0)) > 0) {
                        ds.cashSales -= splitChanged.doubleValue();
                    }
                }
                break;
            case None:
                System.out.println("hello");
                break;
            case CashlessATM:
                ds.cashlessAtmSales += total;
                ds.cashlessCount++;
                break;
            default:
                ds.customPaymentTotals.putIfAbsent(payment,
                        ds.customPaymentTotals.getOrDefault(payment, new BigDecimal(0))
                                .add(new BigDecimal(total)));
                break;
        }
    }

    private Long getKey(Transaction t) {
        if (filter != null) {
            return new DateTime(t.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset()).getMillis();
        } else {
            return 0l;
        }
    }

    private Long getKey(Member m) {
        if (filter != null) {
            return new DateTime(m.getStartDate()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset()).getMillis();
        }
        return 0l;
    }

    private Double calculateTotalCogs(Transaction transaction, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, HashMap<String, ProductBatch> recentBatchMap, double factor) {
        Double totalCogs = 0.0;
        if (transaction.getCart() == null || transaction.getCart().getItems() == null) {
            return totalCogs;
        }
        for (OrderItem item : transaction.getCart().getItems()) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(item.getPrepackageItemId());
            boolean calculate = false;
            if (prepackageProductItem != null) {
                ProductBatch batch = productBatchMap.get(prepackageProductItem.getBatchId());
                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                if (batch != null && prepackage != null) {
                    BigDecimal unitValue = prepackage.getUnitValue();
                    if (unitValue == null || unitValue.doubleValue() == 0) {
                        ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                        unitValue = weightTolerance.getUnitValue();
                    }
                    double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                    double unitPrice = batch.getFinalUnitCost().doubleValue();
                    totalCogs += unitPrice * unitsSold;
                    calculate = true;
                }
            } else if (item.getQuantityLogs() != null && !item.getQuantityLogs().isEmpty()) {
                for (QuantityLog log : item.getQuantityLogs()) {
                    ProductBatch batch = productBatchMap.get(log.getBatchId());
                    if (batch != null) {
                        double unitsSold = log.getQuantity().doubleValue();
                        double unitPrice = batch.getFinalUnitCost().doubleValue();
                        totalCogs += unitPrice * unitsSold;
                        calculate = true;
                    }
                }
            }
            if (!calculate) {
                ProductBatch batch = recentBatchMap.get(item.getProductId());
                BigDecimal unitPrice = (batch == null) ? BigDecimal.ZERO : batch.getFinalUnitCost();
                totalCogs += item.getQuantity().multiply(unitPrice).doubleValue();
            }
        }
        return totalCogs * factor;
    }

    /**
     * This is private method to get latest product batch by product id
     *
     * @return
     */
    private HashMap<String, ProductBatch> getProductBatchByProductId(HashMap<String, ProductBatch> productBatchHashMap) {
        HashMap<String, ProductBatch> returnMap = new HashMap<>();

        for (String key : productBatchHashMap.keySet()) {
            ProductBatch batch = productBatchHashMap.get(key);
            if (returnMap.containsKey(batch.getProductId())) {
                ProductBatch mapBatch = returnMap.get(batch.getProductId());
                DateTime batchDateTime = new DateTime(batch.getCreated());
                if (batchDateTime.isBefore(mapBatch.getCreated())) {
                    batch = mapBatch;
                }
            }
            returnMap.put(batch.getProductId(), batch);
        }

        return returnMap;
    }
}
