package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * Created by Stephen Schmidt on 7/8/2016.
 */
public class DiscountGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    MemberRepository memberRepository;


    private String[] attrs = new String[]{"Transaction No.", "Member", "Employee", "Total Discounts", "After Tax Discount", "Gross Receipts"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public DiscountGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Discount Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());


        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));

        int factor = 1;
        for (Transaction t : results) {
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Cart cart = t.getCart();
            Member member = memberHashMap.get(t.getMemberId());

            if (member != null && cart != null) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], t.getTransNo());
                data.put(attrs[1], member.getFirstName() + " " + member.getLastName());
                Employee em = employeeMap.get(t.getSellerId());
                if (em != null) {
                    data.put(attrs[2], em.getFirstName() + " " + em.getLastName());
                } else {
                    data.put(attrs[2], "Unknown seller");
                }
                data.put(attrs[3], new DollarAmount(cart.getTotalDiscount().doubleValue() * factor));
                data.put(attrs[4], new DollarAmount(cart.getAppliedAfterTaxDiscount() != null ? (cart.getAppliedAfterTaxDiscount().doubleValue() * factor) : 0));
                data.put(attrs[5], new DollarAmount(cart.getTotal().doubleValue() * factor));
                report.add(data);
            }
        }
        return report;
    }
}
