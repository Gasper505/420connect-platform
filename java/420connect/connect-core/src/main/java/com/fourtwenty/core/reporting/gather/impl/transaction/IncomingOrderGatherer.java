package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class IncomingOrderGatherer implements Gatherer {

    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private TerminalRepository terminalRepository;

    private String[] attrs = new String[]{
            "Date", //0
            "Consumer", //1
            "Order Status", //2
            "Transaction No.", //3
            "Order Type", //4
            "Trans Type", //5
            "Trans Status", //6
            "Consumer Tax Type", //7
            "COGs", //8
            "Retail Value", //9
            "Discounts", //10
            "Pre AL Excise Tax", //11
            "Pre NAL Excise Tax", //12
            "Post AL Excise Tax", //13
            "Post NAL Excise Tax", //14
            "City Tax", //15
            "County Tax", //16
            "State Tax", //17
            "Total Tax", //18
            "Delivery Fees", //19
            "Credit/Debit Card Fees", //20
            "After Tax Discount", //21
            "Gross Receipt", //22
            "Employee", //23
            "Terminal", //24
            "Payment Type", //25
            "Promotion(s)", //26
            "Marketing Source" //27
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public IncomingOrderGatherer() {

        Collections.addAll(reportHeaders, attrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Date 0
                GathererReport.FieldType.STRING, // Consumer 1
                GathererReport.FieldType.STRING, // Order Type 2
                GathererReport.FieldType.STRING, // transNo 3
                GathererReport.FieldType.STRING, // Order Status 4
                GathererReport.FieldType.STRING, // type 5
                GathererReport.FieldType.STRING, // status 6
                GathererReport.FieldType.STRING, // Consumer Tax Type 7
                GathererReport.FieldType.CURRENCY, // cogs 8
                GathererReport.FieldType.CURRENCY, // subtotal  9
                GathererReport.FieldType.CURRENCY, // discounts 10
                GathererReport.FieldType.CURRENCY, // preALExciseTax 11
                GathererReport.FieldType.CURRENCY, // preNALExciseTax   12
                GathererReport.FieldType.CURRENCY, // postALExciseTax   13
                GathererReport.FieldType.CURRENCY, // postNALExciseTax  14
                GathererReport.FieldType.CURRENCY, // cityTax   15
                GathererReport.FieldType.CURRENCY, // countyTax 16
                GathererReport.FieldType.CURRENCY, // stateTax  17
                GathererReport.FieldType.CURRENCY, // post taxes    18
                GathererReport.FieldType.CURRENCY, // delivery fees 19
                GathererReport.FieldType.CURRENCY, // credit card fees  20
                GathererReport.FieldType.CURRENCY, // afterTaxDiscount  21
                GathererReport.FieldType.NUMBER, // total sale  22
                GathererReport.FieldType.STRING, // employee    23
                GathererReport.FieldType.STRING, // term    24
                GathererReport.FieldType.STRING, // payment type    25
                GathererReport.FieldType.STRING, // promotions 26
                GathererReport.FieldType.STRING}; // Marketing source 27
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Incoming Order Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<ConsumerCart> consumerCarts = consumerCartRepository.getBracketConsumerCart(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Promotion> promotionMap = promotionRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, LoyaltyReward> rewardMap = rewardRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        List<ConsumerCart> consumerCartList = new ArrayList<>();
        Set<ObjectId> consumerUserIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        List<ObjectId> transactionId = new ArrayList<>();
        String notAvailable = "N/A";
        for (ConsumerCart consumerCart : consumerCarts) {

            if (StringUtils.isNotBlank(consumerCart.getConsumerId()) && ObjectId.isValid(consumerCart.getConsumerId())) {
                consumerUserIds.add(new ObjectId(consumerCart.getConsumerId()));
            }
            if (consumerCart.getCart() != null && !CollectionUtils.isNullOrEmpty(consumerCart.getCart().getItems())) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    if (StringUtils.isNotBlank(orderItem.getProductId()) && ObjectId.isValid(orderItem.getProductId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
            if (StringUtils.isNotBlank(consumerCart.getTransactionId()) && ObjectId.isValid(consumerCart.getTransactionId())) {
                transactionId.add(new ObjectId(consumerCart.getTransactionId()));
            }
            consumerCartList.add(consumerCart);
        }

        HashMap<String, Transaction> transactionMap = transactionRepository.listAsMap(filter.getCompanyId(), transactionId);
        Iterable<Product> products = productRepository.list(filter.getCompanyId(), Lists.newArrayList(productIds));

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.putIfAbsent(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, ConsumerUser> consumerUserMap = consumerUserRepository.listAsMap(Lists.newArrayList(consumerUserIds));

        for (ConsumerCart consumerCart : consumerCartList) {

            ConsumerUser consumerUser = consumerUserMap.get(consumerCart.getConsumerId());

            if (consumerUser == null) {
                continue;
            }

            Transaction transaction = transactionMap.get(consumerCart.getTransactionId());

            Cart cart = consumerCart.getCart();

            String consumerTaxType = "N/A";
            if (cart.getTaxTable() != null) {
                consumerTaxType = cart.getTaxTable().getName();
            }

            double totalPostTax = 0.0;

            totalPostTax = cart.getTotalCalcTax().doubleValue() - cart.getTotalPreCalcTax().doubleValue();
            if (totalPostTax < 0) {
                totalPostTax = 0;
            }
            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;
            double preALExciseTax = 0d;
            double preNALExciseTax = 0d;
            double postALExciseTax = 0d;
            double postNALExciseTax = 0d;
            double afterTaxDiscount = 0d;
            if (cart.getTaxResult() != null) {
                postALExciseTax = cart.getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = cart.getTaxResult().getTotalExciseTax().doubleValue();
                preALExciseTax = cart.getTaxResult().getTotalALExciseTax().doubleValue();
                preNALExciseTax = cart.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                cityTax = cart.getTaxResult().getTotalCityTax().doubleValue();
                countyTax = cart.getTaxResult().getTotalCountyTax().doubleValue();
                stateTax = cart.getTaxResult().getTotalStateTax().doubleValue();
            }

            if (cart.getAppliedAfterTaxDiscount() != null && cart.getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = cart.getAppliedAfterTaxDiscount().doubleValue();
            }

            double creditCardFees = 0;
            if (cart.getCreditCardFee().doubleValue() > 0) {
                creditCardFees = cart.getCreditCardFee().doubleValue();
            }

            double deliveryFees = 0;
            if (cart.getDeliveryFee().doubleValue() > 0) {
                deliveryFees = cart.getDeliveryFee().doubleValue();
            }


            //Getting promotions applied for each transaction
            LinkedHashSet<PromotionReq> promos = cart.getPromotionReqs();
            StringBuilder promotions = new StringBuilder();
            Iterator<PromotionReq> it = promos.iterator();

            while (it.hasNext()) {
                PromotionReq promotionReq = it.next();
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                    if (promotion != null) {
                        promotions.append(promotion.getName());
                        if (it.hasNext()) {
                            promotions.append("; ");
                        }
                    }
                } else {
                    LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                    if (reward != null) {
                        promotions.append(reward.getName());
                        if (it.hasNext()) {
                            promotions.append("; ");
                        }
                    }
                }
            }

            double transCogs = 0;
            for (OrderItem item : cart.getItems()) {

                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                boolean calculated = false;
                double itemCogs = 0;
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        itemCogs += calcCOGS(unitsSold, targetBatch);

                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                itemCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                            }
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    itemCogs = unitCost * item.getQuantity().doubleValue();
                }
                transCogs += itemCogs;
            }

            double total = cart.getTotal().doubleValue();

            Employee emp = employeeMap.get(transaction != null ? transaction.getSellerId() : null);
            String employeeName = notAvailable;
            if (emp != null) {
                employeeName = emp.getFirstName() + " " + emp.getLastName();
            }
            Terminal t = terminalMap.get(transaction != null ? transaction.getSellerTerminalId() : null);
            String terminalName = notAvailable;
            if (t != null) {
                terminalName = t.getName();
            }

            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(consumerCart.getOrderPlacedTime(), filter.getTimezoneOffset()));
            data.put(attrs[1], consumerUser.getFirstName() + " " + consumerUser.getLastName());
            data.put(attrs[2], getConsumerCartStatus(consumerCart.getCartStatus()));
            data.put(attrs[3], TextUtil.textOrEmpty(consumerCart.getTransNo()));
            data.put(attrs[4], (consumerCart.getPickupType() == null) ? "N/A" : consumerCart.getPickupType());
            data.put(attrs[5], transaction != null ? transaction.getTransType() : notAvailable);
            data.put(attrs[6], transaction != null ? transaction.getStatus() : notAvailable);
            data.put(attrs[7], consumerTaxType);
            data.put(attrs[8], new DollarAmount(transCogs)); //cogs
            data.put(attrs[9], new DollarAmount(cart.getSubTotal().doubleValue()));
            data.put(attrs[10], new DollarAmount(cart.getTotalDiscount().doubleValue()));
            data.put(attrs[11], new DollarAmount(preALExciseTax));
            data.put(attrs[12], new DollarAmount(preNALExciseTax));
            data.put(attrs[13], new DollarAmount(postALExciseTax));
            data.put(attrs[14], new DollarAmount(postNALExciseTax));
            data.put(attrs[15], new DollarAmount(cityTax));
            data.put(attrs[16], new DollarAmount(countyTax));
            data.put(attrs[17], new DollarAmount(stateTax));
            data.put(attrs[18], new DollarAmount(totalPostTax));
            data.put(attrs[19], new DollarAmount(deliveryFees));
            data.put(attrs[20], new DollarAmount(creditCardFees));
            data.put(attrs[21], new DollarAmount(afterTaxDiscount));
            data.put(attrs[22], new DollarAmount(total));
            data.put(attrs[23], employeeName);
            data.put(attrs[24], terminalName);
            data.put(attrs[25], cart.getPaymentOption());
            data.put(attrs[26], StringUtils.isNotEmpty(promotions.toString()) ? promotions.toString() : notAvailable);
            data.put(attrs[27], StringUtils.isNotEmpty(consumerUser.getMarketingSource()) ? consumerUser.getMarketingSource() : notAvailable);

            report.add(data);
        }

        return report;
    }

    private String getConsumerCartStatus(ConsumerCart.ConsumerCartStatus cartStatus) {
        String status = "N/A";
        switch (cartStatus) {
            case Placed:
                status = ConsumerCart.ConsumerCartStatus.Placed.toString();
                break;
            case Accepted:
                status = ConsumerCart.ConsumerCartStatus.Accepted.toString();
                break;
            case Completed:
                status = ConsumerCart.ConsumerCartStatus.Completed.toString();
                break;
            case Declined:
                status = ConsumerCart.ConsumerCartStatus.Declined.toString();
                break;
            case CanceledByConsumer:
                status = "Canceled by consumer";
                break;
            case CanceledByDispensary:
                status = "Canceled by dispensary";
                break;
        }
        return status;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}
