package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.SplitPayment;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class JournalEntryGatherer implements Gatherer {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ProductBatchRepository batchRepository;

    private ReportFilter filter;

    private String[] attrs = new String[]{
            "Discounts",
            "Adult Sales",
            "Medical Sales",
            "Non-cannabis Sales",
            "Adult Discounts",
            "Medical Discounts",
            "Non-cannabis Discounts",
            "Adult City Tax",
            "Medical City Tax",
            "Non-cannabis City Tax",
            "Adult Sales Tax",
            "Medical Sales Tax",
            "Non-cannabis Sales Tax",
            "Adult County Tax",
            "Medical County Tax",
            "Non-cannabis County Tax",
            "Adult Federal Tax",
            "Medical Federal Tax",
            "Non-cannabis Federal Tax",
            "Delivery Fees",
            "Credit Card Fees",
            "Adult Inventory Product",
            "Adult Inventory Excise Tax",
            "Medical Inventory Product",
            "Medical Inventory Excise Tax",
            "Non-cannabis COGS Product",
            "COGS Inventory",
            "COGS Inventory Excise"};


    public JournalEntryGatherer() {
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        this.filter = filter;
        MathContext precision = new MathContext(3); // precision for 3 digit
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> categoryIds = new HashSet<>();
        Set<String> paymentOptions = new HashSet<>();

        for (Transaction transaction : transactions) {
            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                productIds.add(new ObjectId(item.getProductId()));
            }
            if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Split) {
                paymentOptions.addAll(Lists.newArrayList(Cart.PaymentOption.Cash.toString(),
                Cart.PaymentOption.Credit.toString(),
                Cart.PaymentOption.Check.toString(),
                Cart.PaymentOption.StoreCredit.toString(),
                Cart.PaymentOption.CashlessATM.toString()));
            } else {
                paymentOptions.add(String.valueOf(transaction.getCart().getPaymentOption()));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds));

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, ProductBatch> productBatchMap = batchRepository.listAsMap(filter.getCompanyId());
        Map<String, ProductBatch> recentBatchMap = batchRepository.getLatestBatchForProductList(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet()));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds));


        HashMap<Long, JournalReport> days = new HashMap<>();


        for (Transaction transaction : transactions) {
            Long key = getKey(transaction);
            JournalReport journal;

            days.putIfAbsent(key, new JournalReport());
            journal = days.get(key);

            journal.day = journal.day == 0 ? key : journal.day;

            if (Transaction.TransactionType.Refund.equals(transaction.getTransType()) && Cart.RefundOption.Retail.equals(transaction.getCart().getRefundOption())) {
                prepareSalesMap(transaction.getCart().getTotal(), journal.refundSalesMap, transaction.getCart().getPaymentOption(), transaction.getCart().getSplitPayment());
                continue;
            }

            TaxResult taxResult = transaction.getCart().getTaxResult();
            BigDecimal totalCityTax = BigDecimal.ZERO;
            BigDecimal totalExciseTax = BigDecimal.ZERO;
            BigDecimal totalStateTax = BigDecimal.ZERO;
            BigDecimal totalCountyTax = BigDecimal.ZERO;
            BigDecimal totalFederalTax = BigDecimal.ZERO;
            BigDecimal afterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount() : BigDecimal.ZERO;

            if (taxResult != null) {
                totalCityTax = taxResult.getTotalCityTax();
                totalExciseTax = (taxResult.getTotalALPostExciseTax().add(taxResult.getTotalExciseTax()));
                totalStateTax = taxResult.getTotalStateTax();
                totalCountyTax = taxResult.getTotalCountyTax();
                totalFederalTax = taxResult.getTotalFedTax();
            }

            BigDecimal total = BigDecimal.ZERO;
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total = total.add(orderItem.getFinalPrice());
                }

            }

            BigDecimal adultAccessSales, medAccessSales, accessCogs;
            BigDecimal adultAccessDis, medAccessDis;
            BigDecimal accessCityTax, accessStateTax, accessFederalTax, accessCountyTax;
            BigDecimal adultAccessCityTax, adultAccessStateTax, adultAccessCountyTax, adultAccessFedTax;
            BigDecimal medAccessCityTax, medAccessStateTax, medAccessCountyTax, medAccessFedTax;
            BigDecimal adultAccessCogs, medAccessCogs;

            accessCogs = adultAccessSales = medAccessSales = adultAccessDis = medAccessDis = accessCityTax = accessStateTax = accessFederalTax = accessCountyTax = adultAccessFedTax = medAccessFedTax = BigDecimal.ZERO;
            adultAccessCityTax = adultAccessStateTax = BigDecimal.ZERO;
            adultAccessCountyTax = medAccessCountyTax = BigDecimal.ZERO;
            medAccessCityTax = medAccessStateTax = BigDecimal.ZERO;
            adultAccessCogs = medAccessCogs = BigDecimal.ZERO;

            BigDecimal itemDiscount = BigDecimal.ZERO;
            for (OrderItem item : transaction.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                ProductCategory category = categoryMap.get(product.getCategoryId());
                if (category == null) {
                    continue;
                }

                itemDiscount = itemDiscount.add(item.getCalcDiscount());

                boolean isCannabis = (product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                        || (product.getCannabisType() != Product.CannabisType.CBD
                        && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                        && product.getCannabisType() != Product.CannabisType.DEFAULT);


                if (isCannabis) {
                    continue; // accessories sales not include cannabis product
                }

                BigDecimal ratio = BigDecimal.ONE;
                if (total.doubleValue() != 0) {
                    ratio = item.getFinalPrice().divide(total, precision);
                }

                BigDecimal propDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().multiply(ratio) : BigDecimal.ZERO;

                BigDecimal totalAccessTax = BigDecimal.ZERO;
                BigDecimal subTotalAfterDiscount = item.getFinalPrice().subtract(propDiscount);

                if (item.getTaxResult() != null) {
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        accessCityTax = accessCityTax.add(item.getTaxResult().getTotalCityTax());
                        accessStateTax = accessStateTax.add(item.getTaxResult().getTotalStateTax());
                        accessFederalTax = accessFederalTax.add(item.getTaxResult().getTotalFedTax());
                        accessCountyTax = accessCountyTax.add(item.getTaxResult().getTotalCountyTax());

                        totalAccessTax = item.getTaxResult().getTotalPostCalcTax();
                    }
                }

                if (taxResult == null && totalAccessTax.equals(BigDecimal.ZERO)) {
                    if ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null) {
                        TaxInfo taxInfo = item.getTaxInfo();
                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            BigDecimal accessoriesCityTax = subTotalAfterDiscount.multiply(taxInfo.getCityTax());
                            BigDecimal accessoriesStateTax = subTotalAfterDiscount.multiply(taxInfo.getStateTax());
                            BigDecimal accessoriesFederalTax = subTotalAfterDiscount.multiply(taxInfo.getFederalTax());

                            accessCityTax = accessCityTax.add(accessoriesCityTax);
                            accessStateTax = accessStateTax.add(accessoriesStateTax);
                            accessFederalTax = accessFederalTax.add(accessoriesFederalTax);
                        }
                    }
                }

                if (transaction.getCart().getFinalConsumerTye() == ConsumerType.AdultUse) {
                    adultAccessSales = adultAccessSales.add(item.getCost());
                    adultAccessDis = adultAccessDis.add((propAfterTaxDiscount.add(propDiscount).add(item.getCalcDiscount())));

                    adultAccessStateTax = accessStateTax;
                    adultAccessCityTax = accessCityTax;
                    adultAccessFedTax = accessFederalTax;
                    adultAccessCountyTax = accessCountyTax;
                    adultAccessCogs = adultAccessCogs.add(calculateCogs(Lists.newArrayList(item), productBatchMap, recentBatchMap, prepackageHashMap, productItemHashMap, toleranceHashMap));

                } else {
                    medAccessDis = medAccessDis.add((propAfterTaxDiscount.add(propDiscount).add(item.getCalcDiscount())));
                    medAccessSales = medAccessSales.add(item.getCost());

                    medAccessStateTax = accessStateTax;
                    medAccessCityTax = accessCityTax;
                    medAccessCountyTax = accessCountyTax;
                    medAccessFedTax = accessFederalTax;
                    medAccessCogs = medAccessCogs.add(calculateCogs(Lists.newArrayList(item), productBatchMap, recentBatchMap, prepackageHashMap, productItemHashMap, toleranceHashMap));

                }

                accessCogs = medAccessCogs.add(adultAccessCogs);

            }
            BigDecimal adultDiscount = BigDecimal.ZERO;
            BigDecimal adultAfterTaxDiscount = BigDecimal.ZERO;

            BigDecimal adultExciseTax = BigDecimal.ZERO;
            BigDecimal adultCityTax = BigDecimal.ZERO;
            BigDecimal adultStateTax = BigDecimal.ZERO;
            BigDecimal adultCountyTax = BigDecimal.ZERO;
            BigDecimal adultFedTax = BigDecimal.ZERO;

            BigDecimal adultCogs = BigDecimal.ZERO;
            BigDecimal adultUse = BigDecimal.ZERO;

            BigDecimal medAfterTaxDiscount = BigDecimal.ZERO;
            BigDecimal medDiscount = BigDecimal.ZERO;

            BigDecimal medCityTax = BigDecimal.ZERO;
            BigDecimal medExciseTax = BigDecimal.ZERO;
            BigDecimal medStateTax = BigDecimal.ZERO;
            BigDecimal medCountyTax = BigDecimal.ZERO;
            BigDecimal medFedTax = BigDecimal.ZERO;

            BigDecimal medCogs = BigDecimal.ZERO;
            BigDecimal medTotalSales = BigDecimal.ZERO;

            if (transaction.getCart().getFinalConsumerTye() == ConsumerType.AdultUse) {
                adultDiscount = transaction.getCart().getCalcCartDiscount().add(itemDiscount);
                adultAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount();

                adultExciseTax = totalExciseTax;
                adultCityTax = totalCityTax;
                adultStateTax = totalStateTax;
                adultCountyTax = totalCountyTax;
                adultFedTax = totalFederalTax;

                adultCogs = calculateCogs(transaction.getCart().getItems(), productBatchMap, recentBatchMap, prepackageHashMap, productItemHashMap, toleranceHashMap);
                adultUse = transaction.getCart().getSubTotal();

            } else {
                medAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount();
                medDiscount = transaction.getCart().getCalcCartDiscount().add(itemDiscount);

                medCityTax = totalCityTax;
                medExciseTax = totalExciseTax;
                medStateTax = totalStateTax;
                medCountyTax = totalCountyTax;
                medFedTax = totalFederalTax;

                medCogs = calculateCogs(transaction.getCart().getItems(), productBatchMap, recentBatchMap, prepackageHashMap, productItemHashMap, toleranceHashMap);
                medTotalSales = transaction.getCart().getSubTotal();


            }

            prepareSalesMap(transaction.getCart().getTotal(), journal.salesMap, transaction.getCart().getPaymentOption(), transaction.getCart().getSplitPayment());

            journal.discount = journal.add(journal.discount, transaction.getCart().getCalcCartDiscount().add(itemDiscount).add(afterTaxDiscount));

            journal.adultInvExciseTax = journal.add(journal.adultInvExciseTax, adultExciseTax);

            journal.adultSales = journal.add(journal.adultSales, adultUse.subtract(adultAccessSales));

            journal.medicalInvExciseTax = journal.add(journal.medicalInvExciseTax, medExciseTax);

            journal.medicalSales = journal.add(journal.medicalSales, medTotalSales.subtract(medAccessSales));

            journal.accessSales = journal.add(journal.accessSales, adultAccessSales.add(medAccessSales));

            journal.adultDiscount = journal.add(journal.adultDiscount, (adultDiscount.add(adultAfterTaxDiscount)).subtract(adultAccessDis));
            journal.medicalDiscount = journal.add(journal.medicalDiscount, (medDiscount.add(medAfterTaxDiscount)).subtract(medAccessDis));

            journal.accessoriesDiscount = journal.add(journal.accessoriesDiscount, adultAccessDis.add(medAccessDis));

            journal.adultCityTax = journal.add(journal.adultCityTax, adultCityTax.subtract(adultAccessCityTax));
            journal.medicalCityTax = journal.add(journal.medicalCityTax, medCityTax.subtract(medAccessCityTax));
            journal.accessCityTax = journal.add(journal.accessCityTax, accessCityTax);

            journal.adultSalesTax = journal.add(journal.adultSalesTax, adultStateTax.subtract(adultAccessStateTax));
            journal.medicalSalesTax = journal.add(journal.medicalSalesTax, medStateTax.subtract(medAccessStateTax));
            journal.accessStateTax = journal.add(journal.accessStateTax, accessStateTax);

            journal.adultCountyTax = journal.add(journal.adultCountyTax, adultCountyTax.subtract(adultAccessCountyTax));
            journal.medicalCountyTax = journal.add(journal.medicalCountyTax, medCountyTax.subtract(medAccessCountyTax));
            journal.accessCountyTax = journal.add(journal.accessCountyTax, accessCountyTax);

            journal.adultFederalTax = journal.add(journal.adultFederalTax, adultFedTax.subtract(adultAccessFedTax));
            journal.medicalFederalTax = journal.add(journal.medicalFederalTax, medFedTax.subtract(medAccessFedTax));
            journal.accessFederalTax = journal.add(journal.accessFederalTax, accessFederalTax);


            if (transaction.getCart().getFinalConsumerTye() == ConsumerType.AdultUse) {
                journal.adultInvProduct = journal.add(journal.adultInvProduct, adultCogs.subtract(adultAccessCogs));
            } else {
                journal.medicalInvProduct = journal.add(journal.medicalInvProduct, medCogs.subtract(medAccessCogs));
            }
            journal.accessoriesCogsProduct = journal.add(journal.accessoriesCogsProduct, accessCogs);

            journal.cogsInv = journal.adultInvProduct.add(journal.medicalInvProduct).add(journal.accessoriesCogsProduct);
            journal.cogsInvExcise = journal.add(journal.adultInvExciseTax, journal.medicalInvExciseTax);

            journal.deliveryFees = journal.add(journal.deliveryFees, transaction.getCart().getDeliveryFee());
            journal.creditCardFees = journal.add(journal.creditCardFees, transaction.getCart().getDeliveryFee());
        }

        return prepareReportDetails(days, paymentOptions);


    }

    private GathererReport prepareReportDetails(HashMap<Long, JournalReport> days, Set<String> paymentOptions) {
        long startMillis = new DateTime(filter.getTimeZoneStartDateMillis()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();
        if (days.isEmpty()) {
            days.put(startMillis, new JournalReport(startMillis));

        }

        SortedSet<Long> sortedAttr = new TreeSet<>(days.keySet());

        ArrayList<String> rowAttrs = new ArrayList<>(days.size());
        Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();


        rowAttrs.add("Detail");
        for (long day : sortedAttr) {
            String dayStr = ProcessorUtil.dateStringWithOffset(day, filter.getTimezoneOffset());
            rowAttrs.add(dayStr);
        }

        for (int i = 0; i < rowAttrs.size(); i++) {
            fieldTypes.put(rowAttrs.get(i), (i == 0) ? GathererReport.FieldType.STRING : GathererReport.FieldType.CURRENCY);
        }

        GathererReport report = new GathererReport(filter, "Journal Entry Report", rowAttrs);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(ProcessorUtil.dateString(startMillis) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        for (String paymentOption : paymentOptions) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("Detail", paymentOption);
            for (JournalReport journal : days.values()) {
                if (journal.salesMap.isEmpty()) {
                    continue;
                }
                String day = ProcessorUtil.dateStringWithOffset(journal.day, filter.getTimezoneOffset());
                data.put(day, journal.getValue("Sales", paymentOption));
            }
            report.add(data);
        }

        for (String paymentOption : paymentOptions) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("Detail", paymentOption + " Refunds");
            for (JournalReport journal : days.values()) {
                if (journal.refundSalesMap.isEmpty()) {
                    continue;
                }
                String day = ProcessorUtil.dateStringWithOffset(journal.day, filter.getTimezoneOffset());
                data.put(day, journal.getValue("Refunds", paymentOption));
            }
            report.add(data);
        }


        for (String attr : attrs) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("Detail", attr);
            for (JournalReport journal : days.values()) {
                String day = ProcessorUtil.dateStringWithOffset(journal.day, filter.getTimezoneOffset());
                data.put(day, journal.getValue(attr, ""));
            }
            report.add(data);
        }

        return report;

    }

    private BigDecimal calculateCogs(List<OrderItem> items, HashMap<String, ProductBatch> productBatchMap, Map<String, ProductBatch> recentBatchMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> productItemHashMap, HashMap<String, ProductWeightTolerance> toleranceHashMap) {
        BigDecimal cogs = BigDecimal.ZERO;

        for (OrderItem item : items) {
            boolean isCalculated = false;
            if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    ProductBatch batch = productBatchMap.get(prepackageProductItem.getBatchId());
                    if (prepackage != null && batch != null) {
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        BigDecimal quantity = unitValue.multiply(item.getQuantity());
                        BigDecimal unitPrice = batch.getFinalUnitCost();
                        cogs = cogs.add((quantity.multiply(unitPrice)));
                        isCalculated = true;
                    }
                }
            } else if (CollectionUtils.isNotEmpty(item.getQuantityLogs())) {
                for (QuantityLog log : item.getQuantityLogs()) {
                    ProductBatch batch = null;
                    if (StringUtils.isNotBlank(log.getBatchId())) {
                        batch = productBatchMap.get(log.getBatchId());
                    }

                    if (batch == null) {
                        batch = recentBatchMap.get(item.getProductId());
                    }

                    if (batch != null) {
                        BigDecimal unitPrice = batch.getFinalUnitCost();
                        BigDecimal quantity = log.getQuantity();
                        cogs = cogs.add((quantity.multiply(unitPrice)));
                        isCalculated = true;
                    }
                }
            }

            if (!isCalculated) {
                ProductBatch batch = recentBatchMap.get(item.getProductId());
                if (batch != null) {
                    BigDecimal quantity = item.getQuantity();
                    BigDecimal unitPrice = batch.getFinalUnitCost();
                    cogs = cogs.add((quantity.multiply(unitPrice)));
                }
            }
        }

        return cogs;
    }

    private Long getKey(Transaction t) {
        if (filter != null) {
            return new DateTime(t.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset()).getMillis();
        }
        return 0L;
    }

    /**
     * Private method to prepare transaction sales detail according to payment option
     *
     * @param total         : total
     * @param paymentMap    : payment Map
     * @param paymentOption : payment option
     * @param splitPayment  : split payment details
     */
    private void prepareSalesMap(BigDecimal total, HashMap<String, BigDecimal> paymentMap, Cart.PaymentOption paymentOption, SplitPayment splitPayment) {
        BigDecimal sales = total;
        if (paymentOption == Cart.PaymentOption.Split && splitPayment != null) {
            BigDecimal creditSales = splitPayment.getCreditDebitAmt();
            BigDecimal cashSales = splitPayment.getCashAmt();
            BigDecimal checkSales = splitPayment.getCheckAmt();
            BigDecimal storeCreditSales = splitPayment.getStoreCreditAmt();
            BigDecimal cashlessAtm = splitPayment.getCashlessAmt();

            BigDecimal splitChanged = splitPayment.getTotalSplits().subtract(sales);

            if (splitChanged.compareTo(new BigDecimal(0)) > 0) {
                cashSales = cashSales.subtract(splitChanged);
            }

            paymentMap.put(Cart.PaymentOption.Cash.toString(), cashSales.add(paymentMap.getOrDefault(Cart.PaymentOption.Cash.toString(), BigDecimal.ZERO)));
            paymentMap.put(Cart.PaymentOption.Credit.toString(), creditSales.add(paymentMap.getOrDefault(Cart.PaymentOption.Credit.toString(), BigDecimal.ZERO)));
            paymentMap.put(Cart.PaymentOption.Check.toString(), checkSales.add(paymentMap.getOrDefault(Cart.PaymentOption.Check.toString(), BigDecimal.ZERO)));
            paymentMap.put(Cart.PaymentOption.StoreCredit.toString(), storeCreditSales.add(paymentMap.getOrDefault(Cart.PaymentOption.StoreCredit.toString(), BigDecimal.ZERO)));
            paymentMap.put(Cart.PaymentOption.CashlessATM.toString(), cashlessAtm.add(paymentMap.getOrDefault(Cart.PaymentOption.CashlessATM.toString(), BigDecimal.ZERO)));

        } else {
            sales = sales.add(paymentMap.getOrDefault(paymentOption.toString(), BigDecimal.ZERO));
            paymentMap.put(paymentOption.toString(), sales);
        }
    }


    public class JournalReport {
        HashMap<String, BigDecimal> salesMap;
        HashMap<String, BigDecimal> refundSalesMap;
        BigDecimal adultSales, medicalSales, accessSales;
        BigDecimal medicalInvExciseTax, adultInvExciseTax;
        BigDecimal adultDiscount, medicalDiscount, accessoriesDiscount;
        BigDecimal adultCityTax, medicalCityTax, accessCityTax;
        BigDecimal adultSalesTax, medicalSalesTax, accessStateTax;
        BigDecimal adultCountyTax, medicalCountyTax, accessCountyTax;
        BigDecimal adultFederalTax, medicalFederalTax, accessFederalTax;
        BigDecimal adultInvProduct, medicalInvProduct, accessoriesCogsProduct;
        BigDecimal cogsInv, cogsInvExcise;
        BigDecimal discount, deliveryFees, creditCardFees;
        long day;

        private JournalReport() {
            salesMap = new HashMap<>();
            refundSalesMap = new HashMap<>();
            adultSales = medicalSales = accessSales = BigDecimal.ZERO;
            medicalInvExciseTax = adultInvExciseTax = BigDecimal.ZERO;
            adultDiscount = medicalDiscount = accessoriesDiscount = BigDecimal.ZERO;
            adultCityTax = medicalCityTax = accessCityTax = BigDecimal.ZERO;
            adultSalesTax = medicalSalesTax = accessStateTax = BigDecimal.ZERO;
            adultInvProduct = medicalInvProduct = accessoriesCogsProduct = BigDecimal.ZERO;
            cogsInv = cogsInvExcise = BigDecimal.ZERO;
            discount = deliveryFees = creditCardFees = BigDecimal.ZERO;
            adultCountyTax = medicalCountyTax = accessCountyTax = BigDecimal.ZERO;
            adultFederalTax = medicalFederalTax = accessFederalTax = BigDecimal.ZERO;

        }

        private JournalReport(long key) {
            this();
            day = key;
        }

        public BigDecimal add(BigDecimal num1, BigDecimal num2) {
            return num1.add(num2).setScale(2, RoundingMode.HALF_UP);
        }

        public Object getValue(String attr, String paymentType) {
            switch (attr) {
                case "Sales":
                    return NumberUtils.round(salesMap.getOrDefault(paymentType, BigDecimal.ZERO), 2);
                case "Discounts":
                    return NumberUtils.round(discount.negate(), 2);
                case "Refunds":
                    return NumberUtils.round(refundSalesMap.getOrDefault(paymentType, BigDecimal.ZERO).negate(), 2);
                case "Adult Sales":
                    return NumberUtils.round(adultSales, 2);
                case "Medical Sales":
                    return NumberUtils.round(medicalSales, 2);
                case "Non-cannabis Sales":
                    return NumberUtils.round(accessSales, 2);
                case "Adult Discounts":
                    return NumberUtils.round(adultDiscount.negate(), 2);
                case "Medical Discounts":
                    return NumberUtils.round(medicalDiscount.negate(), 2);
                case "Non-cannabis Discounts":
                    return NumberUtils.round(accessoriesDiscount.negate(), 2);
                case "Adult City Tax":
                    return NumberUtils.round(adultCityTax, 2);
                case "Medical City Tax":
                    return NumberUtils.round(medicalCityTax, 2);
                case "Non-cannabis City Tax":
                    return NumberUtils.round(accessCityTax, 2);
                case "Adult Sales Tax":
                    return NumberUtils.round(adultSalesTax, 2);
                case "Medical Sales Tax":
                    return NumberUtils.round(medicalSalesTax, 2);
                case "Non-cannabis Sales Tax":
                    return NumberUtils.round(accessStateTax, 2);
                case "Adult County Tax":
                    return NumberUtils.round(adultCountyTax, 2);
                case "Medical County Tax":
                    return NumberUtils.round(medicalCountyTax, 2);
                case "Non-cannabis County Tax":
                    return NumberUtils.round(accessCountyTax, 2);
                case "Adult Federal Tax":
                    return NumberUtils.round(adultFederalTax, 2);
                case "Medical Federal Tax":
                    return NumberUtils.round(medicalFederalTax, 2);
                case "Non-cannabis Federal Tax":
                    return NumberUtils.round(accessFederalTax, 2);
                case "Delivery Fees":
                    return NumberUtils.round(deliveryFees, 2);
                case "Credit Card Fees":
                    return NumberUtils.round(creditCardFees, 2);
                case "Adult Inventory Product":
                    return NumberUtils.round(adultInvProduct, 2);
                case "Adult Inventory Excise Tax":
                    return NumberUtils.round(adultInvExciseTax, 2);
                case "Medical Inventory Product":
                    return NumberUtils.round(medicalInvProduct, 2);
                case "Medical Inventory Excise Tax":
                    return NumberUtils.round(medicalInvExciseTax, 2);
                case "Non-cannabis COGS Product":
                    return NumberUtils.round(accessoriesCogsProduct, 2);
                case "COGS Inventory":
                    return NumberUtils.round(cogsInv, 2);
                case "COGS Inventory Excise":
                    return NumberUtils.round(cogsInvExcise, 2);
                default:
                    return BigDecimal.ZERO;
            }
        }
    }

}