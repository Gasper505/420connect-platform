package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by stephen on 12/2/16.
 */
public class LossGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private PrepackageRepository prepackageRepository;

    String[] attrs = new String[]{"Date", "TransNo", "Employee", "Product", "Quantity", "COGS", "Note"};
    ArrayList<String> reportHeaders = new ArrayList<>();
    HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public LossGatherer() {

        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.STRING};
        Collections.addAll(reportHeaders, attrs);
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Loss Report", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        Iterable<Transaction> losses = transactionRepository.getLosses(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId());
        Iterable<ProductBatch> batches = batchRepository.listAll(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> newestBatches = new HashMap<>();
        HashMap<String, ProductBatch> batchByIds = new HashMap<>();
        for (ProductBatch b : batches) {
            Product p = productMap.get(b.getProductId());
            if (p == null || !p.isActive()) {
                continue;
            }
            if (newestBatches.containsKey(p.getId())) {
                ProductBatch oldBatch = newestBatches.get(p.getId());
                if (b.getCreated() > oldBatch.getCreated()) {
                    newestBatches.put(p.getId(), b);
                }
            } else {
                newestBatches.put(p.getId(), b);
            }
            batchByIds.put(b.getId(), b);
        }
        for (Transaction l : losses) {
            for (OrderItem orderItem : l.getCart().getItems()) {
                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], ProcessorUtil.timeStampWithOffsetLong(l.getProcessedTime(), filter.getTimezoneOffset()));
                data.put(attrs[1], l.getTransNo());

                Employee empl = employeeMap.get(l.getSellerId());
                if (empl != null) {
                    data.put(attrs[2], empl.getFirstName() + " " + empl.getLastName());
                } else {
                    data.put(attrs[2], "N/A");
                }


                Product p = productMap.get(orderItem.getProductId());

                if (p != null) {
                    if (orderItem.getQuantityLogs().size() > 0) {
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            String pName = p.getName();
                            // cogs

                            ProductBatch batch = null;
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                batch = batchByIds.get(quantityLog.getBatchId());
                            }

                            double quantity = orderItem.getQuantity().doubleValue();
                            if (StringUtils.isNotBlank(quantityLog.getPrepackageItemId())) {
                                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(quantityLog.getPrepackageItemId());
                                if (prepackageProductItem != null) {
                                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                    if (prepackage != null) {
                                        pName = String.format("%s (%s)", pName, prepackage.getName());

                                        // convert to grams to get COGs
                                        quantity = quantity * prepackage.getUnitValue().doubleValue();
                                    }
                                }
                            }

                            data.put(attrs[3], pName);
                            data.put(attrs[4], quantityLog.getQuantity().doubleValue());

                            // use latest batch if batch is null
                            if (batch == null) {
                                batch = newestBatches.get(p.getId());
                            }
                            Double cost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();
                            double cogs = quantity * cost; //compute cogs for this inventory
                            data.put(attrs[5], NumberUtils.round(cogs, 2));
                        }
                    } else {

                        double quantity = orderItem.getQuantity().doubleValue();
                        data.put(attrs[3], p.getName());
                        data.put(attrs[4], quantity);

                        ProductBatch batch = newestBatches.get(p.getId());
                        Double cost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();
                        double cogs = quantity * cost; //compute cogs for this inventory
                        data.put(attrs[5], NumberUtils.round(cogs, 2));
                    }
                } else {
                    // unknown product
                    data.put(attrs[3], "Unknown");
                    data.put(attrs[4], 0);
                    data.put(attrs[5], 0);
                }

                data.put(attrs[6], l.getNote().getMessage());
                report.add(data);
            }
        }
        return report;
    }
}
