package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalLocationRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.services.mgmt.DeliveryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class MileageReportByTransactionGatherer implements Gatherer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MileageReportByTransactionGatherer.class);
    private String[] attrs = new String[]{"Transaction No", "Mileage"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private TransactionRepository transactionRepository;
    private TerminalLocationRepository terminalLocationRepository;
    private DeliveryService deliveryService;
    private static final String MILES = " miles";

    public MileageReportByTransactionGatherer(TransactionRepository transactionRepository, TerminalLocationRepository terminalLocationRepository, DeliveryService deliveryService) {
        this.transactionRepository = transactionRepository;
        this.terminalLocationRepository = terminalLocationRepository;
        this.deliveryService = deliveryService;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Mileage Report By Transaction", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Transaction> transactionHashMap = transactionRepository.listTransactionByDate(filter.getCompanyId(), filter.getShopId(), "{created:1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Iterable<TerminalLocation> terminalLocationList = terminalLocationRepository.listByShop(filter.getCompanyId(), filter.getShopId(), "{created:1}");

        Map<String, List<TerminalLocation>> transactionLocationMap = deliveryService.getTerminalLocationByTransaction(transactionHashMap, terminalLocationList);

        HashMap<String, TransactionInfo> distanceHashMap = new HashMap<>();

        NumberFormat formatter = new DecimalFormat("#.##");
        Set<String> transactionKeys = transactionHashMap.keySet();
        TransactionInfo transactionInfo;
        for (String transactionKey : transactionKeys) {
            Transaction transaction = transactionHashMap.get(transactionKey);

            if (transaction.isMileageCalculated()) {
                transactionInfo = new TransactionInfo(transaction.getTransNo(), transaction.getMileage().doubleValue() + MILES);
            } else {
                List<TerminalLocation> terminalLocations = transactionLocationMap.get(transactionKey);

                Double distance = deliveryService.calculateDistance(terminalLocations);

                transactionInfo = new TransactionInfo(transaction.getTransNo(), formatter.format(distance) + MILES);
            }
            distanceHashMap.put(transaction.getId(), transactionInfo);
        }

        Set<String> distanceKeys = distanceHashMap.keySet();
        for (String distanceKey : distanceKeys) {
            transactionInfo = distanceHashMap.get(distanceKey);
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], transactionInfo.getName());
            data.put(attrs[1], transactionInfo.getDistance());

            report.add(data);
        }

        return report;
    }

    private class TransactionInfo {
        String name;
        String distance;

        public TransactionInfo(String name, String distance) {
            this.name = name;
            this.distance = distance;
        }

        public String getName() {
            return name;
        }

        public String getDistance() {
            return distance;
        }
    }
}
