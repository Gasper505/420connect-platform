package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 12/27/17.
 */
public class PaidInOutActivityGatherer implements Gatherer {

    @Inject
    CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    PaidInOutItemRepository paidInOutItemRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    ShopRepository shopRepository;

    private String[] attrs = new String[]{"Date", "Cash Drawer Date", "Employee", "Terminal",
            "Starting Cash", "Day End", "Daily Sales",
            "Paid In", "Paid Out", "Cash Drop", "Notes"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public PaidInOutActivityGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, // Date
                GathererReport.FieldType.STRING,  // Cash Drawer Date
                GathererReport.FieldType.STRING, // Employee
                GathererReport.FieldType.STRING, // Terminal
                GathererReport.FieldType.STRING, // Starting Cash
                GathererReport.FieldType.STRING, // Day End
                GathererReport.FieldType.STRING, // Daily Sales
                GathererReport.FieldType.CURRENCY, // Paid In
                GathererReport.FieldType.CURRENCY, // Paid Out
                GathererReport.FieldType.CURRENCY, // Cash Drop
                GathererReport.FieldType.STRING // Notes
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }


    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Paid In/Out Activity Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);


        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        Iterable<CashDrawerSession> cashDrawerSessions = cashDrawerSessionRepository.listByShopWithDatesCreate(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        HashMap<String, CashDrawerSession> cashDrawerSessionHashMap = new HashMap<>();
        for (CashDrawerSession cashDrawerSession : cashDrawerSessions) {
            cashDrawerSessionHashMap.put(cashDrawerSession.getId(), cashDrawerSession);
        }

        Iterable<PaidInOutItem> paidInOutItems = paidInOutItemRepository.listByShopWithCreatedDateSort(filter.getCompanyId(), filter.getShopId(), "{created:1}", filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        List<PaidInOutItem> items = Lists.newArrayList(paidInOutItems);
        for (PaidInOutItem item : items) {
            CashDrawerSession session = cashDrawerSessionHashMap.get(item.getCdSessionId());
            double carriedOver = 0d;
            if (item.isCarryOver()
                    || "Auto carry over to next day.".equalsIgnoreCase(item.getReason())) {
                carriedOver += item.getAmount().doubleValue();
            }
            if (session != null && (session.getAmtBeforeCarryOver() == null || session.getAmtBeforeCarryOver().doubleValue() == 0)) {
                String date = ProcessorUtil.timeStampWithOffset(item.getCreated(), filter.getTimezoneOffset());
                BigDecimal total = session.getTotalCashDrop().add(session.getTotalCashIn()).add(session.getTotalCashOut());
                double expected = session.getStartCash().doubleValue()
                        + session.getCashSales().doubleValue()
                        + session.getCheckSales().doubleValue()
                        + session.getTotalCashIn().doubleValue() - session.getTotalCashOut().doubleValue() - session.getTotalCashDrop().doubleValue();

                expected += carriedOver;
                session.setAmtBeforeCarryOver(new BigDecimal(expected));
            }

        }
        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());

        String defaultCountry = shop == null ? "US" : shop.getDefaultCountry();

        for (PaidInOutItem item : items) {
            CashDrawerSession session = cashDrawerSessionHashMap.get(item.getCdSessionId());
            Employee employee = employeeHashMap.get(item.getEmployeeId());
            Terminal terminal = terminalHashMap.get(item.getTerminalId());
            if (session != null && employee != null && terminal != null) {
                String date = ProcessorUtil.timeStampWithOffsetLong(item.getCreated(), filter.getTimezoneOffset());
                String cDate = session.getDate();
                String empName = employee.getFirstName() + " " + employee.getLastName();
                String terminalName = terminal.getName();
                BigDecimal starting = session.getStartCash();
                BigDecimal expectedCash = session.getAmtBeforeCarryOver();
                BigDecimal dailySales = session.getCashSales().add(session.getCheckSales()).add(session.getCreditSales());
                BigDecimal paidIn = new BigDecimal(0);
                BigDecimal paidOut = new BigDecimal(0);
                BigDecimal cashDrop = new BigDecimal(0);
                String notes = item.getReason();


                if (item.getType() == PaidInOutItem.PaidInOutType.PaidIn) {
                    paidIn = item.getAmount();
                } else if (item.getType() == PaidInOutItem.PaidInOutType.CashDrop) {
                    cashDrop = item.getAmount();
                } else if (item.getType() == PaidInOutItem.PaidInOutType.PaidOut) {
                    paidOut = item.getAmount();
                }


                HashMap<String, Object> data = new HashMap<>();
                /*"Date", "Cash Drawer Date", "Employee","Terminal",
                        "Starting Cash", "Day End", "Daily Sales",
                        "Paid In", "Paid Out", "Cash Drop", "Notes";*/

                data.put(attrs[0], date);
                data.put(attrs[1], cDate);
                data.put(attrs[2], empName);
                data.put(attrs[3], terminalName);
                data.put(attrs[4], TextUtil.toCurrency(starting.doubleValue(), defaultCountry));
                data.put(attrs[5], TextUtil.toCurrency(expectedCash.doubleValue(), defaultCountry));
                data.put(attrs[6], TextUtil.toCurrency(dailySales.doubleValue(), defaultCountry));
                data.put(attrs[7], new DollarAmount(paidIn));
                data.put(attrs[8], new DollarAmount(paidOut));
                data.put(attrs[9], new DollarAmount(cashDrop));
                data.put(attrs[10], notes);

                report.add(data);
            }
        }

        return report;
    }
}
