package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephen on 10/28/16.
 */
public class ProductSalesByCannabisTypeGatherer implements Gatherer {
    private ProductRepository productRepository;
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Type", "Retail Value", "Total Discounts", "City Taxes", "County Taxes", "State Taxes", "Federal Taxes", "Total Taxes", "Delivery Fees", "Credit Card Fees", "After Tax Discount", "Gross Receipts"};
    private ArrayList<String> reportHeaders = new ArrayList<>(attrs.length);
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public ProductSalesByCannabisTypeGatherer(TransactionRepository transactionRepository, ProductRepository productRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Cannabis Type", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Double[] strainSales = new Double[]{0d, 0d, 0d, 0d, 0d}; //sativa, indica, hybrid
        String[] strainNames = new String[]{"Sativa", "Indica", "Hybrid", "CBD", "Other"};
        Map<String, Product> products = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, SaleWrapper> results = new HashMap<>();

        int factor = 1;
        for (Transaction t : transactions) {

            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            HashMap<String, Double> propRatioMap = new HashMap<>();
            double total = 0.0;
            for (OrderItem orderItem : t.getCart().getItems()) {

                // skip if old refund method
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                Product product = products.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                }


            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : t.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                Product product = products.get(orderItem.getProductId());
                if (product != null) {

                    propRatioMap.put(orderItem.getId(), 1d); // 100 %
                    if (product.isDiscountable() && total > 0) {
                        double finalCost = orderItem.getFinalPrice().doubleValue();
                        double ratio = finalCost / total;

                        propRatioMap.put(orderItem.getId(), ratio);
                    }
                }

            }

            double totalPreTax = 0;
            for (OrderItem item : t.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }
                Product product = products.get(item.getProductId());

                if (product != null) {

                    totalPreTax += item.getCalcPreTax().doubleValue();

                    String type = product.getFlowerType();
                    if (StringUtils.isBlank(type)) {
                        type = "Other";
                    }

                    SaleWrapper wrapper = results.get(type);
                    if (wrapper == null) {
                        wrapper = new SaleWrapper();
                        results.put(type, wrapper);
                    }

                    Double ratio = propRatioMap.get(item.getId());
                    if (ratio == null) {
                        ratio = 1d;
                    }

                    Double propCartDiscount = t.getCart().getCalcCartDiscount() != null ? t.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                    Double propDeliveryFee = t.getCart().getDeliveryFee() != null ? t.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                    Double propCCFee = t.getCart().getCreditCardFee() != null ? t.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                    Double propAfterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount() != null ? t.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;


                    double discount = item.getCalcDiscount().doubleValue() + propCartDiscount;

                    discount = NumberUtils.round(discount, 6);

                    double preALExciseTax = 0;
                    double preNALExciseTax = 0;
                    double postALExciseTax = 0;
                    double postNALExciseTax = 0;
                    double totalCityTax = 0;
                    double totalCountyTax = 0;
                    double totalStateTax = 0;
                    double totalFedTax = 0;
                    double postTax = 0;

                    double subTotalAfterDiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                    if (item.getTaxResult() != null) {
                        preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                        preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                        postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                        postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            totalCityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                            totalStateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                            totalCountyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                            totalFedTax = item.getTaxResult().getTotalFedTax().doubleValue();

                            postTax += item.getTaxResult().getTotalPostCalcTax().doubleValue() + postALExciseTax + postNALExciseTax;
                        }
                    }
                    if (postTax == 0) {
                        if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                            TaxInfo taxInfo = item.getTaxInfo();
                            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                                totalCityTax = subTotalAfterDiscount * taxInfo.getCityTax().doubleValue();
                                totalStateTax = subTotalAfterDiscount * taxInfo.getStateTax().doubleValue();
                                totalFedTax = subTotalAfterDiscount * taxInfo.getFederalTax().doubleValue();
                                postTax = totalCityTax + totalStateTax + totalFedTax;
                            }
                        }
                    }

                    if (postTax == 0) {
                        postTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                    }


                    wrapper.retailValue += item.getCost().doubleValue() * factor;
                    wrapper.name = type;
                    wrapper.deliveryFees += propDeliveryFee * factor;
                    wrapper.subTotals += item.getFinalPrice().doubleValue() * factor;
                    wrapper.discounts += discount * factor;
                    wrapper.afterTaxDiscount += propAfterTaxDiscount * factor;
                    wrapper.tax += (item.getCalcTax().doubleValue() + postALExciseTax + postNALExciseTax) * factor;
                    wrapper.cityTax += totalCityTax * factor;
                    wrapper.countyTax += totalCountyTax * factor;
                    wrapper.stateTax += totalStateTax * factor;
                    wrapper.federalTax += totalFedTax * factor;
                    wrapper.creditCartFee += propCCFee * factor;

                    double grossReceipt = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4) + NumberUtils.round(propDeliveryFee, 4) +
                            NumberUtils.round(postTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);


                    wrapper.gross += NumberUtils.round(grossReceipt, 6) * factor;
                }
            }
        }

        for (String vName : results.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            SaleWrapper wrapper = results.get(vName);
            data.put(attrs[0], vName);
            data.put(attrs[1], new DollarAmount(wrapper.retailValue));
            data.put(attrs[2], new DollarAmount(wrapper.discounts));
            data.put(attrs[3], new DollarAmount(wrapper.cityTax));
            data.put(attrs[4], new DollarAmount(wrapper.countyTax));
            data.put(attrs[5], new DollarAmount(wrapper.stateTax));
            data.put(attrs[6], new DollarAmount(wrapper.federalTax));
            data.put(attrs[7], new DollarAmount(wrapper.tax));
            data.put(attrs[8], new DollarAmount(wrapper.deliveryFees));
            data.put(attrs[9], new DollarAmount(wrapper.creditCartFee));
            data.put(attrs[10], new DollarAmount(wrapper.afterTaxDiscount));
            data.put(attrs[11], new DollarAmount(wrapper.gross));
            report.add(data);
        }

        return report;
    }

    public static final class SaleWrapper {
        public String name;
        public double subTotals = 0;
        public double retailValue = 0;
        public double discounts = 0;
        public double afterTaxDiscount = 0;
        public double deliveryFees = 0;
        public double tax = 0;
        public double cityTax = 0;
        public double countyTax = 0;
        public double stateTax = 0;
        public double federalTax = 0;
        public double creditCartFee = 0;
        public double gross = 0;
    }
}
