package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Stephen Schmidt on 7/10/2016.
 */
public class ProfitLossGatherer implements Gatherer {
    private static final Log LOG = LogFactory.getLog(ProfitLossGatherer.class);
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private ProductBatchRepository batchRepository;
    private PrepackageRepository prepackageRepository;
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private ProductWeightToleranceRepository weightToleranceRepository;

    @Inject
    private DailySummaryGatherer dailySummaryGatherer;

    private String[] attrs = new String[]{"Date", "Gross Receipts", "Cost", "Loss", "Profit", "Discount", "% of Margin"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private ReportFilter filter;

    public ProfitLossGatherer(TransactionRepository transactionRepository, ProductRepository productRepository, ProductBatchRepository batchRepository, PrepackageRepository prepackageRepository, PrepackageProductItemRepository prepackageProductItemRepository, ProductWeightToleranceRepository productWeightToleranceRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.batchRepository = batchRepository;
        this.prepackageProductItemRepository = prepackageProductItemRepository;
        this.prepackageRepository = prepackageRepository;
        this.weightToleranceRepository = productWeightToleranceRepository;

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.PERCENTAGE};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        this.filter = filter;
        GathererReport report = new GathererReport(filter, "Profit-Loss and Margin Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);


        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Iterable<Transaction> losses = transactionRepository.getLosses(filter.getCompanyId(), filter.getShopId(), filter.getTimeZoneStartDateMillis(),
                filter.getTimeZoneEndDateMillis());
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        buildDailyReport(report, transactions, losses, productMap);

        return report;
    }

    private void buildDailyReport(GathererReport report, Iterable<Transaction> transactions,
                                  Iterable<Transaction> losses, HashMap<String, Product> productMap) {
        //Maps to keep track of totals by day
        HashMap<Long, ProfitLossByDay> days = new HashMap<>();
        Set<Long> daysInBracket = new HashSet<>();

        HashMap<String, ProductBatch> productBatchHashMap = batchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, ProductBatch> recentBatchMap = getProductBatchByProductId(productBatchHashMap);


        for (Transaction t : transactions) {
            //Set up day if its not in the map, otherwise retrieve the PFLD object for this key
            HashMap<String, Double> cogsMap = calculateTotalCogs(t, productBatchHashMap, prepackageHashMap, productItemHashMap, toleranceHashMap, recentBatchMap);
            Long key = getKey(t);
            ProfitLossByDay pfld;
            if (!daysInBracket.contains(key)) { //add to our set of keys if its not there already
                daysInBracket.add(key);
                pfld = new ProfitLossByDay(key);
            } else {
                pfld = days.get(key);
            }

            int factor = 1;
            if (Transaction.TransactionType.Refund.equals(t.getTransType()) && Cart.RefundOption.Retail.equals(t.getCart().getRefundOption())) {
                factor = -1;
            }
            double cogs = 0;
            //Calculate
            for (OrderItem item : t.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                Double unitCost = cogsMap.get(item.getId());
                if (unitCost != null && unitCost > 0 && !unitCost.isInfinite() && item.getQuantity() != null && item.getQuantity().doubleValue() > 0) {
                    cogs += unitCost;
                    pfld.totalCost += (unitCost.doubleValue()) * factor;
                }
            }
            double total = t.getCart().getTotal().doubleValue() * factor;
            if (t.getTransType() == Transaction.TransactionType.Refund
                    && t.getCart().getRefundOption() == Cart.RefundOption.Void
                    && t.getCart().getSubTotal().doubleValue() == 0) {
                total = 0;
            }

            pfld.totalSales += total; //.getFinalPrice();
            double totalDiscount = t.getCart().getTotalDiscount() != null ? t.getCart().getTotalDiscount().doubleValue() : 0.0;
            pfld.discount += totalDiscount * factor;
            days.put(key, pfld);
        }

        for (Transaction l : losses) {
            HashMap<String, Double> unitCostMap = calculateTotalCogs(l, productBatchHashMap, prepackageHashMap, productItemHashMap, toleranceHashMap, recentBatchMap);
            Long key = getKey(l);
            ProfitLossByDay pfld;
            if (!daysInBracket.contains(key)) {
                daysInBracket.add(key);
                pfld = new ProfitLossByDay(key);
            } else {
                pfld = days.get(key);
            }
            for (OrderItem it : l.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == l.getTransType() && Cart.RefundOption.Void == l.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == it.getStatus()) {
                    continue;
                }
                double unitCost = 0d;
                if (unitCostMap.get(it.getId()) != null) {
                    unitCost = unitCostMap.get(it.getId());
                }
                pfld.totalLoss += unitCost * -1;
            }
            days.put(key, pfld);
        }
        //Sort out our data and add it to the report
        ArrayList<ProfitLossByDay> dayList = new ArrayList<>();
        for (ProfitLossByDay pfd : days.values()) {
            dayList.add(pfd);
        }

        Collections.sort(dayList);
        for (ProfitLossByDay profitLossByDay : dayList) {
            report.add(profitLossByDay.getDailyData());
        }
    }


    //util classes and methods below
    private Long getKey(Transaction t) {
        return new DateTime(t.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset()).getMillis();
    }

    private class ProfitLossByDay implements Comparable<ProfitLossByDay> {
        Long day;
        Double totalSales, totalCost, totalLoss, discount;

        ProfitLossByDay(Long day) {
            this.day = day;
            totalSales = totalCost = totalLoss = discount = 0d;
        }

        private Double getMargin() {
            return totalSales - totalCost - totalLoss;
        }

        private HashMap<String, Object> getDailyData() {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.dateString(day));
            data.put(attrs[1], new DollarAmount(totalSales));
            data.put(attrs[2], new DollarAmount(totalCost));
            data.put(attrs[3], new DollarAmount(totalLoss));
            data.put(attrs[4], new DollarAmount(totalSales - totalCost));
            data.put(attrs[5], new DollarAmount(discount));
            if (totalSales > 0) {
                data.put(attrs[6], new Percentage(getMargin() / totalSales));
            } else {
                data.put(attrs[6], new Percentage(0.0));
            }
            return data;
        }

        public int compareTo(ProfitLossByDay o) {
            return new Long(this.day - o.day).intValue();
        }
    }

    private HashMap<String, Double> calculateTotalCogs(Transaction transaction, HashMap<String, ProductBatch> productBatchMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, HashMap<String, ProductBatch> recentBatchMap) {
        HashMap<String, Double> cogsMap = new HashMap<>();
        if (transaction.getCart() == null || transaction.getCart().getItems() == null) {
            return cogsMap;
        }
        for (OrderItem item : transaction.getCart().getItems()) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(item.getPrepackageItemId());
            boolean calculate = false;
            if (prepackageProductItem != null) {
                ProductBatch batch = productBatchMap.get(prepackageProductItem.getBatchId());
                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                if (batch != null && prepackage != null) {
                    BigDecimal unitValue = prepackage.getUnitValue();
                    if (unitValue == null || unitValue.doubleValue() == 0) {
                        ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                        unitValue = weightTolerance.getUnitValue();
                    }
                    double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                    double unitPrice = batch.getFinalUnitCost().doubleValue();
                    cogsMap.put(item.getId(), (cogsMap.get(item.getId()) == null ? 0 : cogsMap.get(item.getId())) + unitPrice * unitsSold);
                    calculate = true;
                }
            } else if (item.getQuantityLogs() != null && !item.getQuantityLogs().isEmpty()) {
                double cogs = 0;
                for (QuantityLog log : item.getQuantityLogs()) {
                    ProductBatch batch = productBatchMap.get(log.getBatchId());
                    if (batch != null) {
                        cogs += log.getQuantity().doubleValue() * batch.getFinalUnitCost().doubleValue();
                        calculate = true;
                    }
                }
                cogsMap.put(item.getId(), (cogsMap.get(item.getId()) == null ? 0 : cogsMap.get(item.getId())) + cogs);
            }
            if (!calculate) {
                ProductBatch batch = recentBatchMap.get(item.getProductId());
                BigDecimal unitPrice = (batch == null) ? BigDecimal.ZERO : batch.getFinalUnitCost();
                cogsMap.put(item.getId(), (cogsMap.get(item.getId()) == null ? 0 : cogsMap.get(item.getId())) + item.getQuantity().doubleValue() * unitPrice.doubleValue());
            }
        }
        return cogsMap;
    }

    /**
     * This is private method to get latest product batch by product id
     *
     * @return
     */
    public HashMap<String, ProductBatch> getProductBatchByProductId(HashMap<String, ProductBatch> productBatchHashMap) {
        HashMap<String, ProductBatch> returnMap = new HashMap<>();

        for (String key : productBatchHashMap.keySet()) {
            ProductBatch batch = productBatchHashMap.get(key);
            if (returnMap.containsKey(batch.getProductId())) {
                ProductBatch mapBatch = returnMap.get(batch.getProductId());
                DateTime batchDateTime = new DateTime(batch.getCreated());
                if (batchDateTime.isBefore(mapBatch.getCreated())) {
                    batch = mapBatch;
                }
            }
            returnMap.put(batch.getProductId(), batch);
        }

        return returnMap;
    }
}
