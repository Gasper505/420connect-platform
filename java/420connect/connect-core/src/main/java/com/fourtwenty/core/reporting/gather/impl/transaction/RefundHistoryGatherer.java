package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class RefundHistoryGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private ProductService productService;
    private MemberRepository memberRepository;
    private EmployeeRepository employeeRepository;

    private ArrayList<String> reportHeaders;
    private Map<String, GathererReport.FieldType> fields = new LinkedHashMap<>();

    public RefundHistoryGatherer(TransactionRepository transactionRepository, ProductService productService,
                                 EmployeeRepository employeeRepository, MemberRepository memberRepository) {
        this.transactionRepository = transactionRepository;
        this.productService = productService;
        this.employeeRepository = employeeRepository;
        this.memberRepository = memberRepository;


        fields.put("Date", GathererReport.FieldType.STRING);
        fields.put("Transaction No.", GathererReport.FieldType.STRING);
        fields.put("Employee", GathererReport.FieldType.STRING);
        fields.put("Customer", GathererReport.FieldType.STRING);
        fields.put("With Inventory", GathererReport.FieldType.STRING);
        fields.put("Refund As", GathererReport.FieldType.STRING);
        fields.put("Refund Amount", GathererReport.FieldType.CURRENCY);
        fields.put("Product", GathererReport.FieldType.STRING);
        fields.put("Quantity", GathererReport.FieldType.NUMBER);

        reportHeaders = Lists.newArrayList(fields.keySet());
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Refund History", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fields);


        HashMap<String, String> productMap = productService.getProductNameMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Transaction> results = transactionRepository.getRefundData(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());


        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));


        for (Transaction t : transactions) {
            for (OrderItem item : t.getCart().getItems()) {
                if (item.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    HashMap<String, Object> data = new HashMap<>();

                    int i = 0;
                    data.put(reportHeaders.get(i), ProcessorUtil.timeStampWithOffsetLong(t.getProcessedTime(), filter.getTimezoneOffset()));
                    data.put(reportHeaders.get(++i), t.getTransNo());
                    Employee employee = employeeMap.get(t.getSellerId());
                    String employeeName = employee != null ? employee.getFirstName() + " " + employee.getLastName() : "";
                    data.put(reportHeaders.get(++i), employeeName);

                    Member member = memberHashMap.get(t.getMemberId());
                    String memberName = member != null ? member.getFirstName() + " " + member.getLastName() : "";
                    data.put(reportHeaders.get(++i), memberName);

                    String withInventory = t.getStatus() == Transaction.TransactionStatus.RefundWithInventory ? "Yes" : "No";
                    data.put(reportHeaders.get(++i), withInventory);
                    data.put(reportHeaders.get(++i), t.getCart().getPaymentOption().toString());
                    data.put(reportHeaders.get(++i), new DollarAmount(item.getFinalPrice().doubleValue()));
                    data.put(reportHeaders.get(++i), productMap.get(item.getProductId()));
                    data.put(reportHeaders.get(++i), item.getQuantity());
                    report.add(data);
                }
            }
        }
        return report;
    }
}
