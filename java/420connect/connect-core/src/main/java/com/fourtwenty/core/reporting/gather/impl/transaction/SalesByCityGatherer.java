package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * Created by stephen on 9/14/16.
 */
public class SalesByCityGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private String[] attrs = new String[]{"City", "State", "Subtotal Sales", "Discount", "City Tax", "County Tax", "State Tax", "Total Taxes", "Delivery Fee", "Credit/Debit Fee", "After Tax Discounts", "Gross Receipts", "Percentage of Sales"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private ProductRepository productRepository;

    public SalesByCityGatherer(TransactionRepository transactionRepository, MemberRepository memberRepository, ProductRepository productRepository) {
        this.transactionRepository = transactionRepository;
        this.memberRepository = memberRepository;
        this.productRepository = productRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // 0
                GathererReport.FieldType.STRING, // 1
                GathererReport.FieldType.CURRENCY, // 2
                GathererReport.FieldType.CURRENCY, // 3
                GathererReport.FieldType.CURRENCY, // 4
                GathererReport.FieldType.CURRENCY, // 5
                GathererReport.FieldType.CURRENCY, // 6
                GathererReport.FieldType.CURRENCY, // 7
                GathererReport.FieldType.CURRENCY, // 8
                GathererReport.FieldType.CURRENCY, // 9
                GathererReport.FieldType.CURRENCY, // 10
                GathererReport.FieldType.CURRENCY, // 11
                GathererReport.FieldType.PERCENTAGE// 12
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By City", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());


        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));


        HashMap<String, SalesByCity> salesByCity = new HashMap<>();
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Double totalSales = 0.0;
        int factor = 1;
        for (Transaction t : transactions) {
            //it will add  Pre-Taxes in report
            double totalPreTax = 0;

            /* factor is +ve if normal transaction and -ve if -ve transaction*/
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            for (OrderItem item : t.getCart().getItems()) {
                Product p = productMap.get(item.getProductId());
                if (p != null) {
                    totalPreTax += item.getCalcPreTax().doubleValue() * factor;
                }
            }
            if (t.getCart().getTotal().doubleValue() > 0) {
                Member m = memberHashMap.get(t.getMemberId());
                if (m != null) {
                    String state = "Unknown";
                    String city = "Unknown";


                    Address address = null;
                    if (t.getDeliveryAddress() != null) {
                        address = t.getDeliveryAddress();
                    } else if (m.getAddress() != null) {
                        address = m.getAddress();
                    }

                    if (address != null) {
                        if (StringUtils.isNotBlank(address.getCity())) {
                            city = address.getCity();
                        }

                        if (StringUtils.isNotBlank(address.getState())) {
                            state = address.getState();
                        }
                    }


                    String cityState = city.trim().toLowerCase() + state.trim().toLowerCase();

                    SalesByCity sbc = salesByCity.get(cityState);

                    double total = t.getCart().getTotal().doubleValue() * factor;
                    double creditCardFees = 0d;
                    double deliveryFees = 0d;
                    double afterTaxDiscount = 0d;

                    if (t.getCart().getAppliedAfterTaxDiscount() != null && t.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                        afterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount().doubleValue() * factor;
                    }

                    if (t.getCart().getCreditCardFee() != null && t.getCart().getCreditCardFee().doubleValue() > 0) {
                        creditCardFees += t.getCart().getCreditCardFee().doubleValue() * factor;
                    }

                    if (t.getCart().getDeliveryFee() != null && t.getCart().getDeliveryFee().doubleValue() > 0) {
                        deliveryFees += t.getCart().getDeliveryFee().doubleValue() * factor;
                    }

                    if (t.getTransType() == Transaction.TransactionType.Refund
                            && t.getCart().getRefundOption() == Cart.RefundOption.Void
                            && t.getCart().getSubTotal().doubleValue() == 0) {
                        creditCardFees = 0;
                        deliveryFees = 0;
                        afterTaxDiscount = 0;
                        total = 0;
                    }


                    if (sbc != null) { //city already in map
                        sbc.subtotal += t.getCart().getSubTotal().doubleValue() * factor;
                        sbc.deliveryFees += deliveryFees;
                        sbc.discount += t.getCart().getCalcCartDiscount().doubleValue() * factor;
                        sbc.afterTaxDiscount += afterTaxDiscount;
                        sbc.tax = (t.getCart().getTotalCalcTax().doubleValue() - t.getCart().getTotalPreCalcTax().doubleValue()) * factor; //t.getCart().getTax().doubleValue() * t.getCart().getSubTotalDiscount().doubleValue();
                        sbc.totalNoTax += t.getCart().getSubTotalDiscount().doubleValue() * factor;
                        sbc.total += total;
                        sbc.ccFees += creditCardFees;

                        if (t.getCart().getTaxResult() != null) {
                            sbc.cityTax += t.getCart().getTaxResult().getTotalCityTax().doubleValue() * factor;
                            sbc.stateTax += t.getCart().getTaxResult().getTotalStateTax().doubleValue() * factor;
                            sbc.countyTax += t.getCart().getTaxResult().getTotalCountyTax().doubleValue() * factor;
                        }


                    } else { //new city
                        SalesByCity newSbc = new SalesByCity();
                        newSbc.city = city;
                        newSbc.state = state;
                        newSbc.subtotal = t.getCart().getSubTotal().doubleValue() * factor;
                        newSbc.deliveryFees += deliveryFees;
                        newSbc.discount = t.getCart().getCalcCartDiscount().doubleValue() * factor;
                        newSbc.afterTaxDiscount = afterTaxDiscount;
                        newSbc.tax = (t.getCart().getTotalCalcTax().doubleValue() - t.getCart().getTotalPreCalcTax().doubleValue()) * factor;   //t.getCart().getTax().doubleValue() * t.getCart().getSubTotalDiscount().doubleValue();
                        newSbc.totalNoTax = t.getCart().getSubTotalDiscount().doubleValue() * factor;
                        newSbc.total = total;
                        newSbc.ccFees = creditCardFees;

                        if (t.getCart().getTaxResult() != null) {
                            newSbc.cityTax += t.getCart().getTaxResult().getTotalCityTax().doubleValue() * factor;
                            newSbc.stateTax += t.getCart().getTaxResult().getTotalStateTax().doubleValue() * factor;
                            newSbc.countyTax += t.getCart().getTaxResult().getTotalCountyTax().doubleValue() * factor;
                        }

                        salesByCity.put(cityState, newSbc);
                    }
                    totalSales += total;
                }
            }
        }
        for (String key : salesByCity.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            SalesByCity sbc = salesByCity.get(key);
            data.put(attrs[0], sbc.city);
            data.put(attrs[1], sbc.state);
            data.put(attrs[2], new DollarAmount(sbc.subtotal));
            data.put(attrs[3], new DollarAmount(sbc.discount));
            data.put(attrs[4], new DollarAmount(sbc.cityTax));
            data.put(attrs[5], new DollarAmount(sbc.countyTax));
            data.put(attrs[6], new DollarAmount(sbc.stateTax));
            data.put(attrs[7], new DollarAmount(sbc.tax));
            data.put(attrs[8], new DollarAmount(sbc.deliveryFees));
            data.put(attrs[9], new DollarAmount(sbc.ccFees));
            data.put(attrs[10], new DollarAmount(sbc.afterTaxDiscount));
            data.put(attrs[11], new DollarAmount(sbc.total));
            data.put(attrs[12], new Percentage(sbc.total / totalSales));
            report.add(data);
        }

        return report;
    }

    private class SalesByCity {
        private String city;
        private String state;
        private Double subtotal;
        private Double discount;
        private double afterTaxDiscount = 0;
        private Double totalNoTax;
        private double deliveryFees = 0;
        private double ccFees = 0;
        private Double tax;
        private Double total;
        private double cityTax = 0;
        private double countyTax = 0;
        private double stateTax = 0;
    }
}
