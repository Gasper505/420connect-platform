package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;


public class SalesByConsumerTypeGatherer implements Gatherer {
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TaxSummaryGatherer taxSummaryGatherer;
    private String[] attrs = new String[]{"Breakdown", "Cannabis Retail Value", "Non-Cannabis Retail Value", "Cannabis Discounts", "Non-Cannabis Discounts", "Total Discounts", "After Tax Discount", "Gross Revenue", "City Tax", "County Tax", "State Tax", "Total Tax", "Delivery Fees", "Credit/Debit Card Fees", "Gross Receipt", "COGS", "Net Profit", "Number of Visits"};
    private String[] federalAttrs = new String[]{"Breakdown", "Cannabis Retail Value", "Non-Cannabis Retail Value", "Cannabis Discounts", "Non-Cannabis Discounts", "Total Discounts", "After Tax Discount", "Gross Revenue", "City Tax", "County Tax", "State Tax", "Federal Tax", "Total Tax", "Delivery Fees", "Credit/Debit Card Fees", "Gross Receipt", "COGS", "Net Profit", "Number of Visits"};


    private ArrayList<String> reportHeaders = new ArrayList<>();
    private ArrayList<String> fedHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private Map<String, GathererReport.FieldType> fedFieldTypes = new HashMap<>();

    public SalesByConsumerTypeGatherer() {

        Collections.addAll(reportHeaders, attrs);
        Collections.addAll(fedHeaders, federalAttrs);

        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };

        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }

        GathererReport.FieldType[] fedtypes = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.STRING
        };

        for (int i = 0; i < federalAttrs.length; i++) {
            fedFieldTypes.put(federalAttrs[i], fedtypes[i]);
        }

    }

    @Override
    public GathererReport gather(ReportFilter filter) {


        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(filter.getCompanyId(), filter.getShopId());

        // fetch the data
        Iterable<Product> products = productRepository.list(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }


        List<Transaction> transactions = Lists.newArrayList(results);


        RevenueSummary totalRevenuesSummary = new RevenueSummary();
        RevenueSummary adultUseRevenuesSummary = new RevenueSummary();
        RevenueSummary mmicRevenuesSummary = new RevenueSummary();
        RevenueSummary otherSummary = new RevenueSummary();
        RevenueSummary thirdPartyRevenuesSummary = new RevenueSummary();

        totalRevenuesSummary.name = "Total Revenue";
        adultUseRevenuesSummary.name = "Adult Use Revenues";
        mmicRevenuesSummary.name = "Medicinal MMIC Revenues";
        thirdPartyRevenuesSummary.name = "Medicinal Third Party Revenues";
        otherSummary.name = "Other";

        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());
        // use federal

        GathererReport report = new GathererReport(filter, "Sales by Consumer Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        boolean useFederal = false;
        if (shop.getTaxInfo() != null && shop.getTaxInfo().getFederalTax().doubleValue() > 0) {
            useFederal = true;
            report = new GathererReport(filter, "Sales by Consumer Report", fedHeaders);
            report.setReportFieldTypes(fedFieldTypes);
        } else {
            report.setReportFieldTypes(fieldTypes);
        }

        report.setReportPostfix(GathererReport.DATE_BRACKET);

        int factor = 1;
        for (Transaction ts : transactions) {

            double cannabisRevenue = 0d;
            double nonCannabisRevenue = 0d;
            double discounts = 0;
            double exciseTax = 0;
            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;
            double federalTax = 0;
            double totalTax = 0;
            double deliveryFees = 0;
            double creditCardFees = 0d;
            double grossReceipts = 0;
            double totalCogs = 0;
            double numOfVisits = 1;
            double cannabisDiscounts = 0d;
            double nonCananbisDiscounts = 0d;
            double grossRevenue = 0d;
            double afterTaxDiscount = 0d;

            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            RevenueSummary curSummary = otherSummary;
            ConsumerType consumerType = ts.getCart().getFinalConsumerTye();

            if (consumerType == ConsumerType.MedicinalThirdParty) {
                curSummary = thirdPartyRevenuesSummary;
            } else if (consumerType == ConsumerType.MedicinalState) {
                curSummary = mmicRevenuesSummary;
            } else if (consumerType == ConsumerType.AdultUse) {
                curSummary = adultUseRevenuesSummary;
            }


            grossReceipts = ts.getCart().getTotal().doubleValue();
            discounts = ts.getCart().getTotalDiscount().doubleValue();
            deliveryFees = ts.getCart().getDeliveryFee().doubleValue();

            if (ts.getCart().getAppliedAfterTaxDiscount() != null && ts.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount().doubleValue();
            }

            if (ts.getCart().isEnableCreditCardFee() && ts.getCart().getCreditCardFee().doubleValue() > 0) {
                creditCardFees = ts.getCart().getCreditCardFee().doubleValue();
            }

            if (ts.getCart().getTaxResult() != null) {
                TaxResult taxResult = ts.getCart().getTaxResult();
                exciseTax = taxResult.getTotalExciseTax().doubleValue() + taxResult.getTotalALPostExciseTax().doubleValue();

                cityTax = NumberUtils.round(taxResult.getTotalCityTax().doubleValue(), 2);
                countyTax = NumberUtils.round(taxResult.getTotalCountyTax().doubleValue(), 2);
                stateTax = NumberUtils.round(taxResult.getTotalStateTax().doubleValue(), 2);
                federalTax = NumberUtils.round(taxResult.getTotalFedTax().doubleValue(), 2);
                totalTax = NumberUtils.round(taxResult.getTotalPostCalcTax().doubleValue(), 2);
            }
            totalTax = ts.getCart().getTotalCalcTax().doubleValue() - ts.getCart().getTotalPreCalcTax().doubleValue();
            if (totalTax < 0) {
                totalTax = 0;
            }

            int totalFinalCost = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }
                totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
            }

            // Calculate COGs & cannabis revenue
            for (OrderItem orderItem : ts.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }


                Product product = productMap.get(orderItem.getProductId());
                boolean cannabis = false;


                int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

                double targetCartDiscount = ts.getCart().getCalcCartDiscount().doubleValue() * ratio;

                if (product != null && categoryHashMap.get(product.getCategoryId()) != null) {
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());

                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        cannabis = true;
                    }


                    // now calccreditCardFeesulate COGs
                    // calculate cost of goods
                    boolean calculated = false;
                    if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        // if prepackage is set, use prepackage
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null && targetBatch != null) {
                                calculated = true;

                                BigDecimal unitValue = prepackage.getUnitValue();
                                if (unitValue == null || unitValue.doubleValue() == 0) {
                                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                    unitValue = weightTolerance.getUnitValue();
                                }
                                // calculate the total quantity based on the prepackage value
                                totalCogs += calcCOGS(orderItem.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                            }
                        }
                    } else if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    totalCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                }
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                        totalCogs = unitCost * orderItem.getQuantity().doubleValue();
                    }
                }

                if (cannabis) {
                    cannabisRevenue += orderItem.getCost().doubleValue();
                    cannabisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                } else {
                    nonCannabisRevenue += orderItem.getCost().doubleValue();
                    nonCananbisDiscounts += orderItem.getCalcDiscount().doubleValue() + targetCartDiscount;
                }
            }

            double cannabisRevenueFactor = cannabisRevenue * factor;
            double nonCannabisRevenueFactor = nonCannabisRevenue * factor;
            double discountFactor = discounts * factor;
            double grossReceiptsFactor = grossReceipts * factor;
            double totalCogsFactor = totalCogs * factor;


            //PATCH
            if (ts.getTransType() == Transaction.TransactionType.Refund
                    && ts.getCart().getRefundOption() == Cart.RefundOption.Void
                    && ts.getCart().getSubTotal().doubleValue() == 0) {
                creditCardFees = 0;
                deliveryFees = 0;
                afterTaxDiscount = 0;
                grossReceiptsFactor = 0;
            }


            // apply current summary
            curSummary.grossReceipt += grossReceiptsFactor;
            curSummary.cannabisRevenue += cannabisRevenueFactor;
            curSummary.nonCannabisRevenue += nonCannabisRevenueFactor;
            curSummary.cannabisDiscounts += cannabisDiscounts * factor;
            curSummary.nonCannabisDiscounts += nonCananbisDiscounts * factor;
            curSummary.discount += discountFactor;
            curSummary.afterTaxDiscount += afterTaxDiscount * factor;
            curSummary.grossRevenue += (cannabisRevenueFactor + nonCannabisRevenueFactor - discountFactor);
            curSummary.exciseTax += exciseTax * factor;
            curSummary.cityTax += cityTax * factor;
            curSummary.countyTax += countyTax * factor;
            curSummary.stateTax += stateTax * factor;
            curSummary.fedralTax += federalTax * factor;
            curSummary.totalTax += totalTax * factor;
            curSummary.cogs += totalCogsFactor;
            curSummary.deliveryFees += deliveryFees * factor;
            curSummary.creditCardFees += creditCardFees * factor;
            curSummary.netProfit += (grossReceiptsFactor - totalCogsFactor);
            curSummary.numberOfVisits += numOfVisits;

            // apply to total

            totalRevenuesSummary.grossReceipt += grossReceiptsFactor;
            totalRevenuesSummary.cannabisRevenue += cannabisRevenue * factor;
            totalRevenuesSummary.nonCannabisRevenue += nonCannabisRevenue * factor;
            totalRevenuesSummary.cannabisDiscounts += cannabisDiscounts * factor;
            totalRevenuesSummary.nonCannabisDiscounts += nonCananbisDiscounts * factor;
            totalRevenuesSummary.discount += discounts * factor;
            totalRevenuesSummary.afterTaxDiscount += afterTaxDiscount * factor;
            totalRevenuesSummary.grossRevenue += (cannabisRevenueFactor + nonCannabisRevenueFactor - discountFactor);
            totalRevenuesSummary.exciseTax += exciseTax * factor;
            totalRevenuesSummary.cityTax += cityTax * factor;
            totalRevenuesSummary.countyTax += countyTax * factor;
            totalRevenuesSummary.stateTax += stateTax * factor;
            totalRevenuesSummary.fedralTax += federalTax * factor;
            totalRevenuesSummary.totalTax += totalTax * factor;
            totalRevenuesSummary.cogs += totalCogsFactor;
            totalRevenuesSummary.deliveryFees += deliveryFees * factor;
            totalRevenuesSummary.creditCardFees += creditCardFees * factor;
            totalRevenuesSummary.netProfit += (grossReceiptsFactor - totalCogsFactor);
            totalRevenuesSummary.numberOfVisits += numOfVisits;

        }

        report.add(convert(totalRevenuesSummary, useFederal));
        report.add(convert(adultUseRevenuesSummary, useFederal));
        report.add(convert(mmicRevenuesSummary, useFederal));
        report.add(convert(thirdPartyRevenuesSummary, useFederal));
        report.add(convert(otherSummary, useFederal));
        return report;
    }

    private HashMap<String, Object> convert(RevenueSummary revenueSummary, boolean useFed) {
        HashMap<String, Object> data = new HashMap<>();

        data.put(attrs[0], revenueSummary.name);
        data.put(attrs[1], prepareAmount(revenueSummary.cannabisRevenue));
        data.put(attrs[2], prepareAmount(revenueSummary.nonCannabisRevenue));
        data.put(attrs[3], prepareAmount(revenueSummary.cannabisDiscounts));
        data.put(attrs[4], prepareAmount(revenueSummary.nonCannabisDiscounts));
        data.put(attrs[5], prepareAmount(revenueSummary.discount));
        data.put(attrs[6], prepareAmount(revenueSummary.afterTaxDiscount));
        data.put(attrs[7], prepareAmount(revenueSummary.grossRevenue));
        data.put(attrs[8], prepareAmount(revenueSummary.cityTax));
        data.put(attrs[9], prepareAmount(revenueSummary.countyTax));
        data.put(attrs[10], prepareAmount(revenueSummary.stateTax));
        data.put(attrs[11], prepareAmount(revenueSummary.totalTax));
        data.put(attrs[12], prepareAmount(revenueSummary.deliveryFees));
        data.put(attrs[13], prepareAmount(revenueSummary.creditCardFees));
        data.put(attrs[14], prepareAmount(revenueSummary.grossReceipt));
        data.put(attrs[15], prepareAmount(revenueSummary.cogs));
        data.put(attrs[16], prepareAmount(revenueSummary.netProfit));
        data.put(attrs[17], revenueSummary.numberOfVisits);
        if (useFed) {
            data.put("Federal Tax", prepareAmount(revenueSummary.fedralTax));
        }

        return data;
    }

    private DollarAmount prepareAmount(double amount) {
        return new DollarAmount(NumberUtils.round(amount, 2));
    }

    private class RevenueSummary {
        String name = "";
        double revenue = 0d;
        double cannabisRevenue = 0d;
        double nonCannabisRevenue = 0d;
        double cannabisDiscounts = 0d;
        double nonCannabisDiscounts = 0d;
        double grossRevenue = 0d;
        double discount = 0d;
        double afterTaxDiscount = 0d;
        double exciseTax = 0d;
        double cityTax = 0d;
        double countyTax = 0d;
        double stateTax = 0d;
        double fedralTax = 0d;
        double totalTax = 0d;
        double grossReceipt = 0d;
        double cogs = 0d;
        double netProfit = 0d;
        double numberOfVisits = 0d;
        double deliveryFees = 0d;
        double creditCardFees = 0d;
    }


    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }


}
