package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;

import java.util.*;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class SalesByDayGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Date", "Transactions", "Subtotal", "Discount", "After Tax Discount", "City Tax", "County Tax", "State Tax", "Total Taxes", "Delivery Fee", "Gross Receipts", "Percent of Total"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private ProductRepository productRepository;

    public SalesByDayGatherer(TransactionRepository transactionRepository, ProductRepository productRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.PERCENTAGE};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Day Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        Double totalSales = 0.0;
        HashMap<Long, DailySalesStat> dailySalesMap = new HashMap<>();
        List<Long> sortedKeys = new ArrayList<>(); //keep track of the keys in order so we can keep the sort order in the report
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        int factor = 1;
        for (Transaction t : results) {

            //calculate Pre-Taxes
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }


            double total = t.getCart().getTotal().doubleValue();
            double creditCardFees = 0d;
            double deliveryFees = 0d;
            double afterTaxDiscount = 0d;


            double totalPreTax = 0;
            for (OrderItem item : t.getCart().getItems()) {
                Product p = productMap.get(item.getProductId());
                if (p != null) {
                    totalPreTax += item.getCalcPreTax().doubleValue();
                }
            }

            double totalTax = (t.getCart().getTotalCalcTax().doubleValue() - t.getCart().getTotalPreCalcTax().doubleValue()) * factor;

            if (t.getCart().getAppliedAfterTaxDiscount() != null && t.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount().doubleValue() * factor;
            }

            if (t.getCart().getCreditCardFee() != null && t.getCart().getCreditCardFee().doubleValue() > 0) {
                creditCardFees += t.getCart().getCreditCardFee().doubleValue() * factor;
            }

            if (t.getCart().getDeliveryFee() != null && t.getCart().getDeliveryFee().doubleValue() > 0) {
                deliveryFees += t.getCart().getDeliveryFee().doubleValue() * factor;
            }

            if (t.getTransType() == Transaction.TransactionType.Refund
                    && t.getCart().getRefundOption() == Cart.RefundOption.Void
                    && t.getCart().getSubTotal().doubleValue() == 0) {
                creditCardFees = 0;
                deliveryFees = 0;
                afterTaxDiscount = 0;
                total = 0;
            }

            long key = new DateTime(t.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();
            if (dailySalesMap.get(key) == null) {

                DailySalesStat oldDay = new DailySalesStat();
                oldDay.date = key;
                oldDay.transactions++;
                oldDay.subtotal += t.getCart().getSubTotal().doubleValue() * factor;
                oldDay.deliveryFees += deliveryFees * factor;
                oldDay.discount += t.getCart().getTotalDiscount().doubleValue() * factor;
                oldDay.afterTaxDiscount += afterTaxDiscount * factor;
                oldDay.tax += totalTax;
                oldDay.total += total * factor;
                dailySalesMap.put(key, oldDay);

                if (t.getCart().getTaxResult() != null) {
                    oldDay.totalPreTax = t.getCart().getTaxResult().getTotalPreCalcTax().doubleValue() * factor;
                    oldDay.cityTax = t.getCart().getTaxResult().getTotalCityTax().doubleValue() * factor;
                    oldDay.stateTax = t.getCart().getTaxResult().getTotalStateTax().doubleValue() * factor;
                    oldDay.countyTax = t.getCart().getTaxResult().getTotalCountyTax().doubleValue() * factor;
                }
                sortedKeys.add(key);
            } else {
                DailySalesStat oldDay = dailySalesMap.get(key);
                oldDay.transactions++;
                oldDay.subtotal += t.getCart().getSubTotal().doubleValue() * factor;
                oldDay.deliveryFees += deliveryFees * factor;
                oldDay.discount += t.getCart().getTotalDiscount().doubleValue() * factor;
                oldDay.afterTaxDiscount += afterTaxDiscount * factor;
                oldDay.tax += totalTax;
                oldDay.total += total * factor;

                if (t.getCart().getTaxResult() != null) {
                    oldDay.totalPreTax = t.getCart().getTaxResult().getTotalPreCalcTax().doubleValue() * factor;
                    oldDay.cityTax += t.getCart().getTaxResult().getTotalCityTax().doubleValue() * factor;
                    oldDay.stateTax += t.getCart().getTaxResult().getTotalStateTax().doubleValue() * factor;
                    oldDay.countyTax += t.getCart().getTaxResult().getTotalCountyTax().doubleValue() * factor;
                }
                dailySalesMap.put(key, oldDay);
            }
        }

        for (Long key : dailySalesMap.keySet()) {
            totalSales += dailySalesMap.get(key).total;
        }

        for (Long key : sortedKeys) {
            HashMap<String, Object> data = new HashMap<>();
            DailySalesStat dss = dailySalesMap.get(key);
            data.put(attrs[0], ProcessorUtil.dateString(dss.date));
            data.put(attrs[1], dss.transactions);
            data.put(attrs[2], new DollarAmount(dss.subtotal));
            data.put(attrs[3], new DollarAmount(dss.discount));
            data.put(attrs[4], new DollarAmount(dss.afterTaxDiscount));
            data.put(attrs[5], new DollarAmount(dss.cityTax));
            data.put(attrs[6], new DollarAmount(dss.countyTax));
            data.put(attrs[7], new DollarAmount(dss.stateTax));
            data.put(attrs[8], new DollarAmount(dss.tax));
            data.put(attrs[9], new DollarAmount(dss.deliveryFees));
            data.put(attrs[10], new DollarAmount(dss.total));
            data.put(attrs[11], new Percentage(dss.total / totalSales));
            report.add(data);
        }
        return report;
    }

    private class DailySalesStat {
        long date;
        int transactions;
        double subtotal;
        double discount;
        double afterTaxDiscount = 0;
        double tax;
        double total;
        double deliveryFees;
        double totalPreTax;
        double cityTax = 0;
        double countyTax = 0;
        double stateTax = 0;

        public DailySalesStat() {
        }
    }
}
