package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

public class SalesByHourGatherer implements Gatherer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalesByHourGatherer.class);

    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private String[] attrs = new String[]{"Hour", "Recreational Sales", "Medical Sales", "Total Sales", "Recreational Discounts", "Medicinal Discounts", "Total Discounts", "City Tax", "County Tax", "State Tax", "Pre ALExcise Tax", "Pre NALExcise Tax", "Post ALExcise Tax", "Post NALExcise Tax", "Credit Card Fees", "Gross Receipts", "Number of Transactions"};

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByHourGatherer(TransactionRepository transactionRepository, MemberRepository memberRepository) {
        this.transactionRepository = transactionRepository;
        this.memberRepository = memberRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        List<Transaction> transactions = new ArrayList<>();
        if (filter.getType() == ReportType.SALES_BY_HOUR) {
            long startDate = filter.getTimeZoneStartDateMillis();
            long endDate = filter.getTimeZoneEndDateMillis();


            while (startDate < endDate) {
                long end = new DateTime(startDate).plusDays(1).minusSeconds(1).getMillis();
                if (end > endDate) {
                    end = endDate;
                }
                LOGGER.info(String.format("Start: %s End : %s", new DateTime(startDate), new DateTime(endDate)));

                Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                        startDate, end);

                transactions.addAll(0, Lists.newArrayList(results));
                startDate = new DateTime(startDate).plusDays(1).getMillis();
            }
        } else {
            Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                    filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
            transactions.addAll(0, Lists.newArrayList(results));
        }
        return prepareSalesByHour(filter, transactions);

    }

    private GathererReport prepareSalesByHour(ReportFilter filter, List<Transaction> results) {
        GathererReport report = new GathererReport(filter, "Sales By Hour Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<Integer, HoursSalesStat> hoursSalesMap = new HashMap<>();
        List<Integer> sortedKeys = new ArrayList<>();
        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();

        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        for (int i = 0; i < 24; i++) {
            HoursSalesStat stat = new HoursSalesStat();
            sortedKeys.add(i);
            hoursSalesMap.put(i, stat);
        }
        int factor = 1;
        for (Transaction transaction : transactions) {
            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            DateTime processedTime = new DateTime(transaction.getProcessedTime()).minusMinutes(filter.getTimezoneOffset());
            int hourOfDay = processedTime.getHourOfDay();
//            processedTime = processedTime.withTimeAtStartOfDay();
//            processedTime = processedTime.plusHours(hourOfDay);
            int key = hourOfDay;
            HoursSalesStat hoursSalesStat = null;
            if (hoursSalesMap.get(key) == null) {
                hoursSalesStat = new HoursSalesStat();
                //commented for processing by 24 hour format
              /*DateTimeFormatter dtf = DateTimeFormat.forPattern("hh:mm a");
              hoursSalesStat.hour = dtf.print(processedTime);*/
                sortedKeys.add(key);
            } else {
                hoursSalesStat = hoursSalesMap.get(key);
            }

            hoursSalesStat.transactions++;
            Cart cart = transaction.getCart();
            if (cart != null) {
                BigDecimal sale = cart.getSubTotal();
                ConsumerType consumerType = cart.getFinalConsumerTye();

                if (consumerType == null || consumerType.equals(ConsumerType.AdultUse)) {
                    hoursSalesStat.recreationalSale += sale.doubleValue() * factor;
                    hoursSalesStat.recDiscounts += cart.getTotalDiscount().doubleValue() * factor;
                } else {
                    hoursSalesStat.medicinalSale += sale.doubleValue() * factor;
                    hoursSalesStat.medicinalDiscounts += cart.getTotalDiscount().doubleValue() * factor;
                }


                hoursSalesStat.totalSale += (sale.doubleValue() * factor);//hoursSalesStat.medicinalSale + hoursSalesStat.recreationalSale;
                hoursSalesStat.totalDiscounts += cart.getTotalDiscount().doubleValue() * factor;

                if (cart.getTaxResult() != null) {
                    hoursSalesStat.cityTax += cart.getTaxResult().getTotalCityTax().doubleValue() * factor;
                    hoursSalesStat.stateTax += cart.getTaxResult().getTotalStateTax().doubleValue() * factor;
                    hoursSalesStat.countyTax += cart.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                    hoursSalesStat.preALExciseTax += cart.getTaxResult().getTotalALExciseTax().doubleValue() * factor;
                    hoursSalesStat.preNALExciseTax += cart.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;
                    hoursSalesStat.postALExciseTax += cart.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                    hoursSalesStat.postNALExciseTax += cart.getTaxResult().getTotalExciseTax().doubleValue() * factor;
                }


                double creditCardFees = 0d;
                if (cart.getCreditCardFee() != null && cart.getCreditCardFee().doubleValue() > 0) {
                    creditCardFees = cart.getCreditCardFee().doubleValue() * factor;
                }

                double total = cart.getTotal().doubleValue() * factor;
                if (transaction.getTransType() == Transaction.TransactionType.Refund
                        && cart.getRefundOption() == Cart.RefundOption.Void
                        && cart.getSubTotal().doubleValue() == 0) {
                    total = 0;
                    creditCardFees = 0d;
                }

                hoursSalesStat.grossReceipts += total;
                hoursSalesStat.creditCardFees += creditCardFees;
            }
            hoursSalesMap.put(key, hoursSalesStat);
        }

        for (Integer key : sortedKeys) {
            HashMap<String, Object> data = new HashMap<>();
            HoursSalesStat hoursSalesStat = hoursSalesMap.get(key);
            data.put(attrs[0], key);
            data.put(attrs[1], new DollarAmount(hoursSalesStat.recreationalSale));
            data.put(attrs[2], new DollarAmount(hoursSalesStat.medicinalSale));
            data.put(attrs[3], new DollarAmount(hoursSalesStat.totalSale));
            data.put(attrs[4], new DollarAmount(hoursSalesStat.medicinalDiscounts));
            data.put(attrs[5], new DollarAmount(hoursSalesStat.recDiscounts));
            data.put(attrs[6], new DollarAmount(hoursSalesStat.totalDiscounts));
            data.put(attrs[7], new DollarAmount(hoursSalesStat.cityTax));
            data.put(attrs[8], new DollarAmount(hoursSalesStat.countyTax));
            data.put(attrs[9], new DollarAmount(hoursSalesStat.stateTax));
            data.put(attrs[10], new DollarAmount(hoursSalesStat.preALExciseTax));
            data.put(attrs[11], new DollarAmount(hoursSalesStat.preNALExciseTax));
            data.put(attrs[12], new DollarAmount(hoursSalesStat.postALExciseTax));
            data.put(attrs[13], new DollarAmount(hoursSalesStat.postNALExciseTax));
            data.put(attrs[14], new DollarAmount(hoursSalesStat.creditCardFees));
            data.put(attrs[15], new DollarAmount(hoursSalesStat.grossReceipts));
            data.put(attrs[16], hoursSalesStat.transactions);
            report.add(data);
        }
        return report;
    }

    private class HoursSalesStat {
        String hour;
        int transactions = 0;
        double recreationalSale = 0;
        double medicinalSale = 0;
        double totalSale = 0;
        double recDiscounts = 0;
        double medicinalDiscounts = 0;
        double totalDiscounts = 0;
        double cityTax = 0;
        double countyTax = 0;
        double stateTax = 0;
        double preALExciseTax = 0;
        double preNALExciseTax = 0;
        double postALExciseTax = 0;
        double postNALExciseTax = 0;
        double creditCardFees = 0;

        double grossReceipts = 0;

        public HoursSalesStat() {
        }
    }
}
