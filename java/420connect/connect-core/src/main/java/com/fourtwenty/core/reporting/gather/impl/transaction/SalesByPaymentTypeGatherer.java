package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class SalesByPaymentTypeGatherer implements Gatherer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SalesByPaymentTypeGatherer.class);

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private TerminalRepository terminalRepository;
    private String[] attrs = new String[]{"Date", "Terminal", "Payment Type", "Gross Receipts", "Transactions", "Average Transaction"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private ReportFilter filter;

    public SalesByPaymentTypeGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Payment Type", reportHeaders);
        this.filter = filter;
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        /*Iterable<SalesByPaymentTypeTerminal> results = transactionRepository.getSalesByPaymentTypeTerminal(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());*/
        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(filter.getCompanyId());

        HashMap<Integer, DailySalesByPaymentType> daysInResults = new HashMap();
        ArrayList<DailySalesByPaymentType> dayList = new ArrayList<>();

        int factor = 1;
        for (Transaction t : results) {
            //For old refund transaction
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Long day = getKey(t);
            String payment = t.getCart().getPaymentOption().name();
            Terminal terminal = terminalHashMap.get(t.getSellerTerminalId());
            if (terminal != null) {
                int hash = (day + terminal.getName() + payment).hashCode();
                if (t.getCart() == null) {
                    LOGGER.info("Null cart on transaction: ", t.getId());
                }
                DailySalesByPaymentType dspt;
                if (daysInResults.containsKey(hash)) {
                    dspt = daysInResults.get(hash);
                } else {
                    dspt = new DailySalesByPaymentType(day, payment, terminal.getName());
                }

                dspt.sales += t.getCart().getTotal().doubleValue() * factor;
                /*if (t.getCart().getPaymentOption() == Cart.PaymentOption.CashlessATM && t.getCart().getChangeDue() != null) {
                    DailySalesByPaymentType cashSales;
                    int salesHash = (day + terminal.getName() + Cart.PaymentOption.Cash.name()).hashCode();
                    if (daysInResults.containsKey(hash)) {
                        cashSales = daysInResults.get(salesHash);
                    } else {
                        cashSales = new DailySalesByPaymentType(day, Cart.PaymentOption.Cash.name(), terminal.getName());
                    }
                    cashSales.sales -= t.getCart().getChangeDue().doubleValue();
                    daysInResults.put(salesHash, cashSales);
                }*/
                dspt.count += (factor > 0) ? 1 : 0;
                daysInResults.put(hash, dspt);

            }

        }
        for (DailySalesByPaymentType dspt : daysInResults.values()) {
            dayList.add(dspt);
        }
        Collections.sort(dayList);
        for (DailySalesByPaymentType dspt : dayList) {
            report.add(dspt.getData());
        }

        return report;
    }

    private class DailySalesByPaymentType implements Comparable<DailySalesByPaymentType> {
        Long day;
        Double sales;
        int count;
        String terminal;
        String paymentType;

        DailySalesByPaymentType(Long day, String paymentType, String terminal) {
            this.day = day;
            this.paymentType = paymentType;
            this.terminal = terminal;
            sales = 0d;
            count = 0;
        }

        double getAverageTransaction() {
            return (count == 0) ? 0 : (sales) / count;
        }

        HashMap<String, Object> getData() {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], ProcessorUtil.dateString(day));
            data.put(attrs[1], terminal);
            data.put(attrs[2], paymentType);
            data.put(attrs[3], new DollarAmount(sales));
            data.put(attrs[4], count);
            data.put(attrs[5], new DollarAmount(getAverageTransaction()));
            return data;
        }

        @Override
        public int compareTo(DailySalesByPaymentType o) {
            int day = new Long(this.day - o.day).intValue();
            if (day == 0) {
                if (terminal.compareTo(o.terminal) == 0) {
                    return paymentType.compareTo(o.paymentType);
                }
                return terminal.compareTo(o.terminal);
            }
            return this.day.compareTo(o.day);
        }

        @Override
        public int hashCode() {
            return (day + terminal + paymentType).hashCode();
        }
    }

    private Long getKey(Transaction t) {
        return new DateTime(t.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().plusMinutes(filter.getTimezoneOffset()).getMillis();
    }


}
