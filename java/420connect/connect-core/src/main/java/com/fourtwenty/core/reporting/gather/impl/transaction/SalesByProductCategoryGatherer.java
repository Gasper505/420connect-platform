package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 7/8/2016.
 */
public class SalesByProductCategoryGatherer implements Gatherer {
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    private String[] attrs = new String[]{
            "Product Category",  //0
            "# Trans", //1
            "COGS", //2
            "Retail Value", //3
            "Product Discounts", //4
            "Subtotal Sales", //5
            "Cart Discounts", //6
            "Sales", //7
            "Pre ALExcise Tax", //8
            "Pre NALExcise Tax", //9
            "Post ALExcise Tax", //10
            "Post NALExcise Tax", //11
            "City Tax", //12
            "County Tax", //13
            "State Tax", //14
            "Federal Tax", //15
            "Total Tax", //16
            "Delivery Fees", //17
            "Credit/Debit Card Fees", //18
            "After Tax Discounts", //19
            "Gross Receipt",//20
            "Units Sold" //21
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByProductCategoryGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, //product category // 0
                GathererReport.FieldType.NUMBER, // 1
                GathererReport.FieldType.CURRENCY, // 2
                GathererReport.FieldType.CURRENCY, // 3
                GathererReport.FieldType.CURRENCY, // 4
                GathererReport.FieldType.CURRENCY, // 5
                GathererReport.FieldType.CURRENCY, // 6
                GathererReport.FieldType.CURRENCY, // 7
                GathererReport.FieldType.CURRENCY, // 8
                GathererReport.FieldType.CURRENCY, // 9
                GathererReport.FieldType.CURRENCY, // 10
                GathererReport.FieldType.CURRENCY, // 11
                GathererReport.FieldType.CURRENCY, // 12
                GathererReport.FieldType.CURRENCY, // 13
                GathererReport.FieldType.CURRENCY, // 14
                GathererReport.FieldType.CURRENCY, // 15
                GathererReport.FieldType.CURRENCY, // 16
                GathererReport.FieldType.CURRENCY, // 17
                GathererReport.FieldType.CURRENCY, // 18
                GathererReport.FieldType.CURRENCY, // 19
                GathererReport.FieldType.CURRENCY,// 20
                GathererReport.FieldType.NUMBER // 21
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Product Category", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, CategorySale> results = prepareCategorySaleReport(filter);
/*
            "Product Category",  //0
            "# Trans", //1
            "COGS", //2
            "Retail Value", //3
            "Product Discounts", //4
            "Subtotal Sales", //5
            "Cart Discounts", //6
            "Sales", //7
            "Pre ALExcise Tax", //8
            "Pre NALExcise Tax", //9
            "Post ALExcise Tax", //10
            "Post NALExcise Tax", //11
            "City Tax", //12
            "County Tax", //13
            "State Tax", //14
            "Federal Tax", //15
            "Total Tax", //16
            "Delivery Fees", //17
            "Credit/Debit Card Fees", //18
            "After Tax Discounts", //19
            "Gross Receipt"//20
            */

        for (String name : results.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            CategorySale categorySale = results.get(name);
            data.put(attrs[0], name);
            data.put(attrs[1], categorySale.transCount);
            data.put(attrs[2], new DollarAmount(categorySale.cogs));
            data.put(attrs[3], new DollarAmount(categorySale.retailValue));
            data.put(attrs[4], new DollarAmount(categorySale.itemDiscounts));
            data.put(attrs[5], new DollarAmount(categorySale.subTotals));
            data.put(attrs[6], new DollarAmount(categorySale.discounts));
            data.put(attrs[7], new DollarAmount(categorySale.sales));
            data.put(attrs[8], new DollarAmount(categorySale.preALExciseTax));
            data.put(attrs[9], new DollarAmount(categorySale.preNALExciseTax));
            data.put(attrs[10], new DollarAmount(categorySale.postALExciseTax));
            data.put(attrs[11], new DollarAmount(categorySale.postNALExciseTax));
            data.put(attrs[12], new DollarAmount(categorySale.cityTax));
            data.put(attrs[13], new DollarAmount(categorySale.countyTax));
            data.put(attrs[14], new DollarAmount(categorySale.stateTax));
            data.put(attrs[15], new DollarAmount(categorySale.federalTax));
            data.put(attrs[16], new DollarAmount(categorySale.tax));
            data.put(attrs[17], new DollarAmount(categorySale.deliveryFees));
            data.put(attrs[18], new DollarAmount(categorySale.creditCardFees));
            data.put(attrs[19], new DollarAmount(categorySale.afterTaxDiscount));

            data.put(attrs[20], new DollarAmount(categorySale.grossReceipt));
            data.put(attrs[21], new DollarAmount(categorySale.unitSold));

            report.add(data);
        }

        return report;
    }

    /**
     * Public method to prepare sales by category report
     *
     * @param filter : report filter
     */
    public HashMap<String, CategorySale> prepareCategorySaleReport(ReportFilter filter) {
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductBatch> batchHashMap = batchRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, CategorySale> results = new HashMap<>();
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        double unitSold = 0;
        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : batchHashMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        Iterable<Product> products = productRepository.listAll(filter.getCompanyId());
        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        int factor = 1;
        for (Transaction t : transactions) {
            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            // Calculate discounts proportionally so we can apply taxes

            HashMap<String, Double> propRatioMap = new HashMap<>();

            if (t.getCart().getItems().size() > 0) {
                double total = 0.0;
                for (OrderItem orderItem : t.getCart().getItems()) {
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {
                        if (product.isDiscountable()) {
                            total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                        }
                    }
                }

                // Calculate the ratio to be applied
                for (OrderItem orderItem : t.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {

                        propRatioMap.put(orderItem.getId(), 1d); // 100 %
                        if (product.isDiscountable() && total > 0) {
                            double finalCost = orderItem.getFinalPrice().doubleValue();
                            double ratio = finalCost / total;

                            propRatioMap.put(orderItem.getId(), ratio);
                        }
                    }

                }
            }

            double totalPreTax = 0;
            double cTotal = 0;
            List<String> visited = new ArrayList<>();
            for (OrderItem item : t.getCart().getItems()) {

                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                double taxRate = 0d;

                taxRate = t.getCart().getTax().doubleValue();
                if (item.getTaxInfo() != null
                        && item.getTaxType() == TaxInfo.TaxType.Custom
                        && item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                    taxRate = item.getTaxInfo().getTotalTax().doubleValue();
                }
                if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PreTaxed) {
                    taxRate = 0;
                }

                Product p = productMap.get(item.getProductId());
                if (p != null) {
                    totalPreTax += item.getCalcPreTax().doubleValue();

                    String catName = categoryMap.get(p.getCategoryId()).getName();
                    CategorySale categorySale = results.get(catName);
                    if (categorySale == null) {
                        categorySale = new CategorySale();
                        results.put(catName, categorySale);
                    }
                    // only count transCount if we haven't already counted -- doing this to avoid counting multiple categories in 1 trans
                    if (!visited.contains(p.getCategoryId())) {
                        categorySale.transCount++;
                        visited.add(p.getCategoryId());
                    }


                    Double ratio = propRatioMap.get(item.getId());
                    if (ratio == null) {
                        ratio = 1d;
                    }

                    Double propCartDiscount = t.getCart().getCalcCartDiscount() != null ? t.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                    Double propDeliveryFee = t.getCart().getDeliveryFee() != null ? t.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                    Double propCCFee = t.getCart().getCreditCardFee() != null ? t.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                    Double propAfterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount() != null ? t.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;


//                  double targetCartDiscount = t.getCart().getCalcCartDiscount().doubleValue() * ratio;
                    cTotal += propCartDiscount;

                    // calculated postTax
                    double itemDiscount = item.getCalcDiscount().doubleValue();


                    double expectedFinalCost = (item.getCost().doubleValue() - itemDiscount);
                    if (NumberUtils.round(expectedFinalCost, 2) != NumberUtils.round(item.getFinalPrice().doubleValue(), 2)) {
                        System.out.println("Difference in final cost");
                    }

                    double sales = NumberUtils.round(item.getFinalPrice().doubleValue() - propCartDiscount, 5);


                    unitSold = item.getQuantity().doubleValue();
                    double postTax = 0;

                    double preALExciseTax = 0;
                    double preNALExciseTax = 0;
                    double postALExciseTax = 0;
                    double postNALExciseTax = 0;
                    double totalCityTax = 0;
                    double totalCountyTax = 0;
                    double totalStateTax = 0;
                    double totalFedTax = 0;

                    double subTotalAfterdiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                    if (item.getTaxResult() != null) {
                        preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                        preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                        postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                        postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            totalCityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                            totalStateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                            totalCountyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                            totalFedTax = item.getTaxResult().getTotalFedTax().doubleValue();

                            postTax += item.getTaxResult().getTotalPostCalcTax().doubleValue() + postALExciseTax + postNALExciseTax;
                        }
                    }
                    if (postTax == 0) {
                        if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                            TaxInfo taxInfo = item.getTaxInfo();
                            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                                totalCityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                                totalStateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                                totalFedTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();

                                postTax = totalCityTax + totalStateTax + totalFedTax;
                            }
                        }
                    }

                    if (postTax == 0) {
                        postTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                    }

                    double totalTax = postTax;

                    categorySale.name = catName;
                    categorySale.retailValue += item.getCost().doubleValue() * factor;
                    categorySale.itemDiscounts += itemDiscount * factor;
                    categorySale.subTotals += item.getFinalPrice().doubleValue() * factor;
                    categorySale.discounts += propCartDiscount * factor;
                    categorySale.sales += sales * factor;

                    categorySale.totalPreTax += totalPreTax * factor;
                    categorySale.preALExciseTax += preALExciseTax * factor;
                    categorySale.preNALExciseTax += preNALExciseTax * factor;
                    categorySale.postALExciseTax += postALExciseTax * factor;
                    categorySale.postNALExciseTax += postNALExciseTax * factor;
//                        categorySale.exciseTax += totalExciseTax;
                    categorySale.cityTax += totalCityTax * factor;
                    categorySale.countyTax += totalCountyTax * factor;
                    categorySale.stateTax += totalStateTax * factor;
                    categorySale.federalTax += totalFedTax * factor;
                    categorySale.tax += totalTax * factor;

                    categorySale.deliveryFees += propDeliveryFee * factor;
                    categorySale.creditCardFees += propCCFee * factor;
                    categorySale.afterTaxDiscount += propAfterTaxDiscount * factor;

                    categorySale.unitSold += unitSold * factor;
                    double gross = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4) + NumberUtils.round(propDeliveryFee, 4) +
                            NumberUtils.round(totalTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);
                    categorySale.grossReceipt += gross * factor;

                    // caclulate cost of goods
                    boolean calculated = false;
                    double cogs = 0;
                    if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                        // if prepackage is set, use prepackage
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                        if (prepackageProductItem != null) {
                            ProductBatch targetBatch = batchHashMap.get(prepackageProductItem.getBatchId());
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            if (prepackage != null && targetBatch != null) {
                                calculated = true;

                                BigDecimal unitValue = prepackage.getUnitValue();
                                if (unitValue == null || unitValue.doubleValue() == 0) {
                                    ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                    unitValue = weightTolerance.getUnitValue();
                                }
                                // calculate the total quantity based on the prepackage value
                                cogs += calcCOGS(item.getQuantity().doubleValue() * unitValue.doubleValue(), targetBatch);
                            }
                        }
                    } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : item.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = batchHashMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                }
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = getUnitCost(p, recentBatchMap, productsByCompanyLinkId);
                        cogs = unitCost * item.getQuantity().doubleValue();
                    }

                    categorySale.cogs += cogs * factor;

                }


            }

            int cartDiscount = (int) (t.getCart().getCalcCartDiscount().doubleValue() * 100);
            int myCTotal = (int) (NumberUtils.round(cTotal, 2) * 100);
            if (myCTotal != cartDiscount) {
                System.out.println("cart discount doesn't match");
            }

        }
        return results;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public static final class CategorySale {
        public String name;
        public double retailValue = 0;
        public double itemDiscounts = 0;
        public double subTotals = 0;
        public double discounts = 0;
        public double afterTaxDiscount = 0;
        public double sales = 0;
        public double deliveryFees = 0;
        public double creditCardFees = 0;
        public double totalPreTax = 0;
        public double preALExciseTax = 0;
        public double preNALExciseTax = 0;
        public double postALExciseTax = 0;
        public double postNALExciseTax = 0;
        public double cityTax = 0;
        public double stateTax = 0;
        public double countyTax = 0;
        public double tax = 0;
        public double grossReceipt = 0;
        public double cogs = 0;
        public double transCount = 0;
        public double federalTax = 0;
        public double unitSold = 0;
    }
}
