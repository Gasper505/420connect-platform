package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.model.reportmodels.SalesByProduct;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public class SalesByProductGatherer implements Gatherer {
    private String[] attrs = new String[]{"Product", // 0
            "SKU", //1
            "Category", // 2
            "Vendor", // 3
            "Brand", // 4
            "Units Sold", // 5
            "COGS", // 6
            "Retail Value", // 7
            "Subtotal Sales", // 8
            "Pre ALExcise Tax", // 9
            "Pre NALExcise Tax", // 10
            "Post ALExcise Tax", // 11
            "Post NALExcise Tax", // 12
            "City Tax", // 13
            "County Tax", // 14
            "State Tax", // 15
            "Federal Tax", // 16
            "Total Tax", // 17
            "Credit/Debit Card Fees", // 18
            "Delivery Fees", // 19
            "After Tax Discounts", // 20
            "Margin", // 21
            "Revenue/Unit", // 22
            "Gross receipts", // 23
            "% of Sales"}; // 24


    private List<String> reportHeaders = new ArrayList<>(attrs.length);

    @Inject
    private ProductRepository productRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductBatchRepository batchRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private BrandRepository brandRepository;

    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByProductGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // 0
                GathererReport.FieldType.STRING, // 1
                GathererReport.FieldType.STRING, // 2
                GathererReport.FieldType.STRING, // 3
                GathererReport.FieldType.STRING, // 4
                GathererReport.FieldType.NUMBER, // 5
                GathererReport.FieldType.CURRENCY, // 6
                GathererReport.FieldType.CURRENCY, // 7
                GathererReport.FieldType.CURRENCY, // 8
                GathererReport.FieldType.CURRENCY, // 9
                GathererReport.FieldType.CURRENCY, // 10
                GathererReport.FieldType.CURRENCY, // 11
                GathererReport.FieldType.CURRENCY, // 12
                GathererReport.FieldType.CURRENCY, // 13
                GathererReport.FieldType.CURRENCY, // 14
                GathererReport.FieldType.CURRENCY, // 15
                GathererReport.FieldType.CURRENCY, // 16
                GathererReport.FieldType.CURRENCY, // 17
                GathererReport.FieldType.CURRENCY, // 18
                GathererReport.FieldType.CURRENCY, // 19
                GathererReport.FieldType.CURRENCY, // 20
                GathererReport.FieldType.CURRENCY, // 21
                GathererReport.FieldType.CURRENCY, // 22
                GathererReport.FieldType.CURRENCY, // 23
                GathererReport.FieldType.PERCENTAGE// 24
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Product Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        Iterable<Product> products = productRepository.listAll(filter.getCompanyId());
        HashMap<String, Vendor> vendorMap = vendorRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAllAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }

        HashMap<String, Brand> brandMap = brandRepository.listAllAsMap(filter.getCompanyId());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        Double totalSales = 0d;

        HashMap<String, Double> cogsByProduct = new HashMap<>();
        HashMap<String, Double> preTaxByProducts = new HashMap<>();

        HashMap<String, ProductSale> productSales = new HashMap<>();

        int factor = 1;
        for (Transaction t : transactions) {

            HashMap<String, Double> propRatioMap = new HashMap<>();
            double total = 0.0;
            for (OrderItem orderItem : t.getCart().getItems()) {

                // skip if old refund method
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                Product product = productMap.get(orderItem.getProductId());
                if (product != null && product.isDiscountable()) {
                    total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                }


            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : t.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                Product product = productMap.get(orderItem.getProductId());
                if (product != null) {

                    propRatioMap.put(orderItem.getId(), 1d); // 100 %
                    if (product.isDiscountable() && total > 0) {
                        double finalCost = orderItem.getFinalPrice().doubleValue();
                        double ratio = finalCost / total;

                        propRatioMap.put(orderItem.getId(), ratio);
                    }
                }

            }

            for (OrderItem item : t.getCart().getItems()) {

                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }


                factor = 1;
                if (Transaction.TransactionType.Refund == t.getTransType() && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                    factor = -1;
                }
                Product p1 = productMap.get(item.getProductId());
                if (p1 != null) {


                    Double cogs = 0d;
                    String identifier = p1.getId();
                    String pName = p1.getName();
                    String pSku = p1.getSku();
                    boolean calculated = false;
                    PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                    if (prepackageProductItem != null) {
                        ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                        Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                        if (prepackage != null && targetBatch != null) {
                            calculated = true;
                            identifier += "_" + prepackage.getId();
                            pName = String.format("%s (%s)", p1.getName(), prepackage.getName());
                            BigDecimal unitValue = prepackage.getUnitValue();
                            if (unitValue == null || unitValue.doubleValue() == 0) {
                                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                unitValue = weightTolerance.getUnitValue();
                            }
                            // calculate the total quantity based on the prepackage value
                            double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                            cogs += calcCOGS(unitsSold, targetBatch);
                            cogsByProduct.put(p1.getId(), cogs);
                        }
                    } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                        // otherwise, use quantity logs
                        for (QuantityLog quantityLog : item.getQuantityLogs()) {
                            if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                                ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                                if (targetBatch != null) {
                                    calculated = true;
                                    cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                                }
                            }
                        }
                    }

                    if (!calculated) {
                        double unitCost = getUnitCost(p1, recentBatchMap, productsByCompanyLinkId);
                        cogs = unitCost * item.getQuantity().doubleValue();
                    }


                    ProductSale productSale = productSales.get(identifier);
                    if (productSale == null) {
                        productSale = new ProductSale();
                        productSales.put(identifier, productSale);
                    }

                    ProductCategory cat = categoryMap.get(p1.getCategoryId());
                    String categoryName = "N/A";
                    if (cat != null) {
                        categoryName = cat.getName();
                    }
                    Vendor vendor = vendorMap.get(p1.getVendorId());
                    String vendorName = "N/A";
                    if (vendor != null) {
                        vendorName = vendor.getName();
                    }


                    Double ratio = propRatioMap.get(item.getId());
                    if (ratio == null) {
                        ratio = 1d;
                    }

                    Double propCartDiscount = t.getCart().getCalcCartDiscount() != null ? t.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                    Double propDeliveryFee = t.getCart().getDeliveryFee() != null ? t.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                    Double propCCFee = t.getCart().getCreditCardFee() != null ? t.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                    Double propAfterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount() != null ? t.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;


                    double quantitySold = item.getQuantity().doubleValue();
                    productSale.totalSold += NumberUtils.round((quantitySold * factor), 2);

                    double preALExciseTax = 0;
                    double preNALExciseTax = 0;
                    double postALExciseTax = 0;
                    double postNALExciseTax = 0;
                    double totalCityTax = 0;
                    double totalCountyTax = 0;
                    double totalStateTax = 0;
                    double totalFedTax = 0;
                    double postTax = 0;

                    double subTotalAfterDiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                    if (item.getTaxResult() != null) {
                        preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                        preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                        postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                        postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            totalCityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                            totalStateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                            totalCountyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                            totalFedTax = item.getTaxResult().getTotalFedTax().doubleValue();

                            postTax += item.getTaxResult().getTotalPostCalcTax().doubleValue() + postALExciseTax + postNALExciseTax;
                        }
                    }
                    if (postTax == 0) {
                        if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                            TaxInfo taxInfo = item.getTaxInfo();
                            if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                                totalCityTax = subTotalAfterDiscount * taxInfo.getCityTax().doubleValue();
                                totalStateTax = subTotalAfterDiscount * taxInfo.getStateTax().doubleValue();
                                totalFedTax = subTotalAfterDiscount * taxInfo.getFederalTax().doubleValue();
                                postTax = totalCityTax + totalStateTax + totalFedTax;
                            }
                        }
                    }

                    if (postTax == 0) {
                        postTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                    }

                    double grossReceipt = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4) + NumberUtils.round(propDeliveryFee, 4) +
                            NumberUtils.round(postTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);

                    double newMargin = (item.getFinalPrice().doubleValue() - cogs);
                    double margins = productSale.margins + (newMargin * factor);
                    double revUnit = 0;
                    if (productSale.totalSold > 0) {
                        revUnit = margins / productSale.totalSold;
                    }

                    Brand brand = brandMap.get(p1.getBrandId());

                    productSale.name = pName;
                    productSale.sku = pSku;
                    productSale.categoryName = categoryName;
                    productSale.vendor = vendorName;
                    productSale.brand = brand != null ? brand.getName() : "";
                    productSale.retailValue += (item.getCost().doubleValue() * factor);
                    productSale.subTotals += (item.getFinalPrice().doubleValue() * factor);
                    productSale.cogs += (cogs * factor);
                    productSale.preALExciseTax += (preALExciseTax * factor);
                    productSale.preNALExciseTax += (preNALExciseTax * factor);
                    productSale.postALExciseTax += (postALExciseTax * factor);
                    productSale.postNALExciseTax += (postNALExciseTax * factor);
                    productSale.cityTax += (totalCityTax * factor);
                    productSale.countyTax += (totalCountyTax * factor);
                    productSale.stateTax += (totalStateTax * factor);
                    productSale.federalTax += (totalFedTax * factor);
                    productSale.totalPostTax += (postTax * factor);
                    productSale.margins = (margins * factor);
                    productSale.revenuePerUnit = (revUnit * factor);
                    productSale.percentageOfSales = 0;
                    productSale.creditCardFees += (propCCFee * factor);
                    productSale.discount += (propCartDiscount * factor);
                    productSale.afterTaxDiscount += (propAfterTaxDiscount * factor);
                    productSale.deliveryFees += (propDeliveryFee * factor);
                    productSale.grossReceipt += grossReceipt * factor;

                    totalSales += (item.getFinalPrice().doubleValue() * factor);
                }
            }
        }

        List<ProductSale> sales = Lists.newArrayList(productSales.values());
        sales.sort(new Comparator<ProductSale>() {
            @Override
            public int compare(ProductSale o1, ProductSale o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        for (ProductSale productSale : sales) {
            HashMap<String, Object> data = new HashMap<>();

            data.put(attrs[0], productSale.name);
            data.put(attrs[1], productSale.sku);
            data.put(attrs[2], productSale.categoryName);
            data.put(attrs[3], productSale.vendor);
            data.put(attrs[4], productSale.brand);
            data.put(attrs[5], productSale.totalSold);
            data.put(attrs[6], new DollarAmount(productSale.cogs));
            data.put(attrs[7], new DollarAmount(productSale.retailValue));
            data.put(attrs[8], new DollarAmount(productSale.subTotals));
            data.put(attrs[9], new DollarAmount(productSale.preALExciseTax));
            data.put(attrs[10], new DollarAmount(productSale.preNALExciseTax));
            data.put(attrs[11], new DollarAmount(productSale.postALExciseTax));
            data.put(attrs[12], new DollarAmount(productSale.postNALExciseTax));
            data.put(attrs[13], new DollarAmount(productSale.cityTax));
            data.put(attrs[14], new DollarAmount(productSale.countyTax));
            data.put(attrs[15], new DollarAmount(productSale.stateTax));
            data.put(attrs[16], new DollarAmount(productSale.federalTax));
            data.put(attrs[17], new DollarAmount(productSale.totalPostTax));
            data.put(attrs[18], new DollarAmount(productSale.creditCardFees));
            data.put(attrs[19], new DollarAmount(productSale.deliveryFees));
            data.put(attrs[20], new DollarAmount(productSale.afterTaxDiscount));
            data.put(attrs[21], new DollarAmount(productSale.margins));
            data.put(attrs[22], new DollarAmount(productSale.revenuePerUnit));
            data.put(attrs[23], new DollarAmount(productSale.grossReceipt));

            double percentage = productSale.subTotals / totalSales;
            data.put(attrs[24], new Percentage(NumberUtils.round(percentage, 2)));


            report.add(data);

        }
        return report;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }

    private double getMargin(HashMap<String, ProductBatch> batchMap, SalesByProduct product) {
        ProductBatch batch = batchMap.get(product.getId());

        if (batch != null) {
            double unitSold = product.getUnitsSold() == null ? 0 : product.getUnitsSold();
            double sales = product.getSales() == null ? 0 : product.getSales();
            double batchCostPerUnit = batch.getFinalUnitCost().doubleValue();
            return sales - batchCostPerUnit * unitSold;
        } else {
            return 0;
        }
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public static final class ProductSale {
        public String name;
        public String sku;
        public String categoryName;
        public String vendor;
        public String brand;
        public double retailValue = 0;
        public double subTotals = 0;
        public double totalSold = 0;
        public double cogs = 0;
        public double margins = 0;
        public double totals = 0;
        public double preALExciseTax = 0;
        public double preNALExciseTax = 0;
        public double postALExciseTax = 0;
        public double postNALExciseTax = 0;
        public double cityTax = 0;
        public double stateTax = 0;
        public double countyTax = 0;
        public double federalTax = 0;
        public double creditCardFees = 0;
        public double totalPostTax = 0;
        public double revenuePerUnit = 0;
        public double percentageOfSales = 0;
        public double discount = 0.0;
        public double afterTaxDiscount = 0.0;
        public double deliveryFees = 0.0;
        public double grossReceipt = 0.0;
    }

}
