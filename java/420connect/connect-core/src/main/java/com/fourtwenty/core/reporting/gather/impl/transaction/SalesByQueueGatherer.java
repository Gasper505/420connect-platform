package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class SalesByQueueGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private String[] attrs = new String[]{"Queue Type", "Gross Receipts", "Transactions", "Average Transaction"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    private ReportFilter filter;

    public SalesByQueueGatherer(TransactionRepository repository) {
        this.transactionRepository = repository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.STRING};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Queue Type", reportHeaders);
        this.filter = filter;
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        HashMap<String, DailySalesByQ> days = new HashMap<>();

        for (Transaction t : results) {
            String q = t.getQueueType().name();
            DailySalesByQ dsq;
            if (days.containsKey(q)) {
                dsq = days.get(q);
            } else {
                dsq = new DailySalesByQ(q);
            }

            int factor = 1;
            if (Transaction.TransactionType.Refund.equals(t.getTransType()) && Cart.RefundOption.Retail.equals(t.getCart().getRefundOption())) {
                factor = -1;
            }

            double total = t.getCart().getTotal().doubleValue() * factor;
            if (t.getTransType() == Transaction.TransactionType.Refund
                    && t.getCart().getRefundOption() == Cart.RefundOption.Void
                    && t.getCart().getSubTotal().doubleValue() == 0) {
                total = 0;
            }

            dsq.transactions++;
            dsq.totalQueueSales += total;
            days.put(q, dsq);
        }
        ArrayList<DailySalesByQ> list = new ArrayList<>();
        for (DailySalesByQ dsq : days.values()) {
            list.add(dsq);
        }
        Collections.sort(list);
        for (DailySalesByQ dsq : list) {
            report.add(dsq.getData());
        }

        return report;
    }

    class DailySalesByQ implements Comparable<DailySalesByQ> {
        String queue;
        int transactions;
        Double totalQueueSales;

        DailySalesByQ(String queue) {
            this.queue = queue;
            transactions = 0;
            totalQueueSales = 0d;
        }

        HashMap<String, Object> getData() {
            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], queue);
            data.put(attrs[1], new DollarAmount(totalQueueSales));
            data.put(attrs[2], transactions);
            double avgSales = transactions > 0 ? totalQueueSales / transactions : 0;
            data.put(attrs[3], new DollarAmount(avgSales));
            return data;
        }

        @Override
        public int compareTo(DailySalesByQ o) {
            return queue.compareTo(o.queue);
        }
    }
}
