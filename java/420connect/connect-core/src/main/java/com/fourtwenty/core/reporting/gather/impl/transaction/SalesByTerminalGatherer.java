package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stephen on 11/17/16.
 */
public class SalesByTerminalGatherer implements Gatherer {
    private TransactionRepository transactionRepository;

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private HashMap<String, GathererReport.FieldType> fieldTypes = new HashMap<>();
    String[] attrs = new String[]{"Date", "Employee", "Trans. No.", "Subtotal", "Discounts", "Taxes", "Total"};

    public SalesByTerminalGatherer(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
        GathererReport.FieldType[] fields = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.NUMBER,
                GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY};
        Collections.addAll(reportHeaders, attrs);
        for (int i = 0; i < fields.length; i++) {
            fieldTypes.put(attrs[i], fields[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Terminal", reportHeaders);
        report.setReportFieldTypes(fieldTypes);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, List<SalesByTerminal>> salesByTerminalMap = new HashMap<>();

        return report;
    }

    private class SalesByTerminal {
        private String termId;
        private String employeeName;
        private String transNo;
        private Double subtotal;
        private Double discounts;
        private Double taxes;

    }
}
