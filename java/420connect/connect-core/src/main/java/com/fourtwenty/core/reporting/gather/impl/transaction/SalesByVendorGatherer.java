package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.util.NumberUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Schmidt on 7/10/2016.
 */
public class SalesByVendorGatherer implements Gatherer {
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private VendorRepository vendorRepository;
    private String[] attrs = new String[]{"Vendor", "Transaction Type", "Subtotal Sales", "Delivery Fees", "Discounts", "After Tax Discount", "Tax", "Gross Receipt"};
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public SalesByVendorGatherer(TransactionRepository transactionRepository, ProductRepository productRepository, VendorRepository vendorRepository) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.vendorRepository = vendorRepository;
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{GathererReport.FieldType.STRING, GathererReport.FieldType.STRING, GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY, GathererReport.FieldType.CURRENCY};
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Sales By Vendor", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);

        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());
        HashMap<String, Vendor> vendorMap = vendorRepository.listAllAsMap(filter.getCompanyId());
        Iterable<Transaction> transactions = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, VendorSale> results = new HashMap<>();

        int factor = -1;
        for (Transaction t : transactions) {

            factor = 1;
            if (t.getTransType() == Transaction.TransactionType.Refund && t.getCart() != null && t.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            HashMap<String, Double> propRatioMap = new HashMap<>();
            double total = 0.0;
            for (OrderItem orderItem : t.getCart().getItems()) {
                // skip if old refund method
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }

                Product product = productMap.get(orderItem.getProductId());
                if (product == null || !product.isDiscountable()) {
                    continue;
                }

                total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : t.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }

                Product product = productMap.get(orderItem.getProductId());
                if (product == null) {
                    continue;
                }
                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product.isDiscountable() && total > 0) {
                    double finalCost = orderItem.getFinalPrice().doubleValue();
                    double ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }

            }

            for (OrderItem item : t.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == t.getTransType() && Cart.RefundOption.Void == t.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                double ratio = propRatioMap.getOrDefault(item.getId(), 1D);

                double avgCartDiscount = t.getCart().getCalcCartDiscount() != null ? (t.getCart().getCalcCartDiscount().doubleValue() * ratio) : 0;
                double avgDeliveryFee = t.getCart().getDeliveryFee() != null ? (t.getCart().getDeliveryFee().doubleValue() * ratio) : 0;;
                double avgAfterTaxDiscount = t.getCart().getAppliedAfterTaxDiscount() != null ? (t.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio) : 0;;
                double avgCCFees = t.getCart().getCreditCardFee() != null ? (t.getCart().getCreditCardFee().doubleValue() * ratio) : 0;;

                double calcTax = 0;

                if (item.getTaxResult() != null) {
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        calcTax += item.getTaxResult().getTotalPostCalcTax().doubleValue() + item.getTaxResult().getTotalALPostExciseTax().doubleValue() + item.getTaxResult().getTotalExciseTax().doubleValue();
                    }
                }

                if (calcTax == 0) {
                    if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                        TaxInfo taxInfo = item.getTaxInfo();
                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            calcTax = item.getTaxResult().getTotalCityTax().doubleValue()
                                    + item.getTaxResult().getTotalStateTax().doubleValue()
                                    + item.getTaxResult().getTotalCountyTax().doubleValue()
                                    + item.getTaxResult().getTotalFedTax().doubleValue();
                        }
                    }
                }

                if (calcTax == 0) {
                    calcTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                }

                Product p = productMap.get(item.getProductId());
                if (p != null) {
                    Vendor v = vendorMap.get(p.getVendorId());
                    if (v != null) {
                        String vendorName = v.getName();
                        boolean isRefund = (factor == -1);
                        VendorSale vendorSale = results.get(vendorName + isRefund);
                        if (vendorSale == null) {
                            vendorSale = new VendorSale();
                            results.put((vendorName + isRefund), vendorSale);
                        }

                        double discount = avgCartDiscount;

                        discount = NumberUtils.round(discount, 6);

                        vendorSale.name = vendorName;
                        vendorSale.deliveryFees += (avgDeliveryFee * factor);
                        vendorSale.subTotals += (item.getCost().doubleValue() * factor);
                        vendorSale.discounts += (discount * factor);
                        vendorSale.afterTaxDiscount += (avgAfterTaxDiscount * factor);
                        vendorSale.tax += (calcTax * factor);

                        double grossReceipt = item.getFinalPrice().doubleValue() - avgCartDiscount + avgDeliveryFee +
                                calcTax + avgCCFees - avgAfterTaxDiscount;

                        vendorSale.totals += NumberUtils.round((grossReceipt * factor), 6);
                        vendorSale.refund = isRefund;
                    }
                }

            }
        }
        for (String vName : results.keySet()) {
            HashMap<String, Object> data = new HashMap<>();
            VendorSale vendorSale = results.get(vName);
            data.put(attrs[0], vendorSale.name);
            data.put(attrs[1], vendorSale.refund ? "Refund" : "Sale");
            data.put(attrs[2], new DollarAmount(vendorSale.subTotals));
            data.put(attrs[3], new DollarAmount(vendorSale.deliveryFees));
            data.put(attrs[4], new DollarAmount(vendorSale.discounts));
            data.put(attrs[5], new DollarAmount(vendorSale.afterTaxDiscount));
            data.put(attrs[6], new DollarAmount(vendorSale.tax));
            data.put(attrs[7], new DollarAmount(vendorSale.totals));
            report.add(data);
        }

        return report;
    }

    public static final class VendorSale {
        public String name;
        public double subTotals = 0;
        public double discounts = 0;
        public double afterTaxDiscount = 0;
        public double deliveryFees = 0;
        public double tax = 0;
        public double totals = 0;
        public boolean refund = false;
    }
}
