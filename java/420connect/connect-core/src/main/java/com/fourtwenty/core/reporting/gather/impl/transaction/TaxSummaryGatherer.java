package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fourtwenty.core.domain.models.company.CompoundTaxRate;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.*;

public class TaxSummaryGatherer implements Gatherer {
    private static final int ROUND_TAX_DECIMAL = 4;

    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private ProductRepository productRepository;
    private ProductCategoryRepository categoryRepository;

    private String[] attrs = new String[]{"Date", "Recreational Sales", "Medical Sales",
            "Total Sales", "Pre ALExcise Tax", "Pre NALExcise Tax", "Post ALExcise Tax", "Post NALExcise Tax",
            "Pre City Tax", "Pre County Tax", "Pre State Tax",
            "City Tax", "County Tax", "State Tax", "Federal Tax", "Total Tax",
            "Cannabis Tax", "Non-Cannabis Tax",
            "Credit/Debit Card Fees", "Gross Receipts"};

    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TaxSummaryGatherer() {
    }

    public TaxSummaryGatherer(TransactionRepository transactionRepository, MemberRepository memberRepository, ProductRepository productRepository, ProductCategoryRepository categoryRepository) {
        this.transactionRepository = transactionRepository;
        this.memberRepository = memberRepository;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.DATE,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY,
                GathererReport.FieldType.CURRENCY
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Tax Summary Report", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);
        Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<Long, TaxSummary> taxSummeryMap = new HashMap<>();
        List<Long> sortedKeys = new ArrayList<>();

        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (transaction.getCart() != null && transaction.getCart().getItems() != null) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));
        HashMap<String, Product> productMap = productRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(filter.getCompanyId(), filter.getShopId());

        CannabisTaxSummary cannabisTaxSummary = new CannabisTaxSummary();
        NonCannabisTaxSummary nonCannabisTaxSummary = new NonCannabisTaxSummary();

        double total = 0d;
        double totalDiscounts = 0d;
        int factor = 1;
        for (Transaction transaction : transactions) {
            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            long key = new DateTime(transaction.getProcessedTime()).minusMinutes(filter.getTimezoneOffset()).withTimeAtStartOfDay().getMillis();
            TaxSummary taxSummary = null;
            if (taxSummeryMap.get(key) == null) {
                taxSummary = new TaxSummary();
                taxSummary.date = key;
                sortedKeys.add(key);
            } else {
                taxSummary = taxSummeryMap.get(key);
            }

            Cart cart = transaction.getCart();
            if (cart != null) {

                HashMap<String, Double> discountHasMap = new HashMap<>();
                calculateOrderItemTax(cart, cart.getTaxResult(), discountHasMap, transaction);

                double creditCardFees = 0d;
                if (transaction.getCart().isEnableCreditCardFee() && transaction.getCart().getCreditCardFee().doubleValue() > 0) {
                    creditCardFees = transaction.getCart().getCreditCardFee().doubleValue();
                }

                double deliveryFees = 0d;
                if (transaction.getCart().getDeliveryFee().doubleValue() > 0) {
                    deliveryFees = transaction.getCart().getDeliveryFee().doubleValue();
                }

                double afterTaxDiscount = 0d;
                if (transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                    afterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount().doubleValue();
                }

                double subTotal = 0d;
                subTotal = cart.getSubTotal().doubleValue();
                subTotal -= cart.getTotalDiscount().doubleValue();
                subTotal = NumberUtils.round(subTotal, 2);
                //BigDecimal sale = cart.getSubTotal();
                //subTotal = cart.getSubTotalDiscount().doubleValue();
                total += cart.getSubTotal().doubleValue();
                total = NumberUtils.round(total, 2);
                totalDiscounts += cart.getTotalDiscount().doubleValue();

                boolean recreational = false;
                ConsumerType consumerType = cart.getFinalConsumerTye();

                if (consumerType.equals(ConsumerType.AdultUse)) {
                    recreational = true;
                }

                if (recreational) {
                    taxSummary.recreationalSale += subTotal * factor;
                } else {
                    taxSummary.medicinalSale += subTotal * factor;
                }


                double totalTax = 0d;
                if (transaction.getCart().getItems() != null) {
                    for (OrderItem orderItem : transaction.getCart().getItems()) {
                        Product product = productMap.get(orderItem.getProductId());

                        if (product != null && categoryMap.get(product.getCategoryId()) != null &&
                                (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail
                                        || orderItem.getStatus() != OrderItem.OrderItemStatus.Refunded)) {

                            ProductCategory category = categoryMap.get(product.getCategoryId());

                            totalTax += orderItem.getCalcTax().doubleValue();
                            double cartDiscount = discountHasMap.getOrDefault(orderItem.getId(), 0d);
                            double totalItemTax = 0d;

                            double avgCreditCardFees = creditCardFees / transaction.getCart().getItems().size();
                            double avgAfterTaxDis = afterTaxDiscount / transaction.getCart().getItems().size();
                            double avgDeliveryFees = deliveryFees / transaction.getCart().getItems().size();

                            totalItemTax = orderItem.getTaxResult().getTotalCityTax().doubleValue() + orderItem.getTaxResult().getTotalCountyTax().doubleValue() +
                                    orderItem.getTaxResult().getTotalStateTax().doubleValue() + orderItem.getTaxResult().getTotalFedTax().doubleValue()
                                    + orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue()
                                    + orderItem.getTaxResult().getTotalExciseTax().doubleValue();

                            double grossReceipt = (orderItem.getFinalPrice().doubleValue() - cartDiscount + totalItemTax + avgDeliveryFees + avgCreditCardFees - avgAfterTaxDis) * factor;

                            if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                                    || (product.getCannabisType() != Product.CannabisType.CBD
                                    && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                                    && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                                if (orderItem.getTaxResult() != null) {

                                    taxSummary.cannabisTax += NumberUtils.round(totalItemTax, 4) * factor;
                                    cannabisTaxSummary.cityTax += orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor;
                                    cannabisTaxSummary.countyTax += orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                                    cannabisTaxSummary.stateTax += orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor;
                                    cannabisTaxSummary.federalTax += orderItem.getTaxResult().getTotalFedTax().doubleValue() * factor;
//                                    cannabisTaxSummary.exciseTax += orderItem.getExciseTax().doubleValue() == 0 ? orderItem.getTaxResult().getTotalExciseTax().doubleValue() : orderItem.getExciseTax().doubleValue();
                                    // Pre AL/NAL Excise Tax
                                    cannabisTaxSummary.preALExciseTax += orderItem.getTaxResult().getOrderItemPreALExciseTax().doubleValue() * factor;
                                    cannabisTaxSummary.preNALExciseTax += orderItem.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;

                                    //Post AL/NAL Excise Tax
                                    cannabisTaxSummary.postALExciseTax += orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                                    cannabisTaxSummary.postNALExciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                                    cannabisTaxSummary.cannabisTax += NumberUtils.round(totalItemTax, 4) * factor;

                                    cannabisTaxSummary.grossReceipts += grossReceipt;

                                    // round
                                    /*taxSummary.cannabisTax = NumberUtils.round(taxSummary.cannabisTax,ROUND_TAX_DECIMAL);
                                    cannabisTaxSummary.cityTax = NumberUtils.round(cannabisTaxSummary.cityTax,ROUND_TAX_DECIMAL);
                                    cannabisTaxSummary.countyTax = NumberUtils.round(cannabisTaxSummary.countyTax,ROUND_TAX_DECIMAL);
                                    cannabisTaxSummary.stateTax = NumberUtils.round(cannabisTaxSummary.stateTax,ROUND_TAX_DECIMAL);
                                    cannabisTaxSummary.exciseTax = NumberUtils.round(cannabisTaxSummary.exciseTax,ROUND_TAX_DECIMAL);
                                    cannabisTaxSummary.cannabisTax = NumberUtils.round(cannabisTaxSummary.cannabisTax,ROUND_TAX_DECIMAL);*/

                                } else {
                                    System.out.println("no tax results");
                                }
                            } else {
                                nonCannabisTaxSummary.totalSale += (orderItem.getFinalPrice().doubleValue() - cartDiscount) * factor;
                                if (orderItem.getTaxResult() != null) {
                                    taxSummary.nonCannabisTax += NumberUtils.round(totalItemTax, 4) * factor;
                                    nonCannabisTaxSummary.cityTax += orderItem.getTaxResult().getTotalCityTax().doubleValue() * factor;
                                    nonCannabisTaxSummary.countyTax += orderItem.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                                    nonCannabisTaxSummary.stateTax += orderItem.getTaxResult().getTotalStateTax().doubleValue() * factor;
                                    nonCannabisTaxSummary.federalTax += orderItem.getTaxResult().getTotalFedTax().doubleValue();
//                                    nonCannabisTaxSummary.exciseTax += orderItem.getExciseTax().doubleValue() == 0 ? orderItem.getTaxResult().getTotalExciseTax().doubleValue() : orderItem.getExciseTax().doubleValue();
                                    // Pre AL/NAL Excise Tax
                                    nonCannabisTaxSummary.preALExciseTax += orderItem.getTaxResult().getOrderItemPreALExciseTax().doubleValue() * factor;
                                    nonCannabisTaxSummary.preNALExciseTax += orderItem.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;

                                    //Post AL/NAL Excise Tax
                                    nonCannabisTaxSummary.postALExciseTax += orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                                    nonCannabisTaxSummary.postNALExciseTax += orderItem.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                                    nonCannabisTaxSummary.nonCannabisTax += NumberUtils.round(totalItemTax, 4) * factor;

                                    nonCannabisTaxSummary.grossReceipts += grossReceipt;
                                    // round
                                    /*taxSummary.nonCannabisTax = NumberUtils.round(taxSummary.nonCannabisTax,ROUND_TAX_DECIMAL);
                                    nonCannabisTaxSummary.cityTax = NumberUtils.round(nonCannabisTaxSummary.cityTax,ROUND_TAX_DECIMAL);
                                    nonCannabisTaxSummary.countyTax = NumberUtils.round(nonCannabisTaxSummary.countyTax,ROUND_TAX_DECIMAL);
                                    nonCannabisTaxSummary.stateTax = NumberUtils.round(nonCannabisTaxSummary.stateTax,ROUND_TAX_DECIMAL);
                                    nonCannabisTaxSummary.exciseTax = NumberUtils.round(nonCannabisTaxSummary.exciseTax,ROUND_TAX_DECIMAL);
                                    nonCannabisTaxSummary.nonCannabisTax = NumberUtils.round(nonCannabisTaxSummary.nonCannabisTax,ROUND_TAX_DECIMAL);*/
                                } else {
                                    System.out.println("no tax results");
                                }
                            }
                        }
                    }
                }

                taxSummary.totalSale = (taxSummary.medicinalSale + taxSummary.recreationalSale) * factor;
                if (cart.getTaxResult() != null) {
                    taxSummary.preCityTax += cart.getTaxResult().getTotalStatePreTax().doubleValue() * factor;
                    taxSummary.preStateTax += cart.getTaxResult().getTotalCityPreTax().doubleValue() * factor;
                    taxSummary.preCountyTax += cart.getTaxResult().getTotalCountyPreTax().doubleValue() * factor;

                    taxSummary.cityTax += cart.getTaxResult().getTotalCityTax().doubleValue() * factor;
                    taxSummary.stateTax += cart.getTaxResult().getTotalStateTax().doubleValue() * factor;
                    taxSummary.countyTax += cart.getTaxResult().getTotalCountyTax().doubleValue() * factor;
                    taxSummary.federalTax += cart.getTaxResult().getTotalFedTax().doubleValue() * factor;
//                    taxSummary.exciseTax += cart.getTaxResult().getTotalExciseTax().doubleValue();

                    // Pre AL/NAL Excise Tax
                    taxSummary.preALExciseTax += cart.getTaxResult().getTotalALExciseTax().doubleValue() * factor;
                    taxSummary.preNALExciseTax += cart.getTaxResult().getTotalNALPreExciseTax().doubleValue() * factor;

                    //Post AL/NAL Excise Tax
                    taxSummary.postALExciseTax += cart.getTaxResult().getTotalALPostExciseTax().doubleValue() * factor;
                    taxSummary.postNALExciseTax += cart.getTaxResult().getTotalExciseTax().doubleValue() * factor;

                }
                //double totalTax = transaction.getCart().getTotalCalcTax().doubleValue() - transaction.getCart().getTotalPreCalcTax().doubleValue();
                if (totalTax < 0) {
                    totalTax = 0;
                }

                taxSummary.totalTax += totalTax * factor;
                taxSummary.grossReceipts += transaction.getCart().getTotal().doubleValue() * factor;

                taxSummary.creditCardFees += creditCardFees * factor;
            }
            taxSummeryMap.put(key, taxSummary);
        }
        for (Long key : sortedKeys) {
            HashMap<String, Object> data = new HashMap<>();
            TaxSummary taxSummary = taxSummeryMap.get(key);
            data.put(attrs[0], ProcessorUtil.dateString(taxSummary.date));
            data.put(attrs[1], new DollarAmount(taxSummary.recreationalSale));
            data.put(attrs[2], new DollarAmount(taxSummary.medicinalSale));
            data.put(attrs[3], new DollarAmount(taxSummary.totalSale));
//            data.put(attrs[4], new DollarAmount(taxSummary.exciseTax));
            data.put(attrs[4], new DollarAmount(taxSummary.preALExciseTax));
            data.put(attrs[5], new DollarAmount(taxSummary.preNALExciseTax));
            data.put(attrs[6], new DollarAmount(taxSummary.postALExciseTax));
            data.put(attrs[7], new DollarAmount(taxSummary.postNALExciseTax));
            data.put(attrs[8], new DollarAmount(taxSummary.preCityTax));
            data.put(attrs[9], new DollarAmount(taxSummary.preCountyTax));
            data.put(attrs[10], new DollarAmount(taxSummary.preStateTax));
            data.put(attrs[11], new DollarAmount(taxSummary.cityTax));
            data.put(attrs[12], new DollarAmount(taxSummary.countyTax));
            data.put(attrs[13], new DollarAmount(taxSummary.stateTax));
            data.put(attrs[14], new DollarAmount(taxSummary.federalTax));
            data.put(attrs[15], new DollarAmount(taxSummary.totalTax));
            data.put(attrs[16], new DollarAmount(taxSummary.cannabisTax));
            data.put(attrs[17], new DollarAmount(taxSummary.nonCannabisTax));
            data.put(attrs[18], new DollarAmount(taxSummary.creditCardFees));
            data.put(attrs[19], new DollarAmount(taxSummary.grossReceipts));
            report.add(data);
        }

        //Prepare summary table data
        Map<String, Map<String, Object>> summaryData = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        Map<String, Object> cannabisSummaryMap = mapper.convertValue(cannabisTaxSummary, new TypeReference<Map<String, Object>>() {
        });
        summaryData.put("Cannabis Summary", cannabisSummaryMap);

        Map<String, Object> nonCannabisSummaryMap = mapper.convertValue(nonCannabisTaxSummary, new TypeReference<Map<String, Object>>() {
        });
        summaryData.put("Non Cannabis Summary", nonCannabisSummaryMap);


        report.setSummaryData(summaryData);
        return report;
    }

    private class TaxSummary {
        long date;
        double recreationalSale;
        double medicinalSale;
        double totalSale;

        double preCityTax = 0;
        double preCountyTax = 0;
        double preStateTax = 0;

        double cityTax = 0;
        double countyTax = 0;
        double stateTax = 0;
        //        double exciseTax=0;
        double preALExciseTax = 0;
        double preNALExciseTax = 0;
        double postALExciseTax = 0;
        double postNALExciseTax = 0;
        double federalTax = 0;
        double cannabisTax = 0d;
        double nonCannabisTax = 0d;
        double creditCardFees = 0d;
        double grossReceipts = 0;
        double totalTax = 0;

        public TaxSummary() {

        }
    }

    private class CannabisTaxSummary {
        @JsonProperty("Cannabis Tax")
        double cannabisTax = 0d;
        @JsonProperty("Total Sales")
        double totalSale = 0d;
        @JsonProperty("City Tax")
        double cityTax = 0d;
        @JsonProperty("County Tax")
        double countyTax = 0d;
        @JsonProperty("State Tax")
        double stateTax = 0d;
        @JsonProperty("Federal Tax")
        double federalTax = 0d;
        //        @JsonProperty("Excise Tax")
//        double exciseTax = 0d;
        @JsonProperty("Pre ALExcise Tax")
        double preALExciseTax = 0d;
        @JsonProperty("Pre NALExcise Tax")
        double preNALExciseTax = 0d;
        @JsonProperty("Post ALExcise Tax")
        double postALExciseTax = 0d;
        @JsonProperty("Post NALExcise Tax")
        double postNALExciseTax = 0d;
        @JsonProperty("Gross Receipts")
        double grossReceipts = 0d;

        public CannabisTaxSummary() {
        }

        public double getCannabisTax() {
            return cannabisTax;
        }

        public void setCannabisTax(double cannabisTax) {
            this.cannabisTax = cannabisTax;
        }

        public double getTotalSale() {
            return totalSale;
        }

        public void setTotalSale(double totalSale) {
            this.totalSale = totalSale;
        }

        public double getCityTax() {
            return cityTax;
        }

        public void setCityTax(double cityTax) {
            this.cityTax = cityTax;
        }

        public double getCountyTax() {
            return countyTax;
        }

        public void setCountyTax(double countyTax) {
            this.countyTax = countyTax;
        }

        public double getStateTax() {
            return stateTax;
        }

        public void setStateTax(double stateTax) {
            this.stateTax = stateTax;
        }

//        public double getExciseTax() {
//            return exciseTax;
//        }
//
//        public void setExciseTax(double exciseTax) {
//            this.exciseTax = exciseTax;
//        }

        public double getGrossReceipts() {
            return grossReceipts;
        }

        public void setGrossReceipts(double grossReceipts) {
            this.grossReceipts = grossReceipts;
        }

        public double getPreALExciseTax() {
            return preALExciseTax;
        }

        public void setPreALExciseTax(double preALExciseTax) {
            this.preALExciseTax = preALExciseTax;
        }

        public double getPreNALExciseTax() {
            return preNALExciseTax;
        }

        public void setPreNALExciseTax(double preNALExciseTax) {
            this.preNALExciseTax = preNALExciseTax;
        }

        public double getPostALExciseTax() {
            return postALExciseTax;
        }

        public void setPostALExciseTax(double postALExciseTax) {
            this.postALExciseTax = postALExciseTax;
        }

        public double getPostNALExciseTax() {
            return postNALExciseTax;
        }

        public void setPostNALExciseTax(double postNALExciseTax) {
            this.postNALExciseTax = postNALExciseTax;
        }

        public double getFederalTax() {
            return federalTax;
        }

        public void setFederalTax(double federalTax) {
            this.federalTax = federalTax;
        }
    }

    private class NonCannabisTaxSummary {
        @JsonProperty("Non-Cannabis Tax")
        double nonCannabisTax = 0d;
        @JsonProperty("Total Sales")
        double totalSale = 0d;
        @JsonProperty("City Tax")
        double cityTax = 0d;
        @JsonProperty("County Tax")
        double countyTax = 0d;
        @JsonProperty("State Tax")
        double stateTax = 0d;
        @JsonProperty("Federal Tax")
        double federalTax = 0d;
        //        @JsonProperty("Excise Tax")
//        double exciseTax = 0d;
        @JsonProperty("Pre ALExcise Tax")
        double preALExciseTax = 0d;
        @JsonProperty("Pre NALExcise Tax")
        double preNALExciseTax = 0d;
        @JsonProperty("Post ALExcise Tax")
        double postALExciseTax = 0d;
        @JsonProperty("Post NALExcise Tax")
        double postNALExciseTax = 0d;
        @JsonProperty("Gross Receipts")
        double grossReceipts = 0d;

        public NonCannabisTaxSummary() {
        }

        public double getNonCannabisTax() {
            return nonCannabisTax;
        }

        public void setNonCannabisTax(double nonCannabisTax) {
            this.nonCannabisTax = nonCannabisTax;
        }

        public double getTotalSale() {
            return totalSale;
        }

        public void setTotalSale(double totalSale) {
            this.totalSale = totalSale;
        }

        public double getCityTax() {
            return cityTax;
        }

        public void setCityTax(double cityTax) {
            this.cityTax = cityTax;
        }

        public double getCountyTax() {
            return countyTax;
        }

        public void setCountyTax(double countyTax) {
            this.countyTax = countyTax;
        }

        public double getStateTax() {
            return stateTax;
        }

        public void setStateTax(double stateTax) {
            this.stateTax = stateTax;
        }

//        public double getExciseTax() {
//            return exciseTax;
//        }
//
//        public void setExciseTax(double exciseTax) {
//            this.exciseTax = exciseTax;
//        }

        public double getGrossReceipts() {
            return grossReceipts;
        }

        public void setGrossReceipts(double grossReceipts) {
            this.grossReceipts = grossReceipts;
        }

        public double getPreALExciseTax() {
            return preALExciseTax;
        }

        public void setPreALExciseTax(double preALExciseTax) {
            this.preALExciseTax = preALExciseTax;
        }

        public double getPreNALExciseTax() {
            return preNALExciseTax;
        }

        public void setPreNALExciseTax(double preNALExciseTax) {
            this.preNALExciseTax = preNALExciseTax;
        }

        public double getPostALExciseTax() {
            return postALExciseTax;
        }

        public void setPostALExciseTax(double postALExciseTax) {
            this.postALExciseTax = postALExciseTax;
        }

        public double getPostNALExciseTax() {
            return postNALExciseTax;
        }

        public void setPostNALExciseTax(double postNALExciseTax) {
            this.postNALExciseTax = postNALExciseTax;
        }

        public double getFederalTax() {
            return federalTax;
        }

        public void setFederalTax(double federalTax) {
            this.federalTax = federalTax;
        }
    }

    public void calculateOrderItemTax(final Cart cart, final TaxResult txTaxResult, HashMap<String, Double> discountPropMap, Transaction transaction) {

        int totalFinalCost = 0;
        for (OrderItem item : cart.getItems()) {
            totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
        }

        for (OrderItem orderItem : cart.getItems()) {
            discountPropMap.put(orderItem.getId(), 0d);

            int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
            double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;

            double targetCartDiscount = cart.getCalcCartDiscount().doubleValue() * ratio;

            discountPropMap.put(orderItem.getId(), targetCartDiscount);


            if (orderItem.getTaxResult() == null) {
                double totalCityTax = 0d;
                double totalCountyTax = 0d;
                double totalStateTax = 0d;
                double totalFedTax = 0d;
                double totalExciseTax = 0d;

                double totalPostTax = 0;
                double totalPreTax = 0;

                double cityTax = 0d;
                double countyTax = 0d;
                double stateTax = 0d;
                double fedTax = 0d;
                double exciseTax = 0d;
                double preALExciseTax = 0d;
                double preNALExciseTax = 0d;
                double postALExciseTax = 0d;
                double postNALExciseTax = 0d;

                TaxResult orderItemTaxResult = new TaxResult();

                if (orderItem.getTaxTable() != null || cart.getTaxTable() != null) {
                    CompoundTaxTable taxTable = orderItem.getTaxTable() != null ? orderItem.getTaxTable() : cart.getTaxTable();

                    if (taxTable.getTaxType() == TaxInfo.TaxType.Exempt) {
                        continue;
                    }
                    // set calc tax to 0, initials
                    orderItem.setCalcTax(new BigDecimal(0));
                    orderItem.setCalcPreTax(new BigDecimal(0));


                    double sales = NumberUtils.round(orderItem.getFinalPrice().doubleValue() - targetCartDiscount, 5);

                    double lineTotalPostTax = 0;
                    double lineTotalPreTax = 0;
                    if (taxTable.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {

                        double subTotalWithTax = sales;
                        double costWithDiscount = subTotalWithTax;

                        double subExciseTax = orderItem.getExciseTax() == null && orderItem.getExciseTax().doubleValue() == 0 ? orderItem.getTaxResult().getTotalExciseTax().doubleValue() : orderItem.getExciseTax().doubleValue();
                        double curTax = 0d;
                        double taxOnExciseTax = 0d;
                        for (CompoundTaxRate curTaxRate : taxTable.getActivePostTaxes()) {
                            if (curTaxRate.isActive()) {
                                double productTax = 0d;
                                double productExciseTax = 0d;
                                if (curTaxRate.isCompound()) {
                                    subTotalWithTax += NumberUtils.round(curTax, ROUND_TAX_DECIMAL);

                                    // reset curTax
                                    curTax = 0d;
                                    productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                    curTax += productTax;

                                    if (curTaxRate.isActiveExciseTax()) {
                                        subExciseTax += NumberUtils.round(taxOnExciseTax, ROUND_TAX_DECIMAL);

                                        taxOnExciseTax = 0d;
                                        productExciseTax = subExciseTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                        taxOnExciseTax += productExciseTax;
                                    }

                                } else {
                                    productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                    curTax += productTax;

                                    if (curTaxRate.isActiveExciseTax()) {
                                        productExciseTax = subExciseTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                        taxOnExciseTax += productExciseTax;
                                    }
                                }

                                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.City) {
                                    cityTax += productTax + productExciseTax;
                                    orderItemTaxResult.setTotalCityTax(new BigDecimal(productTax + productExciseTax));
                                }

                                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.County) {
                                    countyTax += productTax + productExciseTax;
                                    orderItemTaxResult.setTotalCountyTax(new BigDecimal(productTax + productExciseTax));
                                }

                                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.State) {
                                    stateTax += productTax + productExciseTax;
                                    orderItemTaxResult.setTotalStateTax(new BigDecimal(productTax + productExciseTax));
                                }

                                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.Federal) {
                                    fedTax += productTax + productExciseTax;
                                    orderItemTaxResult.setTotalFedTax(new BigDecimal(productTax + productExciseTax));
                                }
                            }
                        }

                        subTotalWithTax += NumberUtils.round(curTax, ROUND_TAX_DECIMAL);
                        lineTotalPostTax = NumberUtils.round(subTotalWithTax, ROUND_TAX_DECIMAL) - costWithDiscount;
                        lineTotalPostTax = NumberUtils.round(lineTotalPostTax, ROUND_TAX_DECIMAL);

                        orderItem.setCalcTax(new BigDecimal(lineTotalPostTax));
                        orderItem.setCalcPreTax(new BigDecimal(0));


                        totalPostTax += lineTotalPostTax;
                    } else {
                        // calculate preTax
                        List<CompoundTaxTable.ActiveTax> activeTaxes = taxTable.getActivePreTaxes();

                        List<CompoundTaxTable.ActiveTax> reverseList = Lists.reverse(activeTaxes);

                        double discountToApply = orderItem.getCalcDiscount().doubleValue();
                        double subTotalWithTax = orderItem.getFinalPrice().doubleValue() - discountToApply;
                        double totalNonCompTaxes = 0;
                        for (CompoundTaxTable.ActiveTax activeTax : reverseList) {
                            if (!activeTax.compound) {
                                totalNonCompTaxes += activeTax.taxRate.doubleValue();
                            }
                        }

                        for (CompoundTaxTable.ActiveTax activeTax : reverseList) {
                            double preCost;
                            double preTax;

                            if (activeTax.compound) {
                                preCost = (subTotalWithTax - lineTotalPreTax) / (1 + activeTax.taxRate.doubleValue() / 100);
                                if (preCost < 0) {
                                    preCost = 0;
                                }
                                preTax = (subTotalWithTax - lineTotalPreTax) - preCost;
                            } else {
                                preCost = subTotalWithTax / (1 + totalNonCompTaxes / 100);
                                if (preCost < 0) {
                                    preCost = 0;
                                }
                                preTax = preCost * activeTax.taxRate.doubleValue() / 100;
                            }


                            lineTotalPreTax += preTax;

                            switch(activeTax.territory) {
                                case City:
                                    orderItemTaxResult.setTotalCityPreTax(new BigDecimal(preTax));
                                    break;
                                case County:
                                    orderItemTaxResult.setTotalCountyPreTax(new BigDecimal(preTax));
                                    break;
                                case State:
                                    orderItemTaxResult.setTotalStatePreTax(new BigDecimal(preTax));
                                    break;
                                case Federal:
                                    orderItemTaxResult.setTotalFedPreTax(new BigDecimal(preTax));
                                    break;
                            }

                            orderItem.setCalcTax(new BigDecimal(0));
                            orderItem.setCalcPreTax(new BigDecimal(lineTotalPreTax));
                        }
                    }
                } else {
                    TaxInfo taxInfo = orderItem.getTaxInfo();

                    double totalRegTax = 0d;
                    if (TaxInfo.TaxProcessingOrder.PostTaxed == orderItem.getTaxOrder()) {
                        totalRegTax = txTaxResult.getTotalPostCalcTax().doubleValue();
                    } else {
                        totalRegTax = txTaxResult.getTotalPreCalcTax().doubleValue();
                    }

                    if (taxInfo.getTotalTax().doubleValue() != 0) {
                        cityTax = this.calculateRegularTax(totalRegTax, taxInfo.getTotalTax().doubleValue(), taxInfo.getCityTax().doubleValue());
                        stateTax = this.calculateRegularTax(totalRegTax, taxInfo.getTotalTax().doubleValue(), taxInfo.getStateTax().doubleValue());
                        fedTax = this.calculateRegularTax(totalRegTax, taxInfo.getTotalTax().doubleValue(), taxInfo.getFederalTax().doubleValue());

                        totalCityTax += cityTax;
                        totalCountyTax += stateTax;
                        totalFedTax += fedTax;

                        txTaxResult.setTotalCityTax(new BigDecimal(NumberUtils.round(totalCityTax, ROUND_TAX_DECIMAL)));
                        txTaxResult.setTotalCountyTax(new BigDecimal(NumberUtils.round(totalCountyTax, ROUND_TAX_DECIMAL)));
                        txTaxResult.setTotalFedTax(new BigDecimal(NumberUtils.round(totalFedTax, ROUND_TAX_DECIMAL)));
                    }
                }
                exciseTax = orderItem.getExciseTax().doubleValue() == 0 ? (orderItem.getTaxResult() == null ? 0 : orderItem.getTaxResult().getTotalExciseTax().doubleValue()) : orderItem.getExciseTax().doubleValue();
                // Pre AL/NAL Excise Tax
                preALExciseTax = orderItem.getTaxResult() == null ? 0 : orderItem.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                preNALExciseTax = orderItem.getTaxResult() == null ? 0 : orderItem.getTaxResult().getTotalNALPreExciseTax().doubleValue();

                //Post AL/NAL Excise Tax
                postALExciseTax = orderItem.getTaxResult() == null ? 0 : orderItem.getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = orderItem.getTaxResult() == null ? 0 : orderItem.getTaxResult().getTotalExciseTax().doubleValue();


                orderItemTaxResult.setTotalPreCalcTax(new BigDecimal(NumberUtils.round(totalPreTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalPostCalcTax(new BigDecimal(NumberUtils.round(totalPostTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalCityTax(new BigDecimal(NumberUtils.round(cityTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalCountyTax(new BigDecimal(NumberUtils.round(countyTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalStateTax(new BigDecimal(NumberUtils.round(stateTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalFedTax(new BigDecimal(NumberUtils.round(fedTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalExciseTax(new BigDecimal(NumberUtils.round(exciseTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalALExciseTax(new BigDecimal(NumberUtils.round(preALExciseTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalNALPreExciseTax(new BigDecimal(NumberUtils.round(preNALExciseTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalALPostExciseTax(new BigDecimal(NumberUtils.round(postALExciseTax, ROUND_TAX_DECIMAL)));
                orderItemTaxResult.setTotalExciseTax(new BigDecimal(NumberUtils.round(postNALExciseTax, ROUND_TAX_DECIMAL)));


                orderItem.setTaxResult(orderItemTaxResult);
            }
        }
    }

    private double calculateRegularTax(double totalRegTax, double totalTax, double tax) {
        return (totalRegTax * tax) / totalTax;
    }
}


