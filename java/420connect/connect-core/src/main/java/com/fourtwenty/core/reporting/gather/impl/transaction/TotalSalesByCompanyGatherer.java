package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.SplitPayment;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TotalSalesByCompanyGatherer implements Gatherer {
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;
    @Inject
    private MetrcService metrcService;
    @Inject
    private ShopRepository shopRepository;

    private String[] attrs = new String[]{
            "Shop", //0
            "Date", //1
            "Member", //2
            "Transaction No.", //3
            "Trans Type", //4
            "Trans Status", //5
            "Consumer Tax Type", //6
            "COGs", //7
            "Retail Value", //8
            "Discounts", //9
            "Pre AL Excise Tax", //10
            "Pre NAL Excise Tax", //11
            "Pre City Tax", //12
            "Pre County Tax", //13
            "Pre State Tax", //14
            "Post AL Excise Tax", //15
            "Post NAL Excise Tax", //16
            "City Tax", //17
            "County Tax", //18
            "State Tax", //19
            "Total Tax", //20
            "Delivery Fees", //21
            "Credit/Debit Card Fees", //22
            "After Tax Discount", //23
            "Gross Receipt", //24
            "Employee", //25
            "Terminal", //26
            "Payment Type", //27
            "Promotion(s)", //28
            "Marketing Source", //29
            "Tags", //30
            "Gender", //31
            "Age", //32
            "Created By", //33
            "Created Date", //34
            "Prepared By", //35
            "Prepared Date", //36
            "Packed  By", //37
            "Packed Date" //38
    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TotalSalesByCompanyGatherer() {
        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Shop 0
                GathererReport.FieldType.STRING, // Date 1
                GathererReport.FieldType.STRING, // Member 2
                GathererReport.FieldType.STRING, // transNo 3
                GathererReport.FieldType.STRING, // type 4
                GathererReport.FieldType.STRING, // status 5
                GathererReport.FieldType.STRING, // Consumer Tax Type 6
                GathererReport.FieldType.CURRENCY, // cogs 7
                GathererReport.FieldType.CURRENCY, // subtotal 8
                GathererReport.FieldType.CURRENCY, // discounts 9
                GathererReport.FieldType.CURRENCY, // preALExciseTax 10
                GathererReport.FieldType.CURRENCY, // preNALExciseTax 11
                GathererReport.FieldType.CURRENCY, // precityTax 12
                GathererReport.FieldType.CURRENCY, // precountyTax 13
                GathererReport.FieldType.CURRENCY, // prestateTax 14
                GathererReport.FieldType.CURRENCY, // postALExciseTax 15
                GathererReport.FieldType.CURRENCY, // postNALExciseTax 16
                GathererReport.FieldType.CURRENCY, // cityTax 17
                GathererReport.FieldType.CURRENCY, // countyTax 18
                GathererReport.FieldType.CURRENCY, // stateTax 19
                GathererReport.FieldType.CURRENCY, // post taxes 20
                GathererReport.FieldType.CURRENCY, // delivery fees 21
                GathererReport.FieldType.CURRENCY, // credit card fees 22
                GathererReport.FieldType.CURRENCY, // afterTaxDiscount 23
                GathererReport.FieldType.NUMBER, // total sale 24
                GathererReport.FieldType.STRING, // employee 25
                GathererReport.FieldType.STRING, // terminal 26
                GathererReport.FieldType.STRING, // payment type 27
                GathererReport.FieldType.STRING, // promotions 28
                GathererReport.FieldType.STRING, //marketing src 29
                GathererReport.FieldType.STRING, //tags 30
                GathererReport.FieldType.STRING, // CreatedBy 31
                GathererReport.FieldType.STRING, // CreatedDate 32
                GathererReport.FieldType.STRING, // CreatedBy 33
                GathererReport.FieldType.STRING, // CreatedDate 34
                GathererReport.FieldType.STRING, // prepared By 35
                GathererReport.FieldType.STRING,// prepared Date 36
                GathererReport.FieldType.STRING, // packed By 37
                GathererReport.FieldType.STRING}; // packed Date 38
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {

        Iterable<Transaction> results = transactionRepository.getBracketSalesByCompany(filter.getCompanyId(),
                filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());
        HashMap<String, Shop> shopMap = shopRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, Promotion> promotionMap = promotionRepository.listAllAsMap(filter.getCompanyId());
        HashMap<String, LoyaltyReward> rewardMap = rewardRepository.listAllAsMap(filter.getCompanyId());
        Iterable<Product> products = productRepository.list(filter.getCompanyId());
        HashMap<String, ProductBatch> allBatchMap = batchRepository.listAsMap(filter.getCompanyId());
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(filter.getCompanyId());
        HashMap<String, PrepackageProductItem> productItemHashMap = prepackageProductItemRepository.listAsMap(filter.getCompanyId());
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        for (ProductBatch batch : allBatchMap.values()) {
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch == null) {
                recentBatchMap.put(batch.getProductId(), batch);
            } else if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        HashMap<String, Product> productMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();
        for (Product p : products) {
            productMap.put(p.getId(), p);
            List<Product> items = productsByCompanyLinkId.get(p.getCompanyLinkId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(p);
            productsByCompanyLinkId.put(p.getCompanyLinkId(), items);
        }


        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
            transactions.add(transaction);
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(objectIds));

        boolean enableMetrc = false;
        String stateCode = metrcService.getShopStateCode(filter.getCompanyId(), filter.getShopId(), false);
        if (stateCode != null) {
            MetrcAccount metrcAccount = metrcAccountRepository.getMetrcAccount(filter.getCompanyId(), stateCode);
            if (metrcAccount != null) {
                for (MetrcFacilityAccount metrcFacilityAccount : metrcAccount.getFacilities()) {
                    if (metrcFacilityAccount.getShopId() != null && metrcFacilityAccount.getShopId().equalsIgnoreCase(filter.getShopId())) {
                        if (metrcFacilityAccount.isEnabled()) {
                            enableMetrc = true;
                            fieldTypes.put("MetrcId", GathererReport.FieldType.STRING);
                            reportHeaders.add("MetrcId");
                        }
                    }
                }
            }
        }

        Set<String> paymentOptions = new HashSet<>();
        for (Transaction ts : transactions) {
            paymentOptions.add(ts.getCart().getPaymentOption().toString());
            if (ts.getCart().getPaymentOption() == Cart.PaymentOption.Split && ts.getCart().getSplitPayment() != null) {
                paymentOptions.add(Cart.PaymentOption.Cash.toString());
                paymentOptions.add(Cart.PaymentOption.Credit.toString());
                paymentOptions.add(Cart.PaymentOption.Check.toString());
                paymentOptions.add(Cart.PaymentOption.StoreCredit.toString());
                paymentOptions.add(Cart.PaymentOption.CashlessATM.toString());
            }
        }

        reportHeaders.addAll(paymentOptions);

        for (String paymentOption : paymentOptions) {
            fieldTypes.put(paymentOption, GathererReport.FieldType.CURRENCY);
        }

        GathererReport report = null;
        StringBuilder builder = null;
        report = new GathererReport(filter, "Total Sales By Company", reportHeaders);
        report.setReportPostfix(GathererReport.DATE_BRACKET);
        report.setReportFieldTypes(fieldTypes);


        String nAvail = "N/A";
        long now = DateTime.now().getMillis();
        int factor = 1;
        for (Transaction ts : transactions) {
            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart() != null && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Member member = memberHashMap.get(ts.getMemberId());

            if (member == null) {
                continue;
            }

            String consumerTaxType = ts.getCart().getFinalConsumerTye().getDisplayName();
            /*
            if (ts.getCart().getTaxTable() != null) {
                consumerTaxType = ts.getCart().getTaxTable().getName();
            }*/

            double units = 0.0;
            double totalPreTax = 0;
            double totalPostTax = 0.0;

            totalPostTax = ts.getCart().getTotalCalcTax().doubleValue() - ts.getCart().getTotalPreCalcTax().doubleValue();
            if (totalPostTax < 0) {
                totalPostTax = 0;
            }
            double cityTax = 0;
            double countyTax = 0;
            double stateTax = 0;

            double preCityTax = 0;
            double preCountyTax = 0;
            double preStateTax = 0;

            double federalTax = 0;
            double exciseTax = 0;
            double preALExciseTax = 0d;
            double preNALExciseTax = 0d;
            double postALExciseTax = 0d;
            double postNALExciseTax = 0d;
            double afterTaxDiscount = 0d;
            if (ts.getCart().getTaxResult() != null) {
                exciseTax = ts.getCart().getTaxResult().getTotalExciseTax().doubleValue();
                postALExciseTax = ts.getCart().getTaxResult().getTotalALPostExciseTax().doubleValue();
                postNALExciseTax = ts.getCart().getTaxResult().getTotalExciseTax().doubleValue();
                preALExciseTax = ts.getCart().getTaxResult().getTotalALExciseTax().doubleValue();
                preNALExciseTax = ts.getCart().getTaxResult().getTotalNALPreExciseTax().doubleValue();
                cityTax = ts.getCart().getTaxResult().getTotalCityTax().doubleValue();
                countyTax = ts.getCart().getTaxResult().getTotalCountyTax().doubleValue();
                stateTax = ts.getCart().getTaxResult().getTotalStateTax().doubleValue();
                //totalPostTax = ts.getCart().getTaxResult().getTotalPostCalcTax().doubleValue();

                preCityTax = ts.getCart().getTaxResult().getTotalCityPreTax().doubleValue();
                preCountyTax = ts.getCart().getTaxResult().getTotalCountyPreTax().doubleValue();
                preStateTax = ts.getCart().getTaxResult().getTotalStatePreTax().doubleValue();
            }

            if (ts.getCart().getAppliedAfterTaxDiscount() != null && ts.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
                afterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount().doubleValue();
            }

            double creditCardFees = 0;
            if (ts.getCart().getCreditCardFee().doubleValue() > 0) {
                creditCardFees = ts.getCart().getCreditCardFee().doubleValue();
            }

            double deliveryFees = 0;
            if (ts.getCart().getDeliveryFee().doubleValue() > 0) {
                deliveryFees = ts.getCart().getDeliveryFee().doubleValue();
            }


            //Getting promotions applied for each transaction
            LinkedHashSet<PromotionReq> promos = ts.getCart().getPromotionReqs();
            StringBuilder sb = new StringBuilder();
            Iterator<PromotionReq> it = promos.iterator();

            while (it.hasNext()) {
                PromotionReq promotionReq = it.next();
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                    if (promotion != null) {
                        sb.append(promotion.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                } else {
                    LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                    if (reward != null) {
                        sb.append(reward.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                }
            }

            double transCogs = 0;
            for (OrderItem item : ts.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

//                if (!(ts.getTransType() == Transaction.TransactionType.Sale
//                        || (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart().getRefundOption() == Cart.RefundOption.Retail
//                        && item.getStatus() == OrderItem.OrderItemStatus.Refunded))) {
//                    continue;
//                }

                Product product = productMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                boolean calculated = false;
                double itemCogs = 0;
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        itemCogs += calcCOGS(unitsSold, targetBatch);

                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                itemCogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch);
                            }
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    itemCogs = unitCost * item.getQuantity().doubleValue();
                }
                transCogs += itemCogs;
            }


            Employee emp = employeeMap.get(ts.getSellerId());
            String employeeName = "";
            if (emp != null) {
                employeeName = emp.getFirstName() + " " + emp.getLastName();
            }
            Terminal t = terminalMap.get(ts.getSellerTerminalId());
            String terminalName = "";
            if (t != null) {
                terminalName = t.getName();
            }

            double total = ts.getCart().getTotal().doubleValue() * factor;
            if (ts.getTransType() == Transaction.TransactionType.Refund
                    && ts.getCart().getRefundOption() == Cart.RefundOption.Void
                    && ts.getCart().getSubTotal().doubleValue() == 0) {
                creditCardFees = 0;
                deliveryFees = 0;
                afterTaxDiscount = 0;
                total = 0;
            }

            String memberName = member.getFirstName() + " " + member.getLastName();
            String createdBy = "";
            String prepareBy = "";
            String packedBy = "";
            String preparedDate = "";
            String packedDate = "";

            if (StringUtils.isNotBlank(ts.getCreatedById())) {
                Employee createdEmployee = employeeMap.get(ts.getCreatedById());
                createdBy = createdEmployee.getFirstName() + " "+ createdEmployee.getLastName();
            }

            if (ts.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment) {
                if (StringUtils.isNotBlank(ts.getPreparedBy())) {
                    Employee prepareEmployee = employeeMap.get(ts.getPreparedBy());
                    if (prepareEmployee != null) {
                        prepareBy = prepareEmployee.getFirstName() + " " + prepareEmployee.getLastName();
                    }
                }
                if (StringUtils.isNotBlank(ts.getPackedBy())) {
                    Employee packedEmployee = employeeMap.get(ts.getPackedBy());
                    packedBy = packedEmployee.getFirstName() + " " + packedEmployee.getLastName();
                }
                preparedDate = (ts.getPreparedDate() != null) ? ProcessorUtil.timeStampWithOffsetLong(ts.getPreparedDate(), filter.getTimezoneOffset()) : "";
                packedDate = (ts.getPackedDate() != null) ? ProcessorUtil.timeStampWithOffsetLong(ts.getPackedDate(), filter.getTimezoneOffset()) : "";

            }

            HashMap<String, Object> data = new HashMap<>();
            data.put(attrs[0], shopMap.get(ts.getShopId()).getName());
            data.put(attrs[1], ProcessorUtil.timeStampWithOffsetLong(ts.getProcessedTime(), filter.getTimezoneOffset()));
            data.put(attrs[2], memberName);
            data.put(attrs[3], ts.getTransNo());
            data.put(attrs[4], ts.getTransType());
            data.put(attrs[5], ts.getStatus());
            data.put(attrs[6], consumerTaxType);
            data.put(attrs[7], new DollarAmount(transCogs * factor)); //cogs
            data.put(attrs[8], new DollarAmount(ts.getCart().getSubTotal().doubleValue() * factor));
            data.put(attrs[9], new DollarAmount(ts.getCart().getTotalDiscount().doubleValue() * factor));

            data.put(attrs[10], new DollarAmount(preALExciseTax * factor));
            data.put(attrs[11], new DollarAmount(preNALExciseTax * factor));

            data.put(attrs[12], new DollarAmount(preCityTax * factor));
            data.put(attrs[13], new DollarAmount(preCountyTax * factor));
            data.put(attrs[14], new DollarAmount(preStateTax * factor));

            data.put(attrs[15], new DollarAmount(postALExciseTax * factor));
            data.put(attrs[16], new DollarAmount(postNALExciseTax * factor));
            data.put(attrs[17], new DollarAmount(cityTax * factor));
            data.put(attrs[18], new DollarAmount(countyTax * factor));
            data.put(attrs[19], new DollarAmount(stateTax * factor));
            data.put(attrs[20], new DollarAmount(totalPostTax * factor));
            data.put(attrs[21], new DollarAmount(deliveryFees * factor));
            data.put(attrs[22], new DollarAmount(creditCardFees * factor));
            data.put(attrs[23], new DollarAmount(afterTaxDiscount * factor));
            data.put(attrs[24], new DollarAmount(total));
            data.put(attrs[25], employeeName);
            data.put(attrs[26], terminalName);
            data.put(attrs[27], ts.getCart().getPaymentOption());
            data.put(attrs[28], sb.toString());
            data.put(attrs[29], TextUtil.textOrEmpty(member.getMarketingSource()));
            StringBuilder tagBuilder = new StringBuilder();
            if (ts.getOrderTags() != null) {
                ts.getOrderTags().forEach((tag) -> tagBuilder.append(tag + ";"));
            }
            data.put(attrs[30],tagBuilder.toString());
            data.put(attrs[31], member.getSex().name());
            data.put(attrs[32], member.getDob() != null ? DateUtil.getYearsBetweenTwoDates(member.getDob(), now) : "");
            data.put(attrs[33], StringUtils.isNotBlank(createdBy) ? createdBy : "N/A");
            data.put(attrs[34], ProcessorUtil.timeStampWithOffsetLong(ts.getCreated(), filter.getTimezoneOffset()));
            data.put(attrs[35], StringUtils.isNotBlank(prepareBy) ? prepareBy : "N/A");
            data.put(attrs[36], StringUtils.isNotBlank(preparedDate) ? preparedDate : "N/A");
            data.put(attrs[37], StringUtils.isNotBlank(packedBy) ? packedBy : "N/A");
            data.put(attrs[38], StringUtils.isNotBlank(packedDate) ? packedDate : "N/A");


            if (enableMetrc) {
                String metrcId = "Not Submitted";
                if (ts.getMetrcId() != null) {
                    metrcId = String.valueOf(ts.getMetrcId());
                } else if (ts.getTraceSubmitStatus() != Transaction.TraceSubmissionStatus.None) {
                    metrcId = ts.getTraceSubmitStatus().name();
                    if (ts.getTraceSubmitStatus() == Transaction.TraceSubmissionStatus.SubmissionPending) {
                        metrcId = Transaction.TraceSubmissionStatus.SubmissionWaitingID.name();
                    }
                }
                data.put("MetrcId", metrcId);
            }

            data.put(ts.getCart().getPaymentOption().toString(), new DollarAmount(total));

            if (ts.getCart().getPaymentOption() == Cart.PaymentOption.Split && ts.getCart().getSplitPayment() != null) {
                SplitPayment splitPayment = ts.getCart().getSplitPayment();
                BigDecimal cashSales = splitPayment.getCashAmt();
                BigDecimal creditSales = splitPayment.getCreditDebitAmt();
                BigDecimal checkSales = splitPayment.getCheckAmt();
                BigDecimal storeCreditSales = splitPayment.getStoreCreditAmt();
                BigDecimal cashlessAtm = splitPayment.getCashlessAmt();

                BigDecimal splitChanged = splitPayment.getTotalSplits().subtract(BigDecimal.valueOf(total));

                if (splitChanged.compareTo(new BigDecimal(0)) > 0) {
                    cashSales = cashSales.subtract(splitChanged);
                }

                data.put(Cart.PaymentOption.Cash.toString(), new DollarAmount(cashSales.doubleValue() * factor));
                data.put(Cart.PaymentOption.Credit.toString(), new DollarAmount(creditSales.doubleValue() * factor));
                data.put(Cart.PaymentOption.Check.toString(), new DollarAmount(checkSales.doubleValue() * factor));
                data.put(Cart.PaymentOption.StoreCredit.toString(), new DollarAmount(storeCreditSales.doubleValue() * factor));
                data.put(Cart.PaymentOption.CashlessATM.toString(), new DollarAmount(cashlessAtm.doubleValue() * factor));
            }

            for (String paymentOption : paymentOptions) {
                data.putIfAbsent(paymentOption, new DollarAmount(0D));
            }

            report.add(data);


        }

        return report;
    }

    private double calcCOGS(final double quantity, final ProductBatch batch) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost;
        return NumberUtils.round(total, 2);
    }


    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }
}