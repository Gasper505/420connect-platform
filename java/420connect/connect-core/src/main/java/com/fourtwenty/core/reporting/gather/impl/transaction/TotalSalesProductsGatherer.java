package com.fourtwenty.core.reporting.gather.impl.transaction;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.ExciseTaxInfoRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.reporting.gather.Gatherer;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.SalesDependentDetails;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.services.mgmt.ReportDetailsService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class TotalSalesProductsGatherer implements Gatherer {

    @Inject
    ShopRepository shopRepository;
    @Inject
    ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    private ReportDetailsService reportDetailsService;

    private String[] attrs = new String[]{
            "Date", //0
            "Trans No.", //1
            "Trans Type", //2
            "Trans Status", //3
            "Product SKU", //4
            "Product Name", //4
            "Product Category", //5
            "Brand Name", //6
            "Vendor", //7
            "Member", //8
            "Consumer Tax Type", //9
            "Cannabis", //10
            "Quantity Sold", //11
            "Batch", //12
            "COGs", //13
            "Retail Value", //14
            "Product Discounts", //15
            "Subtotal", //16
            "Cart Discounts", //17
            "Final Subtotal", //18
            "Pre ALExcise Tax", //19
            "Pre NALExcise Tax", //20

            "Pre City Tax", //21
            "Pre County Tax", //22
            "Pre State Tax", //23
            "Pre Federal Tax", //24

            "Post ALExcise Tax", //25
            "Post NALExcise Tax", //26
            "City Tax", //27
            "County Tax", //28
            "State Tax", //29
            "Federal Tax", //30
            "Total Tax", //31
            "After Tax Discount", //32
            "Delivery Fees", //33
            "Credit Card Fees", //34
            "Gross Receipt", //35
            "Employee", //36
            "Terminal", //37
            "Payment Type", //38
            "Promotion(s)", //39
            "Marketing Source", //40
            "Member Group", // 41
            "Zip Code", // 42
            "Member State", //43
            "Date Joined", //44
            "Gender", //45
            "DOB", //46
            "Age", //47
            "Loyalty Points", //48
            "Created By", //49
            "Created Date", //50
            "Prepared By", //51
            "Prepared Date", //52
            "Packed  By", //53
            "Packed Date", //54
            "Member ID",
            "Discount Notes"

    };
    private ArrayList<String> reportHeaders = new ArrayList<>();
    private Map<String, GathererReport.FieldType> fieldTypes = new HashMap<>();

    public TotalSalesProductsGatherer() {

        Collections.addAll(reportHeaders, attrs);
        GathererReport.FieldType[] types = new GathererReport.FieldType[]{
                GathererReport.FieldType.STRING, // Date 0
                GathererReport.FieldType.STRING, // transNo 1
                GathererReport.FieldType.STRING, // type 2
                GathererReport.FieldType.STRING, // status 3
                GathererReport.FieldType.STRING, // Product SKU 4
                GathererReport.FieldType.STRING, // Product Name 4
                GathererReport.FieldType.STRING, // Product Category 5
                GathererReport.FieldType.STRING, // Brand Name 6
                GathererReport.FieldType.STRING, // Vendor 7
                GathererReport.FieldType.STRING, // Member 8
                GathererReport.FieldType.STRING, // Consumer Tax Type 9
                GathererReport.FieldType.STRING, // Cannabis 10
                GathererReport.FieldType.STRING, // Quantity Sold 11
                GathererReport.FieldType.STRING, // Batch 12
                GathererReport.FieldType.CURRENCY, // COGs 13
                GathererReport.FieldType.CURRENCY, // Cost 14
                GathererReport.FieldType.CURRENCY, // product discount 15
                GathererReport.FieldType.CURRENCY, // Subtotal 16
                GathererReport.FieldType.CURRENCY, // cart discounts 17
                GathererReport.FieldType.CURRENCY, // final subtotal 18
                GathererReport.FieldType.CURRENCY, // after tax discounts 19
                GathererReport.FieldType.CURRENCY, // pre AL excise tax 20
                GathererReport.FieldType.CURRENCY, // pre NAL excise tax 21

                GathererReport.FieldType.CURRENCY, // cityTax 22
                GathererReport.FieldType.CURRENCY, // countyTax 23
                GathererReport.FieldType.CURRENCY, // stateTax 24
                GathererReport.FieldType.CURRENCY, // stateTax 25

                GathererReport.FieldType.CURRENCY, // post AL excise tax 26
                GathererReport.FieldType.CURRENCY, // post NAL excise tax 27
                GathererReport.FieldType.CURRENCY, // cityTax 28
                GathererReport.FieldType.CURRENCY, // countyTax 29
                GathererReport.FieldType.CURRENCY, // stateTax 30
                GathererReport.FieldType.CURRENCY, // federal 31
                GathererReport.FieldType.CURRENCY, // post taxes 32
                GathererReport.FieldType.CURRENCY, // delivery fees 33
                GathererReport.FieldType.CURRENCY, // Credit Card fees 34
                GathererReport.FieldType.NUMBER, // total sale 35
                GathererReport.FieldType.STRING, // employee 36
                GathererReport.FieldType.STRING, // term 37
                GathererReport.FieldType.STRING, // payment type 38
                GathererReport.FieldType.STRING, // promotions 39
                GathererReport.FieldType.STRING, // marketing src 40
                GathererReport.FieldType.STRING, // Member Group 41
                GathererReport.FieldType.STRING, // Zip 42
                GathererReport.FieldType.STRING, // Zip 43
                GathererReport.FieldType.STRING, // Joined Date 44
                GathererReport.FieldType.STRING, // Gender 45
                GathererReport.FieldType.STRING, //DOB 46
                GathererReport.FieldType.STRING, //AGE 47
                GathererReport.FieldType.STRING, //Loyalty Points 48
                GathererReport.FieldType.STRING, // CreatedBy 49
                GathererReport.FieldType.STRING, // CreatedDate 50
                GathererReport.FieldType.STRING, // prepared By 51
                GathererReport.FieldType.STRING, // prepared Date 52
                GathererReport.FieldType.STRING, // packed By 53
                GathererReport.FieldType.STRING , // packed Date 54
                GathererReport.FieldType.STRING,  //Loyalty Points 55,
                GathererReport.FieldType.STRING,// 56
                GathererReport.FieldType.STRING //  Discount Notes
        };
        for (int i = 0; i < attrs.length; i++) {
            fieldTypes.put(attrs[i], types[i]);
        }
    }

    @Override
    public GathererReport gather(ReportFilter filter) {
        GathererReport report = new GathererReport(filter, "Completed Sales Detail Report", reportHeaders);
        report.setReportPostfix(ProcessorUtil.dateString(filter.getTimeZoneStartDateMillis()) + " - " +
                ProcessorUtil.dateString(filter.getTimeZoneEndDateMillis()));
        report.setReportFieldTypes(fieldTypes);

        SalesDependentDetails details = reportDetailsService.prepareSalesDependentDetails(filter, true, true);
        List<Transaction> transactions = details.getTransactions();
        HashMap<String, Employee> employeeMap = details.getEmployees();
        HashMap<String, Terminal> terminalMap = details.getTerminals();
        HashMap<String, Promotion> promotionMap = details.getPromotions();
        HashMap<String, LoyaltyReward> rewardMap = details.getRewards();
        HashMap<String, Member> memberHashMap = details.getMembers();
        HashMap<String, Product> productMap = details.getProducts();

        HashMap<String, ProductBatch> allBatchMap = details.getAllBatches();
        HashMap<String, Prepackage> prepackageHashMap = details.getPrepackages();
        HashMap<String, PrepackageProductItem> productItemHashMap = details.getPrepackageProductItems();
        HashMap<String, ProductWeightTolerance> toleranceHashMap = details.getWeightTolerances();
        HashMap<String, ProductBatch> recentBatchMap = details.getRecentBatchMap();

        HashMap<String, List<Product>> productsByCompanyLinkId = details.getProductsByCompanyLinkId();
        HashMap<String, MemberGroup> memberGroupHashMap = details.getMemberGroups();
        HashMap<String, Brand> brandHashMap = details.getBrands();
        HashMap<String, ProductCategory> categoryHashMap = details.getCategories();
        HashMap<String, Vendor> vendorHashMap = details.getVendors();

        Shop shop = shopRepository.get(filter.getCompanyId(), filter.getShopId());

        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }
        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);


        long now = DateTime.now().getMillis();
        int factor = 1;
        for (Transaction ts : transactions) {
            factor = 1;
            if (ts.getTransType() == Transaction.TransactionType.Refund && ts.getCart() != null && ts.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }
            Member member = memberHashMap.get(ts.getMemberId());

            if (member == null) {
                continue;
            }

            Cart cart = ts.getCart();
            if (cart == null) {
                continue;
            }

            String consumerTaxType = cart.getFinalConsumerTye().getDisplayName();
            /*if (cart.getTaxTable() != null) {
                consumerTaxType = cart.getTaxTable().getName();
            }*/

            Employee emp = employeeMap.get(ts.getSellerId());
            String employeeName = "";
            if (emp != null) {
                employeeName = emp.getFirstName() + " " + emp.getLastName();
            }

            Terminal terminal = terminalMap.get(ts.getSellerTerminalId());
            String terminalName = "";
            if (terminal != null) {
                terminalName = terminal.getName();
            }

            //Getting promotions applied for each transaction
            LinkedHashSet<PromotionReq> promos = cart.getPromotionReqs();
            StringBuilder sb = new StringBuilder();
            Iterator<PromotionReq> it = promos.iterator();

            while (it.hasNext()) {
                PromotionReq promotionReq = it.next();
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    Promotion promotion = promotionMap.get(promotionReq.getPromotionId());

                    if (promotion != null) {
                        sb.append(promotion.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                } else {
                    LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                    if (reward != null) {
                        sb.append(reward.getName());
                        if (it.hasNext()) {
                            sb.append("; ");
                        }
                    }
                }
            }


            String processedDate = ProcessorUtil.timeStampWithOffsetLong(ts.getProcessedTime(), filter.getTimezoneOffset());
            Cart.PaymentOption paymentOption = cart.getPaymentOption();
            String transNo = ts.getTransNo();


            // Calculate discounts proportionally so we can apply taxes

            HashMap<String, Double> propRatioMap = new HashMap<>();

            if (ts.getCart().getItems().size() > 0) {
                double total = 0.0;

                // Sum up the total
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {
                        if (product.isDiscountable()) {
                            total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                        }
                    }
                }

                // Calculate the ratio to be applied
                for (OrderItem orderItem : ts.getCart().getItems()) {
                    if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                            && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                        continue;
                    }
                    Product product = productMap.get(orderItem.getProductId());
                    if (product != null) {

                        propRatioMap.put(orderItem.getId(), 1d); // 100 %
                        if (product.isDiscountable() && total > 0) {
                            double finalCost = orderItem.getFinalPrice().doubleValue();
                            double ratio = finalCost / total;

                            propRatioMap.put(orderItem.getId(), ratio);
                        }
                    }

                }
            }

            // Now do real calculation
            for (OrderItem item : cart.getItems()) {

                String productId = item.getProductId();
                Product product = productMap.get(productId);
                if (product == null) {
                    continue;
                }


                boolean cannabis = false;
                double cityTax = 0;
                double countyTax = 0;
                double stateTax = 0;
                double exciseTax = 0;
                double federalTax = 0;


                double preCityTax = 0;
                double preCountyTax = 0;
                double preStateTax = 0;
                double preFederalTax = 0;


                Double ratio = propRatioMap.get(item.getId());
                if (ratio == null) {
                    ratio = 1d;
                }

                Double propCartDiscount = ts.getCart().getCalcCartDiscount() != null ? ts.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propDeliveryFee = ts.getCart().getDeliveryFee() != null ? ts.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                Double propCCFee = ts.getCart().getCreditCardFee() != null ? ts.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                Double propAfterTaxDiscount = ts.getCart().getAppliedAfterTaxDiscount() != null ? ts.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;

                double preALExciseTax = 0;
                double preNALExciseTax = 0;
                double postALExciseTax = 0;
                double postNALExciseTax = 0;

                double totalTax = 0;
                double subTotalAfterdiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                if (item.getTaxResult() != null) {
                    preALExciseTax = item.getTaxResult().getOrderItemPreALExciseTax().doubleValue();
                    preNALExciseTax = item.getTaxResult().getTotalNALPreExciseTax().doubleValue();
                    postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                    postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        cityTax = item.getTaxResult().getTotalCityTax().doubleValue();
                        stateTax = item.getTaxResult().getTotalStateTax().doubleValue();
                        countyTax = item.getTaxResult().getTotalCountyTax().doubleValue();
                        federalTax = item.getTaxResult().getTotalFedTax().doubleValue();
                        exciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                        totalTax += item.getTaxResult().getTotalPostCalcTax().doubleValue();
                    }


                    preCityTax = item.getTaxResult().getTotalCityPreTax().doubleValue();
                    preCountyTax = item.getTaxResult().getTotalCountyPreTax().doubleValue();
                    preStateTax = item.getTaxResult().getTotalStatePreTax().doubleValue();
                    preFederalTax = item.getTaxResult().getTotalFedPreTax().doubleValue();

                }
                if (totalTax == 0) {
                    if ((item.getTaxTable() == null || item.getTaxTable().isActive() == false) && item.getTaxInfo() != null) {
                        TaxInfo taxInfo = item.getTaxInfo();
                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            cityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                            stateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                            federalTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();
                            totalTax = cityTax + stateTax + federalTax;
                        } else {
                            preCityTax = subTotalAfterdiscount * taxInfo.getCityTax().doubleValue();
                            preStateTax = subTotalAfterdiscount * taxInfo.getStateTax().doubleValue();
                            preFederalTax = subTotalAfterdiscount * taxInfo.getFederalTax().doubleValue();
                        }
                    }
                }

                if (item.getExciseTax().doubleValue() > 0) {
                    exciseTax = item.getExciseTax().doubleValue();
                }

                if (totalTax == 0) {
                    totalTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                }

                totalTax += postALExciseTax + postNALExciseTax;

                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                Brand brand = brandHashMap.get(product.getBrandId());
                ProductCategory.UnitType unitType = category.getUnitType();
                if ((product.getCannabisType() == Product.CannabisType.DEFAULT && category.isCannabis())
                        || (product.getCannabisType() != Product.CannabisType.CBD
                        && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                        && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                    cannabis = true;
                }

                double finalAfterTax = item.getFinalPrice().doubleValue() - NumberUtils.round(propCartDiscount, 4)
                        + NumberUtils.round(propDeliveryFee, 4) + NumberUtils.round(totalTax, 4) + NumberUtils.round(propCCFee, 4) - NumberUtils.round(propAfterTaxDiscount, 4);

                Vendor vendor = vendorHashMap.get(product.getVendorId());

                StringBuilder batchLogs = new StringBuilder();
                Double cogs = 0d;
                boolean calculated = false;
                String productName = product.getName();
                PrepackageProductItem prepackageProductItem = productItemHashMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    ProductBatch targetBatch = allBatchMap.get(prepackageProductItem.getBatchId());

                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null && targetBatch != null) {
                        calculated = true;
                        BigDecimal unitValue = prepackage.getUnitValue();
                        if (unitValue == null || unitValue.doubleValue() == 0) {
                            ProductWeightTolerance weightTolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            unitValue = weightTolerance.getUnitValue();
                        }
                        // calculate the total quantity based on the prepackage value
                        double unitsSold = item.getQuantity().doubleValue() * unitValue.doubleValue();
                        cogs += calcCOGS(unitsSold, targetBatch, factor);

                        batchLogs.append(targetBatch.getSku());

                        productName = String.format("%s (%s)", product.getName(), prepackage.getName());
                    }
                } else if (item.getQuantityLogs() != null && item.getQuantityLogs().size() > 0) {
                    // otherwise, use quantity logs
                    for (QuantityLog quantityLog : item.getQuantityLogs()) {
                        if (StringUtils.isNotBlank(quantityLog.getBatchId())) {
                            ProductBatch targetBatch = allBatchMap.get(quantityLog.getBatchId());
                            if (targetBatch != null) {
                                calculated = true;
                                cogs += calcCOGS(quantityLog.getQuantity().doubleValue(), targetBatch, factor);
                            }
                        }
                        ProductBatch batch = allBatchMap.get(quantityLog.getBatchId());
                        if (batch != null) {
                            batch = getRecentBatch(product, recentBatchMap, productsByCompanyLinkId);
                        }
                        if (batch != null) {
                            batchLogs.append(batch.getSku() + ",");
                        }
                    }
                }

                if (!calculated) {
                    double unitCost = getUnitCost(product, recentBatchMap, productsByCompanyLinkId);
                    cogs = unitCost * item.getQuantity().doubleValue() * factor;
                }


                String unitTypeStr = (category != null && category.getUnitType() == ProductCategory.UnitType.grams) ? "g" : "ea";

                double itemCogs = cogs;
                double itemCost = item.getCost().doubleValue() * factor;
                double itemDiscount = item.getCalcDiscount().doubleValue() * factor;
                double itemfinalPrice = item.getFinalPrice().doubleValue() * factor;
                double itemCartDiscount = propCartDiscount * factor;
                double finalSubtotal = itemfinalPrice - itemCartDiscount;
                double itemAfterTaxDiscount = propAfterTaxDiscount * factor;
                double itemPreALExciseTax = preALExciseTax * factor;
                double itemPreNalExciseTax = preNALExciseTax * factor;

                double itemPreCityTax = preCityTax * factor;
                double itemPreCountyTax = preCountyTax * factor;
                double itemPreStateTax = preStateTax * factor;
                double itemPreFederalTax = preFederalTax * factor;

                double itemPOSTALExciseTax = postALExciseTax * factor;
                double itemPOSTNALExciseTax = postNALExciseTax * factor;
                double itemCityTax = cityTax * factor;
                double itemCountyTax = countyTax * factor;
                double itemStateTax = stateTax * factor;
                double itemFederalTAx = federalTax * factor;
                double itemTotalTAx = totalTax * factor;
                double itemDeliveryFee = propDeliveryFee * factor;
                double itemPropCCFee = propCCFee * factor;
                double itemGrossReceipt = finalAfterTax * factor;

                if ((Transaction.TransactionType.Refund == ts.getTransType() && Cart.RefundOption.Void == ts.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    // zero out since this is the old refund
                    itemCogs = 0d;
                    itemCost = 0d;
                    itemDiscount = 0d;
                    itemfinalPrice = 0d;
                    itemCartDiscount = 0d;
                    itemAfterTaxDiscount = 0d;
                    itemPreALExciseTax = 0d;
                    itemPreNalExciseTax = 0d;
                    itemPOSTALExciseTax = 0d;
                    itemPOSTNALExciseTax = 0d;
                    itemCityTax = 0d;
                    itemCountyTax = 0d;
                    itemStateTax = 0d;
                    itemFederalTAx = 0d;
                    itemTotalTAx = 0d;
                    itemDeliveryFee = 0d;
                    itemPropCCFee = 0d;
                    itemGrossReceipt = 0d;
                }


                String createdBy = "";
                String prepareBy = "";
                String packedBy = "";
                String preparedDate = "";
                String packedDate = "";

                if (StringUtils.isNotBlank(ts.getCreatedById())) {
                    Employee createdEmployee = employeeMap.get(ts.getCreatedById());
                    createdBy = createdEmployee.getFirstName() + " " + createdEmployee.getLastName();
                }

                if (ts.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment || ts.getCheckoutType() == Shop.ShopCheckoutType.FulfillmentThreeStep) {
                    if (StringUtils.isNotBlank(ts.getPreparedBy())) {
                        Employee prepareEmployee = employeeMap.get(ts.getPreparedBy());
                        if (prepareEmployee != null) {
                            prepareBy = prepareEmployee.getFirstName() + " " + prepareEmployee.getLastName();
                        }
                    }
                    if (StringUtils.isNotBlank(ts.getPackedBy())) {
                        Employee packedEmployee = employeeMap.get(ts.getPackedBy());
                        packedBy = packedEmployee.getFirstName() + " " + packedEmployee.getLastName();
                    }
                    preparedDate = (ts.getPreparedDate() != null) ? ProcessorUtil.timeStampWithOffsetLong(ts.getPreparedDate(), filter.getTimezoneOffset()) : "";
                    packedDate = (ts.getPackedDate() != null) ? ProcessorUtil.timeStampWithOffsetLong(ts.getPackedDate(), filter.getTimezoneOffset()) : "";
                }

                HashMap<String, Object> data = new HashMap<>();
                data.put(attrs[0], processedDate);
                data.put(attrs[1], transNo);
                data.put(attrs[2], ts.getTransType());
                data.put(attrs[3], ts.getStatus());
                data.put(attrs[4], product.getSku());
                data.put(attrs[5], productName);
                data.put(attrs[6], category.getName());
                data.put(attrs[7], (brand == null || StringUtils.isBlank(brand.getName())) ? "" : brand.getName());
                data.put(attrs[8], vendor != null ? vendor.getName() : ""); // vendor
                data.put(attrs[9], member.getFirstName() + " " + member.getLastName());
                data.put(attrs[10], consumerTaxType);
                data.put(attrs[11], cannabis ? "Yes" : "No");

                data.put(attrs[12], item.getQuantity().doubleValue() + " " + unitTypeStr);
                data.put(attrs[13], batchLogs.toString());
                data.put(attrs[14], new DollarAmount(itemCogs)); // cogs
                data.put(attrs[15], new DollarAmount(itemCost));
                data.put(attrs[16], new DollarAmount(itemDiscount));
                data.put(attrs[17], new DollarAmount(itemfinalPrice));
                data.put(attrs[18], new DollarAmount(itemCartDiscount));
                data.put(attrs[19], new DollarAmount(finalSubtotal));

                data.put(attrs[20], new DollarAmount(itemPreALExciseTax));
                data.put(attrs[21], new DollarAmount(itemPreNalExciseTax));

                data.put(attrs[22], new DollarAmount(itemPreCityTax));
                data.put(attrs[23], new DollarAmount(itemPreCountyTax));
                data.put(attrs[24], new DollarAmount(itemPreStateTax));
                data.put(attrs[25], new DollarAmount(itemPreFederalTax));

                data.put(attrs[26], new DollarAmount(itemPOSTALExciseTax));
                data.put(attrs[27], new DollarAmount(itemPOSTNALExciseTax));

                data.put(attrs[28], new DollarAmount(itemCityTax));
                data.put(attrs[29], new DollarAmount(itemCountyTax));
                data.put(attrs[30], new DollarAmount(itemStateTax));
                data.put(attrs[31], new DollarAmount(itemFederalTAx));
                data.put(attrs[32], new DollarAmount(itemTotalTAx));
                data.put(attrs[33], new DollarAmount(itemAfterTaxDiscount));
                data.put(attrs[34], new DollarAmount(itemDeliveryFee));
                data.put(attrs[35], new DollarAmount(itemPropCCFee));
                data.put(attrs[36], new DollarAmount(itemGrossReceipt));

                data.put(attrs[37], employeeName);
                data.put(attrs[38], terminalName);
                data.put(attrs[39], paymentOption);
                data.put(attrs[40], sb.toString());
                data.put(attrs[41], TextUtil.textOrEmpty(member.getMarketingSource()));

                String memberGroup = "";
                if (memberGroupHashMap.containsKey(member.getMemberGroupId())) {
                    memberGroup = memberGroupHashMap.get(member.getMemberGroupId()).getName();
                }
/*

                GathererReport.FieldType.STRING, // Joined Date 29
                GathererReport.FieldType.STRING, // Gender 30
                GathererReport.FieldType.STRING, //DOB 31
                GathererReport.FieldType.STRING //Loyalty Points 32
 */
                String zipCode = "";
                String memberState = "";
                if (member.getAddress() != null) {
                    zipCode = member.getAddress().getZipCode();
                    memberState = member.getAddress().getState();
                }

                data.put(attrs[42], memberGroup);
                data.put(attrs[43], TextUtil.textOrEmpty(zipCode)); // zip
                data.put(attrs[44], TextUtil.textOrEmpty(memberState)); // state
                data.put(attrs[45], DateUtil.toDateFormatted(member.getStartDate(), shop.getTimeZone()));
                data.put(attrs[46], member.getSex().name());
                data.put(attrs[47], DateUtil.toDateFormatted(member.getDob()));
                data.put(attrs[48], member.getDob() != null ? DateUtil.getYearsBetweenTwoDates(member.getDob(), now) : ""); // age
                data.put(attrs[49], NumberUtils.round(member.getLoyaltyPoints(), 2));
                data.put(attrs[50], StringUtils.isNotBlank(createdBy) ? createdBy : "N/A");
                data.put(attrs[51], ProcessorUtil.timeStampWithOffsetLong(ts.getCreated(), filter.getTimezoneOffset()));
                data.put(attrs[52], StringUtils.isNotBlank(prepareBy) ? prepareBy : "N/A");
                data.put(attrs[53], StringUtils.isNotBlank(preparedDate) ? preparedDate : "N/A");
                data.put(attrs[54], StringUtils.isNotBlank(packedBy) ? packedBy : "N/A");
                data.put(attrs[55], StringUtils.isNotBlank(packedDate) ? packedDate : "N/A");
                data.put(attrs[56], member.getId());
                data.put(attrs[57], StringUtils.isNotBlank(item.getDiscountNotes()) ? item.getDiscountNotes() : "N/A");
                report.add(data);
            }
        }
        return report;
    }

    public double calcCOGS(final double quantity, final ProductBatch batch, int factor) {

        double unitCost = batch == null ? 0 : batch.getFinalUnitCost().doubleValue();

        double total = quantity * unitCost * factor;
        return NumberUtils.round(total, 2);
    }


    public double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public ProductBatch getRecentBatch(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }
        return batch;
    }
}
