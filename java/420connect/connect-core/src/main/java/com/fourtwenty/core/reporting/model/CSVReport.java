package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.reporting.exception.ReportException;
import com.fourtwenty.core.reporting.processing.FormatProcessor;

import java.io.InputStream;

/**
 * Created by Stephen Schmidt on 4/9/2016.
 */
public class CSVReport extends ProcessedReport {
    InputStream csvData;

    public CSVReport(FormatProcessor.ReportFormat format,
                     String reportName, String startDate, String endDate) {
        this.format = format;
        this.reportName = reportName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public CSVReport(FormatProcessor.ReportFormat format,
                     String reportName, String startDate, String endDate, InputStream output) {
        this(format, reportName, startDate, endDate);
        csvData = output;
    }

    public void setCsvData(InputStream data) throws ReportException {
        if (data instanceof InputStream) {
            this.csvData = data;
        } else {
            throw new ReportException("CSVReport: Expected data type of OutputStream");
        }
    }

    @Override
    public String getContentType() {
        return "text/csv";
    }

    @Override
    public InputStream getData() {
        return csvData;
    }
}
