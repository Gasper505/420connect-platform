package com.fourtwenty.core.reporting.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DailySummary implements Comparable<DailySummary> {
    public HashSet<Cart.PaymentOption> customPaymentOptions = new HashSet<>();
    public HashMap<Cart.PaymentOption, BigDecimal> customPaymentTotals = new HashMap<>();
    public HashMap<String, EmployeeStats> employeeStatsMap = new HashMap<>();

    public Long day;
    public int totalVisits, newMembers, refunds, totalAllVisits;
    public int cashCount, checkCount, creditcount, storeCreditCount, splitCount = 0;
    public int cashlessCount = 0;

    public Double cashSales, checkSales, creditSales, storeCreditSales, totalRefunds, totalDiscounts, afterTaxDiscount, splitSales, cogs, subTotalSales, deliveryFees, creditFees, cashlessAtmSales;
    public Double gross = 0d;
    public LinkedHashSet<String> memberList = new LinkedHashSet<>();


    public double recSales, mmicSales, tpSales = 0; // SALES BEFORE DISCOUNTS

    public Double totalRecTax = 0d;
    public Double totalMedTax = 0d;
    public Double totalTax = 0d;
    public double salesItems = 0d;
    public double refundItems = 0d;
    public int salesCount = 0;
    public int refundCount = 0;

    public double preALExciseTax = 0d;
    public double preNAlExciseTax = 0d;
    public double preCountyTax = 0d;
    public double preCityTax = 0d;
    public double preStateTax = 0d;
    public double preFedralTax = 0d;

    public double postALExciseTax = 0d;
    public double postNALExciseTax = 0d;
    public double postCountyTax = 0d;
    public double postStateTax = 0d;
    public double postCityTax = 0d;
    public double postFedralTax = 0d;

    public double postTotalTax = 0d;
    public double transactionCount = 0d;
    public double totalrefundCount = 0d;
    public double fees = 0d;
    public double grossProfit = 0d;
    public double preTotalTax = 0d;

    public List<EmployeeStats> employeeStatsList = new ArrayList<>();
    public DailySummary() {
    }

    public DailySummary(Long day, HashSet<Cart.PaymentOption> customPaymentOptions) {
        this.day = day;
        this.customPaymentOptions = customPaymentOptions;
        totalRefunds = totalDiscounts = afterTaxDiscount = cashSales = checkSales = creditSales = storeCreditSales = splitSales = subTotalSales = cogs = deliveryFees = creditFees = cashlessAtmSales = 0d;
        totalVisits = refunds = newMembers = 0;
        memberList = new LinkedHashSet<>();
    }

    @JsonIgnore
    public Double getTotalSales() {
        return cashSales + checkSales + creditSales + splitSales;
    }
    @JsonIgnore
    @Override
    public int compareTo(DailySummary o) {
        return new Long(day - o.day).intValue();
    }

    @JsonIgnore
    private double getAvg(double amt, int count) {
        double avg = 0d;
        if (count > 0) {
            avg = amt / count;
        }
        return avg;
    }

    @JsonIgnore
    public HashMap<String, Object> getData(ArrayList<String> reportHeaders) {
        HashMap<String, Object> data = new HashMap<>();
        int i = 0;
        data.put(reportHeaders.get(i), ProcessorUtil.dateString(day));
        data.put(reportHeaders.get(++i), newMembers);
        data.put(reportHeaders.get(++i), memberList.size());
        data.put(reportHeaders.get(++i), totalAllVisits);
        data.put(reportHeaders.get(++i), totalVisits);
        data.put(reportHeaders.get(++i), refunds);

        data.put(reportHeaders.get(++i), cashCount);
        data.put(reportHeaders.get(++i), checkCount);
        data.put(reportHeaders.get(++i), creditcount);
        data.put(reportHeaders.get(++i), storeCreditCount);
        data.put(reportHeaders.get(++i), splitCount);
        data.put(reportHeaders.get(++i), cashlessCount);


        data.put(reportHeaders.get(++i), new DollarAmount(gross));
        data.put(reportHeaders.get(++i), new DollarAmount(cashSales));
        data.put(reportHeaders.get(++i), new DollarAmount(checkSales));
        data.put(reportHeaders.get(++i), new DollarAmount(creditSales));
        data.put(reportHeaders.get(++i), new DollarAmount(storeCreditSales));
        for (Cart.PaymentOption paymentOption : customPaymentOptions) {
            data.put(
                    reportHeaders.get(++i),
                    new DollarAmount(
                            customPaymentTotals.getOrDefault(paymentOption, new BigDecimal(0))).doubleValue());
        }
        data.put(reportHeaders.get(++i), new DollarAmount(splitSales));
        data.put(reportHeaders.get(++i), new DollarAmount(cashlessAtmSales));


        double avgReceipts = getAvg(gross, totalVisits);
        double avgCashRecipts = getAvg(cashSales, cashCount);
        double avgCheckReceipts = getAvg(checkSales, checkCount);
        double avgCreditReceipts = getAvg(creditSales, creditcount);
        double avgStoreCreditReceipts = getAvg(storeCreditSales, storeCreditCount);
        double avgSplitReceipts = getAvg(splitSales, splitCount);
        double avgCashlessSales = getAvg(cashlessAtmSales, cashlessCount);

        data.put(reportHeaders.get(++i), new DollarAmount(avgReceipts));
        data.put(reportHeaders.get(++i), new DollarAmount(avgCashRecipts));
        data.put(reportHeaders.get(++i), new DollarAmount(avgCheckReceipts));
        data.put(reportHeaders.get(++i), new DollarAmount(avgCreditReceipts));
        data.put(reportHeaders.get(++i), new DollarAmount(avgStoreCreditReceipts));
        data.put(reportHeaders.get(++i), new DollarAmount(avgSplitReceipts));
        data.put(reportHeaders.get(++i), new DollarAmount(avgCashlessSales));


        data.put(reportHeaders.get(++i), new DollarAmount(totalDiscounts));
        data.put(reportHeaders.get(++i), new DollarAmount(afterTaxDiscount));
        data.put(reportHeaders.get(++i), new DollarAmount(totalRefunds));
        data.put(reportHeaders.get(++i), new DollarAmount(cogs));

        data.put(reportHeaders.get(++i), new DollarAmount(subTotalSales));

        data.put(reportHeaders.get(++i), new DollarAmount(recSales));
        data.put(reportHeaders.get(++i), new DollarAmount(mmicSales));
        data.put(reportHeaders.get(++i), new DollarAmount(tpSales));


        data.put(reportHeaders.get(++i), new DollarAmount(deliveryFees));
        data.put(reportHeaders.get(++i), new DollarAmount(creditFees));


        data.put(reportHeaders.get(++i), new DollarAmount(preALExciseTax));
        data.put(reportHeaders.get(++i), new DollarAmount(preNAlExciseTax));
        data.put(reportHeaders.get(++i), new DollarAmount(preCountyTax));
        data.put(reportHeaders.get(++i), new DollarAmount(preCityTax));
        data.put(reportHeaders.get(++i), new DollarAmount(preStateTax));
        data.put(reportHeaders.get(++i), new DollarAmount(preFedralTax));

        data.put(reportHeaders.get(++i), new DollarAmount(postALExciseTax));
        data.put(reportHeaders.get(++i), new DollarAmount(postNALExciseTax));
        data.put(reportHeaders.get(++i), new DollarAmount(postCountyTax));
        data.put(reportHeaders.get(++i), new DollarAmount(postStateTax));
        data.put(reportHeaders.get(++i), new DollarAmount(postCityTax));
        data.put(reportHeaders.get(++i), new DollarAmount(postFedralTax));

        data.put(reportHeaders.get(++i), new DollarAmount(postTotalTax));

        return data;
    }

    public static class EmployeeStats {
        public String sellerId;
        public double sales = 0;
        public int salesCount = 0;
        public double refunds = 0;
        public int refundCount = 0;
        public String name;

        public EmployeeStats() {
        }
    }
}