package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import org.joda.time.DateTime;

import java.util.*;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public class GathererReport {
    public static final int TIMESTAMP = 0;
    public static final int DATE_BRACKET = 1;

    ReportFilter filter;
    String reportName;
    List<HashMap<String, Object>> data;
    String postfix;
    Set<String> headers = new LinkedHashSet<>();
    Map<String, FieldType> fieldTypes = new HashMap<>();
    Map<String, Map<String, Object>> summaryData = new HashMap<>();

    public GathererReport(ReportFilter filter, String reportName, List<String> headers) {
        this.filter = filter;
        this.reportName = reportName;
        data = new ArrayList<>();
        this.headers.addAll(headers);
    }

    public void setReportHeaders(ArrayList<String> reportHeaders) {
        this.headers.clear();
        this.headers.addAll(reportHeaders);
    }

    public enum FieldType {

        NUMBER("number"),
        CURRENCY("currency"),
        PERCENTAGE("percentage"),
        STRING("string"),
        DATE("date"),
        BIGDECIMAL("bigdecimal"),
        BOOLEAN("boolean"),
        WHOLE_NUMBER("number");

        private String type;

        FieldType(String type) {
            this.type = type;
        }
    }

    public void setData(List<HashMap<String, Object>> data) {
        this.data = data;
    }

    public void add(HashMap<String, Object> row) {
        this.data.add(row);
    }

    public List<HashMap<String, Object>> getData() {
        return data;
    }

    public Set<String> getHeaders() {
        return headers;
    }

    public String getReportName() {
        return reportName;
    }

    public String getStartDate() {
        return filter.getStartDate();
    }

    public String getEndDate() {
        return filter.getEndDate();
    }

    public void setReportPostfix(String postfix) {
        this.postfix = postfix;
    }

    public void setReportPostfix(int postfix) {
        switch (postfix) {
            case TIMESTAMP:
                setReportPostfix(ProcessorUtil.timeStampWithOffset(DateTime.now().getMillis(), filter.getTimezoneOffset()));
                break;
            case DATE_BRACKET:
                setReportPostfix(filter.getStartDate() + " - " + filter.getEndDate());
                break;
        }
    }

    public String getReportPostfix() {
        return this.postfix;
    }

    public void setReportFieldTypes(Map<String, FieldType> fieldtypeMap) {
        this.fieldTypes = fieldtypeMap;
    }

    public Map<String, FieldType> getFieldTypes() {
        return fieldTypes;
    }

    public Map<String, Map<String, Object>> getSummaryData() {
        return summaryData;
    }

    public void setSummaryData(Map<String, Map<String, Object>> summaryData) {
        this.summaryData = summaryData;
    }
}
