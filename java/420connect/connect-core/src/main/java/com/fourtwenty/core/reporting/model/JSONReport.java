package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.reporting.processing.FormatProcessor;

/**
 * Created by Stephen Schmidt on 6/3/2016.
 */
public class JSONReport extends ProcessedReport {
    String jsonData;
    //JSONObject jsonData;

    public JSONReport(FormatProcessor.ReportFormat format,
                      String reportName, String startDate, String endDate, String jsonData) {
        this.format = format;
        this.reportName = reportName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.jsonData = jsonData;
    }

    @Override
    public String getContentType() {
        return "application/json";
    }

    @Override
    public Object getData() {
        return jsonData;
    }
}
