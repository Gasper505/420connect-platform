package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.reporting.processing.FormatProcessor;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public abstract class ProcessedReport implements Report {

    FormatProcessor.ReportFormat format;
    String contentType;
    String reportName;
    String startDate;
    String endDate;

    @Override
    public FormatProcessor.ReportFormat getFormat() {
        return format;
    }

    @Override
    public String getReportName() {
        return reportName;
    }

    public String getLongReportName() {
        return reportName + " from " + startDate + " to " + endDate;
    }

    @Override
    public String getStartDate() {
        return startDate;
    }

    @Override
    public String getEndDate() {
        return endDate;
    }

    @Override
    public String getContentType() {
        return contentType;
    }


    public abstract Object getData();
}
