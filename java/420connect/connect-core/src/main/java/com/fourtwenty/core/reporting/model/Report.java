package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.reporting.processing.FormatProcessor;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public interface Report {
    Object getData();

    String getReportName();

    String getLongReportName();

    String getStartDate();

    String getEndDate();

    String getContentType();

    FormatProcessor.ReportFormat getFormat();
}
