package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.HashMap;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public class ReportFilter {
    private static final Log LOG = LogFactory.getLog(ReportFilter.class);
//time.equalsIgnoreCase("today") || time.equalsIgnoreCase("week") || time.equalsIgnoreCase("month") || time.equalsIgnoreCase("year")
    private static final String TODAY = "today";
    private static final String WEEK = "week";
    private static final String MONTH = "month";
    private static final String YEAR = "year";


    public static final long SECOND_IN_MILLIS = 1000;
    public static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
    public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS * 60;
    public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS * 24;
    public static final long WEEK_IN_MILLIS = DAY_IN_MILLIS * 7;
    public static final String ALL_EMPLOYEES = "ALL";
    public static final String ALL_INVENTORIES = "ALL";
    public static final String ALL_CATEGORIES = "ALL";

    public static final long COUNT = 10000;


    ConnectAuthToken token;
    ReportType type;
    HashMap<String, String> map;
    Long startDateMillis, endDateMillis;
    String startDate, endDate;
    Long timeZoneStartDateMillis, timeZoneEndDateMillis;
    int timezoneOffset = 0;
    String employeeId = ALL_EMPLOYEES; // default is all employees
    String inventoryId = ALL_INVENTORIES;
    String categoryId = ALL_CATEGORIES;
    String reconciliationId;
    String memberId;
    int limit = 10;
    String customerId;
    InventoryOperation.SourceType sourceType = InventoryOperation.SourceType.None;
    String productId;
    String currentEmployeeId;
    String requestId;
    int monthsDuration = 0;
    boolean isToday = false;
    String timezone = ConnectAuthToken.DEFAULT_REQ_TIMEZONE;

    String batchId;

    public ReportFilter(Shop shop, ReportType type, ConnectAuthToken token, String keyValueString, String startDate, String endDate) {
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.token = token;
        map = new HashMap<>();
        if (keyValueString != null) {
            String[] parts = keyValueString.split(",");
            for (String part : parts) {
                String[] keyPairs = part.split(":");
                if (keyPairs.length == 2) {
                    map.put(keyPairs[0], keyPairs[1]);
                }
            }

        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        if (startDate != null) {
            startDateMillis = formatter.parseDateTime(startDate).getMillis();
        } else {
            startDateMillis = 0l;
        }

        DateTime jodaEndDate = null;
        if (endDate != null) {
            jodaEndDate = formatter.parseDateTime(endDate);
            jodaEndDate = jodaEndDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
            endDateMillis = jodaEndDate.getMillis();
        } else {
            jodaEndDate = DateTime.now();
            endDateMillis = jodaEndDate.getMillis();
        }

        //DateTime today = DateUtil.nowWithTimeZone(shop.getTimeZone());
        String todayDate = DateUtil.toDateFormatted(DateUtil.nowUTC().getMillis(),shop.getTimeZone());
        timezone = shop.getTimeZone();
        if (startDate != null) {
            if (todayDate.equalsIgnoreCase(startDate)) {
                isToday = true;
            }
        }


        /*if (getMap().containsKey("timezoneOffset")) {
            String offset = getMap().get("timezoneOffset");
            timezoneOffset = Integer.parseInt(offset);
        }*/

        int shopTimezoneOffset = shop.getTimezoneOffsetInMinutes()*-1;

        /*if (getMap().containsKey("timezoneOffset")) {
            String offset = getMap().get("timezoneOffset");
            timezoneOffset = Integer.parseInt(offset);
        }*/
        timezoneOffset = shopTimezoneOffset;

        if (getMap().containsKey("employeeId")) {
            String empId = getMap().get("employeeId");
            if (StringUtils.isNotBlank(empId)) {
                employeeId = empId;
            }
        }

        if (getMap().containsKey("inventoryId")) {
            String invId = getMap().get("inventoryId");
            if (StringUtils.isNotBlank(invId)) {
                inventoryId = invId;
            }
        }
        if (getMap().containsKey("categoryId")) {
            String catId = getMap().get("categoryId");
            if (StringUtils.isNotBlank(catId)) {
                categoryId = catId;
            }
        }
        if (getMap().containsKey("reconciliationId")) {
            String reconcileId = getMap().get("reconciliationId");
            if (StringUtils.isNotBlank(reconcileId)) {
                reconciliationId = reconcileId;
            }
        }
        if (getMap().containsKey("memberId")) {
            String memId = getMap().get("memberId");
            if (StringUtils.isNotBlank(memId)) {
                memberId = memId;
            }
        }
        if (getMap().containsKey("customerId")) {
            String custId = getMap().get("customerId");
            if (StringUtils.isNotBlank(custId)) {
                customerId = custId;
            }
        }

        if (getMap().containsKey("sourceType")) {
            String source = getMap().get("sourceType");
            try {
                sourceType = InventoryOperation.SourceType.valueOf(source);
            } catch (Exception e) {
                sourceType = InventoryOperation.SourceType.None;
            }
        }

        if (getMap().containsKey("productId")) {
            String prodId = getMap().get("productId");
            if (StringUtils.isNotBlank(prodId)) {
                productId = prodId;
            }
        }

        if (getMap().containsKey("batchId")) {
            String prodBatchId = getMap().get("batchId");
            if (StringUtils.isNotBlank(prodBatchId)) {
                batchId = prodBatchId;
            }
        }

        DateTime estartDate = new DateTime(startDateMillis);
        timeZoneStartDateMillis = estartDate.withTimeAtStartOfDay().plusMinutes(timezoneOffset).getMillis();
        timeZoneEndDateMillis = jodaEndDate.plusMinutes(timezoneOffset).getMillis();

        DateTime currentTimeInPDT = DateUtil.nowWithTimeZone(null);
        int hrOfDay = currentTimeInPDT.getHourOfDay();
        LOG.info("hrOfDay: " + hrOfDay);
        int months = DateUtil.getMonthsBetweenTwoDates(timeZoneStartDateMillis, timeZoneEndDateMillis);
        this.monthsDuration = months;
    }

    public String getTimezone() {
        return timezone;
    }


    public boolean isToday() {
        return isToday;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public boolean shouldGenerate() {
        return monthsDuration > 2;
    }

    public HashMap<String, String> getMap() {
        return map;
    }

    public int getTimezoneOffset() {
        return timezoneOffset;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public Long getTimeZoneEndDateMillis() {
        return timeZoneEndDateMillis;
    }

    public Long getTimeZoneStartDateMillis() {
        return timeZoneStartDateMillis;
    }

    public ReportType getType() {
        return type;
    }

    public String getCompanyId() {
        return token.getCompanyId();
    }

    public String getShopId() {
        return token.getShopId();
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public boolean isAllInventories() {
        return ALL_INVENTORIES.equals(inventoryId);
    }

    public boolean isAllEmployees() {
        return ALL_EMPLOYEES.equals(employeeId);
    }

    public String getReconciliationId() {
        return reconciliationId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Company.CompanyMembersShareOption getCompanyMembersShareOption() {
        return token.getMembersShareOption();
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public InventoryOperation.SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(InventoryOperation.SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCurrentEmployeeId() {
        return currentEmployeeId = token.getActiveTopUser().getUserId();
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getStartDateMillis() {
        return startDateMillis;
    }
}
