package com.fourtwenty.core.reporting.model;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SalesDependentDetails {
    private List<Transaction> transactions = new ArrayList<>();
    private HashMap<String, Employee> employees = new HashMap<>();
    private HashMap<String, Terminal> terminals = new HashMap<>();
    private HashMap<String, Promotion> promotions = new HashMap<>();
    private HashMap<String, LoyaltyReward> rewards = new HashMap<>();
    private HashMap<String, Member> members = new HashMap<>();
    private HashMap<String, Product> products = new HashMap<>();
    private HashMap<String, Vendor> vendors = new HashMap<>();
    private HashMap<String, ProductCategory> categories = new HashMap<>();
    private HashMap<String, Brand> brands = new HashMap<>();

    private HashMap<String, ProductBatch> allBatches = new HashMap<>();
    private HashMap<String, Prepackage> prepackages = new HashMap<>();
    private HashMap<String, PrepackageProductItem> prepackageProductItems = new HashMap<>();

    private HashMap<String, ProductWeightTolerance> weightTolerances = new HashMap<>();
    private HashMap<String, MemberGroup> memberGroups = new HashMap<>();
    private HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
    private HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public SalesDependentDetails setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    public SalesDependentDetails setEmployees(HashMap<String, Employee> employees) {
        this.employees = employees;
        return this;
    }

    public HashMap<String, Terminal> getTerminals() {
        return terminals;
    }

    public SalesDependentDetails setTerminals(HashMap<String, Terminal> terminals) {
        this.terminals = terminals;
        return this;
    }

    public HashMap<String, Promotion> getPromotions() {
        return promotions;
    }

    public SalesDependentDetails setPromotions(HashMap<String, Promotion> promotions) {
        this.promotions = promotions;
        return this;
    }

    public HashMap<String, LoyaltyReward> getRewards() {
        return rewards;
    }

    public SalesDependentDetails setRewards(HashMap<String, LoyaltyReward> rewards) {
        this.rewards = rewards;
        return this;
    }

    public HashMap<String, Member> getMembers() {
        return members;
    }

    public SalesDependentDetails setMembers(HashMap<String, Member> members) {
        this.members = members;
        return this;
    }

    public HashMap<String, Product> getProducts() {
        return products;
    }

    public SalesDependentDetails setProducts(HashMap<String, Product> products) {
        this.products = products;
        return this;
    }

    public HashMap<String, Vendor> getVendors() {
        return vendors;
    }

    public SalesDependentDetails setVendors(HashMap<String, Vendor> vendors) {
        this.vendors = vendors;
        return this;
    }

    public HashMap<String, ProductCategory> getCategories() {
        return categories;
    }

    public SalesDependentDetails setCategories(HashMap<String, ProductCategory> categories) {
        this.categories = categories;
        return this;
    }

    public HashMap<String, Brand> getBrands() {
        return brands;
    }

    public SalesDependentDetails setBrands(HashMap<String, Brand> brands) {
        this.brands = brands;
        return this;
    }

    public HashMap<String, ProductBatch> getAllBatches() {
        return allBatches;
    }

    public SalesDependentDetails setAllBatches(HashMap<String, ProductBatch> allBatches) {
        this.allBatches = allBatches;
        return this;
    }

    public HashMap<String, Prepackage> getPrepackages() {
        return prepackages;
    }

    public SalesDependentDetails setPrepackages(HashMap<String, Prepackage> prepackages) {
        this.prepackages = prepackages;
        return this;
    }

    public HashMap<String, PrepackageProductItem> getPrepackageProductItems() {
        return prepackageProductItems;
    }

    public SalesDependentDetails setPrepackageProductItems(HashMap<String, PrepackageProductItem> prepackageProductItems) {
        this.prepackageProductItems = prepackageProductItems;
        return this;
    }

    public HashMap<String, ProductWeightTolerance> getWeightTolerances() {
        return weightTolerances;
    }

    public SalesDependentDetails setWeightTolerances(HashMap<String, ProductWeightTolerance> weightTolerances) {
        this.weightTolerances = weightTolerances;
        return this;
    }

    public HashMap<String, MemberGroup> getMemberGroups() {
        return memberGroups;
    }

    public SalesDependentDetails setMemberGroups(HashMap<String, MemberGroup> memberGroups) {
        this.memberGroups = memberGroups;
        return this;
    }

    public SalesDependentDetails setRecentBatchMap(HashMap<String, ProductBatch> recentBatchMap) {
        this.recentBatchMap = recentBatchMap;
        return this;
    }

    public HashMap<String, ProductBatch> getRecentBatchMap() {
        return recentBatchMap;
    }

    public HashMap<String, List<Product>> getProductsByCompanyLinkId() {
        return productsByCompanyLinkId;
    }

    public SalesDependentDetails setProductsByCompanyLinkId(HashMap<String, List<Product>> productsByCompanyLinkId) {
        this.productsByCompanyLinkId = productsByCompanyLinkId;
        return this;
    }
}
