package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/30/2016.
 */
public class BestSellingProduct {
    @JsonProperty("_id")
    String id;

    Double sales;

    public String getId() {
        return id;
    }

    public Double getSales() {
        return sales;
    }
}
