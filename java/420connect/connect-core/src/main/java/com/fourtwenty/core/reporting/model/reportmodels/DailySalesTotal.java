package com.fourtwenty.core.reporting.model.reportmodels;

/**
 * Created by Stephen Schmidt on 5/24/2016.
 */
public class DailySalesTotal {
    String day = "";
    Double total = 0.0;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
