package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 6/21/2016.
 */
public class InventoryByStrain {
    @JsonProperty("_id")
    String id;
    double value;

    public String getName() {
        return id;
    }

    public double getValue() {
        return value;
    }
}
