package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.product.ProductQuantity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 11/8/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryHistory {
    @JsonProperty("_id")
    private String productId;
    private String employeeId;
    private String reference;
    private List<ProductQuantity> productQuantities = new ArrayList<ProductQuantity>();

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<ProductQuantity> getProductQuantities() {
        return productQuantities;
    }

    public void setProductQuantities(List<ProductQuantity> productQuantities) {
        this.productQuantities = productQuantities;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
