package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/14/2016.
 */
public class InventoryReport {
    @JsonProperty("_id")
    String id;
    double totalQuant;
    double totalValue;

    public String get_id() {
        return id;
    }

    public void set_id(String _id) {
        this.id = _id;
    }

    public double getTotalQuant() {
        return totalQuant;
    }

    public void setTotalQuant(double totalQuant) {
        this.totalQuant = totalQuant;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

}
