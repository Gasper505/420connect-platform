package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/3/2016.
 */
public class MemberByField {
    @JsonProperty("_id")
    private String id;
    private Long count;

    public String get_id() {
        return id;
    }

    public void set_id(String _id) {
        this.id = _id;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
