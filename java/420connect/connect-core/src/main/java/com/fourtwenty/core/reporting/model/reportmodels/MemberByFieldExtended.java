package com.fourtwenty.core.reporting.model.reportmodels;

import java.util.ArrayList;

public class MemberByFieldExtended extends MemberByField {
    private ArrayList<MemberReportView> members;

    public void setMembers(ArrayList<MemberReportView> members) {
        this.members = members;
    }

    public ArrayList<MemberReportView> getMembers() {
        return members;
    }

}
