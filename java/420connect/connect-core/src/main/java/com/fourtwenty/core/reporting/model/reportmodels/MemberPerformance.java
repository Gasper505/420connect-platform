package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.serializers.DobDeserializer;
import com.fourtwenty.core.domain.serializers.DobSerializer;
import com.fourtwenty.core.domain.serializers.GenderEnumDeserializer;
import com.fourtwenty.core.domain.serializers.GenderEnumSerializer;

import java.math.BigDecimal;

public class MemberPerformance {

    @JsonProperty("_id")
    private String id;
    private String primaryPhone;
    @JsonSerialize(using = DobSerializer.class)
    @JsonDeserialize(using = DobDeserializer.class)
    private Long dob;
    @JsonSerialize(using = GenderEnumSerializer.class)
    @JsonDeserialize(using = GenderEnumDeserializer.class)
    private BaseMember.Gender sex = BaseMember.Gender.OTHER;
    private String firstName;
    private String lastName;
    private String marketingSource;
    private String memberGroupId;
    private long created;
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private BigDecimal loyaltyPoints = new BigDecimal(0);
    private Address address;
    private Long lastVisitDate;

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public BaseMember.Gender getSex() {
        return sex;
    }

    public void setSex(BaseMember.Gender sex) {
        this.sex = sex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public String getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(BigDecimal loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Long lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }
}
