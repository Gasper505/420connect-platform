package com.fourtwenty.core.reporting.model.reportmodels;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import org.bson.types.ObjectId;

public class MemberReportView {
    private ObjectId id;
    private long created;
    private ConsumerType consumerType = ConsumerType.AdultUse;

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }
}
