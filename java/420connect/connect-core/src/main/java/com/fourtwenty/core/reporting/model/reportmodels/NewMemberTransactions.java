package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/12/2016.
 */
public class NewMemberTransactions {
    @JsonProperty("_id")
    String id;
    int count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
