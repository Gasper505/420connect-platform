package com.fourtwenty.core.reporting.model.reportmodels;

/**
 * Created by mdo on 1/10/17.
 */
public class PaymentTypeKey {
    String paymentOption;
    String terminalId;

    public String getPaymentOption() {
        return paymentOption;
    }

    public String getTerminalId() {
        return terminalId;
    }
}
