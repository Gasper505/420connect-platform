package com.fourtwenty.core.reporting.model.reportmodels;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by Stephen Schmidt on 7/11/2016.
 */
public class Percentage extends Number {
    BigDecimal value;

    public Percentage(Double value) {
        try {
            if (value == null) {
                this.value = BigDecimal.valueOf(0);
            } else {
                this.value = BigDecimal.valueOf(value);
            }
        } catch (Exception e) {
            this.value = BigDecimal.ZERO;
        }
    }

    public Percentage(BigDecimal decimalValue) {
        this.value = decimalValue;
    }

    @Override
    public String toString() {
        Double d = this.value.multiply(new BigDecimal(100)).doubleValue();
        return new DecimalFormat("#.##").format(d);
    }

    @Override
    public int intValue() {
        return value.intValue();
    }

    @Override
    public long longValue() {
        return value.longValue();
    }

    @Override
    public float floatValue() {
        return value.floatValue();
    }

    @Override
    public double doubleValue() {
        return value.doubleValue();
    }
}
