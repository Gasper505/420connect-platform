package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.product.BatchQuantity;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatchByInventory {

    @JsonProperty("_id")
    private String inventoryId;
    private List<BatchQuantity> batchQuantity;

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public List<BatchQuantity> getBatchQuantity() {
        return batchQuantity;
    }

    public void setBatchQuantity(List<BatchQuantity> batchQuantity) {
        this.batchQuantity = batchQuantity;
    }
}
