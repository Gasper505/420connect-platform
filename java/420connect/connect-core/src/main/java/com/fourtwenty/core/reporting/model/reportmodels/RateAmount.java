package com.fourtwenty.core.reporting.model.reportmodels;

import java.math.BigDecimal;

/**
 * Created by Stephen Schmidt on 7/11/2016.
 */
public class RateAmount extends Number {
    Double value;

    public RateAmount(Double value) {
        this.value = value;
        if (this.value == null) {
            this.value = 0d;
        }
    }

    public RateAmount(BigDecimal dValue) {
        if (dValue != null) {
            this.value = dValue.doubleValue();
        } else {
            this.value = 0d;
        }
    }

    public RateAmount(Float value) {
        if (value == null) {
            this.value = 0d;
        } else {
            this.value = new Double(value);
        }
    }

    @Override
    public String toString() {
        return String.format("%.4f", value);
    }

    @Override
    public int intValue() {
        return value.intValue();
    }

    @Override
    public long longValue() {
        return value.longValue();
    }

    @Override
    public float floatValue() {
        return value.floatValue();
    }

    @Override
    public double doubleValue() {
        return value;
    }
}
