package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/22/2016.
 */
public class RecentTransaction {
    @JsonProperty("id")
    String id;
    Long processedTime;
    int items;
    double total;
    String queueName;

    public String getId() {
        return id;
    }

    public Long getTime() {
        return processedTime;
    }

    public int getItems() {
        return items;
    }

    public double getTotal() {
        return total;
    }

    public String getQueueName() {
        return queueName;
    }
}
