package com.fourtwenty.core.reporting.model.reportmodels;

/**
 * Created by Stephen Schmidt on 6/6/2016.
 */
public class SalesByDay {
    Long date;
    Double sales;

    public SalesByDay(Long date, Double sales) {
        this.date = date;
        this.sales = sales;
    }

    public void setSales(Double sales) {
        this.sales = sales;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getDate() {
        return date;
    }

    public Double getSales() {
        return sales;
    }


}
