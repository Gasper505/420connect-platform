package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 7/9/2016.
 */
public class SalesByEmployee {
    @JsonProperty("_id")
    String employeeId;
    Double sales;
    Integer transactions;

    public String getEmployeeId() {
        return employeeId;
    }

    public Double getSales() {
        return sales;
    }

    public Integer getTransactions() {
        return transactions;
    }
}
