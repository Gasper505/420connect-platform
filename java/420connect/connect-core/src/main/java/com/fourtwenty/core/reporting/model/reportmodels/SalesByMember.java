package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/10/2016.
 */
public class SalesByMember {
    @JsonProperty("_id")
    String id;
    int count;
    Float subtotal;
    Float total;
    Float tax;
    Float discount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float sales) {
        this.total = sales;
    }

}
