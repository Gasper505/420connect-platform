package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class SalesByPaymentType {
    @JsonProperty("_id")
    String payment;
    Double sales;
    Integer transactions;

    public String getPayment() {
        return payment;
    }

    public Double getSales() {
        return sales;
    }

    public Integer getTransactions() {
        return transactions;
    }
}
