package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 1/10/17.
 */
public class SalesByPaymentTypeTerminal {
    @JsonProperty("_id")
    PaymentTypeKey id;
    Double sales;
    Integer transactions;

    public PaymentTypeKey getId() {
        return id;
    }

    public Double getSales() {
        return sales;
    }

    public Integer getTransactions() {
        return transactions;
    }
}
