package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 5/30/2016.
 */
public class SalesByProduct {
    @JsonProperty("_id")
    String id;
    Double sales;
    Double tax;
    Double units;

    public Double getUnitsSold() {
        return units;
    }

    public String getId() {
        return id;
    }

    public Double getTax() {
        return tax;
    }

    public Double getSales() {
        return sales;
    }

    public void setSales(Double sales) {
        this.sales = sales;
    }


}
