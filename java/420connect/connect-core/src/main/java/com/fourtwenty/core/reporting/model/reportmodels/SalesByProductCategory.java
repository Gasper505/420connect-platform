package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesByProductCategory {
    @JsonProperty("_id")
    private String categoryId;

    private int count;
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private double totalSoldCost = 0.0d;
    @Deprecated
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    @DecimalMin("0")
    private double soldQuantity;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotalSoldCost() {
        return totalSoldCost;
    }

    public void setTotalSoldCost(double totalSoldCost) {
        this.totalSoldCost = totalSoldCost;
    }

    public double getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(double soldQuantity) {
        this.soldQuantity = soldQuantity;
    }
}
