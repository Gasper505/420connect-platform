package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class SalesByQueue {
    @JsonProperty("_id")
    String queue;
    Double sales;
    Integer transactions;

    public String getQueue() {
        return queue;
    }

    public Double getSales() {
        return sales;
    }

    public Integer getTransactions() {
        return transactions;
    }
}
