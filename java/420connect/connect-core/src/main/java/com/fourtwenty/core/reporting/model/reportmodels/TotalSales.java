package com.fourtwenty.core.reporting.model.reportmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Stephen Schmidt on 7/7/2016.
 */
public class TotalSales {

    @JsonProperty("_id")
    String transNo;
    Long processedTime;
    String memberId;
    Double subTotal;
    Double totalDiscount;
    Double tax;
    Double sales;
    Double units;

    public String getTransactionNumber() {
        return transNo;
    }

    public Long getDate() {
        return processedTime;
    }

    public String getCustomerId() {
        return memberId;
    }

    public Double getSubtotal() {
        return subTotal;
    }

    public Double getDiscount() {
        return totalDiscount;
    }

    public Double getTax() {
        return tax;
    }

    public Double getSales() {
        return sales;
    }


    public Double getUnits() {
        return units;
    }
}
