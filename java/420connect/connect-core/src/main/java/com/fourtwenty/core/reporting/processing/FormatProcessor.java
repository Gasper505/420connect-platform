package com.fourtwenty.core.reporting.processing;

import com.amazonaws.util.json.Jackson;
import com.fourtwenty.core.reporting.model.CSVReport;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.JSONReport;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.reporting.model.reportmodels.DollarAmount;
import com.fourtwenty.core.reporting.model.reportmodels.Percentage;
import com.fourtwenty.core.reporting.model.reportmodels.RateAmount;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public class FormatProcessor implements Process {
    public enum ReportFormat {
        CSV,
        JSON,
        PDF,
        DEFAULT;

        public static ReportFormat getType(String formatString) {
            try {
                return Enum.valueOf(ReportFormat.class, formatString.toUpperCase());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return ReportFormat.DEFAULT;
            }
        }
    }

    @Inject
    public FormatProcessor() {

    }

    @Override
    public Report process(ReportFormat format, GathererReport report) {
        switch (format) {
            case CSV:
                return processCSV(report);
            case JSON:
                return processJSON(report);
            case PDF:
                return processPDF(report);
            case DEFAULT:
                return null;
            default:
                return null;
        }
    }

    private CSVReport processCSV(GathererReport report) {
        if (report.getData() == null && report.getData().size() < 1) {
            return null;
        }
        //addTotalCVSRow(report);
        List<HashMap<String, Object>> data = report.getData();
        String name = report.getReportName() + " - " + report.getReportPostfix();
        StringBuilder builder = new StringBuilder();
        builder.append(name);
        builder.append("\n");
        printCSVRow(builder, report.getHeaders());
        for (HashMap<String, Object> row : data) {
            printCSVRow(builder, row, report.getHeaders());
        }
        ByteArrayInputStream bis = new ByteArrayInputStream(builder.toString().getBytes());
        return new CSVReport(ReportFormat.CSV, name, report.getStartDate(), report.getEndDate(), bis);
    }

    private Report processJSON(GathererReport report) {
        String reportJson = Jackson.toJsonString(report);
        return new JSONReport(ReportFormat.JSON, report.getReportName(), report.getStartDate(), report.getEndDate(), reportJson);

    }

    private Report processPDF(GathererReport report) {
        return null;
    }

    /**
     * Method to print a CSV row.
     *
     * @param builder the printstream for report output.
     * @param row     the current row for your current row data
     */
    private void printCSVRow(StringBuilder builder, HashMap<String, Object> row, Set<String> headers) {
        Iterator<String> it = headers.iterator();
        while (it.hasNext()) {
            String key = it.next();
            String value = "";
            Object item = row.get(key);

            if (item instanceof Percentage) {
                Percentage p = (Percentage) item;
                value = p.toString() + "%";
            } else if (item instanceof DollarAmount) {
                DollarAmount d = (DollarAmount) item;
                value = "$" + d.toString();
            } else if (item instanceof RateAmount) {
                RateAmount d = (RateAmount) item;
                value = "$" + d.toString();
            } else if (item == null) {
                value = "";
            } else {
                String cell = item.toString();
                //below pulls extra commas out of result data to avoid shifted reults

                cell = cell.replaceAll(",", " ");
                cell = cell.replaceAll("\n", "; ");
                cell = cell.replaceAll("<br/>", "; ");
                cell = cell.replaceAll("; ", "|");
                cell = cell.replaceAll("\t", " ");

                value = cell;
            }
            builder.append(value);
            //if that wasn't the last item in the row print a comma
            if (it.hasNext()) {
                builder.append(",");
                //otherwise print a newline so the next time your write it is on a new line
            } else {
                builder.append("\n");
            }
        }
    }

    private void printCSVRow(StringBuilder builder, Iterable<String> heads) {
        Iterator<String> it = heads.iterator();
        while (it.hasNext()) {
            String header = it.next();
            header = header.replaceAll(",", " ");
            header = header.replaceAll("\n", "; ");
            header = header.replaceAll("<br/>", "; ");
            builder.append(header); //add the corrected format entry
            if (it.hasNext()) {
                builder.append(",");
            } else {
                builder.append("\n");
            }
        }
    }

    private void addTotalCVSRow(GathererReport report) {
        HashMap<String, Object> data = new HashMap<>();
        Iterator<String> header = report.getHeaders().iterator();
        BigDecimal total = BigDecimal.ZERO;
        List<HashMap<String, Object>> listdata = report.getData();
        while (header.hasNext()) {
            String key = header.next();
            for (HashMap<String, Object> row : listdata) {
                switch (report.getFieldTypes().get(key)) {
                    case CURRENCY:
                    case NUMBER:
                        data.putIfAbsent(key, BigDecimal.ZERO);
                        total = new BigDecimal(data.get(key).toString());
                        Object rowData = row.get(key);
                        if (rowData != null) {
                            total = total.add(new BigDecimal(rowData.toString()));
                        }
                        data.put(key, total);
                        break;
                    default:
                        data.put(key, "");
                }
            }
        }

        report.add(data);
    }

}
