package com.fourtwenty.core.reporting.processing;

import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.Report;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public interface Process {

    Report process(FormatProcessor.ReportFormat format, GathererReport report);
}
