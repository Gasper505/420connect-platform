package com.fourtwenty.core.reporting.processing;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * Created by Stephen Schmidt on 5/7/2016.
 */
public class ProcessorUtil {

    public static String dateString(Long jodaSeconds) {
        DateTime dt = new DateTime(jodaSeconds);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String dateStringWithOffset(Long jodaSeconds, int minutesOffset) {
        DateTime dt = new DateTime(jodaSeconds);
        dt = dt.minusMinutes(minutesOffset);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String dateTimeString(Long jodaSeconds) {
        DateTime dt = new DateTime(jodaSeconds);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy - hh:mm a, z");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String timestamp(Long jodaSeconds) {
        DateTime dt = new DateTime(jodaSeconds);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a z");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String timeStampWithOffset(Long jodaSeconds, int minutesOffset) {
        DateTime dt = new DateTime(jodaSeconds);
        dt = dt.minusMinutes(minutesOffset);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String timeStampWithOffsetLong(Long jodaSeconds, int minutesOffset) {
        DateTime dt = new DateTime(jodaSeconds);
        dt = dt.minusMinutes(minutesOffset);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss:SSS a");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String timeOfDay(Long jodaSeconds) {
        DateTime dt = new DateTime(jodaSeconds);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm:ss a z");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String timeOfDayWithOffset(Long jodaSeconds, int minutesOffset) {
        DateTime dt = new DateTime(jodaSeconds);
        dt = dt.minusMinutes(minutesOffset);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm:ss a");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

    public static String dayString(Long jodaSeconds) {
        DateTime dt = new DateTime(jodaSeconds);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("EEEE");
        if (jodaSeconds == null) {
            return "";
        }
        return fmt.print(dt);
    }

}
