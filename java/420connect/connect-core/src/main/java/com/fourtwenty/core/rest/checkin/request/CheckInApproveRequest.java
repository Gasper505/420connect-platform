package com.fourtwenty.core.rest.checkin.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.checkin.CheckIn;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInApproveRequest {
    private CheckIn.CheckInStatus status;
    private boolean addToQueue;
    private String queueName;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public CheckIn.CheckInStatus getStatus() {
        return status;
    }

    public void setStatus(CheckIn.CheckInStatus status) {
        this.status = status;
    }

    public boolean isAddToQueue() {
        return addToQueue;
    }

    public void setAddToQueue(boolean addToQueue) {
        this.addToQueue = addToQueue;
    }
}
