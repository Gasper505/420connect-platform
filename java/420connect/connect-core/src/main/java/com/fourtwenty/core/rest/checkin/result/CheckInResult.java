package com.fourtwenty.core.rest.checkin.result;

import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.domain.models.customer.Member;

public class CheckInResult extends CheckIn {
    private Member member;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
