package com.fourtwenty.core.rest.comments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.comments.UserActivity;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserActivityAddRequest {
    private String employeeId;
    private String message;
    private UserActivity.ReferenceType referenceType;
    private String referenceId;
    private UserActivity.ActivityType activityType;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserActivity.ReferenceType getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(UserActivity.ReferenceType referenceType) {
        this.referenceType = referenceType;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public UserActivity.ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(UserActivity.ActivityType activityType) {
        this.activityType = activityType;
    }
}
