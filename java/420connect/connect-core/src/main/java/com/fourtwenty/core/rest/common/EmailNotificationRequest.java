package com.fourtwenty.core.rest.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailNotificationRequest extends EmailRequest {
    private long summaryDate = 0;

    public long getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(long summaryDate) {
        this.summaryDate = summaryDate;
    }
}
