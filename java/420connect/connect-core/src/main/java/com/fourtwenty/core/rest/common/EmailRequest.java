package com.fourtwenty.core.rest.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 10/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailRequest {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
