package com.fourtwenty.core.rest.compliance;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IntakeMetrcPackageRequest {
    @NotEmpty
    private String productId;
    private String batchId;
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal costPerUnit = new BigDecimal(0);

    private Long sellBy;
    private Long expirationDate;

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getSellBy() {
        return sellBy;
    }

    public void setSellBy(Long sellBy) {
        this.sellBy = sellBy;
    }
}
