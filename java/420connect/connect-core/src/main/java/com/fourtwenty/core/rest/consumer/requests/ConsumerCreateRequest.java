package com.fourtwenty.core.rest.consumer.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.serializers.GenderEnumDeserializer;
import com.fourtwenty.core.domain.serializers.GenderEnumSerializer;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 2/11/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerCreateRequest {
    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private String password;
    private String phoneNumber;
    private Long dob;
    private String marketingSource;
    private CompanyAsset agreement;

    @JsonSerialize(using = GenderEnumSerializer.class)
    @JsonDeserialize(using = GenderEnumDeserializer.class)
    private BaseMember.Gender sex = BaseMember.Gender.OTHER;

    private String contractId;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public BaseMember.Gender getSex() {
        return sex;
    }

    public void setSex(BaseMember.Gender sex) {
        this.sex = sex;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public CompanyAsset getAgreement() {
        return agreement;
    }

    public void setAgreement(CompanyAsset agreement) {
        this.agreement = agreement;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
}
