package com.fourtwenty.core.rest.consumer.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.auth.EmailLoginRequest;

/**
 * Created by mdo on 12/19/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerLoginRequest extends EmailLoginRequest {
}
