package com.fourtwenty.core.rest.consumer.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.serializers.GenderEnumDeserializer;
import com.fourtwenty.core.domain.serializers.GenderEnumSerializer;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 12/19/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerRegisterRequest {
    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private String phoneNumber;
    private Long dob;
    private String marketingSource;
    private boolean textOptIn;
    private boolean emailOptIn;
    private boolean medical;
    private ConsumerType consumerType = ConsumerType.AdultUse;
    @JsonSerialize(using = GenderEnumSerializer.class)
    @JsonDeserialize(using = GenderEnumDeserializer.class)
    private BaseMember.Gender sex = BaseMember.Gender.OTHER;

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isTextOptIn() {
        return textOptIn;
    }

    public void setTextOptIn(boolean textOptIn) {
        this.textOptIn = textOptIn;
    }

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public boolean isMedical() {
        return medical;
    }

    public void setMedical(boolean medical) {
        this.medical = medical;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public BaseMember.Gender getSex() {
        return sex;
    }

    public void setSex(BaseMember.Gender sex) {
        this.sex = sex;
    }
}
