package com.fourtwenty.core.rest.consumer.results;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;

/**
 * Created by mdo on 12/19/16.
 */
public class ConsumerAuthResult {
    private String accessToken;
    private String assetAccessToken;
    private ConsumerUser user;
    private long loginTime;
    private long expirationTime;
    private String sessionId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAssetAccessToken() {
        return assetAccessToken;
    }

    public void setAssetAccessToken(String assetAccessToken) {
        this.assetAccessToken = assetAccessToken;
    }

    public long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public ConsumerUser getUser() {
        return user;
    }

    public void setUser(ConsumerUser user) {
        this.user = user;
    }
}
