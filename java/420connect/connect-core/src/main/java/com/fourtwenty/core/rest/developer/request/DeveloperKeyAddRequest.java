package com.fourtwenty.core.rest.developer.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 2/15/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeveloperKeyAddRequest {
    @NotEmpty
    private String shopId;
    private boolean enableExposeSales = false;
    private String name;
    private boolean enableExposeMembers = Boolean.FALSE;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public boolean isEnableExposeSales() {
        return enableExposeSales;
    }

    public void setEnableExposeSales(boolean enableExposeSales) {
        this.enableExposeSales = enableExposeSales;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnableExposeMembers() {
        return enableExposeMembers;
    }

    public void setEnableExposeMembers(boolean enableExposeMembers) {
        this.enableExposeMembers = enableExposeMembers;
    }
}
