package com.fourtwenty.core.rest.dispensary.requests;


/**
 * Created by Stephen Schmidt on 2/2/2016.
 */
public abstract class BaseAddRequest {
    private String importId;

    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

}
