package com.fourtwenty.core.rest.dispensary.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 10/25/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateToleranceRequest {
    @NotEmpty
    private String name;
    @DecimalMin("0.0")
    private BigDecimal startWeight = new BigDecimal(0);
    @DecimalMin("0.0")
    private BigDecimal endWeight = new BigDecimal(0);
    @DecimalMin("0.0")
    private BigDecimal unitValue = new BigDecimal(0);
    private int priority;
    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getEndWeight() {
        return endWeight;
    }

    public void setEndWeight(BigDecimal endWeight) {
        this.endWeight = endWeight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public BigDecimal getStartWeight() {
        return startWeight;
    }

    public void setStartWeight(BigDecimal startWeight) {
        this.startWeight = startWeight;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }
}
