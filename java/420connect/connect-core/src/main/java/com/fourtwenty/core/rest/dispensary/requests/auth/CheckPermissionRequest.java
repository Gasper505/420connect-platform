package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 7/12/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckPermissionRequest extends PinRequest {
    private String permission;

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
