package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 10/2/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailLoginRequest {

    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;

    private String timezone;

    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.AuthenticationApp;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }
}
