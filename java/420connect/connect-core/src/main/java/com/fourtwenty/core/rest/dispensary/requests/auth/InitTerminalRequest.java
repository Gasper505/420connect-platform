package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 8/28/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InitTerminalRequest extends EmailLoginRequest {
    ///@NotEmpty
    private String deviceId;
    private String version = "1.0.0";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
