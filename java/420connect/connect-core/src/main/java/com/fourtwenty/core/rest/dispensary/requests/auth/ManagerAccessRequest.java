package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 5/15/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ManagerAccessRequest {
    @NotEmpty
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
