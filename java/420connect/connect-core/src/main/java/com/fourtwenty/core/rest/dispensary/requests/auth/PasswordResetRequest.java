package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 8/3/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordResetRequest {
    @NotEmpty
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
