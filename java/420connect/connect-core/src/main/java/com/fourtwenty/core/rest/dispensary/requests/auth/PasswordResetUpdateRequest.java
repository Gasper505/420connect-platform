package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 8/3/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordResetUpdateRequest {
    @NotEmpty
    private String resetCode;
    @NotEmpty
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }
}
