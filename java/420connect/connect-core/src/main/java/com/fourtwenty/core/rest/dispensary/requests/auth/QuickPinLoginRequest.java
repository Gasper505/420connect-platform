package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;

/**
 * Created by mdo on 9/20/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickPinLoginRequest extends PinRequest {
    private String deviceId;
    private String version = "1.0.0";
    private String deviceToken;
    private ConnectAuthToken.ConnectAppType appType = ConnectAuthToken.ConnectAppType.Blaze;
    private Terminal.DeviceType deviceType = Terminal.DeviceType.iOS;
    private String androidVersion;
    private String sdkVersion;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public ConnectAuthToken.ConnectAppType getAppType() {
        return appType;
    }

    public void setAppType(ConnectAuthToken.ConnectAppType appType) {
        this.appType = appType;
    }

    public Terminal.DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Terminal.DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }
}
