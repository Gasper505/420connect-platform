package com.fourtwenty.core.rest.dispensary.requests.auth;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 10/9/15.
 */
public class UpdateTokenRequest {
    @NotEmpty
    private String shopId;
    private ConnectAuthToken.GrowAppType growAppType = ConnectAuthToken.GrowAppType.None;
    private String companyId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public ConnectAuthToken.GrowAppType getGrowAppType() {
        return growAppType;
    }

    public void setGrowAppType(ConnectAuthToken.GrowAppType growAppType) {
        this.growAppType = growAppType;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
