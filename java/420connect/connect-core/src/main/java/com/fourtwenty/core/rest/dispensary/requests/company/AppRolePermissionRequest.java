package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Role;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppRolePermissionRequest {

    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Retail;
    private String roleId;
    private Set<Role.Permission> enabledPermissions;
    private Set<Role.Permission> disabledPermissions;

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Set<Role.Permission> getEnabledPermissions() {
        return enabledPermissions;
    }

    public void setEnabledPermissions(Set<Role.Permission> enabledPermissions) {
        this.enabledPermissions = enabledPermissions;
    }

    public Set<Role.Permission> getDisabledPermissions() {
        return disabledPermissions;
    }

    public void setDisabledPermissions(Set<Role.Permission> disabledPermissions) {
        this.disabledPermissions = disabledPermissions;
    }
}
