package com.fourtwenty.core.rest.dispensary.requests.company;

import org.hibernate.validator.constraints.NotEmpty;

public class CompanyContactLogAddRequest {
    @NotEmpty
    private String customerCompanyId;
    @NotEmpty
    private String companyContactId;
    @NotEmpty
    private String employeeId;
    private String log;

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public String getCompanyContactId() {
        return companyContactId;
    }

    public void setCompanyContactId(String companyContactId) {
        this.companyContactId = companyContactId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
