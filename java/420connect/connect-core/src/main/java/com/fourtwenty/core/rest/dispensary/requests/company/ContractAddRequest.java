package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Contract;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;

/**
 * Created by Stephen Schmidt on 12/7/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractAddRequest {
    @NotEmpty
    private String name;
    private String text;
    @Min(0)
    private long expiration;
    private boolean active;
    private boolean required;
    private Boolean enableWitnessSignature;
    private Boolean enableEmployeeSignature;

    private CompanyAsset pdfFile;
    private Contract.ContractContentType contentType = Contract.ContractContentType.TEXT;

    public Contract.ContractContentType getContentType() {
        return contentType;
    }

    public void setContentType(Contract.ContractContentType contentType) {
        this.contentType = contentType;
    }

    public CompanyAsset getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(CompanyAsset pdfFile) {
        this.pdfFile = pdfFile;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isActive() {
        return active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getEnableWitnessSignature() {
        return enableWitnessSignature;
    }

    public void setEnableWitnessSignature(Boolean enableWitnessSignature) {
        this.enableWitnessSignature = enableWitnessSignature;
    }

    public Boolean getEnableEmployeeSignature() {
        return enableEmployeeSignature;
    }

    public void setEnableEmployeeSignature(Boolean enableEmployeeSignature) {
        this.enableEmployeeSignature = enableEmployeeSignature;
    }
}
