package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CustomerCompany;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CustomerCompanyAddRequest {
    @NotEmpty
    private String name;
    private String email;
    private CustomerCompany.CompanyType companyType;
    private List<Address> addresses = new ArrayList<>();
    private String license;
    private String website;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal credits = new BigDecimal(0);
    private List<CompanyAsset> attachments = new ArrayList<>();
    private CustomerCompany.EntityTaxType entityTaxType;
    private String officePhoneNumber;
    private String mobileNumber;
    private CustomerCompany.LicenceType licenceType;
    private boolean relatedEntity;
    private Note note;
    private Long licenceExpirationDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CustomerCompany.CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CustomerCompany.CompanyType companyType) {
        this.companyType = companyType;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BigDecimal getCredits() {
        return credits;
    }

    public void setCredits(BigDecimal credits) {
        this.credits = credits;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public CustomerCompany.EntityTaxType getEntityTaxType() {
        return entityTaxType;
    }

    public void setEntityTaxType(CustomerCompany.EntityTaxType entityTaxType) {
        this.entityTaxType = entityTaxType;
    }

    public String getOfficePhoneNumber() {
        return officePhoneNumber;
    }

    public void setOfficePhoneNumber(String officePhoneNumber) {
        this.officePhoneNumber = officePhoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public CustomerCompany.LicenceType getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(CustomerCompany.LicenceType licenceType) {
        this.licenceType = licenceType;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Long getLicenceExpirationDate() {
        return licenceExpirationDate;
    }

    public void setLicenceExpirationDate(Long licenceExpirationDate) {
        this.licenceExpirationDate = licenceExpirationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
