package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 12/7/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EndDrawerRequest {
    private String comment;
    private double actualCash = 0;
    private Double startAmount;

    public Double getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(Double startAmount) {
        this.startAmount = startAmount;
    }

    public double getActualCash() {
        return actualCash;
    }

    public void setActualCash(double actualCash) {
        this.actualCash = actualCash;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
