package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;

/**
 * Created by Gaurav Saini on 2/5/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberGroupAddRequest {
    @NotEmpty
    private String name;
    private String companyId;
    private boolean active;
    private boolean defaultGroup;
    private boolean deleted;
    private boolean updated;
    private BigDecimal discount;
    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Percentage;
    private boolean enablePromotion = Boolean.FALSE;

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDefaultGroup() {
        return defaultGroup;
    }

    public void setDefaultGroup(boolean defaultGroup) {
        this.defaultGroup = defaultGroup;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    public boolean isEnablePromotion() {
        return enablePromotion;
    }

    public void setEnablePromotion(boolean enablePromotion) {
        this.enablePromotion = enablePromotion;
    }
}
