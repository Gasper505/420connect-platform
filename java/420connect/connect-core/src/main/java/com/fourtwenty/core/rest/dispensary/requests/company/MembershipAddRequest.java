package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.customer.CustomerAddRequest;

import java.util.LinkedHashSet;
import java.util.List;


/**
 * Created by Stephen Schmidt on 11/21/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipAddRequest extends CustomerAddRequest {

    private boolean anonymous = false;
    private boolean enabledCareGiver = Boolean.FALSE;
    private boolean enabledCannabisLimit = Boolean.FALSE;
    private LinkedHashSet<String> tags = new LinkedHashSet<>();

    private List<String> careGivers;

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public List<String> getCareGivers() {
        return careGivers;
    }

    public void setCareGivers(List<String> careGivers) {
        this.careGivers = careGivers;
    }

    public boolean isEnabledCareGiver() {
        return enabledCareGiver;
    }

    public void setEnabledCareGiver(boolean enabledCareGiver) {
        this.enabledCareGiver = enabledCareGiver;
    }

    public boolean isEnabledCannabisLimit() {
        return enabledCannabisLimit;
    }

    public void setEnabledCannabisLimit(boolean enabledCannabisLimit) {
        this.enabledCannabisLimit = enabledCannabisLimit;
    }

    public LinkedHashSet<String> getTags() {
        return tags;
    }

    public void setTags(LinkedHashSet<String> tags) {
        this.tags = tags;
    }
}
