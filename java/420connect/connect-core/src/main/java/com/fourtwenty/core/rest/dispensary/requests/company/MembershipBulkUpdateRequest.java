package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.customer.Member;

import java.util.List;

/**
 * Created on 7/11/17 3:48 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipBulkUpdateRequest {

    public enum MembershipBulkOperationType {
        SMS_OPT_IN, EMAIL_OPT_IN, STATUS, MEMBERSHIP_GROUP, MEMBERSHIP_TYPE, VERIFIED, MARKETING_SOURCE, ADD_NOTE, DELETE, LOYALTY, ID_VERIFIED
    }

    private List<String> memberIds;
    private MembershipBulkOperationType membershipBulkOperationType;
    private Boolean smsOptIn;
    private Boolean emailOptIn;
    private Member.MembershipStatus membershipStatus;
    private String memberGroupId;
    private Boolean medical;
    private Boolean verified;
    private String marketingSource;
    private String note;
    private Boolean enableLoyalty;
    private ConsumerType consumerType;
    private boolean updateAll;


    public List<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(List<String> memberIds) {
        this.memberIds = memberIds;
    }

    public MembershipBulkOperationType getMembershipBulkOperationType() {
        return membershipBulkOperationType;
    }

    public void setMembershipBulkOperationType(MembershipBulkOperationType membershipBulkOperationType) {
        this.membershipBulkOperationType = membershipBulkOperationType;
    }

    public Boolean getSmsOptIn() {
        return smsOptIn;
    }

    public void setSmsOptIn(Boolean smsOptIn) {
        this.smsOptIn = smsOptIn;
    }

    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public Member.MembershipStatus getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(Member.MembershipStatus membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    public String getMemberGroupId() {
        return memberGroupId;
    }

    public Boolean getMedical() {
        return medical;
    }

    public void setMedical(Boolean medical) {
        this.medical = medical;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getEnableLoyalty() {
        return enableLoyalty;
    }

    public void setEnableLoyalty(Boolean enableLoyalty) {
        this.enableLoyalty = enableLoyalty;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public boolean isUpdateAll() {
        return updateAll;
    }

    public void setUpdateAll(boolean updateAll) {
        this.updateAll = updateAll;
    }
}
