package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.customer.Member;

/**
 * Created by Stephen Schmidt on 11/21/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipUpdateRequest extends Member {


    /*private List<Preference> preferences = new ArrayList<>();
    private Long membershipLevelId;

    public Long getMembershipLevelId() {
        return membershipLevelId;
    }

    public void setMembershipLevelId(Long membershipLevelId) {
        this.membershipLevelId = membershipLevelId;
    }

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }*/
}
