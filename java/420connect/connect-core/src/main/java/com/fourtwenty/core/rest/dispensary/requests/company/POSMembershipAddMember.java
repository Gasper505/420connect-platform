package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.Note;

import java.util.List;

/**
 * Created by mdo on 12/13/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class POSMembershipAddMember extends MembershipAddRequest {
    private String id;
    private String shopContractId;
    private String signatureAssetId;
    private String employeeSignatureAssetId;
    private String witnessSignatureAssetId;
    private String employeeName;
    private String witnessName;
    private boolean signed;
    List<Note> notes;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getWitnessName() {
        return witnessName;
    }

    public void setWitnessName(String witnessName) {
        this.witnessName = witnessName;
    }

    public String getEmployeeSignatureAssetId() {
        return employeeSignatureAssetId;
    }

    public void setEmployeeSignatureAssetId(String employeeSignatureAssetId) {
        this.employeeSignatureAssetId = employeeSignatureAssetId;
    }

    public String getWitnessSignatureAssetId() {
        return witnessSignatureAssetId;
    }

    public void setWitnessSignatureAssetId(String witnessSignatureAssetId) {
        this.witnessSignatureAssetId = witnessSignatureAssetId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public boolean isSigned() {
        return signed;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public String getShopContractId() {
        return shopContractId;
    }

    public void setShopContractId(String shopContractId) {
        this.shopContractId = shopContractId;
    }

    public String getSignatureAssetId() {
        return signatureAssetId;
    }

    public void setSignatureAssetId(String signatureAssetId) {
        this.signatureAssetId = signatureAssetId;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
}
