package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 12/7/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaidInOutRequest {
    private boolean paidIn = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    private String reason = "";
    private boolean cashVault = false;
    private String toTerminalId;
    private String terminalId;
    private PaidInOutItem.PaidInOutType type;

    public boolean isCashVault() {
        return cashVault;
    }

    public void setCashVault(boolean cashVault) {
        this.cashVault = cashVault;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isPaidIn() {
        return paidIn;
    }

    public void setPaidIn(boolean paidIn) {
        this.paidIn = paidIn;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getToTerminalId() {
        return toTerminalId;
    }

    public void setToTerminalId(String toTerminalId) {
        this.toTerminalId = toTerminalId;
    }

    public PaidInOutItem.PaidInOutType getType() {
        return type;
    }

    public void setType(PaidInOutItem.PaidInOutType type) {
        this.type = type;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
}