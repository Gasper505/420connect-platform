package com.fourtwenty.core.rest.dispensary.requests.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Role;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mdo on 6/15/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleAddRequest {
    @NotEmpty
    private String name;
    private Set<Role.Permission> permissions = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Role.Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Role.Permission> permissions) {
        this.permissions = permissions;
    }
}
