package com.fourtwenty.core.rest.dispensary.requests.consumeruser.request;

public class OnlineConsumerUserRequest {

    public enum OnlineUserQueryType {
        TODAY,
        PENDING,
        ACCEPTED,
        REJECTED
    }

}
