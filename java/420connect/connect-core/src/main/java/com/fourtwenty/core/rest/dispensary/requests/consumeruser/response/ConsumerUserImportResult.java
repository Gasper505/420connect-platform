package com.fourtwenty.core.rest.dispensary.requests.consumeruser.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerUserImportResult {

    private List<ConsumerUser> addSuccess;
    private List<ConsumerUser> addFailed;

    public List<ConsumerUser> getAddSuccess() {
        return addSuccess;
    }

    public void setAddSuccess(List<ConsumerUser> addSuccess) {
        this.addSuccess = addSuccess;
    }

    public List<ConsumerUser> getAddFailed() {
        return addFailed;
    }

    public void setAddFailed(List<ConsumerUser> addFailed) {
        this.addFailed = addFailed;
    }
}
