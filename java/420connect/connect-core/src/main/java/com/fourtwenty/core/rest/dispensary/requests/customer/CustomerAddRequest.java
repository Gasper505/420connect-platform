package com.fourtwenty.core.rest.dispensary.requests.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.rest.dispensary.requests.BaseAddRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 8/26/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerAddRequest extends BaseAddRequest implements Importable {
    private List<Identification> identifications;
    private List<Recommendation> recommendations;
    private String id;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private String middleName;
    private Address address;
    private Long dob;
    private String primaryPhone;
    private boolean textOptIn = true;
    private String email;
    private boolean emailOptIn = true;
    private boolean medical;
    private boolean enableLoyalty = false;
    private String searchText;
    private String photoId;
    private BaseMember.Gender sex = BaseMember.Gender.OTHER;
    private String doctorImportId;
    private String memberGroupId;
    private long startDate;
    private Member.MembershipStatus status = Member.MembershipStatus.Active;
    private String marketingSource;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal loyaltyPoints = new BigDecimal(0);
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private ArrayList<Address> addresses = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }


    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(BigDecimal loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public boolean isEnableLoyalty() {
        return enableLoyalty;
    }

    public void setEnableLoyalty(boolean enableLoyalty) {
        this.enableLoyalty = enableLoyalty;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public Member.MembershipStatus getStatus() {
        return status;
    }

    public void setStatus(Member.MembershipStatus status) {
        this.status = status;
    }

    public String getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public List<Identification> getIdentifications() {
        return identifications;
    }

    public void setIdentifications(List<Identification> identifications) {
        this.identifications = identifications;
    }

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }


    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }


    public boolean isTextOptIn() {
        return textOptIn;
    }

    public void setTextOptIn(boolean textOptIn) {
        this.textOptIn = textOptIn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public boolean isMedical() {
        return medical;
    }

    public void setMedical(boolean medical) {
        this.medical = medical;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public BaseMember.Gender getSex() {
        return sex;
    }

    public void setSex(BaseMember.Gender sex) {
        this.sex = sex;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public String getDoctorImportId() {
        return doctorImportId;
    }

    public void setDoctorImportId(String doctorImportId) {
        this.doctorImportId = doctorImportId;
    }


    public void addIdentifcation(Identification id) {
        if (identifications == null) {
            identifications = new ArrayList<>();
        }
        identifications.add(id);
    }

    public void addRecommendation(Recommendation rec) {
        if (recommendations == null) {
            recommendations = new ArrayList<>();
        }
        recommendations.add(rec);
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }
}
