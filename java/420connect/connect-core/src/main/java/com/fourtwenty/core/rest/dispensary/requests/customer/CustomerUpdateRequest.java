package com.fourtwenty.core.rest.dispensary.requests.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.customer.Preference;

import java.util.List;

/**
 * Created by Stephen Schmidt on 11/11/15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerUpdateRequest extends CustomerAddRequest {
    private List<Preference> preferences;

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }
}
