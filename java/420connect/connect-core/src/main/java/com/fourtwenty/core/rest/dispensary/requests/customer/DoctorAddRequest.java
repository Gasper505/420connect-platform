package com.fourtwenty.core.rest.dispensary.requests.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.rest.dispensary.requests.BaseAddRequest;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DoctorAddRequest extends BaseAddRequest implements Importable {
    @NotEmpty
    private String license;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private Address address;
    private String phoneNumber;
    private String email;
    private String fax;
    private String website;
    private String degree;
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
