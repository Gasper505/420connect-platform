package com.fourtwenty.core.rest.dispensary.requests.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DoctorUpdateRequest extends DoctorAddRequest {
}
