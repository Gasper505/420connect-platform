package com.fourtwenty.core.rest.dispensary.requests.customer;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.customer.Doctor;

/**
 * Created by Stephen Schmidt on 11/15/2015.
 */

public class RecommendationAddRequest {
    private Long customerId;
    private String recommendationNumber;
    private String verifyWebsite;
    private String verifyPhoneNumber;
    private Long issueDate;
    private Long expirationDate;
    private String state;
    private Doctor doctor;

    private CompanyAsset frontPhoto;
    private CompanyAsset backPhoto;

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getRecommendationNumber() {
        return recommendationNumber;
    }

    public void setRecommendationNumber(String recommendationNumber) {
        this.recommendationNumber = recommendationNumber;
    }

    public String getVerifyWebsite() {
        return verifyWebsite;
    }

    public void setVerifyWebsite(String verifyWebsite) {
        this.verifyWebsite = verifyWebsite;
    }

    public String getVerifyPhoneNumber() {
        return verifyPhoneNumber;
    }

    public void setVerifyPhoneNumber(String verifyPhoneNumber) {
        this.verifyPhoneNumber = verifyPhoneNumber;
    }

    public Long getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Long issueDate) {
        this.issueDate = issueDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public CompanyAsset getFrontPhoto() {
        return frontPhoto;
    }

    public void setFrontPhoto(CompanyAsset frontPhoto) {
        this.frontPhoto = frontPhoto;
    }

    public CompanyAsset getBackPhoto() {
        return backPhoto;
    }

    public void setBackPhoto(CompanyAsset backPhoto) {
        this.backPhoto = backPhoto;
    }
}