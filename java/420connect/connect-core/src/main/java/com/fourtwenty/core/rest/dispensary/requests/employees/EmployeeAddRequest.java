package com.fourtwenty.core.rest.dispensary.requests.employees;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by mdo on 10/2/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeAddRequest implements InternalAllowable {
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String pin;
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;
    private List<String> shops = new ArrayList<>();
    @NotEmpty
    private String roleId;
    private String phoneNumber;
    private String inventoryId;
    private Address address;
    private Boolean canApplyCustomDiscount = false;
    private String vinNo;
    private String driversLicense;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleLicensePlate;
    private String dlExpirationDate;
    private LinkedHashSet<CompanyFeatures.AppTarget> appAccessList = new LinkedHashSet<>();
    private String insuranceCompanyName;
    private long insuranceExpireDate;
    private String policyNumber;
    private long registrationExpireDate;
    private String externalId;
    private boolean driver;
    private List<Note> notes = new ArrayList<>();

    public boolean isDriver() {
        return driver;
    }

    public void setDriver(boolean driver) {
        this.driver = driver;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getShops() {
        return shops;
    }

    public void setShops(List<String> shops) {
        this.shops = shops;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getCanApplyCustomDiscount() {
        return canApplyCustomDiscount;
    }

    public void setCanApplyCustomDiscount(Boolean canApplyCustomDiscount) {
        this.canApplyCustomDiscount = canApplyCustomDiscount;
    }

    public String getVinNo() {
        return vinNo;
    }

    public void setVinNo(String vinNo) {
        this.vinNo = vinNo;
    }

    public String getDriversLicense() {
        return driversLicense;
    }

    public void setDriversLicense(String driversLicense) {
        this.driversLicense = driversLicense;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlate() {
        return vehicleLicensePlate;
    }

    public void setVehicleLicensePlate(String vehicleLicensePlate) {
        this.vehicleLicensePlate = vehicleLicensePlate;
    }

    public String getDlExpirationDate() {
        return dlExpirationDate;
    }

    public void setDlExpirationDate(String dlExpirationDate) {
        this.dlExpirationDate = dlExpirationDate;
    }


    public LinkedHashSet<CompanyFeatures.AppTarget> getAppAccessList() {
        return appAccessList;
    }

    public void setAppAccessList(LinkedHashSet<CompanyFeatures.AppTarget> appAccessList) {
        this.appAccessList = appAccessList;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public long getInsuranceExpireDate() {
        return insuranceExpireDate;
    }

    public void setInsuranceExpireDate(long insuranceExpireDate) {
        this.insuranceExpireDate = insuranceExpireDate;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public long getRegistrationExpireDate() {
        return registrationExpireDate;
    }

    public void setRegistrationExpireDate(long registrationExpireDate) {
        this.registrationExpireDate = registrationExpireDate;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getExternalId() {
        return this.externalId;
    }
}
