package com.fourtwenty.core.rest.dispensary.requests.employees;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on 9/11/17 6:29 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeInviteTokenRequest {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
