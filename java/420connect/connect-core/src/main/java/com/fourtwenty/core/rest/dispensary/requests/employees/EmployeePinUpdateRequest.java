package com.fourtwenty.core.rest.dispensary.requests.employees;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 7/6/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeePinUpdateRequest {
    @NotEmpty
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
