package com.fourtwenty.core.rest.dispensary.requests.employees;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 6/22/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeTimeCardRequest {
    @NotEmpty
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
