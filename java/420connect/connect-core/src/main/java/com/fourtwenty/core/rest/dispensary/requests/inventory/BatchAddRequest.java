package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.PotencyMG;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductBatchLabel;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.domain.serializers.ProductBatchTrackTraceSystemDeserializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 10/12/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchAddRequest implements InternalAllowable {
    @NotEmpty
    private String productId;
    private Boolean active;
    @Min(0)
    @DecimalMin("0.0")
    private BigDecimal cost;
    @DecimalMin("0.0")
    private BigDecimal quantity;
    private String sku;
    private Double thc;
    private Double cbn;
    private Double cbd;
    private Double cbda;
    private Double thca;
    @Min(value = 1)
    private long purchasedDate;
    private long sellBy;
    private List<Note> notes = new ArrayList<>();
    private Long fullFilledDate;
    private boolean trackTraceVerified = false;
    private String trackPackageLabel;
    private String trackHarvestBatch;
    private String trackHarvestDate;
    @JsonDeserialize(using = ProductBatchTrackTraceSystemDeserializer.class)
    private ProductBatch.TrackTraceSystem trackTraceSystem = ProductBatch.TrackTraceSystem.METRC;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0.0")
    private BigDecimal costPerUnit;

    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal perUnitExciseTax = new BigDecimal(0);
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal totalExcisetax = new BigDecimal(0);
    private Boolean voidStatus = false;

    //Changes according incoming batch
    private String brandId;
    private Long receiveDate;
    private String poNumber;
    private ProductBatch.BatchStatus status = ProductBatch.BatchStatus.RECEIVED;
    private Inventory.InventoryType targetInventory = Inventory.InventoryType.Storage;
    private CompanyAsset batchQRAsset;

    private ProductBatchLabel productBatchLabel;
    private String lotId;
    private String metrcTagId;
    private String purchaseOrderId;

    private PotencyMG potencyAmount;
    private boolean isPrepaidTax;
    private Note referenceNote;
    private PurchaseOrder.FlowerSourceType flowerSourceType = PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT;
    private List<CompanyAsset> attachments = new ArrayList<>();
    private long expirationDate;
    private String licenseId;
    private String labelInfo;

    private List<BatchBundleItems> bundleItems = new ArrayList<>();

    private String vendorId;

    /* Operations Required*/
    private String externalId;
    private String externalLicense;
    private String roomId; // inventoryId
    @DecimalMin("0.0")
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal actualWeightPerUnit;
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal waste;
    private Boolean unProcessed = false;
    private double moistureLoss;


    public String getTrackHarvestBatch() {
        return trackHarvestBatch;
    }

    public void setTrackHarvestBatch(String trackHarvestBatch) {
        this.trackHarvestBatch = trackHarvestBatch;
    }

    public String getTrackHarvestDate() {
        return trackHarvestDate;
    }

    public void setTrackHarvestDate(String trackHarvestDate) {
        this.trackHarvestDate = trackHarvestDate;
    }

    public Long getFullFilledDate() {
        return fullFilledDate;
    }

    public void setFullFilledDate(Long fullFilledDate) {
        this.fullFilledDate = fullFilledDate;
    }

    public String getTrackPackageLabel() {
        return trackPackageLabel;
    }

    public void setTrackPackageLabel(String trackPackageLabel) {
        this.trackPackageLabel = trackPackageLabel;
    }

    public ProductBatch.TrackTraceSystem getTrackTraceSystem() {
        return trackTraceSystem;
    }

    public void setTrackTraceSystem(ProductBatch.TrackTraceSystem trackTraceSystem) {
        this.trackTraceSystem = trackTraceSystem;
    }

    public boolean isTrackTraceVerified() {
        return trackTraceVerified;
    }

    public void setTrackTraceVerified(boolean trackTraceVerified) {
        this.trackTraceVerified = trackTraceVerified;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public long getSellBy() {
        return sellBy;
    }

    public void setSellBy(long sellBy) {
        this.sellBy = sellBy;
    }

    public long getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(long purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Double getCbd() {
        return cbd;
    }

    public void setCbd(Double cbd) {
        this.cbd = cbd;
    }

    public Double getCbda() {
        return cbda;
    }

    public void setCbda(Double cbda) {
        this.cbda = cbda;
    }

    public Double getCbn() {
        return cbn;
    }

    public void setCbn(Double cbn) {
        this.cbn = cbn;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getThc() {
        return thc;
    }

    public void setThc(Double thc) {
        this.thc = thc;
    }

    public Double getThca() {
        return thca;
    }

    public void setThca(Double thca) {
        this.thca = thca;
    }


    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public BigDecimal getPerUnitExciseTax() {
        return perUnitExciseTax;
    }

    public void setPerUnitExciseTax(BigDecimal perUnitExciseTax) {
        this.perUnitExciseTax = perUnitExciseTax;
    }

    public BigDecimal getTotalExcisetax() {
        return totalExcisetax;
    }

    public void setTotalExcisetax(BigDecimal totalExcisetax) {
        this.totalExcisetax = totalExcisetax;
    }

    public Boolean getVoidStatus() {
        return voidStatus;
    }

    public void setVoidStatus(Boolean voidStatus) {
        this.voidStatus = voidStatus;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Long getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Long receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public ProductBatch.BatchStatus getStatus() {
        return status;
    }

    public void setStatus(ProductBatch.BatchStatus status) {
        this.status = status;
    }

    public Inventory.InventoryType getTargetInventory() {
        return targetInventory;
    }

    public void setTargetInventory(Inventory.InventoryType targetInventory) {
        this.targetInventory = targetInventory;
    }

    public CompanyAsset getBatchQRAsset() {
        return batchQRAsset;
    }

    public void setBatchQRAsset(CompanyAsset batchQRAsset) {
        this.batchQRAsset = batchQRAsset;
    }

    public ProductBatchLabel getProductBatchLabel() {
        return productBatchLabel;
    }

    public void setProductBatchLabel(ProductBatchLabel productBatchLabel) {
        this.productBatchLabel = productBatchLabel;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getMetrcTagId() {
        return metrcTagId;
    }

    public void setMetrcTagId(String metrcTagId) {
        this.metrcTagId = metrcTagId;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public PotencyMG getPotencyAmount() {
        return potencyAmount;
    }

    public void setPotencyAmount(PotencyMG potencyAmount) {
        this.potencyAmount = potencyAmount;
    }

    public boolean isPrepaidTax() {
        return isPrepaidTax;
    }

    public void setPrepaidTax(boolean prepaidTax) {
        isPrepaidTax = prepaidTax;
    }

    public Note getReferenceNote() {
        return referenceNote;
    }

    public void setReferenceNote(Note referenceNote) {
        this.referenceNote = referenceNote;
    }

    public PurchaseOrder.FlowerSourceType getFlowerSourceType() {
        return flowerSourceType;
    }

    public void setFlowerSourceType(PurchaseOrder.FlowerSourceType flowerSourceType) {
        this.flowerSourceType = flowerSourceType;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getLabelInfo() {
        return labelInfo;
    }

    public void setLabelInfo(String labelInfo) {
        this.labelInfo = labelInfo;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public List<BatchBundleItems> getBundleItems() {
        return bundleItems;
    }

    public void setBundleItems(List<BatchBundleItems> bundleItems) {
        this.bundleItems = bundleItems;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getExternalLicense() {
        return externalLicense;
    }

    public void setExternalLicense(String externalLicense) {
        this.externalLicense = externalLicense;
    }

    public BigDecimal getActualWeightPerUnit() {
        return actualWeightPerUnit;
    }

    public void setActualWeightPerUnit(BigDecimal actualWeightPerUnit) {
        this.actualWeightPerUnit = actualWeightPerUnit;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public BigDecimal getWaste() {
        return waste;
    }

    public void setWaste(BigDecimal waste) {
        this.waste = waste;
    }

    public Boolean getUnProcessed() {
        return unProcessed;
    }

    public void setUnProcessed(Boolean unProcessed) {
        this.unProcessed = unProcessed;
    }

    public double getMoistureLoss() {
        return moistureLoss;
    }

    public void setMoistureLoss(double moistureLoss) {
        this.moistureLoss = moistureLoss;
    }

}
