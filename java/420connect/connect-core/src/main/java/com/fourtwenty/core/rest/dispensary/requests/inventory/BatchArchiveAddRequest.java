package com.fourtwenty.core.rest.dispensary.requests.inventory;

public class BatchArchiveAddRequest {
    private String batchId;
    private String toBatchId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getToBatchId() {
        return toBatchId;
    }

    public void setToBatchId(String toBatchId) {
        this.toBatchId = toBatchId;
    }
}
