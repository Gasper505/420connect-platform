package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchBundleItems {
    private String productId;

    private List<BatchItems> batchItems = new ArrayList<>();

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<BatchItems> getBatchItems() {
        return batchItems;
    }

    public void setBatchItems(List<BatchItems> batchItems) {
        this.batchItems = batchItems;
    }

    public static class BatchItems {
        private String batchId;
        private String inventoryId;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal quantity;

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public String getInventoryId() {
            return inventoryId;
        }

        public void setInventoryId(String inventoryId) {
            this.inventoryId = inventoryId;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void setQuantity(BigDecimal quantity) {
            this.quantity = quantity;
        }
    }
}
