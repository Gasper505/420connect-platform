package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchBundleItemsRequest extends BatchBundleItems {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantity = BigDecimal.ZERO;

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
