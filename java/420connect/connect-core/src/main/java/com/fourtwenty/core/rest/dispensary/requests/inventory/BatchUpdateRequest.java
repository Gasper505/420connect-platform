package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 10/12/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchUpdateRequest {
    private Boolean active;
    private Float cost;
    private Double quantity;
    private String sku;
    private Double thc;
    private Double cbn;
    private Double cbd;
    private Double cbda;
    private Double thca;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Double getCbd() {
        return cbd;
    }

    public void setCbd(Double cbd) {
        this.cbd = cbd;
    }

    public Double getCbda() {
        return cbda;
    }

    public void setCbda(Double cbda) {
        this.cbda = cbda;
    }

    public Double getCbn() {
        return cbn;
    }

    public void setCbn(Double cbn) {
        this.cbn = cbn;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getThc() {
        return thc;
    }

    public void setThc(Double thc) {
        this.thc = thc;
    }

    public Double getThca() {
        return thca;
    }

    public void setThca(Double thca) {
        this.thca = thca;
    }
}
