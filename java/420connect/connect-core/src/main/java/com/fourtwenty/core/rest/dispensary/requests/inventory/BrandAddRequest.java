package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;

import java.util.LinkedHashSet;

public class BrandAddRequest implements InternalAllowable {
    private String name;
    private Boolean active;
    private String website;
    private String phoneNo;
    private CompanyAsset brandLogo = new CompanyAsset();
    private LinkedHashSet<String> vendorList = new LinkedHashSet<>();

    private String externalId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public CompanyAsset getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(CompanyAsset brandLogo) {
        this.brandLogo = brandLogo;
    }

    public LinkedHashSet<String> getVendorList() {
        return vendorList;
    }

    public void setVendorList(LinkedHashSet<String> vendorList) {
        this.vendorList = vendorList;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
