package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 6/24/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BulkInventoryTransferRequest {
    private String fromShopId;
    private String toShopId;
    private List<InventoryTransferRequest> transfers = new ArrayList<>();

    public String getFromShopId() {
        return fromShopId;
    }

    public void setFromShopId(String fromShopId) {
        this.fromShopId = fromShopId;
    }

    public String getToShopId() {
        return toShopId;
    }

    public void setToShopId(String toShopId) {
        this.toShopId = toShopId;
    }

    public List<InventoryTransferRequest> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<InventoryTransferRequest> transfers) {
        this.transfers = transfers;
    }
}
