package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.ProductBatch;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BulkProductBatchUpdateRequest {
    public enum BatchUpdateOperationType {
        STATUS, DELETE_BATCHES, UPDATE_BATCHES_TO_ARCHIVE, VOID_STATUS
    }

    private BatchUpdateOperationType operationType;
    private List<String> productBatchIds = new ArrayList<>();
    private ProductBatch.BatchStatus batchStatus;
    private boolean archive = false;
    private boolean voidStatus = false;

    public BatchUpdateOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(BatchUpdateOperationType operationType) {
        this.operationType = operationType;
    }

    public List<String> getProductBatchIds() {
        return productBatchIds;
    }

    public void setProductBatchIds(List<String> productBatchIds) {
        this.productBatchIds = productBatchIds;
    }

    public ProductBatch.BatchStatus getBatchStatus() {
        return batchStatus;
    }

    public void setBatchStatus(ProductBatch.BatchStatus batchStatus) {
        this.batchStatus = batchStatus;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    public boolean isVoidStatus() {
        return voidStatus;
    }

    public void setVoidStatus(boolean voidStatus) {
        this.voidStatus = voidStatus;
    }
}
