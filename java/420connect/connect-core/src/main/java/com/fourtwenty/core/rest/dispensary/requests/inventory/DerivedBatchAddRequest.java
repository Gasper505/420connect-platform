package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.DerivedProductBatchInfo;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DerivedBatchAddRequest {

    @NotEmpty
    private String productId;
    private String vendorId;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal quantity = BigDecimal.ZERO;

    private Map<String, List<DerivedProductBatchInfo>> productMap = new HashMap<>();

    private String sku;
    private String trackPackageLabel;
    private String trackHarvestBatch;
    private String trackHarvestDate;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0.0")
    private BigDecimal costPerUnit;
    private String licenseId;
    private ProductBatch.BatchStatus status = ProductBatch.BatchStatus.READY_FOR_SALE;
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal actualWeightPerUnit;
    private String roomId;
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal waste;
    private boolean newMetrcTag = false;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public Map<String, List<DerivedProductBatchInfo>> getProductMap() {
        return productMap;
    }

    public void setProductMap(Map<String, List<DerivedProductBatchInfo>> productMap) {
        this.productMap = productMap;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getTrackPackageLabel() {
        return trackPackageLabel;
    }

    public void setTrackPackageLabel(String trackPackageLabel) {
        this.trackPackageLabel = trackPackageLabel;
    }

    public String getTrackHarvestBatch() {
        return trackHarvestBatch;
    }

    public void setTrackHarvestBatch(String trackHarvestBatch) {
        this.trackHarvestBatch = trackHarvestBatch;
    }

    public String getTrackHarvestDate() {
        return trackHarvestDate;
    }

    public void setTrackHarvestDate(String trackHarvestDate) {
        this.trackHarvestDate = trackHarvestDate;
    }

    public BigDecimal getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(BigDecimal costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public ProductBatch.BatchStatus getStatus() {
        return status;
    }

    public void setStatus(ProductBatch.BatchStatus status) {
        this.status = status;
    }

    public BigDecimal getActualWeightPerUnit() {
        return actualWeightPerUnit;
    }

    public void setActualWeightPerUnit(BigDecimal actualWeightPerUnit) {
        this.actualWeightPerUnit = actualWeightPerUnit;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public BigDecimal getWaste() {
        return waste;
    }

    public void setWaste(BigDecimal waste) {
        this.waste = waste;
    }

    public boolean isNewMetrcTag() {
        return newMetrcTag;
    }

    public void setNewMetrcTag(boolean newMetrcTag) {
        this.newMetrcTag = newMetrcTag;
    }
}
