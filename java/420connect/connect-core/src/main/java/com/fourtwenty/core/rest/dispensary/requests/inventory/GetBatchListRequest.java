package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 10/12/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetBatchListRequest {

    private List<String> batchIdList = new ArrayList<>();

    public List<String> getBatchIdList() {
        return batchIdList;
    }

    public void setBatchIdList(List<String> batchIdList) {
        this.batchIdList = batchIdList;
    }
}
