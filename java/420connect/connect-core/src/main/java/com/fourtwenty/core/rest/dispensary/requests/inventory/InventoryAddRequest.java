package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.LinkedHashSet;

/**
 * Created by mdo on 7/2/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryAddRequest implements InternalAllowable {
    @NotEmpty
    private String name;
    private boolean active;
    private LinkedHashSet<String> regionIds = new LinkedHashSet<>();
    private String externalId;
    private boolean dryRoom;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedHashSet<String> getRegionIds() {
        return regionIds;
    }

    public void setRegionIds(LinkedHashSet<String> regionIds) {
        this.regionIds = regionIds;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public boolean isDryRoom() {
        return dryRoom;
    }

    public void setDryRoom(boolean dryRoom) {
        this.dryRoom = dryRoom;
    }
}
