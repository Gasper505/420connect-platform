package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 11/18/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryDeleteRequest {
    @NotEmpty
    private String deleteInventoryId;
    @NotEmpty
    private String toInventoryId; // This ID is used to move products from a deleted inventory to new inventory

    public String getDeleteInventoryId() {
        return deleteInventoryId;
    }

    public void setDeleteInventoryId(String deleteInventoryId) {
        this.deleteInventoryId = deleteInventoryId;
    }

    public String getToInventoryId() {
        return toInventoryId;
    }

    public void setToInventoryId(String toInventoryId) {
        this.toInventoryId = toInventoryId;
    }
}
