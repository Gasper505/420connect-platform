package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raja Dushyant Vashishtha
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryHistoryRequest {

    private List<InventoryTransferHistory.TransferStatus> statusList = new ArrayList<>();

    public List<InventoryTransferHistory.TransferStatus> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<InventoryTransferHistory.TransferStatus> statusList) {
        this.statusList = statusList;
    }
}
