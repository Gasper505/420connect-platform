package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;

/**
 * Created by mdo on 6/24/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryTransferRequest {
    @NotEmpty
    private String productId;
    @NotEmpty
    private String fromInventoryId;
    private String fromBatchId;

    @NotEmpty
    private String toInventoryId;
    private String toBatchId;

    private String prepackageItemId;
    private BigDecimal transferAmount;


    public String getFromBatchId() {
        return fromBatchId;
    }

    public void setFromBatchId(String fromBatchId) {
        this.fromBatchId = fromBatchId;
    }

    public String getToBatchId() {
        return toBatchId;
    }

    public void setToBatchId(String toBatchId) {
        this.toBatchId = toBatchId;
    }

    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getFromInventoryId() {
        return fromInventoryId;
    }

    public void setFromInventoryId(String fromInventoryId) {
        this.fromInventoryId = fromInventoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getToInventoryId() {
        return toInventoryId;
    }

    public void setToInventoryId(String toInventoryId) {
        this.toInventoryId = toInventoryId;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }
}
