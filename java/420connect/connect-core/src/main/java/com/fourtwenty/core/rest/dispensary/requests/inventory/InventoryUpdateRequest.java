package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 7/2/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryUpdateRequest extends InventoryAddRequest {
}
