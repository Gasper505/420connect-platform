package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 3/25/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LabelQueryItem {
    public enum LabelQueryType {
        All,
        None,
        Latest
    }

    private String categoryId;
    private String productId;
    private String batchId;
    private String prepackageLineItemId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getPrepackageLineItemId() {
        return prepackageLineItemId;
    }

    public void setPrepackageLineItemId(String prepackageLineItemId) {
        this.prepackageLineItemId = prepackageLineItemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
