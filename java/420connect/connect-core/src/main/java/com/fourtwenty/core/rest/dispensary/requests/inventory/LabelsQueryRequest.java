package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 3/25/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LabelsQueryRequest {
    private List<LabelQueryItem> queries = new ArrayList<>();

    public List<LabelQueryItem> getQueries() {
        return queries;
    }

    public void setQueries(List<LabelQueryItem> queries) {
        this.queries = queries;
    }
}
