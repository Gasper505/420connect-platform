package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 5/24/17.
 */
public class MemberGroupPricesAddRequest {
    private String memberGroupId;
    private String productId;
    private int priority = 0;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0f);
    private List<ProductPriceRange> priceRanges = new ArrayList<>();
    private List<ProductPriceBreak> priceBreaks = new ArrayList<>();


    public List<ProductPriceBreak> getPriceBreaks() {
        return priceBreaks;
    }

    public void setPriceBreaks(List<ProductPriceBreak> priceBreaks) {
        this.priceBreaks = priceBreaks;
    }

    public List<ProductPriceRange> getPriceRanges() {
        return priceRanges;
    }

    public void setPriceRanges(List<ProductPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
