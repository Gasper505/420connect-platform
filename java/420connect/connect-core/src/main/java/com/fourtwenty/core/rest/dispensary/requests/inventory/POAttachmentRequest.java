package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fourtwenty.core.domain.models.company.CompanyAsset;

/**
 * Created by decipher on 6/10/17 5:02 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class POAttachmentRequest {

    private CompanyAsset asset;

    public CompanyAsset getAsset() {
        return asset;
    }

    public void setAsset(CompanyAsset asset) {
        this.asset = asset;
    }
}
