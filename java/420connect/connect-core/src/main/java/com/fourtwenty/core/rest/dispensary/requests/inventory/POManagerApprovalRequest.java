package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by decipher on 5/10/17 7:22 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class POManagerApprovalRequest {

    private CompanyAsset companyAsset;

    @NotEmpty
    private String status;

    private String declineReason;

    private String pin;

    public CompanyAsset getCompanyAsset() {
        return companyAsset;
    }

    public void setCompanyAsset(CompanyAsset companyAsset) {
        this.companyAsset = companyAsset;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
