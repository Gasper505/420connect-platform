package com.fourtwenty.core.rest.dispensary.requests.inventory;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by decipher on 3/10/17 4:33 PM
 * Abhishek Samuel- (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class POProductAddRequest {

    @NotEmpty
    private String productId;
    @NotEmpty
    @DecimalMin("0")
    private BigDecimal requestQuantity = new BigDecimal(0);
    private String notes;
    @DecimalMin("0")
    private BigDecimal totalCost = new BigDecimal(0);

    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);

    @DecimalMin("0")
    private BigDecimal finalTotalCost = new BigDecimal(0);

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getRequestQuantity() {
        return requestQuantity;
    }

    public void setRequestQuantity(BigDecimal requestQuantity) {
        this.requestQuantity = requestQuantity;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getFinalTotalCost() {
        return finalTotalCost;
    }

    public void setFinalTotalCost(BigDecimal finalTotalCost) {
        this.finalTotalCost = finalTotalCost;
    }

}
