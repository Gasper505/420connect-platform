package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.services.testsample.request.POBatchAddRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by decipher on 5/10/17 1:04 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class POProductRequestUpdate {

    @NotEmpty
    private String productRequestId;
    @DecimalMin("0")
    private BigDecimal receivedQuantity;
    private Boolean status;
    private String declineReason;
    private ProductBatch.BatchStatus receiveBatchStatus = ProductBatch.BatchStatus.READY_FOR_SALE;
    private List<POBatchAddRequest> batchAddDetails;

    public String getProductRequestId() {
        return productRequestId;
    }

    public void setProductRequestId(String productRequestId) {
        this.productRequestId = productRequestId;
    }

    public BigDecimal getReceivedQuantity() {
        return receivedQuantity;
    }

    public void setReceivedQuantity(BigDecimal receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public ProductBatch.BatchStatus getReceiveBatchStatus() {
        return receiveBatchStatus;
    }

    public void setReceiveBatchStatus(ProductBatch.BatchStatus receiveBatchStatus) {
        this.receiveBatchStatus = receiveBatchStatus;
    }

    public List<POBatchAddRequest> getBatchAddDetails() {
        return batchAddDetails;
    }

    public void setBatchAddDetails(List<POBatchAddRequest> batchAddDetails) {
        this.batchAddDetails = batchAddDetails;
    }
}
