package com.fourtwenty.core.rest.dispensary.requests.inventory;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by decipher on 6/10/17 12:42 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class POProductUpdateRequest {

    private String poProductRequestId;
    @NotEmpty
    private String productId;
    @NotEmpty
    @DecimalMin("0")
    private BigDecimal requestQuantity;
    private String notes;
    @DecimalMin("0")
    private BigDecimal totalCost;

    public String getPoProductRequestId() {
        return poProductRequestId;
    }

    public void setPoProductRequestId(String poProductRequestId) {
        this.poProductRequestId = poProductRequestId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getRequestQuantity() {
        return requestQuantity;
    }

    public void setRequestQuantity(BigDecimal requestQuantity) {
        this.requestQuantity = requestQuantity;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }
}
