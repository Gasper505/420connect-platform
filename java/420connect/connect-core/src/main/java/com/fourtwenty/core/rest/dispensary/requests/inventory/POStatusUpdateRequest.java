package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;

/**
 * Created by decipher on 5/10/17 6:14 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class POStatusUpdateRequest {

    private PurchaseOrder.PurchaseOrderStatus status = PurchaseOrder.PurchaseOrderStatus.InProgress;
    private String declineMessage;

    public PurchaseOrder.PurchaseOrderStatus getStatus() {
        return status;
    }

    public void setStatus(PurchaseOrder.PurchaseOrderStatus status) {
        this.status = status;
    }

    public String getDeclineMessage() {
        return declineMessage;
    }

    public void setDeclineMessage(String declineMessage) {
        this.declineMessage = declineMessage;
    }
}
