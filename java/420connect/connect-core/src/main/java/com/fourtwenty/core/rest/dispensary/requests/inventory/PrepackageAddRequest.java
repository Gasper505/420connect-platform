package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 3/15/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrepackageAddRequest {
    private boolean customWeight = true;
    private String toleranceId;
    private String name;
    @DecimalMin("0")
    private BigDecimal unitValue = new BigDecimal(0);
    @NotEmpty
    private String productId;
    @DecimalMin("0")
    private BigDecimal price = new BigDecimal(0);
    private boolean active;
    private String sku;

    public boolean isCustomWeight() {
        return customWeight;
    }

    public void setCustomWeight(boolean customWeight) {
        this.customWeight = customWeight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getToleranceId() {
        return toleranceId;
    }

    public void setToleranceId(String toleranceId) {
        this.toleranceId = toleranceId;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
