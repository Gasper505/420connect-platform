package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;

/**
 * Created by mdo on 3/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrepackageItemReduceRequest {
    @NotEmpty
    private String inventoryId;
    @Min(1)
    private int reduceBy = 0;

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public int getReduceBy() {
        return reduceBy;
    }

    public void setReduceBy(int reduceBy) {
        this.reduceBy = reduceBy;
    }
}
