package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.BundleItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.rest.dispensary.requests.BaseAddRequest;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Stephen Schmidt on 10/6/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductAddRequest extends BaseAddRequest implements Importable, InternalAllowable {
//    @NotEmpty Commenting it as it may empty in case of derived product
    private String categoryId;
    private String categoryName;
    private String sku;
//    @NotEmpty Commenting it as it may empty in case of derived product
    private String vendorId;
    private String vendorImportId;
    private String brandId;
    @ApiModelProperty(value = "product name", required = true)
    @NotEmpty
    private String name;
    private String description = "";
    private String flowerType;
    private BigDecimal unitPrice = new BigDecimal(0);
    private String genetics;
    private List<ProductPriceRange> priceRanges = new ArrayList<>();
    private List<CompanyAsset> assets = new ArrayList<>();
    private List<MedicalCondition> medicalConditions = new ArrayList<>();
    private boolean enableMixMatch = false; // MixMatch within same categoryId and same price points
    private BigDecimal lowThreshold = new BigDecimal(0);
    private boolean lowInventoryNotification = false;
    private boolean showInWidget = true;
    private boolean medicinal = true;
    private boolean byGram = true;
    private boolean byPrepackage = false;
    private LinkedHashSet<String> tags = new LinkedHashSet<>();
    private Product.ProductSaleType productSaleType = Product.ProductSaleType.Medicinal;
    private boolean enableExciseTax = Boolean.FALSE;
    private Product.CannabisType cannabisType = Product.CannabisType.DEFAULT;
    private boolean active;
    private boolean priceIncludesExcise = Boolean.FALSE;
    private boolean priceIncludesALExcise = Boolean.TRUE;
    private Product.CustomGramType customGramType = Product.CustomGramType.GRAM;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal customWeight = new BigDecimal(0f);
    private boolean automaticReOrder = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal reOrderLevel = new BigDecimal(0f);
    private Product.ProductType productType = Product.ProductType.REGULAR;
    private List<BundleItem> bundleItems = new ArrayList<>();
    private String externalId;
    private String producerMfg;
    private String producerLicense;
    private Address producerAddress;

    private Set<String> secondaryVendors;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal wmThreshold = BigDecimal.ZERO;


    private Product.WeightPerUnit weightPerUnit = Product.WeightPerUnit.EACH;
    private String toleranceId;

    private boolean sellable = true;
    public boolean isPriceIncludesALExcise() {
        return priceIncludesALExcise;
    }

    public void setPriceIncludesALExcise(boolean priceIncludesALExcise) {
        this.priceIncludesALExcise = priceIncludesALExcise;
    }

    public boolean isPriceIncludesExcise() {
        return priceIncludesExcise;
    }

    public void setPriceIncludesExcise(boolean priceIncludesExcise) {
        this.priceIncludesExcise = priceIncludesExcise;
    }

    public Product.ProductSaleType getProductSaleType() {
        return productSaleType;
    }

    public void setProductSaleType(Product.ProductSaleType type) {
        this.productSaleType = type;
    }

    public boolean isEnableMixMatch() {
        return enableMixMatch;
    }

    public void setEnableMixMatch(boolean enableMixMatch) {
        this.enableMixMatch = enableMixMatch;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(String flowerType) {
        this.flowerType = flowerType;
    }


    public String getGenetics() {
        return genetics;
    }

    public void setGenetics(String genetics) {
        this.genetics = genetics;
    }

    public List<MedicalCondition> getMedicalConditions() {
        return medicalConditions;
    }

    public void setMedicalConditions(List<MedicalCondition> medicalConditions) {
        this.medicalConditions = medicalConditions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductPriceRange> getPriceRanges() {
        return priceRanges;
    }

    public void setPriceRanges(List<ProductPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorImportId() {
        return vendorImportId;
    }

    public void setVendorImportId(String vendorImportId) {
        this.vendorImportId = vendorImportId;
    }

    public BigDecimal getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(BigDecimal lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public boolean isLowInventoryNotification() {
        return lowInventoryNotification;
    }

    public void setLowInventoryNotification(boolean lowInventoryNotification) {
        this.lowInventoryNotification = lowInventoryNotification;
    }

    public boolean isShowInWidget() {
        return showInWidget;
    }

    public void setShowInWidget(boolean showInWidget) {
        this.showInWidget = showInWidget;
    }

    public boolean isMedicinal() {
        return medicinal;
    }

    public void setMedicinal(boolean medicinal) {
        this.medicinal = medicinal;
    }

    public boolean isByGram() {
        return byGram;
    }

    public void setByGram(boolean byGram) {
        this.byGram = byGram;
    }

    public boolean isByPrepackage() {
        return byPrepackage;
    }

    public void setByPrepackage(boolean byPrepackage) {
        this.byPrepackage = byPrepackage;
    }

    public LinkedHashSet<String> getTags() {
        return tags;
    }

    public void setTags(LinkedHashSet<String> tags) {
        this.tags = tags;
    }

    public boolean isEnableExciseTax() {
        return enableExciseTax;
    }

    public void setEnableExciseTax(boolean enableExciseTax) {
        this.enableExciseTax = enableExciseTax;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Product.CannabisType getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(Product.CannabisType cannabisType) {
        this.cannabisType = cannabisType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public BigDecimal getCustomWeight() {
        return customWeight;
    }

    public void setCustomWeight(BigDecimal customWeight) {
        this.customWeight = customWeight;
    }

    public boolean isAutomaticReOrder() {
        return automaticReOrder;
    }

    public void setAutomaticReOrder(boolean automaticReOrder) {
        this.automaticReOrder = automaticReOrder;
    }

    public BigDecimal getReOrderLevel() {
        return reOrderLevel;
    }

    public void setReOrderLevel(BigDecimal reOrderLevel) {
        this.reOrderLevel = reOrderLevel;
    }

    public Product.CustomGramType getCustomGramType() {
        return customGramType;
    }

    public void setCustomGramType(Product.CustomGramType customGramType) {
        this.customGramType = customGramType;
    }

    public Product.ProductType getProductType() {
        return productType;
    }

    public void setProductType(Product.ProductType productType) {
        this.productType = productType;
    }

    public List<BundleItem> getBundleItems() {
        return bundleItems;
    }

    public void setBundleItems(List<BundleItem> bundleItems) {
        this.bundleItems = bundleItems;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getExternalId() {
        return this.externalId;
    }

    public Set<String> getSecondaryVendors() {
        return secondaryVendors;
    }

    public void setSecondaryVendors(Set<String> secondaryVendors) {
        this.secondaryVendors = secondaryVendors;
    }

    public Product.WeightPerUnit getWeightPerUnit() {
        return weightPerUnit;
    }

    public void setWeightPerUnit(Product.WeightPerUnit weightPerUnit) {
        this.weightPerUnit = weightPerUnit;
    }

    public String getToleranceId() {
        return toleranceId;
    }

    public void setToleranceId(String toleranceId) {
        this.toleranceId = toleranceId;
    }

    public String getProducerMfg() {
        return producerMfg;
    }

    public void setProducerMfg(String producerMfg) {
        this.producerMfg = producerMfg;
    }

    public String getProducerLicense() {
        return producerLicense;
    }

    public void setProducerLicense(String producerLicense) {
        this.producerLicense = producerLicense;
    }

    public Address getProducerAddress() {
        return producerAddress;
    }

    public void setProducerAddress(Address producerAddress) {
        this.producerAddress = producerAddress;
    }


    public boolean isSellable() {
        return sellable;
    }

    public void setSellable(boolean sellable) {
        this.sellable = sellable;
    }

    public BigDecimal getWmThreshold() {
        return wmThreshold;
    }

    public void setWmThreshold(BigDecimal wmThreshold) {
        this.wmThreshold = wmThreshold;
    }
}
