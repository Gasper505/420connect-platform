package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.DerivedProductBatchLog;
import com.fourtwenty.core.domain.models.product.ProductBatch;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatchQuantityResult extends ProductBatch {

    private Map<String, BigDecimal> batchQuantityMap = new HashMap<>();
    private DerivedProductBatchLog derivedProductBatchLog;

    public Map<String, BigDecimal> getBatchQuantityMap() {
        return batchQuantityMap;
    }

    public void setBatchQuantityMap(Map<String, BigDecimal> batchQuantityMap) {
        this.batchQuantityMap = batchQuantityMap;
    }

    public DerivedProductBatchLog getDerivedProductBatchLog() {
        return derivedProductBatchLog;
    }

    public void setDerivedProductBatchLog(DerivedProductBatchLog derivedProductBatchLog) {
        this.derivedProductBatchLog = derivedProductBatchLog;
    }
}
