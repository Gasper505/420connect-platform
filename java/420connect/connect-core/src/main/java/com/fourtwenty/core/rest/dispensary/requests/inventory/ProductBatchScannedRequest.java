package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.BarcodeItem;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatchScannedRequest {
    private String mertcId;
    private String barcode;
    private BarcodeItem.BarcodeEntityType entityType;

    public String getMertcId() {
        return mertcId;
    }

    public void setMertcId(String mertcId) {
        this.mertcId = mertcId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public BarcodeItem.BarcodeEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(BarcodeItem.BarcodeEntityType entityType) {
        this.entityType = entityType;
    }
}
