package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBatchStatusRequest {
    private ProductBatch.BatchStatus status;

    public ProductBatch.BatchStatus getStatus() {
        return status;
    }

    public void setStatus(ProductBatch.BatchStatus status) {
        this.status = status;
    }
}
