package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBulkUpdatePriceWeightPrice {

    private String weightToleranceId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getWeightToleranceId() {
        return weightToleranceId;
    }

    public void setWeightToleranceId(String weightToleranceId) {
        this.weightToleranceId = weightToleranceId;
    }
}
