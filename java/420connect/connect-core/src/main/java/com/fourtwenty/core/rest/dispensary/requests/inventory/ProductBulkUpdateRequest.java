package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBulkUpdateRequest {

    public enum ProductsUpdateOperationType {
        STATUS, COPY_PRODUCTS, VENDOR, MIX_MATCH, DELETE_PRODUCTS, WEEDMAP_STATUS, TAX, LOW_INVENTORY_NOTIFICATION, PRICE, UPDATE_CATEGORY, PRODUCT_SALE_TYPE,
        UPDATE_TAG, UPDATE_COMPLEX_TAX, CANNABIS_TYPE, ENABLE_EXCISE_TAX, SHOW_IN_WIDGET, UPDATE_BRAND, FLOWER_TYPE, WEIGHT_PER_UNIT, PRICING_TEMPLATE
    }

    private ProductsUpdateOperationType operationType;
    private boolean active = true;
    private String toShopId;
    private String vendorId;
    private boolean enableMixMatch;
    private boolean enableWeedmap = false;
    private TaxInfo.TaxType taxType = TaxInfo.TaxType.Inherit;
    private TaxInfo.TaxProcessingOrder taxOrder = TaxInfo.TaxProcessingOrder.PostTaxed; // Default is Post Taxed
    private TaxInfo customTaxInfo;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal lowThreshold;
    private boolean lowInventoryNotification = false;
    private List<String> productIds = new ArrayList<>();
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0f);
    private Boolean categoryInGrams;
    private List<ProductBulkUpdatePriceWeightPrice> productBulkUpdatePriceWeightPrice;
    private String categoryId;
    private Product.ProductSaleType productSaleType;
    private LinkedHashSet<String> tags = new LinkedHashSet<>();
    private List<CompoundTaxTable> compoundTaxTables;
    private Product.CannabisType cannabisType;
    private Boolean priceIncludesExcise = Boolean.FALSE;
    private boolean showInWidget = false;
    private String brandId;
    private String flowerType;
    private Product.WeightPerUnit weightPerUnit = Product.WeightPerUnit.EACH;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal customWeight = new BigDecimal(0f);
    private Product.CustomGramType customGramType = Product.CustomGramType.GRAM;
    private String pricingTemplateId;


    public ProductsUpdateOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ProductsUpdateOperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getToShopId() {
        return toShopId;
    }

    public void setToShopId(String toShopId) {
        this.toShopId = toShopId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public boolean isEnableMixMatch() {
        return enableMixMatch;
    }

    public void setEnableMixMatch(boolean enableMixMatch) {
        this.enableMixMatch = enableMixMatch;
    }

    public boolean isEnableWeedmap() {
        return enableWeedmap;
    }

    public void setEnableWeedmap(boolean enableWeedmap) {
        this.enableWeedmap = enableWeedmap;
    }

    public TaxInfo.TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxInfo.TaxType taxType) {
        this.taxType = taxType;
    }

    public TaxInfo.TaxProcessingOrder getTaxOrder() {
        return taxOrder;
    }

    public void setTaxOrder(TaxInfo.TaxProcessingOrder taxOrder) {
        this.taxOrder = taxOrder;
    }

    public TaxInfo getCustomTaxInfo() {
        return customTaxInfo;
    }

    public void setCustomTaxInfo(TaxInfo customTaxInfo) {
        this.customTaxInfo = customTaxInfo;
    }

    public BigDecimal getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(BigDecimal lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public boolean isLowInventoryNotification() {
        return lowInventoryNotification;
    }

    public void setLowInventoryNotification(boolean lowInventoryNotification) {
        this.lowInventoryNotification = lowInventoryNotification;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Boolean isCategoryInGrams() {
        return categoryInGrams;
    }

    public void setCategoryInGrams(Boolean categoryInGrams) {
        this.categoryInGrams = categoryInGrams;
    }

    public List<ProductBulkUpdatePriceWeightPrice> getProductBulkUpdatePriceWeightPrice() {
        return productBulkUpdatePriceWeightPrice;
    }

    public void setProductBulkUpdatePriceWeightPrice(List<ProductBulkUpdatePriceWeightPrice> productBulkUpdatePriceWeightPrice) {
        this.productBulkUpdatePriceWeightPrice = productBulkUpdatePriceWeightPrice;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Product.ProductSaleType getProductSaleType() {
        return productSaleType;
    }

    public void setProductSaleType(Product.ProductSaleType productSaleType) {
        this.productSaleType = productSaleType;
    }

    public LinkedHashSet<String> getTags() {
        return tags;
    }

    public void setTags(LinkedHashSet<String> tags) {
        this.tags = tags;
    }

    public List<CompoundTaxTable> getCompoundTaxTables() {
        return compoundTaxTables;
    }

    public void setCompoundTaxTables(List<CompoundTaxTable> compoundTaxTables) {
        this.compoundTaxTables = compoundTaxTables;
    }

    public Product.CannabisType getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(Product.CannabisType cannabisType) {
        this.cannabisType = cannabisType;
    }

    public Boolean getPriceIncludesExcise() {
        return priceIncludesExcise;
    }

    public void setPriceIncludesExcise(Boolean priceIncludesExcise) {
        this.priceIncludesExcise = priceIncludesExcise;
    }

    public boolean isShowInWidget() {
        return showInWidget;
    }

    public void setShowInWidget(boolean showInWidget) {
        this.showInWidget = showInWidget;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public Boolean getCategoryInGrams() {
        return categoryInGrams;
    }

    public String getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(String flowerType) {
        this.flowerType = flowerType;
    }

    public Product.WeightPerUnit getWeightPerUnit() {
        return weightPerUnit;
    }

    public void setWeightPerUnit(Product.WeightPerUnit weightPerUnit) {
        this.weightPerUnit = weightPerUnit;
    }

    public BigDecimal getCustomWeight() {
        return customWeight;
    }

    public void setCustomWeight(BigDecimal customWeight) {
        this.customWeight = customWeight;
    }

    public Product.CustomGramType getCustomGramType() {
        return customGramType;
    }

    public void setCustomGramType(Product.CustomGramType customGramType) {
        this.customGramType = customGramType;
    }

    public String getPricingTemplateId() {
        return pricingTemplateId;
    }

    public void setPricingTemplateId(String pricingTemplateId) {
        this.pricingTemplateId = pricingTemplateId;
    }
}
