package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 10/12/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategoryAddRequest implements InternalAllowable {
    @NotEmpty
    private String name;
    private boolean cannabis;
    private ProductCategory.UnitType unitType = ProductCategory.UnitType.units;
    private int priority = 0;
    private boolean active = true;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal lowThreshold;
    private String wmCategory;
    private String externalId;


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public ProductCategory.UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(ProductCategory.UnitType unitType) {
        this.unitType = unitType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCannabis() {
        return cannabis;
    }

    public void setCannabis(boolean cannabis) {
        this.cannabis = cannabis;
    }

    public BigDecimal getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(BigDecimal lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public String getWmCategory() {
        return wmCategory;
    }

    public void setWmCategory(String wmCategory) {
        this.wmCategory = wmCategory;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }
    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
