package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 4/17/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategoryUpdateOrderRequest {
    private List<ProductCategoryOrderItem> categoryOrders = new ArrayList<>();

    public List<ProductCategoryOrderItem> getCategoryOrders() {
        return categoryOrders;
    }

    public void setCategoryOrders(List<ProductCategoryOrderItem> categoryOrders) {
        this.categoryOrders = categoryOrders;
    }
}
