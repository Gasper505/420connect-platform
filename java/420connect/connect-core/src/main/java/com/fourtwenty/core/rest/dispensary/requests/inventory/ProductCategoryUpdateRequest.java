package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 10/12/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategoryUpdateRequest extends ProductCategoryAddRequest {
}
