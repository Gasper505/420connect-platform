package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class ProductLossRequest {

    private String productId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantity;
    private String batchId;
    private String prepackageItemId;
    private ReconciliationHistoryList.ReconciliationReason reconciliationReason;
    private String note;
    private String batchSku;


    public String getPrepackageItemId() {
        return prepackageItemId;
    }

    public void setPrepackageItemId(String prepackageItemId) {
        this.prepackageItemId = prepackageItemId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public ReconciliationHistoryList.ReconciliationReason getReconciliationReason() {
        return reconciliationReason;
    }

    public void setReconciliationReason(ReconciliationHistoryList.ReconciliationReason reconciliationReason) {
        this.reconciliationReason = reconciliationReason;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }
}
