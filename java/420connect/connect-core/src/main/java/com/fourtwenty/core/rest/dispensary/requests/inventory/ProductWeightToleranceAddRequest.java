package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalWeightSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWeightToleranceAddRequest {
    @NotEmpty
    private String name;
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal startWeight = new BigDecimal(0);
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal endWeight = new BigDecimal(0);
    private int priority;
    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal unitValue;

    @DecimalMin("0")
    @JsonSerialize(using = BigDecimalWeightSerializer.class)
    private BigDecimal weightValue;
    private boolean enabled = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getStartWeight() {
        return startWeight;
    }

    public void setStartWeight(BigDecimal startWeight) {
        this.startWeight = startWeight;
    }

    public BigDecimal getEndWeight() {
        return endWeight;
    }

    public void setEndWeight(BigDecimal endWeight) {
        this.endWeight = endWeight;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(BigDecimal unitValue) {
        this.unitValue = unitValue;
    }

    public BigDecimal getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(BigDecimal weightValue) {
        this.weightValue = weightValue;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
