package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.AdjustmentInfo;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by decipher on 3/10/17 4:11 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseOrderAddRequest {

    private String vendorId;
    private String notes;
    @NotEmpty
    private String poPaymentTerms;
    @NotEmpty
    private String paymentType;
    @NotEmpty
    private List<POProductAddRequest> poProductAddRequestList = new ArrayList<>();
    private long customTermDate;
    private String reference;
    private Vendor.ArmsLengthType transactionType = Vendor.ArmsLengthType.ARMS_LENGTH;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal fees = new BigDecimal(0);


    private PurchaseOrder.CustomerType customerType = PurchaseOrder.CustomerType.VENDOR;

    private String deliveryAddress;
    private Long deliveryTime;
    private long deliveryDate;
    private String termsAndCondition;
    private PurchaseOrder.FlowerSourceType flowerSourceType = PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT;

    private long purchaseOrderDate;
    private String licenseId;
    private List<AdjustmentInfo> adjustmentInfoList = new ArrayList<>();

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPoPaymentTerms() {
        return poPaymentTerms;
    }

    public void setPoPaymentTerms(String poPaymentTerms) {
        this.poPaymentTerms = poPaymentTerms;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public List<POProductAddRequest> getPoProductAddRequestList() {
        return poProductAddRequestList;
    }

    public void setPoProductAddRequestList(List<POProductAddRequest> poProductAddRequestList) {
        this.poProductAddRequestList = poProductAddRequestList;
    }

    public long getCustomTermDate() {
        return customTermDate;
    }

    public void setCustomTermDate(long customTermDate) {
        this.customTermDate = customTermDate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Vendor.ArmsLengthType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Vendor.ArmsLengthType transactionType) {
        this.transactionType = transactionType;
    }

    public PurchaseOrder.CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(PurchaseOrder.CustomerType customerType) {
        this.customerType = customerType;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

    public PurchaseOrder.FlowerSourceType getFlowerSourceType() {
        return flowerSourceType;
    }

    public void setFlowerSourceType(PurchaseOrder.FlowerSourceType flowerSourceType) {
        this.flowerSourceType = flowerSourceType;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getFees() {
        return fees;
    }

    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    public long getPurchaseOrderDate() {
        return purchaseOrderDate;
    }

    public void setPurchaseOrderDate(long purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public List<AdjustmentInfo> getAdjustmentInfoList() {
        return adjustmentInfoList;
    }

    public void setAdjustmentInfoList(List<AdjustmentInfo> adjustmentInfoList) {
        this.adjustmentInfoList = adjustmentInfoList;
    }
}
