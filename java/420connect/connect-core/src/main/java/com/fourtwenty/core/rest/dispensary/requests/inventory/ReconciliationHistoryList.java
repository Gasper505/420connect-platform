package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ReconciliationHistoryList {

    public enum ReconciliationReason {
        AUDIT("Incorrect Quantity","Entry Error"),
        DAMAGED("Damage (BCC)","Waste"),
        WASTE_DISPLAY("Waste (Unusable Product)", "Waste"),
        WASTE_EXPIRED("Waste (Unusable Product)","Waste"),
        WASTE_RETURN("Waste (Unusable Product)","Waste"),
        WASTE_DISPOSAL("Waste (Unusable Product)","Waste"),
        DISPLAY_SAMPLE("Display Sample","In-House Quality Control"),
        FREE_MEDICINAL_CANNABIS_GOODS("Free Medicinal Cannabis Goods","In-House Quality Control"),
        INCORRECT_QUANTITY("Incorrect Quantity","Entry Error"),
        MANDATED_DESTRUCTION("Mandated Destruction","Waste"),
        OVERSOLD("Oversold","Over/Under Sold"),
        PUBLIC_SAFETY_RECALL("Public Safety/Recall","Waste"),
        SCALE_VARIANCE("Scale Variance","Scale Variance"),
        THEFT("Theft","Theft"),
        UNDERSOLD("Undersold","Over/Under Sold"),
        VOLUNTARY_SURRENDER("Voluntary Surrender","Waste"),

        STORE_TRANSFER("Incorrect Quantity","During Transfer"),
        PO_ERROR("Incorrect Quantity","Entry Error"),
        SAMPLES("Display Sample","In-House Quality Control"),
        RETURN_TO_VENDOR("Incorrect Quantity","During Transfer"),
        OTHER("Incorrect Quantity","Entry Error");

        ReconciliationReason(String metrc, String akReason) {
            this.metrcReason = metrc;
            this.akMetrcReason = akReason;
        }
        public String metrcReason;
        public String akMetrcReason;
    }

    private String productId;
    private String productName;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal oldQuantity;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal newQuantity;
    private String prePackageItemId;
    private String terminalId;
    private String inventoryId;
    private boolean reportLoss = false;

    private ReconciliationHistoryList.ReconciliationReason reconciliationReason;
    private Note note;
    private String batchId;
    private String batchSku;
    private String brandName;
    private List<InventoryOperation> operations = new ArrayList<>();

    public List<InventoryOperation> getOperations() {
        return operations;
    }

    public void setOperations(List<InventoryOperation> operations) {
        this.operations = operations;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getOldQuantity() {
        return oldQuantity;
    }

    public void setOldQuantity(BigDecimal oldQuantity) {
        this.oldQuantity = oldQuantity;
    }

    public BigDecimal getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(BigDecimal newQuantity) {
        this.newQuantity = newQuantity;
    }

    public String getPrePackageItemId() {
        return prePackageItemId;
    }

    public void setPrePackageItemId(String prePackageItemId) {
        this.prePackageItemId = prePackageItemId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public boolean isReportLoss() {
        return reportLoss;
    }

    public void setReportLoss(boolean reportLoss) {
        this.reportLoss = reportLoss;
    }

    public ReconciliationReason getReconciliationReason() {
        return reconciliationReason;
    }

    public void setReconciliationReason(ReconciliationReason reconciliationReason) {
        this.reconciliationReason = reconciliationReason;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
