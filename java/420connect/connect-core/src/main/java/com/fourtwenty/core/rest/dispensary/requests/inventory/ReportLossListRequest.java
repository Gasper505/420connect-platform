package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportLossListRequest {

    private String terminalId;
    private String inventoryId;
    private boolean reportLoss = false;
    private String note;
    private List<ProductLossRequest> productLossRequests;
    private boolean batchReconcile = Boolean.FALSE;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public boolean isReportLoss() {
        return reportLoss;
    }

    public void setReportLoss(boolean reportLoss) {
        this.reportLoss = reportLoss;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<ProductLossRequest> getProductLossRequests() {
        return productLossRequests;
    }

    public void setProductLossRequests(List<ProductLossRequest> productLossRequests) {
        this.productLossRequests = productLossRequests;
    }

    public boolean isBatchReconcile() {
        return batchReconcile;
    }

    public void setBatchReconcile(boolean batchReconcile) {
        this.batchReconcile = batchReconcile;
    }

}
