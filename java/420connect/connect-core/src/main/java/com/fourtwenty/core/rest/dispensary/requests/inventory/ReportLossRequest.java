package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * Created by mdo on 7/21/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportLossRequest {
    @NotEmpty
    private String productId;
    @DecimalMin("0.0")
    private BigDecimal quantity;
    @NotEmpty
    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
