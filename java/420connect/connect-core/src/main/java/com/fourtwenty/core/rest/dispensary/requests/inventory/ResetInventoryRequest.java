package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gaurav Saini on 07/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetInventoryRequest {

    private List<String> productId = new ArrayList<>();
    @NotEmpty
    private String fromInventoryId;
    @NotEmpty
    private String toInventoryId;

    public List<String> getProductId() {
        return productId;
    }

    public void setProductId(List<String> productId) {
        this.productId = productId;
    }

    public String getFromInventoryId() {
        return fromInventoryId;
    }

    public void setFromInventoryId(String fromInventoryId) {
        this.fromInventoryId = fromInventoryId;
    }

    public String getToInventoryId() {
        return toInventoryId;
    }

    public void setToInventoryId(String toInventoryId) {
        this.toInventoryId = toInventoryId;
    }
}
