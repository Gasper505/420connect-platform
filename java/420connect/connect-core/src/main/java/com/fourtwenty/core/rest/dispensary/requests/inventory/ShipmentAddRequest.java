package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.purchaseorder.ShipperInformation;
import com.fourtwenty.core.domain.models.purchaseorder.ProductMetrcInfo;
import com.fourtwenty.core.domain.models.purchaseorder.DistributorInformation;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentAddRequest {

    private String shippingManifestId;
    private List<ProductMetrcInfo> productMetrcInfo = new ArrayList<>();
    private ShipperInformation shipperInformation;
    private String shippingManifestNo;
    private long deliveryDate;
    private long deliveryTime;
    private List<Note> notes;
    private Long shippedDateTime;
    private Long estimatedArrival;
    private DistributorInformation distributorInformation;
    private Shipment.ShipmentStatus shipmentStatus = Shipment.ShipmentStatus.Incoming;


    public String getShippingManifestId() {
        return shippingManifestId;
    }

    public void setShippingManifestId(String shippingManifestId) {
        this.shippingManifestId = shippingManifestId;
    }

    public List<ProductMetrcInfo> getProductMetrcInfo() {
        return productMetrcInfo;
    }

    public void setProductMetrcInfo(List<ProductMetrcInfo> productMetrcInfo) {
        this.productMetrcInfo = productMetrcInfo;
    }

    public ShipperInformation getShipperInformation() {
        return shipperInformation;
    }

    public void setShipperInformation(ShipperInformation shipperInformation) {
        this.shipperInformation = shipperInformation;
    }

    public String getShippingManifestNo() {
        return shippingManifestNo;
    }

    public void setShippingManifestNo(String shippingManifestNo) {
        this.shippingManifestNo = shippingManifestNo;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Long getShippedDateTime() {
        return shippedDateTime;
    }

    public void setShippedDateTime(Long shippedDateTime) {
        this.shippedDateTime = shippedDateTime;
    }

    public Long getEstimatedArrival() {
        return estimatedArrival;
    }

    public void setEstimatedArrival(Long estimatedArrival) {
        this.estimatedArrival = estimatedArrival;
    }

    public DistributorInformation getDistributorInformation() {
        return distributorInformation;
    }

    public void setDistributorInformation(DistributorInformation distributorInformation) {
        this.distributorInformation = distributorInformation;
    }

    public Shipment.ShipmentStatus getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(Shipment.ShipmentStatus shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }
}