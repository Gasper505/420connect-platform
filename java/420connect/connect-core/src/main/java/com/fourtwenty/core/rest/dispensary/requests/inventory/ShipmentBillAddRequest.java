package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.rest.dispensary.requests.BaseAddRequest;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentBillAddRequest extends BaseAddRequest implements Importable {
    @NotEmpty
    private String purchaseOrderId;
    private List<ShipmentBillPayment> paymentHistory;
    private String vendorId;
    private String createdBy;
    private List<CompanyAsset> assets;
    private List<POProductRequest> poProductRequest;
    private Long receivedDate;
    private String deliveredBy;
    private String receivedByEmployeeId;
    private String createdByEmployeeId;
    private String completedByEmployeeId;
    private Long completedDate;
    private String paymentStatus;

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public List<ShipmentBillPayment> getPaymentHistory() {
        return paymentHistory;
    }

    public void setPaymentHistory(List<ShipmentBillPayment> paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public List<POProductRequest> getPoProductRequest() {
        return poProductRequest;
    }

    public void setPoProductRequest(List<POProductRequest> poProductRequest) {
        this.poProductRequest = poProductRequest;
    }

    public Long getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Long receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public String getReceivedByEmployeeId() {
        return receivedByEmployeeId;
    }

    public void setReceivedByEmployeeId(String receivedByEmployeeId) {
        this.receivedByEmployeeId = receivedByEmployeeId;
    }

    public String getCreatedByEmployeeId() {
        return createdByEmployeeId;
    }

    public void setCreatedByEmployeeId(String createdByEmployeeId) {
        this.createdByEmployeeId = createdByEmployeeId;
    }

    public String getCompletedByEmployeeId() {
        return completedByEmployeeId;
    }

    public void setCompletedByEmployeeId(String completedByEmployeeId) {
        this.completedByEmployeeId = completedByEmployeeId;
    }

    public Long getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Long completedDate) {
        this.completedDate = completedDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
