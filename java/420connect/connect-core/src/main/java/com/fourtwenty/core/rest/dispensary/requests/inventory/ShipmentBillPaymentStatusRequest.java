package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBillPayment;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentBillPaymentStatusRequest {

    private BigDecimal amountPaid;
    private long paidDate;
    private String notes;
    private ShipmentBillPayment.PaymentType paymentType;
    private String id;
    private String referenceNo;

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public long getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(long paidDate) {
        this.paidDate = paidDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ShipmentBillPayment.PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(ShipmentBillPayment.PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
