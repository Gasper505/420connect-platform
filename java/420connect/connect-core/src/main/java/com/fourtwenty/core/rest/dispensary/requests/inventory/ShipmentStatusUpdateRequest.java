package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.purchaseorder.Shipment;;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentStatusUpdateRequest {

    private Shipment.ShipmentStatus status = Shipment.ShipmentStatus.Incoming;
    private String declineMessage;

    public Shipment.ShipmentStatus getStatus() {
        return status;
    }

    public void setStatus(Shipment.ShipmentStatus status) {
        this.status = status;
    }

    public String getDeclineMessage() {
        return declineMessage;
    }

    public void setDeclineMessage(String declineMessage) {
        this.declineMessage = declineMessage;
    }
}
