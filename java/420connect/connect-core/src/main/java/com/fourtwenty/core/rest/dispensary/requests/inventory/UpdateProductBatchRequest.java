package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fourtwenty.core.domain.models.product.DerivedProductBatchInfo;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.ProductBatch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpdateProductBatchRequest extends ProductBatch {

    private Inventory.InventoryType targetInventory = Inventory.InventoryType.Storage;

    private Map<String, List<DerivedProductBatchInfo>> productMap = new HashMap<>();

    public Inventory.InventoryType getTargetInventory() {
        return targetInventory;
    }

    public void setTargetInventory(Inventory.InventoryType targetInventory) {
        this.targetInventory = targetInventory;
    }

    public Map<String, List<DerivedProductBatchInfo>> getProductMap() {
        return productMap;
    }

    public void setProductMap(Map<String, List<DerivedProductBatchInfo>> productMap) {
        this.productMap = productMap;
    }

}
