package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.interfaces.InternalAllowable;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.rest.dispensary.requests.BaseAddRequest;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by mdo on 10/12/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorAddRequest extends BaseAddRequest implements Importable, InternalAllowable {
    private Boolean active;
    @NotEmpty
    private String name;
    private Boolean medical;
    private String phone;
    private String email;
    private String website;
    private String fax;
    private Address address;
    private String description;
    private String contactFirstName;
    private String contactLastName;
    private String licenseNumber;
    private String firstName;
    private String lastName;
    private List<CompanyAsset> assets = new ArrayList<>();
    private long licenseExpirationDate;
    private Vendor.ArmsLengthType armsLengthType;
    private LinkedHashSet<String> brands = new LinkedHashSet<>();

    private Vendor.CompanyType companyType;
    private List<Address> additionalAddressList = new ArrayList<>();
    private Vendor.LicenceType licenceType;
    private boolean relatedEntity;
    private String mobileNumber;
    private List<Note> notes = new ArrayList<>();
    private Vendor.VendorType vendorType = Vendor.VendorType.VENDOR;
    private String dbaName;
    private List<CompanyLicense> companyLicenses = new ArrayList<>();
    private String employeeId;
    private String accountOwnerId;

    private String defaultPaymentTerm;
    private String externalId;

    private String salesPerson;
    private String contactPerson;
    private String defaultContactId;
    private String connectedShop;

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getMedical() {
        return medical;
    }

    public void setMedical(Boolean medical) {
        this.medical = medical;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public long getLicenseExpirationDate() {
        return licenseExpirationDate;
    }

    public void setLicenseExpirationDate(long licenseExpirationDate) {
        this.licenseExpirationDate = licenseExpirationDate;
    }

    public Vendor.ArmsLengthType getArmsLengthType() {
        return armsLengthType;
    }

    public void setArmsLengthType(Vendor.ArmsLengthType armsLengthType) {
        this.armsLengthType = armsLengthType;
    }

    public LinkedHashSet<String> getBrands() {
        return brands;
    }

    public void setBrands(LinkedHashSet<String> brands) {
        this.brands = brands;
    }

    public Vendor.CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Vendor.CompanyType companyType) {
        this.companyType = companyType;
    }

    public List<Address> getAdditionalAddressList() {
        return additionalAddressList;
    }

    public void setAdditionalAddressList(List<Address> additionalAddressList) {
        this.additionalAddressList = additionalAddressList;
    }

    public Vendor.LicenceType getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(Vendor.LicenceType licenceType) {
        this.licenceType = licenceType;
    }

    public boolean isRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(boolean relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Vendor.VendorType getVendorType() {
        return vendorType;
    }

    public void setVendorType(Vendor.VendorType vendorType) {
        this.vendorType = vendorType;
    }

    public String getDbaName() {
        return dbaName;
    }

    public void setDbaName(String dbaName) {
        this.dbaName = dbaName;
    }

    public List<CompanyLicense> getCompanyLicenses() {
        return companyLicenses;
    }

    public void setCompanyLicenses(List<CompanyLicense> companyLicenses) {
        this.companyLicenses = companyLicenses;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getAccountOwnerId() {
        return accountOwnerId;
    }

    public void setAccountOwnerId(String accountOwnerId) {
        this.accountOwnerId = accountOwnerId;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getDefaultPaymentTerm() {
        return defaultPaymentTerm;
    }

    public void setDefaultPaymentTerm(String defaultPaymentTerm) {
        this.defaultPaymentTerm = defaultPaymentTerm;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getDefaultContactId() {
        return defaultContactId;
    }

    public void setDefaultContactId(String defaultContactId) {
        this.defaultContactId = defaultContactId;
    }

    public String getConnectedShop() {
        return connectedShop;
    }

    public void setConnectedShop(String connectedShop) {
        this.connectedShop = connectedShop;
    }
}
