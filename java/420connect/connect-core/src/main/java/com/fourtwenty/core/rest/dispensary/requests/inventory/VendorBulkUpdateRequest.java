package com.fourtwenty.core.rest.dispensary.requests.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Vendor;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorBulkUpdateRequest {

    public enum VendorUpdateOperationType {
        STATUS, ENABLE_BACK_ORDER, TAX_TYPE, BRAND
    }

    private VendorUpdateOperationType operationType;
    private List<String> vendorId = new ArrayList<>();
    private boolean active = false;
    private boolean backOrderEnabled = false;
    private Vendor.ArmsLengthType armsLengthType;
    private LinkedHashSet<String> brands = new LinkedHashSet<>();


    public VendorUpdateOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(VendorUpdateOperationType operationType) {
        this.operationType = operationType;
    }

    public List<String> getVendorId() {
        return vendorId;
    }

    public void setVendorId(List<String> vendorId) {
        this.vendorId = vendorId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isBackOrderEnabled() {
        return backOrderEnabled;
    }

    public void setBackOrderEnabled(boolean backOrderEnabled) {
        this.backOrderEnabled = backOrderEnabled;
    }

    public Vendor.ArmsLengthType getArmsLengthType() {
        return armsLengthType;
    }

    public void setArmsLengthType(Vendor.ArmsLengthType armsLengthType) {
        this.armsLengthType = armsLengthType;
    }

    public LinkedHashSet<String> getBrands() {
        return brands;
    }

    public void setBrands(LinkedHashSet<String> brands) {
        this.brands = brands;
    }

}
