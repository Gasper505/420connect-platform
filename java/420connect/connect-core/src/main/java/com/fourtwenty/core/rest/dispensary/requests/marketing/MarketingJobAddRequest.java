package com.fourtwenty.core.rest.dispensary.requests.marketing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 7/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketingJobAddRequest {
    private String message; // can be html if email
    private MarketingJob.MarketingJobType jobType = MarketingJob.MarketingJobType.SMS;
    private MarketingJob.MarketingRecipientType recipientType = MarketingJob.MarketingRecipientType.AllMembers;
    private List<String> memberGroupIds = new ArrayList<>();
    private Long reqSendDate;
    private int inactiveDays = 0;
    private String categoryId;
    private long startDate;
    private long endDate;
    private String vendorId;
    private int noOfVisits;
    private BigDecimal amountDollars;
    private MarketingJob.MarketingType type;
    private int dayBefore;
    private boolean enable;
    private CompanyAsset memberAsset;
    private Set<String> memberTags;

    public int getInactiveDays() {
        return inactiveDays;
    }

    public void setInactiveDays(int inactiveDays) {
        this.inactiveDays = inactiveDays;
    }

    public MarketingJob.MarketingJobType getJobType() {
        return jobType;
    }

    public void setJobType(MarketingJob.MarketingJobType jobType) {
        this.jobType = jobType;
    }

    public List<String> getMemberGroupIds() {
        return memberGroupIds;
    }

    public void setMemberGroupIds(List<String> memberGroupIds) {
        this.memberGroupIds = memberGroupIds;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MarketingJob.MarketingRecipientType getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(MarketingJob.MarketingRecipientType recipientType) {
        this.recipientType = recipientType;
    }

    public Long getReqSendDate() {
        return reqSendDate;
    }

    public void setReqSendDate(Long reqSendDate) {
        this.reqSendDate = reqSendDate;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public int getNoOfVisits() {
        return noOfVisits;
    }

    public void setNoOfVisits(int noOfVisits) {
        this.noOfVisits = noOfVisits;
    }


    public BigDecimal getAmountDollars() {
        return amountDollars;
    }

    public void setAmountDollars(BigDecimal amountDollars) {
        this.amountDollars = amountDollars;
    }

    public MarketingJob.MarketingType getType() {
        return type;
    }

    public void setType(MarketingJob.MarketingType type) {
        this.type = type;
    }

    public int getDayBefore() {
        return dayBefore;
    }

    public void setDayBefore(int dayBefore) {
        this.dayBefore = dayBefore;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public CompanyAsset getMemberAsset() {
        return memberAsset;
    }

    public void setMemberAsset(CompanyAsset memberAsset) {
        this.memberAsset = memberAsset;
    }

    public Set<String> getMemberTags() {
        return memberTags;
    }

    public void setMemberTags(Set<String> memberTags) {
        this.memberTags = memberTags;
    }
}
