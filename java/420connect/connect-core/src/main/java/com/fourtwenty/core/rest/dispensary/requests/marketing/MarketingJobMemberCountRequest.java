package com.fourtwenty.core.rest.dispensary.requests.marketing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketingJobMemberCountRequest {

    private MarketingJob.MarketingRecipientType recipientType = MarketingJob.MarketingRecipientType.AllMembers;
    private int inactiveDays = 0;
    private List<String> memberGroupIds = new ArrayList<>();
    private String vendorId;
    private String categoryId;
    private long startDate;
    private long endDate;
    private int noOfVisits;
    private BigDecimal amountDollars;
    private int dayBefore;
    private CompanyAsset memberAsset;
    private Set<String> memberTags;

    public MarketingJob.MarketingRecipientType getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(MarketingJob.MarketingRecipientType recipientType) {
        this.recipientType = recipientType;
    }

    public int getInactiveDays() {
        return inactiveDays;
    }

    public void setInactiveDays(int inactiveDays) {
        this.inactiveDays = inactiveDays;
    }

    public List<String> getMemberGroupIds() {
        return memberGroupIds;
    }

    public void setMemberGroupIds(List<String> memberGroupIds) {
        this.memberGroupIds = memberGroupIds;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public int getNoOfVisits() {
        return noOfVisits;
    }

    public void setNoOfVisits(int noOfVisits) {
        this.noOfVisits = noOfVisits;
    }

    public BigDecimal getAmountDollars() {
        return amountDollars;
    }

    public void setAmountDollars(BigDecimal amountDollars) {
        this.amountDollars = amountDollars;
    }

    public int getDayBefore() {
        return dayBefore;
    }

    public void setDayBefore(int dayBefore) {
        this.dayBefore = dayBefore;
    }

    public CompanyAsset getMemberAsset() {
        return memberAsset;
    }

    public void setMemberAsset(CompanyAsset memberAsset) {
        this.memberAsset = memberAsset;
    }

    public Set<String> getMemberTags() {
        return memberTags;
    }

    public void setMemberTags(Set<String> memberTags) {
        this.memberTags = memberTags;
    }
}
