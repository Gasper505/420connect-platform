package com.fourtwenty.core.rest.dispensary.requests.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationInfoAddRequest {
    private NotificationInfo.NotificationType notificationType;
    private long lastSent = 0;
    private boolean active;
    private String shopId;
    private String defaultEmail;
    private String defaultNumber;

    public NotificationInfo.NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationInfo.NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public long getLastSent() {
        return lastSent;
    }

    public void setLastSent(long lastSent) {
        this.lastSent = lastSent;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getDefaultEmail() {
        return defaultEmail;
    }

    public void setDefaultEmail(String defaultEmail) {
        this.defaultEmail = defaultEmail;
    }

    public String getDefaultNumber() {
        return defaultNumber;
    }

    public void setDefaultNumber(String defaultNumber) {
        this.defaultNumber = defaultNumber;
    }
}
