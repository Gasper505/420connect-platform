package com.fourtwenty.core.rest.dispensary.requests.partners;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerCompanyContactRequest extends CompanyContact {
    @NotEmpty
    private String employeeId;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
