package com.fourtwenty.core.rest.dispensary.requests.partners;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.queues.EmployeeReassignRequest;
import org.hibernate.validator.constraints.NotEmpty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerEmployeeReassignRequest extends EmployeeReassignRequest {
    @NotEmpty
    private String currentTerminalId;
    @NotEmpty
    private String currentEmployeeId;

    public String getCurrentTerminalId() {
        return currentTerminalId;
    }

    public void setCurrentTerminalId(String currentTerminalId) {
        this.currentTerminalId = currentTerminalId;
    }

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }
}
