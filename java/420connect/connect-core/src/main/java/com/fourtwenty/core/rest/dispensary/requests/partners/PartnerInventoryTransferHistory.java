package com.fourtwenty.core.rest.dispensary.requests.partners;

import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import org.hibernate.validator.constraints.NotEmpty;

public class PartnerInventoryTransferHistory extends InventoryTransferHistory {
    @NotEmpty
    private String currentEmployeeId;

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }
}
