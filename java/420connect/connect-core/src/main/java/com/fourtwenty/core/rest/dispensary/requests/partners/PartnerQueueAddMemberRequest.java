package com.fourtwenty.core.rest.dispensary.requests.partners;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberRequest;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerQueueAddMemberRequest extends QueueAddMemberRequest {
    @NotEmpty
    private String terminalId;
    @NotEmpty
    private String employeeId;

    @Override
    public String getTerminalId() {
        return terminalId;
    }

    @Override
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    @Override
    public String getEmployeeId() {
        return employeeId;
    }

    @Override
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
