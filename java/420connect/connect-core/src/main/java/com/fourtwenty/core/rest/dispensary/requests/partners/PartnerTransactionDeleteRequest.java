package com.fourtwenty.core.rest.dispensary.requests.partners;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.queues.TransactionDeleteRequest;
import org.hibernate.validator.constraints.NotEmpty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerTransactionDeleteRequest extends TransactionDeleteRequest {

    @NotEmpty
    private String terminalId;
    @NotEmpty
    private String employeeId;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }


    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
