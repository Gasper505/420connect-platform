package com.fourtwenty.core.rest.dispensary.requests.partners;

import com.fourtwenty.core.domain.models.product.Vendor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerVendorUpdateRequest extends Vendor {

    private String employeeId;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
