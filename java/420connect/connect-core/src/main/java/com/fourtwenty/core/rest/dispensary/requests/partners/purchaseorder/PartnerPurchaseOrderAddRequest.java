package com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder;

import com.fourtwenty.core.rest.dispensary.requests.inventory.PurchaseOrderAddRequest;
import org.hibernate.validator.constraints.NotEmpty;

public class PartnerPurchaseOrderAddRequest extends PurchaseOrderAddRequest {

    @NotEmpty
    private String currentEmployeeId;

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }
}
