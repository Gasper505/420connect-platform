package com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import org.hibernate.validator.constraints.NotEmpty;

public class PartnerPurchaseOrderUpdateRequest extends PurchaseOrder {
    @NotEmpty
    private String currentEmployeeId;

    public String getCurrentEmployeeId() {
        return currentEmployeeId;
    }

    public void setCurrentEmployeeId(String currentEmployeeId) {
        this.currentEmployeeId = currentEmployeeId;
    }
}
