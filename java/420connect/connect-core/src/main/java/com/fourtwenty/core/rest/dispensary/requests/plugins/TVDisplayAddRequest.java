package com.fourtwenty.core.rest.dispensary.requests.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by decipher on 14/10/17 3:19 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TVDisplayAddRequest {
    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}