package com.fourtwenty.core.rest.dispensary.requests.promotions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;

/**
 * Created by mdo on 1/3/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromoCriteriaAddRequest {
    private PromotionRule.PromotionRuleType criteriaType;
    private String categoryId;
    private String productId;
    private Double minQty = 0d;
    private Double maxQty = 0d;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public PromotionRule.PromotionRuleType getCriteriaType() {
        return criteriaType;
    }

    public void setCriteriaType(PromotionRule.PromotionRuleType criteriaType) {
        this.criteriaType = criteriaType;
    }

    public Double getMaxQty() {
        return maxQty;
    }

    public void setMaxQty(Double maxQty) {
        this.maxQty = maxQty;
    }

    public Double getMinQty() {
        return minQty;
    }

    public void setMinQty(Double minQty) {
        this.minQty = minQty;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
