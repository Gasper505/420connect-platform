package com.fourtwenty.core.rest.dispensary.requests.promotions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;

/**
 * Created by mdo on 1/3/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionAddRequest {
    @NotEmpty
    private String name;

    private Promotion.PromotionType promotionType = Promotion.PromotionType.Cart;

    private Promotion.PromoSource promoSource = Promotion.PromoSource.Manual;

    private BigDecimal discountAmt = new BigDecimal(0);

    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Cash;

    private boolean active = Boolean.TRUE;
    private boolean stackable = Boolean.TRUE;

    public boolean isStackable() {
        return stackable;
    }

    public void setStackable(boolean stackable) {
        this.stackable = stackable;
    }

    public Promotion.PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Promotion.PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Promotion.PromoSource getPromoSource() {
        return promoSource;
    }

    public void setPromoSource(Promotion.PromoSource promoSource) {
        this.promoSource = promoSource;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }
}
