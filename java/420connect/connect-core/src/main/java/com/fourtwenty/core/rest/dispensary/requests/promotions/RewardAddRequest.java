package com.fourtwenty.core.rest.dispensary.requests.promotions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;

/**
 * Created by mdo on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RewardAddRequest {
    @Min(0)
    private int points = 0;
    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
