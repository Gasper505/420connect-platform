package com.fourtwenty.core.rest.dispensary.requests.promotions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;

/**
 * Created on 9/10/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StorewideSaleAddRequest {
    @NotEmpty
    private String name;
    private BigDecimal discountAmt = new BigDecimal(0);
    private OrderItem.DiscountType discountType = OrderItem.DiscountType.Percentage;
    private boolean active = Boolean.FALSE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public OrderItem.DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(OrderItem.DiscountType discountType) {
        this.discountType = discountType;
    }

    
}
