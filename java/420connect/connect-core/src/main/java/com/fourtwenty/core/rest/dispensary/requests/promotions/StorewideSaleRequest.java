package com.fourtwenty.core.rest.dispensary.requests.promotions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.loyalty.StorewideSale;

/**
 * Created on 9/10/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StorewideSaleRequest extends StorewideSale {
    private String startTime;
    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
