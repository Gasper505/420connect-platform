package com.fourtwenty.core.rest.dispensary.requests.queues;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 7/22/16.
 */
public class AdminQueueAddMemberRequest extends QueueAddMemberRequest {
    @NotEmpty
    private String queueName;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
}
