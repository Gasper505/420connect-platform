package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.ConsumerType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnonymousQueueAddRequest {
    private ConsumerType consumerType = ConsumerType.AdultUse;

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }
}
