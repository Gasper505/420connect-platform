package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.store.ConsumerCart;

/**
 * Created by mdo on 6/15/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerOrderUpdateETAStatusRequest {
    private ConsumerCart.ConsumerTrackingStatus trackingStatus = ConsumerCart.ConsumerTrackingStatus.Packaged;
    private int eta = -1;

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public ConsumerCart.ConsumerTrackingStatus getTrackingStatus() {
        return trackingStatus;
    }

    public void setTrackingStatus(ConsumerCart.ConsumerTrackingStatus trackingStatus) {
        this.trackingStatus = trackingStatus;
    }
}
