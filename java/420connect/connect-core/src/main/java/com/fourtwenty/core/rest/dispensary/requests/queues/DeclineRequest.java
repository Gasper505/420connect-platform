package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 5/22/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeclineRequest {
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
