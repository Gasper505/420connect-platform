package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.store.ConsumerCart;

/**
 * Created by mdo on 1/15/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeReassignRequest {
    private String employeeId;

    private int eta = -1;

    private boolean transferItems = Boolean.FALSE;

    private boolean createOnfleetTask = Boolean.FALSE;

    private String onFleetTeamId;

    private String overrideInventoryId;

    private ConsumerCart consumerCart;

    private boolean cartUpdated = Boolean.FALSE;

    private boolean createTookanTask;

    private String tookanTeamId;

    private String packedBy;

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public boolean isTransferItems() {
        return transferItems;
    }

    public void setTransferItems(boolean transferItems) {
        this.transferItems = transferItems;
    }

    public boolean getCreateOnfleetTask() {
        return createOnfleetTask;
    }

    public void setCreateOnfleetTask(boolean createOnfleetTask) {
        this.createOnfleetTask = createOnfleetTask;
    }

    public String getOnFleetTeamId() {
        return onFleetTeamId;
    }

    public void setOnFleetTeamId(String onFleetTeamId) {
        this.onFleetTeamId = onFleetTeamId;
    }

    public String getOverrideInventoryId() {
        return overrideInventoryId;
    }

    public void setOverrideInventoryId(String overrideInventoryId) {
        this.overrideInventoryId = overrideInventoryId;
    }

    public ConsumerCart getConsumerCart() {
        return consumerCart;
    }

    public void setConsumerCart(ConsumerCart consumerCart) {
        this.consumerCart = consumerCart;
    }

    public boolean isCartUpdated() {
        return cartUpdated;
    }

    public void setCartUpdated(boolean cartUpdated) {
        this.cartUpdated = cartUpdated;
    }

    public boolean isCreateTookanTask() {
        return createTookanTask;
    }

    public void setCreateTookanTask(boolean createTookanTask) {
        this.createTookanTask = createTookanTask;
    }

    public String getTookanTeamId() {
        return tookanTeamId;
    }

    public void setTookanTeamId(String tookanTeamId) {
        this.tookanTeamId = tookanTeamId;
    }

    public String getPackedBy() {
        return packedBy;
    }

    public void setPackedBy(String packedBy) {
        this.packedBy = packedBy;
    }
}
