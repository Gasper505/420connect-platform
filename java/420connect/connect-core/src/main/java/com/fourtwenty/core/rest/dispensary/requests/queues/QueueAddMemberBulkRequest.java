package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QueueAddMemberBulkRequest {

    @NotEmpty
    private String queueName;
    private String transID;
    private QueueAddMemberRequest queueAddMemberRequest;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public QueueAddMemberRequest getQueueAddMemberRequest() {
        return queueAddMemberRequest;
    }

    public void setQueueAddMemberRequest(QueueAddMemberRequest queueAddMemberRequest) {
        this.queueAddMemberRequest = queueAddMemberRequest;
    }

    public String getTransID() {
        return transID;
    }

    public void setTransID(String transID) {
        this.transID = transID;
    }
}
