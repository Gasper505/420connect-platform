package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mdo on 1/17/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueueAddMemberRequest {
    @NotEmpty
    private String memberId;
    private String employeeId;
    private Boolean createOnFleetTask = Boolean.FALSE;
    private String memo;
    private String onFleetTeamId;
    private boolean createTookanTask;
    private String tookanTeamId;
    private String terminalId;
    private Set<String> orderTags = new HashSet<>();
    private String kioskCheckInId;
    private long deliveryDate;
    private long pickUpDate;
    private long completeAfter;

    public Long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(Long pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public Long getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(Long completeAfter) {
        this.completeAfter = completeAfter;
    }

    public Set<String> getOrderTags() {
        return orderTags;
    }

    public void setOrderTags(Set<String> orderTags) {
        this.orderTags = orderTags;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Boolean getCreateOnFleetTask() {
        return createOnFleetTask;
    }

    public void setCreateOnFleetTask(Boolean createOnFleetTask) {
        this.createOnFleetTask = createOnFleetTask;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getOnFleetTeamId() {
        return onFleetTeamId;
    }

    public void setOnFleetTeamId(String onFleetTeamId) {
        this.onFleetTeamId = onFleetTeamId;
    }

    public boolean isCreateTookanTask() {
        return createTookanTask;
    }

    public void setCreateTookanTask(boolean createTookanTask) {
        this.createTookanTask = createTookanTask;
    }

    public String getTookanTeamId() {
        return tookanTeamId;
    }

    public void setTookanTeamId(String tookanTeamId) {
        this.tookanTeamId = tookanTeamId;
    }

    public String getKioskCheckInId() {
        return kioskCheckInId;
    }

    public void setKioskCheckInId(String kioskCheckInId) {
        this.kioskCheckInId = kioskCheckInId;
    }
}
