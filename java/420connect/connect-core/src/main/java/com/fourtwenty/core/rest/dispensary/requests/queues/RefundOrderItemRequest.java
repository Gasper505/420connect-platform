package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.RefundTransactionRequest;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundOrderItemRequest {

    private String orderItemId;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantity;


    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal refundAmt;

    private Long created;
    private String createdById;
    private Cart.PaymentOption refundAs;
    private RefundTransactionRequest.RefundType refundType = RefundTransactionRequest.RefundType.Refund;
    private String note;
    private boolean withInventory;
    private String createdUser;

    public BigDecimal getRefundAmt() {
        return refundAmt;
    }

    public void setRefundAmt(BigDecimal refundAmt) {
        this.refundAmt = refundAmt;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public Cart.PaymentOption getRefundAs() {
        return refundAs;
    }

    public void setRefundAs(Cart.PaymentOption refundAs) {
        this.refundAs = refundAs;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isWithInventory() {
        return withInventory;
    }

    public void setWithInventory(boolean withInventory) {
        this.withInventory = withInventory;
    }

    public RefundTransactionRequest.RefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundTransactionRequest.RefundType refundType) {
        this.refundType = refundType;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RefundOrderItemRequest that = (RefundOrderItemRequest) o;
        return orderItemId.equals(that.orderItemId) &&
                quantity.equals(that.quantity) &&
                refundAmt.equals(that.refundAmt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItemId, quantity, refundAmt);
    }
}
