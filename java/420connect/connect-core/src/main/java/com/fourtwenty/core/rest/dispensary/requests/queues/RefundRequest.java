package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.RefundTransactionRequest;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 5/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundRequest {
    public enum RefundVersion {
        NEW,
        OLD
    }
    private boolean withInventory;
    private String note;
    private Cart.PaymentOption refundAs;
    private List<Double> loc = new ArrayList<>(); // Location in [lon,lat] // array format
    private List<RefundOrderItemRequest> refundOrderItemRequests = new ArrayList<>();

    private RefundVersion refundVersion = RefundVersion.OLD;

    @Deprecated
    private List<String> refundOrders = new ArrayList<>(); // deprecated

    private RefundTransactionRequest.RefundType refundType = RefundTransactionRequest.RefundType.Refund;
    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalRefundAmt = BigDecimal.ZERO;

    public List<String> getRefundOrders() {
        return refundOrders;
    }

    public void setRefundOrders(List<String> refundOrders) {
        this.refundOrders = refundOrders;
    }

    public List<Double> getLoc() {
        return loc;
    }

    public void setLoc(List<Double> loc) {
        this.loc = loc;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isWithInventory() {
        return withInventory;
    }

    public void setWithInventory(boolean withInventory) {
        this.withInventory = withInventory;
    }

    public Cart.PaymentOption getRefundAs() {
        return refundAs;
    }

    public void setRefundAs(Cart.PaymentOption refundAs) {
        this.refundAs = refundAs;
    }

    public List<RefundOrderItemRequest> getRefundOrderItemRequests() {
        return refundOrderItemRequests;
    }

    public void setRefundOrderItemRequests(List<RefundOrderItemRequest> refundOrderItemRequests) {
        this.refundOrderItemRequests = refundOrderItemRequests;
    }

    public RefundTransactionRequest.RefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundTransactionRequest.RefundType refundType) {
        this.refundType = refundType;
    }

    public BigDecimal getTotalRefundAmt() {
        return totalRefundAmt;
    }

    public void setTotalRefundAmt(BigDecimal totalRefundAmt) {
        this.totalRefundAmt = totalRefundAmt;
    }


    public RefundVersion getRefundVersion() {
        return refundVersion;
    }

    public void setRefundVersion(RefundVersion refundVersion) {
        this.refundVersion = refundVersion;
    }
}
