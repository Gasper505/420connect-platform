package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 12/29/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionDeleteRequest {
    public enum DeclineReason {
        MEMBER_CANCELLED,
        CANNOT_DELIVER_TO_ADDRESS,
        MEMBER_BANNED,
        PRODUCT_NOT_EXISTS,
        INCOMPLETE_INFORMATION,
        SHOP_CANCELLED,
        ALREADY_COMPLETED,
        DUPLICATE,
        TESTING,
        OTHER
    }

    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
