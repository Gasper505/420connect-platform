package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by decipher on 6/12/17 4:11 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsRequest {

    public enum QueryType {
        SALES, ADJUSTMENT, OTHER
    }

    public enum AssignStatus {
        ASSIGNED, UNASSIGNED, COMPLETED, INPROGRESS
    }

    private QueryType queryType;

    public QueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }
}
