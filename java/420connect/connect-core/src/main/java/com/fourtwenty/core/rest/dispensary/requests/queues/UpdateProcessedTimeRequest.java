package com.fourtwenty.core.rest.dispensary.requests.queues;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 10/31/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateProcessedTimeRequest {
    private Long processedTime;

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
    }
}
