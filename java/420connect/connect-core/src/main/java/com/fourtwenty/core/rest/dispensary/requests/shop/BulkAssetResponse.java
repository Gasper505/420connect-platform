package com.fourtwenty.core.rest.dispensary.requests.shop;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import org.codehaus.jackson.annotate.JsonProperty;

public class BulkAssetResponse {

    CompanyAsset companyAsset;
    String id;
    String type;
    @JsonProperty("signed_type")
    String signed_type;

    public static final String MEMBER = "member";
    public static final String MEMBER_UPDATE = "memberUpdate";
    public static final String TRANSACTION = "transaction";


    public CompanyAsset getCompanyAsset() {
        return companyAsset;
    }

    public void setCompanyAsset(CompanyAsset companyAsset) {
        this.companyAsset = companyAsset;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSigned_type() {
        return signed_type;
    }

    public void setSigned_type(String signed_type) {
        this.signed_type = signed_type;
    }
}

