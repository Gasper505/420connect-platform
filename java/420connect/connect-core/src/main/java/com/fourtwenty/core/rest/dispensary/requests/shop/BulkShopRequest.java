package com.fourtwenty.core.rest.dispensary.requests.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberBulkRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.TransactionBulkRequest;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BulkShopRequest {

    private List<POSMembershipAddMember> members = new ArrayList<>();
    private List<MembershipUpdateRequest> updatedMembers = new ArrayList<>();
    private List<QueueAddMemberBulkRequest> queueAddMembers = new ArrayList<>();
    private List<TransactionBulkRequest> transactions = new ArrayList<>();


    public List<POSMembershipAddMember> getMembers() {
        return members;
    }

    public void setMembers(List<POSMembershipAddMember> members) {
        this.members = members;
    }

    public List<MembershipUpdateRequest> getUpdatedMembers() {
        return updatedMembers;
    }

    public void setUpdatedMembers(List<MembershipUpdateRequest> updatedMembers) {
        this.updatedMembers = updatedMembers;
    }

    public List<QueueAddMemberBulkRequest> getQueueAddMembers() {
        return queueAddMembers;
    }

    public void setQueueAddMembers(List<QueueAddMemberBulkRequest> queueAddMembers) {
        this.queueAddMembers = queueAddMembers;
    }

    public List<TransactionBulkRequest> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionBulkRequest> transactions) {
        this.transactions = transactions;
    }
}
