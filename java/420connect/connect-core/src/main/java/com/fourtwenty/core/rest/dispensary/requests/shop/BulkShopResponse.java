package com.fourtwenty.core.rest.dispensary.requests.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberBulkRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.TransactionBulkRequest;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BulkShopResponse {
    @NotEmpty
    private List<Member> members;
    private List<Member> updatedMembers;
    private List<QueueAddMemberBulkRequest> queueAddMemberLst;
    private List<Transaction> transactionlst;

    private List<BulkErrorInformation<POSMembershipAddMember>> memberErrors = new ArrayList<>();
    private List<BulkErrorInformation<MembershipUpdateRequest>> updateMemberErrors = new ArrayList<>();
    private List<BulkErrorInformation<QueueAddMemberBulkRequest>> queueAddMemberErrors = new ArrayList<>();
    private List<BulkErrorInformation<TransactionBulkRequest>> transactionErrors = new ArrayList<>();


    public List<Member> getUpdatedMembers() {
        return updatedMembers;
    }

    public void setUpdatedMembers(List<Member> updatedMembers) {
        this.updatedMembers = updatedMembers;
    }

    public List<BulkErrorInformation<MembershipUpdateRequest>> getUpdateMemberErrors() {
        return updateMemberErrors;
    }

    public void setUpdateMemberErrors(List<BulkErrorInformation<MembershipUpdateRequest>> updateMemberErrors) {
        this.updateMemberErrors = updateMemberErrors;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public List<QueueAddMemberBulkRequest> getQueueAddMemberLst() {
        return queueAddMemberLst;
    }

    public void setQueueAddMemberLst(List<QueueAddMemberBulkRequest> queueAddMemberLst) {
        this.queueAddMemberLst = queueAddMemberLst;
    }

    public List<Transaction> getTransactionlst() {
        return transactionlst;
    }

    public void setTransactionlst(List<Transaction> transactionlst) {
        this.transactionlst = transactionlst;
    }

    public List<BulkErrorInformation<POSMembershipAddMember>> getMemberErrors() {
        return memberErrors;
    }

    public void setMemberErrors(List<BulkErrorInformation<POSMembershipAddMember>> memberErrors) {
        this.memberErrors = memberErrors;
    }

    public List<BulkErrorInformation<QueueAddMemberBulkRequest>> getQueueAddMemberErrors() {
        return queueAddMemberErrors;
    }

    public void setQueueAddMemberErrors(List<BulkErrorInformation<QueueAddMemberBulkRequest>> queueAddMemberErrors) {
        this.queueAddMemberErrors = queueAddMemberErrors;
    }

    public List<BulkErrorInformation<TransactionBulkRequest>> getTransactionErrors() {
        return transactionErrors;
    }

    public void setTransactionErrors(List<BulkErrorInformation<TransactionBulkRequest>> transactionErrors) {
        this.transactionErrors = transactionErrors;
    }

    public void addMemberError(POSMembershipAddMember member, String error) {
        memberErrors.add(new BulkErrorInformation<>(member, error));
    }

    public void addUpdateMemberError(MembershipUpdateRequest member, String error) {
        updateMemberErrors.add(new BulkErrorInformation<>(member, error));
    }

    public void addQueueMemberError(QueueAddMemberBulkRequest queueAddMemberBulkRequest, String error) {
        queueAddMemberErrors.add(new BulkErrorInformation<>(queueAddMemberBulkRequest, error));
    }

    public void addTransactionError(TransactionBulkRequest transaction, String error) {
        transactionErrors.add(new BulkErrorInformation<>(transaction, error));
    }

    private class BulkErrorInformation<T> {
        private T obj;
        private String error;

        public BulkErrorInformation(T obj, String error) {
            this.obj = obj;
            this.error = error;
        }

        public T getObj() {
            return obj;
        }

        public void setObj(T obj) {
            this.obj = obj;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
    }

}
