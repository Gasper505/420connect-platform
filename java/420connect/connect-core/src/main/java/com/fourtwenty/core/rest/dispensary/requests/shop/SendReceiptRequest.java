package com.fourtwenty.core.rest.dispensary.requests.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 4/27/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendReceiptRequest {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
