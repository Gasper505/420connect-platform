package com.fourtwenty.core.rest.dispensary.requests.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 2/1/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopAddRequest {
    @NotEmpty
    private String name;
    private String defaultCountry;
    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Retail;

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    private Shop.ShopType shopType = Shop.ShopType.Medicinal;

    public Shop.ShopType getShopType() {
        return shopType;
    }

    public void setShopType(Shop.ShopType shopType) {
        this.shopType = shopType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }
}
