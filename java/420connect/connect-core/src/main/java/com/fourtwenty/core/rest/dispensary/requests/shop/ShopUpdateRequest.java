package com.fourtwenty.core.rest.dispensary.requests.shop;

import java.util.HashMap;
import java.util.Map;

public class ShopUpdateRequest {

    private Map<String, Object> shopInfoMap = new HashMap<>();

    public Map<String, Object> getShopInfoMap() {
        return shopInfoMap;
    }

    public void setShopInfoMap(Map<String, Object> shopInfoMap) {
        this.shopInfoMap = shopInfoMap;
    }
}
