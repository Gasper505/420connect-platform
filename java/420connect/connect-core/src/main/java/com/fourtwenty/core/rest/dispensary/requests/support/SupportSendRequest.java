package com.fourtwenty.core.rest.dispensary.requests.support;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 8/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupportSendRequest {
    @NotEmpty
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
