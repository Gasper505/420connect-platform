package com.fourtwenty.core.rest.dispensary.requests.terminals;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Shop;

/**
 * Created by mdo on 10/9/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalAddRequest extends TerminalUpdateRequest {
    private String deviceModel;
    private String deviceVersion;
    private String deviceName;
    private String appVersion;
    private String deviceId;
    private String deviceToken;
    private Shop.ShopCheckoutType checkoutType = Shop.ShopCheckoutType.Direct;
    private boolean onPremEnabled = false;

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Shop.ShopCheckoutType getCheckoutType() {
        return checkoutType;
    }

    public void setCheckoutType(Shop.ShopCheckoutType checkoutType) {
        this.checkoutType = checkoutType;
    }

    public boolean isOnPremEnabled() {
        return onPremEnabled;
    }

    public void setOnPremEnabled(boolean onPremEnabled) {
        this.onPremEnabled = onPremEnabled;
    }
}
