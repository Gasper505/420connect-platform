package com.fourtwenty.core.rest.dispensary.requests.terminals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 11/1/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalAdminAddRequest extends TerminalAddRequest {
    private boolean active = true;
    private String assignedInventoryId;
    private String shopId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAssignedInventoryId() {
        return assignedInventoryId;
    }

    public void setAssignedInventoryId(String assignedInventoryId) {
        this.assignedInventoryId = assignedInventoryId;
    }

}
