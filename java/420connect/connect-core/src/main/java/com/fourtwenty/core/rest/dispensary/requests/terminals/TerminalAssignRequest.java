package com.fourtwenty.core.rest.dispensary.requests.terminals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 12/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalAssignRequest {
    private String terminalId;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
}
