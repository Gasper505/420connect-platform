package com.fourtwenty.core.rest.dispensary.requests.terminals;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 5/15/16.
 */
public class TerminalSettingsUpdateRequest {
    @NotEmpty
    private String terminalId;
    @NotEmpty
    private String inventoryId;
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
}
