package com.fourtwenty.core.rest.dispensary.requests.terminals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Shop;

/**
 * Created by mdo on 4/4/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalUpdateNameRequest {
    private String name;
    private boolean active;
    private String assignedInventoryId;
    private String shopId;
    private String cvAccountId;
    private Shop.ShopCheckoutType checkoutType = Shop.ShopCheckoutType.Direct;
    private boolean onPremEnabled = false;

    public String getCvAccountId() {
        return cvAccountId;
    }

    public void setCvAccountId(String cvAccountId) {
        this.cvAccountId = cvAccountId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getAssignedInventoryId() {
        return assignedInventoryId;
    }

    public void setAssignedInventoryId(String assignedInventoryId) {
        this.assignedInventoryId = assignedInventoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Shop.ShopCheckoutType getCheckoutType() {
        return checkoutType;
    }

    public void setCheckoutType(Shop.ShopCheckoutType checkoutType) {
        this.checkoutType = checkoutType;
    }

    public boolean isOnPremEnabled() {
        return onPremEnabled;
    }

    public void setOnPremEnabled(boolean onPremEnabled) {
        this.onPremEnabled = onPremEnabled;
    }
}
