package com.fourtwenty.core.rest.dispensary.requests.terminals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 10/9/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalUpdateRequest {
    @NotEmpty
    private String name;
    private String deviceToken;
    private String deviceType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
