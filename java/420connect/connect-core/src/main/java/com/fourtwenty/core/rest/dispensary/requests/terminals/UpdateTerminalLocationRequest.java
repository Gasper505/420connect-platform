package com.fourtwenty.core.rest.dispensary.requests.terminals;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 7/2/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateTerminalLocationRequest {
    private double longitude;
    private double latitude;
    private String deviceId;
    private String name;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
