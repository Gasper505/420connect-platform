package com.fourtwenty.core.rest.dispensary.requests.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 10/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetAccountAddRequest {
}
