package com.fourtwenty.core.rest.dispensary.requests.thirdparty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Gaurav Saini on 11/7/17.
 */
public class HeadsetAddRequest {

    @JsonProperty("CompanyId")
    private long companyId;
    @JsonProperty("Name")
    private String name = "";
    @JsonProperty("Address1")
    private String address1 = "";
    @JsonProperty("Address2")
    private String address2 = "";
    @JsonProperty("City")
    private String city = "";
    @JsonProperty("State")
    private String state = "";
    @JsonProperty("Zip")
    private String zip = "";
    @JsonProperty("Timezone")
    private String timezone = "";
    @JsonProperty("ContactName")
    private String contactName = "";
    @JsonProperty("ContactEmail")
    private String contactEmail = "";
    @JsonProperty("Partner")
    private String partner = "";
    @JsonProperty("PartnerKey")
    private String partnerKey = "";

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }
}
