package com.fourtwenty.core.rest.dispensary.requests.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 2/10/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TPAccountAddRequest {
    @NotEmpty
    private String name;
    private ThirdPartyAccount.ThirdPartyAccountType accountType = ThirdPartyAccount.ThirdPartyAccountType.None;
    @NotEmpty
    private String username;
    private String password;
    @NotEmpty
    private String shopId;
    @JsonProperty(value = "active")
    private boolean status = Boolean.FALSE;
    private String clientId;
    private String clientSecret;
    private String locationId;
    private String merchantId;
    private String token;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ThirdPartyAccount.ThirdPartyAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(ThirdPartyAccount.ThirdPartyAccountType accountType) {
        this.accountType = accountType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
