package com.fourtwenty.core.rest.dispensary.requests.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;

import java.util.HashMap;

/**
 * Created by mdo on 9/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TPShopAccountAddRequest {
    private ThirdPartyShopAccount.ThirdPartyShopAccountType accountType = ThirdPartyShopAccount.ThirdPartyShopAccountType.Hypur;
    private boolean enabled = false;
    private String shopApiKey;
    private Long lastSyncDate;
    private boolean verified;
    private String entityISN;

    private HashMap<String, String> settings = new HashMap<>();

    public ThirdPartyShopAccount.ThirdPartyShopAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(ThirdPartyShopAccount.ThirdPartyShopAccountType accountType) {
        this.accountType = accountType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public HashMap<String, String> getSettings() {
        return settings;
    }

    public void setSettings(HashMap<String, String> settings) {
        this.settings = settings;
    }

    public String getShopApiKey() {
        return shopApiKey;
    }

    public void setShopApiKey(String shopApiKey) {
        this.shopApiKey = shopApiKey;
    }

    public Long getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Long lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getEntityISN() {
        return entityISN;
    }

    public void setEntityISN(String entityISN) {
        this.entityISN = entityISN;
    }
}

