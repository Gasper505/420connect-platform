package com.fourtwenty.core.rest.dispensary.requests.thirdparty;

import com.fourtwenty.core.thirdparty.headset.models.HeadsetTicketItem;

import java.util.List;

/**
 * Created by Gaurav Saini on 4/7/17.
 */
public class TicketAddRequest {

    private String id = "";
    private String utcDate = ""; // date-time ex : 2017-07-04T09:41:34.375Z
    private String localTime = ""; // date-time ex : 2017-07-04T09:41:34.375Z
    private String ticketType = "";
    private String customerReference = "";
    private String employeeReference = "";
    private String paymentType = "";
    private int total;
    private int tax;
    private int subTotal;
    private int discount;
    private List<HeadsetTicketItem> items;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(String utcDate) {
        this.utcDate = utcDate;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getEmployeeReference() {
        return employeeReference;
    }

    public void setEmployeeReference(String employeeReference) {
        this.employeeReference = employeeReference;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public List<HeadsetTicketItem> getItems() {
        return items;
    }

    public void setItems(List<HeadsetTicketItem> items) {
        this.items = items;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
