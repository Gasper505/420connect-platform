package com.fourtwenty.core.rest.dispensary.requests.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrderTagsRequest {

    private Set<String> orderTags = new HashSet<>();

    public Set<String> getOrderTags() {
        return orderTags;
    }

    public void setOrderTags(Set<String> orderTags) {
        this.orderTags = orderTags;
    }
}
