package com.fourtwenty.core.rest.dispensary.requests.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderScheduleUpdateRequest {
    private long scheduleDate;
    private long completeAfter;

    public long getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(long scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public long getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(long completeAfter) {
        this.completeAfter = completeAfter;
    }
}
