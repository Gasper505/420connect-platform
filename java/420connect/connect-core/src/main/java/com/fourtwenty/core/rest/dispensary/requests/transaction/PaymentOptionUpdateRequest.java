package com.fourtwenty.core.rest.dispensary.requests.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.SplitPayment;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentOptionUpdateRequest {
    private Cart.PaymentOption paymentOption;
    private SplitPayment splitPayment;

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public SplitPayment getSplitPayment() {
        return splitPayment;
    }

    public void setSplitPayment(SplitPayment splitPayment) {
        this.splitPayment = splitPayment;
    }
}
