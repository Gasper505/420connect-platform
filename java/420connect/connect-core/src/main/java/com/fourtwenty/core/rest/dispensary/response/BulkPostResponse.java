package com.fourtwenty.core.rest.dispensary.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BulkPostResponse<T> {
    private T request;
    private String error;

    public BulkPostResponse(T request, String error) {
        this.request = request;
        this.error = error;
    }

    public T getRequest() {
        return request;
    }

    public void setRequest(T request) {
        this.request = request;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
