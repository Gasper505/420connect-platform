package com.fourtwenty.core.rest.dispensary.results;

import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BrandResult extends Brand {

    private List<Vendor> vendors;
    private String thirdPartyBrandName;

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public String getThirdPartyBrandName() {
        return thirdPartyBrandName;
    }

    public void setThirdPartyBrandName(String thirdPartyBrandName) {
        this.thirdPartyBrandName = thirdPartyBrandName;
    }
}
