package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)

public class CannabisResult {
    private Product.CannabisType blazeCannabisType;
    private String cannabisType;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal currentUsage;

    public Product.CannabisType getBlazeCannabisType() {
        return blazeCannabisType;
    }

    public void setBlazeCannabisType(Product.CannabisType blazeCannabisType) {
        this.blazeCannabisType = blazeCannabisType;
    }

    public String getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(String cannabisType) {
        this.cannabisType = cannabisType;
    }

    public BigDecimal getCurrentUsage() {
        return currentUsage;
    }

    public void setCurrentUsage(BigDecimal currentUsage) {
        this.currentUsage = currentUsage;
    }
}
