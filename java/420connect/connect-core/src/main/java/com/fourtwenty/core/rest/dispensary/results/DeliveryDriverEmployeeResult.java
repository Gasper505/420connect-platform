package com.fourtwenty.core.rest.dispensary.results;

import com.fourtwenty.core.domain.models.company.Employee;

/**
 * Created by decipher on 1/12/17 11:27 AM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class DeliveryDriverEmployeeResult extends Employee {

    private boolean routing = false;
    private String terminalName;
    private String inventoryName;
    private Long activeTransaction;

    public boolean isRouting() {
        return routing;
    }

    public void setRouting(boolean routing) {
        this.routing = routing;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getActiveTransaction() {
        return activeTransaction;
    }

    public void setActiveTransaction(Long activeTransaction) {
        this.activeTransaction = activeTransaction;
    }
}
