package com.fourtwenty.core.rest.dispensary.results;

import com.fourtwenty.core.domain.models.company.Employee;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by decipher on 29/11/17 4:27 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class EmployeeLocationResult extends Employee {

    private List<Double> loc = new ArrayList<>();

    private String terminalName = "";

    public List<Double> getLoc() {
        return loc;
    }

    public void setLoc(List<Double> loc) {
        this.loc = loc;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }
}
