package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeRecentLocationResult {

    @MongoId
    @MongoObjectId
    private String id; //employee id
    private List<Double> loc = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Double> getLoc() {
        return loc;
    }

    public void setLoc(List<Double> loc) {
        this.loc = loc;
    }
}
