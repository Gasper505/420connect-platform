package com.fourtwenty.core.rest.dispensary.results;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TerminalLocation;

import java.util.List;

/**
 * Created by decipher on 29/11/17 5:49 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class EnRouteDriverEmployeeResult extends Employee {

    private List<TerminalLocation> terminalLocationList;

    public List<TerminalLocation> getTerminalLocationList() {
        return terminalLocationList;
    }

    public void setTerminalLocationList(List<TerminalLocation> terminalLocationList) {
        this.terminalLocationList = terminalLocationList;
    }
}
