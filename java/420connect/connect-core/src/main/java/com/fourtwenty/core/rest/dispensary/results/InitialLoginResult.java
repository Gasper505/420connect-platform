package com.fourtwenty.core.rest.dispensary.results;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.rest.dispensary.results.auth.LoginResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 9/23/15.
 */
public class InitialLoginResult extends LoginResult {
    private Company company;
    private List<ShopResult> shops;
    private ShopResult assignedShop;
    private boolean newDevice;
    private Terminal assignedTerminal;
    private ConnectAuthToken.ConnectAppType appType = ConnectAuthToken.ConnectAppType.Blaze;
    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Retail;
    private List<ShopResult> employeeShops = new ArrayList<>();
    private String inventoryName;


    public ConnectAuthToken.ConnectAppType getAppType() {
        return appType;
    }

    public void setAppType(ConnectAuthToken.ConnectAppType appType) {
        this.appType = appType;
    }

    public Terminal getAssignedTerminal() {
        return assignedTerminal;
    }

    public void setAssignedTerminal(Terminal assignedTerminal) {
        this.assignedTerminal = assignedTerminal;
    }

    public boolean isNewDevice() {
        return newDevice;
    }

    public void setNewDevice(boolean newDevice) {
        this.newDevice = newDevice;
    }

    public ShopResult getAssignedShop() {
        return assignedShop;
    }

    public void setAssignedShop(ShopResult assignedShop) {
        this.assignedShop = assignedShop;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<ShopResult> getShops() {
        return shops;
    }

    public void setShops(List<ShopResult> shops) {
        this.shops = shops;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }

    public List<ShopResult> getEmployeeShops() {
        return employeeShops;
    }

    public void setEmployeeShops(List<ShopResult> employeeShops) {
        this.employeeShops = employeeShops;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }
}
