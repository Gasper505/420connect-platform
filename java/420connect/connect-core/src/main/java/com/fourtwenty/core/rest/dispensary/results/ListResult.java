package com.fourtwenty.core.rest.dispensary.results;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 10/9/15.
 */
public class ListResult<T> {
    private List<T> values = new ArrayList<>();

    public ListResult() {
    }

    public ListResult(List<T> values) {
        this.values = values;
    }

    public List<T> getValues() {
        return values;
    }

    public void setValues(List<T> values) {
        this.values = values;
    }
}
