package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketingMemberCountResult {

    private long count;


    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
