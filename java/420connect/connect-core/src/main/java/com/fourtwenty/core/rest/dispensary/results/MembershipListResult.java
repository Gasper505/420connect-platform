package com.fourtwenty.core.rest.dispensary.results;

/**
 * Created by mdo on 8/26/15.
 */
public class MembershipListResult {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
