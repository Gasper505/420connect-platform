package com.fourtwenty.core.rest.dispensary.results;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineUserResult {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private ConsumerType consumerType = ConsumerType.AdultUse;
    private boolean member = Boolean.FALSE;
    private long registeredDate;
    private String memberId;

    private String marketingSource;
    private Long acceptedDate;
    private Long rejectedDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public boolean isMember() {
        return member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public long getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(long registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public Long getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Long acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Long getRejectedDate() {
        return rejectedDate;
    }

    public void setRejectedDate(Long rejectedDate) {
        this.rejectedDate = rejectedDate;
    }
}
