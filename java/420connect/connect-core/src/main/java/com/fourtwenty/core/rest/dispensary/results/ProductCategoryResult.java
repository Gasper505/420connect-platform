package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.ProductCategory;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategoryResult extends ProductCategory {

    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
