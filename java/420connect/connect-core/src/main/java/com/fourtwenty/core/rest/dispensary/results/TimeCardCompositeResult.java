package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TimeCard;

/**
 * Created by mdo on 6/24/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeCardCompositeResult {
    private Employee employee;
    private TimeCard timeCard;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public TimeCard getTimeCard() {
        return timeCard;
    }

    public void setTimeCard(TimeCard timeCard) {
        this.timeCard = timeCard;
    }
}
