package com.fourtwenty.core.rest.dispensary.results;

public class TransactionCountResult {
    private long incomingOrders;
    private long inProgress;
    private long assigned;
    private long unAssigned;
    private long completed;

    public long getIncomingOrders() {
        return incomingOrders;
    }

    public void setIncomingOrders(long incomingOrders) {
        this.incomingOrders = incomingOrders;
    }

    public long getInProgress() {
        return inProgress;
    }

    public void setInProgress(long inProgress) {
        this.inProgress = inProgress;
    }

    public long getAssigned() {
        return assigned;
    }

    public void setAssigned(long assigned) {
        this.assigned = assigned;
    }

    public long getUnAssigned() {
        return unAssigned;
    }

    public void setUnAssigned(long unAssigned) {
        this.unAssigned = unAssigned;
    }

    public long getCompleted() {
        return completed;
    }

    public void setCompleted(long completed) {
        this.completed = completed;
    }
}
