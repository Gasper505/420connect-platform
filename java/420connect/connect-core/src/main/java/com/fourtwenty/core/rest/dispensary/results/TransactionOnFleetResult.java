package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.Transaction;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionOnFleetResult extends Transaction {

    private boolean createdAtOnFleet;
    private String error;
    private String cause;

    public boolean isCreatedAtOnFleet() {
        return createdAtOnFleet;
    }

    public void setCreatedAtOnFleet(boolean createdAtOnFleet) {
        this.createdAtOnFleet = createdAtOnFleet;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
