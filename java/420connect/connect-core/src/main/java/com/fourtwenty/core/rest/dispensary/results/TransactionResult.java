package com.fourtwenty.core.rest.dispensary.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.math.BigDecimal;

/**
 * Created by decipher on 5/12/17 4:23 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionResult {

    private String id;
    private long processedTime;
    private String transNo;
    private Transaction.QueueType queueType;
    private Transaction.TransactionType transType;
    private String sellerName;
    private String memberName;
    private BigDecimal total;
    private Long metrcId;
    private String sellerTerminalName;

    public Long getMetrcId() {
        return metrcId;
    }

    public void setMetrcId(Long metrcId) {
        this.metrcId = metrcId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(long processedTime) {
        this.processedTime = processedTime;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public Transaction.QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(Transaction.QueueType queueType) {
        this.queueType = queueType;
    }

    public Transaction.TransactionType getTransType() {
        return transType;
    }

    public void setTransType(Transaction.TransactionType transType) {
        this.transType = transType;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getSellerTerminalName() {
        return sellerTerminalName;
    }

    public void setSellerTerminalName(String sellerTerminalName) {
        this.sellerTerminalName = sellerTerminalName;
    }
}
