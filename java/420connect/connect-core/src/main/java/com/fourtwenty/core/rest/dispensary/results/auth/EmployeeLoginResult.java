package com.fourtwenty.core.rest.dispensary.results.auth;

/**
 * Created by mdo on 9/23/15.
 */
public class EmployeeLoginResult extends LoginResult {
    private boolean terminalValidationRequired = false;

    public boolean isTerminalValidationRequired() {
        return terminalValidationRequired;
    }

    public void setTerminalValidationRequired(boolean terminalValidationRequired) {
        this.terminalValidationRequired = terminalValidationRequired;
    }
}
