package com.fourtwenty.core.rest.dispensary.results.auth;

import com.fourtwenty.core.domain.models.company.Employee;

import java.util.UUID;

/**
 * Created by mdo on 8/28/15.
 */
public class LoginResult {
    private String accessToken;
    private String assetAccessToken;
    private Employee employee;
    private long loginTime;
    private long expirationTime;
    private String sessionId;

    public LoginResult() {
        if (sessionId == null) {
            sessionId = UUID.randomUUID().toString();
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getAssetAccessToken() {
        return assetAccessToken;
    }

    public void setAssetAccessToken(String assetAccessToken) {
        this.assetAccessToken = assetAccessToken;
    }
}
