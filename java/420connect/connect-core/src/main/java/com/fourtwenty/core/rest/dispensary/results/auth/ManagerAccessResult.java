package com.fourtwenty.core.rest.dispensary.results.auth;

import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.product.Inventory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 5/15/16.
 */
public class ManagerAccessResult {
    private String accessToken;
    private String assetAccessToken;
    private boolean success;
    private List<Terminal> terminals = new ArrayList<>();
    private List<Inventory> inventories = new ArrayList<>();
    private List<Shop> shops = new ArrayList<>();
    private CompanyFeatures companyFeatures;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAssetAccessToken() {
        return assetAccessToken;
    }

    public void setAssetAccessToken(String assetAccessToken) {
        this.assetAccessToken = assetAccessToken;
    }

    public CompanyFeatures getCompanyFeatures() {
        return companyFeatures;
    }

    public void setCompanyFeatures(CompanyFeatures companyFeatures) {
        this.companyFeatures = companyFeatures;
    }

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    public List<Terminal> getTerminals() {
        return terminals;
    }

    public void setTerminals(List<Terminal> terminals) {
        this.terminals = terminals;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
