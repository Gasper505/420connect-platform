package com.fourtwenty.core.rest.dispensary.results.auth;

import com.fourtwenty.core.rest.dispensary.results.InitialLoginResult;

public class SwitchApplicationResult extends InitialLoginResult {

    private String redirectURL;

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }
}
