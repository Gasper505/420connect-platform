package com.fourtwenty.core.rest.dispensary.results.common;

import com.fourtwenty.core.domain.models.generic.Asset;

import java.io.InputStream;

/**
 * Created by mdo on 12/3/15.
 */
public class AssetStreamResult {
    private Asset asset;
    private InputStream stream;
    private String contentType;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }

    public String getContentType() {
        if (asset == null) {
            return "image/jpeg";
        }

        if (asset.getAssetType() == Asset.AssetType.Photo) {
            return "image/jpeg";
        } else if (asset.getAssetType() == Asset.AssetType.Document) {
            return "application/pdf";
        }
        return "image/jpeg";
    }
}
