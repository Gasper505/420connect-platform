package com.fourtwenty.core.rest.dispensary.results.common;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;

/**
 * Created by mdo on 5/25/18.
 */
public class CompositeProductInfoSyncResult {
    private DateSearchResult<Product> products = new DateSearchResult<>();
    private DateSearchResult<MemberGroupPrices> memberGroupPrices = new DateSearchResult<>();
    private DateSearchResult<BatchQuantity> batchQuantities = new DateSearchResult<>();
    private DateSearchResult<ProductPrepackageQuantity> prepackageQuantities = new DateSearchResult<>();
    private DateSearchResult<ProductWeightTolerance> weightTolerance = new DateSearchResult<>();
    private DateSearchResult<Promotion> promotions = new DateSearchResult<>();
    private DateSearchResult<Prepackage> prepackages = new DateSearchResult<>();
    private DateSearchResult<PrepackageProductItem> prepackageItems = new DateSearchResult<>();
    private DateSearchResult<BarcodeItem> barcodes = new DateSearchResult<>();
    private DateSearchResult<ProductBatch> productBatches = new DateSearchResult<>();
    private DateSearchResult<LoyaltyReward> loyaltyRewards = new DateSearchResult<>();


    public DateSearchResult<BarcodeItem> getBarcodes() {
        return barcodes;
    }

    public DateSearchResult<BatchQuantity> getBatchQuantities() {
        return batchQuantities;
    }

    public DateSearchResult<LoyaltyReward> getLoyaltyRewards() {
        return loyaltyRewards;
    }

    public DateSearchResult<MemberGroupPrices> getMemberGroupPrices() {
        return memberGroupPrices;
    }

    public DateSearchResult<PrepackageProductItem> getPrepackageItems() {
        return prepackageItems;
    }

    public DateSearchResult<ProductPrepackageQuantity> getPrepackageQuantities() {
        return prepackageQuantities;
    }

    public DateSearchResult<Prepackage> getPrepackages() {
        return prepackages;
    }

    public DateSearchResult<ProductBatch> getProductBatches() {
        return productBatches;
    }

    public DateSearchResult<Product> getProducts() {
        return products;
    }

    public DateSearchResult<Promotion> getPromotions() {
        return promotions;
    }

    public DateSearchResult<ProductWeightTolerance> getWeightTolerance() {
        return weightTolerance;
    }

    public void setBarcodes(DateSearchResult<BarcodeItem> barcodes) {
        this.barcodes = barcodes;
    }

    public void setBatchQuantities(DateSearchResult<BatchQuantity> batchQuantities) {
        this.batchQuantities = batchQuantities;
    }

    public void setLoyaltyRewards(DateSearchResult<LoyaltyReward> loyaltyRewards) {
        this.loyaltyRewards = loyaltyRewards;
    }

    public void setMemberGroupPrices(DateSearchResult<MemberGroupPrices> memberGroupPrices) {
        this.memberGroupPrices = memberGroupPrices;
    }

    public void setPrepackageItems(DateSearchResult<PrepackageProductItem> prepackageItems) {
        this.prepackageItems = prepackageItems;
    }

    public void setPrepackageQuantities(DateSearchResult<ProductPrepackageQuantity> prepackageQuantities) {
        this.prepackageQuantities = prepackageQuantities;
    }

    public void setPrepackages(DateSearchResult<Prepackage> prepackages) {
        this.prepackages = prepackages;
    }

    public void setProductBatches(DateSearchResult<ProductBatch> productBatches) {
        this.productBatches = productBatches;
    }

    public void setProducts(DateSearchResult<Product> products) {
        this.products = products;
    }

    public void setPromotions(DateSearchResult<Promotion> promotions) {
        this.promotions = promotions;
    }

    public void setWeightTolerance(DateSearchResult<ProductWeightTolerance> weightTolerance) {
        this.weightTolerance = weightTolerance;
    }
}
