package com.fourtwenty.core.rest.dispensary.results.common;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;

/**
 * Created by mdo on 5/25/18.
 */
public class CompositeShopSyncResult {
    private SearchResult<Member> members = new SearchResult<>();
    private DateSearchResult<Transaction> transactions = new DateSearchResult<>();
    private DateSearchResult<ConsumerCart> consumerCarts = new DateSearchResult<>();
    private DateSearchResult<ProductWeightTolerance> weightTolerance = new DateSearchResult<>();
    private DateSearchResult<Product> products = new DateSearchResult<>();
    private DateSearchResult<BatchQuantity> batchQuantities = new DateSearchResult<>();
    private DateSearchResult<ProductCategory> productCategories = new DateSearchResult<>();
    private DateSearchResult<MemberGroupPrices> memberGroupPrices = new DateSearchResult<>();
    private DateSearchResult<PrepackageProductItem> prepackageItems = new DateSearchResult<>();
    private DateSearchResult<Promotion> promotions = new DateSearchResult<>();
    private DateSearchResult<Prepackage> prepackages = new DateSearchResult<>();
    private DateSearchResult<ProductPrepackageQuantity> prepackageQuantities = new DateSearchResult<>();
    private DateSearchResult<BarcodeItem> barcodes = new DateSearchResult<>();
    private DateSearchResult<LoyaltyReward> loyaltyRewards = new DateSearchResult<>();
    private DateSearchResult<CashDrawerSession> cashDrawers = new DateSearchResult<>();
    private DateSearchResult<ProductBatch> productBatches = new DateSearchResult<>();
    private DateSearchResult<ShopResult> shops = new DateSearchResult<>();
    private DateSearchResult<MemberGroup> memberGroups = new DateSearchResult<>();
    private DateSearchResult<Contract> contracts = new DateSearchResult<>();
    private DateSearchResult<Doctor> doctors = new DateSearchResult<>();
    private DateSearchResult<Employee> employees = new DateSearchResult<>();
    private DateSearchResult<Vendor> vendors = new DateSearchResult<>();
    private DateSearchResult<ThirdPartyAccount> thirdPartyAccounts = new DateSearchResult<>();


    public DateSearchResult<BarcodeItem> getBarcodes() {
        return barcodes;
    }

    public DateSearchResult<BatchQuantity> getBatchQuantities() {
        return batchQuantities;
    }

    public DateSearchResult<CashDrawerSession> getCashDrawers() {
        return cashDrawers;
    }

    public DateSearchResult<ConsumerCart> getConsumerCarts() {
        return consumerCarts;
    }

    public DateSearchResult<Contract> getContracts() {
        return contracts;
    }

    public DateSearchResult<Doctor> getDoctors() {
        return doctors;
    }

    public DateSearchResult<Employee> getEmployees() {
        return employees;
    }

    public DateSearchResult<LoyaltyReward> getLoyaltyRewards() {
        return loyaltyRewards;
    }

    public DateSearchResult<MemberGroupPrices> getMemberGroupPrices() {
        return memberGroupPrices;
    }

    public DateSearchResult<MemberGroup> getMemberGroups() {
        return memberGroups;
    }

    public SearchResult<Member> getMembers() {
        return members;
    }

    public DateSearchResult<PrepackageProductItem> getPrepackageItems() {
        return prepackageItems;
    }

    public DateSearchResult<ProductPrepackageQuantity> getPrepackageQuantities() {
        return prepackageQuantities;
    }

    public DateSearchResult<Prepackage> getPrepackages() {
        return prepackages;
    }

    public DateSearchResult<ProductBatch> getProductBatches() {
        return productBatches;
    }

    public DateSearchResult<ProductCategory> getProductCategories() {
        return productCategories;
    }

    public DateSearchResult<Product> getProducts() {
        return products;
    }

    public DateSearchResult<Promotion> getPromotions() {
        return promotions;
    }

    public DateSearchResult<ShopResult> getShops() {
        return shops;
    }

    public DateSearchResult<ThirdPartyAccount> getThirdPartyAccounts() {
        return thirdPartyAccounts;
    }

    public DateSearchResult<Transaction> getTransactions() {
        return transactions;
    }

    public DateSearchResult<Vendor> getVendors() {
        return vendors;
    }

    public DateSearchResult<ProductWeightTolerance> getWeightTolerance() {
        return weightTolerance;
    }


    public void setBarcodes(DateSearchResult<BarcodeItem> barcodes) {
        this.barcodes = barcodes;
    }

    public void setBatchQuantities(DateSearchResult<BatchQuantity> batchQuantities) {
        this.batchQuantities = batchQuantities;
    }

    public void setCashDrawers(DateSearchResult<CashDrawerSession> cashDrawers) {
        this.cashDrawers = cashDrawers;
    }

    public void setConsumerCarts(DateSearchResult<ConsumerCart> consumerCarts) {
        this.consumerCarts = consumerCarts;
    }

    public void setContracts(DateSearchResult<Contract> contracts) {
        this.contracts = contracts;
    }

    public void setDoctors(DateSearchResult<Doctor> doctors) {
        this.doctors = doctors;
    }

    public void setEmployees(DateSearchResult<Employee> employees) {
        this.employees = employees;
    }

    public void setLoyaltyRewards(DateSearchResult<LoyaltyReward> loyaltyRewards) {
        this.loyaltyRewards = loyaltyRewards;
    }

    public void setMemberGroupPrices(DateSearchResult<MemberGroupPrices> memberGroupPrices) {
        this.memberGroupPrices = memberGroupPrices;
    }

    public void setMemberGroups(DateSearchResult<MemberGroup> memberGroups) {
        this.memberGroups = memberGroups;
    }

    public void setMembers(SearchResult<Member> members) {
        this.members = members;
    }

    public void setPrepackageItems(DateSearchResult<PrepackageProductItem> prepackageItems) {
        this.prepackageItems = prepackageItems;
    }

    public void setPrepackageQuantities(DateSearchResult<ProductPrepackageQuantity> prepackageQuantities) {
        this.prepackageQuantities = prepackageQuantities;
    }

    public void setPrepackages(DateSearchResult<Prepackage> prepackages) {
        this.prepackages = prepackages;
    }

    public void setProductBatches(DateSearchResult<ProductBatch> productBatches) {
        this.productBatches = productBatches;
    }

    public void setProductCategories(DateSearchResult<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

    public void setProducts(DateSearchResult<Product> products) {
        this.products = products;
    }

    public void setPromotions(DateSearchResult<Promotion> promotions) {
        this.promotions = promotions;
    }

    public void setShops(DateSearchResult<ShopResult> shops) {
        this.shops = shops;
    }

    public void setThirdPartyAccounts(DateSearchResult<ThirdPartyAccount> thirdPartyAccounts) {
        this.thirdPartyAccounts = thirdPartyAccounts;
    }

    public void setTransactions(DateSearchResult<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void setVendors(DateSearchResult<Vendor> vendors) {
        this.vendors = vendors;
    }

    public void setWeightTolerance(DateSearchResult<ProductWeightTolerance> weightTolerance) {
        this.weightTolerance = weightTolerance;
    }
}
