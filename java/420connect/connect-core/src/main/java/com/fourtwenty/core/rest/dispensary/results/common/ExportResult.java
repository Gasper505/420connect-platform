package com.fourtwenty.core.rest.dispensary.results.common;

import java.io.InputStream;

/**
 * Created by mdo on 1/25/17.
 */
public class ExportResult {
    private InputStream stream;
    private String contentType;
    private String name;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }
}
