package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.company.AppRolePermission;
import com.fourtwenty.core.domain.models.company.Role;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppRolePermissionResult extends AppRolePermission {

    List<Role> roles = new ArrayList<>();

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
