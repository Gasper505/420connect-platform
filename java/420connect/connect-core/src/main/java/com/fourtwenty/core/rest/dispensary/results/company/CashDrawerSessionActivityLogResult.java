package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.company.CashDrawerSessionActivityLog;

public class CashDrawerSessionActivityLogResult extends CashDrawerSessionActivityLog implements Comparable<CashDrawerSessionActivityLogResult> {

    private String memberName;
    private String createdByEmployeeName;
    private String transactionNumber;
    private CashDrawerSessionResult cashDrawerSessionResult;

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getCreatedByEmployeeName() {
        return createdByEmployeeName;
    }

    public void setCreatedByEmployeeName(String createdByEmployeeName) {
        this.createdByEmployeeName = createdByEmployeeName;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public CashDrawerSessionResult getCashDrawerSessionResult() {
        return cashDrawerSessionResult;
    }

    public void setCashDrawerSessionResult(CashDrawerSessionResult cashDrawerSessionResult) {
        this.cashDrawerSessionResult = cashDrawerSessionResult;
    }

    @Override
    public int compareTo(CashDrawerSessionActivityLogResult o) {
        if (this.getCreated() > o.getCreated())
            return -1;
        else if (this.getCreated() < o.getCreated())
            return 1;
        else
            return 0;
    }
}
