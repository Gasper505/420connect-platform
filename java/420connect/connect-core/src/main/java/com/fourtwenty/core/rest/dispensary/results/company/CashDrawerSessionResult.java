package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;

/**
 * Created by mdo on 12/7/16.
 */
public class CashDrawerSessionResult extends CashDrawerSession {
    private Employee startEmployee;
    private Employee endEmployee;
    private Terminal terminal;

    public Employee getEndEmployee() {
        return endEmployee;
    }

    public void setEndEmployee(Employee endEmployee) {
        this.endEmployee = endEmployee;
    }

    public Employee getStartEmployee() {
        return startEmployee;
    }

    public void setStartEmployee(Employee startEmployee) {
        this.startEmployee = startEmployee;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }
}