package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.product.Vendor;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyContactResult extends CompanyContact {

    private Vendor customerCompany;

    public Vendor getCustomerCompany() {
        return customerCompany;
    }

    public void setCustomerCompany(Vendor customerCompany) {
        this.customerCompany = customerCompany;
    }
}
