package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.company.DeliveryTaxRate;
import com.fourtwenty.core.domain.models.product.BlazeRegion;

import java.util.List;

public class DeliveryTaxRateResult extends DeliveryTaxRate {

    private List<BlazeRegion> blazeRegions;

    public List<BlazeRegion> getBlazeRegions() {
        return blazeRegions;
    }

    public void setBlazeRegions(List<BlazeRegion> blazeRegions) {
        this.blazeRegions = blazeRegions;
    }
}
