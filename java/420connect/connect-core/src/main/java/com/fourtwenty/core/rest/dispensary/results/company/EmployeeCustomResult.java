package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeCustomResult extends Employee {
    private List<Shop> shopResult = new ArrayList<>();
    private String terminalName;

    private List<EmployeeOnFleetInfoResult> employeeOnFleetInfoResults;

    public List<EmployeeOnFleetInfoResult> getEmployeeOnFleetInfoResults() {
        return employeeOnFleetInfoResults;
    }

    public void setEmployeeOnFleetInfoResults(List<EmployeeOnFleetInfoResult> employeeOnFleetInfoResults) {
        this.employeeOnFleetInfoResults = employeeOnFleetInfoResults;
    }

    public List<Shop> getShopResult() {
        return shopResult;
    }

    public void setShopResult(List<Shop> shopResult) {
        this.shopResult = shopResult;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }
}
