package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.TerminalLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDailyMileageResult {

    private Map<Long, String> driverMileage = new HashMap<>();

    private List<TerminalLocation> terminalLocations = new ArrayList<>();

    public Map<Long, String> getDriverMileage() {
        return driverMileage;
    }

    public void setDriverMileage(Map<Long, String> driverMileage) {
        this.driverMileage = driverMileage;
    }

    public List<TerminalLocation> getTerminalLocations() {
        return terminalLocations;
    }

    public void setTerminalLocations(List<TerminalLocation> terminalLocations) {
        this.terminalLocations = terminalLocations;
    }
}
