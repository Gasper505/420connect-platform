package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Employee;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDistanceResult extends Employee implements Comparable<EmployeeDistanceResult> {

    private boolean assignedToMember;
    private String distance;
    private int distanceValue;
    private int percentageValue;
    private String time;
    private String inventoryRatio;
    private int activeTransaction;
    private String terminalName;

    public int getPercentageValue() {
        return percentageValue;
    }

    public void setPercentageValue(int percentageValue) {
        this.percentageValue = percentageValue;
    }

    public boolean isAssignedToMember() {
        return assignedToMember;
    }

    public void setAssignedToMember(boolean assignedToMember) {
        this.assignedToMember = assignedToMember;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getDistanceValue() {
        return distanceValue;
    }

    public void setDistanceValue(Integer distanceValue) {
        this.distanceValue = distanceValue;
    }

    public void setDistanceValue(int distanceValue) {
        this.distanceValue = distanceValue;
    }

    public String getInventoryRatio() {
        return inventoryRatio;
    }

    public void setInventoryRatio(String inventoryRatio) {
        this.inventoryRatio = inventoryRatio;
    }

    @Override
    public int compareTo(EmployeeDistanceResult obj) {
        int distance1 = this.distanceValue;
        int distance2 = obj.distanceValue;
        if (distance1 <= 0) {
            distance1 = Integer.MAX_VALUE;
        }
        if (distance2 <= 0) {
            distance2 = Integer.MAX_VALUE;
        }


        if (this.getPercentageValue() != obj.getPercentageValue()) {
            return Integer.compare(this.getPercentageValue(),obj.getPercentageValue()) * -1;
        } else if (distance1 != distance2) {
            return Integer.compare(distance1,distance2);
        } else {
            return Integer.compare(this.getActiveTransaction(),obj.getActiveTransaction());
        }
        /*
        if (this.getDistance() == null && obj.getDistance() == null)
            return 0;
        else if (this.getDistance() == null || obj.getDistance() == null) {
            if (this.getDistance() == null) {
                return 1;
            } else if (obj.getDistance() == null) {
                return -1;
            } else if (this.getDistanceValue() == obj.getDistanceValue()) {
                return 0;
            } else if (this.getDistanceValue() > obj.getDistanceValue()) {
                return 1;
            } else
                return -1;
        } else if (this.getDistanceValue() < obj.getDistanceValue()) {
            return -1;
        } else if (this.getDistanceValue() == obj.getDistanceValue()) {
            return 0;
        } else
            return 1;*/
    }

    public int getActiveTransaction() {
        return activeTransaction;
    }

    public void setActiveTransaction(int activeTransaction) {
        this.activeTransaction = activeTransaction;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }
}
