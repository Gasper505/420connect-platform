package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 1/21/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeManifestResult {
    private Company company;
    private Employee employee;
    private List<Transaction> transactions = new ArrayList<Transaction>();

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
