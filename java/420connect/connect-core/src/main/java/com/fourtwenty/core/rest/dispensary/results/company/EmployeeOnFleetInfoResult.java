package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.company.EmployeeOnFleetInfo;

import java.util.HashMap;

public class EmployeeOnFleetInfoResult extends EmployeeOnFleetInfo {

    private String shopName;

    //<OnFleetTeamId, OnFleetTeamName>
    private HashMap<String, String> employeeOnFleetTeams;

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public HashMap<String, String> getEmployeeOnFleetTeams() {
        return employeeOnFleetTeams;
    }

    public void setEmployeeOnFleetTeams(HashMap<String, String> employeeOnFleetTeams) {
        this.employeeOnFleetTeams = employeeOnFleetTeams;
    }
}
