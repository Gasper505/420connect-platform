package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Employee;

import java.util.List;

/**
 * Created by mdo on 12/1/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeResult extends Employee {
    private String terminalName;

    private List<EmployeeOnFleetInfoResult> employeeOnFleetInfoResults;

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public List<EmployeeOnFleetInfoResult> getEmployeeOnFleetInfoResults() {
        return employeeOnFleetInfoResults;
    }

    public void setEmployeeOnFleetInfoResults(List<EmployeeOnFleetInfoResult> employeeOnFleetInfoResults) {
        this.employeeOnFleetInfoResults = employeeOnFleetInfoResults;
    }
}
