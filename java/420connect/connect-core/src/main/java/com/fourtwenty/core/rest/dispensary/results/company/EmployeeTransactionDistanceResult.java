package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public class EmployeeTransactionDistanceResult {

    private SearchResult<EmployeeDistanceResult> employeeDistanceResult;
    private Transaction transaction;
    private ConsumerCart consumerCart;

    public SearchResult<EmployeeDistanceResult> getEmployeeDistanceResult() {
        return employeeDistanceResult;
    }

    public void setEmployeeDistanceResult(SearchResult<EmployeeDistanceResult> employeeDistanceResult) {
        this.employeeDistanceResult = employeeDistanceResult;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public ConsumerCart getConsumerCart() {
        return consumerCart;
    }

    public void setConsumerCart(ConsumerCart consumerCart) {
        this.consumerCart = consumerCart;
    }
}
