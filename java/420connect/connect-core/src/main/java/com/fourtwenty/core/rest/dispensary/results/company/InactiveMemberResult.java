package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 7/31/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InactiveMemberResult {
    private int inactiveMembers;
    private int daysInactive;

    public int getDaysInactive() {
        return daysInactive;
    }

    public void setDaysInactive(int daysInactive) {
        this.daysInactive = daysInactive;
    }

    public int getInactiveMembers() {
        return inactiveMembers;
    }

    public void setInactiveMembers(int inactiveMembers) {
        this.inactiveMembers = inactiveMembers;
    }
}
