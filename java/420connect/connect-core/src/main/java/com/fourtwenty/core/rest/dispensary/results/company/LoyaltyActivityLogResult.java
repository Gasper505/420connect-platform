package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;

/**
 * Created by mdo on 8/16/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoyaltyActivityLogResult extends LoyaltyActivityLog {
    private String memberName;
    private String employeeName;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
}
