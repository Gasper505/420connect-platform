package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 8/4/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoyaltyMemberReward {
    public enum MemberRewardState {
        Used,
        Available,
        Locked
    }

    private String rewardId;
    private String rewardName;
    private String rewardDescription;
    private int pointsRequired;
    private String memberId;
    private MemberRewardState rewardState = MemberRewardState.Locked;
    private Long usedDate;
    private Long availableDate;
    private String discountInfo;

    public String getDiscountInfo() {
        return discountInfo;
    }

    public void setDiscountInfo(String discountInfo) {
        this.discountInfo = discountInfo;
    }

    public Long getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(Long availableDate) {
        this.availableDate = availableDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getPointsRequired() {
        return pointsRequired;
    }

    public void setPointsRequired(int pointsRequired) {
        this.pointsRequired = pointsRequired;
    }

    public String getRewardDescription() {
        return rewardDescription;
    }

    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public MemberRewardState getRewardState() {
        return rewardState;
    }

    public void setRewardState(MemberRewardState rewardState) {
        this.rewardState = rewardState;
    }

    public Long getUsedDate() {
        return usedDate;
    }

    public void setUsedDate(Long usedDate) {
        this.usedDate = usedDate;
    }
}
