package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.MemberGroup;

/**
 * Created by mdo on 7/31/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberGroupResult extends MemberGroup {

}
