package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.customer.CareGiver;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.MemberActivity;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.dispensary.results.CannabisResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 10/30/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberResult extends Member {
    private List<String> expStatuses = new ArrayList<>();
    private List<CareGiver> careGiverList = new ArrayList<>();
    private StateCannabisLimit stateCannabisLimit;
    private HashMap<Product.CannabisType, BigDecimal> currentCannabisLimit;
    private List<CannabisResult> cannabisResult;
    private List<MemberActivity> memberActivities = new ArrayList<>();
    private boolean inQueue;
    private long activeTransaction;

    @Override
    public List<String> getExpStatuses() {
        return expStatuses;
    }

    @Override
    public void setExpStatuses(List<String> expStatuses) {
        this.expStatuses = expStatuses;
    }

    public List<CareGiver> getCareGiverList() {
        return careGiverList;
    }

    public void setCareGiverList(List<CareGiver> careGiverList) {
        this.careGiverList = careGiverList;
    }

    public StateCannabisLimit getStateCannabisLimit() {
        return stateCannabisLimit;
    }

    public void setStateCannabisLimit(StateCannabisLimit stateCannabisLimit) {
        this.stateCannabisLimit = stateCannabisLimit;
    }

    public HashMap<Product.CannabisType, BigDecimal> getCurrentCannabisLimit() {
        return currentCannabisLimit;
    }

    public void setCurrentCannabisLimit(HashMap<Product.CannabisType, BigDecimal> currentCannabisLimit) {
        this.currentCannabisLimit = currentCannabisLimit;
    }

    public List<CannabisResult> getCannabisResult() {
        return cannabisResult;
    }

    public void setCannabisResult(List<CannabisResult> cannabisResult) {
        this.cannabisResult = cannabisResult;
    }

    public List<MemberActivity> getMemberActivities() {
        return memberActivities;
    }

    public void setMemberActivities(List<MemberActivity> memberActivities) {
        this.memberActivities = memberActivities;
    }

    public boolean isInQueue() {
        return inQueue;
    }

    public void setInQueue(boolean inQueue) {
        this.inQueue = inQueue;
    }

    public long getActiveTransaction() {
        return activeTransaction;
    }

    public void setActiveTransaction(long activeTransaction) {
        this.activeTransaction = activeTransaction;
    }
}
