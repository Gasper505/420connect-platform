package com.fourtwenty.core.rest.dispensary.results.company;

import com.fourtwenty.core.domain.models.generic.BaseModel;

import java.util.Set;

public class MemberTagResult extends BaseModel {
    private Set<String> memberTags;

    public Set<String> getMemberTags() {
        return memberTags;
    }

    public void setMemberTags(Set<String> memberTags) {
        this.memberTags = memberTags;
    }

}
