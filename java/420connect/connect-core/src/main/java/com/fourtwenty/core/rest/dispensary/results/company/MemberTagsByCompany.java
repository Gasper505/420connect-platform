package com.fourtwenty.core.rest.dispensary.results.company;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class MemberTagsByCompany {
    @JsonProperty("_id")
    private String id;
    private Set<String> tags;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }
}
