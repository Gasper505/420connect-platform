package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchByCategoryResult {
    @JsonProperty("_id")
    private String productId;
    private List<BatchQuantityResult> batchResult;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<BatchQuantityResult> getBatchResult() {
        return batchResult;
    }

    public void setBatchResult(List<BatchQuantityResult> batchResult) {
        this.batchResult = batchResult;
    }
}
