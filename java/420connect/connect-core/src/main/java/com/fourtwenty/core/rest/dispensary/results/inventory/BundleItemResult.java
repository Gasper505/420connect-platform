package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fourtwenty.core.domain.models.product.BundleItem;

public class BundleItemResult extends BundleItem {

    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
