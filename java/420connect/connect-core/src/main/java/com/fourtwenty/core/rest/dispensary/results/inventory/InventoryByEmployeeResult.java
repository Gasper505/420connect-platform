package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Raja Dushyant Vashishtha
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryByEmployeeResult {

    private String employeeId;
    private String employeeName;
    private String quantity;
    private String prepackage;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrepackage() {
        return prepackage;
    }

    public void setPrepackage(String prepackage) {
        this.prepackage = prepackage;
    }
}
