package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;

/**
 * Created by Raja Dushyant Vashishtha
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryTransferHistoryResult extends InventoryTransferHistory {

    private String fromShopName;
    private String toShopName;
    private String fromInventoryName;
    private String toInventoryName;
    private String createdByEmployeeName;
    private String acceptByEmployeeName;
    private String declineByEmployeeName;

    public String getFromShopName() {
        return fromShopName;
    }

    public void setFromShopName(String fromShopName) {
        this.fromShopName = fromShopName;
    }

    public String getToShopName() {
        return toShopName;
    }

    public void setToShopName(String toShopName) {
        this.toShopName = toShopName;
    }

    public String getFromInventoryName() {
        return fromInventoryName;
    }

    public void setFromInventoryName(String fromInventoryName) {
        this.fromInventoryName = fromInventoryName;
    }

    public String getToInventoryName() {
        return toInventoryName;
    }

    public void setToInventoryName(String toInventoryName) {
        this.toInventoryName = toInventoryName;
    }

    public String getCreatedByEmployeeName() {
        return createdByEmployeeName;
    }

    public void setCreatedByEmployeeName(String createdByEmployeeName) {
        this.createdByEmployeeName = createdByEmployeeName;
    }

    public String getAcceptByEmployeeName() {
        return acceptByEmployeeName;
    }

    public void setAcceptByEmployeeName(String acceptByEmployeeName) {
        this.acceptByEmployeeName = acceptByEmployeeName;
    }

    public String getDeclineByEmployeeName() {
        return declineByEmployeeName;
    }

    public void setDeclineByEmployeeName(String declineByEmployeeName) {
        this.declineByEmployeeName = declineByEmployeeName;
    }
}
