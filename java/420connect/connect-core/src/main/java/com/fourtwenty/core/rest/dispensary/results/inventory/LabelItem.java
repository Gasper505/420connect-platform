package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelQueryItem;

import java.math.BigDecimal;

/**
 * Created by mdo on 3/25/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LabelItem extends LabelQueryItem {
    private BarcodeItem.BarcodeEntityType barcodeEntityType = BarcodeItem.BarcodeEntityType.Product;
    private String categoryName;
    private String productName;
    private String productSKU;
    private String batchSKU;
    private long batchDate;
    private String prepackageLineItemSKU;
    private String labelSKU;
    private String name;
    private String prepackageName;

    private double thc;
    private double cbn;
    private double cbd;
    private String trackPackageLabel;
    private String trackPackageSystem;
    private String flowerType;

    private String vendorName;
    private BigDecimal quantity;
    private String measurement;
    private boolean cannabis;
    private String unitType;
    private String vendorLicenseNumber;
    private String trackHarvestBatch;
    private String trackHarvestDate;
    private String labelInfo;

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(String flowerType) {
        this.flowerType = flowerType;
    }

    public double getCbd() {
        return cbd;
    }

    public void setCbd(double cbd) {
        this.cbd = cbd;
    }

    public double getCbn() {
        return cbn;
    }

    public void setCbn(double cbn) {
        this.cbn = cbn;
    }

    public double getThc() {
        return thc;
    }

    public void setThc(double thc) {
        this.thc = thc;
    }

    public String getTrackPackageLabel() {
        return trackPackageLabel;
    }

    public void setTrackPackageLabel(String trackPackageLabel) {
        this.trackPackageLabel = trackPackageLabel;
    }

    public String getTrackPackageSystem() {
        return trackPackageSystem;
    }

    public void setTrackPackageSystem(String trackPackageSystem) {
        this.trackPackageSystem = trackPackageSystem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrepackageName() {
        return prepackageName;
    }

    public void setPrepackageName(String prepackageName) {
        this.prepackageName = prepackageName;
    }

    public BarcodeItem.BarcodeEntityType getBarcodeEntityType() {
        return barcodeEntityType;
    }

    public void setBarcodeEntityType(BarcodeItem.BarcodeEntityType barcodeEntityType) {
        this.barcodeEntityType = barcodeEntityType;
    }

    public String getLabelSKU() {
        return labelSKU;
    }

    public void setLabelSKU(String labelSKU) {
        this.labelSKU = labelSKU;
    }

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public long getBatchDate() {
        return batchDate;
    }

    public void setBatchDate(long batchDate) {
        this.batchDate = batchDate;
    }

    public String getBatchSKU() {
        return batchSKU;
    }

    public void setBatchSKU(String batchSKU) {
        this.batchSKU = batchSKU;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPrepackageLineItemSKU() {
        return prepackageLineItemSKU;
    }

    public void setPrepackageLineItemSKU(String prepackageLineItemSKU) {
        this.prepackageLineItemSKU = prepackageLineItemSKU;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public boolean isCannabis() {
        return cannabis;
    }

    public void setCannabis(boolean cannabis) {
        this.cannabis = cannabis;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getVendorLicenseNumber() {
        return vendorLicenseNumber;
    }

    public void setVendorLicenseNumber(String vendorLicenseNumber) {
        this.vendorLicenseNumber = vendorLicenseNumber;
    }

    public String getTrackHarvestBatch() {
        return trackHarvestBatch;
    }

    public void setTrackHarvestBatch(String trackHarvestBatch) {
        this.trackHarvestBatch = trackHarvestBatch;
    }

    public String getTrackHarvestDate() {
        return trackHarvestDate;
    }

    public void setTrackHarvestDate(String trackHarvestDate) {
        this.trackHarvestDate = trackHarvestDate;
    }

    public String getLabelInfo() {
        return labelInfo;
    }

    public void setLabelInfo(String labelInfo) {
        this.labelInfo = labelInfo;
    }
}
