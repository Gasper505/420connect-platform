package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LimitedProductResult extends ProductLimitedResult {
    private String vendorId;
    private Set<String> secondaryVendors = new HashSet<>();
    private String brandId;
    private List<ProductQuantity> quantities = new ArrayList<>();
    private String categoryId;
    private boolean active = true;
    private String sku;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0f);
    private List<ProductPriceRange> priceRanges = new ArrayList<>();
    private ProductCategory category;
    private String brandName;
    private String companyLinkId;

    public String getVendorId() {
        return vendorId;
    }

    public LimitedProductResult setVendorId(String vendorId) {
        this.vendorId = vendorId;
        return this;
    }

    public Set<String> getSecondaryVendors() {
        return secondaryVendors;
    }

    public LimitedProductResult setSecondaryVendors(Set<String> secondaryVendors) {
        this.secondaryVendors = secondaryVendors;
        return this;
    }

    public String getBrandId() {
        return brandId;
    }

    public LimitedProductResult setBrandId(String brandId) {
        this.brandId = brandId;
        return this;
    }

    public List<ProductQuantity> getQuantities() {
        return quantities;
    }

    public LimitedProductResult setQuantities(List<ProductQuantity> quantities) {
        this.quantities = quantities;
        return this;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public LimitedProductResult setCategoryId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public LimitedProductResult setActive(boolean active) {
        this.active = active;
        return this;
    }

    public String getSku() {
        return sku;
    }

    public LimitedProductResult setSku(String sku) {
        this.sku = sku;
        return this;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public LimitedProductResult setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public List<ProductPriceRange> getPriceRanges() {
        return priceRanges;
    }

    public LimitedProductResult setPriceRanges(List<ProductPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
        return this;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public LimitedProductResult setCategory(ProductCategory category) {
        this.category = category;
        return this;
    }

    public String getBrandName() {
        return brandName;
    }

    public LimitedProductResult setBrandName(String brandName) {
        this.brandName = brandName;
        return this;
    }

    public String getCompanyLinkId() {
        String key = this.getSku();//String.format("%s_%s_%s_%s", categoryName, this.getName().toLowerCase(), this.getVendorId(), unitType);

        String encoded = Base64.getEncoder().encodeToString(key.getBytes(Charset.forName("UTF8")));
        return encoded;
    }

    public LimitedProductResult setCompanyLinkId(String companyLinkId) {
        this.companyLinkId = companyLinkId;
        return this;
    }
}
