package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.product.MemberGroupPrices;

/**
 * Created by mdo on 5/24/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberGroupPricesResult extends MemberGroupPrices {
    private MemberGroup memberGroup;

    public MemberGroup getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(MemberGroup memberGroup) {
        this.memberGroup = memberGroup;
    }
}
