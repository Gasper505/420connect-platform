package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import java.util.List;

/**
 * Created by decipher on 23/10/17 5:59 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class POProductRequestResult extends POProductRequest {

    private Product product;
    private boolean cannabis;
    private List<BundleItemResult> bundleItems;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public boolean isCannabis() {
        return cannabis;
    }

    public void setCannabis(boolean cannabis) {
        this.cannabis = cannabis;
    }

    public List<BundleItemResult> getBundleItems() {
        return bundleItems;
    }

    public void setBundleItems(List<BundleItemResult> bundleItems) {
        this.bundleItems = bundleItems;
    }

    public POProductRequestResult(POProductRequest poProductRequest) {

        setDeclineReason(poProductRequest.getDeclineReason());
        setExciseTax(poProductRequest.getExciseTax());
        setNotes(poProductRequest.getNotes());
        setProductId(poProductRequest.getProductId());
        setProductName(poProductRequest.getProductName());
        setReceivedQuantity(poProductRequest.getReceivedQuantity());
        setRequestQuantity(poProductRequest.getRequestQuantity());
        setRequestStatus(poProductRequest.getRequestStatus());
        setTotalCost(poProductRequest.getTotalCost());
        setTotalExciseTax(poProductRequest.getTotalExciseTax());
        setTrackingPackagesList(poProductRequest.getTrackingPackagesList());
        setUnitPrice(poProductRequest.getUnitPrice());
        setTrackTraceSystem(poProductRequest.getTrackTraceSystem());

        setShopId(poProductRequest.getShopId());
        setDirty(poProductRequest.isDirty());

        setCompanyId(poProductRequest.getCompanyId());

        setCreated(poProductRequest.getCreated());
        setUpdated(poProductRequest.isUpdated());
        setModified(poProductRequest.getModified());
        setDeleted(poProductRequest.isDeleted());

        setDiscount(poProductRequest.getDiscount());
        setFinalTotalCost(poProductRequest.getFinalTotalCost());
        setId(poProductRequest.getId());
    }

    public POProductRequestResult() {
    }
}
