package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Prepackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 3/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrepackageGroupResult extends Prepackage {
    private int totalQuantity = 0;
    private List<PrepackageProductItemView> prepackages = new ArrayList<>();

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public List<PrepackageProductItemView> getPrepackages() {
        return prepackages;
    }

    public void setPrepackages(List<PrepackageProductItemView> prepackages) {
        this.prepackages = prepackages;
    }
}
