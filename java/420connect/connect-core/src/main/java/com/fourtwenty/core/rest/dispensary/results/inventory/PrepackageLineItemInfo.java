package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductBatch;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PrepackageLineItemInfo extends PrepackageProductItem {

    private ProductBatch productBatch;
    private Prepackage prepackage;

    public ProductBatch getProductBatch() {
        return productBatch;
    }

    public void setProductBatch(ProductBatch productBatch) {
        this.productBatch = productBatch;
    }

    public Prepackage getPrepackage() {
        return prepackage;
    }

    public void setPrepackage(Prepackage prepackage) {
        this.prepackage = prepackage;
    }
}
