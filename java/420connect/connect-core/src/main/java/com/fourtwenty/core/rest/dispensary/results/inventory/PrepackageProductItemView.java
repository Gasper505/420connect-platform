package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 4/1/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrepackageProductItemView extends PrepackageProductItem {
    private int quantity = 0;
    private List<ProductPrepackageQuantity> quantities = new ArrayList<>();
    private ProductBatch batch;

    public ProductBatch getBatch() {
        return batch;
    }

    public void setBatch(ProductBatch batch) {
        this.batch = batch;
    }

    public List<ProductPrepackageQuantity> getQuantities() {
        return quantities;
    }

    public void setQuantities(List<ProductPrepackageQuantity> quantities) {
        this.quantities = quantities;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
