package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Product;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductByVendorResult extends Product {

    @DecimalMin("0")
    private BigDecimal quantityAvailable;
    @DecimalMin("0")
    private BigDecimal quantityInPO;

    public BigDecimal getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(BigDecimal quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public BigDecimal getQuantityInPO() {
        return quantityInPO;
    }

    public void setQuantityInPO(BigDecimal quantityInPO) {
        this.quantityInPO = quantityInPO;
    }
}
