package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.ShopBaseModel;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponseHit;
import org.json.JSONObject;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCustomResult extends ShopBaseModel implements ElasticSearchCapable {

    private String id;
    private String name;
    private List<CompanyAsset> assets = new ArrayList<>();
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal unitPrice = new BigDecimal(0f);
    private String flowerType;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantityAvailable = new BigDecimal(0);
    private boolean active = true;
    private ProductCategory.UnitType unitType = ProductCategory.UnitType.units;
    private CompanyAsset productCategoryPhoto;
    private String sku;
    private String vendorName;
    private boolean showCostInWidget = Boolean.FALSE;
    private String armsLengthType;
    private String brandName;


    public ProductCustomResult() {
    }

    public ProductCustomResult(Product product) {
        setId(product.getId());
        setName(product.getName());
        setFlowerType(product.getFlowerType());
        setActive(product.isActive());
        setBrandName(product.getBrand() != null ? product.getBrand().getName() : "");
        setVendorName(product.getVendor() != null ? product.getVendor().getName() : "");
        setShowCostInWidget(product.isShowInWidget());
    }


    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CompanyAsset> getAssets() {
        return assets;
    }

    public void setAssets(List<CompanyAsset> assets) {
        this.assets = assets;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(String flowerType) {
        this.flowerType = flowerType;
    }

    public BigDecimal getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(BigDecimal quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ProductCategory.UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(ProductCategory.UnitType unitType) {
        this.unitType = unitType;
    }

    public CompanyAsset getProductCategoryPhoto() {
        return productCategoryPhoto;
    }

    public void setProductCategoryPhoto(CompanyAsset productCategoryPhoto) {
        this.productCategoryPhoto = productCategoryPhoto;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public boolean isShowCostInWidget() {
        return showCostInWidget;
    }

    public void setShowCostInWidget(boolean showCostInWidget) {
        this.showCostInWidget = showCostInWidget;
    }

    public String getArmsLengthType() {
        return armsLengthType;
    }

    public void setArmsLengthType(String armsLengthType) {
        this.armsLengthType = armsLengthType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }


    ///
    /// Elastic Search Support
    ///

    @Override
    public ElasticSearchIndex getElasticSearchIndex() {
        ElasticSearchIndex index = new ElasticSearchIndex();
        index.setIndex("products");
        index.setType("product");

        final Map<String, FieldIndex> properties = new HashMap<>();
        properties.put("companyId", new FieldIndex("keyword", null, true));
        properties.put("shopId", new FieldIndex("keyword", null, true));
        properties.put("name", new FieldIndex("keyword", null, true));
        properties.put("flowerType", new FieldIndex("keyword", null, true));
        properties.put("active", new FieldIndex("keyword", null, true));
        properties.put("productCategoryPhoto", new FieldIndex("keyword", null, true));
        properties.put("brandName", new FieldIndex("keyword", null, true));
        properties.put("vendorName", new FieldIndex("keyword", null, true));
        properties.put("showCostInWidget", new FieldIndex("keyword", null, true));
        properties.put("deleted", new FieldIndex("keyword", null, true));
        index.setProperties(properties);

        return index;
    }

    @Override
    public void loadFrom(AWSSearchResponseHit hit) {
        for (String key : hit.source.keySet()) {
            switch (key) {
                case "companyId":
                    setCompanyId(hit.source.get(key).toString());
                    break;
                case "shopId":
                    setShopId(hit.source.get(key).toString());
                    break;
                case "name":
                    setName(hit.source.get(key).toString());
                    break;
                case "flowerType":
                    setFlowerType(hit.source.get(key).toString());
                    break;
                case "active":
                    setActive(new Boolean(hit.source.get(key).toString()));
                    break;
                case "productCategoryPhoto":
                    CompanyAsset asset = new CompanyAsset();
                    asset.setThumbURL(hit.source.get(key).toString());
                    setProductCategoryPhoto(asset);
                    break;
                case "brandName":
                    setBrandName(hit.source.get(key).toString());
                    break;
                case "vendorName":
                    setVendorName(hit.source.get(key).toString());
                    break;
                case "showInWidget":
                    setShowCostInWidget(new Boolean(hit.source.get(key).toString()));
                    break;
            }
        }
    }

    @Override
    public JSONObject toElasticSearchObject() {
        final JSONObject jsonObject = new JSONObject();

        jsonObject.put("companyId", getCompanyId());
        jsonObject.put("shopId", getShopId());
        jsonObject.put("name", getName());
        jsonObject.put("flowerType", getFlowerType());
        jsonObject.put("active", isActive());
        jsonObject.put("productCategoryPhoto", getProductCategoryPhoto() != null ?
                getProductCategoryPhoto().getThumbURL() : "");
        jsonObject.put("brandName", getBrandName());
        jsonObject.put("vendorName", getVendorName());
        jsonObject.put("showInWidget", isShowCostInWidget());
        jsonObject.put("deleted", isDeleted());

        return jsonObject;
    }
}
