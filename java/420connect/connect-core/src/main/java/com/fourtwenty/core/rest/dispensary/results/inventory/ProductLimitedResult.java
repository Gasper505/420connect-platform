package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fourtwenty.core.domain.models.generic.ShopBaseModel;

/**
 * Created by mdo on 1/15/18.
 */
public class ProductLimitedResult extends ShopBaseModel {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
