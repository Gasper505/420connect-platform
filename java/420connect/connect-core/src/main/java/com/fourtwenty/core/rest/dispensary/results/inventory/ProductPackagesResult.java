package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductQuantity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by decipher on 2/12/17 2:11 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class ProductPackagesResult {

    private String id;
    private String productName;
    private List<ProductQuantity> quantities = new ArrayList<>();
    private ProductCategory category;
    private Prepackage prepackage;
    private double prePackageQuantity;
    private String inventoryName;
    private Boolean isPrePackage = Boolean.FALSE;
    private String brandName;
    private String productSku;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<ProductQuantity> getQuantities() {
        return quantities;
    }

    public void setQuantities(List<ProductQuantity> quantities) {
        this.quantities = quantities;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public Prepackage getPrepackage() {
        return prepackage;
    }

    public void setPrepackage(Prepackage prepackage) {
        this.prepackage = prepackage;
    }

    public double getPrePackageQuantity() {
        return prePackageQuantity;
    }

    public void setPrePackageQuantity(double prePackageQuantity) {
        this.prePackageQuantity = prePackageQuantity;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Boolean getPrePackage() {
        return isPrePackage;
    }

    public void setPrePackage(Boolean prePackage) {
        isPrePackage = prePackage;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductSku() {
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }
}
