package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.HoldQuantityInfo;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPrepackageQuantityResult extends HoldQuantityInfo {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
