package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPriceBreakResult {
    private List<ProductPriceBreak> priceBreaks = new ArrayList<>();

    public List<ProductPriceBreak> getPriceBreaks() {
        return priceBreaks;
    }

    public void setPriceBreaks(List<ProductPriceBreak> priceBreaks) {
        this.priceBreaks = priceBreaks;
    }
}
