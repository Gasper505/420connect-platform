package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.thirdparty.ProductTagGroups;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 11/19/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResult extends Product {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal quantityAvailable = new BigDecimal(0);
    private String brandName;
    private List<BundleItemResult> bundleItemResults = new ArrayList<>();
    private List<Vendor> secondaryVendorResult = new ArrayList<>();
    private String thirdPartyProductName;
    private String thirdPartyBrandName;
    @Deprecated
    private String tagGroupName;
    @Deprecated
    private String discoveryTagName;
    private List<String> wmTags = new ArrayList<>();
    private String thirdPartyBrandId;
    private String thirdPartyProductId;
    @Deprecated
    private String tagGroupId;
    @Deprecated
    private String discoveryTagId;
    private List<ProductTagGroups> productTagGroups = new ArrayList<>();

    public BigDecimal getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(BigDecimal quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public List<BundleItemResult> getBundleItemResults() {
        return bundleItemResults;
    }

    public void setBundleItemResults(List<BundleItemResult> bundleItemResults) {
        this.bundleItemResults = bundleItemResults;
    }

    public List<Vendor> getSecondaryVendorResult() {
        return secondaryVendorResult;
    }

    public void setSecondaryVendorResult(List<Vendor> secondaryVendorResult) {
        this.secondaryVendorResult = secondaryVendorResult;
    }

    public String getThirdPartyProductName() {
        return thirdPartyProductName;
    }

    public void setThirdPartyProductName(String thirdPartyProductName) {
        this.thirdPartyProductName = thirdPartyProductName;
    }

    public String getThirdPartyBrandName() {
        return thirdPartyBrandName;
    }

    public void setThirdPartyBrandName(String thirdPartyBrandName) {
        this.thirdPartyBrandName = thirdPartyBrandName;
    }

    public String getTagGroupName() {
        return tagGroupName;
    }

    public void setTagGroupName(String tagGroupName) {
        this.tagGroupName = tagGroupName;
    }

    public String getDiscoveryTagName() {
        return discoveryTagName;
    }

    public void setDiscoveryTagName(String discoveryTagName) {
        this.discoveryTagName = discoveryTagName;
    }

    public List<String> getWmTags() {
        return wmTags;
    }

    public void setWmTags(List<String> wmTags) {
        this.wmTags = wmTags;
    }

    public String getThirdPartyBrandId() {
        return thirdPartyBrandId;
    }

    public void setThirdPartyBrandId(String thirdPartyBrandId) {
        this.thirdPartyBrandId = thirdPartyBrandId;
    }

    public String getThirdPartyProductId() {
        return thirdPartyProductId;
    }

    public void setThirdPartyProductId(String thirdPartyProductId) {
        this.thirdPartyProductId = thirdPartyProductId;
    }

    public String getTagGroupId() {
        return tagGroupId;
    }

    public void setTagGroupId(String tagGroupId) {
        this.tagGroupId = tagGroupId;
    }

    public String getDiscoveryTagId() {
        return discoveryTagId;
    }

    public void setDiscoveryTagId(String discoveryTagId) {
        this.discoveryTagId = discoveryTagId;
    }

    public List<ProductTagGroups> getProductTagGroups() {
        return productTagGroups;
    }

    public void setProductTagGroups(List<ProductTagGroups> productTagGroups) {
        this.productTagGroups = productTagGroups;
    }
}
