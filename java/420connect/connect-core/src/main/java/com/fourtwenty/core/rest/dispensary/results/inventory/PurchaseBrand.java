package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Comparator;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseBrand implements Comparator<PurchaseBrand> {
    private String brandId;
    private BigDecimal quantity;
    private String brandName;

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    @Override
    public int compare(PurchaseBrand o1, PurchaseBrand o2) {
        return o1.getQuantity().compareTo(o2.getQuantity());
    }
}
