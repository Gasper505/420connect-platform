package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;

import java.util.List;

/**
 * Created by decipher on 23/10/17 5:58 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseOrderItemResult extends PurchaseOrder {

    private Vendor vendor;

    private Employee approvedByMember;

    private List<POProductRequestResult> poProductRequestResultList;

    private List<POActivity> poActivityLog;

    private String companyLogo;

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Employee getApprovedByMember() {
        return approvedByMember;
    }

    public void setApprovedByMember(Employee approvedByMember) {
        this.approvedByMember = approvedByMember;
    }

    public List<POProductRequestResult> getPoProductRequestResultList() {
        return poProductRequestResultList;
    }

    public void setPoProductRequestResultList(List<POProductRequestResult> poProductRequestResultList) {
        this.poProductRequestResultList = poProductRequestResultList;
    }

    public List<POActivity> getPoActivityLog() {
        return poActivityLog;
    }

    public void setPoActivityLog(List<POActivity> poActivityLog) {
        this.poActivityLog = poActivityLog;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }
}
