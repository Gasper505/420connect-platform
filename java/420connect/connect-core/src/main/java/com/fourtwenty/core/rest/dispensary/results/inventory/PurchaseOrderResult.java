package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;

import java.util.LinkedHashSet;

public class PurchaseOrderResult extends PurchaseOrderItemResult {
    private ShipmentBill shipmentBill;
    private boolean isReceived;
    private boolean isMetrc;
    private LinkedHashSet<String> metrcId;

    public ShipmentBill getShipmentBill() {
        return shipmentBill;
    }

    public void setShipmentBill(ShipmentBill shipmentBill) {
        this.shipmentBill = shipmentBill;
    }

    public boolean isReceived() {
        return isReceived;
    }

    public void setReceived(boolean received) {
        isReceived = received;
    }

    public boolean isMetrc() {
        return isMetrc;
    }

    public void setMetrc(boolean metrc) {
        isMetrc = metrc;
    }

    public LinkedHashSet<String> getMetrcId() {
        return metrcId;
    }

    public void setMetrcId(LinkedHashSet<String> metrcId) {
        this.metrcId = metrcId;
    }
}

