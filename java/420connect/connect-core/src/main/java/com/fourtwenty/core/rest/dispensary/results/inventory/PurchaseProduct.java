package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Comparator;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseProduct implements Comparator<PurchaseProduct> {
    private String productId;
    private BigDecimal quantity = BigDecimal.ZERO;
    private String productName;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public int compare(PurchaseProduct o1, PurchaseProduct o2) {
        return o1.getQuantity().compareTo(o2.getQuantity());
    }
}
