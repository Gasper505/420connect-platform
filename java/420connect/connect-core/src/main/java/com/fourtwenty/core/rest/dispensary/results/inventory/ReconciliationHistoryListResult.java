package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fourtwenty.core.rest.dispensary.requests.inventory.ReconciliationHistoryList;

public class ReconciliationHistoryListResult extends ReconciliationHistoryList {
    private String trackHarvestBatchId;
    private long batchPurchaseDate;
    private String trackPackageLabel;

    private ReconciliationHistoryListResult() {}

    public ReconciliationHistoryListResult(ReconciliationHistoryList reconciliationHistoryList) {
        setOperations(reconciliationHistoryList.getOperations());
        setProductId(reconciliationHistoryList.getProductId());
        setProductName(reconciliationHistoryList.getProductName());
        setOldQuantity(reconciliationHistoryList.getOldQuantity());
        setNewQuantity(reconciliationHistoryList.getNewQuantity());
        setPrePackageItemId(reconciliationHistoryList.getPrePackageItemId());
        setTerminalId(reconciliationHistoryList.getTerminalId());
        setInventoryId(reconciliationHistoryList.getInventoryId());
        setReportLoss(reconciliationHistoryList.isReportLoss());
        setReconciliationReason(reconciliationHistoryList.getReconciliationReason());
        setNote(reconciliationHistoryList.getNote());
        setBatchId(reconciliationHistoryList.getBatchId());
        setBatchSku(reconciliationHistoryList.getBatchSku());
        setBrandName(reconciliationHistoryList.getBrandName());

    }
    public String getTrackHarvestBatchId() {
        return trackHarvestBatchId;
    }

    public void setTrackHarvestBatchId(String trackHarvestBatchId) {
        this.trackHarvestBatchId = trackHarvestBatchId;
    }

    public long getBatchPurchaseDate() {
        return batchPurchaseDate;
    }

    public void setBatchPurchaseDate(long batchPurchaseDate) {
        this.batchPurchaseDate = batchPurchaseDate;
    }

    public String getTrackPackageLabel() {
        return trackPackageLabel;
    }

    public void setTrackPackageLabel(String trackPackageLabel) {
        this.trackPackageLabel = trackPackageLabel;
    }
}
