package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ReconciliationHistory;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReconciliationHistoryResult extends ReconciliationHistory {
    private String inventoryName;
    private String categoryName;
    private ProductCategory.UnitType categoryUnitType;
    private String terminalName;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalLoss;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal allTotalLoss;
    private boolean reportLoss;
    private String employeeName;
    private List<ReconciliationHistoryListResult> reconciliationsResult = new ArrayList<>();


    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public BigDecimal getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(BigDecimal totalLoss) {
        this.totalLoss = totalLoss;
    }

    public BigDecimal getAllTotalLoss() {
        return allTotalLoss;
    }

    public void setAllTotalLoss(BigDecimal allTotalLoss) {
        this.allTotalLoss = allTotalLoss;
    }

    public boolean isReportLoss() {
        return reportLoss;
    }

    public void setReportLoss(boolean reportLoss) {
        this.reportLoss = reportLoss;
    }

    public ProductCategory.UnitType getCategoryUnitType() {
        return categoryUnitType;
    }

    public void setCategoryUnitType(ProductCategory.UnitType categoryUnitType) {
        this.categoryUnitType = categoryUnitType;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public List<ReconciliationHistoryListResult> getReconciliationsResult() {
        return reconciliationsResult;
    }

    public void setReconciliationsResult(List<ReconciliationHistoryListResult> reconciliationsResult) {
        this.reconciliationsResult = reconciliationsResult;
    }
}
