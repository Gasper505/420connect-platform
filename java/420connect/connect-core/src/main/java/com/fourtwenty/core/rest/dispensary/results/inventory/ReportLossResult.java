package com.fourtwenty.core.rest.dispensary.results.inventory;

/**
 * Created by mdo on 7/21/16.
 */
public class ReportLossResult {
    private String productId;
    private double newQuantity;

    public double getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(double newQuantity) {
        this.newQuantity = newQuantity;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
