package com.fourtwenty.core.rest.dispensary.results.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;

import java.util.List;

/**
 * Created by decipher on 26/10/17 4:54 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipmentBillRequestResult extends ShipmentBill {
    private Vendor vendor;
    private PurchaseOrder purchaseOrder;
    private PurchaseOrder backOrder;
    private String backOrderPOId;

    private List<POProductRequestResult> poProductRequestResultList;

    public PurchaseOrder getBackOrder() {
        return backOrder;
    }

    public void setBackOrder(PurchaseOrder backOrder) {
        this.backOrder = backOrder;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public String getBackOrderPOId() {
        return backOrderPOId;
    }

    public void setBackOrderPOId(String backOrderPOId) {
        this.backOrderPOId = backOrderPOId;
    }

    public List<POProductRequestResult> getPoProductRequestResultList() {
        return poProductRequestResultList;
    }

    public void setPoProductRequestResultList(List<POProductRequestResult> poProductRequestResultList) {
        this.poProductRequestResultList = poProductRequestResultList;
    }
}
