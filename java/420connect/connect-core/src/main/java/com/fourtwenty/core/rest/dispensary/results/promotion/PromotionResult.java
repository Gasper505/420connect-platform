package com.fourtwenty.core.rest.dispensary.results.promotion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.loyalty.Promotion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionResult extends Promotion {
    private String startTime;
    private String endTime;


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
