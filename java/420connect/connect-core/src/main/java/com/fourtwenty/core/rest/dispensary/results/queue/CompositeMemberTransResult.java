package com.fourtwenty.core.rest.dispensary.results.queue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Transaction;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompositeMemberTransResult {
    private Transaction transaction;
    private Member member;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
