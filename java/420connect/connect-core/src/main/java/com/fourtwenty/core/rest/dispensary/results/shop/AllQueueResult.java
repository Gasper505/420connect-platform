package com.fourtwenty.core.rest.dispensary.results.shop;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 6/30/16.
 */
public class AllQueueResult {
    private SearchResult<Transaction> walkin = new SearchResult<>();
    private SearchResult<Transaction> delivery = new SearchResult<>();
    private SearchResult<Transaction> online = new SearchResult<>();
    private SearchResult<DashboardQueueResult> incoming = new SearchResult<>();
    private SearchResult<Transaction> special = new SearchResult<>();


    public SearchResult<DashboardQueueResult> getIncoming() {
        return incoming;
    }

    public void setIncoming(SearchResult<DashboardQueueResult> incoming) {
        this.incoming = incoming;
    }

    public SearchResult<Transaction> getDelivery() {
        return delivery;
    }

    public void setDelivery(SearchResult<Transaction> delivery) {
        this.delivery = delivery;
    }

    public SearchResult<Transaction> getOnline() {
        return online;
    }

    public void setOnline(SearchResult<Transaction> online) {
        this.online = online;
    }

    public SearchResult<Transaction> getWalkin() {
        return walkin;
    }

    public void setWalkin(SearchResult<Transaction> walkin) {
        this.walkin = walkin;
    }

    public SearchResult<Transaction> getSpecial() {
        return special;
    }

    public void setSpecial(SearchResult<Transaction> special) {
        this.special = special;
    }
}
