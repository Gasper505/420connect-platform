package com.fourtwenty.core.rest.dispensary.results.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Transaction;


/**
 * Created by mdo on 5/21/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerCartResult extends ConsumerCart {
    private boolean membershipAccepted = false;
    private ConsumerUser consumerUser;
    private Member member;
    private Transaction transaction;
    private Shop shop;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public ConsumerUser getConsumerUser() {
        return consumerUser;
    }

    public void setConsumerUser(ConsumerUser consumerUser) {
        this.consumerUser = consumerUser;
    }

    public boolean isMembershipAccepted() {
        return membershipAccepted;
    }

    public void setMembershipAccepted(boolean membershipAccepted) {
        this.membershipAccepted = membershipAccepted;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
