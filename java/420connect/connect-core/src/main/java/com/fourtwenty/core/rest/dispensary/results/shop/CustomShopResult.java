package com.fourtwenty.core.rest.dispensary.results.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomShopResult extends Shop {

    private ExciseTaxInfo exciseTaxInfo;
    private ExciseTaxInfo nonCannabisTaxInfo;

    public ExciseTaxInfo getExciseTaxInfo() {
        return exciseTaxInfo;
    }

    public void setExciseTaxInfo(ExciseTaxInfo exciseTaxInfo) {
        this.exciseTaxInfo = exciseTaxInfo;
    }

    public ExciseTaxInfo getNonCannabisTaxInfo() {
        return nonCannabisTaxInfo;
    }

    public void setNonCannabisTaxInfo(ExciseTaxInfo nonCannabisTaxInfo) {
        this.nonCannabisTaxInfo = nonCannabisTaxInfo;
    }
}
