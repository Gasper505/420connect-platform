package com.fourtwenty.core.rest.dispensary.results.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 8/30/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardQueueResult {
    private String orderNo;
    private String memberName;
    private Long startTime;


    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
}
