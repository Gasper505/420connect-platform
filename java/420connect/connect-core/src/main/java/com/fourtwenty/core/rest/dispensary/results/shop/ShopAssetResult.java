package com.fourtwenty.core.rest.dispensary.results.shop;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopAssetResult {

    private String shopAssetToken;
    private boolean copied;

    public String getShopAssetToken() {
        return shopAssetToken;
    }

    public void setShopAssetToken(String shopAssetToken) {
        this.shopAssetToken = shopAssetToken;
    }

    public boolean isCopied() {
        return copied;
    }

    public void setCopied(boolean copied) {
        this.copied = copied;
    }
}
