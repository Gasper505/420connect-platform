package com.fourtwenty.core.rest.dispensary.results.shop;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopAssetToken {

    private String folderName;
    private long expirationDate;
    private String shopId;
    private boolean validToken;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public boolean isValidToken() {
        DateTime time = new DateTime(expirationDate);
        return time.isAfterNow();
    }

    public void setValidToken(boolean validToken) {
        this.validToken = validToken;
    }

}
