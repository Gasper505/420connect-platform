package com.fourtwenty.core.rest.dispensary.results.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;

/**
 * Created by mdo on 11/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopResult extends Shop {
    private boolean enableMetrc = false;
    private boolean enableDeliveryMessaging = false;
    private ExciseTaxInfo exciseTaxInfo;
    private ExciseTaxInfo nonCannabisTaxInfo;


    // Metrc related stuff -- accessible by grow shops
    private String metrcLicense;
    private String metrcAuthToken;
    private String metrcURLPath;


    public String getMetrcAuthToken() {
        return metrcAuthToken;
    }

    public void setMetrcAuthToken(String metrcAuthToken) {
        this.metrcAuthToken = metrcAuthToken;
    }

    public String getMetrcURLPath() {
        return metrcURLPath;
    }

    public void setMetrcURLPath(String metrcURLPath) {
        this.metrcURLPath = metrcURLPath;
    }

    public ExciseTaxInfo getExciseTaxInfo() {
        return exciseTaxInfo;
    }

    public void setExciseTaxInfo(ExciseTaxInfo exciseTaxInfo) {
        this.exciseTaxInfo = exciseTaxInfo;
    }

    public boolean isEnableDeliveryMessaging() {
        return enableDeliveryMessaging;
    }

    public void setEnableDeliveryMessaging(boolean enableDeliveryMessaging) {
        this.enableDeliveryMessaging = enableDeliveryMessaging;
    }

    public boolean isEnableMetrc() {
        return enableMetrc;
    }

    public void setEnableMetrc(boolean enableMetrc) {
        this.enableMetrc = enableMetrc;
    }

    public String getMetrcLicense() {
        return metrcLicense;
    }

    public void setMetrcLicense(String metrcLicense) {
        this.metrcLicense = metrcLicense;
    }

    public ExciseTaxInfo getNonCannabisTaxInfo() {
        return nonCannabisTaxInfo;
    }

    public void setNonCannabisTaxInfo(ExciseTaxInfo nonCannabisTaxInfo) {
        this.nonCannabisTaxInfo = nonCannabisTaxInfo;
    }
}
