package com.fourtwenty.core.rest.dispensary.results.tvdisplay;

import java.util.List;

public class EmployeeStatsResponse<E> {
    private String employeeName;
    private List<E> resultList;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public List getResultList() {
        return resultList;
    }

    public void setResultList(List resultList) {
        this.resultList = resultList;
    }
}
