package com.fourtwenty.core.rest.dispensary.results.tvdisplay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.serializers.TruncateTwoDigitsSerializer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeStatsResult {
    private List<EmployeeStatsResponse> brandDetails;
    private List<EmployeeStatsResponse> productDetails;
    private long totalVisits;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal totalSales = BigDecimal.ZERO;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal averageSales = BigDecimal.ZERO;
    private List<SalesByCategory> salesByCategory = new ArrayList<>();
    private List<SalesByBrand> salesByBrands = new ArrayList<>();
    private Integer refundSales = 0;
    private Integer onlineOrders = 0;
    private List<SalesByMemberType> salesByConsumer = new ArrayList<>();
    private List<SalesByBudTender> budTenderSale = new ArrayList<>();
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal onlineOrdersPercent = BigDecimal.ZERO;
    @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
    private BigDecimal refundSalesPercent = BigDecimal.ZERO;

    public EmployeeStatsResult(List<EmployeeStatsResponse> brandDetails, List<EmployeeStatsResponse> productDetails) {
        this.brandDetails = brandDetails;
        this.productDetails = productDetails;
    }

    public List<EmployeeStatsResponse> getBrandDetails() {
        return brandDetails;
    }

    public void setBrandDetails(List<EmployeeStatsResponse> brandDetails) {
        this.brandDetails = brandDetails;
    }

    public List<EmployeeStatsResponse> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(List<EmployeeStatsResponse> productDetails) {
        this.productDetails = productDetails;
    }

    public long getTotalVisits() {
        return totalVisits;
    }

    public void setTotalVisits(long totalVisits) {
        this.totalVisits = totalVisits;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public BigDecimal getAverageSales() {
        return averageSales;
    }

    public void setAverageSales(BigDecimal averageSales) {
        this.averageSales = averageSales;
    }

    public List<SalesByCategory> getSalesByCategory() {
        return salesByCategory;
    }

    public void setSalesByCategory(List<SalesByCategory> salesByCategory) {
        this.salesByCategory = salesByCategory;
    }

    public List<SalesByBrand> getSalesByBrands() {
        return salesByBrands;
    }

    public void setSalesByBrands(List<SalesByBrand> salesByBrands) {
        this.salesByBrands = salesByBrands;
    }

    public List<SalesByMemberType> getSalesByConsumer() {
        return salesByConsumer;
    }

    public void setSalesByConsumer(List<SalesByMemberType> salesByConsumer) {
        this.salesByConsumer = salesByConsumer;
    }


    public Integer getRefundSales() {
        return refundSales;
    }

    public void setRefundSales(Integer refundSales) {
        this.refundSales = refundSales;
    }

    public Integer getOnlineOrders() {
        return onlineOrders;
    }

    public void setOnlineOrders(Integer onlineOrders) {
        this.onlineOrders = onlineOrders;
    }


    public List<SalesByBudTender> getBudTenderSale() {
        return budTenderSale;
    }

    public void setBudTenderSale(List<SalesByBudTender> budTenderSale) {
        this.budTenderSale = budTenderSale;
    }

    public BigDecimal getOnlineOrdersPercent() {
        return onlineOrdersPercent;
    }

    public void setOnlineOrdersPercent(BigDecimal onlineOrdersPercent) {
        this.onlineOrdersPercent = onlineOrdersPercent;
    }

    public BigDecimal getRefundSalesPercent() {
        return refundSalesPercent;
    }

    public void setRefundSalesPercent(BigDecimal refundSalesPercent) {
        this.refundSalesPercent = refundSalesPercent;
    }

    public static class SalesByMemberType {
        private ConsumerType consumerType;
        private Integer salesCount = 0;
        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        private BigDecimal salesPercent = BigDecimal.ZERO;

        public ConsumerType getConsumerType() {
            return consumerType;
        }

        public void setConsumerType(ConsumerType consumerType) {
            this.consumerType = consumerType;
        }

        public Integer getSalesCount() {
            return salesCount;
        }

        public void setSalesCount(Integer salesCount) {
            this.salesCount = salesCount;
        }

        public BigDecimal getSalesPercent() {
            return salesPercent;
        }

        public void setSalesPercent(BigDecimal salesPercent) {
            this.salesPercent = salesPercent;
        }
    }

    public static class SalesByBudTender {
        private String employeeId;
        private String employeeName;
        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        private BigDecimal sales = BigDecimal.ZERO;
        @JsonSerialize(using = TruncateTwoDigitsSerializer.class)
        private BigDecimal averageSales = BigDecimal.ZERO;
        private long vists;

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public BigDecimal getSales() {
            return sales;
        }

        public void setSales(BigDecimal sales) {
            this.sales = sales;
        }

        public BigDecimal getAverageSales() {
            return averageSales;
        }

        public void setAverageSales(BigDecimal averageSales) {
            this.averageSales = averageSales;
        }

        public long getVists() {
            return vists;
        }

        public void setVists(long vists) {
            this.vists = vists;
        }
    }
}
