package com.fourtwenty.core.rest.features;

import java.util.ArrayList;
import java.util.List;

public class ActiveConversationResult {
    private String transactionId;
    private String transNo;
    private List<ConversationResult> twilioChats = new ArrayList<>();
    private List<ConversationResult> pusherChats = new ArrayList<>();

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public List<ConversationResult> getTwilioChats() {
        return twilioChats;
    }

    public void setTwilioChats(List<ConversationResult> twilioChats) {
        this.twilioChats = twilioChats;
    }

    public List<ConversationResult> getPusherChats() {
        return pusherChats;
    }

    public void setPusherChats(List<ConversationResult> pusherChats) {
        this.pusherChats = pusherChats;
    }
}
