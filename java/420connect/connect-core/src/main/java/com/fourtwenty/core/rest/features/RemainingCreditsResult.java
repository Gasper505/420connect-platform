package com.fourtwenty.core.rest.features;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 7/25/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemainingCreditsResult {
    private int smsCredits = 0;
    private int emailCredits = 0;

    public int getEmailCredits() {
        return emailCredits;
    }

    public void setEmailCredits(int emailCredits) {
        this.emailCredits = emailCredits;
    }

    public int getSmsCredits() {
        return smsCredits;
    }

    public void setSmsCredits(int smsCredits) {
        this.smsCredits = smsCredits;
    }
}
