package com.fourtwenty.core.rest.features;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;

/**
 * Created by mdo on 7/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SMSBuyRequest {
    @Min(1)
    private int amount = 0;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
