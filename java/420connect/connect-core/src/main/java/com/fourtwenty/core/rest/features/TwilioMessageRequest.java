package com.fourtwenty.core.rest.features;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.Conversation;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TwilioMessageRequest {
    private String transactionId;
    private String message;
    private String employeeId;
    private Conversation.ConversationType conversationType = Conversation.ConversationType.TWILLIO;
    private Conversation.Sender sender = Conversation.Sender.EMPLOYEE;



    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Conversation.ConversationType getConversationType() {
        return conversationType;
    }

    public void setConversationType(Conversation.ConversationType conversationType) {
        this.conversationType = conversationType;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Conversation.Sender getSender() {
        return sender;
    }

    public void setSender(Conversation.Sender sender) {
        this.sender = sender;
    }

}
