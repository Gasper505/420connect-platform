package com.fourtwenty.core.rest.marketing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;

/**
 * Created by mdo on 7/20/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketingJobResult extends MarketingJob {
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
