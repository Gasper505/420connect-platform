package com.fourtwenty.core.rest.marketing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.plugins.TVDisplay;

/**
 * Created by mdo on 11/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TVDisplayResult extends TVDisplay {
    private String companyName;
    private String companyLogoURL;
    private String shopName;
    private String shopLogoURL;

    public String getCompanyLogoURL() {
        return companyLogoURL;
    }

    public void setCompanyLogoURL(String companyLogoURL) {
        this.companyLogoURL = companyLogoURL;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLogoURL() {
        return shopLogoURL;
    }

    public void setShopLogoURL(String shopLogoURL) {
        this.shopLogoURL = shopLogoURL;
    }
}
