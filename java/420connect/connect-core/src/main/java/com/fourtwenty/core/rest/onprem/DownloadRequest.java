package com.fourtwenty.core.rest.onprem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DownloadRequest {

    private long afterDate;
    private Long beforeDate;
    private boolean isForcefulDownload = false;
    @NotNull
    private String entityName;
    private String shopId;

    public DownloadRequest(long afterDate, Long beforeDate, boolean isForcefulDownload, String entityName, String shopId) {
        this.afterDate = afterDate;
        this.beforeDate = beforeDate;
        this.isForcefulDownload = isForcefulDownload;
        this.entityName = entityName;
        this.shopId = shopId;
    }

    public DownloadRequest() {
    }

    public long getAfterDate() {
        return afterDate;
    }

    public void setAfterDate(long afterDate) {
        this.afterDate = afterDate;
    }

    public long getBeforeDate() {
        return beforeDate;
    }

    public void setBeforeDate(long beforeDate) {
        this.beforeDate = beforeDate;
    }

    public boolean isForcefulDownload() {
        return isForcefulDownload;
    }

    public void setForcefulDownload(boolean forcefulDownload) {
        isForcefulDownload = forcefulDownload;
    }

    public void setBeforeDate(Long beforeDate) {
        this.beforeDate = beforeDate;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
