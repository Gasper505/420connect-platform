package com.fourtwenty.core.rest.partners.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerWebHookAddRequest {
    @Deprecated
    private String url;
    @NotNull
    private PartnerWebHook.PartnerWebHookType webHookType;
    private String shopId;

    @NotNull
    private List<String> webHookUrls;

    public PartnerWebHook.PartnerWebHookType getWebHookType() {
        return webHookType;
    }

    public void setWebHookType(PartnerWebHook.PartnerWebHookType webHookType) {
        this.webHookType = webHookType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public List<String> getWebHookUrls() {
        return webHookUrls;
    }

    public void setWebHookUrls(List<String> webHookUrls) {
        this.webHookUrls = webHookUrls;
    }
}
