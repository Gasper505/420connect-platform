package com.fourtwenty.core.rest.partners.request.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerWebHookResponse {
    // Should be empty class as we are not recording response from webhooks
}
