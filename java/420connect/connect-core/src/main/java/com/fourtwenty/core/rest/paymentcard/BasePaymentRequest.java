package com.fourtwenty.core.rest.paymentcard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.paymentcard.clover.CloverRequest;
import com.fourtwenty.core.rest.paymentcard.mtrac.MtracTransactionRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BasePaymentRequest {
    private CloverRequest cloverRequest;
    private MtracTransactionRequest mtracRequest;
    private PaymentType paymentType;

    public CloverRequest getCloverRequest() {
        return cloverRequest;
    }

    public void setCloverRequest(CloverRequest cloverRequest) {
        this.cloverRequest = cloverRequest;
    }

    public MtracTransactionRequest getMtracRequest() {
        return mtracRequest;
    }

    public void setMtracRequest(MtracTransactionRequest mtracRequest) {
        this.mtracRequest = mtracRequest;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public enum PaymentType {
        MTRAC,
        CLOVER
    }
}
