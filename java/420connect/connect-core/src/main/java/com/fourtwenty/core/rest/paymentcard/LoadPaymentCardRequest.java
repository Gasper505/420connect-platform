package com.fourtwenty.core.rest.paymentcard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.CreditCard;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadPaymentCardRequest {
    private PaymentCardType type;
    private String paymentCardNumber;
    private BigDecimal amount;
    private CreditCard creditCard;
    @Deprecated
    private String linxCardNumber;
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getLinxCardNumber() {
        return linxCardNumber;
    }

    public void setLinxCardNumber(String linxCardNumber) {
        this.linxCardNumber = linxCardNumber;
    }

    public PaymentCardType getType() {
        return type;
    }

    public void setType(PaymentCardType type) {
        this.type = type;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

    public void setPaymentCardNumber(String paymentCardNumber) {
        this.paymentCardNumber = paymentCardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @Override
    public String toString() {
        return "LoadPaymentCardRequest{" +
                "type=" + type +
                ", paymentCardNumber='" + paymentCardNumber + '\'' +
                ", amount=" + amount +
                ", creditCard=" + creditCard +
                ", linxCardNumber='" + linxCardNumber + '\'' +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
