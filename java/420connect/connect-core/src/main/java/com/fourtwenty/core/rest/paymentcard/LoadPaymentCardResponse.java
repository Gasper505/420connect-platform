package com.fourtwenty.core.rest.paymentcard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardResult;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadPaymentCardResponse {
    private BigDecimal loadedTotal;
    private BigDecimal feeTotal;
    private BigDecimal chargedTotal;

    public LoadPaymentCardResponse() {
    }

    /*

            LOG.info("Result Total Value: " + linxLoadResult.getTotalValue());
            LOG.info("Result Total Paid: " + linxLoadResult.getTotalPaid());
            LOG.info("Result Message: " + linxLoadResult.getMessage());
     */
    public LoadPaymentCardResponse(LoadPaymentCardResult result) {
        setLoadedTotal(result.getLoadedTotal());
        setFeeTotal(result.getFeeTotal());
        setChargedTotal(result.getChargedTotal());

    }

    public BigDecimal getLoadedTotal() {
        return loadedTotal;
    }

    public void setLoadedTotal(BigDecimal loadedTotal) {
        this.loadedTotal = loadedTotal;
    }

    public BigDecimal getFeeTotal() {
        return feeTotal;
    }

    public void setFeeTotal(BigDecimal feeTotal) {
        this.feeTotal = feeTotal;
    }

    public BigDecimal getChargedTotal() {
        return chargedTotal;
    }

    public void setChargedTotal(BigDecimal chargedTotal) {
        this.chargedTotal = chargedTotal;
    }
}
