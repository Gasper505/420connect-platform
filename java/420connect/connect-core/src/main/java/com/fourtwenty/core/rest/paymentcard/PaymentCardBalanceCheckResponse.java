package com.fourtwenty.core.rest.paymentcard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckResult;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentCardBalanceCheckResponse {
    private String status;
    private BigDecimal balance;
    @Deprecated
    private String cardStatus; //


    public PaymentCardBalanceCheckResponse() {
    }

    public PaymentCardBalanceCheckResponse(PaymentCardBalanceCheckResult result) {
        setStatus(result.getStatus());
        setBalance(result.getBalance());
        setCardStatus(result.getStatus());
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
