package com.fourtwenty.core.rest.paymentcard.clover;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloverRequest {
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("expMonth")
    private int expMonth;
    @JsonProperty("cvv")
    private String cvv;
    @JsonProperty("amount")
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @JsonDeserialize(using = BigDecimalTwoDigitsDeserializer.class)
    @DecimalMin("0")
    private BigDecimal amount = new BigDecimal(0);
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("last4")
    private String accoutLastFourNumber;
    @JsonProperty("expYear")
    private int expYear;
    @JsonProperty("first6")
    private String accoutFirstSixNumber;
    @JsonProperty("cardEncrypted")
    private String cardEncrypted;
    @JsonProperty
    private String transactionId;
    @JsonProperty("cardNumber")
    private String cardNumber;

    public CloverPayRequest toPayRequest() {
        CloverPayRequest request = new CloverPayRequest();
        request.setOrderId(this.orderId);
        request.setExpMonth(this.expMonth);
        request.setOrderId(this.getOrderId());
        request.setCvv(this.getCvv());
        request.setAmount(this.getAmount().multiply(new BigDecimal(100)).intValue());
        request.setCurrency("usd");
        request.setExpYear(this.getExpYear());
        request.setAccoutFirstSixNumber(this.getAccoutFirstSixNumber());
        request.setAccoutLastFourNumber(this.getAccoutLastFourNumber());
        request.setCardNumber(this.getCardNumber());
        request.setTransactionId(this.getTransactionId());
        request.setCardEncrypted(this.getCardEncrypted());
        return request;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccoutLastFourNumber() {
        return accoutLastFourNumber;
    }

    public void setAccoutLastFourNumber(String accoutLastFourNumber) {
        this.accoutLastFourNumber = accoutLastFourNumber;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getAccoutFirstSixNumber() {
        return accoutFirstSixNumber;
    }

    public void setAccoutFirstSixNumber(String accoutFirstSixNumber) {
        this.accoutFirstSixNumber = accoutFirstSixNumber;
    }

    public String getCardEncrypted() {
        return cardEncrypted;
    }

    public void setCardEncrypted(String cardEncrypted) {
        this.cardEncrypted = cardEncrypted;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
