package com.fourtwenty.core.rest.paymentcard.mtrac;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MtracRequest {
    @JsonProperty("auth_token")
    protected String authToken;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
