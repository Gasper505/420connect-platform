package com.fourtwenty.core.rest.payments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;

/**
 * Created by mdo on 8/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BraintreeBuyRequest {
    @Min(500)
    private int quantity;
    private String nonce;

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
