package com.fourtwenty.core.rest.payments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 8/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BraintreeTokenResult {
    private String clientToken;

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }
}
