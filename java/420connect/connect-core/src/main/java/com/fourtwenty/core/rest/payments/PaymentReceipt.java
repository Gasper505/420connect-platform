package com.fourtwenty.core.rest.payments;

import com.fourtwenty.core.domain.models.payment.PaymentComponent;
import com.fourtwenty.core.domain.models.payment.PaymentProvider;

import java.math.BigDecimal;

/**
 * Created on 1/11/17 5:26 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public class PaymentReceipt {
    private Long purchaseDate;
    private BigDecimal cost = new BigDecimal(0);
    private String paymentTransactionId;
    private PaymentProvider paymentProvider;
    private PaymentComponent paymentComponent;
    private long quantity = 0;
    private BigDecimal unitCost = new BigDecimal(0);

    public Long getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Long purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public void setPaymentTransactionId(String paymentTransactionId) {
        this.paymentTransactionId = paymentTransactionId;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public PaymentComponent getPaymentComponent() {
        return paymentComponent;
    }

    public void setPaymentComponent(PaymentComponent paymentComponent) {
        this.paymentComponent = paymentComponent;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }
}
