package com.fourtwenty.core.rest.payments;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fourtwenty.core.domain.models.payment.PaymentComponent;
import com.fourtwenty.core.domain.models.payment.PaymentRecurringType;

/**
 * Created on 23/10/17 5:40 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public class PaymentRequest {

    private double amount;
    @JsonIgnore
    private static final String currency = "USD";
    @JsonIgnore
    private PaymentRecurringType paymentRecurringType;
    @JsonIgnore
    private PaymentComponent paymentComponent;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public PaymentRecurringType getPaymentRecurringType() {
        return paymentRecurringType;
    }

    public void setPaymentRecurringType(PaymentRecurringType paymentRecurringType) {
        this.paymentRecurringType = paymentRecurringType;
    }

    public PaymentComponent getPaymentComponent() {
        return paymentComponent;
    }

    public void setPaymentComponent(PaymentComponent paymentComponent) {
        this.paymentComponent = paymentComponent;
    }
}
