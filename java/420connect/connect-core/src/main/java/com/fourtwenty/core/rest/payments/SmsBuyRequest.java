package com.fourtwenty.core.rest.payments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.payments.stripe.StripeSMSBuyRequest;
import com.fourtwenty.core.rest.payments.wepay.WepayCreditCardCheckoutRequest;

/**
 * Created by Raja Dushyant Vashishtha
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SmsBuyRequest {

    private StripeSMSBuyRequest stripeSMSBuyRequest;

    private WepayCreditCardCheckoutRequest wepayCreditCardCheckoutRequest;

    public StripeSMSBuyRequest getStripeSMSBuyRequest() {
        return stripeSMSBuyRequest;
    }

    public void setStripeSMSBuyRequest(StripeSMSBuyRequest stripeSMSBuyRequest) {
        this.stripeSMSBuyRequest = stripeSMSBuyRequest;
    }

    public WepayCreditCardCheckoutRequest getWepayCreditCardCheckoutRequest() {
        return wepayCreditCardCheckoutRequest;
    }

    public void setWepayCreditCardCheckoutRequest(WepayCreditCardCheckoutRequest wepayCreditCardCheckoutRequest) {
        this.wepayCreditCardCheckoutRequest = wepayCreditCardCheckoutRequest;
    }
}
