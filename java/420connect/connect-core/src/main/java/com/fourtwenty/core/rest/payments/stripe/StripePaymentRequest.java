package com.fourtwenty.core.rest.payments.stripe;

import com.fourtwenty.core.rest.payments.PaymentRequest;

/**
 * Created on 24/10/17 2:14 AM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public class StripePaymentRequest extends PaymentRequest {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
