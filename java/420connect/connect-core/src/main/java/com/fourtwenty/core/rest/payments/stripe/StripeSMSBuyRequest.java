package com.fourtwenty.core.rest.payments.stripe;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;

/**
 * Created on 24/10/17 2:01 AM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class StripeSMSBuyRequest extends StripePaymentRequest {
    @Min(500)
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
