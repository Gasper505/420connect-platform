package com.fourtwenty.core.rest.payments.wepay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * Created on 11/9/17 3:56 PM by Raja Dushyant Vashishtha
 * Sr. Software Engineer
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class WepayBuyRequest {
    private BigDecimal amount;
    private String shortDescription;
    private String type;
    private final String currency = "USD";
    @Min(500)
    private int quantity;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmountAsDouble() {
        return amount == null ? 0.0 : amount.doubleValue();
    }

    public String getCurrency() {
        return currency;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
