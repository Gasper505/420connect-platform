package com.fourtwenty.core.rest.payments.wepay;

import javax.validation.Valid;

/**
 * Created on 11/9/17 6:11 PM by Raja Dushyant Vashishtha
 * Sr. Software Engineer
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */

public class WepayCreditCardCheckoutRequest extends WepayBuyRequest {
    @Valid
    private long creditCardId;

    public long getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(long creditCardId) {
        this.creditCardId = creditCardId;
    }

    public String getPaymentType() {
        return "credit_card";
    }

}
