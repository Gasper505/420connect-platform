package com.fourtwenty.core.rest.payments.wepay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on 11/9/17 5:57 PM by Raja Dushyant Vashishtha
 * Sr. Software Engineer
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WepayResult {

    protected Boolean status = Boolean.FALSE;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
