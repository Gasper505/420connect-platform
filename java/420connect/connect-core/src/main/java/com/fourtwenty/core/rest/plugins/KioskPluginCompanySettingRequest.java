package com.fourtwenty.core.rest.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskPluginCompanySettingRequest extends PluginCompanySettingRequest {

    private List<KioskPluginShopSettingRequest> shopPluginSettingRequests;

    public List<KioskPluginShopSettingRequest> getShopPluginSettingRequests() {
        return shopPluginSettingRequests;
    }

    public void setShopPluginSettingRequests(List<KioskPluginShopSettingRequest> shopPluginSettingRequests) {
        this.shopPluginSettingRequests = shopPluginSettingRequests;
    }
}
