package com.fourtwenty.core.rest.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskPluginShopSettingRequest extends PluginShopSettingRequest {
    private String deviceName;
    private String welcomeText;
    private String backgroundColor;
    private CompanyAsset backgroundImage;

    private String themeColor;
    private String buttonTextColor;


    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getWelcomeText() {
        return welcomeText;
    }

    public void setWelcomeText(String welcomeText) {
        this.welcomeText = welcomeText;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public CompanyAsset getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(CompanyAsset backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String getButtonTextColor() {
        return buttonTextColor;
    }

    public void setButtonTextColor(String buttonTextColor) {
        this.buttonTextColor = buttonTextColor;
    }
}
