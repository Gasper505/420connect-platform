package com.fourtwenty.core.rest.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessagingPluginCompanySettingRequest extends PluginCompanySettingRequest {
    private List<MessagingPluginShopSettingRequest> shopPluginSettingRequests;

    public List<MessagingPluginShopSettingRequest> getShopPluginSettingRequests() {
        return shopPluginSettingRequests;
    }

    public void setShopPluginSettingRequests(List<MessagingPluginShopSettingRequest> shopPluginSettingRequests) {
        this.shopPluginSettingRequests = shopPluginSettingRequests;
    }
}
