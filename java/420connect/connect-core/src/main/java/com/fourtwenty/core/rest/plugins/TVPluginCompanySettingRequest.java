package com.fourtwenty.core.rest.plugins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TVPluginCompanySettingRequest extends PluginCompanySettingRequest {
}
