package com.fourtwenty.core.rest.purchaseorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;

import java.util.List;

/**
 * Created by mdo on 10/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class POCompleteRequest {
    private String deliveredBy;
    private String notes;
    private String vendorId;
    private String createdBy;
    private long receivedDate;
    private String receivedByEmployeeId;
    private List<CompanyAsset> asset;
    private List<POProductRequest> poProductRequestList;

    private List<POProductBORequest> backOrderProductList;

    private CompanyAsset managerReceiveSignature;

    private String customerCompanyId;
    private PurchaseOrder.CustomerType customerType = PurchaseOrder.CustomerType.VENDOR;
    private String termsAndCondition;

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public long getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(long receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getReceivedByEmployeeId() {
        return receivedByEmployeeId;
    }

    public void setReceivedByEmployeeId(String receivedByEmployeeId) {
        this.receivedByEmployeeId = receivedByEmployeeId;
    }

    public List<CompanyAsset> getAsset() {
        return asset;
    }

    public void setAsset(List<CompanyAsset> asset) {
        this.asset = asset;
    }

    public List<POProductRequest> getPoProductRequestList() {
        return poProductRequestList;
    }

    public void setPoProductRequestList(List<POProductRequest> poProductRequestList) {
        this.poProductRequestList = poProductRequestList;
    }

    public List<POProductBORequest> getBackOrderProductList() {
        return backOrderProductList;
    }

    public void setBackOrderProductList(List<POProductBORequest> backOrderProductList) {
        this.backOrderProductList = backOrderProductList;
    }

    public CompanyAsset getManagerReceiveSignature() {
        return managerReceiveSignature;
    }

    public void setManagerReceiveSignature(CompanyAsset managerReceiveSignature) {
        this.managerReceiveSignature = managerReceiveSignature;
    }

    public String getCustomerCompanyId() {
        return customerCompanyId;
    }

    public void setCustomerCompanyId(String customerCompanyId) {
        this.customerCompanyId = customerCompanyId;
    }

    public PurchaseOrder.CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(PurchaseOrder.CustomerType customerType) {
        this.customerType = customerType;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

}
