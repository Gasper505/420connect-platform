package com.fourtwenty.core.rest.store.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mdo on 5/21/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractSignRequest {
    @NotEmpty
    private String contractId;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
}
