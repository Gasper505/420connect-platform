package com.fourtwenty.core.rest.store.requests;

import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderItemRequest extends ProductCostRequest {

    private boolean useUnitQty = false;
    private ProductWeightTolerance.WeightKey weightKey = ProductWeightTolerance.WeightKey.UNIT;

    public boolean isUseUnitQty() {
        return useUnitQty;
    }

    public void setUseUnitQty(boolean useUnitQty) {
        this.useUnitQty = useUnitQty;
    }

    public ProductWeightTolerance.WeightKey getWeightKey() {
        return weightKey;
    }

    public void setWeightKey(ProductWeightTolerance.WeightKey weightKey) {
        this.weightKey = weightKey;
    }
}
