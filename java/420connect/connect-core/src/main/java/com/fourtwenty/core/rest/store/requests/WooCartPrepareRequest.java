package com.fourtwenty.core.rest.store.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WooCartPrepareRequest {

    private List<OrderItemRequest> productCostRequests;
    private String promoCode;
    private ConsumerCart.ConsumerOrderPickupType pickupType = ConsumerCart.ConsumerOrderPickupType.Delivery;

    private Address deliveryAddress;
    private long deliveryDate;
    private String rewardName;
    private Cart.PaymentOption paymentOption = Cart.PaymentOption.None;

    public List<OrderItemRequest> getProductCostRequests() {
        return productCostRequests;
    }

    public void setProductCostRequests(List<OrderItemRequest> productCostRequests) {
        this.productCostRequests = productCostRequests;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public ConsumerCart.ConsumerOrderPickupType getPickupType() {
        return pickupType;
    }

    public void setPickupType(ConsumerCart.ConsumerOrderPickupType pickupType) {
        this.pickupType = pickupType;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }
}
