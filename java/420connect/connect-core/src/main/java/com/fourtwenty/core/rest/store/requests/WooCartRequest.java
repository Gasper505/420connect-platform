package com.fourtwenty.core.rest.store.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WooCartRequest {

    private String memo;
    @Email
    private String email;
    private Address address;
    private String primaryPhone;

    private String promoCode;
    private String consumerCartId;
    @NotBlank
    private String sessionId;
    private List<OrderItemRequest> productCostRequests;
    private ConsumerUser.ConsumerOrderNotificationType notificationType;
    private ConsumerCart.ConsumerOrderPickupType pickupType = ConsumerCart.ConsumerOrderPickupType.Delivery;
    private Boolean placeOrder = Boolean.FALSE;
    private long deliveryDate = 0;
    private Address deliveryAddress;

    private String rewardName;

    private Cart.PaymentOption paymentOption = Cart.PaymentOption.None;

    public String getConsumerCartId() {
        return consumerCartId;
    }

    public void setConsumerCartId(String consumerCartId) {
        this.consumerCartId = consumerCartId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public List<OrderItemRequest> getProductCostRequests() {
        return productCostRequests;
    }

    public void setProductCostRequests(List<OrderItemRequest> productCostRequests) {
        this.productCostRequests = productCostRequests;
    }

    public ConsumerCart.ConsumerOrderPickupType getPickupType() {
        return pickupType;
    }

    public void setPickupType(ConsumerCart.ConsumerOrderPickupType pickupType) {
        this.pickupType = pickupType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public ConsumerUser.ConsumerOrderNotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(ConsumerUser.ConsumerOrderNotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Boolean getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(Boolean placeOrder) {
        this.placeOrder = placeOrder;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }
}
