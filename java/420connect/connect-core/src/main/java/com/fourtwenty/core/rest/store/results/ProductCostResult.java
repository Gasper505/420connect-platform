package com.fourtwenty.core.rest.store.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import com.fourtwenty.core.rest.store.requests.ProductCostRequest;

import java.math.BigDecimal;

/**
 * Created by mdo on 6/2/17.
 */
@JsonIgnoreProperties
public class ProductCostResult extends ProductCostRequest {
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal cost = new BigDecimal(0);

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
