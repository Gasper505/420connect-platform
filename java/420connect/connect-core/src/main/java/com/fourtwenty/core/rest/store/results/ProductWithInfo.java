package com.fourtwenty.core.rest.store.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;

/**
 * Created by mdo on 5/17/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWithInfo extends Product {
    private double thc;
    private double cbn;
    private double cbd;
    private double cbda;
    private double thca;

    private Brand brand;

    public double getCbd() {
        return cbd;
    }

    public void setCbd(double cbd) {
        this.cbd = cbd;
    }

    public double getCbda() {
        return cbda;
    }

    public void setCbda(double cbda) {
        this.cbda = cbda;
    }

    public double getCbn() {
        return cbn;
    }

    public void setCbn(double cbn) {
        this.cbn = cbn;
    }

    public double getThc() {
        return thc;
    }

    public void setThc(double thc) {
        this.thc = thc;
    }

    public double getThca() {
        return thca;
    }

    public void setThca(double thca) {
        this.thca = thca;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
}
