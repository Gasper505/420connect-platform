package com.fourtwenty.core.rest.store.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;

import java.util.HashMap;

/**
 * Created by mdo on 5/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoreInfoResult {
    private Shop shop;
    private Contract contract;
    private HashMap<String, ProductWeightTolerance> toleranceMap = new HashMap<>();
    private String companyLogoURL;

    public HashMap<String, ProductWeightTolerance> getToleranceMap() {
        return toleranceMap;
    }

    public void setToleranceMap(HashMap<String, ProductWeightTolerance> toleranceMap) {
        this.toleranceMap = toleranceMap;
    }

    public String getCompanyLogoURL() {
        return companyLogoURL;
    }

    public void setCompanyLogoURL(String companyLogoURL) {
        this.companyLogoURL = companyLogoURL;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
