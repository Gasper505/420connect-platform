package com.fourtwenty.core.rest.store.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.serializers.BigDecimalFourDigitsSerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalPercentageSerializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WooComCartResult {

    private TaxResult taxResult;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal discount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal subTotal = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalDiscount = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal calcCartDiscount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal subTotalDiscount = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal total = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    private BigDecimal tax = new BigDecimal(0);

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalCalcTax = new BigDecimal(0);
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal totalPreCalcTax = new BigDecimal(0);

    private boolean enableDeliveryFee = false;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal deliveryFee = new BigDecimal(0);

    private List<WooComCartItem> items = new ArrayList<>();

    private String sessionId;

    private String consumerCartId;

    private List<ConsumerCart.ConsumerOrderPickupType> pickupTypes = new ArrayList<>();

    private Address pickUpAddress;

    private String promoCode;

    private long deliveryDate;

    private Address deliveryAddress;

    @JsonSerialize(using = BigDecimalFourDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal pointSpent = new BigDecimal(0);

    private String rewardName;

    private HashMap<String, String> rewardErrorMap = new HashMap();

    private WooMemberGroupResult memberGroup;

    private String errorMsg;

    private Cart.PaymentOption paymentOption = Cart.PaymentOption.None;

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal creditCardFee = new BigDecimal(0);


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WooComCartItem {
        private String productId;

        private boolean useUnitQty = false;
        private ProductWeightTolerance.WeightKey weightKey = ProductWeightTolerance.WeightKey.UNIT;

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
        private BigDecimal quantity = new BigDecimal(0);

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
        private BigDecimal origQuantity = new BigDecimal(0);

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal discountedQty = new BigDecimal(0);

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal cost = new BigDecimal(0);
        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal unitPrice = new BigDecimal(0);
        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
        private BigDecimal discount = new BigDecimal(0);
        private OrderItem.DiscountType discountType = OrderItem.DiscountType.Cash;

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal calcDiscount = new BigDecimal(0);

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal finalPrice = new BigDecimal(0);

        @DecimalMin("0.0")
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        private BigDecimal requestQuantity = new BigDecimal(0);

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public boolean isUseUnitQty() {
            return useUnitQty;
        }

        public void setUseUnitQty(boolean useUnitQty) {
            this.useUnitQty = useUnitQty;
        }

        public ProductWeightTolerance.WeightKey getWeightKey() {
            return weightKey;
        }

        public void setWeightKey(ProductWeightTolerance.WeightKey weightKey) {
            this.weightKey = weightKey;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void setQuantity(BigDecimal quantity) {
            this.quantity = quantity;
        }

        public BigDecimal getOrigQuantity() {
            return origQuantity;
        }

        public void setOrigQuantity(BigDecimal origQuantity) {
            this.origQuantity = origQuantity;
        }

        public BigDecimal getDiscountedQty() {
            return discountedQty;
        }

        public void setDiscountedQty(BigDecimal discountedQty) {
            this.discountedQty = discountedQty;
        }

        public BigDecimal getCost() {
            return cost;
        }

        public void setCost(BigDecimal cost) {
            this.cost = cost;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public OrderItem.DiscountType getDiscountType() {
            return discountType;
        }

        public void setDiscountType(OrderItem.DiscountType discountType) {
            this.discountType = discountType;
        }

        public BigDecimal getCalcDiscount() {
            return calcDiscount;
        }

        public void setCalcDiscount(BigDecimal calcDiscount) {
            this.calcDiscount = calcDiscount;
        }

        public BigDecimal getFinalPrice() {
            return finalPrice;
        }

        public void setFinalPrice(BigDecimal finalPrice) {
            this.finalPrice = finalPrice;
        }

        public BigDecimal getRequestQuantity() {
            return requestQuantity;
        }

        public void setRequestQuantity(BigDecimal requestQuantity) {
            this.requestQuantity = requestQuantity;
        }
    }

    public TaxResult getTaxResult() {
        return taxResult;
    }

    public void setTaxResult(TaxResult taxResult) {
        this.taxResult = taxResult;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getCalcCartDiscount() {
        return calcCartDiscount;
    }

    public void setCalcCartDiscount(BigDecimal calcCartDiscount) {
        this.calcCartDiscount = calcCartDiscount;
    }

    public BigDecimal getSubTotalDiscount() {
        return subTotalDiscount;
    }

    public void setSubTotalDiscount(BigDecimal subTotalDiscount) {
        this.subTotalDiscount = subTotalDiscount;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getTotalCalcTax() {
        return totalCalcTax;
    }

    public void setTotalCalcTax(BigDecimal totalCalcTax) {
        this.totalCalcTax = totalCalcTax;
    }

    public BigDecimal getTotalPreCalcTax() {
        return totalPreCalcTax;
    }

    public void setTotalPreCalcTax(BigDecimal totalPreCalcTax) {
        this.totalPreCalcTax = totalPreCalcTax;
    }

    public List<WooComCartItem> getItems() {
        return items;
    }

    public void setItems(List<WooComCartItem> items) {
        this.items = items;
    }

    public boolean isEnableDeliveryFee() {
        return enableDeliveryFee;
    }

    public void setEnableDeliveryFee(boolean enableDeliveryFee) {
        this.enableDeliveryFee = enableDeliveryFee;
    }

    public BigDecimal getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(BigDecimal deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getConsumerCartId() {
        return consumerCartId;
    }

    public void setConsumerCartId(String consumerCartId) {
        this.consumerCartId = consumerCartId;
    }

    public List<ConsumerCart.ConsumerOrderPickupType> getPickupTypes() {
        return pickupTypes;
    }

    public void setPickupTypes(List<ConsumerCart.ConsumerOrderPickupType> pickupTypes) {
        this.pickupTypes = pickupTypes;
    }

    public Address getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(Address pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public BigDecimal getPointSpent() {
        return pointSpent;
    }

    public void setPointSpent(BigDecimal pointSpent) {
        this.pointSpent = pointSpent;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public HashMap<String, String> getRewardErrorMap() {
        return rewardErrorMap;
    }

    public void setRewardErrorMap(HashMap<String, String> rewardErrorMap) {
        this.rewardErrorMap = rewardErrorMap;
    }

    public WooMemberGroupResult getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(WooMemberGroupResult memberGroup) {
        this.memberGroup = memberGroup;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WooMemberGroupResult {
        private String name;
        @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
        @DecimalMin("0")
        private BigDecimal discount = new BigDecimal(0);
        private OrderItem.DiscountType discountType = OrderItem.DiscountType.Percentage;

        public WooMemberGroupResult() {
        }

        public WooMemberGroupResult(String name, BigDecimal discount, OrderItem.DiscountType discountType) {
            this.name = name;
            this.discount = discount;
            this.discountType = discountType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public OrderItem.DiscountType getDiscountType() {
            return discountType;
        }

        public void setDiscountType(OrderItem.DiscountType discountType) {
            this.discountType = discountType;
        }
    }

    public BigDecimal getCreditCardFee() {
        return creditCardFee;
    }

    public void setCreditCardFee(BigDecimal creditCardFee) {
        this.creditCardFee = creditCardFee;
    }
}
