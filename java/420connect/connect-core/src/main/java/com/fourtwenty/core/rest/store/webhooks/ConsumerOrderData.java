package com.fourtwenty.core.rest.store.webhooks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerOrderData {
    private String consumerOrderId;
    private String consumerOrderNo;
    private String consumerName;
    private String memberName;
    private Long orderTime;
    private ConsumerCart.ConsumerCartStatus orderStatus;
    private Cart cart;
    private boolean membershipAccepted;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal loyaltyPoints = new BigDecimal(0);
    private String memberId;
    private String consumerId;

    public String getConsumerOrderId() {
        return consumerOrderId;
    }

    public void setConsumerOrderId(String consumerOrderId) {
        this.consumerOrderId = consumerOrderId;
    }

    public String getConsumerOrderNo() {
        return consumerOrderNo;
    }

    public void setConsumerOrderNo(String consumerOrderNo) {
        this.consumerOrderNo = consumerOrderNo;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public ConsumerCart.ConsumerCartStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(ConsumerCart.ConsumerCartStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public boolean isMembershipAccepted() {
        return membershipAccepted;
    }

    public void setMembershipAccepted(boolean membershipAccepted) {
        this.membershipAccepted = membershipAccepted;
    }

    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(BigDecimal loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }
}
