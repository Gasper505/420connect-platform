package com.fourtwenty.core.rest.store.webhooks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberData {
    private String id;
    private long created;
    private long modified;
    private String memberName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Address address;
    private Member.MembershipStatus status;
    private ConsumerType consumerType;
    private String memberGroupId;
    private String email;
    private Long dob;
    private BaseMember.Gender sex;
    private boolean textOptIn = true;
    private boolean emailOptIn = true;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isTextOptIn() {
        return textOptIn;
    }

    public void setTextOptIn(boolean textOptIn) {
        this.textOptIn = textOptIn;
    }

    public boolean isEmailOptIn() {
        return emailOptIn;
    }

    public void setEmailOptIn(boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Member.MembershipStatus getStatus() {
        return status;
    }

    public void setStatus(Member.MembershipStatus status) {
        this.status = status;
    }

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(ConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public String getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public BaseMember.Gender getSex() {
        return sex;
    }

    public void setSex(BaseMember.Gender sex) {
        this.sex = sex;
    }
}
