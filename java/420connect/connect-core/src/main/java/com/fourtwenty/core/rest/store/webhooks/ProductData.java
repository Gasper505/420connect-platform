package com.fourtwenty.core.rest.store.webhooks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.rest.store.results.ProductWithInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductData extends ProductWithInfo {

    private boolean isNew;

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}
