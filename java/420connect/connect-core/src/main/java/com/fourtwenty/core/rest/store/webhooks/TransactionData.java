package com.fourtwenty.core.rest.store.webhooks;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class TransactionData {
    private String id;
    private String shopId;
    private String terminalId;
    private long created;
    private String transactionNo;
    private String memberId;
    private String consumerCartId;
    private String consumerUserId;
    private String memberName;
    private Transaction.TransactionType transactionType;
    private Transaction.QueueType queueType;
    private Transaction.FulfillmentStep fulfillmentStep;
    private String sellerName;
    private Cart.PaymentOption paymentOption;
    private Cart.PaymentType paymentType;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal discount;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal totalTax;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal total;
    private String promoCode;
    private long processedTime;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal loyaltyPoints = new BigDecimal(0);
    private boolean membershipAccepted;

    public String getConsumerUserId() {
        return consumerUserId;
    }

    public void setConsumerUserId(String consumerUserId) {
        this.consumerUserId = consumerUserId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(long processedTime) {
        this.processedTime = processedTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConsumerCartId() {
        return consumerCartId;
    }

    public void setConsumerCartId(String consumerCartId) {
        this.consumerCartId = consumerCartId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Transaction.TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Transaction.TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Transaction.QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(Transaction.QueueType queueType) {
        this.queueType = queueType;
    }

    public Transaction.FulfillmentStep getFulfillmentStep() {
        return fulfillmentStep;
    }

    public void setFulfillmentStep(Transaction.FulfillmentStep fulfillmentStep) {
        this.fulfillmentStep = fulfillmentStep;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Cart.PaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Cart.PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public Cart.PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Cart.PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(BigDecimal loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public boolean isMembershipAccepted() {
        return membershipAccepted;
    }

    public void setMembershipAccepted(boolean membershipAccepted) {
        this.membershipAccepted = membershipAccepted;
    }
}

