package com.fourtwenty.core.rest.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 7/18/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwilioReceiveMessage {
    @JsonProperty("ToCountry")
    private String toCountry;
    @JsonProperty("ToState")
    private String toState;
    @JsonProperty("SmsMessageSid")
    private String smsMessageSid;
    @JsonProperty("NumMedia")
    private int numMedia;
    @JsonProperty("ToCity")
    private String toCity;
    @JsonProperty("SmsSid")
    private String smsSid;
    @JsonProperty("FromState")
    private String fromState;
    @JsonProperty("SmsStatus")
    private String smsStatus;
    @JsonProperty("FromCity")
    private String fromCity;
    @JsonProperty("Body")
    private String body;
    @JsonProperty("FromCountry")
    private String fromCountry;
    @JsonProperty("To")
    private String to;
    @JsonProperty("ToZip")
    private String toZip;
    @JsonProperty("NumSegments")
    private int numSegments;
    @JsonProperty("MessageSid")
    private String messageSid;
    @JsonProperty("AccountSid")
    private String accountSid;
    @JsonProperty("From")
    private String from;
    @JsonProperty("ApiVersion")
    private String apiVersion;

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getMessageSid() {
        return messageSid;
    }

    public void setMessageSid(String messageSid) {
        this.messageSid = messageSid;
    }

    public int getNumMedia() {
        return numMedia;
    }

    public void setNumMedia(int numMedia) {
        this.numMedia = numMedia;
    }

    public int getNumSegments() {
        return numSegments;
    }

    public void setNumSegments(int numSegments) {
        this.numSegments = numSegments;
    }

    public String getSmsMessageSid() {
        return smsMessageSid;
    }

    public void setSmsMessageSid(String smsMessageSid) {
        this.smsMessageSid = smsMessageSid;
    }

    public String getSmsSid() {
        return smsSid;
    }

    public void setSmsSid(String smsSid) {
        this.smsSid = smsSid;
    }

    public String getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    public String getToZip() {
        return toZip;
    }

    public void setToZip(String toZip) {
        this.toZip = toZip;
    }
}
