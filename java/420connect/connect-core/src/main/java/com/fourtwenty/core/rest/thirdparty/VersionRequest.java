
package com.fourtwenty.core.rest.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;

public class VersionRequest {
    WeedmapAccount.Version version = WeedmapAccount.Version.V1;

    public WeedmapAccount.Version getVersion() {
        return version;
    }

    public void setVersion(WeedmapAccount.Version version) {
        this.version = version;
    }
}

