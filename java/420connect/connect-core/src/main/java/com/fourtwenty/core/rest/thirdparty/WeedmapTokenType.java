
package com.fourtwenty.core.rest.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapTokenType {
    private String code;
    private TokenType type;

    public TokenType getType() {
        return type;
    }

    public void setType(TokenType type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public enum TokenType {
        FETCHTOKEN("authorization_code"),
        REFRESHTOKEN("refresh_token");
        String value;

        TokenType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
