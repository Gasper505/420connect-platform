package com.fourtwenty.core.rest.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 4/13/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapUpdateAPIRequest {

    private List<WeedmapApiKeyMap> apiKeyList = new ArrayList<>();

    public List<WeedmapApiKeyMap> getApiKeyList() {
        return apiKeyList;
    }

    public void setApiKeyList(List<WeedmapApiKeyMap> apiKeyList) {
        this.apiKeyList = apiKeyList;
    }
}
