package com.fourtwenty.core.security;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.internal.InternalApiSecurityProvider;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.partner.PartnerSecurityProvider;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.store.StoreSecurityProvider;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.common.AuditLogService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ContainerRequest;

import javax.servlet.annotation.WebFilter;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Method;

/**
 * The AuthenticationFilter object.
 * User: mdo
 * Date: 8/23/13
 * Time: 1:57 AM
 */
@WebFilter
public class AuthenticationFilter implements ContainerRequestFilter {
    @Inject
    private SecurityUtil securityUtil;
    @Inject
    private ConnectConfiguration configuration;
    @Inject
    AuditLogService auditLogService;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    DeveloperKeyRepository developerKeyRepository;

    @Context
    private ResourceInfo resourceInfo;

    private static final Log LOG = LogFactory.getLog(AuthenticationFilter.class);

    public AuthenticationFilter() {
    }

    public void filter(ContainerRequestContext requestContext) {
        // INFO: BACKWARD COMPATIBILITY
        Method theMethod = resourceInfo.getResourceMethod();
        Secured secure = theMethod.getAnnotation(Secured.class);
        StoreSecured storeSecured = theMethod.getAnnotation(StoreSecured.class);
        PartnerDeveloperSecured partnerDeveloperSecured = theMethod.getAnnotation(PartnerDeveloperSecured.class);
        InternalSecured internalSecured = theMethod.getAnnotation(InternalSecured.class);
        if (secure != null && secure.required()) {
            ContainerRequest request = (ContainerRequest) requestContext.getRequest();

            ConnectAuthToken multiToken = (ConnectAuthToken) request.getProperty("token");
            if (multiToken == null) {
                throw new BlazeAuthException("Authorization", "Request is not authenticated.");
            } else if (!multiToken.isValid()) {
                if (configuration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL || configuration.getEnv() == ConnectConfiguration.EnvironmentType.DEV) {

                    if ("56cfa9c8e38179985229e5a4".equalsIgnoreCase(multiToken.getManagerId())) {
                        return;
                    }
                }

                String errorMsg = multiToken.isValidTTL() ? "Invalid AccessToken" : "Token expired";
                throw new BlazeAuthException("Authorization", errorMsg);
            } else if (secure.requiredShop()) {
                if (multiToken.getShopId() == null) {
                    throw new BlazeAuthException("Authorization", "A shop id is required.");
                }
            } else if (secure.appType() != multiToken.getAppType()) {
                throw new BlazeAuthException("Authorization", "Invalid token domain for application.");
            }

            Audit audit = theMethod.getAnnotation(Audit.class);
            Api api = resourceInfo.getResourceClass().getAnnotation(Api.class);
            if (audit != null) {
                try {
                    auditLogService.addAuditLog(multiToken, request.getPath(true), api.value(), audit.action());
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }

            // Check to see if this company is valid
            if (secure.appType() == ConnectAuthToken.ConnectAppType.Connect) {
                Company company = companyRepository.getById(multiToken.getCompanyId());
                if (company == null || !company.isActive()) {
                    throw new BlazeAuthException("Authorization", "This company is no longer active.");
                }
                // Inject more data
                multiToken.setMembersShareOption(company.getMembersShareOption());

            }
        } else if (storeSecured != null && storeSecured.required()) {
            ContainerRequest request = (ContainerRequest) requestContext.getRequest();

            StoreAuthToken storeToken = (StoreAuthToken) request.getProperty(StoreSecurityProvider.TOKEN_KEY);

            if (storeToken == null) {
                throw new BlazeAuthException("Authorization", "Invalid API Key.");
            } else if (!storeToken.isValid()) {

                String errorMsg = storeToken.isValidTTL() ? "Request is not authenticated." : "Token expired";
                throw new BlazeAuthException("Authorization", errorMsg);
            } else if (storeSecured.authRequired() && !storeToken.isAuthenticated()) {
                throw new BlazeAuthException("Authorization", "User is not authenticated");
            }

        } else if (partnerDeveloperSecured != null && partnerDeveloperSecured.required()) {
            ContainerRequest request = (ContainerRequest) requestContext.getRequest();

            PartnerAuthToken partnerToken = (PartnerAuthToken) request.getProperty(PartnerSecurityProvider.TOKEN_KEY);

            if (partnerToken == null) {
                throw new BlazeAuthException("Authorization", "Invalid PARTNER or Developer API Key.");
            } else if (!partnerToken.isValid()) {
                throw new BlazeAuthException("Authorization", "Invalid PARTNER or Developer API Key.");
            } else if (partnerDeveloperSecured.storeRequired() && !partnerToken.isStoreEnabled()) {
                throw new BlazeAuthException("Authorization", "Store is not enabled.");
            } else if (partnerDeveloperSecured.userRequired() && !partnerToken.isAuthenticated()) {
                throw new BlazeAuthException("Authorization", "User is not specified.");
            }
        } else if (internalSecured != null && internalSecured.required()) {
            ContainerRequest request = (ContainerRequest) requestContext.getRequest();
            InternalApiAuthToken internalToken = (InternalApiAuthToken) request.getProperty(InternalApiSecurityProvider.TOKEN_KEY);
            if (internalToken == null) {
                throw new BlazeAuthException("Authorization", "Invalid Internal Api Token");
            } else if (!internalToken.isValid()) {
                String errorMsg = internalToken.isValidTTL() ? "Token expired" : "Unauthorized Internal Api Request";
                throw new BlazeAuthException("Authorization", errorMsg);
            } else if (internalSecured.requiredShop()) {
                if (internalToken.getShopId() == null) {
                    throw new BlazeAuthException("Authorization", "A shop id is required.");
                }
            }
        }

    }

    public static ConnectAuthToken runSecurityCheck(ContainerRequestContext requestContext, SecurityUtil securityUtil, UriInfo uriInfo) {
        ContainerRequest request = (ContainerRequest) requestContext.getRequest();
        ConnectAuthToken token = (ConnectAuthToken) requestContext.getProperty("token");

        if (token != null) return token;

        // check query params
        String authString = getToken(request, "Authorization");
        String accessToken = null;
        String tokenType = null;
        if (StringUtils.isNotEmpty(authString)) {
            String[] parts = authString.split(" ");
            if (parts.length != 2) return null;
            accessToken = parts[1];
        } else {
            accessToken = uriInfo.getQueryParameters().getFirst("token");
        }

        if (accessToken == null) {
            return null;
        }

        if (!StringUtils.isBlank(accessToken)) {
            try {
                token = securityUtil.isInternalApiToken(accessToken) ? securityUtil.decryptInternalApiToken(accessToken) : securityUtil.decryptAccessToken(accessToken);
                request.setProperty("token", token);
            } catch (Exception e) {
                //throw new BlazeAuthException("Authorization", "Invalid AccessToken");
                return null;
            }
        }
        return token;
    }


    private static String getToken(ContainerRequest request, String propertyName) {
        String accessToken = request.getHeaderString(propertyName);

        return accessToken;
    }
}
