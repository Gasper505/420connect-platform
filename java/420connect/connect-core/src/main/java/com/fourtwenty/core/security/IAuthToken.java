package com.fourtwenty.core.security;

/**
 * Created by mdo on 5/9/17.
 */
public interface IAuthToken {

    String getCompanyId();
    String getShopId();
    String getTerminalId();

    boolean isValid();

    boolean isValidTTL();
}
