package com.fourtwenty.core.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.regex.Pattern;

/**
 * Created by mdo on 5/10/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Version implements Comparable<Version> {

    private String version;

    public final String get() {
        return this.version;
    }

    public Version(String version) {
        if (version == null)
            throw new IllegalArgumentException("Version can not be null");

        version = version.toLowerCase();
        version = version.replaceAll(Pattern.quote(".debug"), "");
        version = version.replaceAll(Pattern.quote(".dev"), "");
        version = version.replaceAll(Pattern.quote(".stage"), "");
        version = version.replaceAll(Pattern.quote(".rc"), "");
        version = version.replaceAll(Pattern.quote(".release"), "");

        if (!version.matches("[0-9]+(\\.[0-9]+)*"))
            throw new IllegalArgumentException("Invalid version format");
        this.version = version;
    }

    @Override
    public int compareTo(Version that) {
        if (that == null)
            return 1;
        String[] thisParts = this.get().split("\\.");
        String[] thatParts = that.get().split("\\.");
        int length = Math.max(thisParts.length, thatParts.length);
        for (int i = 0; i < length; i++) {
            int thisPart = i < thisParts.length ?
                    Integer.parseInt(thisParts[i]) : 0;
            int thatPart = i < thatParts.length ?
                    Integer.parseInt(thatParts[i]) : 0;
            if (thisPart < thatPart)
                return -1;
            if (thisPart > thatPart)
                return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (that == null)
            return false;
        if (this.getClass() != that.getClass())
            return false;
        return this.compareTo((Version) that) == 0;
    }

    public boolean isLesserOrEqualTo(Version thatVersion) {
        int result = compareTo(thatVersion);
        return result < 0;
    }
}