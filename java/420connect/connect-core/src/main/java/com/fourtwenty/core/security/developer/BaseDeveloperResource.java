package com.fourtwenty.core.security.developer;

import com.fourtwenty.core.security.tokens.DeveloperAuthToken;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 2/2/17.
 */
public abstract class BaseDeveloperResource {
    @Inject
    protected Provider<DeveloperAuthToken> token;

    public DeveloperAuthToken getDeveloperToken() {
        if (token == null) {
            return null;
        }
        return token.get();
    }

    protected Response ok() {
        return Response.ok().build();
    }
}
