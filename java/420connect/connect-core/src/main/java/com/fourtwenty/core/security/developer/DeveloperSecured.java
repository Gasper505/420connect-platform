package com.fourtwenty.core.security.developer;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mdo on 2/2/17.
 */
@Target({ElementType.TYPE, ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DeveloperSecured {
    boolean required() default true;

    ConnectAuthToken.ConnectAppType appType() default ConnectAuthToken.ConnectAppType.Blaze;
}