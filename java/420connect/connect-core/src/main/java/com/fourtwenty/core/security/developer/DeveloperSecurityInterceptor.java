package com.fourtwenty.core.security.developer;

import com.fourtwenty.core.exceptions.DeveloperAuthException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by mdo on 2/2/17.
 */
public class DeveloperSecurityInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object obj = invocation.getThis();
        if (obj instanceof BaseDeveloperResource) {
            BaseDeveloperResource resource = (BaseDeveloperResource) obj;

            if (resource.getDeveloperToken().isValid()) {
                return invocation.proceed();
            } else {
                throw new DeveloperAuthException("accessToken", "Invalid Developer Key");
            }
        }
        return invocation.proceed();
    }
}
