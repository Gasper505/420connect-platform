package com.fourtwenty.core.security.developer;

/**
 * Created by mdo on 2/2/17.
 */

import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.security.tokens.DeveloperAuthToken;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import com.google.inject.servlet.RequestScoped;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ContainerRequest;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;


@RequestScoped
public class DeveloperSecurityProvider implements Provider<DeveloperAuthToken> {

    final SecurityUtil securityUtil;
    @Inject
    Provider<ServiceLocator> serviceLocatorProvider;
    @Inject
    Provider<UriInfo> uriInfoProvider;
    @Inject
    DeveloperKeyRepository developerKeyRepository;


    @Inject
    public DeveloperSecurityProvider(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }


    @Override
    public DeveloperAuthToken get() {
        ContainerRequestContext context = getContainerRequestContext(serviceLocatorProvider.get());

        DeveloperAuthToken token = runDeveloperSecurityCheck(context, securityUtil, uriInfoProvider.get());
        if (token == null) {
            token = new DeveloperAuthToken();
        }
        return token;
    }


    public DeveloperAuthToken runDeveloperSecurityCheck(ContainerRequestContext requestContext, SecurityUtil securityUtil, UriInfo uriInfo) {
        ContainerRequest request = (ContainerRequest) requestContext.getRequest();
        DeveloperAuthToken token = (DeveloperAuthToken) requestContext.getProperty("developerToken");

        if (token != null) return token;

        // check query params
        String authString = getToken(request, "Authorization");
        String accessToken = authString;

        if (accessToken == null) {
            return null;
        }

        if (!StringUtils.isBlank(accessToken)) {
            try {
                //token = securityUtil.decryptAccessToken(accessToken);
                token = new DeveloperAuthToken();
                DeveloperKey developerKey = developerKeyRepository.getDeveloperKey(accessToken);
                if (developerKey != null) {
                    token.setCompanyId(developerKey.getCompanyId());
                    token.setShopId(developerKey.getShopId());
                }
                request.setProperty("developerToken", token);
            } catch (Exception e) {
                //throw new BlazeAuthException("Authorization", "Invalid AccessToken");
                return null;
            }
        }
        return token;
    }


    private static String getToken(ContainerRequest request, String propertyName) {
        String accessToken = request.getHeaderString(propertyName);

        return accessToken;
    }


    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }
}
