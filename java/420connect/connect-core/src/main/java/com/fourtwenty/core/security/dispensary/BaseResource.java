package com.fourtwenty.core.security.dispensary;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.ws.rs.core.Response;

public abstract class BaseResource {
    @Inject
    protected Provider<ConnectAuthToken> token;

    public ConnectAuthToken getToken() {
        if (token == null) {
            return null;
        }
        return token.get();
    }

    protected Response ok() {
        return Response.ok().build();
    }
}
