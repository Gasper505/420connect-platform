package com.fourtwenty.core.security.dispensary;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {
    boolean required() default true;

    boolean requiredShop() default false;

    boolean checkTerminal() default false;

    ConnectAuthToken.ConnectAppType appType() default ConnectAuthToken.ConnectAppType.Connect;
}