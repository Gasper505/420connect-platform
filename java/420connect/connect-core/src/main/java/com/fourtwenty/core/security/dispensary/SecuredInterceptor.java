package com.fourtwenty.core.security.dispensary;

import com.fourtwenty.core.exceptions.BlazeAuthException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class SecuredInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object obj = invocation.getThis();
        if (obj instanceof BaseResource) {
            BaseResource resource = (BaseResource) obj;

            if (resource.getToken().isNormalValid()) {
                if (resource.getToken().isValidTTL()) {
                    return invocation.proceed();
                } else {
                    throw new BlazeAuthException("accessToken", "Access Token Expired.");
                }
            } else {
                throw new BlazeAuthException("accessToken", "Invalid AccessToken");
            }
        }
        return invocation.proceed();
    }

}
