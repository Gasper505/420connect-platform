package com.fourtwenty.core.security.dispensary;

import com.fourtwenty.core.security.AuthenticationFilter;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import com.google.inject.servlet.RequestScoped;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;

@RequestScoped
public class SecurityProvider implements Provider<ConnectAuthToken> {

    final SecurityUtil securityUtil;
    @Inject
    Provider<ServiceLocator> serviceLocatorProvider;
    @Inject
    Provider<UriInfo> uriInfoProvider;


    @Inject
    public SecurityProvider(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }


    @Override
    public ConnectAuthToken get() {
        ContainerRequestContext context = getContainerRequestContext(serviceLocatorProvider.get());

        ConnectAuthToken token = AuthenticationFilter.runSecurityCheck(context, securityUtil, uriInfoProvider.get());
        if (token == null) {
            token = new ConnectAuthToken();
        }
        return token;
    }


    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }
}
