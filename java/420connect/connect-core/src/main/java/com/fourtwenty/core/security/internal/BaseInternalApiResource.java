package com.fourtwenty.core.security.internal;

import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.google.inject.Provider;

import javax.ws.rs.core.Response;

public class BaseInternalApiResource {

    protected InternalApiAuthToken token;

    public BaseInternalApiResource(Provider<InternalApiAuthToken> tokenProvider) {
        this.token = tokenProvider.get();
    }

    public InternalApiAuthToken getToken() {

        if (token == null) {
            return null;
        }
        return token;
    }

    protected Response ok() {
        return Response.ok().build();
    }
}
