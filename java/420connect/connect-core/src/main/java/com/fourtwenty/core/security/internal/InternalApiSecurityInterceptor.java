package com.fourtwenty.core.security.internal;

import com.fourtwenty.core.exceptions.BlazeAuthException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class InternalApiSecurityInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object obj = invocation.getThis();
        if (obj instanceof BaseInternalApiResource) {
            BaseInternalApiResource resource = (BaseInternalApiResource) obj;

            if (resource.getToken().isValid()) {
                return invocation.proceed();
            } else {
                throw new BlazeAuthException("Authorization", "Invalid internal api request");
            }
        }
        return invocation.proceed();
    }
}
