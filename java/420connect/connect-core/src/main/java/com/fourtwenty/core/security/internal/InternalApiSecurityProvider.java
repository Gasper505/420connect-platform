package com.fourtwenty.core.security.internal;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.servlet.RequestScoped;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ContainerRequest;
import org.joda.time.DateTime;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.container.ContainerRequestContext;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;


@RequestScoped
public class InternalApiSecurityProvider implements Provider<InternalApiAuthToken> {

    public static final String TOKEN_KEY = "internalApiToken";

    final SecurityUtil securityUtil;

    @Inject
    private ConnectConfiguration config;

    @Inject
    Provider<ServiceLocator> serviceLocatorProvider;


    @Inject
    public InternalApiSecurityProvider(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }

    @Override
    public InternalApiAuthToken get() {
        ContainerRequestContext context = getContainerRequestContext(serviceLocatorProvider.get());
        InternalApiAuthToken token = runDeveloperSecurityCheck(context);
        if (token == null) {
            token = new InternalApiAuthToken();
        }
        return token;
    }


    private InternalApiAuthToken runDeveloperSecurityCheck(ContainerRequestContext requestContext) {
        ContainerRequest request = (ContainerRequest) requestContext.getRequest();
        InternalApiAuthToken token = (InternalApiAuthToken) requestContext.getProperty(TOKEN_KEY);


        String authString = getToken(request, "auth_signature");
        String timestamp = getToken(request, "auth_timestamp");
        String timezone = getToken(request, "timezone");
        String tokenString = getToken(request, "Authorization");

        if (StringUtils.isBlank(timezone)) {
            timezone = "UTC";
        }

        if (StringUtils.isBlank(authString) || StringUtils.isBlank(tokenString)
                || StringUtils.isBlank(timestamp) || !timestamp.matches("\\d+")) return null;

        if (token == null) {
            token = new InternalApiAuthToken();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            if(!(formatter.format(new Date()).equals(formatter.format(new Date(Long.parseLong(timestamp)))))) return  null;
        }

        if (StringUtils.isNotEmpty(tokenString)) {
            String[] parts = tokenString.split(" ");
            if (parts.length != 2 || !securityUtil.isInternalApiToken(parts[1])) return null;
            ConnectAuthToken authToken = securityUtil.decryptInternalApiToken(parts[1]);
            token.setShopId(authToken.getShopId());
            token.setCompanyId(authToken.getCompanyId());
            token.setExpirationDate(authToken.getExpirationDate());
        }

        token.setVerified(checkHMACSignature(authString, timestamp, timezone));
        request.setProperty(TOKEN_KEY, token);
        return token;
    }

    private static String getToken(ContainerRequest request, String propertyName) {
        String accessToken = request.getHeaderString(propertyName);
        if (StringUtils.isBlank(accessToken)) {
            String query = request.getRequestUri().getQuery();
            if (query != null && query.contains(propertyName)) {
                String[] queryParts = query.split("&");
                for (String qp : queryParts) {
                    if (qp.startsWith(propertyName)) {
                        return qp.replaceAll(propertyName + "=", "");
                    }
                }
            }
        }
        return accessToken;
    }

    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }

    private Boolean checkHMACSignature(String authString, String timestamp, String timezone) {

        DateTime time = new DateTime(DateUtil.toDateTime(Long.parseLong(timestamp), timezone)
                .plusMinutes(config.getAuthTTL())
                .getMillis());

        if(!time.isAfterNow()) return Boolean.FALSE;

        String signature = generateHMACSignature(timestamp, config.getAppSecret());
        return authString.equals(securityUtil.encryptByInternalApiSecret(signature));
    }

    private String generateHMACSignature(String data, String secret) {
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(new SecretKeySpec(secret.getBytes(), "HmacSHA1"));
            return Base64.getEncoder().encodeToString(mac.doFinal(data.getBytes("UTF-8")));
        } catch (Exception e) {
            throw new BlazeAuthException("HMAC", "HMAC Hash generation Failed");
        }
    }
}
