package com.fourtwenty.core.security.internal;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface InternalSecured {

    boolean required() default false;

    boolean requiredShop() default false;

    boolean requiredExternalId() default false;

    ConnectAuthToken.ConnectAppType appType() default ConnectAuthToken.ConnectAppType.Grow;
}
