package com.fourtwenty.core.security.onprem;

import com.fourtwenty.core.security.tokens.OnPremToken;
import com.google.inject.Provider;

import javax.ws.rs.core.Response;

public class OnPremBaseResource {
    protected OnPremToken token;

    public OnPremBaseResource(Provider<OnPremToken> tokenProvider) {
        this.token = tokenProvider.get();
    }

    public OnPremToken getToken() {
        if (token == null) {
            return null;
        }
        return token;
    }

    protected Response ok() {
        return Response.ok().build();
    }
}
