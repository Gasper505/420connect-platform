package com.fourtwenty.core.security.onprem;

import com.fourtwenty.core.exceptions.BlazeAuthException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class OnPremInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object obj = invocation.getThis();
        if (obj instanceof OnPremBaseResource) {
            OnPremBaseResource resource = (OnPremBaseResource) obj;

            if (resource.getToken().isValid()) {
                return invocation.proceed();
            } else {
                throw new BlazeAuthException("accessToken", "OnPrem is not enabled or Invalid Signature");
            }
        }
        return invocation.proceed();
    }
}
