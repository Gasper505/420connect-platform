package com.fourtwenty.core.security.onprem;


import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.security.tokens.OnPremToken;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;


public class OnPremProvider implements Provider<OnPremToken> {

    private final ConnectConfiguration config;
    private final SecurityUtil securityUtil;

    @Inject
    Provider<HttpServletRequest> requestProvider;

    @Inject
    private CompanyRepository companyRepository;

    @Inject
    public OnPremProvider(SecurityUtil securityUtil, ConnectConfiguration config) {
        this.securityUtil = securityUtil;
        this.config = config;
    }

    @Override
    public OnPremToken get() {

        OnPremToken token = runSecurityCheck();
        if (token == null) {
            token = new OnPremToken();
        }
        return token;
    }

    private OnPremToken runSecurityCheck() {

        OnPremToken token = null;

        String companyId = requestProvider.get().getHeader("companyId");
        String signature = requestProvider.get().getHeader("signature");


        if (StringUtils.isNoneBlank(companyId) && StringUtils.isNoneBlank(signature)) {
            Company company = companyRepository.getById(companyId);
            if (company != null && company.getOnPremCompanyConfig() != null && company.getOnPremCompanyConfig().isOnPremEnable()) {
                String onPremKey = company.getOnPremCompanyConfig().getOnPremKey();
                String iv = company.getOnPremCompanyConfig().getIv();
                if (StringUtils.isNotBlank(onPremKey) && StringUtils.isNotBlank(iv)) {
                    String decryptedSignature = securityUtil.decryptByOnPremApiSecret(signature, onPremKey, iv);
                    if (companyId.equals(decryptedSignature)) {
                        token = new OnPremToken();
                        token.setOnPremKey(onPremKey);
                        token.setOnPremEnabled(Boolean.TRUE);
                        token.setCompanyId(companyId);
                    }
                }
            }
        }
        return token;
    }
}
