package com.fourtwenty.core.security.partner;

import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.google.inject.Provider;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 2/8/18.
 */
public abstract class BasePartnerDevResource {
    protected PartnerAuthToken token;

    public BasePartnerDevResource(Provider<PartnerAuthToken> tokenProvider) {
        this.token = tokenProvider.get();
    }

    public PartnerAuthToken getPartnerToken() {
        if (token == null) {
            return null;
        }
        return token;
    }

    protected Response ok() {
        return Response.ok().build();
    }
}
