package com.fourtwenty.core.security.partner;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mdo on 2/8/18.
 */
@Target({ElementType.TYPE, ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PartnerDeveloperSecured {
    boolean required() default true;

    boolean userRequired() default false;
    boolean storeRequired() default true;

    ConnectAuthToken.ConnectAppType appType() default ConnectAuthToken.ConnectAppType.Blaze;
}