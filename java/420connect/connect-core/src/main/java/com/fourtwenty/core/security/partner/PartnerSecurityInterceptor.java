package com.fourtwenty.core.security.partner;

import com.fourtwenty.core.exceptions.BlazeAuthException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by mdo on 4/21/17.
 */
public class PartnerSecurityInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object obj = invocation.getThis();
        if (obj instanceof BasePartnerDevResource) {
            BasePartnerDevResource resource = (BasePartnerDevResource) obj;

            if (resource.getPartnerToken().isValid()) {
                return invocation.proceed();
            } else {
                throw new BlazeAuthException("accessToken", "Invalid PARTNER/Store Key");
            }
        }
        return invocation.proceed();
    }
}
