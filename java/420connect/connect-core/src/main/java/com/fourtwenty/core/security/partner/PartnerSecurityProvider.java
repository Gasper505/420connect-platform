package com.fourtwenty.core.security.partner;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.domain.repositories.store.StoreWidgetKeyRepository;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.developer.PartnerKeyService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ContainerRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;

/**
 * Created by mdo on 4/21/17.
 */
public class PartnerSecurityProvider implements Provider<PartnerAuthToken> {
    private static final Logger LOG = Logger.getLogger(PartnerSecurityProvider.class);

    public static final String TOKEN_KEY = "partnerToken";
    public static final String CONSUMER_UID = "cuid";

    final SecurityUtil securityUtil;
    @Inject
    Provider<ServiceLocator> serviceLocatorProvider;
    @Inject
    Provider<UriInfo> uriInfoProvider;
    @Inject
    StoreWidgetKeyRepository widgetKeyRepository;
    @Inject
    PartnerKeyService partnerKeyService;
    @Inject
    ConnectConfiguration configuration;
    @Inject
    Provider<HttpServletRequest> requestProvider;
    @Inject
    ShopRepository shopRepository;
    @Inject
    DeveloperKeyRepository developerKeyRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    ConsumerUserRepository consumerUserRepository;


    @Inject
    public PartnerSecurityProvider(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }


    @Override
    public PartnerAuthToken get() {
        ContainerRequestContext context = getContainerRequestContext(serviceLocatorProvider.get());

        PartnerAuthToken token = runDeveloperSecurityCheck(context, securityUtil, uriInfoProvider.get());
        if (token == null) {
            token = new PartnerAuthToken();
        }
        return token;
    }


    public PartnerAuthToken runDeveloperSecurityCheck(ContainerRequestContext requestContext, SecurityUtil securityUtil, UriInfo uriInfo) {
        ContainerRequest request = (ContainerRequest) requestContext.getRequest();
        PartnerAuthToken token = (PartnerAuthToken) requestContext.getProperty(TOKEN_KEY);

        if (token != null) return token;

        HttpServletRequest servletRequest = requestProvider.get();
        // check query params
        String partnerAPIKey = getToken(request, "partner_key");
        String authString = getToken(request, "Authorization"); // Developer Key
        String accessToken = authString;


        if (partnerAPIKey == null) {
            return null;
        }
        if (accessToken == null) {
            return null;
        }
        String remoteAddr = servletRequest.getRemoteAddr();
        String remoteHost = servletRequest.getRemoteHost();
        String origin = servletRequest.getHeader("Origin");
        String referer = servletRequest.getHeader("Referer");
        //LOG.info("remoteAddr: " + remoteAddr);
        //LOG.info("remoteHost: " + remoteHost);
        //LOG.info("origin: " + origin);
        //LOG.info("referer: " + referer);

        if (StringUtils.isNotBlank(accessToken) && StringUtils.isNotBlank(partnerAPIKey)) {

            DeveloperKey developerKey = developerKeyRepository.getDeveloperKey(accessToken);
            PartnerKey partnerKey = partnerKeyService.getPartnerKey(partnerAPIKey);

            if (developerKey == null || partnerKey == null || developerKey.isActive() == false || partnerKey.isActive() == false) {
                return null;
            }

            Company company = companyRepository.getById(developerKey.getCompanyId());

            if (company == null || company.isActive() == false) {
                return null;
            }

            // query the store itself
            Shop shop = shopRepository.get(developerKey.getCompanyId(), developerKey.getShopId());
            if (shop == null) {
                return null;
            }

            // PARTNER_KEY requires Online Store to be enabled.
            boolean storeEnabled = false;
            if (partnerKey == null) {
                if (shop.getOnlineStoreInfo() == null || !shop.getOnlineStoreInfo().isEnabled()) {
                    return null;
                }
            } else {
                storeEnabled = shop.getOnlineStoreInfo() != null && shop.getOnlineStoreInfo().isEnabled();
            }


            token = new PartnerAuthToken();
            token.setSource(partnerKey.getName());
            token.setCompanyId(developerKey.getCompanyId());
            token.setShopId(developerKey.getShopId());
            token.setConsumerId(null);
            token.setAuthenticated(false);
            token.setPartnerKey(partnerAPIKey);
            token.setDeveloperKey(accessToken);
            token.setStoreEnabled(storeEnabled);

            String cuid = getToken(request, CONSUMER_UID);
            if (StringUtils.isNotBlank(cuid)) {
                ConsumerUser consumerUser = consumerUserRepository.getById(cuid);
                if (consumerUser != null) {
                    token.setAuthenticated(consumerUser.isActive());
                    token.setConsumerId(consumerUser.getId());
                }
            }
            request.setProperty(TOKEN_KEY, token);
        }
        return token;
    }


    private static String getToken(ContainerRequest request, String propertyName) {
        String accessToken = request.getHeaderString(propertyName);
        if (StringUtils.isBlank(accessToken)) {
            String query = request.getRequestUri().getQuery();
            if (query != null && query.contains(propertyName)) {
                String[] queryParts = query.split("&");
                for (String qp : queryParts) {
                    if (qp.startsWith(propertyName)) {
                        return qp.replaceAll(propertyName + "=", "");
                    }
                }
            }
        }
        return accessToken;
    }


    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }
}
