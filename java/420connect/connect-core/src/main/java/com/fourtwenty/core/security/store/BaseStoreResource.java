package com.fourtwenty.core.security.store;

import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.ws.rs.core.Response;

/**
 * Created by mdo on 4/21/17.
 */
public abstract class BaseStoreResource {
    @Inject
    protected Provider<StoreAuthToken> token;

    public StoreAuthToken getStoreToken() {
        if (token == null) {
            return null;
        }
        return token.get();
    }

    protected Response ok() {
        return Response.ok().build();
    }
}
