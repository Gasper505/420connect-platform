package com.fourtwenty.core.security.store;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mdo on 4/21/17.
 */
@Target({ElementType.TYPE, ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface StoreSecured {
    boolean required() default true;

    boolean authRequired() default false;

    ConnectAuthToken.ConnectAppType appType() default ConnectAuthToken.ConnectAppType.StoreWidget;
}
