package com.fourtwenty.core.security.store;

import com.fourtwenty.core.exceptions.BlazeAuthException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by mdo on 4/21/17.
 */
public class StoreSecurityInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object obj = invocation.getThis();
        if (obj instanceof BaseStoreResource) {
            BaseStoreResource resource = (BaseStoreResource) obj;

            if (resource.getStoreToken().isValid()) {
                return invocation.proceed();
            } else {
                throw new BlazeAuthException("accessToken", "Invalid Store Key");
            }
        }
        return invocation.proceed();
    }
}
