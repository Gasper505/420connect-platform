package com.fourtwenty.core.security.store;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.models.store.StoreWidgetKey;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.domain.repositories.store.StoreWidgetKeyRepository;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.developer.PartnerKeyService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ContainerRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;

/**
 * Created by mdo on 4/21/17.
 */
public class StoreSecurityProvider implements Provider<StoreAuthToken> {
    private static final Logger LOG = Logger.getLogger(StoreSecurityProvider.class);

    public static final String TOKEN_KEY = "storeToken";
    public static final String CONSUMER_UID = "cuid";

    final SecurityUtil securityUtil;
    @Inject
    Provider<ServiceLocator> serviceLocatorProvider;
    @Inject
    Provider<UriInfo> uriInfoProvider;
    @Inject
    StoreWidgetKeyRepository widgetKeyRepository;
    @Inject
    ConnectConfiguration configuration;
    @Inject
    Provider<HttpServletRequest> requestProvider;
    @Inject
    ShopRepository shopRepository;
    @Inject
    DeveloperKeyRepository developerKeyRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    PartnerKeyService partnerKeyService;
    @Inject
    ConsumerUserRepository consumerUserRepository;


    @Inject
    public StoreSecurityProvider(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }


    @Override
    public StoreAuthToken get() {
        ContainerRequestContext context = getContainerRequestContext(serviceLocatorProvider.get());

        StoreAuthToken token = runDeveloperSecurityCheck(context, securityUtil, uriInfoProvider.get());
        if (token == null) {
            token = new StoreAuthToken();
        }
        return token;
    }


    public StoreAuthToken runDeveloperSecurityCheck(ContainerRequestContext requestContext, SecurityUtil securityUtil, UriInfo uriInfo) {
        ContainerRequest request = (ContainerRequest) requestContext.getRequest();
        StoreAuthToken token = (StoreAuthToken) requestContext.getProperty(TOKEN_KEY);

        if (token != null) return token;

        HttpServletRequest servletRequest = requestProvider.get();
        // check query params
        String apiKey = getToken(request, "api_key");
        String accessToken = getToken(request, "Authorization");

        String partnerAPIKey = getToken(request, "partner_key");

        if (apiKey == null && partnerAPIKey == null) {
            return null;
        }
        String remoteAddr = servletRequest.getRemoteAddr();
        String remoteHost = servletRequest.getRemoteHost();
        String origin = servletRequest.getHeader("Origin");
        String referer = servletRequest.getHeader("Referer");
        //LOG.info("remoteAddr: " + remoteAddr);
        //LOG.info("remoteHost: " + remoteHost);
        //LOG.info("origin: " + origin);
        //LOG.info("referer: " + referer);

        if (StringUtils.isNotBlank(partnerAPIKey)) {
            token = getPartnerStoreToken(request, partnerAPIKey, accessToken);
        } else if (StringUtils.isNotBlank(apiKey)) {
            token = getStoreAuthToken(request, apiKey, accessToken);
        }

        request.setProperty(TOKEN_KEY, token);
        return token;
    }

    private StoreAuthToken getPartnerStoreToken(ContainerRequest request, String partnerAPIKey, String accessToken) {
        DeveloperKey developerKey = developerKeyRepository.getDeveloperKey(accessToken);
        PartnerKey partnerKey = partnerKeyService.getPartnerKey(partnerAPIKey);

        if (developerKey == null || partnerKey == null || developerKey.isActive() == false || partnerKey.isActive() == false) {
            return null;
        }

        Company company = companyRepository.getById(developerKey.getCompanyId());

        if (company == null || company.isActive() == false) {
            return null;
        }

        // query the store itself
        Shop shop = shopRepository.get(developerKey.getCompanyId(), developerKey.getShopId());
        if (shop == null) {
            return null;
        }


        // PARTNER_KEY requires Online Store to be enabled.
        boolean storeEnabled = false;
        if (partnerKey == null) {
            if (shop.getOnlineStoreInfo() == null || !shop.getOnlineStoreInfo().isEnabled()) {
                return null;
            }
        } else {
            storeEnabled = shop.getOnlineStoreInfo() != null && shop.getOnlineStoreInfo().isEnabled();
        }


        StoreAuthToken token = new StoreAuthToken();
        token.setCompanyId(developerKey.getCompanyId());
        token.setShopId(developerKey.getShopId());
        token.setConsumerId(null);
        token.setAuthenticated(false);
        token.setSource(partnerKey.getName());
        token.setEnableSales(developerKey.isEnableExposeSales());
        token.setEnableMembers(developerKey.isEnableExposeMembers());

        token.setStoreEnabled(storeEnabled);

        String cuid = getToken(request, CONSUMER_UID);
        if (StringUtils.isNotBlank(cuid)) {
            ConsumerUser consumerUser = consumerUserRepository.getById(cuid);
            if (consumerUser != null) {
                token.setAuthenticated(consumerUser.isActive());
                token.setConsumerId(consumerUser.getId());
            }
        }

        return token;

    }

    private StoreAuthToken getStoreAuthToken(ContainerRequest request, String apiKey, String accessToken) {

        StoreWidgetKey storeKey = widgetKeyRepository.getStoreKey(apiKey);
        if (storeKey == null) {
            return null;
        }
        if (configuration != null
                && configuration.getEnv() == ConnectConfiguration.EnvironmentType.PROD) {
            if (!storeKey.isActive()) {
                return null;
            }
        }

        // query the store itself
        Shop shop = shopRepository.get(storeKey.getCompanyId(), storeKey.getShopId());
        if (shop == null) {
            return null;
        }
        if (shop.getOnlineStoreInfo() == null || !shop.getOnlineStoreInfo().isEnabled()) {
            return null;
        }


        StoreAuthToken token = new StoreAuthToken();
        token.setCompanyId(storeKey.getCompanyId());
        token.setShopId(storeKey.getShopId());
        token.setConsumerId(null);
        token.setAuthenticated(false);
        if (StringUtils.isNotBlank(accessToken)) {

            accessToken = accessToken.replaceFirst("Token ", "");
            try {
                // decrypt member auth store token
                StoreAuthToken memberToken = securityUtil.decryptStoreToken(accessToken);
                if (memberToken != null) {
                    token.setAuthenticated(true);
                    token.setConsumerId(memberToken.getConsumerId());
                }
            } catch (Exception e) {
                // ignore
            }
        }

        request.setProperty(TOKEN_KEY, token);
        return token;
    }


    private static String getToken(ContainerRequest request, String propertyName) {
        String accessToken = request.getHeaderString(propertyName);
        if (StringUtils.isBlank(accessToken)) {
            String query = request.getRequestUri().getQuery();
            if (query != null && query.contains(propertyName)) {
                String[] queryParts = query.split("&");
                for (String qp : queryParts) {
                    if (qp.startsWith(propertyName)) {
                        return qp.replaceAll(propertyName + "=", "");
                    }
                }
            }
        }
        return accessToken;
    }


    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }
}
