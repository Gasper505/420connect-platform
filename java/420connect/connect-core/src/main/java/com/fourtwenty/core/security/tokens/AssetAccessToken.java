package com.fourtwenty.core.security.tokens;

/**
 * Created by mdo on 5/25/16.
 */
public class AssetAccessToken {
    private String companyId;
    private String shopId;
    private String userId;
    private long signedDate;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public long getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(long signedDate) {
        this.signedDate = signedDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
