package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.security.IAuthToken;
import com.fourtwenty.core.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 8/28/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectAuthToken implements IAuthToken {
    public static final String DEFAULT_REQ_TIMEZONE = "America/Los_Angeles";


    public enum ConnectAppType {
        Connect,
        Blaze,
        Developer,
        StoreWidget,
        Distribution,
        Grow,
        DeliveryApp
    }

    public enum GrowAppType {
        None,
        Sales,
        Operations
    }

    private String companyId;
    private String shopId;
    private String managerId;
    private List<TerminalUser> activeUsers = new ArrayList<>();
    private long initDate;
    private String terminalId;
    private String tokenId;
    private String activeDeviceId;
    private long expirationDate;
    private String timeCardId; // related to timecardDEFAULT_TIMEZONE_OFFSET
    private String employeeSessoinId; // related to timecard
    private ConnectAppType appType = ConnectAppType.Connect;
    private String consumerUserId;
    private String requestTimeZone = DEFAULT_REQ_TIMEZONE;
    private Company.CompanyMembersShareOption membersShareOption = Company.CompanyMembersShareOption.Shared;
    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Retail;
    private Map<String, String> appLinks;
    private GrowAppType growAppType = GrowAppType.None;

    public ConnectAuthToken copy() {
        ConnectAuthToken connectAuthToken = new ConnectAuthToken();
        connectAuthToken.setCompanyId(companyId);
        connectAuthToken.setShopId(shopId);
        connectAuthToken.setManagerId(managerId);
        connectAuthToken.setActiveUsers(activeUsers);
        connectAuthToken.setInitDate(initDate);
        connectAuthToken.setTerminalId(terminalId);
        connectAuthToken.setTokenId(tokenId);
        connectAuthToken.setActiveDeviceId(activeDeviceId);
        connectAuthToken.setExpirationDate(expirationDate);
        connectAuthToken.setTimeCardId(timeCardId);
        connectAuthToken.setEmployeeSessoinId(employeeSessoinId);
        connectAuthToken.setAppType(appType);
        connectAuthToken.setConsumerUserId(consumerUserId);
        connectAuthToken.setRequestTimeZone(DEFAULT_REQ_TIMEZONE);
        connectAuthToken.setMembersShareOption(membersShareOption);
        connectAuthToken.setAppTarget(appTarget);
        connectAuthToken.setGrowAppType(growAppType);
        return connectAuthToken;
    }

    public boolean isValid() {
        if (initDate == 0 || companyId == null || managerId == null) {
            return false;
        }
        return isValidTTL();
    }

    public boolean isNormalValid() {
        if (initDate == 0 || companyId == null || managerId == null) {
            return false;
        }
        return true;
    }

    public int getTimezoneOffsetInMinutes() {
        return DateUtil.getTimezoneOffsetInMinutes(getRequestTimeZone());
    }

    public String getRequestTimeZone() {
        if (StringUtils.isBlank(requestTimeZone)) {
            return DEFAULT_REQ_TIMEZONE;
        }
        return requestTimeZone;
    }

    public void setRequestTimeZone(String requestTimeZone) {
        this.requestTimeZone = requestTimeZone;
    }

    public String getConsumerUserId() {
        return consumerUserId;
    }

    public void setConsumerUserId(String consumerUserId) {
        this.consumerUserId = consumerUserId;
    }

    public boolean isValidTTL() {
        DateTime time = new DateTime(expirationDate);
        return time.isAfterNow();
    }

    public ConnectAppType getAppType() {
        return appType;
    }

    public void setAppType(ConnectAppType appType) {
        this.appType = appType;
    }

    public String getEmployeeSessoinId() {
        return employeeSessoinId;
    }

    public void setEmployeeSessoinId(String employeeSessoinId) {
        this.employeeSessoinId = employeeSessoinId;
    }

    public String getTimeCardId() {
        return timeCardId;
    }

    public void setTimeCardId(String timeCardId) {
        this.timeCardId = timeCardId;
    }

    public TerminalUser getActiveTopUser() {
        if (activeUsers != null && activeUsers.size() > 0) {
            return activeUsers.get(activeUsers.size() - 1);
        }
        return null;
    }


    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getActiveDeviceId() {
        return activeDeviceId;
    }

    public void setActiveDeviceId(String activeDeviceId) {
        this.activeDeviceId = activeDeviceId;
    }

    public List<TerminalUser> getActiveUsers() {
        return activeUsers;
    }

    public void setActiveUsers(List<TerminalUser> activeUsers) {
        this.activeUsers = activeUsers;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public long getInitDate() {
        return initDate;
    }

    public void setInitDate(long initDate) {
        this.initDate = initDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public Company.CompanyMembersShareOption getMembersShareOption() {
        return membersShareOption;
    }

    public void setMembersShareOption(Company.CompanyMembersShareOption membersShareOption) {
        this.membersShareOption = membersShareOption;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }

    public String getShopIdByShareOption() {
        if (getMembersShareOption() == Company.CompanyMembersShareOption.Isolated) {
            return getShopId();
        }
        return null;
    }

    public Map<String, String> getAppLinks() {
        return appLinks;
    }

    public void setAppLinks(Map<String, String> appLinks) {
        this.appLinks = appLinks;
    }

    public GrowAppType getGrowAppType() {
        return growAppType;
    }

    public void setGrowAppType(GrowAppType growAppType) {
        this.growAppType = growAppType;
    }
}
