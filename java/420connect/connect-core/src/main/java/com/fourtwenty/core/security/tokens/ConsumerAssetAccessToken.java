package com.fourtwenty.core.security.tokens;

/**
 * Created by mdo on 5/17/17.
 */
public class ConsumerAssetAccessToken {
    private String consumerId;
    private long signedDate;
    private long expirationDate;

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }


    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public long getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(long signedDate) {
        this.signedDate = signedDate;
    }
}
