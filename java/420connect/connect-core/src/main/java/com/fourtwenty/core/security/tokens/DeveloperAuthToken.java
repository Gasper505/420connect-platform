package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.security.IAuthToken;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by mdo on 2/2/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeveloperAuthToken implements IAuthToken {
    private String companyId;
    private String shopId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    @Override
    public String getTerminalId() {
        return null;
    }

    @Override
    public boolean isValid() {
        return StringUtils.isNotBlank(companyId);
    }

    @Override
    public boolean isValidTTL() {
        return false;
    }
}
