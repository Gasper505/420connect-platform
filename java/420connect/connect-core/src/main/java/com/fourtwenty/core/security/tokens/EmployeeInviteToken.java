package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.security.IAuthToken;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * Created on 9/11/17 10:04 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeInviteToken implements IAuthToken {

    private String companyId;
    private long initDate;
    private long expirationDate;
    private String employeeId;
    private String shopId;

    public String getCompanyId() {
        return companyId;
    }

    @Override
    public String getTerminalId() {
        return null;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public long getInitDate() {
        return initDate;
    }

    public void setInitDate(long initDate) {
        this.initDate = initDate;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean isValid() {
        DateTime time = new DateTime(expirationDate);
        return !StringUtils.isBlank(companyId) && time.isAfterNow();
    }

    @Override
    public boolean isValidTTL() {
        DateTime time = new DateTime(expirationDate);
        return time.isAfterNow();
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }
}
