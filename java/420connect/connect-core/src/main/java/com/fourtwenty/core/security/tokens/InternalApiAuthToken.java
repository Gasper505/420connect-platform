package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InternalApiAuthToken extends ConnectAuthToken {

    private CompanyFeatures.AppTarget appTarget = CompanyFeatures.AppTarget.Grow;
    private Boolean verified = Boolean.FALSE;
    private long expirationDate;
    private String companyId;
    private String shopId;

    public void setAppTarget(CompanyFeatures.AppTarget appTarget) {
        this.appTarget = appTarget;
    }

    public CompanyFeatures.AppTarget getAppTarget() {
        return appTarget;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    @Override
    public String getCompanyId() {
        return companyId;
    }

    @Override
    public String getShopId() {
        return shopId;
    }

    @Override
    public String getTerminalId() {
        return null;
    }

    @Override
    public boolean isValid() {
        return verified & this.isValidTTL();
    }

    @Override
    public boolean isValidTTL() {
        DateTime time = new DateTime(expirationDate);
        return time.isAfterNow();
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }
}
