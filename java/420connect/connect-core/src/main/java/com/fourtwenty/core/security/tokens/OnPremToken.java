package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnPremToken {

    private String onPremKey = StringUtils.EMPTY;
    private boolean onPremEnabled = false;
    private String companyId;

    public boolean isValid() {
        return StringUtils.isNotBlank(onPremKey) && onPremEnabled;
    }

    public String getOnPremKey() {
        return onPremKey;
    }

    public void setOnPremKey(String onPremKey) {
        this.onPremKey = onPremKey;
    }

    public boolean isOnPremEnabled() {
        return onPremEnabled;
    }

    public void setOnPremEnabled(boolean onPremEnabled) {
        this.onPremEnabled = onPremEnabled;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
