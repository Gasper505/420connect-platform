package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by mdo on 2/8/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerAuthToken extends StoreAuthToken {
    private String partnerKey;
    private String developerKey;
    private boolean storeEnabled = false;

    public String getDeveloperKey() {
        return developerKey;
    }

    public void setDeveloperKey(String developerKey) {
        this.developerKey = developerKey;
    }

    @Override
    public boolean isValid() {
        if (StringUtils.isBlank(partnerKey) || StringUtils.isBlank(developerKey)) {
            return false;
        }
        return super.isValid();
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }

    public boolean isStoreEnabled() {
        return storeEnabled;
    }

    public void setStoreEnabled(boolean storeEnabled) {
        this.storeEnabled = storeEnabled;
    }
}
