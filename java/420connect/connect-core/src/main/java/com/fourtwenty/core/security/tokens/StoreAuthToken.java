package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * Created by mdo on 4/21/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoreAuthToken extends DeveloperAuthToken {
    private String source = "OnlineWidget";
    private String consumerId;
    private boolean authenticated = false;
    private long expirationDate;
    private long loginDate;
    private boolean enableSales = false;
    private boolean enableMembers = Boolean.FALSE;
    private boolean storeEnabled = true;

    public boolean isStoreEnabled() {
        return storeEnabled;
    }

    public void setStoreEnabled(boolean storeEnabled) {
        this.storeEnabled = storeEnabled;
    }

    public boolean isEnableSales() {
        return enableSales;
    }

    public void setEnableSales(boolean enableSales) {
        this.enableSales = enableSales;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public long getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(long loginDate) {
        this.loginDate = loginDate;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isEnableMembers() {
        return enableMembers;
    }

    public void setEnableMembers(boolean enableMembers) {
        this.enableMembers = enableMembers;
    }

    @Override
    public boolean isValid() {
        return super.isValid() && StringUtils.isNotBlank(getShopId());
    }


    @Override
    public boolean isValidTTL() {
        if (authenticated == false) {
            return true;
        }
        DateTime time = new DateTime(expirationDate);
        return time.isAfterNow();
    }
}
