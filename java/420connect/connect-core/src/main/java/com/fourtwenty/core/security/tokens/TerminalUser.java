package com.fourtwenty.core.security.tokens;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;

/**
 * Created by mdo on 9/20/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TerminalUser {
    private String userId;
    private long signinDate;
    private String firstName;
    private String lastName;

    public TerminalUser() {

    }

    public TerminalUser(String userId, String firstName, String lastName) {
        this.userId = userId;
        this.signinDate = DateTime.now().getMillis();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getSigninDate() {
        return signinDate;
    }

    public void setSigninDate(long signinDate) {
        this.signinDate = signinDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
