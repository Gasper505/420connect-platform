package com.fourtwenty.core.services;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.google.inject.Provider;

/**
 * Created by mdo on 8/28/15.
 */
public class AbstractAuthServiceImpl {
    protected final ConnectAuthToken token;

    public AbstractAuthServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        this.token = tokenProvider.get();
    }

    public ConnectAuthToken getToken() {
        return token;
    }
}
