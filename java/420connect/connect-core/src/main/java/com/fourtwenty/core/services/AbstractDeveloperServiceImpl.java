package com.fourtwenty.core.services;

import com.fourtwenty.core.security.tokens.DeveloperAuthToken;
import com.google.inject.Provider;

/**
 * Created by mdo on 2/2/17.
 */
public class AbstractDeveloperServiceImpl {
    protected final DeveloperAuthToken devToken;

    public AbstractDeveloperServiceImpl(Provider<DeveloperAuthToken> tokenProvider) {
        this.devToken = tokenProvider.get();
    }

    public DeveloperAuthToken getDeveloperToken() {
        return devToken;
    }
}
