package com.fourtwenty.core.services;

import com.fourtwenty.core.security.tokens.OnPremToken;
import com.google.inject.Provider;

public abstract class AbstractOnPremServiceImpl {
    protected final OnPremToken onPremToken;

    protected AbstractOnPremServiceImpl(Provider<OnPremToken> provider) {
        this.onPremToken = provider.get();
    }

    public OnPremToken getOnPremToken() {
        return onPremToken;
    }
}
