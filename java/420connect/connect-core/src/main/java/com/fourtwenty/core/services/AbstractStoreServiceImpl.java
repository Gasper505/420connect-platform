package com.fourtwenty.core.services;

import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.google.inject.Provider;

/**
 * Created by mdo on 4/21/17.
 */
public class AbstractStoreServiceImpl {
    protected final StoreAuthToken storeToken;

    public AbstractStoreServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        this.storeToken = tokenProvider.get();
    }

    public StoreAuthToken getStoreToken() {
        return storeToken;
    }
}
