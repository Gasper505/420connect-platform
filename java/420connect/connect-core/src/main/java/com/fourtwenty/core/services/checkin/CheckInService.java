package com.fourtwenty.core.services.checkin;

import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.rest.checkin.request.CheckInApproveRequest;
import com.fourtwenty.core.rest.checkin.request.CheckInRequest;
import com.fourtwenty.core.rest.checkin.result.CheckInResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CheckInService {
    CheckIn addCheckIn(CheckInRequest request);

    CheckIn updateCheckIn(String kioskCheckInId, CheckIn request);

    CheckInResult getCheckIn(String kioskCheckInId);

    SearchResult<CheckInResult> getCheckIns(String startDate, String endDate, int start, int limit);

    CheckIn acceptCheckIn(String kioskCheckInId, CheckInApproveRequest request);

    SearchResult<CheckInResult> getCheckIns(long startDate, long endDate, int start, int limit);

}
