package com.fourtwenty.core.services.checkin.impl;

import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.checkin.CheckInRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.UniqueSequenceRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.checkin.request.CheckInApproveRequest;
import com.fourtwenty.core.rest.checkin.request.CheckInRequest;
import com.fourtwenty.core.rest.checkin.result.CheckInResult;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.checkin.CheckInService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.mgmt.impl.QueuePOSServiceImpl;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fourtwenty.core.domain.models.company.Role.Permission.WebMembersAddToQueue;
import static com.fourtwenty.core.domain.models.company.Role.Permission.iPadQueueAddMember;

public class CheckInServiceImpl extends AbstractAuthServiceImpl implements CheckInService {

    private static final String CHECKIN = "CheckIn";
    private static final String MEMBER_CANNOT_BLANK = "Member cannot be blank for check in";
    private static final String MEMBER_ID_INVALID = "Member Id is invalid";
    private static final String MEMBER_NOT_FOUND = "Member is not found.";
    private static final String CHECKIN_ID = "CheckIn id is invalid";
    private static final String CHECKIN_NOT_FOUND = "CheckIn not found.";
    private static final String CHECKIN_ALREADY_ACCEPTED = "CheckIn already accepted/declined.";
    private static final String CHECKIN_STATUS_NOT_FOUND = "CheckIn status cannot be blank.";


    @Inject
    private MemberRepository memberRepository;
    @Inject
    private CheckInRepository checkInRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private RoleService roleService;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private QueuePOSServiceImpl queuePOSService;

    @Inject
    private UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    private RealtimeService realtimeService;

    @Inject
    public CheckInServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Override method to add checkin from
     *
     * @param request : checkIn request
     * @return : checkIn
     */

    @Override
    public CheckIn addCheckIn(CheckInRequest request) {
        if (request == null || StringUtils.isBlank(request.getMemberId())) {
            throw new BlazeInvalidArgException(CHECKIN, MEMBER_CANNOT_BLANK);
        }
        if (!ObjectId.isValid(request.getMemberId())) {
            throw new BlazeInvalidArgException(CHECKIN, MEMBER_ID_INVALID);
        }

        Member dbMember = memberRepository.getById(request.getMemberId());
        if (dbMember == null) {
            throw new BlazeInvalidArgException(CHECKIN, MEMBER_NOT_FOUND);
        }

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), CheckIn.UNIQUE_SEQ_PRIORITY);
        String checkInNo = String.valueOf(sequence.getCount());

        String shopId = (StringUtils.isBlank(request.getShopId())) ? token.getShopId() : request.getShopId();
        CheckIn checkIn = new CheckIn();
        checkIn.prepare(token.getCompanyId());
        checkIn.setShopId(shopId);
        checkIn.setMemberId(request.getMemberId());
        checkIn.setCheckInTime(DateTime.now().getMillis());
        checkIn.setEmployeeId(token.getActiveTopUser().getUserId());
        checkIn.setCheckInNo(checkInNo);
        CheckIn checkin = checkInRepository.save(checkIn);

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.CheckInUpdateEvent,null);
        return checkin;

    }

    /**
     * Override method to update checkin
     *
     * @param checkInId : checkin id
     * @param request   : update request
     * @return : CheckIn
     */
    @Override
    public CheckIn updateCheckIn(String checkInId, CheckIn request) {
        if (StringUtils.isBlank(checkInId) && !ObjectId.isValid(checkInId)) {
            throw new BlazeInvalidArgException(CHECKIN, CHECKIN_ID);
        }

        CheckIn dbCheckIn = checkInRepository.getById(checkInId);
        if (dbCheckIn == null) {
            throw new BlazeInvalidArgException(CHECKIN, CHECKIN_NOT_FOUND);
        }
        if (request == null || StringUtils.isBlank(request.getMemberId())) {
            throw new BlazeInvalidArgException(CHECKIN, MEMBER_CANNOT_BLANK);
        }

        if (!ObjectId.isValid(request.getMemberId())) {
            throw new BlazeInvalidArgException(CHECKIN, MEMBER_ID_INVALID);
        }

        Member dbMember = memberRepository.getById(request.getMemberId());
        if (dbMember == null) {
            throw new BlazeInvalidArgException(CHECKIN, MEMBER_NOT_FOUND);
        }

        dbCheckIn.setMemberId(request.getMemberId());
        dbCheckIn.setCheckInTime(request.getCheckInTime());
        dbCheckIn.setActive(request.isActive());
        dbCheckIn = checkInRepository.update(token.getCompanyId(), checkInId, dbCheckIn);

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.CheckInUpdateEvent,null);
        return dbCheckIn;

    }

    /**
     * Override method to get checkIn by id
     *
     * @param checkInId : checkIn id
     * @return : checkIn
     */
    @Override
    public CheckInResult getCheckIn(String checkInId) {
        CheckInResult result = checkInRepository.get(token.getCompanyId(), checkInId, CheckInResult.class);
        prepareCheckInResult(Lists.newArrayList(result));
        return result;
    }

    /**
     * Override method to get checkin by date
     *
     * @param startDate : start date
     * @param endDate   : end date
     * @param start     : start
     * @param limit     : limit
     * @return : search result
     */

    @Override
    public SearchResult<CheckInResult> getCheckIns(String startDate, String endDate, int start, int limit) {
        limit = (limit == 0) ? 200 : limit;

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

        DateTime endDateTime;
        if (StringUtils.isNotBlank(endDate)) {
            endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
        } else {
            endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
        }

        DateTime startDateTime;
        if (StringUtils.isNotBlank(startDate)) {
            startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
        } else {
            startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        String timeZone = token.getRequestTimeZone();
        if (timeZone == null && shop != null) {
            timeZone = shop.getTimeZone();
        }

        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

        long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
        long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

        SearchResult<CheckInResult> result = checkInRepository.findActiveItemsWithDateAndLimit(token.getCompanyId(), token.getShopId(), timeZoneStartDateMillis, timeZoneEndDateMillis, start, limit, CheckInResult.class);

        prepareCheckInResult(result.getValues());

        return result;

    }

    /**
     * Override method to accept/decline setting
     *
     * @param checkInId : checkIn Id
     * @param request   : request
     * @return : checkIn
     */
    @Override
    public CheckIn acceptCheckIn(String checkInId, CheckInApproveRequest request) {
        if (StringUtils.isBlank(checkInId) && !ObjectId.isValid(checkInId)) {
            throw new BlazeInvalidArgException(CHECKIN, CHECKIN_ID);
        }

        CheckIn dbCheckIn = checkInRepository.getById(checkInId);
        if (dbCheckIn == null) {
            throw new BlazeInvalidArgException(CHECKIN, CHECKIN_NOT_FOUND);
        }

        if (request == null || request.getStatus() == null) {
            throw new BlazeInvalidArgException(CHECKIN, CHECKIN_STATUS_NOT_FOUND);
        }
        if (dbCheckIn.getStatus() != CheckIn.CheckInStatus.InProgress) {
            throw new BlazeInvalidArgException(CHECKIN, CHECKIN_ALREADY_ACCEPTED);
        }

        checkEmployeeAccessibility();

        if (request.isAddToQueue()) {
            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            long activeTransCount = transactionRepository.getActiveTransactionCount(token.getShopId(), dbCheckIn.getMemberId());
            if (shop.getNumAllowActiveTrans() <= activeTransCount) {
                throw new BlazeInvalidArgException(CHECKIN, String.format("Member has %s active orders in queue. Please complete current order(s) before accepting new one.", activeTransCount));
            }
        }

        if (request.getStatus() == CheckIn.CheckInStatus.Accepted) {
            dbCheckIn.setApprovedBy(token.getActiveTopUser().getUserId());
            dbCheckIn.setApprovedDate(DateTime.now().getMillis());
        } else if (request.getStatus() == CheckIn.CheckInStatus.Declined) {
            dbCheckIn.setApprovedBy(token.getActiveTopUser().getUserId());
            dbCheckIn.setApprovedDate(DateTime.now().getMillis());
        }
        dbCheckIn.setStatus(request.getStatus());


        if (request.getStatus() == CheckIn.CheckInStatus.Accepted && request.isAddToQueue()) {
            QueueAddMemberRequest addToQueueRequest = new QueueAddMemberRequest();
            addToQueueRequest.setMemberId(dbCheckIn.getMemberId());
            Transaction transaction = queuePOSService.addToQueue(request.getQueueName(), addToQueueRequest, -1);

            dbCheckIn.setStatus(CheckIn.CheckInStatus.AddedToQueue);
            dbCheckIn.setTransactionId(transaction.getId());
            dbCheckIn.setActive(false);
        }

        checkInRepository.update(token.getCompanyId(), checkInId, dbCheckIn);

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.CheckInUpdateEvent,null);
        return dbCheckIn;
    }

    /**
     * Private method to check if current logged in employee is admin or manager to accept checkin
     */
    private void checkEmployeeAccessibility() {
        Role role = null;
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee != null) {
            role = roleService.getRoleById(employee.getRoleId());
        }

        if (role == null ||
                !(role.getName().equalsIgnoreCase("Admin")
                        || role.getPermissions().contains(iPadQueueAddMember)
                        || role.getPermissions().contains(WebMembersAddToQueue))) {
            throw new BlazeInvalidArgException("Member", "You are not authorized to accept checkin request.");
        }
    }

    /**
     * Private method to prepare member checkin result.
     *
     * @param result : checkin result
     */
    private void prepareCheckInResult(List<CheckInResult> result) {
        List<ObjectId> memberIds = new ArrayList<>();

        for (CheckInResult checkIns : result) {
            memberIds.add(new ObjectId(checkIns.getMemberId()));
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), memberIds);

        for (CheckInResult checkIns : result) {
            checkIns.setMember(memberHashMap.get(checkIns.getMemberId()));
        }
    }

    /**
     * Get checkin using by timestamp
     *
     * @param startDate : long
     * @param endDate : long
     * @param start : int
     * @param limit : int
     * @return
     */
    @Override
    public SearchResult<CheckInResult> getCheckIns(long startDate, long endDate, int start, int limit) {
        startDate = (startDate < 0) ? 0 : startDate;
        endDate = (endDate == 0) ? DateTime.now().getMillis() : endDate;
        start  = (start < 0) ? 0 : start;
        limit = (limit == 0) ? 200 : limit;
        SearchResult<CheckInResult> result = checkInRepository.findActiveItemsWithDateAndLimit(token.getCompanyId(), token.getShopId(), startDate, endDate, start, limit, CheckInResult.class);
        prepareCheckInResult(result.getValues());
        return result;
    }
}
