package com.fourtwenty.core.services.comments;

import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.rest.comments.UserActivityAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface UserActivityService {
    UserActivity addUserActivity(final UserActivityAddRequest activityAddRequest);

    SearchResult<UserActivity> getAllCommentsByReferenceId(final String referenceId, final int start, final int limit);
}
