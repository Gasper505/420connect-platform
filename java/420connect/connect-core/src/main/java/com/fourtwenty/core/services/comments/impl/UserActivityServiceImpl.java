package com.fourtwenty.core.services.comments.impl;

import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.domain.repositories.comments.UserActivityRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.comments.UserActivityAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class UserActivityServiceImpl extends AbstractAuthServiceImpl implements UserActivityService {

    private UserActivityRepository userActivityRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(UserActivityServiceImpl.class);
    private static final String USER_ACTIVITY_NOT_FOUND = "No User comments found with this referenceId.";
    private static final String ACTIVITY = "Activity";
    private static final String ACTIVITY_NOT_EMPTY = "Reference can't be Empty.";

    @Inject
    public UserActivityServiceImpl(Provider<ConnectAuthToken> token,
                                   UserActivityRepository userActivityRepository) {
        super(token);
        this.userActivityRepository = userActivityRepository;
    }

    /**
     * This method is used to add new user activity.
     *
     * @param activityAddRequest
     * @return
     */
    @Override
    public UserActivity addUserActivity(UserActivityAddRequest activityAddRequest) {
        if (StringUtils.isBlank(activityAddRequest.getReferenceId())) {
            throw new BlazeInvalidArgException(ACTIVITY, ACTIVITY_NOT_EMPTY);
        }

        UserActivity userActivity = new UserActivity();
        userActivity.prepare(token.getCompanyId());
        userActivity.setShopId(token.getShopId());
        userActivity.setEmployeeId(token.getActiveTopUser().getUserId());
        userActivity.setMessage(activityAddRequest.getMessage());
        userActivity.setReferenceType(activityAddRequest.getReferenceType());
        userActivity.setReferenceId(activityAddRequest.getReferenceId());
        userActivity.setActivityType(activityAddRequest.getActivityType());
        return userActivityRepository.save(userActivity);

    }

    /**
     * This method is used to get all activities through referenceId
     *
     * @param referenceId
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<UserActivity> getAllCommentsByReferenceId(String referenceId, int start, int limit) {
        if (StringUtils.isBlank(referenceId)) {
            throw new BlazeInvalidArgException(ACTIVITY, ACTIVITY_NOT_EMPTY);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return userActivityRepository.getAllCommentsByReferenceId(token.getCompanyId(), referenceId, "{modified:-1}", start, limit);
    }
}

