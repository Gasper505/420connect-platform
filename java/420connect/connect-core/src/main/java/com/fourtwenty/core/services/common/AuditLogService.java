package com.fourtwenty.core.services.common;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;

/**
 * Created by mdo on 7/13/16.
 */
public interface AuditLogService {
    void addAuditLog(ConnectAuthToken connectAuthToken, String path, String category, String action);

    void addAuditLog(ConnectAuthToken connectAuthToken, String path, String category, String action, String data);
}
