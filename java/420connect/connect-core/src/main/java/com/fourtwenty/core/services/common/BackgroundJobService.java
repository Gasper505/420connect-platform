package com.fourtwenty.core.services.common;

/**
 * Created by mdo on 5/1/16.
 */
public interface BackgroundJobService {
    void addTransactionJob(String companyId, String shopId, String queuedTransactionId);
    void addComplianceSyncJob(String companyId, String shopId, String complianceSyncJobId);

    void addWeedmapSyncJob(String companyId, String shopId, String weedmapSyncJobId);

    void addLeaflymapSyncJob(String companyId, String shopId, String leaflySyncJobId);
}
