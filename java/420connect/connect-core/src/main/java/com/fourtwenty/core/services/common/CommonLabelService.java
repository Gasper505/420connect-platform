package com.fourtwenty.core.services.common;


import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelsQueryRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.LabelItem;

import java.io.File;
import java.util.List;

/**
 * Created by mdo on 3/14/17.
 */
public interface CommonLabelService {
    enum BarcodeType {
        CODE128,
        EAN13,
        QR
    }

    enum SKUPosition {
        Top,
        Bottom,
        None
    }

    File generateBarcode(final String companyId, final String shopId,String sku, int angle, int height, String barcodeType, SKUPosition skuPosition);

    SearchResult<LabelItem> getLabelItems(final String companyId, final String shopId,LabelsQueryRequest request);

    List<CompanyAsset> getQrCodeLabelItems(final String companyId, final String shopId,LabelsQueryRequest request);
}
