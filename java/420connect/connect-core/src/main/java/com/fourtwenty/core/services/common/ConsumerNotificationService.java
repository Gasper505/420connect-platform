package com.fourtwenty.core.services.common;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.store.ConsumerCart;

/**
 * Created by mdo on 7/1/17.
 */
public interface ConsumerNotificationService {
    void sendNewOrderSubmittedNotification(final ConsumerCart dbCart, final ConsumerUser consumerUser, final Shop shop);

    void sendUpdateOrderdNotification(final ConsumerCart dbCart, final ConsumerUser consumerUser, final Shop shop, final NotificationInfo.NotificationType notificationType);
}
