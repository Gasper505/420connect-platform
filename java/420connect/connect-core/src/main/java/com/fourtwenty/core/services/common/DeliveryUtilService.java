package com.fourtwenty.core.services.common;

import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.models.transaction.Transaction;

import java.util.List;

public interface DeliveryUtilService {
    double calculateMileageByTransaction(String companyId, String shopId, Transaction transaction);
    Double calculateDistance(List<TerminalLocation> locations);
    double distanceRaw(double lat1, double lon1, double lat2, double lon2, char unit);
}
