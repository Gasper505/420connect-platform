package com.fourtwenty.core.services.common;

import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;

import java.util.List;

/**
 * Created by mdo on 7/11/17.
 */
public interface EmployeeNotificationService {
    void sendAssignedOrderNotification(Transaction transaction, String employeeId);

    void sendMessageToAllTerminals(String companyId, String shopId, String message, FcmPayload.Type notificationType, String transactionId, String transNo, String title, FcmPayload.SubType subType);

    void sendMessageToSpecifiedTerminals(String companyId, String shopId, List<String> terminals, String message, FcmPayload.Type notificationType, String transactionId, String transNo, String title, FcmPayload.SubType subType, Conversation conversation);
}
