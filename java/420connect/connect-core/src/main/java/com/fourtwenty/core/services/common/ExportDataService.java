package com.fourtwenty.core.services.common;

import com.fourtwenty.core.rest.dispensary.results.common.ExportResult;

/**
 * Created by mdo on 1/25/17.
 */
public interface ExportDataService {
    ExportResult exportMembers(String companyId, String shopId);

    ExportResult exportPhysicians(String companyId, String shopId);

    ExportResult exportVendors(String companyId, String shopId);

    ExportResult exportProducts(String companyId, String shopId);
}
