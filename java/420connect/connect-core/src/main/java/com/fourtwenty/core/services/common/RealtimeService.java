package com.fourtwenty.core.services.common;

/**
 * Created by mdo on 1/23/16.
 */
public interface RealtimeService {
    enum RealtimeEventType {
        QueueUpdateEvent,
        InventoryUpdateEvent,
        MembersUpdateEvent,
        DoctorsUpdateEvent,
        TerminalsUpdateEvent,
        MemberGroupsCreateEvent,
        MemberGroupsUpdateEvent,
        CashDrawerUpdateEvent,
        PromotionUpdateEvent,
        RewardsUpdateEvent,
        ProductsUpdateEvent,
        ConsumerCartsUpdate,
        ShopInfoUpdateEvent,
        CheckInUpdateEvent,
        StorewideSaleUpdateEvent
    }

    void sendRealTimeEvent(String receiverId, RealtimeEventType eventType, Object data);

    void sendRealTimeEventCompanyId(String companyId, RealtimeEventType eventType, Object data);
}
