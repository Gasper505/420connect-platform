package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.domain.models.company.AuditLog;
import com.fourtwenty.core.domain.repositories.dispensary.AuditLogRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.common.AuditLogService;
import com.google.inject.Inject;

/**
 * Created by mdo on 7/13/16.
 */
public class AuditLogServiceImpl implements AuditLogService {
    @Inject
    AuditLogRepository auditLogRepository;

    @Override
    public void addAuditLog(ConnectAuthToken connectAuthToken, String path, String category, String action) {
        addAuditLog(connectAuthToken, path, category, action, null);
    }

    @Override
    public void addAuditLog(ConnectAuthToken connectAuthToken, String path, String category, String action, String data) {
        if (connectAuthToken != null) {
            AuditLog auditLog = new AuditLog();
            auditLog.setCompanyId(connectAuthToken.getCompanyId());
            auditLog.setShopId(connectAuthToken.getShopId());
            auditLog.setEmployeeId(connectAuthToken.getActiveTopUser().getUserId());
            auditLog.setTerminalId(connectAuthToken.getTerminalId());
            auditLog.setDeviceId(connectAuthToken.getActiveDeviceId());
            auditLog.setTokenId(connectAuthToken.getTokenId());
            auditLog.setTimeCardId(connectAuthToken.getTimeCardId());
            auditLog.setEmployeeSessionId(connectAuthToken.getEmployeeSessoinId());
            auditLog.setData(data);

            auditLog.setAction(action);
            auditLog.setCategory(category);
            auditLog.setApiPath(path);
            auditLogRepository.save(auditLog);
        }
    }
}
