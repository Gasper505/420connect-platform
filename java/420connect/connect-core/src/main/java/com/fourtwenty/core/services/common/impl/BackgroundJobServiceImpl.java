package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.thirdparty.AmazonSQSFifoService;
import com.fourtwenty.core.services.thirdparty.RabbitMQFifoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by mdo on 5/1/16.
 */
public class BackgroundJobServiceImpl implements BackgroundJobService {
    private static final Logger LOG = LoggerFactory.getLogger(BackgroundJobServiceImpl.class);


    @Inject
    AmazonSQSFifoService fifoService;
    @Inject
    ConnectConfiguration connectConfiguration;
    @Inject
    private RabbitMQFifoService rabbitMQFifoService;

    @Override
    public void addTransactionJob(String companyId, String shopId, String queuedTransactionId) {
        LOG.info("Publish Transaction jobs");
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        if (processorConfig != null) {
            if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
                fifoService.publishQueuedTransaction(companyId, shopId, queuedTransactionId);
            } else if (processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.RabbitMQ) {
                rabbitMQFifoService.publishQueuedTransaction(companyId, shopId, queuedTransactionId);
            }
        }
    }

    @Override
    public void addComplianceSyncJob(String companyId, String shopId, String complianceSyncJobId) {
        LOG.info("Publish Compliance jobs");
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        if (processorConfig != null
                && processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
            fifoService.publishComplianceSyncJob(companyId, shopId, complianceSyncJobId);
        }
    }

    @Override
    public void addWeedmapSyncJob(String companyId, String shopId, String weedmapSyncJobId) {
        LOG.info("Publish weedmap sync jobs");
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        if (processorConfig != null
                && processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
            fifoService.publishWeedmapSyncJob(companyId, shopId, weedmapSyncJobId);
        }
    }

    @Override
    public void addLeaflymapSyncJob(String companyId, String shopId, String leaflySyncJobId) {
        LOG.info("Publish leafly sync jobs");
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        if (processorConfig != null
                && processorConfig.getProcessorType() == TransProcessorConfig.TransProcessorType.SQSQueue) {
            fifoService.publishLeaflySyncJob(companyId, shopId, leaflySyncJobId);
        }
    }
}
