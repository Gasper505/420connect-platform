package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelQueryItem;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelsQueryRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.LabelItem;
import com.fourtwenty.core.services.common.CommonLabelService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.QrCodeUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.AbstractBarcodeBean;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 3/14/17.
 */
public class CommonLabelServiceImpl implements CommonLabelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonLabelServiceImpl.class);

    @Inject
    ProductRepository productRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private AmazonS3Service amazonS3Service;


    @Override
    public SearchResult<LabelItem> getLabelItems(final String companyId, final String shopId,LabelsQueryRequest request) {

        if (request.getQueries().size() == 0) {
            return new SearchResult<>();
        }

        List<String> categoryIds = new ArrayList<>();
        List<ObjectId> categoryObjIds = new ArrayList<>();
        List<ObjectId> batchIds = new ArrayList<>();
        List<ObjectId> prepackageIds = new ArrayList<>();
        Set<String> productIds = new LinkedHashSet<>();
        List<ObjectId> vendorIds = new ArrayList<>();
        for (LabelQueryItem queryItem : request.getQueries()) {
            if (StringUtils.isNotEmpty(queryItem.getCategoryId()) && ObjectId.isValid(queryItem.getCategoryId())) {
                categoryIds.add(queryItem.getCategoryId());
                categoryObjIds.add(new ObjectId(queryItem.getCategoryId()));
            }
            if (StringUtils.isNotEmpty(queryItem.getProductId()) && ObjectId.isValid(queryItem.getProductId())) {
                productIds.add(queryItem.getProductId());
            }


            if (StringUtils.isNotEmpty(queryItem.getBatchId())
                    && ObjectId.isValid(queryItem.getBatchId())) {
                batchIds.add(new ObjectId(queryItem.getBatchId()));
            }

            if (StringUtils.isNotEmpty(queryItem.getPrepackageLineItemId())
                    && ObjectId.isValid(queryItem.getPrepackageLineItemId())) {
                prepackageIds.add(new ObjectId(queryItem.getPrepackageLineItemId()));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.getProductsForCategoriesAsMap(companyId,
                categoryIds);

        for (Product product : productHashMap.values()) {
            productIds.add(product.getId());
            if (StringUtils.isNotBlank(product.getVendorId()) && ObjectId.isValid(product.getVendorId())) {
                vendorIds.add(new ObjectId(product.getVendorId()));
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(companyId, vendorIds);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(companyId, categoryObjIds);

        List<String> productIdStrArray = Lists.newArrayList(productIds);

        // Batches
        HashMap<String, ProductBatch> productBatchHashMap =
                batchRepository.getBatchesForProductsMap(companyId, shopId, productIdStrArray);

        HashMap<String, PrepackageProductItem> prepackageProductItems =
                prepackageProductItemRepository.getPrepackagesForProductsAsMap(companyId,
                        shopId, productIdStrArray);

        final HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(companyId, shopId, productIdStrArray);

        // Now create the LabelItem
        SearchResult<LabelItem> result = new SearchResult<>();
        for (LabelQueryItem queryItem : request.getQueries()) {
            String prepackageItemId = StringUtils.isBlank(queryItem.getPrepackageLineItemId()) ? LabelQueryItem.LabelQueryType.None.name() : queryItem.getPrepackageLineItemId();
            String batchId = StringUtils.isBlank(queryItem.getBatchId()) ? LabelQueryItem.LabelQueryType.Latest.name() : queryItem.getBatchId();
            String productId = StringUtils.isBlank(queryItem.getProductId()) ? LabelQueryItem.LabelQueryType.None.name() : queryItem.getProductId();


            List<Product> filteredProducts = new ArrayList<>();
            // Let's apply the filters
            if (LabelQueryItem.LabelQueryType.All.name().equalsIgnoreCase(productId)) {
                // Add all products to the query
                filteredProducts.addAll(productHashMap.values());
            } else if (ObjectId.isValid(productId)) {
                // Only add the single product

                // Get Product
                Product product = productHashMap.get(productId);
                if (product != null) {
                    filteredProducts.add(product);
                }
            }

            // Sort the batches for easy find
            List<ProductBatch> sortedBatches = Lists.newArrayList(productBatchHashMap.values());
            Collections.sort(sortedBatches, new Comparator<ProductBatch>() {
                @Override
                public int compare(ProductBatch o1, ProductBatch o2) {
                    Long purchasedDate = (Long) o1.getPurchasedDate();
                    Long purchasedDate2 = (Long) o2.getPurchasedDate();

                    return purchasedDate2.compareTo(purchasedDate);
                }
            });

            // Now, let's handle the batches
            LinkedHashMap<String, List<ProductBatch>> batchesPerProductMap = new LinkedHashMap<>();
            if (LabelQueryItem.LabelQueryType.Latest.name().equalsIgnoreCase(batchId)) {
                // Just get the latest batches
                // Sort

                // Build batchesPerProduct
                for (Product product : filteredProducts) {
                    for (ProductBatch batch : sortedBatches) {
                        // find the first match and move on. First match should be the latest
                        if (batch.getProductId().equalsIgnoreCase(product.getId())) {
                            batchesPerProductMap.put(product.getId(), Lists.newArrayList(batch));
                            break;
                        }
                    }
                }
            } else if (LabelQueryItem.LabelQueryType.All.name().equalsIgnoreCase(batchId)) {
                // Build batchesPerProduct
                for (Product product : filteredProducts) {
                    List<ProductBatch> batches = new ArrayList<>();
                    for (ProductBatch batch : sortedBatches) {
                        // find the first match and move on. First match should be the latest
                        if (batch.getProductId().equalsIgnoreCase(product.getId())) {
                            batches.add(batch);
                            batchesPerProductMap.put(product.getId(), batches);
                        }
                    }
                }
            } else if (ObjectId.isValid(batchId)) {
                // One
                ProductBatch productBatch = productBatchHashMap.get(queryItem.getBatchId());
                if (productBatch != null) {
                    batchesPerProductMap.put(productBatch.getProductId(), Lists.newArrayList(productBatch));
                }
            }


            // Sort the batches for easy find
            List<PrepackageProductItem> sortedPrepackageItems = Lists.newArrayList(prepackageProductItems.values());
            Collections.sort(sortedPrepackageItems, new Comparator<PrepackageProductItem>() {
                @Override
                public int compare(PrepackageProductItem o1, PrepackageProductItem o2) {
                    Prepackage prepackage1 = prepackageHashMap.get(o1.getPrepackageId());
                    Prepackage prepackage2 = prepackageHashMap.get(o2.getPrepackageId());

                    if (prepackage1 == null && prepackage2 != null) {
                        return -1;
                    } else if (prepackage2 == null && prepackage1 != null) {
                        return 1;
                    } else if (prepackage1 == null && prepackage2 == null) {
                        return 0;
                    }

                    String name1 = StringUtils.isBlank(prepackage1.getName()) ? "" : prepackage1.getName();
                    String name2 = StringUtils.isBlank(prepackage2.getName()) ? "" : prepackage2.getName();
                    // both should now not be null
                    return name1.compareTo(name2);
                }
            });

            // Now, let's handle the prepackages
            LinkedHashMap<String, List<PrepackageProductItem>> prepackageItemsPerProduct = new LinkedHashMap<>();


            if (LabelQueryItem.LabelQueryType.All.name().equalsIgnoreCase(prepackageItemId)) {
                // Build prepackageItemsPerProduct. Can't believe we're doing Big(0^4)
                for (Collection<ProductBatch> batches : batchesPerProductMap.values()) {
                    List<PrepackageProductItem> items = new ArrayList<>();
                    for (ProductBatch batch : batches) {
                        for (PrepackageProductItem item : sortedPrepackageItems) {
                            // find the first match and move on. First match should be the latest
                            if (item.getBatchId().equalsIgnoreCase(batch.getId())) {
                                items.add(item);
                                prepackageItemsPerProduct.put(item.getProductId(), items);
                            }
                        }
                    }

                }
            } else if (ObjectId.isValid(prepackageItemId)) {
                // One
                PrepackageProductItem item = prepackageProductItems.get(prepackageItemId);
                if (item != null) {
                    prepackageItemsPerProduct.put(item.getProductId(), Lists.newArrayList(item));
                }
            }


            // Now create the labels
            for (Product product : filteredProducts) {
                Vendor vendor = vendorHashMap.get(product.getVendorId());
                String vendorName = "";
                String vendorLicenseNumber = StringUtils.EMPTY;
                if (vendor != null) {
                    vendorName = vendor.getName();
                    CompanyLicense companyLicense = vendor.getCompanyLicense(""); // This will give default company license.
                    if (companyLicense != null) {
                        vendorLicenseNumber = companyLicense.getLicenseNumber();
                    } else {
                        vendorLicenseNumber = vendor.getLicenseNumber();
                    }
                }

                ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());

                // Create the label with the batches only if there are no prepackages
                List<PrepackageProductItem> prepackageItems = prepackageItemsPerProduct.get(product.getId());
                if (prepackageItems == null || prepackageItems.size() == 0) {
                    List<ProductBatch> batches = batchesPerProductMap.get(product.getId());
                    if (batches != null) {
                        for (ProductBatch batch : batches) {
                            LabelItem labelItem = createLabelItem(companyId,shopId,product, batch, null, null, vendorName, productCategory, vendorLicenseNumber);
                            result.getValues().add(labelItem);
                        }
                    }
                } else {
                    // only create prepackages
                    for (PrepackageProductItem item : prepackageItems) {
                        ProductBatch productBatch = productBatchHashMap.get(item.getBatchId());
                        LabelItem labelItem = createLabelItem(companyId,shopId,product, productBatch, item, prepackageHashMap.get(item.getPrepackageId()), vendorName, productCategory, vendorLicenseNumber);
                        result.getValues().add(labelItem);
                    }
                }
            }


        }

        result.setTotal((long) result.getValues().size());
        result.setSkip(0);
        result.setLimit(result.getValues().size());

        Collections.sort(result.getValues(), new Comparator<LabelItem>() {
            @Override
            public int compare(LabelItem o1, LabelItem o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return result;
    }

    private LabelItem createLabelItem(final String companyId, final String shopId, final Product product,
                                      final ProductBatch productBatch,
                                      final PrepackageProductItem item,
                                      final Prepackage prepackage, String vendorName, ProductCategory productCategory, String vendorLicenseNumber) {
        LabelItem labelItem = new LabelItem();
        if (product == null) {
            return null;
        }

        labelItem.setProductId(product.getId());
        labelItem.setProductName(product.getName());
        if (StringUtils.isNotBlank(product.getSku())) {
            labelItem.setBatchSKU(product.getSku());
            labelItem.setLabelSKU(product.getSku());
        } else {
            labelItem.setProductSKU("NO SKU");
            labelItem.setLabelSKU("NO SKU");
        }
        labelItem.setBarcodeEntityType(BarcodeItem.BarcodeEntityType.Product);

        String productName = product.getName();
        if (productName.length() > 20) {
            productName = productName.substring(0, 20);
        }

        labelItem.setThc(product.getThc());
        labelItem.setCbd(product.getCbd());
        labelItem.setCbn(product.getCbn());
        labelItem.setFlowerType(product.getFlowerType());
        labelItem.setVendorName(vendorName);
        labelItem.setVendorLicenseNumber(vendorLicenseNumber);

        Shop shop = shopRepository.getById(shopId);
        String priceStr = "(By Grams)";
        if (product.getPriceRanges().size() == 0) {
            priceStr = TextUtil.toCurrency(product.getUnitPrice().doubleValue(), shop.getDefaultCountry());
        }
        labelItem.setName(String.format("%s %s", productName, priceStr));
        if (productBatch != null) {
            labelItem.setBatchDate(productBatch.getPurchasedDate());
            labelItem.setBatchId(productBatch.getId());
            if (StringUtils.isNotBlank(productBatch.getSku())) {
                labelItem.setBatchSKU(productBatch.getSku());
                labelItem.setLabelSKU(productBatch.getSku());
            } else {
                labelItem.setProductSKU("NO SKU");
                labelItem.setLabelSKU("NO SKU");
            }
            labelItem.setBarcodeEntityType(BarcodeItem.BarcodeEntityType.Batch);

            labelItem.setTrackPackageLabel(productBatch.getTrackPackageLabel());
            labelItem.setTrackPackageSystem(productBatch.getTrackTraceSystem().name());

            labelItem.setThc(productBatch.getThc());
            labelItem.setCbd(productBatch.getCbd());
            labelItem.setCbn(productBatch.getCbn());
            labelItem.setTrackHarvestBatch(productBatch.getTrackHarvestBatch());
            labelItem.setTrackHarvestDate(productBatch.getTrackHarvestDate());
            labelItem.setQuantity(new BigDecimal(1));
            labelItem.setLabelInfo(productBatch.getLabelInfo());
        }

        labelItem.setMeasurement("ea");
        if (productCategory != null) {
            labelItem.setCategoryName(productCategory.getName());
            boolean cannabis = productCategory.isCannabis();
            if ((product.getCannabisType() == Product.CannabisType.DEFAULT && productCategory.isCannabis())
                    || (product.getCannabisType() != Product.CannabisType.CBD
                    && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                    && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                cannabis = true;
            }
            labelItem.setCannabis(cannabis);
            labelItem.setUnitType(productCategory.getUnitType().getType());
            if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
                labelItem.setMeasurement("g");
            } else {
                if (Product.WeightPerUnit.CUSTOM_GRAMS == product.getWeightPerUnit()) {
                    labelItem.setMeasurement(product.getCustomWeight() + " " + product.getCustomGramType().getType());
                } else {
                    labelItem.setMeasurement(product.getWeightPerUnit().getWeightPerUnit());
                }
            }
        }

        if (item != null) {
            labelItem.setPrepackageLineItemSKU(item.getSku());
            labelItem.setPrepackageLineItemId(item.getId());
            labelItem.setLabelSKU(item.getSku());
            labelItem.setBarcodeEntityType(BarcodeItem.BarcodeEntityType.Prepackage);
            BigDecimal price = product.getUnitPrice();

            if (prepackage != null) {
                price = prepackage.getPrice();
                labelItem.setPrepackageName(prepackage.getName());
                if (product != null) {
                    for (ProductPriceRange range : product.getPriceRanges()) {
                        if (range.getWeightToleranceId().equals(prepackage.getToleranceId())) {
                            price = range.getPrice();
                            break;
                        }
                    }
                }
                labelItem.setQuantity(prepackage.getUnitValue());
                labelItem.setMeasurement("ea");
            }

            labelItem.setName(String.format("%s %s\n", productName, TextUtil.toCurrency(price.doubleValue(), shop.getDefaultCountry())));
        }
        return labelItem;
    }

    @Override
    public File generateBarcode(final String companyId, final String shopId,String sku, int angle, int height, String barcodeType, SKUPosition skuPosition) {
        if (height <= 0) {
            height = 30;
        }
        //Create the barcode bean
        AbstractBarcodeBean bean = new Code128Bean();
        if (StringUtils.isNotBlank(barcodeType)) {
            if (barcodeType.equalsIgnoreCase(BarcodeType.EAN13.name())) {
                bean = new EAN13Bean();
            } else if (barcodeType.equalsIgnoreCase(BarcodeType.QR.name())) {
                //bean = new QRCodeBean();
                // not supported right now
            }
        }
        //EAN13Bean bean = new EAN13Bean();
        final int dpi = 300;
        bean.setModuleWidth(1.0);
        bean.setBarHeight(height);
        bean.setFontSize(12.0);
        bean.doQuietZone(false);


        //Open output file
        File imgFile = new File(UUID.randomUUID().toString() + ".png");
        OutputStream out = null;
        try {
            try {
                out = new FileOutputStream(imgFile);
                //Set up the canvas provider for monochrome PNG output
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_GRAY, false, angle);

                if (skuPosition == SKUPosition.None) {
                    bean.setMsgPosition(HumanReadablePlacement.HRP_NONE);
                } else if (skuPosition == SKUPosition.Top) {
                    bean.setMsgPosition(HumanReadablePlacement.HRP_TOP);
                }
                // default is sku bottom
                //Generate the barcode
                bean.generateBarcode(canvas, sku);

                //Signal end of generation
                canvas.finish();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgFile;
    }

    @Override
    public List<CompanyAsset> getQrCodeLabelItems(final String companyId, final String shopId,LabelsQueryRequest request) {
        SearchResult<LabelItem> labelItems = getLabelItems(companyId,shopId,request);
        List<CompanyAsset> qrItemsAsset = new ArrayList<>();

        if (labelItems == null || labelItems.getValues() == null) {
            return qrItemsAsset;
        }

        List<String> assetKeyNames = new ArrayList<>();

        for (LabelItem labelItem : labelItems.getValues()) {
            String key = String.format("%s_%s_%s_%s.png", labelItem.getLabelSKU().replaceAll("[&#$%^]", ""), labelItem.getProductId(), labelItem.getBatchId(), labelItem.getPrepackageLineItemId());
            assetKeyNames.add(key);
        }

        HashMap<String, CompanyAsset> assetHashMap = companyAssetRepository.getAssetByKeyAsMap(companyId, assetKeyNames);

        for (LabelItem labelItem : labelItems.getValues()) {

            File file;
            String itemInfo = "";
            CompanyAsset asset;
            String key = String.format("%s_%s_%s_%s", labelItem.getLabelSKU().replaceAll("[&#$%^]", ""), labelItem.getProductId(), labelItem.getBatchId(), labelItem.getPrepackageLineItemId());

            try {

                itemInfo = labelItem.getLabelSKU();//String.format("%s_%s_%s_%s", labelItem.getLabelSKU(), labelItem.getProductId(), labelItem.getBatchId(), labelItem.getPrepackageLineItemId());

                asset = assetHashMap.get(key + ".png");
                if (asset != null) {
                    continue;
                }

                InputStream inputStream = QrCodeUtil.getQrCode(itemInfo);
                file = QrCodeUtil.stream2file(inputStream, ".png");
            } catch (Exception e) {
                LOGGER.info(String.format("Error in creating qr code for %s", String.format("%s_%s_%s_%s", labelItem.getLabelSKU(), labelItem.getProductId(), labelItem.getBatchId(), labelItem.getPrepackageLineItemId())));
                continue;
            }

            if (file != null) {
                UploadFileResult result = amazonS3Service.uploadFilePublic(file, key, ".png");
                if (Objects.nonNull(result) && StringUtils.isNotBlank(result.getUrl())) {
                    asset = new CompanyAsset();
                    asset.prepare(companyId);
                    asset.setName("Label_Item_QR");
                    asset.setKey(result.getKey());
                    asset.setActive(true);
                    asset.setType(Asset.AssetType.Photo);
                    asset.setPublicURL(result.getUrl());
                    asset.setSecured(false);
                    qrItemsAsset.add(asset);
                }

            }
        }
        if (!qrItemsAsset.isEmpty()) {
            companyAssetRepository.save(qrItemsAsset);
        }
        qrItemsAsset.addAll(assetHashMap.values());
        return qrItemsAsset;
    }
}
