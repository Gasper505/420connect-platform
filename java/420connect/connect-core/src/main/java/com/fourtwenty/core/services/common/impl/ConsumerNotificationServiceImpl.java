package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.services.common.ConsumerNotificationService;
import com.fourtwenty.core.services.thirdparty.GoogleAPIService;
import com.fourtwenty.core.services.thirdparty.models.GoogleShortURLResult;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by mdo on 7/1/17.
 */
public class ConsumerNotificationServiceImpl implements ConsumerNotificationService {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerNotificationServiceImpl.class);

    private static final String ONLINE_WIDGET_SOURCE = "OnlineWidget";
    private static final String GREEN_RUSH = "Greenrush";


    public enum ETA {
        Five(5,"ETA is within 5 minutes."),
        TEN(10, "ETA is between 5 to 10 min."),
        TWENTY(20, "ETA is between 10 to 20 min."),
        THIRTY(30, "ETA is between 15 to 30 min."),
        HOUR(60, "ETA is within an 1 hr."),
        HOURHALF(90, "ETA is within 1.5 hrs."),
        TWOHOURS(120, "ETA is within 2 hrs."),
        THREEHOURS(180, "ETA is within 3 hrs."),
        FIVEHOURS(300, "ETA is within 5 hrs.");

        ETA(int minutes, String msg) {
            this.minutes = minutes;
            this.msg = msg;
        }

        public static String getETAMessage(int minutes) {
            ETA[] etas = ETA.values();
            for (int i = 0; i < etas.length; i++) {
                ETA eta = etas[i];
                if (eta.minutes == minutes) {
                    return eta.msg;
                }
            }
            return "";
        }
        public int minutes;
        public String msg;
    }


    @Inject
    ConnectConfiguration configuration;
    @Inject
    GoogleAPIService googleAPIService;
    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    private NotificationInfoRepository notificationInfoRepository;

    @Override
    public void sendNewOrderSubmittedNotification(final ConsumerCart consumerCart, final ConsumerUser consumerUser, final Shop shop) {
        NotificationInfo notificationInfo =  notificationInfoRepository.getByShopAndType(consumerCart.getCompanyId(), consumerCart.getShopId(), NotificationInfo.NotificationType.Consumer_New_Order);

        boolean shouldNotify = true;
        if (notificationInfo != null) {
            LOG.info("Notification info not found or inactive for Incoming Orders");
            shouldNotify = notificationInfo.isActive();
        }
        LOG.info("Notify member: " + shouldNotify);

        if (shouldNotify && !GREEN_RUSH.equalsIgnoreCase(consumerCart.getSource())
                && consumerUser.getNotificationType() != ConsumerUser.ConsumerOrderNotificationType.None) {
            GoogleShortURLResult result = googleAPIService.requestShortUrl(configuration.getTrackingURL() + "?key=" + consumerCart.getPublicKey());

            if (consumerUser.getNotificationType() == ConsumerUser.ConsumerOrderNotificationType.Text) {

                if (result != null) {
                    amazonServiceManager.sendSMSMessage(Lists.newArrayList(consumerUser.getPrimaryPhone()),
                            String.format("Thank You. You have successfully placed an order. You can track your order at %s.", result.getId()), shop);
                } else {
                    amazonServiceManager.sendSMSMessage(Lists.newArrayList(consumerUser.getPrimaryPhone()),
                            String.format("Thank you. You have successfully placed an order."), shop);
                }
            } else if (consumerUser.getNotificationType() == ConsumerUser.ConsumerOrderNotificationType.Email) {
                // Send emails
                String message = "";
                if (result != null) {
                    message = String.format("You have successfully placed an order through %s. You can track your order at <a href=\"%s\">%s</a>.",
                            shop.getName(), result.getId(), result.getId());
                } else {
                    message = String.format("You have successfully placed an order through %s.", shop.getName());
                }


                String fromEMail = "";
                if (shop != null && org.apache.commons.lang3.StringUtils.isNotBlank(shop.getEmailAdress())) {
                    fromEMail = shop.getEmailAdress();
                } else {
                    fromEMail = "support@blaze.me";
                }
                amazonServiceManager.sendEmail(fromEMail, consumerUser.getEmail(), shop.getName() + ": Your Order Status", message, null, shop.getEmailAdress(), "Blaze Support");
            }
        }
    }

    @Override
    public void sendUpdateOrderdNotification(ConsumerCart dbCart, ConsumerUser consumerUser, Shop shop, NotificationInfo.NotificationType notificationType) {
        LOG.info("");
        NotificationInfo orderNotification =  notificationInfoRepository.getByShopAndType(dbCart.getCompanyId(), dbCart.getShopId(), notificationType);

        if (orderNotification != null && !orderNotification.isActive()) {
            LOG.info(String.format("Notification info not found or inactive for %s", notificationType));
            return;
        }

        LOG.info("Notify member: " + notificationType + " tracking: " + dbCart.getTrackingStatus().name());

        if (!GREEN_RUSH.equalsIgnoreCase(dbCart.getSource())
                && consumerUser.getNotificationType() != ConsumerUser.ConsumerOrderNotificationType.None) {
            String testUrl = configuration.getTrackingURL() + "?key=" + dbCart.getPublicKey();
            if (dbCart.getTrackingStatus() == ConsumerCart.ConsumerTrackingStatus.Delivered) {
                testUrl = testUrl + "&page=summary";
            }
            GoogleShortURLResult result = googleAPIService.requestShortUrl(testUrl);

            String statusMsg = dbCart.getTrackingStatus().msg.toLowerCase();

            if (consumerUser.getNotificationType() == ConsumerUser.ConsumerOrderNotificationType.Text) {

                String message = "";
                if (shop.getOnlineStoreInfo() != null && shop.getOnlineStoreInfo().isUseCustomETA()) {
                    if (result != null) {
                        message = String.format("Your order has been updated. You can track your order at %s.", result.getId());
                    } else {
                        message = String.format("Your order is %s. %s",statusMsg,shop.getOnlineStoreInfo().getCustomMessageETA());
                    }
                } else {
                    if (result != null) {
                        message = String.format("Your order has been updated to: %s. You can track your order at %s.", dbCart.getTrackingStatus().name(), result.getId());
                    } else {
                        if (dbCart.getTrackingStatus() != ConsumerCart.ConsumerTrackingStatus.Declined && dbCart.getTrackingStatus() != ConsumerCart.ConsumerTrackingStatus.Delivered) {
                            String etaMsg = ETA.getETAMessage(dbCart.getEta());
                            message = String.format("Your order is %s. %s", statusMsg,etaMsg);
                        } else {
                            message = String.format("Your order is %s.", statusMsg);
                        }
                    }
                }

                if (StringUtils.isNotBlank(message)) {
                    //message = message;///String.format("%s: %s",shop.getName(),message);
                    LOG.info("Texting notifications..." + message);
                    amazonServiceManager.sendSMSMessage(Lists.newArrayList(consumerUser.getPrimaryPhone()), message, shop);
                }
            } else if (consumerUser.getNotificationType() == ConsumerUser.ConsumerOrderNotificationType.Email) {
                // Send emails
                String message = "";
                if (result != null) {
                    message = String.format("Your order from %s has been updated to: %s. You can track your order at <a href=\"%s\">%s</a>.",
                            shop.getName(), dbCart.getTrackingStatus().name(), result.getId(), result.getId());
                } else {
                    message = String.format("Your order from %s has been updated to: %s.", shop.getName(), dbCart.getTrackingStatus().name());
                }

                String fromEMail = "";
                if (shop != null && org.apache.commons.lang3.StringUtils.isNotBlank(shop.getEmailAdress())) {
                    fromEMail = shop.getEmailAdress();
                } else {
                    fromEMail = "support@blaze.me";
                }
                amazonServiceManager.sendEmail(fromEMail, consumerUser.getEmail(), shop.getName() + ": Your Order Status", message, null, shop.getEmailAdress(), shop.getName());
            }
        }
    }


}
