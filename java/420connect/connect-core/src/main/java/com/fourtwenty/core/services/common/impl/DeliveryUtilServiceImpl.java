package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalLocationRepository;
import com.fourtwenty.core.services.common.DeliveryUtilService;

import javax.inject.Inject;
import java.util.List;

public class DeliveryUtilServiceImpl implements DeliveryUtilService {

    @Inject
    private TerminalLocationRepository terminalLocationRepository;


    @Override
    public double calculateMileageByTransaction(String companyId, String shopId, Transaction transaction) {
        double distance = 0;
        List<TerminalLocation> terminalLocationList = terminalLocationRepository.getLocationByTransaction(companyId, shopId, "{created:1}",
                transaction.getAssignedEmployeeId(), transaction.getTerminalId(), 0, Integer.MAX_VALUE,
                transaction.getStartRouteDate(), transaction.getEndRouteDate());
        if (terminalLocationList == null) {
            return distance;
        }

        distance = calculateDistance(terminalLocationList);

        return distance;
    }



    public Double calculateDistance(List<TerminalLocation> locations) {
        double distance = 0;
        if (locations == null) {
            return distance;
        }
        for (int i = 0; i < locations.size(); i++) {
            if (i != locations.size() - 1) {
                distance += distanceRaw(locations.get(i).getLoc().get(1), locations.get(i).getLoc().get(0),
                        locations.get(i + 1).getLoc().get(1), locations.get(i + 1).getLoc().get(0), 'M');
            }
        }
        return distance;
    }

    /**
     * Return the distance (straight line) between two points (lat,long)
     * <p>
     * M = Miles
     * K = Kilometers
     * N = Nautical Miles
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @param unit one of M,K,N
     * @return
     */
    public double distanceRaw(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }

        if (Double.isNaN(dist)) {
            dist = 0;
        }

        return (dist);
    }


    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
