package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.managed.PushNotificationManager;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.common.EmployeeNotificationService;
import com.google.common.collect.Lists;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 7/11/17.
 */
public class EmployeeNotificationServiceImpl implements EmployeeNotificationService {
    @Inject
    ConnectConfiguration configuration;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    PushNotificationManager pushNotificationManager;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    MemberRepository memberRepository;

    @Override
    public void sendAssignedOrderNotification(Transaction transaction, String employeeId) {
        Employee employee = employeeRepository.get(transaction.getCompanyId(), employeeId);

        if (employee != null) {
            Terminal terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
            if (terminal != null) {
                Member member = memberRepository.get(transaction.getCompanyId(), transaction.getMemberId());
                if (member != null) {
                    String message = String.format("You have been assigned an order from %s %s. Order Number : #%s", member.getFirstName(), member.getLastName(), transaction.getTransNo());
                    String title =  String.format("New Order");
                    pushNotificationManager.sendPushNotification(
                            message, Lists.newArrayList(terminal), 0, FcmPayload.Type.TASK, transaction.getId(), transaction.getTransNo(), title, FcmPayload.SubType.NEW_ORDER);
                }
            }
        }
    }

    @Override
    public void sendMessageToAllTerminals(final String companyId, final String shopId, final String message, FcmPayload.Type notificationType, final String transactionId, String  transNo, String title, FcmPayload.SubType subType) {
        Iterable<Terminal> terminals = terminalRepository.listAllByShopActive(companyId, shopId);
        if (StringUtils.isNotBlank(message)) {
                pushNotificationManager.sendPushNotification(
                        String.format(message),
                        Lists.newArrayList(terminals), 0, notificationType, transactionId, transNo, title, subType);
        }
    }

    @Override
    public void sendMessageToSpecifiedTerminals(String companyId, String shopId, List<String> terminals, String message, FcmPayload.Type notificationType, String transactionId, String transNo, String title, FcmPayload.SubType subType, Conversation conversation) {
        List<ObjectId> terminalObjId = new ArrayList<>();
        terminals.forEach(id -> {
            if (StringUtils.isNotBlank(id) && ObjectId.isValid(id)) {
                terminalObjId.add(new ObjectId(id));
            }
        });
        Iterable<Terminal> terminalList = terminalRepository.list(companyId, terminalObjId);
        if (StringUtils.isNotBlank(message)) {
            if (notificationType.equals(FcmPayload.Type.CHAT)) {
                for (Terminal terminal : terminalList) {
                    for (Terminal.DeviceDetail deviceDetail : terminal.getDeviceDetails()) {
                        if (deviceDetail.getAppType() == ConnectAuthToken.ConnectAppType.DeliveryApp) {
                            continue;
                        }
                        pushNotificationManager.sendPushNotification(String.format(message), Lists.newArrayList(terminalList), 0, notificationType, transactionId, transNo, title, subType, conversation);
                    }
                }
            } else {
                pushNotificationManager.sendPushNotification(String.format(message), Lists.newArrayList(terminalList), 0, notificationType, transactionId, transNo, title, subType);
            }
        }
    }

}
