package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.rest.dispensary.results.common.ExportResult;
import com.fourtwenty.core.services.common.ExportDataService;

/**
 * Created by mdo on 1/25/17.
 */
public class ExportDataServiceImpl implements ExportDataService {

    public ExportDataServiceImpl() {
    }

    @Override
    public ExportResult exportMembers(String companyId, String shopId) {
        return null;
    }

    @Override
    public ExportResult exportPhysicians(String companyId, String shopId) {
        return null;
    }

    @Override
    public ExportResult exportVendors(String companyId, String shopId) {
        return null;
    }

    @Override
    public ExportResult exportProducts(String companyId, String shopId) {
        return null;
    }
}
