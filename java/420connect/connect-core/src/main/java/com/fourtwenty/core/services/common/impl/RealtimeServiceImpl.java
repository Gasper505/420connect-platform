package com.fourtwenty.core.services.common.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.managed.PushNotificationManager;
import com.fourtwenty.core.services.common.RealtimeService;
import com.google.inject.Inject;

/**
 * Created by mdo on 1/23/16.
 */
public class RealtimeServiceImpl implements RealtimeService {
    @Inject
    PushNotificationManager notificationManager;
    @Inject
    ShopRepository shopRepository;

    @Override
    public void sendRealTimeEvent(String receiverId, RealtimeEventType eventType, Object data) {
        notificationManager.sendPusherMessage(receiverId, eventType.name(), data);
    }

    @Override
    public void sendRealTimeEventCompanyId(String companyId, RealtimeEventType eventType, Object data) {
        Iterable<Shop> shops = shopRepository.list(companyId);
        for (Shop shop : shops) {
            sendRealTimeEvent(shop.getId(), eventType, data);
        }
    }
}
