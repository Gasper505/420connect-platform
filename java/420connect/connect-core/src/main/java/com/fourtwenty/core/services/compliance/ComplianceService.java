package com.fourtwenty.core.services.compliance;

import com.blaze.clients.metrcs.models.strains.MetrcStrainRequest;
import com.fourtwenty.core.domain.models.compliance.*;
import com.fourtwenty.core.rest.common.LabelValuePair;
import com.fourtwenty.core.rest.compliance.IntakeMetrcPackageRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.io.InputStream;
import java.util.List;

public interface ComplianceService {

    void sync();
    void resetSync();

    boolean assignMetrcBatches(InputStream tagFileInput);
    boolean bulkCreateItemsStrains(InputStream tagFileInput);
    CompliancePackage intakePackage(String compliancePackageId, IntakeMetrcPackageRequest request);


    SearchResult<ComplianceCategory> getComplianceCategories();
    SearchResult<CompliancePackage> getCompliancePackages(CompliancePackage.PackageStatus status, String term,int skip, int limit);
    ListResult<LabelValuePair> getCompliancePackagesTags(CompliancePackage.PackageStatus status, String term, int skip, int limit);
    SearchResult<ComplianceItem> getComplianceItems(String term, int skip, int limit);
    SearchResult<ComplianceSaleReceipt> getSaleReceipts(String term, String startDate, String endDate, int skip, int limit);
    ComplianceSaleReceipt getMetrcSaleById(String saleId);


    SearchResult<ComplianceStrain> getComplianceStrains(String term, int skip, int limit);
    SearchResult<ComplianceSyncJob> getComplianceSyncJobs(int skip, int limit);

    boolean createNewMetrcBatches(List<ComplianceBatchPackagePair> metrcBatchesResquests);
    boolean bulkCreateItems(List<ComplianceBatchPackagePair> metrcItemsRequests);
    boolean bulkCreateMetrcStrain(List<MetrcStrainRequest> metrcStrainRequests);

}
