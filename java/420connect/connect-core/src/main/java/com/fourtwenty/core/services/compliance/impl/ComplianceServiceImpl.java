package com.fourtwenty.core.services.compliance.impl;


import com.blaze.clients.metrcs.MetrcMeasurement;
import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.blaze.clients.metrcs.models.strains.MetrcStrainRequest;
import com.blaze.clients.metrcs.models.strains.MetricsStrain;
import com.fourtwenty.core.domain.models.compliance.*;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.repositories.compliance.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.sync.ComplianceSyncJobRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.compliance.IntakeCompliancePackageEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.rest.common.LabelValuePair;
import com.fourtwenty.core.rest.compliance.IntakeMetrcPackageRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.compliance.ComplianceService;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.MeasurementConversionUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ComplianceServiceImpl extends AbstractAuthServiceImpl implements ComplianceService {
    private static final Log LOG = LogFactory.getLog(ComplianceServiceImpl.class);
    private static final String COMPLIANCE = "Compliance";

    @Inject
    BlazeEventBus eventBus;
    @Inject
    CompliancePackageRepository compliancePackageRepository;
    @Inject
    ComplianceItemRepository complianceItemRepository;

    @Inject
    ComplianceSaleReceiptRepository saleReceiptRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    ComplianceStrainRepository complianceStrainRepository;
    @Inject
    ComplianceCategoryRepository complianceCategoryRepository;
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    MetrcService metrcService;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    BrandRepository brandRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    ComplianceSyncJobRepository complianceSyncJobRepository;
    @Inject
    ProductWeightToleranceRepository toleranceRepository;


    @Inject
    public ComplianceServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @Override
    public void sync() {
        ComplianceSyncJob complianceSyncJob = new ComplianceSyncJob();
        complianceSyncJob.prepare(token.getCompanyId());
        complianceSyncJob.setShopId(token.getShopId());
        complianceSyncJob.setComplianceType(ProductBatch.TrackTraceSystem.METRC);
        complianceSyncJob.setJobType(ComplianceSyncJob.ComplianceSyncJobType.SYNC);
        complianceSyncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED);
        complianceSyncJob.setRequestTime(DateUtil.nowUTC().getMillis());
        complianceSyncJob.setEmployeeId(token.getActiveTopUser().getUserId());
        complianceSyncJob = complianceSyncJobRepository.save(complianceSyncJob);

        backgroundJobService.addComplianceSyncJob(token.getCompanyId(), token.getShopId(), complianceSyncJob.getId());
    }

    @Override
    public void resetSync() {

    }

    @Override
    public CompliancePackage intakePackage(String compliancePackageId, IntakeMetrcPackageRequest request) {
        if (StringUtils.isBlank(request.getProductId())) {
            throw new BlazeInvalidArgException("Product", "Product ID is not valid.");
        }

        CompliancePackage compliancePackage = compliancePackageRepository.get(token.getCompanyId(),compliancePackageId);
        if (compliancePackage == null) {
            throw new BlazeInvalidArgException("CompliancePackage", "Compliance Package does not exist with this identifier.");
        }

        Product product = productRepository.get(token.getCompanyId(),request.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("Product", "Product is invalid.");
        }



        if (StringUtils.isNotBlank(request.getBatchId())) {
            ProductBatch productBatch = productBatchRepository.get(token.getCompanyId(),request.getBatchId());
            if (productBatch == null) {
                throw new BlazeInvalidArgException("ProductBatch", "ProductBatch is invalid.");
            }

            // just assign the compliance batch
            productBatchRepository.updateMetrcTag(request.getBatchId(),compliancePackage.getData().getLabel(),compliancePackage.getBlazeMeasurement());
            compliancePackageRepository.setComplianceProductBatch(compliancePackage.getCompanyId(),compliancePackage.getId(),request.getProductId(), request.getBatchId());

            compliancePackage.setProductId(request.getProductId());
            compliancePackage.setProductBatchId(request.getBatchId());

        } else {
            // create new metrc product batch
            IntakeCompliancePackageEvent event = new IntakeCompliancePackageEvent();
            event.setCompanyId(token.getCompanyId());
            event.setShopId(token.getShopId());
            event.setCompliancePackage(compliancePackage);
            event.setProductId(request.getProductId());
            event.setBatchId(request.getBatchId());
            event.setCreateNew(true);
            event.setCostPerUnit(request.getCostPerUnit());
            event.setExpirationDate(request.getExpirationDate());
            event.setSellBy(request.getSellBy());

            MetricsPackages metricsPackage = compliancePackage.getData();
            if (metricsPackage.getUnitOfMeasureName().equalsIgnoreCase(MetrcMeasurement.Ounces.measurementName)) {
                ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(token.getCompanyId(),
                        ProductWeightTolerance.WeightKey.OUNCE);
                BigDecimal ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue;
                if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
                    ozUnitValue = ozTolerance.getUnitValue();
                }

                BigDecimal blazeQuantity = ozUnitValue.multiply(metricsPackage.getQuantity());
                if (blazeQuantity.doubleValue() < 0) {
                    blazeQuantity = new BigDecimal(0);
                }
                compliancePackage.setBlazeQuantity(blazeQuantity);
                compliancePackage.setBlazeMeasurement(ProductWeightTolerance.WeightKey.GRAM);
            } else {
                compliancePackage.setBlazeQuantity(metricsPackage.getQuantity());
                compliancePackage.setBlazeMeasurement(metricsPackage.getUnitOfMeasureName().equalsIgnoreCase(MetrcMeasurement.Grams.measurementName) ? ProductWeightTolerance.WeightKey.GRAM : ProductWeightTolerance.WeightKey.UNIT);
            }

            if (StringUtils.isNotBlank(request.getProductId())) {
                if (product != null) {
                    ProductCategory category = categoryRepository.get(token.getCompanyId(),product.getCategoryId());
                    BigDecimal newQty = MeasurementConversionUtil.convertToBatchQuantity(toleranceRepository,
                            token.getCompanyId(),
                            compliancePackage.getBlazeMeasurement(),
                            compliancePackage.getData().getQuantity(),
                            compliancePackage.getBlazeQuantity(),
                            category.getUnitType(),
                            product);
                    compliancePackage.setBlazeQuantity(newQty);
                    compliancePackage.setBlazeMeasurement(product.getWeightPerUnit().getCorrespondingWeightKey());
                }
            }

            eventBus.post(event);

        }
        return compliancePackage;
    }

    @Override
    public boolean assignMetrcBatches(InputStream tagFileInput) {
        if (tagFileInput == null) {
            throw new BlazeInvalidArgException("TagFile", "Tag file not found.");
        }
        CSVParser csvParser = (new Parser()).createCSVParser(tagFileInput);
        if (csvParser != null) {


            Iterator<CSVRecord> csvIter = csvParser.iterator();

            List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();

            while (csvIter.hasNext()) {
                CSVRecord csvRecord = csvIter.next();
                String tag = csvRecord.get("Metrc Package Label");
                String batchId = csvRecord.get("Internal ID");
                String productId = csvRecord.get("Product ID");
                String strainName = csvRecord.get("Strain Name");
                if (StringUtils.isNotBlank(tag) && StringUtils.isNotBlank(batchId)) {
                    ComplianceBatchPackagePair batchPair = new ComplianceBatchPackagePair();
                    batchPair.setBatchId(batchId);
                    batchPair.setTag(tag);
                    batchPair.setProductId(productId);
                    batchPair.setStrainName(strainName.trim());
                    packageTags.add(batchPair);
                }
            }
            if (packageTags.isEmpty()) {
                throw new BlazeInvalidArgException("Tag", "CSV did not contain any tags.");
            }

            ComplianceSyncJob complianceSyncJob = new ComplianceSyncJob();
            complianceSyncJob.prepare(token.getCompanyId());
            complianceSyncJob.setShopId(token.getShopId());
            complianceSyncJob.setComplianceType(ProductBatch.TrackTraceSystem.METRC);
            complianceSyncJob.setJobType(ComplianceSyncJob.ComplianceSyncJobType.CREATE_EXT_PACKAGES);
            complianceSyncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED);
            complianceSyncJob.setRequestTime(DateUtil.nowUTC().getMillis());
            complianceSyncJob.setPackages(packageTags);
            complianceSyncJob.setEmployeeId(token.getActiveTopUser().getUserId());
            complianceSyncJob = complianceSyncJobRepository.save(complianceSyncJob);

            backgroundJobService.addComplianceSyncJob(token.getCompanyId(), token.getShopId(), complianceSyncJob.getId());
            return true;
        }
        return false;
    }

    @Override
    public boolean bulkCreateItemsStrains(InputStream tagFileInput) {
        if (tagFileInput == null) {
            throw new BlazeInvalidArgException("TagFile", "Tag file not found.");
        }
        CSVParser csvParser = (new Parser()).createCSVParser(tagFileInput);
        if (csvParser != null) {


            Iterator<CSVRecord> csvIter = csvParser.iterator();

            List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();

            while (csvIter.hasNext()) {
                CSVRecord csvRecord = csvIter.next();
                String productId = csvRecord.get("Product ID");
                String strainName = csvRecord.get("Strain Name");
                if (StringUtils.isNotBlank(productId) && StringUtils.isNotBlank(strainName)) {
                    ComplianceBatchPackagePair batchPair = new ComplianceBatchPackagePair();
                    batchPair.setProductId(productId);
                    batchPair.setStrainName(strainName.trim());
                    packageTags.add(batchPair);
                }
            }
            if (packageTags.isEmpty()) {
                throw new BlazeInvalidArgException("Tag", "CSV did not contain any tags.");
            }

            ComplianceSyncJob complianceSyncJob = new ComplianceSyncJob();
            complianceSyncJob.prepare(token.getCompanyId());
            complianceSyncJob.setShopId(token.getShopId());
            complianceSyncJob.setComplianceType(ProductBatch.TrackTraceSystem.METRC);
            complianceSyncJob.setJobType(ComplianceSyncJob.ComplianceSyncJobType.SYNC_UP);
            complianceSyncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED);
            complianceSyncJob.setRequestTime(DateUtil.nowUTC().getMillis());
            complianceSyncJob.setPackages(packageTags);
            complianceSyncJob.setEmployeeId(token.getActiveTopUser().getUserId());
            complianceSyncJob = complianceSyncJobRepository.save(complianceSyncJob);

            backgroundJobService.addComplianceSyncJob(token.getCompanyId(), token.getShopId(), complianceSyncJob.getId());
        }
        return false;
    }

    @Override
    public SearchResult<ComplianceCategory> getComplianceCategories() {
        return complianceCategoryRepository.findItems(token.getCompanyId(),token.getShopId(),"{key:1}",0,500);
    }

    @Override
    public SearchResult<CompliancePackage> getCompliancePackages(CompliancePackage.PackageStatus status, String term,int skip, int limit) {
        skip = skip < 0 ? 0 : skip;
        limit = (limit == 0 || limit > 100) ? 100 : limit;

        if (StringUtils.isBlank(term)) {
            return compliancePackageRepository.getCompliancePackages(token.getCompanyId(), token.getShopId(), status, skip, limit);
        } else {
            return compliancePackageRepository.getCompliancePackages(token.getCompanyId(), token.getShopId(), status, term,skip, limit);
        }
    }

    @Override
    public ListResult<LabelValuePair> getCompliancePackagesTags(CompliancePackage.PackageStatus status, String term, int skip, int limit) {
        skip = skip < 0 ? 0 : skip;
        limit = (limit == 0 || limit > 100) ? 100 : limit;

        SearchResult<CompliancePackage> packageSearchResult = new SearchResult<CompliancePackage>();
        if (StringUtils.isBlank(term)) {
            packageSearchResult = compliancePackageRepository.getCompliancePackages(token.getCompanyId(), token.getShopId(), status, skip, limit);
        } else {
            packageSearchResult = compliancePackageRepository.getCompliancePackages(token.getCompanyId(), token.getShopId(), status, term,skip, limit);
        }

        ListResult<LabelValuePair> searchResult = new ListResult<LabelValuePair>();

        for (CompliancePackage compliancePackage : packageSearchResult.getValues()) {
            LabelValuePair pair = new LabelValuePair();
            pair.setLabel(compliancePackage.getKey());
            pair.setValue(compliancePackage.getKey());
            searchResult.getValues().add(pair);
        }
        return searchResult;
    }

    @Override
    public SearchResult<ComplianceItem> getComplianceItems(String term, int skip, int limit) {
        skip = skip < 0 ? 0 : skip;
        limit = (limit == 0 || limit > 100) ? 100 : limit;

        if (StringUtils.isBlank(term)) {
            return complianceItemRepository.getComplianceItems(token.getCompanyId(), token.getShopId(),skip, limit);
        } else {
            return complianceItemRepository.getComplianceItems(token.getCompanyId(), token.getShopId(), term,skip, limit);
        }
    }

    @Override
    public SearchResult<ComplianceSaleReceipt> getSaleReceipts(String term, String startDate, String endDate, int skip, int limit) {
        skip = skip < 0 ? 0 : skip;
        limit = (limit == 0 || limit > 100) ? 100 : limit;

        // Parse Incoming Dates
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        long startDateMillis = 0l;
        DateTime jodaEndDate = DateTime.now();

        if (startDate != null) {
            startDateMillis = formatter.parseDateTime(startDate).getMillis();
        }

        if (endDate != null) {
            jodaEndDate = formatter.parseDateTime(endDate);
            jodaEndDate = jodaEndDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
        }

        int timezoneOffset = token.getTimezoneOffsetInMinutes();

        DateTime estartDate = new DateTime(startDateMillis);
        long timeZoneStartDateMillis = estartDate.withTimeAtStartOfDay().plusMinutes(timezoneOffset).getMillis();
        long timeZoneEndDateMillis = jodaEndDate.plusMinutes(timezoneOffset).getMillis();

        if (StringUtils.isBlank(term)) {
            return saleReceiptRepository.getSaleReceipts(token.getCompanyId(), token.getShopId(),timeZoneStartDateMillis, timeZoneEndDateMillis,skip, limit);
        } else {
            return saleReceiptRepository.getSaleReceipts(token.getCompanyId(), token.getShopId(), term,timeZoneStartDateMillis, timeZoneEndDateMillis,skip, limit);
        }
    }

    @Override
    public ComplianceSaleReceipt getMetrcSaleById(String saleId) {
        if (StringUtils.isBlank(saleId)) {
            throw new BlazeInvalidArgException("SaleId", "SaleId does not exist.");
        }
        return saleReceiptRepository.getMetrcSaleByKey(token.getCompanyId(),token.getShopId(),saleId);
    }


    @Override
    public SearchResult<ComplianceStrain> getComplianceStrains(String term, int skip, int limit) {
        skip = skip < 0 ? 0 : skip;
        limit = (limit == 0 || limit > 100) ? 100 : limit;

        if (StringUtils.isBlank(term)) {
            return complianceStrainRepository.getComplianceStrains(token.getCompanyId(), token.getShopId(),skip, limit);
        } else {
            return complianceStrainRepository.getComplianceStrains(token.getCompanyId(), token.getShopId(), term,skip, limit);
        }
    }

    @Override
    public SearchResult<ComplianceSyncJob> getComplianceSyncJobs(int skip, int limit) {
        skip = skip < 0 ? 0 : skip;
        limit = (limit == 0 || limit > 100) ? 100 : limit;

        return complianceSyncJobRepository.findItems(token.getCompanyId(),token.getShopId(),"{modified:-1}",skip,limit);
    }

    /**
     * This method is used to create metrc packages
     *
     * @param reqMetrcPackages
     * @return
     */
    @Override
    public boolean createNewMetrcBatches(List<ComplianceBatchPackagePair> reqMetrcPackages) {

        Iterable<ProductBatch> productBatches = productBatchRepository.listAllByShop(token.getCompanyId(),token.getShopId());
        HashMap<String,ProductBatch> batchHashMap = new HashMap<>();
        for (ProductBatch productBatch : productBatches) {
            batchHashMap.put(productBatch.getId(),productBatch);
        }

        if (reqMetrcPackages != null) {
            List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();

            for (ComplianceBatchPackagePair compliancePackage : reqMetrcPackages) {
                if (StringUtils.isBlank(compliancePackage.getProductId())) {
                    throw new BlazeInvalidArgException("Compliance Product", "Product does not exist.");
                }
                if (StringUtils.isBlank(compliancePackage.getBatchId())) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Product does not exist for strain %s", compliancePackage.getStrainName()));
                }

               ProductBatch batch =  batchHashMap.get(compliancePackage.getBatchId());

                if (batch == null) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Product does not exist for strain %s", compliancePackage.getStrainName()));
                }

                if (StringUtils.isBlank(compliancePackage.getTag())) {
                    throw new BlazeInvalidArgException("Tag", "Tag Id does not exist.");
                }

                if (StringUtils.isNotBlank(compliancePackage.getTag()) && StringUtils.isNotBlank(compliancePackage.getBatchId()) && StringUtils.isNotBlank(compliancePackage.getProductId())) {
                    ComplianceBatchPackagePair batchPair = new ComplianceBatchPackagePair();
                    batchPair.setBatchId(compliancePackage.getBatchId());
                    batchPair.setProductId(compliancePackage.getProductId());
                    batchPair.setTag(compliancePackage.getTag());
                    batchPair.setStrainName(compliancePackage.getStrainName().trim());
                    packageTags.add(batchPair);
                }

                if (packageTags.isEmpty()) {
                    throw new BlazeInvalidArgException("Compliance Tag", "Compliance batchs did not contain any tags.");
                }
            }
            ComplianceSyncJob complianceSyncJob = new ComplianceSyncJob();
            complianceSyncJob.prepare(token.getCompanyId());
            complianceSyncJob.setShopId(token.getShopId());
            complianceSyncJob.setComplianceType(ProductBatch.TrackTraceSystem.METRC);
            complianceSyncJob.setJobType(ComplianceSyncJob.ComplianceSyncJobType.CREATE_EXT_PACKAGES);
            complianceSyncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED);
            complianceSyncJob.setRequestTime(DateUtil.nowUTC().getMillis());
            complianceSyncJob.setPackages(packageTags);
            complianceSyncJob.setEmployeeId(token.getActiveTopUser().getUserId());
            complianceSyncJob = complianceSyncJobRepository.save(complianceSyncJob);

            backgroundJobService.addComplianceSyncJob(token.getCompanyId(), token.getShopId(), complianceSyncJob.getId());
            return true;
        }
        return false;
    }


    /**
     * This method is used to create metrc items
     * @param reqMetrcItems
     * @return
     */

    @Override
    public boolean bulkCreateItems(List<ComplianceBatchPackagePair> reqMetrcItems) {
        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), token.getShopId());
        if (reqMetrcItems != null) {
            List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();

            for (ComplianceBatchPackagePair complianceItem : reqMetrcItems) {

                if (StringUtils.isBlank(complianceItem.getProductId())) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Product does not exist for strain %s", complianceItem.getStrainName()));
                }
                Product product = productHashMap.get(complianceItem.getProductId());
                if (product == null) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Product does not exist for strain %s", complianceItem.getStrainName()));
                }
                if (StringUtils.isBlank(complianceItem.getStrainName())) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Strain can't be blank for product %s", complianceItem.getProductId()));
                }

                if(StringUtils.isNotBlank(product.getComplianceId())){
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Product %s is already linked with compliance Id", product.getName()));
                }

                if (StringUtils.isNotBlank(complianceItem.getProductId()) && StringUtils.isNotBlank(complianceItem.getStrainName())) {
                    ComplianceBatchPackagePair batchPair = new ComplianceBatchPackagePair();
                    batchPair.setProductId(complianceItem.getProductId());
                    batchPair.setStrainName(complianceItem.getStrainName().trim());
                    packageTags.add(batchPair);
                }

                if (packageTags.isEmpty()) {
                    throw new BlazeInvalidArgException("Items", "Compliance Items did not contain any tags.");
                }
            }
            //compliance items job

            ComplianceSyncJob complianceSyncJob = new ComplianceSyncJob();

            complianceSyncJob.prepare(token.getCompanyId());
            complianceSyncJob.setShopId(token.getShopId());
            complianceSyncJob.setComplianceType(ProductBatch.TrackTraceSystem.METRC);
            complianceSyncJob.setJobType(ComplianceSyncJob.ComplianceSyncJobType.CREATE_ITEMS);
            complianceSyncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED);
            complianceSyncJob.setRequestTime(DateUtil.nowUTC().getMillis());
            complianceSyncJob.setPackages(packageTags);
            complianceSyncJob.setEmployeeId(token.getActiveTopUser().getUserId());


            complianceSyncJob = complianceSyncJobRepository.save(complianceSyncJob);
            backgroundJobService.addComplianceSyncJob(token.getCompanyId(), token.getShopId(), complianceSyncJob.getId());
            return true;
        } else {
            throw new BlazeInvalidArgException("Items", "Compliance Items not found");
        }
    }

    /**
     * This method is used to create  metrc strains.
     * @param reqMetrcStrain
     * @return
     */
    @Override
    public boolean bulkCreateMetrcStrain(List<MetrcStrainRequest> reqMetrcStrain) {
        List<MetrcStrainRequest> strains = new ArrayList<>();
        HashMap<String, ComplianceStrain> complianceStrainMap = complianceStrainRepository.listAsMap(token.getCompanyId(), token.getShopId());
        List<String> strainName = new ArrayList<>();

        for (ComplianceStrain cs : complianceStrainMap.values()) {
            if (cs.getData() != null) {
                MetricsStrain metricsS = cs.getData();
                if (StringUtils.isNotBlank(metricsS.getName())) {
                    strainName.add(metricsS.getName());
                }
            }
        }

        if (reqMetrcStrain != null) {
            for (MetrcStrainRequest strain : reqMetrcStrain) {
                if (StringUtils.isBlank(strain.getName())) {
                    throw new BlazeInvalidArgException(COMPLIANCE, "Strain name can't be blank");
                }
                String cleanName = StringUtils.normalizeSpace(strain.getName().trim());
                boolean isExsits = strainName.contains(cleanName);
                if (isExsits) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Strain  %s already exists in the current Facility.", strain.getName()));
                }
                BigDecimal indicaPercentage = BigDecimal.valueOf(strain.getIndicaPercentage());
                BigDecimal sativaPercentage = BigDecimal.valueOf(strain.getSativaPercentage());
                if (indicaPercentage.add(sativaPercentage).doubleValue() < 100 || indicaPercentage.add(sativaPercentage).doubleValue() > 100) {
                    throw new BlazeInvalidArgException(COMPLIANCE, String.format("Strain %s Indica and Sativa Percentages combined must equal 100.", strain.getName()));
                }
                strains.add(strain);
            }

            //compliance items job
            ComplianceSyncJob complianceSyncJob = new ComplianceSyncJob();

            complianceSyncJob.prepare(token.getCompanyId());
            complianceSyncJob.setShopId(token.getShopId());
            complianceSyncJob.setComplianceType(ProductBatch.TrackTraceSystem.METRC);
            complianceSyncJob.setJobType(ComplianceSyncJob.ComplianceSyncJobType.CREATE_STRAINS);
            complianceSyncJob.setStatus(ComplianceSyncJob.ComplianceSyncJobStatus.QUEUED);
            complianceSyncJob.setRequestTime(DateUtil.nowUTC().getMillis());
            complianceSyncJob.setStrains(strains);
            complianceSyncJob.setEmployeeId(token.getActiveTopUser().getUserId());


            complianceSyncJob = complianceSyncJobRepository.save(complianceSyncJob);
            backgroundJobService.addComplianceSyncJob(token.getCompanyId(), token.getShopId(), complianceSyncJob.getId());
            return true;
        } else
            throw new BlazeInvalidArgException(COMPLIANCE, "Requested strains can't be empty");
    }

}


