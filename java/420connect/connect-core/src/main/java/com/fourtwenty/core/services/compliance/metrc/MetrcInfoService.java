package com.fourtwenty.core.services.compliance.metrc;

public interface MetrcInfoService {
    void sync(String companyId, String shopId);
    void restSync(String companyId, String shopId);
}
