package com.fourtwenty.core.services.compliance.metrc;

import com.fourtwenty.core.domain.models.transaction.Transaction;

/**
 * Created by mdo on 2/22/18.
 */
public interface MetrcProcessingService {
    void submitUpdateReceipt(Transaction transaction, boolean forceSendToMetrc);
}
