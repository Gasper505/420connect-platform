package com.fourtwenty.core.services.compliance.metrc.impl;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsPatientsAPIService;
import com.blaze.clients.metrcs.MetricsSalesAPIService;
import com.blaze.clients.metrcs.models.CustomerType;
import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.patients.MetrcPatientResult;
import com.blaze.clients.metrcs.models.patients.MetricsPatient;
import com.blaze.clients.metrcs.models.sales.MetricsReceipt;
import com.blaze.clients.metrcs.models.sales.MetricsReceiptList;
import com.blaze.clients.metrcs.models.sales.MetricsSaleReceipt;
import com.blaze.clients.metrcs.models.sales.MetricsSaleTransaction;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.compliance.ComplianceCategory;
import com.fourtwenty.core.domain.models.compliance.CompliancePackage;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.ProductMetrcInfo;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.domain.models.purchaseorder.ShippingBatchDetails;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.compliance.ComplianceCategoryRepository;
import com.fourtwenty.core.domain.repositories.compliance.CompliancePackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchBundleItems;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchBundleItems.BatchItems;
import com.fourtwenty.core.services.compliance.metrc.MetrcProcessingService;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import com.mdo.pusher.RestErrorException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mdo on 2/22/18.
 */
public class MetrcProcessingServiceImpl implements MetrcProcessingService {
    private static final Log LOG = LogFactory.getLog(MetrcProcessingServiceImpl.class);

    @Inject
    private CompanyRepository companyRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;

    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ComplianceCategoryRepository complianceCategoryRepository;
    @Inject
    CompliancePackageRepository compliancePackageRepository;

    @Inject
    PurchaseOrderRepository purchaseOrderRepository;

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MetrcService metrcService;


    /**
     * Override method to submit transation to metrc
     *
     * @param transaction      : transaction
     * @param forceSendToMetrc : if this is true(from api call) then transaction always send to metrc and if false(call from job) then it depends on metrc facility setting
     */
    @Override
    public void submitUpdateReceipt(Transaction transaction, boolean forceSendToMetrc) {
        if (transaction == null) {
            return;
        }

        final MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                transaction.getCompanyId(),
                transaction.getShopId());
        final MetricsSalesAPIService metricsSalesAPIService = new MetricsSalesAPIService(metrcAuthorization);


        final Company company = companyRepository.getById(transaction.getCompanyId());
        final Shop shop = shopRepository.get(transaction.getCompanyId(), transaction.getShopId());
        String stateCode = metrcService.getShopStateCode(transaction.getCompanyId(), transaction.getShopId());
        final MetrcAccount metrcAccount = metrcAccountRepository.getMetrcAccount(company.getId(), stateCode);
        if (metrcAccount != null && metrcAccount.isEnabled()) {
            final Cart cart = transaction.getCart();
            if (cart != null && (
                    transaction.getStatus() == Transaction.TransactionStatus.Completed
                            || transaction.getStatus() == Transaction.TransactionStatus.RefundWithInventory)) {

                final List<MetrcFacilityAccount> facilities = metrcAccount.getFacilities();

                // get the facility for this shop
                MetrcFacilityAccount metrcFacilityAccount = null;
                if (facilities != null) {
                    for (MetrcFacilityAccount facility : facilities) {
                        if (transaction.getShopId().equals(facility.getShopId())) {
                            metrcFacilityAccount = facility;
                            break;
                        }
                    }
                }

                // if facility is enabled or valid and delivery transaction enabled, let's continue
                if (metrcFacilityAccount != null
                        && StringUtils.isNotBlank(metrcFacilityAccount.getFacLicense())
                        && metrcFacilityAccount.isEnabled() && metrcFacilityAccount.isEnableSync()) {

                    if (!forceSendToMetrc && !metrcFacilityAccount.isEnableDeliveryTransaction()) {
                        if (transaction.getQueueType() == Transaction.QueueType.Delivery) {
                            return;
                        }
                    }
                    List<MetricsSaleTransaction> metricsSaleTransactions = generateSalesTransactions(transaction);

                    /*if (shop.getAddress().getState().equals("CO1")) {
                        LOG.info("create sale Transaction");
                        // call sale transaction
                        sendTransactionToMetrc(transaction, metrcFacilityAccount, metricsSaleTransactions, shop);

                    } else {*/
                        LOG.info("create sale receipt");
                        // call sale receipt
                        sendReceiptToMetrc(transaction, metrcFacilityAccount, metricsSaleTransactions,shop);

                    //}
                }
            }
        }
    }


    private List<MetricsSaleTransaction> generateSalesTransactions(final Transaction transaction) {
        ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(transaction.getCompanyId(),
                ProductWeightTolerance.WeightKey.OUNCE);
        ProductWeightTolerance eighthTolerance = toleranceRepository.getToleranceForWeight(transaction.getCompanyId(),
                ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
        ProductWeightTolerance fourthTolerance = toleranceRepository.getToleranceForWeight(transaction.getCompanyId(),
                ProductWeightTolerance.WeightKey.QUARTER);

        double ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue.doubleValue();
        double eighthUnitValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue.doubleValue();
        double fourthUnitValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue.doubleValue();
        if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
            ozUnitValue = ozTolerance.getUnitValue().doubleValue();
        }

        if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
            eighthUnitValue = eighthTolerance.getUnitValue().doubleValue();
        }

        if (fourthTolerance != null && fourthTolerance.getUnitValue().doubleValue() > 0) {
            fourthUnitValue = fourthTolerance.getUnitValue().doubleValue();
        }

        final List<OrderItem> orderItems = transaction.getCart().getItems();
        // Total Final cost
        int totalFinalCost = 0;
        for (OrderItem item : transaction.getCart().getItems()) {
            if (item.getStatus() != OrderItem.OrderItemStatus.Refunded) {
                totalFinalCost += (int) (item.getFinalPrice().doubleValue() * 100);
            }
        }

        List<MetricsSaleTransaction> metricsSaleTransactions = new ArrayList<>();
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getStatus() != OrderItem.OrderItemStatus.Refunded) {
                if (orderItem.getQuantityLogs() != null && orderItem.getQuantityLogs().size() > 0) {
                    List<MetricsSaleTransaction> sales = getSalesTrans(transaction,
                            orderItem,
                            totalFinalCost,
                            ozTolerance,
                            eighthTolerance,
                            fourthTolerance);
                    metricsSaleTransactions.addAll(sales);

                } else {

                    //For Metrc Cost
                    int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
                    double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;
                    BigDecimal targetCartDiscount = transaction.getCart().getCalcCartDiscount().multiply(BigDecimal.valueOf(ratio));

                    BigDecimal preTax = orderItem.getCalcPreTax().setScale(2,RoundingMode.HALF_EVEN);
                    if (orderItem.getTaxResult() != null) {
                        BigDecimal preSalesTax = preTax;
                        if (preTax.doubleValue() <= 0) {
                            preSalesTax = orderItem.getTaxResult().getTotalPreCalcTax().setScale(2,RoundingMode.HALF_EVEN);
                        }
                        //preTax = orderItem.getTaxResult().getTotalPreCalcTax().doubleValue();


                        // preExcise
                        preSalesTax = preSalesTax.add(orderItem.getTaxResult().getTotalALExciseTax().setScale(2,RoundingMode.HALF_EVEN));
                        preSalesTax = preSalesTax.add(orderItem.getTaxResult().getTotalNALPreExciseTax().setScale(2,RoundingMode.HALF_EVEN));
                        preTax = preSalesTax;
                    }


                    //double finalMetrcCost = (orderItem.getFinalPrice().doubleValue()) - (targetCartDiscount) - preTax;
                    BigDecimal finalMetrcCost = (orderItem.getFinalPrice().subtract(targetCartDiscount).subtract(preTax));


                    MetricsSaleTransaction saleTransaction = createSalesTran(transaction, orderItem.getProductId(),
                            orderItem.getBatchId(),
                            orderItem.getPrepackageItemId(),
                            orderItem.getQuantity().doubleValue(), finalMetrcCost.doubleValue(), eighthUnitValue, fourthUnitValue);
                    if (saleTransaction != null) {
                        metricsSaleTransactions.add(saleTransaction);
                    }
                }
            }
        }
        return metricsSaleTransactions;
    }

    private MetricsSaleTransaction createSalesTran(final Transaction transaction,
                                                   final String productId,
                                                   final String batchId,
                                                   final String prepackageItemId,
                                                   double quantity,
                                                   final double metrcCost,
                                                   final double eighthUnitValue,
                                                   final double fourthUnitValue) {
        ProductBatch productBatch = null;

        if (StringUtils.isNotBlank(batchId)) {
            productBatch = productBatchRepository.get(transaction.getCompanyId(), batchId);
        } else {
            // get latest batch for product
            productBatch = productBatchRepository.getLatestBatchForProductWithMetrc(transaction.getCompanyId(),
                    transaction.getShopId(),
                    productId);
        }

        if (StringUtils.isNotBlank(prepackageItemId)) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemRepository.get(transaction.getCompanyId(), prepackageItemId);
            if (prepackageProductItem != null) {
                productBatch = productBatchRepository.get(transaction.getCompanyId(), prepackageProductItem.getBatchId());

                Prepackage prepackage = prepackageRepository.get(transaction.getCompanyId(), prepackageProductItem.getPrepackageId());
                if (prepackage != null) {
                    double unitValue = prepackage.getUnitValue().doubleValue();
                    if (unitValue == 0) {
                        prepackage.getToleranceId();
                        ProductWeightTolerance tolerance = toleranceRepository.get(transaction.getCompanyId(), prepackage.getToleranceId());
                        unitValue = tolerance.getUnitValue().doubleValue();
                    }
                    quantity = quantity * unitValue; // convert to grams
                }
            }
        }

        if (productBatch != null
                && productBatch.getTrackTraceSystem().equals(ProductBatch.TrackTraceSystem.METRC)
                && (productBatch.getTrackPackageId() != null || productBatch.getTrackPackageLabel() != null)) {
            final Product product = productRepository.get(transaction.getCompanyId(), productId);

            if (product != null) {

                MetricsSaleTransaction saleTransaction = new MetricsSaleTransaction();
                saleTransaction.setPackageLabel(productBatch.getTrackPackageLabel());
                saleTransaction.setTotalAmount(metrcCost);


                final ProductCategory category = productCategoryRepository.get(transaction.getCompanyId(), product.getCategoryId());

                if (StringUtils.isNotBlank(category.getComplianceId())) {
                    ComplianceCategory complianceCategory = complianceCategoryRepository.getById(category.getComplianceId());
                    if (complianceCategory.getData() != null && "CountBased".equalsIgnoreCase(complianceCategory.getData().getQuantityType())) {

                        // metrc track is also in units so send in units
                        productBatch.setTrackWeight(ProductWeightTolerance.WeightKey.UNIT);
                    }
                }

                if (category.getUnitType() == ProductCategory.UnitType.grams) {
                    // everything is already in grams
                    saleTransaction.setUnitOfMeasure("Grams");
                    saleTransaction.setQuantity(quantity);
                } else {
                    double quantityInGrams = quantity;
                    saleTransaction.setUnitOfMeasure("Grams");


                    CompliancePackage compliancePackage = compliancePackageRepository.getCompliancePackageByProductBatch(product.getCompanyId(),
                            product.getShopId(),
                            productBatch.getId());

                    if ((productBatch.getTrackWeight() == null || productBatch.getTrackWeight() != ProductWeightTolerance.WeightKey.UNIT)
                        || (compliancePackage != null && compliancePackage.getData() != null && "Grams".equalsIgnoreCase(compliancePackage.getData().getUnitOfMeasureName()))) {
                        // if weight == null or weight not equal to UNIT

                        if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                            double quantityInOunces = quantity;
                            quantityInOunces = NumberUtils.round(quantityInOunces, 6);
                            saleTransaction.setQuantity(quantityInOunces);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                            quantityInGrams = (quantity / 2); // divide by 2 to get half a gram per t
                            quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                            quantityInGrams = quantity * eighthUnitValue; // multiply by eighth to get amount in grams eighth value
                            quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                            quantityInGrams = quantity * fourthUnitValue; // multiply by FOURTH to get amount in grams fourth value
                            quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null
                                && product.getCustomWeight().doubleValue() > 0) {
                            if (Product.CustomGramType.GRAM == product.getCustomGramType()) {
                                quantityInGrams = quantity * product.getCustomWeight().doubleValue();
                                quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                            } else if (Product.CustomGramType.MILLIGRAM == product.getCustomGramType()) {
                                quantityInGrams = quantity * (product.getCustomWeight().doubleValue() / 1000);
                                quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                            }

                        } else {
                            // set to EACH
                            saleTransaction.setUnitOfMeasure("Each");
                            quantityInGrams = quantity;
                        }
                    } else {
                        // metrc track is also in units so send in units
                        saleTransaction.setUnitOfMeasure("Each");
                        quantityInGrams = quantity;


                    }


                    saleTransaction.setQuantity(quantityInGrams);
                }

                // fake
                return saleTransaction;
            }
        }
        return null;
    }

    /* Create sales tran for bundle items */
    private MetricsSaleTransaction createSalesTranForBundleItem(final Transaction transaction,
                                                   final Product product,
                                                   final ProductBatch productBatch,
                                                   final String prepackageItemId,
                                                   double quantity,
                                                   final double metrcCost,
                                                   final String reqMetrcLabel, //Outgoing metrc label used in shipping manifest
                                                   final double eighthUnitValue,
                                                   final double fourthUnitValue) {

        if (productBatch != null
                && productBatch.getTrackTraceSystem().equals(ProductBatch.TrackTraceSystem.METRC)
                && (productBatch.getTrackPackageId() != null || reqMetrcLabel != null)) {

            if (product != null) {

                MetricsSaleTransaction saleTransaction = new MetricsSaleTransaction();
                saleTransaction.setPackageLabel(reqMetrcLabel);
                saleTransaction.setTotalAmount(metrcCost);


                final ProductCategory category = productCategoryRepository.get(product.getCompanyId(), product.getCategoryId());

                if (StringUtils.isNotBlank(category.getComplianceId())) {
                    ComplianceCategory complianceCategory = complianceCategoryRepository.getById(category.getComplianceId());
                    if (complianceCategory.getData() != null && "CountBased".equalsIgnoreCase(complianceCategory.getData().getQuantityType())) {

                        // metrc track is also in units so send in units
                        productBatch.setTrackWeight(ProductWeightTolerance.WeightKey.UNIT);
                    }
                }

                if (category.getUnitType() == ProductCategory.UnitType.grams) {
                    // everything is already in grams
                    saleTransaction.setUnitOfMeasure("Grams");
                    saleTransaction.setQuantity(quantity);
                } else {
                    double quantityInGrams = quantity;
                    saleTransaction.setUnitOfMeasure("Grams");


                    CompliancePackage compliancePackage = compliancePackageRepository.getCompliancePackageByProductBatch(product.getCompanyId(),
                            product.getShopId(),
                            productBatch.getId());

                    if ((productBatch.getTrackWeight() == null || productBatch.getTrackWeight() != ProductWeightTolerance.WeightKey.UNIT)
                        || (compliancePackage != null && compliancePackage.getData() != null && "Grams".equalsIgnoreCase(compliancePackage.getData().getUnitOfMeasureName()))) {
                        // if weight == null or weight not equal to UNIT

                        if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                            double quantityInOunces = quantity;
                            quantityInOunces = NumberUtils.round(quantityInOunces, 6);
                            saleTransaction.setQuantity(quantityInOunces);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                            quantityInGrams = (quantity / 2); // divide by 2 to get half a gram per t
                            quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                            quantityInGrams = quantity * eighthUnitValue; // multiply by eighth to get amount in grams eighth value
                            quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                            quantityInGrams = quantity * fourthUnitValue; // multiply by FOURTH to get amount in grams fourth value
                            quantityInGrams = NumberUtils.round(quantityInGrams, 6);
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null
                                && product.getCustomWeight().doubleValue() > 0) {
                            if (Product.CustomGramType.GRAM == product.getCustomGramType()) {
                                quantityInGrams = quantity * product.getCustomWeight().doubleValue();
                                quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                            } else if (Product.CustomGramType.MILLIGRAM == product.getCustomGramType()) {
                                quantityInGrams = quantity * (product.getCustomWeight().doubleValue() / 1000);
                                quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                            }

                        } else {
                            // set to EACH
                            saleTransaction.setUnitOfMeasure("Each");
                            quantityInGrams = quantity;
                        }
                    } else {
                        // metrc track is also in units so send in units
                        saleTransaction.setUnitOfMeasure("Each");
                        quantityInGrams = quantity;


                    }


                    saleTransaction.setQuantity(quantityInGrams);
                }

                // fake
                return saleTransaction;
            }
        }
        return null;
    }


    private List<MetricsSaleTransaction> createSalesTransForBundle(final Transaction transaction,
                                                                final String productId,
                                                                final String connectedProductBatchId,
                                                                final String purchaseOrderId,
                                                                final String prepackageItemId,
                                                                double quantity,
                                                                final double metrcCost,
                                                                final double eighthUnitValue,
                                                                final double fourthUnitValue) {
        List<MetricsSaleTransaction> metrcsBundleSaleTransactions = new ArrayList<>();
        ProductBatch connectedProductBatch = productBatchRepository.getById(connectedProductBatchId); //assigned distro batch of bundle product

        //Get PO assigned shipment
        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(transaction.getCompanyId(), purchaseOrderId);
        Shipment shipment = purchaseOrder.getAssignedShipments().size()> 0? purchaseOrder.getAssignedShipments().get(0):null; 
        
        //Find in productMetrcInfo the connectedProductBatchId
        ShippingBatchDetails connectedBatchDetails = null;
        for(ProductMetrcInfo productMetrcInfo : shipment.getProductMetrcInfo()) {
            for(ShippingBatchDetails batchDetails : productMetrcInfo.getBatchDetails()) {
                if(batchDetails.getBatchId().equals(connectedProductBatch.getId())) { //Verify batch id of the distro bundle product
                    connectedBatchDetails = batchDetails;
                    break;
                }
            }
            if(connectedBatchDetails != null) {
                break;
            }
        }

        if(connectedBatchDetails == null) {
            return metrcsBundleSaleTransactions;
        }

        List<ShippingBatchDetails.MetrcPackageDetails> packages = connectedBatchDetails.getMetrcPackageDetails();

        if(connectedProductBatch.getBundleItems() != null) {

            //Calculate total amount for bundle item ratios
            double totalCost = 0.0;
            for(BatchBundleItems bundleItem : connectedProductBatch.getBundleItems()) { //Iterate over bundle items of distro product (parent)
                for(BatchItems bundleBatch : bundleItem.getBatchItems()) { //Iterate over the batches of the child
                    ProductBatch connectedBundleItemBatch = productBatchRepository.getById(bundleBatch.getBatchId());
                    double bundleBatchQuantity = quantity * (bundleBatch.getQuantity().floatValue()/connectedProductBatch.getQuantity().floatValue());
                    totalCost += connectedBundleItemBatch.getCostPerUnit().floatValue() * bundleBatchQuantity;
                }
            }

            for(BatchBundleItems bundleItem : connectedProductBatch.getBundleItems()) { //Iterate over bundle items of distro product (parent)
                Product bundleProduct = productRepository.getById(bundleItem.getProductId()); //Bundle item product (child)
                for(BatchItems bundleBatch : bundleItem.getBatchItems()) { //Iterate over the batches of the child
                    ProductBatch connectedBundleItemBatch = productBatchRepository.getById(bundleBatch.getBatchId());
                    
                    double bundleBatchQuantity = quantity * (bundleBatch.getQuantity().floatValue()/connectedProductBatch.getQuantity().floatValue());
                    
                    ShippingBatchDetails.MetrcPackageDetails metrcPackageDetails = packages.stream().filter(
                        element -> element.getBatchId().equals(bundleBatch.getBatchId())).findAny().orElse(null);

                    String reqMetrcLabel = metrcPackageDetails!=null?metrcPackageDetails.getReqMetrcLabel():null; // Get outgoing metrc tag label

                    //Calculate bundle item ratio
                    double ratio = connectedBundleItemBatch.getCostPerUnit().floatValue() * bundleBatchQuantity / totalCost; // ratio = unitPrice * quantity / totalCost
                    //Calculate metrc cost for bundle item
                    double bundleItemMetrcCost = metrcCost * ratio;
                    MetricsSaleTransaction saleTransaction = createSalesTranForBundleItem(transaction,
                                                            bundleProduct,
                                                            connectedBundleItemBatch,
                                                            prepackageItemId,
                                                            bundleBatchQuantity,
                                                            bundleItemMetrcCost,
                                                            reqMetrcLabel,
                                                            eighthUnitValue,fourthUnitValue);
                    if (saleTransaction != null) {
                        metrcsBundleSaleTransactions.add(saleTransaction);
                    }
                }
            }
        }
        
        return metrcsBundleSaleTransactions;

    }



    /*
    ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(transaction.getCompanyId(),
                ProductWeightTolerance.WeightKey.OUNCE);
        ProductWeightTolerance eighthTolerance = toleranceRepository.getToleranceForWeight(transaction.getCompanyId(),
                ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
        ProductWeightTolerance fourthTolerance = toleranceRepository.getToleranceForWeight(transaction.getCompanyId(),
                ProductWeightTolerance.WeightKey.QUARTER);

     */
    private List<MetricsSaleTransaction> getSalesTrans(final Transaction transaction,
                                                       final OrderItem orderItem,
                                                       final int totalFinalCost,
                                                       final ProductWeightTolerance ozTolerance,
                                                       ProductWeightTolerance eighthTolerance,
                                                       ProductWeightTolerance fourthTolerance)  {
        double ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue.doubleValue();
        double eighthUnitValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue.doubleValue();
        double fourthUnitValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue.doubleValue();
        if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
            ozUnitValue = ozTolerance.getUnitValue().doubleValue();
        }

        if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
            eighthUnitValue = eighthTolerance.getUnitValue().doubleValue();
        }

        if (fourthTolerance != null && fourthTolerance.getUnitValue().doubleValue() > 0) {
            fourthUnitValue = fourthTolerance.getUnitValue().doubleValue();
        }


        BigDecimal orderQty = orderItem.getQuantity();

        //For Metrc Cost
        int finalCost = (int) (orderItem.getFinalPrice().doubleValue() * 100);
        double ratio = totalFinalCost > 0 ? finalCost / (double) totalFinalCost : 0;
        BigDecimal targetCartDiscount = transaction.getCart().getCalcCartDiscount().multiply(BigDecimal.valueOf(ratio));

        BigDecimal preTax = orderItem.getCalcPreTax().setScale(2,RoundingMode.HALF_EVEN);
        if (orderItem.getTaxResult() != null) {
            BigDecimal preSalesTax = preTax;
            if (preTax.doubleValue() <= 0) {
                preSalesTax = orderItem.getTaxResult().getTotalPreCalcTax().setScale(2,RoundingMode.HALF_EVEN);
            }
            //preTax = orderItem.getTaxResult().getTotalPreCalcTax().doubleValue();


            // preExcise
            preSalesTax = preSalesTax.add(orderItem.getTaxResult().getTotalALExciseTax().setScale(2,RoundingMode.HALF_EVEN));
            preSalesTax = preSalesTax.add(orderItem.getTaxResult().getTotalNALPreExciseTax().setScale(2,RoundingMode.HALF_EVEN));
            preTax = preSalesTax;
        }


        //double finalMetrcCost = (orderItem.getFinalPrice().doubleValue()) - (targetCartDiscount) - preTax;
        BigDecimal finalMetrcCost = (orderItem.getFinalPrice().subtract(targetCartDiscount).subtract(preTax));

        List<MetricsSaleTransaction> metricsSaleTransactions = new ArrayList<>();
        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {

            double quantity = quantityLog.getQuantity().doubleValue();

            double metrcCost = finalMetrcCost.multiply(quantityLog.getQuantity().divide(orderQty,2, RoundingMode.HALF_EVEN)).doubleValue();


            ProductBatch productBatch = productBatchRepository.get(transaction.getCompanyId(), quantityLog.getBatchId());
            if(productBatch != null && StringUtils.isNotBlank(productBatch.getConnectedBatchId())) {
                List<MetricsSaleTransaction> metrcsBundleSaleTransactions = createSalesTransForBundle(transaction, 
                orderItem.getProductId(), 
                productBatch.getConnectedBatchId(), //connected distro batch id
                productBatch.getPurchaseOrderId(), //PO of retail batch
                quantityLog.getPrepackageItemId(),
                quantity,
                metrcCost,
                eighthUnitValue,fourthUnitValue);
                metricsSaleTransactions.addAll(metrcsBundleSaleTransactions);
            }else {
                MetricsSaleTransaction saleTransaction = createSalesTran(transaction,
                orderItem.getProductId(),
                quantityLog.getBatchId(),
                quantityLog.getPrepackageItemId(),
                quantity,
                metrcCost,
                eighthUnitValue,fourthUnitValue);
                if (saleTransaction != null) {
                    metricsSaleTransactions.add(saleTransaction);
                }
            }
        }
        return metricsSaleTransactions;
    }


    private void sendReceiptToMetrc(final Transaction transaction,
                                    final MetrcFacilityAccount metrcFacilityAccount,
                                    final List<MetricsSaleTransaction> metricsSaleTransactions,
                                    final Shop shop) {

        final MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                transaction.getCompanyId(),
                transaction.getShopId());
        final MetricsSalesAPIService metricsSalesAPIService = new MetricsSalesAPIService(metrcAuthorization);
        final MetricsPatientsAPIService metricsPatientsAPIService = new MetricsPatientsAPIService(metrcAuthorization);

        // add receipt info if there are transactions to send
        if (metricsSaleTransactions.size() > 0 || transaction.getMetrcId() != null) {

            if (transaction.getTraceSubmitStatus() == Transaction.TraceSubmissionStatus.SubmissionPending
                    || transaction.getTraceSubmitStatus() == Transaction.TraceSubmissionStatus.SubmissionWaitingID) {
                LOG.info("SUBMITTED AND WAITING ON ID.");
                return;
            }

            Member member = memberRepository.get(transaction.getCompanyId(), transaction.getMemberId());
            String patientLicenseNo = null;
            Recommendation recommendation = null;
            CustomerType customerType = CustomerType.Consumer;

            if (shop.getShopType() == Shop.ShopType.Recreational) {
                customerType = CustomerType.Consumer;
            } else if (shop.getShopType() == Shop.ShopType.Medicinal) {
                customerType = CustomerType.Patient;
                if (member.getRecommendations() != null && member.getRecommendations().size() > 0) {
                    // sort by date
                    member.getRecommendations().sort(new Comparator<Recommendation>() {
                        @Override
                        public int compare(Recommendation o1, Recommendation o2) {
                            Long exp1 = o1.getExpirationDate() == null ? 0 : o1.getExpirationDate();
                            Long exp2 = o2.getExpirationDate() == null ? 0 : o2.getExpirationDate();

                            return exp2.compareTo(exp1);
                        }
                    });

                    recommendation = member.getRecommendations().get(0);
                    patientLicenseNo = recommendation.getRecommendationNumber();
                    customerType = CustomerType.Patient;
                }
                
            } else  if (member != null) {

                // otherwise, check consumer type
                customerType = member.getConsumerType() == ConsumerType.AdultUse ? CustomerType.Consumer : CustomerType.Patient;

                if (customerType == CustomerType.Patient) {
                    if (member.getRecommendations() != null && member.getRecommendations().size() > 0) {
                        // sort by date
                        member.getRecommendations().sort(new Comparator<Recommendation>() {
                            @Override
                            public int compare(Recommendation o1, Recommendation o2) {
                                Long exp1 = o1.getExpirationDate() == null ? 0 : o1.getExpirationDate();
                                Long exp2 = o2.getExpirationDate() == null ? 0 : o2.getExpirationDate();

                                return exp2.compareTo(exp1);
                            }
                        });

                        recommendation = member.getRecommendations().get(0);
                        patientLicenseNo = recommendation.getRecommendationNumber();
                        customerType = CustomerType.Patient;
                    } else {
                        customerType = CustomerType.Consumer;
                    }
                }
            }



            // create receipt
            MetricsSaleReceipt receipt = new MetricsSaleReceipt();
            receipt.setPatientLicenseNumber(patientLicenseNo);
            receipt.setSalesCustomerType(customerType.getMetrcValue());
            DateTime dateTime = new DateTime(transaction.getProcessedTime());
            receipt.setSalesDateTime(dateTime.toString());

            boolean updating = false;
            if (transaction.getMetrcId() != null) {
                updating = true;
                receipt.setId(transaction.getMetrcId());
            }

            if (StringUtils.isNotBlank(patientLicenseNo) && recommendation != null && metricsPatientsAPIService != null) {
                if (metrcFacilityAccount.getStateCode().equalsIgnoreCase("MI")) {
                    // also send patient info
                    boolean shouldAdd = true;
                    try {
                        MetrcPatientResult patientResult = metricsPatientsAPIService.getPatientStatus(patientLicenseNo, metrcFacilityAccount.getFacLicense());
                        if (patientResult == null) {
                            LOG.error("Did not find patient..");
                        } else {
                            shouldAdd = false;
                        }
                    } catch (Exception e) {
                        LOG.error("Did not find patient..");
                    }
                    if (shouldAdd) {
                        try {

                            String issueDate = DateUtil.toDateFormatted(recommendation.getIssueDate(), shop.getTimeZone(), "yyyy-MM-dd");
                            String expDate = DateUtil.toDateFormatted(recommendation.getExpirationDate(), shop.getTimeZone(), "yyyy-MM-dd");

                            Long nowMillis = DateUtil.nowUTC().getMillis();
                            String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyy-MM-dd");


                            MetricsPatient metricsPatient = new MetricsPatient();
                            metricsPatient.setLicenseNumber(patientLicenseNo);
                            metricsPatient.setActualDate(todayFormatted);
                            metricsPatient.setLicenseEffectiveEndDate(expDate);
                            metricsPatient.setLicenseEffectiveStartDate(issueDate);

                            List<MetricsPatient> patients = new ArrayList<>();
                            metricsPatientsAPIService.addPatient(patients,metrcFacilityAccount.getFacLicense());
                            LOG.info("Patients added...");


                        } catch (Exception e) {
                            LOG.error("Patients added...error: ",e);
                        }
                    }
                }
            }


            // set thet ransactions
            receipt.setTransactions(metricsSaleTransactions);

            List<MetricsSaleReceipt> metricsSaleReceipts = new ArrayList<>();
            metricsSaleReceipts.add(receipt);

            // there are receipts, let's send it
            try {
                for (MetricsSaleReceipt saleReceipt : metricsSaleReceipts) {
                    for (MetricsSaleTransaction saleTransaction : saleReceipt.getTransactions()) {
                        LOG.info(String.format("Label: %s ; Qty: %.3f ; Cost: $%.3f", saleTransaction.getPackageLabel(),
                                saleTransaction.getQuantity(),
                                saleTransaction.getTotalAmount()));
                    }
                }

                transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.SubmissionInProgress);
                if (updating) {
                    if (metricsSaleTransactions.size() == 0) {
                        // we are now deleting the whole transaction
                        LOG.info("Deleting receipt from Metrc: " + transaction.getMetrcId());
                        metricsSalesAPIService.deleteSaleReceipt(transaction.getMetrcId().toString(), metrcFacilityAccount.getFacLicense());
                        LOG.info("Delete metrc receipt successful");
                        transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.Deleted);
                    } else {
                        LOG.info("Updating receipt to Metrc");
                        String result = metricsSalesAPIService.updateSaleReceipts(metricsSaleReceipts, metrcFacilityAccount.getFacLicense());
                        LOG.info("Update receipts to Metrc: SUCCESS -- " + result);
                        transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.Completed);
                    }
                } else {
                    LOG.info("Submitting new receipt to Metrc");
                    String result = metricsSalesAPIService.createSaleReceipts(metricsSaleReceipts, metrcFacilityAccount.getFacLicense());
                    LOG.info("Sending receipts to Metrc: SUCCESS -- " + result);
                    transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.SubmissionWaitingID);


                    MetricsReceiptList items = metricsSalesAPIService.getSaleReceipts(metrcFacilityAccount.getFacLicense(), dateTime.minusSeconds(5).toString());

                    for (MetricsReceipt metricsReceipt : items) {
                        DateTime saleTime = new DateTime(metricsReceipt.getSalesDateTime());
                        long receiptTime = saleTime.getMillis() / 1000;
                        long processedTime = dateTime.getMillis() / 1000;

                        if (receiptTime == processedTime) {
                            // in seconds
                            transactionRepository.updateMetrcId(transaction.getCompanyId(), transaction.getId(), metricsReceipt.getId());
                            LOG.info("Update Transaction with metrc Id -- Success -- " + metricsReceipt.getId());
                            break;
                        }
                    }
                }

            } catch (RestErrorException e) {
                LOG.error("Error sending metrc receipt: " + e.getErrorResponse(), e);
                if (e.getErrorResponse() instanceof MetricsErrorList) {
                    String message = null;
                    MetricsErrorList errors = (MetricsErrorList)e.getErrorResponse();
                    if (!errors.isEmpty()) {
                        message = errors.get(0).getMessage();
                    }
                    transactionRepository.updateTraceSubmitStatusMessage(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.SubmissionError,message);
                } else {
                    transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.SubmissionError);
                }

            } catch (Exception e) {
                LOG.error("Error sending metrc receipt", e);
            }
        }
    }

    private void sendTransactionToMetrc(final Transaction transaction,
                                        final MetrcFacilityAccount metrcFacilityAccount,
                                        final List<MetricsSaleTransaction> metricsSaleTransactions, Shop shop) {

        final MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                transaction.getCompanyId(),
                transaction.getShopId());
        final MetricsSalesAPIService metricsSalesAPIService = new MetricsSalesAPIService(metrcAuthorization);

        if (metricsSaleTransactions.size() > 0) {
            try {
                Long nowMillis = DateTime.now().getMillis();
                String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyy-MM-dd");
                transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.SubmissionInProgress);
                LOG.info("Creating sales transactions");
                String result = metricsSalesAPIService.createSaleTransactionsByDate(metricsSaleTransactions, todayFormatted, metrcFacilityAccount.getFacLicense());
                LOG.info("Sending sale transaction: SUCCESS -- ");
                transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.Completed);

            } catch (RestErrorException e) {
                LOG.error("Error sending sale transaction " + e.getErrorResponse(), e);
                transactionRepository.updateTraceSubmitStatus(transaction.getCompanyId(), transaction.getId(), Transaction.TraceSubmissionStatus.SubmissionError);
            } catch (Exception e) {
                LOG.error("Error sending transaction", e);
            }
        }
    }





}