package com.fourtwenty.core.services.credits;

import com.fourtwenty.core.domain.models.credits.CreditSaleLineItem;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.features.RemainingCreditsResult;
import com.fourtwenty.core.rest.features.SMSBuyRequest;

/**
 * Created by mdo on 7/18/17.
 */
public interface CreditService {

    SearchResult<CreditSaleLineItem> getCreditSaleLogs(int start, int limit);

    CreditSaleLineItem buySMSCredit(SMSBuyRequest buyRequest);

    RemainingCreditsResult getRemainingCredits();

    CreditSaleLineItem getCreditSalesItem(String companyId, String id);
}
