package com.fourtwenty.core.services.credits.impl;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.credits.CreditSaleLineItem;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CreditSaleLineItemRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.features.RemainingCreditsResult;
import com.fourtwenty.core.rest.features.SMSBuyRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.credits.CreditService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.joda.time.DateTime;

import java.math.BigDecimal;

/**
 * Created by mdo on 7/18/17.
 */
public class CreditServiceImpl extends AbstractAuthServiceImpl implements CreditService {
    @Inject
    CreditSaleLineItemRepository creditSaleLineItemRepository;
    @Inject
    AppRepository appRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;

    @Inject
    public CreditServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public CreditSaleLineItem buySMSCredit(SMSBuyRequest buyRequest) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        double cost = buyRequest.getAmount() * app.getSmsCost().doubleValue() / 100;

        CreditSaleLineItem saleLineItem = new CreditSaleLineItem();
        saleLineItem.prepare(token.getCompanyId());
        saleLineItem.setShopId(token.getShopId());
        saleLineItem.setPurchaseDate(DateTime.now().getMillis());
        saleLineItem.setAmountBought(buyRequest.getAmount());
        saleLineItem.setCreditType(CreditSaleLineItem.CreditType.SMS);
        saleLineItem.setCost(BigDecimal.valueOf(cost));
        saleLineItem.setEmployeeId(token.getActiveTopUser().getUserId());

        // Update company featuresO
        companyFeaturesRepository.addSMSCredit(token.getCompanyId(), buyRequest.getAmount());
        creditSaleLineItemRepository.save(saleLineItem);
        return saleLineItem;
    }

    @Override
    public RemainingCreditsResult getRemainingCredits() {
        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        RemainingCreditsResult remainingCreditsResult = new RemainingCreditsResult();
        remainingCreditsResult.setSmsCredits(companyFeatures.getSmsMarketingCredits());
        remainingCreditsResult.setEmailCredits(companyFeatures.getEmailMarketingCredits());
        return remainingCreditsResult;
    }

    @Override
    public SearchResult<CreditSaleLineItem> getCreditSaleLogs(int start, int limit) {
        if (limit > 100) {
            limit = 100;
        }
        return creditSaleLineItemRepository.findItemsWithSort(token.getCompanyId(), "{buyDate:-1}", start, limit);
    }

    @Override
    public CreditSaleLineItem getCreditSalesItem(final String companyId, final String id) {
        return creditSaleLineItemRepository.get(companyId, id);
    }

}
