package com.fourtwenty.core.services.developer;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 2/2/17.
 */
public interface DeveloperInventoryService {
    SearchResult<ProductCategory> getProductCategories();

    ProductCategory getProductCategoryById(String categoryId);

    SearchResult<Product> searchProducts(String categoryId, String productId, int start, int limit);
}
