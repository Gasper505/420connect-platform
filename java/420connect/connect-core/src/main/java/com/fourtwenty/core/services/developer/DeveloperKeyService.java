package com.fourtwenty.core.services.developer;

import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.rest.developer.request.DeveloperKeyAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 2/14/17.
 */
public interface DeveloperKeyService {
    DeveloperKey createNewDeveloperKey(DeveloperKeyAddRequest request);

    SearchResult<DeveloperKey> getDeveloperKeys();

    DeveloperKey updateDeveloperKey(String developerKeyId, DeveloperKeyAddRequest request);

    void deleteDeveloperKey(String developerKeyId);
}
