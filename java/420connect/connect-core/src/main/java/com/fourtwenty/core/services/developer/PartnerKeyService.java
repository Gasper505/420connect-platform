package com.fourtwenty.core.services.developer;

import com.fourtwenty.core.domain.models.developer.PartnerKey;

/**
 * Created by mdo on 2/9/18.
 */
public interface PartnerKeyService {
    PartnerKey getPartnerKey(String key);
}
