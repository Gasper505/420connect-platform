package com.fourtwenty.core.services.developer.impl;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.DeveloperAuthToken;
import com.fourtwenty.core.services.AbstractDeveloperServiceImpl;
import com.fourtwenty.core.services.developer.DeveloperInventoryService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 2/2/17.
 */
public class DeveloperInventoryServiceImpl extends AbstractDeveloperServiceImpl implements DeveloperInventoryService {
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    VendorRepository vendorRepository;

    @Inject
    public DeveloperInventoryServiceImpl(Provider<DeveloperAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<ProductCategory> getProductCategories() {
        return productCategoryRepository.findItems(devToken.getCompanyId(), devToken.getShopId(), 0, 300);
    }

    @Override
    public ProductCategory getProductCategoryById(String categoryId) {
        return productCategoryRepository.get(devToken.getCompanyId(), categoryId);
    }

    @Override
    public SearchResult<Product> searchProducts(String categoryId, String productId, int start, int limit) {

        if (limit <= 0 || limit > 200)
            limit = 200;

        SearchResult<Product> results = new SearchResult<>();
        if (StringUtils.isNotEmpty(productId)) {
            Product product = productRepository.get(devToken.getCompanyId(), productId);
            if (product != null) {
                results.getValues().add(product);
            }
        } else if (StringUtils.isNotEmpty(categoryId)) {
            results = productRepository.findProductsByCategoryId(devToken.getCompanyId(), devToken.getShopId(), categoryId, "{name:1}", start, limit);
        } else {
            results = productRepository.findItems(devToken.getCompanyId(), devToken.getShopId(), "{name:1}", start, limit);
        }

        assignedProductDependencies(results);
        return results;
    }

    private void assignedProductDependencies(SearchResult<Product> products) {
        List<ObjectId> vendorIds = new ArrayList<>();
        for (Product p : products.getValues()) {
            if (p.getVendorId() != null && ObjectId.isValid(p.getVendorId())) {
                vendorIds.add(new ObjectId(p.getVendorId()));
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(devToken.getCompanyId(), vendorIds);
        for (Product p : products.getValues()) {
            p.setVendor(vendorHashMap.get(p.getVendorId()));
        }
    }
}
