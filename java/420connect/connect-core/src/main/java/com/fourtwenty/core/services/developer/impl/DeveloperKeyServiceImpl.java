package com.fourtwenty.core.services.developer.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.developer.request.DeveloperKeyAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.developer.DeveloperKeyService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by mdo on 2/14/17.
 */
public class DeveloperKeyServiceImpl extends AbstractAuthServiceImpl implements DeveloperKeyService {

    @Inject
    DeveloperKeyRepository developerKeyRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    ShopRepository shopRepository;

    @Inject
    public DeveloperKeyServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public DeveloperKey createNewDeveloperKey(DeveloperKeyAddRequest request) {

        Shop shop = shopRepository.get(token.getCompanyId(), request.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("DeveloperKeys", "Shop does not exist.");
        }

        DeveloperKey developerKey = new DeveloperKey();
        developerKey.prepare(token.getCompanyId());
        developerKey.setShopId(request.getShopId());
        developerKey.setActive(true);
        String key = securityUtil.generateKey();
        String secret = securityUtil.generateSecret();
        developerKey.setKey(key);
        if (StringUtils.isBlank(request.getName())) {
            developerKey.setName("Default");
        } else {
            developerKey.setName(request.getName());
        }
        developerKey.setSecret(secret);
        developerKey.setEnableExposeSales(request.isEnableExposeSales());
        developerKey.setEnableExposeMembers(request.isEnableExposeMembers());

        developerKeyRepository.save(developerKey);
        return developerKey;
    }

    @Override
    public SearchResult<DeveloperKey> getDeveloperKeys() {
        return developerKeyRepository.getActiveItems(token.getCompanyId());
    }

    @Override
    public DeveloperKey updateDeveloperKey(String developerKeyId, DeveloperKeyAddRequest request) {
        DeveloperKey devKey = developerKeyRepository.get(token.getCompanyId(), developerKeyId);
        if (devKey == null) {
            throw new BlazeInvalidArgException("DeveloperKeys", "Developer key does not exist.");
        }

        if (StringUtils.isBlank(request.getName())) {
            devKey.setName("Default");
        } else {
            devKey.setName(request.getName());
        }
        devKey.setShopId(request.getShopId());
        devKey.setEnableExposeSales(request.isEnableExposeSales());
        devKey.setEnableExposeMembers(request.isEnableExposeMembers());

        developerKeyRepository.update(token.getCompanyId(), devKey.getId(), devKey);

        return devKey;
    }

    @Override
    public void deleteDeveloperKey(String developerKeyId) {
        DeveloperKey devKey = developerKeyRepository.get(token.getCompanyId(), developerKeyId);
        if (devKey == null) {
            throw new BlazeInvalidArgException("DeveloperKeys", "Developer Keys does not exist.");
        } else if (devKey.isDeleted()) {
            throw new BlazeInvalidArgException("DeveloperKeys", "Developer Keys is already deleted.");
        }
        developerKeyRepository.removeByIdSetState(token.getCompanyId(), devKey.getId());
    }
}
