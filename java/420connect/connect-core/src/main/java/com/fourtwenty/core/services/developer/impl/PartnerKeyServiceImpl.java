package com.fourtwenty.core.services.developer.impl;

import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.repositories.developer.PartnerKeyRepository;
import com.fourtwenty.core.services.developer.PartnerKeyService;
import com.google.inject.Inject;

/**
 * Created by mdo on 2/9/18.
 */
public class PartnerKeyServiceImpl implements PartnerKeyService {
    @Inject
    PartnerKeyRepository partnerKeyRepository;

    @Override
    public PartnerKey getPartnerKey(String key) {
        return partnerKeyRepository.getPartnerKey(key);
    }
}
