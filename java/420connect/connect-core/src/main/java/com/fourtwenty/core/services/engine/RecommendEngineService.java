package com.fourtwenty.core.services.engine;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.engine.SuggestedPromotion;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 1/26/18.
 */
public interface RecommendEngineService {
    SearchResult<SuggestedPromotion> getSuggestedPromotions(Transaction transaction);
}
