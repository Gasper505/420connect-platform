package com.fourtwenty.core.services.engine.impl;

import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.engine.SuggestedPromotion;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.engine.RecommendEngineService;
import com.fourtwenty.core.services.mgmt.PromotionProcessService;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by mdo on 1/26/18.
 */
public class RecommendEngineServiceImpl extends AbstractAuthServiceImpl implements RecommendEngineService {
    @Inject
    PromotionProcessService promotionProcessService;


    @Inject
    public RecommendEngineServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<SuggestedPromotion> getSuggestedPromotions(Transaction transaction) {
        return promotionProcessService.getSuggestedPromotions(token.getCompanyId(), token.getShopId(), transaction);
    }
}
