package com.fourtwenty.core.services.global;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;

import java.math.BigDecimal;
import java.util.HashMap;

public interface CannabisLimitService {

    StateCannabisLimit getCannabisLimitByConsumerTypeAndState(String companyId, String state, ConsumerType consumerType);

    boolean checkCannabisLimit(String companyId, Shop shop, String memberId, Cart newCart, ConsumerCart consumerCart, boolean widget);

    HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit(String companyId, Shop shop, Member member, ConsumerUser consumerUser, StateCannabisLimit cannabisLimit, Cart newCart, ConsumerCart consumerCart, boolean widget);

    HashMap<Product.CannabisType, BigDecimal> prepareMemberDataForCannabis(MemberResult member, HashMap<Product.CannabisType, BigDecimal> cannabisTypeMap);
}
