package com.fourtwenty.core.services.global;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.Shop;

/**
 * Created by mdo on 8/5/18.
 */
public interface CashDrawerProcessorService {
    void processCurrentCashDrawer(Shop shop, CashDrawerSession dbLogResult, boolean isNew);

    ;
}
