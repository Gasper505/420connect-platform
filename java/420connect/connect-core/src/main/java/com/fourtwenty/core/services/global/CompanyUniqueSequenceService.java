package com.fourtwenty.core.services.global;

public interface CompanyUniqueSequenceService {
    long getNewIdentifier(String companyId, String criteria, long count);
}
