package com.fourtwenty.core.services.global;

import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface ReportingInfoService {
    ReportingInfo createReportingInfo(ReportingInfo reportingInfo);

    SearchResult<ReportingInfo> getReportingInfo(int start, int limit);

    ReportingInfo updateReportingInfo(String reportingInfoId, ReportingInfo reportingInfo);

    ReportingInfo getReportingInfoById(String reportingInfoId);
}
