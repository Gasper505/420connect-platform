package com.fourtwenty.core.services.global.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.global.StateCannabisLimitRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class CannabisLimitServiceImpl implements CannabisLimitService {
    private static final Log LOG = LogFactory.getLog(CannabisLimitServiceImpl.class);
    @Inject
    private StateCannabisLimitRepository stateCannabisLimitRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductWeightToleranceRepository toleranceRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private CompanyRepository companyRepository;

    @Override
    public StateCannabisLimit getCannabisLimitByConsumerTypeAndState(String companyId, String state, ConsumerType consumerType) {
        Company company = companyRepository.getById(companyId);

        StateCannabisLimit stateCannabisLimit = null;
        if (company != null) {
            switch (consumerType) {
                case AdultUse:
                    stateCannabisLimit = company.getAdultUseLimit();
                    break;
                case MedicinalThirdParty:
                    stateCannabisLimit = company.getThirdPartyLimit();
                    break;
                case MedicinalState:
                    stateCannabisLimit = company.getMmicLimit();
                    break;
            }
        }
        if (stateCannabisLimit == null) {
            return stateCannabisLimitRepository.getStateCannabisLimit(consumerType, state);
        } else {
            return stateCannabisLimit;
        }
    }


    @Override
    public boolean checkCannabisLimit(String companyId, Shop shop, String memberId, Cart newCart, ConsumerCart consumerCart, boolean widget) {
        if (StringUtils.isBlank(memberId)) {
            return true; // member does not exist so we just return true.
        }

        ConsumerUser consumerUser = null;
        Member member = null;
        if (widget) {
            consumerUser = consumerUserRepository.getById(memberId);
            if (consumerUser == null) {
                return true;
            } else {
                if (consumerUser != null) {
                    if (StringUtils.isNotBlank(consumerUser.getMemberId())) {
                        member = memberRepository.get(companyId, consumerUser.getMemberId());
                    }
                }
            }
        } else {
            member = memberRepository.get(companyId, memberId);
            if (member == null) {
                return true; // member does not exist so we just return true.
            }
        }


        if (shop == null) {
            return true; // member does not exist so we just return true.
        }
        if (shop.isEnableCannabisLimit() == false) {
            return true;
        }

        StateCannabisLimit cannabisLimit = null;
        if (widget) {
            cannabisLimit = this.getCannabisLimitByConsumerTypeAndState(companyId,shop.getAddress().getState(), consumerUser.getConsumerType());
        } else {
            cannabisLimit = this.getCannabisLimitByConsumerTypeAndState(companyId,shop.getAddress().getState(), member.getConsumerType());
        }

        if (cannabisLimit == null) {
            return true; // member does not exist so we just return true.
        }

        HashMap<Product.CannabisType, BigDecimal> cannabisMap = getAggregatedUserLimits(companyId, shop, member, consumerUser, cannabisLimit, newCart, consumerCart, widget);

        if (cannabisMap.size() == 0 && cannabisMap.isEmpty()) {
            return true;
        }

        printLimits(cannabisMap);

        boolean result = true;

        validate(cannabisLimit.getConcentrates(), Product.CannabisType.CONCENTRATE,cannabisMap,"State Cannabis Limit for concentrates is reached.");
        validate(cannabisLimit.getNonConcentrates(), Product.CannabisType.NON_CONCENTRATE,cannabisMap,"State Cannabis Limit for non-concentrates is reached.");
        validate(cannabisLimit.getPlants(), Product.CannabisType.PLANT,cannabisMap,"State Cannabis Limit for plants is reached.");
        validate(cannabisLimit.getThc(), Product.CannabisType.THC,cannabisMap,"State Cannabis Limit for THC is reached.");
        validate(cannabisLimit.getSeeds(), Product.CannabisType.SEEDS,cannabisMap,"State Cannabis Limit for seeds is reached.");


        if (cannabisLimit.getMax().doubleValue() > 0) {

            double conValue = 0;
            double nonValue = 0;
            BigDecimal concentrates = cannabisMap.get(Product.CannabisType.CONCENTRATE); //doubleValue();
            if (concentrates != null) {
                conValue = concentrates.doubleValue();
            }
            BigDecimal nonConcentrates = cannabisMap.get(Product.CannabisType.NON_CONCENTRATE); //.doubleValue();
            if (nonConcentrates != null) {
                nonValue = nonConcentrates.doubleValue();
            }
            double max = NumberUtils.round(conValue + nonValue, 4);
            if (cannabisLimit.getMax().doubleValue() < max) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }

        }
        return result;
    }

    private void validate(BigDecimal stateLimit, Product.CannabisType cannabisType, HashMap<Product.CannabisType, BigDecimal> cannabisMap, String error) {
        if (cannabisMap.containsKey(cannabisType)) {
            double limit = NumberUtils.round(stateLimit,2);
            double userUsage = NumberUtils.round(cannabisMap.get(cannabisType),2);
            if (userUsage > 0) {
                if (limit > 0 && limit < userUsage) {
                    LOG.info(String.format("%s limit: %.2f,  user: %.2f",cannabisType.toString(),limit,userUsage));
                    throw new BlazeInvalidArgException("CannabisLimit", error);
                }
            }
        }
    }

    private void printLimits(HashMap<Product.CannabisType, BigDecimal> cannabisMap) {
        LOG.info("Limits:");
        for (Map.Entry<Product.CannabisType, BigDecimal> entry : cannabisMap.entrySet()) {
            LOG.info(entry.getKey() + " : " + entry.getValue());
        }
    }

    private BigDecimal applyProductWeightPerUnitType(String companyId,
                                                     OrderItem orderItem, double quantity,
                                                     HashMap<String, Product> productHashMap,
                                                     HashMap<String, ProductCategory> productCategoryHashMap,
                                                     ProductWeightTolerance ozTolerance,
                                                     ProductWeightTolerance eighthTolerance,
                                                     ProductWeightTolerance fourthTolerance) {

        double ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue.doubleValue();
        double eighthUnitValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue.doubleValue();
        double fourthUnitValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue.doubleValue();
        if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
            ozUnitValue = ozTolerance.getUnitValue().doubleValue();
        }

        if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
            eighthUnitValue = eighthTolerance.getUnitValue().doubleValue();
        }

        if (fourthTolerance != null && fourthTolerance.getUnitValue().doubleValue() > 0) {
            fourthUnitValue = fourthTolerance.getUnitValue().doubleValue();
        }

        double quantityInGrams = 0.0;
        if (orderItem != null) {
            if (productHashMap.containsKey(orderItem.getProductId())) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null) {
                    ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());//productCategoryRepository.get(companyId, product.getCategoryId());
                    if (productCategory != null) {
                        if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
                            quantityInGrams = quantity; // already in grams

                        } else {
                            quantityInGrams = quantity;

                            if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                                quantityInGrams = quantity;
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                                quantityInGrams = (quantity / 2); // divide by 2 to get half a gram per t
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                                quantityInGrams = quantity * eighthUnitValue; // multiply by eighth to get amount in grams eighth value
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                                quantityInGrams = quantity * fourthUnitValue; // multiply by FOURTH to get amount in grams fourth value
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.OUNCE) {
                                quantityInGrams = quantity * ozUnitValue; // multiply by ounce to get amount in grams ounce value
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_OUNCE) {
                                quantityInGrams = quantity * (ozUnitValue / 2); // divide by 2 to get amount in grams half ounce value
                                quantityInGrams = NumberUtils.round(quantityInGrams, 6);

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH) {
                                quantityInGrams = orderItem.getQuantity().doubleValue();

                            } else if (product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && product.getCustomWeight() != null && product.getCustomWeight().doubleValue() >= 0) {
                                if (product.getCustomGramType() == Product.CustomGramType.GRAM) {
                                    quantityInGrams = quantity * product.getCustomWeight().doubleValue();
                                    quantityInGrams = NumberUtils.round(quantityInGrams, 2);
                                } else if (Product.CustomGramType.MILLIGRAM == product.getCustomGramType()) {
                                    quantityInGrams = quantity * (product.getCustomWeight().doubleValue() / 1000);
                                    quantityInGrams = NumberUtils.round(quantityInGrams, 3);
                                }
                            }
                        }
                    }
                }
            }
        }
        return BigDecimal.valueOf(quantityInGrams);
    }

    public HashMap<Product.CannabisType, BigDecimal> getAggregatedUserLimits(String companyId, Shop shop, Member member, ConsumerUser consumerUser, StateCannabisLimit cannabisLimit, Cart newCart, ConsumerCart consumerCart, boolean widget) {
        HashMap<Product.CannabisType, Product.CannabisType> typeMapping = Product.CannabisType.getCannabisMapping();

        HashMap<Product.CannabisType, BigDecimal> rawLimits = userCurrentCannabisLimit(companyId, shop, member, consumerUser, cannabisLimit, newCart, consumerCart, widget);

        HashMap<Product.CannabisType, BigDecimal> cannabisMap = new HashMap<>();

        for (Product.CannabisType rawCannaTypeKey : rawLimits.keySet()) {
            Product.CannabisType cannabisType = typeMapping.get(rawCannaTypeKey);
            if (cannabisType != null) {
                if (cannabisMap.containsKey(cannabisType)) {
                    BigDecimal updateQuantity = cannabisMap.get(cannabisType).add(rawLimits.get(rawCannaTypeKey));
                    cannabisMap.put(cannabisType, updateQuantity);
                } else {
                    cannabisMap.put(cannabisType, rawLimits.get(rawCannaTypeKey));
                }
            }
        }
        return cannabisMap;
    }

    @Override
    public HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit(String companyId, Shop shop, Member member, ConsumerUser consumerUser, StateCannabisLimit cannabisLimit, Cart newCart, ConsumerCart consumerCart, boolean widget) {
        HashMap<Product.CannabisType, BigDecimal> cannabisMap = new HashMap<>();

        if (cannabisLimit == null) {
            return cannabisMap;
        }

        String timeZone = shop.getTimeZone();

        // this is in UTC
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

        int duration = 0;
        if (cannabisLimit.getDuration() > 0) {
            duration = cannabisLimit.getDuration() - 1;
        }

        long timeZoneStartDateMillis = DateTime.now().minusDays(duration).getMillis(); // 24 hrs ago
        timeZoneStartDateMillis = DateUtil.nowWithTimeZone(timeZone).withTimeAtStartOfDay().minusDays(duration).getMillis();


        long timeZoneEndDateMillis = DateTime.now().getMillis();
        LOG.info(String.format("Today: %d, Start: %d, end: %d -- timezone: %d", DateUtil.nowUTC().getMillis(), timeZoneStartDateMillis, timeZoneEndDateMillis, timeZoneOffset));

        LinkedHashSet<ObjectId> productPrepackageItemIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productIds = new LinkedHashSet<>();
        List<Cart> salesCarts = new ArrayList<>();
        HashMap<String,Transaction> salesTransIds = new HashMap<>();
        List<Transaction> refundTransactions = new ArrayList<>();

        HashMap<String,ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(companyId);

        if (widget) {
            Iterable<ConsumerCart> consumerCarts = consumerCartRepository.getBracketSaleByConsumerId(companyId, consumerUser.getId(), timeZoneStartDateMillis, timeZoneEndDateMillis);
            List<ConsumerCart> consumerUserSales = Lists.newArrayList(consumerCarts);
            if (consumerCart != null) {
                consumerUserSales.add(consumerCart);
            }

            for (ConsumerCart consumerUserSale : consumerUserSales) {
                Cart cart = consumerUserSale.getCart();
                salesCarts.add(cart);
                if (cart != null && cart.getItems() != null) {
                    for (OrderItem orderItem : cart.getItems()) {
                        if (orderItem != null) {
                            if (StringUtils.isNotBlank(orderItem.getProductId()) && StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                productPrepackageItemIds.add(new ObjectId(orderItem.getPrepackageItemId()));
                            }
                            productIds.add(new ObjectId(orderItem.getProductId()));
                        }
                    }
                }
            }

            if (member != null && ObjectId.isValid(member.getId())) {
                Iterable<Transaction> transactions = transactionRepository.getBracketSalesByMemberId(companyId,member.getId(), timeZoneStartDateMillis, timeZoneEndDateMillis);
                List<Transaction> bracketSales = Lists.newArrayList(transactions);

                for (Transaction transaction : bracketSales) {
                    Cart cart = transaction.getCart();
                    if (transaction.getTransType() == Transaction.TransactionType.Sale) {
                        salesTransIds.put(transaction.getId(),transaction);
                        salesCarts.add(cart);
                        if (cart != null && cart.getItems() != null) {
                            for (OrderItem orderItem : cart.getItems()) {
                                if (orderItem != null) {
                                    if (StringUtils.isNotBlank(orderItem.getProductId()) && StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                        productPrepackageItemIds.add(new ObjectId(orderItem.getPrepackageItemId()));
                                    }
                                    productIds.add(new ObjectId(orderItem.getProductId()));
                                }
                            }
                        }
                    } else {
                        refundTransactions.add(transaction);
                    }
                }
            }

            if (consumerUserSales.isEmpty()) {
                return cannabisMap; // consumer user does not exist so we just return true.
            }

        } else {
            Iterable<Transaction> transactions = transactionRepository.getBracketSalesByMemberId(shop.getCompanyId(),  member.getId(), timeZoneStartDateMillis, timeZoneEndDateMillis);
            List<Transaction> bracketSales = Lists.newArrayList(transactions);
            if (newCart != null) {
                Transaction newTrans = new Transaction();
                newTrans.setCart(newCart);
                bracketSales.add(newTrans);
            }

            for (Transaction transaction : bracketSales) {
                Cart cart = transaction.getCart();
                if (transaction.getTransType() == Transaction.TransactionType.Sale) {
                    salesTransIds.put(transaction.getId(),transaction);
                    salesCarts.add(cart);
                    if (cart != null && cart.getItems() != null) {
                        for (OrderItem orderItem : cart.getItems()) {
                            if (orderItem != null) {
                                if (StringUtils.isNotBlank(orderItem.getProductId()) && StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                    productPrepackageItemIds.add(new ObjectId(orderItem.getPrepackageItemId()));
                                }
                                if (StringUtils.isNotBlank(orderItem.getProductId())) {
                                    productIds.add(new ObjectId(orderItem.getProductId()));
                                }
                            }
                        }
                    }
                } else {
                    refundTransactions.add(transaction);
                }
            }

            if (bracketSales.isEmpty()) {
                return cannabisMap; // member does not exist so we just return true.
            }
        }

        // Get valid refund carts to deduct
        List<Cart> refundCarts = new ArrayList<>();
        for (Transaction transaction : refundTransactions) {
            if (StringUtils.isNotBlank(transaction.getParentTransactionId())
                && salesTransIds.containsKey(transaction.getParentTransactionId())) {
                // if it was sold today and refunded, then let's add it to the carts to deduct
                refundCarts.add(transaction.getCart());
            }
        }



        HashMap<String, Product> productHashMap = productRepository.listAsMap(companyId, Lists.newArrayList(productIds));
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(companyId, Lists.newArrayList(productPrepackageItemIds));

        List<ObjectId> prepackageIds = new ArrayList<>();
        for (String prepackageProductItemId : prepackageProductItemHashMap.keySet()) {
            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(prepackageProductItemId);
            if (prepackageProductItem != null) {
                prepackageIds.add(new ObjectId(prepackageProductItem.getPrepackageId()));
            }
        }

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAsMap(companyId, prepackageIds);

        List<ObjectId> toleranceIds = new ArrayList<>();
        for (String prepackageId : prepackageHashMap.keySet()) {
            Prepackage prepackage = prepackageHashMap.get(prepackageId);
            if (prepackage != null && prepackage.getToleranceId() != null && ObjectId.isValid(prepackage.getToleranceId())) {
                toleranceIds.add(new ObjectId(prepackage.getToleranceId()));
            }
        }

        HashMap<String, ProductWeightTolerance> productWeightToleranceHashMap = toleranceRepository.listAsMap(companyId, toleranceIds);


        ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(companyId,
                ProductWeightTolerance.WeightKey.OUNCE);
        ProductWeightTolerance eighthTolerance = toleranceRepository.getToleranceForWeight(companyId,
                ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
        ProductWeightTolerance fourthTolerance = toleranceRepository.getToleranceForWeight(companyId,
                ProductWeightTolerance.WeightKey.QUARTER);

        UserCannabisStats stats = new UserCannabisStats();
        // calculate today's sales
        calculateUsage(false,companyId,stats,salesCarts,productCategoryHashMap,prepackageProductItemHashMap,prepackageHashMap,productWeightToleranceHashMap,productHashMap,ozTolerance,eighthTolerance,fourthTolerance);


        calculateUsage(true,companyId,stats,refundCarts,productCategoryHashMap,prepackageProductItemHashMap,prepackageHashMap,productWeightToleranceHashMap,productHashMap,ozTolerance,eighthTolerance,fourthTolerance);


        if (productHashMap == null) {
            return cannabisMap;
        }

        HashMap<Product.CannabisType, Product.CannabisType> typeMapping = Product.CannabisType.getCannabisMapping();

        for (String productId : stats.quantityMap.keySet()) {
            if (productHashMap.containsKey(productId)) {
                Product product = productHashMap.get(productId);
                if (product == null) {
                    continue;
                }
                Product.CannabisType cannabisType = product.getCannabisType(); //typeMapping.get(product.getCannabisType());

                if (cannabisType != null) {
                    if (cannabisType == Product.CannabisType.DEFAULT) {
                        // check Product Category & use category's cannabis type
                        ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                        if (productCategory != null && productCategory.getCannabisType() != null) {
                            cannabisType = productCategory.getCannabisType();
                        }
                    }

                    // Aggegrate based on cannabis type
                    if (cannabisMap.containsKey(cannabisType)) {
                        BigDecimal updateQuantity = cannabisMap.get(cannabisType).add(stats.quantityMap.get(product.getId()));
                        cannabisMap.put(cannabisType, updateQuantity);
                    } else {
                        cannabisMap.put(cannabisType, stats.quantityMap.get(product.getId()));
                    }
                }
            }
        }
        cannabisMap.put(Product.CannabisType.THC,BigDecimal.valueOf(stats.thcLimit));
        return cannabisMap;
    }

    private void calculateUsage(boolean shouldNegate,
                                String companyId,
                                UserCannabisStats userCannabisStats,
                                List<Cart> carts,
                                HashMap<String,ProductCategory> productCategoryHashMap,
                                HashMap<String,PrepackageProductItem> prepackageProductItemHashMap,
                                HashMap<String,Prepackage> prepackageHashMap,
                                HashMap<String,ProductWeightTolerance> productWeightToleranceHashMap,
                                HashMap<String,Product> productHashMap,
                                ProductWeightTolerance ozTolerance,
                                ProductWeightTolerance eighthTolerance,
                                ProductWeightTolerance fourthTolerance) {

        for (Cart cart : carts) {
            if (cart != null && cart.getItems() != null) {
                for (OrderItem orderItem : cart.getItems()) {
                    double quantity = orderItem.getQuantity().doubleValue();
                    if (StringUtils.isNotBlank(orderItem.getProductId()) && StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        if (prepackageProductItemHashMap.containsKey(orderItem.getPrepackageItemId())) {
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                if (prepackageHashMap.containsKey(prepackageProductItem.getPrepackageId())) {
                                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                    if (prepackage != null) {
                                        double unitValue = prepackage.getUnitValue().doubleValue();
                                        if (unitValue == 0) {
                                            if (productWeightToleranceHashMap.containsKey(prepackage.getToleranceId())) {
                                                ProductWeightTolerance tolerance = productWeightToleranceHashMap.get(prepackage.getToleranceId());
                                                unitValue = tolerance.getUnitValue().doubleValue();
                                            }
                                        }
                                        quantity = quantity * unitValue; // convert to grams

                                        // apply product weight per unit type
                                        BigDecimal quantityInGrams = applyProductWeightPerUnitType(companyId, orderItem, quantity, productHashMap, productCategoryHashMap,ozTolerance, eighthTolerance, fourthTolerance);

                                        quantityInGrams = shouldNegate ? quantityInGrams.negate() : quantityInGrams;

                                        if (productHashMap != null) {
                                            Product product = productHashMap.get(orderItem.getProductId());
                                            if (product != null) {
                                                LOG.info(String.format("Adding %s: %.4f", product.getCannabisType().name(), quantityInGrams.doubleValue()));
                                            }
                                        }
                                        if (userCannabisStats.quantityMap.containsKey(orderItem.getProductId())) {
                                            BigDecimal updatedQuantity = userCannabisStats.quantityMap.get(orderItem.getProductId()).add(quantityInGrams);
                                            userCannabisStats.quantityMap.put(orderItem.getProductId(), updatedQuantity);
                                        } else {
                                            userCannabisStats.quantityMap.put(orderItem.getProductId(), quantityInGrams);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        BigDecimal quantityInGrams = applyProductWeightPerUnitType(companyId, orderItem, quantity, productHashMap,productCategoryHashMap, ozTolerance, eighthTolerance, fourthTolerance);


                        quantityInGrams = shouldNegate ? quantityInGrams.negate() : quantityInGrams;

                        if (userCannabisStats.quantityMap.containsKey(orderItem.getProductId())) {
                            BigDecimal updateQuantity = userCannabisStats.quantityMap.get(orderItem.getProductId()).add(quantityInGrams);
                            userCannabisStats.quantityMap.put(orderItem.getProductId(), updateQuantity);

                        } else {
                            userCannabisStats.quantityMap.put(orderItem.getProductId(), quantityInGrams);
                        }


                        // print
                        if (productHashMap != null) {
                            Product product = productHashMap.get(orderItem.getProductId());
                            if (product != null) {
                                if (product.getPotencyAmount() != null) {
                                    PotencyMG potencyMG = product.getPotencyAmount();
                                    double thc = NumberUtils.round(NumberUtils.valueOrZero(potencyMG.getThc()) * orderItem.getQuantity().doubleValue(), 4);
                                    thc = shouldNegate ? thc * -1 : thc;
                                    userCannabisStats.thcLimit += thc;

                                    LOG.info(String.format("Adding %s: %.4f", Product.CannabisType.THC, thc));
                                }

                                LOG.info(String.format("Adding %s: %.4f", product.getCannabisType().name(), quantityInGrams.doubleValue()));
                            }
                        }
                    }
                }
            }
        }
    }


    class UserCannabisStats {
        HashMap<String, BigDecimal> quantityMap = new HashMap<>();
        double thcLimit = 0;
    }

    @Override
    public HashMap<Product.CannabisType, BigDecimal> prepareMemberDataForCannabis(MemberResult member, HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit) {

        HashMap<Product.CannabisType, BigDecimal> userLimit = new HashMap<>();
        for (Product.CannabisType cannabisType : userCurrentCannabisLimit.keySet()) {

            BigDecimal currentUsage = userCurrentCannabisLimit.get(cannabisType);

            Product.CannabisType cannabisDetail = null;
            if (cannabisType.getType() == 0) {
                cannabisDetail = Product.CannabisType.CONCENTRATE;
            } else if (cannabisType.getType() == 1) {
                cannabisDetail = Product.CannabisType.NON_CONCENTRATE;
            } else if (cannabisType.getType() == 2) {
                cannabisDetail = Product.CannabisType.PLANT;
            } else if (cannabisType.getType() == 3) {
                cannabisDetail = Product.CannabisType.SEEDS;
            } else if (cannabisType.getType() == 4) {
                cannabisDetail = Product.CannabisType.THC;

            } else {
                continue;
            }
            if (userLimit.containsKey(cannabisDetail)) {
                BigDecimal updateQuantity = userLimit.get(cannabisDetail).add(currentUsage);
                updateQuantity = updateQuantity.setScale(2, RoundingMode.HALF_UP);
                userLimit.put(cannabisDetail, updateQuantity);
            } else {
                //currentUsage = NumberUtils.round2(currentUsage,2).setScale(2);
                currentUsage = currentUsage.setScale(2, RoundingMode.HALF_UP);
                userLimit.put(cannabisDetail, currentUsage);
            }
        }
        member.setCurrentCannabisLimit(userLimit);
        return userLimit;
    }
}
