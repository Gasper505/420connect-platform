package com.fourtwenty.core.services.global.impl;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.SplitPayment;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.PaidInOutItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by mdo on 8/5/18.
 */
public class CashDrawerProcessorServiceImpl implements CashDrawerProcessorService {
    @Inject
    private PaidInOutItemRepository paidInOutItemRepository;
    @Inject
    private TransactionRepository transactionRepository;

    @Override
    public void processCurrentCashDrawer(Shop shop, CashDrawerSession dbLogResult, boolean isNew) {
        DateTime cdDate = DateUtil.parseDateKey(dbLogResult.getDate());

        if (shop == null) {
            return;
        }
        String timeZone = shop.getTimeZone();
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

        long timeZoneStartDateMillis = cdDate.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
        DateTime jodaEndDate = cdDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
        long timeZoneEndDateMillis = jodaEndDate.minusMinutes(timeZoneOffset).getMillis();
        // Total Sales
        Iterable<Transaction> transactions = transactionRepository.getBracketSalesForTerminalId(shop.getCompanyId(),
                shop.getId(),
                dbLogResult.getTerminalId(),
                timeZoneStartDateMillis,
                timeZoneEndDateMillis);

        // Sales
        double totalCashSales = 0.0;
        double totalCashlessAtmSales = 0.0;
        double totalCheckSales = 0.0;
        double totalCreditSales = 0.0;
        double totalStoreCreditSales = 0.0;
        HashMap<Cart.PaymentOption, BigDecimal> customSalesTotals = new HashMap<>();

        // Refunds
        double totalRefunds = 0.0;
        double totalCashRefunds = 0.0;
        double totalCashlessAtmRefunds = 0.0;
        double totalCheckRefunds = 0.0;
        double totalCreditRefunds = 0.0;
        double totalStoreCreditRefunds = 0.0;
        double cashlessATMChanged = 0.0;
        HashMap<Cart.PaymentOption, BigDecimal> customRefundTotals = new HashMap<>();

        BigDecimal customTotal = null;
        for (Transaction transaction : transactions) {

            SplitPayment splitPayment = transaction.getCart().getSplitPayment();
            boolean isSale = transaction.getTransType() == Transaction.TransactionType.Sale;
            boolean isNewRefund = transaction.getTransType() == Transaction.TransactionType.Refund &&
                    transaction.getCart().getRefundOption() == Cart.RefundOption.Retail;
            boolean isOldRefund = transaction.getTransType() == Transaction.TransactionType.Refund &&
                    transaction.getCart().getRefundOption() == Cart.RefundOption.Void;

            if (isSale) {
                switch (transaction.getCart().getPaymentOption()) {
                    case Cash:
                        totalCashSales += transaction.getCart().getTotal().doubleValue();
                        break;
                    case Credit:
                        totalCreditSales += transaction.getCart().getTotal().doubleValue();
                        break;
                    case Check:
                        totalCheckSales += transaction.getCart().getTotal().doubleValue();
                        break;
                    case StoreCredit:
                        totalStoreCreditSales += transaction.getCart().getTotal().doubleValue();
                        break;
                    case Split:
                        if (splitPayment != null) {
                            totalCashSales += splitPayment.getCashAmt().doubleValue();
                            totalCreditSales += splitPayment.getCreditDebitAmt().doubleValue();
                            totalCheckSales += splitPayment.getCheckAmt().doubleValue();
                            totalCashlessAtmSales += splitPayment.getCashlessAmt().doubleValue();
                            totalStoreCreditSales += splitPayment.getStoreCreditAmt().doubleValue();

                            // account for change
                            BigDecimal splitChanged = splitPayment.getTotalSplits().subtract(transaction.getCart().getTotal());

                            if (splitChanged.compareTo(new BigDecimal(0)) > 0) {
                                totalCashSales -= splitChanged.doubleValue();
                            }
                        }
                        break;
                    case CashlessATM:
                        totalCashlessAtmSales += transaction.getCart().getTotal().doubleValue();
                        cashlessATMChanged += transaction.getCart().getChangeDue().abs().doubleValue();
                        break;
                    default:
                        customTotal = customSalesTotals
                                .getOrDefault(transaction.getCart().getPaymentOption(), new BigDecimal(0))
                                .add(new BigDecimal(transaction.getCart().getTotal().doubleValue()));
                        customSalesTotals.put(transaction.getCart().getPaymentOption(), customTotal);
                        break;
                }
            } else if (isNewRefund) {

                switch (transaction.getCart().getPaymentOption()) {
                    case Cash:
                        totalCashRefunds += transaction.getCart().getTotal().doubleValue();
                        break;
                    case Credit:
                        totalCreditRefunds += transaction.getCart().getTotal().doubleValue();
                        break;
                    case Check:
                        totalCheckRefunds += transaction.getCart().getTotal().doubleValue();
                        break;
                    case StoreCredit:
                        totalStoreCreditRefunds += transaction.getCart().getTotal().doubleValue();
                        break;
                    case CashlessATM:
                        totalCashlessAtmRefunds += transaction.getCart().getTotal().doubleValue();
                        //totalCashSales -= transaction.getCart().getChangeDue().doubleValue();
                        break;
                    case Split:
                        if (splitPayment != null) {
                            totalCashRefunds += splitPayment.getCashAmt().doubleValue();
                            totalCreditRefunds += splitPayment.getCreditDebitAmt().doubleValue();
                            totalCheckRefunds += splitPayment.getCheckAmt().doubleValue();
                            totalStoreCreditRefunds += splitPayment.getStoreCreditAmt().doubleValue();
                            totalCashlessAtmRefunds += splitPayment.getCashlessAmt().doubleValue();

                            // account for change
                            BigDecimal splitChanged = splitPayment.getTotalSplits().subtract(transaction.getCart().getTotal());
                            if (splitChanged.compareTo(new BigDecimal(0)) > 0) {
                                totalCashRefunds -= splitChanged.doubleValue();
                            }
                        }
                        break;
                    default:
                        customTotal = customRefundTotals
                                .getOrDefault(transaction.getCart().getPaymentOption(), new BigDecimal(0))
                                .add(new BigDecimal(transaction.getCart().getTotal().doubleValue()));
                        customRefundTotals.put(transaction.getCart().getPaymentOption(), customTotal);
                        break;
                }
            } else if (isOldRefund) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    if (item.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                        totalRefunds += item.getFinalPrice().doubleValue();

                        // calculate check cash refunds
                        switch (transaction.getCart().getPaymentOption()) {
                            case Cash:
                                totalCashRefunds += item.getFinalPrice().doubleValue();
                                break;
                            case Check:
                                totalCheckRefunds += item.getFinalPrice().doubleValue();
                                break;
                            case Credit:
                                totalCreditRefunds += item.getFinalPrice().doubleValue();
                                break;
                        }
                    }
                }
            }
        }
        // Get Total Refunds
        double cashIns = 0.0;
        double cashOuts = 0.0;
        double cashDrop = 0.0;
        if (!isNew) {
            Iterable<PaidInOutItem> cashIOLogs = paidInOutItemRepository.getPaidInOuts(shop.getCompanyId(),
                    shop.getId(),
                    dbLogResult.getId());
            for (PaidInOutItem paidInOutItem : cashIOLogs) {
                if (paidInOutItem.getType() == PaidInOutItem.PaidInOutType.PaidIn) {
                    cashIns += paidInOutItem.getAmount().doubleValue();
                } else if (paidInOutItem.getType() == PaidInOutItem.PaidInOutType.PaidOut) {
                    cashOuts += paidInOutItem.getAmount().doubleValue();
                } else {
                    cashDrop += paidInOutItem.getAmount().doubleValue();
                }
            }
        }

        double expectedCash = dbLogResult.getStartCash().doubleValue() + totalCashSales + totalCheckSales  + cashIns - cashlessATMChanged - cashOuts - cashDrop - totalCashRefunds - totalCheckRefunds - totalCashlessAtmRefunds;


        // Sales
        dbLogResult.setCashSales(new BigDecimal(NumberUtils.round(totalCashSales, 2)));
        dbLogResult.setCreditSales(new BigDecimal(NumberUtils.round(totalCreditSales, 2)));
        dbLogResult.setCheckSales(new BigDecimal(NumberUtils.round(totalCheckSales, 2)));
        dbLogResult.setStoreCreditSales(new BigDecimal(NumberUtils.round(totalStoreCreditSales, 2)));
        dbLogResult.setCustomSalesTotals(customSalesTotals);
        dbLogResult.setTotalCashlessAtmSales(new BigDecimal(NumberUtils.round(totalCashlessAtmSales, 2)));
        dbLogResult.setTotalCashlessAtmChange(BigDecimal.valueOf(NumberUtils.round(cashlessATMChanged * -1, 2)));

        // Refunds
        dbLogResult.setCashRefunds(new BigDecimal(NumberUtils.round(totalCashRefunds, 2)));
        dbLogResult.setCreditRefunds(new BigDecimal(NumberUtils.round(totalCreditRefunds, 2)));
        dbLogResult.setCheckRefunds(new BigDecimal(NumberUtils.round(totalCheckRefunds, 2)));
        dbLogResult.setStoreCreditRefunds(new BigDecimal(NumberUtils.round(totalStoreCreditRefunds, 2)));
        dbLogResult.setCustomRefundTotals(customRefundTotals);
        dbLogResult.setTotalCashlessAtmRefunds(BigDecimal.valueOf(totalCashlessAtmRefunds));

        dbLogResult.setTotalRefunds(new BigDecimal(NumberUtils.round(totalRefunds, 2)));


        // Cash specific
        dbLogResult.setExpectedCash(new BigDecimal(NumberUtils.round(expectedCash, 2)));
        dbLogResult.setTotalCashIn(new BigDecimal(NumberUtils.round(cashIns, 2)));
        dbLogResult.setTotalCashOut(new BigDecimal(NumberUtils.round(cashOuts, 2)));
        dbLogResult.setTotalCashDrop(new BigDecimal(NumberUtils.round(cashDrop, 2)));
    }
}
