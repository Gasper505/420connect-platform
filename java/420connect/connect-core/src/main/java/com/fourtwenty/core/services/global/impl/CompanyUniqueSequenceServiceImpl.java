package com.fourtwenty.core.services.global.impl;

import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyUniqueSequenceRepository;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.google.inject.Inject;

public class CompanyUniqueSequenceServiceImpl implements CompanyUniqueSequenceService {

    @Inject
    private CompanyUniqueSequenceRepository companyUniqueSequenceRepository;

    /**
     * Override method to get next sequence number according to given criteria
     *
     * @param companyId : companyId
     * @param criteria  : criteria
     * @param count     : count
     * @return return new sequence count
     */
    @Override
    public long getNewIdentifier(String companyId, String criteria, long count) {
        CompanyUniqueSequence sequence = companyUniqueSequenceRepository.getNewIdentifier(companyId, criteria);

        if (sequence.getCount() <= 0) {
            return getOrUpdateUniqueSequenceCount(companyId, criteria, count, sequence);
        }
        return sequence.getCount();
    }

    private long getOrUpdateUniqueSequenceCount(String companyId, String criteria, long count, CompanyUniqueSequence sequence) {
        /*Check if any record is there for this company*/
        if (count > 0) {
            sequence.setCount(count);
            companyUniqueSequenceRepository.update(companyId, sequence.getId(), sequence);
        }
        sequence = companyUniqueSequenceRepository.getNewIdentifier(companyId, criteria); // get a fresh number
        return sequence.getCount();
    }


}
