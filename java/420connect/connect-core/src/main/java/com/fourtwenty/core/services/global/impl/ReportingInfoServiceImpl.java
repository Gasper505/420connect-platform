package com.fourtwenty.core.services.global.impl;

import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.domain.repositories.global.ReportingInfoRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.global.ReportingInfoService;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class ReportingInfoServiceImpl extends AbstractAuthServiceImpl implements ReportingInfoService {

    private static final String REPORTING_INFO = "Reporting info";
    private static final String REPORTING_INFO_NOT_FOUND = "Reporting info not found";
    private static final String REPORTING_TYPE = "Reporting info type";
    private static final String REPORTING_TYPE_NOT_EMPTY = "Reporting info type can't be empty";

    @Inject
    private ReportingInfoRepository reportingInfoRepository;

    @Inject
    public ReportingInfoServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * This method create reporting info
     *
     * @param reportingInfo : reporting info {@link ReportingInfo}
     */
    @Override
    public ReportingInfo createReportingInfo(ReportingInfo reportingInfo) {
        if (reportingInfo.getReportingInfoType() == null) {
            throw new BlazeInvalidArgException(REPORTING_TYPE, REPORTING_TYPE_NOT_EMPTY);
        }
        reportingInfo.prepare();
        return reportingInfoRepository.save(reportingInfo);
    }

    /**
     * This method fetch list of reporting info
     *
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<ReportingInfo> getReportingInfo(int start, int limit) {
        return reportingInfoRepository.findItems(start, limit);
    }

    /**
     * Update reporting info type
     *
     * @param reportingInfoId : reporting info id
     * @param reportingInfo   : @link {@link ReportingInfo}
     */
    @Override
    public ReportingInfo updateReportingInfo(String reportingInfoId, ReportingInfo reportingInfo) {

        ReportingInfo dbReportingInfo = reportingInfoRepository.getById(reportingInfoId);
        if (dbReportingInfo == null) {
            throw new BlazeInvalidArgException(REPORTING_INFO, REPORTING_INFO_NOT_FOUND);
        }

        if (reportingInfo.getReportingInfoType() == null) {
            throw new BlazeInvalidArgException(REPORTING_TYPE, REPORTING_TYPE_NOT_EMPTY);
        }

        dbReportingInfo.prepare();
        dbReportingInfo.setActive(reportingInfo.getActive());
        dbReportingInfo.setLastSync(reportingInfo.getLastSync());
        dbReportingInfo.setReportingInfoType(reportingInfo.getReportingInfoType());
        dbReportingInfo.setSubReportTypes(reportingInfo.getSubReportTypes());

        return reportingInfoRepository.update(reportingInfoId, reportingInfo);
    }

    /**
     * Get reporting info by id
     *
     * @param reportingInfoId : id of reporting info
     * @return @link {@link ReportingInfo}
     */
    @Override
    public ReportingInfo getReportingInfoById(String reportingInfoId) {
        ReportingInfo reportingInfo = reportingInfoRepository.getById(reportingInfoId);

        if (reportingInfo == null) {
            throw new BlazeInvalidArgException(REPORTING_INFO, REPORTING_INFO_NOT_FOUND);
        }

        return reportingInfo;
    }
}
