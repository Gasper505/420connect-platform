package com.fourtwenty.core.services.imaging;

import com.amazonaws.services.s3.model.ObjectMetadata;

import java.io.File;
import java.io.InputStream;

/**
 * Created by mdo on 10/26/17.
 */
public interface ImageProcessorService {
    class ImageProcessingResult {
        public ImageStreamResult origURL;
        public ImageStreamResult largeX2;
        public ImageStreamResult large;
        public ImageStreamResult medium;
        public ImageStreamResult small;
    }


    class ImageStreamResult {
        public InputStream inputStream;
        public ObjectMetadata objectMetadata;
        public String size;
    }

    ImageProcessingResult processImage(File file, String fileName);

}
