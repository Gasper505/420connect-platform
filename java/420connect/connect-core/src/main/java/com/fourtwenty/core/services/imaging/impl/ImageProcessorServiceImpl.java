package com.fourtwenty.core.services.imaging.impl;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.fourtwenty.core.services.imaging.ImageProcessorService;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;

/**
 * Created by mdo on 10/26/17.
 */
public class ImageProcessorServiceImpl implements ImageProcessorService {
    private static final Log LOG = LogFactory.getLog(ImageProcessorServiceImpl.class);


    @Override
    public ImageProcessingResult processImage(File file, String fileName) {

        try {
            ImageProcessingResult result = new ImageProcessingResult();
            result.largeX2 = scaleImage(file, 1600, 1600, fileName);
            result.large = scaleImage(file, 800, 800, fileName);
            result.medium = scaleImage(file, 500, 500, fileName);
            result.small = scaleImage(file, 200, 200, fileName);
            return result;

        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("Error processing images: " + fileName, e);
            return null;
        }
    }

    public ImageStreamResult scaleImage(File file, int width, int height, String fileName) throws IOException {

        String ext2 = FilenameUtils.getExtension(fileName); // returns "exe"

        String lowerExt = ext2.toLowerCase();
        String format = "jpeg";
        if (lowerExt.contains("png")) {
            format = "png";
        }


        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Thumbnails.of(file)
                .size(width, height)
                .outputQuality(.90)
                .outputFormat(format)
                .toOutputStream(os);

        byte[] buffer = os.toByteArray();

        if (buffer.length > 0) {
            InputStream is = new ByteArrayInputStream(buffer);
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentLength(buffer.length);

            if (lowerExt.contains("jpg") || lowerExt.contentEquals("jpeg")) {
                meta.setContentType("image/jpeg");
            } else if (lowerExt.contains("png")) {
                meta.setContentType("image/png");
            }
            ImageStreamResult result = new ImageStreamResult();
            result.inputStream = is;
            result.objectMetadata = meta;
            result.size = String.format("%dx%d", width, height);
            return result;
        }
        return null;
    }

}
