package com.fourtwenty.core.services.inventory;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchByCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchQuantityResult;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by mdo on 11/1/17.
 */
public interface BatchQuantityService {
    DateSearchResult<BatchQuantity> retrieveBatchQuantities(String companyId, String shopId, long afterDate, long beforeDate);

    <E extends BatchQuantity> SearchResult<E> getBatchQuantitiesForProduct(String companyId, String shopId, String productId, String inventoryId, Class<E> clazz, int start, int limit);

    BatchQuantity addBatchQuantity(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToAdd);

    void subtractBatchQuantity(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToSubtract);


    List<QuantityLog> fakeSubtractBatchQuantity(String companyId, String shopId, String productId, String inventoryId,
                                                String batchId, BigDecimal quantityToSubtract, String orderItemId);

    SearchResult<BatchQuantityResult> getBatchQuantitiesForInventoryAndBatch(String companyId, String shopId, String inventoryId, List<String> batchIdList, String productId, int start, int limit);

    List<BatchByCategoryResult> getBatchQuantitiesForProduct(String companyId, String shopId, String inventoryId, List<String> productId);

}
