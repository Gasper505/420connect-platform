package com.fourtwenty.core.services.inventory;

import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferStatusRequest;

public interface CommonInventoryService {
    InventoryTransferHistory bulkInventoryTransfer(String companyId, String shopId, String currentEmployeeId, InventoryTransferHistory request);

    InventoryTransferHistory updateInventoryTransferHistoryStatus(String historyId, String companyId, String shopId, String currentEmployeeId, InventoryTransferStatusRequest request);

    InventoryTransferHistory updateInventoryTransfer(String historyId, String companyId, String shopId, String currentEmployeeId, InventoryTransferHistory request);
}
