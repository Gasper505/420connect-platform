package com.fourtwenty.core.services.inventory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.serializers.TruncateSixDigitSerialzer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class InventoryActionLog {

    private String productName;
    private Product.CannabisType cannabisType = Product.CannabisType.DEFAULT;
    private Product.WeightPerUnit weightPerUnit;
    private String brandName;
    private String batchSku;
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal price;

    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal committedQuantity = BigDecimal.ZERO;
    @JsonSerialize(using = TruncateSixDigitSerialzer.class)
    @DecimalMin("0")
    private BigDecimal deliveredQuantity = BigDecimal.ZERO;



    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Product.CannabisType getCannabisType() {
        return cannabisType;
    }

    public void setCannabisType(Product.CannabisType cannabisType) {
        this.cannabisType = cannabisType;
    }

    public Product.WeightPerUnit getWeightPerUnit() {
        return weightPerUnit;
    }

    public void setWeightPerUnit(Product.WeightPerUnit weightPerUnit) {
        this.weightPerUnit = weightPerUnit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }


    public BigDecimal getCommittedQuantity() {
        return committedQuantity;
    }

    public void setCommittedQuantity(BigDecimal committedQuantity) {
        this.committedQuantity = committedQuantity;
    }

    public BigDecimal getDeliveredQuantity() {
        return deliveredQuantity;
    }

    public void setDeliveredQuantity(BigDecimal deliveredQuantity) {
        this.deliveredQuantity = deliveredQuantity;
    }

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }

}
