package com.fourtwenty.core.services.inventory;

import com.fourtwenty.core.domain.models.product.InventoryOperation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 9/11/18.
 */
public interface InventoryOperationService {
    InventoryOperationResult revertRecentForDerivedBatch(final String companyId,
                                                         final String shopId,
                                                         final String productId,
                                                         final String prepackageItemId,
                                                         final String batchId,
                                                         final String inventoryId,
                                                         final BigDecimal quantity,
                                                         final String employeeId,
                                                         final InventoryOperation.SourceType sourceType,
                                                         final String sourceId,
                                                         final String sourceChildId,
                                                         String request,
                                                         InventoryOperation.SubSourceAction subSourceAction);

    class InventoryOperationResult {
        public List<InventoryOperation> operations = new ArrayList<InventoryOperation>();
        public BigDecimal resultTotal = new BigDecimal(0);
    }

    InventoryOperationResult getRecent(final String companyId,
                                       final String shopId,
                                       final String sourceId);
    InventoryOperation getRecentOperation(final String companyId,
                                       final String shopId,
                                       final String sourceId);


    InventoryOperation getRecentOperation(final String companyId,
                                          final String shopId,
                                          final String sourceId,
                                          final String sourceChildId);

    Iterable<InventoryOperation> getAllOperationsFor(final String companyId,
                                          final String shopId,
                                          final String sourceChildId,
                                          final String requestId);


    InventoryOperation getRecentOperation(String companyId, String shopId, String productId, String batchId, String inventoryId, String prepackageItemId);

    InventoryOperationResult adjust(final String companyId,
                                    final String shopId,
                                    final String productId,
                                    final String prepackageItemId,
                                    final String batchId,
                                    final String inventoryId,
                                    final BigDecimal quantity,
                                    final String employeeId,
                                    final InventoryOperation.SourceType sourceType,
                                    final String sourceId,
                                    final String sourceChildId,
                                    String request,
                                    InventoryOperation.SubSourceAction subSourceAction);




    InventoryOperationResult revertRecent(String companyId, String shopId, InventoryOperation.SourceType sourceType, String sourceId);

}
