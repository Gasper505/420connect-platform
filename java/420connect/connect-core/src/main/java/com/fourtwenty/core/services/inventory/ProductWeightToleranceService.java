package com.fourtwenty.core.services.inventory;

import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductWeightToleranceAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ToleranceEnableRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface ProductWeightToleranceService {
    SearchResult<ProductWeightTolerance> getWeightTolerances();

    DateSearchResult<ProductWeightTolerance> getWeightTolerances(long afterDate, long beforeDate);

    ProductWeightTolerance addWeightTolerance(ProductWeightToleranceAddRequest tolerance);

    ProductWeightTolerance updateWeightTolerance(String weightToleranceId, ProductWeightTolerance tolerance);

    ProductWeightTolerance getWeightToleranceById(String toleranceId);

    void deleteWeightTolerance(String weightToleranceId);

    ProductWeightTolerance enableWeightTolerance(String weightToleranceId, ToleranceEnableRequest request);
}
