package com.fourtwenty.core.services.inventory.impl;

import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchByCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchQuantityResult;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 11/1/17.
 * <p>
 * This class is session agnostic and requires user to pass in companyId and shopId
 */
public class BatchQuantityServiceImpl implements BatchQuantityService {
    // LOG
    private static final Logger LOG = LoggerFactory.getLogger(BatchQuantityServiceImpl.class);
    @Inject
    BatchQuantityRepository batchQuantityRepository;
    @Inject
    ProductBatchRepository productBatchRepository;

    @Override
    public <E extends BatchQuantity> SearchResult<E> getBatchQuantitiesForProduct(final String companyId, final String shopId,
                                                                                  final String productId, final String inventoryId, Class<E> clazz, int start, int limit) {

        return batchQuantityRepository.getBatchQuantitiesForInventory(companyId, shopId, productId, inventoryId, clazz, start, limit);
    }

    @Override
    public DateSearchResult<BatchQuantity> retrieveBatchQuantities(String companyId, String shopId, long afterDate, long beforeDate) {
        if (beforeDate == 0) {
            beforeDate = DateTime.now().getMillis();
        }
        DateSearchResult<BatchQuantity> results = batchQuantityRepository.findItemsWithDate(companyId, shopId, afterDate, beforeDate);
        return results;
    }


    @Override
    public BatchQuantity addBatchQuantity(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToAdd) {
        String targetBatchId = batchId;
        long purchasedDate = 0;

        // if batchId is not specified, then get the latest batch
        if (StringUtils.isBlank(batchId)) {
            ProductBatch batch = productBatchRepository.getLatestBatchForProduct(companyId, shopId, productId);
            if (batch != null) {
                targetBatchId = batch.getId();
                purchasedDate = batch.getPurchasedDate();
            }
        } else {
            ProductBatch productBatch = productBatchRepository.get(companyId, targetBatchId);
            if (productBatch != null) {
                purchasedDate = productBatch.getPurchasedDate();
            }
        }


        BatchQuantity dbBatchQuantity = batchQuantityRepository.getBatchQuantityForInventory(companyId, shopId, productId, inventoryId, targetBatchId);

        if (quantityToAdd != null && quantityToAdd.doubleValue() > 0) {
            if (dbBatchQuantity != null) {
                BigDecimal newAmt = dbBatchQuantity.getQuantity().add(quantityToAdd);
                dbBatchQuantity.setQuantity(newAmt);
                dbBatchQuantity.setBatchPurchaseDate(purchasedDate);
                batchQuantityRepository.update(companyId, dbBatchQuantity.getId(), dbBatchQuantity);

            } else {

                // batch doesn't exist so let's just add a new batch quantity
                dbBatchQuantity = new BatchQuantity();
                dbBatchQuantity.prepare(companyId);
                dbBatchQuantity.setShopId(shopId);
                dbBatchQuantity.setBatchId(targetBatchId);
                dbBatchQuantity.setProductId(productId);
                dbBatchQuantity.setQuantity(quantityToAdd);
                dbBatchQuantity.setInventoryId(inventoryId);
                dbBatchQuantity.setBatchPurchaseDate(purchasedDate);
                batchQuantityRepository.save(dbBatchQuantity);
            }
        }

        return dbBatchQuantity;
    }

    @Override
    public void subtractBatchQuantity(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToSubtract) {

        if (StringUtils.isNotBlank(batchId)) {
            BatchQuantity dbBatchQuantity = batchQuantityRepository.getBatchQuantityForInventory(companyId, shopId, productId, inventoryId, batchId);
            if (dbBatchQuantity != null) {
                if (quantityToSubtract != null && quantityToSubtract.doubleValue() > 0) {
                    BigDecimal newAmt = dbBatchQuantity.getQuantity().subtract(quantityToSubtract);
                    if (newAmt.doubleValue() < 0) {
                        newAmt = new BigDecimal(0);
                    }
                    dbBatchQuantity.setQuantity(newAmt);
                    batchQuantityRepository.update(companyId, dbBatchQuantity.getId(), dbBatchQuantity);
                }
            }
        } else {
            // batchId not specify, so subtract from oldest to newest
            double curQty = quantityToSubtract.doubleValue();
            Iterable<BatchQuantity> sortedBatches = batchQuantityRepository.getBatchQuantitiesForInventorySorted(companyId, shopId, productId, inventoryId, "{batchPurchaseDate:-1}");
            for (BatchQuantity batchQuantity : sortedBatches) {
                if (batchQuantity.getQuantity().doubleValue() > 0 && curQty > 0) {
                    double leftOver = curQty - batchQuantity.getQuantity().doubleValue();
                    if (leftOver <= 0) {
                        batchQuantity.setQuantity(new BigDecimal(batchQuantity.getQuantity().doubleValue() - curQty));
                        // we are done. so break from loop
                        curQty = 0;
                    } else {
                        batchQuantity.setQuantity(new BigDecimal(0));
                        curQty = NumberUtils.round(curQty, 2);
                    }

                    batchQuantityRepository.update(companyId, batchQuantity.getId(), batchQuantity);
                    // save the batch
                }
            }

        }

    }


    @Override
    public List<QuantityLog> fakeSubtractBatchQuantity(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToSubtract, String orderItemId) {
        List<QuantityLog> quantityLogs = new ArrayList<>();
        if (StringUtils.isNotBlank(batchId)) {
            BatchQuantity dbBatchQuantity = batchQuantityRepository.getBatchQuantityForInventory(companyId, shopId, productId, inventoryId, batchId);
            if (dbBatchQuantity != null) {
                if (quantityToSubtract != null
                        && quantityToSubtract.doubleValue() > 0
                        && quantityToSubtract.doubleValue() <= dbBatchQuantity.getQuantity().doubleValue()) {
                    BigDecimal newAmt = dbBatchQuantity.getQuantity().subtract(quantityToSubtract);
                    dbBatchQuantity.setQuantity(newAmt);

                    QuantityLog quantityLog = addQuantityLog(orderItemId, inventoryId, batchId, quantityToSubtract);
                    quantityLogs.add(quantityLog);
                }
            }
        }
        if (quantityLogs.isEmpty()) {
            // batchId not specify, so subtract from oldest to newest
            double curQty = quantityToSubtract.doubleValue();
            Iterable<BatchQuantity> sortedBatches = batchQuantityRepository.getBatchQuantitiesForInventorySorted(companyId, shopId, productId, inventoryId, "{batchPurchaseDate:1}");
            for (BatchQuantity batchQuantity : sortedBatches) {
                if (StringUtils.isNotBlank(batchQuantity.getBatchId())
                        && batchQuantity.getQuantity().doubleValue() > 0 && curQty > 0) {
                    double leftOver = batchQuantity.getQuantity().doubleValue() - curQty;

                    if (leftOver >= 0) {
                        // this means there were enough amt in the batch
                        batchQuantity.setQuantity(BigDecimal.valueOf(leftOver)); //assign left over to batchQuantity

                        QuantityLog quantityLog = addQuantityLog(orderItemId, batchQuantity.getInventoryId(), batchQuantity.getBatchId(), BigDecimal.valueOf(curQty));
                        quantityLogs.add(quantityLog);

                        // set to 0
                        curQty = 0;

                    } else {
                        // less, so batchQuantity didn't have enough, let's continue to loop
                        curQty = NumberUtils.round(Math.abs(leftOver), 2);

                        QuantityLog quantityLog = addQuantityLog(orderItemId, batchQuantity.getInventoryId(),
                                batchQuantity.getBatchId(), batchQuantity.getQuantity());
                        quantityLogs.add(quantityLog);

                        batchQuantity.setQuantity(new BigDecimal(0)); //assign 0 over to batchQuantity
                    }

                }
            }
        }

        // TO BE SAFE! WE NEED TO MAKE SURE THINGS ARE DEDUCTED AT THE BAG LEVEL AT LEAST!!!!!
        double totalDeducted = 0;
        for (QuantityLog quantityLog : quantityLogs) {
            totalDeducted += quantityLog.getQuantity().doubleValue();
        }

        totalDeducted = NumberUtils.round(totalDeducted, 2);
        if (totalDeducted < NumberUtils.round(quantityToSubtract.doubleValue(), 2)) {
            quantityLogs.clear();

            QuantityLog quantityLog = addQuantityLog(orderItemId, inventoryId, batchId, quantityToSubtract);
            quantityLogs.add(quantityLog);
        }

        return quantityLogs;
    }

    /**
     * Get batch quantities by inventory id and batch ids
     *
     * @param companyId   : company id
     * @param shopId      : shop id
     * @param inventoryId : inventory id
     * @param batchIdList : batch id list
     * @param productId
     * @param start
     * @param limit
     */
    @Override
    public SearchResult<BatchQuantityResult> getBatchQuantitiesForInventoryAndBatch(String companyId, String shopId, String inventoryId, List<String> batchIdList, String productId, int start, int limit) {
        return batchQuantityRepository.getBatchQuantitiesForInventoryAndBatch(companyId, shopId, inventoryId, batchIdList, productId, start, limit);
    }

    public QuantityLog addQuantityLog(String orderItemId, String inventoryId, String batchId, BigDecimal quantity) {
        QuantityLog quantityLog = new QuantityLog();
        quantityLog.setId(String.format("%s_%s_%s", orderItemId, inventoryId, batchId));
        quantityLog.setInventoryId(inventoryId);
        quantityLog.setBatchId(batchId);
        quantityLog.setQuantity(quantity);
        return quantityLog;
    }

    @Override
    public List<BatchByCategoryResult> getBatchQuantitiesForProduct(String companyId, String shopId, String inventoryId, List<String> productIds) {
        return batchQuantityRepository.getBatchQuantitiesForInventory(companyId, shopId, inventoryId, productIds);
    }
}
