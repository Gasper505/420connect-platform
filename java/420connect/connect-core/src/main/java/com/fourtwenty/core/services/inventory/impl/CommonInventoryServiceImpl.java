package com.fourtwenty.core.services.inventory.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkInventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferStatusRequest;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.CommonInventoryService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class CommonInventoryServiceImpl implements CommonInventoryService {

    private static final String TRANSFER_REQUEST = "Transfer Request";
    private static final String TRANSFER_REQUEST_NOT_FOUND = "Please add inventory to transfer";
    private static final String INVENTORY_TRANSFER_HISTORY = "Inventory transfer history";
    private static final String INVENTORY_TRANSFER_NOT_FOUND = "Inventory transfer history does not exist.";

    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private InventoryTransferHistoryRepository transferHistoryRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private ProductBatchRepository productBatchRepository;
    
    @Override
    public InventoryTransferHistory bulkInventoryTransfer(String companyId, String shopId, String currentEmployeeId, InventoryTransferHistory request) {
        if (request.getTransferLogs().size() == 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, TRANSFER_REQUEST_NOT_FOUND);
        }

        String fromShopId = StringUtils.isBlank(request.getFromShopId()) ? shopId : request.getFromShopId();
        String toShopId = StringUtils.isBlank(request.getToShopId()) ? shopId : request.getToShopId();

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(companyId);

        if (!inventoryHashMap.containsKey(request.getFromInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'from' inventory.");
        }
        if (!inventoryHashMap.containsKey(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'to' inventory.");
        }
        if (request.getFromInventoryId().equalsIgnoreCase(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Cannot transfer within the same inventory.");
        }

        List<ObjectId> productObjectIds = new ArrayList<>();
        for (InventoryTransferLog log : request.getTransferLogs()) {
            productObjectIds.add(new ObjectId(log.getProductId()));
        }
        HashMap<String, Product> productMap = productRepository.findItemsInAsMap(companyId, fromShopId, productObjectIds);
        request.setToShopId(toShopId);
        request.setFromShopId(fromShopId);

        this.checkInventoryForTransfer(companyId, shopId, request, productMap);

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(companyId, fromShopId, "InventoryTransfer");
        String transferNo = "" + sequence.getCount();

        InventoryTransferHistory transferHistory = new InventoryTransferHistory();
        transferHistory.prepare(companyId);
        transferHistory.setShopId(shopId);
        transferHistory.setCreateByEmployeeId(currentEmployeeId);
        transferHistory.setStatus(InventoryTransferHistory.TransferStatus.PENDING);
        transferHistory.setFromShopId(fromShopId);
        transferHistory.setToShopId(toShopId);
        transferHistory.setFromInventoryId(request.getFromInventoryId());
        transferHistory.setToInventoryId(request.getToInventoryId());
        transferHistory.setTransferNo(transferNo);
        transferHistory.setTransferByBatch(request.isTransferByBatch());

        processInventoryTransferForNewProducts(companyId, request, transferHistory);

        InventoryTransferHistory updatedTransferHistory = transferHistoryRepository.save(transferHistory);
        if (request.isCompleteTransfer()) {
            InventoryTransferStatusRequest statusRequest = new InventoryTransferStatusRequest();
            statusRequest.setStatus(Boolean.TRUE);

            this.updateInventoryTransferHistoryStatus(updatedTransferHistory.getId(), companyId, shopId, currentEmployeeId, statusRequest);
        } else {
            this.createQueueTransactionJobForTransfer(companyId, shopId, transferHistory);
        }
        return updatedTransferHistory;
    }

    @Override
    public InventoryTransferHistory updateInventoryTransferHistoryStatus(String historyId, String companyId, String shopId, String currentEmployeeId, InventoryTransferStatusRequest statusRequest) {

        InventoryTransferHistory dbTransferHistory = transferHistoryRepository.get(companyId, historyId);
        if (dbTransferHistory == null) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, INVENTORY_TRANSFER_NOT_FOUND);
        }

        if (statusRequest.isStatus()) {
            dbTransferHistory.setCompleteTransfer(true);
        }

        if (dbTransferHistory.getStatus() != InventoryTransferHistory.TransferStatus.PENDING) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Inventory history can't be updated");
        }


        Employee employee = employeeRepository.get(companyId, currentEmployeeId);
        checkEmployeeAccessibility(companyId, employee);

        if (!statusRequest.isStatus()) {
            //If declined then update status
            dbTransferHistory.setStatus(InventoryTransferHistory.TransferStatus.DECLINED);
            dbTransferHistory.setDeclineByEmployeeId(currentEmployeeId);
            dbTransferHistory.setDeclinedDate(DateTime.now().getMillis());
            this.createQueueTransactionJobForTransfer(companyId, shopId, dbTransferHistory);

        } else {
            String fromShopId = dbTransferHistory.getFromShopId();
            String toShopId = dbTransferHistory.getToShopId();
            dbTransferHistory.setAcceptByEmployeeId(currentEmployeeId);
            dbTransferHistory.setAcceptedDate(DateTime.now().getMillis());

            List<ObjectId> objectIds = new ArrayList<>();

            //Products with batch ids
            List<String> productsWithBatchIds = new ArrayList<>();
            //Products without batch ids (If product does not have batch id then use their latest batch id)
            List<String> productsWithoutBatchIds = new ArrayList<>();
            //From batch ids
            List<ObjectId> fromBatchIds = new ArrayList<>();
            for (InventoryTransferLog log : dbTransferHistory.getTransferLogs()) {
                objectIds.add(new ObjectId(log.getProductId()));
                log.setPrevTransferAmt(log.getTransferAmount());
                if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                    productsWithBatchIds.add(log.getProductId());
                    fromBatchIds.add(new ObjectId(log.getFromBatchId()));
                } else {
                    productsWithoutBatchIds.add(log.getProductId());
                }
            }

            //<Product is, from product batch>
            Map<String, ProductBatch> fromProductBatchMap = new HashMap<>();
            HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(companyId, fromBatchIds);
            for (String batch : batchHashMap.keySet()) {
                ProductBatch productBatch = batchHashMap.get(batch);
                fromProductBatchMap.putIfAbsent(productBatch.getProductId(), productBatch);
            }

            //<Product is, latest product batch>
            Map<String, ProductBatch> latestProductBatchMap = productBatchRepository.getLatestBatchForProductList(companyId, shopId, productsWithoutBatchIds);

            //Create map by combining from batch map and latest product batch map
            Map<String, ProductBatch> productBatchMap = new HashMap<>();
            productBatchMap.putAll(fromProductBatchMap);
            productBatchMap.putAll(latestProductBatchMap);


            //Get products that needs to transfer
            Iterable<Product> dbProducts = productRepository.findItemsIn(companyId, fromShopId, objectIds);
            HashMap<String, Product> fromShopProductMap = new HashMap<>();
            for (Product p : dbProducts) {
                fromShopProductMap.put(p.getId(), p);
            }

            processInventoryTransfer(companyId, shopId, currentEmployeeId,dbTransferHistory, fromShopId, toShopId, fromShopProductMap);
            this.createQueueTransactionJobForTransfer(companyId, shopId, dbTransferHistory);

        }

        return transferHistoryRepository.update(companyId, historyId, dbTransferHistory);
    }

    @Override
    public InventoryTransferHistory updateInventoryTransfer(String historyId, String companyId, String shopId, String currentEmployeeId, InventoryTransferHistory transferHistory) {
        InventoryTransferHistory dbInventoryTransferHistory = transferHistoryRepository.get(companyId, historyId);
        if (dbInventoryTransferHistory == null) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, INVENTORY_TRANSFER_NOT_FOUND);
        }

        if (dbInventoryTransferHistory.getStatus() != InventoryTransferHistory.TransferStatus.PENDING) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Inventory history can't be update");
        }

        String fromShopId = StringUtils.isBlank(transferHistory.getFromShopId()) ? shopId : transferHistory.getFromShopId();
        String toShopId = StringUtils.isBlank(transferHistory.getToShopId()) ? shopId : transferHistory.getToShopId();

        if (transferHistory.getTransferLogs().size() == 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, TRANSFER_REQUEST_NOT_FOUND);
        }

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(companyId);

        if (!inventoryHashMap.containsKey(transferHistory.getFromInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'from' inventory.");
        }
        if (!inventoryHashMap.containsKey(transferHistory.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'to' inventory.");
        }
        if (transferHistory.getFromInventoryId().equalsIgnoreCase(transferHistory.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Cannot transfer within the same inventory.");
        }

        dbInventoryTransferHistory.setFromShopId(fromShopId);
        dbInventoryTransferHistory.setToShopId(toShopId);
        dbInventoryTransferHistory.setFromInventoryId(transferHistory.getFromInventoryId());
        dbInventoryTransferHistory.setToInventoryId(transferHistory.getToInventoryId());
        dbInventoryTransferHistory.setTransferByBatch(transferHistory.isTransferByBatch());

        processInventoryTransferForNewProducts(companyId, transferHistory, dbInventoryTransferHistory);

        createQueueTransactionJobForTransfer(companyId, shopId, dbInventoryTransferHistory);

        return transferHistoryRepository.update(companyId, historyId, dbInventoryTransferHistory);
    }

    private void processInventoryTransferForNewProducts(String companyId, InventoryTransferHistory request, InventoryTransferHistory transferHistory) {

        LinkedHashSet<InventoryTransferLog> logList = new LinkedHashSet<>();

        List<ObjectId> prepackageItemIds = new ArrayList<>();
        for (InventoryTransferLog log : request.getTransferLogs()) {

            if (StringUtils.isBlank(log.getProductId()) || !ObjectId.isValid(log.getProductId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, log.getProductId() + " is an invalid id.");
            } else if (log.getTransferAmount().doubleValue() <= 0) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Transfer amount must be greater than 0.");
            }

            logList.add(log);
            if (StringUtils.isNotBlank(log.getPrepackageItemId()) && ObjectId.isValid(log.getPrepackageItemId())) {
                prepackageItemIds.add(new ObjectId(log.getPrepackageItemId()));
            }
        }

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(companyId);
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(companyId, prepackageItemIds);

        for (InventoryTransferLog log : logList) {
            if (prepackageProductItemHashMap.containsKey(log.getPrepackageItemId())) {
                PrepackageProductItem item = prepackageProductItemHashMap.get(log.getPrepackageItemId());
                log.setFromBatchId(item.getBatchId());

                Prepackage prepackage = prepackageHashMap.get(item.getPrepackageId());
                if (prepackage != null) {
                    log.setPrepackageName(prepackage.getName());
                }
            }
        }

        transferHistory.setTransferLogs(logList);

    }
    private void processInventoryTransfer(String companyId, String shopId, String currentEmployeeId, InventoryTransferHistory dbTransferHistory, String fromShopId, String toShopId, HashMap<String, Product> fromShopProductMap) {
        HashMap<String, Product> otherProductHashMap = new HashMap<>();
        final HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(companyId);
        boolean isTransferToAnotherShop = false;

        Cart cart = new Cart();
        cart.prepare(companyId);

        // Check if inventory is being transferred to another shop
        if (StringUtils.isNotBlank(toShopId)
                && !fromShopId.equalsIgnoreCase(toShopId)) {
            isTransferToAnotherShop = true;
            // Find products of shop in which inventory needs to be transfer
            Iterable<Product> otherShopProducts = productRepository.listAllByShop(companyId, toShopId);
            // Create product map with name_vendorId as key
            for (Product product : otherShopProducts) {
                product.setCategory(productCategoryHashMap.get(product.getCategoryId()));
                String key = product.getCompanyLinkId();
                otherProductHashMap.put(key, product);
            }
        }

        List<Product> pendingProducts = new ArrayList<>();

        List<InventoryTransferRequest> transferRequests = new ArrayList<>();

        BulkInventoryTransferRequest bulkInventoryTransferRequest = new BulkInventoryTransferRequest();
        bulkInventoryTransferRequest.setFromShopId(fromShopId);
        bulkInventoryTransferRequest.setToShopId(toShopId);
        bulkInventoryTransferRequest.setTransfers(transferRequests);

        for (InventoryTransferLog log : dbTransferHistory.getTransferLogs()) {
            log.setPrevTransferAmt(log.getTransferAmount());
            InventoryTransferRequest transferRequest = new InventoryTransferRequest();

            transferRequest.setFromInventoryId(dbTransferHistory.getFromInventoryId());
            transferRequest.setToInventoryId(dbTransferHistory.getToInventoryId());
            transferRequest.setFromBatchId(log.getFromBatchId());
            transferRequest.setToBatchId(log.getToBatchId());
            transferRequest.setProductId(log.getProductId());
            transferRequest.setPrepackageItemId(log.getPrepackageItemId());
            transferRequest.setTransferAmount(log.getTransferAmount());

            transferRequests.add(transferRequest);

            Product dbProduct = fromShopProductMap.get(log.getProductId());
            if (dbProduct == null) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Product does not exist: " + log.getProductId());
            }

            // Check for same product
            if (isTransferToAnotherShop) {
                dbProduct.setCategory(productCategoryHashMap.get(dbProduct.getCategoryId()));
            }

            OrderItem order = new OrderItem();
            order.prepare(companyId);
            order.setStatus(OrderItem.OrderItemStatus.Active);
            order.setCost(new BigDecimal(0));
            order.setQuantity(log.getTransferAmount());
            order.setFinalPrice(new BigDecimal(0));
            order.setProductId(dbProduct.getId());
            order.setPrepackageItemId(log.getPrepackageItemId());
            cart.getItems().add(order);
        }

        // Create Transaction Transfers
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(companyId, fromShopId, "TransactionPriority");
        String transNo = "" + sequence.getCount();
        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
        for (Product p : pendingProducts) {
            List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(companyId, shopId, p.getId());
            productChangeLogs.add(productChangeLogRepository.createNewChangeLog(currentEmployeeId,
                    p, productPrepackageQuantities, String.format("Trans #%s: Transfer, tr:%s", transNo, dbTransferHistory.getTransferNo())));
            productRepository.update(companyId, p.getId(), p);
        }
        productChangeLogRepository.save(productChangeLogs);


        Transaction trans = new Transaction();
        trans.setActive(false);
        trans.setTransNo(transNo);
        trans.setMemberId(null);
        trans.setCompanyId(companyId);
        trans.setShopId(fromShopId);
        trans.setQueueType(Transaction.QueueType.None);
        trans.setStatus(Transaction.TransactionStatus.Void);
        trans.setTerminalId(null);
        trans.setMemberId(null);
        trans.setCheckinTime(DateTime.now().getMillis());
        trans.setSellerId(currentEmployeeId);
        trans.setCreatedById(currentEmployeeId);
        trans.setPriority(sequence.getCount());
        //Add transfer history
        trans.setTransferRequest(bulkInventoryTransferRequest);
        trans.setProcessedTime(DateTime.now().getMillis());
        trans.setCompletedTime(trans.getProcessedTime());
        trans.setStartTime(DateTime.now().getMillis());
        trans.setEndTime(DateTime.now().getMillis());

        // Set cart
        cart.setPaymentOption(Cart.PaymentOption.None);
        trans.setCart(cart);
        trans.setTransType(Transaction.TransactionType.Transfer);
        trans.setTransferShopId(toShopId);

        cart.setTotal(new BigDecimal(0));
        cart.setChangeDue(new BigDecimal(0));
        cart.setCashReceived(new BigDecimal(0));
        cart.setSubTotal(new BigDecimal(0));
        transactionRepository.save(trans);


        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(fromShopId,
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    private void checkInventoryForTransfer(String companyId, String shopId, InventoryTransferHistory request, HashMap<String, Product> productMap) {
        for (InventoryTransferLog inventoryTransferLog : request.getTransferLogs()) {

            if (StringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {
                ProductPrepackageQuantity fromPreQuantity = prepackageQuantityRepository.getQuantity(companyId,
                        request.getFromShopId(),
                        request.getFromInventoryId(),
                        inventoryTransferLog.getPrepackageItemId());
                if (fromPreQuantity == null) {
                    throw new BlazeInvalidArgException("PrepackageProduct", "Prepackage Line Item does not exist");
                }

                int transferAmount = inventoryTransferLog.getTransferAmount().intValue();
                if (fromPreQuantity.getQuantity() < transferAmount) {
                    throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Amount specified exceeds available inventory for product");
                }

            } else {
                Product product = productMap.get(inventoryTransferLog.getProductId());
                ProductQuantity productQuantity = null;
                if (product == null) {
                    throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Product doesn't exists.");
                }
                for (ProductQuantity pq : product.getQuantities()) {
                    if (pq.getInventoryId().equals(request.getFromInventoryId())) {
                        productQuantity = pq;
                        break;
                    }
                }

                BigDecimal inventoryTransferAmount = inventoryTransferLog.getTransferAmount();
                if (request.isTransferByBatch()) {
                    if (StringUtils.isNotBlank(inventoryTransferLog.getFromBatchId()) && ObjectId.isValid(inventoryTransferLog.getFromBatchId())) {
                        BatchQuantity dbBatchQuantity = batchQuantityRepository.getBatchQuantityForInventory(companyId, shopId, inventoryTransferLog.getProductId(), request.getFromInventoryId(), inventoryTransferLog.getFromBatchId());
                        if (dbBatchQuantity != null) {
                            if (dbBatchQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                                throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than batch quantity.");
                            }
                        }
                    } else {
                        Iterable<BatchQuantity> sortedBatches = batchQuantityRepository.getBatchQuantitiesForInventorySorted(companyId, shopId, inventoryTransferLog.getProductId(), request.getFromInventoryId(), "{batchPurchaseDate:-1}");
                        for (BatchQuantity batchQuantity : sortedBatches) {
                            if (batchQuantity != null && batchQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                                throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than batch quantity");
                            }
                        }
                    }

                    if (productQuantity == null || productQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                        throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than inventory amount for: " + product.getName());
                    }
                } else {
                    if (productQuantity == null || productQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                        throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than inventory amount for: " + product.getName());
                    }
                }
            }
        }
    }

    private void createQueueTransactionJobForTransfer(String companyId, String shopId, InventoryTransferHistory history) {

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(shopId);
        queuedTransaction.setCompanyId(companyId);
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.InventoryTransfer);
        queuedTransaction.setSellerId(history.getCreateByEmployeeId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(history.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(companyId, shopId, queuedTransaction.getId());

    }

    private void checkEmployeeAccessibility(String companyId, Employee employee) {
        Role role = roleRepository.get(companyId, employee.getRoleId());

        //Firstly check if employee is manager or admin
        if (role != null
                && !role.getName().equalsIgnoreCase("Manager")
                && !role.getName().equalsIgnoreCase("Admin")) {
            //If not then check that employee's is assigned to that terminal or not
            if (role.getPermissions().contains(Role.Permission.WebInventoryTransfer)
                    || role.getPermissions().contains(Role.Permission.WebInventoryManage)) {
                throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Sorry! You are not authorized for inventory transfer");
            }
        }
    }


}
