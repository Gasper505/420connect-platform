package com.fourtwenty.core.services.inventory.impl;

import com.fourtwenty.core.domain.models.product.InventoryOperation;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryActionRepository;
import com.fourtwenty.core.services.inventory.InventoryOperationService;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by mdo on 9/11/18.
 */
public class InventoryOperationServiceImpl implements InventoryOperationService {

    @Inject
    InventoryActionRepository inventoryActionRepository;

    @Override
    public InventoryOperationResult getRecent(String companyId, String shopId, String sourceId) {
        InventoryOperation operation = inventoryActionRepository.getRecentInventoryAction(companyId, shopId, sourceId);
        return createResult(operation);
    }

    @Override
    public InventoryOperation getRecentOperation(String companyId, String shopId, String sourceId) {
        InventoryOperation operation = inventoryActionRepository.getRecentInventoryAction(companyId, shopId, sourceId);
        return operation;
    }

    @Override
    public InventoryOperation getRecentOperation(String companyId, String shopId, String productId, String batchId, String inventoryId, String prepackageItemId) {
        if (StringUtils.isBlank(prepackageItemId)) {
            return inventoryActionRepository.getRecentInventoryAction(companyId, shopId, productId, batchId, inventoryId);
        } else {
            return inventoryActionRepository.getRecentPrepackageInventoryAction(companyId, shopId, productId, batchId, inventoryId, prepackageItemId);
        }
    }

    @Override
    public InventoryOperation getRecentOperation(String companyId, String shopId, String sourceId, String sourceChildId) {
        InventoryOperation operation = inventoryActionRepository.getRecentInventoryAction(companyId, shopId, sourceId,sourceChildId);
        return operation;
    }

    @Override
    public Iterable<InventoryOperation> getAllOperationsFor(String companyId, String shopId, String sourceChildId, String requestId) {
        return inventoryActionRepository.getRecentInventoryActionsByRequestId(companyId, shopId,sourceChildId,requestId);
    }

    @Override
    public InventoryOperationResult adjust(final String companyId,
                                           final String shopId,
                                           final String productId,
                                           final String prepackageItemId,
                                           final String batchId,
                                           final String inventoryId,
                                           final BigDecimal quantity,
                                           final String employeeId,
                                           final InventoryOperation.SourceType sourceType,
                                           final String sourceId,
                                           final String sourceChildId,
                                           String request,
                                           InventoryOperation.SubSourceAction subSourceAction) {


        InventoryOperationResult inventoryOperation = applyOperation(InventoryOperation.Operation.Adjust,
                companyId,
                shopId, productId,
                prepackageItemId,
                batchId,
                inventoryId,
                quantity,
                employeeId,
                sourceType,
                sourceId,
                sourceChildId,
                request,
                subSourceAction);

        return inventoryOperation;
    }

    @Override
    public InventoryOperationResult revertRecent(String companyId, String shopId, InventoryOperation.SourceType sourceType, String sourceId) {
        InventoryOperation lastOperation = inventoryActionRepository.getLastActionBySource(companyId, shopId, sourceId, sourceType);

        // we can only revert the adjusting
        InventoryOperationResult myResult = new InventoryOperationResult();
        if (lastOperation != null && lastOperation.getAction() == InventoryOperation.Operation.Adjust) {
            String requestId = lastOperation.getRequestId();

            if (StringUtils.isNotBlank(requestId)) {
                // Revert all with this requestId
                Iterable<InventoryOperation> operations = inventoryActionRepository.getActionsBySourceRequest(companyId, shopId, sourceId, sourceType, requestId);
                List<InventoryOperation> toBeReverted = Lists.newArrayList(operations);
                for (InventoryOperation operation : toBeReverted) {
                    BigDecimal revertQty = operation.getQuantity().multiply(new BigDecimal(-1));

                    // NOW REVERT ALL CHANGES AND MAINTAIN RUNNING TOTAL
                    InventoryOperationResult result1 = applyOperation(InventoryOperation.Operation.Revert,
                            companyId,
                            shopId,
                            operation.getProductId(),
                            operation.getPrepackageItemId(),
                            operation.getBatchId(),
                            operation.getInventoryId(),
                            revertQty,
                            operation.getEmployeeId(),
                            operation.getSourceType(),
                            operation.getSourceId(),
                            operation.getSourceChildId(),
                            operation.getRequestId(), operation.getSubSourceAction());

                    myResult.operations.addAll(result1.operations);
                }
            }
        }
        return myResult;
    }

    private InventoryOperationResult applyOperation(final InventoryOperation.Operation operation,
                                                    final String companyId,
                                                    final String shopId,
                                                    final String productId,
                                                    final String prepackageItemId,
                                                    final String batchId,
                                                    final String inventoryId,
                                                    final BigDecimal quantity,
                                                    final String employeeId,
                                                    final InventoryOperation.SourceType sourceType,
                                                    final String sourceId,
                                                    final String sourceChildId,
                                                    String request,
                                                    InventoryOperation.SubSourceAction subSourceAction) {
        // make sure this adjust didn't already happened
        InventoryOperation recentOperation = null;
        boolean prepackaged = false;
        String lPrepackageItemId = prepackageItemId;

        BigDecimal runningTotal = new BigDecimal(0);
        BigDecimal quantityToAdd = quantity;
        if (StringUtils.isNotBlank(lPrepackageItemId)) {
            prepackaged = true;
            recentOperation = inventoryActionRepository.getRecentPrepackageInventoryAction(companyId, shopId, productId, batchId, inventoryId, lPrepackageItemId);
        } else {
            lPrepackageItemId = null;
            recentOperation = inventoryActionRepository.getRecentInventoryAction(companyId, shopId, productId, batchId, inventoryId);
        }

        if (recentOperation != null) {
            runningTotal = recentOperation.getRunningTotal();
        }

        if (quantityToAdd == null) {
            quantityToAdd = new BigDecimal(0);
        }

        runningTotal = runningTotal.add(quantity);

        final InventoryOperation inventoryOperation = new InventoryOperation();
        inventoryOperation.prepare(companyId);
        inventoryOperation.setShopId(shopId);
        inventoryOperation.setQuantity(quantityToAdd);
        inventoryOperation.setPrepackageItemId(lPrepackageItemId);
        inventoryOperation.setPrepackaged(prepackaged);
        inventoryOperation.setBatchId(batchId);
        inventoryOperation.setInventoryId(inventoryId);
        inventoryOperation.setProductId(productId);
        inventoryOperation.setEmployeeId(employeeId);
        inventoryOperation.setSourceId(sourceId);
        inventoryOperation.setSourceType(sourceType);
        inventoryOperation.setRequestId(request);
        inventoryOperation.setSourceChildId(sourceChildId);
        inventoryOperation.setAction(operation);
        inventoryOperation.setSubSourceAction(subSourceAction);
        inventoryOperation.setRunningTotal(runningTotal);

        InventoryOperation dbAction = inventoryActionRepository.save(inventoryOperation);

        return createResult(dbAction);
    }

    private InventoryOperationResult createResult(InventoryOperation operation) {
        InventoryOperationResult result = new InventoryOperationResult();
        if (operation != null) {
            result.operations.add(operation);
            result.resultTotal = operation.getRunningTotal();
        }
        return result;
    }

    @Override
    public InventoryOperationResult revertRecentForDerivedBatch(String companyId, String shopId, String productId, String prepackageItemId, String batchId, String inventoryId, BigDecimal quantity, String employeeId, InventoryOperation.SourceType sourceType, String sourceId, String sourceChildId, String request, InventoryOperation.SubSourceAction subSourceAction) {
        InventoryOperation lastOperation = inventoryActionRepository.getLastActionBySource(companyId, shopId, sourceId, sourceType, subSourceAction, batchId);

        // we can only revert the adjusting
        InventoryOperationResult myResult = new InventoryOperationResult();
        if (lastOperation != null && lastOperation.getAction() == InventoryOperation.Operation.Adjust) {
            String requestId = lastOperation.getRequestId();

            if (StringUtils.isNotBlank(requestId)) {
                // Revert all with this requestId
                Iterable<InventoryOperation> operations = inventoryActionRepository.getActionsBySourceRequest(companyId, shopId, sourceId, sourceType, requestId);
                List<InventoryOperation> toBeReverted = Lists.newArrayList(operations);
                for (InventoryOperation operation : toBeReverted) {
                    BigDecimal revertQty = operation.getQuantity().multiply(new BigDecimal(-1));

                    // NOW REVERT ALL CHANGES AND MAINTAIN RUNNING TOTAL
                    InventoryOperationResult result1 = applyOperation(InventoryOperation.Operation.Revert,
                        companyId,
                        shopId,
                        operation.getProductId(),
                        operation.getPrepackageItemId(),
                        operation.getBatchId(),
                        operation.getInventoryId(),
                        revertQty,
                        operation.getEmployeeId(),
                        operation.getSourceType(),
                        operation.getSourceId(),
                        operation.getSourceChildId(),
                        operation.getRequestId(), operation.getSubSourceAction());

                    myResult.operations.addAll(result1.operations);
                }
            }
        }
        return myResult;
    }

}
