package com.fourtwenty.core.services.inventory.impl;

import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductWeightToleranceAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ToleranceEnableRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.ProductWeightToleranceService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

public class ProductWeightToleranceServiceImpl extends AbstractAuthServiceImpl implements ProductWeightToleranceService {

    private static final String WEIGHT_TOLERANCE = "ProductWeightTolerance";
    private static final String WEIGHT_TOLERANCE_NOT_EXISTS = "Product Weight Tolerance does not exist.";
    private static final String INVALID_ARGUMENT = "Invalid arguments";
    private static final String CUSTOM_WEIGHT_ALLOWED_DELETED = "Only custom weights are allowed to be deleted.";
    private static final String CUSTOM_WEIGHT_ALLOWED_ENABLED = "Default weights are not allowed to be disabled/enabled.";
    private static final String NAME_CANNOT_BE_BLANK = "Weight Tolerance name cannot be blank.";
    private static final String TOLERANCE_ALREADY_EXISTS = "Weight Tolerance already exists with name %s.";

    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private RealtimeService realtimeService;

    @Inject
    public ProductWeightToleranceServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    /**
     * Override method to get weight tolerances
     *
     * @return : return search result
     */
    @Override
    public SearchResult<ProductWeightTolerance> getWeightTolerances() {
        return weightToleranceRepository.findItemsWithSort(token.getCompanyId(), "{startWeight:1}", 0, 100);
    }

    /**
     * Override method to weight tolerance according to afterDate and beforeDate
     *
     * @param afterDate  : afterDate
     * @param beforeDate : beforeDate
     * @return return list
     */
    @Override
    public DateSearchResult<ProductWeightTolerance> getWeightTolerances(long afterDate, long beforeDate) {
        return weightToleranceRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    /**
     * Overriode method to add weight tolerance
     *
     * @param request : request
     * @return : weight tolerance
     */
    @Override
    public ProductWeightTolerance addWeightTolerance(ProductWeightToleranceAddRequest request) {
        if (request == null) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, "Request cannot be blank.");
        }

        if (StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, NAME_CANNOT_BE_BLANK);
        }

        ProductWeightTolerance weightTolerance = weightToleranceRepository.getToleranceByName(token.getCompanyId(), request.getName());
        if (weightTolerance != null) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, String.format(TOLERANCE_ALREADY_EXISTS, request.getName()));
        }

        weightTolerance = new ProductWeightTolerance();
        weightTolerance.prepare(token.getCompanyId());
        weightTolerance.setName(request.getName());
        weightTolerance.setEndWeight(request.getEndWeight());
        weightTolerance.setPriority(request.getPriority());
        weightTolerance.setStartWeight(request.getStartWeight());
        weightTolerance.setUnitValue(request.getUnitValue());
        weightTolerance.setEnabled(request.isEnabled());
        weightTolerance.setWeightValue(request.getWeightValue());
        weightTolerance.setWeightKey(ProductWeightTolerance.WeightKey.CUSTOM);


        weightToleranceRepository.save(weightTolerance);
        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);

        return weightTolerance;
    }

    /**
     * Override method to update weight tolerance
     *
     * @param toleranceId : tolerance id
     * @param request     : tolerance request
     * @return : weight tolerance
     */
    @Override
    public ProductWeightTolerance updateWeightTolerance(String toleranceId, ProductWeightTolerance request) {
        if (StringUtils.isBlank(toleranceId)) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, INVALID_ARGUMENT);
        }

        ProductWeightTolerance dbTolerance = weightToleranceRepository.get(token.getCompanyId(), toleranceId);

        if (dbTolerance == null) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, WEIGHT_TOLERANCE_NOT_EXISTS);
        }

        ProductWeightTolerance weightTolerance = weightToleranceRepository.getToleranceByName(token.getCompanyId(), request.getName());
        if (weightTolerance != null && !weightTolerance.getId().equalsIgnoreCase(dbTolerance.getId())) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, String.format(TOLERANCE_ALREADY_EXISTS, request.getName()));
        }

        dbTolerance.setName(request.getName());
        dbTolerance.setEndWeight(request.getEndWeight());
        dbTolerance.setPriority(request.getPriority());
        dbTolerance.setStartWeight(request.getStartWeight());
        dbTolerance.setUnitValue(request.getUnitValue());
        dbTolerance.setEnabled(request.isEnabled());
        dbTolerance.setWeightValue(request.getWeightValue());

        weightToleranceRepository.update(token.getCompanyId(), toleranceId, dbTolerance);
        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);

        return dbTolerance;
    }

    /**
     * Override method to get tolerance by id
     *
     * @param toleranceId : toleranceId
     * @return tolerance
     */
    @Override
    public ProductWeightTolerance getWeightToleranceById(String toleranceId) {
        return weightToleranceRepository.get(token.getCompanyId(), toleranceId);
    }

    /**
     * Override method to delete weight tolerance
     *
     * @param weightToleranceId : weight tolerance id
     */
    @Override
    public void deleteWeightTolerance(String weightToleranceId) {
        if (StringUtils.isBlank(weightToleranceId)) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, INVALID_ARGUMENT);
        }

        ProductWeightTolerance dbTolerance = weightToleranceRepository.get(token.getCompanyId(), weightToleranceId);

        if (dbTolerance == null) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, WEIGHT_TOLERANCE_NOT_EXISTS);
        }

        if (dbTolerance.getWeightKey() != ProductWeightTolerance.WeightKey.CUSTOM) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, CUSTOM_WEIGHT_ALLOWED_DELETED);
        }

        weightToleranceRepository.removeById(token.getCompanyId(), weightToleranceId);
    }

    /**
     * Override method to enable weight tolerance
     *
     * @param toleranceId : toleranceId
     * @param request     : request
     * @return : weight tolerance
     */
    @Override
    public ProductWeightTolerance enableWeightTolerance(String toleranceId, ToleranceEnableRequest request) {
        if (StringUtils.isBlank(toleranceId)) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, INVALID_ARGUMENT);
        }

        ProductWeightTolerance dbTolerance = weightToleranceRepository.get(token.getCompanyId(), toleranceId);

        if (dbTolerance == null) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, WEIGHT_TOLERANCE_NOT_EXISTS);
        }

        if (dbTolerance.getWeightKey() != ProductWeightTolerance.WeightKey.CUSTOM) {
            throw new BlazeInvalidArgException(WEIGHT_TOLERANCE, CUSTOM_WEIGHT_ALLOWED_ENABLED);
        }

        weightToleranceRepository.enableWeightTolerance(toleranceId, request.isEnabled());

        dbTolerance.setEnabled(request.isEnabled());
        return dbTolerance;
    }
}
