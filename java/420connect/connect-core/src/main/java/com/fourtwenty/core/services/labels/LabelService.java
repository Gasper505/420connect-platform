package com.fourtwenty.core.services.labels;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelsQueryRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.LabelItem;
import com.fourtwenty.core.services.common.CommonLabelService;

import java.io.File;
import java.util.List;

public interface LabelService {
    File getBarcodeImage(String barcodeId,int angle, int height, String barcodeType);
    File generateBarcode(String sku, int angle, int height, String barcodeType, CommonLabelService.SKUPosition skuPosition);
    SearchResult<LabelItem> getLabelItems(LabelsQueryRequest request);

    List<CompanyAsset> getQrCodeLabelItems(LabelsQueryRequest request);
}
