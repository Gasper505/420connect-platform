package com.fourtwenty.core.services.labels.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelsQueryRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.LabelItem;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.CommonLabelService;
import com.fourtwenty.core.services.labels.LabelService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.io.File;
import java.util.List;

public class LabelServiceImpl extends AbstractAuthServiceImpl implements LabelService {
    @Inject
    CommonLabelService commonLabelService;
    @Inject
    BarcodeService barcodeService;

    @Inject
    public LabelServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public File getBarcodeImage(String barcodeId, int angle, int height, String barcodeType) {
        return barcodeService.getBarcodeImage(token.getCompanyId(),barcodeId,angle,height,barcodeType);
    }

    @Override
    public File generateBarcode(String sku, int angle, int height, String barcodeType, CommonLabelService.SKUPosition skuPosition) {
        return commonLabelService.generateBarcode(token.getCompanyId(),token.getShopId(),sku,angle,height,barcodeType,skuPosition);
    }

    @Override
    public SearchResult<LabelItem> getLabelItems(LabelsQueryRequest request) {
        return commonLabelService.getLabelItems(token.getCompanyId(), token.getShopId(), request);
    }

    @Override
    public List<CompanyAsset> getQrCodeLabelItems(LabelsQueryRequest request) {
        return commonLabelService.getQrCodeLabelItems(token.getCompanyId(),token.getShopId(),request);
    }
}
