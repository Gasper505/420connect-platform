package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Adjustment;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface AdjustmentService {

    Adjustment getAdjustmentById(String adjustmentId);

    SearchResult<Adjustment> getAdjustments(int start, int limit);

    Adjustment addAdjustment(Adjustment adjustment);

    Adjustment updateAdjustment(String adjustmentId, Adjustment adjustment);

    void deleteAdjustment(String adjustmentId);
}
