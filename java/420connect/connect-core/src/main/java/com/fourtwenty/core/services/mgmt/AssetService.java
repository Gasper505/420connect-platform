package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopAssetResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mdo on 10/26/15.
 */
public interface AssetService {
    CompanyAsset uploadAssetPublic(InputStream inputStream, String name, CompanyAsset.AssetType assetType, String mimeType, String keyName);

    CompanyAsset uploadAssetPublic(InputStream inputStream, String name, CompanyAsset.AssetType type, String mimeType);

    CompanyAsset uploadAssetPublicFromUrl(String url, CompanyAsset.AssetType type, String keyname);

    CompanyAsset uploadAssetPrivate(InputStream inputStream, String name, CompanyAsset.AssetType type, String mimeType);

    CompanyAsset uploadAssetPrivate(InputStream inputStream, String name, String fileName, String fileExtension, CompanyAsset.AssetType type);

    AssetStreamResult getCompanyAssetStream(String assetKey, String assetToken);

    AssetStreamResult getConsumerUserAssetStream(String consumerUserId, String assetKey, String assetToken);

    File getCombinedContract(String contractAssetKey, String signedContractAssetKey, String contentType, String assetToken) throws IOException;

    File getAgreement(String signedContractId, String contractAssetKey, String assetToken) throws IOException;

    File getCombinedContractByMember(String memberId, String signedContractId, String assetToken);

    ShopAssetResult copyMemberAssetByShop(String shopId);

    FileInputStream exportMemberAssetByShop(String shopAssetToken);

    AssetStreamResult getCompanyAssetStream(String assetKey, String assetToken, boolean handleError, boolean isReportRequest, String filename);


    AssetStreamResult getCompanyAssetStreamWithoutAsset(String assetKey, String assetToken, CompanyAsset asset);
}
