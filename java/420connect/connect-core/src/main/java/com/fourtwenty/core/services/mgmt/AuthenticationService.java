package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.rest.dispensary.requests.auth.*;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyRegisterRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalAssignRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalSetupRequest;
import com.fourtwenty.core.rest.dispensary.results.InitialLoginResult;
import com.fourtwenty.core.rest.dispensary.results.auth.ManagerAccessResult;
import com.fourtwenty.core.rest.dispensary.results.auth.SwitchApplicationResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;

/**
 * Created by mdo on 8/28/15.
 */
public interface AuthenticationService {

    InitialLoginResult getCurrentActiveEmployee();

    InitialLoginResult adminLogin(EmailLoginRequest request);

    InitialLoginResult updateTokenWithShop(UpdateTokenRequest request);

    InitialLoginResult renewToken(ConnectAuthToken.GrowAppType growAppType);

    InitialLoginResult registerCompany(CompanyRegisterRequest request);

    // POS
    InitialLoginResult terminalManagerLogin(InitTerminalRequest request);

    ManagerAccessResult managerSettingsAccess(ManagerAccessRequest request);


    // must be authenticated
    InitialLoginResult loginWithPin(PinRequest request);

    InitialLoginResult terminalAssignEmployee(TerminalAssignRequest request);

    InitialLoginResult terminalEmployeeLogin(QuickPinLoginRequest request);

    void terminalEmployeeLogout();

    InitialLoginResult setupTerminal(TerminalSetupRequest request);

    void checkPermission(CheckPermissionRequest request);

    SwitchApplicationResult switchApplication(String accessToken, String appName, String requestShopId, ConnectAuthToken.GrowAppType growAppType);

    InitialLoginResult renewToken();
}
