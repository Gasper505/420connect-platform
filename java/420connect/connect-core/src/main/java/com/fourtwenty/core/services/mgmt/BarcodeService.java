package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;

import java.io.File;
import java.util.List;

/**
 * Created by mdo on 3/6/17.
 */
public interface BarcodeService {
    DateSearchResult<BarcodeItem> getBarcodesWithDates(final String companyId, final String shopId,long afterDate, long beforeDate);

    BarcodeItem createBarcodeItemIfNeeded(final String companyId, String shopId,
                                          String productId,
                                          BarcodeItem.BarcodeEntityType entityType,
                                          String entityId, String currentKey, String oldKey, boolean transferShop);

    void deleteBarcodesForProduct(String productId);

    File getBarcodeImage(final String companyId,String barcodeId, int angle, int height, String barcodeType);

    List<BarcodeItem> getBarCodesByLabel(String companyId, String shopId,
                                         String label, BarcodeItem.BarcodeEntityType entityType);

    BarcodeItem getBarcodeItemByBarcode(final String companyId, final String shopId, final BarcodeItem.BarcodeEntityType entityType, final String barcode);

    List<BarcodeItem> getAvailableBarCodes(final String companyId, final String shopId,
                                           BarcodeItem.BarcodeEntityType entityType, List<String> currentKeys);

    void deleteBarCodesForProducts(final String companyId, final String shopId,List<String> productIds);
}
