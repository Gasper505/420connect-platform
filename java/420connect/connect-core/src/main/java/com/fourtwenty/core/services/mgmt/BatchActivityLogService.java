package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.BatchActivityLog;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;

public interface BatchActivityLogService {

    BatchActivityLog addBatchActivityLog(final String batchId, final String employeeId, final String log);

    SearchResult<BatchActivityLog> getAllBatchActivityLog(final String companyId, final String batchId, final int start, final int limit);

    void addBatchActivityLog(final List<ObjectId> batchIds, final String employeeId, final String logFor);

    HashMap<String, List<BatchActivityLog>> findItemsByBatchId(String companyId, String shopId, List<String> batchIds);

}
