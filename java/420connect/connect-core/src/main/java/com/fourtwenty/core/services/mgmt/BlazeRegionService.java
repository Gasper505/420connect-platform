package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface BlazeRegionService {

    BlazeRegion getRegionById(String regionId);

    BlazeRegion createRegion(BlazeRegion blazeRegion);

    BlazeRegion updateRegion(String regionId, BlazeRegion blazeRegion);

    SearchResult<BlazeRegion> getRegions(int start, int limit);

    void deleteRegionById(String regionId);
}
