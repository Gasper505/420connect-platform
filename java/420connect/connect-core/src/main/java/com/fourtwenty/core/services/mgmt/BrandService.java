package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

public interface BrandService {

    Brand addBrand(BrandAddRequest request);

    Brand updateBrand(final String brandId, Brand request);

    SearchResult<BrandResult> getAllBrand(final String term, final int start, final int limit);

    void deleteBrand(final String brandId);

    BrandResult getBrandByBrandId(final String brandId);

    SearchResult<BrandResult> getAllDeletedBrand(final int skip, final int limit);

    List<Vendor> getVendorsByBrand(String brandId);

    void bulkBrandUpdate(BrandBulkUpdateRequest request);
}
