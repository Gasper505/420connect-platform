package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.CustomerInfo;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.RefundTransactionRequest;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.services.mgmt.models.LinkedOrderItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 12/23/16.
 */
public interface CartService {

    class MixMatchOrder {

        public OrderItem orderItem;
        public String key;
        public boolean whole = false;
        public boolean matched = false;
        public Double wholeValue = 0d;
        public Double realQuantity = 0d;
        public ProductPriceRange priceRange;
        public ProductPriceBreak priceBreak;
        public Product product;
    }
    class MixMatchGroup {

        public String key;
        public List<MixMatchOrder> orders = new ArrayList<>();
        public ProductPriceRange groupPriceRange;
        public ProductPriceBreak groupPriceBreak;
        public Double groupValue = 0d;
        public Double totalQuantity = 0d;
    }
    class MixMatchItem {

        public double totalQuantity = 0d;
        public double itemQuantity = 0d;
        public OrderItem orderItem;
        public String key;
        public ProductPriceRange groupPriceRange;
        public ProductPriceRange itemPriceRange;
        public ProductPriceBreak groupPriceBreak;
        public ProductPriceBreak itemPriceBreak;
        public Product product;
        public boolean matched = false;

    }
    void prepareCart(Shop shop,
                     Transaction trans,
                     boolean reset,
                     Transaction.TransactionStatus incomingStatus,
                     final boolean assignProducts,
                     final CustomerInfo customerInfo,
                     final boolean newCart);

    List<MixMatchGroup> buildMixMatchStructure(final List<LinkedOrderItem> orderItems,
                                               final HashMap<String, Product> productHashMap,
                                               final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                               final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                               final HashMap<String, Prepackage> prepackageHashMap,
                                               final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap);

    HashMap<String, MixMatchItem> getMixMatchItemMap(final List<LinkedOrderItem> orderItems,
                                                     final HashMap<String, Product> productHashMap,
                                                     final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                     final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                                     final HashMap<String, Prepackage> prepackageHashMap,
                                                     final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap);

    Cart processEntireCartPricingPromos(final Shop shop,
                                        final Cart myCart,
                                        final boolean reset,
                                        final boolean assignProducts,
                                        final MemberGroup memberGroup,
                                        final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap,
                                        final HashMap<String, Product> productHashMap,
                                        final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                        final HashMap<String, Prepackage> prepackageHashMap,
                                        final HashMap<String, ProductCategory> categoryHashMap,
                                        final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                        final Transaction transaction,
                                        final Transaction.TransactionStatus incomingStatus,
                                        final CustomerInfo customerInfo);




    double applyPostTaxDiscount(Transaction transaction, double subTotal);

    PromotionReqLog createPromotionLog(String promotionId, String finalizedDiscountId, String companyId, double discountCashAmt);

    Transaction createRefundTransaction(Shop shop, Transaction dbTrans, RefundTransactionRequest request, String sellerTerminalId, String sellerId);

    Transaction finalizeOrderItem(Shop shop, Transaction trans, String orderItemId);

    Transaction unfinalizeOrderItem(Transaction trans, String orderItemId);

    Transaction finalizeAllOrderItems(Shop shop, Transaction trans);

    Transaction unfinalizeAllOrderItems(Shop shop, Transaction trans);

    void prepareCartForPaymentType(Shop shop, Transaction dbTransaction, CustomerInfo customerInfo);
}
