package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.CashDrawerSessionActivityLog;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionActivityLogResult;

import java.math.BigDecimal;

public interface CashDrawerActivityLogService {

    SearchResult<CashDrawerSessionActivityLogResult> getActiveCaseDrawerActivityLogs(String cashDrawerSessionId, long startDate, long endDate, int start, int limit);

    CashDrawerSessionActivityLog createCashDrawerActivityType(PaidInOutItem paidInOutItem, CashDrawerSession cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType cashDrawerType, BigDecimal amount, Transaction transaction);
}
