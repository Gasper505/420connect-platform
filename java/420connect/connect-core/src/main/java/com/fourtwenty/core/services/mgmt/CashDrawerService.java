package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.DrawerStartCashUpdateRequest;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.rest.dispensary.requests.company.EndDrawerRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.PaidInOutRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StartDrawerRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionResult;

/**
 * Created by mdo on 12/7/16.
 */
public interface CashDrawerService {
    <E extends CashDrawerSession> DateSearchResult<E> getCashDrawersByDate(long afterDate, long beforeDate);

    SearchResult<CashDrawerSessionResult> getCashDrawerLogs(int start, int limit);

    CashDrawerSessionResult startCashDrawer(StartDrawerRequest request);

    CashDrawerSessionResult endCashDrawer(String cdSessionId, EndDrawerRequest request);

    CashDrawerSessionResult reopenCashDrawer(String cdSessionId);

    CashDrawerSessionResult editCashDrawer(String cdSessionId, CashDrawerSession cashDrawerSession);

    CashDrawerSessionResult getCashDrawerLogById(String cdSessionId);

    CashDrawerSessionResult refreshCashDrawer(String cdSessionId);

    DateSearchResult<PaidInOutItem> getPaidIOsByDate(long afterDate, long beforeDate);

    SearchResult<PaidInOutItem> getPaidIOs(String cdSessionId, int start, int limit);

    PaidInOutItem enterPaidIO(String cdSessionId, PaidInOutRequest request);

    PaidInOutItem editPaidIO(String paidIOItemId, PaidInOutItem request);

    void deletePaidIO(String paidIOItemId);

    CashDrawerSessionResult updateStartingCash(String cdSessionId, DrawerStartCashUpdateRequest startCashUpdateRequest);


    String getPaidInOutItemReceipt(String cdSessionId, String paidIOItemId, int width);

    SearchResult<CashDrawerSessionResult> getCurrentActiveCashDrawer();
}
