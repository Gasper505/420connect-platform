package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.BarcodeItem;

public interface CommonBarCodeService {
    BarcodeItem createBarcodeItemIfNeeded(String companyId,
                                          String shopId,
                                          String productId,
                                          BarcodeItem.BarcodeEntityType entityType,
                                          String entityId, String currentKey, String oldKey, boolean transferShop);
}
