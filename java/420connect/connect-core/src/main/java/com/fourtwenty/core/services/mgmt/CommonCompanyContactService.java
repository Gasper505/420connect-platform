package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;

public interface CommonCompanyContactService {

    CompanyContact createCompanyContact(String companyId, String shopId, CompanyContact companyContact, String employeeId, String employeeName);

    CompanyContact updateCompanyContact(String companyId, String shopId, String contactId, CompanyContact companyContact, String employeeId, String employeeName);

    CompanyContactResult getCompanyContactById(String companyId, String contactId);

    SearchResult<CompanyContact> getCompanyContacts(String companyId, String shopId, String startDate, String endDate, int skip, int limit);
}
