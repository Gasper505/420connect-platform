package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CommonProductService {
    Product getProductById(String companyId, String shopId, String productId);

    Product addProduct(String companyId, String shopId, ProductAddRequest addRequest);

    Product updateProduct(String companyId, String shopId, String productId, Product product, Boolean isPartner, String activeUserId, String activeUserName);

    SearchResult<Product> getProducts(String companyId, String shopId, String startDate, String endDate, int skip, int limit);
    SearchResult<Product> getProducts(String companyId, String shopId, long startDate, long endDate, int skip, int limit);
}
