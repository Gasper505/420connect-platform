package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;

import java.util.HashMap;

public interface CommonVendorService {
    VendorResult getVendor(String companyId, String vendorId);

    HashMap<String, Brand> prepareVendor(String companyId, VendorResult vendorResult);

    void prepareVendorResult(VendorResult vendorResult, HashMap<String, Brand> brandHashMap);

    void prepareSearchResult(String companyId, SearchResult<VendorResult> result);

    Vendor addVendor(String companyId, String shopId, VendorAddRequest request, String employeeId);

    <E extends  Vendor> Vendor updateVendor(String companyId, String shopId, String vendorId, E vendor, Boolean isPartner, String activeUserId, String activeUserName);

    SearchResult<Vendor> getVendors(String companyId, String shopId, String startDate, String endDate, int skip, int limit);

    long getEndDate(Shop shop, String endDate);

    long getStartDate(Shop shop, String startDate);

    Company checkCompanyAvailability(String companyId);

    Shop checkShopAvailability(String companyId, String shopId);
}
