package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompanyContactLog;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyContactLogAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CompanyContactLogService {
    CompanyContactLog addCompanyContactLog(final CompanyContactLogAddRequest request);

    SearchResult<CompanyContactLog> getAllCompanyContactLogsByCustomerCompany(final String customerCompanyId, final int start, final int limit);
}
