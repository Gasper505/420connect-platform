package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;

public interface CompanyContactService {
    CompanyContact createCompanyContact(CompanyContact companyContact);

    CompanyContact updateCompanyContact(String contactId, CompanyContact companyContact);

    CompanyContactResult getCompanyContactById(String contactId);

    void deleteCompanyContact(String contactId);

    SearchResult<CompanyContactResult> getAllCompanyContact(String customerCompanyId, int start, int limit, String term);

    CompanyContactResult getCompanyContactById(String contactId, boolean ieRequired);
}
