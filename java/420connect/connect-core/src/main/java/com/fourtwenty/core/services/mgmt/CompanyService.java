package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmployeeSeatRequest;

import java.io.InputStream;
import java.util.List;

/**
 * Created by mdo on 5/15/16.
 */
public interface CompanyService {
    Company getCurrentCompany();

    List<Company> getCompanies();

    Company updateCompanyLogo(InputStream inputStream);

    Company updateCompany(Company company);

    CompanyFeatures getCompanyFeatures();

    Company updateCompanyQueueUrl(String queueUrl);

    void sendEmailToCompany(final String companyId, final CompanyEmailRequest request);

    Company.OnPremCompanyConfig getOnPremConfigs(String companyId);

    void updateCompanyMaxEmployee(CompanyEmployeeSeatRequest request);
}
