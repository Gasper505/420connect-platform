package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.rest.dispensary.requests.company.ContractAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by Stephen Schmidt on 12/7/2015.
 */
public interface ContractService {
    DateSearchResult<Contract> getCompanyContracts(long afterDate, long beforeDate);

    SearchResult<Contract> getContractsForShop();

    Contract getContract(String contractId);

    void setActiveContract(String contractId);

    Contract addContract(ContractAddRequest addRequest);

    Contract updateContract(String contractId, Contract contract);

    void deleteContract(String contractId);


}
