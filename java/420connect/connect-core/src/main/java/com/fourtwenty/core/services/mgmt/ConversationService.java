package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.rest.features.ActiveConversationResult;
import com.fourtwenty.core.rest.features.ConversationResult;
import com.fourtwenty.core.rest.features.TwilioMessageRequest;
import com.fourtwenty.core.rest.features.TwilioResult;

import java.util.List;


public interface ConversationService {
    List<Conversation> getTransactionConversation(final String transactionId, final long afterDate, final long beforeDate, final Conversation.ConversationType type);

    String getTransactionToAndFromNumber(final String toNUmber, final String fromNumber);

    Conversation getConversationByMessageId(final String messageId);

    void updateConversationByMessageId(final String messageId, final String messageStatus);

    Conversation sendConversation(final TwilioMessageRequest request);

    void receiveConversation(final String transactionId, final String message, final String sid);

    boolean isOptOut(String message, String number, String fromNumber);

    List<Conversation> getTransactionConversation(final String transactionId, final long afterDate, final long beforeDate);
    TwilioResult<List<ConversationResult>> getTransactionConversationByCurrentUser(final long afterDate, final long beforeDate, final Conversation.ConversationType type);

    Conversation updateMessageStatus(String chatId, Conversation.Status status);

    TwilioResult<List<ConversationResult>> getConversationByTransaction(final String transactionId, final long afterDate, final long beforeDate, final Conversation.ConversationType type);
    TwilioResult<List<ActiveConversationResult>>  getActiveTransactionChat();

}
