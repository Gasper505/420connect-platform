package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CreditHistory;
import com.fourtwenty.core.rest.dispensary.requests.company.CreditHistoryRequest;

public interface CreditHistoryService {
    CreditHistory addCreditHistory(final CreditHistoryRequest request);

    CreditHistory updateCreditHistory(String customerCompanyId, final CreditHistoryRequest request);
}
