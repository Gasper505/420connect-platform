package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CultivationTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;

public interface CultivationTaxInfoService {
    CultivationTaxInfo getCultivationTaxInfoForShop(final Shop shop);
}
