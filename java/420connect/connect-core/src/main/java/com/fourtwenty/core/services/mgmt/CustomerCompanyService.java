package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CustomerCompany;
import com.fourtwenty.core.rest.dispensary.requests.company.CustomerCompanyAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CustomerCompanyCredit;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface CustomerCompanyService {
    CustomerCompany addCustomerCompany(CustomerCompanyAddRequest customerCompanyAddRequest);

    CustomerCompany getCustomerCompany(String customerCompanyId);

    CustomerCompany updateCustomerCompany(String customerCompanyId, CustomerCompany customerCompany);

    void deleteCustomerCompany(String customerCompanyId);

    SearchResult<CustomerCompany> getAllCustomerCompany(int start, int limit, String term);

    CompanyAsset getCustomerCompanyAttachment(String customerCompanyId, String attachmentId);

    CompanyAsset updateCustomerCompanyAttachment(String customerCompanyId, String attachmentId, CompanyAsset asset);

    void deleteCustomerCompanyAttachment(String customerCompanyId, String attachmentId);

    CompanyAsset addCustomerCompanyAttachment(String customerCompanyId, CompanyAsset asset);

    CustomerCompany addCreditInCustomerCompany(final String customerCompanyId, final CustomerCompanyCredit companyCredit);

    CustomerCompany updateCreditInCustomerCompany(final String customerCompanyId, final CustomerCompanyCredit companyCredit);
}
