package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyRegisterRequest;

import java.util.HashMap;

/**
 * Created by mdo on 11/22/15.
 */
public interface DefaultDataService {
    void initializeDefaultCompanyData(String companyId, ConnectProduct connectProduct, CompanyRegisterRequest registerRequest);

    void initializeDefaultShopData(Shop shop, HashMap<String, MemberGroup> membershipLevelHashMap, boolean newCompany, Company company);
}
