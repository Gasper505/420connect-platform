package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.DeliveryZone;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.*;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeDailyMileageResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeTransactionDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryByEmployeeResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductPackagesResult;
import com.fourtwenty.core.services.inventory.InventoryActionLog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 11/9/17.
 */
public interface DeliveryService {
    DeliveryZone addDeliverZone(DeliveryZone deliveryZone);

    DeliveryZone updateDeliveryArea(String deliveryAreaId, DeliveryZone deliveryZone);

    void deleteDeliveryArea(String deliveryAreaId);

    SearchResult<DeliveryZone> getAllDeliveryZone(int start, int limit, DeliveryZone.DeliveryZoneType zoneType);

    SearchResult<EmployeeLocationResult> getFreeDrivers(int start, int limit);

    List<EnRouteDriverEmployeeResult> getEnRouteDrivers();

    EmployeeDailyMileageResult getDailyMileageByDriver(String employeeId);

    SearchResult<TransactionMileageResult> getMileageByDriverAndTransaction(String employeeId, int start, int limit);

    SearchResult<DeliveryDriverEmployeeResult> getDeliveryDriverEmployee(String term, int start, int limit);

    DeliveryZone getDeliveryZoneById(String deliveryZoneId);

    SearchResult<Transaction> getTransactionByEmployeeId(String employeeId, int start, int limit);

    SearchResult<ProductPackagesResult> getInventoryByEmployee(String employeeId, int start, int limit, String searchTerm);

    EmployeeTransactionDistanceResult getEmployeesByDistance(String memberId, int start, int limit, String publicKey, String transactionId);

    SearchResult<InventoryByEmployeeResult> getProductInventoryByEmployee(String productId, int start, int limit);

    SearchResult<EmployeeLocationResult> getEnRouteEmployee();

    Map<String, List<TerminalLocation>> getTerminalLocationByTransaction(HashMap<String, Transaction> transactionHashMap, Iterable<TerminalLocation> terminalLocationList);

    Double calculateDistance(List<TerminalLocation> terminalLocations);

    double calculateMileageByTransaction(Transaction transaction);

    List<EmployeeDistanceResult> getClockedInEmployees(String transactionId, Employee.SortOption sort, int limit);

    List<MemberDistanceResult> getEmployeeDistanceByTranactions(String employeeId, String location);

    SearchResult<InventoryActionLog> getAllInventoryLog(String employeeId,  int start, int limit, String searchTerm, long afterDate, long beforeDate);
}
