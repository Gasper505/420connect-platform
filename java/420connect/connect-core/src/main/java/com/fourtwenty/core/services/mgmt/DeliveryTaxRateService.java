package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.DeliveryTaxRate;
import com.fourtwenty.core.rest.dispensary.results.company.DeliveryTaxRateResult;

import java.util.List;

public interface DeliveryTaxRateService {
    List<DeliveryTaxRateResult> getDeliveryTaxRatesByShop(String shopId);

    DeliveryTaxRateResult getDeliveryTaxRateById(String taxRateId);

    void deleteDeliveryTaxRateById(String taxRateId);

    DeliveryTaxRate createDeliveryTaxRate(DeliveryTaxRate deliveryTaxRate);

    DeliveryTaxRate updateDeliveryTaxRate(String taxRateId, DeliveryTaxRate deliveryTaxRate);
}
