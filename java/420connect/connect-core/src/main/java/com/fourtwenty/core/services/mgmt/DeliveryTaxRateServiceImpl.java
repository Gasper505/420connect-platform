package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.DeliveryTaxRate;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.domain.repositories.dispensary.BlazeRegionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.DeliveryTaxRateRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.company.DeliveryTaxRateResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DeliveryTaxRateServiceImpl extends AbstractAuthServiceImpl implements DeliveryTaxRateService {

    private static final String SHOP = "Shop";
    private static final String DELIVERY_TAX_RATE = "Delivery tax rate";
    private static final String SHOP_NOT_FOUND = "Shop not found";
    private static final String DELIVERY_TAX_RATE_NOT_FOUND = "Delivery tax rate not found";
    private static final String REGION_NOT_FOUND = "Region not found";
    private static final String REGION_NOT_AVAILABLE = "Please add regions";

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BlazeRegionRepository blazeRegionRepository;
    @Inject
    private DeliveryTaxRateRepository deliveryTaxRateRepository;

    @Inject
    public DeliveryTaxRateServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * This method returns list of delivery tax rates by shop
     * @param shopId : shop id for which list of tax rate needs to return
     */
    @Override
    public List<DeliveryTaxRateResult> getDeliveryTaxRatesByShop(String shopId) {
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        List<DeliveryTaxRateResult> deliveryTaxRates = deliveryTaxRateRepository.listAllByShopId(token.getCompanyId(), shopId, "{created : -1}" , DeliveryTaxRateResult.class);

        this.prepareResult(deliveryTaxRates);
        return deliveryTaxRates;
    }

    private void prepareResult(List<DeliveryTaxRateResult> deliveryTaxRates) {
        Set<ObjectId> regionIds = new HashSet<>();
        for (DeliveryTaxRateResult deliveryTaxRate : deliveryTaxRates) {
            if (deliveryTaxRate.getRegionIds() == null) {
                continue;
            }
            for (String regionId : deliveryTaxRate.getRegionIds()) {
                regionIds.add(new ObjectId(regionId));
            }
        }

        HashMap<String, BlazeRegion> regionMap = blazeRegionRepository.listAsMap(token.getCompanyId(), new ArrayList<>(regionIds));
        for (DeliveryTaxRateResult deliveryTaxRate : deliveryTaxRates) {
            if (deliveryTaxRate.getRegionIds() == null) {
                continue;
            }
            List<BlazeRegion> blazeRegions = new ArrayList<>();
            for (String regionId : deliveryTaxRate.getRegionIds()) {
                BlazeRegion blazeRegion = regionMap.get(regionId);
                if (blazeRegion != null) {
                    blazeRegions.add(blazeRegion);
                }
            }
            deliveryTaxRate.setBlazeRegions(blazeRegions);
        }
    }

    /**
     * THis method fetch delivery tax rate by id
     * @param taxRateId : delivery tax rate's id
     */
    @Override
    public DeliveryTaxRateResult getDeliveryTaxRateById(String taxRateId) {

        DeliveryTaxRateResult deliveryTaxRate = deliveryTaxRateRepository.get(token.getCompanyId(), taxRateId, DeliveryTaxRateResult.class);
        if (deliveryTaxRate == null) {
            throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, DELIVERY_TAX_RATE_NOT_FOUND);
        }

        this.prepareDeliveryTaxRate(deliveryTaxRate);

        return deliveryTaxRate;
    }

    private void prepareDeliveryTaxRate(DeliveryTaxRateResult deliveryTaxRate) {
        if (deliveryTaxRate.getRegionIds() == null) {
            return;
        }
        Set<ObjectId> regionIds = new HashSet<>();
        for (String regionId : deliveryTaxRate.getRegionIds()) {
            regionIds.add(new ObjectId(regionId));
        }

        Iterable<BlazeRegion> regions = blazeRegionRepository.list(token.getCompanyId(), new ArrayList<>(regionIds));
        deliveryTaxRate.setBlazeRegions(Lists.newArrayList(regions));
    }

    /**
     * This method deletes tax rate by id
     * @param taxRateId : delivery tax rate's id
     */
    @Override
    public void deleteDeliveryTaxRateById(String taxRateId) {
        DeliveryTaxRate deliveryTaxRate = deliveryTaxRateRepository.get(token.getCompanyId(), taxRateId);
        if (deliveryTaxRate == null) {
            throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, DELIVERY_TAX_RATE_NOT_FOUND);
        }

        deliveryTaxRateRepository.removeById(token.getCompanyId(), taxRateId);
    }

    /**
     * Create delivery tax rate
     * @param request :
     */
    @Override
    public DeliveryTaxRate createDeliveryTaxRate(DeliveryTaxRate request) {

        if (request.getRegionIds() == null || request.getRegionIds().size() == 0) {
            throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, REGION_NOT_AVAILABLE);
        }

        List<ObjectId> regionIds = new ArrayList<>();
        for (String regionId : request.getRegionIds()) {
            if (ObjectId.isValid(regionId)) {
                regionIds.add(new ObjectId(regionId));
            }
        }


        HashMap<String, BlazeRegion> regionMap = blazeRegionRepository.listAsMap(token.getCompanyId(), regionIds);
        for (String regionId : request.getRegionIds()) {
            BlazeRegion blazeRegion = regionMap.get(regionId);
            if (blazeRegion == null) {
                throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, REGION_NOT_FOUND);
            }
        }

        DeliveryTaxRate taxRate = new DeliveryTaxRate();

        taxRate.prepare(token.getCompanyId());
        taxRate.setShopId(token.getShopId());
        taxRate.setName(request.getName());
        taxRate.setActive(request.isActive());
        taxRate.setRegionIds(request.getRegionIds());
        taxRate.setUseComplexTax(request.isUseComplexTax());
        taxRate.setTaxOrder(request.getTaxOrder());
        taxRate.setTaxInfo(request.getTaxInfo());
        taxRate.setTaxTables(request.getTaxTables());
        this.prepareTaxTable(taxRate);
        this.prepareTaxInfo(taxRate);

        return deliveryTaxRateRepository.save(taxRate);
    }

    /**
     * Update delivery tax rate
     * @param taxRateId : tax rate id
     * @param request : updated data object {@link DeliveryTaxRate}
     */
    @Override
    public DeliveryTaxRate updateDeliveryTaxRate(String taxRateId, DeliveryTaxRate request) {
        DeliveryTaxRate dbTaxRate = deliveryTaxRateRepository.get(token.getCompanyId(), taxRateId);
        if (dbTaxRate == null) {
            throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, DELIVERY_TAX_RATE_NOT_FOUND);
        }

        if (request.getRegionIds() == null || request.getRegionIds().size() == 0) {
            throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, REGION_NOT_AVAILABLE);
        }

        List<ObjectId> regionIds = new ArrayList<>();
        for (String regionId : request.getRegionIds()) {
            if (ObjectId.isValid(regionId)) {
                regionIds.add(new ObjectId(regionId));
            }
        }


        HashMap<String, BlazeRegion> regionMap = blazeRegionRepository.listAsMap(token.getCompanyId(), regionIds);
        for (String regionId : request.getRegionIds()) {
            BlazeRegion blazeRegion = regionMap.get(regionId);
            if (blazeRegion == null) {
                throw new BlazeInvalidArgException(DELIVERY_TAX_RATE, REGION_NOT_FOUND);
            }
        }

        dbTaxRate.prepare(token.getCompanyId());
        dbTaxRate.setShopId(token.getShopId());
        dbTaxRate.setName(request.getName());
        dbTaxRate.setActive(request.isActive());
        dbTaxRate.setRegionIds(request.getRegionIds());
        dbTaxRate.setUseComplexTax(request.isUseComplexTax());
        dbTaxRate.setTaxOrder(request.getTaxOrder());
        dbTaxRate.setTaxInfo(request.getTaxInfo());
        dbTaxRate.setTaxTables(request.getTaxTables());
        this.prepareTaxTable(dbTaxRate);
        this.prepareTaxInfo(dbTaxRate);

        return deliveryTaxRateRepository.update(token.getCompanyId(), taxRateId, dbTaxRate);
    }

    private void prepareTaxInfo(DeliveryTaxRate taxRate) {
        if (taxRate.getTaxInfo() != null) {
            taxRate.getTaxInfo().prepare();
        }
    }

    private void prepareTaxTable(DeliveryTaxRate taxRate) {
        if (taxRate.getTaxTables() != null) {
            for (CompoundTaxTable taxTable : taxRate.getTaxTables()) {
                taxTable.prepare(token.getCompanyId());
                taxTable.setShopId(token.getShopId());
                taxTable.reset();
            }
        }
        if (taxRate.getTaxTables() == null || taxRate.getTaxTables().size() == 0) {
            List<CompoundTaxTable> taxTables = new ArrayList<>();
            taxRate.setTaxTables(taxTables);

            ConsumerType[] consumerTypes = ConsumerType.values();
            for (ConsumerType consumerType : consumerTypes) {
                if (consumerType == ConsumerType.Other) {
                    continue;
                }
                CompoundTaxTable taxTable = new CompoundTaxTable();
                taxTable.prepare(token.getCompanyId());
                taxTable.setShopId(token.getShopId());
                taxTable.setName(consumerType.getDisplayName());
                taxTable.setConsumerType(consumerType);
                taxTables.add(taxTable);
            }
        }
    }
}
