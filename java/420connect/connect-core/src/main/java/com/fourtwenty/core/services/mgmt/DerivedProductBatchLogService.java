package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.DerivedProductBatchInfo;
import com.fourtwenty.core.domain.models.product.DerivedProductBatchLog;
import com.fourtwenty.core.domain.models.product.Product;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface DerivedProductBatchLogService {
    DerivedProductBatchLog createLog(Product product, BigDecimal quantity, Map<String, List<DerivedProductBatchInfo>> productMap);

    DerivedProductBatchLog getById(String companyId, String derivedLogId);

    DerivedProductBatchLog updateLog(DerivedProductBatchLog log);

    HashMap<String, DerivedProductBatchLog> listAsMap(String companyId, List<ObjectId> derivedLogIds);
}
