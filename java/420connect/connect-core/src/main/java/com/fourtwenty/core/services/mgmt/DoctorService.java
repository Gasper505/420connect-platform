package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.rest.dispensary.requests.customer.DoctorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
public interface DoctorService {
    Doctor getDoctorById(String doctorId);

    SearchResult<Doctor> getDoctorsByDate(long afterDate, long beforeDate);

    SearchResult<Doctor> searchDoctorsByTerm(String term, String physicianId, int start, int limit);

    List<Doctor> getAllDoctors();

    Doctor updateDoctor(String doctorId, Doctor doctor);

    Doctor addDoctor(DoctorAddRequest addRequest);

    Doctor sanitize(DoctorAddRequest addRequest);

    void deleteDoctor(String doctorId);
}
