package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.DriverLicenseLog;

public interface DriverLicenseLogService {
    DriverLicenseLog saveLicenseLog(String licenseDetail);
}
