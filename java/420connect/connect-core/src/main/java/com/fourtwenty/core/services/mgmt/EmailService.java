package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;

/**
 * Created by mdo on 8/13/16.
 */
public interface EmailService {
    void sendWelcomeEmail(Employee employee, String unencryptedPassword);

    void sendWinEmail(Company company, Employee employee, Shop shop);
}
