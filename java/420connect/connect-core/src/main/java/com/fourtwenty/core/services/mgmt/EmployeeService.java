package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeCredentialUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeePinUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeCustomResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeManifestResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;


public interface EmployeeService {
    Employee addEmployee(EmployeeAddRequest addRequest);

    Employee getEmployeeById(String employeeId);

    Employee updateEmployee(String employeeId, Employee employee);

    Employee getEmployeeByQuickPin(String companyId, String quickPin);

    void updateCredentials(String employeeId, EmployeeCredentialUpdateRequest request);

    void updatePin(String employeeId, EmployeePinUpdateRequest request);

    SearchResult<EmployeeResult> searchEmployees(String searchTerm, int start, int limit, String accessConfig, String shopId, String roleId);

    void deleteEmployee(String employeeId);

    DateSearchResult<Employee> findEmployees(long afterDate, long beforeDate);

    EmployeeManifestResult getEmployeeManifest(String date);

    String getPermissionProjection();

    SearchResult<EmployeeResult> getEmployeeList(String term, int start, int limit);

    SearchResult<EmployeeResult> getEmployeeByRole(String companyId);

    SearchResult<EmployeeCustomResult> getAllEmployeeList(String term, int start, int limit);

    EmployeeCustomResult getCustomEmployeeById(String employeeId);
}
