package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;

public interface ExciseTaxInfoService {
    ExciseTaxInfo getExciseTaxInfoForShop(Shop shop);
}
