package com.fourtwenty.core.services.mgmt;

public interface ExportService {
    StringBuilder exportProductsByShop(String shopId, String assetToken);
}
