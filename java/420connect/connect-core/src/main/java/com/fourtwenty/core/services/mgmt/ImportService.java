package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.importer.model.ProductBatchResult;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Stephen Schmidt on 1/26/2016.
 */
public interface ImportService {

    void importFiles(InputStream vendorInput, InputStream doctorInput, InputStream productInput,
                     InputStream customerInput, InputStream assetFolder, InputStream promotionFile);

    /* handle importing productsList CSV */
    Response parseBatchImport(InputStream productsList);

    Response importProductBatch(List<ProductBatchResult> productBatchResult);

    Response importMeadowProducts(InputStream inputStream);


    Response assignUniqueIds(InputStream inputStream);
}
