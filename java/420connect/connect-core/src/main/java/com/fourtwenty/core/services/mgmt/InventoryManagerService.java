package com.fourtwenty.core.services.mgmt;

import java.math.BigDecimal;

/**
 * Created by mdo on 10/15/17.
 */
public interface InventoryManagerService {
    // Everything deal with batches
    BigDecimal getCurrentQuantityForProduct(String companyId, String shopId, String productId);

    BigDecimal getCurrentQuantityForBatch(String companyId, String shopId, String productId,
                                          String batchId);

    BigDecimal getCurrentQuantityForInventory(String companyId, String shopId, String productId,
                                              String inventoryId);

    BigDecimal getCurrentQuantityForBatchNInventory(String companyId, String shopId, String productId,
                                                    String inventoryId,
                                                    String batchId);


    /**
     * Add the specific quantity to the batch for a particular productId
     * <p>
     * If batchId is not specified, then the latest most recent batch will be used.
     *
     * @param companyId
     * @param shopId
     * @param productId
     * @param inventoryId
     * @param batchId
     * @return
     */
    BigDecimal addQuantityToBatch(String companyId,
                                  String shopId,
                                  String productId,
                                  String inventoryId,
                                  String batchId,
                                  BigDecimal quantityToAdd);

    /**
     * Deduct the specific quantity to the batch for a particular productId
     * <p>
     * If batchId is not specified, then the latest most recent batch will be used.
     *
     * @param companyId
     * @param shopId
     * @param productId
     * @param inventoryId
     * @param batchId
     * @return
     */
    BigDecimal deductQuantityFromBatch(String companyId,
                                       String shopId,
                                       String productId,
                                       String inventoryId,
                                       String batchId,
                                       BigDecimal quantityToDeduct);

}
