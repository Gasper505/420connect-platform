package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.FilterType;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.ProductBatchQueuedTransaction;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesResult;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchByCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchQuantityResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryShopBaseResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ReconciliationHistoryResult;


import java.math.BigDecimal;
import java.util.List;

/**
 * Created by mdo on 10/26/15.
 */
public interface InventoryService {
    // Inventory
    SearchResult<Inventory> getShopInventories(String shopId);

    DateSearchResult<Inventory> getCompanyInventories(long afterDate, long beforeDate);

    Inventory addInventory(InventoryAddRequest request);

    Inventory updateInventory(String inventoryId, InventoryUpdateRequest request);

    void deleteInventory(String inventoryId);

    void deleteInventory(InventoryDeleteRequest deleteRequest);

    void addNewBatchInventoryQuantity(ProductBatch productBatch, Product dbProduct, String receivingInventoryId, Inventory.InventoryType type);

    // Batches
    ProductBatch addBatch(BatchAddRequest request);

    ProductBatch updateBatch(String batchId, UpdateProductBatchRequest batchToUpdate);

    SearchResult<ProductBatch> getBatches(String productId, int start, int limit, String term, ProductBatch.BatchStatus status);

    DateSearchResult<ProductBatch> getBatchesWithDates(long afterDate, long beforeDate);

    void deleteBatch(String batchId);

    ProductBatch publish(String batchId);

    ProductBatch unpublish(String batchId);

    ProductBatch archive(BatchArchiveAddRequest request);

    // Inventory
    void bulkTransferInventory(BulkInventoryTransferRequest request);

    void reportLoss(ReportLossRequest request);

    void inventoryReconciliation(ReportLossListRequest request);

    void takeSnapshot();

    void resetInventoryToSafe(ResetInventoryRequest request);

    SearchResult<BatchQuantityResult> getBatchByLabel(String inventoryId, String label, String productId, int start, int limit);

    ProductBatch getOldestBatchByProductId(String productId, String transactionId);

    SearchResult<ReconciliationHistoryResult> getAllReconciliationHistory(int start, int limit);


    ReconciliationHistoryResult getReconciliationHistoryById(String historyId);

    BigDecimal calculateExciseTaxByUnitCost(ExciseTaxCalculateRequest request);

    void calculateExciseTaxForBatch(ProductBatch productBatch, BigDecimal costPerUnit, boolean isCannabis);

    List<InventoryShopBaseResult> getInventoriesPerShop();

    void compositeAddInventoryTransfer(List<BulkInventoryTransferRequest> request);


    void inventoryReconciliationByBatch(ReportLossListRequest request);

    SearchResult<ProductBatchQuantityResult> getProductBatchesWithQuantity(String productId, int start, int limit);

    ReconciliationHistoryResult getReconciliationByNo(String reconciliationNo);

    void createQueuedTransactionJobForBatchQuantity(ProductBatch productBatch, Inventory.InventoryType inventoryType, ProductBatchQueuedTransaction.OperationType operationType, BigDecimal quantity, String sourceChildId, InventoryOperation.SubSourceAction subSourceAction);

    AssetStreamResult getProductBatchQRAsset(String batchId, String assetToken);

    long getPendingStatusForQueuedTransaction();

    void unArchiveBatch(String batchId);

    List<BatchByCategoryResult> getProductBatchesByCategory(String categoryId, String inventoryId, FilterType filterType);

    GetComplianceBatchesResult getAllComplianceBatches(boolean activeBatches, boolean onHoldBatches, boolean inactiveBatches);

    Long getLastReconciliation(String shopId);

    ProductBatch createDerivedProductBatch(DerivedBatchAddRequest request);

    BigDecimal calculateEstimateCogs(List<BatchBundleItemsRequest> items);

}
