package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Raja Dushyant Vashishtha
 */
public interface InventoryTransferHistoryService {
    InventoryTransferHistoryResult getInventoryTransferHistoryById(String historyId);

    InventoryTransferHistory bulkTransferInventory(InventoryTransferHistory request);

    SearchResult<InventoryTransferHistoryResult> getInventoryHistories(int start, int limit, String status, String startDate, String endDate);

    InventoryTransferHistory updateInventoryHistoryResult(String historyId, InventoryTransferHistory transferHistory);

    InventoryTransferHistory updateInventoryTransferStatus(String historyId, InventoryTransferStatusRequest status);


    InventoryTransferHistory bulkTransferInventoryByBatches(InventoryTransferHistory request);


    DateSearchResult<InventoryTransferHistoryResult> getInventoryTransferByDate(long afterDate, long beforeDate);

    List<BulkPostResponse> compositeAddInventoryTransfer(List<InventoryTransferHistory> inventoryTransferHistoryList);

    InventoryTransferHistoryResult getInventoryTransferHistoryByNo(String transferNo);

    InputStream createPdfForShippingManifest(String transferId);

    InputStream createManifestForBulk(String transferId);

    SearchResult<InventoryTransferHistoryResult> getAllInventoryHistories(int start, int limit, String status, String startDate, String endDate, final String searchTerm);

    void resetInventoryToSafe(String productId);
}
