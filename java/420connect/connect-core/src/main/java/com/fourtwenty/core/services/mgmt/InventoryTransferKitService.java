package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.InventoryTransferKit;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface InventoryTransferKitService {
    SearchResult<InventoryTransferKit> getInventoryTransferKits(int start, int limit);

    InventoryTransferKit getInventoryTransferKitById(String id);

    InventoryTransferKit createInventoryTransferKit(InventoryTransferKit kit);

    InventoryTransferKit updateInventoryTransferKit(String id, InventoryTransferKit kit);

    void deleteInventoryTransferKit(String id);
}
