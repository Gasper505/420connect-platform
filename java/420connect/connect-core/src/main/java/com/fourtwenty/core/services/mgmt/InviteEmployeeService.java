package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.InviteEmployee;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteTokenRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.RegisterInviteeRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by decipher on 22/11/17 12:03 PM
 * Raja (Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface InviteEmployeeService {
    InviteEmployee inviteEmployee(EmployeeInviteRequest request);

    InviteEmployee registerInvitee(RegisterInviteeRequest registerInviteeRequest);

    InviteEmployee getInviteeInfo(EmployeeInviteTokenRequest employeeInviteTokenRequest);

    SearchResult<InviteEmployee> getInviteEmployeeList(int start, int limit);

    InviteEmployee getInviteEmployeeById(String employeeId);

    void resendInvite(String inviteEmployeeId);
}
