package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;

/**
 * Created by mdo on 8/8/17.
 */
public interface LoyaltyActivityService {
    LoyaltyActivityLog addLoyaltyPoints(String companyId, Transaction transaction);

    LoyaltyActivityLog refundLoyaltyPoints(String companyId, Transaction transaction);

    boolean isUsed(String companyId, String memberId, String rewardId);
}
