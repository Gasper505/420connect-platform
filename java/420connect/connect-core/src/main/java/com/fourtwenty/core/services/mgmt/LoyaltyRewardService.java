package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.rest.dispensary.requests.promotions.RewardAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyActivityLogResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyMemberReward;

/**
 * Created by mdo on 8/3/17.
 */
public interface LoyaltyRewardService {

    SearchResult<LoyaltyReward> getAllRewards(int start, int limit);

    LoyaltyReward getRewardById(String rewardId);

    LoyaltyReward addReward(RewardAddRequest request);

    LoyaltyReward updateReward(String rewardId, LoyaltyReward reward);

    void deleteReward(String rewardId);

    LoyaltyReward publishReward(String rewardId);

    LoyaltyReward unpublishReward(String rewardId);


    //Get loyaltyrewards for members
    SearchResult<LoyaltyMemberReward> getRewardsForMembers(String memberId);

    SearchResult<LoyaltyMemberReward> getRewardsForMembers(String companyId, String shopId, Member member);

    DateSearchResult<LoyaltyReward> getRewardsByDate(long afterDate, long beforeDate);

    DateSearchResult<LoyaltyActivityLog> getLoyaltyUsageReward(long afterDate, long beforeDate);

    SearchResult<LoyaltyActivityLogResult> getLoyaltyUsageReward(LoyaltyActivityLog.LoyaltyActivityType activityType, int start, int limit);
}
