package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.rest.dispensary.requests.marketing.MarketingJobAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.marketing.MarketingJobMemberCountRequest;
import com.fourtwenty.core.rest.dispensary.results.MarketingMemberCountResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.marketing.MarketingJobResult;

/**
 * Created by mdo on 7/6/17.
 */
public interface MarketingJobService {
    SearchResult<MarketingJobResult> getCompletedJobs(int start, int limit);

    SearchResult<MarketingJobResult> getPendingJobs(int start, int limit);

    MarketingJob createMarketingJob(MarketingJobAddRequest addRequest);

    MarketingJob duplicateMarketingJob(String marketingJobId);

    MarketingJob updateMarketingJob(String marketingJobId, MarketingJob marketingJob);

    void removeMarketingJob(String marketingJobId);

    MarketingMemberCountResult getMemberCountForMarketingJob(MarketingJobMemberCountRequest request);
}
