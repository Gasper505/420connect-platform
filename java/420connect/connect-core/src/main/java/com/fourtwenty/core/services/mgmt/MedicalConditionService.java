package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.rest.dispensary.results.ListResult;

/**
 * Created by mdo on 11/9/15.
 */
public interface MedicalConditionService {
    ListResult<MedicalCondition> getAllMedicalConditions();
}
