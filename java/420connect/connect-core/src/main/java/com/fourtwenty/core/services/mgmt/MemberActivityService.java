package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.customer.MemberActivity;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface MemberActivityService {
    MemberActivity addMemberActivity(String companyId, String shopId, String employeeId, String message, String memberId);

    SearchResult<MemberActivity> getMemberActivities(String companyId, String shopId, String memberId, int start, int limit);
}
