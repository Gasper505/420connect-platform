package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;

/**
 * Created by mdo on 7/11/16.
 */
public interface MemberGroupService {
    DateSearchResult<MemberGroup> getMemberGroupsByDate(long afterDate, long beforeDate);


}
