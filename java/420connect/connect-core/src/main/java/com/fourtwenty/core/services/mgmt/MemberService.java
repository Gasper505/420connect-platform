package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.DriverLicense;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.CareGiver;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.DriverLicenseRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.MemberLoyaltyAdjustRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.InactiveMemberResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;

import java.util.HashMap;
import java.util.List;


public interface MemberService {
    SearchResult<MemberLimitedView> getMembershipsForActiveShop(String term, int start, int limit);

    SearchResult<MemberLimitedView> getMembersForDoctorId(String doctorId, String text, int start, int limit);

    Member updateMembership(String memberId, MembershipUpdateRequest updateRequest);

    void removeMembership(String memberId);

    Member addMembership(MembershipAddRequest addRequest);
    Member addAnonymousMember(String ticketNumber, ConsumerType consumerType);

    Member addOrUpdateMember(Member member, MembershipAddRequest addRequest, SignedContract signedContract);

    MemberResult getMembership(String membershipId, boolean addCannabisLimits, boolean addActivities);

    Member updateLoyaltyPoint(String memberId, MemberLoyaltyAdjustRequest request);

    Member sanitize(MembershipAddRequest addRequest, HashMap<String, Doctor> doctorMap) throws BlazeInvalidArgException;

    SearchResult<MemberLimitedView> prepareDataForView(SearchResult<Member> memberSearchResult, boolean limitView);

    HashMap<String, String> getMemberNameMap(String companyId);

    InactiveMemberResult getInactiveMembers(int daysInactive);

    void bulkUpdateMembers(MembershipBulkUpdateRequest membershipBulkUpdateRequest);

    SearchResult<CareGiver> getCareGivers(int start, int limit);

    CareGiver getCareGiverById(String careGiverId);

    CareGiver addCareGiver(CareGiver careGiver);

    List<Member> getMembersOfCareGiver(String careGiverId);

    CareGiver updateCareGiver(String careGiverId, CareGiver careGiver);

    void deleteCareGiver(String careGiverId);

    Member updateMemberBanStatus(String memberId, StatusRequest statusRequest);

    DriverLicense parseDriverLicense(DriverLicenseRequest request);

    List<String> getAllMemberTags();

    void updateConsumerUserInfo(ConsumerUser consumerUser, Member member);

    Member getMemberByEmail(String email);

    SearchResult<Member> getMembersByLicenceNo(String licenceNumber);
}
