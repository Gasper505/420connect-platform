package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.request.OnlineConsumerUserRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.DeclineRequest;
import com.fourtwenty.core.rest.dispensary.results.OnlineUserResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface OnlineCustomerService {
    ConsumerUser getConsumerUserById(String userId);

    SearchResult<OnlineUserResult> findConsumerUser(int start, int limit, OnlineConsumerUserRequest.OnlineUserQueryType queryType, String marketingSource, String term);

    ConsumerUser acceptConsumerUser(String userId, ConsumerUser consumerUser);

    ConsumerUser declineConsumerUser(String userId, DeclineRequest request);
}
