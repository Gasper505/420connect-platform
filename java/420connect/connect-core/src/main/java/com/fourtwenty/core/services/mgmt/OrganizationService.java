package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Organization;
import java.util.List;

public interface OrganizationService {

    List<Organization> getOrganizations();

    List<Organization> getCurrentEmployeeOrganizations();

    Organization addOrganization(Organization organization);
}