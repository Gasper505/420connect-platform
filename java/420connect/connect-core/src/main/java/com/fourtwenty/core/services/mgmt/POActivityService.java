package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by decipher on 4/10/17 5:37 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface POActivityService {
    POActivity addPOActivity(String purchaseOrderId, String employeeId, String log);

    SearchResult<POActivity> getAllPOActivityList(int start, int limit, String purchaseOrderId);

    POActivity addActivityComment(ActivityCommentRequest request);
}
