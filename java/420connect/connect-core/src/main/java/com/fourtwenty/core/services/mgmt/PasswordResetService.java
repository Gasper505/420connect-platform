package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.PasswordReset;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetUpdateRequest;

/**
 * Created by mdo on 8/3/16.
 */
public interface PasswordResetService {
    PasswordReset requestPasswordReset(PasswordResetRequest request);

    void updatePassword(PasswordResetUpdateRequest request);
}
