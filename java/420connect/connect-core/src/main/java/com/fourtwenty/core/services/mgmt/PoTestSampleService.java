package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;

import java.util.List;

public interface PoTestSampleService {
    List<TestSample> addTestSamples(List<TestSampleAddRequest> requestList);
}
