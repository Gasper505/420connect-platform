package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemReduceRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemUpdateStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageGroupResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageLineItemInfo;

import java.util.Map;

/**
 * Created by mdo on 3/6/17.
 */
public interface PrepackageProductService {
    DateSearchResult<Prepackage> getPrepackagesWithDate(long afterDate, long beforeDate);

    DateSearchResult<PrepackageProductItem> getPrepackagesWithDateItems(long afterDate, long beforeDate);

    DateSearchResult<ProductPrepackageQuantity> getPrepackageQuantitiesWithDate(long afterDate, long beforeDate);

    SearchResult<PrepackageProductItem> getPrepackages(String productId);

    SearchResult<PrepackageGroupResult> getPrepackagesGroup(String productId, String shopId);

    PrepackageProductItem addPrepackageLineItem(String prepackageId, PrepackageItemAddRequest prepackageItemAddRequest);

    PrepackageProductItem reducePrepackageLineItem(String prepackageId, PrepackageItemReduceRequest reduceRequest);

    PrepackageProductItem deletePrepackageLineItem(String prepackageLineItemId);

    PrepackageProductItem updatePrepackageItemStatus(String prepackageLineItemId, PrepackageItemUpdateStatusRequest request);

    // Prepackage Groups
    Prepackage addPrepackage(PrepackageAddRequest addRequest);

    Prepackage updatePrepackage(String prepackageId, Prepackage prepackage);

    void deletePrepackage(String prepackageId);

    Map<String, SearchResult<PrepackageGroupResult>> getPrepackagesByCategoryId(String categoryId);

    Map<String, PrepackageLineItemInfo> getPrepackageLineItemInfoById(String prepackageItemsIds);
}
