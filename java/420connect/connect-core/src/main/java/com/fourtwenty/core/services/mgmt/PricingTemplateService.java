package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.PricingTemplate;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductPriceBreakResult;

public interface PricingTemplateService {
    PricingTemplate createPricingTemplate(PricingTemplate pricingTemplate);

    PricingTemplate updatePricingTemplate(String pricingTemplateId, PricingTemplate pricingTemplate);

    PricingTemplate getPricingTemplateById(String pricingTemplateId);

    void deletePricingTemplateById(String pricingTemplateId);

    SearchResult<PricingTemplate> getPricingTemplates(int start, int limit);

    ProductPriceBreakResult getPriceBreaksForUnitType(String weightPerUnit);
}
