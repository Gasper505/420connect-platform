package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.importer.model.CustomProductBatchResult;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkProductBatchUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductBatchScannedRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductBatchStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;
import java.util.Map;

public interface ProductBatchService {

    ProductBatchResult getProductBatch(String productBatchId);

    SearchResult<ProductBatchResult> getAllProductBatches(int start, int limit, final String searchTerm, final ProductBatch.BatchFilter filter);

    SearchResult<ProductBatchResult> getAllArchivedProductBatches(int start, int limit);

    ProductBatch updateProductBatchStatus(String productBatchId, ProductBatchStatusRequest request);

    SearchResult<ProductBatchResult> getAllProductBatchesByState(int start, int limit, boolean state);

    SearchResult<ProductBatchResult> getAllProductBatchesByStatus(int start, int limit, ProductBatch.BatchStatus status, final String searchTerm, final ProductBatch.BatchFilter filter);

    SearchResult<ProductBatchResult> getAllScannedProductBatch(ProductBatchScannedRequest request, int start, int limit);

    SearchResult<ProductBatchResult> getAllDeletedProductBatch(int start, int limit);

    void bulkProductBatchUpdate(BulkProductBatchUpdateRequest request);

    ProductBatch updateProductBatchVoidStatus(String productBatchId, boolean voidStatus);

    SearchResult<ProductBatchResult> getAllProductBatchesByVoidStatus(int start, int limit, boolean voidStatus);

    void batchMoveToSale(ProductBatch batch, ProductBatchStatusRequest request);

    void prepareProductBatchResult(List<ProductBatchResult> productBatchResults);

    CustomProductBatchResult getProductBatchWithProduct(String productBatchId);

    SearchResult<CustomProductBatchResult> getAllProductBatchesWithProduct(int start, int limit, String searchTerm, ProductBatch.BatchFilter filter);

    Map<String, ProductBatch> getBatchesByBatchesId(List<String> batchIdList);
}
