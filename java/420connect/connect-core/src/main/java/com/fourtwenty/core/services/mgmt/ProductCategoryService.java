package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryUpdateOrderRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 10/14/15.
 */
public interface ProductCategoryService {
    ListResult<ProductCategory> getProductCategories(String shopId);

    SearchResult<ProductCategory> getProductCategories(long afterDate, long beforeDate);

    ProductCategory createProductCategory(ProductCategoryAddRequest request);

    ProductCategory updateProductCategory(String productCategoryId, ProductCategory request);

    void deleteProductCategory(String productCategoryId);

    ListResult<ProductCategory> updateCategoryOrders(ProductCategoryUpdateOrderRequest request);

    ProductCategory getProductCategoryById(String productCategoryId);
}
