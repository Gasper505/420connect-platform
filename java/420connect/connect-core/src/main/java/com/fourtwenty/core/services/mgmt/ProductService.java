package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.FilterType;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Stephen Schmidt on 10/6/2015.
 */
public interface ProductService {
    ProductResult getProductById(String productId);

    Product addProduct(ProductAddRequest addRequest);

    Product sanitize(ProductAddRequest importRequest, HashMap<String, Vendor> vendorHashMap);

    Product updateProduct(String productId, Product product);

    SearchResult<ProductResult> searchProductsByCategoryId(String shopId, String categoryId, String productId,
                                                           int start, int limit, boolean quantity, String term, String inventoryId, FilterType status, String batchSku);

    SearchResult<ProductLimitedResult> getProductListLimited();

    DateSearchResult<Product> getProducts(long afterDate, long beforeDate);

    void deleteProduct(String productId);

    List<ProductWeightTolerance> getWeightTolerances();

    HashMap<String, String> getProductNameMap(String companyId, String shopId);

    SearchResult<Product> getProductsByIds(List<String> productIds);

    SearchResult<Product> getProductsByVendorId(String vendorId, boolean active);

    Product getProductByBatchSKU(String batchSKU);

    // Conversion
    Product convertToProduct(String productId, InventoryConvertToProductRequest request);

    SearchResult<Product> copyProductsToShop(CopyProductRequest copyRequest);

    // MemberGroupPrices
    SearchResult<MemberGroupPricesResult> getMemberGroupPrices(String productId);

    DateSearchResult<MemberGroupPrices> getMemberGroupPrices(long afterDate, long beforeDate);

    MemberGroupPricesResult getGroupPricesForMember(String productId, String memberId);

    MemberGroupPricesResult addMemberGroupPrices(String productId, MemberGroupPricesAddRequest addRequest);

    MemberGroupPricesResult updatePrices(String productId, String pricesId, MemberGroupPrices groupPrices);

    void deleteMemberGroupPrices(String productId, String groupPricesId);

    SearchResult<ProductCustomResult> searchAllProducts(String shopId, String categoryId, int start, int limit, String term, FilterType status);

    void bulkProductUpdates(ProductBulkUpdateRequest productUpdateRequest);

    Product createProductFromExisting(CreateProductFromExisting request);

    ProductQuantityInfo getQuantityInfo(String productId);

    DateSearchResult<Product> getProductsByDates(long afterDate, long beforeDate);
    DateSearchResult<Brand> getBrandsByDates(long afterDate, long beforeDate);

    SearchResult<ProductResult> getAllProductsByVendorBrand(String vendorId, int start, int limit);

    List<ProductPriceRange> populatePriceRanges(Product product, List<ProductPriceRange> priceRanges, String memberGroupId, ArrayList<ProductWeightTolerance> weightTolerancesList);

    List<ProductPriceBreak> populatePriceBreaks(Product product, ProductCategory category, List<ProductPriceBreak> priceBreaks,
                                                boolean isDefault, ArrayList<ProductWeightTolerance> weightTolerancesList, boolean reset, Boolean isPricingTemplate);

    void updateMemberGroupPrices(PricingTemplate pricingTemplate);

    void removePricingTemplate(String companyId, String shopId, PricingTemplate pricingTemplate);

    void applyPricingTemplateUpdate(PricingTemplate pricingTemplate);

    SearchResult<LimitedProductResult> searchLimitedProductsByCategoryId(String shopId, String categoryId, String productId,
                                                                         int start, int limit, boolean quantity, String term, String inventoryId, FilterType status, String batchSku);
}
