package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.OrderedDiscountable;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.engine.SuggestedPromotion;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.loyalty.*;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * Created by mdo on 12/15/17.
 */
public interface PromotionProcessService {
    SearchResult<SuggestedPromotion> getSuggestedPromotions(String companyId, String shopId, Transaction transaction);

    Promotion findPromotionWithCode(String companyId, String shopId, String promoCode);

    void applyPromo(final Shop shop,
                    final Transaction transaction,
                    final Cart workingCart,
                    final HashMap<String, Product> productHashMap,
                    final Transaction.TransactionStatus incomingStatus,
                    final HashMap<String, ProductCategory> productCategoryHashMap,
                    final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                    final OrderedDiscountable discountable,
                    final Member member);

    BigDecimal getRewardDiscountForProduct(final LoyaltyReward reward, OrderItem orderItem,
                                           final HashMap<String, Product> productHashMap,
                                           final HashMap<String, ProductCategory> productCategoryHashMap,
                                           final HashMap<String, ProductWeightTolerance> weightToleranceHashMap);

    BigDecimal getRewardDiscountAmountForProduct(final LoyaltyReward reward, OrderItem orderItem,
                                                        final HashMap<String, Product> productHashMap,
                                                        final HashMap<String, ProductCategory> productCategoryHashMap,
                                                        final HashMap<String, ProductWeightTolerance> weightToleranceHashMap);
    
    PromotionReqLog createPromotionLog(String promotionId, String finalizedDiscountId, String companyId, double discountCashAmt);
}
