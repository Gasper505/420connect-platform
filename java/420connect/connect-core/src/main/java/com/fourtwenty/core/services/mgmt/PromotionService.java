package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.promotion.PromotionResult;

/**
 * Created by mdo on 1/3/17.
 */
public interface PromotionService {
    Promotion findPromotionWithCode(String promoCode);

    Promotion addPromotion(PromotionAddRequest request);

    PromotionResult updatePromotion(String promotionId, PromotionRequest promotionRequest);

    void deletePromotion(String promotionId);


    // Getters
    PromotionResult getPromotionById(String promotionId);

    DateSearchResult<Promotion> getPromotionsByDate(long afterDate, long beforeDate);

    SearchResult<Promotion> getPromotions(int start, int limit);
}
