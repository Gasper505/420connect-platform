package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.purchaseorder.AdjustmentInfo;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrderLimitedResult;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.ShipmentBillResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderResult;
import com.fourtwenty.core.rest.purchaseorders.POCompleteRequest;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import org.bson.types.ObjectId;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by decipher on 3/10/17 3:58 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface PurchaseOrderService {
    PurchaseOrder addPurchaseOrder(PurchaseOrderAddRequest purchaseOrderAddRequest);

    PurchaseOrder getPurchaseOrderById(String purchaseOrderId);

    SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(int start, int limit, PurchaseOrder.CustomerType customerType, String searchTerm, String vendorId,PurchaseOrder.PurchaseOrderSort sortOption);

    PurchaseOrderItemResult updatePOProductRequestStatus(String poID, POProductRequestUpdate poProductRequestUpdate);

    PurchaseOrder updatePurchaseOrderStatus(String poID, POStatusUpdateRequest poStatusUpdateRequest);

    PurchaseOrder updatePOManagerApproval(String poID, POManagerApprovalRequest poManagerApprovalRequest);

    PurchaseOrder updatePurchaseOrder(String poID, PurchaseOrder purchaseOrderAddRequest);

    PurchaseOrder addPOAttachment(String poID, POAttachmentRequest poAttachmentRequest);

    CompanyAsset getPOAttachment(String purchaseOrderId, String attachmentId);

    PurchaseOrder updatePOAttachment(String purchaseOrderId, String attachmentId, POAttachmentRequest poAttachmentRequest);

    void deletePOAttachment(String purchaseOrderId, String attachmentId);

    PurchaseOrder receivePurchaseOrder(String purchaseOrderId);

    PurchaseOrder markPurchaseOrder(String purchaseOrderId);

    SearchResult<PurchaseOrderItemResult> getPOListByStatus(int start, int limit, String status, PurchaseOrder.CustomerType customerType, String searchTerm, PurchaseOrder.PurchaseOrderSort sortOption);

    ShipmentBillResult completeShipmentArrival(String poId, POCompleteRequest request);

    void sendEmailToAccounting(String poId, EmailRequest emailRequest);

    PurchaseOrderItemResult getPurchaseOrderItemById(String purchaseOrderId);

    PurchaseOrder saveAsPendingPO(String poId, PurchaseOrder purchaseOrder);

    MetrcVerifiedPackage getMetricPackageByLabel(String label);

    SearchResult<PurchaseOrderItemResult> getArchivedPurchaseOrder(int start, int limit, PurchaseOrder.CustomerType customerType, final String term);

    PurchaseOrder archivePurchaseOrder(String purchaseOrderId);

    void emailPOToVendor(String purchaseOrderId);

    PurchaseOrderItemResult updateAllPOProductRequestStatus(String poID, List<POProductRequestUpdate> poProductRequestUpdates);

    TaxResult calculateArmsLengthExciseTax(Shop shop, List<POProductRequest> poProductRequest, Map<String, Product> productMap, List<ObjectId> productCategoryIds, boolean isPurchaseOrder);

    void emailPOToCustomerCompany(String purchaseOrderId);

    PurchaseOrder preparePurchaseOrder(PurchaseOrderAddRequest purchaseOrderRequest);

    BigDecimal processAdjustments(List<AdjustmentInfo> adjustmentInfoList, BigDecimal reqGrandTotal);

    InputStream createPdfForPurchaseOrder(final String purchaseOrderId);

    DateSearchResult<PurchaseOrderResult> getAllPOByDates(long startDate, long endDate);

    List<BulkPostResponse> compositePOUpdate(List<PurchaseOrder> request);

    TaxResult calculateCultivationTax(TaxResult taxResult, List<POProductRequest> productRequestList, PurchaseOrder purchaseOrder, HashMap<String, Product> productMap, List<ObjectId> productCategoryId);

    void returnToVendorFromPOs(VendorReturnRequest request);

    SearchResult<PurchaseOrder> getPurchaseOrdersByProductId(final int start, final int limit, final String sortOptions, final String productId);

    PurchaseOrderItemResult getPurchaseOrderByNo(String poNumber);

    InputStream generatePoShippingManifest(String purchaseOrderId);

    List<PurchaseOrderLimitedResult> getPoLimitedDetails(PurchaseOrder.PurchaseOrderStatus status, List<String> vendorIds, List<String> productIds);

    PurchaseOrder copyPurchaseOrder(String purchaseOrderId);
}
