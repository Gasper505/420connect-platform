package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.receipt.ReceiptInfo;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.transaction.TransactionLimitedResult;
import com.fourtwenty.core.rest.dispensary.requests.queues.*;
import com.fourtwenty.core.rest.dispensary.requests.shop.SendReceiptRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.DeliveryLocationRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.AddOrderTagsRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.OrderScheduleUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.PaymentOptionUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.*;
import com.fourtwenty.core.rest.dispensary.results.queue.CompositeMemberTransResult;
import com.fourtwenty.core.rest.dispensary.results.shop.AllQueueResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;

import java.io.InputStream;
import java.util.List;

/**
 * Created by mdo on 9/6/17.
 */
public interface QueuePOSService {

    // Transactions
    AllQueueResult getTransactionsWithDate();

    Transaction getTransactionById(String transactionId);

    Transaction getTransactionByTransNo(String transNo);

    SearchResult<ConsumerCartResult> getIncomingOrders(int start, int limit);

    SearchResult<ConsumerCartResult> getRejectedOrders(int start, int limit);

    DateSearchResult<Transaction> getTransactionsForMembers(String memberId, long afterDate, long beforeDate);

    SearchResult<Transaction> getTransactions(String queueName, String transNo, String transactionId, String memberId, boolean active, int start, int limit);

    SearchResult<Transaction> getCompletedTransactionsWithDates(String transNo, String transactionId, String memberId, String startDate, String endDate, int timezoneOffset);

    DateSearchResult<Transaction> getTransactions(long afterDate, long beforeDate);

    SearchResult<Transaction> findRecentTransactions(String transNo);

    Transaction addToQueue(String transID, String queueName, QueueAddMemberRequest request);

    Transaction addToQueue(String queueName, QueueAddMemberRequest request, long ticketNumber);
    CompositeMemberTransResult addAnonymousUserToQueue(String queueName, AnonymousQueueAddRequest request);

    void deleteFromQueue(String transactionId, TransactionDeleteRequest deleteRequest);


    Transaction signTransaction(String transactionId, InputStream inputStream, String fileName, String mimeType);

    Transaction completeTransaction(String transactionId, Transaction transaction, boolean avoidCannabis);
    Transaction markAsPaid(String transactionId, final Cart.PaymentOption paymentOption);
    Transaction markAsUnpaid(String transactionId, final Cart.PaymentOption paymentOption);

    Transaction prepareCart(String transactionId, Transaction transaction);

    Transaction prepareRefundCart(String transactionId, RefundRequest request);

    Transaction holdTransaction(String transactionId, Transaction transaction);
    Transaction claimTransaction(String transactionId);


    Transaction exitTransaction(String transactionId);

    Transaction startTransaction(String transactionId, boolean reassignEmployee);

    Transaction stopTransaction(String transactionId);

    Transaction refundTransaction(String transactionId, RefundRequest request);

    Transaction updateProcessedTime(String transactionId, UpdateProcessedTimeRequest request);

    Transaction updateTransactionNote(String transactionId, UpdateTransMemoRequest memoRequest);

    Transaction reassignEmployee(String transactionId, EmployeeReassignRequest request);

    Transaction changeQueueType(String transactionId, Transaction transaction);
    Transaction unassignedTransaction(String transactionId);

    void sendToMetrc(String transactionId);
    void resendToMetrc(long startDate, long endDate, boolean submitErrors);

    //Incoming Orders
    Transaction acceptIncomingOrder(String consumerCartId, EmployeeReassignRequest request);

    ConsumerCartResult declineIncomingOrder(String consumerCartId, DeclineRequest request);

    ConsumerCartResult acceptIncomingMemberStatus(String consumerCartId, ConsumerCartResult consumerCart);

    ConsumerCartResult declineIncomingMemberStatus(String consumerCartId, DeclineRequest request);

    ConsumerCartResult undoRejectedIncomingOrder(String consumerCartId);

    ConsumerCartResult updateTrackingStatus(String consumerCartId, ConsumerOrderUpdateETAStatusRequest request);

    DateSearchResult<ConsumerCartResult> getConsumerOrders(long afterDate, long beforeDate);


    // Receipts
    void sendTransactionReceipt(String transactionId, SendReceiptRequest receiptRequest, Boolean isDeliveryManifest);

    //Save Transaction
    Transaction setTransactionToFulfill(Transaction transaction, String transactionId);

    //Delivery Routes
    Transaction updateDeliveryStartRoute(final String transactionId, final DeliveryLocationRequest locationRequest);

    Transaction updateDeliveryEndRoute(final String transactionId, final DeliveryLocationRequest locationRequest);

    SearchResult<TransactionResult> getTransactionsForShop(TransactionsRequest.QueryType status, int start, int limit, String memberId, String startDate, String endDate);

    String getTransactionReceipt(String transactionId, int width, ReceiptInfo.ReceiptType receiptType);

    Transaction unlockTransaction(String transactionId, StatusRequest request);

    Transaction handleAddToQueueForBulk(String transID, String queueName, QueueAddMemberRequest request);

    SearchResult<Transaction> getTransactionByStatus(TransactionsRequest.AssignStatus status, int start, int limit, String term, long afterDate, long beforeDate);

    Transaction completeTransaction(String transactionId, Transaction transaction, boolean avoidCannabis, boolean fromBulk);

    TransactionCountResult getOrdersCount();

    MemberSalesDetail getMemberSalesDetail(String memberId);

    List<Cart.PaymentOption> getRefundPaymentOptions(String transactionId);

    SearchResult<QueuedTransaction> getQueuedTransactions(String transactionId);

    Transaction holdUnAssignedTransaction(String transactionId, Transaction transaction);

    Transaction addOrUpdateOrderTag(String transactionId, AddOrderTagsRequest request);

    Transaction updatePackedBy(String transactionId, EmployeePackByRequest request);

    Transaction updateScheduleTimeForTransaction(String transactionId, OrderScheduleUpdateRequest request);

    Transaction updateDeliveryAddress(String transactionId, Address request);

    Member acceptOrUpdateConsumer(ConsumerUser dbConsumerUser, ConsumerUser consumerUser, String memberId);

    SearchResult<TransactionLimitedResult> getTransactionsWithLimitedResult(long afterDate, long beforeDate, int start, int limit);

    AllQueueResult getTransactionsWithDate(long startDate, long endDate);

    Transaction finalizeTransaction(String transactionId, boolean finalize);

    Transaction updatePaymentMethod(String transactionId, PaymentOptionUpdateRequest paymentOption, boolean isPrepare);

    InputStream bulkPdfForTransactions(String transactionIds);

    SearchResult<Transaction> getAllTransactionByEmployee(String employeeId, int start, int limit, String searchTerm);

    InputStream createPdfForTransaction(String transactionId);
}
