package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.rest.dispensary.requests.customer.RecommendationAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.customer.RecommendationUpdateRequest;

/**
 * Created by Stephen Schmidt on 11/15/2015.
 */
public interface RecommendationService {

    Recommendation addRecommendation(RecommendationAddRequest addRequest);

    Recommendation updateRecommendation(Long recommedationId, RecommendationUpdateRequest updateRequest);

    void deleteRecommendation(Long recommendationId);
}
