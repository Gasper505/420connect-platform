package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.SalesDependentDetails;

public interface ReportDetailsService {
    SalesDependentDetails prepareSalesDependentDetails(ReportFilter filter, boolean orderItemBased, boolean addDependent);
}
