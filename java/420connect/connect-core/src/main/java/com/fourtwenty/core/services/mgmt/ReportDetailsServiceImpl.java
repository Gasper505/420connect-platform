package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageProductItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PrepackageRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.model.SalesDependentDetails;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ReportDetailsServiceImpl implements ReportDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportDetailsServiceImpl.class);

    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private LoyaltyRewardRepository rewardRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private BrandRepository brandRepository;

    @Override
    public SalesDependentDetails prepareSalesDependentDetails(ReportFilter filter, boolean orderItemBased, boolean addDependent) {
        boolean fetchWithLimit = (filter.getType() == ReportType.TOTAL_SALES || filter.getType() == ReportType.TOTAL_SALES_PRODUCTS || filter.getType() == ReportType.SALES_BY_HOUR);

        long startDate = filter.getTimeZoneStartDateMillis();
        long endDate = filter.getTimeZoneEndDateMillis();

        List<Transaction> transactions = new ArrayList<>();
        HashMap<String, Employee> employeeMap = new HashMap<>();
        HashMap<String, Terminal> terminalMap = new HashMap<>();
        HashMap<String, Promotion> promotionMap = new HashMap<>();
        HashMap<String, LoyaltyReward> rewardMap = new HashMap<>();
        HashMap<String, Member> memberHashMap = new HashMap<>();
        HashMap<String, Product> productHashMap = new HashMap<>();
        HashMap<String, Vendor> vendorHashMap = new HashMap<>();
        HashMap<String, ProductCategory> categoryHashMap = new HashMap<>();
        HashMap<String, Brand> brandHashMap = new HashMap<>();

        HashMap<String, ProductBatch> allBatchMap = new HashMap<>();
        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        HashMap<String, PrepackageProductItem> productItemHashMap = new HashMap<>();

        while (startDate < endDate) {
            long end = endDate;
            if (fetchWithLimit) {
                end = new DateTime(startDate).plusDays(1).minusSeconds(1).getMillis();
                if (end > endDate) {
                    end = endDate;
                }
            }

            LOGGER.info(String.format("Start: %s End : %s", new DateTime(startDate), new DateTime(end)));

            Iterable<Transaction> results = transactionRepository.getBracketSales(filter.getCompanyId(), filter.getShopId(),
                    startDate, end);

            transactions.addAll(0, Lists.newArrayList(results));

            startDate = (fetchWithLimit) ? new DateTime(startDate).plusDays(1).getMillis() : endDate;

            if (!addDependent) {
                continue;
            }

            LinkedHashSet<ObjectId> memberIds = new LinkedHashSet<>();
            Set<ObjectId> terminalIds = new HashSet<>();
            Set<ObjectId> productIds = new HashSet<>();
            Set<ObjectId> employeeIds = new HashSet<>();
            Set<ObjectId> promotionIds = new HashSet<>();
            Set<ObjectId> rewardIds = new HashSet<>();
            Set<ObjectId> vendorIds = new HashSet<>();
            Set<ObjectId> categoryIds = new HashSet<>();
            Set<ObjectId> brandIds = new HashSet<>();
            for (Transaction transaction : results) {
                if (StringUtils.isNotBlank(transaction.getSellerTerminalId()) && ObjectId.isValid(transaction.getSellerTerminalId()) && !terminalMap.containsKey(transaction.getSellerTerminalId())) {
                    terminalIds.add(new ObjectId(transaction.getSellerTerminalId()));
                }

                if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId()) && !memberHashMap.containsKey(transaction.getMemberId())) {
                    memberIds.add(new ObjectId(transaction.getMemberId()));
                }

                if (StringUtils.isNotBlank(transaction.getSellerId()) && ObjectId.isValid(transaction.getSellerId()) && !employeeMap.containsKey(transaction.getSellerId())) {
                    employeeIds.add(new ObjectId(transaction.getSellerId()));
                }

                if (StringUtils.isNotBlank(transaction.getPackedBy()) && ObjectId.isValid(transaction.getPackedBy()) && !employeeMap.containsKey(transaction.getPackedBy())) {
                    employeeIds.add(new ObjectId(transaction.getPackedBy()));
                }

                if (StringUtils.isNotBlank(transaction.getCreatedById()) && ObjectId.isValid(transaction.getCreatedById()) && !employeeMap.containsKey(transaction.getCreatedById())) {
                    employeeIds.add(new ObjectId(transaction.getCreatedById()));
                }

                if (StringUtils.isNotBlank(transaction.getPreparedBy()) && ObjectId.isValid(transaction.getPreparedBy()) && !employeeMap.containsKey(transaction.getPreparedBy())) {
                    employeeIds.add(new ObjectId(transaction.getPreparedBy()));
                }

                for (OrderItem item : transaction.getCart().getItems()) {
                    if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId()) && !productHashMap.containsKey(item.getProductId())) {
                        productIds.add(new ObjectId(item.getProductId()));
                    }
                }

                for (PromotionReq promotionReq : transaction.getCart().getPromotionReqs()) {
                    if (StringUtils.isNotBlank(promotionReq.getRewardId()) && ObjectId.isValid(promotionReq.getRewardId()) && !rewardMap.containsKey(promotionReq.getRewardId())) {
                        rewardIds.add(new ObjectId(promotionReq.getRewardId()));
                    }

                    if (StringUtils.isNotBlank(promotionReq.getPromotionId()) && ObjectId.isValid(promotionReq.getPromotionId()) && !promotionMap.containsKey(promotionReq.getPromotionId())) {
                        promotionIds.add(new ObjectId(promotionReq.getPromotionId()));
                    }
                }
            }
            memberHashMap.putAll(memberRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(memberIds)));
            employeeMap.putAll(employeeRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(employeeIds)));
            terminalMap.putAll(terminalRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(terminalIds)));
            promotionMap.putAll(promotionRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(promotionIds)));
            rewardMap.putAll(rewardRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(rewardIds)));

            productHashMap.putAll(productRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(productIds)));

            allBatchMap.putAll(batchRepository.getBatchesForProductsMap(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet())));
            prepackageHashMap.putAll(prepackageRepository.getPrepackagesForProductsAsMap(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet())));
            productItemHashMap.putAll(prepackageProductItemRepository.getPrepackagesForProductsAsMap(filter.getCompanyId(), filter.getShopId(), Lists.newArrayList(productHashMap.keySet())));

            if (orderItemBased) {
                for (Product product : productHashMap.values()) {
                    if (StringUtils.isNotBlank(product.getVendorId()) && ObjectId.isValid(product.getVendorId()) && !vendorHashMap.containsKey(product.getVendorId())) {
                        vendorIds.add(new ObjectId(product.getVendorId()));
                    }
                    if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId()) && !categoryHashMap.containsKey(product.getCategoryId())) {
                        categoryIds.add(new ObjectId(product.getCategoryId()));
                    }
                    if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId()) && !brandHashMap.containsKey(product.getBrandId())) {
                        brandIds.add(new ObjectId(product.getBrandId()));
                    }
                }

                vendorHashMap.putAll(vendorRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(vendorIds)));
                categoryHashMap.putAll(categoryRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(categoryIds)));
                brandHashMap.putAll(brandRepository.listAsMap(filter.getCompanyId(), Lists.newArrayList(brandIds)));

            }
        }
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(filter.getCompanyId());
        HashMap<String, MemberGroup> memberGroupHashMap = memberGroupRepository.listAsMap(filter.getCompanyId());

        HashMap<String, ProductBatch> recentBatchMap = new HashMap<>();
        HashMap<String, List<Product>> productsByCompanyLinkId = new HashMap<>();

        for (ProductBatch batch : allBatchMap.values()) {
            recentBatchMap.putIfAbsent(batch.getProductId(), batch);
            ProductBatch oldBatch = recentBatchMap.get(batch.getProductId());
            if (oldBatch.getPurchasedDate() < batch.getPurchasedDate()) {
                recentBatchMap.put(batch.getProductId(), batch);
            }
        }

        for (Product p : productHashMap.values()) {
            productsByCompanyLinkId.putIfAbsent(p.getCompanyLinkId(), new ArrayList<>());
            productsByCompanyLinkId.get(p.getCompanyLinkId()).add(p);
        }

        SalesDependentDetails details = new SalesDependentDetails();
        details.setTransactions(transactions)
                .setEmployees(employeeMap)
                .setMembers(memberHashMap)
                .setProducts(productHashMap)
                .setTerminals(terminalMap)
                .setPromotions(promotionMap)
                .setRewards(rewardMap)
                .setVendors(vendorHashMap)
                .setCategories(categoryHashMap)
                .setBrands(brandHashMap)
                .setAllBatches(allBatchMap)
                .setRecentBatchMap(recentBatchMap)
                .setPrepackages(prepackageHashMap)
                .setPrepackageProductItems(productItemHashMap)
                .setMemberGroups(memberGroupHashMap)
                .setWeightTolerances(toleranceHashMap);
        return details;
    }

}
