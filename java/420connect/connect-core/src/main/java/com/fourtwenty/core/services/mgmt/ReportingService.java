package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.model.Report;

import java.util.List;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public interface ReportingService {

    Report getReport(String type, String startDate, String endDate, String filter, String format, ReportTrack.ReportSectionType section, String email, boolean generate);

    ReportTrack addOrUpdateTrackCount(ReportType type);

    List<ReportTrack> getFrequentlyUsedReport();

    String getDailySummaryReport(String terminalId, String date, int width);
}
