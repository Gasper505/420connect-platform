package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.rest.dispensary.requests.company.ResetDeleteRequest;

public interface ResetDataService {

    void deleteAllMembers(ResetDeleteRequest deleteRequest);

    void deleteAllProducts(ResetDeleteRequest deleteRequest);

    void deleteAllProductStocks(ResetDeleteRequest deleteRequest);

    void deleteAllVendors(ResetDeleteRequest deleteRequest);

    void deleteAllDoctors(ResetDeleteRequest deleteRequest);
}
