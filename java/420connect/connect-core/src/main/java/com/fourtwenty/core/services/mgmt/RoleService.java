package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.AppRolePermission;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.rest.dispensary.requests.company.AppRolePermissionRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.RoleAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.AppRolePermissionResult;
import com.fourtwenty.core.rest.dispensary.results.company.RolePermissionResult;

import java.util.List;

/**
 * Created by mdo on 6/15/16.
 */
public interface RoleService {
    SearchResult<Role> getAllRoles();

    SearchResult<Role> getRoles(long afterDate, long beforeDate);

    Role getRoleById(String roleId);

    Role getRoleById(String companyId, String roleId);

    Role addRole(RoleAddRequest role);

    Role updateRole(String roleId, Role role);

    void deleteRole(String roleId);

    boolean hasPermission(String companyId, String roleId, Role.Permission permission);

    void checkPermission(String companyId, String roleId, Role.Permission permission);

    boolean canAccessRestricted(String companyId, String shopId, String employeeId);

    AppRolePermission assignPermissionByRole(AppRolePermissionRequest request);

    List<AppRolePermissionResult> getPermissionsByCompany();

    List<RolePermissionResult> getAllPermissions();
}
