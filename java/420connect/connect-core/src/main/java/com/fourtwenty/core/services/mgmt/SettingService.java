package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.rest.dispensary.requests.UpdateToleranceRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MemberGroupAddRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberGroupResult;

/**
 * Created by mdo on 10/25/15.
 */
public interface SettingService {
    ListResult<ProductWeightTolerance> getProductTolerances();

    void updateTolerance(String toleranceId, UpdateToleranceRequest request);

    SearchResult<MemberGroupResult> getMemberGroups();

    SearchResult<MemberGroupResult> getActiveMemberGroups();

    MemberGroup updateMemberGroup(String memberGroupId, MemberGroup group);

    MemberGroup createMemberGroup(MemberGroupAddRequest addRequest);

    void deleteMemberGroup(String memberGroupId);

    boolean checkForPermission(String password);
}
