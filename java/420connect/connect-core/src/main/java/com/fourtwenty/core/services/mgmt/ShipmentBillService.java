package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentBillPaymentStatusRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentBillUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ShipmentBillRequestResult;
import com.fourtwenty.core.rest.purchaseorders.POCompleteRequest;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public interface ShipmentBillService {
    ShipmentBillRequestResult getShipmentBillById(final String shipmentId);

    ShipmentBill addShipmentBill(final String poId, final POCompleteRequest request, PurchaseOrder purchaseOrder);

    ShipmentBill updateShipmentBill(final String shipmentBillId, final ShipmentBill shipmentBill);

    ShipmentBill getShipmentBillByPoId(final String poId);

    ShipmentBill addShipmentBillPayment(String shipmentBillId, ShipmentBillPaymentStatusRequest shipmentBillPayment);

    SearchResult<ShipmentBillRequestResult> getAllShipmentBills(int start, int limit, PurchaseOrder.CustomerType customerType);

    SearchResult<ShipmentBillRequestResult> getArchivedShipmentBills(int start, int limit);

    ShipmentBill archiveShipmentBill(String shipmentBillId);

    void emailShipmentBillToAccounting(String shipmentBillId, EmailRequest emailRequest);

    ShipmentBill updateShipmentBillPayment(String shipmentBillId, String paymentId, ShipmentBillPaymentStatusRequest request);

    HashMap<String, ShipmentBill> getShipmentBillByPoAsMap(List<String> purchaseOrderId);

    void deleteShipmentBillPayment(String shipmentBillId, String paymentId);

    void closeShipmentBill(String shipmentBillId);

    ShipmentBill updateReceivedDate(String shipmentBillId, ShipmentBillUpdateRequest request);

    void updatePurchaseIncompleteOrder(String purchaseOrderId);

    InputStream createPdfForShipmentBill(String shippingManifestId);
}