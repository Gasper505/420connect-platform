package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentStatusUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import java.util.List;

public interface ShipmentService {
    Shipment addShipment(ShipmentAddRequest shipmentAddRequest);

    Shipment addShipment(Shipment shipment);

    Shipment getShipmentById(String shipmentId);

    SearchResult<Shipment> getAllShipment(int start, int limit, String searchTerm, Shipment.ShipmentSort sortOption);

    SearchResult<Shipment> getShipmentListByStatus(int start, int limit, String status, String searchTerm, Shipment.ShipmentSort sortOption);

    Shipment updateShipmentStatus(String shipmentId, ShipmentStatusUpdateRequest shipmentStatusUpdateRequest);

    List<Shipment> getAvailableShipments();

    Shipment rejectShipment(String shipmentId);
 }