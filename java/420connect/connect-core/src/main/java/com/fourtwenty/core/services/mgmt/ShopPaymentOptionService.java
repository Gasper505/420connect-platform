package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;

import java.util.List;


public interface ShopPaymentOptionService {
    List<ShopPaymentOption> getPaymentOptions();

    void savePaymentOptions(List<ShopPaymentOption> shopPaymentOptions);
}
