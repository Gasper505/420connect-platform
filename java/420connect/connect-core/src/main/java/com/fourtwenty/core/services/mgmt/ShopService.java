package com.fourtwenty.core.services.mgmt;


import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.rest.common.EmailNotificationRequest;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.ShopAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.ShopUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.CustomShopResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import java.util.List;

public interface ShopService {
    // Shop
    SearchResult<CustomShopResult> searchShops(CompanyFeatures.AppTarget appTarget);

    List<ShopResult> getBlazeConnectionsByShopId(String shopId);

    ListResult<Shop> list();

    DateSearchResult<ShopResult> getShops(long afterDate, long beforeDate);

    Shop addShop(ShopAddRequest shopAddRequest);

    Shop updateShop(String shopId, Shop shop);

    void sendDailySummaryDataByEmail(EmailNotificationRequest request, String shopId);
    void sendLowInventoryNotificationByEmail(EmailRequest request);

    Shop updateShopData(String shopId, ShopUpdateRequest request);
}
