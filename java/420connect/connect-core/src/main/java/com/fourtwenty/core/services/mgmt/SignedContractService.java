package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.SignedContract;

import java.util.List;

/**
 * Created by Stephen Schmidt on 12/28/2015.
 */
public interface SignedContractService {

    List<SignedContract> getSignedContracts();

    SignedContract getSignedContractByMembership(String memberId, Boolean active);
}
