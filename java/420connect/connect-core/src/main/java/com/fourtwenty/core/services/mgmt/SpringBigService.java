package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.thirdparty.springbig.models.SpringMemberResponse;

import java.util.List;

public interface SpringBigService {

    SpringBigInfo getSpringInfoById(String id);

    List<SpringBigInfo> getSpringBigInfoList();

    SpringBigInfo updateSpringBigInfoByShop(SpringBigInfo springBigInfo);

    SpringBigInfo getSpringBigInfoByShop(String shopId);

    SpringBigInfo createSpringBigInfoByShop(SpringBigInfo springBigInfo);

    SpringMemberResponse getSpringBigMemberById(String memberId);

}
