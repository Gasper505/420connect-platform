package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.loyalty.StorewideSale;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.dispensary.requests.promotions.StorewideSaleAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.StorewideSaleRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.StorewideSaleResult;

/**
 * Created on 9/10/19.
 */
public interface StorewideSaleService {
    StorewideSale addStorewideSale(StorewideSaleAddRequest request);

    StorewideSaleResult updateStorewideSale(String StorewideSaleId, StorewideSaleRequest StorewideSaleRequest);

    void deleteStorewideSale(String StorewideSaleId);


    // Getters
    StorewideSaleResult getStorewideSaleById(String StorewideSaleId);

    DateSearchResult<StorewideSale> getStorewideSalesByDate(long afterDate, long beforeDate);

    SearchResult<StorewideSale> getStorewideSales(int start, int limit);

    StorewideSaleResult enableStorewideSale(String storewideSaleId);

    StorewideSaleResult disableStorewideSale(String storewideSaleId);

    Product applyActiveStorewideSaleForProduct(Product product);
}
