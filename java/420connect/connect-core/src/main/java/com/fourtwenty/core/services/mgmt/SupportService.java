package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.rest.dispensary.requests.support.SupportSendRequest;

/**
 * Created by mdo on 8/10/16.
 */
public interface SupportService {
    void sendSupportEmail(SupportSendRequest request);
}
