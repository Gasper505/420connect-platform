package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.plugins.TVDisplay;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.rest.dispensary.requests.plugins.TVDisplayAddRequest;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.EmployeeStatsResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.marketing.TVDisplayResult;

import java.util.List;
import java.util.Map;

/**
 * Created by decipher on 14/10/17 2:42 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public interface TVDisplayService {
    TVDisplayResult getTVDisplaySetting(String uniqueIdentifier, Long tvNo);

    TVDisplay addTVDisplay(TVDisplayAddRequest tvDisplayAddRequest);

    TVDisplay updateTVDisplay(String tvDisplayId, TVDisplay tvDisplay);

    SearchResult<TVDisplay> getAllTVDisplay(int start, int limit);

    List<MemberLimitedView> getWaitListForAllActiveTransactions(final String uniqueIdentifier);

    SearchResult<Product> getProductByCategoryAndIdentifier(final String uniqueIdentifier, final String category, final int start, final int limit);

    TVDisplay getTVDisplayById(String tvDisplayId);

    void deleteTVDisplay(String tvDisplayId);

    Map<String, SearchResult<Transaction>> getTvDisplayTransactionsByContent(String uniqueIdentifier, String contentId, int start, int limit);

    EmployeeStatsResult getEmployeeStatsDetail(String uniqueIdentifier, String contentId, int start, int limit, int timeZoneOffset);
}
