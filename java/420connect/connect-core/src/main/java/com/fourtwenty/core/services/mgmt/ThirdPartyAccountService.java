package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.TPAccountAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 2/10/17.
 */
public interface ThirdPartyAccountService {
    ThirdPartyAccount getAccountById(String accountId);

    DateSearchResult<ThirdPartyAccount> getThirdPartyAccounts(long afterDate, long beforeDate);

    SearchResult<ThirdPartyAccount> getThirdPartyAccounts();

    ThirdPartyAccount addAccount(TPAccountAddRequest request);

    ThirdPartyAccount updateAccount(String accountId, ThirdPartyAccount account);

    void deleteThirdPartyAccount(String accountId);


}
