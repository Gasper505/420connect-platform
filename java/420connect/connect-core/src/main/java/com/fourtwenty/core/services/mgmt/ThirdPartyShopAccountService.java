package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.TPShopAccountAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 8/25/17.
 */
public interface ThirdPartyShopAccountService {

    ThirdPartyShopAccount getAccountById(String accountId);

    ThirdPartyShopAccount getAccountByType(ThirdPartyShopAccount.ThirdPartyShopAccountType accountType);

    DateSearchResult<ThirdPartyShopAccount> getThirdPartyShopAccounts(long afterDate, long beforeDate);

    SearchResult<ThirdPartyShopAccount> getThirdPartyShopAccounts();

    ThirdPartyShopAccount addAccount(TPShopAccountAddRequest request);

    ThirdPartyShopAccount updateAccount(String accountId, ThirdPartyShopAccount account);

    void deleteThirdPartyShopAccount(String accountId);
}
