package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.rest.dispensary.requests.auth.PinRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.TimeCardCompositeResult;

/**
 * Created by mdo on 6/24/16.
 */
public interface TimeCardService {
    // Get current active timecards
    SearchResult<TimeCardCompositeResult> getActiveTimeCards();

    SearchResult<TimeCardCompositeResult> getTimecardHistory(int start, int limit);

    // Clock In
    TimeCardCompositeResult clockIn(PinRequest request);

    TimeCardCompositeResult updateTimecard(String timecardId, TimeCard timeCard);

    // Clock Out
    void clockOut(String timeCardId, PinRequest request);

    void adminClockOut(String timeCardId);

    void userClockOut();

    TimeCardCompositeResult userClockIn();
}
