package com.fourtwenty.core.services.mgmt;

import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;

import java.util.List;

/**
 * Created by Stephen Schmidt on 10/12/2015.
 */
public interface VendorService {

    List<Vendor> getAllVendors();

    Vendor addVendor(VendorAddRequest addRequest);

    Vendor sanitize(VendorAddRequest importRequest);

    Vendor updateVendor(String vendorId, Vendor request);

    VendorResult getVendor(String vendorId);

    SearchResult<VendorResult> getVendors(String vendorId, int start, int limit, Vendor.VendorType vendorType, String term, List<Vendor.CompanyType> companyType);

    SearchResult<VendorResult> getVendorsByDate(long afterDate, long beforeDate);

    void deleteVendor(String vendorId);

    SearchResult<VendorResult> getAllVendorByCategoryId(String categoryId);

    SearchResult<Brand> getBrandsByVendorId(String vendorId, int start, int limit, String term);

    boolean checkValidVendors(List<String> vendorList);

    void bulkVendorUpdates(VendorBulkUpdateRequest vendorBulkUpdateRequest);

    Vendor getDefaultVendor(String companyId);
}
