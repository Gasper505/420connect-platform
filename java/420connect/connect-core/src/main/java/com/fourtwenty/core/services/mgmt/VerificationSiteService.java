package com.fourtwenty.core.services.mgmt;

/**
 * Created by Gaurav Saini on 16/5/17.
 */
public interface VerificationSiteService {

    String getHelloMDPatient(String verificationCode);
}
