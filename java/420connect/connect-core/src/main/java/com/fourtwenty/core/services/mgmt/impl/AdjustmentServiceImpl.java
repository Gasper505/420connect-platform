package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Adjustment;
import com.fourtwenty.core.domain.repositories.dispensary.AdjustmentRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.AdjustmentService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;

public class AdjustmentServiceImpl extends AbstractAuthServiceImpl implements AdjustmentService {

    private static final String ADJUSTMENT = "Adjustment";
    private static final String ADJUSTMENT_NOT_EXIST = "Adjustment is not exist";
    private static final String ADJUSTMENT_NAME_EXIST = "Adjustment name already exist";
    private static final String ENTER_ADJUSTMENT_NAME = "Please enter name of adjustment";

    @Inject
    private AdjustmentRepository adjustmentRepository;

    @Inject
    public AdjustmentServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Get adjustment by id
     * @param adjustmentId : Adjustment id
     */
    @Override
    public Adjustment getAdjustmentById(String adjustmentId) {
        Adjustment adjustment = adjustmentRepository.get(token.getCompanyId(), adjustmentId);

        if (adjustment == null) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ADJUSTMENT_NOT_EXIST);
        }

        return adjustment;
    }

    /**
     * Get adjustment list
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<Adjustment> getAdjustments(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return adjustmentRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);
    }

    /**
     * Add Adjustment
     * @param request : Adjustment request
     */
    @Override
    public Adjustment addAdjustment(Adjustment request) {
        if (StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ENTER_ADJUSTMENT_NAME);
        }

        Adjustment adjustmentByName = adjustmentRepository.getAdjustmentByName(token.getCompanyId(), token.getShopId(), request.getName());
        if (adjustmentByName != null) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ADJUSTMENT_NAME_EXIST);
        }

        Adjustment adjustment = new Adjustment();
        adjustment.prepare(token.getCompanyId());
        adjustment.setShopId(token.getShopId());

        adjustment.setActive(request.isActive());
        adjustment.setName(request.getName());
        adjustment.setType(request.getType());

        return adjustmentRepository.save(adjustment);
    }

    /**
     * Update Adjustment
     * @param adjustmentId : Adjustment id
     * @param request : update request
     */
    @Override
    public Adjustment updateAdjustment(String adjustmentId, Adjustment request) {
        if (StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ENTER_ADJUSTMENT_NAME);
        }

        Adjustment dbAdjustment = adjustmentRepository.get(token.getCompanyId(), adjustmentId);
        if (dbAdjustment == null) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ADJUSTMENT_NOT_EXIST);
        }

        Adjustment adjustmentByName = adjustmentRepository.getAdjustmentByName(token.getCompanyId(), token.getShopId(), request.getName());
        if (adjustmentByName != null && !adjustmentByName.getId().equalsIgnoreCase(adjustmentId)) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ADJUSTMENT_NAME_EXIST);
        }

        dbAdjustment.prepare(token.getCompanyId());
        dbAdjustment.setActive(request.isActive());
        dbAdjustment.setType(request.getType());
        dbAdjustment.setName(request.getName());

        return adjustmentRepository.update(token.getCompanyId(), adjustmentId, dbAdjustment);
    }

    @Override
    public void deleteAdjustment(String adjustmentId) {
        Adjustment dbAdjustment = adjustmentRepository.get(token.getCompanyId(), adjustmentId);
        if (dbAdjustment == null) {
            throw new BlazeInvalidArgException(ADJUSTMENT, ADJUSTMENT_NOT_EXIST);
        }

        adjustmentRepository.removeById(token.getCompanyId(), adjustmentId);
    }
}
