package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.consumer.ConsumerAsset;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopAssetResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopAssetToken;
import com.fourtwenty.core.security.tokens.AssetAccessToken;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.imaging.ImageProcessorService;
import com.fourtwenty.core.services.mgmt.AssetService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.joda.time.DateTime;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by mdo on 10/26/15.
 */
public class AssetServiceImpl extends AbstractAuthServiceImpl implements AssetService {
    static final Log LOG = LogFactory.getLog(AssetServiceImpl.class);
    private static final String CONTRACT = "Contract";
    private static final String CONTRACT_NOT_FOUND = "Contract not found";
    private static final String SHOP = "Shop";
    private static final String SHOP_NOT_FOUND = "Shop not found";
    private static final String INVALID_TOKEN = "Invalid asset download token";
    private static final String TOKEN = "Token";
    private static final String TOKEN_EXPIRED = "Token expired";
    private static final String MEMBER_ASSET = "Member's Asset";
    private static final String ERROR_DOWNLOAD_ASSET = "Error while downloading member's asset";

    @Inject
    AmazonS3Service amazonS3Service;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ConsumerUserRepository consumerUserRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    ImageProcessorService imageProcessorService;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ContractRepository contractRepository;

    @Inject
    public AssetServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public com.fourtwenty.core.domain.models.company.CompanyAsset uploadAssetPrivate(InputStream inputStream,
                                                                                     String name,
                                                                                     CompanyAsset.AssetType assetType, String mimeType) {
        if (inputStream == null || StringUtils.isBlank(name)) {
            throw new BlazeInvalidArgException("Form", "Invalid name or stream");
        }

        String ext2 = FilenameUtils.getExtension(name); // returns "exe"
        if (StringUtils.isBlank(ext2)) {
            if (assetType == Asset.AssetType.Document) {
                ext2 = ".pdf";
            } else if (assetType == Asset.AssetType.Photo) {
                ext2 = assetType == Asset.AssetType.Photo ? ".jpeg" : ".png";
                if (mimeType != null) {
                    if (mimeType.equalsIgnoreCase("image/png")) {
                        ext2 = ".png";
                    } else if (mimeType.equalsIgnoreCase("image/jpeg") || mimeType.equalsIgnoreCase("image/jpg")) {
                        ext2 = ".jpeg";
                    }
                }
            }
        } else {
            ext2 = "." + ext2;
        }

        try {
            File file = stream2file(inputStream, ext2);


            String keyName = UUID.randomUUID().toString();
            String key = amazonS3Service.uploadFile(file, keyName, ext2);
            com.fourtwenty.core.domain.models.company.CompanyAsset asset = new com.fourtwenty.core.domain.models.company.CompanyAsset();

            if (ext2.equalsIgnoreCase(".jpg") ||
                    ext2.equalsIgnoreCase(".jpeg") ||
                    ext2.equalsIgnoreCase(".png")) {
                ImageProcessorService.ImageProcessingResult imageProcessingResult = imageProcessorService.processImage(file, file.getName());

                if (imageProcessingResult != null) {
                    if (imageProcessingResult.largeX2 != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.largeX2.inputStream,
                                imageProcessingResult.largeX2.objectMetadata,
                                keyName + "-" + imageProcessingResult.largeX2.size, ext2);
                        if (resizeResult != null) {
                            asset.setLargeX2URL(resizeResult.getUrl());
                        }
                    }

                    if (imageProcessingResult.large != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.large.inputStream,
                                imageProcessingResult.large.objectMetadata,
                                keyName + "-" + imageProcessingResult.large.size, ext2);

                        if (resizeResult != null) {
                            asset.setLargeURL(resizeResult.getUrl());
                        }
                    }

                    if (imageProcessingResult.medium != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.medium.inputStream,
                                imageProcessingResult.medium.objectMetadata,
                                keyName + "-" + imageProcessingResult.medium.size, ext2);
                        if (resizeResult != null) {
                            asset.setMediumURL(resizeResult.getUrl());
                        }
                    }

                    if (imageProcessingResult.small != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.small.inputStream,
                                imageProcessingResult.small.objectMetadata,
                                keyName + "-" + imageProcessingResult.small.size, ext2);
                        if (resizeResult != null) {
                            asset.setThumbURL(resizeResult.getUrl());
                        }
                    }
                }
            }

            asset.prepare(token.getCompanyId());
            asset.setName(name);
            asset.setKey(key);
            asset.setActive(true);
            asset.setType(assetType);
            companyAssetRepository.save(asset);
            return asset;
        } catch (IOException e) {
            e.printStackTrace();
            throw new BlazeInvalidArgException("Blaze", "Cannot upload file.");
        }
    }

    @Override
    public CompanyAsset uploadAssetPublic(InputStream inputStream, String name, CompanyAsset.AssetType assetType, String mimeType, String keyName) {
        if (inputStream == null || StringUtils.isBlank(name)) {
            throw new BlazeInvalidArgException("Form", "Invalid name or stream");
        }

        String ext2 = FilenameUtils.getExtension(name); // returns "exe"
        if (StringUtils.isBlank(ext2)) {
            ext2 = assetType == Asset.AssetType.Document ? ".pdf" : ".jpeg";

            if (mimeType != null) {
                if (mimeType.equalsIgnoreCase("image/png")) {
                    ext2 = ".png";
                } else if (mimeType.equalsIgnoreCase("image/jpeg") || mimeType.equalsIgnoreCase("image/jpg")) {
                    ext2 = ".jpeg";
                }
            }
        } else {
            ext2 = "." + ext2;
        }

        CompanyAsset asset = new CompanyAsset();
        UploadFileResult result = null;
        if (StringUtils.isBlank(keyName)) {
            keyName = UUID.randomUUID().toString();
        }
        try {
            File file = stream2file(inputStream, ext2);
            result = amazonS3Service.uploadFilePublic(file, keyName, ext2);

            if (ext2.equalsIgnoreCase(".jpg") ||
                    ext2.equalsIgnoreCase(".jpeg") ||
                    ext2.equalsIgnoreCase(".png")) {
                ImageProcessorService.ImageProcessingResult imageProcessingResult = imageProcessorService.processImage(file, file.getName());

                if (imageProcessingResult != null) {
                    if (imageProcessingResult.largeX2 != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.largeX2.inputStream,
                                imageProcessingResult.largeX2.objectMetadata,
                                keyName + "-" + imageProcessingResult.largeX2.size, ext2);
                        if (resizeResult != null) {
                            asset.setLargeX2URL(resizeResult.getUrl());
                        }
                    }

                    if (imageProcessingResult.large != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.large.inputStream,
                                imageProcessingResult.large.objectMetadata,
                                keyName + "-" + imageProcessingResult.large.size, ext2);

                        if (resizeResult != null) {
                            asset.setLargeURL(resizeResult.getUrl());
                        }
                    }

                    if (imageProcessingResult.medium != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.medium.inputStream,
                                imageProcessingResult.medium.objectMetadata,
                                keyName + "-" + imageProcessingResult.medium.size, ext2);
                        if (resizeResult != null) {
                            asset.setMediumURL(resizeResult.getUrl());
                        }
                    }

                    if (imageProcessingResult.small != null) {
                        UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.small.inputStream,
                                imageProcessingResult.small.objectMetadata,
                                keyName + "-" + imageProcessingResult.small.size, ext2);
                        if (resizeResult != null) {
                            asset.setThumbURL(resizeResult.getUrl());
                        }
                    }
                }
            }
            asset.prepare(token.getCompanyId());
            asset.setName(name);
            asset.setKey(result.getKey());
            asset.setActive(true);
            asset.setType(assetType);
            asset.setPublicURL(result.getUrl());
            asset.setOrigURL(result.getUrl());
            asset.setSecured(false);
            companyAssetRepository.save(asset);

        } catch (IOException e) {
            e.printStackTrace();
            throw new BlazeInvalidArgException("Blaze", "Cannot upload file.");
        }


        return asset;
    }


    @Override
    public CompanyAsset uploadAssetPublic(InputStream inputStream, String name, CompanyAsset.AssetType assetType, String mimeType) {
        return uploadAssetPublic(inputStream, name, assetType, mimeType, null);
    }

    @Override
    public CompanyAsset uploadAssetPublicFromUrl(String url, CompanyAsset.AssetType type, String keyname) {
        try {
            URL publicURL = new URL(url);
            InputStream inputStream = publicURL.openStream();

            String name = FilenameUtils.getBaseName(publicURL.getPath());
            String ext = "." + FilenameUtils.getExtension(publicURL.getPath());

            return uploadAssetPublic(inputStream, name, type, null, keyname);
        } catch (Exception e) {
            LOG.error("error retrieving image", e);
            return null;
        }
    }

    @Override
    public CompanyAsset uploadAssetPrivate(InputStream inputStream, String name,
                                           String fileName, String fileExtension, CompanyAsset.AssetType assetType) {
        if (inputStream == null || StringUtils.isBlank(name)) {
            throw new BlazeInvalidArgException("Form", "Invalid name or stream");
        }

        try {
            File file = stream2file(inputStream, fileExtension);
            String key = amazonS3Service.uploadFile(file, token.getCompanyId() + "-" + fileName, "." + fileExtension);
            CompanyAsset asset = companyAssetRepository.getAssetByKey(token.getCompanyId(), key);
            if (asset != null) {
                return asset;
            }
            asset = new com.fourtwenty.core.domain.models.company.CompanyAsset();
            asset.prepare(token.getCompanyId());
            asset.setName(name);
            asset.setKey(key);
            asset.setActive(true);
            asset.setType(assetType);
            companyAssetRepository.save(asset);
            return asset;
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Upload", "Cannot upload file");
        }
    }

    @Override
    public AssetStreamResult getCompanyAssetStream(String assetKey, String assetToken) {
        return getCompanyAssetStream(assetKey, assetToken, false);
    }

    private AssetStreamResult getCompanyAssetStream(String assetKey, String assetToken, boolean isReportRequest) {
        if (assetKey != null && (assetKey.startsWith("420default-") || isReportRequest)) {
            AssetStreamResult result = amazonS3Service.downloadFile(assetKey, true);
            if (result == null) {
                throw new BlazeInvalidArgException("Asset", "Invalid asset.");
            }
            result.setAsset(null);
            return result;
        }

        String companyId = null;
        String shopId = null;
        if (token != null && token.isValid() && StringUtils.isNotEmpty(token.getCompanyId())) {
            companyId = token.getCompanyId();
            shopId = token.getShopId();
        } else if (StringUtils.isNotEmpty(assetToken)) {
            try {
                AssetAccessToken assetAccessToken = securityUtil.decryptAssetAccessToken(assetToken);
                companyId = assetAccessToken.getCompanyId();
                shopId = assetAccessToken.getShopId();
            } catch (Exception e) {
                // ignore
            }
        }
        if (companyId == null) {
            throw new BlazeAuthException("Authorization", "Invalid companyId");
        }

        CompanyAsset asset = companyAssetRepository.getAssetByKey(companyId, assetKey);
        if (asset == null) {
            // Find by category
            ProductCategory productCategory = categoryRepository.getCategoryByPhotoKey(companyId, assetKey);
            if (productCategory != null) {
                asset = productCategory.getPhoto();
            }

            if (asset == null) {
                Product product = productRepository.findProductWithAssetKey(companyId, assetKey);
                if (product != null) {
                    for (CompanyAsset asset1 : product.getAssets()) {
                        if (asset1.getKey().equalsIgnoreCase(assetKey)) {
                            asset = asset1;
                        }
                    }
                }
            }


        }

        if (asset == null) {
            throw new BlazeInvalidArgException("CompanyAsset", "Invalid asset");
        } else {
            LOG.info("Accessing image: " + asset.getKey() + " secured: " + asset.getKey() + " purl: " + asset.getPublicURL());
            if (asset.getPublicURL() != null && asset.getPublicURL().contains("420default-")) {
                asset.setKey("420default-" + asset.getName());
            }
        }

        AssetStreamResult result = amazonS3Service.downloadFile(assetKey, asset.isSecured());
        result.setAsset(asset);
        return result;
    }

    @Override
    public AssetStreamResult getConsumerUserAssetStream(String consumerUserId, String assetKey, String assetToken) {
        String companyId = null;
        String shopId = null;
        if (token != null && token.isValid() && StringUtils.isNotEmpty(token.getCompanyId())) {
            companyId = token.getCompanyId();
            shopId = token.getShopId();
        } else if (StringUtils.isNotEmpty(assetToken)) {
            try {
                AssetAccessToken assetAccessToken = securityUtil.decryptAssetAccessToken(assetToken);
                companyId = assetAccessToken.getCompanyId();
                shopId = assetAccessToken.getShopId();
            } catch (Exception e) {
                // ignore
            }
        }


        if (companyId == null) {
            throw new BlazeAuthException("Authorization", "Invalid companyId");
        }


        ConsumerUser dbConsumerUser = consumerUserRepository.getById(consumerUserId);

        if (dbConsumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }

        ConsumerAsset consumerAsset = null;
        if (StringUtils.isBlank(assetKey)) {
            throw new BlazeInvalidArgException("AssetKey", "Unknown asset key.");
        }
        // look at dl image
        if (dbConsumerUser.getDlPhoto() != null
                && assetKey.equalsIgnoreCase(dbConsumerUser.getDlPhoto().getKey())) {
            consumerAsset = dbConsumerUser.getDlPhoto();
        }

        // look at rec image
        if (dbConsumerUser.getRecPhoto() != null
                && assetKey.equalsIgnoreCase(dbConsumerUser.getRecPhoto().getKey())) {
            consumerAsset = dbConsumerUser.getRecPhoto();
        }

        if (consumerAsset == null) {
            // look at company asset
            CompanyAsset companyAsset = companyAssetRepository.getAssetByKey(companyId, assetKey);
            if (companyAsset != null) {
                AssetStreamResult result = amazonS3Service.downloadFile(companyAsset.getKey(), true);
                result.setAsset(companyAsset);
                return result;
            }
        }

        if (consumerAsset == null) {
            throw new BlazeInvalidArgException("AssetKey", "Unknown asset key.");
        }
        AssetStreamResult result = amazonS3Service.downloadFile(consumerAsset.getKey(), true);
        result.setAsset(consumerAsset);
        return result;
    }

    /**
     * Get Combined PDF (Contract PDF + Signed Image)
     *
     * @param contractAssetKey
     * @param signedContractAssetKey
     * @param contentType
     * @param assetToken
     * @return
     */
    @Override
    public File getCombinedContract(String contractAssetKey, String signedContractAssetKey, String contentType, String assetToken) {
        if (!contentType.equals("PDF")) {
            throw new BlazeInvalidArgException("AssetResource", "Content Type must be 'PDF'.");
        }
        //make Directory where PDF will be store
        String userDir = System.getProperty("user.dir");
        File contractDir = new File(userDir + "/contract");
        if (!contractDir.exists()) {
            contractDir.mkdir();
        }
        //delete older contract
        deleteOlderFiles(1, contractDir.getAbsolutePath());

        //Get Contract PDF
        AssetStreamResult result = this.getCompanyAssetStream(contractAssetKey, assetToken);
        String key = result.getKey();

        File downloadablePDF = new File(contractDir.getAbsolutePath() + "/" + key + "_combine.pdf");

        // Get Contract Signed IMAGE
        AssetStreamResult signedResult = this.getCompanyAssetStream(signedContractAssetKey, assetToken);
        try {
            PDDocument document = PDDocument.load(result.getStream());
            document.addPage(new PDPage());
            PDPage page = document.getPage(document.getNumberOfPages() - 1);

            InputStream imageInputStream = signedResult.getStream();
            BufferedImage bufferImage = ImageIO.read(imageInputStream);
            PDImageXObject pdImage = LosslessFactory.createFromImage(document, bufferImage);

            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            contentStream.drawImage(pdImage, 20, 600, 550, 200);

            contentStream.close();
            document.save(downloadablePDF);
            document.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return downloadablePDF;
    }

    /**
     * When the Contract is signed digitally
     *
     * @param signedContractId
     * @return Contract with Message
     */
    @Override
    public File getAgreement(String signedContractId, String contractAssetKey, String assetToken) throws IOException {

        //make Directory where PDF will be store
        String userDir = System.getProperty("user.dir");
        File contractDir = new File(userDir + "/contract");
        if (!contractDir.exists()) {
            contractDir.mkdir();
        }
        //delete older contract
        deleteOlderFiles(1, contractDir.getAbsolutePath());

        //Get Contract PDF
        AssetStreamResult result = this.getCompanyAssetStream(contractAssetKey, assetToken);
        String key = result.getKey();
        File downloadablePDF = new File(contractDir.getAbsolutePath() + "/" + key + "_combine.pdf");

        PDDocument document = PDDocument.load(result.getStream());
        document.addPage(new PDPage());
        PDPage page = document.getPage(document.getNumberOfPages() - 1);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        ConsumerUser consumerUser = consumerUserRepository.getConsumerUserBySignedContractId(signedContractId);
        String firstName = consumerUser.getFirstName();
        String lastName = consumerUser.getLastName();

        List<SignedContract> signedContracts = consumerUser.getSignedContracts();
        String signedDate = null;
        for (SignedContract signedContract : signedContracts) {
            String contractId = signedContract.getId();
            if (signedContractId.equals(contractId)) {
                Shop shop = shopRepository.getById(signedContract.getShopId());
                signedDate = DateUtil.toDateTimeFormatted(signedContract.getSignedDate(), shop.getTimeZone());
            }
        }

        contentStream.beginText();
        contentStream.newLineAtOffset(25, 700);
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
        contentStream.showText("This agreement was digitally signed by " + firstName + " " + lastName + " on" + signedDate);
        contentStream.newLineAtOffset(25, 700); // need to test position of TEXT
        contentStream.endText();
        contentStream.close();
        document.save(downloadablePDF);
        document.close();

        return downloadablePDF;
    }

    /**
     * Delete one day older File
     *
     * @param day
     * @param dirPath
     */
    public static void deleteOlderFiles(int day, String dirPath) {
        File directory = new File(dirPath);
        if (directory.exists()) {
            File[] listFiles = directory.listFiles();
            long purgeTime = System.currentTimeMillis() - (day * 24 * 60 * 60 * 1000);
            for (File listFile : listFiles) {
                if (listFile.lastModified() < purgeTime) {
                    listFile.delete();
                }
            }
        }
    }


    private static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }

    /**
     * This method gets member's combined contract
     *
     * @param memberId         : member id
     * @param signedContractId : contract id
     * @param assetToken
     */
    @Override
    public File getCombinedContractByMember(String memberId, String signedContractId, String assetToken) {
        Member member = memberRepository.getById(memberId);
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member not found");
        }

        List<SignedContract> signedContracts = member.getContracts();

        if (signedContracts == null) {
            throw new BlazeInvalidArgException(CONTRACT, CONTRACT_NOT_FOUND);
        }

        SignedContract signedContract = null;
        for (SignedContract contract : signedContracts) {
            if (contract.getId().equalsIgnoreCase(signedContractId)) {
                signedContract = contract;
            }
        }

        if (signedContract == null) {
            throw new BlazeInvalidArgException(CONTRACT, CONTRACT_NOT_FOUND);
        }

        //make Directory where PDF will be store
        String userDir = System.getProperty("user.dir");
        File contractDir = new File(userDir + "/contract");
        if (!contractDir.exists()) {
            contractDir.mkdir();
        }
        //delete older contract
        deleteOlderFiles(1, contractDir.getAbsolutePath());

        Contract contract = contractRepository.getById(signedContract.getContractId());

        //Get Contract PDF
        AssetStreamResult contractResult = this.getCompanyAssetStream(contract.getPdfFile().getKey(), assetToken);
        String contractKey = contractResult.getKey();

        File downloadablePDF = new File(contractDir.getAbsolutePath() + "/" + contractKey + "_combine.pdf");
        try {
            PDDocument document = PDDocument.load(contractResult.getStream());

            //Combined member signature in contract
            if (signedContract.getSignaturePhoto() != null) {
                generateCombinedContract(this.getCompanyAssetStream(signedContract.getSignaturePhoto().getKey(), assetToken), document);
            }

            //Combined employee signature
            if (signedContract.getEmployeeSignaturePhoto() != null) {
                generateCombinedContract(this.getCompanyAssetStream(signedContract.getEmployeeSignaturePhoto().getKey(), assetToken), document);
            }

            //Combined witness signature
            if (signedContract.getWitnessSignaturePhoto() != null) {
                generateCombinedContract(this.getCompanyAssetStream(signedContract.getWitnessSignaturePhoto().getKey(), assetToken), document);
            }

            document.save(downloadablePDF);
            document.close();

        } catch (Exception e) {
            LOG.info(e.getMessage());
        }

        return downloadablePDF;
    }

    private void generateCombinedContract(AssetStreamResult signature, PDDocument document) {
        try {
            InputStream imageInputStream = signature.getStream();
            BufferedImage bufferImage = ImageIO.read(imageInputStream);
            PDImageXObject pdImage = LosslessFactory.createFromImage(document, bufferImage);

            document.addPage(new PDPage());
            PDPage page = document.getPage(document.getNumberOfPages() - 1);

            PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, false);
            contentStream.drawImage(pdImage, 20, 600, 550, 200);
            contentStream.close();

        } catch (IOException e) {
            LOG.info(e.getMessage());
        } finally {

        }
    }

    /**
     * @param shopId : shop id for which we need to get all asset
     */
    @Override
    public ShopAssetResult copyMemberAssetByShop(String shopId) {

        ShopAssetResult result = new ShopAssetResult();

        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        try {
            String shopFolder = shop.getName() + "--" + DateTime.now().getMillis();

            Iterable<Member> memberHashMap = memberRepository.list(token.getCompanyId());

            //<memberId, <key (folder name, info)>
            HashMap<String, HashMap<String, Object>> memberInfoMap = new HashMap<>();
            LinkedHashSet<String> memberFolderList = new LinkedHashSet<>();
            for (Member member : memberHashMap) {
                String memberFolder = member.getFirstName() + " " + member.getLastName() + "-" + member.getId();
                memberFolderList.add(memberFolder);

                HashMap<String, Object> memberDataMap = new HashMap<>();
                memberDataMap.put("folder", memberFolder);
                List<String> assetKeyList = new ArrayList<>();

                //Get identification's asset keys
                if (member.getIdentifications() != null) {
                    for (Identification identification : member.getIdentifications()) {
                        if (identification.getAssets() != null) {
                            for (CompanyAsset companyAsset : identification.getAssets()) {
                                if (StringUtils.isNotBlank(companyAsset.getKey())) {
                                    assetKeyList.add(companyAsset.getKey());
                                }
                            }
                        }
                        if (identification.getFrontPhoto() != null && StringUtils.isNotBlank(identification.getFrontPhoto().getKey())) {
                            assetKeyList.add(identification.getFrontPhoto().getKey());
                        }
                    }
                }

                //Get recommendation's asset keys
                if (member.getRecommendations() != null) {
                    for (Recommendation recommendation : member.getRecommendations()) {
                        if (recommendation.getAssets() != null) {
                            for (CompanyAsset companyAsset : recommendation.getAssets()) {
                                if (StringUtils.isNotBlank(companyAsset.getKey())) {
                                    assetKeyList.add(companyAsset.getKey());
                                }
                            }
                        }
                        if (recommendation.getFrontPhoto() != null && StringUtils.isNotBlank(recommendation.getFrontPhoto().getKey())) {
                            assetKeyList.add(recommendation.getFrontPhoto().getKey());
                        }
                    }
                }

                //Get contract's asset key
                if (member.getContracts() != null) {
                    for (SignedContract contract : member.getContracts()) {
                        if (contract.getSignaturePhoto() != null && StringUtils.isNotBlank(contract.getSignaturePhoto().getKey())) {
                            assetKeyList.add(contract.getSignaturePhoto().getKey());
                        }
                        if (contract.getEmployeeSignaturePhoto() != null && StringUtils.isNotBlank(contract.getEmployeeSignaturePhoto().getKey())) {
                            assetKeyList.add(contract.getEmployeeSignaturePhoto().getKey());
                        }
                        if (contract.getWitnessSignaturePhoto() != null && StringUtils.isNotBlank(contract.getWitnessSignaturePhoto().getKey())) {
                            assetKeyList.add(contract.getWitnessSignaturePhoto().getKey());
                        }
                    }
                }

                if (assetKeyList.size() > 0) {
                    memberDataMap.put("asset", assetKeyList);
                    memberInfoMap.putIfAbsent(member.getId(), memberDataMap);
                }
            }

            amazonS3Service.createFolder(shopFolder);

            LOG.info("Copying: " + memberInfoMap.size());
            int i = 0;
            for (Map.Entry<String, HashMap<String, Object>> entry : memberInfoMap.entrySet()) {

                i++;
                HashMap<String, Object> empData = entry.getValue();

                String destinationFolder = shopFolder + "/" + (String) empData.get("folder");
                List<String> assetKeyList = (List<String>) empData.get("asset");
                String memberFolderName = empData.get("folder").toString();
                LOG.info(String.format("%d / %d, Copying assets for: %s", i, memberInfoMap.size(), memberFolderName));

                boolean created = false;
                if (assetKeyList != null) {
                    for (String sourceKey : assetKeyList) {
                        try {
                            if (!created) {
                                amazonS3Service.createFolder(shopFolder + "/" + memberFolderName);
                                created = true;
                            }

                            amazonS3Service.copyFile(destinationFolder, sourceKey, sourceKey);
                        } catch (Exception e) {
                            LOG.info("Error copying file: " + sourceKey + ", member: " + memberFolderName);
                        }
                    }
                }
            }


            LOG.info("Copy done!!");
            ShopAssetToken token = new ShopAssetToken();
            token.setExpirationDate(DateTime.now().plusDays(1).getMillis());
            token.setFolderName(shopFolder);
            token.setShopId(shopId);

            result.setCopied(true);
            result.setShopAssetToken(securityUtil.encryptAssetDownloadToken(token));
            return result;
        } catch (Exception e) {
            LOG.error("Error while copying asset of shop : " + shopId + ", error :" + e.getMessage(), e);
        }

        return result;
    }

    @Override
    public FileInputStream exportMemberAssetByShop(String shopAssetToken) {
        ShopAssetToken token = securityUtil.decryptAssetDownloadToken(shopAssetToken);
        if (token == null) {
            throw new BlazeInvalidArgException(TOKEN, INVALID_TOKEN);
        }
        if (!token.isValidToken() || StringUtils.isBlank(token.getFolderName())) {
            throw new BlazeInvalidArgException(TOKEN, TOKEN_EXPIRED);
        }

        try {
            String basePath = System.getProperty("java.io.tmpdir");
            String extension = ".zip";
            String directory = basePath + File.separator + token.getFolderName();
            File dir = new File(directory);

            //Download folder in tmp
            MultipleFileDownload multipleFileDownload = amazonS3Service.downloadFolder(token.getFolderName(), dir);

            if (multipleFileDownload != null && multipleFileDownload.getState().toString().equals("Completed")) {
                String zipFileName = directory + extension;

                File dirObj = new File(directory + File.separator);
                ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
                addDir(dirObj, out);
                out.close();

                if (dirObj.exists()) {
                    delete(dirObj);
                }
                return new FileInputStream(zipFileName);
            } else {
                throw new BlazeInvalidArgException(MEMBER_ASSET, ERROR_DOWNLOAD_ASSET);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(MEMBER_ASSET, ERROR_DOWNLOAD_ASSET);
        }
    }

    private void addDir(File dirObj, ZipOutputStream out) throws IOException {
        File[] files = dirObj.listFiles();
        byte[] tmpBuf = new byte[1024];

        for (File file : files) {
            if (file.isDirectory()) {
                addDir(file, out);
                continue;
            }
            FileInputStream in = new FileInputStream(file.getAbsolutePath());
            out.putNextEntry(new ZipEntry(file.getAbsolutePath()));
            int len;
            while ((len = in.read(tmpBuf)) > 0) {
                out.write(tmpBuf, 0, len);
            }
            out.closeEntry();
            in.close();
        }
    }

    public void delete(File file) throws IOException {

        if (file.isDirectory()) {
            if (file.list().length == 0) {
                file.delete();
            } else {
                String files[] = file.list();
                for (String temp : files) {
                    File fileDelete = new File(file, temp);
                    delete(fileDelete);
                }
                if (file.list().length == 0) {
                    file.delete();
                }
            }
        } else {
            file.delete();
        }
    }

    @Override
    public AssetStreamResult getCompanyAssetStream(String assetKey, String assetToken, boolean handleError, boolean isReportRequest, String fileName) {
        AssetStreamResult result = null;
        try {
            result = getCompanyAssetStream(assetKey, assetToken, isReportRequest);
            if (result != null) {
                result.setKey(fileName);
            }

        } catch (Exception e) {
            LOG.error("Error while getting asset :" + assetKey + "");
        }
        if (handleError && result == null) {
            result = new AssetStreamResult();
            result.setContentType("application/pdf");
            result.setStream(new ByteArrayInputStream(PdfGenerator.getNoDocumentPdf()));
            result.setKey(assetKey);
            CompanyAsset asset = new CompanyAsset();
            asset = new CompanyAsset();
            asset.setType(Asset.AssetType.Document);
            result.setAsset(asset);
        } else if (result == null) {
            throw new BlazeInvalidArgException("CompanyAsset", "Invalid asset");
        }

        return result;
    }

    @Override
    public AssetStreamResult getCompanyAssetStreamWithoutAsset(String assetKey, String assetToken, CompanyAsset asset) {
        if (assetKey != null && assetKey.startsWith("420default-")) {
            AssetStreamResult result = amazonS3Service.downloadFile(assetKey, true);
            if (result == null) {
                throw new BlazeInvalidArgException("Asset", "Invalid asset.");
            }
            result.setAsset(null);
            return result;
        }

        String companyId = null;
        String shopId = null;
        if (token != null && token.isValid() && StringUtils.isNotEmpty(token.getCompanyId())) {
            companyId = token.getCompanyId();
            shopId = token.getShopId();
        } else if (StringUtils.isNotEmpty(assetToken)) {
            try {
                AssetAccessToken assetAccessToken = securityUtil.decryptAssetAccessToken(assetToken);
                companyId = assetAccessToken.getCompanyId();
                shopId = assetAccessToken.getShopId();
            } catch (Exception e) {
                // ignore
            }
        }
        if (companyId == null) {
            throw new BlazeAuthException("Authorization", "Invalid companyId");
        }

        if (asset == null) {
            // Find by category
            ProductCategory productCategory = categoryRepository.getCategoryByPhotoKey(companyId, assetKey);
            if (productCategory != null) {
                asset = productCategory.getPhoto();
            }

            if (asset == null) {
                Product product = productRepository.findProductWithAssetKey(companyId, assetKey);
                if (product != null) {
                    for (CompanyAsset asset1 : product.getAssets()) {
                        if (asset1.getKey().equalsIgnoreCase(assetKey)) {
                            asset = asset1;
                        }
                    }
                }
            }


        }

        if (asset == null) {
            throw new BlazeInvalidArgException("CompanyAsset", "Invalid asset");
        } else {
            LOG.info("Accessing image: " + asset.getKey() + " secured: " + asset.getKey() + " purl: " + asset.getPublicURL());
            if (asset.getPublicURL() != null && asset.getPublicURL().contains("420default-")) {
                asset.setKey("420default-" + asset.getName());
            }
        }

        AssetStreamResult result = amazonS3Service.downloadFile(assetKey, asset.isSecured());
        if (result != null) {
            result.setAsset(asset);
        }
        return result;
    }

    /*private AssetStreamResult getReportRequest(String assetKey, String assetToken) {
        AssetStreamResult result = new AssetStreamResult();
        try {
            AssetAccessToken assetAccessToken = securityUtil.decryptAssetAccessToken(assetToken);
            String companyId = assetAccessToken.getCompanyId();
            String shopId = assetAccessToken.getShopId();
            String employeeId = assetAccessToken.getUserId();
            result = reportRequestService.downloadReport(companyId, shopId, employeeId, assetKey);
        } catch (Exception e) {
            LOG.error("Error in report request download", e);
        }
        return result;
    }*/

}