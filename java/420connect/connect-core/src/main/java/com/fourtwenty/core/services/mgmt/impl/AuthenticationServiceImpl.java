package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.caching.CacheService;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.SwitchableApplication;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.models.common.MetrcConfig;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.VersionErrorException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.rest.dispensary.requests.auth.*;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyRegisterRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalAssignRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalSetupRequest;
import com.fourtwenty.core.rest.dispensary.results.InitialLoginResult;
import com.fourtwenty.core.rest.dispensary.results.auth.ManagerAccessResult;
import com.fourtwenty.core.rest.dispensary.results.auth.SwitchApplicationResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberTagResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import com.fourtwenty.core.security.Version;
import com.fourtwenty.core.security.tokens.AssetAccessToken;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.TerminalUser;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.AuditLogService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.AuthenticationService;
import com.fourtwenty.core.services.mgmt.DefaultDataService;
import com.fourtwenty.core.services.mgmt.EmailService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.payments.StripeService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by mdo on 8/28/15.
 */
public class AuthenticationServiceImpl extends AbstractAuthServiceImpl implements AuthenticationService {
    private static final Log LOG = LogFactory.getLog(AuthenticationServiceImpl.class);
    private static final String EMPLOYEE_NOT_ACCESS = "You don't have access for this application. Please contact admin.";
    private static final String ASSIGNED_SHOP_NOT_FOUND = "You don't have access for this application or assigned shop is not active. Please contact admin.";

    @Inject
    SecurityUtil securityUtil;
    @Inject
    ConnectConfiguration connectConfiguration;

    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    DefaultDataService defaultDataService;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ContractRepository contractRepository;
    @Inject
    RoleService roleService;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    TimeCardRepository timeCardRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    AuditLogService auditLogService;
    @Inject
    ConnectProductRepository connectProductRepository;
    @Inject
    EmailService emailService;
    @Inject
    RoleRepository roleRepository;
    @Inject
    PasswordResetRepository passwordResetRepository;
    @Inject
    BackgroundTaskManager taskManager;
    @Inject
    AppRepository appRepository;
    @Inject
    ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    private MetrcAccountRepository metrcAccountRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private CacheService cacheService;
    @Inject
    private StripeService stripeService;
    @Inject
    IntegrationSettingRepository integrationSettingRepository;

    private static final String ACCESS_TOKEN = "Access Token";
    private static final String INVALID_ACCESS_TOKEN = "Invalid access token";
    private static final String SWITCH_APPLICATION = "Switch Application";
    private static final int CACHE_LIMIT_MINS = 10; // 10 minutes

    @Inject
    public AuthenticationServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    private void assignExciseTax(List<ShopResult> shops, Iterable<ExciseTaxInfo> exciseTaxInfos) {
        if (exciseTaxInfos != null) {
            for (ShopResult shop : shops) {
                if (shop != null && shop.getAddress() != null) {
                    if (shop.getAddress().getState() != null && shop.getAddress().getCountry() != null) {
                        for (ExciseTaxInfo exciseTaxInfo : exciseTaxInfos) {
                            if (shop.getAddress().getState().equalsIgnoreCase(exciseTaxInfo.getState())
                                    && shop.getAddress().getCountry().equalsIgnoreCase(exciseTaxInfo.getCountry())) {
                                if (exciseTaxInfo.getCannabisTaxType() == ExciseTaxInfo.CannabisTaxType.Cannabis) {
                                    shop.setExciseTaxInfo(exciseTaxInfo);
                                }
                                if (exciseTaxInfo.getCannabisTaxType() == ExciseTaxInfo.CannabisTaxType.NonCannabis) {
                                    shop.setNonCannabisTaxInfo(exciseTaxInfo);
                                }
                            }
                        }
                    }

                }
            }
        }

        // set default values for non cannabis taxes
        for (ShopResult shop : shops) {
            if (shop.getNonCannabisTaxes() == null) {
                shop.setNonCannabisTaxes(new NonCannabisTaxInfo());
            }
            if (shop.getNonCannabisTaxes().getTaxInfo() == null) {
                shop.getNonCannabisTaxes().setTaxInfo(new TaxInfo());
            }


            if (shop.getNonCannabisTaxes().getTaxInfo() != null && StringUtils.isBlank(shop.getNonCannabisTaxes().getTaxInfo().getId())) {
                TaxInfo taxInfo = shop.getNonCannabisTaxes().getTaxInfo();
                taxInfo.prepare();
                shop.getNonCannabisTaxes().setTaxInfo(taxInfo);
            }
            if (CollectionUtils.isNullOrEmpty(shop.getNonCannabisTaxes().getTaxTables())) {
                List<ConsumerType> consumerTypes = Lists.newArrayList(ConsumerType.values());
                for (ConsumerType consumerType : consumerTypes) {
                    CompoundTaxTable taxTable = new CompoundTaxTable();
                    if (consumerType == ConsumerType.Other) {
                        continue;
                    }
                    taxTable.prepare(shop.getCompanyId());
                    taxTable.setShopId(shop.getId());
                    taxTable.setName(consumerType.getDisplayName());
                    taxTable.setConsumerType(consumerType);

                    taxTable.setCityTax(new CompoundTaxRate());
                    taxTable.setCountyTax(new CompoundTaxRate());
                    taxTable.setFederalTax(new CompoundTaxRate());
                    taxTable.setStateTax(new CompoundTaxRate());
                    taxTable.reset();

                    taxTable.getCityTax().prepare(shop.getCompanyId());
                    taxTable.getCityTax().setShopId(shop.getId());

                    taxTable.getCountyTax().prepare(shop.getCompanyId());
                    taxTable.getCountyTax().setShopId(shop.getId());

                    taxTable.getFederalTax().prepare(shop.getCompanyId());
                    taxTable.getFederalTax().setShopId(shop.getId());

                    taxTable.getStateTax().prepare(shop.getCompanyId());
                    taxTable.getStateTax().setShopId(shop.getId());

                    shop.getNonCannabisTaxes().getTaxTables().add(taxTable);
                }
            }
        }
    }

    @Override
    public InitialLoginResult getCurrentActiveEmployee() {
        List<TerminalUser> terminalUsers = token.getActiveUsers();
        if (terminalUsers.size() == 0) {
            throw new BlazeAuthException("ActiveSession", "This session is invalid");
        }
        Employee employee = employeeRepository.getById(token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeAuthException("ActiveSession", "This session is invalid. User no longer exists.");
        }
        Company company = companyRepository.getById(token.getCompanyId());

        if (company == null || !company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }
        Iterable<ShopResult> items = shopRepository.list(token.getCompanyId(), ShopResult.class);
        List<ShopResult> shops = Lists.newArrayList(items);
        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();
        assignExciseTax(shops, exciseTaxInfos);

        ShopResult assignedShop = shopRepository.get(token.getCompanyId(), token.getShopId(), ShopResult.class);
        assignExciseTax(Lists.newArrayList(assignedShop), exciseTaxInfos);
        Terminal terminal = null;
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
        }

        Role role = roleService.getRoleById(employee.getCompanyId(), employee.getRoleId());
        boolean isAdmin = (role != null && role.getName().equalsIgnoreCase("Admin"));

        ShopResult lastLoggedInShop = null;
        if (StringUtils.isNotBlank(employee.getLastLoggedInShopId())) {
            lastLoggedInShop = shopRepository.get(employee.getCompanyId(), employee.getLastLoggedInShopId(), ShopResult.class);
        }

        List<ShopResult> employeeShops = new ArrayList<>();
        if (CompanyFeatures.AppTarget.AuthenticationApp == token.getAppTarget()) {
            assignedShop = this.getAssignedShop(shops, employee, lastLoggedInShop, employeeShops, isAdmin);
        } else {
            assignedShop = this.getAssignedShopByAppTarget(shops, token.getAppTarget(), employee, lastLoggedInShop, employeeShops, isAdmin);
        }


        employee.setRole(role);
        // hide password
        employee.setPassword(null);

        prepareAssignedShop(assignedShop);
        prepareEmployeeShops(employeeShops, token.getCompanyId());

        InitialLoginResult result = new InitialLoginResult();
        result.setAccessToken(getNewEncryptedToken(token));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), token.getShopId(), employee.getId())));
        result.setEmployee(employee);
        result.setShops(shops);
        result.setAssignedShop(assignedShop);
        result.setLoginTime(token.getInitDate());
        result.setExpirationTime(token.getExpirationDate());
        result.setAssignedTerminal(terminal);
        result.setCompany(company);
        result.setAppTarget(token.getAppTarget());
        result.setEmployeeShops(employeeShops);

        return result;
    }

    @Override
    public InitialLoginResult terminalAssignEmployee(TerminalAssignRequest request) {
        List<TerminalUser> terminalUsers = token.getActiveUsers();
        if (terminalUsers.size() == 0) {
            throw new BlazeAuthException("ActiveSession", "This session is invalid");
        }
        Employee dbEmployee = employeeRepository.getById(token.getActiveTopUser().getUserId());
        if (dbEmployee == null) {
            throw new BlazeAuthException("ActiveSession", "This session is invalid. User no longer exists.");
        }
        Company company = companyRepository.getById(token.getCompanyId());

        if (company == null || !company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }


        // Check to see if this Device Id is a new Device
        Terminal terminal = terminalRepository.get(token.getCompanyId(), request.getTerminalId());
        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }
        if (!terminal.isActive() || terminal.isDeleted()) {
            throw new BlazeInvalidArgException("Terminal", "Terminal is no longer active.");
        }

        employeeRepository.setTerminalId(token.getCompanyId(), dbEmployee.getId(), terminal.getId());

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        Iterable<ShopResult> items = shopRepository.list(token.getCompanyId(), ShopResult.class);
        List<ShopResult> shops = Lists.newArrayList(items);
        String shopId = null;
        ShopResult assignedShop = null;
        List<ShopResult> assignedShops = new ArrayList<>();


        assignedShop = shopRepository.get(token.getCompanyId(), terminal.getShopId(), ShopResult.class);

        if (assignedShop == null) {
            // Filter out shops assigned to this employee
            for (ShopResult shop : shops) {
                if (dbEmployee.getShops().contains(shop.getId())) {
                    assignedShops.add(shop);
                }
            }
            if (assignedShops.size() > 0) {
                assignedShop = assignedShops.get(0);
            } else if (shops != null && shops.size() > 0) {
                assignedShop = shops.get(0);
                shopId = assignedShop.getId();
            }
        }

        assignExciseTax(shops, exciseTaxInfos);
        assignExciseTax(Lists.newArrayList(assignedShop), exciseTaxInfos);


        // Set assigned terminal
        token.setTerminalId(terminal.getId());
        if (dbEmployee.getAssignedTerminalId() == null
                || !terminal.getId().equalsIgnoreCase(dbEmployee.getAssignedTerminalId())) {
            // Update assigned terminal if it's not set or different
            dbEmployee.setAssignedTerminalId(terminal.getId());
            employeeRepository.updateEmployeeAssignedTerminal(dbEmployee.getCompanyId(), dbEmployee.getId(), terminal.getId());
        }

        // hide password
        dbEmployee.setPassword(null);

        Role role = roleService.getRoleById(dbEmployee.getCompanyId(), dbEmployee.getRoleId());
        dbEmployee.setRole(role);

        boolean isAdmin = (role != null && role.getName().equalsIgnoreCase("Admin"));

        List<ShopResult> employeeShops = new ArrayList<>();
        for (ShopResult shop : shops) {
            if (dbEmployee.getShops() != null && ((dbEmployee.getShops().contains(shop.getId()) && shop.isActive()) || isAdmin)) {
                employeeShops.add(shop);
            }
        }

        InitialLoginResult result = new InitialLoginResult();
        result.setAccessToken(getNewEncryptedToken(token));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(dbEmployee.getCompanyId(), shopId, dbEmployee.getId())));
        result.setEmployee(dbEmployee);
        result.setShops(Lists.newArrayList(shops));
        result.setAssignedShop(assignedShop);
        result.setLoginTime(token.getInitDate());
        result.setExpirationTime(token.getExpirationDate());
        result.setCompany(company);
        result.setAssignedTerminal(terminal);
        result.setAppTarget(token.getAppTarget());
        result.setEmployeeShops(employeeShops);
        return result;
    }

    @Override
    public InitialLoginResult adminLogin(EmailLoginRequest request) {
        if (request == null || StringUtils.isBlank(request.getEmail()) || StringUtils.isBlank(request.getPassword())) {
            throw new BlazeAuthException("EmailLoginRequest", "Invalid Argument");
        }

        String email = request.getEmail().toLowerCase();
        Employee employee = employeeRepository.getEmployeeByEmail(email);

        if (employee == null) {
            throw new BlazeAuthException("EmailLoginRequest", "Invalid credentials");
        }

        if (employee.isDisabled()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }

        if (employee.isDeleted()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }

        if (!securityUtil.checkPassword(request.getPassword(), employee.getPassword())) {
            throw new BlazeAuthException("EmailLoginRequest", "Invalid credentials");
        }


        Role role = roleService.getRoleById(employee.getCompanyId(), employee.getRoleId());
        boolean isAdmin = (role != null && role.getName().equalsIgnoreCase("Admin"));

        if (!isAdmin && request.getAppTarget() != CompanyFeatures.AppTarget.AuthenticationApp && employee.getAppAccessList() != null && !employee.getAppAccessList().contains(request.getAppTarget())) {
            throw new BlazeInvalidArgException("EmailLoginRequest", EMPLOYEE_NOT_ACCESS);
        }

        Company company = companyRepository.getById(employee.getCompanyId());

        if (company == null || !company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }

        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(employee.getCompanyId());
        if (companyFeatures == null) {
            throw new BlazeInvalidArgException("CompanyFeatures", "Unknown company feature.");
        }

        // Check for availableApps
        CompanyFeatures.AppTarget companyAppTarget = request.getAppTarget();
        if (companyFeatures.getAvailableApps() == null || !companyFeatures.getAvailableApps().contains(companyAppTarget)) {
            throw new BlazeAuthException("Company", company.getName() + " is not configured for " + request.getAppTarget());
        }

        roleService.checkPermission(employee.getCompanyId(), employee.getRoleId(), Role.Permission.WebAdminLogin);

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        //hideEmployeeAuthInfo(employee);
        employee.setPassword(null);

        Iterable<ShopResult> items = shopRepository.list(employee.getCompanyId(), ShopResult.class);
        List<ShopResult> shops = Lists.newArrayList(items);
        ShopResult lastLoggedInShop = null;
        if (StringUtils.isNotBlank(employee.getLastLoggedInShopId())) {
            lastLoggedInShop = shopRepository.get(employee.getCompanyId(), employee.getLastLoggedInShopId(), ShopResult.class);
        }

        String shopId = null;
        ShopResult assignedShop = null;
        List<ShopResult> employeeShops = new ArrayList<>();

        if (request.getAppTarget() == null) {
            request.setAppTarget(CompanyFeatures.AppTarget.AuthenticationApp);
        }


        if (CompanyFeatures.AppTarget.AuthenticationApp == request.getAppTarget()) {
            assignedShop = this.getAssignedShop(shops, employee, lastLoggedInShop, employeeShops, isAdmin);
        } else {
            assignedShop = this.getAssignedShopByAppTarget(shops, request.getAppTarget(), employee, lastLoggedInShop, employeeShops, isAdmin);
        }


        //If employee does not have request target's shop then throw error
        if (assignedShop == null) {
            throw new BlazeInvalidArgException("EmailLoginRequest", ASSIGNED_SHOP_NOT_FOUND);
        }

        shopId = assignedShop.getId();

        patchTaxInfo(assignedShop);

        assignExciseTax(shops, exciseTaxInfos);
        assignExciseTax(Lists.newArrayList(assignedShop), exciseTaxInfos);


        Terminal terminal = null;
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
        }

        employee.setRole(role);

        ConnectAuthToken connectAuthToken = createNewTokenWebsite(employee.getCompanyId(),
                shopId,
                employee.getId(),
                employee.getAssignedTerminalId(),
                employee.getFirstName(),
                employee.getLastName(), assignedShop.getAppTarget());

        connectAuthToken.setRequestTimeZone(request.getTimezone());

        employee.setLastLoggedInShopId(shopId);

        prepareAssignedShop(assignedShop);
        prepareEmployeeShops(employeeShops, company.getId());


        InitialLoginResult result = new InitialLoginResult();
        result.setAccessToken(getNewEncryptedToken(connectAuthToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), shopId, employee.getId())));
        result.setEmployee(employee);
        result.setShops(shops);
        result.setLoginTime(connectAuthToken.getInitDate());
        result.setExpirationTime(connectAuthToken.getExpirationDate());
        result.setAssignedShop(assignedShop);
        result.setAssignedTerminal(terminal);
        result.setCompany(company);
        result.setAppTarget(request.getAppTarget());
        result.setEmployeeShops(employeeShops);

        employeeRepository.updateLastLogin(employee.getCompanyId(), employee.getId(), shopId);

        auditLogService.addAuditLog(connectAuthToken, "/api/v1/mgmt/session", "Management - Authentication", "Admin Login");
        //taskManager.takeInventorySnapshot(employee.getCompanyId(), shopId);

        return result;
    }

    private ShopResult getAssignedShop(List<ShopResult> shops, Employee employee, ShopResult lastLoggedInShop, List<ShopResult> employeeAllShops, boolean isAdmin) {
        ShopResult assignedShop = null;

        for (ShopResult shop : shops) {
            if (employee.getShops() != null && ((employee.getShops().contains(shop.getId()) && shop.isActive()) || isAdmin)) {
                employeeAllShops.add(shop);
            }
        }

        List<ShopResult> employeeShops = new ArrayList<>();
        employeeAllShops.forEach(shopResult -> {
            if (shopResult.isActive()) {
                employeeShops.add(shopResult);
            }
        });

        //If employee have last logged in shop then check if employee have access to it
        if (lastLoggedInShop != null) {
            LOG.info("Last Login Shop: " + lastLoggedInShop.getName());
            //check if user have last logged in shop in access list

            for (ShopResult shop : employeeShops) {
                if (lastLoggedInShop.getId().equalsIgnoreCase(shop.getId())) {

                    if (!shop.isActive() || (isAdmin && !employee.getAppAccessList().contains(lastLoggedInShop.getAppTarget()))) {
                        break;
                    }
                    assignedShop = shop;

                    break;
                }
            }
        }

        // default
        if (assignedShop == null) {
            //If user does not have last logged in shop or does not have access to last logged in shop then check weather it have retail shop. If yes then return retail shop otherwise return different shop
            for (ShopResult shop : employeeShops) {
                //If admin then assign any shop and if not admin then check app target first
                if (isAdmin || (shop.isActive() && employee.getAppAccessList().contains(shop.getAppTarget()))) {
                    assignedShop = shop;
                    break;
                }
            }
        }

        return assignedShop;
    }

    private ShopResult getAssignedShopByAppTarget(List<ShopResult> shops, CompanyFeatures.AppTarget requestAppTarget, Employee employee, ShopResult lastLoggedInShop, List<ShopResult> employeeAllShops, boolean isAdmin) {

        if (!isAdmin && !employee.getAppAccessList().contains(requestAppTarget)) {
            throw new BlazeInvalidArgException("EmailLoginRequest", EMPLOYEE_NOT_ACCESS);
        }

        ShopResult assignedShop = null;
        Boolean lastLoggedHaveAccess = Boolean.FALSE;

        //Get employee's access shop list
        for (ShopResult shop : shops) {
            if (employee.getShops() != null && ((employee.getShops().contains(shop.getId()) && shop.isActive()) || isAdmin)) {
                employeeAllShops.add(shop);
            }
        }

        List<ShopResult> employeeShops = new ArrayList<>();
        employeeAllShops.forEach(shopResult -> {
            if (shopResult.isActive()) {
                employeeShops.add(shopResult);
            }
        });

        if (lastLoggedInShop != null && lastLoggedInShop.getAppTarget() == requestAppTarget) {
            //check if user have last logged in shop in access list and match with app target
            for (ShopResult shop : employeeShops) {
                if (lastLoggedInShop.getId().equalsIgnoreCase(shop.getId()) && requestAppTarget == shop.getAppTarget()
                        && (isAdmin || (shop.isActive() && employee.getAppAccessList().contains(lastLoggedInShop.getAppTarget())))) {
                    lastLoggedHaveAccess = Boolean.TRUE;
                    break;
                }
            }

            if (lastLoggedHaveAccess) {
                assignedShop = lastLoggedInShop;
            } else {
                for (ShopResult shop : employeeShops) {
                    if (requestAppTarget == shop.getAppTarget() && (isAdmin || (shop.isActive() && employee.getAppAccessList().contains(shop.getAppTarget())))) {
                        assignedShop = shop;
                    }
                }
            }

        } else {

            //If user does not have last logged in shop or does not have access to last logged in shop then check weather it have retail shop. If yes then return retail shop otherwise return different shop
            for (ShopResult shop : employeeShops) {
                if (shop.getAppTarget() == requestAppTarget && (shop.isActive() && employee.getAppAccessList().contains(shop.getAppTarget()) || isAdmin)) {
                    assignedShop = shop;
                    break;
                }
            }
        }
        if (CollectionUtils.isNullOrEmpty(employeeShops)) {
            assignedShop = null;
        }
        return assignedShop;

    }

    @Override
    public InitialLoginResult updateTokenWithShop(UpdateTokenRequest request) {
        InitialLoginResult result = new InitialLoginResult();

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeAuthException("Employee", "Invalid employee.");
        }

        Shop requestShop = shopRepository.getById(request.getShopId());
        //Company company = companyRepository.getById(employee.getCompanyId());
        Company company = companyRepository.getById(requestShop.getCompanyId());

        if (company == null || !company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }
        Terminal terminal = null;
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(company.getId(), employee.getAssignedTerminalId());
        }
        Role role = roleService.getRoleById(employee.getCompanyId(), employee.getRoleId());
        employee.setRole(role);

        boolean isAdmin = (role != null && ("Admin").equalsIgnoreCase(role.getName()));

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();
        Iterable<ShopResult> items = shopRepository.getEmployeeShops(employee);
        List<ShopResult> shops = Lists.newArrayList(items);
        ShopResult assignedShop = null;
        List<ShopResult> employeeAllShop = new ArrayList<>();

        // switch shop
        for (ShopResult shop : shops) {
            if (isAdmin || (employee.getShops().contains(shop.getId()) && shop.isActive())) {
                employeeAllShop.add(shop);
            }
        }

        List<ShopResult> employeeShop = new ArrayList<>();
        employeeAllShop.forEach(shopResult -> {
            if (shopResult.isActive()) {
                employeeShop.add(shopResult);
            }
        });

        for (ShopResult shop : employeeShop) {
            if ((shop.getId().equalsIgnoreCase(request.getShopId()))) {
                assignedShop = shop;
            }
        }


        if (assignedShop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        assignExciseTax(shops, exciseTaxInfos);
        assignExciseTax(Lists.newArrayList(assignedShop), exciseTaxInfos);

        String companyId = null;
        if(token.getCompanyId() != assignedShop.getCompanyId()) {
            companyId = assignedShop.getCompanyId();
        }

        if (!isAdmin && !assignedShop.isActive()) {
            throw new BlazeInvalidArgException("EmailLoginRequest", ASSIGNED_SHOP_NOT_FOUND);
        }

        ConnectAuthToken newToken = updateAuthToken(token, companyId, request.getShopId(), assignedShop);
        employee.setLastLoggedInShopId(request.getShopId());

        prepareAssignedShop(assignedShop);
        prepareEmployeeShops(employeeAllShop, newToken.getCompanyId());

        if (newToken.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
            if (request.getGrowAppType() == ConnectAuthToken.GrowAppType.None) {
                throw new BlazeInvalidArgException("GrowAppType", "GrowAppType is missing");
            }
            newToken.setGrowAppType(request.getGrowAppType());
            result.setAccessToken(getInternalApiEncryptedToken(newToken));
            AssetAccessToken newAssetToken = createNewAssetToken(newToken.getCompanyId(), request.getShopId(), newToken.getActiveTopUser().getUserId());
            if (newToken.getGrowAppType() == ConnectAuthToken.GrowAppType.Operations) {
                result.setAssetAccessToken(getInternalApiEncryptedAssetToken(newAssetToken));
            } else {
                result.setAssetAccessToken(getNewEncryptedAssetToken(newAssetToken));
            }
        } else {
            newToken.setGrowAppType(ConnectAuthToken.GrowAppType.None);
            result.setAccessToken(getNewEncryptedToken(newToken));
            result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(newToken.getCompanyId(), request.getShopId(), newToken.getActiveTopUser().getUserId())));
        }

        result.setEmployee(employee);
        result.setShops(shops);
        result.setLoginTime(newToken.getInitDate());
        result.setExpirationTime(newToken.getExpirationDate());
        result.setAssignedShop(assignedShop);
        result.setAssignedTerminal(terminal);
        result.setCompany(company);
        result.setAppTarget(assignedShop.getAppTarget());
        result.setEmployeeShops(employeeAllShop);
        employeeRepository.updateLastLogin(employee.getCompanyId(), employee.getId(), request.getShopId());

        return result;
    }

    @Override
    public InitialLoginResult renewToken() {
        return renewToken(ConnectAuthToken.GrowAppType.None);
    }

    @Override
    public InitialLoginResult renewToken(ConnectAuthToken.GrowAppType growAppType) {
        ConnectAuthToken newToken = rewnewAuthToken(token);
        newToken.setGrowAppType(growAppType); //set grow app type for grow shop

        InitialLoginResult result = new InitialLoginResult();

        prepareLoginResult(newToken, result);

//        result.setAccessToken(getNewEncryptedToken(newToken));
        result.setLoginTime(newToken.getInitDate());
        result.setExpirationTime(newToken.getExpirationDate());
        return result;
    }

    private void prepareLoginResult(ConnectAuthToken token, InitialLoginResult loginResult) {


        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId(), "{password:0,pin:0}");
        Company company = companyRepository.getById(token.getCompanyId());

        //Iterable<ShopResult> items = shopRepository.list(token.getCompanyId(), ShopResult.class);
        Iterable<ShopResult> items = shopRepository.getEmployeeShops(employee);

        Role role = roleService.getRoleById(employee.getCompanyId(), employee.getRoleId());
        boolean isAdmin = (role != null && role.getName().equalsIgnoreCase("admin"));
        employee.setRole(role);

        List<ShopResult> shops = Lists.newArrayList(items);
        String shopId = null;
        ShopResult currentShop = null;
        List<ShopResult> employeeAllShops = new ArrayList<>();
        List<ShopResult> employeeShops = new ArrayList<>();

        // Filter out shops assigned to this employee
        for (ShopResult shop : shops) {
            if ((employee.getShops().contains(shop.getId()) && shop.isActive()) || isAdmin) {
                employeeAllShops.add(shop);
            }
        }

        // Filter out shops assigned to this employee
        for (ShopResult shop : employeeAllShops) {
            if (shop.isActive()) {
                employeeShops.add(shop);
            }
        }

        for (ShopResult employeeShop : employeeShops) {
            if ((employee.getShops().contains(employeeShop.getId()) && employeeShop.isActive()) || isAdmin) {
                if ((CompanyFeatures.AppTarget.AuthenticationApp == token.getAppTarget() || employeeShop.getAppTarget() == token.getAppTarget()) && employeeShop.getId().equalsIgnoreCase(token.getShopId())) {
                    currentShop = employeeShop;
                }
            }
        }
        //Check if employee contains shop for requested app target
        boolean employeeAccess = Boolean.FALSE;
        for (ShopResult shop : employeeShops) {
            if ((CompanyFeatures.AppTarget.AuthenticationApp == token.getAppTarget() || shop.getAppTarget().equals(token.getAppTarget()))) {
                employeeAccess = Boolean.TRUE;
                //If current shop not exist then assign new one
                if (currentShop == null) {
                    currentShop = shop;
                }
                break;
            }
        }

        if ((isAdmin && currentShop == null) || (!isAdmin && !employeeAccess && currentShop == null)) {
            throw new BlazeInvalidArgException("EmailLoginRequest", EMPLOYEE_NOT_ACCESS);
        }

        Terminal terminal = null;
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(token.getCompanyId(), employee.getAssignedTerminalId());
        }

        shopId = currentShop.getId();

        assignExciseTax(shops, exciseTaxInfos);
        token.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());

        prepareAssignedShop(currentShop);
        prepareEmployeeShops(employeeAllShops, token.getCompanyId());

        if (token.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
            prepareAccessTokenForGrowShop(token, loginResult);
            AssetAccessToken newAssetToken = createNewAssetToken(token.getCompanyId(), shopId, employee.getId());
            if (token.getGrowAppType() == ConnectAuthToken.GrowAppType.Operations) {
                loginResult.setAssetAccessToken(getInternalApiEncryptedAssetToken(newAssetToken));
            } else {
                loginResult.setAssetAccessToken(getNewEncryptedAssetToken(newAssetToken));
            }
        } else {
            loginResult.setAccessToken(getNewEncryptedToken(token));
            loginResult.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(token.getCompanyId(), shopId, employee.getId())));
        }

        loginResult.setLoginTime(token.getInitDate());
        loginResult.setExpirationTime(token.getExpirationDate());
        loginResult.setEmployee(employee);
        loginResult.setCompany(company);
        loginResult.setAssignedShop(currentShop);
        loginResult.setShops(shops);
        loginResult.setAppTarget(token.getAppTarget());
        loginResult.setEmployeeShops(employeeAllShops);
        loginResult.setAssignedTerminal(terminal);


    }

    @Override
    public InitialLoginResult registerCompany(CompanyRegisterRequest request) {
        if (request == null
                || StringUtils.isBlank(request.getProductSKU())
                || StringUtils.isBlank(request.getEmail())
                || StringUtils.isBlank(request.getName())
                || StringUtils.isBlank(request.getPhoneNumber())
                || StringUtils.isBlank(request.getFirstName())
                || StringUtils.isBlank(request.getLastName())) {
            throw new BlazeInvalidArgException("CompanyRegsiter", "Invalid arguments");
        }
        String email = request.getEmail().toLowerCase().trim();
        Company company = companyRepository.getCompanyByEmail(email);
        if (company != null) {
            throw new BlazeInvalidArgException("Company.Email", "An existing account exist");
        }

        Employee employee = employeeRepository.getEmployeeByEmail(email);
        if (employee != null) {
            throw new BlazeInvalidArgException("Employee.Email", "An existing account exist");
        }

        ConnectProduct connectProduct = connectProductRepository.getProductBySKU(request.getProductSKU());
        if (connectProduct == null) {
            throw new BlazeInvalidArgException("ConnectProduct", "This product does not exist.");
        }


        // Create Company
        company = new Company();
        company.setIsId(request.getIsId());
        company.setId(ObjectId.get().toString());
        company.setEmail(email);
        company.setName(request.getName());
        company.setPhoneNumber(request.getPhoneNumber());
        company.setProductSKU(request.getProductSKU());

        Address address = new Address();
        address.setAddress(request.getAddress());
        address.setCity(request.getCity());
        address.setState(request.getState());
        address.setZipCode(request.getZipCode());

        company.setAddress(address);
        company.setWebsite(request.getWebsite());

        if (company.getAddress() != null) {
            company.getAddress().prepare(company.getId());
        }

        // Pricing Option
        company.setPricingOpt(Company.CompanyPricing.Normal);

        companyRepository.save(company);
        defaultDataService.initializeDefaultCompanyData(company.getId(), connectProduct,request);

        InitialLoginResult result = null;
        boolean doubleShopCreation = false;

        // If both apps are available then we are creating 2 shops, 1 for retail & 1 for distribution
        if (connectProduct.getAvailableApps().contains(CompanyFeatures.AppTarget.Retail) && connectProduct.getAvailableApps().contains(CompanyFeatures.AppTarget.Distribution)) {
            doubleShopCreation = true;
            createShop(connectProduct, company, request, address, CompanyFeatures.AppTarget.Distribution, doubleShopCreation);
            Shop shop = createShop(connectProduct, company, request, address, CompanyFeatures.AppTarget.Retail, doubleShopCreation);
            defaultDataService.initializeDefaultShopData(shop, null, true, company);
            employee = createEmployee(company, request, shop, connectProduct);
            result = prepareInitialLoginResult(employee, company, shop, request, CompanyFeatures.AppTarget.Retail);

        } else if (connectProduct.getAvailableApps().contains(CompanyFeatures.AppTarget.Retail)) {
            doubleShopCreation = false;
            Shop shop = createShop(connectProduct, company, request, address, CompanyFeatures.AppTarget.Retail, doubleShopCreation);
            defaultDataService.initializeDefaultShopData(shop, null, true, company);
            employee = createEmployee(company, request, shop, connectProduct);
            result = prepareInitialLoginResult(employee, company, shop, request, CompanyFeatures.AppTarget.Retail);

        } else if (connectProduct.getAvailableApps().contains(CompanyFeatures.AppTarget.Distribution)) {
            doubleShopCreation = false;
            Shop shop = createShop(connectProduct, company, request, address, CompanyFeatures.AppTarget.Distribution, doubleShopCreation);
            defaultDataService.initializeDefaultShopData(shop, null, true, company);
            employee = createEmployee(company, request, shop, connectProduct);
            result = prepareInitialLoginResult(employee, company, shop, request, CompanyFeatures.AppTarget.Distribution);
        }  else if (connectProduct.getAvailableApps().contains(CompanyFeatures.AppTarget.Grow)) {
            doubleShopCreation = false;
            Shop shop = createShop(connectProduct, company, request, address, CompanyFeatures.AppTarget.Grow, doubleShopCreation);
            defaultDataService.initializeDefaultShopData(shop, null, true, company);
            employee = createEmployee(company, request, shop, connectProduct);
            result = prepareInitialLoginResult(employee, company, shop, request, CompanyFeatures.AppTarget.Grow);
        }

        if (company != null) {
            stripeService.createCustomerFromCompany(company);
        }

        return result;
    }

    private void prepareAccessTokenForGrowShop(ConnectAuthToken token, InitialLoginResult result) {
        try {
            SwitchableApplication authenticationApp = connectConfiguration.getBlazeApplicationsConfig().getRedirectURL(CompanyFeatures.AppTarget.AuthenticationApp.toString());
            Map<String, String> appLinks = new HashMap<>();
            if (Objects.nonNull(authenticationApp) && Objects.nonNull(connectConfiguration.getSalesAppLink())) {
                appLinks.put("authAppURI", authenticationApp.getAppLink());
                appLinks.put("salesAppURI", connectConfiguration.getSalesAppLink());
            }
            token.setAppLinks(appLinks);
            result.setAccessToken(URLEncoder.encode(getInternalApiEncryptedToken(token), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            LOG.error("Error in creating Access Token");
        }
    }

    private Shop createShop(ConnectProduct connectProduct, Company company, CompanyRegisterRequest request, Address address, CompanyFeatures.AppTarget appTarget, boolean doubleShopCreation) {
        // Create Shop
        String shopName = request.getName();
        if (doubleShopCreation) {
            shopName = shopName + "-" + appTarget.toString();
        }
        Shop shop = new Shop();
        shop.setId(ObjectId.get().toString());
        shop.setCompanyId(company.getId());
        shop.setName(shopName);
        shop.setPhoneNumber(request.getPhoneNumber());
        shop.setEmailAdress(request.getEmail().toLowerCase());
        shop.setAddress(address);
        shop.setShopType(request.getShopType());
        if (shop.getAddress() != null) {
            shop.getAddress().prepare();
        }

        // tax infox
        shop.setTimeZone(ConnectAuthToken.DEFAULT_REQ_TIMEZONE);

        shop.setShowDeliveryQueue(connectProduct.getAvailableQueues().contains(Transaction.QueueType.Delivery));
        shop.setShowWalkInQueue(connectProduct.getAvailableQueues().contains(Transaction.QueueType.WalkIn));
        shop.setShowOnlineQueue(connectProduct.getAvailableQueues().contains(Transaction.QueueType.Online));
        shop.setShowSpecialQueue(connectProduct.getAvailableQueues().contains(Transaction.QueueType.Special));
        shop.setAppTarget(appTarget);

        shopRepository.save(shop);

        return shop;
    }

    private Employee createEmployee(Company company, CompanyRegisterRequest request, Shop shop, ConnectProduct connectProduct) {
        String genPassword = securityUtil.generatPassword();
        // employee
        Employee employee = new Employee();
        employee.setId(ObjectId.get().toString());
        employee.setCompanyId(company.getId());
        employee.setFirstName(request.getFirstName());
        employee.setLastName(request.getLastName());
        employee.setPin("0000");
        employee.setShops(Lists.newArrayList(shop.getId()));
        employee.setEmail(request.getEmail().toLowerCase().trim());
        employee.setPassword(securityUtil.encryptPassword(genPassword));

        employee.setAppAccessList(new LinkedHashSet<>(connectProduct.getAvailableApps()));

        Role role = roleRepository.getRoleByName(company.getId(), "Admin");
        if (role != null) {
            employee.setRoleId(role.getId());
        } else {
            throw new BlazeInvalidArgException("Role", "Admin role does not exist.");
        }

        employeeRepository.save(employee);


        hideEmployeeAuthInfo(employee);
        return employee;
    }

    private InitialLoginResult prepareInitialLoginResult(Employee employee, Company company, Shop shop, CompanyRegisterRequest request, CompanyFeatures.AppTarget appTarget) {

        ConnectAuthToken newToken = createNewToken(company.getId(), shop.getId(), employee.getId(), null, employee.getFirstName(), employee.getLastName(), DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis(), appTarget);

        newToken.setRequestTimeZone(request.getTimezone());

        InitialLoginResult result = new InitialLoginResult();
        result.setAccessToken(getNewEncryptedToken(newToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), shop.getId(), employee.getId())));
        result.setEmployee(employee);
        //result.setShops(Lists.newArrayList((ShopResult)shop));
        result.setLoginTime(newToken.getInitDate());
        result.setExpirationTime(newToken.getExpirationDate());
        //result.setAssignedShop((ShopResult)shop);

        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmployeeId(employee.getId());
        passwordReset.setCompanyId(employee.getCompanyId());
        passwordReset.setResetCode(securityUtil.getNextResetCode());
        passwordReset.setExpirationDate(DateTime.now().plusDays(2).getMillis());
        passwordReset.setExpired(false);

        // set all previous password reset to expired
        passwordResetRepository.setPasswordExpired(employee.getCompanyId(), employee.getId());
        passwordResetRepository.save(passwordReset);


        emailService.sendWinEmail(company, employee, shop);
        emailService.sendWelcomeEmail(employee, passwordReset.getResetCode());
        auditLogService.addAuditLog(newToken, "/api/v1/mgmt/session/register", "Management - Authentication", "Register Company");
        return result;
    }

    private void checkVersion(String version) {
        // Check app version
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        String requiredVersion = connectConfiguration.getRequiredVersion();
        String msg = "We've released new critical features. A new update is required.";
        if (app != null && StringUtils.isNotBlank(app.getReqIOSVersion())) {
            requiredVersion = app.getReqIOSVersion();
            if (StringUtils.isNotBlank(app.getIosVersionUpdateMsg())) {
                msg = app.getIosVersionUpdateMsg();
            }
        }

        Version incomingVersion = new Version(version);
        Version currentVersion = new Version(requiredVersion);
        if (incomingVersion.isLesserOrEqualTo(currentVersion)) {
            throw new VersionErrorException("Version", msg);
        }
    }

    @Override
    public InitialLoginResult terminalManagerLogin(InitTerminalRequest request) {
        if (request == null || StringUtils.isBlank(request.getEmail()) || StringUtils.isBlank(request.getPassword())) {
            throw new BlazeAuthException("InitTerminalRequest", "Invalid Argument");
        }

        checkVersion(request.getVersion());

        String email = request.getEmail().toLowerCase();
        Employee employee = employeeRepository.getEmployeeByEmail(email);

        if (employee == null) {
            throw new BlazeAuthException("EmailLoginRequest", "Invalid credentials");
        }

        if (employee.isDisabled()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }


        if (!securityUtil.checkPassword(request.getPassword(), employee.getPassword())) {
            throw new BlazeAuthException("EmailLoginRequest", "Invalid credentials");
        }

        final String companyId = employee.getCompanyId();

        roleService.checkPermission(companyId, employee.getRoleId(), Role.Permission.iPadInitiation);

        hideEmployeeAuthInfo(employee);

        Company company = companyRepository.getById(companyId);

        if (company == null || !company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }

        Role role = roleService.getRoleById(employee.getRoleId());
        employee.setRole(role);


        Iterable<ShopResult> shops = shopRepository.list(companyId, ShopResult.class);

        List<ShopResult> shopList = Lists.newArrayList(shops);

        // Check to see if this Device Id is a new Device


        Terminal terminal = terminalRepository.getTerminalByDeviceId(companyId, request.getDeviceId());
        InitialLoginResult result = new InitialLoginResult();

        if (terminal == null) {
            terminal = terminalRepository.get(companyId, employee.getAssignedTerminalId());
            // if terminal == null, use the employee assigned terminalId
            if (terminal != null) {
                // save current deviceId to the terminal
                terminal.setDeviceId(request.getDeviceId());
                terminal.setCurrentEmployeeId(employee.getId());
                terminalRepository.update(companyId, terminal.getId(), terminal);
            }
        } else {
            if (StringUtils.isBlank(employee.getAssignedTerminalId())) {
                employeeRepository.updateEmployeeAssignedTerminal(token.getCompanyId(), employee.getId(), terminal.getId());
            }


            // save current employeeId to the terminal
            terminal.setCurrentEmployeeId(employee.getId());
            terminalRepository.update(companyId, terminal.getId(), terminal);
        }

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        ShopResult assignedShop = null;
        String assignedShopId = null;

        List<ShopResult> employeeShops = new ArrayList<>();
        for (ShopResult shop : shopList) {
            if (employee.getShops() != null && employee.getShops().contains(shop.getId()) && shop.isActive() && shop.getAppTarget() == request.getAppTarget()) {
                employeeShops.add(shop);
                assignedShopId = shop.getId();
                assignedShop = shop;
            }
        }

        String terminalId = null;
        if (terminal != null) {
            String shopId = terminal.getShopId();
            ShopResult shop = shopRepository.get(companyId, shopId, ShopResult.class);
            result.setAssignedShop(shop);
            assignedShopId = shop.getId();
            assignedShop = shop;
            terminalId = terminal.getId();
            patchTaxInfo(shop);


            // check terminal
            if (terminal.isDeleted() || terminal.isActive() == false) {

                // unset terminal usagege
                if (terminal.getId().equalsIgnoreCase(employee.getAssignedTerminalId())) {
                    employeeRepository.updateEmployeeAssignedTerminal(token.getCompanyId(), employee.getId(), "");
                }


                terminalId = null;
                terminal = null;
            }
        }

        if (assignedShopId == null && shopList.size() > 0) {
            assignedShopId = shopList.get(0).getId();
        }

        assignExciseTax(shopList, exciseTaxInfos);
        assignExciseTax(Lists.newArrayList(assignedShop), exciseTaxInfos);

        ConnectAuthToken newToken = createNewToken(company.getId(), assignedShopId, employee.getId(), terminalId, employee.getFirstName(), employee.getLastName(), DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis(), request.getAppTarget());


        newToken.setRequestTimeZone(request.getTimezone());

        newToken.setExpirationDate(DateTime.now().plusYears(1).getMillis());
        result.setCompany(company);
        result.setAccessToken(getNewEncryptedToken(newToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), assignedShopId, employee.getId())));
        result.setShops(shopList);
        result.setNewDevice(terminal == null);
        result.setEmployee(employee);
        result.setLoginTime(newToken.getInitDate());
        result.setExpirationTime(newToken.getExpirationDate());
        result.setAssignedTerminal(terminal);
        result.setAssignedShop(assignedShop);
        result.setAppTarget(request.getAppTarget());
        result.setEmployeeShops(employeeShops);

        auditLogService.addAuditLog(newToken, "/api/v1/session/terminal/init", "POS - Session", "Initialize Terminal");

        //taskManager.takeInventorySnapshot(employee.getCompanyId(), assignedShopId);

        return result;
    }


    @Override
    public InitialLoginResult terminalEmployeeLogin(QuickPinLoginRequest request) {
        if (request == null || StringUtils.isBlank(request.getPin())) {
            throw new BlazeAuthException("QuickPinLoginRequest", "Invalid Argument");
        }

        checkVersion(request.getVersion());

        String companyId = token.getCompanyId();

        Employee employee = employeeRepository.getEmployeeByQuickPin(companyId, request.getPin());

        if (employee == null) {
            throw new BlazeAuthException("QuickPinLoginRequest", "Invalid pin");
        }
        if (employee.isDisabled()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }
        if (employee.isDeleted()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }


        // Get Terminal
        Terminal terminal = null;

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop.getTermSalesOption() == Shop.TerminalSalesOption.AssignedEmployeeTerminal
            || shop.getTermSalesOption() == Shop.TerminalSalesOption.SellerAssignedTerminal) {
            if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
                terminal = terminalRepository.get(token.getCompanyId(), employee.getAssignedTerminalId());
            }
        }

        if (terminal == null) {
            terminal = terminalRepository.getById(token.getTerminalId());
        }

        if (terminal == null) {
            // try to retrieve using incoming request id
            terminal = terminalRepository.getTerminalByDeviceId(token.getCompanyId(), request.getDeviceId());

        }

        if (terminal == null) {
            throw new BlazeAuthException("QuickPinLoginRequest", "This terminal does not exist");
        }

        if (!terminal.isActive()) {
            throw new BlazeAuthException("QuickPinLoginRequest", "This terminal is no longer active.");
        }
        if (terminal.isDeleted()) {
            throw new BlazeAuthException("QuickPinLoginRequest", "This terminal has been deleted.");
        }
        /*get device details*/

        Terminal.DeviceDetail deviceDetail = terminal.getDeviceDetail(request.getAppType(), request.getDeviceId(), request.getDeviceType(), false);
        Inventory inventory = new Inventory();
        if (StringUtils.isNotBlank(terminal.getAssignedInventoryId())) {
            inventory = inventoryRepository.get(token.getCompanyId(), terminal.getAssignedInventoryId());
        }
        /*get device details*/


        // Update app version if necessary
        if (StringUtils.isBlank(terminal.getAppVersion())
                || !terminal.getAppVersion().equalsIgnoreCase(request.getVersion())
                || !employee.getId().equalsIgnoreCase(terminal.getCurrentEmployeeId())
                || deviceDetail != null && ((request.getDeviceId() != null && !request.getDeviceId().equalsIgnoreCase(deviceDetail.getDeviceId())))
                || deviceDetail != null && deviceDetail.getDeviceType() != request.getDeviceType()
                || deviceDetail != null && deviceDetail.getAppType() != request.getAppType()
                || deviceDetail != null && !deviceDetail.isActive()) {
            Terminal otherTerminal = terminalRepository.getTerminalByDeviceId(token.getCompanyId(), request.getDeviceId());
            boolean shouldUpdate = false;
            if (otherTerminal != null && !terminal.getId().equalsIgnoreCase(otherTerminal.getId())) {
                // invalidate the other terminal using this id
                otherTerminal.setDeviceId(UUID.randomUUID().toString());

                Terminal.DeviceDetail otherDeviceDetail = otherTerminal.getDeviceDetail(request.getAppType(), request.getDeviceId(), request.getDeviceType(), true);
                if (otherDeviceDetail != null) {
                    otherDeviceDetail.setDeviceId(otherTerminal.getDeviceId());
                }
                terminalRepository.update(token.getCompanyId(), otherTerminal.getId(), otherTerminal);
                shouldUpdate = true;
            }

            if ((request.getDeviceId() != null && !request.getDeviceId().equalsIgnoreCase(deviceDetail.getDeviceId()))
                    || request.getDeviceType() != deviceDetail.getDeviceType()
                    || request.getAppType() != deviceDetail.getAppType()
                    || !deviceDetail.isActive()) {
                shouldUpdate = true;
            }
            terminal.setCurrentEmployeeId(employee.getId());
            terminal.setAppVersion(request.getVersion());
            terminal.setDeviceId(request.getDeviceId());
            deviceDetail.setDeviceId(request.getDeviceId());
            deviceDetail.setAppType(request.getAppType());
            deviceDetail.setDeviceToken(request.getDeviceToken());
            deviceDetail.setDeviceType(request.getDeviceType());
            deviceDetail.setDeviceVersion(request.getAndroidVersion());
            deviceDetail.setActive(true);
            terminalRepository.update(token.getCompanyId(), terminal.getId(), terminal);

            LOG.info("Updating terminal...id: " + terminal.getId());
            if (shouldUpdate) {
                realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.TerminalsUpdateEvent,null);
            }
        }


        roleService.checkPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.iPadQuickPinLogin);


        // Create new session
        EmployeeSession employeeSession = new EmployeeSession();
        employeeSession.prepare(token.getCompanyId());
        employeeSession.setEmployeeId(employee.getId());
        employeeSession.setTerminalId(terminal.getId());
        employeeSession.setStartTime(DateTime.now().getMillis());
        employeeSession.setShopId(terminal.getShopId());


        // Check TimeCard
        TimeCard timeCard = timeCardRepository.getActiveTimeCard(token.getCompanyId(), terminal.getShopId(), employee.getId());
        if (timeCard == null) {
            timeCard = new TimeCard();
            timeCard.prepare(token.getCompanyId());
            timeCard.setShopId(token.getShopId());
            timeCard.setClockin(true);
            timeCard.setClockInTime(DateTime.now().getMillis());
            timeCard.setEmployeeId(employee.getId());
            employeeSession.setTimeCardId(timeCard.getId());
            timeCard.getSessions().add(employeeSession);
            timeCardRepository.save(timeCard);
        } else {
            // checktimeCart
            int hours = DateUtil.getHoursBetweenTwoDates(timeCard.getClockInTime(), DateTime.now().getMillis());
            if (hours >= 16) {
                // end this timeCard and create a new one
                timeCard.setClockOutTime(DateTime.now().getMillis());
                timeCard.setClockin(false);
                if (timeCard.getSessions().size() > 0) {
                    EmployeeSession lastSession = timeCard.getSessions().get(timeCard.getSessions().size() - 1);
                    lastSession.setEndTime(DateTime.now().getMillis());
                }
                timeCardRepository.update(token.getCompanyId(), timeCard.getId(), timeCard);

                timeCard = new TimeCard();
                timeCard.prepare(token.getCompanyId());
                timeCard.setShopId(token.getShopId());
                timeCard.setClockin(true);
                timeCard.setClockInTime(DateTime.now().getMillis());
                timeCard.setEmployeeId(employee.getId());
                employeeSession.setTimeCardId(timeCard.getId());
                timeCard.getSessions().add(employeeSession);
                timeCardRepository.save(timeCard);
            } else {
                employeeSession.setTimeCardId(timeCard.getId());
                timeCard.getSessions().add(employeeSession);
                timeCardRepository.addTimeCardSession(token.getCompanyId(), timeCard.getId(), employeeSession);
            }
        }

        // assign the terminal to this employee if it's different
        if (StringUtils.isBlank(employee.getAssignedTerminalId())) {
            employee.setAssignedTerminalId(terminal.getId());
            employeeRepository.updateEmployeeAssignedTerminal(employee.getCompanyId(), employee.getId(), terminal.getId());
        }

        Role role = roleService.getRoleById(employee.getRoleId());
        employee.setRole(role);


        ConnectAuthToken newToken = updateAuthToken(token, terminal.getShopId(),
                employee.getId(), terminal.getId(), true, terminal.getDeviceId(), employee.getFirstName(), employee.getLastName());

        newToken.setRequestTimeZone(request.getTimezone());
        newToken.setEmployeeSessoinId(employeeSession.getId());
        newToken.setTimeCardId(timeCard.getId());
        newToken.setExpirationDate(DateTime.now().plusYears(1).getMillis());


        InitialLoginResult result = new InitialLoginResult();
        result.setAccessToken(getNewEncryptedToken(newToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), terminal.getShopId(), employee.getId())));
        result.setEmployee(employee);
        result.setLoginTime(newToken.getInitDate());
        result.setExpirationTime(newToken.getExpirationDate());
        result.setAssignedTerminal(terminal);
        result.setInventoryName(inventory == null ? "N/A" : StringUtils.isNotBlank(inventory.getName()) ? inventory.getName() : "N/A");
        //result.setTerminalValidationRequired(true);

        // Success, updated terminal token
        if (StringUtils.isNotBlank(request.getDeviceToken())) {
            terminal.setDeviceToken(request.getDeviceToken());
            if (deviceDetail != null) {
                deviceDetail.setDeviceToken(request.getDeviceToken());
            }
            terminalRepository.update(terminal.getCompanyId(), terminal.getId(), terminal);
        }


        employeeRepository.updateLastLogin(employee.getCompanyId(), employee.getId(), shop.getId());
        // take snapshot if needed
        //taskManager.takeInventorySnapshot(employee.getCompanyId(), terminal.getShopId());
        return result;
    }


    @Override
    public InitialLoginResult loginWithPin(PinRequest request) {

        // Check to see if this Device Id is a new Device
        Employee employee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), request.getPin());

        if (employee == null) {
            throw new BlazeAuthException("QuickPinLoginRequest", "Invalid pin");
        }
        if (employee.isDisabled()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }

        if (employee.isDeleted()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }

        Company company = companyRepository.getById(employee.getCompanyId());

        if (company == null || !company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }

        roleService.checkPermission(employee.getCompanyId(), employee.getRoleId(), Role.Permission.WebAdminLogin);

        //hideEmployeeAuthInfo(employee);
        employee.setPassword(null);

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        Iterable<ShopResult> items = shopRepository.list(employee.getCompanyId(), ShopResult.class);
        List<ShopResult> shops = Lists.newArrayList(items);
        String shopId = null;
        ShopResult assignedShop = null;
        List<ShopResult> assignedShops = new ArrayList<>();
        // Filter out shops assigned to this employee
        for (ShopResult shop : shops) {
            if (employee.getShops().contains(shop.getId())) {
                assignedShops.add(shop);
            }
        }
        if (assignedShops.size() > 0) {
            assignedShop = assignedShops.get(0);
        }


        if (shops != null && shops.size() > 0) {
            assignedShop = shops.get(0);
            shopId = assignedShop.getId();
        }

        assignExciseTax(shops, exciseTaxInfos);

        Terminal terminal = null;
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
        }

        patchTaxInfo(assignedShop);
        Role role = roleService.getRoleById(employee.getCompanyId(), employee.getRoleId());
        employee.setRole(role);

        ConnectAuthToken connectAuthToken = createNewToken(employee.getCompanyId(),
                shopId,
                employee.getId(),
                employee.getAssignedTerminalId(),
                employee.getFirstName(),
                employee.getLastName(), DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis(), token.getAppTarget());

        connectAuthToken.setRequestTimeZone(request.getTimezone());

        InitialLoginResult result = new InitialLoginResult();
        result.setAccessToken(getNewEncryptedToken(connectAuthToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), shopId, employee.getId())));
        result.setEmployee(employee);
        result.setShops(shops);
        result.setLoginTime(connectAuthToken.getInitDate());
        result.setExpirationTime(connectAuthToken.getExpirationDate());
        result.setAssignedShop(assignedShop);
        result.setAssignedTerminal(terminal);
        result.setCompany(company);

        auditLogService.addAuditLog(connectAuthToken, "/api/v1/mgmt/session", "Management - Authentication", "Admin Login");

        //taskManager.takeInventorySnapshot(employee.getCompanyId(), shopId);
        return result;
    }

    private void patchTaxInfo(Shop shop) {
        if (shop != null) {
            // PATCH
            if (shop.getTaxInfo() != null) {
                if (StringUtils.isBlank(shop.getTaxInfo().getId())) {
                    shop.getTaxInfo().prepare();
                    if (StringUtils.isBlank(shop.getTimeZone())) {
                        shop.setTimeZone(ConnectAuthToken.DEFAULT_REQ_TIMEZONE);
                    }
                    // Update shop with valid tax Info
                    shopRepository.update(shop.getCompanyId(), shop.getId(), shop);
                }
            }
        }
    }

    @Override
    public void terminalEmployeeLogout() {
        TimeCard timeCard = timeCardRepository.get(token.getCompanyId(), token.getTimeCardId());
        if (timeCard == null) {
            throw new BlazeAuthException("QuickPinLoginRequest", "Employee does not have a timecard.");
        }

        for (EmployeeSession employeeSession : timeCard.getSessions()) {
            // Sign this user out
            if (employeeSession.getId().equalsIgnoreCase(token.getEmployeeSessoinId())) {

                // Make sure we're not already logged out
                if (employeeSession.getEndTime() == null) {
                    employeeSession.setEndTime(DateTime.now().getMillis());
                    timeCardRepository.update(token.getCompanyId(), timeCard.getId(), timeCard);
                }
                break;
            }
        }
    }

    @Override
    public ManagerAccessResult managerSettingsAccess(ManagerAccessRequest request) {
        Employee employee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), request.getPin());

        if (employee == null) {
            throw new BlazeInvalidArgException("Invalid", "Invalid pin or not a manager.");
        }
        if (employee.isDisabled()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }

        if (employee.isDeleted()) {
            throw new BlazeAuthException("EmailLoginRequest", "Your account is no longer active. Please speak to your admin.");
        }
        roleService.checkPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.iPadManageSettingsView);


        Iterable<Shop> shops = shopRepository.list(employee.getCompanyId());
        Iterable<Terminal> terminals = terminalRepository.list(token.getCompanyId());
        Iterable<Inventory> inventories = inventoryRepository.list(token.getCompanyId());
        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(employee.getCompanyId());

        ConnectAuthToken newToken = updateAuthToken(token, token.getShopId(),
                employee.getId(), token.getTerminalId(), true, token.getActiveDeviceId(), employee.getFirstName(), employee.getLastName());
        newToken.setRequestTimeZone(token.getRequestTimeZone());

        ManagerAccessResult result = new ManagerAccessResult();
        result.setAccessToken(getNewEncryptedToken(newToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), token.getShopId(), employee.getId())));
        result.setSuccess(true);
        result.setInventories(Lists.newArrayList(inventories));
        result.setTerminals(Lists.newArrayList(terminals));
        result.setShops(Lists.newArrayList(shops));
        result.setCompanyFeatures(companyFeatures);

        return result;
    }

    @Override
    public InitialLoginResult setupTerminal(TerminalSetupRequest request) {
        Terminal dbTerminal = terminalRepository.get(token.getCompanyId(), request.getTerminalId());

        if (dbTerminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        ShopResult shop = shopRepository.get(token.getCompanyId(), dbTerminal.getShopId(), ShopResult.class);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist for this terminal.");
        }


        Terminal oldTerminal = terminalRepository.getTerminalByDeviceId(token.getCompanyId(), request.getDeviceId());
        if (oldTerminal != null) {
            oldTerminal.setDeviceId(UUID.randomUUID().toString());
            terminalRepository.update(token.getCompanyId(), oldTerminal.getId(), oldTerminal);
        }


        dbTerminal.setDeviceId(request.getDeviceId());
        dbTerminal.setAppVersion(request.getAppVersion());
        dbTerminal.setDeviceModel(request.getDeviceModel());
        dbTerminal.setDeviceToken(request.getDeviceToken());
        dbTerminal.setDeviceVersion(request.getDeviceVersion());
        dbTerminal.setDeviceType(request.getDeviceType());
        dbTerminal.setDeviceName(request.getDeviceName());

        if (!StringUtils.isBlank(request.getInventoryId())) {
            Inventory inventory = inventoryRepository.get(token.getCompanyId(), request.getInventoryId());
            if (inventory != null) {
                dbTerminal.setAssignedInventoryId(inventory.getId());
            }
        }

        terminalRepository.update(token.getCompanyId(), dbTerminal.getId(), dbTerminal);

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId(), "{password:0,pin:0}");
        if (StringUtils.isBlank(employee.getAssignedTerminalId())
                || !dbTerminal.getId().equalsIgnoreCase(employee.getAssignedTerminalId())) {
            // Update assigned terminal if it's not set or different
            employee.setAssignedTerminalId(dbTerminal.getId());
            employeeRepository.updateEmployeeAssignedTerminal(employee.getCompanyId(), employee.getId(), dbTerminal.getId());
        }

        assignExciseTax(Lists.newArrayList(shop), exciseTaxInfos);

        ConnectAuthToken newToken = updateAuthToken(token, shop.getId(),
                employee.getId(), dbTerminal.getId(), true, token.getActiveDeviceId(), employee.getFirstName(), employee.getLastName());

        newToken.setRequestTimeZone(token.getRequestTimeZone());
        newToken.setExpirationDate(DateTime.now().plusYears(1).getMillis());

        InitialLoginResult result = new InitialLoginResult();

        result.setAccessToken(getNewEncryptedToken(newToken));
        result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(employee.getCompanyId(), dbTerminal.getShopId(), employee.getId())));
        //request.set
        result.setAssignedTerminal(dbTerminal);
        result.setAssignedShop(shop);
        result.setEmployee(employee);
        result.setLoginTime(newToken.getInitDate());
        result.setExpirationTime(newToken.getExpirationDate());
        result.setNewDevice(false);


        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(dbTerminal.getShopId(),
                RealtimeService.RealtimeEventType.TerminalsUpdateEvent, null);
        return result;
    }

    @Override
    public void checkPermission(CheckPermissionRequest request) {
        Employee employee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), request.getPin());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
        }

        Role.Permission permission = Role.Permission.None;
        try {
            permission = Role.Permission.valueOf(request.getPermission());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Permission", "Invalid permission.");
        }

        roleService.checkPermission(token.getCompanyId(), employee.getRoleId(), permission);
    }

    private void hideEmployeeAuthInfo(Employee employee) {
        employee.setPassword(null);
        employee.setPin(null);
    }

    private String getInternalApiEncryptedToken(ConnectAuthToken authToken) {
        return securityUtil.encryptByInternalApiSecret(JsonSerializer.toJson(authToken));
    }

    private String  getInternalApiEncryptedAssetToken(AssetAccessToken assetAccessToken) {
        return securityUtil.encryptByInternalApiSecret(JsonSerializer.toJson(assetAccessToken));
    }

    private String getNewEncryptedToken(ConnectAuthToken authToken) {
        return securityUtil.createAccessToken(authToken);
    }

    private String getNewEncryptedAssetToken(AssetAccessToken assetAccessToken) {
        return securityUtil.createAssetAccessToken(assetAccessToken);
    }

    private AssetAccessToken createNewAssetToken(String companyId, String shopId, String userId) {
        AssetAccessToken token = new AssetAccessToken();
        token.setCompanyId(companyId);
        token.setShopId(shopId);
        token.setUserId(userId);
        token.setSignedDate(DateTime.now().getMillis());
        return token;
    }


    private ConnectAuthToken createNewToken(String companyId,
                                            String shopId,
                                            String managerId,
                                            String terminalId,
                                            String firstName,
                                            String lastName, long time, CompanyFeatures.AppTarget appTarget) {
        ConnectAuthToken newToken = new ConnectAuthToken();
        newToken.setCompanyId(companyId);
        newToken.setShopId(shopId);
        newToken.setManagerId(managerId);
        newToken.setTerminalId(terminalId);
        List<TerminalUser> userList = new ArrayList<>();
        userList.add(new TerminalUser(managerId, firstName, lastName));
        newToken.setActiveUsers(userList);
        newToken.setInitDate(DateTime.now().getMillis());
        newToken.setExpirationDate(time);

        if (appTarget == null) {
            appTarget = CompanyFeatures.AppTarget.Retail;
        }
        newToken.setAppTarget(appTarget);
        return newToken;
    }

    private ConnectAuthToken createNewTokenWebsite(String companyId,
                                                   String shopId,
                                                   String managerId,
                                                   String terminalId,
                                                   String firstName,
                                                   String lastName, CompanyFeatures.AppTarget appTarget) {
        ConnectAuthToken newToken = new ConnectAuthToken();
        newToken.setCompanyId(companyId);
        newToken.setShopId(shopId);
        newToken.setManagerId(managerId);
        newToken.setTerminalId(terminalId);
        List<TerminalUser> userList = new ArrayList<>();
        userList.add(new TerminalUser(managerId, firstName, lastName));
        newToken.setActiveUsers(userList);
        newToken.setInitDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusDays(7).getMillis());

        if (appTarget == null) {
            appTarget = CompanyFeatures.AppTarget.Retail;
        }
        newToken.setAppTarget(appTarget);
        return newToken;
    }

    private ConnectAuthToken updateAuthToken(final ConnectAuthToken currentToken, final String shopId, ShopResult shop) {
        ConnectAuthToken newToken = currentToken.copy();
        newToken.setShopId(shopId);
        newToken.setInitDate(DateTime.now().getMillis());
        if (StringUtils.isNotBlank(currentToken.getRequestTimeZone())) {
            newToken.setRequestTimeZone(currentToken.getRequestTimeZone());
        }
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        newToken.setAppTarget(shop.getAppTarget());

        return newToken;
    }

    private ConnectAuthToken updateAuthToken(final ConnectAuthToken currentToken, final String companyId, final String shopId, ShopResult shop) {
        ConnectAuthToken newToken = currentToken.copy();
        newToken.setShopId(shopId);
        newToken.setInitDate(DateTime.now().getMillis());
        if (StringUtils.isNotBlank(currentToken.getRequestTimeZone())) {
            newToken.setRequestTimeZone(currentToken.getRequestTimeZone());
        }
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        newToken.setAppTarget(shop.getAppTarget());

        if(companyId != null) {
            newToken.setCompanyId(companyId);
        }

        return newToken;
    }

    private ConnectAuthToken returnAuthToken(final ConnectAuthToken currentToken) {
        ConnectAuthToken newToken = currentToken.copy();
        newToken.setInitDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        return newToken;
    }


    private ConnectAuthToken rewnewAuthToken(final ConnectAuthToken currentToken) {
        ConnectAuthToken newToken = (new Kryo()).copy(currentToken);
        newToken.setInitDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        return newToken;
    }

    private ConnectAuthToken updateAuthToken(final ConnectAuthToken currentToken, final String shopId,
                                             final String activeUserId, String terminalId,
                                             final boolean shouldPop, final String activeDeviceId,
                                             final String firstName, final String lastName) {
        ConnectAuthToken newToken = new ConnectAuthToken();
        newToken.setCompanyId(currentToken.getCompanyId());
        newToken.setShopId(shopId);
        newToken.setManagerId(currentToken.getManagerId());
        newToken.setTerminalId(terminalId);
        newToken.setActiveDeviceId(activeDeviceId);
        List<TerminalUser> userList = currentToken.getActiveUsers();
        if (userList == null) {
            userList = new ArrayList<>();
        } else if (userList.size() > 0 && shouldPop) {
            userList.remove(userList.size() - 1); // remove last
        }
        userList.add(new TerminalUser(activeUserId, firstName, lastName));
        newToken.setInitDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        newToken.setActiveUsers(userList);
        return newToken;
    }

    @Override
    public SwitchApplicationResult switchApplication(String accessToken, String appName, String requestShopId, ConnectAuthToken.GrowAppType growAppType) {

        if (StringUtils.isBlank(accessToken)) {
            throw new BlazeAuthException(ACCESS_TOKEN, INVALID_ACCESS_TOKEN);
        }

        if (StringUtils.isBlank(appName)) {
            throw new BlazeInvalidArgException(SWITCH_APPLICATION, "Please provide a valid Application name");
        }

        if (StringUtils.isBlank(requestShopId))
            throw new BlazeInvalidArgException(SWITCH_APPLICATION, "Requested Shop can't be blank");

        ConnectAuthToken connectAuthToken;
        try {
              connectAuthToken = securityUtil.isInternalApiToken(accessToken) ?
                      securityUtil.decryptInternalApiToken(accessToken) : securityUtil.decryptAccessToken(accessToken);
        } catch (Exception e) {
            throw new BlazeAuthException(ACCESS_TOKEN, INVALID_ACCESS_TOKEN);
        }

        if (!connectAuthToken.isValid()) {
            throw new BlazeAuthException(ACCESS_TOKEN, INVALID_ACCESS_TOKEN);
        }

        SwitchableApplication switchableApplication = connectConfiguration.getBlazeApplicationsConfig().getRedirectURL(appName);
        if (Objects.isNull(switchableApplication)) {
            throw new BlazeInvalidArgException(SWITCH_APPLICATION, "No application found with name : " + appName);
        }

        Employee employee = employeeRepository.get(connectAuthToken.getCompanyId(), connectAuthToken.getActiveTopUser().getUserId(), "{password:0,pin:0}");

        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee does not found");
        }
        Role role = roleService.getRoleById(employee.getCompanyId(), employee.getRoleId());
        boolean isAdmin = (role != null && role.getName().equalsIgnoreCase("admin"));

        if (!isAdmin && employee.getAppAccessList() != null && !employee.getAppAccessList().contains(CompanyFeatures.AppTarget.valueOf(appName)) && !CompanyFeatures.AppTarget.AuthenticationApp.toString().equals(appName)) {
            throw new BlazeInvalidArgException("Employee", "You don't have access for this application");
        }

        if (!isAdmin && !employee.getAppAccessList().contains(CompanyFeatures.AppTarget.valueOf(appName)) && !CompanyFeatures.AppTarget.AuthenticationApp.toString().equals(appName)) {
            throw new BlazeInvalidArgException("EmailLoginRequest", EMPLOYEE_NOT_ACCESS);
        }

        Company company = companyRepository.getById(connectAuthToken.getCompanyId());

        if (Objects.isNull(company)) {
            throw new BlazeAuthException("Company", "No company found");
        } else if (!company.isActive()) {
            throw new BlazeAuthException("Company", company.getName() + " is not active.");
        }

        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(connectAuthToken.getCompanyId());
        if (companyFeatures == null) {
            throw new BlazeInvalidArgException("CompanyFeatures", "Unknown company feature.");
        }

        // Check for availableApps
        if (companyFeatures.getAvailableApps() == null || !companyFeatures.getAvailableApps().contains(CompanyFeatures.AppTarget.valueOf(appName))) {
            throw new BlazeAuthException("Company", company.getName() + " is not configured for " + appName);
        }

        employee.setRole(role);

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();

        Iterable<ShopResult> items = shopRepository.getEmployeeShops(employee);

        List<ShopResult> shops = Lists.newArrayList(items);
        String shopId = null;
        ShopResult currentShop = null;
        List<ShopResult> employeeAllShops = new ArrayList<>();
        List<ShopResult> employeeShops = new ArrayList<>();

        // Filter out shops assigned to this employee
        for (ShopResult shop : shops) {
            if ((employee.getShops().contains(shop.getId()) && shop.isActive()) || isAdmin) {
                employeeAllShops.add(shop);
            }
        }

        // Filter out shops assigned to this employee
        for (ShopResult shop : employeeAllShops) {
            if (shop.isActive()) {
                employeeShops.add(shop);
            }
        }

        for (ShopResult shop : employeeShops) {
            if ((employee.getShops().contains(shop.getId()) && shop.isActive()) || isAdmin) {
                if (shop.getAppTarget() == CompanyFeatures.AppTarget.valueOf(appName) && shop.getId().equalsIgnoreCase(requestShopId)) {
                    currentShop = shop;
                }
            }
        }
        //Check if employee contains shop for requested app target
        boolean employeeAccess = Boolean.FALSE;
        for (ShopResult shop : employeeShops) {
            if (shop.getAppTarget().equals(CompanyFeatures.AppTarget.valueOf(appName))) {
                employeeAccess = Boolean.TRUE;
                //If current shop not exist then assign new one
                if (currentShop == null) {
                    currentShop = shop;
                }
                break;
            }
        }
        if (CompanyFeatures.AppTarget.AuthenticationApp.toString().equals(appName)) {
            for (ShopResult shop : employeeShops) {
                if (shop.getId().equals(requestShopId)) {
                    employeeAccess = Boolean.TRUE;
                    currentShop = shop;
                    break;
                }
            }
        }


        if ((isAdmin && currentShop == null) || (!isAdmin && !employeeAccess && currentShop == null)) {
            throw new BlazeInvalidArgException("EmailLoginRequest", EMPLOYEE_NOT_ACCESS);
        }

        if (!isAdmin && !currentShop.isActive()) {
            throw new BlazeInvalidArgException("EmailLoginRequest", ASSIGNED_SHOP_NOT_FOUND);
        }

        shopId = currentShop.getId();

        assignExciseTax(shops, exciseTaxInfos);

        ConnectAuthToken redirectToken = createNewToken(connectAuthToken.getCompanyId(),
                shopId,
                connectAuthToken.getManagerId(),
                connectAuthToken.getTerminalId(),
                connectAuthToken.getActiveTopUser().getFirstName(),
                connectAuthToken.getActiveTopUser().getLastName(),
                DateTime.now().plusSeconds(connectConfiguration.getBlazeApplicationsConfig().getTokenExpire()).getMillis(), CompanyFeatures.AppTarget.valueOf(appName));

        redirectToken.setRequestTimeZone(connectAuthToken.getRequestTimeZone());
        SwitchApplicationResult result = new SwitchApplicationResult();

        Terminal terminal = null;
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
        }

        prepareAssignedShop(currentShop);
        prepareEmployeeShops(employeeAllShops, redirectToken.getCompanyId());

        employee.setLastLoggedInShopId(shopId);
        if (redirectToken.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
            if (Objects.isNull(growAppType) || growAppType.equals(ConnectAuthToken.GrowAppType.None)) {
                throw new BlazeInvalidArgException("GrowAppType", "growAppType is missing");
            }
            redirectToken.setGrowAppType(growAppType);
            prepareAccessTokenForGrowShop(redirectToken, result);
            if (ConnectAuthToken.GrowAppType.Sales == growAppType) {
                result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(redirectToken.getCompanyId(), shopId, employee.getId())));
            } else {
                result.setAssetAccessToken(getInternalApiEncryptedAssetToken(createNewAssetToken(redirectToken.getCompanyId(), shopId, employee.getId())));
            }
        } else {
            redirectToken.setGrowAppType(ConnectAuthToken.GrowAppType.None);
            result.setAccessToken(getNewEncryptedToken(redirectToken));
            result.setAssetAccessToken(getNewEncryptedAssetToken(createNewAssetToken(redirectToken.getCompanyId(), shopId, employee.getId())));
        }

        result.setLoginTime(redirectToken.getInitDate());
        result.setExpirationTime(redirectToken.getExpirationDate());
        result.setEmployee(employee);
        result.setRedirectURL(switchableApplication.getAppLink());
        result.setCompany(company);
        result.setAssignedShop(currentShop);
        result.setShops(shops);
        result.setAssignedTerminal(terminal);
        result.setAppTarget(redirectToken.getAppTarget());
        result.setEmployeeShops(employeeAllShops);

        employeeRepository.updateLastLogin(redirectToken.getCompanyId(), employee.getId(), shopId);

        return result;
    }

    private void prepareAssignedShop(ShopResult shopResult) {
        if (shopResult == null) {
            return;
        }

        if (!Objects.isNull(shopResult.getAddress()) && StringUtils.isNotBlank(shopResult.getAddress().getState())) {
            String stateCode = shopResult.getAddress().getState();

            MetrcAccount metrcAccount = metrcAccountRepository.getMetrcAccount(shopResult.getCompanyId(), stateCode);
            if (metrcAccount != null && metrcAccount.getFacilities() != null && !metrcAccount.getFacilities().isEmpty()) {



                for (MetrcFacilityAccount facilityAccount : metrcAccount.getFacilities()) {
                    if (shopResult.getId().equals(facilityAccount.getShopId())) {
                        shopResult.setEnableMetrc(facilityAccount.isEnabled() && StringUtils.isNotBlank(facilityAccount.getFacLicense()));
                        shopResult.setMetrcLicense(facilityAccount.getFacLicense());

                        if (shopResult.getAppTarget() == CompanyFeatures.AppTarget.Grow) {

                            try {
                                IntegrationSettingConstants.Integrations.Metrc.MetrcState state = IntegrationSettingConstants.Integrations.Metrc.MetrcState.fromPrefix(stateCode);

                                MetrcConfig metrcConfig = integrationSettingRepository.getMetricConfig(
                                        state,
                                        facilityAccount.getEnvironment());
                                if (metrcConfig != null) {
                                    shopResult.setMetrcURLPath(metrcConfig.getHost());
                                    String binaryData = metrcConfig.getVendorKey() + ":" + facilityAccount.getUserApiKey();
                                    String accessToken = Base64.encodeBase64String(binaryData.getBytes());
                                    shopResult.setMetrcAuthToken(accessToken);
                                }

                            } catch (Exception e) {
                                // Ignore
                            }
                        }

                        break;
                    }
                }
            }
        }

        String aCacheKey = shopResult.getId() + "_" + "MemberTag";
        final String cacheKey = cacheService.generateKey(aCacheKey, CacheService.CacheType.DATA);
        MemberTagResult item = cacheService.getItem(cacheKey, MemberTagResult.class);
        if (item != null) {
            if (DateUtil.getMinutesBetweenTwoDates(item.getCreated(), DateTime.now().getMillis()) > CACHE_LIMIT_MINS) {
                LOG.info("Invalidate member tag cached item...");
                cacheService.invalidateItems(new String[]{cacheKey});
            } else {
                LOG.info("Member tag cached item found...");
                shopResult.setMembersTag(item.getMemberTags());
                return;
            }
        }

        LOG.info("Member tag cached item missed...");
        if (shopResult.getMembersTag() == null || shopResult.getMembersTag().size() == 0) {
            List<String> allMemberTags = memberRepository.getAllMemberTags(shopResult.getCompanyId());
            shopResult.setMembersTag(new HashSet<String>(allMemberTags));
        }

        MemberTagResult result = new MemberTagResult();
        result.prepare();
        result.setMemberTags(shopResult.getMembersTag());

        // Cache it for 10 minutes
        cacheService.putItem(cacheKey, result);
    }

    private void prepareEmployeeShops(List<ShopResult> employeeShops, String companyId) {
        Set<String> stateCodes = new HashSet<>();
        employeeShops.forEach(shopResult -> {
            String stateCode = shopResult.getAddress() == null ? "" : shopResult.getAddress().getState();
            if (StringUtils.isNotBlank(stateCode)) {
              stateCodes.add(stateCode);
            }
        });
        if (StringUtils.isBlank(companyId)) {
            companyId = token.getCompanyId();
        }
        HashMap<String, MetrcAccount> metrcAccounts = metrcAccountRepository.getMetrcAccountWithStateCode(companyId, new ArrayList<>(stateCodes));

        for (ShopResult shop : employeeShops) {
            Address address = shop.getAddress();
            if (address==null){
                continue;
            }
            if (StringUtils.isNotBlank(address.getState()) && metrcAccounts.containsKey(address.getState())) {
                MetrcAccount metrcAccount = metrcAccounts.get(address.getState());
                if (metrcAccount == null || metrcAccount.getFacilities() == null) {
                    continue;
                }
                for (MetrcFacilityAccount facilityAccount : metrcAccount.getFacilities()) {
                    if (shop.getId().equals(facilityAccount.getShopId())) {
                        shop.setEnableMetrc(facilityAccount.isEnabled() && StringUtils.isNotBlank(facilityAccount.getFacLicense()));
                        shop.setMetrcLicense(facilityAccount.getFacLicense());
                    }
                }
            }

        }

    }
}
