package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.services.common.CommonLabelService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import jersey.repackaged.com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.File;
import java.util.List;


/**
 * Created by mdo on 3/6/17.
 */
public class BarcodeServiceImpl implements BarcodeService {
    private static final Log LOG = LogFactory.getLog(BarcodeServiceImpl.class);
    @Inject
    CompanyUniqueSequenceRepository sequenceRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    BarcodeItemRepository barcodeItemRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    CommonLabelService commonLabelService;
    @Inject
    private ShopRepository shopRepository;


    @Override
    public DateSearchResult<BarcodeItem> getBarcodesWithDates(final String companyId, final String shopId, long afterDate, long beforeDate) {
        return barcodeItemRepository.findItemsWithDate(companyId, afterDate, beforeDate);
    }

    private BarcodeItem createBarcodeItem(final String companyId, final String shopId, String productId, final BarcodeItem.BarcodeEntityType entityType, final String entityId) {
        if (StringUtils.isBlank(productId)) {
            productId = entityId;
        }


        BarcodeItem item = new BarcodeItem();
        item.prepare(companyId);
        item.setShopId(shopId);
        item.setEntityType(entityType);
        item.setEntityId(entityId);
        item.setProductId(productId);

        Company company = companyRepository.getById(companyId);
        String substr = "";
        if (company.getName().length() >= 2) {
            substr = company.getName().substring(0, 1);
        }
        String idstr = productId.substring(entityId.length() - 3, productId.length()).toUpperCase();
        /// 3 digits name -  product-category -- product-type -- product name -- sku number
        /// 2 - 3 - 3 - 4
        /// AB-AB3-PRO-10
        String key = String.format("%s-%s", idstr, entityType.getCode()).toUpperCase();
        CompanyUniqueSequence uniqueSequence = sequenceRepository.getNewIdentifier(companyId, key);

        String sku = String.format("%s%s%s%d", substr, idstr, entityType.getCode(), uniqueSequence.getCount());
        item.setNumber((int) uniqueSequence.getCount());
        item.setBarcode(sku);
        barcodeItemRepository.save(item);

        return item;
    }

    @Override
    public BarcodeItem createBarcodeItemIfNeeded(final String companyId, final String shopId, String productId,
                                                 BarcodeItem.BarcodeEntityType entityType,
                                                 String entityId, String currentKey,
                                                 String oldKey,
                                                 boolean transferShop) {

        BarcodeItem item = null;
        if (oldKey != null) {
            BarcodeItem oldItem = barcodeItemRepository.getBarcode(companyId, shopId, entityType, oldKey);
            if (oldItem != null) {
                if (StringUtils.isNotBlank(oldKey) && currentKey.equalsIgnoreCase(oldKey) && oldItem.getEntityId().equalsIgnoreCase(entityId)) {
                    return oldItem; // same key so just return it. we don't have to create a new one
                }

                // delete old key so it can be re-used
                oldItem.setDeleted(true);
                oldItem.setBarcode(String.format("%s-del-%d", oldItem.getBarcode(), DateTime.now().getMillis()));
                barcodeItemRepository.update(companyId, oldItem.getId(), oldItem);
            }

        }
        //If current key matched with deleted barcode
        BarcodeItem deletedItem = barcodeItemRepository.getDeletedBarcode(companyId, shopId, entityType, currentKey);
        if (deletedItem != null) {
            deletedItem.setDeleted(false);
            if (!deletedItem.getProductId().equals(productId)) {
                LOG.info(String.format("Updating productId for %s to %s", currentKey, productId));
                deletedItem.setProductId(productId);
            }
            if (!deletedItem.getEntityId().equals(entityId)) {
                LOG.info(String.format("Updating entityId for %s to %s", currentKey, entityId));
                deletedItem.setEntityId(entityId);
            }
            LOG.info("Found old barcode -- reusing: " + currentKey);
            barcodeItemRepository.update(companyId, deletedItem.getId(), deletedItem);
            LOG.info("Found old barcode -- reusing: success" + currentKey);
            return deletedItem;
        }

        // test new key
        item = barcodeItemRepository.getBarcode(companyId, shopId, entityType, currentKey);
        if (item != null) {
            // If not the same entityType, then throw an exception stating that it's being used by another product/batch
            if (!item.getEntityId().equalsIgnoreCase(entityId)) {
                String productName = "";
                Product otherProduct = productRepository.get(companyId, item.getProductId());

                if (otherProduct != null) {
                    productName = otherProduct.getName();
                }
                if (transferShop
                        && entityType == BarcodeItem.BarcodeEntityType.Batch
                        && item.getProductId().equalsIgnoreCase(productId)) {
                    // ignore if you're transferring to another shop and the barcode already exists for this same product
                } else {
                    Shop shop = shopRepository.get(companyId, shopId);
                    if (shop == null) {
                        throw new BlazeInvalidArgException("Shop", "Shop not found");
                    }

                    if (!shop.isRetail()) {
                        throw new BlazeInvalidArgException("Barcode",
                                String.format("Unique ID '%s' is currently used by another %s for product '%s'.", currentKey, entityType.name(), productName));
                    } else {
                        throw new BlazeInvalidArgException("Barcode",
                                String.format("SKU '%s' is currently used by another %s for product '%s'.", currentKey, entityType.name(), productName));
                    }
                }
            }

            if (!item.getProductId().equalsIgnoreCase(productId)) {
                item.setProductId(productId);
                barcodeItemRepository.update(companyId, item.getId(), item);
            }
            return item;
        }
        if (StringUtils.isBlank(currentKey)) {
            return createBarcodeItem(companyId,shopId, productId, entityType, entityId);
        }
        currentKey = currentKey.toUpperCase();
        CompanyUniqueSequence uniqueSequence = sequenceRepository.getNewIdentifier(companyId, currentKey);

        item = new BarcodeItem();
        item.prepare(companyId);
        item.setShopId(shopId);
        item.setEntityType(entityType);
        item.setEntityId(entityId);
        item.setBarcode(currentKey);
        item.setProductId(productId);
        item.setNumber((int) uniqueSequence.getCount());

        // Save new barcode with the SKU
        barcodeItemRepository.save(item);
        return item;
    }

    @Override
    public void deleteBarcodesForProduct(String productId) {
        int deleted = barcodeItemRepository.removeAllInProductIds(Lists.newArrayList(productId));
        LOG.info("Deleted barcodes: " + deleted);
    }

    @Override
    public File getBarcodeImage(final String companyId, String barcodeId, int angle, int height, String barcodeType) {
        BarcodeItem item = barcodeItemRepository.get(companyId, barcodeId);
        if (item == null) {
            throw new BlazeInvalidArgException("Barcode", "Barcode does not exist.");
        }

        return commonLabelService.generateBarcode(companyId,item.getShopId(),item.getBarcode(), angle, height, barcodeType, CommonLabelService.SKUPosition.Bottom);
    }

    /**
     * Get bar code items by label
     *
     * @param companyId  : company id
     * @param shopId     : shop id
     * @param label      : label from which bar codes needs to be find
     * @param entityType : entity type (Product, Batch, Prepackage)
     */
    @Override
    public List<BarcodeItem> getBarCodesByLabel(String companyId, String shopId, String label, BarcodeItem.BarcodeEntityType entityType) {
        return barcodeItemRepository.getBarCodesByLabel(companyId, shopId, label, entityType);
    }

    @Override
    public BarcodeItem getBarcodeItemByBarcode(String companyId, String shopId, BarcodeItem.BarcodeEntityType entityType, String barcode) {
        return barcodeItemRepository.getBarcode(companyId, shopId, entityType, barcode);
    }

    @Override
    public List<BarcodeItem> getAvailableBarCodes(final String companyId, final String shopId,
                                                  BarcodeItem.BarcodeEntityType entityType, List<String> currentKeys) {

        return Lists.newArrayList(barcodeItemRepository.getAvailableBarCodes(companyId, shopId, entityType, currentKeys));

    }

    @Override
    public void deleteBarCodesForProducts(String companyId, String shopId, List<String> productIds) {
        Iterable<BarcodeItem> barCodeItems = barcodeItemRepository.getAllBarCodesForProduct(companyId, shopId, productIds);

        long now = DateTime.now().getMillis();

        for (BarcodeItem barCode : barCodeItems) {
            barCode.setDeleted(true);
            barCode.setBarcode(now + "-del-" + barCode.getBarcode());
            barCode.setModified(DateTime.now().getMillis());
            barcodeItemRepository.update(barCode.getId(), barCode);
        }

    }
}
