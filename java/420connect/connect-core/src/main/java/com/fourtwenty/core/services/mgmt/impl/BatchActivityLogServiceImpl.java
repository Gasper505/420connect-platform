package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.BatchActivityLog;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.repositories.dispensary.BatchActivityLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.BatchActivityLogService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BatchActivityLogServiceImpl extends AbstractAuthServiceImpl implements BatchActivityLogService {

    private static final String ACTIVITY_LOG = "Product Batch Activity Log";
    private static final String ACTIVITY_LOG_NOT_FOUND = "Batch activity log does not found";
    private static final String BATCH_NOT_FOUND = "Product Batch not found";

    private BatchActivityLogRepository batchActivityLogRepository;
    private ProductBatchRepository productBatchRepository;

    @Inject
    public BatchActivityLogServiceImpl(Provider<ConnectAuthToken> token, BatchActivityLogRepository batchActivityLogRepository, ProductBatchRepository productBatchRepository) {
        super(token);
        this.batchActivityLogRepository = batchActivityLogRepository;
        this.productBatchRepository = productBatchRepository;
    }

    @Override
    public BatchActivityLog addBatchActivityLog(String batchId, String employeeId, String log) {
        if (StringUtils.isNotBlank(batchId)) {
            BatchActivityLog batchActivityLog = new BatchActivityLog();
            batchActivityLog.prepare(token.getCompanyId());
            batchActivityLog.setShopId(token.getShopId());
            batchActivityLog.setEmployeeId(token.getActiveTopUser().getUserId());
            batchActivityLog.setTargetId(batchId);
            batchActivityLog.setLog(log);
            batchActivityLog.setActivityType(BatchActivityLog.ActivityType.NORMAL_LOG);
            batchActivityLogRepository.save(batchActivityLog);
        }
        return null;
    }

    @Override
    public SearchResult<BatchActivityLog> getAllBatchActivityLog(String companyId, String batchId, int start, int limit) {
        if (StringUtils.isBlank(batchId)) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, BATCH_NOT_FOUND);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return batchActivityLogRepository.getBatchActivityLogByBatchId(token.getCompanyId(), token.getShopId(), batchId, "{modified:-1}", start, limit);
    }

    @Override
    public void addBatchActivityLog(List<ObjectId> batchIds, String employeeId, String logFor) {
        if (batchIds == null || batchIds.isEmpty()) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, "Batch list cannot be empty");
        }
        List<BatchActivityLog> batchActivityLogs = new ArrayList<>();
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), batchIds);
        for (String key : batchHashMap.keySet()) {
            ProductBatch batch = batchHashMap.get(key);
            BatchActivityLog batchActivityLog = new BatchActivityLog();
            batchActivityLog.prepare(token.getCompanyId());
            batchActivityLog.setShopId(token.getShopId());
            batchActivityLog.setEmployeeId(token.getActiveTopUser().getUserId());
            batchActivityLog.setTargetId(batch.getId());
            batchActivityLog.setLog(batch.getSku() + logFor);
            batchActivityLog.setActivityType(BatchActivityLog.ActivityType.NORMAL_LOG);
            batchActivityLogs.add(batchActivityLog);
        }
        if (!batchActivityLogs.isEmpty()) {
            batchActivityLogRepository.save(batchActivityLogs);
        }
    }

    @Override
    public HashMap<String, List<BatchActivityLog>> findItemsByBatchId(String companyId, String shopId, List<String> batchIds) {
        return batchActivityLogRepository.findItemsByBatchIdAsMap(companyId, shopId, batchIds);
    }
}
