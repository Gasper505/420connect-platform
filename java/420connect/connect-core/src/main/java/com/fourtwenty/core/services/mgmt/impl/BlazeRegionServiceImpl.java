package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.domain.repositories.dispensary.BlazeRegionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.BlazeRegionService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlazeRegionServiceImpl extends AuthenticationServiceImpl implements BlazeRegionService {

    private static final Logger LOG = LoggerFactory.getLogger(BlazeRegionServiceImpl.class);

    private static final String REGION = "Region";
    private static final String REGION_NOT_FOUND = "Region does not found";

    @Inject
    private BlazeRegionRepository blazeRegionRepository;

    @Inject
    public BlazeRegionServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * This method gets region by id
     *
     * @param regionId : region's id that need to fetch
     */
    @Override
    public BlazeRegion getRegionById(String regionId) {
        BlazeRegion blazeRegion = blazeRegionRepository.get(token.getCompanyId(), regionId, BlazeRegion.class);

        if (blazeRegion == null) {
            throw new BlazeInvalidArgException(REGION, REGION_NOT_FOUND);
        }

        return blazeRegion;
    }

    /**
     * This method create new region
     *
     * @param request : Region's information
     */
    @Override
    public BlazeRegion createRegion(BlazeRegion request) {

        BlazeRegion region = new BlazeRegion();
        region.prepare(token.getCompanyId());
        region.setName(request.getName());
        region.setActive(request.isActive());
        region.setLatitude(request.getLatitude());
        region.setLongitude(request.getLongitude());
        region.setZipCodes(request.getZipCodes());

        return blazeRegionRepository.save(region);
    }

    /**
     * This method update region
     *
     * @param regionId : region id that needs to updated
     * @param request  : region information that need to update
     */
    @Override
    public BlazeRegion updateRegion(String regionId, BlazeRegion request) {

        BlazeRegion dbRegion = blazeRegionRepository.get(token.getCompanyId(), regionId);

        if (dbRegion == null) {
            throw new BlazeInvalidArgException(REGION, REGION_NOT_FOUND);
        }

        dbRegion.prepare(token.getCompanyId());
        dbRegion.setName(request.getName());
        dbRegion.setActive(request.isActive());
        dbRegion.setLatitude(request.getLatitude());
        dbRegion.setLongitude(request.getLongitude());
        dbRegion.setZipCodes(request.getZipCodes());

        return blazeRegionRepository.update(token.getCompanyId(), regionId, dbRegion);
    }

    /**
     * This method fetch list of regions
     *
     * @param start : start
     * @param limit : limit
     * @return : list of region
     */
    @Override
    public SearchResult<BlazeRegion> getRegions(int start, int limit) {

        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        return blazeRegionRepository.findItems(token.getCompanyId(), start, limit, BlazeRegion.class);
    }

    /**
     * This method delete region by id
     *
     * @param regionId : region id that need to be delete
     */
    @Override
    public void deleteRegionById(String regionId) {
        BlazeRegion region = blazeRegionRepository.get(token.getCompanyId(), regionId);
        if (region == null) {
            throw new BlazeInvalidArgException(REGION, REGION_NOT_FOUND);
        }

        blazeRegionRepository.removeById(token.getCompanyId(), regionId);
    }


}
