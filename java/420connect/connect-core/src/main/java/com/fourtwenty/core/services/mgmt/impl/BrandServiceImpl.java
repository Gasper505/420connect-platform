package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.BrandService;
import com.fourtwenty.core.services.mgmt.VendorService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;

public class BrandServiceImpl extends AbstractAuthServiceImpl implements BrandService {
    private BrandRepository brandRepository;
    private VendorRepository vendorRepository;
    private VendorService vendorService;
    private ProductRepository productRepository;

    private static final String BRAND = "Brand";
    private static final String BRAND_NOT_FOUND = "No data found for this brand.";
    private static final String BRAND_DELETED = "Brand is Already deleted!";
    private static final String BRAND_NAME_EXISTS = "Another Brand with this same name exists";
    private static final String VENDOR = "Vendor";
    private static final String VENDOR_NOT_FOUND = "Vendor does not found";
    private static final String BRAND_NAME_NOT_BLANK = "Brand name cannot be blank";
    private static final String SELECT_BRAND = "Please select brand";
    private static final String SELECT_OPERATION = "Please select operation type";
    private static final String VALID_BRAND = "Please select valid brands";

    @Inject
    public BrandServiceImpl(Provider<ConnectAuthToken> token, BrandRepository brandRepository, VendorRepository vendorRepository, VendorService vendorService, ProductRepository productRepository) {
        super(token);
        this.brandRepository = brandRepository;
        this.vendorRepository = vendorRepository;
        this.vendorService = vendorService;
        this.productRepository = productRepository;
    }

    @Override
    public Brand addBrand(BrandAddRequest request) {
        if (StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException(BRAND, BRAND_NAME_NOT_BLANK);
        }

        String name = request.getName().trim();
        Brand other = brandRepository.getBrandByName(token.getCompanyId(), name);
        if (other != null) {
            throw new BlazeInvalidArgException(BRAND, BRAND_NAME_EXISTS);
        }

        boolean validVendors = vendorService.checkValidVendors(Lists.newArrayList(request.getVendorList()));
        if (!validVendors) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }

        Brand brand = new Brand();
        brand.prepare(token.getCompanyId());
        brand.setName(request.getName());
        brand.setActive(request.getActive());
        brand.setWebsite(request.getWebsite());
        brand.setPhoneNo(request.getPhoneNo());
        brand.setBrandLogo(request.getBrandLogo());
        brand.setVendorList(request.getVendorList());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(request.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            brand.setExternalId(request.getExternalId());
        }

        Brand dbBrand = brandRepository.save(brand);

        //update brandId in vendors
        if (request.getVendorList() != null) {
            updateBrandInVendors(request.getVendorList(), dbBrand.getId());
        }
        return brand;
    }

    @Override
    public SearchResult<BrandResult> getAllBrand(String searchTerm, int start, int limit) {
        if (limit <= 0 || limit > 1000) {
            limit = 1000;
        }
        SearchResult<BrandResult> result;
        if (StringUtils.isBlank(searchTerm)) {
            result = brandRepository.getAllBrand(token.getCompanyId(), start, limit, "{modified:-1}", BrandResult.class);
        } else {
            result = brandRepository.findBrandBySearchTerms(token.getCompanyId(), searchTerm, start, limit, BrandResult.class);
        }

        prepareSearchResult(result);
        return result;

    }

    private void prepareSearchResult(SearchResult<BrandResult> result) {
        if (result != null && result.getValues() != null) {

            Set<ObjectId> vendorIds = new HashSet<>();

            for (BrandResult brandResult : result.getValues()) {
                if (brandResult.getVendorList() != null) {
                    for (String vendorId : brandResult.getVendorList()) {
                        if (ObjectId.isValid(vendorId)) {
                            vendorIds.add(new ObjectId(vendorId));
                        }
                    }
                }
            }

            HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(token.getCompanyId(), new ArrayList<>(vendorIds));

            for (BrandResult brandResult : result.getValues()) {
                prepareBrand(brandResult, vendorHashMap);
            }
        }
    }

    private void prepareBrand(BrandResult brandResult, HashMap<String, Vendor> vendorHashMap) {
        List<Vendor> vendorList = new ArrayList<>();

        if (brandResult.getVendorList() != null && vendorHashMap != null) {
            for (String vendorId : brandResult.getVendorList()) {
                Vendor vendor = vendorHashMap.get(vendorId);
                if (vendor != null) {
                    vendorList.add(vendor);
                }
            }
        }
        brandResult.setVendors(vendorList);
    }

    @Override
    public void deleteBrand(String brandId) {
        Brand dbproductBrand = brandRepository.get(token.getCompanyId(), brandId);
        if (dbproductBrand == null) {
            throw new BlazeInvalidArgException(BRAND, BRAND_NOT_FOUND);
        } else if (dbproductBrand.isDeleted()) {
            throw new BlazeInvalidArgException(BRAND, BRAND_DELETED);
        } else {

            productRepository.bulkUpdateBrandId(token.getCompanyId(), token.getShopId(), brandId);

            List<String> brands = new ArrayList<>();
            brands.add(brandId);

            vendorRepository.bulkRemoveBrand(token.getCompanyId(), brands);
            brandRepository.removeByIdSetState(token.getCompanyId(), brandId);
        }
    }

    @Override
    public Brand updateBrand(String brandId, Brand request) {
        Brand dbbrand = brandRepository.get(token.getCompanyId(), brandId);
        if (dbbrand == null) {
            throw new BlazeInvalidArgException(BRAND, BRAND_NOT_FOUND);
        }

        if (StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException(BRAND, BRAND_NAME_NOT_BLANK);
        }

        boolean validVendors = vendorService.checkValidVendors(Lists.newArrayList(request.getVendorList()));
        if (!validVendors) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }

        String name = request.getName().trim();
        Brand other = brandRepository.getBrandByName(token.getCompanyId(), name);
        if (other == null || request.getName().equalsIgnoreCase(dbbrand.getName())) {
            dbbrand.setName(name);
            dbbrand.setActive(request.getActive());
            dbbrand.setWebsite(request.getWebsite());
            dbbrand.setPhoneNo(request.getPhoneNo());
            dbbrand.setBrandLogo(request.getBrandLogo());
            dbbrand.setVendorList(request.getVendorList());
            if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
                if (StringUtils.isBlank(request.getExternalId())) {
                    throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
                }
                dbbrand.setExternalId(request.getExternalId());
            }

            dbbrand = brandRepository.update(token.getCompanyId(), brandId, dbbrand);
        } else {
            throw new BlazeInvalidArgException(BRAND, BRAND_NAME_EXISTS);
        }

        //update brandId in vendors
        if (request.getVendorList() != null) {
            updateBrandInVendors(request.getVendorList(), dbbrand.getId());
        }

        return dbbrand;
    }

    @Override
    public BrandResult getBrandByBrandId(String brandId) {
        BrandResult brandResult = brandRepository.get(token.getCompanyId(), brandId, BrandResult.class);

        HashMap<String, Vendor> vendorHashMap = null;
        if (brandResult.getVendorList() != null) {
            List<ObjectId> vendorIds = new ArrayList<>();
            for (String vendorId : brandResult.getVendorList()) {
                vendorIds.add(new ObjectId(vendorId));
            }
            vendorHashMap = vendorRepository.listAsMap(token.getCompanyId(), vendorIds);
        }

        prepareBrand(brandResult, vendorHashMap);

        return brandResult;
    }

    @Override
    public SearchResult<BrandResult> getAllDeletedBrand(int skip, int limit) {
        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        SearchResult<BrandResult> result = brandRepository.getAllDeletedBrand(token.getCompanyId(), skip, limit, "{modified:-1}", BrandResult.class);

        prepareSearchResult(result);
        return result;
    }

    @Override
    public List<Vendor> getVendorsByBrand(String brandId) {

        Brand brand = brandRepository.get(token.getCompanyId(), brandId);
        if (brand == null) {
            throw new BlazeInvalidArgException(BRAND, BRAND_NOT_FOUND);
        }

        Iterable<Vendor> vendors = vendorRepository.getVendorsByBrand(token.getCompanyId(), brandId);
        if (vendors != null) {
            return Lists.newArrayList(vendors);
        }

        return new ArrayList<>();
    }

    private void updateBrandInVendors(LinkedHashSet<String> vendorList, String brandId) {

        List<ObjectId> vendors = new ArrayList<>();
        if (vendorList != null) {
            for (String vendorIds : vendorList) {
                vendors.add(new ObjectId(vendorIds));
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(token.getCompanyId(), vendors);

        if (vendorHashMap != null) {
            for (String vendorId : vendorList) {
                if (vendorHashMap.containsKey(vendorId)) {
                    Vendor vendor = vendorHashMap.get(vendorId);
                    LinkedHashSet<String> brands = vendor.getBrands();
                    if (!vendor.getBrands().contains(brandId)) {
                        brands.add(brandId);
                    }
                    vendor.setBrands(brands);
                    vendorRepository.update(token.getCompanyId(), vendor.getId(), vendor);
                }
            }
        }
    }

    /**
     * This method perform operations for bulk update
     * @param request
     */
    @Override
    public void bulkBrandUpdate(BrandBulkUpdateRequest request) {
        if (request.getBrandIds() == null || request.getBrandIds().size() == 0) {
            throw new BlazeInvalidArgException(BRAND, SELECT_BRAND);
        }

        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(BRAND, SELECT_OPERATION);
        }

        List<ObjectId> brandObjectIds = new ArrayList<>();
        for (String id : request.getBrandIds()) {
            if (ObjectId.isValid(id)) {
                brandObjectIds.add(new ObjectId(id));
            } else {
                throw new BlazeInvalidArgException(BRAND, VALID_BRAND);
            }
        }

        switch (request.getOperationType()) {
            case WEBSITE:
                brandRepository.bulkUpdateWebsite(token.getCompanyId(), brandObjectIds, request.getWebsite());
                break;
            case PHONE_NUMBER:
                brandRepository.bulkUpdatePhoneNo(token.getCompanyId(), brandObjectIds, request.getPhoneNumber());
                break;
            case STATUS:
                brandRepository.bulkUpdateStatus(token.getCompanyId(), brandObjectIds, request.getActive());
                break;
            case DELETE:
                productRepository.bulkRemoveBrand(token.getCompanyId(), token.getShopId(), request.getBrandIds());
                vendorRepository.bulkRemoveBrand(token.getCompanyId(), request.getBrandIds());
                brandRepository.deleteBrands(token.getCompanyId(), brandObjectIds);
                break;
        }

    }
}
