package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.CustomerInfo;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.loyalty.*;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.AdjustmentInfo;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.DiscountVersionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundOrderItemRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.RefundRequest;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.mgmt.PromotionProcessService;
import com.fourtwenty.core.services.mgmt.models.LinkedOrderItem;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import joptsimple.internal.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by mdo on 12/23/16.
 */
public class CartServiceImpl implements CartService {
    private static final Log LOG = LogFactory.getLog(CartServiceImpl.class);
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    PromotionProcessService promotionProcessService;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    PromotionRepository promotionRepository;
    @Inject
    LoyaltyRewardRepository rewardRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    MemberGroupPricesRepository memberGroupPricesRepository;
    @Inject
    AppRepository appRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @javax.inject.Inject
    UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    private DeliveryTaxRateRepository deliveryTaxRateRepository;
    @Inject
    private BlazeRegionRepository blazeRegionRepository;
    @Inject
    private AdjustmentRepository adjustmentRepository;
    @Inject
    private CultivationTaxInfoRepository cultivationTaxInfoRepository;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private ProductVersionRepository productVersionRepository;
    @Inject
    private DiscountVersionRepository discountVersionRepository;


    public void prepareCart(Shop shop,
                            Transaction trans,
                            boolean reset,
                            Transaction.TransactionStatus incomingStatus,
                            final boolean assignProducts,
                            final CustomerInfo customerInfo,
                            final boolean newCart) {
        if (trans == null) {
            return;
        }
        if (shop == null) {
            return;
        }
        Cart cart = trans.getCart();
        //update customer info's zipcode with transactions zipcode
        if (trans.getDeliveryAddress() != null && StringUtils.isNotBlank(trans.getDeliveryAddress().getZipCode())) {
            customerInfo.setZipCode(trans.getDeliveryAddress().getZipCode());
        }

        MemberGroup memberGroup = customerInfo != null ? customerInfo.getMemberGroup() : null;
        if (cart != null) {
            cart.setCompanyId(shop.getCompanyId());
            cart.setModified(DateTime.now().getMillis());
            if (reset) {
                cart.setId(ObjectId.get().toString());
                cart.setCreated(DateTime.now().getMillis());
            }
            boolean potentialDeliveryFee = mayHaveDeliveryFees(shop, trans);
            cart.setEnableDeliveryFee(potentialDeliveryFee);
            //setting tip
            if (shop.isEnableTip() && (cart.getTipAmount().intValue() > 0))
                cart.setTipAmount(cart.getTipAmount());
            else
                cart.setTipAmount(new BigDecimal(0));

            DeliveryTaxRate deliveryTaxRate = null;
            if (Transaction.QueueType.Delivery == trans.getQueueType()) {
                deliveryTaxRate = this.getDeliveryTaxRateByZipCode(shop, trans, customerInfo);
            }
            // set tax
            if (deliveryTaxRate != null && deliveryTaxRate.getTaxInfo() != null && deliveryTaxRate.isActive()) {
                cart.setTax(deliveryTaxRate.getTaxInfo().getTotalTax());
            } else if (shop.getTaxInfo() != null) {
                cart.setTax(shop.getTaxInfo().getTotalTax());
            } else {
                cart.setTax(new BigDecimal(0));
            }
            cart.setTaxTable(null);

            ConsumerType consumerType = ConsumerType.AdultUse;
            if (customerInfo != null) {
                consumerType = customerInfo.getConsumerType();
            }

            if (deliveryTaxRate != null && deliveryTaxRate.isUseComplexTax() && deliveryTaxRate.isActive()) {
                CompoundTaxTable compoundTaxTable = null;
                if (deliveryTaxRate.getTaxTables() != null) {
                    for (CompoundTaxTable taxTable : deliveryTaxRate.getTaxTables()) {
                        if (taxTable.getConsumerType() == consumerType) {
                            if (taxTable.getConsumerType() == ConsumerType.AdultUse && "Other".equalsIgnoreCase(taxTable.getName())) {
                                continue;
                            }
                            compoundTaxTable = taxTable;
                            break;
                        }
                    }
                }
                if (compoundTaxTable != null) {
                    cart.setTaxTable(compoundTaxTable.copyTaxTable());
                }
            } else if (shop.isUseComplexTax() || (shop.getNonCannabisTaxes() != null && shop.getNonCannabisTaxes().isUseComplexTax())) {
                //=
                if (shop.isUseComplexTax()) {
                    CompoundTaxTable compoundTaxTable = null;
                    if (shop.getTaxTables() != null) {
                        for (CompoundTaxTable taxTable : shop.getTaxTables()) {
                            if (taxTable.getConsumerType() == consumerType) {
                                if (taxTable.getConsumerType() == ConsumerType.AdultUse && "Other".equalsIgnoreCase(taxTable.getName())) {
                                    // Mixmatch
                                    continue;
                                }
                                compoundTaxTable = taxTable;
                                break;
                            }
                        }
                    }
                    if (compoundTaxTable != null) {
                        cart.setTaxTable(compoundTaxTable.copyTaxTable());
                    }
                }
                if ((shop.getNonCannabisTaxes() != null && shop.getNonCannabisTaxes().isUseComplexTax())) {
                    CompoundTaxTable compoundTaxTable = null;
                    if ((shop.getNonCannabisTaxes() != null && shop.getNonCannabisTaxes().getTaxTables() != null)) {
                        for (CompoundTaxTable taxTable : shop.getNonCannabisTaxes().getTaxTables()) {
                            if (taxTable.getConsumerType() == consumerType) {
                                if (taxTable.getConsumerType() == ConsumerType.AdultUse && "Other".equalsIgnoreCase(taxTable.getName())) {
                                    // Mixmatch
                                    continue;
                                }
                                compoundTaxTable = taxTable;
                                break;
                            }
                        }
                    }
                    if (compoundTaxTable != null) {
                        cart.setNonCannabisTaxTable(compoundTaxTable.copyTaxTable());
                    }
                }
            }
            cart.setConsumerType(consumerType);

            if (cart.getSplitPayment() != null) {
                cart.getSplitPayment().prepare(cart.getCompanyId());
            }

            // Get tolerance map
            HashMap<String, ProductWeightTolerance> toleranceHashMap = toleranceRepository.getActiveTolerancesAsMap(shop.getCompanyId());
            List<ObjectId> objectIds = new ArrayList<>();
            List<String> productIds = new ArrayList<>();

            List<ObjectId> vendorIds = new ArrayList<>();

            // get products
            for (OrderItem orderItem : cart.getItems()) {

                if (StringUtils.isBlank(orderItem.getOrderItemId())) {
                    orderItem.setOrderItemId(UUID.randomUUID().toString());
                }

                if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                    objectIds.add(new ObjectId(orderItem.getProductId()));
                    productIds.add(orderItem.getProductId());
                }
                if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail && trans.getRefundVersion() != RefundRequest.RefundVersion.NEW
                    && (orderItem.getOverridePrice() == null || orderItem.getOverridePrice().doubleValue() == 0)) {
                    orderItem.setOverridePrice(null);
                }
            }
            // Fetch data
            HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(shop.getCompanyId(),
                    shop.getId(), objectIds);
            HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAllAsMap(shop.getCompanyId(), shop.getId());
            HashMap<String, PrepackageProductItem> prepackageProductItemHashMap =
                    prepackageProductItemRepository.getPrepackagesForProductsAsMap(shop.getCompanyId(),
                            shop.getId(), productIds);
            HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(shop.getCompanyId(),
                    shop.getId(), productIds);

            HashMap<String, MemberGroupPrices> memberGroupPricesHashMap = new HashMap<>();
            if (customerInfo != null && customerInfo.getMemberGroup() != null) {
                memberGroupPricesHashMap = memberGroupPricesRepository.getPricesMapForProductGroup(shop.getCompanyId(), shop.getId(), productIds, customerInfo.getMemberGroup().getId());
            }

            // Populate prepackage with weigh tolerance values
            for (Prepackage prepackage : prepackageHashMap.values()) {
                if (!prepackage.isCustomWeight() && StringUtils.isNotBlank(prepackage.getToleranceId())) {
                    ProductWeightTolerance tolerance = toleranceHashMap.get(prepackage.getToleranceId());

                    if (tolerance != null) {
                        prepackage.setUnitValue(tolerance.getUnitValue());
                        prepackage.setName(tolerance.getName());
                    }
                }
            }

            // apply discounts for members if possible
            if (newCart) {
                // only do this for new carts
                if (trans.getCart().getDiscount().doubleValue() <= 0) {
                    double itemDiscounts = 0;
                    for (OrderItem orderItem : trans.getCart().getItems()) {
                        itemDiscounts += orderItem.getDiscount().doubleValue();
                    }
                    if (itemDiscounts <= 0 && memberGroup != null && memberGroup.isActive()) {
                        if (memberGroup.isEnablePromotion() && StringUtils.isNotBlank(memberGroup.getPromotionId())) {

                            Promotion promotion = promotionRepository.get(memberGroup.getCompanyId(), memberGroup.getPromotionId());

                            if (promotion != null) {
                                LinkedHashSet<PromotionReq> promotionReqs = cart.getPromotionReqs();
                                if (promotionReqs == null) {
                                    promotionReqs = new LinkedHashSet<>();
                                }
                                // make sure requests doesn't already included this promo
                                boolean alreadyIncluded = false;
                                for (PromotionReq req : promotionReqs) {
                                    if (req != null && StringUtils.isNotBlank(req.getPromotionId())
                                            && req.getPromotionId().equalsIgnoreCase(promotion.getId())) {
                                        alreadyIncluded = true;
                                        break;
                                    }
                                }
                                if (!alreadyIncluded) {
                                    PromotionReq promotionReq = new PromotionReq();
                                    promotionReq.setPromotionId(promotion.getId());

                                    promotionReqs.add(promotionReq);
                                    cart.setPromotionReqs(promotionReqs);
                                }
                            }
                        } else if (!memberGroup.isEnablePromotion()) {
                            // no item discounts as well. Add member group discounts here
                            trans.getCart().setDiscount(memberGroup.getDiscount());
                            trans.getCart().setDiscountType(memberGroup.getDiscountType());
                        }
                    }
                }
            }

            // process pricing
            Cart processedCart = this.processEntireCartPricingPromos(shop,
                    trans.getCart(), reset,
                    assignProducts,
                    memberGroup,
                    memberGroupPricesHashMap,
                    productHashMap,
                    prepackageProductItemHashMap,
                    prepackageHashMap,
                    categoryHashMap,
                    toleranceHashMap,
                    trans,
                    incomingStatus,
                    customerInfo
            );
            trans.setCart(processedCart);


            double totalRefund = 0.0;
            double refundWithoutDiscount = 0.0;
            double subTotal = 0d;
            double discountedRefunded = 0.0;
            double totalDiscounts = 0.0;
            double origSubTotal = 0d;
            // calculate final price with discounts
            for (OrderItem orderItem : trans.getCart().getItems()) {
                // reset orig to quantity
                orderItem.setQuantity(orderItem.getOrigQuantity());

                /*if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }*/


                // Calculate final price
                // If discountType == Percentage, calculate percentage
                // if discountType == Cash, use actual discount
                // finalCost = cost - discount
                double cost = orderItem.getCost().doubleValue(); // Should have been calculated in previous step
                double finalCost = cost;

                if (orderItem.getDiscount().doubleValue() > 0 && cost > 0) {
                    if (orderItem.getDiscountType() == OrderItem.DiscountType.Cash) {
                        finalCost = cost - orderItem.getDiscount().doubleValue();
                        orderItem.setCalcDiscount(orderItem.getDiscount());
                    } else {
                        double orderDiscount = (cost * orderItem.getDiscount().doubleValue() / 100);
                        finalCost = cost - orderDiscount;
                        orderItem.setCalcDiscount(new BigDecimal(orderDiscount));
                    }
                }

                orderItem.setFinalPrice(new BigDecimal(finalCost)); //NumberUtils.round(finalCost,2));
                if (trans.getTransType() == Transaction.TransactionType.Refund) {
                    if (orderItem.getTotalRefundAmount() != null && orderItem.getTotalRefundAmount().doubleValue() > 0) {
                        // OVERRIDE TO USE REFUND TOTAL AMT AS THE FINAL PRICE SO CALCULATION WOULD BE CORRECT

                        if (orderItem.getTotalRefundAmount().doubleValue() < orderItem.getFinalPrice().doubleValue()) {
                            double orderDiscount = orderItem.getFinalPrice().doubleValue() - orderItem.getTotalRefundAmount().doubleValue();
                            orderItem.setCalcDiscount(new BigDecimal(orderDiscount));
                            orderItem.setDiscount(new BigDecimal(orderDiscount));
                            orderItem.setDiscountNotes(orderItem.getDiscountNotes());
                        }

                        if (trans.getRefundVersion() != RefundRequest.RefundVersion.NEW) {
                            orderItem.setFinalPrice(orderItem.getTotalRefundAmount());
                        }
                    }
                }

                // calculate total discounts
                if (orderItem.getDiscountType() == OrderItem.DiscountType.Cash) {
                    totalDiscounts += orderItem.getDiscount().doubleValue();
                } else {
                    totalDiscounts += orderItem.getCost().doubleValue() * (orderItem.getDiscount().doubleValue() / 100);
                }

                subTotal += orderItem.getFinalPrice().doubleValue();
                origSubTotal += orderItem.getCost().doubleValue();
            }


            // Prepare final cart cost
            trans.getCart().setRefundAmount(new BigDecimal(0));

            // Calculate subtotal
            trans.getCart().setSubTotal(new BigDecimal(origSubTotal)); //NumberUtils.round(subTotalPreDiscount,2));


            // Calculate Discount
            double totalCalcCartDiscount = 0;
            if (subTotal > 0) {
                if (trans.getCart().getDiscountType() == OrderItem.DiscountType.Cash) {
                    // if discount is greater than subTotal, then set the discount to be the subtotal
                    if (subTotal <= trans.getCart().getDiscount().doubleValue()) {
                        trans.getCart().setDiscount(new BigDecimal(subTotal));
                    }

                    subTotal = subTotal - trans.getCart().getDiscount().doubleValue();
                    totalDiscounts = totalDiscounts + trans.getCart().getDiscount().doubleValue();
                    totalCalcCartDiscount = trans.getCart().getDiscount().doubleValue();
                } else {

                    double cartDiscount = (subTotal * trans.getCart().getDiscount().doubleValue() / 100);
                    totalCalcCartDiscount = cartDiscount;
                    subTotal = subTotal - cartDiscount;
                    totalDiscounts = totalDiscounts + cartDiscount;
                }
            }

            trans.getCart().setSubTotalDiscount(new BigDecimal(subTotal)); //NumberUtils.round(subTotal,2));

            // Calculate discounts proportionally so we can apply taxes
            HashMap<String, Double> propDiscountMap = new HashMap<>();
            if (trans.getCart().getItems().size() > 0) {
                double total = 0.0;
                for (OrderItem orderItem : trans.getCart().getItems()) {
                    /*if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                        continue;
                    }*/

                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product != null) {
                        if (product.isDiscountable()) {
                            total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                        }
                    }
                }

                // Use proportions
                for (OrderItem orderItem : trans.getCart().getItems()) {
                    /*if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                        continue;
                    }*/

                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product != null) {
                        if (StringUtils.isNotBlank(product.getVendorId()) && ObjectId.isValid(product.getVendorId())) {
                            vendorIds.add(new ObjectId(product.getVendorId()));
                        }
                        propDiscountMap.put(orderItem.getOrderItemId(), 0d);
                        if (product.isDiscountable()) {
                            double finalCost = orderItem.getFinalPrice().doubleValue();
                            double ratio = finalCost / total;
                            double propDiscount = NumberUtils.round(totalCalcCartDiscount * ratio, 6);

                            propDiscountMap.put(orderItem.getOrderItemId(), propDiscount);
                        }
                    }
                }
            }
            trans.getCart().setCalcCartDiscount(new BigDecimal(totalCalcCartDiscount));

            HashMap<String, Vendor> vendorMap = new HashMap<String, Vendor>();

            if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
                vendorMap = vendorRepository.listAsMap(shop.getCompanyId(), vendorIds);
            }

            // Apply product based taxes
            TaxResult taxResult = applyTax(customerInfo, trans.getCart(), productHashMap, shop, propDiscountMap, true, vendorMap, categoryHashMap, prepackageProductItemHashMap, prepackageHashMap, toleranceHashMap, deliveryTaxRate);
            double totalPostTax = taxResult.getTotalPostCalcTax().doubleValue();
            double totalPreTax = taxResult.getTotalPreCalcTax().doubleValue();
            double totalExciseTax = taxResult.getTotalExciseTax().doubleValue();
            double toatlALExciseTax = taxResult.getTotalALExciseTax().doubleValue();

            // apply tax
            //double totalTax = subTotal * trans.getCart().getTax().doubleValue();
            subTotal = subTotal + totalPostTax;

            subTotal = applyPostTaxDiscount(trans, subTotal);

            totalPostTax += totalPreTax; // add pre-taxed to the total taxed
            trans.getCart().setTotalCalcTax(new BigDecimal(totalPostTax));
            trans.getCart().setTotalPreCalcTax(new BigDecimal(totalPreTax));
            trans.getCart().setTotalExciseTax(new BigDecimal(totalExciseTax));
            trans.getCart().setTotalALExciseTax(new BigDecimal(toatlALExciseTax));
            trans.getCart().setTaxResult(taxResult);

            // Apply delivery Fee
            if (trans.getTransType() == Transaction.TransactionType.Sale) {
                applyDeliveryFee(shop, trans, customerInfo, subTotal);
            }

            // sanity test (after discounts)
            if (subTotal < trans.getCart().getDeliveryFee().doubleValue()) {
                cart.setDeliveryFee(new BigDecimal(0));
            }

            // set delivery fee
            if (trans.getCart().getDeliveryFee() == null) {
                trans.getCart().setDeliveryFee(new BigDecimal(0));
            }


//          Apply Credit card fee
            applyCreditCardFee(shop, trans, subTotal);

            if (trans.getCart().getCreditCardFee() == null) {
                trans.getCart().setCreditCardFee(new BigDecimal(0));
            }

            //Update credit card fees according to split payment.
            if (trans.getCart().getPaymentOption() == Cart.PaymentOption.Split && trans.getCart().getSplitPayment() != null && trans.getCart().getCreditCardFee().doubleValue() > 0) {
                SplitPayment splitPayment = trans.getCart().getSplitPayment();
                if (splitPayment.getCreditDebitAmt() != null && splitPayment.getCreditDebitAmt().doubleValue() > 0) {
                    double ccFees = trans.getCart().getCreditCardFee().doubleValue();
                    double splitCcFees = (subTotal == 0) ? 0 : (ccFees * splitPayment.getCreditDebitAmt().doubleValue() / subTotal);
                    trans.getCart().setCreditCardFee(BigDecimal.valueOf(splitCcFees));
                }
            }
            // add delivery fee in
            subTotal += trans.getCart().getDeliveryFee().doubleValue();

            //Add credit card fee
            subTotal += trans.getCart().getCreditCardFee().doubleValue();

            double total = subTotal;
            if (total < 0) {
                total = 0;
            }


            //double total = queuedTransaction.getCart().getSales();
            double roundedTotal = total;
            double roundOffTotal = 0.0;

            if (Shop.TaxRoundOffType.FIVE_CENT.equals(shop.getTaxRoundOffType()) && trans.getCart().getPaymentOption() == Cart.PaymentOption.Cash) {
                roundOffTotal = NumberUtils.roundToNearestCent(roundedTotal);
            } else if (Shop.TaxRoundOffType.NEAREST_DOLLAR == shop.getTaxRoundOffType()) {
                roundOffTotal = NumberUtils.roundToNearestCent(roundedTotal);
            } else if (Shop.TaxRoundOffType.UP_DOLLAR == shop.getTaxRoundOffType()) {
                roundOffTotal = NumberUtils.ceil(roundedTotal);
            } else if (Shop.TaxRoundOffType.DOWN_DOLLAR == shop.getTaxRoundOffType()) {
                roundOffTotal = NumberUtils.floor(roundedTotal);
            } else {
                roundOffTotal = NumberUtils.round(roundedTotal, 2);
            }

            trans.getCart().setRoundAmt(new BigDecimal(Math.abs(roundedTotal - roundOffTotal)));
            trans.getCart().setTotal(new BigDecimal(roundOffTotal));
            trans.getCart().setTaxRoundOffType(shop.getTaxRoundOffType());

            // reset total discounts amount (adhering to refunded items)
            double currentDiscount = totalDiscounts; //transaction.getCart().getTotalDiscount();
            currentDiscount -= discountedRefunded;

            // zero out just in case
            if (currentDiscount < 0) {
                currentDiscount = 0;
            }

            trans.getCart().setTotalDiscount(new BigDecimal(currentDiscount)); //currentDiscount);

            if (trans.getTransType() == Transaction.TransactionType.Refund) {
                if (trans.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                    trans.getCart().setRefundAmount(trans.getCart().getTotal());
                }
            } else {
                // Sale
                if (trans.getCart().getPaymentOption() == Cart.PaymentOption.Split) {
                    if (trans.getCart().getSplitPayment() != null) {
                        // account for change
                        BigDecimal changedDue = cart.getSplitPayment().getTotalSplits().subtract(cart.getTotal());
                        trans.getCart().setCashReceived(trans.getCart().getSplitPayment().getCashAmt());
                        if (changedDue.doubleValue() > 0) {
                            // implies cashReceived > totalCashValue
                            trans.getCart().setChangeDue(changedDue);
                        }
                    }
                }
            }

            if (!CollectionUtils.isNullOrEmpty(cart.getAdjustmentInfoList())) {
                BigDecimal adjustmentAmt = processAdjustments(shop.getCompanyId(), cart.getAdjustmentInfoList());

                BigDecimal grandTotal = adjustmentAmt.add(BigDecimal.valueOf(roundOffTotal));

                trans.getCart().setTotal(grandTotal);
                trans.getCart().setAdjustmentAmount(adjustmentAmt);
            }

        }
    }

    public double applyPostTaxDiscount(Transaction transaction, double subTotal) {
        double currentSubTotal = subTotal;
        Cart cart = transaction.getCart();
        cart.setAppliedAfterTaxDiscount(new BigDecimal(0));
        if (cart.getAfterTaxDiscount() != null && cart.getAfterTaxDiscount().doubleValue() > 0) {

            double myDiscount = NumberUtils.round(cart.getAfterTaxDiscount().doubleValue(), 2);
            cart.setAfterTaxDiscountNotes(cart.getAfterTaxDiscountNotes());
            currentSubTotal = currentSubTotal - myDiscount;
            if (currentSubTotal < 0) {
                cart.setAppliedAfterTaxDiscount(new BigDecimal(subTotal));
                currentSubTotal = 0;
            } else {
                cart.setAppliedAfterTaxDiscount(new BigDecimal(myDiscount));
            }
        }
        return currentSubTotal;
    }

    private double getUnitCost(Product p, HashMap<String, ProductBatch> batchMap, HashMap<String, List<Product>> linkToProductMap) {
        ProductBatch batch = batchMap.get(p.getId());

        if (batch == null) {
            List<Product> products = linkToProductMap.get(p.getCompanyLinkId());
            if (products != null) {
                for (Product prod : products) {
                    batch = batchMap.get(prod.getId());
                    if (batch != null) {
                        break;
                    }
                }
            }
        }

        double unitCost = 0;
        if (batch != null) {
            unitCost = batch.getFinalUnitCost().doubleValue();
        }
        return unitCost;
    }

    public TaxResult applyTax(CustomerInfo customerInfo, Cart cart,
                              HashMap<String, Product> productHashMap,
                              Shop shop,
                              HashMap<String, Double> propDiscountMap,
                              boolean reset,
                              HashMap<String, Vendor> vendorMap,
                              HashMap<String, ProductCategory> categoryHashMap,
                              HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                              HashMap<String, Prepackage> prepackageHashMap,
                              HashMap<String, ProductWeightTolerance> weightToleranceHashMap, DeliveryTaxRate deliveryTaxRate) {

        int roundTaxDecimal = 2;
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        //TODO It should be removed after rounding finalization.
        if (!shop.isRetail()) {
            roundTaxDecimal = 4;
        } else if (app != null && app.getDefaultTaxRoundOff() != 0) {
            roundTaxDecimal = app.getDefaultTaxRoundOff();
        }
        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }
        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);
        ExciseTaxInfo nonCannabisTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country, ExciseTaxInfo.CannabisTaxType.NonCannabis);
        CultivationTaxInfo cultivationTaxInfo = cultivationTaxInfoRepository.getCultivationTaxInfoByState(state, country);

        List<String> productIds = new ArrayList<>();
        for (OrderItem orderItem : cart.getItems()) {
            productIds.add(orderItem.getProductId());
        }
        HashMap<String, ProductBatch> allBatchMap = productBatchRepository.getBatchesForProductsMap(shop.getCompanyId(), productIds);


        return calculateTax(customerInfo, cart, productHashMap,
                shop, propDiscountMap,
                reset, vendorMap, categoryHashMap,
                prepackageProductItemHashMap,
                prepackageHashMap,
                weightToleranceHashMap, exciseTaxInfo, allBatchMap, roundTaxDecimal, deliveryTaxRate, cultivationTaxInfo, nonCannabisTaxInfo);
    }

    public TaxResult calculateTax(CustomerInfo customerInfo, Cart cart,
                                  HashMap<String, Product> productHashMap,
                                  Shop shop,
                                  HashMap<String, Double> propDiscountMap,
                                  boolean reset,
                                  final HashMap<String, Vendor> vendorMap,
                                  final HashMap<String, ProductCategory> categoryHashMap,
                                  final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                  final HashMap<String, Prepackage> prepackageHashMap,
                                  final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                                  final ExciseTaxInfo cannabisTaxInfo,
                                  final HashMap<String, ProductBatch> allBatchMap,
                                  int roundTaxDecimal, DeliveryTaxRate deliveryTaxRate, CultivationTaxInfo cultivationTaxInfo,
                                  final ExciseTaxInfo nonCannabisTaxInfo) {


        // Apply product based taxes
        double totalPostTax = 0;
        double totalPreTax = 0;

        double totalCityTax = 0d;
        double totalCountyTax = 0d;
        double totalStateTax = 0d;
        double totalFedTax = 0d;

        double totalCityPreTax = 0d;
        double totalCountyPreTax = 0d;
        double totalStatePreTax = 0d;
        double totalFedPreTax = 0d;

        double totalALExciseTax = 0d;
        double totalALPOSTExciseTax = 0d;
        double totalNALPreExciseTax = 0d;
        double totalNALPOSTExciseTax = 0d;
        double totalCultivationTax = 0d;

        double totalPlantTax = 0d;
        double totalPlantOz = 0d;
        double plantTaxOz = 0d;

        double totalFlowerTax = 0d;
        double totalFlowerOz = 0d;
        double flowerTaxOz = 0d;

        double totalLeafTax = 0d;
        double totalLeafOz = 0d;
        double leafTaxOz = 0d;

        HashMap<String, ProductBatch> oldestAvailBatchMap = new HashMap<>();
        List<ProductBatch> sortedBatches = Lists.newArrayList(allBatchMap.values());
        Collections.sort(sortedBatches, new Comparator<ProductBatch>() {
            @Override
            public int compare(ProductBatch o1, ProductBatch o2) {
                return ((Long)o2.getPurchasedDate()).compareTo(o1.getPurchasedDate());
            }
        });
        for (ProductBatch batch : sortedBatches) {
            ProductBatch oldestBatch = oldestAvailBatchMap.get(batch.getProductId());
            if (oldestBatch == null) {
                oldestAvailBatchMap.put(batch.getProductId(), batch);
            } else if (batch.isArchived() == false
                    && batch.getLiveQuantity().doubleValue() > 0
                    && batch.getFinalUnitCost().doubleValue() > 0
                    && oldestBatch.getPurchasedDate() > batch.getPurchasedDate()) {
                oldestAvailBatchMap.put(batch.getProductId(), batch);
            }
        }

        ExciseTaxInfo exciseTaxInfo = cannabisTaxInfo; //set default to cannabis tax info

        for (OrderItem orderItem : cart.getItems()) {
            /*if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }*/

            Product product = productHashMap.get(orderItem.getProductId());


            if (product != null) {
                if (reset) {
                    orderItem.setTaxTable(null);
                    // set product tax table in order item tax table for override custom taxes
                    if (product.getTaxType() == TaxInfo.TaxType.Custom && !CollectionUtils.isNullOrEmpty(product.getTaxTables())) {
                        for (CompoundTaxTable pTaxTable : product.getTaxTables()) {
                            if (pTaxTable.getConsumerType() == customerInfo.getConsumerType() && pTaxTable.isActive()) {
                                orderItem.setTaxTable(pTaxTable);
                            }
                        }
                    }
                }
                double lineCityTax = 0d;
                double lineCountyTax = 0d;
                double lineStateTax = 0d;
                double lineFedTax = 0d;

                double costPerUnit = 0d;
                String batchId = orderItem.getBatchId();
                double rawQuantity = orderItem.getQuantity().doubleValue();
                if (StringUtils.isNotBlank(batchId)) {
                    // get batch
                    ProductBatch productBatch = allBatchMap.get(batchId);
                    if (productBatch != null) {
                        costPerUnit = productBatch.getFinalUnitCost().doubleValue();
                        orderItem.setBatchSku(productBatch.getSku());
                    }
                } else if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                    PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                    if (prepackageProductItem != null) {
                        ProductBatch productBatch = allBatchMap.get(batchId);
                        if (productBatch != null) {
                            costPerUnit = productBatch.getFinalUnitCost().doubleValue();
                        }

                        Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                        if (prepackage != null) {
                            BigDecimal unitValue = prepackage.getUnitValue();
                            if (unitValue == null || unitValue.doubleValue() == 0) {
                                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                unitValue = weightTolerance.getUnitValue();
                            }
                            if (unitValue != null) {
                                rawQuantity = orderItem.getQuantity().doubleValue() * unitValue.doubleValue();
                            }
                        }
                    }

                } else {
                    // get latest batch
                    ProductBatch oldestAvailBatch = oldestAvailBatchMap.get(product.getId());
                    if (oldestAvailBatch != null) {
                        costPerUnit = oldestAvailBatch.getFinalUnitCost().doubleValue();
                    }
                }
                double cogs = NumberUtils.round(costPerUnit * rawQuantity, 4);

                orderItem.setCogs(BigDecimal.valueOf(cogs));
                TaxResult orderItemTaxResult = orderItem.getTaxResult();
                if (orderItemTaxResult == null) {
                    orderItemTaxResult = new TaxResult();
                    orderItemTaxResult.prepare();
                }
                orderItemTaxResult.reset();
                orderItem.setTaxResult(orderItemTaxResult);

                double orderItemExciseTax = 0;
                boolean shouldCalcPreNALExcise = false;

                ProductCategory productCategory = categoryHashMap.get(product.getCategoryId());
                boolean isCannabis = product.isCannabisProduct(productCategory != null && productCategory.isCannabis());
                if (vendorMap != null) {
                    Vendor vendor = null; //vendorMap.get(product.getVendorId());

                    if (!shop.isRetail()) {
                        // if appTarget is Distirbution/Grow, use the customer info's armslength, etc
                        if (customerInfo != null) {
                            vendor = new Vendor();
                            vendor.prepare(shop.getCompanyId());
                            vendor.setArmsLengthType(customerInfo.getArmsLengthType());
                            vendor.setCompanyType(customerInfo.getCompanyType());
                        }
                    } else {
                        // Otherwise, if it's retail, use the vendor's arms length
                        vendor = vendorMap.get(product.getVendorId());
                    }
                    if (vendor != null) {
                        if (((cannabisTaxInfo != null
                                && shop.isEnableExciseTax())
                                || (shop.isGrow() && cultivationTaxInfo != null)
                                || (nonCannabisTaxInfo != null && shop.isEnableNonCannabisTax()))
                                && productCategory != null) {
                            if (shop.isGrow() && cultivationTaxInfo != null && customerInfo != null && customerInfo.isEnableCultivationTax()) {
                                CultivationTaxResult cultivationTaxResult = new CultivationTaxResult();
                                orderItemTaxResult.setCultivationTaxResult(cultivationTaxResult);
                                orderItem.setCalcCultivationTax(taxCalulationService.calculateEstimateCultivationTax(orderItem.getQuantity().doubleValue(), product, productCategory, cultivationTaxResult, cultivationTaxInfo, allBatchMap, weightToleranceHashMap, batchId));

                            } else {
                                boolean isEnableExciseTax = false;
                                if (isCannabis) {
                                    exciseTaxInfo = cannabisTaxInfo;
                                    isEnableExciseTax = shop.isEnableExciseTax();
                                }

                                if (isEnableExciseTax && exciseTaxInfo != null) {
                                    if (Vendor.ArmsLengthType.NON_ARMS_LENGTH.equals(vendor.getArmsLengthType())) {
                                        if (CompanyFeatures.AppTarget.Retail == shop.getAppTarget()) {
                                            // only calculate non-arms-length if it's retail
                                            if (!product.isPriceIncludesExcise()) {
                                                double discountToApply = propDiscountMap.getOrDefault(orderItem.getOrderItemId(), 0d);
                                                double subTotal = orderItem.getFinalPrice().doubleValue() - discountToApply;

                                                if (shop.isNalExciseFromRetailCost()) {
                                                    // use original retail cost regardless
                                                    subTotal = orderItem.getCost().doubleValue();
                                                }

                                                BigDecimal exciseTax = exciseTaxInfo.getExciseTax();
                                                orderItemExciseTax = (subTotal * exciseTax.doubleValue()) / 100;

                                                totalNALPOSTExciseTax += orderItemExciseTax;

                                                orderItemTaxResult.setTotalExciseTax(new BigDecimal(orderItemExciseTax));
                                                orderItem.setExciseTax(new BigDecimal(orderItemExciseTax));
                                            } else {
                                                shouldCalcPreNALExcise = true;
                                            }
                                        }
                                    } else {
                                        boolean isOverride = false;
                                        if (!shop.isRetail() && (orderItem.getOverridePrice() != null && orderItem.getOverridePrice().doubleValue() > 0)) {
                                            cogs = orderItem.getOverridePrice().multiply(orderItem.getQuantity()).doubleValue() - orderItem.getDiscount().doubleValue();
                                            cogs = NumberUtils.round(cogs, 4);
                                            isOverride = true;
                                        }

                                        double itemALExciseTax = (cogs * (1 + (exciseTaxInfo.getStateMarkUp().doubleValue() / 100))) * (exciseTaxInfo.getExciseTax().doubleValue() / 100);

                                        double discountToApply = propDiscountMap.getOrDefault(orderItem.getOrderItemId(), 0d);
                                        double finalPrice = orderItem.getFinalPrice().doubleValue() - discountToApply;

                                        if (!shop.isAlExciseOnZeroPrice()) {
                                            // if don't excise on zero price, then zero out item excise tax if final price = 0
                                            if (finalPrice <= 0) {
                                                itemALExciseTax = 0;
                                            }
                                        }


                                        orderItemTaxResult.setTotalALExciseTax(new BigDecimal(itemALExciseTax));
                                        if (!isOverride) {
                                            if (product.isPriceIncludesALExcise()) {
                                                totalALExciseTax += itemALExciseTax;
                                                orderItemTaxResult.setTotalALExciseTax(new BigDecimal(itemALExciseTax));
                                            } else {
                                                orderItemExciseTax += itemALExciseTax;
                                                totalALPOSTExciseTax += itemALExciseTax;
                                                orderItem.setExciseTax(new BigDecimal(orderItemExciseTax));
                                                orderItemTaxResult.setTotalALPostExciseTax(new BigDecimal(itemALExciseTax));
                                            }
                                        } else {
                                            orderItemExciseTax += itemALExciseTax;
                                            totalALPOSTExciseTax += itemALExciseTax;
                                            orderItem.setExciseTax(new BigDecimal(orderItemExciseTax));
                                            orderItemTaxResult.setTotalALPostExciseTax(new BigDecimal(itemALExciseTax));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Calculate Taxes
                if ((deliveryTaxRate != null && deliveryTaxRate.isUseComplexTax() && deliveryTaxRate.isActive()) || (isCannabis && shop.isUseComplexTax() && cart.getTaxTable() != null) || orderItem.getTaxTable() != null || (!isCannabis && shop.getNonCannabisTaxes() != null && shop.getNonCannabisTaxes().isUseComplexTax() && cart.getNonCannabisTaxTable() != null)) {
                    // Complex Taxes
                    orderItemTaxResult = calculateComplexPostTax(cart, product, reset, orderItem, exciseTaxInfo,
                            propDiscountMap, shouldCalcPreNALExcise, orderItemTaxResult, roundTaxDecimal, shop, isCannabis);
                } else {
                    // simple taxes
                    orderItemTaxResult = calculateSimpleTax(shop, product, orderItem, shouldCalcPreNALExcise, exciseTaxInfo, propDiscountMap, orderItemTaxResult, deliveryTaxRate, isCannabis);
                }


                // Set the Taxes
                totalPostTax += orderItemTaxResult.getTotalPostCalcTax().doubleValue();
                totalPreTax += orderItemTaxResult.getTotalPreCalcTax().doubleValue();

                totalCityTax += orderItemTaxResult.getTotalCityTax().doubleValue();
                totalCountyTax += orderItemTaxResult.getTotalCountyTax().doubleValue();
                totalStateTax += orderItemTaxResult.getTotalStateTax().doubleValue();
                totalFedTax += orderItemTaxResult.getTotalFedTax().doubleValue();

                totalCityPreTax += orderItemTaxResult.getTotalCityPreTax().doubleValue();
                totalCountyPreTax += orderItemTaxResult.getTotalCountyPreTax().doubleValue();
                totalStatePreTax += orderItemTaxResult.getTotalStatePreTax().doubleValue();
                totalFedPreTax += orderItemTaxResult.getTotalFedPreTax().doubleValue();

                // pre NAL excise Tax
                totalNALPreExciseTax += orderItemTaxResult.getTotalNALPreExciseTax().doubleValue();

                totalCultivationTax += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalCultivationTax().doubleValue() : 0;

                totalLeafTax += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalLeafTax().doubleValue() : 0;
                totalLeafOz += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalLeafOz().doubleValue() : 0;
                leafTaxOz += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getLeafTaxOz().doubleValue() : 0;

                totalPlantTax += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalPlantTax().doubleValue() : 0;
                totalPlantOz += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalPlantOz().doubleValue() : 0;
                plantTaxOz += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getPlantTaxOz().doubleValue() : 0;

                totalFlowerTax += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalFlowerTax().doubleValue() : 0;
                totalFlowerOz += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getTotalFlowerOz().doubleValue() : 0;
                flowerTaxOz += (orderItemTaxResult.getCultivationTaxResult() != null) ? orderItemTaxResult.getCultivationTaxResult().getFlowerTaxOz().doubleValue() : 0;

            }
        }

        totalPostTax += totalNALPOSTExciseTax + totalALPOSTExciseTax + totalCultivationTax;
        if (Shop.TaxRoundOffType.FIVE_CENT.equals(shop.getTaxRoundOffType()) && cart.getPaymentOption() == Cart.PaymentOption.Cash) {
            totalPostTax = NumberUtils.roundToNearestCent(totalPostTax);
        }

        TaxResult taxResult = new TaxResult();
        taxResult.prepare();
        taxResult.setTotalPreCalcTax(new BigDecimal(NumberUtils.round(totalPreTax, 2)));
        taxResult.setTotalPostCalcTax(new BigDecimal(NumberUtils.round(totalPostTax, 2)));
        taxResult.setTotalCityTax(new BigDecimal(NumberUtils.round(totalCityTax, 2)));
        taxResult.setTotalCountyTax(new BigDecimal(NumberUtils.round(totalCountyTax, 2)));
        taxResult.setTotalStateTax(new BigDecimal(NumberUtils.round(totalStateTax, 2)));
        taxResult.setTotalFedTax(new BigDecimal(NumberUtils.round(totalFedTax, 2)));

        taxResult.setTotalCityPreTax(new BigDecimal(NumberUtils.round(totalCityPreTax, 2)));
        taxResult.setTotalCountyPreTax(new BigDecimal(NumberUtils.round(totalCountyPreTax, 2)));
        taxResult.setTotalStatePreTax(new BigDecimal(NumberUtils.round(totalStatePreTax, 2)));
        taxResult.setTotalFedPreTax(new BigDecimal(NumberUtils.round(totalFedPreTax, 2)));

        taxResult.setTotalExciseTax(new BigDecimal(NumberUtils.round(totalNALPOSTExciseTax, 2)));
        taxResult.setTotalALExciseTax(new BigDecimal(NumberUtils.round(totalALExciseTax, 2)));
        taxResult.setTotalNALPreExciseTax(new BigDecimal(NumberUtils.round(totalNALPreExciseTax, 2)));
        taxResult.setTotalALPostExciseTax(new BigDecimal(NumberUtils.round(totalALPOSTExciseTax, 2)));

        // added for cultivation tax calculation
        CultivationTaxResult cultivationTaxResult = new CultivationTaxResult();
        taxResult.setCultivationTaxResult(cultivationTaxResult);

        cultivationTaxResult.setTotalCultivationTax(new BigDecimal(NumberUtils.round(totalCultivationTax, 2)));

        cultivationTaxResult.setTotalFlowerTax(new BigDecimal(NumberUtils.round(totalFlowerTax, 4)));
        cultivationTaxResult.setTotalFlowerOz(new BigDecimal(NumberUtils.round(totalFlowerOz, 4)));
        cultivationTaxResult.setFlowerTaxOz(new BigDecimal(NumberUtils.round(flowerTaxOz, 4)));

        cultivationTaxResult.setTotalLeafTax(new BigDecimal(NumberUtils.round(totalLeafTax, 4)));
        cultivationTaxResult.setTotalLeafOz(new BigDecimal(NumberUtils.round(totalLeafOz, 4)));
        cultivationTaxResult.setLeafTaxOz(new BigDecimal(NumberUtils.round(leafTaxOz, 4)));

        cultivationTaxResult.setTotalPlantTax(new BigDecimal(NumberUtils.round(totalPlantTax, 4)));
        cultivationTaxResult.setTotalPlantOz(new BigDecimal(NumberUtils.round(totalPlantOz, 4)));
        cultivationTaxResult.setPlantTaxOz(new BigDecimal(NumberUtils.round(plantTaxOz, 4)));

        return taxResult;
    }

    private TaxResult calculateComplexPostTax(Cart cart,
                                              Product product,
                                              boolean reset,
                                              OrderItem orderItem,
                                              ExciseTaxInfo exciseTaxInfo,
                                              HashMap<String, Double> propDiscountMap,
                                              boolean shouldCalcPreNALExcise,
                                              TaxResult orderItemTaxResult, int ROUND_TAX_DECIMAL, Shop shop, boolean isCannabis) {
        double lineTotalPostTax = 0d;

        // PreTax
        double lineTotalPreTax = 0d;
        double orderItemPreExcisTax = 0d;
        double orderItemExciseTax = 0d;

        // post nal excise tax
        if (orderItemTaxResult.getTotalExciseTax() != null) {
            orderItemExciseTax = orderItemTaxResult.getTotalExciseTax().doubleValue();
        }

        // post al excise tax
        if (orderItemTaxResult.getTotalALPostExciseTax() != null) {
            orderItemExciseTax += orderItemTaxResult.getTotalALPostExciseTax().doubleValue();
        }


        CompoundTaxTable cartTaxTable = cart.getTaxTable(); // default tax table
        if (shop.isEnableNonCannabisTax()) {
            if (isCannabis == false && cart.getNonCannabisTaxTable() != null) {
                cartTaxTable = cart.getNonCannabisTaxTable();
            }
        }


        CompoundTaxTable taxTable = orderItem.getTaxTable() != null ? orderItem.getTaxTable() : cartTaxTable; //Here we have already set delivery tax rates's tax table in prepare cart method (If necessary) so using cart's tax table
        if (taxTable == null) {
            return orderItemTaxResult;
        }
        ConsumerType consumerType = taxTable.getConsumerType();

        if (product.getTaxType() == TaxInfo.TaxType.Exempt) {
            return orderItemTaxResult;
        }

        // find the taxtable of the product & override accordingly
        if (reset) {
            // If product does not have tax tables then use shop's tax table if shop has complex tax enabled and tax table not blank else it will use cart's tax table
            List<CompoundTaxTable> productTaxTables = (CollectionUtils.isNullOrEmpty(product.getTaxTables()) && shop.isUseComplexTax() && !CollectionUtils.isNullOrEmpty(shop.getTaxTables())) ? shop.getTaxTables() : product.getTaxTables();
            if (!CollectionUtils.isNullOrEmpty(productTaxTables)) {
                if (product.getTaxType() == TaxInfo.TaxType.Custom) {
                    for (CompoundTaxTable pTaxTable : productTaxTables) {
                        if (pTaxTable.getConsumerType() == consumerType && pTaxTable.isActive()) {
                            taxTable = pTaxTable;
                        }
                    }
                }
            }
        }

        if (taxTable.getTaxType() == TaxInfo.TaxType.Exempt) {
            return orderItemTaxResult;
        }
        if (taxTable != null) {
            orderItem.setTaxTable(taxTable.copyTaxTable());
        }
        // set calc tax to 0, initials
        orderItem.setCalcTax(new BigDecimal(0));
        orderItem.setCalcPreTax(new BigDecimal(0));


        double discountToApply = propDiscountMap.getOrDefault(orderItem.getOrderItemId(), 0d);
        double subTotalWithTax = orderItem.getFinalPrice().doubleValue() - discountToApply;
        double costWithDiscount = subTotalWithTax;


        //**********PRETAX CALCULATION**************
        // calculate preTax
        List<CompoundTaxTable.ActiveTax> activeTaxes = taxTable.getActivePreTaxes();
        List<CompoundTaxTable.ActiveTax> reverseList = Lists.reverse(activeTaxes);
        double totalNonCompTaxes = 0;
        for (CompoundTaxTable.ActiveTax activeTax : reverseList) {
            if (!activeTax.compound) {
                totalNonCompTaxes += activeTax.taxRate.doubleValue();
            }
        }
        for (CompoundTaxTable.ActiveTax activeTax : reverseList) {
            double preCost = 0;
            double preTax = 0;
            if (activeTax.compound) {
                preCost = (subTotalWithTax - lineTotalPreTax) / (1 + activeTax.taxRate.doubleValue() / 100);
                if (preCost < 0) {
                    preCost = 0;
                }
                preTax = (subTotalWithTax - lineTotalPreTax) - preCost;

            } else {
                preCost = subTotalWithTax / (1 + totalNonCompTaxes / 100);
                if (preCost < 0) {
                    preCost = 0;
                }
                preTax = preCost * activeTax.taxRate.doubleValue() / 100;

            }

            lineTotalPreTax += preTax;

            switch(activeTax.territory) {
                case City:
                    orderItemTaxResult.setTotalCityPreTax(new BigDecimal(preTax));
                    break;
                case County:
                    orderItemTaxResult.setTotalCountyPreTax(new BigDecimal(preTax));
                    break;
                case State:
                    orderItemTaxResult.setTotalStatePreTax(new BigDecimal(preTax));
                    break;
                case Federal:
                    orderItemTaxResult.setTotalFedPreTax(new BigDecimal(preTax));
                    break;
            }

            orderItem.setCalcPreTax(new BigDecimal(lineTotalPreTax));
        }


        //NEED TO FIGURE OUT PRE-TAX ON PRICE THAT INCLUDES EXCISE
        if (shouldCalcPreNALExcise) {
            double preCost = ((subTotalWithTax - lineTotalPreTax) / (1 + (exciseTaxInfo.getExciseTax().doubleValue() / 100)));
            if (preCost < 0) {
                preCost = 0;
            }
            double preTax = ((subTotalWithTax - lineTotalPreTax) - preCost);
            orderItemPreExcisTax = preTax;
            //totalNALPreExciseTax += orderItemPreExcisTax;
            orderItemTaxResult.setTotalNALPreExciseTax(new BigDecimal(orderItemPreExcisTax));
        }


        //******POST TAX CALCULATION*******************
        // Calculate POST Taxes

        double subExciseTax = orderItemExciseTax;
        double curTax = 0d;
        double taxOnExciseTax = 0d;
        for (CompoundTaxRate curTaxRate : taxTable.getActivePostTaxes()) {
            if (curTaxRate.isActive()) {
                double productTax = 0d;
                double productExciseTax = 0d;
                if (curTaxRate.isCompound()) {
                    subTotalWithTax += NumberUtils.round(curTax, ROUND_TAX_DECIMAL);

                    // reset curTax
                    curTax = 0d;
                    productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                    curTax += productTax;


                    if (curTaxRate.isActiveExciseTax()) {
                        subExciseTax += NumberUtils.round(taxOnExciseTax, ROUND_TAX_DECIMAL);

                        taxOnExciseTax = 0d;
                        productExciseTax = subExciseTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                        taxOnExciseTax += productExciseTax;
                    }

                } else {
                    productTax = subTotalWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                    curTax += productTax;

                    if (curTaxRate.isActiveExciseTax()) {
                        productExciseTax = subExciseTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                        taxOnExciseTax += productExciseTax;
                    }
                }

                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.City) {
                    orderItemTaxResult.setTotalCityTax(new BigDecimal(productTax + productExciseTax));
                }

                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.County) {
                    orderItemTaxResult.setTotalCountyTax(new BigDecimal(productTax + productExciseTax));
                }

                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.State) {
                    orderItemTaxResult.setTotalStateTax(new BigDecimal(productTax + productExciseTax));
                }

                if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.Federal) {
                    orderItemTaxResult.setTotalFedTax(new BigDecimal(productTax + productExciseTax));
                }
            }
        }

        subTotalWithTax += NumberUtils.round(curTax, ROUND_TAX_DECIMAL);
        lineTotalPostTax = NumberUtils.round(subTotalWithTax, ROUND_TAX_DECIMAL) - costWithDiscount;
        lineTotalPostTax = NumberUtils.round(lineTotalPostTax, ROUND_TAX_DECIMAL);

        // exciseTax
        subExciseTax += NumberUtils.round(taxOnExciseTax, ROUND_TAX_DECIMAL);
        double lineTotalExciseTax = NumberUtils.round(subExciseTax, ROUND_TAX_DECIMAL) - orderItemExciseTax;
        if (lineTotalExciseTax < 0) {
            lineTotalExciseTax = 0;
        }


        lineTotalPostTax += NumberUtils.round(lineTotalExciseTax, ROUND_TAX_DECIMAL);

        orderItem.setExciseTax(new BigDecimal(lineTotalExciseTax));
        orderItem.setCalcPreTax(new BigDecimal(lineTotalPreTax));
        orderItem.setCalcTax(new BigDecimal(lineTotalPostTax));
        orderItemTaxResult.setTotalPreCalcTax(orderItem.getCalcPreTax());
        orderItemTaxResult.setTotalPostCalcTax(new BigDecimal(lineTotalPostTax));

        return orderItemTaxResult;
    }

    private TaxResult calculateSimpleTax(Shop shop,
                                         Product product,
                                         OrderItem orderItem,
                                         boolean shouldCalcPreNALExcise,
                                         ExciseTaxInfo exciseTaxInfo,
                                         HashMap<String, Double> propDiscountMap,
                                         TaxResult orderItemTaxResult, DeliveryTaxRate deliveryTaxRate,
                                         boolean isCannabis) {
        //SIMPLE TAX CALCULATION
        // First use the assigned tax/order from the shop
        TaxInfo taxInfo = null;
        TaxInfo.TaxProcessingOrder taxOrder = null;
        if (deliveryTaxRate != null && !deliveryTaxRate.isUseComplexTax() && deliveryTaxRate.getTaxInfo() != null && deliveryTaxRate.isActive()) {
            taxInfo = deliveryTaxRate.getTaxInfo();
        } else if (!isCannabis && shop.isEnableNonCannabisTax() && shop.getNonCannabisTaxes() != null) {
            taxInfo = shop.getNonCannabisTaxes().getTaxInfo();
            taxOrder = shop.getNonCannabisTaxes().getTaxOrder();
        } else {
            taxInfo = shop.getTaxInfo();
            taxOrder = shop.getTaxOrder();
        }

        // If it's custom, then assign the taxOrder and taxInfo
        if (product.getTaxType() == TaxInfo.TaxType.Custom && product.getCustomTaxInfo() != null) {
            taxInfo = product.getCustomTaxInfo();
            taxOrder = product.getTaxOrder();
        }

        // set tax info of the order for record keeping
        if (taxInfo != null) {
            taxInfo.setId(ObjectId.get().toString());
        }
        orderItem.setTaxInfo(taxInfo);
        orderItem.setTaxOrder(taxOrder);
        orderItem.setTaxType(product.getTaxType());

        // set calc tax to 0, initials
        orderItem.setCalcTax(new BigDecimal(0));
        orderItem.setCalcPreTax(new BigDecimal(0));

        if (product.getTaxType() == TaxInfo.TaxType.Exempt) {
            return orderItemTaxResult;
        }


        double lineTotalPostTax = 0d;

        // PreTax
        double lineTotalPreTax = 0d;
        double orderItemPreExcisTax = 0d;
        double orderItemExciseTax = orderItemTaxResult.getTotalExciseTax().doubleValue();


        // Calculate tax
        if (taxInfo != null) {
            double discountToApply = propDiscountMap.getOrDefault(orderItem.getOrderItemId(), 0d);

            double costWithDiscount = orderItem.getFinalPrice().doubleValue() - discountToApply;

            if (costWithDiscount < 0) {
                costWithDiscount = 0;
            }
            if (taxOrder == TaxInfo.TaxProcessingOrder.PostTaxed) {

                costWithDiscount += orderItemExciseTax;

                double productTax = 0d;
                if (taxInfo.getCityTax().doubleValue() != 0) {
                    double lineCityTax = costWithDiscount * taxInfo.getCityTax().doubleValue();
                    productTax += lineCityTax;
                    orderItemTaxResult.setTotalCityTax(new BigDecimal(lineCityTax));
                }

                if (taxInfo.getFederalTax().doubleValue() != 0) {
                    double lineFedTax = costWithDiscount * taxInfo.getFederalTax().doubleValue();
                    productTax += lineFedTax;
                    orderItemTaxResult.setTotalFedTax(new BigDecimal(lineFedTax));
                }

                if (taxInfo.getStateTax().doubleValue() != 0) {
                    double lineStateTax = costWithDiscount * taxInfo.getStateTax().doubleValue();
                    productTax += lineStateTax;
                    orderItemTaxResult.setTotalStateTax(new BigDecimal(lineStateTax));
                }

                orderItem.setCalcTax(new BigDecimal(productTax));
                orderItem.setCalcPreTax(new BigDecimal(0));
                lineTotalPostTax = productTax;

                orderItemTaxResult.setTotalPostCalcTax(new BigDecimal(productTax));


                // calculate preTax for excise tax
                if (shouldCalcPreNALExcise) {
                    double preCost = (costWithDiscount / (1 + (exciseTaxInfo.getExciseTax().doubleValue() / 100)));
                    if (preCost < 0) {
                        preCost = 0;
                    }
                    double preTax = (costWithDiscount - preCost);
                    orderItemPreExcisTax = preTax;

                    orderItemTaxResult.setTotalNALPreExciseTax(new BigDecimal(orderItemPreExcisTax));
                }

            } else {
                // calculate pre-taxed
                // Formula
                // post-cost = pre-cost * (1 + tax)
                // post-cost / (1 + tax) = pre-cost
                double linePreTax = 0;
                if (taxInfo.getTotalTax().doubleValue() > 0) {
                    //Check this
                    double preCost = (costWithDiscount / (1 + taxInfo.getTotalTax().doubleValue()));
                    if (preCost < 0) {
                        preCost = 0;
                    }
                    linePreTax = (costWithDiscount - preCost);

                    if (taxInfo.getCityTax().doubleValue() != 0 && taxInfo.getTotalTax().doubleValue() != 0) {
                        double lineCityTax = this.calculateRegularPretax(taxInfo.getCityTax().doubleValue(), taxInfo.getTotalTax().doubleValue(), linePreTax);
                        orderItemTaxResult.setTotalCityPreTax(new BigDecimal(lineCityTax));
                    }

                    if (taxInfo.getFederalTax().doubleValue() != 0 && taxInfo.getTotalTax().doubleValue() != 0) {
                        double lineFedTax = this.calculateRegularPretax(taxInfo.getFederalTax().doubleValue(), taxInfo.getTotalTax().doubleValue(), linePreTax);
                        orderItemTaxResult.setTotalFedPreTax(new BigDecimal(lineFedTax));
                    }

                    if (taxInfo.getStateTax().doubleValue() != 0 && taxInfo.getTotalTax().doubleValue() != 0) {
                        double lineStateTax = this.calculateRegularPretax(taxInfo.getStateTax().doubleValue(), taxInfo.getTotalTax().doubleValue(), linePreTax);
                        orderItemTaxResult.setTotalStatePreTax(new BigDecimal(lineStateTax));
                    }


                    orderItem.setCalcTax(new BigDecimal(0));
                    orderItem.setCalcPreTax(new BigDecimal(linePreTax));
                    orderItemTaxResult.setTotalPreCalcTax(new BigDecimal(linePreTax));
                }
                lineTotalPreTax = linePreTax;
                // calculate preTax for excise tax
                if (shouldCalcPreNALExcise) {
                    double preCost = ((costWithDiscount - linePreTax) / (1 + (exciseTaxInfo.getExciseTax().doubleValue() / 100)));
                    if (preCost < 0) {
                        preCost = 0;
                    }
                    double preTax = ((costWithDiscount - linePreTax) - preCost);
                    orderItemPreExcisTax = preTax;

                    orderItemTaxResult.setTotalNALPreExciseTax(new BigDecimal(orderItemPreExcisTax));
                }
            }
        }

        orderItem.setCalcPreTax(new BigDecimal(lineTotalPreTax));
        orderItem.setCalcTax(new BigDecimal(lineTotalPostTax));
        orderItemTaxResult.setTotalPostCalcTax(new BigDecimal(lineTotalPostTax));

        return orderItemTaxResult;
    }

    private DeliveryTaxRate getDeliveryTaxRateByZipCode(Shop shop, Transaction trans, CustomerInfo customerInfo) {
        Member member = memberRepository.get(shop.getCompanyId(), trans.getMemberId());
        String zipCode = "";
        if (member == null && customerInfo != null && StringUtils.isNotBlank(customerInfo.getZipCode())) {
            zipCode = customerInfo.getZipCode();
        } else if (member != null && member.getAddress() != null && StringUtils.isNotBlank(member.getAddress().getZipCode())) {
            zipCode = member.getAddress().getZipCode();
        }

        DeliveryTaxRate taxRate = null;
        if (StringUtils.isNotBlank(zipCode)) {
            List<DeliveryTaxRate> deliveryTaxRates = deliveryTaxRateRepository.listAllByShopId(shop.getCompanyId(), shop.getId(), "{created : -1}", DeliveryTaxRate.class);

            Set<ObjectId> regionIds = new HashSet<>();
            for (DeliveryTaxRate taxRateResult : deliveryTaxRates) {
                if (taxRateResult.getRegionIds() == null) {
                    continue;
                }
                for (String regionId : taxRateResult.getRegionIds()) {
                    regionIds.add(new ObjectId(regionId));
                }
            }

            HashMap<String, BlazeRegion> regionMap = blazeRegionRepository.listAsMap(shop.getCompanyId(), new ArrayList<>(regionIds));
            for (DeliveryTaxRate taxRateResult : deliveryTaxRates) {
                if (!taxRateResult.isActive() && taxRateResult.getRegionIds() == null) {
                    continue;
                }
                for (String regionId : taxRateResult.getRegionIds()) {
                    BlazeRegion blazeRegion = regionMap.get(regionId);
                    if (blazeRegion != null && blazeRegion.getZipCodes().contains(zipCode)) {
                        taxRate = taxRateResult;
                        break;
                    }
                }
                if (taxRate != null) {
                    break;
                }
            }
            return taxRate;
        }
        return null;
    }

    private double calculateRegularPretax(double tax, double totalTax, double preTax) {
        return (preTax / totalTax) * tax;
    }

    private boolean mayHaveDeliveryFees(final Shop shop, Transaction trans) {

        if (shop != null
                && trans.getQueueType() == Transaction.QueueType.Delivery
                && trans.getTransType() != Transaction.TransactionType.Refund) {
            List<DeliveryFee> deliveryFees = shop.getDeliveryFees();
            // Make sure it's in order
            deliveryFees.sort(new DeliveryFee.DeliveryFeeComparator());

            for (DeliveryFee fee : deliveryFees) {
                boolean enabled = fee.isEnabled();
                if (enabled) {
                    return true;
                }
            }
        }

        return false;
    }

    private Cart applyDeliveryFee(final Shop shop, Transaction trans, CustomerInfo customerInfo, double subTotalAfterTax) {
        Cart cart = trans.getCart();
        if (cart != null) {
            cart.setDeliveryFee(new BigDecimal(0));
            cart.setEnableDeliveryFee(false);
            if (shop != null
                    && trans.getQueueType() == Transaction.QueueType.Delivery
                    && trans.getTransType() != Transaction.TransactionType.Refund) {
                boolean enableDeliveryFee = false;
                // Always start with 0

                // Set Appropriate delivery Fee
                BigDecimal subTotal = cart.getSubTotalDiscount();
                if (subTotal == null) {
                    subTotal = new BigDecimal(0);
                }
                List<DeliveryFee> deliveryFees = shop.getDeliveryFees();
                // Make sure it's in order
                deliveryFees.sort(new DeliveryFee.DeliveryFeeComparator());

                Member member = memberRepository.get(shop.getCompanyId(), trans.getMemberId());

                String zipCode = "";
                if (customerInfo != null && StringUtils.isNotBlank(customerInfo.getZipCode())) {
                    zipCode = customerInfo.getZipCode();
                } else if (member != null && member.getAddress() != null && StringUtils.isNotBlank(member.getAddress().getZipCode())) {
                    zipCode = member.getAddress().getZipCode();
                }

                DeliveryFee deliveryFee = null;
                //Get delivery by zip code if available
                if (StringUtils.isNotBlank(zipCode)) {
                    for (DeliveryFee fee : deliveryFees) {
                        if (fee.isEnabled()
                                && DeliveryFee.DeliveryFeeType.ZIP_Code == fee.getFeeType()
                                && fee.getZipCodes() != null
                                && fee.getZipCodes().contains(zipCode)) {
                            deliveryFee = fee;
                        }
                    }
                }

                //Get delivery fee by subtotal if not available by zip code
                if (deliveryFee == null) {
                    for (DeliveryFee fee : deliveryFees) {
                        if (subTotal.compareTo(fee.getSubTotalThreshold()) >= 0
                                && fee.isEnabled()
                                && DeliveryFee.DeliveryFeeType.CART_SUB_TOTAL == fee.getFeeType()) {
                            deliveryFee = fee;
                        }
                    }
                }

                if (deliveryFee != null) {
                    enableDeliveryFee = Boolean.TRUE;
                    if (deliveryFee.getType() == OrderItem.DiscountType.Percentage) {
                        double fee = subTotalAfterTax * deliveryFee.getFee().doubleValue() / 100;
                        cart.setDeliveryFee(BigDecimal.valueOf(fee));
                    } else {
                        cart.setDeliveryFee(deliveryFee.getFee());
                    }
                }

                cart.setEnableDeliveryFee(enableDeliveryFee);

                // apply discount if necessary
                if (cart.getDeliveryDiscount().doubleValue() > 0) {
                    double deliveryDiscount = cart.getDeliveryDiscount().doubleValue();
                    if (cart.getDeliveryDiscountType() == OrderItem.DiscountType.Percentage) {
                        deliveryDiscount = cart.getDeliveryFee().doubleValue() * (deliveryDiscount / 100);
                        deliveryDiscount = NumberUtils.round(deliveryDiscount, 2);
                    }
                    double newDeliveryFee = cart.getDeliveryFee().doubleValue() - deliveryDiscount;

                    if (StringUtils.isNotBlank(cart.getDeliveryPromotionId())) {
                        cart.getPromotionReqLogs().add(this.createPromotionLog(cart.getDeliveryPromotionId(), null, trans.getCompanyId(), deliveryDiscount));
                    }

                    newDeliveryFee = NumberUtils.round(newDeliveryFee, 2);
                    if (newDeliveryFee < 0) {
                        newDeliveryFee = 0;
                    }
                    cart.setDeliveryFee(new BigDecimal(newDeliveryFee));
                }

            }
        }
        return cart;
    }

    private void applyCreditCardFee(Shop shop, Transaction trans, double subTotalAfterTax) {
        Cart cart = trans.getCart();

        if (cart != null
                && cart.getPaymentOption() != null
                && (cart.getPaymentOption() == Cart.PaymentOption.Credit
                || (cart.getPaymentOption() == Cart.PaymentOption.Split
                && trans.getCart().getSplitPayment() != null && trans.getCart().getSplitPayment().getCreditDebitAmt().doubleValue() > 0))
                && !CollectionUtils.isNullOrEmpty(shop.getCreditCardFeeList())
                && trans.getTransType() != Transaction.TransactionType.Refund) {

            BigDecimal subTotal = cart.getSubTotalDiscount();
            if (subTotal == null) {
                subTotal = new BigDecimal(0);
            }

            List<CreditCardFee> creditCardFeeList = shop.getCreditCardFeeList();
            //Sort delivery fees
            creditCardFeeList.sort(new CreditCardFee.CreditCardFeeDescendingComparator());

            for (CreditCardFee cardFee : creditCardFeeList) {
                BigDecimal fee = cardFee.getSubTotalThreshold() != null ? cardFee.getSubTotalThreshold() : new BigDecimal(0);
                BigDecimal ccFee = cardFee.getFee() != null ? cardFee.getFee() : new BigDecimal(0);
                if (cardFee.isEnabled() && subTotal.compareTo(fee) >= 0 && cardFee.getType() == OrderItem.DiscountType.Cash) {
                    cart.setCreditCardFee(ccFee);
                    cart.setEnableCreditCardFee(Boolean.TRUE);
                    break;
                } else if (cardFee.isEnabled() && cardFee.getFee().doubleValue() > 0 && cardFee.getType() == OrderItem.DiscountType.Percentage) {
                    fee = BigDecimal.valueOf(ccFee.doubleValue() * subTotalAfterTax / 100);
                    cart.setCreditCardFee(fee);
                    cart.setEnableCreditCardFee(Boolean.TRUE);
                    break;
                }
            }

        } else {
            cart.setCreditCardFee(new BigDecimal(0));
            cart.setEnableCreditCardFee(Boolean.FALSE);
        }
    }

    @Override
    public List<MixMatchGroup> buildMixMatchStructure(final List<LinkedOrderItem> orderItems,
                                                      final HashMap<String, Product> productHashMap,
                                                      final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                      final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                                      final HashMap<String, Prepackage> prepackageHashMap,
                                                      final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap) {

        //New Algorithm:
        //1. Group everything based on pricing. Only add items that are whole in List A.
        //2. Add all items to be list to be processed. (List A). Should only contain whole pricing items.
        //3. Sort items in List A from low to high (qty)
        //4. While all ListA is not empty. Copy all items to in-processed list (List B). Clear List A.
        //5. For all items in List B, add i1+i2+..+in. If sum is whole (pricing), then done. Add ListB to result list.
        //6. If not, send i1 to List A.
        //7. Repeat 3-6 until List A is fully empty. Result should contain a list of list of items -- should contain final whole pricing

        HashMap<String, List<MixMatchOrder>> itemsMap = new HashMap<>();

        for (OrderItem orderItem : orderItems) {
            Product p = productHashMap.get(orderItem.getProductId());
            if (p != null) {
                String mainKey = null;
                // mix and match -- disabled for price breaks for now
                if (p.isEnableMixMatch()
                        && (p.getPriceRanges().size() > 0 || p.getPriceBreaks().size() > 0)
                        && orderItem.isIgnoreMixMatch() == false) {
                    mainKey = getProductPriceKey(p, memberGroupPricesHashMap, toleranceHashMap);
                } else {
                    mainKey = orderItem.getOrderItemId();
                }
                List<MixMatchOrder> mapItems = itemsMap.get(mainKey);
                if (mapItems == null) {
                    mapItems = new ArrayList<>();
                    itemsMap.put(mainKey, mapItems);
                }

                double orderQuantity = orderItem.getQuantity().doubleValue();

                if (StringUtils.isNotEmpty(orderItem.getPrepackageItemId())
                        && ObjectId.isValid(orderItem.getPrepackageItemId())) {
                    PrepackageProductItem prepackageItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                    if (prepackageItem != null) {
                        Prepackage prepackage = prepackageHashMap.get(prepackageItem.getPrepackageId());

                        // Quantity is orderItem.getQuantity() * prepackage.unitValue
                        orderQuantity = orderItem.getQuantity().doubleValue() * prepackage.getUnitValue().doubleValue();
                    }
                }

                MixMatchOrder mixMatchOrder = new MixMatchOrder();
                ProductPriceRange productPriceRange = getPriceRange(orderQuantity, p, toleranceHashMap, memberGroupPricesHashMap);
                ProductPriceBreak productPriceBreak = getPriceBreak(orderQuantity, p, memberGroupPricesHashMap);
                mixMatchOrder.orderItem = orderItem;
                mixMatchOrder.priceRange = productPriceRange;
                mixMatchOrder.priceBreak = productPriceBreak;
                mixMatchOrder.product = p;
                mixMatchOrder.wholeValue = orderQuantity;
                mixMatchOrder.realQuantity = orderQuantity;
                mixMatchOrder.key = mainKey;

                mixMatchOrder.whole = false;

                if (productPriceRange != null && productPriceRange.getWeightTolerance() != null) {
                    double startWeight = NumberUtils.round(productPriceRange.getWeightTolerance().getStartWeight().doubleValue(), 2);
                    double endWeight = NumberUtils.round(productPriceRange.getWeightTolerance().getEndWeight().doubleValue(), 2);
                    double qty = orderQuantity;
                    if (qty >= startWeight &&
                            qty <= endWeight) {
                        // Whole
                        mixMatchOrder.whole = true;
                        mixMatchOrder.wholeValue = productPriceRange.getWeightTolerance().getWeightValue().doubleValue();
                    }
                } else if (productPriceBreak != null) {
                    if (orderQuantity == productPriceBreak.getQuantity()) {
                        mixMatchOrder.whole = true;
                        mixMatchOrder.wholeValue = (double) productPriceBreak.getQuantity();
                    }
                }

                mapItems.add(mixMatchOrder);

            }
        }

        List<MixMatchGroup> result = new ArrayList<>();
        //System.out.println(itemsMap);
        Comparator<MixMatchOrder> comparator = (o1, o2) -> o1.realQuantity.compareTo(o2.realQuantity);

        /*
            New Mix&Match algorithm, add all all quantity of the items together. If amount slowly deduct the smallest one
         */
        for (List<MixMatchOrder> items : itemsMap.values()) {
            List<MixMatchOrder> pendingItems = new ArrayList<>();

            List<MixMatchOrder> workItems = Lists.newArrayList(items);
            workItems.sort(comparator); // sort the algorithm from smallest to largest

            int tries = 0;
            while (tries < items.size() * 2) {
                pendingItems.clear();
                while (workItems.size() > 0) {
                    // sum qty
                    double targetQty = workItems.stream().mapToDouble(value -> value.realQuantity).sum();
                    MixMatchOrder o = workItems.get(0);
                    ProductPriceRange productPriceRange = getPriceRange(targetQty, o.product, toleranceHashMap, memberGroupPricesHashMap);
                    ProductPriceBreak productPriceBreak = getPriceBreak(targetQty, o.product, memberGroupPricesHashMap);
                    if (productPriceRange != null && productPriceRange.getWeightTolerance() != null) {

                        double startWeight = NumberUtils.round(productPriceRange.getWeightTolerance().getStartWeight().doubleValue(), 2);
                        double endWeight = NumberUtils.round(productPriceRange.getWeightTolerance().getEndWeight().doubleValue(), 2);
                        if (targetQty >= startWeight &&
                                targetQty <= endWeight) {
                            // remove all, we're done
                            MixMatchGroup group = new MixMatchGroup();
                            group.key = o.key;
                            group.groupValue = productPriceRange.getWeightTolerance().getWeightValue().doubleValue();
                            group.groupPriceRange = productPriceRange;
                            group.totalQuantity = targetQty;
                            group.orders = Lists.newArrayList(workItems);
                            result.add(group);

                            // found one; continue
                            workItems.clear();
                            break;
                        }
                    } else if (productPriceBreak != null) {
                        if (targetQty == productPriceBreak.getQuantity()) {
                            MixMatchGroup group = new MixMatchGroup();
                            group.key = o.key;
                            group.groupValue = (double) productPriceBreak.getQuantity();
                            group.groupPriceBreak = productPriceBreak;
                            group.totalQuantity = targetQty;
                            group.orders = Lists.newArrayList(workItems);
                            result.add(group);

                            workItems.clear();
                            break;
                        }
                    }

                    //We got to this point meaning there's no match, let's remove the last one and check again
                    MixMatchOrder pendingOrder = workItems.remove(0);
                    pendingItems.add(pendingOrder);
                }

                workItems.addAll(pendingItems);
                tries++;
            }

            LOG.info("Pending count: " + pendingItems.size());
            for (MixMatchOrder mixMatchOrder : pendingItems) {
                // any pending items will be it's own mixmatch that's non-mixmatched
                MixMatchGroup group = new MixMatchGroup();
                group.key = mixMatchOrder.key;
                if (mixMatchOrder.priceRange != null) {
                    group.groupValue = mixMatchOrder.priceRange.getWeightTolerance().getWeightValue().doubleValue();
                } else if (mixMatchOrder.priceBreak != null) {
                    group.groupValue = (double) mixMatchOrder.priceBreak.getQuantity();
                } else {
                    group.groupValue = mixMatchOrder.wholeValue;
                }

                group.groupPriceRange = mixMatchOrder.priceRange;
                group.groupPriceBreak = mixMatchOrder.priceBreak;
                group.totalQuantity = mixMatchOrder.realQuantity;
                group.orders = Lists.newArrayList(mixMatchOrder);
                result.add(group);
            }
        }

        // This is the result
        return result;
    }

    public HashMap<String, MixMatchItem> getMixMatchItemMap(final List<LinkedOrderItem> orderItems,
                                                            final HashMap<String, Product> productHashMap,
                                                            final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                            final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                                            final HashMap<String, Prepackage> prepackageHashMap,
                                                            final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap) {
        List<MixMatchGroup> groups = buildMixMatchStructure(orderItems, productHashMap, toleranceHashMap, prepackageProductItemHashMap, prepackageHashMap, memberGroupPricesHashMap);
        HashMap<String, MixMatchItem> itemHashMap = new HashMap<>();

        for (MixMatchGroup group : groups) {
            boolean matched = group.orders.size() > 1;
            for (MixMatchOrder order : group.orders) {
                MixMatchItem item = new MixMatchItem();
                item.orderItem = order.orderItem;
                item.totalQuantity = group.totalQuantity;
                item.itemQuantity = order.realQuantity;
                item.key = order.key;
                item.groupPriceRange = group.groupPriceRange;
                item.groupPriceBreak = group.groupPriceBreak;
                item.product = order.product;
                item.matched = matched;
                item.itemPriceRange = order.priceRange;
                item.itemPriceBreak = order.priceBreak;
                itemHashMap.put(order.orderItem.getOrderItemId(), item);
            }
        }

        return itemHashMap;
    }

    private String getProductPriceKey(Product product,
                                      final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap,
                                      final HashMap<String, ProductWeightTolerance> toleranceHashMap) {
        StringBuilder keyBuilder = new StringBuilder(product.getCategoryId());
        List<ProductPriceRange> priceRanges = Lists.newArrayList(product.getPriceRanges());
        List<ProductPriceBreak> priceBreaks = Lists.newArrayList(product.getPriceBreaks());

        MemberGroupPrices memberGroupPrices = memberGroupPricesHashMap.get(product.getId());
        if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
            priceRanges = Lists.newArrayList(memberGroupPrices.getPriceRanges());
            priceBreaks = Lists.newArrayList(memberGroupPrices.getPriceBreaks());
        }

        if (priceRanges != null && priceRanges.size() > 0) {
            priceRanges.removeIf(new Predicate<ProductPriceRange>() {
                @Override
                public boolean test(ProductPriceRange productPriceRange) {
                    ProductWeightTolerance tolerance = toleranceHashMap.get(productPriceRange.getWeightToleranceId());
                    return tolerance == null || tolerance.isEnabled() == false || tolerance.isDeleted();
                }
            });

            priceRanges.sort(new Comparator<ProductPriceRange>() {
                @Override
                public int compare(ProductPriceRange o1, ProductPriceRange o2) {
                    Integer one = new Integer(o1.getPriority());
                    Integer two = new Integer(o2.getPriority());
                    return one.compareTo(two);
                }
            });
            for (ProductPriceRange range : priceRanges) {
                keyBuilder.append("_" + range.getPrice());
            }
        } else {
            // This is a Unit based product
            // to make sure same weightType
            keyBuilder.append("_").append(product.getWeightPerUnit());
            priceBreaks.removeIf(new Predicate<ProductPriceBreak>() {
                @Override
                public boolean test(ProductPriceBreak productPriceBreak) {
                    return !productPriceBreak.isActive();
                }
            });

            priceBreaks.sort(new Comparator<ProductPriceBreak>() {
                @Override
                public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                    Integer one = new Integer(o1.getPriority());
                    Integer two = new Integer(o2.getPriority());
                    return one.compareTo(two);
                }
            });

            for (ProductPriceBreak priceBreak : priceBreaks) {
                keyBuilder.append("_").append(priceBreak.getPrice());
            }

        }

        return keyBuilder.toString();
    }

    private ProductPriceRange getPriceRange(double quantity,
                                            Product product,
                                            final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                            final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap) {
        if (product.getPriceRanges() == null) {
            return  null;
        }
        List<ProductPriceRange> priceRanges = Lists.newArrayList(product.getPriceRanges());

        MemberGroupPrices memberGroupPrices = memberGroupPricesHashMap.get(product.getId());
        if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
            priceRanges = Lists.newArrayList(memberGroupPrices.getPriceRanges());
        }

        priceRanges.sort(new Comparator<ProductPriceRange>() {
            @Override
            public int compare(ProductPriceRange o1, ProductPriceRange o2) {
                Integer one = new Integer(o1.getPriority());
                Integer two = new Integer(o2.getPriority());
                return one.compareTo(two) * -1;
            }
        });

        for (ProductPriceRange productPriceRange : priceRanges) {
            ProductWeightTolerance tolerance = toleranceHashMap.get(productPriceRange.getWeightToleranceId());
            if (tolerance != null) {
                double startWeight = NumberUtils.round(tolerance.getStartWeight().doubleValue(), 2);
                if (quantity >= startWeight) {
                    return productPriceRange;
                }
            }
        }

        if (priceRanges.size() > 0) {
            // get last item
            return priceRanges.get(priceRanges.size() - 1);
        }
        return null;
    }

    private ProductPriceBreak getPriceBreak(double quantity, Product product, final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap) {
        if (product.getPriceBreaks() == null) {
            return  null;
        }
        List<ProductPriceBreak> priceBreaks = Lists.newArrayList(product.getPriceBreaks());

        MemberGroupPrices memberGroupPrices = memberGroupPricesHashMap.get(product.getId());
        if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
            priceBreaks = Lists.newArrayList(memberGroupPrices.getPriceBreaks());
        }

        // Removing inactive breaks
        priceBreaks.removeIf(new Predicate<ProductPriceBreak>() {
            @Override
            public boolean test(ProductPriceBreak productPriceBreak) {
                return !productPriceBreak.isActive();
            }
        });

        priceBreaks.sort(new Comparator<ProductPriceBreak>() {
            @Override
            public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                Integer one = new Integer(o1.getPriority());
                Integer two = new Integer(o2.getPriority());
                return one.compareTo(two) * -1;
            }
        });

        for (ProductPriceBreak priceBreak : priceBreaks) {

            if (priceBreak.isActive() && quantity == priceBreak.getQuantity()) {
                return priceBreak;
            }
        }

        if (priceBreaks.size() > 0) {
            // get last item
            return priceBreaks.get(priceBreaks.size() - 1);
        }
        return null;
    }

    //Set finalizedDiscountId to promotions/rewards that hasn't been finalized yet
    private void finalizePromotionReqs(Cart workingCart, Shop shop){
        //Array with finalized promotions/rewards ids
        List<PromotionReq> promotionReqs = new ArrayList<>();
        for (OrderItem item : workingCart.getItems()) {
            if(item.isFinalized()) {
                //Add order item promotion reqs to array
                for (PromotionReq promotionReq : item.getPromotionReqs()) {
                    promotionReqs.add(promotionReq);
                }
            }
        }

        if (!CollectionUtils.isNullOrEmpty(promotionReqs)){
            for (PromotionReq promotionReq : workingCart.getPromotionReqs()) {
                if (promotionReq.getFinalizedDiscountId() == null) {
                    //Get First Matching PromotionReq by PromotionId or RewardId
                    PromotionReq promotionReqResult = promotionReqs.stream().filter(
                        p ->
                            (p.getPromotionId() != null && p.getPromotionId().equals(promotionReq.getPromotionId())) ||
                            (p.getRewardId() != null && p.getRewardId().equals(promotionReq.getRewardId()))
                        ).findFirst().orElse(null);
                    if (promotionReqResult!=null){
                        if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                            DiscountVersion discountVersion = discountVersionRepository.findLastDiscountVersionByPromotionId(shop.getCompanyId(), shop.getId(), promotionReq.getPromotionId());
                            if (discountVersion == null) {
                                Promotion promotion = promotionRepository.get(shop.getCompanyId(), promotionReq.getPromotionId());

                                discountVersion = new DiscountVersion(promotion);
                                discountVersion = discountVersionRepository.save(discountVersion);
                            }
                            promotionReq.setFinalizedDiscountId(discountVersion.getId());
                        } else if (StringUtils.isNotBlank(promotionReq.getRewardId())){
                            DiscountVersion discountVersion = discountVersionRepository.findLastDiscountVersionByRewardId(shop.getCompanyId(), shop.getId(), promotionReq.getRewardId());
                            if (discountVersion == null) {
                                LoyaltyReward reward = rewardRepository.get(shop.getCompanyId(), promotionReq.getRewardId());
                                discountVersion = new DiscountVersion(reward);
                                discountVersion = discountVersionRepository.save(discountVersion);
                            }
                            promotionReq.setFinalizedDiscountId(discountVersion.getId());
                        }
                    }else {
                        //Promotion req belongs to unfinalized order item
                        promotionReq.setFinalizedDiscountId(null);
                    }
                }
            }
        }
    }


    public Cart processEntireCartPricingPromos(final Shop shop,
                                               final Cart myCart,
                                               final boolean reset,
                                               final boolean assignProducts,
                                               final MemberGroup memberGroup,
                                               final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap,
                                               final HashMap<String, Product> productHashMap,
                                               final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                               final HashMap<String, Prepackage> prepackageHashMap,
                                               final HashMap<String, ProductCategory> categoryHashMap,
                                               final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                               final Transaction transaction,
                                               final Transaction.TransactionStatus incomingStatus,
                                               final CustomerInfo customerInfo) {
        Kryo kryo = new Kryo();
        Cart workingCart = kryo.copy(myCart);


        // add promotion if needed
        if (StringUtils.isNotBlank(workingCart.getPromoCode())) {
            boolean found = false;
            Promotion promotionWithCode = promotionProcessService.findPromotionWithCode(shop.getCompanyId(), shop.getId(), workingCart.getPromoCode());
            LOG.info("Found promoCode: " + workingCart.getPromoCode() + " obj: " + promotionWithCode);


            if (promotionWithCode != null && promotionWithCode.isConsumerFacing()) {
                for (PromotionReq promotionReq : workingCart.getPromotionReqs()) {
                    if (StringUtils.isNotBlank(promotionReq.getPromotionId()) && promotionReq.getPromotionId().equalsIgnoreCase(promotionWithCode.getId())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    // add this promotion to the promotion requests
                    LOG.info("promotion added..");
                    workingCart.getPromotionReqs().add(new PromotionReq(promotionWithCode.getId(), null, promotionWithCode.getFinalizedDiscountId()));
                }
            } else {
                workingCart.setPromoCode(Strings.EMPTY);
            }
            /*
            if(promotionWithCode == null){
                throw new BlazeInvalidArgException("Promotion","Promotion code does not exist.");
            }

            if(!promotionWithCode.isConsumerFacing()){
                throw new BlazeInvalidArgException("Promotion","Applied promo code: "+promotionWithCode.getName()+" is not for consumer user.");
            }else  {
                for (PromotionReq promotionReq : workingCart.getPromotionReqs()) {
                    if (promotionReq.getPromotionId().equalsIgnoreCase(promotionWithCode.getId())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    // add this promotion to the promotion requests
                    LOG.info("promotion added..");
                    workingCart.getPromotionReqs().add(new PromotionReq(promotionWithCode.getId(),null));
                }
            }(/*/
        }


        /**
         * STEP ONE: CLEAR PREVIOUS PROMOTION PROCESSES
         */
        // subtract previous logs
        double discount = workingCart.getDiscount() != null ? workingCart.getDiscount().doubleValue() : 0;

        for (PromotionReqLog promotionReqLog : workingCart.getPromotionReqLogs()) {
            discount -= promotionReqLog.getAmount().doubleValue();
            for (PromotionReq promotionReq : workingCart.getPromotionReqs()) {
                if (StringUtils.isNotBlank(promotionReq.getPromotionId()) && promotionReq.getPromotionId().equalsIgnoreCase(promotionReqLog.getPromotionId())) {
                    promotionReq.setAssigned(true);
                }
                if (StringUtils.isNotBlank(promotionReq.getRewardId()) && promotionReq.getRewardId().equalsIgnoreCase(promotionReqLog.getRewardId())) {
                    promotionReq.setAssigned(true);
                }
            }
        }
        finalizePromotionReqs(workingCart,shop);


        if (discount < 0) {
            discount = 0;
        }
        workingCart.getPromotionReqLogs().clear();
        workingCart.setDeliveryPromotionId(null);
        workingCart.setDiscount(new BigDecimal(discount));
        workingCart.setCartDiscountNotes(myCart.getCartDiscountNotes());
        workingCart.setPointSpent(new BigDecimal(0));

        workingCart.setDeliveryDiscount(new BigDecimal(0));
        workingCart.setDeliveryDiscountType(OrderItem.DiscountType.Cash);


        // rest any previous promotions
        if (workingCart.getPromotionReqs().size() > 0) {
            // reset accordingly
            if (transaction.getTransType() != Transaction.TransactionType.Refund
                    && transaction.getCart().getRefundOption() != Cart.RefundOption.Retail) {
                workingCart.setDiscount(new BigDecimal(0));
                workingCart.setCalcCartDiscount(new BigDecimal(0));
                workingCart.setTotalDiscount(new BigDecimal(0));
                workingCart.setDiscountType(OrderItem.DiscountType.Cash);
            }
        }

        // reset working cart
        for (OrderItem orderItem : workingCart.getItems()) {
            orderItem.setMixMatched(false); // if it's mixmatch once, then the whole thing is mixmatched
            /*if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }*/

            Product product = productHashMap.get(orderItem.getProductId());
            if (product == null || !product.isActive()) {
                if (transaction.getTransType() != Transaction.TransactionType.Refund) {
                    throw new BlazeInvalidArgException("Product", "Product is no longer available");
                }
            }

            if (shop.isDisableMedicalProductsForAdultUse() && (transaction.getTransType() != Transaction.TransactionType.Refund) &&
                    ConsumerType.AdultUse == customerInfo.getConsumerType() &&
                    (product != null && Product.ProductSaleType.Medicinal == product.getProductSaleType())) {
                throw new BlazeInvalidArgException("Product", "This is a medicinal only product.");
            }

            if (orderItem.getPromotionReqs().size() > 0) {
                orderItem.getPromotionReqs().clear();
                orderItem.setDiscount(new BigDecimal(0));
                orderItem.setDiscountType(OrderItem.DiscountType.Cash);
            }

            // reset the discounted and active qty
            orderItem.setOrigQuantity(orderItem.getQuantity());
            orderItem.setDiscountedQty(new BigDecimal(0));
            orderItem.setCalcDiscount(new BigDecimal(0));
            orderItem.setFinalPrice(orderItem.getCost());
            orderItem.setAvailableDiscountQty(orderItem.getQuantity());
        }

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(shop.getCompanyId(), shop.getId());
        Iterable<ProductWeightTolerance> tolerances = weightToleranceRepository.list(shop.getCompanyId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = new HashMap<>();
        for (ProductWeightTolerance tolerance : tolerances) {
            weightToleranceHashMap.put(tolerance.getWeightKey().weightName, tolerance);
        }

        if (Transaction.TransactionType.Refund == transaction.getTransType()) {
            Kryo kryoCart = new Kryo();
            Cart cart = kryoCart.copy(workingCart);
            Iterator<PromotionReq> reqIterator = workingCart.getPromotionReqs().iterator();
            while (reqIterator.hasNext()) {
                PromotionReq promotionReq = reqIterator.next();
                LinkedHashSet<PromotionReq> promotionReqs = new LinkedHashSet<>();
                promotionReqs.add(promotionReq);
                cart.setPromotionReqs(promotionReqs);
                try {
                    this.processPromotion(shop, cart, reset, assignProducts, memberGroup, memberGroupPricesHashMap, productHashMap,
                            prepackageProductItemHashMap,
                            prepackageHashMap,
                            categoryHashMap,
                            toleranceHashMap,
                            transaction, incomingStatus, productCategoryHashMap, weightToleranceHashMap);
                } catch (Exception e) {
                    if (e instanceof BlazeInvalidArgException) {
                        String promoId = StringUtils.isNotEmpty(promotionReq.getPromotionId()) ? promotionReq.getPromotionId() : promotionReq.getRewardId();
                        LOG.error("Promotion (" + promoId + ") could not applied for refund transaction :" + transaction.getId() + " " + e.getMessage());
                        reqIterator.remove();
                    }
                }
            }
        }

        this.processPromotion(shop, workingCart, reset, assignProducts, memberGroup, memberGroupPricesHashMap, productHashMap,
                prepackageProductItemHashMap,
                prepackageHashMap,
                categoryHashMap,
                toleranceHashMap,
                transaction, incomingStatus, productCategoryHashMap, weightToleranceHashMap);

        List<OrderItem> origOrderItems = Lists.newArrayList(workingCart.getItems());

        origOrderItems = collapseOrderItems(origOrderItems, workingCart, assignProducts, memberGroup, memberGroupPricesHashMap,
                productHashMap,
                prepackageProductItemHashMap,
                prepackageHashMap,
                categoryHashMap,
                toleranceHashMap);

        workingCart.setItems(origOrderItems);
        return workingCart;
    }

    private void processPromotion(Shop shop, Cart workingCart, boolean reset, boolean assignProducts, MemberGroup memberGroup, HashMap<String, MemberGroupPrices> memberGroupPricesHashMap, HashMap<String, Product> productHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemHashMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, ProductCategory> categoryHashMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, Transaction transaction, Transaction.TransactionStatus incomingStatus, HashMap<String, ProductCategory> productCategoryHashMap, HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        List<OrderedDiscountable> orderedDiscountables = new ArrayList<>();

        Set<ObjectId> promotionIds = new HashSet<>();
        Set<ObjectId> rewardIds = new HashSet<>();
        for (PromotionReq promotionReq : workingCart.getPromotionReqs()) {
            if (StringUtils.isNotBlank(promotionReq.getPromotionId()) && ObjectId.isValid(promotionReq.getPromotionId())) {
                promotionIds.add(new ObjectId(promotionReq.getPromotionId()));
            } else if (StringUtils.isNotBlank(promotionReq.getRewardId()) && ObjectId.isValid(promotionReq.getRewardId())) {
                rewardIds.add(new ObjectId(promotionReq.getRewardId()));
            }
        }

        Map<String, Promotion> promotionMap = promotionRepository.listAsMap(shop.getCompanyId(), Lists.newArrayList(promotionIds));
        Map<String, LoyaltyReward> rewardMap = rewardRepository.listAsMap(shop.getCompanyId(), Lists.newArrayList(rewardIds));

        for (PromotionReq promotionReq : workingCart.getPromotionReqs()) {
            promotionReq.prepare(shop.getCompanyId());
            boolean assigned = promotionReq.isAssigned();

            //Get old version of promotion or reward from discount versions
            if (StringUtils.isNotBlank(promotionReq.getFinalizedDiscountId())) {
                DiscountVersion discountVersion = discountVersionRepository.get(shop.getCompanyId(), promotionReq.getFinalizedDiscountId());
                if(StringUtils.isNotBlank(discountVersion.getPromotionId())) {
                    Promotion promotion = discountVersion.getPromotion();
                    promotion.setAssigned(assigned);
                    promotion.setFinalizedDiscountId(promotionReq.getFinalizedDiscountId());
                    orderedDiscountables.add(promotion);
                } else if(StringUtils.isNotBlank(discountVersion.getRewardId())) {
                    LoyaltyReward reward = discountVersion.getReward();
                    reward.setAssigned(assigned);
                    reward.setFinalizedDiscountId(promotionReq.getFinalizedDiscountId());
                    orderedDiscountables.add(reward);
                }

            } else if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                Promotion promotion = promotionMap.get(promotionReq.getPromotionId());
                promotion.setAssigned(assigned);
                orderedDiscountables.add(promotion);
            } else if (StringUtils.isNotBlank(promotionReq.getRewardId())) {
                LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                reward.setAssigned(assigned);
                // Check if Reward is Out of Date
                /*if (reward.getEndDate() != null && reward.getEndDate() <= DateTime.now().getMillis()) {
                    if (transaction.getTransType() != Transaction.TransactionType.Refund) {
                        throw new BlazeInvalidArgException("Loyalty Reward", "Reward Expired"); // if Loyalty Reward is Expired
                    }
                }*/
                if (reward.getStartDate() != null && reward.getStartDate() >= DateTime.now().getMillis()) {
                    if (transaction.getTransType() != Transaction.TransactionType.Refund) {
                        throw new BlazeInvalidArgException("Loyalty Reward", "Reward will be applicable from " + new Date(reward.getStartDate()));
                    }
                }


                if (reward != null && StringUtils.isNotBlank(reward.getPromotionId())) {
                    Promotion promotion = promotionRepository.get(shop.getCompanyId(), reward.getPromotionId());
                    promotion.setAssigned(assigned);
                    orderedDiscountables.add(promotion);
                } else {
                    orderedDiscountables.add(reward);
                }
            }
        }

        final Member member = memberRepository.get(shop.getCompanyId(), transaction.getMemberId());

        this.processDiscountable(shop, workingCart, reset, assignProducts, memberGroup, memberGroupPricesHashMap, productHashMap,
                prepackageProductItemHashMap,
                prepackageHashMap,
                categoryHashMap,
                toleranceHashMap,
                transaction,
                incomingStatus,
                prepackageHashMap,
                weightToleranceHashMap,
                productCategoryHashMap,
                member,
                orderedDiscountables);

    }

    private void processDiscountable(Shop shop, Cart workingCart, boolean reset, boolean assignProducts, MemberGroup memberGroup, HashMap<String, MemberGroupPrices> memberGroupPricesHashMap, HashMap<String, Product> productHashMap, HashMap<String, PrepackageProductItem> prepackageProductItemHashMap, HashMap<String, Prepackage> prepackageHashMap, HashMap<String, ProductCategory> categoryHashMap, HashMap<String, ProductWeightTolerance> toleranceHashMap, Transaction transaction, Transaction.TransactionStatus incomingStatus, HashMap<String, Prepackage> hashMap, HashMap<String, ProductWeightTolerance> weightToleranceHashMap, HashMap<String, ProductCategory> productCategoryHashMap, Member member, List<OrderedDiscountable> orderedDiscountables) {

        // order the orderedDiscountable
        orderedDiscountables.sort(new OrderedDiscountable.OrderedDiscountableComparator());
        LinkedHashSet<PromotionReq> sortedReqs = new LinkedHashSet<>();
        for (OrderedDiscountable ds : orderedDiscountables) {
            PromotionReq req = new PromotionReq();
            req.prepare(shop.getCompanyId());
            if (ds instanceof LoyaltyReward) {
                LoyaltyReward reward = (LoyaltyReward) ds;
                String id = reward.getId();
                boolean assigned = reward.isAssigned();
                String finalizedDiscountId = reward.getFinalizedDiscountId();

                req.setRewardId(id);
                req.setAssigned(assigned);
                req.setFinalizedDiscountId(finalizedDiscountId);
            } else if (ds instanceof Promotion) {
                Promotion promotion = (Promotion) ds;
                String id = promotion.getId();
                boolean assigned = promotion.isAssigned();
                String finalizedDiscountId = promotion.getFinalizedDiscountId();

                req.setPromotionId(id);
                req.setAssigned(assigned);
                req.setFinalizedDiscountId(finalizedDiscountId);
            }

            sortedReqs.add(req);
        }
        workingCart.setPromotionReqs(sortedReqs);

        OrderedDiscountable discountable = null;
        // Apply promotions after cost has been calculated for each product

        do {

            workingCart = processPricing(shop, workingCart, reset, assignProducts, memberGroup, memberGroupPricesHashMap, productHashMap,
                    prepackageProductItemHashMap,
                    prepackageHashMap,
                    categoryHashMap,
                    toleranceHashMap,
                    transaction);

            discountable = null;
            if (orderedDiscountables.size() > 0) {
                discountable = orderedDiscountables.remove(0);
            }

            if (discountable != null) {
                promotionProcessService.applyPromo(shop, transaction, workingCart, productHashMap, incomingStatus, productCategoryHashMap, weightToleranceHashMap, discountable, member);
            }

        } while (discountable != null);
    }


    private List<OrderItem> collapseOrderItems(List<OrderItem> origOrderItems,
                                               Cart workingCart,
                                               final boolean assignProducts,
                                               final MemberGroup memberGroup,
                                               final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap,
                                               final HashMap<String, Product> productHashMap,
                                               final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                               final HashMap<String, Prepackage> prepackageHashMap,
                                               final HashMap<String, ProductCategory> categoryHashMap,
                                               final HashMap<String, ProductWeightTolerance> toleranceHashMap) {
        // now set the pricing
        for (OrderItem orderItem : origOrderItems) {
            Product product = productHashMap.get(orderItem.getProductId());
            if (product == null) {
                continue;
            }


            // assign the dependencies
            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem == null ? null : prepackageProductItem.getPrepackageId());
            if (assignProducts) {
                orderItem.setProduct(product);
                orderItem.setPrepackage(prepackage);
                orderItem.setPrepackageProductItem(prepackageProductItem);
                if (product != null) {
                    product.setCategory(categoryHashMap.get(product.getCategoryId()));
                }
            } else {
                orderItem.setProduct(null);
                orderItem.setPrepackage(null);
                orderItem.setPrepackageProductItem(null);
            }


            double cost = 0;
            double discount = 0;
            for (OrderItem linkedOrderItem : workingCart.getItems()) {

                if (linkedOrderItem instanceof LinkedOrderItem) {
                    LinkedOrderItem oItem = (LinkedOrderItem) linkedOrderItem;
                    if (oItem.getOrigOrderItemId().equalsIgnoreCase(orderItem.getId())) {
                        cost += linkedOrderItem.getCost().doubleValue();
                        discount += linkedOrderItem.getDiscount().doubleValue();
                        if (linkedOrderItem.isMixMatched()) {
                            orderItem.setMixMatched(true); // if it's mixmatch once, then the whole thing is mixmatched
                        }
                    }
                } else {
                    if (linkedOrderItem.getId().equalsIgnoreCase(orderItem.getId())) {
                        cost += linkedOrderItem.getCost().doubleValue();
                        discount += linkedOrderItem.getDiscount().doubleValue();
                        if (linkedOrderItem.isMixMatched()) {
                            orderItem.setMixMatched(true); // if it's mixmatch once, then the whole thing is mixmatched
                        }
                    }
                }
            }
            double unitPrice = 0;
            orderItem.setDiscount(new BigDecimal(discount));
            orderItem.setDiscountNotes(orderItem.getDiscountNotes());

            if (orderItem.getOrigQuantity().doubleValue() > 0) {
                if (orderItem.getOverridePrice() != null) {
                    cost = orderItem.getOrigQuantity().doubleValue() * orderItem.getOverridePrice().doubleValue();
                    unitPrice = orderItem.getOverridePrice().doubleValue();
                } else {
                    unitPrice = cost / orderItem.getOrigQuantity().doubleValue();
                }

            }


            orderItem.setCost(new BigDecimal(cost)); //NumberUtils.round(cost, 2));
            orderItem.setUnitPrice(new BigDecimal(unitPrice)); //NumberUtils.round(unitPrice,2));;
        }
        return origOrderItems;
    }


    @Override
    public PromotionReqLog createPromotionLog(String promotionId, String finalizedDiscountId, String companyId, double discountCashAmt) {
        return promotionProcessService.createPromotionLog(promotionId, finalizedDiscountId, companyId, discountCashAmt);
    }


    @Override
    public Transaction createRefundTransaction(Shop shop, Transaction dbTrans, RefundTransactionRequest request, String sellerTerminalId, String sellerId) {

        HashMap<String, PromotionReq> promoMap = new HashMap<>();
        HashMap<String, BigDecimal> refundAmtMap = new HashMap<>();

        if (request == null) {
            throw new BlazeInvalidArgException("Refund", "Refund request is missing.");
        }
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("Refund", "Transaction is missing");
        }

        if (dbTrans.isActive()) {
            throw new BlazeInvalidArgException("Refund", "Cannot refund active transction.");
        }


        Transaction refundTransaction = createRefundTransactionHelper(shop.getCompanyId(), shop.getId(), dbTrans, request, sellerTerminalId, sellerId);

        refundTransaction.setProcessedTime(DateTime.now().getMillis());
        refundTransaction.setCompletedTime(DateTime.now().getMillis());
        // FIND AND GET ITEMS THAT NEED TO BE REFUNDED
        List<OrderItem> refundedItems = getRefundedItems(refundTransaction, request, promoMap, refundAmtMap);

        // set refund items
        refundTransaction.getCart().setItems(refundedItems);

        // Now deal with the promotions -- remove any unnecessary promotions
        reprocessPromotions(refundTransaction, promoMap);

        assignRefundAmt(dbTrans, refundAmtMap);


        // if there are no refund items, just return
        if (refundedItems.size() == 0) {
            throw new BlazeInvalidArgException("Refund", "Nothing to refund.");
        }

        // save new transaction
        // prepare again after a refund
        final CustomerInfo customerInfo = getMemberGroup(shop.getCompanyId(), refundTransaction.getMemberId());
        //cartService.prepareCart(shop, refundTransaction, true, newStatus, false, customerInfo, false);
        this.prepareCartForRefund(shop, refundTransaction, true, refundTransaction.getStatus(), false, customerInfo, dbTrans);

        this.processQuantityLogForRefundTransaction(refundTransaction);

        //BR-1431: Remove delivery fee and credit card fee in refund transaction
        this.applyCreditCardFee(shop, refundTransaction, refundTransaction.getCart().getSubTotal().doubleValue());
        this.applyDeliveryFee(shop, refundTransaction, null, refundTransaction.getCart().getSubTotal().doubleValue());

        return refundTransaction;
    }

    // Update quantity logs for refund transaction
    private void processQuantityLogForRefundTransaction(Transaction refundTransaction) {
        List<OrderItem> items = refundTransaction.getCart().getItems();
        for (OrderItem item : items) {
            List<QuantityLog> refundedQuantityLogs = new ArrayList<>();
            double quantity = item.getQuantity().doubleValue();
            for (QuantityLog quantityLog : item.getQuantityLogs()) {
                QuantityLog copyLog = new Kryo().copy(quantityLog);
                double qty = quantityLog.getQuantity().doubleValue();
                if (qty < quantity) {
                    copyLog.setQuantity(new BigDecimal(qty));
                    quantity = quantity - qty;
                } else {
                    copyLog.setQuantity(new BigDecimal(quantity));
                    quantity = 0.0;
                }
                refundedQuantityLogs.add(copyLog);
                if (quantity == 0.0) {
                    break;
                }
            }
            item.getQuantityLogs().clear();
            item.setQuantityLogs(refundedQuantityLogs);
        }
    }

    private CustomerInfo getMemberGroup(final String companyId, final String memberId) {
        Member member = memberRepository.get(companyId, memberId);
        CustomerInfo customerInfo = new CustomerInfo();
        if (member != null) {
            MemberGroup group = memberGroupRepository.get(companyId, member.getMemberGroupId());
            customerInfo.setMemberGroup(group);
            customerInfo.setConsumerType(member.getConsumerType());
        }
        customerInfo.setPatient(member);
        return customerInfo;
    }

    //Update cart discount based on order item
    private void processRefundCartDiscount(Transaction dbTrans, List<OrderItem> refundItems, Transaction refundTransaction) {
        if (refundItems == null || refundItems.isEmpty()) {
            return;
        }
        double cartDiscount = dbTrans.getCart().getCalcCartDiscount().doubleValue();
        if (cartDiscount == 0) {
            return;
        }

        double dbSubTotal = 0d;
        for (OrderItem origOrderItem : dbTrans.getCart().getItems()) {
            dbSubTotal += NumberUtils.round(origOrderItem.getCost().doubleValue(), 6);
        }


        double refundedSubTotal = 0d;
        for (OrderItem orderItem : refundTransaction.getCart().getItems()) {
            refundedSubTotal += NumberUtils.round(orderItem.getOverridePrice().multiply(orderItem.getQuantity()).doubleValue(), 6);
        }
        //refundTransaction.getCart().getSubTotal().doubleValue();

        double ratio = (refundedSubTotal / dbSubTotal);
        double refundDiscount = ratio * cartDiscount;

        refundTransaction.getCart().setDiscount(BigDecimal.valueOf(refundDiscount));
        refundTransaction.getCart().setDiscountType(OrderItem.DiscountType.Cash);
        if (dbTrans.getCart().getAfterTaxDiscount().doubleValue() > 0) {
            double afterTaxDiscount = ratio * dbTrans.getCart().getAppliedAfterTaxDiscount().doubleValue();
            afterTaxDiscount = NumberUtils.round(afterTaxDiscount, 4);
            refundTransaction.getCart().setAfterTaxDiscount(new BigDecimal(afterTaxDiscount));
        }
    }

    private Transaction createRefundTransactionHelper(String companyId, String shopId,
                                                      Transaction dbTrans,
                                                      RefundTransactionRequest request,
                                                      String sellerTerminalId, String sellerId) {

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(companyId, shopId, "TransactionPriority");

        Transaction.TransactionStatus newStatus = request.isWithInventory() ? Transaction.TransactionStatus.RefundWithInventory : Transaction.TransactionStatus.RefundWithoutInventory;


        Kryo kryo = new Kryo();
        Transaction refundTransaction = kryo.copy(dbTrans);
        refundTransaction.setId(ObjectId.get().toString());
        refundTransaction.setTransNo("" + sequence.getCount());
        refundTransaction.setActive(false);
        refundTransaction.setParentTransactionId(dbTrans.getId());
        refundTransaction.setCreated(DateTime.now().getMillis());
        refundTransaction.getCart().setCreated(DateTime.now().getMillis());
        refundTransaction.setTerminalId(sellerTerminalId);
        refundTransaction.setSellerId(sellerId);
        refundTransaction.setSellerTerminalId(sellerTerminalId);
        refundTransaction.setRefundVersion(request.getRefundVersion());
        refundTransaction.getCart().setId(ObjectId.get().toString());

        if (request.getRefundType().equals(RefundTransactionRequest.RefundType.Exchange)) {
            refundTransaction.setTransType(Transaction.TransactionType.Exchange);
        } else {
            refundTransaction.setTransType(Transaction.TransactionType.Refund);
            if (refundTransaction.getCart() != null) {
                refundTransaction.getCart().setRefundOption(Cart.RefundOption.Retail);
            }
        }
        refundTransaction.setLoc(refundTransaction.getLoc());
        refundTransaction.setTimeZone(dbTrans.getTimeZone());

        refundTransaction.setStatus(newStatus);

        Note note = new Note();
        note.setId(ObjectId.get().toString());
        note.setMessage(request.getNote());
        refundTransaction.setNote(note);

        refundTransaction.getCart().setDeliveryFee(new BigDecimal(0));
        refundTransaction.getCart().setRequestRefundAmt(request.getTotalRefundAmt());

        // Set PaymentOption to use selected RefundAs value passed in request
        refundTransaction.getCart().setPaymentOption(request.getRefundAs());

        return refundTransaction;
    }

    private void assignRefundAmt(Transaction dbTrans,
                                 final HashMap<String, BigDecimal> refundAmtMapRef) {
        for (OrderItem orderItem : dbTrans.getCart().getItems()) {
            BigDecimal refundAmt = refundAmtMapRef.get(orderItem.getOrderItemId());
            if (refundAmt != null) {
                if (orderItem.getTotalRefundQty() != null) {
                    BigDecimal totalRefundAmt = orderItem.getTotalRefundQty().add(refundAmt);
                    orderItem.setTotalRefundQty(totalRefundAmt);
                } else {
                    orderItem.setTotalRefundQty(refundAmt);
                }
            }
        }
    }

    private void reprocessPromotions(Transaction refundTransaction,
                                     final HashMap<String, PromotionReq> promoMapRef) {
        if (refundTransaction.getCart().getPromotionReqs().size() > 0) {
            refundTransaction.getCart().getPromotionReqs().removeIf(promotionReq -> {
                if (StringUtils.isNotBlank(promotionReq.getPromotionId())) {
                    return !promoMapRef.containsKey(promotionReq.getPromotionId());
                } else if (StringUtils.isNotBlank(promotionReq.getRewardId())) {
                    return !promoMapRef.containsKey(promotionReq.getRewardId());
                }
                return false;
            });
            refundTransaction.getCart().getPromotionReqLogs().clear();
        }
    }

    private List<OrderItem> getRefundedItems(Transaction refundTransaction,
                                             final RefundTransactionRequest request,
                                             HashMap<String, PromotionReq> promoMapRef,
                                             HashMap<String, BigDecimal> refundAmtMapRef) {

        List<OrderItem> refundedItems = new ArrayList<>();

        for (OrderItem orderItem : refundTransaction.getCart().getItems()) {


            if (request.getRefundOrders() != null && request.getRefundOrders().size() > 0) {
                //OLD IMPLEMENTATION;
                // DEPRECATED
                for (String orderItemId : request.getRefundOrders()) {
                    if (orderItemId.equalsIgnoreCase(orderItem.getOrderItemId())) {
                        orderItem.setParentId(orderItem.getId());
                        refundedItems.add(orderItem);
                        orderItem.resetPrepare(refundTransaction.getCompanyId());
                        orderItem.setOrderItemId(UUID.randomUUID().toString());
                        refundAmtMapRef.put(orderItemId, orderItem.getQuantity());
                        if (orderItem.getPromotionReqs() != null) {
                            for (PromotionReq log : orderItem.getPromotionReqs()) {
                                if (StringUtils.isNotBlank(log.getPromotionId())) {
                                    promoMapRef.put(log.getPromotionId(), log);
                                } else {
                                    promoMapRef.put(log.getRewardId(), log);
                                }
                            }
                        }
                        break;
                    }
                }
            } else {
                //NEW SOLUTION
                for (RefundOrderItemRequest refundItemRequest : request.getRefundOrderItemRequests()) {
                    if (refundItemRequest.getOrderItemId().equalsIgnoreCase(orderItem.getOrderItemId())) {
                        if (orderItem.getTotalRefundQty() != null) {
                            if (refundItemRequest.getQuantity().doubleValue()
                                    <= (orderItem.getQuantity().doubleValue() - orderItem.getTotalRefundQty().doubleValue())) {
                                orderItem.setQuantity(refundItemRequest.getQuantity());
                            }
                        } else {
                            orderItem.setQuantity(refundItemRequest.getQuantity());
                        }
                        orderItem.setParentId(orderItem.getId());
                        refundedItems.add(orderItem);
                        refundAmtMapRef.put(orderItem.getOrderItemId(), orderItem.getQuantity());

                        orderItem.setOrigOrderItemId(orderItem.getOrderItemId());

                        orderItem.resetPrepare(refundTransaction.getCompanyId());
                        orderItem.setOrderItemId(UUID.randomUUID().toString());

                        // set the total refund amt
                        orderItem.setTotalRefundAmount(refundItemRequest.getRefundAmt());
                        orderItem.setTotalRefundQty(refundItemRequest.getQuantity());

                        if (orderItem.getPromotionReqs() != null) {
                            for (PromotionReq log : orderItem.getPromotionReqs()) {
                                if (StringUtils.isNotBlank(log.getPromotionId())) {
                                    promoMapRef.put(log.getPromotionId(), log);
                                } else {
                                    promoMapRef.put(log.getRewardId(), log);
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        return refundedItems;
    }

    // Helper Methods
    private Cart processPricing(final Shop shop,
                                Cart workingCart,
                                final boolean reset,
                                final boolean assignProducts,
                                final MemberGroup memberGroup,
                                final HashMap<String, MemberGroupPrices> memberGroupPricesHashMap,
                                final HashMap<String, Product> productHashMap,
                                final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                final HashMap<String, Prepackage> prepackageHashMap,
                                final HashMap<String, ProductCategory> categoryHashMap,
                                final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                final Transaction transaction) {


        // Preprocess the order items
        for (OrderItem orderItem : workingCart.getItems()) {
            if (orderItem.getId() == null || reset) {
                orderItem.setId(ObjectId.get().toString());
                orderItem.setCreated(DateTime.now().getMillis());
            }
            orderItem.setCompanyId(shop.getCompanyId());
            orderItem.setModified(DateTime.now().getMillis());
            if (StringUtils.isBlank(orderItem.getOrderItemId())) {
                orderItem.setOrderItemId(UUID.randomUUID().toString());
            }
        }


        Company company = companyRepository.getById(shop.getCompanyId());
        LOG.info(String.format("Company: %s, Pricing Opt: %s", company.getName(), company.getPricingOpt()));
        List<LinkedOrderItem> linkedOrderItems = createLinkedOrderItems(company.getPricingOpt(),
                workingCart,
                prepackageProductItemHashMap,
                prepackageHashMap,
                productHashMap,
                categoryHashMap,
                toleranceHashMap,
                transaction);
        HashMap<String, CartService.MixMatchItem> mixMatchItemHashMap = this.getMixMatchItemMap(linkedOrderItems,
                productHashMap,
                toleranceHashMap,
                prepackageProductItemHashMap,
                prepackageHashMap,
                memberGroupPricesHashMap);

        // Calculate pricing based on linkedOrderItems
        for (LinkedOrderItem linkedOrderItem : linkedOrderItems) {
            if (linkedOrderItem.getId() == null || reset) {
                linkedOrderItem.setId(ObjectId.get().toString());
                linkedOrderItem.setCreated(DateTime.now().getMillis());
            }
            linkedOrderItem.setCompanyId(shop.getCompanyId());
            linkedOrderItem.setModified(DateTime.now().getMillis());
            if (StringUtils.isBlank(linkedOrderItem.getOrderItemId())) {
                linkedOrderItem.setOrderItemId(UUID.randomUUID().toString());
            }

            Product product;
            if (linkedOrderItem.isFinalized() && linkedOrderItem.getFinalizedProductId()!=null) {
                String productVersionId = linkedOrderItem.getFinalizedProductId();
                ProductVersion productVersion = productVersionRepository.get(shop.getCompanyId(), productVersionId);
                product = productHashMap.get(linkedOrderItem.getProductId());
                product.setProductVersion(productVersion);
            } else {
                product = productHashMap.get(linkedOrderItem.getProductId());
            }

            if (product == null) {
                continue;
            }

            double quantity = linkedOrderItem.getQuantity() != null ? linkedOrderItem.getQuantity().doubleValue() : 0;
            if (quantity < 0) {
                quantity = 0;
            }


            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(linkedOrderItem.getPrepackageItemId());
            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem == null ? null : prepackageProductItem.getPrepackageId());


            // Calculate Cost;
            // find Price Range
            // 1. If quantity falls between the tolerance, then use the tolerance price
            //      Otherwise, use orderItem.cost = quantity * (tolerance.price * tolerance.unitValue) * quantity
            // 2. If tolerance == null,
            //    then, cost = quantity * product.unitPrice

            // Check MixMatchPricing
            ProductPriceRange productPriceRange;
            ProductPriceBreak productPriceBreak = null;
            CartService.MixMatchItem mixMatchItem = null;


            // Check to see if there's a corresponding member pricing
            List<ProductPriceRange> priceRanges = product.getPriceRanges();
            MemberGroupPrices memberGroupPrices = null;
            if (memberGroup != null) {
                memberGroupPrices = memberGroupPricesHashMap.get(product.getId());
                if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
                    if (product.getPriceRanges().size() > 0) {
                        priceRanges = memberGroupPrices.getPriceRanges();
                    }
                }
            }

            List<ProductPriceBreak> priceBreaks = product.getPriceBreaks();
            if (memberGroup != null) {
                memberGroupPrices = memberGroupPricesHashMap.get(product.getId());
                if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
                    if (product.getPriceBreaks().size() > 0) {
                        priceBreaks = memberGroupPrices.getPriceBreaks();
                    }
                }
            }

            BigDecimal productPrice = product.getUnitPriceWithPriceBreaks(linkedOrderItem.getQuantity().intValue(), memberGroupPrices, linkedOrderItem.getOverridePrice());

            // Set the product price
            if (prepackage != null) {
                productPrice = prepackage.getPrice();
                if (!prepackage.isCustomWeight()) {
                    // Find PR
                    for (ProductPriceRange pr : priceRanges) {
                        if (pr.getWeightToleranceId().equalsIgnoreCase(prepackage.getToleranceId())) {
                            productPrice = pr.getAssignedPrice();
                            prepackage.setPrice(pr.getPrice());
                            ProductWeightTolerance tolerance = toleranceHashMap.get(prepackage.getToleranceId());
                            if (tolerance != null) {
                                prepackage.setName(tolerance.getName());
                                prepackage.setUnitValue(tolerance.getUnitValue());
                            }
                            break;
                        }
                    }
                }
                linkedOrderItem.setMixMatched(false);
            }


            boolean canPrepackageMixMatch = prepackage == null || (prepackage != null && !prepackage.isCustomWeight());
            if (product.isEnableMixMatch() && (priceRanges.size() > 0 || priceBreaks.size() > 0) && canPrepackageMixMatch) {

                mixMatchItem = mixMatchItemHashMap.get(linkedOrderItem.getOrderItemId());
                productPriceRange = mixMatchItem.groupPriceRange;
                productPriceBreak = mixMatchItem.groupPriceBreak;

                if (priceRanges.size() > 0 && productPriceRange == null) {
                    productPriceRange = mixMatchItem.itemPriceRange;
                } else if (priceBreaks.size() > 0 && productPriceBreak == null) {
                    productPriceBreak = mixMatchItem.itemPriceBreak;
                }

                linkedOrderItem.setMixMatched(mixMatchItem.matched);
            } else {
                ProductCategory category = categoryHashMap.get(product.getCategoryId());
                if (category.getUnitType() == ProductCategory.UnitType.grams) {
                    productPriceRange = getPriceRange(quantity, priceRanges, toleranceHashMap);
                } else {
                    productPriceRange = null;
                }
                linkedOrderItem.setMixMatched(false);
            }
            // Calculate the price
            double cost = 0d;

            if (productPriceRange != null && canPrepackageMixMatch) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(productPriceRange.getWeightToleranceId());
                double rangePrice = productPriceRange.getAssignedPrice().doubleValue();
                double toleranceUnitPrice = tolerance != null && tolerance.getWeightValue() != null ? tolerance.getWeightValue().doubleValue() : 0;

                double calcQuantity = quantity;
                if (prepackage != null) {
                    calcQuantity = calcQuantity * prepackage.getUnitValue().doubleValue();
                }


                //Note on Mix&Match: If we're mix and match, then quantity will be lower. Thus, we can just multiply by unitPrice to get the amount
                if (calcQuantity >= tolerance.getStartWeight().doubleValue()
                        && calcQuantity <= tolerance.getEndWeight().doubleValue()) {
                    if (productPriceRange.getPrice() != null) {
                        cost = rangePrice;
                    }
                } else {
                    // It doesn't fall within the range, so let's do some calculations
                    if (linkedOrderItem.isMixMatched() && mixMatchItem != null) {
                        // get order tolerance
                        if (mixMatchItem.groupPriceRange != null) {
                            cost = (rangePrice * mixMatchItem.itemQuantity / mixMatchItem.totalQuantity);
                        } else {
                            ProductPriceRange itemPriceRange = mixMatchItem.itemPriceRange;
                            ProductWeightTolerance itemTolerance = toleranceHashMap.get(itemPriceRange.getWeightToleranceId());
                            if (itemTolerance != null) {
                                cost = (rangePrice / tolerance.getWeightValue().doubleValue()) * itemTolerance.getWeightValue().doubleValue();
                            } else {
                                cost = calcQuantity * (rangePrice / toleranceUnitPrice);
                            }
                        }

                    } else if (prepackage != null) {
                        cost = quantity * productPrice.doubleValue();
                    } else if (toleranceUnitPrice > 0) {
                        cost = calcQuantity * (rangePrice / toleranceUnitPrice);
                    } else {
                        cost = 0;
                    }
                }
            } else if (productPriceBreak != null && canPrepackageMixMatch) {

                double calcQuantity = quantity;
                double breakPrice = productPriceBreak.getAssignedPrice().doubleValue();

                if (calcQuantity == productPriceBreak.getQuantity()) {
                    if (productPriceBreak.getPrice() != null) {
                        cost = breakPrice;
                    }
                } else {
                    if (linkedOrderItem.isMixMatched() && mixMatchItem != null) {
                        if (mixMatchItem.groupPriceBreak != null) {
                            cost = (breakPrice * mixMatchItem.itemQuantity / mixMatchItem.totalQuantity);
                        } else {
                            cost = calcQuantity * (breakPrice / productPriceBreak.getQuantity());
                        }
                    } else {
                        cost = quantity * productPrice.doubleValue();
                    }
                }
            } else {
                cost = quantity * productPrice.doubleValue();
            }

            double unitPrice = 0;
            if (quantity > 0) {
                if (linkedOrderItem.getOverridePrice() != null) {
                    cost = quantity * linkedOrderItem.getOverridePrice().doubleValue();
                    unitPrice = linkedOrderItem.getOverridePrice().doubleValue();
                } else {
                    unitPrice = cost / quantity;
                }

            }

            linkedOrderItem.setCost(new BigDecimal(cost)); //NumberUtils.round(cost, 2));
            linkedOrderItem.setUnitPrice(new BigDecimal(unitPrice)); //NumberUtils.round(unitPrice,2));
            linkedOrderItem.setQuantity(new BigDecimal(quantity)); //NumberUtils.round(quantity, 2));

        }


        // now set the pricing
        List<OrderItem> newOrderItems = new ArrayList<>();
        for (OrderItem orderItem : workingCart.getItems()) {
            Product product = productHashMap.get(orderItem.getProductId());
            if (product == null) {
                continue;
            }

            orderItem.setMixMatched(false);
            double cost = 0;
            double discount = 0;
            OrderItem.DiscountType defaultType = OrderItem.DiscountType.Cash;
            for (LinkedOrderItem linkedOrderItem : linkedOrderItems) {
                if (linkedOrderItem.getOrigOrderItemId().equalsIgnoreCase(orderItem.getId())) {
                    if (linkedOrderItem.isUnlinked()) {
                        newOrderItems.add(linkedOrderItem);
                        linkedOrderItem.setUnlinked(false);
                    } else {
                        cost += linkedOrderItem.getCost().doubleValue();
                        discount += linkedOrderItem.getDiscount().doubleValue();
                        if (linkedOrderItem.isMixMatched()) {
                            orderItem.setMixMatched(true); // if it's mixmatch once, then the whole thing is mixmatched
                        }
                    }
                    defaultType = linkedOrderItem.getDiscountType();
                }
            }
            orderItem.setDiscount(new BigDecimal(discount));
            orderItem.setDiscountType(defaultType);
            if (orderItem.getPromotionReqs() != null && orderItem.getPromotionReqs().size() > 0) {
                orderItem.setDiscountType(OrderItem.DiscountType.Cash); // if there are promotionReqs, set to cash
            }
            double unitPrice = 0;

            if (orderItem.getQuantity().doubleValue() > 0) {
                if (orderItem.getOverridePrice() != null) {
                    cost = orderItem.getQuantity().doubleValue() * orderItem.getOverridePrice().doubleValue();
                    unitPrice = orderItem.getOverridePrice().doubleValue();
                } else {
                    unitPrice = cost / orderItem.getQuantity().doubleValue();
                }

            }


            orderItem.setCost(new BigDecimal(cost)); //NumberUtils.round(cost, 2));
            orderItem.setUnitPrice(new BigDecimal(unitPrice)); //NumberUtils.round(unitPrice,2));;

            double finalPrice = cost - orderItem.getDiscount().doubleValue();
            orderItem.setFinalPrice(new BigDecimal(finalPrice));
        }
        workingCart.getItems().addAll(newOrderItems);

        System.out.println("test");

        return workingCart;
    }


    private List<LinkedOrderItem> createLinkedOrderItems(final Company.CompanyPricing pricingOpt,
                                                         final Cart cart,
                                                         final HashMap<String, PrepackageProductItem> prepackageProductItemHashMap,
                                                         final HashMap<String, Prepackage> prepackageHashMap,
                                                         final HashMap<String, Product> productHashMap,
                                                         final HashMap<String, ProductCategory> categoryHashMap,
                                                         final HashMap<String, ProductWeightTolerance> toleranceHashMap,
                                                         final Transaction transaction) {
        List<LinkedOrderItem> linkOrderItems = new ArrayList<>();
        List<ProductWeightTolerance> tolerances = new ArrayList<>();
        if (toleranceHashMap != null && toleranceHashMap.values() != null) {
            tolerances = Lists.newArrayList(toleranceHashMap.values());
        }
        tolerances.sort(new Comparator<ProductWeightTolerance>() {
            @Override
            public int compare(ProductWeightTolerance o1, ProductWeightTolerance o2) {
                return o1.getStartWeight().compareTo(o2.getStartWeight()) * -1; // highest first
            }
        });

        Kryo kryo = new Kryo();
        boolean isPreparingFulfillment = transaction.isPreparingFulfillment();

        for (OrderItem orderItem : cart.getItems()) {
            /*if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }*/

            if (pricingOpt == Company.CompanyPricing.Normal) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null) {
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());
                    LOG.info(String.format("CategoryId: %s, category: %s", product.getCategoryId(), category));

                    // if gram and not prepackage
                    if (category.getUnitType() == ProductCategory.UnitType.grams) {
                        // grams category, we need to create multiple linkOrderItems

                        BigDecimal curQty = orderItem.getQuantity();

                        boolean hasPrepackage = false;
                        BigDecimal prepackageUnitValue = new BigDecimal(0);
                        if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                            PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(orderItem.getPrepackageItemId());
                            if (prepackageProductItem != null) {
                                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                                if (prepackage != null) {
                                    hasPrepackage = true;
                                    prepackageUnitValue = prepackage.getUnitValue();
                                    curQty = prepackage.getUnitValue().multiply(curQty);
                                }
                            }
                        }

                        boolean deducted = false;
                        if (orderItem.getDiscountedQty().doubleValue() > 0) {

                            OrderItem newOrderItem = kryo.copy(orderItem);
                            LinkedOrderItem linkedOrderItem = LinkedOrderItem.convertFromOrderItem(newOrderItem, isPreparingFulfillment);
                            linkedOrderItem.setQuantity(orderItem.getDiscountedQty());
                            linkedOrderItem.setDiscountedQty(new BigDecimal(0));
                            linkedOrderItem.setDiscount(orderItem.getDiscount());
                            linkedOrderItem.setDiscountType(OrderItem.DiscountType.Cash);
                            linkedOrderItem.setUnlinked(true);
                            linkedOrderItem.setIgnoreMixMatch(true);


                            BigDecimal tempCurQty = new BigDecimal(curQty.doubleValue());
                            if (hasPrepackage && prepackageUnitValue.doubleValue() > 0) {
                                BigDecimal discountedBy = prepackageUnitValue.multiply(orderItem.getDiscountedQty());
                                tempCurQty = tempCurQty.subtract(discountedBy);
                            } else {
                                tempCurQty = tempCurQty.subtract(orderItem.getDiscountedQty());
                            }


                            if (tempCurQty.doubleValue() > 0) {
                                curQty = tempCurQty;
                                orderItem.setDiscountedQty(new BigDecimal(0));
                                orderItem.setDiscount(new BigDecimal(0));
                                orderItem.setQuantity(curQty);
                                deducted = true;

                                linkOrderItems.add(linkedOrderItem);
                            }
                        }

                        // now substract accordingly
                        boolean done = false;
                        LinkedOrderItem linkedOrderItem = null;
                        while (!done && curQty.doubleValue() > 0) {
                            BigDecimal qty = new BigDecimal(curQty.doubleValue());
                            boolean didChange = false;
                            boolean nonMatched = Boolean.FALSE;
                            int iter = 0;
                            for (ProductWeightTolerance tolerance : tolerances) {
                                // if it's between the tolerance, then qty = curQty
                                if (curQty.compareTo(tolerance.getStartWeight()) >= 0 && curQty.compareTo(tolerance.getEndWeight()) <= 0) {
                                    curQty = curQty.subtract(qty);
                                    didChange = true;
                                    break;
                                } else if (curQty.compareTo(tolerance.getEndWeight()) >= 0) {
                                    if (hasPrepackage) {
                                        qty = prepackageUnitValue;
                                        nonMatched = iter == 0; // if it's here, and it's the largest tolerance, so subtract the largest tolerance
                                    } else {
                                        qty = new BigDecimal(tolerance.getUnitValue().doubleValue());
                                        nonMatched = iter == 0; // if it's here, and it's the largest tolerance, so subtract the largest tolerance
                                    }
                                    curQty = curQty.subtract(qty);
                                    didChange = true;
                                    break;
                                }
                                iter++;
                            }
                            double myValue = NumberUtils.round(qty, 6);

                            double qty2 = NumberUtils.round(qty, 2);
                            double curQty2 = NumberUtils.round(curQty, 2);

                            // same amount, so we're done. This is because the qty never changed
                            done = (qty2 == curQty2 && false == didChange);
                            if (nonMatched) {
                                done = true;
                            }

                            if (myValue > 0) {
                                if (hasPrepackage && prepackageUnitValue.doubleValue() > 0) {
                                    myValue = myValue / prepackageUnitValue.doubleValue();
                                    myValue = NumberUtils.round(myValue, 6);
                                }

                                OrderItem newOrderItem = kryo.copy(orderItem);
                                linkedOrderItem = LinkedOrderItem.convertFromOrderItem(newOrderItem, isPreparingFulfillment);
                                if (nonMatched) {
                                    linkedOrderItem.setQuantity(orderItem.getQuantity());
                                } else {
                                    linkedOrderItem.setQuantity(new BigDecimal(myValue));
                                }
                                linkOrderItems.add(linkedOrderItem);

                                linkedOrderItem.setDiscount(new BigDecimal(0));
                                linkedOrderItem.setDiscountedQty(new BigDecimal(0));
                            }

                        }

                        // last one, make sure we retain the discount
                        if (linkedOrderItem != null) {
                            linkedOrderItem.setDiscount(orderItem.getDiscount());
                            linkedOrderItem.setDiscountNotes(orderItem.getDiscountNotes());
                            linkedOrderItem.setDiscountedQty(orderItem.getDiscountedQty());
                        }

                        // now reset original
                        if (hasPrepackage && prepackageUnitValue.doubleValue() > 0 && deducted) {
                            double newQty = orderItem.getQuantity().doubleValue() / prepackageUnitValue.doubleValue();
                            orderItem.setQuantity(new BigDecimal(NumberUtils.round(newQty, 2)));
                        }

                    } else {
                        // look up and pricebreaks
                        // sort the priceBreaks
                        if (product.getPriceBreaks() != null) {
                            BigDecimal curQty = new BigDecimal(0);

                            curQty = orderItem.getQuantity();

                            List<ProductPriceBreak> sortedBreaks = Lists.newArrayList(product.getPriceBreaks());
                            sortedBreaks.sort((o1, o2) -> ((Integer) o1.getQuantity()).compareTo(o2.getQuantity()) * -1);

                            // we will use the price breaks to break up the units to multiple links

                            boolean done = false;
                            LinkedOrderItem linkedOrderItem = null;
                            while (!done && curQty.intValue() > 0) {
                                boolean didChange = false;
                                BigDecimal qty = new BigDecimal(curQty.intValue());
                                for (ProductPriceBreak productPriceBreak : sortedBreaks) {
                                    if (productPriceBreak.isActive() && productPriceBreak.getQuantity() == curQty.intValue()) {
                                        qty = new BigDecimal(productPriceBreak.getQuantity());
                                        curQty = curQty.subtract(qty);
                                        didChange = true;
                                        break;
                                    }
                                }


                                // same amount, so we're done. This is because the qty never changed
                                done = (qty.intValue() == curQty.intValue() && false == didChange);

                                // unit types, just set it
                                if (qty.intValue() > 0) {
                                    OrderItem newOrderItem = kryo.copy(orderItem);
                                    linkedOrderItem = LinkedOrderItem.convertFromOrderItem(newOrderItem, isPreparingFulfillment);
                                    linkedOrderItem.setQuantity(qty);
                                    linkOrderItems.add(linkedOrderItem);

                                    linkedOrderItem.setDiscount(new BigDecimal(0));
                                    linkedOrderItem.setDiscountedQty(new BigDecimal(0));
                                }
                            }
                            // last one, make sure we retain the discount
                            if (linkedOrderItem != null) {
                                linkedOrderItem.setDiscount(orderItem.getDiscount());
                                linkedOrderItem.setDiscountedQty(orderItem.getDiscountedQty());
                            }

                        }

                    }
                }
            } else {
                OrderItem newOrderItem = kryo.copy(orderItem);
                LinkedOrderItem linkedOrderItem = LinkedOrderItem.convertFromOrderItem(newOrderItem, isPreparingFulfillment);
                linkOrderItems.add(linkedOrderItem);
            }
        }

        return linkOrderItems;
    }

    private ProductPriceRange getPriceRange(double quantity, List<ProductPriceRange> productPriceRanges, HashMap<String, ProductWeightTolerance> toleranceHashMap) {
        List<ProductPriceRange> priceRanges = Lists.newArrayList(productPriceRanges);
        priceRanges.sort(new Comparator<ProductPriceRange>() {
            @Override
            public int compare(ProductPriceRange o1, ProductPriceRange o2) {
                Integer one = new Integer(o1.getPriority());
                Integer two = new Integer(o2.getPriority());
                return one.compareTo(two) * -1;
            }
        });

        for (ProductPriceRange productPriceRange : priceRanges) {
            ProductWeightTolerance tolerance = toleranceHashMap.get(productPriceRange.getWeightToleranceId());
            if (tolerance != null) {
                if (quantity >= tolerance.getStartWeight().doubleValue()) {
                    return productPriceRange;
                }
            }
        }

        if (priceRanges.size() > 0) {
            // get last item
            return priceRanges.get(priceRanges.size() - 1);
        }
        return null;
    }

    private BigDecimal processAdjustments(String companyId, List<AdjustmentInfo> adjustmentInfoList) {

        List<ObjectId> adjustmentIds = new ArrayList<>();
        for (AdjustmentInfo info : adjustmentInfoList) {
            if (StringUtils.isNotBlank(info.getAdjustmentId()) && ObjectId.isValid(info.getAdjustmentId())) {
                adjustmentIds.add(new ObjectId(info.getAdjustmentId()));
            }
        }

        HashMap<String, Adjustment> adjustmentMap = adjustmentRepository.listAsMap(companyId, adjustmentIds);

        double adjAmount = 0;
        int factor;
        for (AdjustmentInfo info : adjustmentInfoList) {

            Adjustment adjustment = adjustmentMap.get(info.getAdjustmentId());
            if (adjustment == null || !adjustment.isActive()) {
                throw new BlazeInvalidArgException("Cart", "Please add valid adjustment");
            }

            if (info.getAmount() == null || info.getAmount().doubleValue() == 0) {
                throw new BlazeInvalidArgException("Cart", "Please enter valid adjustment amount");
            }

            factor = 1;
            if (info.isNegative()) {
                factor = -1;
            }
            adjAmount += (info.getAmount().doubleValue() * factor);
        }

        return new BigDecimal(adjAmount);
    }

    private TaxResult calculatePropTax(Transaction refundTransaction, Shop shop,
                                       HashMap<String, BigDecimal> origPropRatioMap, HashMap<String, OrderItem> origItemMap,
                                       Transaction origTransaction, MathContext precision) {
        TaxResult refundTaxResult = new TaxResult();
        refundTaxResult.prepare();

        BigDecimal totalCost = BigDecimal.ZERO;

        for (OrderItem orderItem : refundTransaction.getCart().getItems()) {
            totalCost = totalCost.add(orderItem.getCost());
        }

        for (OrderItem orderItem : refundTransaction.getCart().getItems()) {
            OrderItem origItem = origItemMap.get(orderItem.getOrigOrderItemId());

            BigDecimal refundRatio = orderItem.getQuantity().divide(origItem.getQuantity(), 2, RoundingMode.HALF_EVEN);


            orderItem.setTaxInfo(origItem.getTaxInfo());
            orderItem.setTaxOrder(origItem.getTaxOrder());
            orderItem.setTaxTable(origItem.getTaxTable());

            BigDecimal origRatio = origPropRatioMap.getOrDefault(orderItem.getOrigOrderItemId(), BigDecimal.ONE);
            BigDecimal origPropDiscount = origRatio.multiply(origTransaction.getCart().getCalcCartDiscount());

            BigDecimal origPrice = origItem.getCost().subtract(origPropDiscount);


            BigDecimal refundPropDis = refundRatio.multiply(refundTransaction.getCart().getCalcCartDiscount());

            BigDecimal itemPrice = orderItem.getCost().subtract(refundPropDis);

            TaxResult taxResult = new TaxResult();
            taxResult.prepare();
            if (orderItem.getTaxResult() != null) {
                TaxResult origTaxResult = orderItem.getTaxResult();

                BigDecimal origAlExciseTax = origTaxResult.getTotalALPostExciseTax(); // al post excise tax
                BigDecimal origExciseTax = origTaxResult.getTotalExciseTax(); //Nal post excise tax

                BigDecimal origPreAlExciseTax = origTaxResult.getTotalALExciseTax(); // al pre excise tax
                BigDecimal origPreExciseTax = origTaxResult.getTotalNALPreExciseTax(); //Nal pre excise tax


                BigDecimal orderAmtRatio = orderItem.getQuantity().divide(origItem.getQuantity(), 4, RoundingMode.HALF_EVEN);

                // calculate excise tax and add to itemprice
                BigDecimal propAlExciseTax = origAlExciseTax.multiply(orderAmtRatio);
                BigDecimal propExciseTax = origExciseTax.multiply(orderAmtRatio);

                // calculate excise tax and add to itemprice
                BigDecimal propPreAlExciseTax = origPreAlExciseTax.multiply(orderAmtRatio);
                BigDecimal propPreNalExciseTax = origPreExciseTax.multiply(orderAmtRatio);

                BigDecimal priceWithExcise = itemPrice.add(propExciseTax);
                BigDecimal origPriceWithTax = origPrice.add(origExciseTax);


                taxResult.setTotalCityTax(origTaxResult.getTotalCityTax().multiply(refundRatio));
                taxResult.setTotalStateTax(origTaxResult.getTotalStateTax().multiply(refundRatio));
                taxResult.setTotalCountyTax((origTaxResult.getTotalCountyTax().multiply(refundRatio)));
                taxResult.setTotalFedTax(origTaxResult.getTotalFedTax().multiply(refundRatio));
                taxResult.setTotalExciseTax(propExciseTax);
                taxResult.setTotalALPostExciseTax(propAlExciseTax);

                taxResult.setTotalCityPreTax(origTaxResult.getTotalCityPreTax().multiply(refundRatio));
                taxResult.setTotalStatePreTax(origTaxResult.getTotalStatePreTax().multiply(refundRatio));
                taxResult.setTotalCountyPreTax((origTaxResult.getTotalCountyPreTax().multiply(refundRatio)));
                taxResult.setTotalFedPreTax(origTaxResult.getTotalFedPreTax().multiply(refundRatio));
                taxResult.setTotalNALPreExciseTax(propPreNalExciseTax);
                taxResult.setTotalALExciseTax(propPreAlExciseTax);

                orderItem.setTaxResult(taxResult);

                orderItem.setCalcTax(taxResult.getTotalCityTax().add(taxResult.getTotalStateTax()).add(taxResult.getTotalFedTax()).add(taxResult.getTotalCountyTax()));
                orderItem.setExciseTax(taxResult.getTotalALPostExciseTax().add(taxResult.getTotalExciseTax()));

                taxResult.setTotalPostCalcTax(orderItem.getCalcTax());

                refundTaxResult.setTotalCityTax(refundTaxResult.getTotalCityTax().add(taxResult.getTotalCityTax()));
                refundTaxResult.setTotalStateTax(refundTaxResult.getTotalStateTax().add(taxResult.getTotalStateTax()));
                refundTaxResult.setTotalCountyTax(refundTaxResult.getTotalCountyTax().add(taxResult.getTotalCountyTax()));
                refundTaxResult.setTotalFedTax(refundTaxResult.getTotalFedTax().add(taxResult.getTotalFedTax()));
                refundTaxResult.setTotalExciseTax(refundTaxResult.getTotalExciseTax().add(taxResult.getTotalExciseTax()));
                refundTaxResult.setTotalALPostExciseTax(refundTaxResult.getTotalALPostExciseTax().add(taxResult.getTotalALPostExciseTax()));
                refundTaxResult.setTotalPostCalcTax(refundTaxResult.getTotalPostCalcTax().add(orderItem.getCalcTax().add(orderItem.getExciseTax())));

            }

        }

        return refundTaxResult;
    }

    private void prepareCartForRefund(Shop shop,
                                      Transaction trans,
                                      boolean reset,
                                      Transaction.TransactionStatus incomingStatus,
                                      boolean assignProducts,
                                      CustomerInfo customerInfo,
                                      Transaction parentTransaction) {

        if (trans == null) {
            return;
        }
        if (shop == null) {
            return;
        }
        if (trans.getCart() == null) {
            return;
        }

        MathContext precision = new MathContext(3, RoundingMode.HALF_EVEN); // example 2
        Cart cart = trans.getCart();
        MemberGroup memberGroup = customerInfo != null ? customerInfo.getMemberGroup() : null;

        if (cart.getSplitPayment() != null) {
            cart.getSplitPayment().prepare(cart.getCompanyId());
        }

        // Get tolerance map
        HashMap<String, ProductWeightTolerance> toleranceHashMap = toleranceRepository.getActiveTolerancesAsMap(shop.getCompanyId());
        List<ObjectId> objectIds = new ArrayList<>();
        List<String> productIds = new ArrayList<>();
        List<ObjectId> origProducts = new ArrayList<>();

        // get products
        for (OrderItem orderItem : cart.getItems()) {

            if (StringUtils.isBlank(orderItem.getOrderItemId())) {
                orderItem.setOrderItemId(UUID.randomUUID().toString());
            }

            if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                objectIds.add(new ObjectId(orderItem.getProductId()));
                productIds.add(orderItem.getProductId());
            }
        }

        // Fetch data
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(shop.getCompanyId(),
                shop.getId(), objectIds);
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAllAsMap(shop.getCompanyId(), shop.getId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap =
                prepackageProductItemRepository.getPrepackagesForProductsAsMap(shop.getCompanyId(),
                        shop.getId(), productIds);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(shop.getCompanyId(),
                shop.getId(), productIds);

        HashMap<String, MemberGroupPrices> memberGroupPricesHashMap = new HashMap<>();
        if (customerInfo != null && customerInfo.getMemberGroup() != null) {
            memberGroupPricesHashMap = memberGroupPricesRepository.getPricesMapForProductGroup(shop.getCompanyId(), shop.getId(), productIds, customerInfo.getMemberGroup().getId());
        }

        // Populate prepackage with weigh tolerance values
        for (Prepackage prepackage : prepackageHashMap.values()) {
            if (!prepackage.isCustomWeight() && StringUtils.isNotBlank(prepackage.getToleranceId())) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(prepackage.getToleranceId());

                if (tolerance != null) {
                    prepackage.setName(tolerance.getName());
                    prepackage.setUnitValue(tolerance.getUnitValue());
                }
            }
        }

        HashMap<String, BigDecimal> propRatioMap = new HashMap<>();

        HashMap<String, OrderItem> origItemMap = new HashMap<>();
        for (OrderItem orderItem : parentTransaction.getCart().getItems()) {
            origProducts.add(new ObjectId(orderItem.getProductId()));

        }

        HashMap<String, Product> origProductMap = productRepository.listAsMap(trans.getCompanyId(), origProducts);

        BigDecimal cartTotal = BigDecimal.ZERO;
        for (OrderItem orderItem : parentTransaction.getCart().getItems()) {
            origItemMap.put(orderItem.getOrderItemId(), orderItem);
            Product product = origProductMap.get(orderItem.getProductId());
            if (product == null) {
                continue;
            }
            if (product.isDiscountable()) {
                cartTotal = cartTotal.add(orderItem.getCost());
            }

        }

        for (OrderItem orderItem : parentTransaction.getCart().getItems()) {
            Product product = origProductMap.get(orderItem.getProductId());
            if (product == null) {
                continue;
            }
            BigDecimal ratio = BigDecimal.ONE;
            if (product.isDiscountable() && cartTotal.doubleValue() > 0) {
                ratio = (orderItem.getCost()).divide(cartTotal, precision);
            }
            propRatioMap.put(orderItem.getOrderItemId(), ratio);
        }

        for (OrderItem orderItem : cart.getItems()) {
            OrderItem origOrderItem = origItemMap.get(orderItem.getOrigOrderItemId());
            if (origOrderItem == null) {
                continue;
            }

            BigDecimal overridePrice = orderItem.getUnitPrice();

            BigDecimal ratio = propRatioMap.getOrDefault(origOrderItem.getOrderItemId(), BigDecimal.ONE);

            if (trans.getRefundVersion() == RefundRequest.RefundVersion.NEW) {

                BigDecimal subTotal = origOrderItem.getCost();
                //BigDecimal ratio = propRatioMap.getOrDefault(origOrderItem.getOrderItemId(), BigDecimal.ONE);

                BigDecimal propCartDiscount = parentTransaction.getCart().getCalcCartDiscount() != null ? parentTransaction.getCart().getCalcCartDiscount().multiply(ratio) : BigDecimal.ZERO;
                BigDecimal propAfterTaxDiscount = parentTransaction.getCart().getAppliedAfterTaxDiscount() != null ? parentTransaction.getCart().getAppliedAfterTaxDiscount().multiply(ratio) : BigDecimal.ZERO;

                //Add Taxes
                BigDecimal itemTaxes = BigDecimal.ZERO;
                if (origOrderItem.getTaxResult() != null) {
                    TaxResult taxResult = origOrderItem.getTaxResult();
                    itemTaxes = taxResult.getTotalALPostExciseTax()
                            .add(taxResult.getTotalExciseTax())
                            .add(taxResult.getTotalCityTax())
                            .add(taxResult.getTotalFedTax())
                            .add(taxResult.getTotalStateTax())
                            .add(taxResult.getTotalCountyTax());

                }
                if (itemTaxes == BigDecimal.ZERO) {
                    BigDecimal subTotalAfterDiscount = origOrderItem.getFinalPrice().subtract(propCartDiscount);
                    if ((origOrderItem.getTaxTable() == null || !origOrderItem.getTaxTable().isActive()) && origOrderItem.getTaxInfo() != null) {
                        TaxInfo taxInfo = origOrderItem.getTaxInfo();
                        if (origOrderItem.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            itemTaxes = itemTaxes.add(subTotalAfterDiscount.multiply(taxInfo.getStateTax()))
                                    .add(subTotalAfterDiscount.multiply(taxInfo.getCityTax()))
                                    .add(subTotalAfterDiscount.multiply(taxInfo.getFederalTax()));
                        }
                    }
                }

                if (itemTaxes == BigDecimal.ZERO) {
                    itemTaxes = origOrderItem.getCalcTax();
                }

                BigDecimal total = subTotal.subtract(propCartDiscount).add(itemTaxes).subtract(propAfterTaxDiscount);

                overridePrice = (trans.getCart().getRequestRefundAmt().multiply(subTotal).divide(total, precision)).divide(origOrderItem.getQuantity(), precision);
            }

            orderItem.setOverridePrice(overridePrice);

            // calculate custom discount
            if (orderItem.getCalcDiscount() != null && orderItem.getCalcDiscount().doubleValue() > 0) {
                // calculate discount now
                // Ratio
                BigDecimal orderRatio = orderItem.getQuantity().divide(origOrderItem.getQuantity(),2,BigDecimal.ROUND_HALF_EVEN);
                BigDecimal ratioDiscount = orderRatio.multiply(orderItem.getCalcDiscount());
                orderItem.setDiscount(ratioDiscount);
                orderItem.setDiscountType(OrderItem.DiscountType.Cash);
            }

        }
        //trans.getCart().getPromotionReqs().clear();

        // process pricing
        Cart processedCart = this.processEntireCartPricingPromos(shop,
                trans.getCart(), reset,
                assignProducts,
                memberGroup,
                memberGroupPricesHashMap,
                productHashMap,
                prepackageProductItemHashMap,
                prepackageHashMap,
                categoryHashMap,
                toleranceHashMap,
                trans,
                incomingStatus,
                customerInfo
        );
        trans.setCart(processedCart);

        //update cart discount based on order item
        processRefundCartDiscount(parentTransaction, cart.getItems(), trans);

        double subTotal = 0d;
        double discountedRefunded = 0.0;
        double totalDiscounts = 0.0;
        double origSubTotal = 0d;
        double cartDiscountTotal = 0d;
        // calculate final price with discounts
        for (OrderItem orderItem : trans.getCart().getItems()) {
            // reset orig to quantity
            orderItem.setQuantity(orderItem.getOrigQuantity());
            // Calculate final price
            // If discountType == Percentage, calculate percentage
            // if discountType == Cash, use actual discount
            // finalCost = cost - discount

            BigDecimal cost = orderItem.getCost();
            BigDecimal finalCost = cost;

            if (orderItem.getDiscount().doubleValue() > 0 && cost.doubleValue() > 0) {
                if (orderItem.getDiscountType() == OrderItem.DiscountType.Cash) {
                    finalCost = cost.subtract(orderItem.getDiscount());
                    orderItem.setCalcDiscount(orderItem.getDiscount());
                } else {
                    BigDecimal orderDiscount = cost.multiply(BigDecimal.valueOf(orderItem.getDiscount().doubleValue() / 100));
                    finalCost = cost.subtract(orderDiscount);
                    orderItem.setCalcDiscount(orderDiscount);
                }
            }

            orderItem.setFinalPrice(finalCost);
            double cartDiscount = 0d;
            if (orderItem.getTotalRefundAmount() != null && orderItem.getTotalRefundAmount().doubleValue() > 0) {
                // OVERRIDE TO USE REFUND TOTAL AMT AS THE FINAL PRICE SO CALCULATION WOULD BE CORRECT
                if (orderItem.getTotalRefundAmount().doubleValue() < orderItem.getFinalPrice().doubleValue()) {
                    cartDiscount = orderItem.getFinalPrice().doubleValue() - orderItem.getTotalRefundAmount().doubleValue();
                    cartDiscountTotal+=cartDiscount;
                }
            }


            // calculate total discounts
            if (orderItem.getDiscountType() == OrderItem.DiscountType.Cash) {
                totalDiscounts += orderItem.getDiscount().doubleValue();
            } else {
                totalDiscounts += orderItem.getCost().doubleValue() * (orderItem.getDiscount().doubleValue() / 100);
            }

            subTotal += orderItem.getFinalPrice().doubleValue();
            origSubTotal += orderItem.getCost().doubleValue();
        }


        // Prepare final cart cost
        trans.getCart().setRefundAmount(new BigDecimal(0));

        // Calculate subtotal
        trans.getCart().setSubTotal(new BigDecimal(origSubTotal));

        trans.getCart().setDiscount(new BigDecimal(cartDiscountTotal));


        // Calculate Discount
        double totalCalcCartDiscount = 0;
        if (subTotal > 0) {
            if (trans.getCart().getDiscountType() == OrderItem.DiscountType.Cash) {
                // if discount is greater than subTotal, then set the discount to be the subtotal
                if (subTotal <= trans.getCart().getDiscount().doubleValue()) {
                    trans.getCart().setDiscount(new BigDecimal(subTotal));
                }

//                if (trans.getTransType()!=Transaction.TransactionType.Refund){
//                    subTotal = subTotal - trans.getCart().getDiscount().doubleValue();
//                    totalDiscounts = totalDiscounts + trans.getCart().getDiscount().doubleValue();
//                    totalCalcCartDiscount = trans.getCart().getDiscount().doubleValue();
//                }
                subTotal = subTotal - trans.getCart().getDiscount().doubleValue();
                totalDiscounts = totalDiscounts + trans.getCart().getDiscount().doubleValue();
                totalCalcCartDiscount = trans.getCart().getDiscount().doubleValue();

            } else {

                double cartDiscount = (subTotal * trans.getCart().getDiscount().doubleValue() / 100);
                subTotal = subTotal - cartDiscount;

                totalCalcCartDiscount = cartDiscount;
                totalDiscounts = totalDiscounts + cartDiscount;
            }
        }

        trans.getCart().setSubTotalDiscount(new BigDecimal(subTotal));

        trans.getCart().setCalcCartDiscount(new BigDecimal(totalCalcCartDiscount));

        // Apply product based taxes
        TaxResult taxResult = calculatePropTax(trans, shop, propRatioMap, origItemMap, parentTransaction, precision);
        double totalPostTax = taxResult.getTotalPostCalcTax().doubleValue();
        double totalPreTax = taxResult.getTotalPreCalcTax().doubleValue();
        double totalExciseTax = taxResult.getTotalExciseTax().doubleValue();
        double totalALExciseTax = taxResult.getTotalALExciseTax().doubleValue();

        // apply tax
        //double totalTax = subTotal * trans.getCart().getTax().doubleValue();
        subTotal = subTotal + totalPostTax;

        subTotal = applyPostTaxDiscount(trans, subTotal);

        totalPostTax += totalPreTax; // add pre-taxed to the total taxed
        trans.getCart().setTotalCalcTax(new BigDecimal(totalPostTax));
        trans.getCart().setTotalPreCalcTax(new BigDecimal(totalPreTax));
        trans.getCart().setTotalExciseTax(new BigDecimal(totalExciseTax));
        trans.getCart().setTotalALExciseTax(new BigDecimal(totalALExciseTax));
        trans.getCart().setTaxResult(taxResult);

        double total = subTotal;
        if (total < 0) {
            total = 0;
        }

        double roundedTotal = total;
        double roundOffTotal = 0.0;

        Shop.TaxRoundOffType taxRoundOffType = trans.getCart().getTaxRoundOffType();
        if (taxRoundOffType == null) {
            taxRoundOffType = shop.getTaxRoundOffType();
        }

        if (Shop.TaxRoundOffType.FIVE_CENT.equals(taxRoundOffType) && parentTransaction.getCart().getPaymentOption() == Cart.PaymentOption.Cash) {
            roundOffTotal = NumberUtils.roundToNearestCent(roundedTotal);
        } else if (Shop.TaxRoundOffType.NEAREST_DOLLAR == taxRoundOffType) {
            roundOffTotal = NumberUtils.roundToNearestCent(roundedTotal);
        } else if (Shop.TaxRoundOffType.UP_DOLLAR == taxRoundOffType) {
            roundOffTotal = NumberUtils.ceil(roundedTotal);
        } else if (Shop.TaxRoundOffType.DOWN_DOLLAR == taxRoundOffType){
            roundOffTotal = NumberUtils.floor(roundedTotal);
        } else {
            roundOffTotal = NumberUtils.round(roundedTotal, 2);
        }

        trans.getCart().setRoundAmt(new BigDecimal(Math.abs(roundedTotal - roundOffTotal)));
        trans.getCart().setTotal(new BigDecimal(roundOffTotal));
        trans.getCart().setTaxRoundOffType(taxRoundOffType);

        // reset total discounts amount (adhering to refunded items)
        double currentDiscount = totalDiscounts;
        currentDiscount -= discountedRefunded;

        // zero out just in case
        if (currentDiscount < 0) {
            currentDiscount = 0;
        }

        trans.getCart().setTotalDiscount(new BigDecimal(currentDiscount)); //currentDiscount);
        trans.getCart().setRefundAmount(trans.getCart().getTotal());

    }

    public Transaction finalizeOrderItem(Shop shop, Transaction trans, String orderItemId) {
        if (trans == null) {
            return null;
        }

        Cart cart = trans.getCart();
        OrderItem orderItem = cart.getItems().stream().filter(item -> orderItemId.equals(item.getOrderItemId())).findFirst().orElse(null);
        int index = cart.getItems().indexOf(orderItem);
        if (orderItem == null) {
            return null;
        }

        if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
            orderItem.setFinalized(true);
            ProductVersion productVersion = productVersionRepository.findLastProductVersionByProductId(shop.getCompanyId(),shop.getId(),orderItem.getProductId());
            if(productVersion == null) {
                Product product = productRepository.get(shop.getCompanyId(),orderItem.getProductId());
                //Save the updated product in product versions collection
                productVersion = new ProductVersion();
                productVersion.setProduct(product);
                productVersion = productVersionRepository.save(productVersion);
            }
            orderItem.setFinalizedProductId(productVersion.getId());
        }

        cart.getItems().set(index, orderItem);
        return trans;
    }

    public Transaction unfinalizeOrderItem(Transaction trans, String orderItemId) {
        if (trans == null) {
            return null;
        }

        Cart cart = trans.getCart();
        OrderItem orderItem = cart.getItems().stream().filter(item -> orderItemId.equals(item.getOrderItemId())).findFirst().orElse(null);

        int index = cart.getItems().indexOf(orderItem);
        if (orderItem == null) {
            return null;
        }

        orderItem.setFinalized(false);
        orderItem.setFinalizedProductId(null);

        long countUnfinalizedOrders = cart.getItems().stream().filter(item -> !item.isFinalized()).count();
        if (countUnfinalizedOrders==cart.getItems().size()){
            unfinalizeAllOrderItems(null,trans);
        }

        cart.getItems().set(index, orderItem);
        return trans;
    }

    public Transaction finalizeAllOrderItems(Shop shop, Transaction trans) {
        if (trans == null) {
            return null;
        }

        List<ObjectId> productIds = new ArrayList<>();
        Cart cart = trans.getCart();

        for (OrderItem orderItem : cart.getItems()) {
            if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                productIds.add(new ObjectId(orderItem.getProductId()));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(shop.getCompanyId(),
               shop.getId(), productIds);

        for (OrderItem orderItem : cart.getItems()) {
            if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                orderItem.setFinalized(true);
                Product product = productHashMap.get(orderItem.getProductId());
                ProductVersion productVersion = productVersionRepository.findLastProductVersionByProductId(shop.getCompanyId(),shop.getId(),orderItem.getProductId());
                if(productVersion == null) {
                    //Save the updated product in product versions collection
                    productVersion = new ProductVersion();
                    productVersion.setProduct(product);
                    productVersionRepository.save(productVersion);
                    productVersion = productVersionRepository.findLastProductVersionByProductId(shop.getCompanyId(),shop.getId(),orderItem.getProductId());
                }
                orderItem.setFinalizedProductId(productVersion.getId());
                orderItem.setProduct(product);
            }
        }
        return trans;
    }

    public Transaction unfinalizeAllOrderItems(Shop shop, Transaction trans) {
        if (trans == null) {
            return null;
        }

        List<ObjectId> productIds = new ArrayList<>();
        Cart cart = trans.getCart();

        for (OrderItem orderItem : cart.getItems()) {
            if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                orderItem.setFinalized(false);
                orderItem.setFinalizedProductId(null);
            }

            //Unfinalize promotion reqs in order item
            for (PromotionReq promoReq : orderItem.getPromotionReqs()) {
                promoReq.setFinalizedDiscountId(null);
            }
        }

        //Unfinalize promotions/rewards in promotionReq and promotionReqLog
        for (PromotionReq promoReq : cart.getPromotionReqs()) {
            promoReq.setFinalizedDiscountId(null);
        }
        for (PromotionReq promoReq : cart.getPromotionReqLogs()) {
            promoReq.setFinalizedDiscountId(null);
        }

        return trans;
    }

    public void prepareCartForPaymentType(Shop shop, Transaction transaction, CustomerInfo customerInfo) {
        Cart cart = transaction.getCart();
        // Apply product based taxes
        TaxResult taxResult = transaction.getCart().getTaxResult();
        double totalPostTax = taxResult.getTotalPostCalcTax().doubleValue();
        double subTotal = transaction.getCart().getSubTotalDiscount().doubleValue();
        // apply tax
        subTotal = subTotal + totalPostTax;
        subTotal = applyPostTaxDiscount(transaction, subTotal);


        //update customer info's zipcode with transactions zipcode
        if (transaction.getDeliveryAddress() != null && StringUtils.isNotBlank(transaction.getDeliveryAddress().getZipCode())) {
            customerInfo.setZipCode(transaction.getDeliveryAddress().getZipCode());
        }

        // Apply delivery Fee
        if (transaction.getTransType() == Transaction.TransactionType.Sale) {
            applyDeliveryFee(shop, transaction, customerInfo, subTotal);
        }

        // sanity test (after discounts)
        if (subTotal < transaction.getCart().getDeliveryFee().doubleValue()) {
            cart.setDeliveryFee(new BigDecimal(0));
        }

        // set delivery fee
        if (transaction.getCart().getDeliveryFee() == null) {
            transaction.getCart().setDeliveryFee(new BigDecimal(0));
        }


        // Apply Credit card fee
        applyCreditCardFee(shop, transaction, subTotal);

        if (transaction.getCart().getCreditCardFee() == null) {
            transaction.getCart().setCreditCardFee(new BigDecimal(0));
        }

        //Update credit card fees according to split payment.
        if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Split
                && transaction.getCart().getSplitPayment() != null
                && transaction.getCart().getCreditCardFee().doubleValue() > 0) {

            SplitPayment splitPayment = transaction.getCart().getSplitPayment();
            if (splitPayment.getCreditDebitAmt() != null && splitPayment.getCreditDebitAmt().doubleValue() > 0) {
                BigDecimal ccFees = transaction.getCart().getCreditCardFee();
                double splitCcFees = (subTotal == 0) ? 0 : (ccFees.doubleValue() * splitPayment.getCreditDebitAmt().doubleValue() / subTotal);
                transaction.getCart().setCreditCardFee(BigDecimal.valueOf(splitCcFees));
            }
        }
        // add delivery fee in
        subTotal += transaction.getCart().getDeliveryFee().doubleValue();

        //Add credit card fee
        subTotal += transaction.getCart().getCreditCardFee().doubleValue();

        double total = subTotal;
        if (total < 0) {
            total = 0;
        }


        //double total = queuedTransaction.getCart().getSales();
        double roundedTotal = total;
        double roundOffTotal = 0.0;

        switch (shop.getTaxRoundOffType()) {
            case FIVE_CENT:
                if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Cash) {
                    roundOffTotal = NumberUtils.roundToNearestCent(roundedTotal);
                } else {
                    roundOffTotal = NumberUtils.round(roundedTotal, 2);
                }
                break;
            case NEAREST_DOLLAR:
                roundOffTotal = NumberUtils.roundToNearestCent(roundedTotal);
                break;
            case UP_DOLLAR:
                roundOffTotal = NumberUtils.ceil(roundedTotal);
                break;
            case DOWN_DOLLAR:
                roundOffTotal = NumberUtils.floor(roundedTotal);
                break;
            default:
                roundOffTotal = NumberUtils.round(roundedTotal, 2);

        }

        transaction.getCart().setRoundAmt(new BigDecimal(Math.abs(roundedTotal - roundOffTotal)));
        transaction.getCart().setTotal(new BigDecimal(roundOffTotal));
        transaction.getCart().setTaxRoundOffType(shop.getTaxRoundOffType());

        if (transaction.getTransType() == Transaction.TransactionType.Refund) {
            if (transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                transaction.getCart().setRefundAmount(transaction.getCart().getTotal());
            }
        } else {
            // Sale
            if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Split) {
                if (transaction.getCart().getSplitPayment() != null) {
                    // account for change
                    BigDecimal changedDue = transaction.getCart().getSplitPayment().getTotalSplits().subtract(cart.getTotal());
                    transaction.getCart().setCashReceived(transaction.getCart().getSplitPayment().getCashAmt());
                    if (changedDue.doubleValue() > 0) {
                        // implies cashReceived > totalCashValue
                        transaction.getCart().setChangeDue(changedDue);
                    }
                }
            }
        }

        if (!CollectionUtils.isNullOrEmpty(cart.getAdjustmentInfoList())) {
            BigDecimal adjustmentAmt = processAdjustments(shop.getCompanyId(), cart.getAdjustmentInfoList());
            transaction.getCart().setTotal(adjustmentAmt.add(BigDecimal.valueOf(roundOffTotal)));
            transaction.getCart().setAdjustmentAmount(adjustmentAmt);
        }

    }
}
