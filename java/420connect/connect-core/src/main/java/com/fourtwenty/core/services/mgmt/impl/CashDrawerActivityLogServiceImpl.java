package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionActivityLogResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CashDrawerActivityLogService;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class CashDrawerActivityLogServiceImpl extends AbstractAuthServiceImpl implements CashDrawerActivityLogService {

    private static final Logger logger = LoggerFactory.getLogger(CashDrawerActivityLogServiceImpl.class);

    private CashDrawerActivityLogRepository cashDrawerActivityLogRepository;
    private EmployeeRepository employeeRepository;
    private MemberRepository memberRepository;
    private TransactionRepository transactionRepository;
    private CashDrawerSessionRepository cashDrawerSessionRepository;
    private ShopRepository shopRepository;

    @Inject
    public CashDrawerActivityLogServiceImpl(Provider<ConnectAuthToken> tokenProvider, CashDrawerActivityLogRepository cashDrawerActivityLogRepository,
                                            EmployeeRepository employeeRepository, MemberRepository memberRepository, TransactionRepository transactionRepository, CashDrawerSessionRepository cashDrawerSessionRepository, ShopRepository shopRepository) {
        super(tokenProvider);
        this.cashDrawerActivityLogRepository = cashDrawerActivityLogRepository;
        this.employeeRepository = employeeRepository;
        this.memberRepository = memberRepository;
        this.transactionRepository = transactionRepository;
        this.cashDrawerSessionRepository = cashDrawerSessionRepository;
        this.shopRepository = shopRepository;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SearchResult<CashDrawerSessionActivityLogResult> getActiveCaseDrawerActivityLogs(String cashDrawerSessionId, long startDate, long endDate, int start, int limit) {

        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), cashDrawerSessionId);
        if (Objects.isNull(cashDrawerSession)) {
            throw new BlazeInvalidArgException("CashDrawerActivity", "No cash drawer found");
        }


        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (startDate == 0 && endDate == 0) {
            String date = StringUtils.isBlank(cashDrawerSession.getDate()) ? DateUtil.parseTodayToPattern("yyyyMMdd") : cashDrawerSession.getDate();
            DateTime cdDate = DateUtil.parseDateKey(date);
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(shop.getTimeZone());
            startDate = cdDate.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
            DateTime jodaEndDate = cdDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
            endDate = jodaEndDate.minusMinutes(timeZoneOffset).getMillis();
        }

        SearchResult<CashDrawerSessionActivityLogResult> results = cashDrawerActivityLogRepository.getAllActivityLogForCashDrawerSessionId(token.getCompanyId(), token.getShopId(), cashDrawerSessionId, CashDrawerSessionActivityLogResult.class);
        SearchResult<Transaction> completedTransactions = transactionRepository.getCompletedTransactionsForTerminal(token.getCompanyId(), token.getShopId(), cashDrawerSession.getTerminalId(), startDate, endDate);

        Set<ObjectId> employeeIds = new HashSet<>();
        Set<ObjectId> memberIds = new HashSet<>();


        for (Transaction transaction : completedTransactions.getValues()) {
            if (transaction.getTransType() != Transaction.TransactionType.Sale
                    && transaction.getTransType() != Transaction.TransactionType.Refund) {
                continue;
            }

            memberIds.add(new ObjectId(transaction.getMemberId()));

            CashDrawerSessionActivityLogResult log = new CashDrawerSessionActivityLogResult();
            log.setTransactionNumber(transaction.getTransNo());
            log.setTransactionId(transaction.getId());
            log.setCreated(transaction.getProcessedTime());
            log.setCashDrawerSessionId(cashDrawerSession.getId());
            if (transaction.getTransType().equals(Transaction.TransactionType.Sale)) {
                log.setCashDrawerType(CashDrawerSessionActivityLog.CashDrawerType.Sale);
            } else if (transaction.getTransType().equals(Transaction.TransactionType.Refund)) {
                log.setCashDrawerType(CashDrawerSessionActivityLog.CashDrawerType.Refund);
            }
            log.setSaleAmount(transaction.getCart().getTotal());
            log.setMessage(String.format("#%s - %s (%s)", transaction.getTransNo(), log.getCashDrawerType().getMessage(), transaction.getCart().getPaymentOption()));
            log.setMemberId(transaction.getMemberId());
            log.setCreatedByEmployeeId(transaction.getSellerId());
            log.setCompanyId(transaction.getCompanyId());
            log.setShopId(transaction.getShopId());

            results.getValues().add(log);
        }

        for (CashDrawerSessionActivityLogResult logResult : results.getValues()) {
            if (logResult.getCreatedByEmployeeId() != null && ObjectId.isValid(logResult.getCreatedByEmployeeId())) {
                employeeIds.add(new ObjectId(logResult.getCreatedByEmployeeId()));
            }
        }


        results.getValues().sort((o1, o2) -> o1.getCreated().compareTo(o2.getCreated()) * -1);

        results.setTotal(results.getTotal() + completedTransactions.getValues().size());

        Map<String, Employee> employeeHashMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        Map<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(memberIds));

        for (CashDrawerSessionActivityLogResult activityLogResult : results.getValues()) {
            if (employeeHashMap.containsKey(activityLogResult.getCreatedByEmployeeId())) {
                Employee employee = employeeHashMap.get(activityLogResult.getCreatedByEmployeeId());
                employee.setPassword(null);
                employee.setPin(null);
                activityLogResult.setCreatedByEmployeeName(employee.getFirstName() + " " + employee.getLastName());
            }
            if (StringUtils.isNotBlank(activityLogResult.getMemberId()) && memberHashMap.containsKey(activityLogResult.getMemberId())) {
                Member member = memberHashMap.get(activityLogResult.getMemberId());
                activityLogResult.setMemberName(member.getFirstName() + " " + member.getLastName());
            }
        }

        return results;
    }

    @Override
    public CashDrawerSessionActivityLog createCashDrawerActivityType(PaidInOutItem paidInOutItem, CashDrawerSession cashDrawerSession,
                                                                     CashDrawerSessionActivityLog.CashDrawerType cashDrawerType, BigDecimal amount, Transaction transaction) {
        CashDrawerSessionActivityLog cashDrawerSessionActivityLog = new CashDrawerSessionActivityLog();
        cashDrawerSessionActivityLog.prepare(token.getCompanyId());
        cashDrawerSessionActivityLog.setShopId(token.getShopId());

        if (!Objects.isNull(paidInOutItem)) {
            cashDrawerSessionActivityLog.setPadIoItemId(paidInOutItem.getId());
            cashDrawerSessionActivityLog.setCashDrawerSessionId(cashDrawerSession.getId());
        } else if (!Objects.isNull(transaction)) {
            cashDrawerSessionActivityLog.setTransactionId(transaction.getId());
            cashDrawerSessionActivityLog.setMemberId(transaction.getMemberId());
        }

        cashDrawerSessionActivityLog.setCashDrawerSessionId(cashDrawerSession.getId());
        cashDrawerSessionActivityLog.setCashDrawerType(cashDrawerType);
        cashDrawerSessionActivityLog.setCreatedByEmployeeId(token.getActiveTopUser().getUserId());

        cashDrawerSessionActivityLog.setSaleAmount(amount);
        cashDrawerSessionActivityLog.setMessage(cashDrawerType.getMessage());

        cashDrawerActivityLogRepository.save(cashDrawerSessionActivityLog);
        return cashDrawerSessionActivityLog;
    }
}
