package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.CashDrawerSessionActivityLog;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.DrawerStartCashUpdateRequest;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.repositories.dispensary.CashDrawerSessionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PaidInOutItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.company.EndDrawerRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.PaidInOutRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StartDrawerRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.fourtwenty.core.services.mgmt.CashDrawerActivityLogService;
import com.fourtwenty.core.services.mgmt.CashDrawerService;
import com.fourtwenty.core.services.mgmt.CompanyService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.ReceiptUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import static com.fourtwenty.core.util.ReceiptUtil.repeatContent;

/**
 * Created by mdo on 12/7/16.
 */
public class CashDrawerServiceImpl extends AbstractAuthServiceImpl implements CashDrawerService {
    private static final Log LOGGER = LogFactory.getLog(CashDrawerServiceImpl.class);

    private static final String CASH_DRAWER = "Cash drawer";
    private static final String CASH_DRAWER_NOT_FOUND = "Cash drawer is not found";
    private static final String PAID_IO_NOT_FOUND = "Paid in/out item is not found";

    @Inject
    private CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    private PaidInOutItemRepository paidInOutItemRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    RoleService roleService;
    @Inject
    private CashDrawerActivityLogService cashDrawerActivityLogService;
    @Inject
    private CashDrawerProcessorService cashDrawerProcessorService;
    @Inject
    private CompanyService companyService;


    @Inject
    public CashDrawerServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public DateSearchResult<CashDrawerSessionResult> getCashDrawersByDate(long afterDate, long beforeDate) {

        DateSearchResult<CashDrawerSessionResult> results;
        if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            results = cashDrawerSessionRepository.findItemsWithDate(token.getCompanyId(),
                    token.getShopId(),
                    afterDate,
                    beforeDate,
                    CashDrawerSessionResult.class);
        } else {

            Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
            // Use assignedTerminalId
            String assignedTerminalId = employee.getAssignedTerminalId();
            if (StringUtils.isBlank(assignedTerminalId)) {
                assignedTerminalId = token.getTerminalId(); // use current logged in terminal
            }
            SearchResult<CashDrawerSessionResult> results2 = cashDrawerSessionRepository.getCashDrawerSessionsForShop(token.getCompanyId(),
                    token.getShopId(), assignedTerminalId, 0, 30, CashDrawerSessionResult.class);


            results = new DateSearchResult<>();
            results.setValues(results2.getValues());
            results.setTotal(results2.getTotal());
            results.setAfterDate(0);
            results.setBeforeDate(DateTime.now().getMillis());
        }

        prepareForView(results.getValues());
        return results;
    }

    @Override
    public SearchResult<CashDrawerSessionResult> getCashDrawerLogs(int start, int limit) {

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        SearchResult<CashDrawerSessionResult> results = new SearchResult<>();
        if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            results = cashDrawerSessionRepository.findItems(token.getCompanyId(),
                    token.getShopId(),
                    "{modified:-1}",
                    start,
                    limit,
                    CashDrawerSessionResult.class);
        } else {
            Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
            // Use assignedTerminalId
            String assignedTerminalId = employee.getAssignedTerminalId();
            if (StringUtils.isBlank(assignedTerminalId)) {
                assignedTerminalId = token.getTerminalId(); // use current logged in terminal
            }
            results = cashDrawerSessionRepository.getCashDrawerSessionsForShop(token.getCompanyId(), token.getShopId(), assignedTerminalId, start, limit, CashDrawerSessionResult.class);
        }

        prepareForView(results.getValues());
        return results;
    }

    @Override
    public CashDrawerSessionResult startCashDrawer(StartDrawerRequest request) {


        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("CashDrawer", "Shop does not exist or inactive.");
        }


        CashDrawerSession dbLogResult = cashDrawerSessionRepository.getCashDrawerForDate(token.getCompanyId(),
                token.getShopId(),
                token.getTerminalId(),
                request.getDate());
        if (dbLogResult != null) {
            throw new BlazeInvalidArgException("CashDrawer", "There exists another cash drawer today.");
        }

        // Check cash drawer Date
        try {
            DateUtil.parseDateKey(request.getDate());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("CashDrawer Date", "Cash Drawer date must be in format 'yyyyMMdd'");
        }


        if (StringUtils.isBlank(token.getTerminalId())) {
            throw new BlazeInvalidArgException("TerminalId", "Please choose a valid terminal.");
        }

        CashDrawerSessionResult logResult = new CashDrawerSessionResult();
        logResult.prepare(token.getCompanyId());
        logResult.setTerminalId(token.getTerminalId());
        logResult.setShopId(token.getShopId());
        logResult.setStartEmployeeId(token.getActiveTopUser().getUserId());
        logResult.setStartTime(DateTime.now().getMillis());
        logResult.setStartCash(new BigDecimal(request.getStartAmount()));


        logResult.setDate(request.getDate());
        DateTime cdDate = DateUtil.parseDateKey(logResult.getDate(),shop.getTimeZone());
        logResult.setDateTimestamp(cdDate.plusMinutes(1).getMillis());

        // Process cash drawer
        cashDrawerProcessorService.processCurrentCashDrawer(shop, logResult, true);

        logResult.setStatus(CashDrawerSession.CashDrawerLogStatus.Open);
        cashDrawerSessionRepository.save(logResult);
        cashDrawerActivityLogService.createCashDrawerActivityType(null, logResult, CashDrawerSessionActivityLog.CashDrawerType.StartDrawer, logResult.getExpectedCash(), null);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);

        return logResult;
    }

    @Override
    public CashDrawerSessionResult endCashDrawer(String cdSessionId, EndDrawerRequest request) {
        CashDrawerSessionResult dbLogResult = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId, CashDrawerSessionResult.class);

        if (dbLogResult == null) {
            throw new BlazeInvalidArgException("CashDrawer", "Cash Drawer not found.");
        }

        if (request.getStartAmount() != null) {
            //dbLogResult.setStartCash(request.getStartAmount());
        }

        // Process cash drawer
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        cashDrawerProcessorService.processCurrentCashDrawer(shop, dbLogResult, false);


        // End info
        dbLogResult.setEndEmployeeId(token.getActiveTopUser().getUserId());
        dbLogResult.setActualCash(new BigDecimal(request.getActualCash()));
        dbLogResult.setComment(request.getComment());
        dbLogResult.setEndTime(DateTime.now().getMillis());
        dbLogResult.setStatus(CashDrawerSession.CashDrawerLogStatus.Closed);

        cashDrawerSessionRepository.update(token.getCompanyId(), dbLogResult.getId(), dbLogResult);
        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId);
        cashDrawerActivityLogService.createCashDrawerActivityType(null, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.EndDrawer, dbLogResult.getExpectedCash(), null);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);
        // Prepare for view
        prepareForView(Lists.newArrayList(dbLogResult));
        return dbLogResult;
    }


    @Override
    public CashDrawerSessionResult reopenCashDrawer(String cdSessionId) {
        CashDrawerSessionResult dbLogResult = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId, CashDrawerSessionResult.class);

        if (dbLogResult == null) {
            throw new BlazeInvalidArgException("CashDrawer", "Cash Drawer not found.");
        }

        dbLogResult.setStatus(CashDrawerSession.CashDrawerLogStatus.Open);
        cashDrawerSessionRepository.update(token.getCompanyId(), dbLogResult.getId(), dbLogResult);
        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId);
        cashDrawerActivityLogService.createCashDrawerActivityType(null, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.ReopenDrawer, dbLogResult.getExpectedCash(), null);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);

        prepareForView(Lists.newArrayList(dbLogResult));
        return dbLogResult;
    }

    @Override
    public CashDrawerSessionResult editCashDrawer(String cdSessionId, CashDrawerSession cashDrawerSession) {
        CashDrawerSessionResult dbLogResult = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId, CashDrawerSessionResult.class);

        if (dbLogResult == null) {
            throw new BlazeInvalidArgException("CashDrawer", "Cash Drawer not found.");
        }
        if (dbLogResult.getStatus() == CashDrawerSession.CashDrawerLogStatus.Open) {
            //dbLogResult.setStartCash(cashDrawerSession.getStartCash());
        } else {

            //dbLogResult.setStartCash(cashDrawerSession.getStartCash());
            dbLogResult.setActualCash(cashDrawerSession.getActualCash());
            dbLogResult.setComment(cashDrawerSession.getComment());

            // Get Total Refunds
            Iterable<PaidInOutItem> cashIOLogs = paidInOutItemRepository.getPaidInOuts(token.getCompanyId(),
                    token.getShopId(),
                    cdSessionId);
            double cashIns = 0.0;
            double cashOuts = 0.0;
            double cashDrops = 0.0;
            for (PaidInOutItem paidInOutItem : cashIOLogs) {
                if (paidInOutItem.getType() == PaidInOutItem.PaidInOutType.PaidIn) {
                    cashIns += paidInOutItem.getAmount().doubleValue();
                } else if (paidInOutItem.getType() == PaidInOutItem.PaidInOutType.PaidOut) {
                    cashOuts += paidInOutItem.getAmount().doubleValue();
                } else {
                    cashDrops += paidInOutItem.getAmount().doubleValue();
                }
            }

            dbLogResult.setTotalCashIn(new BigDecimal(NumberUtils.round(cashIns, 2)));
            dbLogResult.setTotalCashOut(new BigDecimal(NumberUtils.round(cashOuts, 2)));
            dbLogResult.setTotalCashDrop(new BigDecimal(NumberUtils.round(cashDrops, 2)));

            double expectedCash = dbLogResult.getStartCash().doubleValue()
                    + dbLogResult.getCashSales().doubleValue()
                    + dbLogResult.getCheckSales().doubleValue()
                    + dbLogResult.getTotalCashIn().doubleValue() - dbLogResult.getTotalCashOut().doubleValue() - dbLogResult.getTotalCashDrop().doubleValue();
            dbLogResult.setExpectedCash(new BigDecimal(expectedCash));

        }
        cashDrawerSessionRepository.update(token.getCompanyId(), dbLogResult.getId(), dbLogResult);
        cashDrawerActivityLogService.createCashDrawerActivityType(null, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.UpdateDrawer, cashDrawerSession.getExpectedCash(), null);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);
        // Prepare for viewfdsafsa
        prepareForView(Lists.newArrayList(dbLogResult));
        return dbLogResult;
    }

    @Override
    public CashDrawerSessionResult getCashDrawerLogById(String cdSessionId) {
        CashDrawerSessionResult logResult = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId, CashDrawerSessionResult.class);

        if (logResult == null) {
            throw new BlazeInvalidArgException("CashDrawer", "Cash Drawer not found.");
        }
        prepareForView(Lists.newArrayList(logResult));
        return logResult;
    }

    @Override
    public CashDrawerSessionResult refreshCashDrawer(String cdSessionId) {
        CashDrawerSessionResult dbLogResult = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId, CashDrawerSessionResult.class);

        if (dbLogResult == null) {
            throw new BlazeInvalidArgException("CashDrawer", "Cash Drawer not found.");
        }

        // Process cash drawer
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        cashDrawerProcessorService.processCurrentCashDrawer(shop, dbLogResult, false);

        // End info
        cashDrawerSessionRepository.update(token.getCompanyId(), dbLogResult.getId(), dbLogResult);
        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId);
        cashDrawerActivityLogService.createCashDrawerActivityType(null, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.RefreshDrawer, cashDrawerSession.getExpectedCash(), null);
        // Prepare for view
        prepareForView(Lists.newArrayList(dbLogResult));
        return dbLogResult;
    }


    private void prepareForView(List<CashDrawerSessionResult> values) {
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(token.getCompanyId());

        for (CashDrawerSessionResult log : values) {
            Employee startEmployee = employeeHashMap.get(log.getStartEmployeeId());
            Employee endEmployee = employeeHashMap.get(log.getEndEmployeeId());
            Terminal terminal = terminalHashMap.get(log.getTerminalId());
            log.setTerminal(terminal);
            log.setStartEmployee(startEmployee);
            log.setEndEmployee(endEmployee);
        }
    }


    @Override
    public DateSearchResult<PaidInOutItem> getPaidIOsByDate(long afterDate, long beforeDate) {

        if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            return paidInOutItemRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
        } else {
            return paidInOutItemRepository.getPaidInOutsTerminal(token.getCompanyId(), token.getShopId(), token.getTerminalId(), 0, 100);
        }
    }

    @Override
    public SearchResult<PaidInOutItem> getPaidIOs(String cdSessionId, int start, int limit) {
        return paidInOutItemRepository.findPaidInOuts(token.getCompanyId(), token.getShopId(), cdSessionId, start, limit);
    }

    @Override
    public PaidInOutItem enterPaidIO(String cdSessionId, PaidInOutRequest request) {
        if (request.getType() == null) {
            throw new BlazeInvalidArgException("CashDrop", "Please select a type.");
        }

        boolean exists = cashDrawerSessionRepository.exist(cdSessionId);
        if (!exists) {
            throw new BlazeInvalidArgException("CashDrawer", "Cash drawer does not exist.");
        }

        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId);
        if (cashDrawerSession == null) {
            throw new BlazeInvalidArgException("CashDrawerSession", "Cash Drawer session does not exist.");
        }

        PaidInOutItem log = new PaidInOutItem();
        log.prepare(token.getCompanyId());
        log.setShopId(token.getShopId());
        log.setCdSessionId(cdSessionId);
        log.setTerminalId(cashDrawerSession.getTerminalId());
        log.setEmployeeId(token.getActiveTopUser().getUserId());
        log.setAmount(request.getAmount());
        log.setReason(request.getReason());
        log.setCashVault(request.isCashVault());
        log.setType(request.getType());
        log.setToTerminalId(null);

        if (request.getType().equals(PaidInOutItem.PaidInOutType.CashDrop) && StringUtils.isBlank(request.getToTerminalId())) {
            throw new BlazeInvalidArgException("Cash Drop", "Please select a drop terminal");
        }

        if (request.getType().equals(PaidInOutItem.PaidInOutType.CashDrop) && log.getTerminalId().equalsIgnoreCase(request.getToTerminalId())) {
            throw new BlazeInvalidArgException("Cash Drop", "Please select a different terminal for Cash Drop");
        }

        Terminal terminal = terminalRepository.get(token.getCompanyId(), cashDrawerSession.getTerminalId());
        //Transfer paid out amount to another terminal latest drawer
        if (request.getType().equals(PaidInOutItem.PaidInOutType.CashDrop)) {

            log.setToTerminalId(request.getToTerminalId());

            PaidInOutItem otherLog = createOtherPaidIn(log, request.getToTerminalId(), terminal);
            if (otherLog != null) {
                log.setOtherPaidItemId(otherLog.getId());
            }
        }

        // persist if everything is successful
        paidInOutItemRepository.save(log);
        switch (log.getType()) {
            case PaidIn:
                cashDrawerActivityLogService.createCashDrawerActivityType(log, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.AddPaidIn, log.getAmount(), null);
                break;
            case PaidOut:
                cashDrawerActivityLogService.createCashDrawerActivityType(log, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.AddPaidOut, log.getAmount(), null);
                break;
            case CashDrop:
                cashDrawerActivityLogService.createCashDrawerActivityType(log, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.AddCashDrop, log.getAmount(), null);
                break;
        }
        //updateCashDrawerPaidInOut(cdSessionId);

        // Process cash drawer
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        cashDrawerProcessorService.processCurrentCashDrawer(shop, cashDrawerSession, false);
        cashDrawerSessionRepository.update(token.getCompanyId(), cashDrawerSession.getId(), cashDrawerSession);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);
        return log;
    }

    private PaidInOutItem createOtherPaidIn(PaidInOutItem log, String toTerminalId, Terminal fromTerminal) {
        Terminal toTerminal = terminalRepository.get(token.getCompanyId(), toTerminalId);
        if (toTerminal == null) {
            throw new BlazeInvalidArgException("Terminal", "To Terminal does not exists.");
        }

        CashDrawerSession dbLogOtherTerminal = cashDrawerSessionRepository.getLatestDrawerForTerminal(token.getCompanyId(), token.getShopId(), toTerminalId);
        if (dbLogOtherTerminal == null) {
            throw new BlazeInvalidArgException("Cash Drawer", "Cash drawer does not exist for drop terminal. Please start cash drawer for this terminal first.");
        }
        PaidInOutItem logOtherTerminal = new PaidInOutItem();
        try {
            BeanUtils.copyProperties(logOtherTerminal, log);
            logOtherTerminal.setId(null);
            logOtherTerminal.prepare(token.getCompanyId());
            logOtherTerminal.setTerminalId(toTerminalId);
            logOtherTerminal.setType(PaidInOutItem.PaidInOutType.PaidIn);
            logOtherTerminal.setCdSessionId(dbLogOtherTerminal.getId());
            logOtherTerminal.setCashDropFromTerminalId(fromTerminal.getId());
            logOtherTerminal.setReason("Cash drop from " + fromTerminal.getName());
        } catch (Exception e) {
            LOGGER.error("Error while copying log into logOtherTerminal");
            e.printStackTrace();
        }

        paidInOutItemRepository.save(logOtherTerminal);

        // Process cash drawer
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        cashDrawerProcessorService.processCurrentCashDrawer(shop, dbLogOtherTerminal, false);

        return logOtherTerminal;
    }

    @Override
    public PaidInOutItem editPaidIO(String cashIOItemId, PaidInOutItem request) {
        PaidInOutItem dbCashLog = paidInOutItemRepository.get(token.getCompanyId(), cashIOItemId);
        if (dbCashLog == null) {
            throw new BlazeInvalidArgException("PaidInOutItem", "PaidInOutItem does not exist.");
        }
        dbCashLog.setAmount(request.getAmount());
        dbCashLog.setReason(request.getReason());
        dbCashLog.setType(request.getType());
        dbCashLog.setEmployeeId(token.getActiveTopUser().getUserId());
        dbCashLog.setCashVault(request.isCashVault());

        Terminal terminal = terminalRepository.get(token.getCompanyId(), dbCashLog.getTerminalId());

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (dbCashLog.getType().equals(PaidInOutItem.PaidInOutType.CashDrop) && StringUtils.isNotBlank(dbCashLog.getToTerminalId())) {

            if (dbCashLog.getToTerminalId().equalsIgnoreCase(request.getToTerminalId())) {

                // do an update if the to cash drawer is same
                PaidInOutItem otherDBLog = paidInOutItemRepository.get(token.getCompanyId(), dbCashLog.getOtherPaidItemId());
                if (otherDBLog != null) {
                    otherDBLog.setAmount(request.getAmount());

                    // Process cash drawer
                    paidInOutItemRepository.update(token.getCompanyId(), otherDBLog.getId(), otherDBLog);

                    CashDrawerSession otherDrawer = cashDrawerSessionRepository.get(token.getCompanyId(), otherDBLog.getCdSessionId());
                    cashDrawerProcessorService.processCurrentCashDrawer(shop, otherDrawer, false);
                }
            } else {
                // delete the previous one and add a new one if cash drawer is different
                try {
                    deletePaidIO(dbCashLog.getOtherPaidItemId());
                } catch (Exception e) {
                    // ignore any errors
                }

                PaidInOutItem otherPaid = createOtherPaidIn(dbCashLog, request.getToTerminalId(), terminal);
                if (otherPaid != null) {
                    dbCashLog.setOtherPaidItemId(otherPaid.getId());
                }
            }
        } else {
            // previously was not a cashdrop
            if (request.getType() == PaidInOutItem.PaidInOutType.CashDrop) {
                // new one is cash drop, so just create corresponding one
                PaidInOutItem otherPaid = createOtherPaidIn(dbCashLog, request.getToTerminalId(), terminal);
                if (otherPaid != null) {
                    dbCashLog.setOtherPaidItemId(otherPaid.getId());
                }
            }

        }

        dbCashLog.setToTerminalId(request.getToTerminalId());
        dbCashLog.setType(request.getType());
        paidInOutItemRepository.update(token.getCompanyId(), dbCashLog.getId(), dbCashLog);
        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), dbCashLog.getCdSessionId());
        switch (dbCashLog.getType()) {
            case PaidIn:
                cashDrawerActivityLogService.createCashDrawerActivityType(dbCashLog, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.UpdatePaidIn, dbCashLog.getAmount(), null);
                break;
            case PaidOut:
                cashDrawerActivityLogService.createCashDrawerActivityType(dbCashLog, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.UpdatePaidOut, dbCashLog.getAmount(), null);
                break;
            case CashDrop:
                cashDrawerActivityLogService.createCashDrawerActivityType(dbCashLog, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.UpdateCashDrop, dbCashLog.getAmount(), null);
                break;
        }

        //updateCashDrawerPaidInOut(dbCashLog.getCdSessionId());

        //CashDrawerSession otherDrawer = cashDrawerSessionRepository.get(token.getCompanyId(), otherDBLog.getCdSessionId());
        cashDrawerProcessorService.processCurrentCashDrawer(shop, cashDrawerSession, false);
        cashDrawerSessionRepository.update(token.getCompanyId(), cashDrawerSession.getId(), cashDrawerSession);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);
        return dbCashLog;
    }


    @Override
    public void deletePaidIO(String cashIOItemId) {
        PaidInOutItem dbCashLog = paidInOutItemRepository.get(token.getCompanyId(), cashIOItemId);
        if (dbCashLog == null) {
            throw new BlazeInvalidArgException("PaidInOutItem", "PaidInOutItem does not exist.");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (dbCashLog.getType().equals(PaidInOutItem.PaidInOutType.CashDrop)) {
            PaidInOutItem paidInItem = paidInOutItemRepository.get(token.getCompanyId(), dbCashLog.getOtherPaidItemId());
            paidInOutItemRepository.removeById(token.getCompanyId(), paidInItem.getId());

            CashDrawerSession otherDrawer = cashDrawerSessionRepository.get(token.getCompanyId(), paidInItem.getCdSessionId());
            cashDrawerProcessorService.processCurrentCashDrawer(shop, otherDrawer, false);
        }

        paidInOutItemRepository.removeById(token.getCompanyId(), cashIOItemId);
        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), dbCashLog.getCdSessionId());

        switch (dbCashLog.getType()) {
            case PaidIn:
                cashDrawerActivityLogService.createCashDrawerActivityType(dbCashLog, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.DeletePaidIn, dbCashLog.getAmount(), null);
                break;
            case PaidOut:
                cashDrawerActivityLogService.createCashDrawerActivityType(dbCashLog, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.DeletePaidOut, dbCashLog.getAmount(), null);
                break;
            case CashDrop:
                cashDrawerActivityLogService.createCashDrawerActivityType(dbCashLog, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.DeleteCashDrop, dbCashLog.getAmount(), null);
                break;
        }

        cashDrawerProcessorService.processCurrentCashDrawer(shop, cashDrawerSession, false);
    }

    @Override
    public CashDrawerSessionResult updateStartingCash(String cdSessionId, DrawerStartCashUpdateRequest startCashUpdateRequest) {
        CashDrawerSessionResult dbLogResult = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId, CashDrawerSessionResult.class);
        if (dbLogResult == null) {
            throw new BlazeInvalidArgException("Cash Drawer", "Cash drawer does not found");
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        Role role = roleRepository.get(token.getCompanyId(), employee.getRoleId());
        if (!role.getName().equalsIgnoreCase("Manager")
                && !role.getName().equalsIgnoreCase("Admin")
                && !role.getName().equalsIgnoreCase("Shop Manager")) {
            throw new BlazeInvalidArgException("Invalid Member", "You are not authorized to perform this operation");
        }

        if (!dbLogResult.getStatus().equals(CashDrawerSession.CashDrawerLogStatus.Open))
            throw new BlazeInvalidArgException("Cash Drawer Status", "Cash drawer is not open");

        dbLogResult.setStartCash(new BigDecimal(startCashUpdateRequest.getStartCashAmount()));

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        cashDrawerProcessorService.processCurrentCashDrawer(shop, dbLogResult, false);

        cashDrawerSessionRepository.update(token.getCompanyId(), cdSessionId, dbLogResult);
        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId);
        cashDrawerActivityLogService.createCashDrawerActivityType(null, cashDrawerSession, CashDrawerSessionActivityLog.CashDrawerType.UpdateStartingCash, dbLogResult.getStartCash(), null);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);

        return dbLogResult;
    }


    @Override
    public String getPaidInOutItemReceipt(String cdSessionId, String paidIOItemId, int width) {
        int length;
        if (width == 39 || width == 32) {
            length = width;
        } else {
            length = 32;
        }


        Company currentCompany = companyService.getCurrentCompany();
        if (currentCompany == null) {
            throw new BlazeInvalidArgException("Company", "Company is not found");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }

        CashDrawerSession cashDrawerSession = cashDrawerSessionRepository.get(token.getCompanyId(), cdSessionId);

        if (cashDrawerSession == null) {
            throw new BlazeInvalidArgException(CASH_DRAWER, CASH_DRAWER_NOT_FOUND);
        }

        PaidInOutItem paidInOutItem = paidInOutItemRepository.get(token.getCompanyId(), paidIOItemId);

        if (paidInOutItem == null) {
            throw new BlazeInvalidArgException(CASH_DRAWER, PAID_IO_NOT_FOUND);
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), cashDrawerSession.getStartEmployeeId());

        DateTime dt = new DateTime();

        String date = DateUtil.toDateFormatted(dt.getMillis(), shop.getTimeZone());
        String time = DateUtil.toTimeFormatted(dt.getMillis(), shop.getTimeZone());
        String dateTime = date.concat(",").concat(" ").concat(time);

        StringBuilder sb = new StringBuilder();

        String cashDrawerDate = DateUtil.toDateFormatted(cashDrawerSession.getStartTime(), shop.getTimeZone());

        String employeeName = "N/A";
        if (employee != null) {
            employeeName = String.format("%s %s", employee.getFirstName(), employee.getLastName().substring(0, 1));
        }

        String toTerminalName = "N/A";
        String type = "N/A";
        if (PaidInOutItem.PaidInOutType.CashDrop == paidInOutItem.getType()) {
            type = "Cash drop";
            Terminal toTerminal = terminalRepository.get(token.getCompanyId(), paidInOutItem.getToTerminalId());

            if (toTerminal != null) {
                toTerminalName = toTerminal.getName();
            }
        } else if (PaidInOutItem.PaidInOutType.PaidIn == paidInOutItem.getType()) {
            type = "Paid in";
        } else if (PaidInOutItem.PaidInOutType.PaidOut == paidInOutItem.getType()) {
            type = "Paid out";
        }

        Terminal terminal = terminalRepository.get(token.getCompanyId(), cashDrawerSession.getTerminalId());
        String terminalName = "N/A";
        if (terminal != null) {
            terminalName = terminal.getName();
        }

        String reason = "N/A";
        if (StringUtils.isNotBlank(paidInOutItem.getReason())) {
            reason = paidInOutItem.getReason();
        }

        sb.append(ReceiptUtil.getCommonItem("Shop", shop.getName(), length)).append("\n");
        sb.append(ReceiptUtil.getCommonItem("Date", dateTime, length)).append("\n");
        sb.append(ReceiptUtil.getCommonItem("Cash Drawer Date", cashDrawerDate, length)).append("\n\n");
        sb.append(ReceiptUtil.getCommonItem("Executed By", employeeName, length)).append("\n");
        sb.append(ReceiptUtil.getCommonItem("Type", type, length)).append("\n\n");
        sb.append(ReceiptUtil.getCommonItem("Terminal", terminalName, length)).append("\n");
        sb.append(ReceiptUtil.getCommonItem("Amount", TextUtil.toCurrency(paidInOutItem.getAmount().doubleValue(), shop.getDefaultCountry()), length)).append("\n");

        if (PaidInOutItem.PaidInOutType.CashDrop == paidInOutItem.getType()) {
            sb.append(ReceiptUtil.getCommonItem("To Terminal", toTerminalName, length)).append("\n");
        }


        String line = repeatContent(length, "", "-", Boolean.TRUE);
        sb.append("\n");
        sb.append(ReceiptUtil.getCommonItem("Reason", reason, length));

        boolean shouldAppendText = true;
        if (length <= 32) {
            shouldAppendText = false;
        }
        sb.append("\n").append("\n");
        if (shouldAppendText) {
            sb.append(line);
            sb.append("\n").append("\n");
        }
        return sb.toString();
    }

    @Override
    public  SearchResult<CashDrawerSessionResult> getCurrentActiveCashDrawer() {
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        // Use assignedTerminalId
        String assignedTerminalId = employee.getAssignedTerminalId();
        if (StringUtils.isBlank(assignedTerminalId)) {
            assignedTerminalId = token.getTerminalId(); // use current logged in terminal
        }

        SearchResult<CashDrawerSessionResult> result = cashDrawerSessionRepository.getOpenDrawersForTerminal(token.getCompanyId(), token.getShopId(), assignedTerminalId, CashDrawerSessionResult.class);
        if (result != null) {
            prepareForView(result.getValues());
        }
        return result;
    }


}
