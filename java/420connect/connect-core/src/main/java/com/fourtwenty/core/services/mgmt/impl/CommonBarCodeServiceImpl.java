package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.BarcodeItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyUniqueSequenceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.services.mgmt.CommonBarCodeService;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

public class CommonBarCodeServiceImpl implements CommonBarCodeService {

    private static final Log LOG = LogFactory.getLog(BarcodeServiceImpl.class);

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private BarcodeItemRepository barcodeItemRepository;
    @Inject
    private CompanyUniqueSequenceRepository sequenceRepository;

    @Override
    public BarcodeItem createBarcodeItemIfNeeded(String companyId, String shopId, String productId, BarcodeItem.BarcodeEntityType entityType, String entityId, String currentKey, String oldKey, boolean transferShop) {
        BarcodeItem item = null;
        if (oldKey != null) {
            BarcodeItem oldItem = barcodeItemRepository.getBarcode(companyId, shopId, entityType, oldKey);
            if (oldItem != null) {
                if (StringUtils.isNotBlank(oldKey) && currentKey.equalsIgnoreCase(oldKey) && oldItem.getEntityId().equalsIgnoreCase(entityId)) {
                    return oldItem; // same key so just return it. we don't have to create a new one
                }

                // delete old key so it can be re-used
                oldItem.setDeleted(true);
                oldItem.setBarcode(String.format("%s-del-%d", oldItem.getBarcode(), DateTime.now().getMillis()));
                barcodeItemRepository.update(companyId, oldItem.getId(), oldItem);
            }

        }
        //If current key matched with deleted barcode
        BarcodeItem deletedItem = barcodeItemRepository.getDeletedBarcode(companyId, shopId, entityType, currentKey);
        if (deletedItem != null) {
            deletedItem.setDeleted(false);
            if (!deletedItem.getProductId().equals(productId)) {
                LOG.info(String.format("Updating productId for %s to %s", currentKey, productId));
                deletedItem.setProductId(productId);
            }
            if (!deletedItem.getEntityId().equals(entityId)) {
                LOG.info(String.format("Updating entityId for %s to %s", currentKey, entityId));
                deletedItem.setEntityId(entityId);
            }
            LOG.info("Found old barcode -- reusing: " + currentKey);
            barcodeItemRepository.update(companyId, deletedItem.getId(), deletedItem);
            LOG.info("Found old barcode -- reusing: success" + currentKey);
            return deletedItem;
        }

        // test new key
        item = barcodeItemRepository.getBarcode(companyId, shopId, entityType, currentKey);
        if (item != null) {
            // If not the same entityType, then throw an exception stating that it's being used by another product/batch
            if (!item.getEntityId().equalsIgnoreCase(entityId)) {
                String productName = "";
                Product otherProduct = productRepository.get(companyId, item.getProductId());

                if (otherProduct != null) {
                    productName = otherProduct.getName();
                }
                if (transferShop
                        && entityType == BarcodeItem.BarcodeEntityType.Batch
                        && item.getProductId().equalsIgnoreCase(productId)) {
                    // ignore if you're transferring to another shop and the barcode already exists for this same product
                } else {
                    Shop shop = shopRepository.get(companyId, shopId);
                    if (shop == null) {
                        throw new BlazeInvalidArgException("Shop", "Shop not found");
                    }

                    if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) {
                        throw new BlazeInvalidArgException("Barcode",
                                String.format("Unique ID '%s' is currently used by another %s for product '%s'.", currentKey, entityType.name(), productName));
                    } else {
                        throw new BlazeInvalidArgException("Barcode",
                                String.format("SKU '%s' is currently used by another %s for product '%s'.", currentKey, entityType.name(), productName));
                    }
                }
            }

            if (!item.getProductId().equalsIgnoreCase(productId)) {
                item.setProductId(productId);
                barcodeItemRepository.update(companyId, item.getId(), item);
            }
            return item;
        }
        if (StringUtils.isBlank(currentKey)) {
            return createBarcodeItem(companyId, shopId, productId, entityType, entityId);
        }
        currentKey = currentKey.toUpperCase();
        CompanyUniqueSequence uniqueSequence = sequenceRepository.getNewIdentifier(companyId, currentKey);

        item = new BarcodeItem();
        item.prepare(companyId);
        item.setShopId(shopId);
        item.setEntityType(entityType);
        item.setEntityId(entityId);
        item.setBarcode(currentKey);
        item.setProductId(productId);
        item.setNumber((int) uniqueSequence.getCount());

        // Save new barcode with the SKU
        barcodeItemRepository.save(item);
        return item;
    }

    private BarcodeItem createBarcodeItem(String companyId, String shopId, String productId, final BarcodeItem.BarcodeEntityType entityType, final String entityId) {
        if (StringUtils.isBlank(productId)) {
            productId = entityId;
        }


        BarcodeItem item = new BarcodeItem();
        item.prepare(companyId);
        item.setShopId(shopId);
        item.setEntityType(entityType);
        item.setEntityId(entityId);
        item.setProductId(productId);

        Company company = companyRepository.getById(companyId);
        String substr = "";
        if (company.getName().length() >= 2) {
            substr = company.getName().substring(0, 1);
        }
        String idstr = productId.substring(entityId.length() - 3, productId.length()).toUpperCase();
        /// 3 digits name -  product-category -- product-type -- product name -- sku number
        /// 2 - 3 - 3 - 4
        /// AB-AB3-PRO-10
        String key = String.format("%s-%s", idstr, entityType.getCode()).toUpperCase();
        CompanyUniqueSequence uniqueSequence = sequenceRepository.getNewIdentifier(companyId, key);

        String sku = String.format("%s%s%s%d", substr, idstr, entityType.getCode(), uniqueSequence.getCount());
        item.setNumber((int) uniqueSequence.getCount());
        item.setBarcode(sku);
        barcodeItemRepository.save(item);

        return item;
    }
}
