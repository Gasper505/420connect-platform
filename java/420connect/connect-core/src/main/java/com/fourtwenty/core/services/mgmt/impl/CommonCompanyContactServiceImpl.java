package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.company.CompanyContactLog;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyContactLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyContactRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;
import com.fourtwenty.core.services.mgmt.CommonCompanyContactService;
import com.fourtwenty.core.services.mgmt.CommonVendorService;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.bson.types.ObjectId;

public class CommonCompanyContactServiceImpl implements CommonCompanyContactService {

    private static final String CUSTOMER_COMPANY = "Customer company";
    private static final String CONTACT = "Contact";
    private static final String CUSTOMER_COMPANY_NOT_FOUND = "Customer company is not found.";
    private static final String CONTACT_NOT_FOUND = "Contact is not found.";
    private static final String VENDOR_NOT_VALID = "Vendor is not valid.";

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private CommonVendorService commonVendorService;
    @Inject
    private CompanyContactRepository companyContactRepository;
    @Inject
    private CompanyContactLogRepository companyContactLogRepository;

    @Override
    public CompanyContact createCompanyContact(String companyId, String shopId, CompanyContact request, String employeeId, String employeeName) {
        if (StringUtils.isBlank(request.getCustomerCompanyId()) || !ObjectId.isValid(request.getCustomerCompanyId())) {
            throw new BlazeInvalidArgException(CONTACT, VENDOR_NOT_VALID);
        }
        Vendor customerCompany = vendorRepository.getById(request.getCustomerCompanyId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        if (StringUtils.isBlank(request.getFirstName()) && StringUtils.isBlank(request.getLastName())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "First Name and Last Name can't be blank.");
        }

        if (StringUtils.isNotBlank(request.getEmail())) {
            if (!EmailValidator.getInstance().isValid(request.getEmail())) {
                throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
            }

            CompanyContact contactByEmail = companyContactRepository.getCompanyContactByEmail(request.getEmail());

            if (contactByEmail != null) {
                throw new BlazeQuickPinExistException("DuplicateEmail", "Another contact exists with this email.");
            }
        }

        CompanyAsset userPhoto = request.getUserPhoto();
        if (userPhoto != null) {
            userPhoto.prepare();
        }
        Note note = request.getNote();
        if (note != null && StringUtils.isNotBlank(note.getMessage())) {
            note.prepare();
            note.setWriterId(employeeId);
            note.setWriterName(employeeName);
        }

        if (request.getAssets() != null) {
            for (CompanyAsset companyAsset : request.getAssets()) {
                companyAsset.prepare();
            }
        }

        CompanyContact companyContact = new CompanyContact();
        companyContact.prepare(companyId);
        companyContact.setSalutation(request.getSalutation());
        companyContact.setFirstName(request.getFirstName());
        companyContact.setLastName(request.getLastName());
        companyContact.setEmail(request.getEmail());
        companyContact.setRole(request.getRole());
        companyContact.setPhoneNumber(request.getPhoneNumber());
        companyContact.setUserPhoto(userPhoto);
        companyContact.setNote(note);
        companyContact.setOfficeNumber(request.getOfficeNumber());
        companyContact.setCustomerCompanyId(request.getCustomerCompanyId());
        companyContact.setAssets(request.getAssets());
        CompanyContact dbCompanyContact = companyContactRepository.save(companyContact);
        if (dbCompanyContact != null) {
            prepareCompanyContactLogs(companyId, shopId, dbCompanyContact.getCustomerCompanyId(), dbCompanyContact.getId(), "Contact added by ",
                    employeeId, employeeName);
        }
        return dbCompanyContact;
    }

    @Override
    public CompanyContact updateCompanyContact(String companyId, String shopId, String contactId, CompanyContact request, String employeeId, String employeeName) {
        if (StringUtils.isBlank(contactId)) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }
        CompanyContact dbCompanyContact = companyContactRepository.getById(contactId);
        if (dbCompanyContact == null) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }

        if (StringUtils.isBlank(request.getCustomerCompanyId()) || !ObjectId.isValid(request.getCustomerCompanyId())) {
            throw new BlazeInvalidArgException(CONTACT, VENDOR_NOT_VALID);
        }

        Vendor customerCompany = vendorRepository.getById(request.getCustomerCompanyId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        if (StringUtils.isNotBlank(request.getEmail())) {
            if (!EmailValidator.getInstance().isValid(request.getEmail())) {
                throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
            }

            if (!dbCompanyContact.getEmail().equalsIgnoreCase(request.getEmail())) {
                CompanyContact contactByEmail = companyContactRepository.getCompanyContactByEmail(request.getEmail());

                if (contactByEmail != null) {
                    throw new BlazeQuickPinExistException("DuplicateEmail", "Another contact exists with this email.");
                }
            }
        }
        CompanyAsset userPhoto = request.getUserPhoto();
        if (userPhoto != null && StringUtils.isNotBlank(userPhoto.getId())) {
            userPhoto.prepare();
        }

        Note note = request.getNote();
        if (note != null && StringUtils.isNotBlank(note.getMessage()) && StringUtils.isBlank(note.getId())) {
            note.prepare();
            note.setWriterId(employeeId);
            note.setWriterName(employeeName);
        } else if (note != null && StringUtils.isNotBlank(note.getMessage())) {
            note.setWriterId(employeeId);
            note.setWriterName(employeeName);
        }

        if (request.getAssets() != null) {
            for (CompanyAsset companyAsset : request.getAssets()) {
                if (StringUtils.isBlank(companyAsset.getId())) {
                    companyAsset.prepare();
                }
            }
        }

        dbCompanyContact.setSalutation(request.getSalutation());
        dbCompanyContact.setFirstName(request.getFirstName());
        dbCompanyContact.setLastName(request.getLastName());
        dbCompanyContact.setEmail(request.getEmail());
        dbCompanyContact.setRole(request.getRole());
        dbCompanyContact.setPhoneNumber(request.getPhoneNumber());
        dbCompanyContact.setUserPhoto(userPhoto);
        dbCompanyContact.setNote(note);
        dbCompanyContact.setOfficeNumber(request.getOfficeNumber());
        dbCompanyContact.setCustomerCompanyId(request.getCustomerCompanyId());
        dbCompanyContact.setAssets(request.getAssets());

        CompanyContact updatedContact = companyContactRepository.update(contactId, dbCompanyContact);
        if (updatedContact != null) {
            prepareCompanyContactLogs(companyId, shopId, updatedContact.getCustomerCompanyId(), updatedContact.getId(), "Contact updated by ", employeeId, employeeName);
        }
        return updatedContact;
    }

    @Override
    public CompanyContactResult getCompanyContactById(String companyId, String contactId) {
        if (StringUtils.isBlank(contactId)) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }
        CompanyContactResult contactResult = companyContactRepository.getCompanyContactById(contactId, CompanyContactResult.class);

        if (contactResult == null) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }

        this.prepareCompanyContactResult(companyId, contactResult);
        return contactResult;
    }

    @Override
    public SearchResult<CompanyContact> getCompanyContacts(String companyId, String shopId, String startDate, String endDate, int skip, int limit) {
        SearchResult<CompanyContact> searchResult = null;
        commonVendorService.checkCompanyAvailability(companyId);
        Shop shop = commonVendorService.checkShopAvailability(companyId, shopId);

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        if (skip < 0) {
            skip = 0;
        }

        if (StringUtils.isBlank(endDate) || StringUtils.isBlank(startDate)) {
            searchResult = companyContactRepository.findItems(companyId, skip, limit);
        } else {
            long timeZoneStartDateMillis = commonVendorService.getStartDate(shop, startDate);
            long timeZoneEndDateMillis = commonVendorService.getEndDate(shop, endDate);

            searchResult = companyContactRepository.findItemsWithDate(companyId, timeZoneStartDateMillis, timeZoneEndDateMillis, skip, limit, "{modified:-1}", CompanyContact.class);
        }

        return searchResult;
    }

    private void prepareCompanyContactResult(String companyId, CompanyContactResult contact) {
        Vendor customerCompany = vendorRepository.get(companyId, contact.getCustomerCompanyId());
        contact.setCustomerCompany(customerCompany);
    }

    private void prepareCompanyContactLogs(String companyId, String shopId, String customerCompanyId, String companyContactId, String log, String employeeId, String employeeName) {

        CompanyContactLog companyContactLog = new CompanyContactLog();
        companyContactLog.prepare(companyId);
        companyContactLog.setShopId(shopId);
        companyContactLog.setCompanyContactId(companyContactId);
        companyContactLog.setCustomerCompanyId(customerCompanyId);
        companyContactLog.setEmployeeId(employeeId);
        companyContactLog.setLog(log + employeeName);
        companyContactLogRepository.save(companyContactLog);
    }
}
