package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompoundTaxRate;
import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.BundleItem;
import com.fourtwenty.core.domain.models.product.MemberGroupPrices;
import com.fourtwenty.core.domain.models.product.PotencyMG;
import com.fourtwenty.core.domain.models.product.PricingTemplate;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MedicalConditionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupPricesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PricingTemplateRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductCustomResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductResult;
import com.fourtwenty.core.rest.store.webhooks.ProductData;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.CommonBarCodeService;
import com.fourtwenty.core.services.mgmt.CommonProductService;
import com.fourtwenty.core.services.thirdparty.WeedmapService;
import com.fourtwenty.core.tasks.MigrateProductsToPublicTask;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public class CommonProductServiceImpl implements CommonProductService {

    private static final Logger LOG = LoggerFactory.getLogger(CommonProductServiceImpl.class);

    private static final String PRODUCT = "Product";
    private static final String VENDOR = "Vendor";
    private static final String PRODUCT_NOT_FOUND = "Product is not found";
    private static final String VENDOR_NOT_FOUND = "Vendor is not found";
    private static final String BRAND_NOT_FOUND = "Brand is not found";
    private static final String PRODUCT_NAME_EXISTS = "Another Product with this same name exists";
    private static final String PRODUCT_CATEGORY = "Product Category";
    private static final String PRODUCT_CATEGORY_NOT_FOUND = "Product category is not found";
    private static final String NOT_VALID_BUNDLE_PRODUCT = "Please select valid product for creating bundle product";
    private static final String BUNDLE_PRODUCT_USE = "Bundle type product can't be used in creation of bundle product";
    private static final String SECONDARY_VENDOR_NOT_FOUND = "Secondary vendor does not exists.";

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private WeedmapService weedmapService;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private CommonBarCodeService commonBarCodeService;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private BackgroundTaskManager backgroundTaskManager;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private PricingTemplateRepository pricingTemplateRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private MedicalConditionRepository medicalConditionRepository;
    @Inject
    private MemberGroupPricesRepository groupPricesRepository;
    @Inject
    private MigrateProductsToPublicTask migrateProductsToPublicTask;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;


    @Override
    public Product getProductById(String companyId, String shopId, String productId) {
        ProductResult product = productRepository.get(companyId, productId, ProductResult.class);
        if (product == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_FOUND);
        }

        SearchResult<Product> results = new SearchResult<>();
        results.getValues().add(product);
        assignedProductDependencies(product, shopId, companyId);

        ProductCategory productCategory = productCategoryRepository.get(companyId, product.getCategoryId());

        if (productCategory != null) {
            // Populate the priceRanges
            if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
                List<ProductPriceRange> ranges = populatePriceRanges(product, product.getPriceRanges(), "default", null, companyId);
                product.setPriceRanges(ranges);
                product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            } else {
                List<ProductPriceBreak> productPriceBreaks = populatePriceBreaks(product, productCategory, product.getPriceBreaks(), true,
                        null, false, Boolean.FALSE, companyId);
                product.setPriceBreaks(productPriceBreaks);
                product.setPriceRanges(new ArrayList<ProductPriceRange>());
            }
        }
        Brand brand = brandRepository.get(companyId, product.getBrandId());
        if (brand != null) {
            product.setBrandName(brand.getName());
        }


        if (product.getTaxTables() == null
                || product.getTaxTables().size() == 0) {
            Shop shop = shopRepository.get(companyId, shopId);
            if (shop != null) {
                if (shop.isUseComplexTax()) {
                    List<CompoundTaxTable> taxes = new ArrayList<>();
                    for (CompoundTaxTable taxTable : shop.getTaxTables()) {
                        taxTable.setId(null);
                        taxTable.prepare(companyId);
                        taxes.add(taxTable);
                        taxTable.setTaxType(TaxInfo.TaxType.Custom);
                        taxTable.setActive(false);

                        taxTable.setCityTax(new CompoundTaxRate());
                        taxTable.setCountyTax(new CompoundTaxRate());
                        taxTable.setStateTax(new CompoundTaxRate());
                        taxTable.setFederalTax(new CompoundTaxRate());

                        taxTable.getCityTax().prepare(companyId);
                        taxTable.getCityTax().setShopId(shopId);

                        taxTable.getCountyTax().prepare(companyId);
                        taxTable.getCountyTax().setShopId(shopId);

                        taxTable.getStateTax().prepare(companyId);
                        taxTable.getStateTax().setShopId(shopId);

                        taxTable.getFederalTax().prepare(companyId);
                        taxTable.getFederalTax().setShopId(shopId);
                        taxTable.reset();
                    }
                    product.setTaxTables(taxes);
                }
            }
        }
        if (!CollectionUtils.isNullOrEmpty(product.getSecondaryVendors())) {
            List<ObjectId> vendorObjIds = new ArrayList<>();
            for (String vendorId : product.getSecondaryVendors()) {
                if (StringUtils.isNotBlank(vendorId) && ObjectId.isValid(vendorId)) {
                    vendorObjIds.add(new ObjectId(vendorId));
                }
            }
            HashMap<String, Vendor> secondaryVendorsMap = vendorRepository.listAsMap(companyId, vendorObjIds);
            product.setSecondaryVendorResult(Lists.newArrayList(secondaryVendorsMap.values()));
        }
        return product;
    }

    @Override
    public Product addProduct(String companyId, String shopId, ProductAddRequest addRequest) {
        Vendor vendor = vendorRepository.get(companyId, addRequest.getVendorId());
        if ((Product.ProductType.REGULAR == addRequest.getProductType() && vendor == null) ||
                (Product.ProductType.DERIVED == addRequest.getProductType() && StringUtils.isNotBlank(addRequest.getVendorId()) && vendor == null)) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }
        if (!CollectionUtils.isNullOrEmpty(addRequest.getSecondaryVendors())) {
            List<ObjectId> vendorObjIds = new ArrayList<>();
            for (String vendorId : addRequest.getSecondaryVendors()) {
                if (ObjectId.isValid(vendorId)) {
                    vendorObjIds.add(new ObjectId(vendorId));
                }
            }
            HashMap<String, Vendor> secondaryVendorsMap = vendorRepository.listAsMap(companyId, vendorObjIds);
            if (secondaryVendorsMap.isEmpty()) {
                throw new BlazeInvalidArgException(VENDOR, SECONDARY_VENDOR_NOT_FOUND);
            }
            addRequest.setSecondaryVendors(secondaryVendorsMap.keySet());
        }
        long result = 0;
        if (StringUtils.isNotBlank(addRequest.getName())) {
            result = productRepository.getProductsByName(companyId, shopId, addRequest.getName(), addRequest.getVendorId(), addRequest.getCategoryId(), addRequest.getFlowerType(), addRequest.getCannabisType());
        }
        if (result > 0) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NAME_EXISTS);
        }

        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }

        ProductCategory category = productCategoryRepository.get(companyId, addRequest.getCategoryId());
        if (category == null) {
            throw new BlazeInvalidArgException(PRODUCT_CATEGORY, PRODUCT_CATEGORY_NOT_FOUND);
        }

        Product product = new Product();
        product.prepare(companyId);
        product.setVendorId(addRequest.getVendorId());
        product.setName(addRequest.getName());
        product.setCategoryId(addRequest.getCategoryId());
        product.setShopId(shopId);
        product.setUnitPrice(addRequest.getUnitPrice());
        product.setSku(addRequest.getSku());
        product.setDescription(addRequest.getDescription());
        product.setPriceRanges(addRequest.getPriceRanges());
        product.setFlowerType(addRequest.getFlowerType());
        product.setGenetics(addRequest.getGenetics());
        product.setEnableMixMatch(addRequest.isEnableMixMatch());
        product.setLowThreshold(addRequest.getLowThreshold());
        product.setLowInventoryNotification(addRequest.isLowInventoryNotification());
        product.setShowInWidget(addRequest.isShowInWidget());
        product.setDiscountable(true);
        product.setMedicinal(addRequest.isMedicinal());
        product.setByGram(addRequest.isByGram());
        product.setByPrepackage(addRequest.isByPrepackage());
        product.setProductSaleType(addRequest.getProductSaleType());
        product.setTags(addRequest.getTags());
        product.setEnableExciseTax(addRequest.isEnableExciseTax());
        product.setCannabisType(addRequest.getCannabisType());
        product.setActive(addRequest.isActive());
        product.setPriceIncludesExcise(addRequest.isPriceIncludesExcise());
        product.setPriceIncludesALExcise(addRequest.isPriceIncludesALExcise());
        product.setAutomaticReOrder(addRequest.isAutomaticReOrder());
        product.setReOrderLevel(addRequest.getReOrderLevel());
        product.setProductType(addRequest.getProductType());

        if (vendor != null) {
            if (Vendor.ArmsLengthType.ARMS_LENGTH == vendor.getArmsLengthType()) {
                product.setPriceIncludesALExcise(shop.isProductPriceIncludeExciseTax());
                product.setPriceIncludesExcise(Boolean.FALSE);
            } else if (Vendor.ArmsLengthType.NON_ARMS_LENGTH == vendor.getArmsLengthType()) {
                product.setPriceIncludesExcise(shop.isProductPriceIncludeExciseTax());
                product.setPriceIncludesALExcise(Boolean.FALSE);
            }
        }

        if (StringUtils.isNotBlank(addRequest.getBrandId())) {

            if (vendor.getBrands().contains(addRequest.getBrandId())) {
                product.setBrandId(addRequest.getBrandId());
            } else {
                throw new BlazeInvalidArgException("Brand", BRAND_NOT_FOUND);
            }

        }
        if (StringUtils.isNotBlank(addRequest.getSku())) {
            Product productBySku = productRepository.findProductBySku(companyId, shopId, addRequest.getSku());
            if (productBySku != null) {
                throw new BlazeInvalidArgException("Barcode",
                        String.format("SKU '%s' is currently used by another product .", addRequest.getSku()));
            }
        }


        product.setBundleItems(addRequest.getBundleItems());
        this.validateBundleProduct(companyId, product);

        product.setCategory(category);

        if (category != null) {
            if (category.getUnitType() == ProductCategory.UnitType.grams) {
                List<ProductWeightTolerance> tolerances = this.getWeightTolerances(companyId);
                // Recreate the tolerance
                List<ProductPriceRange> productPriceRanges = new ArrayList<>();
                for (ProductWeightTolerance tolerance : tolerances) {
                    if (tolerance.isEnabled()) {
                        ProductPriceRange priceRange = new ProductPriceRange();
                        productPriceRanges.add(priceRange);

                        priceRange.setWeightToleranceId(tolerance.getId());
                        priceRange.setWeightTolerance(tolerance);
                        priceRange.setPriority(tolerance.getPriority());
                        priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                    }
                }


                int priority = 0;
                for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                    for (ProductPriceRange newPriceRange : productPriceRanges) {
                        if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                            newPriceRange.setPrice(productPriceRange.getPrice());
                            break;
                        }
                    }
                    priority++;
                }

                product.setPriceRanges(productPriceRanges);
                product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            } else {
                // This is the unit category, use price breaks instead of price ranges
                if (product.getPriceBreaks() != null) {
                    for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                        priceBreak.resetPrepare(companyId);
                        if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                                || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                                || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                                || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                                priceBreak.setActive(true);
                            }
                        } else {
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                                priceBreak.setActive(true);
                            }
                        }
                    }
                }

                populatePriceBreaks(product, category, product.getPriceBreaks(), true, null, false, Boolean.FALSE, companyId);

                product.setPriceBreaks(product.getPriceBreaks());
                product.setPriceRanges(new ArrayList<ProductPriceRange>());
            }
        }

        // Get the asset
        for (CompanyAsset asset : addRequest.getAssets()) {
            asset.prepare(companyId);
        }

        product.setAssets(addRequest.getAssets());
        for (CompanyAsset asset : product.getAssets()) {
            if (StringUtils.isBlank(asset.getPublicURL()) || asset.isSecured()) {
                // Migrate
                try {
                    // Migrate images possible
                    backgroundTaskManager.runTaskInBackground(migrateProductsToPublicTask);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        //add the new product
        product.setMedicalConditions(updateMedicalConditions(addRequest.getMedicalConditions(), shopId));

        product.setCustomWeight(addRequest.getCustomWeight());
        product.setCustomGramType(addRequest.getCustomGramType());

        BarcodeItem item = commonBarCodeService.createBarcodeItemIfNeeded(companyId, product.getShopId(), product.getId(),
                BarcodeItem.BarcodeEntityType.Product, product.getId(), product.getSku(), null, false);
        if (item != null) {
            product.setSku(item.getBarcode());
        }
        product.setSecondaryVendors(addRequest.getSecondaryVendors());

        Product dbProduct = productRepository.save(product);

        //update product tags in shop
        LinkedHashSet<String> shopProductsTag = shop.getProductsTag();
        for (String requestTag : addRequest.getTags()) {
            if (!shopProductsTag.contains(requestTag)) {
                shopProductsTag.add(requestTag);
            }
        }
        shop.setProductsTag(shopProductsTag);
        shopRepository.update(companyId, shopId, shop);

        dbProduct.setVendor(vendor);

        elasticSearchManager.createOrUpdateIndexedDocument(new ProductCustomResult(product));
        realtimeService.sendRealTimeEvent(shopId,
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        callWebHook(dbProduct, true, companyId, shopId);
        return dbProduct;
    }

    @Override
    public Product updateProduct(String companyId, String shopId, String productId, Product product, Boolean isPartner, String activeUserId, String activeUserName) {
        Product dbProduct = productRepository.get(companyId, productId);
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("Product", "Product does not exist with request id");
        }

        ProductCategory category = productCategoryRepository.get(companyId, product.getCategoryId());
        if (category == null) {
            throw new BlazeInvalidArgException(PRODUCT_CATEGORY, PRODUCT_CATEGORY_NOT_FOUND);
        }

        Vendor vendor = vendorRepository.get(companyId, product.getVendorId());
        if ((Product.ProductType.REGULAR == product.getProductType() && vendor == null) ||
                (Product.ProductType.DERIVED == product.getProductType() && StringUtils.isNotBlank(product.getVendorId()) && vendor == null)) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }
        if (!CollectionUtils.isNullOrEmpty(product.getSecondaryVendors())) {
            List<ObjectId> vendorObjIds = new ArrayList<>();
            for (String vendorId : product.getSecondaryVendors()) {
                if (StringUtils.isNotBlank(vendorId) && ObjectId.isValid(vendorId)) {
                    vendorObjIds.add(new ObjectId(vendorId));
                }
            }
            HashMap<String, Vendor> secondaryVendorsMap = vendorRepository.listAsMap(companyId, vendorObjIds);
            if (secondaryVendorsMap.isEmpty()) {
                throw new BlazeInvalidArgException(VENDOR, SECONDARY_VENDOR_NOT_FOUND);
            }
            //Set in product only available vendors later set in dbProduct
            product.setSecondaryVendors(secondaryVendorsMap.keySet());
        }

        dbProduct.setBundleItems(product.getBundleItems());
        this.validateBundleProduct(companyId, dbProduct);

        if (product.getNotes() != null) {
            for (Note note : product.getNotes()) {
                if (note.getId() == null) {
                    note.setId(ObjectId.get().toString());
                    note.setWriterId(activeUserId);
                    note.setWriterName(activeUserName);
                }
            }
        }
        Product.WeightPerUnit oldWeight = dbProduct.getWeightPerUnit();
        if (StringUtils.isNotBlank(dbProduct.getVendorId())
                && StringUtils.isBlank(product.getVendorId())) {
            throw new BlazeInvalidArgException("Product", "VendorId should not be empty.");
        }
        boolean isCategoryUpdate = (!dbProduct.getCategoryId().equals(product.getCategoryId()));
        String oldCategory = dbProduct.getCategoryId();
        List<String> productIds = new ArrayList<>();
        productIds.add(productId);
        if (!product.isActive()) {
            SearchResult<Transaction> transactionByProduct = transactionRepository.getTransactionByProduct(companyId, shopId, "modified:-1", productIds);
            if (transactionByProduct.getValues().size() > 0) {
                String noDetails = "";
                StringBuilder transactionString = new StringBuilder();
                transactionString.append(noDetails);
                for (Transaction transaction : transactionByProduct.getValues()) {
                    transactionString.append(" " + transaction.getTransNo() + ",");
                }

                String transactionSubstring = transactionString.substring(0, transactionString.length() - 1);
                throw new BlazeInvalidArgException("Transaction", "This product is being used in transaction (" + transactionSubstring + "). So can not deactivate it.");
            }
        }

        long result = 0;
        if (StringUtils.isNotBlank(product.getName()) && !product.getName().equalsIgnoreCase(dbProduct.getName())) {
            result = productRepository.getProductsByName(companyId, shopId, product.getName(), product.getVendorId(), product.getCategoryId(), product.getFlowerType(), product.getCannabisType());
        }
        if (result > 0) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NAME_EXISTS);
        }

        PricingTemplate template = pricingTemplateRepository.get(companyId, product.getPricingTemplateId());
        PricingTemplate dbTemplate = pricingTemplateRepository.get(companyId, dbProduct.getPricingTemplateId());


        if (!dbProduct.getWeightPerUnit().equals(product.getWeightPerUnit()) || (dbTemplate != null && !category.getUnitType().equals(dbTemplate.getUnitType()))
                || dbProduct.getUnitPrice().doubleValue() != product.getUnitPrice().doubleValue()) {

            dbProduct.setPricingTemplateId(StringUtils.EMPTY);

            Iterable<MemberGroupPrices> memberGroupPrices = groupPricesRepository.getPricesForProduct(companyId, shopId, productId, MemberGroupPrices.class);

            List<ObjectId> groupPriceId = new ArrayList<>();
            for (MemberGroupPrices memberGroupPrice : memberGroupPrices) {
                groupPriceId.add(new ObjectId(memberGroupPrice.getId()));
            }

            groupPricesRepository.updatePricingTemplate(companyId, shopId, groupPriceId, null);

            if (template != null && ((category.getUnitType().equals(ProductCategory.UnitType.units) && template.getWeightPerUnit().equals(product.getWeightPerUnit()))
                    || ((category.getUnitType().equals(ProductCategory.UnitType.grams) && StringUtils.isNotBlank(product.getPricingTemplateId()))))) {
                dbProduct.setPricingTemplateId(product.getPricingTemplateId());
            }

        } else {
            dbProduct.setPricingTemplateId(product.getPricingTemplateId());
        }

        dbProduct.setActive(product.isActive());
        dbProduct.setCategoryId(product.getCategoryId());
        dbProduct.setCategory(category);
        dbProduct.setName(product.getName());
        dbProduct.setAssets(product.getAssets());
        dbProduct.setDescription(product.getDescription());
        dbProduct.setFlowerType(product.getFlowerType());
        dbProduct.setGenetics(product.getGenetics());
        dbProduct.setMedicalConditions(product.getMedicalConditions());
        dbProduct.setNotes(product.getNotes());
        dbProduct.setUnitPrice(product.getUnitPrice());
        dbProduct.setVendorId(product.getVendorId());
        dbProduct.setEnableMixMatch(product.isEnableMixMatch());
        dbProduct.setEnableWeedmap(product.isEnableWeedmap());
        dbProduct.setWeightPerUnit(product.getWeightPerUnit());
        dbProduct.setTaxOrder(product.getTaxOrder());
        dbProduct.setTaxType(product.getTaxType());
        dbProduct.setCustomTaxInfo(product.getCustomTaxInfo());
        dbProduct.setLowThreshold(product.getLowThreshold());
        dbProduct.setLowInventoryNotification(product.isLowInventoryNotification());
        dbProduct.setShowInWidget(product.isShowInWidget());
        dbProduct.setDiscountable(product.isDiscountable());
        dbProduct.setThc(product.getThc());
        dbProduct.setCbd(product.getCbd());
        dbProduct.setCbda(product.getCbda());
        dbProduct.setCbn(product.getCbn());
        dbProduct.setThca(product.getThca());
        dbProduct.setMedicinal(product.isMedicinal());
        dbProduct.setByGram(product.isByGram());
        dbProduct.setByPrepackage(product.isByPrepackage());
        dbProduct.setProductSaleType(product.getProductSaleType());
        dbProduct.setTags(product.getTags());
        dbProduct.setEnableExciseTax(product.isEnableExciseTax());
        dbProduct.setAutomaticReOrder(product.isAutomaticReOrder());
        dbProduct.setReOrderLevel(product.getReOrderLevel());
        dbProduct.setProductType(product.getProductType());


        if (StringUtils.isNotBlank(product.getBrandId())) {
            if (vendor.getBrands().contains(product.getBrandId())) {
                dbProduct.setBrandId(product.getBrandId());
            } else {
                dbProduct.setBrandId("");
            }
        } else {
            dbProduct.setBrandId("");
        }

        dbProduct.setCannabisType(product.getCannabisType());
        dbProduct.setPriceIncludesExcise(product.isPriceIncludesExcise());
        dbProduct.setPotency(product.isPotency());
        dbProduct.setPriceIncludesALExcise(product.isPriceIncludesALExcise());

        if (product.getPotencyAmount() != null) {
            dbProduct.setPotencyAmount(product.getPotencyAmount());
        } else {
            //Null is not being set in database.
            PotencyMG potencyMG = new PotencyMG();
            dbProduct.setPotencyAmount(potencyMG);
        }

        if (dbProduct.getCustomTaxInfo() != null) {
            dbProduct.getCustomTaxInfo().prepare();
        }

        dbProduct.setTaxTables(product.getTaxTables());
        if (dbProduct.getTaxTables() != null) {
            for (CompoundTaxTable taxTable : dbProduct.getTaxTables()) {
                taxTable.prepare(companyId);
                taxTable.setShopId(shopId);
                if (taxTable.getTaxType() != TaxInfo.TaxType.Exempt) {
                    taxTable.setTaxType(TaxInfo.TaxType.Custom);


                    if (taxTable.getCityTax() != null) {
                        taxTable.getCityTax().prepare(companyId);
                        taxTable.getCityTax().setShopId(shopId);
                    }

                    if (taxTable.getStateTax() != null) {
                        taxTable.getCountyTax().prepare(companyId);
                        taxTable.getCountyTax().setShopId(shopId);
                    }

                    if (taxTable.getCountyTax() != null) {
                        taxTable.getStateTax().prepare(companyId);
                        taxTable.getStateTax().setShopId(shopId);
                    }
                    if (taxTable.getFederalTax() != null) {
                        taxTable.getFederalTax().prepare(companyId);
                        taxTable.getFederalTax().setShopId(shopId);
                    }

                    taxTable.reset();
                }
            }
        }

        if (category != null) {
            if (StringUtils.isNotBlank(dbProduct.getPricingTemplateId())) {
                if (template == null) {
                    template = pricingTemplateRepository.get(companyId, dbProduct.getPricingTemplateId());
                }
                if (template == null) {
                    throw new BlazeInvalidArgException("Pricing template", "Pricing template not found");
                }
                if (!template.getUnitType().equals(category.getUnitType())) {
                    throw new BlazeInvalidArgException("Product", "Product category's unit type does not match with pricing template");
                }
                if (template.getUnitType().equals(ProductCategory.UnitType.units) && !template.getWeightPerUnit().equals(product.getWeightPerUnit())) {
                    throw new BlazeInvalidArgException("Pricing template", "Product's weight per unit type does not match with pricing template");
                }
                this.assignPricingTemplateToProduct(dbProduct, template, category, companyId);
            } else {
                //Update pricing
                if (category.getUnitType() == ProductCategory.UnitType.grams) {
                    List<ProductWeightTolerance> tolerances = this.getWeightTolerances(companyId);
                    // Recreate the tolerance
                    List<ProductPriceRange> productPriceRanges = new ArrayList<>();
                    for (ProductWeightTolerance tolerance : tolerances) {
                        if (tolerance.isEnabled()) {
                            ProductPriceRange priceRange = new ProductPriceRange();
                            productPriceRanges.add(priceRange);

                            priceRange.setWeightToleranceId(tolerance.getId());
                            priceRange.setWeightTolerance(tolerance);
                            priceRange.setPriority(tolerance.getPriority());
                            priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                        }
                    }


                    int priority = 0;
                    for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                        for (ProductPriceRange newPriceRange : productPriceRanges) {
                            if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                                newPriceRange.setPrice(productPriceRange.getPrice());
                                break;
                            }
                        }
                        priority++;
                    }

                    dbProduct.setPriceRanges(productPriceRanges);
                    dbProduct.setPriceBreaks(new ArrayList<ProductPriceBreak>());
                } else {
                    // This is the unit category, use price breaks instead of price ranges
                    if (product.getPriceBreaks() != null) {
                        for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                            priceBreak.prepare(companyId);
                            if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                                    || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                                    || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                                    || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                                if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                                    priceBreak.setActive(true);
                                }
                            } else {
                                if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                                    priceBreak.setActive(true);
                                }
                            }
                        }
                    }
                    boolean reset = oldWeight != product.getWeightPerUnit();
                    populatePriceBreaks(dbProduct, category, product.getPriceBreaks(), true, null, reset, reset, companyId);

                    dbProduct.setPriceBreaks(product.getPriceBreaks());
                    dbProduct.setPriceRanges(new ArrayList<ProductPriceRange>());
                }
            }
        }

//        BarcodeItem item = null;
        // Update sku if needed
        BarcodeItem item = commonBarCodeService.createBarcodeItemIfNeeded(dbProduct.getCompanyId(), dbProduct.getShopId(), dbProduct.getId(),
                BarcodeItem.BarcodeEntityType.Product, product.getId(),
                product.getSku(), // new sku
                dbProduct.getSku(), false);
        if (item != null) {
            dbProduct.setSku(item.getBarcode());
        }

        dbProduct.setCustomWeight(product.getCustomWeight());
        dbProduct.setCustomGramType(product.getCustomGramType());

        dbProduct.setSecondaryVendors(product.getSecondaryVendors());

        dbProduct = productRepository.update(companyId, productId, dbProduct);
        dbProduct.setVendor(vendor);

        elasticSearchManager.createOrUpdateIndexedDocument(new ProductCustomResult(dbProduct));

        //update product tags in shop
        Shop shop = shopRepository.get(companyId, shopId);

        LinkedHashSet<String> shopProductsTag = shop.getProductsTag();
        for (String requestTag : product.getTags()) {
            if (!shopProductsTag.contains(requestTag)) {
                shopProductsTag.add(requestTag);
            }
        }
        shop.setProductsTag(shopProductsTag);
        shopRepository.updateProductTags(companyId, shopId, shopProductsTag);

        realtimeService.sendRealTimeEvent(shopId.toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        callWebHook(dbProduct, false, companyId, shopId);
        if (!dbProduct.isEnableWeedmap() || !dbProduct.isActive() || dbProduct.isDeleted()) {
            weedmapService.removeProductFromWeedMap(Lists.newArrayList(oldCategory), false, Lists.newArrayList(dbProduct));
        }
        return dbProduct;
    }

    @Override
    public SearchResult<Product> getProducts(String companyId, String shopId, String startDate, String endDate, int skip, int limit) {

        SearchResult<Product> searchResult = null;
        this.checkCompanyAvailability(companyId);
        Shop shop = this.checkShopAvailability(companyId, shopId);

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        if (skip < 0) {
            skip = 0;
        }

        if (StringUtils.isBlank(endDate) || StringUtils.isBlank(startDate)) {
            searchResult = productRepository.findItems(companyId, shopId, skip, limit);
        } else {
            DateTime endDateTime;
            if (StringUtils.isNotBlank(endDate)) {
                endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
            } else {
                endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
            }

            DateTime startDateTime;
            if (StringUtils.isNotBlank(startDate)) {
                startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
            } else {
                startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
            }
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

            searchResult = productRepository.findItemsWithDate(companyId, shopId, timeZoneStartDateMillis, timeZoneEndDateMillis, skip, limit, "{modified:-1}");
        }

        return searchResult;
    }

    @Override
    public SearchResult<Product> getProducts(String companyId, String shopId, long startDate, long endDate, int skip, int limit) {
        this.checkCompanyAvailability(companyId);
        Shop shop = this.checkShopAvailability(companyId, shopId);

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        if (skip < 0) {
            skip = 0;
        }


        SearchResult<Product> searchResult = productRepository.findItemsWithDate(companyId, shopId, startDate, endDate, skip, limit, "{modified:-1}");

        return searchResult;
    }

    private Company checkCompanyAvailability(String companyId) {
        Company company = companyRepository.getById(companyId);
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company is not found.");
        }

        return company;
    }

    private Shop checkShopAvailability(String companyId, String shopId) {
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }
        return shop;
    }

    private void assignedProductDependencies(Product product, String shopId, String companyId) {
        List<ObjectId> vendorIds = new ArrayList<>();
        if (product.getVendorId() != null && ObjectId.isValid(product.getVendorId())) {
            vendorIds.add(new ObjectId(product.getVendorId()));
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(companyId, vendorIds);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(companyId, shopId);
        product.setVendor(vendorHashMap.get(product.getVendorId()));
        product.setCategory(productCategoryHashMap.get(product.getCategoryId()));
    }

    private List<ProductPriceRange> populatePriceRanges(final Product product, final List<ProductPriceRange> priceRanges, String memberGroupId, ArrayList<ProductWeightTolerance> weightTolerancesList, String companyId) {
        List<ProductWeightTolerance> tolerances = this.getWeightTolerances(companyId);
        if (weightTolerancesList != null) {
            tolerances = weightTolerancesList;
        }

        // Recreate the tolerance
        List<ProductPriceRange> productPriceRanges = new ArrayList<>();
        for (ProductWeightTolerance tolerance : tolerances) {
            if (tolerance.isEnabled()) {
                ProductPriceRange priceRange = new ProductPriceRange();
                productPriceRanges.add(priceRange);

                priceRange.setWeightToleranceId(tolerance.getId());
                priceRange.setWeightTolerance(tolerance);
                priceRange.setPriority(tolerance.getPriority());
                priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId() + "_" + memberGroupId);
            }
        }


        int priority = 0;
        // fill in the price
        for (ProductPriceRange productPriceRange : priceRanges) {
            for (ProductPriceRange newPriceRange : productPriceRanges) {
                if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                    newPriceRange.setPrice(productPriceRange.getPrice());
                    break;
                }
            }
            priority++;
        }
        return productPriceRanges;
    }

    private List<ProductWeightTolerance> getWeightTolerances(String companyId) {
        Iterable<ProductWeightTolerance> list = weightToleranceRepository.listSort(companyId, "{startWeight:1}");
        return Lists.newArrayList(list);
    }

    private List<ProductPriceBreak> populatePriceBreaks(final Product product,
                                                        ProductCategory category,
                                                        List<ProductPriceBreak> priceBreaks,
                                                        boolean isDefault,
                                                        ArrayList<ProductWeightTolerance> weightTolerancesList,
                                                        boolean reset, Boolean isPricingTemplate, String companyId) {

        // Fill in the priceBreaks or priceRanges
        if (category.getUnitType() == ProductCategory.UnitType.units) {

            final ProductPriceBreak.PriceBreakType[] priceBreakTypes = ProductPriceBreak.PriceBreakType.values();

            // Remove bad price breaks
            priceBreaks.removeIf(new Predicate<ProductPriceBreak>() {
                @Override
                public boolean test(ProductPriceBreak productPriceBreak) {

                    return (product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS)
                            && productPriceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit;
                }
            });

            if (reset) {
                for (ProductPriceBreak productPriceBreak : priceBreaks) {
                    productPriceBreak.setPrice(new BigDecimal(0));
                    productPriceBreak.setActive(false);
                }
            }

            for (ProductPriceBreak.PriceBreakType type : priceBreakTypes) {
                if (type == ProductPriceBreak.PriceBreakType.None) {
                    continue;
                }

                if (type == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                        continue;
                    }
                }

                // find the price break by type
                ProductPriceBreak priceBreak = null;
                for (ProductPriceBreak productPriceBreak : priceBreaks) {
                    if (productPriceBreak.getPriceBreakType() == type) {
                        priceBreak = productPriceBreak;
                        break;
                    }
                }
                // if none is found, create a new one
                if (priceBreak == null) {
                    // Add a new one
                    priceBreak = new ProductPriceBreak();
                    priceBreak.prepare(companyId);
                    priceBreak.setPriceBreakType(type);
                    priceBreak.setActive(false);


                    // add price break to group
                    priceBreaks.add(priceBreak);
                }

                // specify name and quantity
                if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                        product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    priceBreak.setName(type.eachName);
                    priceBreak.setQuantity(type.fullGramValue);
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                    priceBreak.setQuantity(type.fullGramValue);
                    priceBreak.setName(type.gramName);
                } else {
                    priceBreak.setName(type.gramName);
                    priceBreak.setQuantity(type.halfGramValue);
                }

                // default cost
                if (isDefault) {
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                            if (isPricingTemplate) {
                                product.setUnitPrice(priceBreak.getPrice() == null ? BigDecimal.ZERO : priceBreak.getPrice());
                            } else {
                                priceBreak.setPrice(product.getUnitPrice());
                            }
                            priceBreak.setActive(true);
                        }
                    } else {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                            if (isPricingTemplate) {
                                product.setUnitPrice(priceBreak.getPrice() == null ? BigDecimal.ZERO : priceBreak.getPrice());
                            } else {
                                priceBreak.setPrice(product.getUnitPrice());
                            }
                            priceBreak.setActive(true);
                        }
                    }
                }

                if (reset) {
                    priceBreak.setPrice(product.getUnitPrice().multiply(new BigDecimal(priceBreak.getQuantity())));
                }
            }

            // Sort price breaks
            priceBreaks.sort(new Comparator<ProductPriceBreak>() {
                @Override
                public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                    return ((Integer) o1.getQuantity()).compareTo((Integer) o2.getQuantity());
                }
            });
        }

        return priceBreaks;
    }

    private List<MedicalCondition> updateMedicalConditions(List<MedicalCondition> medicalConditions, String shopId) {
        List<MedicalCondition> currentMeds = medicalConditionRepository.list();
        HashMap<String, MedicalCondition> medMap = new HashMap<>();
        for (MedicalCondition current : currentMeds) {
            medMap.put(current.getName(), current);
        }

        Set<MedicalCondition> values = new LinkedHashSet<>();
        for (MedicalCondition condition : medicalConditions) {
            MedicalCondition prev = medMap.get(condition.getName());
            if (prev != null) {
                values.add(prev);
            } else {
                condition.setId(null);
                medicalConditionRepository.save(condition);
                values.add(condition);
                medMap.put(condition.getName(), condition);
            }
        }
        realtimeService.sendRealTimeEvent(shopId.toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        return Lists.newArrayList(values);
    }

    /**
     * This is private method for call product update webhook.
     *
     * @param product
     * @param companyId
     * @param shopId
     */
    private void callWebHook(Product product, boolean isNew, String companyId, String shopId) {
        ProductData request = new ProductData();
        request.setId(product.getId());
        request.setName(product.getName());
        request.setSku(product.getSku());
        request.setFlowerType(product.getFlowerType());
        request.setVendorId(product.getVendorId());
        request.setProductSaleType(product.getProductSaleType());
        request.setBrandId(product.getBrandId());
        request.setCannabisType(product.getCannabisType());
        request.setCategoryId(product.getCategoryId());
        request.setWeightPerUnit(product.getWeightPerUnit());
        request.setPriceBreaks(product.getPriceBreaks());
        request.setPriceRanges(product.getPriceRanges());
        request.setQuantities(product.getQuantities());
        request.setNew(isNew);
        request.setByGram(product.isByGram());
        request.setActive(product.isActive());
        request.setAssets(product.getAssets());
        request.setBrand(product.getBrand());
        request.setByPrepackage(product.isByPrepackage());
        request.setCategory(product.getCategory());
        request.setCbd(product.getCbd());
        request.setCbda(product.getCbda());
        request.setCbn(product.getCbn());
        request.setCustomGramType(product.getCustomGramType());
        request.setCustomTaxInfo(product.getCustomTaxInfo());
        request.setCustomWeight(product.getCustomWeight());
        request.setDescription(product.getDescription());
        request.setDiscountable(product.isDiscountable());
        request.setEnableExciseTax(product.isEnableExciseTax());
        request.setEnableMixMatch(product.isEnableMixMatch());
        request.setEnableWeedmap(product.isEnableWeedmap());
        request.setGenetics(product.getGenetics());
        request.setImportId(product.getImportId());
        request.setLowInventoryNotification(product.isLowInventoryNotification());
        request.setLowThreshold(product.getLowThreshold());
        request.setMedicalConditions(product.getMedicalConditions());
        request.setMedicinal(product.isMedicinal());
        request.setNotes(product.getNotes());
        request.setPotency(product.isPotency());
        request.setPotencyAmount(product.getPotencyAmount());
        request.setPriceIncludesALExcise(product.isPriceIncludesALExcise());
        request.setPriceIncludesExcise(product.isPriceIncludesExcise());
        request.setQbDesktopItemRef(product.getQbDesktopItemRef());
        request.setQbItemRef(product.getQbItemRef());
        request.setTaxOrder(product.getTaxOrder());
        request.setShowInWidget(product.isShowInWidget());
        request.setTags(product.getTags());
        request.setTaxTables(product.getTaxTables());
        request.setTaxType(product.getTaxType());
        request.setThc(product.getThc());
        request.setThca(product.getThca());
        request.setUnitPrice(product.getUnitPrice());
        request.setUnitValue(product.getUnitValue());
        request.setVendor(product.getVendor());
        request.setCreated(product.getCreated());
        request.setCompanyId(product.getCompanyId());
        request.setDeleted(product.isDeleted());
        request.setDirty(product.isDirty());
        request.setShopId(product.getShopId());
        request.setModified(product.getModified());
        request.setUpdated(product.isUpdated());

        try {
            prepareProductForWebHook(request, companyId, shopId);
            partnerWebHookManager.updateProductWebHook(request.getCompanyId(), request.getShopId(), request, PartnerWebHook.PartnerWebHookType.UPDATE_PRODUCT);
        } catch (Exception e) {
            LOG.error("Product web hook failed for shop: " + shopId + ", company: " + companyId);
        }
    }

    private void prepareProductForWebHook(ProductData product, String companyId, String shopId) {
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(companyId);
        Vendor vendor = vendorRepository.getById(product.getVendorId());
        ProductCategory productCategory = productCategoryRepository.getById(product.getCategoryId());
        Brand brand = brandRepository.getById(product.getBrandId());

        product.setVendor(vendor);
        product.setCategory(productCategory);
        product.setBrand(brand);

        ArrayList<ProductWeightTolerance> weightTolerancesList = new ArrayList<ProductWeightTolerance>(toleranceHashMap.values());

        if (productCategory != null) {
            if (productCategory.getUnitType() == ProductCategory.UnitType.grams && !CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
                List<ProductPriceRange> ranges = populatePriceRanges(product, product.getPriceRanges(), "default", weightTolerancesList, companyId);
                product.setPriceRanges(ranges);
                product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            } else if (productCategory.getUnitType() == ProductCategory.UnitType.units && !CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
                List<ProductPriceBreak> productPriceBreaks = populatePriceBreaks(product, productCategory, product.getPriceBreaks(), true, weightTolerancesList, false, Boolean.FALSE, companyId);
                product.setPriceBreaks(productPriceBreaks);
                product.setPriceRanges(new ArrayList<ProductPriceRange>());
            }
        }
    }

    private void assignPricingTemplateToProduct(Product product, PricingTemplate template, ProductCategory category, String companyId) {

        if (!template.isActive()) {
            throw new BlazeInvalidArgException("Pricing Template", "Pricing template is not active.");
        }

        product.setPriceBreaks(template.getPriceBreaks());
        product.setPriceRanges(template.getPriceRanges());

        if (product.getPriceRanges() == null) {
            product.setPriceRanges(new ArrayList<>());
        }
        if (product.getPriceBreaks() == null) {
            product.setPriceBreaks(new ArrayList<>());
        }

        if (!CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
            for (ProductPriceRange priceRange : product.getPriceRanges()) {
                priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
            }
        }


        if (!CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
            if (template != null) {
                for (ProductPriceBreak poriceBreak : product.getPriceBreaks()) {
                    poriceBreak.resetPrepare(product.getCompanyId());
                }
            }
            product.getPriceBreaks().removeIf(priceBreak -> ProductPriceBreak.PriceBreakType.None == priceBreak.getPriceBreakType());

            populatePriceBreaks(product, category, product.getPriceBreaks(), Boolean.TRUE, null, Boolean.FALSE, Boolean.TRUE, companyId);
        }
    }

    private void validateBundleProduct(String companyId, Product product) {
        if (Product.ProductType.BUNDLE == product.getProductType()) {
            if (product.getBundleItems() != null && product.getBundleItems().size() > 0) {
                List<ObjectId> productIds = new ArrayList<>();
                for (BundleItem bundleItem : product.getBundleItems()) {
                    if (StringUtils.isNotBlank(bundleItem.getProductId()) && ObjectId.isValid(bundleItem.getProductId())) {
                        productIds.add(new ObjectId(bundleItem.getProductId()));
                    } else {
                        throw new BlazeInvalidArgException(PRODUCT, NOT_VALID_BUNDLE_PRODUCT);
                    }
                }

                HashMap<String, Product> productMap = productRepository.listAsMap(companyId, productIds);

                for (BundleItem bundleItem : product.getBundleItems()) {
                    Product parentProduct = productMap.get(bundleItem.getProductId());
                    if (parentProduct == null) {
                        throw new BlazeInvalidArgException(PRODUCT, NOT_VALID_BUNDLE_PRODUCT);
                    }

                    if (Product.ProductType.BUNDLE == parentProduct.getProductType()) {
                        throw new BlazeInvalidArgException(PRODUCT, BUNDLE_PRODUCT_USE);
                    }
                }
            }
        }
    }
}
