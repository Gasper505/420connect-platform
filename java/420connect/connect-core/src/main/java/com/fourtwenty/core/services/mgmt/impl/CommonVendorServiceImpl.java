package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.services.mgmt.CommonVendorService;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommonVendorServiceImpl implements CommonVendorService {

    private static final String VENDOR = "Vendor";
    private static final String VENDOR_NOT_FOUND = "Vendor is not found.";
    private static final String COMPANY_TYPE_NOT_FOUND = "Company type cannot be blank.";
    private static final String OWNER_NOT_EXIST = "Account owner isn't exist.";

    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private EmployeeRepository employeeRepository;

    @Override
    public VendorResult getVendor(String companyId, String vendorId) {
        VendorResult vendorResult = vendorRepository.get(companyId, vendorId, VendorResult.class);
        if (vendorResult == null) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }

        HashMap<String, Brand> brandHashMap = prepareVendor(companyId, vendorResult);
        prepareVendorResult(vendorResult, brandHashMap);

        return vendorResult;
    }

    @Override
    public HashMap<String, Brand> prepareVendor(String companyId, VendorResult vendorResult) {
        HashMap<String, Brand> brandHashMap = null;
        if (vendorResult.getBrands() != null) {
            List<ObjectId> brandIds = new ArrayList<>();
            for (String brandId : vendorResult.getBrands()) {
                if (ObjectId.isValid(brandId)) {
                    brandIds.add(new ObjectId(brandId));
                }
            }
            brandHashMap = brandRepository.listAsMap(companyId, brandIds);
        }

        return brandHashMap;
    }

    @Override
    public void prepareVendorResult(VendorResult vendorResult, HashMap<String, Brand> brandHashMap) {
        List<Brand> brandList = new ArrayList<>();

        if (vendorResult.getBrands() != null && brandHashMap != null) {
            for (String brandId : vendorResult.getBrands()) {
                Brand brand = brandHashMap.get(brandId);
                if (brand != null) {
                    brandList.add(brand);
                }
            }
        }

        vendorResult.setBrandList(brandList);
    }

    @Override
    public void prepareSearchResult(String companyId, SearchResult<VendorResult> result) {
        if (result != null && result.getValues() != null) {
            Set<ObjectId> brandIds = new HashSet<>();

            for (VendorResult vendorResult : result.getValues()) {
                if (vendorResult.getBrands() != null) {
                    for (String brandId : vendorResult.getBrands()) {
                        if (ObjectId.isValid(brandId)) {
                            brandIds.add(new ObjectId(brandId));
                        }
                    }
                }
            }

            HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(companyId, new ArrayList<>(brandIds));

            for (VendorResult vendorResult : result.getValues()) {
                this.prepareVendorResult(vendorResult, brandHashMap);
            }
        }
    }

    @Override
    public Vendor addVendor(String companyId, String shopId, VendorAddRequest request, String employeeId) {
        Shop shop = shopRepository.getById(shopId);

        if (StringUtils.isNotEmpty(request.getName())) {
            long count = vendorRepository.countVendorByVendorName(companyId, request.getName());
            if (count > 0) {
                throw new BlazeInvalidArgException("VendorAddRequest", "Vendor with this name already exist.");
            }
        }
        if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) {
            if (StringUtils.isNotBlank(request.getEmail()) && !EmailValidator.getInstance().isValid(request.getEmail())) {
                throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
            }
        }
        Employee accountOwner = null;
        if (StringUtils.isNotBlank(request.getAccountOwnerId())) {
            accountOwner = employeeRepository.get(companyId, request.getAccountOwnerId());
            if (accountOwner == null) {
                throw new BlazeInvalidArgException(VENDOR, OWNER_NOT_EXIST);
            }
        }

        Vendor vendor = new Vendor();
        vendor.setCompanyId(companyId);
        vendor.setName(request.getName());
        vendor.setDescription(request.getDescription());
        vendor.setEmail(request.getEmail());
        vendor.setPhone(request.getPhone());
        vendor.setAddress(request.getAddress());
        vendor.setActive(request.getActive());
        vendor.setLastName(request.getLastName());
        vendor.setFirstName(request.getFirstName());
        vendor.setLicenseNumber(request.getLicenseNumber());
        vendor.setWebsite(request.getWebsite());
        vendor.setFax(request.getFax());
        vendor.setActive(request.getActive());
        vendor.setAssets(request.getAssets());
        if (request.getAddress() != null) {
            request.getAddress().prepare();
        }
        vendor.setBrands(request.getBrands());
        vendor.setLicenseExpirationDate(request.getLicenseExpirationDate());
        vendor.setArmsLengthType(request.getArmsLengthType());

        vendor.setCompanyType(request.getCompanyType());
        if (request.getAdditionalAddressList() != null) {
            for (Address address : request.getAdditionalAddressList()) {
                address.prepare(companyId);
            }
            vendor.setAdditionalAddressList(request.getAdditionalAddressList());
        }

        vendor.setLicenceType(request.getLicenceType());
        vendor.setRelatedEntity(request.isRelatedEntity());
        vendor.setMobileNumber(request.getMobileNumber());
        if (request.getNotes() != null) {
            for (Note note : request.getNotes()) {
                note.prepare();
            }
            vendor.setNotes(request.getNotes());
        }
        vendor.setVendorType(request.getVendorType());
        vendor.setDbaName(request.getDbaName());

        updateCompanyLicense(shop, request.getCompanyLicenses(), request.getCompanyType(), request.getLicenceType(), request.getLicenseNumber(), request.getLicenseExpirationDate());

        vendor.setCompanyLicenses(request.getCompanyLicenses());
        vendor.setCreatedBy(employeeId);
        vendor.setAccountOwnerId(accountOwner != null ? accountOwner.getId() : null);

        return vendorRepository.save(vendor);
    }

    @Override
    public <E extends Vendor> Vendor updateVendor(String companyId, String shopId, String vendorId, E vendor, Boolean isPartner, String activeUserId, String activeUserName) {
        if (StringUtils.isBlank(vendorId) || !ObjectId.isValid(vendorId)) {
            throw new BlazeInvalidArgException("VendorAddRequest", "Invalid vendorId");
        }

        Vendor dbVendor = vendorRepository.get(companyId, vendorId);
        if (dbVendor == null) {
            throw new BlazeInvalidArgException("Vendor", "Vendor doesn't exist.");
        }

        Shop shop = shopRepository.getById(shopId);
        if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution) {
            if (Vendor.VendorType.BOTH.equals(dbVendor.getVendorType()) && !Vendor.VendorType.BOTH.equals(vendor.getVendorType())) {
                throw new BlazeInvalidArgException(VENDOR, "Vendor with type BOTH are not allowed to update.");
            }
        }

        if (dbVendor.getVendorType() != vendor.getVendorType() && !Vendor.VendorType.BOTH.equals(vendor.getVendorType())) {
            throw new BlazeInvalidArgException(VENDOR, "Update from vendor to customer and vice versa does not allowed.");
        }

        Employee accountOwner = null;
        if (StringUtils.isNotBlank(vendor.getAccountOwnerId())) {
            accountOwner = employeeRepository.get(companyId, vendor.getAccountOwnerId());
            if (accountOwner == null) {
                throw new BlazeInvalidArgException(VENDOR, OWNER_NOT_EXIST);
            }
        }

        if (vendor.getAddress() != null) {
            vendor.getAddress().prepare();
        }

        if (vendor.getNotes() != null) {
            for (Note note : vendor.getNotes()) {
                if (note.getId() == null) {
                    note.setId(ObjectId.get().toString());
                    note.setWriterName(activeUserName);
                    note.setWriterId(activeUserId);
                }
            }
        }

        dbVendor.setName(vendor.getName());
        dbVendor.setAddress(vendor.getAddress());
        dbVendor.setWebsite(vendor.getWebsite());
        dbVendor.setEmail(vendor.getEmail());
        dbVendor.setActive(vendor.getActive());
        dbVendor.setDescription(vendor.getDescription());
        dbVendor.setFax(vendor.getFax());
        dbVendor.setFirstName(vendor.getFirstName());
        dbVendor.setLastName(vendor.getLastName());
        dbVendor.setLicenseNumber(vendor.getLicenseNumber());
        dbVendor.setNotes(vendor.getNotes());
        dbVendor.setPhone(vendor.getPhone());
        dbVendor.setAssets(vendor.getAssets());
        dbVendor.setBackOrderEnabled(vendor.getBackOrderEnabled());
        dbVendor.setLicenseExpirationDate(vendor.getLicenseExpirationDate());
        dbVendor.setArmsLengthType(vendor.getArmsLengthType());
        if (vendor.getBrands() != null) {

            dbVendor.setBrands(vendor.getBrands());
        }


        dbVendor.setCompanyType(vendor.getCompanyType());
        if (vendor.getAdditionalAddressList() != null) {
            for (Address address : vendor.getAdditionalAddressList()) {
                address.prepare(companyId);
            }
            dbVendor.setAdditionalAddressList(vendor.getAdditionalAddressList());
        }

        dbVendor.setLicenceType(vendor.getLicenceType());
        dbVendor.setRelatedEntity(vendor.isRelatedEntity());
        dbVendor.setMobileNumber(vendor.getMobileNumber());
        dbVendor.setVendorType(vendor.getVendorType());
        dbVendor.setCredits(vendor.getCredits());
        if (vendor.getNotes() != null) {
            for (Note note : vendor.getNotes()) {
                note.prepare();
            }
            dbVendor.setNotes(vendor.getNotes());
        }
        dbVendor.setDbaName(vendor.getDbaName());
        updateCompanyLicense(shop, vendor.getCompanyLicenses(), vendor.getCompanyType(), vendor.getLicenceType(), vendor.getLicenseNumber(), vendor.getLicenseExpirationDate());
        dbVendor.setCompanyLicenses(vendor.getCompanyLicenses());
        dbVendor.setAccountOwnerId(accountOwner != null ? accountOwner.getId() : null);
        return vendorRepository.update(companyId, vendorId, dbVendor);
    }

    @Override
    public SearchResult<Vendor> getVendors(String companyId, String shopId, String startDate, String endDate, int skip, int limit) {
        SearchResult<Vendor> searchResult = null;
        this.checkCompanyAvailability(companyId);
        Shop shop = this.checkShopAvailability(companyId, shopId);

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        if (skip < 0) {
            skip = 0;
        }

        if (StringUtils.isBlank(endDate) || StringUtils.isBlank(startDate)) {
            searchResult = vendorRepository.findItems(companyId, skip, limit);
        } else {
            long timeZoneStartDateMillis = this.getStartDate(shop, startDate);
            long timeZoneEndDateMillis = this.getEndDate(shop, endDate);

            searchResult = vendorRepository.findItemsWithDate(companyId, timeZoneStartDateMillis, timeZoneEndDateMillis, skip, limit, "{modified:-1}");
        }

        return searchResult;
    }

    @Override
    public long getEndDate(Shop shop, String endDate) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        DateTime endDateTime;
        if (StringUtils.isNotBlank(endDate)) {
            endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
        } else {
            endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
        }
        String timeZone = shop.getTimeZone();
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);
        return endDateTime.minusMinutes(timeZoneOffset).getMillis();
    }

    @Override
    public long getStartDate(Shop shop, String startDate) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

        DateTime startDateTime;
        if (StringUtils.isNotBlank(startDate)) {
            startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
        } else {
            startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
        }
        String timeZone = shop.getTimeZone();
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

        return startDateTime.minusMinutes(timeZoneOffset).getMillis();
    }

    @Override
    public Company checkCompanyAvailability(String companyId) {
        Company company = companyRepository.getById(companyId);
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company is not found.");
        }

        return company;
    }

    @Override
    public Shop checkShopAvailability(String companyId, String shopId) {
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }
        return shop;
    }

    /**
     * Private method to update vendor license to support multiple licenses or create one default as RETAILER
     *
     * @param shop                  : shop
     * @param companyLicenses       : list of company licenses
     * @param companyType           : company type
     * @param licenseType           : license type
     * @param licenseNumber         : license number
     * @param licenseExpirationDate : license expiry date
     */
    private void updateCompanyLicense(Shop shop, List<CompanyLicense> companyLicenses, Vendor.CompanyType companyType, Vendor.LicenceType licenseType, String licenseNumber, long licenseExpirationDate) {
        if (CollectionUtils.isEmpty(companyLicenses) && StringUtils.isNotBlank(licenseNumber)) {
            CompanyLicense companyLicense = new CompanyLicense();
            companyLicense.prepare();
            companyLicense.setLicenseType(licenseType);
            companyLicense.setCompanyType(companyType == null ? Vendor.CompanyType.RETAILER : companyType);
            companyLicense.setLicenseNumber(licenseNumber);
            companyLicense.setLicenseExpirationDate(licenseExpirationDate);
            companyLicense.setToDefault(true);
            companyLicenses.add(companyLicense);
        } else if (CollectionUtils.isNotEmpty(companyLicenses)) {
            String defaultLicenseId = "";
            boolean defaultUpdate = false;

            if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
                CompanyLicense companyLicense = companyLicenses.get(0);
                if (companyLicense != null) {
                    companyLicense.setLicenseType(licenseType);
                    companyLicense.setLicenseNumber(licenseNumber);
                    companyLicense.setCompanyType(companyType == null ? Vendor.CompanyType.RETAILER : companyType);
                }
            }
            for (CompanyLicense companyLicense : companyLicenses) {
                companyLicense.prepare();

                if (companyLicense.getCompanyType() == null) {
                    throw new BlazeInvalidArgException(VENDOR, COMPANY_TYPE_NOT_FOUND);
                }
                // Find if default license is updated.

                if (StringUtils.isBlank(defaultLicenseId)) {
                    defaultLicenseId = companyLicense.getId(); // default set to first
                }
                defaultUpdate = companyLicense.isToDefault();
                if (defaultUpdate) {
                    defaultLicenseId = companyLicense.getId(); // set to latest default
                }
            }

            for (CompanyLicense companyLicense : companyLicenses) {
                if (!companyLicense.getId().equals(defaultLicenseId)) {
                    companyLicense.setToDefault(false);
                } else {
                    companyLicense.setToDefault(true);
                }
            }
        }
    }
}
