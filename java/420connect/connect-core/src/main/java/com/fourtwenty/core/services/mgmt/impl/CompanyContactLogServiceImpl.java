package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CompanyContactLog;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyContactLogRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyContactLogAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CompanyContactLogService;
import com.google.inject.Provider;

import javax.inject.Inject;

public class CompanyContactLogServiceImpl extends AbstractAuthServiceImpl implements CompanyContactLogService {

    private static final String COMPANY_CONTACT_LOG = "company contact log";
    private static final String LOG_NOT_FOUND = "Data not found.";
    private CompanyContactLogRepository companyContactLogRepository;

    @Inject
    public CompanyContactLogServiceImpl(Provider<ConnectAuthToken> token,
                                        CompanyContactLogRepository companyContactLogRepository) {
        super(token);
        this.companyContactLogRepository = companyContactLogRepository;
    }

    @Override
    public CompanyContactLog addCompanyContactLog(CompanyContactLogAddRequest request) {
        try {
            CompanyContactLog companyContactLog = new CompanyContactLog();
            companyContactLog.prepare(token.getCompanyId());
            companyContactLog.setShopId(token.getShopId());
            companyContactLog.setCompanyContactId(request.getCompanyContactId());
            companyContactLog.setCustomerCompanyId(request.getCustomerCompanyId());
            companyContactLog.setEmployeeId(token.getActiveTopUser().getUserId());
            companyContactLog.setLog(request.getLog());
            return companyContactLogRepository.save(companyContactLog);
        } catch (Exception ex) {
            throw new BlazeInvalidArgException(COMPANY_CONTACT_LOG, "Error! while saving data for Company contact log.");
        }
    }

    @Override
    public SearchResult<CompanyContactLog> getAllCompanyContactLogsByCustomerCompany(String customerCompanyId, int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<CompanyContactLog> result = companyContactLogRepository.getAllCompanyContactLogByCustomerCompany(token.getCompanyId(), customerCompanyId, "{modified:-1}", start, limit);
        return result;
    }
}
