package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyContactRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CustomerCompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyContactLogAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CompanyContactLogService;
import com.fourtwenty.core.services.mgmt.CompanyContactService;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.bson.types.ObjectId;

import javax.inject.Inject;

public class CompanyContactServiceImpl extends AbstractAuthServiceImpl implements CompanyContactService {

    private static final String CUSTOMER_COMPANY = "Customer company";
    private static final String CONTACT = "Contact";
    private static final String CUSTOMER_COMPANY_NOT_FOUND = "Customer company does not exist.";
    private static final String CONTACT_NOT_FOUND = "Contact does not exist.";
    private static final String VENDOR_NOT_VALID = "Vendor must be valid.";

    private CompanyContactRepository companyContactRepository;
    private CompanyContactLogService companyContactLogService;
    private VendorRepository vendorRepository;

    @Inject
    public CompanyContactServiceImpl(Provider<ConnectAuthToken> token,
                                     CompanyContactRepository companyContactRepository,
                                     CustomerCompanyRepository customerCompanyRepository,
                                     CompanyContactLogService companyContactLogService, VendorRepository vendorRepository) {
        super(token);
        this.companyContactRepository = companyContactRepository;
        this.companyContactLogService = companyContactLogService;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public CompanyContact createCompanyContact(CompanyContact request) {

        if (StringUtils.isBlank(request.getCustomerCompanyId()) || !ObjectId.isValid(request.getCustomerCompanyId())) {
            throw new BlazeInvalidArgException(CONTACT, VENDOR_NOT_VALID);
        }
        Vendor customerCompany = vendorRepository.getById(request.getCustomerCompanyId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        if (StringUtils.isBlank(request.getFirstName()) && StringUtils.isBlank(request.getLastName())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "First Name and Last Name can't be blank.");
        }

        if (StringUtils.isNotBlank(request.getEmail())) {
            if (!EmailValidator.getInstance().isValid(request.getEmail())) {
                throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
            }

            CompanyContact contactByEmail = companyContactRepository.getCompanyContactByEmail(request.getEmail());

            if (contactByEmail != null) {
                throw new BlazeQuickPinExistException("DuplicateEmail", "Another contact exists with this email.");
            }
        }

        CompanyAsset userPhoto = request.getUserPhoto();
        if (userPhoto != null) {
            userPhoto.prepare();
        }
        Note note = request.getNote();
        if (note != null && StringUtils.isNotBlank(note.getMessage())) {
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        }

        if (request.getAssets() != null) {
            for (CompanyAsset companyAsset : request.getAssets()) {
                companyAsset.prepare();
            }
        }

        CompanyContact companyContact = new CompanyContact();
        companyContact.prepare(token.getCompanyId());
        companyContact.setSalutation(request.getSalutation());
        companyContact.setFirstName(request.getFirstName());
        companyContact.setLastName(request.getLastName());
        companyContact.setEmail(request.getEmail());
        companyContact.setRole(request.getRole());
        companyContact.setPhoneNumber(request.getPhoneNumber());
        companyContact.setUserPhoto(userPhoto);
        companyContact.setNote(note);
        companyContact.setOfficeNumber(request.getOfficeNumber());
        companyContact.setCustomerCompanyId(request.getCustomerCompanyId());
        companyContact.setAssets(request.getAssets());
        CompanyContact dbCompanyContact = companyContactRepository.save(companyContact);
        if (dbCompanyContact != null) {
            prepareCompanyContactLogs(dbCompanyContact.getCustomerCompanyId(), dbCompanyContact.getId(), "Contact added by ");
        }
        return dbCompanyContact;
    }

    private void prepareCompanyContactLogs(String customerCompanyId, String companyContactId, String log) {

        CompanyContactLogAddRequest addRequest = new CompanyContactLogAddRequest();
        addRequest.setCompanyContactId(companyContactId);
        addRequest.setCustomerCompanyId(customerCompanyId);
        addRequest.setEmployeeId(token.getActiveTopUser().getUserId());
        addRequest.setLog(log + token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        companyContactLogService.addCompanyContactLog(addRequest);


    }

    @Override
    public CompanyContact updateCompanyContact(String contactId, CompanyContact request) {
        if (StringUtils.isBlank(contactId)) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }
        CompanyContact dbCompanyContact = companyContactRepository.getById(contactId);
        if (dbCompanyContact == null) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }

        if (StringUtils.isBlank(request.getCustomerCompanyId()) || !ObjectId.isValid(request.getCustomerCompanyId())) {
            throw new BlazeInvalidArgException(CONTACT, VENDOR_NOT_VALID);
        }

        Vendor customerCompany = vendorRepository.getById(request.getCustomerCompanyId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        if (StringUtils.isNotBlank(request.getEmail())) {
            if (!EmailValidator.getInstance().isValid(request.getEmail())) {
                throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
            }

            if (!dbCompanyContact.getEmail().equalsIgnoreCase(request.getEmail())) {
                CompanyContact contactByEmail = companyContactRepository.getCompanyContactByEmail(request.getEmail());

                if (contactByEmail != null) {
                    throw new BlazeQuickPinExistException("DuplicateEmail", "Another contact exists with this email.");
                }
            }
        }
        CompanyAsset userPhoto = request.getUserPhoto();
        if (userPhoto != null && StringUtils.isNotBlank(userPhoto.getId())) {
            userPhoto.prepare();
        }

        Note note = request.getNote();
        if (note != null && StringUtils.isNotBlank(note.getMessage()) && StringUtils.isBlank(note.getId())) {
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        } else if (note != null && StringUtils.isNotBlank(note.getMessage())) {
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        }

        if (request.getAssets() != null) {
            for (CompanyAsset companyAsset : request.getAssets()) {
                if (StringUtils.isBlank(companyAsset.getId())) {
                    companyAsset.prepare();
                }
            }
        }

        dbCompanyContact.setSalutation(request.getSalutation());
        dbCompanyContact.setFirstName(request.getFirstName());
        dbCompanyContact.setLastName(request.getLastName());
        dbCompanyContact.setEmail(request.getEmail());
        dbCompanyContact.setRole(request.getRole());
        dbCompanyContact.setPhoneNumber(request.getPhoneNumber());
        dbCompanyContact.setUserPhoto(userPhoto);
        dbCompanyContact.setNote(note);
        dbCompanyContact.setOfficeNumber(request.getOfficeNumber());
        dbCompanyContact.setCustomerCompanyId(request.getCustomerCompanyId());
        dbCompanyContact.setAssets(request.getAssets());

        CompanyContact updatedContact = companyContactRepository.update(contactId, dbCompanyContact);
        if (updatedContact != null) {
            prepareCompanyContactLogs(updatedContact.getCustomerCompanyId(), updatedContact.getId(), "Contact updated by ");
        }
        return updatedContact;
    }

    @Override
    public CompanyContactResult getCompanyContactById(String contactId) {
        return getCompanyContactById(contactId, true);
    }

    private void prepareCompanyContactResult(CompanyContactResult contact) {
        Vendor customerCompany = vendorRepository.get(token.getCompanyId(), contact.getCustomerCompanyId());
        contact.setCustomerCompany(customerCompany);
    }

    @Override
    public void deleteCompanyContact(String contactId) {
        if (StringUtils.isBlank(contactId)) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }
        companyContactRepository.removeById(contactId);
    }

    @Override
    public SearchResult<CompanyContactResult> getAllCompanyContact(String customerCompanyId, int start, int limit, String term) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<CompanyContactResult> searchResult;

        if (StringUtils.isBlank(customerCompanyId)) {
            if (StringUtils.isBlank(term)) {
                searchResult = companyContactRepository.getAllCompanyContact(token.getCompanyId(), "{modified:-1}", start, limit, CompanyContactResult.class);
            } else {
                searchResult = companyContactRepository.getAllCompanyContactToSearch(token.getCompanyId(), "{firstName:1}", term, start, limit, CompanyContactResult.class);
            }

        } else {
            if (StringUtils.isBlank(term)) {
                searchResult = companyContactRepository.getAllCompanyContactByCustomerCompany(token.getCompanyId(), customerCompanyId, "{modified:-1}", start, limit, CompanyContactResult.class);
            } else {
                searchResult = companyContactRepository.getCompanyContactByCustomerCompanySearch(token.getCompanyId(), customerCompanyId, term, "{modified:-1}", start, limit, CompanyContactResult.class);
            }
        }

        if (searchResult != null && searchResult.getValues() != null) {
            for (CompanyContactResult contactResult : searchResult.getValues()) {
                prepareCompanyContactResult(contactResult);
            }
        }

        return searchResult;
    }

    @Override
    public CompanyContactResult getCompanyContactById(String contactId, boolean isRequired) {
        if (StringUtils.isBlank(contactId)) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }
        CompanyContactResult contactResult = companyContactRepository.getCompanyContactById(contactId, CompanyContactResult.class);

        if (contactResult == null && isRequired) {
            throw new BlazeInvalidArgException(CONTACT, CONTACT_NOT_FOUND);
        }
        if (contactResult != null) {
            this.prepareCompanyContactResult(contactResult);
        }
        return contactResult;
    }
}
