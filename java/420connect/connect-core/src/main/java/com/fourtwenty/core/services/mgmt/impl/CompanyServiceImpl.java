package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmployeeSeatRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CompanyService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import com.amazonaws.util.CollectionUtils;
import javax.inject.Inject;
import java.io.InputStream;
import java.util.List;

/**
 * Created by mdo on 5/15/16.
 */
public class CompanyServiceImpl extends AbstractAuthServiceImpl implements CompanyService {
    private static final String COMPANY = "Company";
    @Inject
    CompanyRepository companyRepository;
    @Inject
    AmazonS3Service amazonS3Service;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    EmployeeRepository employeeRepository;


    @Inject
    public CompanyServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Inject
    private AmazonServiceManager amazonServiceManager;

    @Inject
    private ConnectConfiguration connectConfiguration;

    @Inject
    private ShopRepository shopRepository;

    @Override
    public Company getCurrentCompany() {
        return companyRepository.getById(token.getCompanyId());
    }

    @Override
    public List<Company> getCompanies() {
        return companyRepository.findAllCompany();
    }

    @Override
    public Company updateCompany(Company company) {
        Company dbCompany = companyRepository.getById(token.getCompanyId());

        //dbCompany.setEmail(company.getEmail());
        dbCompany.setSupportEmail(company.getSupportEmail());
        dbCompany.setName(company.getName());
        dbCompany.setSupportEmail(company.getSupportEmail());
        dbCompany.setAddress(company.getAddress());
        dbCompany.setPhoneNumber(company.getPhoneNumber());
        dbCompany.setWebsite(company.getWebsite());
        dbCompany.setEnableLoyalty(company.isEnableLoyalty());
        dbCompany.setDollarToPointRatio(company.getDollarToPointRatio());
        dbCompany.setPricingOpt(company.getPricingOpt());
        dbCompany.setPreferredEmailColor(company.getPreferredEmailColor());
        dbCompany.setLoyaltyAccrueOpt(company.getLoyaltyAccrueOpt());
        if (dbCompany.getAddress() != null) {
            dbCompany.getAddress().prepare(token.getCompanyId());
        }
        dbCompany.setPortalUrl(company.getPortalUrl());
        dbCompany.setBusinessLocation(company.getBusinessLocation());
        dbCompany.setPrimaryContact(company.getPrimaryContact());
        dbCompany.setFax(company.getFax());
        dbCompany.setTaxId(company.getTaxId());
        dbCompany.setEnableSpringBig(company.isEnableSpringBig());
        dbCompany.setDefaultPaymentTerm(company.getDefaultPaymentTerm());
        dbCompany.setSalesPerson(company.getSalesPerson());
        dbCompany.setContactPerson(company.getContactPerson());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(company.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            dbCompany.setExternalId(company.getExternalId());
        }

        companyRepository.update(token.getCompanyId(), dbCompany);
        return dbCompany;
    }

    @Override
    public Company updateCompanyLogo(InputStream inputStream) {
        Company company = companyRepository.getById(token.getCompanyId());
        String url = amazonS3Service.uploadPublicFile(inputStream);
        company.setLogoURL(url);
        companyRepository.update(token.getCompanyId(), company);
        return company;
    }

    @Override
    public CompanyFeatures getCompanyFeatures() {
        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        return companyFeatures;
    }

    @Override
    public Company updateCompanyQueueUrl(String queueUrl) {
        Company company = companyRepository.getById(token.getCompanyId());
        company.setQueueUrl(queueUrl);
        companyRepository.update(token.getCompanyId(), company);
        return company;
    }

    @Override
    public void sendEmailToCompany(String companyId, CompanyEmailRequest request) {
        if (StringUtils.isBlank(companyId)) {
            throw new BlazeInvalidArgException(COMPANY, "Company can't be blank");
        }
        final Company company = companyRepository.getById(companyId);
        if (company == null) {
            throw new BlazeInvalidArgException(COMPANY, "Company not found");
        }
        String body = this.emailBody(company);
        amazonServiceManager.sendEmail("support@blaze.me", request.getEmail(), connectConfiguration.getAppName(), body, null, null, "");


    }

    @Override
    public Company.OnPremCompanyConfig getOnPremConfigs(final String companyId) {
        final Company company = companyRepository.getById(companyId);

        if (company.getOnPremCompanyConfig() == null || !company.getOnPremCompanyConfig().isOnPremEnable()) {
            throw new BlazeInvalidArgException("OnPrem", "OnPrem mode is not enabled for this company");
        }
        company.getOnPremCompanyConfig().setOnPremKey(null);
        company.getOnPremCompanyConfig().setIv(null);
        return company.getOnPremCompanyConfig();
    }


    private String emailBody(Company company) {

        return "Test Email";
    }

    @Override
    public void updateCompanyMaxEmployee(CompanyEmployeeSeatRequest request){
        List<Employee> employees = employeeRepository.getEmployees(request.getCompanyId());
        //Validate that new Employee seats cant be lower than current active employees
        if (!CollectionUtils.isNullOrEmpty(employees)){
            Integer currentEmployeeCount = employees.size();
            if (currentEmployeeCount>request.getQuantity()){
                throw new BlazeInvalidArgException("Employee Seats", "Employees seats can't be lower than " + currentEmployeeCount);
            }
        }
        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(request.getCompanyId());
        companyFeatures.setMaxEmployees(request.getQuantity());
        companyFeaturesRepository.update(companyFeatures.getId(), companyFeatures);

    }

}
