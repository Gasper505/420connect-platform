package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.repositories.dispensary.ContractRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.company.ContractAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ContractService;
import com.google.inject.Provider;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Stephen Schmidt on 12/7/2015.
 */
public class ContractServiceImpl extends AbstractAuthServiceImpl implements ContractService {
    @Inject
    ContractRepository contractRepository;

    @Inject
    public ContractServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public DateSearchResult<Contract> getCompanyContracts(long afterDate, long beforeDate) {
        return contractRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public SearchResult<Contract> getContractsForShop() {
        return contractRepository.findItems(token.getCompanyId(), token.getShopId(), 0, Integer.MAX_VALUE);
    }

    @Override
    public Contract getContract(String contractId) {
        return contractRepository.get(token.getCompanyId(), contractId);
    }

    @Override
    public Contract addContract(ContractAddRequest addRequest) {

        List<Contract> contracts = contractRepository.getActiveContracts(token.getCompanyId(), token.getShopId());
        for (Contract contract : contracts) {
            contract.setActive(false);
            contractRepository.save(contract);
        }

        Contract contract = new Contract();
        contract.setId(ObjectId.get().toString());
        contract.setCompanyId(token.getCompanyId());
        contract.setShopId(token.getShopId());
        contract.setName(addRequest.getName());
        contract.setVersion(1);
        contract.setText(addRequest.getText());
        contract.setActive(addRequest.isActive());
        contract.setRequired(addRequest.isRequired());
        contract.setPdfFile(addRequest.getPdfFile());
        contract.setContentType(addRequest.getContentType());
        contract.setEnableEmployeeSignature(addRequest.getEnableEmployeeSignature());
        contract.setEnableWitnessSignature(addRequest.getEnableWitnessSignature());

        contractRepository.save(contract);
        return contract;
    }

    @Override
    public void setActiveContract(String contractId) {
        //if specified contract exists, set others to deactivated then active selected
        Contract c = contractRepository.get(token.getCompanyId(), contractId);
        if (c != null) {
            contractRepository.setContractsToActive(token.getCompanyId(), token.getShopId(), false);
            contractRepository.activateContract(contractId, !c.isActive());
        }

    }

    @Override
    public Contract updateContract(String contractId, Contract contract) {
        Contract dbContract = contractRepository.get(token.getCompanyId(), contractId);
        if (dbContract == null) {
            throw new BlazeInvalidArgException("Contract", "Contract does not exist.");
        }
        dbContract.setRequired(contract.isRequired());
        dbContract.setActive(contract.isActive());
        dbContract.setName(contract.getName());
        dbContract.setEnableWitnessSignature(contract.getEnableWitnessSignature());
        dbContract.setEnableEmployeeSignature(contract.getEnableEmployeeSignature());
        dbContract.setVersion(dbContract.getVersion() + 1);

        if (dbContract.isActive()) {
            // make sure other contracts are non-active
            contractRepository.setContractsToActive(token.getCompanyId(), token.getShopId(), false);
        }

        contractRepository.update(token.getCompanyId(), dbContract.getId(), dbContract);
        return dbContract;
    }

    @Override
    public void deleteContract(String contractId) {
        contractRepository.removeByIdSetState(token.getCompanyId(), contractId);
    }
}
