package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginShopSetting;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.ConversationJobManager;
import com.fourtwenty.core.reporting.processing.ProcessorUtil;
import com.fourtwenty.core.rest.features.ActiveConversationResult;
import com.fourtwenty.core.rest.features.ConversationResult;
import com.fourtwenty.core.rest.features.TwilioMessage;
import com.fourtwenty.core.rest.features.TwilioMessageRequest;
import com.fourtwenty.core.rest.features.TwilioResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.EmployeeNotificationService;
import com.fourtwenty.core.services.mgmt.ConversationService;
import com.fourtwenty.core.services.plugins.MessagingPluginService;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConversationServiceImpl extends AbstractAuthServiceImpl implements ConversationService {
    private static final Logger log = LoggerFactory.getLogger(ConversationServiceImpl.class);
    private static final String CONVERSATION_SERVICE = "ConversationService";
    private static final List<String> OUTOUT_MESSAGES = Arrays.asList(new String[]{"STOP", "UNSUBSCRIBE"});
    private static final List<String> OUTIN_MESSAGES = Arrays.asList(new String[]{"UNSTOP", "START"});
    @Inject
    private ConversationRepository conversationRepository;
    @Inject
    private TwilioMessageService twilioMessageService;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private MemberRepository memberRepository;
    private ConversationJobManager conversationJobManager;
    private MessagingPluginService messagingPluginService;
    private CompanyFeaturesRepository companyFeaturesRepository;
    private AppRepository appRepository;
    private BouncedNumberRepository bouncedNumberRepository;
    @Inject
    private CompanyBounceNumberRepository companyBounceNumberRepository;
    @Inject
    private EmployeeNotificationService employeeNotificationService;
    @Inject
    private EmployeeRepository employeeRepository;

    @Inject
    public ConversationServiceImpl(Provider<ConnectAuthToken> token, final AppRepository appRepository, final ConversationJobManager conversationJobManager, MessagingPluginService messagingPluginService, CompanyFeaturesRepository companyFeaturesRepository, BouncedNumberRepository bouncedNumberRepository) {
        super(token);
        this.appRepository = appRepository;
        this.conversationJobManager = conversationJobManager;
        this.messagingPluginService = messagingPluginService;
        this.companyFeaturesRepository = companyFeaturesRepository;
        this.bouncedNumberRepository = bouncedNumberRepository;
    }


    @Override
    public List<Conversation> getTransactionConversation(final String transactionId, final long afterDate, final long beforeDate, final Conversation.ConversationType type) {
        long bDate = beforeDate;
        if (bDate <= 0) {
            bDate = DateTime.now().getMillis();
        }
        return conversationRepository.getTransactionConversation(transactionId, afterDate, bDate, type, Conversation.class);
    }

    @Override
    public Conversation getConversationByMessageId(final String sid) {
        return conversationRepository.getConversationByMessageId(sid);
    }

    @Override
    public String getTransactionToAndFromNumber(final String toNumber, final String fromNumber) {
        // Shop shop = getShopByTwilioNumber(..)
        Shop shop = shopRepository.getShopByPhoneNumber(toNumber);
        if (shop != null) {
            final Transaction transaction = conversationRepository.getTransactionToAndFromNumber(toNumber, fromNumber, shop.getCompanyId());

            return transaction.getId();
        }
        return null;
    }

    /**
     * updateConversationByMessageId is used to update the conversation according to status and message id
     *
     * @param messageId
     * @param messageStatus
     */

    @Override
    public void updateConversationByMessageId(final String messageId, final String messageStatus) {

        final Conversation conversation = getConversationByMessageId(messageId);
        if (conversation != null) {
            if ("sent".equalsIgnoreCase(messageStatus)) {
                conversation.setStatus(Conversation.Status.SENT);
                conversation.setSendCompleteDate(new Date());
            } else {
                conversation.setStatus(Conversation.Status.FAILED);
            }
            conversationRepository.update(conversation.getId(), conversation);
        }
    }

    /**
     * sendConversation is used to send conversation on twilio and before send the conversation
     * it save the data into conversation collection if shop is twilio enabled.
     *
     * @param request
     * @return
     */
    @Override
    public Conversation sendConversation(final TwilioMessageRequest request) {
        final String transactionId = request.getTransactionId();
        final String message = request.getMessage();
        final Transaction transaction = transactionRepository.getById(transactionId);
        final String shopId = transaction.getShopId();
        final Shop shop = shopRepository.getById(shopId);
        final Member member = memberRepository.getById(transaction.getMemberId());
        final String memberPhoneNumber = member.getPrimaryPhone();
        final MessagingPluginShopSetting messagingPluginShopSetting = messagingPluginService.getMessagingPluginShopSetting(shopId);
        final CompanyFeatures companyFeature = companyFeaturesRepository.getCompanyFeature(shop.getCompanyId());
        final App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

        if (request.getConversationType() == Conversation.ConversationType.PUSHER) {
            final Conversation conversation = createNewConversation(token.getActiveTopUser().getUserId(), request.getEmployeeId(), member.getId(), request.getSender(), request.getConversationType(), request.getMessage(), transactionId);
            Employee employee = employeeRepository.getById(request.getEmployeeId());
            if ((employee != null) && !(StringUtils.equalsIgnoreCase(token.getActiveTopUser().getUserId(), employee.getId())) && StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
                String title = String.format("#%s - New Message", transaction.getTransNo());
                employeeNotificationService.sendMessageToSpecifiedTerminals(token.getCompanyId(), shopId, Lists.newArrayList(employee.getAssignedTerminalId()), message, FcmPayload.Type.CHAT, request.getTransactionId(), transaction.getTransNo(), title, FcmPayload.SubType.ADD_CHAT, conversation);
            }
            return conversation;
        } else {
            // TODO move to background processing
            if (app.isFakeTwilio() || (messagingPluginShopSetting != null && messagingPluginShopSetting.getEnabled() && companyFeature.getSmsMarketingCredits() > 0)) {
                final Conversation conversation = createNewConversation(token.getActiveTopUser().getUserId(), StringUtils.EMPTY, member.getId(), Conversation.Sender.EMPLOYEE, request.getConversationType(), request.getMessage(), transactionId);
                if (!app.isFakeTwilio()) {
                    //sending message to twilio
                    final TwilioMessage twilioMessage = new TwilioMessage();
                    twilioMessage.setFrom(messagingPluginShopSetting.getTwilioPhoneNumber());
                    twilioMessage.setTo(memberPhoneNumber);
                    twilioMessage.setBody(conversation.getMessage());
                    twilioMessage.setCountry(shop.getAddress().getCountry());
                    twilioMessage.setState(shop.getAddress().getState());

                    conversationJobManager.sendMessageExecutor(conversation, twilioMessage);

                    companyFeaturesRepository.subtractSMSEmailCredits(companyFeature.getCompanyId(), 1, 0);

                } else {
                    conversation.setSid("DUMMY SID" + DateTime.now().getMillis());
                    conversation.setStatus(Conversation.Status.SENT);

                    // Create a dummy response message
                    this.receiveConversation(transactionId, "AUTO-RESPONSE at " + ProcessorUtil.timestamp(DateTime.now().getMillis()), "DUMMY SID " + DateTime.now().getMillis());

                }
                conversationRepository.update(conversation.getId(), conversation);
                return conversation;
            } else {
                throw new BlazeInvalidArgException("twilio", "Twilio is not enabled for this shop");
            }
        }


    }

    private Conversation createNewConversation(String employeeId, String receiverEmployeeId, String memberId, Conversation.Sender sender, Conversation.ConversationType conversationType, String message, String transactionId) {
        final Conversation conversation = new Conversation();
        if (conversationType == Conversation.ConversationType.PUSHER) {
            conversation.prepare(token.getCompanyId()); //prepare conversation only in case of Pusher as we need id of conversation.
        }
        conversation.setEmployeeId(employeeId);
        conversation.setToEmployeeId(receiverEmployeeId);
        conversation.setMemberId(memberId);
        conversation.setTransactionId(transactionId);
        conversation.setMessage(message);
        conversation.setSendRequestDate(new Date());
        conversation.setStatus(Conversation.Status.NEW);
        conversation.setSender(sender);
        conversation.setConversationType(conversationType);
        //Saving conversation information in db
        return conversationRepository.save(conversation);
    }

    /**
     * receiveConversation is used to receive conversation from twilio on these parameter
     * and save a new conversation collection basis on these details.
     *
     * @param transactionId
     * @param message
     * @param sid
     */
    @Override
    public void receiveConversation(final String transactionId, final String message, final String sid) {
        if (transactionId == null) {
            return;
        }

        final Transaction transaction = transactionRepository.getById(transactionId);
        final CompanyFeatures companyFeature = companyFeaturesRepository.getCompanyFeature(transaction.getCompanyId());
        final MessagingPluginShopSetting messagingPluginShopSetting = messagingPluginService.getMessagingPluginShopSetting(transaction.getShopId());
        final App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app.isFakeTwilio() || (messagingPluginShopSetting != null && messagingPluginShopSetting.getEnabled() && companyFeature.getSmsMarketingCredits() > 0)) {
            final Conversation conversation = new Conversation();
            try {
                conversation.setEmployeeId(transaction.getAssignedEmployeeId());
                conversation.setMemberId(transaction.getMemberId());
                conversation.setTransactionId(transactionId);
                conversation.setMessage(message);
                conversation.setSendRequestDate(new Date());
                conversation.setSendCompleteDate(new Date());
                conversation.setStatus(Conversation.Status.RECEIVED);
                conversation.setSender(Conversation.Sender.MEMBER);
                conversation.setSid(sid);

                //Saving conversation information in db
                conversationRepository.save(conversation);

                if (!app.isFakeTwilio()) {
                    companyFeaturesRepository.subtractSMSEmailCredits(companyFeature.getCompanyId(), 1, 0);
                }

            } catch (Exception ex) {
                log.error("No transaction found for this id :" + transactionId, ex);
            }
        }

    }

    @Override
    public boolean isOptOut(String message, final String number, final String shopNumber) {
        if (message != null) {
            message = message.toUpperCase();

            Shop shop = shopRepository.getShopByTwilioNumber(shopNumber);

            if (OUTOUT_MESSAGES.contains(message)) {
                // Add this to bounce number

                final BouncedNumber bouncedNumber = new BouncedNumber();
                bouncedNumber.prepare();
                bouncedNumber.setNumber(number);
                bouncedNumber.setReason("OPT-OUT");
                bouncedNumberRepository.save(bouncedNumber);

                if (shop != null) {
                    CompanyBounceNumber companyBounceNumber = companyBounceNumberRepository.getBouncedNumber(shop.getCompanyId(), number);
                    if (companyBounceNumber == null) {
                        companyBounceNumber = new CompanyBounceNumber();
                        companyBounceNumber.prepare(shop.getCompanyId());
                        companyBounceNumber.setNumber(number);
                        companyBounceNumber.setFromNumber(shopNumber);
                        companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_OUT);
                        companyBounceNumberRepository.save(companyBounceNumber);
                    } else if (companyBounceNumber.getStatus() != CompanyBounceNumber.BounceNumberStatus.OPT_OUT) {
                        companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_OUT);
                        companyBounceNumberRepository.update(companyBounceNumber.getId(), companyBounceNumber);
                    }
                }

                return true;
            } else if (OUTIN_MESSAGES.contains(message)) {
                // remove this from bounce numbers
                bouncedNumberRepository.deleteByNumber(number);
                if (shop != null) {
                    CompanyBounceNumber companyBounceNumber = companyBounceNumberRepository.getBouncedNumber(shop.getCompanyId(), number);
                    if (companyBounceNumber != null && companyBounceNumber.getStatus() != CompanyBounceNumber.BounceNumberStatus.OPT_IN) {
                        companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_OUT);
                        companyBounceNumberRepository.update(companyBounceNumber.getId(), companyBounceNumber);
                    }
                }
            }

        }

        return false;
    }

    /**
     * @param transactionId : String
     * @param afterDate     : long
     * @param beforeDate    : long
     * @return : List<Conversation>
     */
    @Override
    public List<Conversation> getTransactionConversation(String transactionId, long afterDate, long beforeDate) {
        return getTransactionConversation(transactionId, afterDate, beforeDate, Conversation.ConversationType.TWILLIO);
    }

    /**
     * @param afterDate        : long
     * @param beforeDate       : long
     * @param conversationType : ConversationType
     * @return : List<Conversation>
     */
    @Override
    public TwilioResult<List<ConversationResult>> getTransactionConversationByCurrentUser(long afterDate, long beforeDate, Conversation.ConversationType conversationType) {
        long bDate = beforeDate;
        if (bDate <= 0) {
            bDate = DateTime.now().getMillis();
        }
        List<ConversationResult> conversationList = conversationRepository.getTransactionConversationByCurrentUser(token.getActiveTopUser().getUserId(), afterDate, bDate, conversationType, ConversationResult.class);
        return prepareConversation(conversationList);
    }

    @Override
    public TwilioResult<List<ConversationResult>> getConversationByTransaction(String transactionId, long afterDate, long beforeDate, Conversation.ConversationType type) {
        long bDate = beforeDate;
        if (bDate <= 0) {
            bDate = DateTime.now().getMillis();
        }
        return prepareConversation(conversationRepository.getTransactionConversation(transactionId, afterDate, bDate, type, ConversationResult.class));
    }

    /**
     * @param chatId : String
     * @param status : Status
     * @return : Conversation
     */
    @Override
    public Conversation updateMessageStatus(String chatId, Conversation.Status status) {
        if (StringUtils.isBlank(chatId)) {
            throw new BlazeInvalidArgException(CONVERSATION_SERVICE, "Message Id can't be blank");
        }
        Conversation conversation = conversationRepository.getById(chatId);
        if (conversation == null) {
            log.debug("Exception in " + CONVERSATION_SERVICE, "Conversation is not found");
            throw new BlazeInvalidArgException(CONVERSATION_SERVICE, "Conversation is not found");
        }
        conversation.setStatus(status);
        return conversationRepository.update(chatId, conversation);
    }

    /**
     * Private method to prepare conversation
     *
     * @param conversationResultList : conversation list
     * @return
     */
    private TwilioResult<List<ConversationResult>> prepareConversation(List<ConversationResult> conversationResultList) {
        final TwilioResult<List<ConversationResult>> twilioResult = new TwilioResult<>();
        List<ObjectId> senderEmployeeIds = new ArrayList<>();
        List<ObjectId> receiverEmployeeIds = new ArrayList<>();
        List<ConversationResult> newConversationResultList = new ArrayList<>();

        if (conversationResultList.isEmpty()) {
            twilioResult.setStatus(Boolean.FALSE);
            twilioResult.setData(Collections.<ConversationResult>emptyList());
            return twilioResult;
        }

        for (Conversation conversation : conversationResultList) {
            if (StringUtils.isNotBlank(conversation.getEmployeeId()) && ObjectId.isValid(conversation.getEmployeeId())) {
                senderEmployeeIds.add(new ObjectId(conversation.getEmployeeId()));
            }
            if (StringUtils.isNotBlank(conversation.getToEmployeeId()) && ObjectId.isValid(conversation.getToEmployeeId())) {
                receiverEmployeeIds.add(new ObjectId(conversation.getToEmployeeId()));
            }
        }

        HashMap<String, Employee> senderEmployeeList = employeeRepository.findItemsInAsMap(senderEmployeeIds);
        HashMap<String, Employee> receiverEmployeeList = employeeRepository.findItemsInAsMap(receiverEmployeeIds);
        for (ConversationResult conversationResult : conversationResultList) {
            Employee employee = senderEmployeeList.get(conversationResult.getEmployeeId());
            if (employee != null) {
                String firstName = StringUtils.isNotBlank(employee.getFirstName()) ? employee.getFirstName() : "";
                String lastName = StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : "";
                conversationResult.setSenderName(firstName + " " + lastName);
            }

            employee = receiverEmployeeList.get(conversationResult.getToEmployeeId());
            if (employee != null) {
                String firstName = StringUtils.isNotBlank(employee.getFirstName()) ? employee.getFirstName() : "";
                String lastName = StringUtils.isNotBlank(employee.getLastName()) ? employee.getLastName() : "";
                conversationResult.setReceiverName(firstName + " " + lastName);
            }
            newConversationResultList.add(conversationResult);
        }
        twilioResult.setStatus(Boolean.TRUE);
        twilioResult.setData(newConversationResultList);
        return twilioResult;
    }

    /**
     *
     * Override method to return chats
     * @return TwilioResult<List<ActiveConversationResult>>
     * @implNote This is method used to get conversation list by active transaction
     */
    @Override
    public TwilioResult<List<ActiveConversationResult>> getActiveTransactionChat() {
        Iterable<Transaction> transactionIterable = transactionRepository.getAllActiveTransactionsForAssignedEmployee(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());
        List<Transaction> transactionList = Lists.newArrayList(transactionIterable);
        Map<String, Transaction> transactionIdMap = transactionList.stream().collect(Collectors.toMap(Transaction::getId, item -> item));


        // get all transaction ids.
        List<String> transactionIds = transactionList.stream()
                .map(Transaction::getId).collect(Collectors.toList());
        List<ConversationResult> conversationListTwilio = conversationRepository.getActiveTransactionChat(transactionIds, ConversationResult.class);
        TwilioResult<List<ConversationResult>> twilioList = prepareConversation(conversationListTwilio);
        return prepareConversationResult(twilioList.getData(), transactionIdMap);
    }

    /**
     *
     * Private method to prepare chats
     * @param resultList : List<ConversationResult>
     * @return TwilioResult<List<ActiveConversationResult>>
     * @implNote This method is used to set all conversation by transaction id through.
     */
    private TwilioResult<List<ActiveConversationResult>> prepareConversationResult(List<ConversationResult> resultList, Map<String, Transaction> transactionIdMap) {
        TwilioResult<List<ActiveConversationResult>> result = new TwilioResult<>();
        Map<String, ActiveConversationResult> activeTransactionMap = new HashMap<>();

        for (Transaction transaction : transactionIdMap.values()) {

            if (transaction == null) {
                continue;
            }
            if (StringUtils.isNotBlank(transaction.getId())) {
                activeTransactionMap.putIfAbsent(transaction.getId(), new ActiveConversationResult());
                ActiveConversationResult activeConversationResult = activeTransactionMap.get(transaction.getId());
                activeConversationResult.setTransactionId(transaction.getId());
                activeConversationResult.setTransNo(transaction.getTransNo());
                for (ConversationResult conversationResult : resultList) {
                    if (StringUtils.isNotBlank(transaction.getId()) && StringUtils.isNotBlank(conversationResult.getTransactionId()) && (transaction.getId().equals(conversationResult.getTransactionId()))) {
                        if (conversationResult.getConversationType() == null || conversationResult.getConversationType() == Conversation.ConversationType.TWILLIO) {
                            activeConversationResult.getTwilioChats().add(conversationResult);
                        } else {
                            activeConversationResult.getPusherChats().add(conversationResult);
                        }
                    }
                }
            }
        }
        result.setData(Lists.newArrayList(activeTransactionMap.values()));
        return result;
    }


}
