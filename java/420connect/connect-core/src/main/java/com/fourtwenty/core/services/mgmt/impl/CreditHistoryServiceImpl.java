package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CreditHistory;
import com.fourtwenty.core.domain.repositories.dispensary.CreditHistoryRepository;
import com.fourtwenty.core.rest.dispensary.requests.company.CreditHistoryRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CreditHistoryService;
import com.google.inject.Provider;

import javax.inject.Inject;

public class CreditHistoryServiceImpl extends AbstractAuthServiceImpl implements CreditHistoryService {
    private CreditHistoryRepository creditHistoryRepository;
    private static final String CUSTOMER_COMPANY_NOT_FOUND = "There is no Customer Company data found.";
    private static final String CUSTOMER_COMPANY = "Customer Company";
    private static final String COMPANY_NAME_EMPTY = "Company name can't be empty";

    @Inject
    public CreditHistoryServiceImpl(Provider<ConnectAuthToken> token,
                                    CreditHistoryRepository creditHistoryRepository) {
        super(token);
        this.creditHistoryRepository = creditHistoryRepository;
    }

    @Override
    public CreditHistory addCreditHistory(CreditHistoryRequest request) {
        CreditHistory creditHistory = new CreditHistory();
        creditHistory.prepare(token.getCompanyId());
        creditHistory.setShopId(token.getShopId());
        creditHistory.setCustomerCompanyId(request.getCustomerCompanyId());
        creditHistory.setCreditAmount(request.getCreditAmount());
        creditHistory.setCreatedBy(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        return creditHistoryRepository.save(creditHistory);
    }


    @Override
    public CreditHistory updateCreditHistory(String customerCompanyId, CreditHistoryRequest request) {
        CreditHistory creditHistory = new CreditHistory();
        creditHistory.prepare(token.getCompanyId());
        creditHistory.setShopId(token.getShopId());
        creditHistory.setCustomerCompanyId(request.getCustomerCompanyId());
        creditHistory.setCreditAmount(request.getCreditAmount());
        creditHistory.setUpdatedBy(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        return creditHistoryRepository.save(creditHistory);

    }
}
