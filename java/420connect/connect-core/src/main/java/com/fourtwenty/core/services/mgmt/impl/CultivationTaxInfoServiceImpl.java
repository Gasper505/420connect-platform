package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CultivationTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.repositories.dispensary.CultivationTaxInfoRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CultivationTaxInfoService;
import com.google.inject.Provider;

import javax.inject.Inject;

public class CultivationTaxInfoServiceImpl extends AbstractAuthServiceImpl implements CultivationTaxInfoService {

    private CultivationTaxInfoRepository cultivationTaxInfoRepository;

    @Inject
    public CultivationTaxInfoServiceImpl(Provider<ConnectAuthToken> token,
                                         CultivationTaxInfoRepository cultivationTaxInfoRepository) {
        super(token);
        this.cultivationTaxInfoRepository = cultivationTaxInfoRepository;
    }

    @Override
    public CultivationTaxInfo getCultivationTaxInfoForShop(Shop shop) {
        // Default values
        String state = "CA";
        String country = "US";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }
        return cultivationTaxInfoRepository.getCultivationTaxInfoByState(state, country);
    }
}
