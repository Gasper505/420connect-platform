package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CustomerCompany;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.repositories.dispensary.CustomerCompanyRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.rest.dispensary.requests.company.CreditHistoryRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CustomerCompanyAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CustomerCompanyCredit;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CreditHistoryService;
import com.fourtwenty.core.services.mgmt.CustomerCompanyService;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class CustomerCompanyServiceImpl extends AbstractAuthServiceImpl implements CustomerCompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerCompanyServiceImpl.class);
    private static final String CUSTOMER_COMPANY_NOT_FOUND = "There is no Customer Company data found.";
    private static final String CUSTOMER_COMPANY = "Customer Company";
    private static final String COMPANY_NAME_EMPTY = "Company name can't be empty";
    private static final String ATTACHMENT = "Attachment";
    private static final String ATTACHMENT_NOT_FOUND = "Attachment does not found";
    private static final String ATTACHMENT_LIMIT = "Attachment can't be more then 15";

    private CustomerCompanyRepository customerCompanyRepository;
    private CreditHistoryService creditHistoryService;

    @Inject
    public CustomerCompanyServiceImpl(Provider<ConnectAuthToken> token,
                                      CustomerCompanyRepository customerCompanyRepository,
                                      CreditHistoryService creditHistoryService) {
        super(token);
        this.customerCompanyRepository = customerCompanyRepository;
        this.creditHistoryService = creditHistoryService;
    }

    @Override
    public CustomerCompany getCustomerCompany(String customerCompanyId) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany customerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (customerCompany == null) {
            LOGGER.debug("Exception in getCustomerCompany", CUSTOMER_COMPANY_NOT_FOUND);
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        return customerCompany;
    }

    @Override
    public CustomerCompany addCustomerCompany(CustomerCompanyAddRequest request) {

        if (StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, COMPANY_NAME_EMPTY);
        }

        if (request.getCompanyType() == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "Please select company type");
        }
        if (!EmailValidator.getInstance().isValid(request.getEmail())) {
            throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
        }
        CustomerCompany customerCompanyByEmail = customerCompanyRepository.getCustomerCompanyByEmail(request.getEmail());

        if (customerCompanyByEmail != null) {
            throw new BlazeQuickPinExistException("DuplicateEmail", "Another customer company exists with this email.");
        }
        CustomerCompany customerCompany = new CustomerCompany();
        customerCompany.prepare(token.getCompanyId());
        customerCompany.setName(request.getName());
        customerCompany.setActive(true);
        customerCompany.setCompanyType(request.getCompanyType());
        customerCompany.setCredits(request.getCredits());
        customerCompany.setEntityTaxType(request.getEntityTaxType());
        customerCompany.setLicense(request.getLicense());
        customerCompany.setWebsite(request.getWebsite());
        customerCompany.setLicenceType(request.getLicenceType());
        customerCompany.setMobileNumber(request.getMobileNumber());
        customerCompany.setOfficePhoneNumber(request.getOfficePhoneNumber());
        customerCompany.setRelatedEntity(request.isRelatedEntity());
        customerCompany.setLicenceExpirationDate(request.getLicenceExpirationDate());
        customerCompany.setEmail(request.getEmail());
        if (request.getAddresses() != null && request.getAddresses().size() > 0) {
            List<Address> addresses = request.getAddresses();
            List<Address> newAddresses = new ArrayList<>();
            for (Address address : addresses) {
                address.prepare(token.getCompanyId());
                newAddresses.add(address);
            }
            customerCompany.setAddresses(newAddresses);
        }
        if (request.getAttachments() != null && request.getAttachments().size() > 0) {

            List<CompanyAsset> attachments = request.getAttachments();
            List<CompanyAsset> newAttachments = new ArrayList<>();
            for (CompanyAsset asset : attachments) {
                asset.prepare(token.getCompanyId());
                newAttachments.add(asset);
            }
            customerCompany.setAttachments(newAttachments);

        }
        if (request.getNote() != null) {
            Note note = request.getNote();
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            customerCompany.setNote(note);
        }
        return customerCompanyRepository.save(customerCompany);
    }

    @Override
    public CustomerCompany updateCustomerCompany(String customerCompanyId, CustomerCompany customerCompany) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (dbCustomerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        if (StringUtils.isBlank(customerCompany.getName())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, COMPANY_NAME_EMPTY);
        }
        if (!EmailValidator.getInstance().isValid(customerCompany.getEmail())) {
            throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
        }

        if (!dbCustomerCompany.getEmail().equalsIgnoreCase(customerCompany.getEmail())) {
            CustomerCompany customerCompanyByEmail = customerCompanyRepository.getCustomerCompanyByEmail(customerCompany.getEmail());

            if (customerCompanyByEmail != null) {
                throw new BlazeQuickPinExistException("DuplicateEmail", "Another customer company exists with this email.");
            }
        }
        updateCompanyAddress(customerCompany.getAddresses());
        long count = dbCustomerCompany.getAttachments().size();
        if (count < dbCustomerCompany.getMaxAttachment()) {
            updateCompanyAttachment(customerCompany.getAttachments());
        } else {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, ATTACHMENT_LIMIT);
        }


        dbCustomerCompany.setName(customerCompany.getName());
        dbCustomerCompany.setCompanyType(customerCompany.getCompanyType());
        dbCustomerCompany.setAddresses(customerCompany.getAddresses());
        dbCustomerCompany.setLicense(customerCompany.getLicense());
        dbCustomerCompany.setWebsite(customerCompany.getWebsite());
        dbCustomerCompany.setCredits(customerCompany.getCredits());
        dbCustomerCompany.setAttachments(customerCompany.getAttachments());
        dbCustomerCompany.setEntityTaxType(customerCompany.getEntityTaxType());
        dbCustomerCompany.setLicenceType(customerCompany.getLicenceType());
        dbCustomerCompany.setRelatedEntity(customerCompany.isRelatedEntity());
        dbCustomerCompany.setOfficePhoneNumber(customerCompany.getOfficePhoneNumber());
        dbCustomerCompany.setMobileNumber(customerCompany.getMobileNumber());
        dbCustomerCompany.setNote(customerCompany.getNote());
        dbCustomerCompany.setActive(customerCompany.isActive());

        return customerCompanyRepository.update(customerCompanyId, dbCustomerCompany);
    }

    @Override
    public void deleteCustomerCompany(String customerCompanyId) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany customerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        customerCompanyRepository.removeById(customerCompanyId);
    }

    @Override
    public SearchResult<CustomerCompany> getAllCustomerCompany(int start, int limit, String term) {
        if (StringUtils.isBlank(term)) {
            return customerCompanyRepository.getAllCustomerCompany(token.getCompanyId(), "{name:1}", start, limit);
        } else {
            return customerCompanyRepository.getAllCustomerCompany(token.getCompanyId(), start, limit, term);
        }

    }

    @Override
    public CompanyAsset getCustomerCompanyAttachment(String customerCompanyId, String attachmentId) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (dbCustomerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        CompanyAsset companyAsset = this.getAttachmentById(dbCustomerCompany.getAttachments(), attachmentId);

        return companyAsset;
    }

    private CompanyAsset getAttachmentById(List<CompanyAsset> attachments, String attachmentId) {
        CompanyAsset companyAsset = null;
        if (attachments != null) {
            for (CompanyAsset asset : attachments) {
                if (asset.getId().equalsIgnoreCase(attachmentId)) {
                    companyAsset = asset;
                    break;
                }
            }
        }
        if (companyAsset == null) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }
        return companyAsset;
    }

    @Override
    public CompanyAsset updateCustomerCompanyAttachment(String customerCompanyId, String attachmentId, CompanyAsset asset) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (dbCustomerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        CompanyAsset companyAsset = this.getAttachmentById(dbCustomerCompany.getAttachments(), attachmentId);

        if (companyAsset == null) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }

        companyAsset.setName(asset.getName());
        companyAsset.setKey(asset.getKey());
        companyAsset.setType(asset.getType());
        companyAsset.setPublicURL(asset.getPublicURL());
        companyAsset.setActive(asset.isActive());
        companyAsset.setPriority(asset.getPriority());
        companyAsset.setSecured(asset.isSecured());
        companyAsset.setThumbURL(asset.getThumbURL());
        companyAsset.setMediumURL(asset.getMediumURL());
        companyAsset.setLargeURL(asset.getLargeURL());
        long count = dbCustomerCompany.getAttachments().size();
        if (count < dbCustomerCompany.getMaxAttachment()) {
            customerCompanyRepository.update(customerCompanyId, dbCustomerCompany);
        } else {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, ATTACHMENT_LIMIT);
        }


        return companyAsset;
    }

    @Override
    public void deleteCustomerCompanyAttachment(String customerCompanyId, String attachmentId) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (dbCustomerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        CompanyAsset companyAsset = this.getAttachmentById(dbCustomerCompany.getAttachments(), attachmentId);

        if (companyAsset == null) {
            throw new BlazeInvalidArgException(ATTACHMENT, ATTACHMENT_NOT_FOUND);
        }

        List<CompanyAsset> attachments = dbCustomerCompany.getAttachments();
        attachments.remove(companyAsset);
        customerCompanyRepository.update(customerCompanyId, dbCustomerCompany);
    }

    @Override
    public CompanyAsset addCustomerCompanyAttachment(String customerCompanyId, CompanyAsset asset) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.getById(customerCompanyId);
        if (dbCustomerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }

        List<CompanyAsset> attachments = dbCustomerCompany.getAttachments();
        if (attachments == null) {
            attachments = new ArrayList<>();
        }

        asset.prepare();

        attachments.add(asset);

        customerCompanyRepository.update(customerCompanyId, dbCustomerCompany);

        return asset;
    }

    private void updateCompanyAddress(List<Address> companyAddresses) {
        if (companyAddresses != null) {
            for (Address companyAddress : companyAddresses) {
                if (StringUtils.isBlank(companyAddress.getId())) {
                    companyAddress.prepare(token.getCompanyId());
                }
            }
        }
    }

    private void updateCompanyAttachment(List<CompanyAsset> assets) {
        if (assets != null) {
            for (CompanyAsset asset : assets) {
                if (StringUtils.isBlank(asset.getId())) {
                    asset.prepare(token.getCompanyId());
                }
            }
        }
    }

    @Override
    public CustomerCompany addCreditInCustomerCompany(String customerCompanyId, CustomerCompanyCredit companyCredit) {
        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        if (StringUtils.isBlank(companyCredit.getCredit().toString())) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, "Credit can't be empty");
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.get(token.getCompanyId(), customerCompanyId);
        dbCustomerCompany.setCredits(companyCredit.getCredit());
        CustomerCompany updatedCompany = customerCompanyRepository.update(customerCompanyId, dbCustomerCompany);
        if (updatedCompany != null) {
            CreditHistoryRequest creditHistoryRequest = new CreditHistoryRequest();
            creditHistoryRequest.setCustomerCompanyId(customerCompanyId);
            creditHistoryRequest.setCreditAmount(companyCredit.getCredit());
            creditHistoryService.addCreditHistory(creditHistoryRequest);
        }
        return updatedCompany;

    }

    @Override
    public CustomerCompany updateCreditInCustomerCompany(String customerCompanyId, CustomerCompanyCredit companyCredit) {

        if (StringUtils.isBlank(customerCompanyId)) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        CustomerCompany dbCustomerCompany = customerCompanyRepository.get(token.getCompanyId(), customerCompanyId);
        if (dbCustomerCompany == null) {
            throw new BlazeInvalidArgException(CUSTOMER_COMPANY, CUSTOMER_COMPANY_NOT_FOUND);
        }
        dbCustomerCompany.setCredits(companyCredit.getCredit());
        CustomerCompany updatedCompany = customerCompanyRepository.update(customerCompanyId, dbCustomerCompany);
        if (updatedCompany != null) {
            CreditHistoryRequest updateHistoryRequest = new CreditHistoryRequest();
            updateHistoryRequest.setCustomerCompanyId(customerCompanyId);
            updateHistoryRequest.setCreditAmount(companyCredit.getCredit());
            creditHistoryService.updateCreditHistory(customerCompanyId, updateHistoryRequest);
        }
        return updatedCompany;
    }
}
