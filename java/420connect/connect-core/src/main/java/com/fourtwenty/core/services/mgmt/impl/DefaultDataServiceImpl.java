package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.payment.ShopPaymentOptionRepository;
import com.fourtwenty.core.lifecycle.model.CategoryInfo;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyRegisterRequest;
import com.fourtwenty.core.services.mgmt.DefaultDataService;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mdo on 11/22/15.
 */
public class DefaultDataServiceImpl implements DefaultDataService {
    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    DefaultData defaultData;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    RoleRepository roleRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    private AppRolePermissionRepository permissionRepository;
    @Inject
    private ShopPaymentOptionRepository shopPaymentOptionRepository;

    @Override
    public void initializeDefaultCompanyData(String companyId, ConnectProduct connectProduct, CompanyRegisterRequest registerRequest) {

        // Create Default Tolerance
        for (ProductWeightTolerance tolerance : defaultData.getProductTolerances()) {
            tolerance.setCompanyId(companyId);
            tolerance.setEnabled(true);
            tolerance.setId(null);
        }

        toleranceRepository.save(defaultData.getProductTolerances());


        // Create Default Roles
        for (Role role : defaultData.getRoles()) {
            role.setCompanyId(companyId);
            role.setId(null);
        }
        roleRepository.save(defaultData.getRoles());

        // This is for Retail
        Iterable<Role> roles = roleRepository.list(companyId);
        LinkedHashSet<String> rolesList = new LinkedHashSet<>();
        for (Role role : roles) {
            rolesList.add(role.getId());
        }

        // retail
        for (CompanyFeatures.AppTarget appTarget : CompanyFeatures.AppTarget.values()) {
            AppRolePermission rolePermission = new AppRolePermission();
            rolePermission.prepare(companyId);
            rolePermission.setAppTarget(appTarget);
            rolePermission.setRoleList(rolesList);
            permissionRepository.save(rolePermission);
        }


        // Company features
        CompanyFeatures companyFeatures = new CompanyFeatures();
        companyFeatures.setCompanyId(companyId);
        if (connectProduct == null) {
            companyFeatures.setMaxTerminals(2);
            companyFeatures.setMaxShop(1);
            companyFeatures.setMaxEmployees(10);
            companyFeatures.setAvailableQueues(Lists.newArrayList(Transaction.QueueType.Delivery));
            companyFeatures.setAvailableApps(Lists.newArrayList(CompanyFeatures.AppTarget.Retail, CompanyFeatures.AppTarget.AuthenticationApp));
        } else {
            companyFeatures.setMaxTerminals(connectProduct.getMaxTerminals());
            companyFeatures.setMaxShop(connectProduct.getMaxShop());
            companyFeatures.setMaxEmployees(connectProduct.getMaxEmployees());
            companyFeatures.setAvailableQueues(connectProduct.getAvailableQueues());
            companyFeatures.setAvailableApps(connectProduct.getAvailableApps());
        }

        if (registerRequest != null) {
            if (registerRequest.getNumEmployees() >0) {
                companyFeatures.setMaxEmployees(registerRequest.getNumEmployees());
            }
            if (registerRequest.getNumShops() > 0) {
                companyFeatures.setMaxShop(registerRequest.getNumShops());
            }
            if (registerRequest.getNumInventories() > 0) {
                companyFeatures.setMaxInventories(registerRequest.getNumInventories());
            }
            if (registerRequest.getNumTerms() > 0) {
                companyFeatures.setMaxTerminals(registerRequest.getNumTerms());
            }
        }

        companyFeaturesRepository.save(companyFeatures);
    }

    @Override
    public void initializeDefaultShopData(Shop shop, HashMap<String, MemberGroup> membershipLevelHashMap, boolean newCompany, Company company) {
        TaxInfo taxInfo = defaultData.getTaxInfos().get(0);
        shop.setTaxInfo(taxInfo);

        LinkedHashSet<String> flowerTypes = new LinkedHashSet<>();
        flowerTypes.add("Indica");
        flowerTypes.add("Sativa");
        flowerTypes.add("Hybrid");
        flowerTypes.add("CBD");
        shop.setFlowerType(flowerTypes);
        shopRepository.upsert(shop.getId(), shop);

        // Define default payment options
        List<Cart.PaymentOption> defaultPaymentOptions = new ArrayList<>();
        defaultPaymentOptions.add(Cart.PaymentOption.Cash);
        defaultPaymentOptions.add(Cart.PaymentOption.Credit);
        defaultPaymentOptions.add(Cart.PaymentOption.Check);
        defaultPaymentOptions.add(Cart.PaymentOption.Split);
        defaultPaymentOptions.add(Cart.PaymentOption.StoreCredit);
        defaultPaymentOptions.add(Cart.PaymentOption.Linx);
        defaultPaymentOptions.add(Cart.PaymentOption.CashlessATM);

        // Create shopPaymentOptions to save
        List<ShopPaymentOption> shopPaymentOptions = new ArrayList<>();
        shopPaymentOptions.addAll(
                defaultPaymentOptions
                        .stream()
                        .map(po -> {
                            ShopPaymentOption spo = new ShopPaymentOption();
                            spo.prepare(shop.getCompanyId());
                            spo.setShopId(shop.getId());
                            spo.setPaymentOption(po);

                            if (po == Cart.PaymentOption.Linx) {
                                spo.setEnabled(false);
                            } else {
                                spo.setEnabled(true);
                            }

                            return spo;
                        })
                        .collect(Collectors.toList()));

        shopPaymentOptionRepository.save(shopPaymentOptions);


        // Create Default member levels
        if (newCompany || (company != null && company.getMembersShareOption() == Company.CompanyMembersShareOption.Isolated)) {
            for (MemberGroup level : defaultData.getMemberGroups()) {
                level.setId(null);
                level.setCompanyId(shop.getCompanyId());
                level.setShopId(shop.getId());
            }
            List<MemberGroup> levels = memberGroupRepository.save(defaultData.getMemberGroups());
            if (membershipLevelHashMap != null) {
                for (MemberGroup level : levels) {
                    membershipLevelHashMap.put(shop.getCompanyId(), level);
                }
            }
        }


        List<ProductCategory> productCategories = new ArrayList<>();
        for (CategoryInfo catInfo : defaultData.getProductCategories()) {
            if (shop.getAppTarget() != catInfo.getAppTarget()  && !(catInfo.getAppTarget() == null && (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail || shop.getAppTarget() == CompanyFeatures.AppTarget.Distribution))) {
                continue;
            }
            ProductCategory productCategory = new ProductCategory();
            productCategory.setId(null);
            productCategory.setCompanyId(shop.getCompanyId());
            productCategory.setShopId(shop.getId());
            productCategory.setName(catInfo.getName());
            productCategory.setCannabis(catInfo.isCannabis());
            productCategory.setUnitType(ProductCategory.UnitType.toUnitType(catInfo.getUnitType()));

            CompanyAsset asset = new CompanyAsset();
            asset.setId(ObjectId.get().toString());
            asset.setCompanyId(shop.getCompanyId());
            asset.setName(catInfo.getImageURL());
            asset.setKey("420default-" + catInfo.getImageURL());
            asset.setActive(true);
            asset.setSecured(false);
            asset.setPublicURL("https://s3.amazonaws.com/connect-files-public/" + asset.getKey());
            asset.setType(Asset.AssetType.Photo);
            productCategory.setPhoto(asset);
            productCategories.add(productCategory);

        }
        productCategoryRepository.save(productCategories);


        List<Inventory> inventories = defaultData.getInventories();
        for (Inventory inventory : inventories) {
            inventory.setId(null);
            inventory.setCompanyId(shop.getCompanyId());
            inventory.setShopId(shop.getId());
        }
        List<Inventory> myInventories = inventoryRepository.save(inventories);
        System.out.println(myInventories);

        Inventory safeInventory = inventoryRepository.getInventory(shop.getCompanyId(), shop.getId(), Inventory.SAFE);

        if (newCompany) {
            // create default terminal for each
            Terminal terminal = new Terminal();
            terminal.setShopId(shop.getId());
            terminal.setName("Default Terminal");
            terminal.setActive(true);
            terminal.setCompanyId(shop.getCompanyId());
            terminal.setDeviceType("iOS");
            terminal.setDeviceId(UUID.randomUUID().toString());
            if (safeInventory != null) {
                terminal.setAssignedInventoryId(safeInventory.getId());
            }
            terminalRepository.save(terminal);
        }

        if (shop.getTaxTables() == null || shop.getTaxTables().size() == 0) {
            List<CompoundTaxTable> taxTables = new ArrayList<>();
            shop.setTaxTables(taxTables);

            ConsumerType[] consumerTypes = ConsumerType.values();
            for (ConsumerType consumerType : consumerTypes) {
                if (consumerType == ConsumerType.Other) {
                    continue;
                }
                CompoundTaxTable taxTable = new CompoundTaxTable();
                taxTable.prepare(shop.getCompanyId());
                taxTable.setShopId(shop.getId());
                taxTable.setName(consumerType.getDisplayName());
                taxTable.setConsumerType(consumerType);
                taxTables.add(taxTable);
            }
        }
    }
}
