package com.fourtwenty.core.services.mgmt.impl;

import com.esotericsoftware.kryo.Kryo;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.fourtwenty.core.caching.CacheService;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberDistanceResult;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.*;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeDailyMileageResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeTransactionDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryByEmployeeResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductPackagesResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.DeliveryUtilService;
import com.fourtwenty.core.services.inventory.InventoryActionLog;
import com.fourtwenty.core.services.mgmt.DeliveryService;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.services.mgmt.TimeCardService;
import com.fourtwenty.core.services.mgmt.models.DistanceElement;
import com.fourtwenty.core.services.mgmt.models.DistanceElementWrapper;
import com.fourtwenty.core.services.mgmt.models.GoogleDistanceResult;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 11/9/17.
 */
public class DeliveryServiceImpl extends AbstractAuthServiceImpl implements DeliveryService {
    private static final Log LOG = LogFactory.getLog(DeliveryServiceImpl.class);

    private static final String DELIVERY_AREA = "Delivery area";
    private static final String DELIVERY_AREA_NOT_FOUND = "Delivery area not found";
    private static final String NO_LOCATION = "Please provide location";
    private static final String NO_DELIVERY_ZONE = "Please provide delivery zone type";
    private static final String LOCATION = "Location";
    private static final String DELIVERY_ZONE = "Delivery Zone";
    private static final String EMPLOYEE = "Employee";
    private static final String EMPLOYEE_NOT_FOUND = "Employee not found";
    private static final String DRIVER_SUGGESTION = "Driver Suggestion";
    private static final int CACHE_LIMIT_MINS = 10; // 10 minutes


    @Inject
    private ConnectConfiguration connectConfiguration;
    private DeliveryAreaRepository deliveryAreaRepository;
    private TransactionRepository transactionRepository;
    private EmployeeRepository employeeRepository;
    private RoleRepository roleRepository;
    private TerminalLocationRepository terminalLocationRepository;
    private EmployeeService employeeService;
    private TerminalRepository terminalRepository;
    private ProductRepository productRepository;
    private PrepackageRepository prepackageRepository;
    private MemberRepository memberRepository;
    private ProductPrepackageQuantityRepository quantityRepository;
    private ProductCategoryRepository productCategoryRepository;
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    private InventoryRepository inventoryRepository;
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TimeCardService timeCardService;
    @Inject
    private DeliveryUtilService deliveryUtilService;
    @Inject
    private CacheService cacheService;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private  ProductBatchRepository productBatchRepository;

    @Inject
    public DeliveryServiceImpl(Provider<ConnectAuthToken> tokenProvider, DeliveryAreaRepository deliveryAreaRepository, TransactionRepository transactionRepository, EmployeeRepository employeeRepository,
                               RoleRepository roleRepository, TerminalLocationRepository terminalLocationRepository, EmployeeService employeeService,
                               TerminalRepository terminalRepository, ProductRepository productRepository, PrepackageRepository prepackageRepository, MemberRepository memberRepository, ProductPrepackageQuantityRepository quantityRepository, ProductCategoryRepository productCategoryRepository, ProductPrepackageQuantityRepository prepackageQuantityRepository, InventoryRepository inventoryRepository, ConsumerCartRepository consumerCartRepository) {
        super(tokenProvider);
        this.deliveryAreaRepository = deliveryAreaRepository;
        this.transactionRepository = transactionRepository;
        this.employeeRepository = employeeRepository;
        this.roleRepository = roleRepository;
        this.terminalLocationRepository = terminalLocationRepository;
        this.employeeService = employeeService;
        this.terminalRepository = terminalRepository;
        this.productRepository = productRepository;
        this.prepackageRepository = prepackageRepository;
        this.memberRepository = memberRepository;
        this.quantityRepository = quantityRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.prepackageQuantityRepository = prepackageQuantityRepository;
        this.inventoryRepository = inventoryRepository;
        this.consumerCartRepository = consumerCartRepository;
    }


    //https://maps.googleapis.com/maps/api/distancematrix/json?origins=Fresno+CA&destinations=San+Francisco+CA&units=imperial&mode=driving&language=en-US&key=AIzaSyB03btOhaWdGzNVoCPv4PI2uz26ZVJEHQI
    //https://maps.googleapis.com/maps/api/distancematrix/json?origins=36.8223800,-119.6501550&destinations=San+Francisco+CA&units=imperial&mode=driving&language=en-US&key=AIzaSyB03btOhaWdGzNVoCPv4PI2uz26ZVJEHQI

    /**
     * Get the distance using Google apis.
     *
     * @param origin      - Can be lat,longitude or address
     * @param destination Can be latitude,longitude or address
     */
    private GoogleDistanceResult getDistanceWithGoogle(final String aCacheKey,final String origin, final String destination) {

        // Clear cache if more than 5 minutes remove that otw use the cached result
        final String cacheKey = cacheService.generateKey(aCacheKey, CacheService.CacheType.DATA);
        GoogleDistanceResult item = cacheService.getItem(cacheKey, GoogleDistanceResult.class);
        if (item != null) {
            if (DateTime.now().minusMinutes(CACHE_LIMIT_MINS).getMillis() > item.getCreated()) {
                cacheService.invalidateItems(new String[]{cacheKey});
            } else {
                LOG.info("Distance cache item found...");
                return item;
            }
        }

        LOG.info("Distance cache item missed...");
        // Uses Google API services
        final String origins = origin;
        final String destinations = destination;
        final String units = "imperial";
        final String mode = "driving";
        final String language = "en-US";
        final String key = connectConfiguration.getGoogleAPIKey();
        final String apiPath = "https://maps.googleapis.com/maps/api/distancematrix/json";

        /*final String urlPath = String.format("%s?origins=%s&destinations=%s&units=%s&mode=%s&language=%s&key=%s",
                apiPath,
                origins,
                destinations,
                units,
                mode,
                language,
                key);*/
        final String urlPath = String.format("%s?units=%s&mode=%s&language=%s&key=%s",
                apiPath,
                units,
                mode,
                language,
                key);

        try {
            char c = urlPath.charAt(86);
            Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(urlPath).queryParam("origins", origins).queryParam("destinations", destinations);
            GoogleDistanceResult result = SimpleRestUtil.get(GoogleDistanceResult.class, webTarget);

            // Cache it for 5 minutes
            cacheService.putItem(cacheKey, result);

            return result;
        } catch (Exception e) {
            LOG.error("Error retrieving distance", e);
        }
        return null;
    }


    /**
     * This method add delivery area for a shop
     *
     * @param deliveryZone : delivery area
     */
    @Override
    public DeliveryZone addDeliverZone(DeliveryZone deliveryZone) {

        if (deliveryZone.getZoneType() == null) {
            throw new BlazeInvalidArgException(DELIVERY_ZONE, NO_DELIVERY_ZONE);
        }

        if (deliveryZone.getLoc().isEmpty()) {
            throw new BlazeInvalidArgException(LOCATION, NO_LOCATION);
        }

        deliveryZone.prepare(token.getCompanyId());
        deliveryZone.setShopId(token.getShopId());

        return deliveryAreaRepository.save(deliveryZone);
    }

    /**
     * Update delivery area
     *
     * @param deliveryAreaId : delivery area's id that need to be update
     * @param deliveryZone   : delivery area's updates information
     */
    @Override
    public DeliveryZone updateDeliveryArea(String deliveryAreaId, DeliveryZone deliveryZone) {

        DeliveryZone dbDeliveryZone = deliveryAreaRepository.get(token.getCompanyId(), deliveryAreaId);

        if (dbDeliveryZone == null) {
            throw new BlazeInvalidArgException(DELIVERY_AREA, DELIVERY_AREA_NOT_FOUND);
        }

        if (deliveryZone.getZoneType() == null) {
            throw new BlazeInvalidArgException(DELIVERY_ZONE, NO_DELIVERY_ZONE);
        }

        if (deliveryZone.getLoc().isEmpty()) {
            throw new BlazeInvalidArgException(LOCATION, NO_LOCATION);
        }

        dbDeliveryZone.setZoneType(deliveryZone.getZoneType());
        dbDeliveryZone.setLoc(deliveryZone.getLoc());

        return deliveryAreaRepository.update(token.getCompanyId(), deliveryAreaId, dbDeliveryZone);
    }

    /**
     * Delete delivery area by id
     *
     * @param deliveryAreaId : delivery area by id
     */
    @Override
    public void deleteDeliveryArea(String deliveryAreaId) {
        DeliveryZone dbDeliveryZone = deliveryAreaRepository.get(token.getCompanyId(), deliveryAreaId);

        if (dbDeliveryZone == null) {
            throw new BlazeInvalidArgException(DELIVERY_AREA, DELIVERY_AREA_NOT_FOUND);
        }

        deliveryAreaRepository.removeById(token.getCompanyId(), deliveryAreaId);
    }

    /**
     * Get list of delivery zones
     *
     * @param start    : start
     * @param limit    : limit of result
     * @param zoneType : type of DeliveryZoneType
     */
    @Override
    public SearchResult<DeliveryZone> getAllDeliveryZone(int start, int limit, DeliveryZone.DeliveryZoneType zoneType) {
        SearchResult<DeliveryZone> searchResult;
        if (zoneType != null) {
            searchResult = deliveryAreaRepository.getAllDeliveryAreaByZoneType(token.getCompanyId(), token.getShopId(), start, limit, "{created:-1}", zoneType);
        } else {
            searchResult = deliveryAreaRepository.getAllDeliveryArea(token.getCompanyId(), token.getShopId(), start, limit, "{created:-1}");
        }

        return searchResult;
    }

    /**
     * This method gets free delivery drivers with location
     *
     * @param start
     * @param limit
     */
    @Override
    public SearchResult<EmployeeLocationResult> getFreeDrivers(int start, int limit) {

        SearchResult<EmployeeLocationResult> freeDriverSearchResult = new SearchResult<>();

        List<EmployeeLocationResult> freeEmployeeList = new ArrayList<>();

        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(token.getCompanyId());
        SearchResult<Transaction> activeTransactions = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId());
        SearchResult<EmployeeLocationResult> employeeItems = employeeRepository.findItems(token.getCompanyId(), start, limit, EmployeeLocationResult.class);

        List<EmployeeLocationResult> employees = employeeItems.getValues();
        if (employees == null) {
            employees = new ArrayList<>();
        }

        Iterable<TerminalLocation> terminalLocations = getTerminalLocationsForTransactions(activeTransactions);
        Map<String, TerminalLocation> terminalLocationMapByEmployee = new HashMap<>();

        //Create location map by employee id
        for (TerminalLocation terminalLocation : terminalLocations) {
            TerminalLocation location = terminalLocationMapByEmployee.get(terminalLocation.getEmployeeId());
            if (location == null) {
                terminalLocationMapByEmployee.put(terminalLocation.getEmployeeId(), terminalLocation);
            } else if (terminalLocation.getCreated() > location.getCreated()) {
                terminalLocationMapByEmployee.put(terminalLocation.getEmployeeId(), terminalLocation);
            }
        }

        boolean isEmployeeFree;
        for (EmployeeLocationResult employee : employees) {

            isEmployeeFree = true;
            if (activeTransactions.getValues() != null) {
                for (Transaction transaction : activeTransactions.getValues()) {
                    //check if employee is present in active transaction
                    if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && transaction.getAssignedEmployeeId().equalsIgnoreCase(employee.getId())) {
                        isEmployeeFree = false;
                        break;
                    }
                }
            }

            if (isEmployeeFree) {
                TerminalLocation location = terminalLocationMapByEmployee.get(employee.getId());

                if (location != null) {
                    employee.setLoc(location.getLoc());
                }

                Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());
                if (terminal != null) {
                    employee.setTerminalName(terminal.getName());
                }

                freeEmployeeList.add(employee);
            }
        }

        freeDriverSearchResult.setLimit(limit);
        freeDriverSearchResult.setSkip(start);
        freeDriverSearchResult.setValues(freeEmployeeList);
        freeDriverSearchResult.setTotal((long) freeEmployeeList.size());
        return freeDriverSearchResult;
    }

    private Iterable<TerminalLocation> getTerminalLocationsForTransactions(SearchResult<Transaction> activeTransactions) {
        // find first transaction
        long afterDate = DateTime.now().getMillis();
        for (Transaction transaction : activeTransactions.getValues()) {
            if (transaction.getCreated() < afterDate) {
                afterDate = transaction.getCreated();
            }
        }
        Iterable<TerminalLocation> terminalLocations = terminalLocationRepository.getTermLocationsAfter(token.getCompanyId(),
                token.getShopId(),
                afterDate, "{created:-1}");
        return terminalLocations;
    }

    private Iterable<TerminalLocation> getTerminalLocationsForTransactions(SearchResult<Transaction> activeTransactions, String employeeId) {
        // find first transaction
        long afterDate = DateTime.now().getMillis();
        long beforeDate = 0l;
        for (Transaction transaction : activeTransactions.getValues()) {
            if (transaction.getCreated() < afterDate) {
                afterDate = transaction.getCreated();
            }
            if (transaction.getCreated() > beforeDate) {
                beforeDate = transaction.getCreated();
            }
        }
        Iterable<TerminalLocation> terminalLocations = terminalLocationRepository.getTermLocationsWithDate(
                token.getCompanyId(),
                token.getShopId(),
                afterDate,
                beforeDate,
                "{created:-1}",
                employeeId);
        return terminalLocations;
    }

    /**
     * This method gets route of delivery driver employee
     */
    @Deprecated
    @Override
    public List<EnRouteDriverEmployeeResult> getEnRouteDrivers() {
        List<EnRouteDriverEmployeeResult> employeeResults = new ArrayList<>();
        SearchResult<Transaction> activeTransactions = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId());


        HashMap<String, EnRouteDriverEmployeeResult> employeeMap = employeeRepository.getEmployeesAsMap(token.getCompanyId(), EnRouteDriverEmployeeResult.class);
        Iterable<TerminalLocation> terminalLocations = getTerminalLocationsForTransactions(activeTransactions); //listByShopSort(token.getCompanyId(), token.getShopId(), "{created:-1}", 0, Integer.MAX_VALUE);

        //Get assigned employee of active transaction and TerminalLocation of respective driver
        if (activeTransactions.getValues() != null) {
            for (Transaction transaction : activeTransactions.getValues()) {

                EnRouteDriverEmployeeResult employee = employeeMap.get(transaction.getAssignedEmployeeId());

                if (employee != null) {
                    List<TerminalLocation> employeeLocations = new ArrayList<>();
                    for (TerminalLocation terminalLocation : terminalLocations) {
                        if (terminalLocation.getEmployeeId().equalsIgnoreCase(employee.getId()) && terminalLocation.getTerminalId().equalsIgnoreCase(transaction.getTerminalId())) {
                            employeeLocations.add(terminalLocation);
                        }
                    }

                    employee.setTerminalLocationList(employeeLocations);
                    employeeResults.add(employee);
                }
            }
        }

        return employeeResults;
    }

    /**
     * Get en route employees information
     */
    @Override
    public SearchResult<EmployeeLocationResult> getEnRouteEmployee() {
        SearchResult<EmployeeLocationResult> searchResult = new SearchResult<>();
        List<EmployeeLocationResult> locationResultList = new ArrayList<>();

        SearchResult<Transaction> activeTransactions = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId());
        HashMap<String, EmployeeLocationResult> employeeMap = employeeRepository.getEmployeesAsMap(token.getCompanyId(), EmployeeLocationResult.class);
        Iterable<TerminalLocation> terminalLocations = getTerminalLocationsForTransactions(activeTransactions);

        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(token.getCompanyId());

        Map<String, TerminalLocation> terminalLocationMapByEmployee = new HashMap<>();

        //Create location map by employee id
        if (terminalLocations != null) {
            for (TerminalLocation terminalLocation : terminalLocations) {
                TerminalLocation location = terminalLocationMapByEmployee.get(terminalLocation.getEmployeeId());
                if (location == null) {
                    terminalLocationMapByEmployee.put(terminalLocation.getEmployeeId(), terminalLocation);
                } else if (terminalLocation.getCreated() > location.getCreated()) {
                    terminalLocationMapByEmployee.put(terminalLocation.getEmployeeId(), terminalLocation);
                }
            }
        }

        //Get assigned employee of active transaction and TerminalLocation of respective driver
        if (activeTransactions.getValues() != null) {
            for (Transaction transaction : activeTransactions.getValues()) {
                EmployeeLocationResult employee = null;
                if (employeeMap != null && !employeeMap.isEmpty()) {
                    employee = employeeMap.get(transaction.getAssignedEmployeeId());
                }

                if (employee != null) {
                    boolean isExist = checkEmployeeExistInResult(locationResultList, employee, transaction);
                    if (!isExist) {
                        TerminalLocation location = terminalLocationMapByEmployee.get(employee.getId());

                        if (location != null) {
                            employee.setLoc(location.getLoc());
                        }

                        Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());
                        if (terminal != null) {
                            employee.setTerminalName(terminal.getName());
                        }

                        locationResultList.add(employee);
                    }
                }
            }
        }

        searchResult.setTotal((long) locationResultList.size());
        searchResult.setValues(locationResultList);
        searchResult.setSkip(0);
        searchResult.setLimit(locationResultList.size());
        return searchResult;
    }

    private boolean checkEmployeeExistInResult(List<EmployeeLocationResult> locationResultList, EmployeeLocationResult employee, Transaction transaction) {
        boolean isExist = false;

        for (EmployeeLocationResult locationResult : locationResultList) {
            if (locationResult.getId().equalsIgnoreCase(employee.getId())) {
                isExist = true;
            }
        }

        return isExist;
    }

    /**
     * Get mileage of driver
     *
     * @param employeeId
     */
    @Override
    public EmployeeDailyMileageResult getDailyMileageByDriver(String employeeId) {
        EmployeeDailyMileageResult mileageResult = new EmployeeDailyMileageResult();

        Map<Long, String> distanceByDateMap = new HashMap<>();
        Employee employee = employeeRepository.get(token.getCompanyId(), employeeId);

        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

//        Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactionsForAssignedEmployee(token.getCompanyId(), token.getShopId(), employeeId);

        //TODO: NEED TO IMPROVE SPEED OF THIS
        //Get all location by employee (Delivery driver) in ascending order
        Iterable<TerminalLocation> terminalLocationList = terminalLocationRepository.listByShopSortAndEmployee(token.getCompanyId(), token.getShopId(), "{created:1}", 0, Integer.MAX_VALUE, employeeId);

        Map<LocalDate, List<TerminalLocation>> dateLocationMap = new HashMap<>();

        TerminalLocation currentTerminalLocation = null;
        //Create map of terminal location by date
        if (terminalLocationList != null) {
            LocalDate locationCreateDate;
            for (TerminalLocation terminalLocation : terminalLocationList) {
                if (terminalLocation.getLoc() != null) {
                    locationCreateDate = new LocalDate(terminalLocation.getCreated());
                    List<TerminalLocation> locationList = dateLocationMap.get(locationCreateDate);
                    if (locationList == null) {
                        locationList = new ArrayList<>();
                    }
                    locationList.add(terminalLocation);
                    dateLocationMap.put(locationCreateDate, locationList);
                }
                if (currentTerminalLocation == null) {
                    currentTerminalLocation = (new Kryo()).copy(terminalLocation);
                } else if (terminalLocation.getCreated() > currentTerminalLocation.getCreated()) {
                    currentTerminalLocation = (new Kryo()).copy(terminalLocation);
                }
            }
        }

        NumberFormat formatter = new DecimalFormat("#.##");
        Double distance;
        //Calculate distance by date
        for (Map.Entry<LocalDate, List<TerminalLocation>> entry : dateLocationMap.entrySet()) {
            distance = 0D;
            List<TerminalLocation> locations = entry.getValue();
            for (int i = 0; i < locations.size(); i++) {
                if (i != locations.size() - 1) {
                    distance += deliveryUtilService.distanceRaw(locations.get(i).getLoc().get(1), locations.get(i).getLoc().get(0),
                            locations.get(i + 1).getLoc().get(1), locations.get(i + 1).getLoc().get(0), 'M');
                }
            }

            distanceByDateMap.put(entry.getKey().toDateTimeAtStartOfDay().toInstant().getMillis(), formatter.format(distance));
        }

        ArrayList<TerminalLocation> locations = new ArrayList<>();
        locations.add(currentTerminalLocation);
        mileageResult.setTerminalLocations(locations);
        mileageResult.setDriverMileage(distanceByDateMap);
        return mileageResult;
    }

    /**
     * Get mileage of employee by transaction
     *
     * @param employeeId : employee id
     * @param start
     * @param limit
     */
    @Override
    public SearchResult<TransactionMileageResult> getMileageByDriverAndTransaction(String employeeId, int start, int limit) {
        SearchResult<TransactionMileageResult> mileageResult = new SearchResult<>();
        if (limit == 0) {
            limit = 100;
        }

        mileageResult.setSkip(start);
        mileageResult.setLimit(limit);

        List<TransactionMileageResult> mileageResults = new ArrayList<>();

        Employee employee = employeeRepository.get(token.getCompanyId(), employeeId);

        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        //TODO: NEED TO IMPROVE SPEED OF THIS
        //Get all transaction by employee (Delivery driver)
        HashMap<String, Transaction> transactions = transactionRepository.getTransactionsByAssignEmployeeId(token.getCompanyId(), token.getShopId(), employeeId, "{processedTime:-1}", start, limit);
        //Get all location by employee (Delivery driver) in ascending order
        Iterable<TerminalLocation> employeeLocationList = terminalLocationRepository.listByShopSortAndEmployee(token.getCompanyId(), token.getShopId(), "{created:1}", 0, Integer.MAX_VALUE, employeeId);

        ArrayList<TerminalLocation> terminalLocations = Lists.newArrayList(employeeLocationList);
        Map<String, List<TerminalLocation>> transactionLocationMap = new HashMap<>();

        //Terminal location by transaction
        for (Map.Entry<String, Transaction> entry : transactions.entrySet()) {
            //Iterate terminal location, compare terminal id of transaction and terminal location for getting TerminalLocation of Transaction
            if (terminalLocations.size() > 0) {
                for (TerminalLocation empLocation : employeeLocationList) {
                    List<TerminalLocation> locationList = transactionLocationMap.get(entry.getKey());
                    if (locationList == null) {
                        locationList = new ArrayList<>();
                    }
                    //If TerminalLocation's and transaction's terminal id equal then add TerminalLocation for that transaction
                    if (entry.getValue().getTerminalId() != null && entry.getValue().getTerminalId().equalsIgnoreCase(empLocation.getTerminalId()) && entry.getValue().getStartRouteDate() != null && entry.getValue().getEndRouteDate() != null &&
                            (entry.getValue().getStartRouteDate() <= empLocation.getCreated() & entry.getValue().getEndRouteDate() >= empLocation.getCreated())) {
                        locationList.add(empLocation);
                    }
                    transactionLocationMap.put(entry.getKey(), locationList);
                }
            } else {
                List<TerminalLocation> locations = new ArrayList<>();
                transactionLocationMap.put(entry.getKey(), locations);
            }
        }

        NumberFormat formatter = new DecimalFormat("#.##");
        //Calculate mileage for particular transaction
        Double distance = 0d;
        TransactionMileageResult result;
        for (Map.Entry<String, List<TerminalLocation>> entry : transactionLocationMap.entrySet()) {
            Transaction transaction = transactions.get(entry.getKey());
            result = new TransactionMileageResult();
            if (transaction.isMileageCalculated()) {
                distance = transaction.getMileage().doubleValue();
            } else if (entry.getValue().size() > 0) {
                List<TerminalLocation> locations = entry.getValue();
                distance = calculateDistance(locations);
            } else {
                distance = -1.00;
            }

            BigDecimal total = BigDecimal.ZERO;
            if (transaction.getCart() != null) {
                total = transaction.getCart().getTotal();
            }

            result.setId(transaction.getId());
            result.setTransNo(transaction.getTransNo());
            result.setCreated(transaction.getCreated());
            result.setTotal(total);
            if (distance == -1) {
                result.setDistance("Mileage Not Available");
            } else {
                result.setDistance(formatter.format(distance));
            }
            result.setCheckinTime(transaction.getCheckinTime());
            result.setProcessedTime(transaction.getProcessedTime());

            mileageResults.add(result);
        }

        mileageResult.setValues(mileageResults);
        mileageResult.setTotal((long) mileageResults.size());
        return mileageResult;
    }

    /**
     * Get all employee with role of delivery driver and status of their route
     *
     * @param term  : search term
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<DeliveryDriverEmployeeResult> getDeliveryDriverEmployee(String term, int start, int limit) {
        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        SearchResult<DeliveryDriverEmployeeResult> result;
        String projection = employeeService.getPermissionProjection();
        if (StringUtils.isBlank(term)) {
            result = employeeRepository.findItemsByShop(token.getCompanyId(), token.getShopId(), start, limit, projection, DeliveryDriverEmployeeResult.class);
        } else {
            result = employeeRepository.findItemsByShop(token.getCompanyId(), token.getShopId(), term, start, limit, projection, DeliveryDriverEmployeeResult.class);
        }

        processResults(result);
        return result;
    }

    /**
     * This method gets delivery zone by id
     *
     * @param deliveryZoneId : delivery zone's id that need to be fetch
     */
    @Override
    public DeliveryZone getDeliveryZoneById(String deliveryZoneId) {
        DeliveryZone deliveryZone = deliveryAreaRepository.get(token.getCompanyId(), deliveryZoneId);

        if (deliveryZone == null) {
            throw new BlazeInvalidArgException(DELIVERY_ZONE, "Delivery zone not found");
        }

        return deliveryZone;
    }

    /**
     * Get transaction completed by employee id
     *
     * @param employeeId : employee id
     * @param start
     * @param limit
     */
    @Override
    public SearchResult<Transaction> getTransactionByEmployeeId(String employeeId, int start, int limit) {
        SearchResult<Transaction> result = transactionRepository.getTransactionsByAssignEmployeeIdAndStatus(token.getCompanyId(), token.getShopId(), employeeId, "{created:1}", start, limit, Transaction.TransactionStatus.Completed, Transaction.class);

        if (result != null && result.getValues() != null) {
            processTransactionResultByEmployee(result.getValues());
        }

        return result;
    }

    private void processTransactionResultByEmployee(List<Transaction> results) {

        LinkedHashSet<ObjectId> objectIds = new LinkedHashSet<>();
        for (Transaction transaction : results) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
        }
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(objectIds));

        for (Transaction transaction : results) {
            Member member = memberHashMap.get(transaction.getMemberId());
            transaction.setMember(member);
        }
    }

    /**
     * Get inventory by id
     *
     * @param employeeId : employee id
     * @param start
     * @param limit
     */
    @Override
    public SearchResult<ProductPackagesResult> getInventoryByEmployee(String employeeId, int start, int limit, String searchTerm) {
        SearchResult<ProductPackagesResult> result = new SearchResult<>();

        if (limit <= 0) {
            limit = 200;
        }

        List<ProductPackagesResult> productPackagesResults = new ArrayList<>();
        Employee employee = employeeRepository.getById(employeeId);

        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Terminal terminal = terminalRepository.getById(employee.getAssignedTerminalId());

        if (terminal == null) {
            return result;
        }

        SearchResult<Product> products = productRepository.listAllByShopAndTerm(token.getCompanyId(), token.getShopId(), searchTerm);
        Inventory inventory = inventoryRepository.get(token.getCompanyId(), terminal.getAssignedInventoryId());
        List<ObjectId> brandIds = Lists.newArrayList();

        if (products == null) {
            return result;
        }

        List<String> productIds = new ArrayList<>();
        for (Product product : products.getValues()) {
            productIds.add(product.getId());
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), brandIds);

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
        Iterable<Prepackage> prepackages = prepackageRepository.listAllByShopActive(token.getCompanyId(), token.getShopId());
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForInventoryWithQuantity(token.getCompanyId(), token.getShopId(), terminal.getAssignedInventoryId());

        HashMap<String, List<ProductPrepackageQuantity>> prepackagesToQuantities = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
            List<ProductPrepackageQuantity> items = prepackagesToQuantities.get(prepackageQuantity.getPrepackageId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(prepackageQuantity);
            prepackagesToQuantities.put(prepackageQuantity.getPrepackageId(), items);
        }


        for (Product product : products.getValues()) {
            ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
            ProductPackagesResult productResult = new ProductPackagesResult();
            productResult.setCategory(productCategory);
            productResult.setId(product.getId());
            productResult.setProductName(product.getName());
            productResult.setInventoryName(inventory.getName());
            productResult.setProductSku(product.getSku());
            Brand brand = brandHashMap.get(product.getBrandId());
            if (brand != null) {
                productResult.setBrandName(brand.getName());
            }


            List<ProductQuantity> quantities = new ArrayList<>();
            double qty = 0d;
            for (ProductQuantity quantity : product.getQuantities()) {
                if (quantity.getInventoryId().equalsIgnoreCase(terminal.getAssignedInventoryId())) {
                    quantities.add(quantity);
                    qty = quantity.getQuantity().doubleValue();
                    break;
                }
            }

            productResult.setQuantities(quantities);
            if (qty <= 0) {
                // ignore
            } else {
                // add
                productPackagesResults.add(productResult);
            }

            if ((productCategory != null) && (productCategory.getUnitType() == ProductCategory.UnitType.grams)) {
                for (Prepackage prepackage : prepackages) {
                    if (product.getId().equalsIgnoreCase(prepackage.getProductId())) {

                        double total = 0;
                        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackagesToQuantities.get(prepackage.getId());
                        if (productPrepackageQuantities != null) {
                            for (ProductPrepackageQuantity quantity : productPrepackageQuantities) {
                                if (quantity.getInventoryId().equalsIgnoreCase(terminal.getAssignedInventoryId())) {
                                    total += quantity.getQuantity();
                                }
                            }
                        }
                        if (total <= 0) {
                            continue;
                        }

                        String productWithPrepackage = String.format("%s (%s)", product.getName(), prepackage.getName());

                        ProductPackagesResult productPrepackageResult = new ProductPackagesResult();
                        productPrepackageResult.setCategory(productCategory);
                        productPrepackageResult.setId(product.getId());
                        productPrepackageResult.setProductName(productWithPrepackage);
                        productPrepackageResult.setPrepackage(prepackage);
                        productPrepackageResult.setPrePackageQuantity(NumberUtils.round(total, 2));
                        productPrepackageResult.setInventoryName(inventory.getName());
                        productPrepackageResult.setPrePackage(true);
                        productPackagesResults.add(productPrepackageResult);
                    }
                }
            }
        }


        Collections.sort(productPackagesResults, new Comparator<ProductPackagesResult>()  {
            @Override
            public int compare(ProductPackagesResult productResult1, ProductPackagesResult productResult2) {
                if(StringUtils.isBlank(productResult1.getProductName()) && StringUtils.isBlank(productResult2.getProductName())){
                    return 0;
                }
                return productResult1.getProductName().compareToIgnoreCase(productResult2.getProductName());
            }
        });

        int size = productPackagesResults.size();
        List<ProductPackagesResult> productPackagesResultsList;
        if (start > size) {
            productPackagesResultsList = new ArrayList<>();
        } else if ((limit + start) > size) {
            productPackagesResultsList = productPackagesResults.subList(start, size);
        } else {
            productPackagesResultsList = productPackagesResults.subList(start, (start + limit));
        }


        result.setTotal((long) productPackagesResults.size());
        result.setSkip(start);
        result.setValues(productPackagesResultsList);
        result.setLimit(limit);

        return result;
    }

    private void processResults(SearchResult<DeliveryDriverEmployeeResult> result) {
        List<String> employeeIds = new ArrayList<>();

        for (DeliveryDriverEmployeeResult employeeResult : result.getValues()) {
            employeeIds.add(employeeResult.getId());
        }

        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(token.getCompanyId());
        SearchResult<Transaction> activeTransactions = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId());
        HashMap<String, TerminalLocation> employeeRecentLocationMap = terminalLocationRepository.getRecentLocationByEmployee(token.getCompanyId(), token.getShopId(), employeeIds);
        HashMap<String, Long> activeTransactionMap = new HashMap<>();

        for (Transaction transaction : activeTransactions.getValues()) {
            Long count = activeTransactionMap.get(transaction.getAssignedEmployeeId());
            activeTransactionMap.put(transaction.getAssignedEmployeeId(), (count == null) ? 1 : (++count));
        }

        boolean routing;
        String terminalName;
        String inventoryName;
        for (DeliveryDriverEmployeeResult employee : result.getValues()) {
            routing = false;
            inventoryName = "";
            terminalName = "";
            TerminalLocation recentLocation = employeeRecentLocationMap.get(employee.getId());
            for (Transaction transaction : activeTransactions.getValues()) {
                if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && transaction.getAssignedEmployeeId().equalsIgnoreCase(employee.getId())) {
                    routing = true;
                    break;
                }
            }

            Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());
            Inventory inventory = null;
            if (terminal != null) {
                terminalName = terminal.getName();
                inventory = inventoryMap.get(terminal.getAssignedInventoryId());
            }

            if (inventory != null) {
                inventoryName = inventory.getName();
            }

            employee.setInventoryName(inventoryName);
            employee.setTerminalName(terminalName);
            employee.setRouting(routing);
            employee.setRecentLocation(recentLocation);
            Long activeCount = activeTransactionMap.get(employee.getId());
            employee.setActiveTransaction((activeCount == null ? 0 : activeCount));
            employee.setAssignedInventoryId((inventory != null) ? inventory.getId() : "");
        }
    }

    /**
     * Get all employees with assigned to member check and distance from member's address
     *
     * @param memberId
     * @param start                : start
     * @param limit                : limit
     * @param publicKey
     * @param currentTransactionId
     */
    @Override
    public EmployeeTransactionDistanceResult getEmployeesByDistance(String memberId, int start, int limit, String publicKey, String currentTransactionId) {
        EmployeeTransactionDistanceResult result = new EmployeeTransactionDistanceResult();
        SearchResult<EmployeeDistanceResult> items = null;

        Member member = memberRepository.get(token.getCompanyId(), memberId);
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member not found");
        }

        items = employeeRepository.findDriverEmployeeByShop(token.getCompanyId(), token.getShopId(), start, limit, "{password:0, pin:0}", EmployeeDistanceResult.class);

        // filter

        if (items != null && items.getValues() != null) {
            processEmployeeByDistanceResult(items, member, result, currentTransactionId);
        }

        if (StringUtils.isNotBlank(publicKey)) {
            result.setConsumerCart(consumerCartRepository.getConsumerCart(publicKey));
        }

        return result;
    }

    private void processEmployeeByDistanceResult(SearchResult<EmployeeDistanceResult> items, Member member, EmployeeTransactionDistanceResult result, String currentTransactionId) {

        List<EmployeeDistanceResult> employeesDistanceList = items.getValues();
        Iterable<Transaction> transactionList = transactionRepository.getAllActiveTransactions(token.getCompanyId(), token.getShopId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, Product> productMap = productRepository.listAllAsMap(token.getCompanyId(), token.getShopId());

        Transaction currentTransaction = transactionRepository.get(token.getCompanyId(), currentTransactionId);
        HashMap<String, Long> employeeTransactionCount = new HashMap<>();

        List<String> employeeIds = new ArrayList<>();
        for (EmployeeDistanceResult employeeDistanceResult : employeesDistanceList) {
            employeeIds.add(employeeDistanceResult.getId());
        }
        HashMap<String, TerminalLocation> employeeRecentLocationMap = terminalLocationRepository.getRecentLocationByEmployee(token.getCompanyId(), token.getShopId(), employeeIds);


        for (Transaction transaction : transactionList) {
            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId())) {
                Long count = employeeTransactionCount.get(transaction.getAssignedEmployeeId());
                employeeTransactionCount.put(transaction.getAssignedEmployeeId(), (count == null) ? 1 : count + 1);
            }
        }

        Transaction transactionResult = null;
        boolean assignedToMember;
        List<String> employeeList = new ArrayList<>();
        List<String> originList = new ArrayList<>();
        String inventoryRatio;
        for (EmployeeDistanceResult employee : employeesDistanceList) {
            if (!employee.getShops().contains(token.getShopId())) {
                continue;
            }
            if (employee.isDisabled() || employee.isDeleted()) {
                continue;
            }



            assignedToMember = false;

            //Calculate ratio of inventory
            Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());
            String inventoryId = null;
            String terminalName = null;
            if (terminal != null) {
                inventoryId = terminal.getAssignedInventoryId();
                terminalName = terminal.getName();
            }
            employee.setAssignedInventoryId(inventoryId);
            employee.setTerminalName(terminalName);


            InventoryRatio ratioInfo = calculateInventoryRatio(currentTransaction, inventoryId, productMap);;
            inventoryRatio = ratioInfo.ratioDetail;

            if (transactionList != null) {
                for (Transaction transaction : transactionList) {
                    if (member.getId().equalsIgnoreCase(transaction.getMemberId())) {
                        transactionResult = transaction;
                    }

                    if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && transaction.getAssignedEmployeeId().equalsIgnoreCase(employee.getId())) {
                        assignedToMember = true;
                        break;
                    }
                }

                employee.setAssignedToMember(assignedToMember);
            }
            employee.setInventoryRatio(inventoryRatio);
            employee.setPercentageValue(ratioInfo.percentage);

            int count = 0;
            Long aCount = employeeTransactionCount.get(employee.getId());
            if (aCount != null) {
                count = (int)((long)aCount);
            }
            employee.setActiveTransaction(count);


            if (!employee.isDriver()) {
                LOG.info(String.format("Employee %s has driver as false..", employee.getId()));
                continue;
                //don't calculate distance if employee is not driver
            }
            long lastLoginTime = employee.getLastLoginTime() != null ? employee.getLastLoginTime() : 0;
            if (DateTime.now().minusHours(1).isAfter(lastLoginTime)) {
                // If user did not log in to the app in the past hr, then do not calculate
                continue;
            }

            //Get all terminal location of employee
//            TerminalLocation recentLocation = terminalLocationRepository.getRecentLocation(token.getCompanyId(), token.getShopId(), employee.getId());
            if (employee.getRecentLocation() == null) {
                employee.setRecentLocation(employeeRecentLocationMap.get(employee.getId()));
            }
            TerminalLocation recentLocation = employee.getRecentLocation();

            String empLocation = "";
            if (recentLocation != null) {
                List<Double> loc = recentLocation.getLoc();
                empLocation = loc.get(1) + "," + loc.get(0);
                LOG.info(String.format("Employee %s location found..", employee.getId()));

            }
            if (!empLocation.isEmpty()) {
                employeeList.add(employee.getId());
                originList.add(empLocation);
            }
        }

        //Calculate distance of employee to member
        Address address = member.getAddress();
        if (address != null) {
            GoogleDistanceResult distanceWithGoogle = null;
            if (member.getAddress() != null) {

                String origins = StringUtils.join(originList, "|");
                String destination = address.getAddress().trim() + "+" + address.getCity().trim() + "+" + address.getState().trim()
                        + "+" + address.getCountry().trim() + "+" + address.getZipCode().trim();
                destination = destination.replaceAll("[^a-zA-Z0-9]", "+").replaceAll("\\+{2,}", "+");

                if (!destination.isEmpty()) {
                    String cacheKey = createKey(employeeIds,destination);
                    distanceWithGoogle = getDistanceWithGoogle(cacheKey,origins, destination);
                }
            }

            if (distanceWithGoogle != null && distanceWithGoogle.getStatus().equalsIgnoreCase("OK")) {
                List<DistanceElementWrapper> rows = distanceWithGoogle.getRows();
                if (rows != null) {
                    for (int i = 0; i < rows.size(); i++) {
                        if (rows.get(i) != null) {
                            if (rows.get(i).getElements() != null) {
                                assignDistanceToEmployee(rows.get(i).getElements(), employeeList.get(i), employeesDistanceList);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(employeesDistanceList);
        items.setValues(employeesDistanceList);
        items.setTotal((long) employeesDistanceList.size());

        result.setEmployeeDistanceResult(items);
        result.setTransaction(transactionResult);
    }

    private InventoryRatio calculateInventoryRatio(Transaction transaction, String inventoryId, HashMap<String, Product> productMap) {
        int totalQuantity = 0;
        int empQuantity = 0;
        String inventoryRatio;

        HashMap<String,BigDecimal> holdAmt = new HashMap<>();
        HashMap<String,BigDecimal> orderedAmt = new HashMap<>();

        if (transaction != null && transaction.getCart().getItems() != null) {
            for (OrderItem orderItem : transaction.getCart().getItems()) {

                BigDecimal orderQty = orderedAmt.getOrDefault(orderItem.getProductId(),new BigDecimal(0));
                orderQty = orderQty.add(orderItem.getQuantity());
                orderedAmt.put(orderItem.getProductId(),orderQty);


                if (orderItem.getQuantityLogs() != null) {

                    BigDecimal inventoryQty = holdAmt.getOrDefault(orderItem.getProductId(),new BigDecimal(0));
                    for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                        if (quantityLog.getInventoryId() != null && quantityLog.getInventoryId().equalsIgnoreCase(inventoryId) && quantityLog.getQuantity() != null) {
                            inventoryQty = inventoryQty.add(quantityLog.getQuantity());
                        }
                    }
                    holdAmt.put(orderItem.getProductId(),inventoryQty);
                }

            }
        }
        for (String productId : orderedAmt.keySet()) {
            BigDecimal available = holdAmt.getOrDefault(productId,new BigDecimal(0));
            Product product = productMap.get(productId);
            if (product != null) {
                if (product.getQuantities() != null) {
                    for (ProductQuantity pq : product.getQuantities()) {
                        if (pq.getInventoryId() != null && pq.getInventoryId().equalsIgnoreCase(inventoryId)) {
                            available = available.add(pq.getQuantity());
                            break;
                        }
                    }
                }
            }

            BigDecimal orderAmt = orderedAmt.get(productId);
            totalQuantity += orderAmt.intValue();
            if (available.compareTo(orderAmt) >= 0) {
                // has item
                empQuantity += orderAmt.intValue();
            } else {
                empQuantity += available.intValue();
            }


        }

        inventoryRatio = empQuantity + "/" + totalQuantity;

        InventoryRatio ratio = new InventoryRatio();
        ratio.ratioDetail = inventoryRatio;
        if (totalQuantity > 0) {
            ratio.percentage = (int)((empQuantity / (double)totalQuantity) * 100);
        } else {
            ratio.percentage = 0;
        }
        return ratio;
    }

    public static String fmt(double d) {
        if (d == (long) d)
            return Long.toString((long)d);
        else
            return Double.toString(d);
    }

    private String createKey(List<String> employeeIds, String destination) {
        employeeIds.sort(Comparator.naturalOrder());

        StringBuilder sb = new StringBuilder();
        for (String empId : employeeIds) {
            sb.append(empId);
            sb.append("_");
        }
        sb.append(destination);
        return sb.toString().trim();
    }

    private void assignDistanceToEmployee(List<DistanceElement> elements, String employeeId, List<EmployeeDistanceResult> results) {

        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).getStatus().equalsIgnoreCase("OK")) {

                for (EmployeeDistanceResult result : results) {
                    if (elements.get(i).getStatus().equalsIgnoreCase("OK") && result.getId().equalsIgnoreCase(employeeId)) {
                        result.setDistance(elements.get(i).getDistance().getText());
                        result.setTime(elements.get(i).getDuration().getText());
                        result.setDistanceValue(elements.get(i).getDistance().getValue());
                        LOG.info(String.format("Employee %s has calculated distance..", result.getId()));
                        break;
                    }
                }
            }
        }
    }

    /**
     * Get inventory of product by employee
     *
     * @param productId : product id
     * @param start     : start
     * @param limit     : limit
     */
    @Override
    public SearchResult<InventoryByEmployeeResult> getProductInventoryByEmployee(String productId, int start, int limit) {
        SearchResult<InventoryByEmployeeResult> searchResult = new SearchResult<>();
        List<InventoryByEmployeeResult> employeeInventories = new ArrayList<>();

        Product product = productRepository.get(token.getCompanyId(), productId);
        if (product == null) {
            throw new BlazeInvalidArgException("Product", "Product not found");
        }

        SearchResult<Employee> employeeResult = employeeRepository.findItems(token.getCompanyId(), start, limit);
        HashMap<String, Terminal> terminalMap = terminalRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        SearchResult<Prepackage> prepackages = prepackageRepository.getPrepackages(token.getCompanyId(), token.getShopId(), productId);
        Iterable<ProductPrepackageQuantity> prepackageQuantities = quantityRepository.listAllByShop(token.getCompanyId(), token.getShopId());
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId());

        HashMap<String, Integer> prepackageToQuantity = new HashMap<>();
        for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
            Integer amt = prepackageToQuantity.get(prepackageQuantity.getPrepackageId());
            if (amt == null) {
                amt = 0;
            }
            amt += prepackageQuantity.getQuantity();
            //Prepare ProductPrepackageQuantity for prepackage id and inventory id So we can have quantity of corresponding prepackage
            prepackageToQuantity.put(prepackageQuantity.getPrepackageId() + "#$#" + prepackageQuantity.getInventoryId(), amt);
        }

        InventoryByEmployeeResult result = null;
        //Get all employee
        if (employeeResult != null && employeeResult.getValues() != null) {
            for (Employee employee : employeeResult.getValues()) {
                //Get assigned terminal of employee
                Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());

                //If employee is not assigned to any employee then don't add it to result
                if (terminal != null) {
                    result = new InventoryByEmployeeResult();
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());
                    String unit = "";
                    if (category != null && category.getUnitType() != null) {
                        unit = category.getUnitType().toString();
                    }

                    double quantity = calculateProductQuantities(product, terminal.getAssignedInventoryId());

                    StringBuffer prepackage = getProductPrepackage(prepackages, prepackageToQuantity, terminal.getAssignedInventoryId());

                    result.setEmployeeId(employee.getId());
                    result.setEmployeeName(employee.getFirstName() + " " + employee.getLastName());
                    result.setQuantity(quantity + " " + unit);
                    result.setPrepackage(prepackage.toString());
                    employeeInventories.add(result);
                }
            }
        }

        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setValues(employeeInventories);
        searchResult.setTotal((long) employeeInventories.size());
        return searchResult;
    }

    /**
     * This method prepare prepackage for product
     *
     * @param prepackagesResult    : prepackage
     * @param prepackageToQuantity : prepackage product quantity
     * @param inventoryId          : inventory id of terminal
     */
    private StringBuffer getProductPrepackage(SearchResult<Prepackage> prepackagesResult, HashMap<String, Integer> prepackageToQuantity, String inventoryId) {
        StringBuffer currentPrepackages = new StringBuffer();

        if (prepackagesResult == null && prepackagesResult.getValues() == null) {
            return currentPrepackages;
        }

        for (Prepackage prepackage : prepackagesResult.getValues()) {

            Integer amt = prepackageToQuantity.get(prepackage.getId() + "#$#" + inventoryId);
            if (amt != null && amt > 0) {
                if (currentPrepackages.length() > 0) {
                    currentPrepackages.append(", ");
                }
                currentPrepackages.append(String.format("%d of %s", amt, prepackage.getName()));
            }
        }

        return currentPrepackages;
    }

    /**
     * This method calculates quantity of product for provided inventory id
     *
     * @param product     : product
     * @param inventoryId : assigned inventory id to terminal
     */
    private double calculateProductQuantities(Product product, String inventoryId) {
        double quantity = 0;
        for (ProductQuantity productQuantity : product.getQuantities()) {
            //If product quantity's inventory match to terminal's inventory then add it quantity
            if (productQuantity.getInventoryId().equalsIgnoreCase(inventoryId)) {
                quantity += productQuantity.getQuantity().doubleValue();
            }
        }

        return quantity;
    }

    /**
     * This method create list for TerminalLocation for a transaction
     *
     * @param transactionHashMap
     * @param terminalLocationList
     */
    @Override
    public Map<String, List<TerminalLocation>> getTerminalLocationByTransaction(HashMap<String, Transaction> transactionHashMap, Iterable<TerminalLocation> terminalLocationList) {
        Map<String, List<TerminalLocation>> transactionLocationMap = new HashMap<>();
        for (Map.Entry<String, Transaction> entry : transactionHashMap.entrySet()) {
            if (!entry.getValue().isMileageCalculated()) {
                Transaction transaction;
                for (TerminalLocation location : terminalLocationList) {
                    List<TerminalLocation> locationList = transactionLocationMap.get(entry.getKey());
                    transaction = entry.getValue();
                    if (transaction.getTerminalId() != null && transaction.getStartRouteDate() != null && transaction.getEndRouteDate() != null
                            && transaction.getTerminalId().equalsIgnoreCase(location.getTerminalId()) &&
                            (transaction.getStartRouteDate() <= location.getCreated() & transaction.getEndRouteDate() >= location.getCreated())) {
                        if (locationList == null) {
                            locationList = new ArrayList<>();
                        }
                        locationList.add(location);
                        transactionLocationMap.put(entry.getKey(), locationList);
                    }
                }
            }
        }

        return transactionLocationMap;
    }

    /**
     * This method calculates distance covered in Terminal Location
     *
     * @param locations : List<TerminalLocation>
     */
    @Override
    public Double calculateDistance(List<TerminalLocation> locations) {
        return deliveryUtilService.calculateDistance(locations);
    }

    /**
     * Calculate mileage by transaction
     *
     * @param transaction
     */
    @Override
    public double calculateMileageByTransaction(Transaction transaction) {
        return deliveryUtilService.calculateMileageByTransaction(token.getCompanyId(),token.getShopId(),transaction);
    }

    /**
     * Get list of all employees having sufficient products and sort according to given sort option
     *
     * @param transactionId Transaction Id
     * @param sort          : sort option (NAME,RANDOM,DISTANCE)
     * @param limit         :
     * @return
     */
    @Override
    public List<EmployeeDistanceResult> getClockedInEmployees(String transactionId, Employee.SortOption sort, int limit) {
        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (transaction == null) {
            throw new BlazeInvalidArgException(DRIVER_SUGGESTION, "Transaction does not found");
        }
        if (limit == 0) {
            limit = 10;
        }
        if (sort == null) {
            sort = Employee.SortOption.RANDOM;
        }

        // If it's not delivery, just return an empty list
        if (transaction.getQueueType() != Transaction.QueueType.Delivery) {
            return new ArrayList<EmployeeDistanceResult>();
        }

        Member member = memberRepository.get(token.getCompanyId(), transaction.getMemberId());
        if (member == null) {
            throw new BlazeInvalidArgException(DRIVER_SUGGESTION, "Member does not found");
        }
        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAsMap(token.getCompanyId(), token.getShopId());

        List<ObjectId> productIds = new ArrayList<>();
        HashMap<String, Double> productQuantityMap = new HashMap<>();

        if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
            return new ArrayList<>(); //Return blank list if no items found
        }

        for (OrderItem orderItem : transaction.getCart().getItems()) {
            if (StringUtils.isNotBlank(orderItem.getProductId())) {
                productIds.add(new ObjectId(orderItem.getProductId()));
                productQuantityMap.put(orderItem.getProductId(), orderItem.getQuantity().doubleValue());
            }
        }
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), productIds);


        SearchResult<TimeCardCompositeResult> timeCardCompositeResult = timeCardService.getActiveTimeCards();

        List<ObjectId> employeeList = new ArrayList<>();

        List<String> originList = new ArrayList<>();
        List<EmployeeDistanceResult> employeeDistanceResults = new ArrayList<>();
        for (TimeCardCompositeResult timeCard : timeCardCompositeResult.getValues()) {
            Employee employee = timeCard.getEmployee();
            if (employee == null) {
                continue;
            }

            Terminal assignedTerminal = terminalHashMap.get(employee.getAssignedTerminalId());
            if (assignedTerminal == null || assignedTerminal.isDeleted()) {
                continue;
            }

            String assignedInventoryId = assignedTerminal.getAssignedInventoryId();

            double availableQuantity = 0;
            double requestQuantity = 0;

            for (OrderItem orderItem : transaction.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product == null || product.getQuantities() == null) {
                    continue;
                }

                availableQuantity += calculateProductQuantities(product, assignedInventoryId);
                requestQuantity += productQuantityMap.get(product.getId());

            }

            TerminalLocation terminalLocation = employee.getRecentLocation();

            String empLocation = "";
            if (terminalLocation != null) {
                List<Double> loc = terminalLocation.getLoc();
                empLocation = loc.get(1) + "," + loc.get(0);
            }
            if (availableQuantity >= requestQuantity && StringUtils.isNotBlank(empLocation)) {
                employeeList.add(new ObjectId(employee.getId()));
                originList.add(empLocation);
            }
        }
        HashMap<String, EmployeeDistanceResult> employeeDistanceResultHashMap = employeeRepository.listEmployeeAsMap(token.getCompanyId(), employeeList, EmployeeDistanceResult.class);

        Address address = transaction.getDeliveryAddress();
        if (address == null) {
            address = member.getAddress();
        }

        getEmployeeDistanceByMember(address, employeeDistanceResultHashMap, employeeDistanceResults, employeeList, originList);

        if (sort == Employee.SortOption.NAME) {

            employeeDistanceResults.sort(new Comparator<Employee>() {
                @Override
                public int compare(Employee employee1, Employee employee2) {
                    return employee1.getFirstName().toLowerCase().compareTo(employee2.getFirstName().toLowerCase());
                }
            });
        } else if (sort == Employee.SortOption.DISTANCE) {
            Collections.sort(employeeDistanceResults);
        }
        //Get Top 10 or specified in limit from employee distance list
        employeeDistanceResults = employeeDistanceResults.subList(0, (employeeDistanceResults.size() < limit) ? employeeDistanceResults.size() : limit);

        return employeeDistanceResults;
    }

    /**
     * Private method for calculating distance between employee and given member
     *
     * @param destinationAddress - Destination Address
     * @param employeeDistanceResultHashMap
     * @param employeeDistanceResults
     * @param employeeList
     * @param originList
     */
    private void getEmployeeDistanceByMember(Address destinationAddress, HashMap<String, EmployeeDistanceResult> employeeDistanceResultHashMap, List<EmployeeDistanceResult> employeeDistanceResults, List<ObjectId> employeeList, List<String> originList) {
        if (destinationAddress != null) {
            Address address = destinationAddress;
            GoogleDistanceResult distanceWithGoogle = null;
            String origins = StringUtils.join(originList, "|");
            String destination = address.getAddress().trim() + "+" + address.getCity().trim() + "+" + address.getState().trim()
                    + "+" + address.getCountry().trim() + "+" + address.getZipCode().trim();
            destination = destination.replaceAll("[^a-zA-Z0-9]", "+").replaceAll("\\+{2,}", "+");

            if (!destination.isEmpty()) {
                List<String> employeeIds = new ArrayList<>();
                employeeList.forEach((e) -> employeeIds.add(e.toString()));
                String cacheKey = createKey(employeeIds,destination);
                distanceWithGoogle = getDistanceWithGoogle(cacheKey,origins, destination);
            }

            if (distanceWithGoogle != null && distanceWithGoogle.getStatus().equalsIgnoreCase("OK")) {
                List<DistanceElementWrapper> rows = distanceWithGoogle.getRows();
                if (rows != null) {
                    int i = 0;
                    for (DistanceElementWrapper elementWrapper : rows) {
                        if (elementWrapper != null && elementWrapper.getElements() != null) {
                            for (DistanceElement element : elementWrapper.getElements()) {
                                if (element.getStatus().equalsIgnoreCase("OK")) {
                                    EmployeeDistanceResult employeeResult = employeeDistanceResultHashMap.get(employeeList.get(i).toString());
                                    if (employeeResult == null) {
                                        continue;
                                    }
                                    employeeResult.setDistance(element.getDistance().getText());
                                    employeeResult.setTime(element.getDuration().getText());
                                    employeeResult.setDistanceValue(element.getDistance().getValue());
                                    employeeDistanceResults.add(employeeResult);
                                }
                            }
                        }
                        i++;
                    }
                }
            }
        }

    }

    /**
     * public  method for calculating distance between employee and members according to active transactions
     *
     * @param employeeId - employeeId
     * @param  location - location in latitude and longitude
     * return employee total distance and total time with respective members
     */
    @Override
    public List<MemberDistanceResult> getEmployeeDistanceByTranactions(String employeeId, String location) {

        Employee employee;
        List<ObjectId> memberIds = new ArrayList<>();
        employeeId = StringUtils.isBlank(employeeId) ? token.getActiveTopUser().getUserId() : employeeId;

        employee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee does not exsits.");
        }

        Iterable<Transaction> transactionList = transactionRepository.getAllActiveTransactionsForAssignedEmployee(token.getCompanyId(), token.getShopId(), employee.getId());

        for (Transaction transaction : transactionList) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), memberIds);

        SortedMap<String, MemberDistanceResult> resultMap = new TreeMap<>();

        for (Transaction transaction : transactionList) {
            MemberDistanceResult memberDistanceResult = new MemberDistanceResult();
            memberDistanceResult.setTransaction(transaction);
            memberDistanceResult.setMember(memberHashMap.get(transaction.getMemberId()));

            resultMap.put(transaction.getId(), memberDistanceResult);
        }
        String empLocation;
        TerminalLocation recentLocation = employee.getRecentLocation();
        if (StringUtils.isNotBlank(location)) {
            empLocation = location;
        } else {
            if (recentLocation != null) {
                List<Double> loc = recentLocation.getLoc();
                empLocation = loc.get(1) + "," + loc.get(0);
                LOG.info(String.format("Employee %s location found..", employee.getId()));
            } else {
                //return result without distance
                LOG.info(String.format("Employee %s location not found..returning result", employee.getId()));
                return Lists.newArrayList(resultMap.values());
            }
        }

        List<String> destinationList = new ArrayList<>();
        List<String> transactionIds = new ArrayList<>();
        String origin = empLocation;

        String addressStr = "";

        Address address;
        for (Transaction transaction : transactionList) {
            Member member = memberHashMap.get(transaction.getMemberId());
            if (transaction.getDeliveryAddress() != null && StringUtils.isNotBlank(transaction.getDeliveryAddress().getAddressString())) {
                address = transaction.getDeliveryAddress();
            } else if (member != null && member.getAddress() != null && StringUtils.isNotBlank(member.getAddress().getAddressString())) {
                address = member.getAddress();
            } else {
                continue;
            }

            if (address != null) {
                addressStr = address.getAddress().trim() + "+" + address.getCity().trim() + "+" + address.getState().trim()
                        + "+" + address.getCountry().trim() + "+" + address.getZipCode().trim();
                addressStr = addressStr.replaceAll("[^a-zA-Z0-9]", "+").replaceAll("\\+{2,}", "+");
            }

            transactionIds.add(transaction.getId());
            destinationList.add(addressStr);
        }

        if (origin != null) {
            GoogleDistanceResult distanceWithGoogle = null;
            if (employee.getAddress() != null) {

                String origins = origin;
                String destination = StringUtils.join(destinationList, "|");

                if (!origin.isEmpty()) {
                    String cacheKey = createMemberKey(memberIds, origin);
                    distanceWithGoogle = getDistanceWithGoogle(cacheKey, origins, destination);
                }
            }

            if (distanceWithGoogle != null && distanceWithGoogle.getStatus().equalsIgnoreCase("OK")) {

                List<DistanceElementWrapper> rows = distanceWithGoogle.getRows();

                if (rows != null) {
                    int i = 0;
                    for (DistanceElementWrapper elementWrapper : rows) {
                        if (elementWrapper != null && elementWrapper.getElements() != null) {

                            for (DistanceElement element : elementWrapper.getElements()) {
                                if (element.getStatus().equalsIgnoreCase("OK")) {

                                    long totalTime = TimeUnit.SECONDS.toMillis(element.getDuration().getValue());
                                    double mile = NumberUtils.round((element.getDistance().getValue() / 1609.344), 2);

                                    MemberDistanceResult memberDistanceResult = resultMap.get(transactionIds.get(i));
                                    if (memberDistanceResult == null) {
                                        continue;
                                    }
                                    memberDistanceResult.setTotalDistance(String.valueOf(mile).concat(" mi"));
                                    memberDistanceResult.setTotalTime(totalTime);
                                }
                                i++;
                            }
                        }
                    }
                }
            }
        }

        return Lists.newArrayList(resultMap.values());
    }

    private String createMemberKey(List<ObjectId> memberIds, String origins) {
        memberIds.sort(Comparator.naturalOrder());
        StringBuilder sb = new StringBuilder();
        for (ObjectId memberId : memberIds) {
            sb.append(memberId);
            sb.append("_");
        }
        sb.append(origins);
        return sb.toString().trim();
    }

    class InventoryRatio {
        String ratioDetail = "";
        int percentage = 0;
    }


    @Override
    public SearchResult<InventoryActionLog> getAllInventoryLog(String employeeId, int start, int limit, String searchTerm, long afterDate, long beforeDate) {

        String timeZone = token.getRequestTimeZone();
        if (beforeDate == 0) {
            beforeDate = DateTime.now().getMillis();
        }
        if (afterDate < 0) {
            afterDate = DateUtil.toDateTime(beforeDate, timeZone).withTimeAtStartOfDay().getMillis();
        }


        if (start < 0) start = 0;
        if (limit <= 0 || limit > 500) {
            limit = 200;
        }
        SearchResult<InventoryActionLog> inventoryActionLogSearchResult = new SearchResult<>();


        String empId = StringUtils.isNotBlank(employeeId) ? employeeId : token.getActiveTopUser().getUserId();
        Employee employee = employeeRepository.getById(empId);

        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "employee not found");
        }
        Terminal terminal = null;

        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.getTerminalById(token.getCompanyId(), employee.getAssignedTerminalId());
        }

        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal not found");
        }

        HashMap<String, InventoryActionLog> inventoryActionLogs = new HashMap<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> batchIds = new HashSet<>();


        List<Transaction.TransactionStatus> statuses = new ArrayList<>();
        statuses.add(Transaction.TransactionStatus.Queued);
        statuses.add(Transaction.TransactionStatus.Hold);
        statuses.add(Transaction.TransactionStatus.InProgress);
        statuses.add(Transaction.TransactionStatus.Completed);
        statuses.add(Transaction.TransactionStatus.RefundWithInventory);
        statuses.add(Transaction.TransactionStatus.RefundWithoutInventory);

        SearchResult<Transaction> transactions = transactionRepository.getAllEmployeeTransactionsWithStatus(token.getCompanyId(), token.getShopId(), empId, statuses, afterDate, beforeDate);

        for (Transaction transaction : transactions.getValues()) {
            for (OrderItem item : transaction.getCart().getItems()) {
                if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                    if (StringUtils.isNotBlank(quantityLog.getBatchId()) && ObjectId.isValid(quantityLog.getBatchId())) {
                        batchIds.add(new ObjectId(quantityLog.getBatchId()));
                    }
                }
            }
        }

        HashMap<String, Product> productHashMap = productRepository.getProductsWithIdsAndTerm(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds), searchTerm);

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }


        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));

        for (Transaction transaction : transactions.getValues()) {
            if (transaction == null || transaction.getCart() == null || CollectionUtils.isEmpty(transaction.getCart().getItems())) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                Brand brand = null;
                if (StringUtils.isNotBlank(product.getBrandId())) {
                    brand = brandHashMap.get(product.getBrandId());
                }
                for (QuantityLog quantityLog : item.getQuantityLogs()) {
                    if (StringUtils.isNotBlank(terminal.getAssignedInventoryId()) && StringUtils.isNotBlank(quantityLog.getInventoryId()) && quantityLog.getInventoryId().equals(terminal.getAssignedInventoryId())) {
                        BigDecimal quantity;
                        ProductBatch productBatch = batchHashMap.get(quantityLog.getBatchId());
                        String batchId = (productBatch == null) ? "N/A" : productBatch.getId();

                        String key = product.getId() + "_" + batchId;
                        inventoryActionLogs.putIfAbsent(key, new InventoryActionLog());
                        InventoryActionLog inventoryActionLog = inventoryActionLogs.get(key);
                        inventoryActionLog.setProductName(StringUtils.isNotBlank(product.getName()) ? product.getName() : "N/A");
                        inventoryActionLog.setCannabisType(product.getCannabisType());
                        inventoryActionLog.setWeightPerUnit(product.getWeightPerUnit());
                        inventoryActionLog.setPrice(product.getUnitPrice());
                        inventoryActionLog.setBrandName((brand == null) ? "N/A" : StringUtils.isNotBlank(brand.getName()) ? brand.getName() : "N/A");
                        inventoryActionLog.setCommittedQuantity(inventoryActionLog.getCommittedQuantity().add(item.getQuantity()));
                        inventoryActionLog.setBatchSku((productBatch != null && StringUtils.isNotBlank(productBatch.getSku())) ? productBatch.getSku() : "N/A");

                        if (transaction.getStatus() == Transaction.TransactionStatus.Completed) {
                            inventoryActionLog.setDeliveredQuantity(inventoryActionLog.getDeliveredQuantity().add(item.getQuantity()));
                        } else {
                            quantity = inventoryActionLog.getDeliveredQuantity().add(quantityLog.getQuantity());
                            inventoryActionLog.setDeliveredQuantity(quantity);
                        }
                    }
                }
            }
        }

        Collections.sort(Lists.newArrayList(inventoryActionLogs.values()), new Comparator<InventoryActionLog>()  {
            @Override
            public int compare(InventoryActionLog inventoryLog1, InventoryActionLog inventoryLog2) {
                if(StringUtils.isBlank(inventoryLog1.getProductName()) && StringUtils.isBlank(inventoryLog2.getProductName())){
                    return 0;
                }
                return inventoryLog1.getProductName().compareToIgnoreCase(inventoryLog2.getProductName());
            }
        });

        int size = inventoryActionLogs.size();
        List<InventoryActionLog> inventoryActionLogResults;
        if (start > size) {
            inventoryActionLogResults = new ArrayList<>();
        } else if ((limit + start) > size) {
            inventoryActionLogResults = Lists.newArrayList(inventoryActionLogs.values()).subList(start, size);
        } else {
            inventoryActionLogResults = Lists.newArrayList(inventoryActionLogs.values()).subList(start, (start + limit));
        }

        inventoryActionLogSearchResult.setValues(Lists.newArrayList(inventoryActionLogResults));
        inventoryActionLogSearchResult.setLimit(limit);
        inventoryActionLogSearchResult.setSkip(start);
        inventoryActionLogSearchResult.setTotal((long)inventoryActionLogs.size());
        return inventoryActionLogSearchResult;
    }

}
