package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.DerivedProductBatchInfo;
import com.fourtwenty.core.domain.models.product.DerivedProductBatchLog;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.DerivedProductBatchLogRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.DerivedProductBatchLogService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DerivedProductBatchLogServiceImpl extends AbstractAuthServiceImpl implements DerivedProductBatchLogService {

    @Inject
    private DerivedProductBatchLogRepository logRepository;

    @Inject
    public DerivedProductBatchLogServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public DerivedProductBatchLog createLog(Product product, BigDecimal quantity, Map<String, List<DerivedProductBatchInfo>> productMap) {

        DerivedProductBatchLog log = new DerivedProductBatchLog();
        log.prepare(token.getCompanyId());
        log.setShopId(token.getShopId());
        log.setDerivedProductId(product.getId());
        log.setDerivedQuantity(quantity);
        log.setProductMap(productMap);

        return logRepository.save(log);
    }

    @Override
    public DerivedProductBatchLog getById(String companyId, String derivedLogId) {
        return logRepository.get(companyId, derivedLogId);
    }

    @Override
    public DerivedProductBatchLog updateLog(DerivedProductBatchLog log) {
        log.prepare(token.getCompanyId());
        return logRepository.update(token.getCompanyId(), log.getId(), log);
    }

    @Override
    public HashMap<String, DerivedProductBatchLog> listAsMap(String companyId, List<ObjectId> derivedLogIds) {
        return logRepository.listAsMap(companyId, derivedLogIds);
    }
}
