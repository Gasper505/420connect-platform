package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.repositories.dispensary.DoctorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.customer.DoctorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.DoctorService;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
public class DoctorServiceImpl extends AbstractAuthServiceImpl implements DoctorService {
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    RealtimeService realtimeService;

    @Inject
    public DoctorServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public Doctor getDoctorById(String doctorId) {
        if (doctorId == null) {
            throw new BlazeInvalidArgException("DoctorId", "DoctorId cannot be null.");
        }
        return doctorRepository.get(token.getCompanyId(), doctorId);
    }

    @Override
    public SearchResult<Doctor> searchDoctorsByTerm(String term, String physicianId, int start, int limit) {

        if (limit <= 0 || limit > 1000) {
            limit = 1000;
        }
        if (term == null) {
            term = "";
        }
        term = term.toLowerCase();
        if (StringUtils.isNotEmpty(physicianId)) {
            SearchResult<Doctor> result = new SearchResult<>();

            Doctor doctor = doctorRepository.get(token.getCompanyId(), physicianId);
            if (doctor != null) {
                result.getValues().add(doctor);
                result.setTotal(1l);
            }

            return result;
        }
        if (StringUtils.isBlank(term)) {
            return doctorRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
        }

        return doctorRepository.searchDoctorsByTerm(token.getCompanyId(), term, start, limit);
    }

    @Override
    public SearchResult<Doctor> getDoctorsByDate(long afterDate, long beforeDate) {
        // Temporary fix:
        afterDate = 0;
        return doctorRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public List<Doctor> getAllDoctors() {
        SearchResult<Doctor> result = doctorRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
        return result.getValues();
    }

    @Override
    public Doctor addDoctor(DoctorAddRequest addRequest) {

        Doctor dbdoctor = doctorRepository.getDoctorByLicense(token.getCompanyId(), addRequest.getLicense());
        if (dbdoctor != null) {
            throw new BlazeInvalidArgException("Doctor", "A doctor with this license already exist.");
        }

        Doctor doctor = new Doctor();
        doctor.setCompanyId(token.getCompanyId());
        doctor.setFirstName(addRequest.getFirstName());
        doctor.setLastName(addRequest.getLastName());
        if (addRequest.getAddress() != null) {
            doctor.setAddress(addRequest.getAddress());
            if (doctor.getAddress() != null) {
                doctor.getAddress().prepare();
            }
        }
        doctor.setLicense(addRequest.getLicense());
        doctor.setEmail(addRequest.getEmail());
        doctor.setWebsite(addRequest.getWebsite());
        doctor.setPhoneNumber(addRequest.getPhoneNumber());
        doctor.setSearchText(addRequest.getFirstName() + addRequest.getLastName());
        doctor.setActive(addRequest.isActive());

        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.DoctorsUpdateEvent, doctor);
        return doctorRepository.save(doctor);
    }

    /**
     * This method exists to vet the doctor request object and return the parsed doctor object the
     * importerService which will directly interact with the DoctorRepository
     *
     * @param importRequest importRequest object
     * @return
     */
    @Override
    public Doctor sanitize(DoctorAddRequest importRequest) throws BlazeInvalidArgException {
        if (importRequest == null
                || StringUtils.isBlank(importRequest.getFirstName())
                || StringUtils.isBlank(importRequest.getLastName())) {

            throw new BlazeInvalidArgException("DoctorImportRequest", "Invalid Args: null request or missing FN or LN");
        }
        Doctor doctor = new Doctor();
        doctor.setImportId(importRequest.getImportId());
        doctor.setCompanyId(token.getCompanyId());
        doctor.setFirstName(importRequest.getFirstName());
        doctor.setLastName(importRequest.getLastName());
        doctor.setAddress(importRequest.getAddress());
        if (doctor.getAddress() != null && doctor.getAddress().getId() == null) {
            doctor.getAddress().setId(ObjectId.get().toString());
        }
        doctor.setLicense(importRequest.getLicense());
        doctor.setSearchText(importRequest.getFirstName() + " " + importRequest.getLastName() + " " + importRequest.getLicense());
        doctor.setActive(true);
        return doctor;
    }

    @Override
    public Doctor updateDoctor(String doctorId, Doctor doctor) {

        Doctor dbDoctor = doctorRepository.get(token.getCompanyId(), doctorId);
        if (dbDoctor == null) {
            throw new BlazeInvalidArgException("Doctor", "Doctor doesn't exist.");
        }

        doctor.setId(dbDoctor.getId());
        doctor.setCompanyId(token.getCompanyId());

        if (doctor.getAddress() != null) {
            doctor.getAddress().prepare();
        }

        // Check to make sure this is a new license or not
        if (!dbDoctor.getLicense().equalsIgnoreCase(doctor.getLicense())) {
            // Different, now see if there's another doctor with same license
            Doctor anotherDoc = doctorRepository.getDoctorByLicense(token.getCompanyId(), doctor.getLicense());
            if (anotherDoc != null && !anotherDoc.getId().equalsIgnoreCase(dbDoctor.getId())) {
                throw new BlazeInvalidArgException("Doctor.License", "Another doctor with this license already exist.");
            }
        }
        if (doctor.getNotes() != null) {
            for (Note note : doctor.getNotes()) {
                if (note.getId() == null) {
                    note.setId(ObjectId.get().toString());
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }

        dbDoctor.setAddress(doctor.getAddress());
        dbDoctor.setPhoneNumber(doctor.getPhoneNumber());
        dbDoctor.setLicense(doctor.getLicense());
        dbDoctor.setFirstName(doctor.getFirstName());
        dbDoctor.setLastName(doctor.getLastName());
        dbDoctor.setDegree(doctor.getDegree());
        dbDoctor.setFax(doctor.getFax());
        dbDoctor.setEmail(doctor.getEmail());
        dbDoctor.setState(doctor.getState());
        dbDoctor.setWebsite(doctor.getWebsite());
        dbDoctor.setAssets(doctor.getAssets());
        dbDoctor.setActive(doctor.isActive());
        dbDoctor.setNotes(doctor.getNotes());

        doctorRepository.update(token.getCompanyId(), doctor.getId(), dbDoctor);
        return dbDoctor;
    }

    @Override
    public void deleteDoctor(String doctorId) {
        doctorRepository.removeByIdSetState(token.getCompanyId(), doctorId);
    }
}
