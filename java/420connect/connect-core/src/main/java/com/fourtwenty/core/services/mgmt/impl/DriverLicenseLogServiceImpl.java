package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.DriverLicenseLog;
import com.fourtwenty.core.domain.repositories.dispensary.DriverLicenseLogRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.DriverLicenseLogService;
import com.google.inject.Provider;

import javax.inject.Inject;

public class DriverLicenseLogServiceImpl extends AbstractAuthServiceImpl implements DriverLicenseLogService {

    @Inject
    private DriverLicenseLogRepository driverLicenseLogRepository;

    @Inject
    public DriverLicenseLogServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public DriverLicenseLog saveLicenseLog(String licenseDetail) {
        DriverLicenseLog log = new DriverLicenseLog();
        log.prepare(token.getCompanyId());
        log.setShopId(token.getShopId());
        log.setLicenseDetail(licenseDetail);
        log.setEmployeeId(token.getActiveTopUser().getUserId());

        return driverLicenseLogRepository.save(log);
    }
}
