package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.EmailService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;

/**
 * Created by mdo on 8/13/16.
 */
public class EmailServiceImpl extends AbstractAuthServiceImpl implements EmailService {
    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    ConnectConfiguration connectConfiguration;

    @Inject
    public EmailServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public void sendWelcomeEmail(Employee employee, String passwordResetCode) {
        String body = getWelcomeBody(employee, passwordResetCode);
        amazonServiceManager.sendEmail(connectConfiguration.getSupportEmail(), employee.getEmail(), "Welcome to BLAZE Retail", body, null, null, "Blaze Support");
    }

    @Override
    public void sendWinEmail(Company company, Employee employee, Shop shop) {
        String body = getWinBody(company, employee, shop.getShopType().name());
        amazonServiceManager.sendEmail(connectConfiguration.getSupportEmail(), "win@blaze.me", "New Win: " + company.getName(), body, null, null, "Blaze Support");
    }

    private String getWelcomeBody(Employee employee, String passwordResetCode) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/welcome2.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String theString = writer.toString();

        theString = theString.replaceAll(Pattern.quote("{{name}}"), employee.getFirstName() + " " + employee.getLastName());
        theString = theString.replaceAll(Pattern.quote("{{action_url}}"), connectConfiguration.getAppWebsiteURL() + "/password/update?code=" + passwordResetCode);

        return theString;
    }

    private String getWinBody(Company company, Employee employee, String shopType) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/win.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String theString = writer.toString();
        theString = theString.replaceAll(Pattern.quote("{{date}}"), DateTime.now().toLocalDateTime().toString());
        theString = theString.replaceAll(Pattern.quote("{{productSKU}}"), company.getProductSKU());
        theString = theString.replaceAll(Pattern.quote("{{companyId}}"), company.getId());
        theString = theString.replaceAll(Pattern.quote("{{companyName}}"), company.getName());
        theString = theString.replaceAll(Pattern.quote("{{email}}"), company.getEmail());
        theString = theString.replaceAll(Pattern.quote("{{firstName}}"), employee.getFirstName());
        theString = theString.replaceAll(Pattern.quote("{{lastName}}"), employee.getLastName());
        theString = theString.replaceAll(Pattern.quote("{{phoneNumber}}"), emptyOrString(company.getPhoneNumber()));
        theString = theString.replaceAll(Pattern.quote("{{isId}}"), emptyOrString(company.getIsId()));
        theString = theString.replaceAll(Pattern.quote("{{website}}"), emptyOrString(company.getWebsite()));
        theString = theString.replaceAll(Pattern.quote("{{shopType}}"), shopType);
        theString = theString.replaceAll(Pattern.quote("{{address}}"), emptyOrString(company.getAddress().getAddress()));
        theString = theString.replaceAll(Pattern.quote("{{city}}"), emptyOrString(company.getAddress().getCity()));
        theString = theString.replaceAll(Pattern.quote("{{state}}"), emptyOrString(company.getAddress().getState()));
        theString = theString.replaceAll(Pattern.quote("{{zipCode}}"), emptyOrString(company.getAddress().getZipCode()));

        return theString;
    }

    private static String emptyOrString(String s) {
        if (s == null) {
            return "";
        }
        return s;
    }
}
