package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.EmployeeOnFleetInfo;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeamInfo;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.OnFleetTeamsRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TimeCardRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeCredentialUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeePinUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeCustomResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeManifestResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeOnFleetInfoResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.services.payments.StripeService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

public class EmployeeServiceImpl extends AbstractAuthServiceImpl implements EmployeeService {

    private static final String ROLE_NOT_FOUND = "Error! Role does not exist.";
    private static final String ROLE_NOT_MATCH = "Error! Active user role is not Admin.";
    private static final String EMPLOYEE_NOT_FOUND = "Error! Employee does not exist.";
    private static final String ROLE_NAME = "Delivery Driver";

    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    RoleRepository roleRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    TimeCardRepository timeCardRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    private OnFleetTeamsRepository onFleetTeamsRepository;
    @Inject
    private StripeService stripeService;

    @Inject
    public EmployeeServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public Employee addEmployee(EmployeeAddRequest addRequest) {

        String email = addRequest.getEmail().toLowerCase().trim();
        Employee employee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), addRequest.getPin());


        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        long count = employeeRepository.count(token.getCompanyId());

        if (employee != null) {
            throw new BlazeQuickPinExistException("DuplicateQuickPin", "Please choose another quick pin.");
        }
        if (!StringUtils.isNumeric(addRequest.getPin())) {
            throw new BlazeInvalidArgException("Employee.", "Employee Pin should be numeric.");
        }

        employee = employeeRepository.getEmployeeByEmail(email);
        if (employee != null) {
            throw new BlazeQuickPinExistException("DuplicateEmail", "Another employee exists with this email.");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());


        // to check see if this user already exist
        employee = new Employee();
        employee.setCompanyId(token.getCompanyId());
        employee.setLastName(addRequest.getLastName());
        employee.setFirstName(addRequest.getFirstName());
        employee.setPin(addRequest.getPin());
        employee.setEmail(email);
        employee.setPhoneNumber(addRequest.getPhoneNumber());
        employee.setPassword(securityUtil.encryptPassword(addRequest.getPassword()));
        employee.setShops(addRequest.getShops());
        employee.setAssignedInventoryId(addRequest.getInventoryId());
        //employee.setRole(addRequest.getRole());
        employee.setAddress(addRequest.getAddress());
        employee.setDlExpirationDate(addRequest.getDlExpirationDate());
        employee.setDriver(addRequest.isDriver());
        if (employee.getAddress() != null) {
            employee.getAddress().prepare(token.getCompanyId());
        }

        if (employee.getShops() == null || employee.getShops().size() == 0) {
            List<String> shops = new ArrayList<>();
            shops.add(token.getShopId());
            employee.setShops(shops);
        }

        Role role = null;
        if (StringUtils.isNotEmpty(addRequest.getRoleId())) {
            role = roleRepository.get(token.getCompanyId(), addRequest.getRoleId());
            if (role == null) {
                throw new BlazeInvalidArgException("Role", "Role does not exist.");
            }
            String roleId = applyRoleForAdmin(role);
            employee.setRoleId(roleId);
        } else {
            throw new BlazeInvalidArgException("RoleId", "Role id is missing.");
        }

        if ("Admin".equalsIgnoreCase(role.getName())) {
            LinkedHashSet<CompanyFeatures.AppTarget> appAccessList = new LinkedHashSet<>();
            appAccessList.add(CompanyFeatures.AppTarget.AuthenticationApp);
            appAccessList.add(CompanyFeatures.AppTarget.Distribution);
            appAccessList.add(CompanyFeatures.AppTarget.Retail);
            appAccessList.add(CompanyFeatures.AppTarget.Grow);
            addRequest.setAppAccessList(appAccessList);
        } else {
            if (shop != null) {
                if (addRequest.getAppAccessList() == null) {
                    addRequest.setAppAccessList(new LinkedHashSet<>());
                }
                addRequest.getAppAccessList().add(shop.getAppTarget());
            }
        }

        //enable and disable Employee for custom discount
        employee.setCanApplyCustomDiscount(addRequest.getCanApplyCustomDiscount());
        if (!CollectionUtils.isNullOrEmpty(addRequest.getAppAccessList())) {
            employee.setAppAccessList(addRequest.getAppAccessList());
        }

        employee.setVinNo(addRequest.getVinNo());
        employee.setDriversLicense(addRequest.getDriversLicense());
        employee.setVehicleMake(addRequest.getVehicleMake());
        employee.setVehicleModel(addRequest.getVehicleModel());
        employee.setVehicleLicensePlate(addRequest.getVehicleLicensePlate());
        employee.setInsuranceCompanyName(addRequest.getInsuranceCompanyName());
        employee.setInsuranceExpireDate(addRequest.getInsuranceExpireDate());
        employee.setPolicyNumber(addRequest.getPolicyNumber());
        employee.setRegistrationExpireDate(addRequest.getRegistrationExpireDate());


        if (!CollectionUtils.isNullOrEmpty(addRequest.getNotes())) {
            for (Note note : addRequest.getNotes()) {
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            }
        }
        employee.setNotes(addRequest.getNotes());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(addRequest.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            employee.setExternalId(addRequest.getExternalId());
        }

        employeeRepository.save(employee);
        employee.setPin(null);
        employee.setPassword(null);
        employee.setRole(role);

        stripeService.updateSubscriptionEmployeeSeat(token.getCompanyId());
        return employee;
    }

    /**
     * @param role : Request role
     * @implNote : This method is using allow Admins to assign other employees role is admin
     */
    private String applyRoleForAdmin(Role role) {
        String roleId = "";
        if (role.getName().equalsIgnoreCase("Admin")) {
            Employee activeEmployee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
            if (activeEmployee == null) {
                throw new BlazeInvalidArgException("Employee", EMPLOYEE_NOT_FOUND);
            }
            Role activeUserRole = roleRepository.get(token.getCompanyId(), activeEmployee.getRoleId());
            if (activeUserRole == null) {
                throw new BlazeInvalidArgException("Role", ROLE_NOT_FOUND);
            }
            if (activeUserRole.getName().equalsIgnoreCase("Admin")) {
                roleId = role.getId();
            } else {
                throw new BlazeInvalidArgException("Role", ROLE_NOT_MATCH);
            }
        } else {
            roleId = role.getId();
        }
        return roleId;
    }

    @Override
    public Employee getEmployeeById(String employeeId) {
        String projection = getPermissionProjection();
        Employee dbEmployee = employeeRepository.get(token.getCompanyId(), employeeId, projection);
        if (dbEmployee == null) {
            throw new BlazeInvalidArgException("EmployeeId", "Employee does not exist with this id.");
        }
        return dbEmployee;
    }

    @Override
    public Employee getEmployeeByQuickPin(String companyId, String quickPin) {
        return employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), quickPin);
    }

    @Override
    public Employee updateEmployee(String employeeId, Employee updateRequest) {
        Employee dbEmployee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (dbEmployee == null) {
            throw new BlazeInvalidArgException("EmployeeId", "Employee does not exist with this id.");
        }

        Role role = null;
        if (StringUtils.isNotEmpty(updateRequest.getRoleId())) {
            role = roleRepository.get(token.getCompanyId(), updateRequest.getRoleId());
            if (role == null) {
                throw new BlazeInvalidArgException("Role", "Role does not exist.");
            }
            String roleId = applyRoleForAdmin(role);
            dbEmployee.setRoleId(roleId);
        } else {
            throw new BlazeInvalidArgException("RoleId", "Role id is missing.");
        }

        if (updateRequest.getNotes() != null) {
            for (Note note : updateRequest.getNotes()) {
                if (StringUtils.isBlank(note.getId())) {
                    note.prepare();
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }

        dbEmployee.setFirstName(updateRequest.getFirstName());
        dbEmployee.setLastName(updateRequest.getLastName());
        dbEmployee.setNotes(updateRequest.getNotes());
        dbEmployee.setShops(updateRequest.getShops());
        dbEmployee.setPhoneNumber(updateRequest.getPhoneNumber());
        dbEmployee.setAssignedInventoryId(updateRequest.getAssignedInventoryId());
        dbEmployee.setAssignedTerminalId(updateRequest.getAssignedTerminalId());
        dbEmployee.setDisabled(updateRequest.isDisabled());
        dbEmployee.setDriversLicense(updateRequest.getDriversLicense());
        dbEmployee.setDlExpirationDate(updateRequest.getDlExpirationDate());
        dbEmployee.setVehicleMake(updateRequest.getVehicleMake());
        dbEmployee.setAddress(updateRequest.getAddress());
        dbEmployee.setDriver(updateRequest.isDriver());
        if (dbEmployee.getAddress() != null) {
            dbEmployee.getAddress().prepare(token.getCompanyId());
        }

        // Check for Pin updates
        if (StringUtils.isNotBlank(updateRequest.getPin())) {
            // Make sure pins are numeric
            if (!StringUtils.isNumeric(updateRequest.getPin())) {
                throw new BlazeInvalidArgException("Employee Pin", "Employee Pin should be numeric.");
            }

            Employee anotherEmployee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), updateRequest.getPin());
            if (anotherEmployee != null && !anotherEmployee.getId().equalsIgnoreCase(dbEmployee.getId())) {
                throw new BlazeInvalidArgException("Employee Pin", "This pin is in used.");
            }

            dbEmployee.setPin(updateRequest.getPin());
        }

        // Chheck for email updates
        boolean isOwner = token.getActiveTopUser().getUserId().equalsIgnoreCase(dbEmployee.getId());
        if (StringUtils.isNotBlank(updateRequest.getEmail())) {
            String email = updateRequest.getEmail().toLowerCase().trim();
            // check to see if email is updated
            if (!dbEmployee.getEmail().equalsIgnoreCase(email)) {
                if (isOwner) {
                    // Check to make sure another person doesn't have this email
                    Employee otherEmployee = employeeRepository.getEmployeeByEmail(email);
                    if (otherEmployee != null && !otherEmployee.getId().equalsIgnoreCase(dbEmployee.getId())) {
                        throw new BlazeInvalidArgException("Employee", "Duplicate email.");
                    }
                    dbEmployee.setEmail(email);
                } else {
                    throw new BlazeAuthException("Authorization", "You do not have permission to update this employee's email");
                }
            }
        }

        // check to see if there's a new password
        if (StringUtils.isNotBlank(updateRequest.getPassword()) && !updateRequest.getPassword().equalsIgnoreCase(dbEmployee.getPassword())) {
            if (isOwner) {
                dbEmployee.setPassword(securityUtil.encryptPassword(updateRequest.getPassword()));
            } else {
                throw new BlazeAuthException("Authorization", "You do not have permission to update this employee's password");
            }
        }
        //enable and disable Employee for custom discount
        dbEmployee.setCanApplyCustomDiscount(updateRequest.getCanApplyCustomDiscount());

        dbEmployee.setInsuranceExpireDate(updateRequest.getInsuranceExpireDate());
        dbEmployee.setInsuranceCompanyName(updateRequest.getInsuranceCompanyName());
        dbEmployee.setPolicyNumber(updateRequest.getPolicyNumber());
        dbEmployee.setRegistrationExpireDate(updateRequest.getRegistrationExpireDate());
        dbEmployee.setVehiclePin(updateRequest.getVehiclePin());
        dbEmployee.setAppAccessList(updateRequest.getAppAccessList());

        dbEmployee.setVinNo(updateRequest.getVinNo());
        dbEmployee.setVehicleModel(updateRequest.getVehicleModel());
        dbEmployee.setVehicleLicensePlate(updateRequest.getVehicleLicensePlate());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(updateRequest.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            dbEmployee.setExternalId(updateRequest.getExternalId());
        }

        employeeRepository.update(token.getCompanyId(), employeeId, dbEmployee);
        dbEmployee.setPassword(null);
        dbEmployee.setRole(role);
        return dbEmployee;
    }


    @Override
    public void updateCredentials(String employeeId, EmployeeCredentialUpdateRequest request) {
        Employee dbEmployee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (dbEmployee == null) {
            throw new BlazeInvalidArgException("EmployeeId", "Employee does not exist with this id.");
        }

        if (StringUtils.isNotEmpty(request.getEmail())) {
            String email = request.getEmail().toLowerCase().trim();
            if (!dbEmployee.getEmail().equalsIgnoreCase(email)) {
                // Check to make sure another person doesn't have this email
                Employee otherEmployee = employeeRepository.getEmployeeByEmail(email);
                if (otherEmployee != null && !otherEmployee.getId().equalsIgnoreCase(dbEmployee.getId())) {
                    throw new BlazeInvalidArgException("Employee", "Duplicate email.");
                }
                dbEmployee.setEmail(email);
            }
        }

        if (StringUtils.isNotEmpty(request.getPassword())) {
            dbEmployee.setPassword(securityUtil.encryptPassword(request.getPassword()));
        }

        employeeRepository.update(token.getCompanyId(), employeeId, dbEmployee);
    }

    @Override
    public void updatePin(String employeeId, EmployeePinUpdateRequest request) {
        Employee dbEmployee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (dbEmployee == null) {
            throw new BlazeInvalidArgException("EmployeeId", "Employee does not exist with this id.");
        }

        // Make sure pins are numeric
        if (!StringUtils.isNumeric(request.getPin())) {
            throw new BlazeInvalidArgException("Employee Pin", "Employee Pin should be numeric.");
        }

        Employee anotherEmployee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), request.getPin());
        if (anotherEmployee != null && !anotherEmployee.getId().equalsIgnoreCase(dbEmployee.getId())) {
            throw new BlazeInvalidArgException("Employee Pin", "This pin is in used.");
        }

        dbEmployee.setPin(request.getPin());

        employeeRepository.update(token.getCompanyId(), employeeId, dbEmployee);
    }

    @Override
    public SearchResult<EmployeeResult> searchEmployees(String searchTerm, int start, int limit, String accessConfig, String shopId, String roleId) {

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        SearchResult<EmployeeResult> result;
        String projection = getPermissionProjection();
        if (StringUtils.isNotBlank(roleId)) {
            result = (StringUtils.isBlank(searchTerm)) ?
                    employeeRepository.findItemsByRole(token.getCompanyId(), start, limit, projection, roleId, EmployeeResult.class) :
                    employeeRepository.findItemsByRoleAndSearchTerms(token.getCompanyId(), searchTerm, start, limit, projection, roleId, EmployeeResult.class);

        } else if (StringUtils.isNotBlank(accessConfig)) {
            List<CompanyFeatures.AppTarget> appTargets = new ArrayList<>();
            List<String> accessConfigList = Arrays.asList(accessConfig.split("\\s*,\\s*"));
            for (String appTarget : accessConfigList) {
                appTargets.add(CompanyFeatures.AppTarget.valueOf(appTarget));
            }

            result = (StringUtils.isBlank(searchTerm)) ?
                    employeeRepository.findItemsByAccessConfig(token.getCompanyId(), start, limit, projection, appTargets, EmployeeResult.class) :
                    employeeRepository.findItemsByAccessConfigAndTerms(token.getCompanyId(), searchTerm, start, limit, projection, appTargets, EmployeeResult.class);

        } else if (StringUtils.isNotBlank(shopId)) {
            List<String> shopIds = Arrays.asList(shopId.split("\\s*,\\s*"));
            result = (StringUtils.isBlank(searchTerm)) ?
                    employeeRepository.findItemsByShops(token.getCompanyId(), shopIds, start, limit, projection) :
                    employeeRepository.findItemsByShopsAndTerm(token.getCompanyId(), shopIds, searchTerm, start, limit, projection);

        } else if (StringUtils.isBlank(searchTerm)) {
            result = employeeRepository.findItems(token.getCompanyId(), start, limit, projection, EmployeeResult.class);
        } else {
            result = employeeRepository.findItems(token.getCompanyId(), searchTerm, start, limit, projection, EmployeeResult.class);
        }

        processCompositeResults(result);
        return result;
    }

    @Override
    public String getPermissionProjection() {
        String projection = "{password:0,pin:0}";
        try {
            String userId = token.getActiveTopUser().getUserId();
            Employee employee = employeeRepository.get(token.getCompanyId(), userId);
            Role role = roleRepository.get(token.getCompanyId(), employee.getRoleId());
            if (role != null) {
                boolean isManager = role.getPermissions().contains(Role.Permission.WebEmployeeManage);
                if (isManager) {
                    projection = "{password:0}";
                }
            }
        } catch (Exception e) {
            // Ignore
        }
        return projection;
    }

    @Override
    public DateSearchResult<Employee> findEmployees(long afterDate, long beforeDate) {
        DateSearchResult<Employee> employeeSearchResult = employeeRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate, "{password:0}");
        processResults(employeeSearchResult);
        return employeeSearchResult;
    }

    private void processResults(SearchResult<Employee> result) {
        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(token.getCompanyId());

        List<ObjectId> timecardIds = new ArrayList<>();
        for (Employee employee : result.getValues()) {
            Role role = roleHashMap.get(employee.getRoleId());
            employee.setRole(role);
            if (StringUtils.isNotEmpty(employee.getTimecardId()) && ObjectId.isValid(employee.getTimecardId())) {
                timecardIds.add(new ObjectId(employee.getTimecardId()));
            }
        }

        HashMap<String, TimeCard> timeCards = timeCardRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), timecardIds);
        for (Employee employee : result.getValues()) {
            if (StringUtils.isNotEmpty(employee.getTimecardId())) {
                TimeCard timeCard = timeCards.get(employee.getTimecardId());
                employee.setTimeCard(timeCard);
            }
        }
    }

    private void processCompositeResults(SearchResult<EmployeeResult> result) {
        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(token.getCompanyId());
        HashMap<String, Terminal> terminalMap = terminalRepository.listAsMap(token.getCompanyId());

        List<ObjectId> timecardIds = new ArrayList<>();
        for (EmployeeResult employee : result.getValues()) {
            Role role = roleHashMap.get(employee.getRoleId());
            employee.setRole(role);
            if (StringUtils.isNotEmpty(employee.getTimecardId()) && ObjectId.isValid(employee.getTimecardId())) {
                timecardIds.add(new ObjectId(employee.getTimecardId()));
            }

            Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());
            if (terminal != null) {
                employee.setTerminalName(terminal.getName());
            }
        }

        HashMap<String, TimeCard> timeCards = timeCardRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), timecardIds);
        for (Employee employee : result.getValues()) {
            if (StringUtils.isNotEmpty(employee.getTimecardId())) {
                TimeCard timeCard = timeCards.get(employee.getTimecardId());
                employee.setTimeCard(timeCard);
            }
        }
    }

    @Override
    public void deleteEmployee(String employeeId) {
        if (StringUtils.isBlank(employeeId)) return;
        //employeeRepository.removeByIdSetState(token.getCompanyId(),employeeId);
        Employee employee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (employee != null) {
            String newPin = DateTime.now().getMillis() + "--" + employee.getPin();
            String newEmail = DateTime.now().getMillis() + "--" + employee.getEmail();
            employeeRepository.deleteEmployee(token.getCompanyId(), employeeId, newPin, newEmail);
            stripeService.updateSubscriptionEmployeeSeat(token.getCompanyId());
        }
    }

    @Override
    public EmployeeManifestResult getEmployeeManifest(String date) {
        DateTime dateTime = null;
        try {
            dateTime = DateUtil.parseDateKey(date);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Date", "Date must be in format 'yyyyMMdd'");
        }

        Company company = companyRepository.getById(token.getCompanyId());

        if (company == null || !company.isActive()) {
            throw new BlazeInvalidArgException("Company", "Company does not exist.");
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
        }
        employee.setPassword(null);
        employee.setPin(null);


        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Invalid shop.");
        }
        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(shop.getTimeZone());


        // Get Today's Transactions
        long timeZoneStartDateMillis = dateTime.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
        DateTime jodaEndDate = dateTime.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
        long timeZoneEndDateMillis = jodaEndDate.minusMinutes(timeZoneOffset).getMillis();

        Iterable<Transaction> results = transactionRepository.getTotalSales(token.getCompanyId(), token.getShopId(),
                timeZoneStartDateMillis, timeZoneEndDateMillis);

        List<Transaction> transactions = Lists.newArrayList(results);
        assignDependentObjects(transactions, true, true);

        // finalize result
        EmployeeManifestResult result = new EmployeeManifestResult();
        result.setCompany(company);
        result.setEmployee(employee);
        result.setTransactions(transactions);
        return result;
    }

    private void assignDependentObjects(List<Transaction> transactions, boolean assignedProducts, boolean assignEmployees) {
        Set<ObjectId> membersIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();

        for (Transaction transaction : transactions) {
            if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                membersIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (assignEmployees && ObjectId.isValid(transaction.getSellerId())) {
                employeeIds.add(new ObjectId(transaction.getSellerId()));
            }
            if (assignedProducts) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(membersIds));
        HashMap<String, Product> productHashMap = new HashMap<>();
        HashMap<String, Employee> employeeHashMap = new HashMap<>();

        if (assignedProducts) {
            productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));
        }
        if (assignEmployees) {
            employeeHashMap = employeeRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        }

        for (Transaction transaction : transactions) {
            if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                transaction.setMember(memberHashMap.get(transaction.getMemberId()));
            }
            if (assignedProducts) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    item.setProduct(productHashMap.get(item.getProductId()));
                }
            }
            if (assignEmployees) {
                Employee employee = employeeHashMap.get(transaction.getSellerId());
                employee.setPassword(null);
                employee.setPin(null);
                transaction.setSeller(employee);
            }
        }

    }

    /**
     * This method's result contains onfleet result of employee
     *
     * @param term  : term
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<EmployeeResult> getEmployeeList(String term, int start, int limit) {

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        SearchResult<EmployeeResult> result;
        String projection = getPermissionProjection();
        if (StringUtils.isBlank(term)) {
            result = employeeRepository.findItems(token.getCompanyId(), start, limit, projection, EmployeeResult.class);
        } else {
            result = employeeRepository.findItems(token.getCompanyId(), term, start, limit, projection, EmployeeResult.class);
        }

        if (result != null && result.getValues() != null) {
            prepareCustomEmployeeResult(result.getValues());
        }

        return result;
    }

    private void prepareCustomEmployeeResult(List<EmployeeResult> employeeList) {
        Iterable<OnFleetTeams> onFleetTeams = onFleetTeamsRepository.list(token.getCompanyId());

        //Map for onfleet team by shop
        Map<String, OnFleetTeams> teamByShopMap = new HashMap<>();
        for (OnFleetTeams onFleetTeam : onFleetTeams) {
            teamByShopMap.put(onFleetTeam.getShopId(), onFleetTeam);
        }

        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(token.getCompanyId());

        for (EmployeeResult employeeResult : employeeList) {
            if (employeeResult.getEmployeeOnFleetInfoList() != null && employeeResult.getEmployeeOnFleetInfoList().size() != 0) {
                List<EmployeeOnFleetInfoResult> infoResultList = new ArrayList<>();

                for (EmployeeOnFleetInfo onFleetInfo : employeeResult.getEmployeeOnFleetInfoList()) {
                    EmployeeOnFleetInfoResult infoResult = new EmployeeOnFleetInfoResult();
                    if (shopHashMap.containsKey(onFleetInfo.getShopId())) {
                        infoResult.setShopName(shopHashMap.get(onFleetInfo.getShopId()).getName());
                    }
                    infoResult.setShopId(onFleetInfo.getShopId());
                    infoResult.setOnFleetWorkerId(onFleetInfo.getOnFleetWorkerId());
                    infoResult.setOnFleetTeamList(onFleetInfo.getOnFleetTeamList());
                    OnFleetTeams fleetTeams = teamByShopMap.get(onFleetInfo.getShopId());

                    if (onFleetInfo.getOnFleetTeamList() != null && fleetTeams != null && onFleetInfo.getOnFleetTeamList().size() > 0) {
                        HashMap<String, String> employeeOnFleetTeams = new HashMap<>();
                        this.getEmployeeOnFleetTeam(fleetTeams, onFleetInfo, employeeOnFleetTeams);

                        infoResult.setEmployeeOnFleetTeams(employeeOnFleetTeams);
                    }
                    infoResultList.add(infoResult);
                }
                employeeResult.setEmployeeOnFleetInfoResults(infoResultList);
            }
        }


    }

    private void getEmployeeOnFleetTeam(OnFleetTeams fleetTeams, EmployeeOnFleetInfo onFleetInfo, HashMap<String, String> employeeOnFleetTeams) {
        for (String teamId : onFleetInfo.getOnFleetTeamList()) {
            for (OnFleetTeamInfo teamInfo : fleetTeams.getOnFleetTeamInfoList()) {
                if (teamInfo.getTeamId().equalsIgnoreCase(teamId)) {
                    employeeOnFleetTeams.put(teamId, teamInfo.getName());
                    break;
                }
            }
        }
    }

    @Override
    public SearchResult<EmployeeResult> getEmployeeByRole(String companyId) {
        Role employeeRole = roleRepository.getRoleByName(token.getCompanyId(), ROLE_NAME);
        SearchResult<EmployeeResult> result = new SearchResult<>();
        if (employeeRole != null && employeeRole.getId() != null)
            result = employeeRepository.findItemsByRole(token.getCompanyId(), 0, Integer.MAX_VALUE, "{password:0}", employeeRole.getId(), EmployeeResult.class);
        return result;
    }

    @Override
    public SearchResult<EmployeeCustomResult> getAllEmployeeList(String term, int start, int limit) {

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        SearchResult<EmployeeCustomResult> result;
        String projection = getPermissionProjection();
        if (StringUtils.isBlank(term)) {
            result = employeeRepository.findAllEmployee(token.getCompanyId(), start, limit, projection, EmployeeCustomResult.class);
        } else {
            result = employeeRepository.findItems(token.getCompanyId(), term, start, limit, projection, EmployeeCustomResult.class);
        }

        if (result != null && result.getValues() != null) {
            prepareEmployeeCustomResultForInternal(result.getValues());
        }

        return result;
    }

    /**
     * Override method to get employee by id for grow shop.
     *
     * @param employeeId : employeeId
     * @return : employee custom result
     */
    @Override
    public EmployeeCustomResult getCustomEmployeeById(String employeeId) {
        String projection = getPermissionProjection();
        EmployeeCustomResult dbEmployee = employeeRepository.getEmployeeById(employeeId, token.getCompanyId(), projection, EmployeeCustomResult.class);
        if (dbEmployee == null) {
            throw new BlazeInvalidArgException("EmployeeId", "Employee does not exist with this id.");
        }
        prepareEmployeeCustomResultForInternal(Lists.newArrayList(dbEmployee));
        return dbEmployee;
    }

    /**
     * Private method to prepare employee dependent objects.
     *
     * @param employeeList : employee list
     */
    private void prepareEmployeeCustomResultForInternal(List<EmployeeCustomResult> employeeList) {
        Iterable<OnFleetTeams> onFleetTeams = onFleetTeamsRepository.list(token.getCompanyId());

        //Map for onfleet team by shop
        Map<String, OnFleetTeams> teamByShopMap = new HashMap<>();
        for (OnFleetTeams onFleetTeam : onFleetTeams) {
            teamByShopMap.put(onFleetTeam.getShopId(), onFleetTeam);
        }

        Set<ObjectId> roleIds = new HashSet<>();
        Set<ObjectId> terminalIds = new HashSet<>();
        Set<ObjectId> shopIds = new HashSet<>();

        for (EmployeeCustomResult result : employeeList) {
            if (StringUtils.isNotBlank(result.getRoleId()) && ObjectId.isValid(result.getRoleId())) {
                roleIds.add(new ObjectId(result.getRoleId()));
            }
            if (StringUtils.isNotBlank(result.getAssignedTerminalId()) && ObjectId.isValid(result.getAssignedTerminalId())) {
                terminalIds.add(new ObjectId(result.getAssignedTerminalId()));
            }
            if (!CollectionUtils.isNullOrEmpty(result.getShops())) {
                for (String shopId : result.getShops()) {
                    if (StringUtils.isNotBlank(shopId) && ObjectId.isValid(shopId)) {
                        shopIds.add(new ObjectId(shopId));
                    }
                }
            }

            if (!CollectionUtils.isNullOrEmpty(result.getEmployeeOnFleetInfoList())) {
                for (EmployeeOnFleetInfo infoResult : result.getEmployeeOnFleetInfoList()) {
                    if (StringUtils.isNotBlank(infoResult.getShopId()) && ObjectId.isValid(infoResult.getShopId())) {
                        shopIds.add(new ObjectId(infoResult.getShopId()));
                    }
                }
            }
        }

        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(shopIds));
        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(roleIds));
        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(terminalIds));

        for (EmployeeCustomResult employeeResult : employeeList) {
            employeeResult.setRole(roleHashMap.get(employeeResult.getRoleId()));

            Terminal terminal = terminalHashMap.get(employeeResult.getAssignedTerminalId());
            employeeResult.setTerminalName(terminal != null ? terminal.getName() : "");

            if (!CollectionUtils.isNullOrEmpty(employeeResult.getShops())) {
                List<Shop> shops = new ArrayList<>();
                for (String shopId : employeeResult.getShops()) {
                    shops.add(shopHashMap.get(shopId));
                }
                shops.removeAll(Collections.singleton(null));
                employeeResult.setShopResult(shops);
            }

            if (employeeResult.getEmployeeOnFleetInfoList() != null && employeeResult.getEmployeeOnFleetInfoList().size() != 0) {
                List<EmployeeOnFleetInfoResult> infoResultList = new ArrayList<>();

                for (EmployeeOnFleetInfo onFleetInfo : employeeResult.getEmployeeOnFleetInfoList()) {
                    EmployeeOnFleetInfoResult infoResult = new EmployeeOnFleetInfoResult();
                    Shop shop = shopHashMap.get(onFleetInfo.getShopId());

                    if (shop != null) {
                        infoResult.setShopName(shop.getName());
                    }
                    infoResult.setShopId(onFleetInfo.getShopId());
                    infoResult.setOnFleetWorkerId(onFleetInfo.getOnFleetWorkerId());
                    infoResult.setOnFleetTeamList(onFleetInfo.getOnFleetTeamList());
                    OnFleetTeams fleetTeams = teamByShopMap.get(onFleetInfo.getShopId());

                    if (onFleetInfo.getOnFleetTeamList() != null && fleetTeams != null && onFleetInfo.getOnFleetTeamList().size() > 0) {
                        HashMap<String, String> employeeOnFleetTeams = new HashMap<>();
                        this.getEmployeeOnFleetTeam(fleetTeams, onFleetInfo, employeeOnFleetTeams);

                        infoResult.setEmployeeOnFleetTeams(employeeOnFleetTeams);
                    }
                    infoResultList.add(infoResult);
                }
                employeeResult.setEmployeeOnFleetInfoResults(infoResultList);
            }
        }

    }
}
