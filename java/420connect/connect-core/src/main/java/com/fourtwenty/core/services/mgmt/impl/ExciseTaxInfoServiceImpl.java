package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.repositories.dispensary.ExciseTaxInfoRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ExciseTaxInfoService;
import com.google.inject.Provider;

import javax.inject.Inject;

public class ExciseTaxInfoServiceImpl extends AbstractAuthServiceImpl implements ExciseTaxInfoService {

    private ExciseTaxInfoRepository exciseTaxInfoRepository;

    @Inject
    public ExciseTaxInfoServiceImpl(Provider<ConnectAuthToken> token, ExciseTaxInfoRepository exciseTaxInfoRepository) {
        super(token);
        this.exciseTaxInfoRepository = exciseTaxInfoRepository;
    }

    @Override
    public ExciseTaxInfo getExciseTaxInfoForShop(Shop shop) {
        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }
        return exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);
    }
}
