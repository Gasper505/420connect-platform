package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.security.tokens.AssetAccessToken;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ExportService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;

public class ExportServiceImpl extends AbstractAuthServiceImpl implements ExportService {

    private static final Logger logger = LoggerFactory.getLogger(ExportServiceImpl.class);

    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    SecurityUtil securityUtil;

    @Inject
    public ExportServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    private static final String DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private static final String FILE_HEADERS = "Product Id,Product Name,Brand,Category,Retail Value,Metrc Package Label,Batch SKU,Batch Date,Cost Per Unit,THC,CBD,CBN,THCA,CBDA,Potency: THC,Potency: CBD,Potency: CBN,Potency: THCA,Potency: CBDA,";

    @Override
    public StringBuilder exportProductsByShop(String shopId, String assetToken) {
        AssetAccessToken authToken = securityUtil.decryptAssetAccessToken(assetToken);
        shopId = StringUtils.isBlank(shopId) ? token.getShopId() : shopId;

        Iterable<Product> products = productRepository.listByShop(authToken.getCompanyId(), shopId);
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAllAsMap(authToken.getCompanyId());
        Iterable<Inventory> inventories = inventoryRepository.listAllByShopActive(authToken.getCompanyId(), shopId);

        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(authToken.getCompanyId());

        StringBuilder builder = new StringBuilder(FILE_HEADERS);

        for (Inventory inventory : inventories) {
            builder.append("Inv: ").append(inventory.getName()).append(DELIMITER);
        }
        builder.append(NEW_LINE_SEPARATOR);
        for (Product product : products) {
            builder.append(product.getId()).append(DELIMITER);
            String productName = replaceComma(product.getName());
            builder.append(productName).append(DELIMITER);

            Brand brand = brandHashMap.get(product.getBrandId());
            if (brand != null) {
                String brandName = replaceComma(brand.getName());
                builder.append(brandName);
            } else {
                builder.append("");
            }

            builder.append(DELIMITER);

            ProductCategory category = categoryMap.get(product.getCategoryId());
            if (category != null) {
                String categoryName = replaceComma(category.getName());
                builder.append(categoryName);
            } else {
                builder.append("");
            }

            builder.append(DELIMITER);
            // show retail value
            BigDecimal price = new BigDecimal(0);
            if (product.getPriceBreaks() != null && product.getPriceBreaks().size() > 0) {
                price = product.getPriceBreaks().get(0).getPrice();
            } else if (product.getPriceRanges() != null && product.getPriceRanges().size() > 0) {
                price = product.getPriceRanges().get(0).getPrice();
            } else {
                price = product.getUnitPrice();
            }
            if (price == null) {
                price = new BigDecimal(0);
            }
            builder.append(String.format("%.2f", price.doubleValue()));
            builder.append(NEW_LINE_SEPARATOR);
        }

        logger.info("Products CSV created successfully for shop Id : " + shopId);

        return builder;

    }

    /* replace Comma with space */
    private String replaceComma(String str) {
        if (str != null || str.isEmpty()) {
            str = str.replace(',', ' ');
        } else {
            return str;
        }
        return str;
    }
}
