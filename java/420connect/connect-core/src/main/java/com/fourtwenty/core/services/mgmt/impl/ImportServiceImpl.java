package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.importer.exception.ImportException;
import com.fourtwenty.core.importer.main.Importable;
import com.fourtwenty.core.importer.main.Parser;
import com.fourtwenty.core.importer.main.parsers.*;
import com.fourtwenty.core.importer.meadow.MeadowParser;
import com.fourtwenty.core.importer.model.*;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.pos.POSMembershipService;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.google.inject.Provider;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
/**
 * Created by Stephen Schmidt on 1/26/2016.
 */
public class ImportServiceImpl extends AbstractAuthServiceImpl implements ImportService {
    private static final Log LOG = LogFactory.getLog(ImportServiceImpl.class);
    @Inject
    VendorRepository vendorRepository;
    @Inject
    VendorService vendorService;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    DoctorService doctorService;
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductService productService;
    @Inject
    POSMembershipService membershipService;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    InventoryService inventoryService;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    MemberService memberService;
    @Inject
    AssetService assetService;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    ProductChangeLogRepository productChangeLogRepository;
    @Inject
    BarcodeItemRepository barcodeItemRepository;
    @Inject
    BackgroundTaskManager taskManager;
    @Inject
    BatchQuantityRepository batchQuantityRepository;
    @Inject
    private BatchQuantityService batchQuantityService;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private BarcodeService barcodeService;
    @Inject
    private MetrcAccountService metrcAccountService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private BatchActivityLogRepository batchActivityLogRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private MeadowParser meadowParser;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    public ImportServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public void importFiles(InputStream vendorInput, InputStream doctorInput, InputStream productInput,
                            InputStream customerInput, InputStream assetFolder, InputStream promotionFile) {
        Parser parser = new Parser();

        // Upload assetFolder
        HashMap<String, CompanyAsset> assetHashMap = new HashMap<>();
        if (assetFolder != null) {
            assetHashMap = importCompanyAssets(assetFolder);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Blaze", "Shop does not exist.");
        }

        // Now Import
        ImportResult vendorImportResult = new ImportResult(Parser.ParseDataType.Vendor);
        if (vendorInput != null) {
            ParseResult vendorParseResult = parser.parse(vendorInput, new VendorParser(vendorService), shop);
            vendorImportResult = importVendors(vendorParseResult);
        }

        ImportResult doctorImportResult = new ImportResult(Parser.ParseDataType.Doctor);
        if (doctorInput != null) {
            ParseResult doctorParseResult = parser.parse(doctorInput, new DoctorParser(doctorService), shop);
            doctorImportResult = importDoctors(doctorParseResult);
        }
        ImportResult productImportResult = new ImportResult(Parser.ParseDataType.Product);
        if (productInput != null) {
            ParseResult productParseResult = parser.parse(productInput, new ProductParser(productService, vendorService), shop);
            productImportResult = importProducts(shop,productParseResult);
        }

        ImportResult customerImportResult = new ImportResult(Parser.ParseDataType.Customer);
        if (customerInput != null) {
            ParseResult customerParseResult = parser.parse(customerInput, new CustomerParser(memberService, doctorService), shop);
            customerImportResult = importCustomers(customerParseResult);
        }

        ImportResult promotionResult = new ImportResult(Parser.ParseDataType.Promotion);
        if (promotionFile != null) {
            ParseResult parseResult = parser.parse(promotionFile, new PromotionParser(), shop);
            promotionResult = importPromotion(parseResult);
        }

        //finished import cycle, print results
        printImportResult(vendorImportResult, doctorImportResult, productImportResult, customerImportResult, promotionResult);
    }

    @Override
    public Response parseBatchImport(InputStream productsList) {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Blaze", "Shop does not exist.");
        }

        Parser parser = new Parser();
        ParseResult parse = null;
        if (productsList != null) {
            parse = parser.parse(productsList,
                    new ProductBatchParserImport(productService, inventoryRepository, productBatchRepository,
                            inventoryService, batchQuantityService, token, productChangeLogRepository, productRepository, realtimeService, barcodeService, metrcAccountService, categoryRepository, backgroundJobService, queuedTransactionRepository, shopRepository, batchActivityLogRepository, barcodeItemRepository), shop);

        }

        return Response.ok().entity(parse).build();
    }

    @Override
    public Response importMeadowProducts(InputStream inputStream) {
        meadowParser.importMeadowProducts(token.getCompanyId(),token.getShopId(),token.getActiveTopUser().getUserId(),inputStream);
        return Response.ok().build();
    }

    @Override
    public Response importProductBatch(List<ProductBatchResult> productBatchResult) {
        ProductBatchParserImport productBatchParserImport = new ProductBatchParserImport(productService, inventoryRepository, productBatchRepository,
                inventoryService, batchQuantityService, token, productChangeLogRepository, productRepository, realtimeService, barcodeService, metrcAccountService, categoryRepository, backgroundJobService, queuedTransactionRepository, shopRepository, batchActivityLogRepository, barcodeItemRepository);

        productBatchParserImport.importProductBatch(productBatchResult);

        return Response.ok().build();
    }


    @Override
    public Response assignUniqueIds(InputStream tagFileInput) {
        if (tagFileInput == null) {
            throw new BlazeInvalidArgException("TagFile", "Tag file not found.");
        }
        CSVParser csvParser = createCSVParser(tagFileInput);
        List<String> errors = new ArrayList<>();
        if (csvParser != null) {
            HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAllAsMap(token.getCompanyId(),token.getShopId());
            if (productBatchHashMap.size() == 0) {
                return Response.ok().build();
            }


            Iterator<CSVRecord> csvIter = csvParser.iterator();

            //List<ComplianceBatchPackagePair> packageTags = new ArrayList<>();
            int updated = 0;
            int fails = 0;
            int skips = 0;
            long date = DateTime.now().getMillis();
            LOG.info("Date: " + date);
            while (csvIter.hasNext()) {
                CSVRecord csvRecord = csvIter.next();
                String tag = csvRecord.get("Unique #");
                String batchId = csvRecord.get("Internal ID");
                if (StringUtils.isNotBlank(tag) && StringUtils.isNotBlank(batchId)) {
                    tag = tag.trim();
                    batchId = batchId.trim();
                    //ComplianceBatchPackagePair batchPair = new ComplianceBatchPackagePair();
                    //batchPair.setBatchId(batchId);
                    //batchPair.setTag(tag);

                    //.add(batchPair);
                    ProductBatch batch = productBatchHashMap.get(batchId);
                    if (batch != null && !batch.getSku().equalsIgnoreCase(tag)) {
                        // if tag doesn't equal, then let's update
                        // Update sku if needed
                        try {
                            BarcodeItem batchItem = barcodeService.createBarcodeItemIfNeeded(batch.getCompanyId(), batch.getShopId(), batch.getProductId(),
                                    BarcodeItem.BarcodeEntityType.Batch,
                                    batch.getId(), tag, null, false);
                            if (batchItem != null) {
                                batch.setSku(batchItem.getBarcode());
                                batch.setBatchNo(batchItem.getNumber());
                            }
                            productBatchRepository.updateProductBatchSku(token.getCompanyId(),token.getShopId(),batch.getId(),tag);
                            updated++;
                            LOG.info("Updated: " + batch.getProductId() + " - BatchId: " + batchId + " - " + tag);
                        } catch (Exception e) {
                            errors.add(batchId + " : " + tag);
                            fails++;
                        }
                    } else {
                        skips++;
                    }
                }
            }
            LOG.info("Updated: " + updated + ",  fails: " + fails + ", skis: " + skips);

        }
        return Response.ok(errors).build();
    }

    private CSVParser createCSVParser(InputStream inputStream) throws ImportException {
        CSVParser parser = null;
        try {
            Reader in = new InputStreamReader(inputStream);
            CSVFormat format = CSVFormat.EXCEL.withHeader().withSkipHeaderRecord(true);
            parser = new CSVParser(in, format);
        } catch (IOException e) {
            LOG.error(e);
            throw new BlazeInvalidArgException("Parser", "Could not create CSV Parser.");
        }
        return parser;
    }

    private HashMap<String, Importable> buildDbImportIdSet(Parser.ParseDataType dataType) {
        HashMap<String, Importable> idSet = new HashMap<>();

        switch (dataType) {
            case Vendor:
                //get all vendors for company in the DB and add their ID's to the hashset
                SearchResult<Vendor> dbVendors = vendorRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
                if (dbVendors.getTotal() > 0) {
                    List<Vendor> l = dbVendors.getValues();
                    for (Vendor v : l) {
                        idSet.put(v.getVendorKey(), v);
                    }
                }
                return idSet;

            case Doctor:
                //get all doctors for company in the DB and add their ID's to the hashset
                SearchResult<Doctor> dbDoctors = doctorRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
                if (dbDoctors.getTotal() > 0) {
                    for (Doctor d : dbDoctors.getValues()) {
                        if (StringUtils.isNotBlank(d.getImportId())) {
                            idSet.put(d.getImportId(), d);
                        }
                    }
                }
                return idSet;

            case Product:
                //get all products for a company
                SearchResult<Product> pResults = productRepository.findItems(token.getCompanyId(), token.getShopId(), 0, Integer.MAX_VALUE);


                if (!pResults.getValues().isEmpty()) {
                    for (Product p : pResults.getValues()) {
                        if (StringUtils.isNotBlank(p.getImportId())) {
                            idSet.put(p.getImportId(), p);
                        } else {
                            idSet.put(p.getSku(),p);
                        }
                    }
                }
                return idSet;

            case Customer:
                //get all customers for a company
                SearchResult<Member> dbMemberships = memberRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
                if (dbMemberships.getTotal() != 0) {
                    List<Member> l = dbMemberships.getValues();
                    for (Member m : l) {
                        if (StringUtils.isNotBlank(m.getImportId())) {
                            idSet.put(m.getImportId(), m);
                        } else {
                            String importId = m.getFirstName() + "_" + m.getLastName() + "_" + m.getDob();
                            idSet.put(importId, m);
                        }
                    }
                }
                return idSet;
        }

        return null;
    }

    private HashMap<String, CompanyAsset> importCompanyAssets(InputStream zipStream) {
        //ZipFile zipFile = new ZipFile(file);
        HashMap<String, CompanyAsset> assets = new HashMap<>();
        ZipInputStream zipFile = new ZipInputStream(zipStream);
        try {
            ZipEntry entry = null;

            // Get Assets
            SearchResult<CompanyAsset> oldAssets = companyAssetRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
            HashMap<String, CompanyAsset> assetHashMap = new HashMap<>();
            for (CompanyAsset asset : oldAssets.getValues()) {
                assetHashMap.put(asset.getKey(), asset);
            }

            while ((entry = zipFile.getNextEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                String name = entry.getName();
                name = FilenameUtils.getName(name);
                String fileNameWithOutExt = FilenameUtils.removeExtension(name);
                String extension = FilenameUtils.getExtension(name);

                if (StringUtils.isBlank(fileNameWithOutExt)) {
                    continue;
                }

                if (name.startsWith("._")) {
                    continue;
                }
                String keyName = token.getCompanyId() + "-" + name;
                if (assetHashMap.containsKey(keyName)) {
                    // avoid re-uploaded
                    assets.put(keyName, assetHashMap.get(keyName));
                } else {
                    keyName = token.getCompanyId() + "-" + fileNameWithOutExt;
                    CompanyAsset companyAsset = assetService.uploadAssetPublic(zipFile, name, Asset.AssetType.Photo, null, keyName);
                    if (companyAsset != null) {
                        assets.put(companyAsset.getKey(), companyAsset);
                        LOG.info("Imported photo: " + companyAsset.getKey());
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                zipFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return assets;
    }

    private ImportResult importVendors(ParseResult<Vendor> vendorParseResult) {
        ImportResult vendorImportResult = new ImportResult(Parser.ParseDataType.Vendor);

        HashMap<String, Importable> dbVendorIds = buildDbImportIdSet(Parser.ParseDataType.Vendor);

        //Import Vendors
        HashSet<String> importedVendors = new HashSet<>();
        ArrayList<Vendor> vendorList = new ArrayList<>();
        ArrayList<Vendor> updateProductList = new ArrayList<>();
        for (Vendor v : vendorParseResult.getSuccessfulImports()) {

            if (importedVendors.contains(v.getVendorKey())) { //ID already added this import cycle, skip
                vendorImportResult.addFailed(v, "Skipped - Duplicate ID in Vendor CSV file");

            } else if (dbVendorIds != null && dbVendorIds.containsKey(v.getVendorKey())) { //ID already in db, skip
                vendorImportResult.addFailed(v, "Skipped - Vendor ID already exists in DB");

                Vendor oldVendor = (Vendor) dbVendorIds.get(v.getVendorKey());
                if (oldVendor != null) {
                    oldVendor.setImportId(v.getImportId());
                    if (StringUtils.isBlank(oldVendor.getEmail())) {
                        oldVendor.setEmail(v.getEmail());
                    }
                    if (StringUtils.isBlank(oldVendor.getFirstName())) {
                        oldVendor.setEmail(v.getFirstName());
                    }

                    if (StringUtils.isBlank(oldVendor.getLastName())) {
                        oldVendor.setLastName(v.getLastName());
                    }

                    if (StringUtils.isBlank(oldVendor.getPhone())) {
                        oldVendor.setPhone(v.getPhone());
                    }

                    updateProductList.add(oldVendor);
                }
            } else { //not a duplicate, validate request through service and add to batch add list
                try {
                    v.setCreatedBy(token.getActiveTopUser().getUserId());
                    vendorList.add(v);
                    importedVendors.add(v.getVendorKey());
                    vendorImportResult.addSuccess(v);
                } catch (BlazeInvalidArgException e) {
                    vendorImportResult.addFailed(v, e.getMessage());
                }
            }
        }
        //ProductBatch add the list to the DB
        if (!vendorList.isEmpty()) {
            vendorRepository.save(vendorList);
        }
        for (Vendor vendor : updateProductList) {
            vendorRepository.update(vendor.getCompanyId(), vendor.getId(), vendor);
            vendorImportResult.addUpdated(vendor);
        }
        return vendorImportResult;
    }

    private ImportResult importDoctors(ParseResult<Doctor> doctorParseResult) {
        ImportResult doctorImportResult = new ImportResult(Parser.ParseDataType.Doctor);
        HashMap<String, Importable> dbDoctorIds = buildDbImportIdSet(Parser.ParseDataType.Doctor);
        //import doctors
        HashSet<String> importedDoctors = new HashSet<>();
        ArrayList<Doctor> doctorList = new ArrayList<>();
        for (Doctor d : doctorParseResult.getSuccessfulImports()) {
            if (importedDoctors.contains(d.getImportId())) { //ID already added this import cycle, skip
                doctorImportResult.addFailed(d, "Skipped - Duplicate ID in doctor CSV file");

            } else if (dbDoctorIds != null && dbDoctorIds.containsKey(d.getImportId())) { //ID already in db, skip
                doctorImportResult.addFailed(d, "Skipped - doctor ID already exists in DB");

            } else { //valid request, not a duplicate, add to DB
                try {
                    doctorList.add(d);
                    importedDoctors.add(d.getImportId());
                    doctorImportResult.addSuccess(d);
                } catch (BlazeInvalidArgException e) {
                    doctorImportResult.addFailed(d, e.getMessage());
                }
            }
        }
        //ProductBatch add the list to the DB
        if (!doctorList.isEmpty()) {
            doctorRepository.save(doctorList);
        }
        return doctorImportResult;
    }

    private ImportResult importProducts(Shop shop,ParseResult<ProductContainer> productParseResult) {
        ImportResult productImportResult = new ImportResult(Parser.ParseDataType.Product);
        HashMap<String, Importable> dbProductIds = buildDbImportIdSet(Parser.ParseDataType.Product);

        // Get Assets
        SearchResult<CompanyAsset> assets = companyAssetRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
        HashMap<String, CompanyAsset> assetHashMap = new HashMap<>();
        for (CompanyAsset asset : assets.getValues()) {
            assetHashMap.put(asset.getKey(), asset);
        }
        HashMap<String, Importable> productIdsToProducts = new HashMap<>();
        for (Importable importable : dbProductIds.values()) {
            Product product = (Product) importable;
            productIdsToProducts.put(product.getId(), product);
        }


        HashMap<String, ProductCategory> dbCategoryIds = new HashMap<>();
        Iterable<ProductCategory> categoryList = categoryRepository.listByShop(token.getCompanyId(), token.getShopId());
        //Build map of categories and their ID's
        for (ProductCategory pc : categoryList) {
            dbCategoryIds.put(pc.getName().toLowerCase(), pc);
        }
        //Import products
        HashMap<String, Product> importedProducts = new HashMap<>();
        ArrayList<Product> productList = new ArrayList<>();
        ArrayList<Product> updateProductList = new ArrayList<>();

        Inventory safe = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        Iterable<Inventory> inventories = inventoryRepository.listAllByShop(token.getCompanyId(), token.getShopId());
        HashMap<String, Inventory> inventoryHashMap = new HashMap<>();
        for (Inventory inventory : inventories) {
            inventoryHashMap.put(inventory.getName(), inventory);
        }

        ArrayList<ProductContainer> successfulContainers = new ArrayList<>();
        Iterable<Brand> brands = brandRepository.list(token.getCompanyId());
        HashMap<String,Brand> brandHashMap = new HashMap<>();
        for (Brand b : brands) {
            brandHashMap.put(b.getName(),b);
        }

        HashMap<String,Vendor> vendorHashMap = vendorRepository.listAsMap(token.getCompanyId());

        LinkedHashSet<String> newProductTags = new LinkedHashSet<>();
        // Create all categories
        int size = productParseResult.getSuccessfulImports().size();
        int index = 0;
        for (ProductContainer container : productParseResult.getSuccessfulImports()) {
            Product p = container.getProduct();
            String categoryName = container.getCategoryName().trim().toLowerCase();
            if (StringUtils.isNotBlank(p.getSku())) {
                p.setImportId(p.getSku().trim());
            } else {
                p.setImportId(String.format("%s_%s_%s_%s_%s", p.getName(), categoryName, p.getFlowerType(), container.getBrand(), p.getVendorId()));
            }
            if (p.getTags() != null) {
                newProductTags.addAll(p.getTags());
            }

            if (StringUtils.isNotBlank(container.getProductId())) {
                productImportResult.addFailed(p, "Skipped - Product ID already exists in DB");

                Product oldProduct = (Product) productIdsToProducts.get(container.getProductId());
                if (oldProduct != null) {
                    //oldProduct.setPriceBreaks(p.getPriceBreaks());
                   // oldProduct.setPriceRanges(p.getPriceRanges());
                    //oldProduct.setUnitPrice(p.getUnitPrice());

                    //Set assets
                    if (StringUtils.isNotBlank(container.getImage1())) {
                        oldProduct = setProductAssets(container, oldProduct);
                        updateProductList.add(oldProduct);
                        if (oldProduct.getAssets().size() > 0) {
                            LOG.info(String.format("Updating product images: %d of %d",index,size));
                            productRepository.updateCachedAssets(token.getCompanyId(),oldProduct.getId(),oldProduct.getAssets());
                        }
                    }
                }
            } else if (importedProducts.containsKey(p.getImportId())) { //ID already added this import cycle, skip
                productImportResult.addFailed(p, "Skipped - Duplicate ID in Product CSV file");
            } else if (dbProductIds != null && dbProductIds.containsKey(p.getImportId())) { //ID already in db, skip
                productImportResult.addFailed(p, "Skipped - Product ID already exists in DB");

                Product oldProduct = (Product) dbProductIds.get(p.getImportId());
                if (oldProduct != null) {
                    oldProduct.setVendorId(p.getVendorId());
                    oldProduct.setName(p.getName());
                    oldProduct.setPriceBreaks(p.getPriceBreaks());
                    oldProduct.setPriceRanges(p.getPriceRanges());
                    oldProduct.setUnitPrice(p.getUnitPrice());

                    updateProductList.add(oldProduct);

                    //Set assets
                    oldProduct = setProductAssets(container, oldProduct);
                }
            } else { //valid request, not a duplicate, add to DB
                try {
                    //handle category matching or creation here.
                    if (container.getCategoryName() != null && dbCategoryIds.containsKey(container.getCategoryName().trim().toLowerCase())) {
                        ProductCategory productCategory = dbCategoryIds.get(container.getCategoryName().trim().toLowerCase());

                        p.setCategoryId(productCategory.getId());
                        p.setCategory(productCategory);

                    } else {
                        System.out.println("New category created: " + container.getCategoryName());
                        ProductCategory category = new ProductCategory();
                        category.setName(container.getCategoryName());
                        category.setCompanyId(token.getCompanyId());
                        category.setShopId(token.getShopId());
                        category.setUnitType(ProductCategory.UnitType.toUnitType(container.getUnitType()));
                        //save and get id, add to map and
                        String catId = categoryRepository.save(category).getId();
                        p.setCategoryId(catId);
                        dbCategoryIds.put(category.getName().toLowerCase(), category);

                        p.setCategory(category);
                    }

                    // Set assets
                    p = setProductAssets(container, p);

                    if (container.getInventoryQuantities().size() > 0) {
                        for (String iName : container.getInventoryQuantities().keySet()) {
                            BigDecimal value = container.getInventoryQuantities().get(iName);
                            Inventory inventory = inventoryHashMap.get(iName);

                            if (inventory == null) {
                                inventory = new Inventory();
                                inventory.prepare(token.getCompanyId());
                                inventory.setShopId(token.getShopId());
                                inventory.setActive(true);
                                inventory.setName(iName);
                                inventoryRepository.save(inventory);
                                inventoryHashMap.put(iName, inventory);
                            }

                            if (inventory != null && value != null && value.doubleValue() > 0) {

                                ProductQuantity quantity = new ProductQuantity();
                                quantity.setId(ObjectId.get().toString());
                                quantity.setCompanyId(token.getCompanyId());
                                quantity.setShopId(token.getShopId());
                                quantity.setInventoryId(inventory.getId());
                                quantity.setQuantity(value);

                                p.getQuantities().add(quantity);
                            }
                        }
                    } else {
                        ProductQuantity quantity = new ProductQuantity();
                        quantity.setId(ObjectId.get().toString());
                        quantity.setCompanyId(token.getCompanyId());
                        quantity.setShopId(token.getShopId());
                        quantity.setInventoryId(safe.getId());
                        quantity.setQuantity(container.getInventoryAvailable());

                        p.getQuantities().add(quantity);
                    }


                    p.setThc((double) container.getPercentTHC());
                    p.setCbn((double) container.getPercentCBN());
                    p.setCbd((double) container.getPercentCBD());


                    // Deal with brands
                    if (StringUtils.isNotBlank(container.getBrand())) {
                        Brand brand = brandHashMap.getOrDefault(container.getBrand(),null);
                        if (brand == null) {
                            brand = new Brand();
                            brand.prepare(token.getCompanyId());
                            brand.setActive(true);
                            brand.setName(container.getBrand());
                            brandRepository.save(brand); // create a new brand
                            brandHashMap.put(brand.getName(),brand);
                        }
                        p.setBrandId(brand.getId());


                        // deal with vendor
                        Vendor vendor = vendorHashMap.get(p.getVendorId());
                        if (vendor != null) {
                            LinkedHashSet<String> brandIds = vendor.getBrands();
                            if (brandIds == null) {
                                brandIds = new LinkedHashSet<>();
                                vendor.setBrands(brandIds);
                            }
                            if (!brandIds.contains(brand.getId())) {
                                vendor.setBrands(brandIds);
                                brandIds.add(brand.getId());

                                vendorRepository.update(token.getCompanyId(),vendor.getId(),vendor);
                            }
                        }
                    }



                    successfulContainers.add(container);
                    importedProducts.put(p.getImportId(), p);
                    productImportResult.addSuccess(p);
                    productList.add(p);
                } catch (BlazeInvalidArgException e) {
                    productImportResult.addFailed(p, e.getMessage());
                }
            }
            index++;
        }


        if (!productList.isEmpty()) {
            List<Product> savedProducts = productRepository.save(productList);
        }

        // Now Create batch
        List<ProductBatch> productBatches = new ArrayList<>();
        List<BarcodeItem> barcodes = new ArrayList<>();
        List<BatchQuantity> batchQuantities = new ArrayList<>();
        for (ProductContainer container : successfulContainers) {
            Product product = importedProducts.get(container.getProduct().getImportId());
            if (product == null) continue;

            if (container.getInventoryQuantities().size() > 0) {
                double totalQty = 0;
                for (BigDecimal v : container.getInventoryQuantities().values()) {
                    totalQty += v.doubleValue();
                }
                if (totalQty > 0) {
                    BigDecimal costPerUnit = container.getCostPerUnit() != null ? container.getCostPerUnit() : new BigDecimal(0);
                    ProductBatch productBatch = new ProductBatch();
                    productBatch.prepare(token.getCompanyId());
                    productBatch.setProductId(product.getId());
                    productBatch.setShopId(token.getShopId());
                    productBatch.setSku(product.getSku());
                    productBatch.setVendorId(product.getVendorId());
                    productBatch.setQuantity(new BigDecimal(totalQty));
                    productBatch.setThc((double) container.getPercentTHC());
                    productBatch.setCbn((double) container.getPercentCBN());
                    productBatch.setCbd((double) container.getPercentCBD());
                    productBatch.setCost(costPerUnit.multiply(productBatch.getQuantity()));
                    productBatch.setCostPerUnit(costPerUnit);
                    productBatch.setPurchasedDate(container.getDatePurchased());
                    productBatches.add(productBatch);


                    BarcodeItem barcodeItem = new BarcodeItem();
                    barcodeItem.prepare(token.getCompanyId());
                    barcodeItem.setCompanyId(token.getCompanyId());
                    barcodeItem.setShopId(token.getShopId());
                    barcodeItem.setProductId(product.getId());
                    barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Batch);
                    barcodeItem.setEntityId(productBatch.getId());
                    barcodeItem.setBarcode(product.getSku());
                    barcodes.add(barcodeItem);

                    for (String iName : container.getInventoryQuantities().keySet()) {
                        BigDecimal value = container.getInventoryQuantities().get(iName);
                        Inventory inventory = inventoryHashMap.get(iName);

                        if (inventory != null && value != null && value.doubleValue() > 0) {
                            // batch doesn't exist so let's just add a new batch quantity
                            BatchQuantity dbBatchQuantity = new BatchQuantity();
                            dbBatchQuantity.prepare(token.getCompanyId());
                            dbBatchQuantity.setShopId(token.getShopId());
                            dbBatchQuantity.setBatchId(productBatch.getId());
                            dbBatchQuantity.setProductId(product.getId());
                            dbBatchQuantity.setQuantity(value);
                            dbBatchQuantity.setInventoryId(inventory.getId());
                            dbBatchQuantity.setBatchPurchaseDate(container.getDatePurchased());
                            batchQuantities.add(dbBatchQuantity);

                        }
                    }
                }

            } else if (container.getInventoryAvailable().doubleValue() > 0) {
                BigDecimal costPerUnit = container.getCostPerUnit() != null ? container.getCostPerUnit() : new BigDecimal(0);
                ProductBatch productBatch = new ProductBatch();
                productBatch.prepare(token.getCompanyId());
                productBatch.setProductId(product.getId());
                productBatch.setShopId(token.getShopId());
                productBatch.setSku(product.getSku());
                productBatch.setVendorId(product.getVendorId());
                productBatch.setQuantity(container.getInventoryAvailable());
                productBatch.setThc((double) container.getPercentTHC());
                productBatch.setCbn((double) container.getPercentCBN());
                productBatch.setCbd((double) container.getPercentCBD());
                productBatch.setCost(costPerUnit.multiply(productBatch.getQuantity()));
                productBatch.setCostPerUnit(costPerUnit);
                productBatch.setPurchasedDate(container.getDatePurchased());
                productBatches.add(productBatch);


                // batch doesn't exist so let's just add a new batch quantity
                BatchQuantity dbBatchQuantity = new BatchQuantity();
                dbBatchQuantity.prepare(token.getCompanyId());
                dbBatchQuantity.setShopId(token.getShopId());
                dbBatchQuantity.setBatchId(productBatch.getId());
                dbBatchQuantity.setProductId(product.getId());
                dbBatchQuantity.setQuantity(container.getInventoryAvailable());
                dbBatchQuantity.setInventoryId(safe.getId());
                dbBatchQuantity.setBatchPurchaseDate(container.getDatePurchased());
                batchQuantities.add(dbBatchQuantity);


                BarcodeItem barcodeItem = new BarcodeItem();
                barcodeItem.prepare(token.getCompanyId());
                barcodeItem.setCompanyId(token.getCompanyId());
                barcodeItem.setShopId(token.getShopId());
                barcodeItem.setProductId(product.getId());
                barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Batch);
                barcodeItem.setEntityId(productBatch.getId());
                barcodeItem.setBarcode(product.getSku());
                barcodes.add(barcodeItem);
            }

            BarcodeItem barcodeItem = new BarcodeItem();
            barcodeItem.prepare(token.getCompanyId());
            barcodeItem.setCompanyId(token.getCompanyId());
            barcodeItem.setShopId(token.getShopId());
            barcodeItem.setProductId(product.getId());
            barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Product);
            barcodeItem.setEntityId(product.getId());
            barcodeItem.setBarcode(product.getSku());

            barcodes.add(barcodeItem);


        }

        productBatchRepository.save(productBatches);
        barcodeItemRepository.save(barcodes);
        batchQuantityRepository.save(batchQuantities);

        if (newProductTags.size() > 0) {
            LinkedHashSet<String> shopTags = shop.getProductsTag() != null ? shop.getProductsTag() : new LinkedHashSet<>();
            shopTags.addAll(newProductTags);
            shop.setProductsTag(shopTags);
            //shopRepository.updateProductTags(token.getCompanyId(),token.getShopId(),shopTags);
        }

        // Do any updates
        for (Product updatedProduct : updateProductList) {
            productRepository.update(updatedProduct.getCompanyId(), updatedProduct.getId(), updatedProduct);
            productImportResult.addUpdated(updatedProduct);
        }

        taskManager.takeInventorySnapshot(token.getCompanyId(), token.getShopId());


        return productImportResult;
    }

    private ImportResult importCustomers(ParseResult<CustomerContainer> customerParseResult) {
        //collect lists of parsed data
        ImportResult customerImportResult = new ImportResult(Parser.ParseDataType.Customer);
        //Hashsets to keep track of ID's while importing to avoid dupes
        HashMap<String, Importable> dbMembershipIds = buildDbImportIdSet(Parser.ParseDataType.Customer);

        HashSet<String> importedCustomers = new HashSet<>();
        ArrayList<Member> memberList = new ArrayList<>();
        MemberGroup defaultMemberGroup = memberGroupRepository.getDefaultMemberGroup(token.getCompanyId());
        Iterable<MemberGroup> memberGroups = memberGroupRepository.list(token.getCompanyId());
        HashMap<String, MemberGroup> memberGroupHashMap = new HashMap<>();
        // member group by name
        for (MemberGroup group : memberGroups) {
            memberGroupHashMap.put(group.getName().toLowerCase().trim(), group);
        }

        for (CustomerContainer container : customerParseResult.getSuccessfulImports()) {
            Member member = container.getMember();

            MemberGroup targetGroup = defaultMemberGroup;
            if (StringUtils.isNotBlank(container.getPatientGroup())) {
                // check if memberGroup exists
                targetGroup = memberGroupHashMap.get(container.getPatientGroup().toLowerCase().trim());
                if (targetGroup == null) {
                    // let's add this group

                    MemberGroup memberGroup = new MemberGroup();
                    memberGroup.prepare(token.getCompanyId());
                    memberGroup.setShopId(token.getShopId());
                    memberGroup.setActive(true);
                    memberGroup.setName(container.getPatientGroup());
                    memberGroup.setDiscount(new BigDecimal(0));
                    memberGroup.setDefaultGroup(false);
                    memberGroup.setDiscountType(OrderItem.DiscountType.Percentage);
                    memberGroup = memberGroupRepository.save(memberGroup);
                    targetGroup = memberGroup;
                    memberGroupHashMap.put(container.getPatientGroup().toLowerCase().trim(), targetGroup);
                }
            }


            if (importedCustomers.contains(member.getImportId())) { //ID already added this import cycle, skip
                customerImportResult.addFailed(member, "Skipped - Duplicate ID in BaseMember CSV file");
            } else if (dbMembershipIds != null && dbMembershipIds.containsKey(member.getImportId())) { //ID already in db, skip
                customerImportResult.addFailed(member, "Skipped - Member ID already exists in DB");

                try {
                    Member dbMember = (Member) dbMembershipIds.get(member.getImportId());
                    //dbMember.setMemberGroupId(targetGroup.getId());
                    //dbMember.setMemberGroup(targetGroup);
                    dbMember.setDob(member.getDob());
                    //dbMember.setLoyaltyPoints(member.getLoyaltyPoints());
                    //dbMember.setLifetimePoints(member.getLifetimePoints());
                    dbMember.setDob(member.getDob());
                    dbMember.setStartDate(member.getStartDate());
                    if (dbMember.getRecommendations() != null && dbMember.getRecommendations().size() > 0) {
                        if (member.getRecommendations() != null && member.getRecommendations().size() > 0) {
                            Recommendation oldRec = dbMember.getRecommendations().get(0);
                            Recommendation newRec = member.getRecommendations().get(0);

                            if (StringUtils.isBlank(oldRec.getRecommendationNumber())
                                    || (oldRec.getRecommendationNumber().equalsIgnoreCase(newRec.getRecommendationNumber()))) {
                                oldRec.setRecommendationNumber(newRec.getRecommendationNumber());
                                oldRec.setExpirationDate(newRec.getExpirationDate());
                                oldRec.setIssueDate(newRec.getIssueDate());
                            }
                        }
                    } else {
                        if (member.getRecommendations() != null && member.getRecommendations().size() > 0) {
                            dbMember.setRecommendations(member.getRecommendations());
                        }
                    }

                    if (dbMember.getIdentifications() != null && dbMember.getIdentifications().size() > 0) {
                        if (member.getIdentifications() != null && member.getIdentifications().size() > 0) {
                            Identification oldID = dbMember.getIdentifications().get(0);
                            Identification newID = member.getIdentifications().get(0);

                            if (StringUtils.isBlank(oldID.getLicenseNumber())
                                    || (oldID.getLicenseNumber().equalsIgnoreCase(newID.getLicenseNumber()))) {
                                oldID.setLicenseNumber(newID.getLicenseNumber());
                                oldID.setExpirationDate(newID.getExpirationDate());
                            }
                        }
                    } else {
                        if (member.getIdentifications() != null && member.getIdentifications().size() > 0) {
                            dbMember.setIdentifications(member.getIdentifications());
                        }
                    }

                    memberRepository.update(dbMember.getCompanyId(), dbMember.getId(), dbMember);
                } catch (Exception e) {
                    // Ignore
                    LOG.error("Error updating member", e);

                }

            } else if (StringUtils.isBlank(member.getFirstName()) || StringUtils.isBlank(member.getLastName())) {
                customerImportResult.addFailed(member, "Skipped - Member missing first or last name. ");
            } else { //valid request, not a duplicate, add to DB
                member.setMemberGroupId(targetGroup.getId());
                member.setMemberGroup(targetGroup);
                try {
                    if (member.getIdentifications() == null) {
                        System.out.println("Null DL");
                    }
                    if (member.getIdentifications().size() == 0) {
                        member.setStatus(Member.MembershipStatus.Pending);
                    }

                    if (StringUtils.isNotBlank(container.getMemberNote())) {
                        Note note = new Note();
                        note.prepare();
                        note.setWriterId(token.getActiveTopUser().getUserId());
                        note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                        note.setMessage(container.getMemberNote());

                        List<Note> notes = new ArrayList<>();
                        notes.add(note);
                        member.setNotes(notes);
                    }
                    if (member.isMedical()) {
                        member.setConsumerType(ConsumerType.MedicinalThirdParty);
                    }


                    member.setImportId(member.getImportId());
                    memberList.add(member);
                    importedCustomers.add(member.getImportId());
                    member.setStartDate(container.getDateJoined());
                    customerImportResult.addSuccess(member);
                } catch (BlazeInvalidArgException e) {
                    customerImportResult.addFailed(member, e.getMessage());
                }
            }
        }
        //Save member
        if (!memberList.isEmpty()) {
            memberRepository.save(memberList);
        }

        return customerImportResult;
    }


    private ImportResult importPromotion(ParseResult<Promotion> parseResult) {
        Iterable<Promotion> promotions = promotionRepository.getPromotions(token.getCompanyId(), token.getShopId());

        Set<String> promoNames = new HashSet<>();
        Set<String> promoCodes = new HashSet<>();

        for (Promotion promotion : promotions) {
            promoNames.add(promotion.getName());
            promoCodes.addAll(promotion.getPromoCodes());
        }

        List<Promotion> promoList = new ArrayList<>();
        for (Promotion promotion : parseResult.getSuccessfulImports()) {
            if (promoNames.contains(promotion.getName())) {
                parseResult.addFailed(promotion, String.format("Promotion already exists with name : %s", promotion.getName()));
                continue;
            }

            if (!Collections.disjoint(promoCodes, promotion.getPromoCodes())) {
                parseResult.addFailed(promotion, String.format("Promotion already exists with promo codes : %s", promotion.getPromoCodes()));
                continue;
            }

            promotion.prepare(token.getCompanyId());
            promotion.setShopId(token.getShopId());
            promotion.setSun(true);
            promotion.setMon(true);
            promotion.setTues(true);
            promotion.setWed(true);
            promotion.setThur(true);
            promotion.setFri(true);
            promotion.setSat(true);
            promotion.setActive(true);
            promotion.setPromoSource(Promotion.PromoSource.Manual);

            if (promotion.getStartDate() == null || promotion.getStartDate() == 0) {
                promotion.setStartDate(DateTime.now().getMillis());
            }

            if (promotion.getLimitPerCustomer() != 0) {
                promotion.setEnableLimitPerCustomer(true);
            }

            promoList.add(promotion);
        }

        if (!promoList.isEmpty()) {
            promotionRepository.save(promoList);
        }
        return parseResult;
    }

    private void printImportResult(ImportResult... results) {
        StringBuilder sb = new StringBuilder();
        for (ImportResult r : results) {
            if (r == null) {
                continue;
            }
            sb.append("\nResults for " + r.getDataType().getTitle() + ":");
            sb.append("\nSuccesses: " + r.getSuccesses() + " Failures: " + r.getFailures() + " Updates: " + r.getUpdates());
            sb.append("\n-----------------------------------------------------------------");

            List<FailedImport> failedImports = r.getFailedImports();
            for (FailedImport fi : failedImports) {
                sb.append("\nFailed to import entity with ID '" + fi.getEntity().getImportId() + "' - " + fi.getReason());
            }
            sb.append("\n\n\n");
        }
        LOG.info("\n" + sb.toString());
    }

    private Product setProductAssets(ProductContainer container, Product product) {
        product.getAssets().clear();
        for (int i = 1; i <= 5; i++) {
            String imageUrl = null;
            switch (i) {
                case 1:
                    imageUrl = container.getImage1();
                    break;
                case 2:
                    imageUrl = container.getImage2();
                    break;
                case 3:
                    imageUrl = container.getImage3();
                    break;
                case 4:
                    imageUrl = container.getImage4();
                    break;
                case 5:
                    imageUrl = container.getImage5();
                    break;
            }
            if (StringUtils.isNotBlank(imageUrl)) {
                try {
                    URL publicURL = new URL(imageUrl);
                    String name = FilenameUtils.getBaseName(publicURL.getPath());
                    Date date = new Date();
                    long time = date.getTime();
                    Timestamp ts = new Timestamp(time);
                    String keyname = token.getCompanyId() + "-" + name + "-" + i + "-" + time;
                    CompanyAsset companyAsset = assetService.uploadAssetPublicFromUrl(imageUrl, Asset.AssetType.Photo, keyname);
                    if (companyAsset != null) {
                        LOG.info("Uploaded: " + companyAsset.getPublicURL());
                        product.getAssets().add(companyAsset);
                    }
                } catch (Exception e) {
                    LOG.error("error retrieving image", e);
                }
            }
        }
        return product;
    }
}
