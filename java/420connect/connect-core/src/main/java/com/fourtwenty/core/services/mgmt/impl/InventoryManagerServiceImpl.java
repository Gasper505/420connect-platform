package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.services.mgmt.InventoryManagerService;
import com.google.inject.Inject;

import java.math.BigDecimal;

/**
 * Created by mdo on 10/15/17.
 * <p>
 * Non-authenticated Service. Must pass in companyId and shopId
 */
public class InventoryManagerServiceImpl implements InventoryManagerService {
    @Inject
    BatchQuantityRepository batchQuantityRepository;

    @Override
    public BigDecimal addQuantityToBatch(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToAdd) {

        return null;
    }

    @Override
    public BigDecimal getCurrentQuantityForProduct(String companyId, String shopId, String productId) {
        return null;
    }

    @Override
    public BigDecimal getCurrentQuantityForBatch(String companyId, String shopId, String productId, String batchId) {
        return null;
    }

    @Override
    public BigDecimal getCurrentQuantityForInventory(String companyId, String shopId, String productId, String inventoryId) {
        return null;
    }

    @Override
    public BigDecimal getCurrentQuantityForBatchNInventory(String companyId, String shopId, String productId, String inventoryId, String batchId) {
        return null;
    }

    @Override
    public BigDecimal deductQuantityFromBatch(String companyId, String shopId, String productId, String inventoryId, String batchId, BigDecimal quantityToDeduct) {
        return null;
    }
}
