package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.batches.MetrcVerifyCreatePackage;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesEvent;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesResult;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.*;
import com.fourtwenty.core.security.tokens.AssetAccessToken;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.QrCodeUtil;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 10/26/15.
 */
public class InventoryServiceImpl extends AbstractAuthServiceImpl implements InventoryService {
    private static final int MAX_INVENTORY_PER_SHOP = 11; // 9 custom, 1 safe, 1 exchange
    private static final String PRODUCT_NOT_EXIST = "Product does not exist.";
    private static final String BATCH_NOT_EXIST = "Product batch does not exist.";
    private static final String PRODUCT = "Product";
    private static final String PRODUCT_BATCH = "Product Batch";
    private static final String BATCH_NOT_BELONG = "Product Batch does not belongs to provided product";
    private static final String RECONCILIATION = "Reconciliation";
    private static final String METRC_NOT_FOUND = "Metrc Package not found";
    private static final String RECONCILIATION_LOSS_NOT_ALLOWED = "Positive LOSS reconciliation is not allowed.";
    private static final String RECONCILIATION_NOT_ALLOWED = "Positive reconciliation is not allowed.";
    private static final String REGION_NOT_VALID = "Region is not valid";
    private static final String INVENTORY = "Inventory";
    private static final String SHOP_NOT_FOUND = "Shop not found";
    private static final String PARENT_PRODUCT_NOT_FOUND = "Please select product";
    private static final String QUANTITY_NOT_VALID = "Please enter valid quantity";
    private static final String PRODUCT_NOT_FOUND = "Product not found";
    private static final String BATCH_NOT_FOUND = "Quantity not available in %s for selected batch.";
    private static final String VENDOR = "Vendor";
    private static final String VENDOR_NOT_FOUND = "Vendor not found";
    private static final String NOT_VALID_BUNDLE_PRODUCT = "Please select valid product for creating bundle product";
    private static final String PRODUCT_NOT_ENOUGH_QUANTITY = "Selected bundle Item does not have enough quantity.";
    private static final String INVENTORY_NOT_FOUND = "Selected inventory either doesn't exists or is not active.";
    private static final String VENDOR_NOT_FOUND_FOR_PRODUCT = "Selected vendor does not belongs to product: %S";

    private static final String EXTERNAL_ID_MISSING = "External Id cannot be blank.";
    private static final String EXTERNAL_LICENSE_MISSING = "External Batch License cannot be blank.";
    private static final String ROOM_NOT_FOUND = "Selected Room(Inventory) either doesn't exists or is not active.";
    private static final String TOLERANCE_NOT_FOUND = "Selected weight does exists.";

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryServiceImpl.class);
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    ProductChangeLogRepository productChangeLogRepository;
    @Inject
    BarcodeService barcodeService;
    @Inject
    BackgroundTaskManager taskManager;
    @Inject
    ProductPrepackageQuantityRepository quantityRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    MetrcAccountService metrcAccountService;
    @Inject
    BatchQuantityService batchQuantityService;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    ReconciliationHistoryRepository reconciliationHistoryRepository;
    @Inject
    CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    @Inject
    PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    BatchActivityLogService batchActivityLogService;
    @Inject
    private ExciseTaxInfoService exciseTaxInfoService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private CultivationTaxInfoService cultivationTaxInfoService;
    @Inject
    private AssetService assetService;
    @Inject
    private SecurityUtil securityUtil;
    @Inject
    private ProductBatchService productBatchService;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private BlazeRegionRepository regionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private DerivedProductBatchLogService derivedProductBatchLogService;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private VendorService vendorService;
    @Inject
    private BlazeEventBus eventBus;
    @Inject
    private BarcodeItemRepository barcodeItemRepository;

    @Inject
    public InventoryServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public SearchResult<Inventory> getShopInventories(String shopId) {
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }
        return inventoryRepository.findItems(token.getCompanyId(), shopId, 0, 200);
    }


    @Override
    public DateSearchResult<Inventory> getCompanyInventories(long afterDate, long beforeDate) {
        return inventoryRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public Inventory addInventory(InventoryAddRequest request) {
        String inventoryName = request.getName().trim();

        //New inventory can't have "safe" or "exchange" name
        if (inventoryName.equalsIgnoreCase(Inventory.SAFE) || inventoryName.equalsIgnoreCase(Inventory.EXCHANGE) || inventoryName.equalsIgnoreCase(Inventory.QUARANTINE)) {
            throw new BlazeInvalidArgException("Inventory", "Please enter another name");
        }

        Inventory inventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), inventoryName);

        long count = inventoryRepository.count(token.getCompanyId(), token.getShopId());



        if (!token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
            if (count >= companyFeatures.getMaxInventories()) {
                throw new BlazeInvalidArgException(INVENTORY, "Max inventory allowed reached.");
            }
        }
        if (inventory != null) {
            if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)){
                InventoryUpdateRequest inventoryUpdateRequest  = new InventoryUpdateRequest();
                inventoryUpdateRequest.setRegionIds(request.getRegionIds());
                inventoryUpdateRequest.setExternalId(request.getExternalId());
                inventoryUpdateRequest.setDryRoom(request.isDryRoom());
                inventoryUpdateRequest.setActive(request.isActive());
                inventoryUpdateRequest.setName(inventory.getName());
                return this.updateInventory(inventory.getId(),inventoryUpdateRequest);
            } else {
                throw new BlazeInvalidArgException(INVENTORY, "Inventory name already exist.");
            }
        }

        if (!CollectionUtils.isNullOrEmpty(request.getRegionIds())) {
            this.validateRegions(request.getRegionIds());
        }

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(request.getExternalId())) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, EXTERNAL_ID_MISSING);
            }
            Inventory uniqueInventory = inventoryRepository.getInventoryByExternalId(token.getCompanyId(), token.getShopId(), request.getExternalId());
            if (uniqueInventory != null) {
                throw new BlazeInvalidArgException(INVENTORY, "Room " + request.getName() + " is already linked with other inventory.");
            }

        }
        inventory = new Inventory();
        inventory.prepare(token.getCompanyId());
        inventory.setShopId(token.getShopId());
        inventory.setName(inventoryName);
        inventory.setType(Inventory.InventoryType.Field);
        inventory.setActive(request.isActive());
        inventory.setRegionIds(request.getRegionIds());
        inventory.setExternalId(request.getExternalId());
        inventory.setDryRoom(request.isDryRoom());

        inventoryRepository.save(inventory);
        return inventory;
    }

    private void validateRegions(LinkedHashSet<String> regionIds) {

        List<ObjectId> regionObjectIds = new ArrayList<>();
        for (String regionId : regionIds) {
            if (StringUtils.isNotBlank(regionId) && ObjectId.isValid(regionId)) {
                regionObjectIds.add(new ObjectId(regionId));
            } else {
                throw new BlazeInvalidArgException(INVENTORY, REGION_NOT_VALID);
            }
        }

        HashMap<String, BlazeRegion> regionMap = regionRepository.listAsMap(token.getCompanyId(), regionObjectIds);

        for (String regionId : regionIds) {
            BlazeRegion region = regionMap.get(regionId);
            if (region == null) {
                throw new BlazeInvalidArgException(INVENTORY, REGION_NOT_VALID);
            }
        }
    }

    @Override
    public Inventory updateInventory(String inventoryId, InventoryUpdateRequest request) {
        Inventory dbInventory = inventoryRepository.get(token.getCompanyId(), inventoryId);
        if (dbInventory == null) {
            throw new BlazeInvalidArgException("Inventory", "Inventory does not exist with this id.");
        }

        String inventoryName = request.getName().trim();
        Inventory inventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), inventoryName);
        if (inventory != null && !inventory.getId().equalsIgnoreCase(inventoryId)) {
            throw new BlazeInvalidArgException("Inventory", "Another inventory with this name exist.");
        }

        if (Inventory.InventoryType.Storage != dbInventory.getType() && Inventory.InventoryType.Quarantine != dbInventory.getType()) {
            if (request.getName().equalsIgnoreCase(Inventory.SAFE) || dbInventory.getName().equalsIgnoreCase(Inventory.SAFE) || request.getName().equalsIgnoreCase(Inventory.EXCHANGE)
                    || dbInventory.getName().equalsIgnoreCase(Inventory.EXCHANGE) || request.getName().equalsIgnoreCase(Inventory.QUARANTINE) || dbInventory.getName().equalsIgnoreCase(Inventory.QUARANTINE)) {

                throw new BlazeInvalidArgException("Terminal.Name", "Inventory with same name is exists, Please choose another name.");
            }
            dbInventory.setName(inventoryName);
            dbInventory.setType(Inventory.InventoryType.Field);
            dbInventory.setActive(request.isActive());
        }

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(request.getExternalId())) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, EXTERNAL_ID_MISSING);
            }
            Inventory uniqueInventory = inventoryRepository.getInventoryByExternalId(token.getCompanyId(), token.getShopId(), request.getExternalId());
            if (uniqueInventory != null) {
                if (!uniqueInventory.getId().equals(inventoryId)) {
                    throw new BlazeInvalidArgException(INVENTORY, "Room " + request.getName() + " is already liked with other.");
                }

            }
        }

        if (!CollectionUtils.isNullOrEmpty(request.getRegionIds())) {
            this.validateRegions(request.getRegionIds());
        }
        dbInventory.setDryRoom(request.isDryRoom());
        dbInventory.setRegionIds(request.getRegionIds());
        dbInventory.setExternalId(request.getExternalId());


        inventoryRepository.update(token.getCompanyId(), dbInventory.getId(), dbInventory);
        return dbInventory;
    }

    @Override
    public void deleteInventory(String inventoryId) {
        //Iterable<Product> productsWithThisInventory = productRepository.findProductWithAvailableInventory(token.getCompanyId(),inventoryId);
        //List<Product> products = Lists.newArrayList(productsWithThisInventory);
        //Safe inventory can not be deleted
        Inventory inventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), inventoryId);
        if (inventory != null && (inventory.getName().equalsIgnoreCase(Inventory.SAFE) || inventory.getName().equalsIgnoreCase(Inventory.EXCHANGE) || inventory.getName().equalsIgnoreCase(Inventory.QUARANTINE))) {
            throw new BlazeInvalidArgException("Inventory", inventory.getName() + " inventory can't be deleted.");
        }

        long count = productRepository.countProductWithAvailableInventory(token.getCompanyId(), inventoryId);
        if (count == 0) {
            inventoryRepository.removeByIdSetState(token.getCompanyId(), inventoryId);
        } else {
            throw new BlazeInvalidArgException("Inventory", "Please re-assigned inventory quantity before deleting.");
        }
    }

    @Override
    public void deleteInventory(InventoryDeleteRequest deleteRequest) {

        //Safe inventory can not be deleted
        Inventory inventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), deleteRequest.getDeleteInventoryId());
        if (inventory != null && (inventory.getName().equalsIgnoreCase(Inventory.SAFE) || inventory.getName().equalsIgnoreCase(Inventory.EXCHANGE) || inventory.getName().equalsIgnoreCase(Inventory.QUARANTINE))) {
            throw new BlazeInvalidArgException("Inventory", inventory.getName() + " inventory can't be deleted.");
        }

        // Delete all inventory to designated inventory
        if (deleteRequest.getDeleteInventoryId().equalsIgnoreCase(deleteRequest.getToInventoryId())) {
            throw new BlazeInvalidArgException("Inventory", "Transfer inventory must be different from deleted inventory.");
        }

        Iterable<Product> products = productRepository.listByShop(token.getCompanyId(), token.getShopId());
        BulkInventoryTransferRequest bulkInventoryTransferRequest = new BulkInventoryTransferRequest();
        for (Product p : products) {
            for (ProductQuantity quantity : p.getQuantities()) {
                if (quantity.getInventoryId().equalsIgnoreCase(deleteRequest.getDeleteInventoryId())
                        && quantity.getQuantity().doubleValue() > 0) {
                    InventoryTransferRequest transferRequest = new InventoryTransferRequest();
                    transferRequest.setFromInventoryId(deleteRequest.getDeleteInventoryId());
                    transferRequest.setToInventoryId(deleteRequest.getToInventoryId());
                    transferRequest.setProductId(p.getId());
                    transferRequest.setTransferAmount(quantity.getQuantity());

                    bulkInventoryTransferRequest.getTransfers().add(transferRequest);
                }
            }

            //TODO: CHECK TO SEE IF THERE ARE ANY PRODUCT PREPACKAGE QUANTITY
        }
        if (bulkInventoryTransferRequest.getTransfers().size() > 0) {
            bulkTransferInventory(bulkInventoryTransferRequest);
        }

        inventoryRepository.removeByIdSetState(token.getCompanyId(), deleteRequest.getDeleteInventoryId());
    }

    @Override
    public ProductBatch addBatch(BatchAddRequest request) {

        Product dbProduct = productRepository.get(token.getCompanyId(), request.getProductId());
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("BatchAddRequest", "Product does not exist.");
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (Objects.isNull(shop)) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && request.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Selected batch status is not in ready for sale");
        }
        Vendor vendor;
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && StringUtils.isBlank(request.getVendorId())) {
            vendor = vendorService.getDefaultVendor(token.getCompanyId());
            request.setVendorId(vendor.getId());
        } else {
            request.setVendorId(StringUtils.isBlank(request.getVendorId()) ? dbProduct.getVendorId() : request.getVendorId());
            // Check if vendor exist
            vendor = vendorRepository.get(token.getCompanyId(), request.getVendorId());
            if (vendor == null) {
                throw new BlazeInvalidArgException("Vendor", "Vendor does not exist");
            }

        }
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail && !request.getVendorId().equalsIgnoreCase(dbProduct.getVendorId()) && (
            CollectionUtils.isNullOrEmpty(dbProduct.getSecondaryVendors()) || !dbProduct.getSecondaryVendors()
                .contains(request.getVendorId()))) {
            throw new BlazeInvalidArgException(VENDOR,
                String.format(VENDOR_NOT_FOUND_FOR_PRODUCT, dbProduct.getName()));
        }

        ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), dbProduct.getCategoryId());
        if (productCategory == null) {
            throw new BlazeInvalidArgException("BatchAddRequest", "Category does not exist.");
        }

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(request.getExternalId())) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, EXTERNAL_ID_MISSING);
            }
            if (StringUtils.isBlank(request.getExternalLicense())) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, EXTERNAL_LICENSE_MISSING);
            }
        }

        if (StringUtils.isNotBlank(request.getMetrcTagId())) {
            MetricsPackages metricsPackage = null;
            try {
                metricsPackage = metrcAccountService.getMetrcPackageByLabel(request.getMetrcTagId());
            } catch (Exception e) {
                LOGGER.info("Error get metrc packages", e);
            }

            if (metricsPackage == null) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, METRC_NOT_FOUND);
            }
        }

        BigDecimal costPerUnit = request.getCostPerUnit();
        BigDecimal totalCost = request.getCost();

        if (costPerUnit == null || costPerUnit.doubleValue() <= 0) {
            costPerUnit = new BigDecimal(0);
            if (request.getQuantity() != null && request.getQuantity().doubleValue() > 0) {
                if (totalCost != null && totalCost.doubleValue() > 0) {
                    double cost = NumberUtils.round(request.getCost().doubleValue(), 4);
                    double quantity = NumberUtils.round(request.getQuantity().doubleValue(), 4);
                    if (quantity > 0) {
                        double myCost = cost / quantity;
                        costPerUnit = new BigDecimal(myCost);
                    }
                }
            }
        } else {
            if (costPerUnit == null) {
                costPerUnit = new BigDecimal(0);
            }
            if (request.getQuantity() != null && request.getQuantity().doubleValue() > 0) {
                double cost = NumberUtils.round(request.getQuantity().doubleValue() * costPerUnit.doubleValue(), 4);
                totalCost = new BigDecimal(cost);
            }
        }

        ProductBatch productBatch = new ProductBatch();
        productBatch.prepare(token.getCompanyId());
        productBatch.setProductId(dbProduct.getId());
        productBatch.setShopId(token.getShopId());
        productBatch.setSku(request.getSku());
        if (StringUtils.isNotBlank(request.getSku())) {
            productBatch.setSku(request.getSku().trim());
        }
        productBatch.setArmsLengthType(vendor.getArmsLengthType());
        productBatch.setVendorId(request.getVendorId());
        productBatch.setCost(totalCost); //NumberUtils.round(request.getSentCost(),2));
        productBatch.setCostPerUnit(costPerUnit);
        productBatch.setQuantity(request.getQuantity()); //NumberUtils.round(request.getQuantity(),2));
        productBatch.setThc((request.getThc() == null) ? 0 : request.getThc());
        productBatch.setCbn((request.getCbn() == null) ? 0 : request.getCbn());
        productBatch.setCbd((request.getCbd() == null) ? 0 : request.getCbd());
        productBatch.setCbda((request.getCbda() == null) ? 0 : request.getCbda());
        productBatch.setThca((request.getThca() == null) ? 0 : request.getThca());
        productBatch.setPurchasedDate(request.getPurchasedDate());
        productBatch.setSellBy(request.getSellBy());
        productBatch.setTrackHarvestBatch(request.getTrackHarvestBatch());
        productBatch.setTrackHarvestDate(request.getTrackHarvestDate());
        productBatch.setReferenceNote(request.getReferenceNote());
        productBatch.setFlowerSourceType(request.getFlowerSourceType());
        productBatch.setExpirationDate(request.getExpirationDate());
        productBatch.setActualWeightPerUnit(request.getActualWeightPerUnit());
        productBatch.setWaste(request.getWaste());
        productBatch.setUnProcessed(request.getUnProcessed());
        if (productBatch.isUnProcessed()) {
            productBatch.setMoistureLoss(request.getMoistureLoss());
        }

        HashMap<String, Double> bundleProductQuantityMap = new HashMap<>();

        if (dbProduct.getProductType() == Product.ProductType.BUNDLE) {
            bundleProductQuantityMap = validateBundleBatchQuantity(request.getQuantity(), BigDecimal.ZERO, dbProduct.getBundleItems(), request.getBundleItems(), productBatch);
            request.setTargetInventory(Inventory.InventoryType.Storage);
        }

        if (StringUtils.isBlank(request.getBrandId()) && dbProduct.getBrandId() != null) {
            Brand brand = brandRepository.get(token.getCompanyId(), dbProduct.getBrandId());
            if (brand != null) {
                productBatch.setBrandId(dbProduct.getBrandId());
            }
        } else {
            productBatch.setBrandId(request.getBrandId());
        }
        productBatch.setReceiveDate(request.getReceiveDate());

        if (StringUtils.isNotBlank(request.getPurchaseOrderId())) {
            if (!ObjectId.isValid(request.getPurchaseOrderId())) {
                throw new BlazeInvalidArgException("PurchaseOrder", "Purchase order does not found");
            }
            PurchaseOrder dbPurchaseOrder = purchaseOrderRepository.getPOByPOId(token.getCompanyId(), token.getShopId(), request.getPurchaseOrderId());
            if (dbPurchaseOrder == null) {
                throw new BlazeInvalidArgException("PurchaseOrder", "Purchase order does not found");
            }
            productBatch.setPurchaseOrderId(dbPurchaseOrder.getId());
            productBatch.setPoNumber(dbPurchaseOrder.getPoNumber());
        }

        productBatch.setStatus(request.getStatus());
        if (productBatch.getQuantity() == null || productBatch.getQuantity().doubleValue() < 0) {
            productBatch.setQuantity(new BigDecimal(0));
        }

        if (request.getPotencyAmount() != null) {
            productBatch.setPotencyAmount(request.getPotencyAmount());
        }

        CompanyLicense companyLicense = null;
        if (StringUtils.isNotBlank(request.getLicenseId())) {
            companyLicense = vendor.getCompanyLicense(request.getLicenseId());
            productBatch.setLicenseId(companyLicense.getId());
        } else {
            companyLicense = new CompanyLicense();
            companyLicense.setCompanyType(Vendor.CompanyType.RETAILER);
        }

        calculateTaxForBatch(shop, productBatch, request.getQuantity(), vendor, companyLicense, dbProduct, productCategory);


        productBatch.setTrackTraceSystem(request.getTrackTraceSystem());
        if (productBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.MANUAL) {
            productBatch.setTrackPackageLabel("");
            productBatch.setTrackPackageId(null);
            productBatch.setTrackTraceVerified(false);
        } else if (productBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.METRC) {
            productBatch.setTrackPackageLabel(request.getTrackPackageLabel());
        }

        if (dbProduct.getProductType() != Product.ProductType.BUNDLE
                && StringUtils.isNotBlank(productBatch.getTrackPackageLabel())
                && productBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.METRC) {
            MetrcVerifiedPackage metricsPackages = metrcAccountService.getMetrcVerifiedPackage(productBatch.getTrackPackageLabel());
            if (metricsPackages != null && metricsPackages.isRequired()) {
                productBatch.setTrackPackageLabel(productBatch.getTrackPackageLabel());
                productBatch.setTrackTraceSystem(productBatch.getTrackTraceSystem());
                if (metricsPackages.getId() > 0) {
                    productBatch.setTrackPackageId(metricsPackages.getId());
                } else {
                    productBatch.setTrackPackageId(null);
                }
                productBatch.setTrackTraceVerified(true);
                productBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());
            }
        }

        if (productBatch.getQuantity().doubleValue() <= 0) {
            LOGGER.info("Batch Quantity is negative set quantity to zero");
            productBatch.setQuantity(BigDecimal.ZERO);
        }

        // Update sku if needed
        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(dbProduct.getCompanyId(),dbProduct.getShopId(), dbProduct.getId(),
                BarcodeItem.BarcodeEntityType.Batch,
                productBatch.getId(), productBatch.getSku(), null, false);
        if (item != null) {
            productBatch.setSku(item.getBarcode());
            productBatch.setBatchNo(item.getNumber());
        }

        productBatch.setProductBatchLabel(request.getProductBatchLabel());

        if (StringUtils.isNotBlank(productBatch.getSku())) {
            CompanyAsset companyAsset = new CompanyAsset();
            File file = null;
            InputStream inputStream = QrCodeUtil.getQrCode(productBatch.getSku());
            try {
                file = QrCodeUtil.stream2file(inputStream, ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            String keyName = productBatch.getId() + "-" + productBatch.getSku();
            UploadFileResult uploadFileResult = amazonS3Service.uploadFilePublic(file, keyName, ".png");
            companyAsset.prepare(token.getCompanyId());
            companyAsset.setKey(uploadFileResult.getKey());
            companyAsset.setPublicURL(uploadFileResult.getUrl());
            companyAsset.setType(Asset.AssetType.Photo);
            companyAsset.setActive(true);
            companyAsset.setSecured(false);
            productBatch.setBatchQRAsset(companyAsset);
        }
        productBatch.setLotId(request.getLotId());
        productBatch.setMetrcTagId(request.getMetrcTagId());
        productBatch.setPrepaidTax(request.isPrepaidTax());

        if (request.getAttachments() != null && !request.getAttachments().isEmpty()) {
            for (CompanyAsset asset : request.getAttachments()) {
                asset.prepare(token.getCompanyId());
            }
        }
        productBatch.setAttachments(request.getAttachments());
        productBatch.setLabelInfo(request.getLabelInfo());

        productBatch.setExternalId(request.getExternalId());
        productBatch.setExternalLicense(request.getExternalLicense());
        productBatch.setActualWeightPerUnit(productBatch.getActualWeightPerUnit());
        productBatch.setRoomId(request.getRoomId());
        productBatch.setWaste(productBatch.getWaste());

        ProductBatch dbBatch = productBatchRepository.save(productBatch);

        Brand dbBrand = brandRepository.get(dbBatch.getCompanyId(), dbBatch.getBrandId());
        if (!Objects.isNull(dbBrand)) {
            dbBatch.setBrandName(dbBrand.getName());
        } else {
            dbBatch.setBrandName("");
        }
        dbBatch.setProductName(dbProduct.getName());
        dbBatch.setVendorName(vendor.getName());


        batchActivityLogService.addBatchActivityLog(productBatch.getId(), token.getActiveTopUser().getUserId(), productBatch.getSku() + " Batch added");

//        addNewBatchInventoryQuantity(productBatch, dbProduct, null, request.getTargetInventory());
        this.createQueuedTransactionJobForBatchQuantity(productBatch, request.getTargetInventory(), ProductBatchQueuedTransaction.OperationType.Add, productBatch.getQuantity(), "", InventoryOperation.SubSourceAction.AddBatch, bundleProductQuantityMap, dbProduct.getProductType(), dbBatch.getRoomId());
        elasticSearchManager.createOrUpdateIndexedDocument(dbBatch);

        return productBatch;
    }

    public void calculateExciseTaxForBatch(ProductBatch productBatch, BigDecimal costPerUnit, boolean isCannabis) {
        Vendor vendor = vendorRepository.getById(productBatch.getVendorId());
        Vendor.ArmsLengthType armsLengthType = (vendor != null) ? vendor.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;
        HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(token.getCompanyId(), token.getShopId(), productBatch.getQuantity(), costPerUnit, armsLengthType, isCannabis);
        productBatch.setPerUnitExciseTax(taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO));
        productBatch.setTotalExciseTax(taxInfo.getOrDefault("totalExciseTax", BigDecimal.ZERO));
    }

    @Override
    public void createQueuedTransactionJobForBatchQuantity(ProductBatch productBatch, Inventory.InventoryType inventoryType, ProductBatchQueuedTransaction.OperationType operationType, BigDecimal quantity, String sourceChildId, InventoryOperation.SubSourceAction subSourceAction) {
        createQueuedTransactionJobForBatchQuantity(productBatch, inventoryType, operationType, quantity, sourceChildId, subSourceAction, null, Product.ProductType.REGULAR, "");
    }

    public void addNewBatchInventoryQuantity(ProductBatch productBatch, Product dbProduct, String receivingInventoryId, Inventory.InventoryType inventoryType) {
        Inventory dbInventory = null;

        if (receivingInventoryId != null) {
            dbInventory = inventoryRepository.get(token.getCompanyId(), receivingInventoryId);
        }

        if (inventoryType == Inventory.InventoryType.Quarantine && productBatch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE) {
            inventoryType = Inventory.InventoryType.Storage;
        }

        if (dbInventory == null && Inventory.InventoryType.Quarantine == inventoryType) {
            dbInventory = inventoryRepository.getInventory(token.getCompanyId(), dbProduct.getShopId(), Inventory.QUARANTINE);
        } else if (dbInventory == null || !dbInventory.isActive()) {
            dbInventory = inventoryRepository.getInventory(token.getCompanyId(), dbProduct.getShopId(), Inventory.SAFE);
        }

        if (dbInventory == null) {
            dbInventory = new Inventory();
            dbInventory.setCompanyId(token.getCompanyId());
            dbInventory.setShopId(dbProduct.getShopId());
            if (Inventory.InventoryType.Quarantine == inventoryType) {
                dbInventory.setName(Inventory.QUARANTINE);
                dbInventory.setType(Inventory.InventoryType.Quarantine);
            } else {
                dbInventory.setName(Inventory.SAFE);
                dbInventory.setType(Inventory.InventoryType.Storage);
            }
            inventoryRepository.save(dbInventory);
        }


        ProductQuantity quantity = null;
        for (ProductQuantity q : dbProduct.getQuantities()) {
            if (q.getInventoryId().equalsIgnoreCase(dbInventory.getId())) {
                quantity = q;
                break;
            }
        }
        if (quantity == null) {
            quantity = new ProductQuantity();
            quantity.setId(ObjectId.get().toString());
            quantity.setCompanyId(token.getCompanyId());
            quantity.setShopId(dbProduct.getShopId());
            quantity.setInventoryId(dbInventory.getId());
            quantity.setQuantity(productBatch.getQuantity());
            dbProduct.getQuantities().add(quantity);

        } else {
            double newQuantity = quantity.getQuantity().doubleValue() + productBatch.getQuantity().doubleValue();
            quantity.setQuantity(new BigDecimal(newQuantity));
        }

        // add batch quantity
        batchQuantityService.addBatchQuantity(token.getCompanyId(), dbProduct.getShopId(), dbProduct.getId(),
                dbInventory.getId(), productBatch.getId(), productBatch.getQuantity());

        //Add product batch's quantity in product
        // Add change log
        ProductChangeLog changeLog = productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                dbProduct, null, "Add Batch (" + dbInventory.getName() + ")");

        productRepository.update(token.getCompanyId(), dbProduct.getId(), dbProduct);
        productChangeLogRepository.save(changeLog);

        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(dbProduct.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    @Override
    public ProductBatch updateBatch(String batchId, UpdateProductBatchRequest productBatch) {
        ProductBatch dbBatch = productBatchRepository.get(token.getCompanyId(), batchId);
        if (dbBatch == null) {
            throw new BlazeInvalidArgException("BatchId", "Batch doesn't exist.");
        }
        Product dbProduct = productRepository.get(token.getCompanyId(), dbBatch.getProductId());
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("BatchAddRequest", "Product does not exist.");
        }
        boolean isMoveForSale = (productBatch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE && dbBatch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE);

        Shop shop = shopRepository.getById(token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Shop does not exist.");
        }
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && productBatch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Selected batch status is not in ready for sale");
        }
        Vendor vendor;
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && StringUtils.isBlank(productBatch.getVendorId())) {
            vendor = vendorService.getDefaultVendor(token.getCompanyId());
            productBatch.setVendorId(vendor.getId());
        } else {
            // Check if vendor exist
            vendor = vendorRepository.get(token.getCompanyId(), productBatch.getVendorId());
            if (StringUtils.isBlank(productBatch.getDerivedLogId()) && vendor == null) {
                throw new BlazeInvalidArgException("Vendor", "Vendor does not exist");
            }

            if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail && !vendor.getId().equalsIgnoreCase(dbProduct.getVendorId()) && (
                CollectionUtils.isNullOrEmpty(dbProduct.getSecondaryVendors()) || !dbProduct.getSecondaryVendors()
                    .contains(productBatch.getVendorId()))) {
                throw new BlazeInvalidArgException(VENDOR,
                    String.format(VENDOR_NOT_FOUND_FOR_PRODUCT, dbProduct.getName()));
            }
        }

        ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), dbProduct.getCategoryId());
        if (StringUtils.isBlank(productBatch.getDerivedLogId()) && productCategory == null) {
            throw new BlazeInvalidArgException("BatchAddRequest", "Category does not exist.");
        }

        if (StringUtils.isNotBlank(productBatch.getMetrcTagId())) {
            MetricsPackages metricsPackage = metrcAccountService.getMetrcPackageByLabel(productBatch.getMetrcTagId());
            if (metricsPackage == null) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, METRC_NOT_FOUND);
            }
        }

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(productBatch.getExternalId())) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, EXTERNAL_ID_MISSING);
            }
            if (StringUtils.isBlank(productBatch.getExternalLicense())) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, EXTERNAL_LICENSE_MISSING);
            }
        }

        BigDecimal requestedQuantity = productBatch.getQuantity();
        BigDecimal dbQuantity = dbBatch.getQuantity();
        boolean inventoryUpdate = checkIsInventoryUpdated(dbBatch.getRoomId(), productBatch.getRoomId());
        String previousInventoryId = dbBatch.getRoomId();

        // SET FIELDS THAT ARE UPDATABLE
        dbBatch.setPurchasedDate(productBatch.getPurchasedDate());
        dbBatch.setCost(productBatch.getCost()); //NumberUtils.roundCurrency(productBatch.getSentCost()));
        dbBatch.setCbda(productBatch.getCbda());
        dbBatch.setCbn(productBatch.getCbn());
        dbBatch.setCbd(productBatch.getCbd());
        dbBatch.setNotes(productBatch.getNotes());
        dbBatch.setThc(productBatch.getThc());
        dbBatch.setThca(productBatch.getThca());
        dbBatch.setSellBy(productBatch.getSellBy());
        dbBatch.setArchived(productBatch.isArchived());
        dbBatch.setPublished(productBatch.isPublished());
        dbBatch.setTrackHarvestBatch(productBatch.getTrackHarvestBatch());
        dbBatch.setTrackHarvestDate(productBatch.getTrackHarvestDate());
        dbBatch.setReferenceNote(productBatch.getReferenceNote());
        dbBatch.setVendorId(productBatch.getVendorId());
        // now set the quantity
        dbBatch.setQuantity(requestedQuantity);
        dbBatch.setExpirationDate(productBatch.getExpirationDate());
        dbBatch.setExternalId(productBatch.getExternalId());
        dbBatch.setExternalLicense(productBatch.getExternalLicense());
        dbBatch.setActualWeightPerUnit(productBatch.getActualWeightPerUnit());
        dbBatch.setRoomId(productBatch.getRoomId());
        if (dbProduct.getProductType() == Product.ProductType.DERIVED) {
        dbBatch.setWaste(productBatch.getWaste());
        }
        dbBatch.setUnProcessed(productBatch.isUnProcessed());
        if (productBatch.isUnProcessed()) {
            dbBatch.setMoistureLoss(productBatch.getMoistureLoss());
        }

        if (Inventory.InventoryType.Quarantine.equals(productBatch.getTargetInventory())) {
            if (StringUtils.isNotBlank(productBatch.getBrandId())) {
                Brand brand = brandRepository.get(token.getCompanyId(), productBatch.getBrandId());
                if (brand == null) {
                    throw new BlazeInvalidArgException("Brand", "Brand not found.");
                }
                dbBatch.setBrandId(productBatch.getBrandId());
            } else {
                throw new BlazeInvalidArgException("Brand", "Brand id required.");
            }
        } else {
            dbBatch.setBrandId(productBatch.getBrandId());
        }

        dbBatch.setReceiveDate(productBatch.getReceiveDate());
        if (StringUtils.isNotBlank(productBatch.getPurchaseOrderId())) {
            if (!ObjectId.isValid(productBatch.getPurchaseOrderId())) {
                throw new BlazeInvalidArgException("PurchaseOrder", "Purchase order does not found");
            }
            PurchaseOrder dbPurchaseOrder = purchaseOrderRepository.getPOByPOId(token.getCompanyId(), token.getShopId(), productBatch.getPurchaseOrderId());
            if (dbPurchaseOrder == null) {
                throw new BlazeInvalidArgException("PurchaseOrder", "Purchase order does not found");
            }
            dbBatch.setPurchaseOrderId(dbPurchaseOrder.getId());
            dbBatch.setPoNumber(dbPurchaseOrder.getPoNumber());
        } else {
            dbBatch.setPoNumber("");
            dbBatch.setPurchaseOrderId("");
        }
        dbBatch.setStatus((dbBatch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE) ? dbBatch.getStatus() : productBatch.getStatus());
        dbBatch.setMetrcCategory(productBatch.getMetrcCategory());
        dbBatch.setVoidStatus(productBatch.getVoidStatus());
        dbBatch.setActive(productBatch.isActive());
        if (productBatch.getPotencyAmount() != null) {
            dbBatch.setPotencyAmount(productBatch.getPotencyAmount());
        } else {
            PotencyMG potencyMG = new PotencyMG();
            dbBatch.setPotencyAmount(potencyMG);
        }

        dbBatch.setTrackTraceSystem(productBatch.getTrackTraceSystem());
        if (dbBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.MANUAL) {
            dbBatch.setTrackPackageLabel("");
            dbBatch.setTrackPackageId(null);
            dbBatch.setTrackTraceVerified(false);
        }
        if (dbProduct.getProductType() != Product.ProductType.BUNDLE && StringUtils.isNotBlank(productBatch.getTrackPackageLabel())) {
            dbBatch.setTrackTraceVerified(false);

            if (StringUtils.isNotBlank(productBatch.getTrackPackageLabel())) {
                MetrcVerifiedPackage metricsPackages = metrcAccountService.getMetrcVerifiedPackage(productBatch.getTrackPackageLabel());
                if (metricsPackages != null && metricsPackages.isRequired()) {
                    dbBatch.setTrackPackageLabel(productBatch.getTrackPackageLabel());
                    dbBatch.setTrackTraceSystem(productBatch.getTrackTraceSystem());
                    dbBatch.setTrackPackageId(metricsPackages.getId());
                    dbBatch.setTrackTraceVerified(true);
                    dbBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());

                }
            } else {
                dbBatch.setTrackPackageLabel("");
                dbBatch.setTrackPackageId(-1);
                dbBatch.setTrackTraceVerified(false);
            }
        }

        // Update sku if needed
        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(dbBatch.getCompanyId(),dbBatch.getShopId(), dbBatch.getProductId(),
                BarcodeItem.BarcodeEntityType.Batch,
                dbBatch.getId(), productBatch.getSku(), dbBatch.getSku(), false);
        if (item != null) {
            dbBatch.setSku(item.getBarcode());
        } else {
            if (StringUtils.isNotBlank(productBatch.getSku())) {
                dbBatch.setSku(productBatch.getSku().trim());
            }
        }

        BigDecimal costPerUnit = productBatch.getCostPerUnit();
        BigDecimal totalCost = productBatch.getCost();

        if (costPerUnit == null || costPerUnit.doubleValue() <= 0) {
            costPerUnit = new BigDecimal(0);
            if (productBatch.getQuantity() != null && productBatch.getQuantity().doubleValue() > 0) {
                if (totalCost != null && totalCost.doubleValue() > 0) {
                    double cost = NumberUtils.round(productBatch.getCost().doubleValue(), 4);
                    double quantity = NumberUtils.round(productBatch.getQuantity().doubleValue(), 4);
                    if (quantity > 0) {
                        double myCost = cost / quantity;
                        costPerUnit = new BigDecimal(myCost);
                    }
                }
            }
        } else {
            if (costPerUnit == null) {
                costPerUnit = new BigDecimal(0);
            }
            if (productBatch.getQuantity() != null && productBatch.getQuantity().doubleValue() > 0) {
                double cost = NumberUtils.round(productBatch.getQuantity().doubleValue() * costPerUnit.doubleValue(), 4);
                totalCost = new BigDecimal(cost);
            }
        }
        dbBatch.setCostPerUnit(costPerUnit);
        dbBatch.setCost(totalCost);
        dbBatch.setProductBatchLabel(productBatch.getProductBatchLabel());
        dbBatch.setLotId(productBatch.getLotId());
        dbBatch.setMetrcTagId(productBatch.getMetrcTagId());
        dbBatch.setPrepaidTax(productBatch.isPrepaidTax());
        dbBatch.setTrackPackageLabel(productBatch.getTrackPackageLabel());

        // now set the quantity after all metrc package quantity updates
        if (dbBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.MANUAL) {
            dbBatch.setQuantity(requestedQuantity);
        } else if (dbBatch.getTrackTraceSystem() == ProductBatch.TrackTraceSystem.METRC && productBatch.getQuantity().doubleValue() > 0) {
            dbBatch.setQuantity(productBatch.getQuantity());
        } else {
            LOGGER.info("Batch quantity is negative update quantity to zero");
            dbBatch.setQuantity(BigDecimal.ZERO);
        }

        CompanyLicense companyLicense = null;
        if (StringUtils.isBlank(productBatch.getLicenseId())) {
            companyLicense = new CompanyLicense();
            companyLicense.setCompanyType(Vendor.CompanyType.RETAILER);
        } else {
            companyLicense = vendor.getCompanyLicense(productBatch.getLicenseId());
            dbBatch.setLicenseId(companyLicense.getId());
        }
        calculateTaxForBatch(shop, dbBatch, requestedQuantity, vendor, companyLicense, dbProduct, productCategory);

        double difference = requestedQuantity.doubleValue() - dbQuantity.doubleValue();

        Inventory.InventoryType targetInventory = (!shop.isRetail() && dbBatch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE) ? Inventory.InventoryType.Quarantine : Inventory.InventoryType.Storage;
        // Update batch quantity
//        updateBatchInventoryQuantity(dbBatch, productBatch, dbProduct, null, Boolean.TRUE, productBatch.getTargetInventory());
        if (isMoveForSale) {
            ProductBatchStatusRequest updateRequest = new ProductBatchStatusRequest();
            updateRequest.setStatus(ProductBatch.BatchStatus.READY_FOR_SALE);
            productBatchService.batchMoveToSale(dbBatch, updateRequest);
        }
        if (StringUtils.isNotBlank(dbBatch.getDerivedLogId())) {

            DerivedProductBatchLog log = derivedProductBatchLogService.getById(token.getCompanyId(), dbBatch.getDerivedLogId());
            if (log != null && (productBatch.getProductMap() == null || productBatch.getProductMap().isEmpty())) {
                productBatch.setProductMap(log.getProductMap());
            }
            List<ObjectId> batchIds = new ArrayList<>();
            Set<ObjectId> inventoryIds = new HashSet<>();

            for (List<DerivedProductBatchInfo> derivedProductBatchInfos : productBatch.getProductMap().values()) {
                for (DerivedProductBatchInfo info : derivedProductBatchInfos) {
                    if (StringUtils.isNotBlank(info.getInventoryId()) && ObjectId.isValid(info.getInventoryId())) {
                        inventoryIds.add(new ObjectId(info.getInventoryId()));
                    }
                    if (StringUtils.isNotBlank(info.getBatchId()) && ObjectId.isValid(info.getBatchId())) {
                        batchIds.add(new ObjectId(info.getBatchId()));
                    }
                }
            }

            HashMap<String, ProductBatch> batchInfoMap = productBatchRepository.listAsMap(token.getCompanyId(), batchIds);
            HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(inventoryIds));

            validateDerivedProductRequest(productBatch.getProductMap(), productBatch.getProductId(), difference, batchInfoMap, inventoryMap);

            boolean updateBatchInfoQuantity = Boolean.FALSE;
            Map<String, List<DerivedProductBatchInfo>> resultProductMap = new HashMap<>();
            if (log != null) {
                updateBatchInfoQuantity = this.checkDerivedQuantityUpdate(productBatch, log, resultProductMap, requestedQuantity, dbQuantity);
            }

            boolean updateBatchQuantity = Boolean.FALSE;
            //Update quantity
            if (difference != 0) {
                updateBatchQuantity = Boolean.TRUE;
            }
            if (updateBatchInfoQuantity || updateBatchQuantity || inventoryUpdate) {

                for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : productBatch.getProductMap().entrySet()) {
                    for (DerivedProductBatchInfo info : entry.getValue()) {
                        info.prepare();
                        if (StringUtils.isBlank(info.getId())) {
                            info.setId(ObjectId.get().toString());
                        }
                    }
                }

                this.createQueuedTransactionForDerivedBatch(dbBatch, resultProductMap, ProductBatchQueuedTransaction.OperationType.Update, new BigDecimal(difference),
                    InventoryOperation.SubSourceAction.UpdateBatch, updateBatchQuantity, dbQuantity, inventoryUpdate, previousInventoryId);
                if (log != null) {
                    log.setDerivedQuantity(productBatch.getQuantity());
                    log.setProductMap(productBatch.getProductMap());
                }
                derivedProductBatchLogService.updateLog(log);
            }

        } else if (difference != 0) {
            HashMap<String, Double> bundleProductQuantityMap = null;
            if (dbProduct.getProductType() == Product.ProductType.BUNDLE) {
                bundleProductQuantityMap =  validateBundleBatchQuantity(requestedQuantity, dbQuantity, dbProduct.getBundleItems(), productBatch.getBundleItems(), dbBatch);
            }
            this.createQueuedTransactionJobForBatchQuantity(dbBatch, targetInventory, ProductBatchQueuedTransaction.OperationType.Update, new BigDecimal(difference), "", InventoryOperation.SubSourceAction.UpdateBatch,  bundleProductQuantityMap, dbProduct.getProductType(), dbBatch.getRoomId());
        }

        if (productBatch.getAttachments() != null && !productBatch.getAttachments().isEmpty()) {
            for (CompanyAsset asset : productBatch.getAttachments()) {
                asset.prepare(token.getCompanyId());
            }
        }

        dbBatch.setAttachments((productBatch.getAttachments() == null) ? new ArrayList<>() : productBatch.getAttachments());

        dbBatch.setLabelInfo(productBatch.getLabelInfo());

        dbBatch = productBatchRepository.update(token.getCompanyId(), dbBatch.getId(), dbBatch);
        Brand dbBrand = brandRepository.get(dbBatch.getCompanyId(), dbBatch.getBrandId());
        if (!Objects.isNull(dbBrand)) {
            dbBatch.setBrandName(dbBrand.getName());
        } else {
            dbBatch.setBrandName("");
        }
        dbBatch.setProductName(dbProduct.getName());
        if (vendor != null) {
            dbBatch.setVendorName(vendor.getName());
        }

        batchActivityLogService.addBatchActivityLog(dbBatch.getId(), token.getActiveTopUser().getUserId(), dbBatch.getSku() + " Batch updated");

        elasticSearchManager.createOrUpdateIndexedDocument(dbBatch);

        // save batch
        return dbBatch;
    }

    /**
     * @implNote : We have these scenarios for updating product batch
     * -ve quantity, +ve quantity, new product or delete product
     * @param productBatch : product batch info
     * @param log : derived product log
     * @param resultProductMap
     * @param requestedQuantity
     * @param dbQuantity
     */
    private Boolean checkDerivedQuantityUpdate(UpdateProductBatchRequest productBatch, DerivedProductBatchLog log,
                                               Map<String, List<DerivedProductBatchInfo>> resultProductMap, BigDecimal requestedQuantity, BigDecimal dbQuantity) {
        Boolean updateQuantity = Boolean.FALSE;

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        Map<String, List<DerivedProductBatchInfo>> dbProductMap = log.getProductMap();
        Kryo kryo = new Kryo();
        Map<String, List<DerivedProductBatchInfo>> reqProductMap = kryo.copy(productBatch.getProductMap());

        for (Map.Entry<String, List<DerivedProductBatchInfo>> requestEntry : reqProductMap.entrySet()) {
            boolean isProductExists = dbProductMap.containsKey(requestEntry.getKey());
            if (!isProductExists) { // Handles addition of new product
                List<DerivedProductBatchInfo> batchInfos = requestEntry.getValue();
                for (DerivedProductBatchInfo batchInfo : batchInfos) {

                    BigDecimal quantity;
                    if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                        quantity = batchInfo.getQuantity();
                    } else {
                        quantity = this.calculateDerivedQuantity(batchInfo.getQuantity().doubleValue(), requestedQuantity.doubleValue());
                    }
                    batchInfo.setQuantity(quantity.negate());
                    batchInfo.prepare();
                    batchInfo.setId(ObjectId.get().toString());
                }
                resultProductMap.put(requestEntry.getKey(), batchInfos);
                continue;
            }

            for (DerivedProductBatchInfo info : requestEntry.getValue()) {
                if (StringUtils.isBlank(info.getId())) { //Handles the case if new entry/product batch has been added in for existing product
                    resultProductMap.putIfAbsent(requestEntry.getKey(), new ArrayList<>());
                    info.prepare();
                    info.setId(ObjectId.get().toString());

                    BigDecimal quantity;
                    if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                        quantity = info.getQuantity();
                    } else {
                        quantity = this.calculateDerivedQuantity(info.getQuantity().doubleValue(), requestedQuantity.doubleValue());
                    }
                    info.setQuantity(quantity.negate());
                    resultProductMap.get(requestEntry.getKey()).add(info);
                }
                for (DerivedProductBatchInfo dbInfo : dbProductMap.get(requestEntry.getKey())) {
                    //Handles if quantity gets update (+ve or -ve) in existing batches
                    if (dbInfo.getId().equals(info.getId()) && (dbInfo.getQuantity().doubleValue() != info.getQuantity().doubleValue() || requestedQuantity.doubleValue() != dbQuantity.doubleValue())) {
                        double dbInfoQuantity = dbInfo.getQuantity().doubleValue();
                        double reqInfoQuantity = info.getQuantity().doubleValue();
                        if (shop.getAppTarget() != CompanyFeatures.AppTarget.Grow) {
                            dbInfoQuantity *= dbQuantity.doubleValue();
                            reqInfoQuantity *= requestedQuantity.doubleValue();
                        }

                        double difference = dbInfoQuantity - reqInfoQuantity;
                        if (difference != 0) {
                            info.prepare();
                            info.setQuantity(new BigDecimal(difference));

                            resultProductMap.putIfAbsent(requestEntry.getKey(), new ArrayList<>());
                            resultProductMap.get(requestEntry.getKey()).add(info);
                            break;
                        }
                    }
                    // grow shop only
                    if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                        if (dbInfo.getBatchId().equals(info.getBatchId()) && dbInfo.getInventoryId().equals(info.getInventoryId()) && (dbInfo.getQuantity().doubleValue() != info.getQuantity().doubleValue() || requestedQuantity.doubleValue() != dbQuantity.doubleValue())) {
                            double dbInfoQuantity = dbInfo.getQuantity().doubleValue();
                            double reqInfoQuantity = info.getQuantity().doubleValue();

                            double difference = dbInfoQuantity - reqInfoQuantity;
                            if (difference != 0) {
                                info.prepare();
                                info.setQuantity(new BigDecimal(difference));

                                resultProductMap.putIfAbsent(requestEntry.getKey(), new ArrayList<>());
                                resultProductMap.get(requestEntry.getKey()).add(info);
                                break;
                            }
                        }
                    }
                }
            }
        }

        for (Map.Entry<String, List<DerivedProductBatchInfo>> dbEntry : dbProductMap.entrySet()) {

            boolean isProductExists = reqProductMap.containsKey(dbEntry.getKey());

            if (!isProductExists) { //If products is deleted in request then put quantity back
                List<DerivedProductBatchInfo> entryValue = dbEntry.getValue();
                for (DerivedProductBatchInfo batchInfo : entryValue) {
                    BigDecimal quantity;
                    if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                        quantity = batchInfo.getQuantity();
                    } else {
                        quantity = this.calculateDerivedQuantity(batchInfo.getQuantity().doubleValue(), dbQuantity.doubleValue());
                    }

                    batchInfo.setQuantity(quantity);
                }

                resultProductMap.put(dbEntry.getKey(), entryValue);
            } else {

                boolean isBatchExists;
                for (DerivedProductBatchInfo dbInfo : dbEntry.getValue()) {
                    isBatchExists = Boolean.FALSE;
                    for (DerivedProductBatchInfo reqInfo : reqProductMap.get(dbEntry.getKey())) {
                        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                            if (dbInfo.getInventoryId().equals(reqInfo.getInventoryId()) && (dbInfo.getBatchId().equals(reqInfo.getBatchId()))) {
                                isBatchExists = Boolean.TRUE;
                                break;
                            }
                        }
                        if (dbInfo.getId().equals(reqInfo.getId())) {
                            isBatchExists = Boolean.TRUE;
                            break;
                        }
                    }
                    if (!isBatchExists) { //If batch is deleted from request then put back quantity
                        resultProductMap.putIfAbsent(dbEntry.getKey(), new ArrayList<>());
                        dbInfo.prepare();
                        dbInfo.setId(ObjectId.get().toString());
                        BigDecimal quantity;
                        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                            quantity = dbInfo.getQuantity();
                        } else {
                            quantity = this.calculateDerivedQuantity(dbInfo.getQuantity().doubleValue(), dbQuantity.doubleValue());
                        }

                        dbInfo.setQuantity(quantity);
                        resultProductMap.get(dbEntry.getKey()).add(dbInfo);
                    }
                }
            }
        }

        if (resultProductMap.size() > 0) {
            updateQuantity = Boolean.TRUE;
        }

        return updateQuantity;
    }

    private BigDecimal calculateDerivedQuantity(double quantity, double reqQuantity) {
        return new BigDecimal(quantity * reqQuantity);
    }

    private void createQueuedTransactionForDerivedBatch(ProductBatch batch, Map<String, List<DerivedProductBatchInfo>> productMap,
                                                        ProductBatchQueuedTransaction.OperationType operationType, BigDecimal quantity,
                                                        InventoryOperation.SubSourceAction subSourceAction, boolean updateBatchQuantity, BigDecimal previousQuantity, boolean inventoryUpdate, String previousInventoryId) {

        String inventoryName = batch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE ? Inventory.SAFE : Inventory.QUARANTINE;
        Inventory.InventoryType inventoryType = batch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE ? Inventory.InventoryType.Storage : Inventory.InventoryType.Quarantine;

        Inventory inventory = null;
        if (StringUtils.isNotBlank(batch.getRoomId())) {
            inventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), batch.getRoomId());
            if (inventory == null) {
              throw new BlazeInvalidArgException(PRODUCT_BATCH, ROOM_NOT_FOUND);
            }
        }
        if (inventory == null) {
            inventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), inventoryName);
        }

        if (inventory == null) {
            inventory = new Inventory();
            inventory.prepare(token.getCompanyId());
            inventory.setShopId(token.getShopId());
            inventory.setName(inventoryName);
            inventory.setType(inventoryType);
            inventory.setActive(Boolean.TRUE);

            inventoryRepository.save(inventory);
        }

        Inventory safeInventory = (inventoryName.equals(Inventory.SAFE)) ? inventory : inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        if (safeInventory == null) {
            safeInventory = new Inventory();
            safeInventory.prepare(token.getCompanyId());
            safeInventory.setShopId(token.getShopId());
            safeInventory.setName(Inventory.SAFE);
            safeInventory.setType(Inventory.InventoryType.Storage);
            safeInventory.setActive(true);

            inventoryRepository.save(safeInventory);
        }
        if (StringUtils.isBlank(previousInventoryId) && subSourceAction == InventoryOperation.SubSourceAction.UpdateBatch) {
            previousInventoryId = safeInventory.getId();
        }
        //Create queued transaction job
        ProductBatchQueuedTransaction transaction = new ProductBatchQueuedTransaction();
        transaction.prepare(token.getCompanyId());
        transaction.setShopId(token.getShopId());
        transaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        transaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        transaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.ProductBatch);
        transaction.setSellerId(token.getActiveTopUser().getUserId());
        transaction.setTransactionId(batch.getId());
        transaction.setInventoryId(inventory.getId());
        transaction.setOperationType(operationType);
        transaction.setSubSourceAction(subSourceAction);
        transaction.setRequestTime(DateTime.now().getMillis());
        transaction.setQuantity(quantity);
        transaction.setSubSourceAction(subSourceAction);
        transaction.setProductMap(productMap);
        transaction.setProductType(Product.ProductType.DERIVED);
        transaction.setUpdateBatchQuantity(updateBatchQuantity);

        transaction.setDerivedInventoryId(safeInventory.getId());
        transaction.setInventoryUpdate(inventoryUpdate);
        transaction.setPreviousInventoryId(previousInventoryId);
        transaction.setPreviousQuantity(previousQuantity);

        queuedTransactionRepository.save(transaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), transaction.getId());
    }

    /**
     * This is overrided method to get all product batch by productId or search term .
     * Search term may be batch status,batch number,metrc category
     * or  true or false for void status,archived,published,active
     *
     * @param productId
     * @param start
     * @param limit
     * @param term
     * @return
     */
    @Override
    public SearchResult<ProductBatch> getBatches(String productId, int start, int limit, String term, ProductBatch.BatchStatus status) {
        SearchResult<ProductBatch> batchResult = new SearchResult<>();

        if (StringUtils.isNotBlank(term)) {
            if (StringUtils.isNumericSpace(term)) {
                long batchNo = Long.parseLong(term);
                batchResult = productBatchRepository.getBatchesByBatchNo(token.getCompanyId(), token.getShopId(), start, limit, "{modified:-1}", batchNo);
            } else if (term.equalsIgnoreCase("true") || term.equalsIgnoreCase("false")) {
                boolean state = term.equalsIgnoreCase("true") ? true : false;
                batchResult = productBatchRepository.getBatchesByTermForStatus(token.getCompanyId(), token.getShopId(), start, limit, "{modified:-1}", state);
            } else {
                batchResult = productBatchRepository.getBatchesByTerm(token.getCompanyId(), token.getShopId(), start, limit, "{modified:-1}", term);
            }
        } else if (status != null) {
            batchResult = productBatchRepository.getBatchesProductByBatchStatus(token.getCompanyId(), token.getShopId(), productId, start, limit, "{modified:-1}", status);
        } else {
            batchResult = productBatchRepository.getBatches(token.getCompanyId(), token.getShopId(), productId, start, limit, "{modified:-1}", ProductBatch.class);
        }

        List<ObjectId> purchaseOrderIds = new ArrayList<>();
        for (ProductBatch productBatch : batchResult.getValues()) {
            if (StringUtils.isNotBlank(productBatch.getPoNumber()) && ObjectId.isValid(productBatch.getPoNumber()))
                purchaseOrderIds.add(new ObjectId(productBatch.getPoNumber()));
        }

        Iterable<PurchaseOrder> allPurchaseOrderById = purchaseOrderRepository.getAllPurchaseOrderById(token.getCompanyId(), token.getShopId(), purchaseOrderIds);
        Map<String, String> poNumberMap = new HashMap<>();
        if (allPurchaseOrderById != null) {
            for (PurchaseOrder purchaseOrder : allPurchaseOrderById) {
                if (purchaseOrder != null) {
                    if (!poNumberMap.containsKey(purchaseOrder.getId())) {
                        poNumberMap.put(purchaseOrder.getId(), purchaseOrder.getPoNumber());
                    }
                }
            }
        }

        for (ProductBatch productBatch : batchResult.getValues()) {
            if (productBatch != null && StringUtils.isNotBlank(productBatch.getPoNumber())) {
                if (poNumberMap.containsKey(productBatch.getPoNumber())) {
                    productBatch.setPoNumber(poNumberMap.get(productBatch.getPoNumber()));
                }
            }
        }

        return batchResult;
    }

    @Override
    public DateSearchResult<ProductBatch> getBatchesWithDates(long afterDate, long beforeDate) {
        return productBatchRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public void deleteBatch(String batchId) {
        if (StringUtils.isBlank(batchId)) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_EXIST);
        }
        ProductBatch dbBatch = productBatchRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), batchId);
        if (dbBatch == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_EXIST);
        } else if (dbBatch.isDeleted()) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "This batch is already deleted");
        }
        productBatchRepository.removeByIdSetState(token.getCompanyId(), batchId);
        batchActivityLogService.addBatchActivityLog(dbBatch.getId(), token.getActiveTopUser().getUserId(), dbBatch.getSku() + " Batch deleted");
        elasticSearchManager.deleteIndexedDocument(batchId, ProductBatch.class);

    }

    @Override
    public ProductBatch archive(BatchArchiveAddRequest request) {
        ProductBatch updatedProductBatch;
        ProductBatch dbProductBatch = productBatchRepository.get(token.getCompanyId(), request.getBatchId());
        if (dbProductBatch == null) {
            throw new BlazeInvalidArgException("Batch Archive", "Batch doesn't exist.");
        }

        if (dbProductBatch.getQuantity().intValue() > 0 && StringUtils.isBlank(request.getToBatchId())) {
            throw new BlazeInvalidArgException("Batch Archive", "toBatchId is empty");
        }

        String inventories = StringUtils.EMPTY;
        int count = 0;
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

        //Get batch quantities per inventory
        List<ProductBatchByInventory> quantities = batchQuantityRepository.getBatchQuantitiesByInventory(token.getCompanyId(), token.getShopId(), dbProductBatch.getId(), dbProductBatch.getProductId());
        Map<String, List<BatchQuantity>> quantityByInventoryMap = new HashMap<>();
        if (!CollectionUtils.isNullOrEmpty(quantities)) {
            for (ProductBatchByInventory quantity : quantities) {
                quantityByInventoryMap.putIfAbsent(quantity.getInventoryId(), new ArrayList<>());
                quantityByInventoryMap.get(quantity.getInventoryId()).addAll(quantity.getBatchQuantity());
            }
        }

        for (Map.Entry<String, Inventory> entry : inventoryMap.entrySet()) {
            if (entry.getValue() != null && entry.getValue().isActive()) {
                String inventoryId = entry.getKey();
                List<BatchQuantity> batchQuantities = quantityByInventoryMap.get(inventoryId);
                if (!CollectionUtils.isNullOrEmpty(batchQuantities)) {
                    for (BatchQuantity quantity : batchQuantities) {
                        if (quantity.getQuantity().doubleValue() > 0) {
                            if (StringUtils.isNotBlank(inventories)) {
                                inventories = inventories.concat(",");
                            }
                            inventories = inventories.concat(entry.getValue().getName());
                            count += 1;
                        }
                    }
                }
            }
        }

        if (count > 0) {
            throw new BlazeInvalidArgException("Batch Archive", "Batch can't be archived as it have active quantity in inventory " + inventories + ". So please reconcile it before archive");
        }

        //Transfer archive product batch quantity into another selected product batch
        if (StringUtils.isNotBlank(request.getToBatchId())) {
            ProductBatch dbToProductBatch = productBatchRepository.get(token.getCompanyId(), request.getToBatchId());
            BigDecimal productBatchQuantity = dbToProductBatch.getQuantity().add(dbProductBatch.getQuantity());
            dbToProductBatch.setQuantity(productBatchQuantity);
            productBatchRepository.update(token.getCompanyId(), dbToProductBatch.getId(), dbToProductBatch);
        }


        dbProductBatch.setArchived(true);
        dbProductBatch.setArchivedDate(DateTime.now().getMillis());
        dbProductBatch.setQuantity(new BigDecimal(0));

        updatedProductBatch = productBatchRepository.update(token.getCompanyId(), dbProductBatch.getId(), dbProductBatch);

        return updatedProductBatch;
    }

    @Override
    public ProductBatch publish(String batchId) {
        return null;
    }

    @Override
    public ProductBatch unpublish(String batchId) {
        return null;
    }

    @Override
    public void bulkTransferInventory(BulkInventoryTransferRequest request) {
        if (request.getTransfers().size() == 0) {
            throw new BlazeInvalidArgException("TransferRequest", "There's nothing to transfer.");
        }
        final String fromShopId = StringUtils.isBlank(request.getFromShopId()) ? token.getShopId() : request.getFromShopId();
        final String toShopId = StringUtils.isBlank(request.getToShopId()) ? token.getShopId() : request.getToShopId();
        final HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId());

        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(), fromShopId, QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException("TransferRequest", "There are active pending transactions. Please wait until all transactions are completed.");
        }*/
        Cart cart = new Cart();
        cart.prepare(token.getCompanyId());

        // Transfer
        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId());

        List<Product> pendingProducts = new ArrayList<>();
        List<ObjectId> objectIds = new ArrayList<>();
        for (InventoryTransferRequest transfer : request.getTransfers()) {
            if (!ObjectId.isValid(transfer.getProductId())) {
                throw new BlazeInvalidArgException("TransferRequest", transfer.getProductId() + " is an invalid id.");
            } else if (!inventoryHashMap.containsKey(transfer.getFromInventoryId())) {
                throw new BlazeInvalidArgException("TransferRequest", "Invalid 'from' inventory.");
            } else if (!inventoryHashMap.containsKey(transfer.getToInventoryId())) {
                throw new BlazeInvalidArgException("TransferRequest", "Invalid 'to' inventory.");
            } else if (transfer.getTransferAmount().doubleValue() <= 0) {
                throw new BlazeInvalidArgException("TransferRequest", "Transfer amount must be greater than 0.");
            }

            if (transfer.getFromInventoryId().equalsIgnoreCase(transfer.getToInventoryId())) {
                throw new BlazeInvalidArgException("TransferRequest", "Cannot transfer within the same inventory.");
            }
            objectIds.add(new ObjectId(transfer.getProductId()));
        }

        Iterable<Product> dbProducts = productRepository.findItemsIn(token.getCompanyId(), fromShopId, objectIds);
        HashMap<String, Product> productHashMap = new HashMap<>();
        for (Product p : dbProducts) {
            p.setCategory(productCategoryHashMap.get(p.getCompanyId()));
            productHashMap.put(p.getId(), p);
        }

        HashMap<String, Product> otherProductHashMap = new HashMap<>();
        boolean isTransferToAnotherShop = false;

        // Check if we're transfering to another shop
        if (StringUtils.isNotBlank(request.getToShopId())
                && !fromShopId.equalsIgnoreCase(request.getToShopId())) {
            isTransferToAnotherShop = true;
            // Find current set of products from other shop
            Iterable<Product> otherShopProducts = productRepository.listAllByShop(token.getCompanyId(), request.getToShopId());
            // Create product map with name_vendorId as key
            for (Product product : otherShopProducts) {
                product.setCategory(productCategoryHashMap.get(product.getCompanyId()));
                String key = product.getCompanyLinkId();
                otherProductHashMap.put(key, product);
            }
        }


        // Loop through the request transfers and do transfer one at a time
        for (InventoryTransferRequest transfer : request.getTransfers()) {
            Product dbProduct = productHashMap.get(transfer.getProductId());
            if (dbProduct == null) {
                throw new BlazeInvalidArgException("TransferRequest", "Product does not exist: " + transfer.getProductId());
            }

            // Check for same product
            Product otherProduct = null;
            if (isTransferToAnotherShop) {
                String key = dbProduct.getCompanyLinkId();
                otherProduct = otherProductHashMap.get(key);
            }
            transferInventory(transfer, dbProduct, otherProduct, pendingProducts);


            OrderItem order = new OrderItem();
            order.prepare(token.getCompanyId());
            order.setStatus(OrderItem.OrderItemStatus.Active);
            order.setCost(new BigDecimal(0));
            order.setQuantity(transfer.getTransferAmount());
            order.setFinalPrice(new BigDecimal(0));
            order.setProductId(dbProduct.getId());
            order.setPrepackageItemId(transfer.getPrepackageItemId());
            cart.getItems().add(order);

        }
        // Create Transaction Transfers
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), fromShopId, "TransactionPriority");
        String transNo = "" + sequence.getCount();
        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
        for (Product p : pendingProducts) {
            List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), p.getId());
            productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                    p, productPrepackageQuantities, String.format("Trans #%s: Transfer", transNo)));
            productRepository.update(token.getCompanyId(), p.getId(), p);
        }
        productChangeLogRepository.save(productChangeLogs);

        Transaction trans = new Transaction();
        trans.setActive(false);
        trans.setTransNo(transNo);
        trans.setMemberId(null);
        trans.setCompanyId(token.getCompanyId());
        trans.setShopId(fromShopId);
        trans.setQueueType(Transaction.QueueType.None);
        trans.setStatus(Transaction.TransactionStatus.Void);
        trans.setTerminalId(null);
        trans.setMemberId(null);
        trans.setCheckinTime(DateTime.now().getMillis());
        trans.setSellerId(token.getActiveTopUser().getUserId());
        trans.setCreatedById(token.getActiveTopUser().getUserId());
        trans.setPriority(sequence.getCount());
        trans.setTransferRequest(request);
        trans.setProcessedTime(DateTime.now().getMillis());
        trans.setCompletedTime(DateTime.now().getMillis());
        trans.setStartTime(DateTime.now().getMillis());
        trans.setEndTime(DateTime.now().getMillis());

        // Set cart
        cart.setPaymentOption(Cart.PaymentOption.None);
        trans.setCart(cart);
        trans.setTransType(Transaction.TransactionType.Transfer);
        trans.setTransferShopId(request.getToShopId());

        cart.setTotal(new BigDecimal(0));
        cart.setChangeDue(new BigDecimal(0));
        cart.setCashReceived(new BigDecimal(0));
        cart.setSubTotal(new BigDecimal(0));
        transactionRepository.save(trans);


        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(fromShopId,
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    private List<ProductChangeLog> transferInventory(final InventoryTransferRequest transfer,
                                                     Product fromProduct,
                                                     Product otherProduct,
                                                     List<Product> pendingProducts) {

        List<ProductChangeLog> changeLogs = new ArrayList<>();

        // Check for prepackageItemId
        if (StringUtils.isNotEmpty(transfer.getPrepackageItemId())
                && ObjectId.isValid(transfer.getPrepackageItemId())) {
            ProductPrepackageQuantity fromPreQuantity = quantityRepository.getQuantity(token.getCompanyId(),
                    fromProduct.getShopId(),
                    transfer.getFromInventoryId(),
                    transfer.getPrepackageItemId());
            if (fromPreQuantity == null) {
                throw new BlazeInvalidArgException("PrepackageProduct", "Prepackage Line Item does not exist.");
            }

            int transferAmount = transfer.getTransferAmount().intValue();
            if (fromPreQuantity.getQuantity() < transferAmount) {
                throw new BlazeInvalidArgException("TransferRequest", "Amount specified exceeds available inventory for product: " + fromProduct.getName());
            }


            ProductPrepackageQuantity toPreQuantity = null;
            if (otherProduct == null) {
                toPreQuantity = quantityRepository.getQuantity(token.getCompanyId(),
                        fromProduct.getShopId(),
                        transfer.getToInventoryId(),
                        transfer.getPrepackageItemId());

                if (toPreQuantity == null) {
                    // Create a new one
                    toPreQuantity = new ProductPrepackageQuantity();
                    toPreQuantity.prepare(token.getCompanyId());
                    toPreQuantity.setShopId(fromProduct.getShopId());
                    toPreQuantity.setProductId(fromProduct.getId());
                    toPreQuantity.setPrepackageItemId(transfer.getPrepackageItemId());
                    toPreQuantity.setInventoryId(transfer.getToInventoryId());
                    toPreQuantity.setPrepackageId(fromPreQuantity.getPrepackageId());

                    quantityRepository.save(toPreQuantity);
                }
            } else {
                // Do this if toProduct is not null
                Prepackage fromPrepackage = prepackageRepository.get(token.getCompanyId(), fromPreQuantity.getPrepackageId());
                if (fromPrepackage == null) {
                    throw new BlazeInvalidArgException("TransferRequest", "Prepackage not found for product: " + fromProduct.getName());
                }
                PrepackageProductItem fromPrepackageProductItem = prepackageProductItemRepository.get(token.getCompanyId(), transfer.getPrepackageItemId());
                if (fromPrepackageProductItem == null) {
                    throw new BlazeInvalidArgException("TransferRequest", "Prepackage Item not found for product: " + fromProduct.getName());
                }

                // Find matching prepackage with same name
                Prepackage toPrepackage = prepackageRepository.getPrepackage(token.getCompanyId(),
                        otherProduct.getShopId(), otherProduct.getId(), fromPrepackage.getName());

                if (toPrepackage == null) {
                    // Create one with similar attribute
                    toPrepackage = (new Kryo()).copy(fromPrepackage);
                    toPrepackage.setCompanyId(token.getCompanyId());
                    toPrepackage.setId(ObjectId.get().toString());
                    toPrepackage.setProductId(otherProduct.getId());
                    toPrepackage.setShopId(otherProduct.getShopId());
                    toPrepackage.setActive(true);

                    prepackageRepository.save(toPrepackage);
                }

                // Get to Prepackage
                PrepackageProductItem toPrepackageProductItem = prepackageProductItemRepository.getPrepackageProduct(token.getCompanyId(),
                        otherProduct.getShopId(),
                        otherProduct.getId(),
                        toPrepackage.getId(),
                        fromPrepackageProductItem.getBatchId());
                if (toPrepackageProductItem == null) {
                    // Creat a prepackage item with similar attribute
                    toPrepackageProductItem = (new Kryo()).copy(fromPrepackageProductItem);
                    toPrepackageProductItem.setId(ObjectId.get().toString());
                    toPrepackageProductItem.setShopId(otherProduct.getShopId());
                    toPrepackageProductItem.setProductId(otherProduct.getId());
                    toPrepackageProductItem.setPrepackageId(toPrepackage.getId());
                    toPrepackageProductItem.setActive(true);

                    prepackageProductItemRepository.save(toPrepackageProductItem);
                }

                // Now get the toQuantity
                toPreQuantity = quantityRepository.getQuantity(token.getCompanyId(),
                        otherProduct.getShopId(),
                        transfer.getToInventoryId(),
                        toPrepackageProductItem.getId());

                if (toPreQuantity == null) {
                    // Create a new one
                    toPreQuantity = new ProductPrepackageQuantity();
                    toPreQuantity.prepare(token.getCompanyId());
                    toPreQuantity.setShopId(otherProduct.getShopId());
                    toPreQuantity.setProductId(otherProduct.getId());
                    toPreQuantity.setPrepackageItemId(toPrepackageProductItem.getId());
                    toPreQuantity.setInventoryId(transfer.getToInventoryId());
                    toPreQuantity.setPrepackageId(toPrepackageProductItem.getPrepackageId());

                    quantityRepository.save(toPreQuantity);
                }

                // change logs
                List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), otherProduct.getId());
                changeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                        otherProduct, productPrepackageQuantities, "Transfer from another shop."));
            }


            int newQuantity = fromPreQuantity.getQuantity() - transferAmount;

            fromPreQuantity.setQuantity(newQuantity);

            int newToQuantity = toPreQuantity.getQuantity() + transferAmount;
            toPreQuantity.setQuantity(newToQuantity);

            // Now update accordingly
            quantityRepository.update(token.getCompanyId(), fromPreQuantity.getId(), fromPreQuantity);
            quantityRepository.update(token.getCompanyId(), toPreQuantity.getId(), toPreQuantity);

        } else {
            ProductQuantity fromQuantity = null;
            ProductQuantity toQuantity = null;
            for (ProductQuantity dbQuantity : fromProduct.getQuantities()) {
                if (dbQuantity.getInventoryId().equalsIgnoreCase(transfer.getFromInventoryId())) {
                    fromQuantity = dbQuantity;
                } else if (dbQuantity.getInventoryId().equalsIgnoreCase(transfer.getToInventoryId())) {
                    toQuantity = dbQuantity;
                }
            }

            // If toProduct is not null, then find the toQuantity
            if (otherProduct != null) {
                toQuantity = null;
                for (ProductQuantity dbQuantity : otherProduct.getQuantities()) {
                    if (dbQuantity.getInventoryId().equalsIgnoreCase(transfer.getToInventoryId())) {
                        toQuantity = dbQuantity;
                        break;
                    }
                }
            }

            if (fromQuantity == null) {
                throw new BlazeInvalidArgException("TransferRequest", "From ProductQuantity does not exist for: " + fromProduct.getName());
            }

            if (toQuantity == null) {
                toQuantity = new ProductQuantity();
                toQuantity.setCompanyId(token.getCompanyId());
                toQuantity.setId(ObjectId.get().toString());
                toQuantity.setInventoryId(transfer.getToInventoryId());


                if (otherProduct == null) {
                    toQuantity.setShopId(fromProduct.getShopId());
                    fromProduct.getQuantities().add(toQuantity);
                } else {
                    // Sending this to another product
                    toQuantity.setShopId(otherProduct.getShopId());
                    otherProduct.getQuantities().add(toQuantity);
                }
            }

            if (fromQuantity.getQuantity().doubleValue() < transfer.getTransferAmount().doubleValue()) {
                throw new BlazeInvalidArgException("TransferRequest", "Amount specified exceeds available inventory for product: " + fromProduct.getName());
            }
            double newQuantity = fromQuantity.getQuantity().doubleValue() - transfer.getTransferAmount().doubleValue();

            fromQuantity.setQuantity(new BigDecimal(newQuantity)); //NumberUtils.round(newQuantity, 2));

            double newToQuantity = toQuantity.getQuantity().doubleValue() + transfer.getTransferAmount().doubleValue();
            toQuantity.setQuantity(new BigDecimal(newToQuantity)); //NumberUtils.round(newToQuantity, 2));

            pendingProducts.add(fromProduct);

            batchQuantityService.subtractBatchQuantity(fromProduct.getCompanyId(),
                    fromProduct.getShopId(),
                    fromProduct.getId(),
                    transfer.getFromInventoryId(),
                    transfer.getFromBatchId(),
                    transfer.getTransferAmount());

            if (otherProduct != null) {
                pendingProducts.add(otherProduct);

                batchQuantityService.addBatchQuantity(otherProduct.getCompanyId(),
                        otherProduct.getShopId(),
                        otherProduct.getId(),
                        transfer.getToInventoryId(),
                        transfer.getToBatchId(),
                        transfer.getTransferAmount());


                List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), otherProduct.getId());
                changeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                        otherProduct, productPrepackageQuantities, "Transfer from another shop."));
            } else {
                batchQuantityService.addBatchQuantity(fromProduct.getCompanyId(),
                        fromProduct.getShopId(),
                        fromProduct.getId(),
                        transfer.getToInventoryId(),
                        transfer.getToBatchId(),
                        transfer.getTransferAmount());
            }


        }
        return changeLogs;
    }

    @Override
    public void reportLoss(ReportLossRequest request) {
        Product product = productRepository.get(token.getCompanyId(), request.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("ProductId", "Product does not exist.");
        }

        Inventory inventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        if (inventory == null) {
            throw new BlazeInvalidArgException("ReportLoss", "Safe Inventory is not found.");
        }

        if (product.getQuantities() == null || product.getQuantities().isEmpty()) {
            throw new BlazeInvalidArgException("ReportLoss", "Product Quantity not available.");
        }

        double difference = 0;
        double originalQuantity = 0;
        double loss = 0;
        for (ProductQuantity productQuantity : product.getQuantities()) {
            if (inventory.getId().equals(productQuantity.getInventoryId())) {
                originalQuantity = productQuantity.getQuantity().doubleValue();
                loss = request.getQuantity().doubleValue();
                difference = originalQuantity - loss;
                break;
            }
        }

        if (difference < 0) {
            throw new BlazeInvalidArgException("ReportLoss", RECONCILIATION_LOSS_NOT_ALLOWED);
        }

        ReconciliationHistory reconciliationHistory = new ReconciliationHistory();
        reconciliationHistory.prepare(token.getCompanyId());
        reconciliationHistory.setShopId(token.getShopId());
        reconciliationHistory.setEmployeeId(token.getActiveTopUser().getUserId());

        ProductLossRequest productLossRequest = new ProductLossRequest();
        productLossRequest.setProductId(request.getProductId());
        productLossRequest.setQuantity(request.getQuantity());

        ReportLossListRequest reportLossListRequest = new ReportLossListRequest();
        reportLossListRequest.setInventoryId(inventory.getId());
        reportLossListRequest.setNote(request.getNote());
        reportLossListRequest.setTerminalId(null);
        reportLossListRequest.setReportLoss(true);
        reportLossListRequest.setProductLossRequests(Lists.newArrayList(productLossRequest));

        reconciliationHistory(reconciliationHistory, productLossRequest, reportLossListRequest, originalQuantity, difference);

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), "TransactionPriority");
        Transaction trans = new Transaction();
        trans.setActive(false);
        trans.setTransNo("" + sequence.getCount());
        trans.setMemberId(null);
        trans.setCompanyId(token.getCompanyId());
        trans.setShopId(token.getShopId());
        trans.setQueueType(Transaction.QueueType.None);
        trans.setStatus(Transaction.TransactionStatus.Void);
        trans.setTerminalId(null);
        trans.setMemberId(null);
        trans.setCheckinTime(DateTime.now().getMillis());
        trans.setSellerId(token.getActiveTopUser().getUserId());
        trans.setCreatedById(token.getActiveTopUser().getUserId());
        trans.setPriority(sequence.getCount());
        trans.setStartTime(DateTime.now().getMillis());
        trans.setEndTime(DateTime.now().getMillis());

        // Add note
        Note note = new Note();
        note.prepare();
        note.setWriterId(token.getActiveTopUser().getUserId());
        note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        note.setMessage(request.getNote());
        trans.setNote(note);

        // Set cart
        Cart cart = new Cart();
        cart.setId(ObjectId.get().toString());
        cart.setCompanyId(token.getCompanyId());
        cart.setPaymentOption(Cart.PaymentOption.None);
        trans.setCart(cart);
        trans.setTransType(Transaction.TransactionType.Adjustment);
        transactionRepository.save(trans);


        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.prepare(token.getCompanyId());
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCart(cart);
        queuedTransaction.setTransactionId(trans.getId());
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setMemberId(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.Void);
        queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setReconcilliationHistoryId(reconciliationHistory.getId());

        cart.setTotal(new BigDecimal(0));
        cart.setChangeDue(new BigDecimal(0));
        cart.setCashReceived(new BigDecimal(0));
        cart.setSubTotal(new BigDecimal(0));

        OrderItem order = new OrderItem();
        order.prepare(token.getCompanyId());
        order.setStatus(OrderItem.OrderItemStatus.Active);
        order.setCost(new BigDecimal(0));
        order.setQuantity(request.getQuantity());
        order.setFinalPrice(new BigDecimal(0));
        order.setProductId(product.getId());
        cart.getItems().add(order);
        queuedTransactionRepository.save(queuedTransaction);

        reconciliationHistoryRepository.save(reconciliationHistory);
        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
    }

    /**
     * We are manage reconciliation history
     *
     * @param reconciliationHistory : reconciliationHistory
     * @param productLossRequest    : productLossRequest
     * @param reportLossListRequest : reportLossListRequest
     * @param oldQuantity           : oldQuantity
     * @param newQuantity           : newQuantity
     */
    private void reconciliationHistory(ReconciliationHistory reconciliationHistory, ProductLossRequest productLossRequest, ReportLossListRequest reportLossListRequest, double oldQuantity, double newQuantity) {
        String historyNo = null;
        List<ReconciliationHistoryList> reconciliations = reconciliationHistory.getReconciliations();
        if (reconciliations == null) {
            reconciliations = new ArrayList<>();
        }

        ReconciliationHistoryList reconciliationHistoryList = new ReconciliationHistoryList();
        reconciliationHistoryList.setProductId(productLossRequest.getProductId());
        reconciliationHistoryList.setOldQuantity(BigDecimal.valueOf(oldQuantity));
        reconciliationHistoryList.setNewQuantity(BigDecimal.valueOf(newQuantity));
        reconciliationHistoryList.setPrePackageItemId(productLossRequest.getPrepackageItemId());
        reconciliationHistoryList.setTerminalId(reportLossListRequest.getTerminalId());
        reconciliationHistoryList.setInventoryId(reportLossListRequest.getInventoryId());
        reconciliationHistoryList.setReportLoss(reportLossListRequest.isReportLoss());
        reconciliationHistoryList.setReconciliationReason(productLossRequest.getReconciliationReason());
        if (ReconciliationHistoryList.ReconciliationReason.OTHER.equals(productLossRequest.getReconciliationReason()) && StringUtils.isNotBlank(productLossRequest.getNote())) {
            Note note = new Note();
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            note.setMessage(productLossRequest.getNote());
            reconciliationHistoryList.setNote(note);
        }
        if (StringUtils.isNotBlank(productLossRequest.getBatchId())) {
            reconciliationHistoryList.setBatchId(productLossRequest.getBatchId());
        }

        if (StringUtils.isNotBlank(productLossRequest.getBatchSku())) {
            reconciliationHistoryList.setBatchSku(productLossRequest.getBatchSku());
        }
        reconciliations.add(reconciliationHistoryList);
        CompanyUniqueSequence sequence = companyUniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), historyNo);
        if (sequence.getCount() <= 1) {
            // check if there are any purchase order for the company
            long count = purchaseOrderRepository.count(token.getCompanyId());
            if (count > 0) {
                sequence.setCount(100);
                companyUniqueSequenceRepository.update(token.getCompanyId(), sequence.getId(), sequence);
            }
        }
        sequence = companyUniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), historyNo);
        reconciliationHistory.setRequestNo(sequence.getCount());
        reconciliationHistory.setBatchReconcile(reportLossListRequest.isBatchReconcile());


    }

    /**
     * We do not allow positive reconcilications
     *
     * @param request
     */
    @Override
    public void inventoryReconciliation(ReportLossListRequest request) {
        ReconciliationHistory reconciliationHistory = new ReconciliationHistory();

        //Validate if inventory is correct
        Inventory inventory = null;
        if (StringUtils.isBlank(request.getInventoryId()) || (inventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getInventoryId())) == null) {
            throw new BlazeInvalidArgException("Inventory", "Invalid inventory");
        }

        List<ProductChangeLog> productChangeLogs = new ArrayList<>();

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> prepackageItemIds = new ArrayList<>();
        List<String> prepackageItemIdList = new ArrayList<>();
        for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
            if (StringUtils.isNotEmpty(productLossRequest.getProductId()) && ObjectId.isValid(productLossRequest.getProductId())) {
                productIds.add(new ObjectId(productLossRequest.getProductId()));
            }
            if (StringUtils.isNotEmpty(productLossRequest.getPrepackageItemId()) && ObjectId.isValid(productLossRequest.getPrepackageItemId())) {
                prepackageItemIds.add(new ObjectId(productLossRequest.getPrepackageItemId()));
                prepackageItemIdList.add(productLossRequest.getPrepackageItemId());
            }
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, PrepackageProductItem> prepackageItemMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), prepackageItemIds);
        HashMap<String, ProductPrepackageQuantity> quantityHashMap = quantityRepository.getQuantitiesForPrepackageItems(token.getCompanyId(), token.getShopId(), request.getInventoryId(), prepackageItemIdList);

        //Validate reconciliation
        Map<String, ProductQuantity> productQuantityMap = new HashMap<>();
        for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
            Product product = productMap.get(productLossRequest.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_EXIST);
            }

            if (!StringUtils.isBlank(productLossRequest.getPrepackageItemId())) {

                ProductPrepackageQuantity quantity = quantityHashMap.get(productLossRequest.getPrepackageItemId());
                if (quantity != null) {
                    int actualQuantity = productLossRequest.getQuantity().intValue();
                    int difference = quantity.getQuantity() - actualQuantity;

                    //Allow zero difference conciliation
                    /*if (difference == 0) {
                        throw new BlazeInvalidArgException(RECONCILIATION, RECONCILIATION_NOT_ALLOWED);
                    }*/

                    if (request.isReportLoss()) {
                        if (difference <= 0) {
                            throw new BlazeInvalidArgException(RECONCILIATION, RECONCILIATION_LOSS_NOT_ALLOWED);
                        }
                    }
                }
            } else {
                List<ProductQuantity> productQuantities = product.getQuantities();
                ProductQuantity targetPQ = null;
                for (ProductQuantity productQuantity : productQuantities) {
                    if (productQuantity.getInventoryId().equals(request.getInventoryId())) {
                        targetPQ = productQuantity;
                        break;
                    }
                }

                productQuantityMap.put(product.getId(), targetPQ);

                double currentQuantity = targetPQ != null ? targetPQ.getQuantity().doubleValue() : 0;
                double actualQuantity = productLossRequest.getQuantity().doubleValue();
                double difference = currentQuantity - actualQuantity;

                //Allow zero difference conciliation
                /*if (difference == 0) {
                    throw new BlazeInvalidArgException(RECONCILIATION, RECONCILIATION_NOT_ALLOWED);
                }*/

                if (request.isReportLoss()) {
                    if (difference <= 0) {
                        throw new BlazeInvalidArgException(RECONCILIATION, RECONCILIATION_LOSS_NOT_ALLOWED);
                    }
                }

            }

        }

        /* Prepare reconcilliation history*/
        reconciliationHistory.prepare(token.getCompanyId());
        reconciliationHistory.setShopId(token.getShopId());
        reconciliationHistory.setEmployeeId(token.getActiveTopUser().getUserId());

        if (!request.isReportLoss()) {

            for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
                if (!StringUtils.isBlank(productLossRequest.getPrepackageItemId())
                        && ObjectId.isValid(productLossRequest.getPrepackageItemId())) {
                    ProductPrepackageQuantity quantity = quantityHashMap.get(productLossRequest.getPrepackageItemId());
                    if (quantity == null) {
                        reconciliationHistory(reconciliationHistory, productLossRequest, request, 0, Double.parseDouble(String.valueOf(productLossRequest.getQuantity().intValue())));
                    } else {
                        reconciliationHistory(reconciliationHistory, productLossRequest, request, quantity.getQuantity(), Double.parseDouble(String.valueOf(productLossRequest.getQuantity().intValue())));
                    }

                    /*ProductPrepackageQuantity quantity = quantityHashMap.get(productLossRequest.getPrepackageItemId());

                    if (quantity == null) {
                        PrepackageProductItem prepackageProductItem = prepackageItemMap.get(productLossRequest.getPrepackageItemId());
                        // create a new one
                        if (prepackageProductItem != null) {
                            quantity = new ProductPrepackageQuantity();
                            quantity.prepare(token.getCompanyId());
                            quantity.setShopId(token.getShopId());
                            quantity.setProductId(productLossRequest.getProductId());
                            quantity.setPrepackageItemId(productLossRequest.getPrepackageItemId());
                            quantity.setInventoryId(request.getInventoryId());
                            quantity.setPrepackageId(prepackageProductItem.getPrepackageId());
                            quantity.setQuantity(productLossRequest.getQuantity().intValue());
                            quantityRepository.save(quantity);

                            int oldQuantity = 0;
                            int actualQuantity = productLossRequest.getQuantity().intValue();
                            reconciliationHistory(reconciliationHistory,productLossRequest,request, oldQuantity,Double.parseDouble(String.valueOf(actualQuantity)));
                        }
                    } else  if (quantity != null) {
                        int actualQuantity = productLossRequest.getQuantity().intValue();

                        int difference = quantity.getQuantity() - actualQuantity;
                        reconciliationHistory(reconciliationHistory,productLossRequest,request, quantity.getQuantity(),Double.parseDouble(String.valueOf(actualQuantity)));

                        if(difference != 0) {
                            quantity.setQuantity(productLossRequest.getQuantity().intValue());
                        }

                        quantityRepository.update(token.getCompanyId(), quantity.getId(), quantity);
                    }*/

                } else {
                    String productId = productLossRequest.getProductId();
                    Product product = productMap.get(productId);

                    ProductQuantity targetPQ = productQuantityMap.get(productId);

                    if (targetPQ == null) {
                        // create one for this inventory
                        targetPQ = new ProductQuantity();
                        targetPQ.prepare(token.getCompanyId());
                        targetPQ.setShopId(token.getShopId());
                        targetPQ.setInventoryId(request.getInventoryId());
                        product.getQuantities().add(targetPQ);
                    }

                    // just replace the actual quantity outright
                    double currentQuantity = targetPQ.getQuantity().doubleValue();
                    double actualQuantity = productLossRequest.getQuantity().doubleValue();
                    reconciliationHistory(reconciliationHistory, productLossRequest, request, currentQuantity, actualQuantity);

                    targetPQ.setQuantity(new BigDecimal(actualQuantity));
                    // check diff
                    double difference = currentQuantity - actualQuantity;

                    difference = NumberUtils.round(difference, 2);

                    /*if (difference > 0) {
                        // actual is less, so we're reporting a loss
                        batchQuantityService.subtractBatchQuantity(token.getCompanyId(), token.getShopId(), productId, request.getInventoryId(), null, new BigDecimal(difference),null,reconciliationHistory);
                    } else if (difference < 0){
                        batchQuantityService.addBatchQuantity(token.getCompanyId(), token.getShopId(), productId, request.getInventoryId(), null, new BigDecimal(difference),null,reconciliationHistory);
                    }

                    List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), product.getId());
                    productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                            product, productPrepackageQuantities, "Inventory Reconciliation: #" + (reconciliationHistory.getRequestNo() != null ? reconciliationHistory.getRequestNo() : "")));
                    productRepository.update(token.getCompanyId(), product.getId(), product);*/
                }

            }
        } else {
            // In case of Loss
            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), "TransactionPriority");
            Transaction trans = new Transaction();
            trans.setActive(false);
            trans.setTransNo("" + sequence.getCount());
            trans.setMemberId(null);
            trans.setCompanyId(token.getCompanyId());
            trans.setShopId(token.getShopId());
            trans.setQueueType(Transaction.QueueType.None);
            trans.setStatus(Transaction.TransactionStatus.Void);
            trans.setTerminalId(request.getTerminalId());
            trans.setMemberId(null);
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());
            trans.setCreatedById(token.getActiveTopUser().getUserId());
            trans.setPriority(sequence.getCount());
            trans.setStartTime(DateTime.now().getMillis());
            trans.setEndTime(DateTime.now().getMillis());
            trans.setOverrideInventoryId(request.getInventoryId());

            // Add note
            Note note = new Note();
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            note.setMessage(request.getNote());
            trans.setNote(note);

            // Set cart
            Cart cart = new Cart();
            cart.setId(ObjectId.get().toString());
            cart.setCompanyId(token.getCompanyId());
            cart.setPaymentOption(Cart.PaymentOption.None);
            trans.setCart(cart);
            trans.setTransType(Transaction.TransactionType.Adjustment);
            transactionRepository.save(trans);

            QueuedTransaction queuedTransaction = new QueuedTransaction();
            queuedTransaction.prepare(token.getCompanyId());
            queuedTransaction.setShopId(token.getShopId());
            queuedTransaction.setCart(cart);
            queuedTransaction.setTransactionId(trans.getId());
            queuedTransaction.setTerminalId(request.getTerminalId());
            queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
            queuedTransaction.setMemberId(null);
            queuedTransaction.setPendingStatus(Transaction.TransactionStatus.Void);
            queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
            queuedTransaction.setRequestTime(DateTime.now().getMillis());
            queuedTransaction.setReconcilliationHistoryId(reconciliationHistory.getId());

            cart.setTotal(new BigDecimal(0));
            cart.setChangeDue(new BigDecimal(0));
            cart.setCashReceived(new BigDecimal(0));
            cart.setSubTotal(new BigDecimal(0));

            for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
                Product product = productMap.get(productLossRequest.getProductId());

                boolean productUpdated = false;
                if (StringUtils.isNotBlank(productLossRequest.getPrepackageItemId())) {
                    // we are now submitting a loss for the prepackage
                    // FIND THE PREPACKAGE
                    ProductPrepackageQuantity quantity = quantityHashMap.get(productLossRequest.getPrepackageItemId());

                    if (quantity != null) {

                        int actualQuantity = productLossRequest.getQuantity().intValue();
                        int difference = quantity.getQuantity() - actualQuantity;

                        if (difference > 0) {
                            reconciliationHistory(reconciliationHistory, productLossRequest, request, quantity.getQuantity(), Double.parseDouble(String.valueOf(actualQuantity)));
                            reconciliationHistory.setTransNo(trans.getTransNo());

                            OrderItem orderItem = new OrderItem();
                            orderItem.prepare(token.getCompanyId());
                            orderItem.setStatus(OrderItem.OrderItemStatus.Active);
                            orderItem.setCost(new BigDecimal(0));
                            orderItem.setQuantity(BigDecimal.valueOf(difference));    // Reporting difference as a loss transaction
                            orderItem.setFinalPrice(new BigDecimal(0));
                            orderItem.setProductId(product.getId());
                            orderItem.setPrepackageItemId(quantity.getPrepackageItemId());
                            orderItem.setBatchId(productLossRequest.getBatchId());
                            cart.getItems().add(orderItem);
                            productUpdated = true;
                        }

                        if (difference < 0) {
                            throw new BlazeInvalidArgException("Reconciliation", "Positive LOSS reconciliation is not allowed.");
                        }
                    }

                    break;
                } else {

                    ProductQuantity productQuantity = productQuantityMap.get(product.getId());
                    if (productQuantity != null && productQuantity.getInventoryId().equals(request.getInventoryId())) {

                        double currentQuantity = productQuantity.getQuantity().doubleValue();

                        double actualQuantity = productLossRequest.getQuantity().doubleValue();

                        double difference = currentQuantity - actualQuantity;
                        reconciliationHistory(reconciliationHistory, productLossRequest, request, currentQuantity, actualQuantity);

                        // if difference is greater than 0, then we have a loss, since incoming actual is less than current
                        // if difference is less than 0, then we are reporting more.
                        if (difference > 0) {
                            OrderItem orderItem = new OrderItem();
                            orderItem.prepare(token.getCompanyId());
                            orderItem.setStatus(OrderItem.OrderItemStatus.Active);
                            orderItem.setCost(new BigDecimal(0));
                            orderItem.setQuantity(BigDecimal.valueOf(difference));    // Reporting difference as a loss transaction
                            orderItem.setFinalPrice(new BigDecimal(0));
                            orderItem.setProductId(product.getId());

                            cart.getItems().add(orderItem);

                            productUpdated = true;
                        }
                        if (difference < 0) {
                            throw new BlazeInvalidArgException("Reconciliation", "Positive LOSS reconciliation is not allowed.");
                        }
                    }
                }
                if (productUpdated) {
                    List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), product.getId());
                    productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                            product, productPrepackageQuantities, "Inventory Reconciliation: #" + (reconciliationHistory.getRequestNo() != null ? reconciliationHistory.getRequestNo() : "")));
                    productRepository.update(token.getCompanyId(), product.getId(), product);
                }
            }
            if (cart.getItems().size() > 0) {
                queuedTransactionRepository.save(queuedTransaction);
                backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
            }
        }

        reconciliationHistoryRepository.save(reconciliationHistory);

        if (!request.isReportLoss()) {
            createQueueTransactionJobForReconciliation(reconciliationHistory);
        }

        // there are change logs, let's persist it
        if (productChangeLogs.size() > 0) {
            productChangeLogRepository.save(productChangeLogs);
        }
    }

    /**
     * Reconciliation by batch
     * We allow reconciliation in two ways : Only reconciliation and with report loss
     *
     * @param request
     */
    @Override
    public void inventoryReconciliationByBatch(ReportLossListRequest request) {
        ReconciliationHistory reconciliationHistory = new ReconciliationHistory();

        //Validate if inventory is correct
        Inventory inventory = null;
        if (StringUtils.isBlank(request.getInventoryId()) || (inventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getInventoryId())) == null) {
            throw new BlazeInvalidArgException("Inventory", "Invalid inventory");
        }

        List<ProductChangeLog> productChangeLogs = new ArrayList<>();

        //Collect required object (Product and Product batch)
        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> productBatchIds = new ArrayList<>();
        for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
            if (StringUtils.isNotEmpty(productLossRequest.getProductId()) && ObjectId.isValid(productLossRequest.getProductId())) {
                productIds.add(new ObjectId(productLossRequest.getProductId()));
            }
            if (StringUtils.isNotEmpty(productLossRequest.getBatchId()) && ObjectId.isValid(productLossRequest.getBatchId())) {
                productBatchIds.add(new ObjectId(productLossRequest.getBatchId()));
            }
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, ProductBatch> batchMap = productBatchRepository.listAsMap(token.getCompanyId(), productBatchIds);

        HashMap<String, BigDecimal> batchQuantityMap = new HashMap<>();
        //Validate required information
        for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
            Product product = productMap.get(productLossRequest.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_EXIST);
            }

            if (StringUtils.isBlank(productLossRequest.getPrepackageItemId())) {
                //Reconciliation by product batch
                BatchQuantity batchQuantity = batchQuantityRepository.getBatchQuantityForInventory(token.getCompanyId(), token.getShopId(), product.getId(), inventory.getId(), productLossRequest.getBatchId());
                if (batchQuantity == null) {
                    batchQuantity = new BatchQuantity();
                }

                if (batchQuantityMap.containsKey(productLossRequest.getBatchId())) {
                    BigDecimal updateQuantity = batchQuantityMap.get(productLossRequest.getBatchId()).add(productLossRequest.getQuantity());
                    batchQuantityMap.put(productLossRequest.getBatchId(), updateQuantity);
                } else {
                    batchQuantityMap.put(productLossRequest.getBatchId(), batchQuantity.getQuantity());
                }

                double currentQuantity = batchQuantity.getQuantity().doubleValue();
                if (productLossRequest.getQuantity() == null) {
                    productLossRequest.setQuantity(BigDecimal.ZERO);
                }
                double actualQuantity = productLossRequest.getQuantity().doubleValue();
                double difference = currentQuantity - actualQuantity;

                //Allow zero difference conciliation
                /*if (difference == 0) {
                    throw new BlazeInvalidArgException(RECONCILIATION, RECONCILIATION_NOT_ALLOWED);
                }*/

                if (request.isReportLoss()) {
                    if (difference < 0) {
                        throw new BlazeInvalidArgException(RECONCILIATION, RECONCILIATION_LOSS_NOT_ALLOWED);
                    }
                }
            }

        }

        /* Prepare reconciliation history first for queue transaction job*/
        reconciliationHistory.prepare(token.getCompanyId());
        reconciliationHistory.setShopId(token.getShopId());
        reconciliationHistory.setEmployeeId(token.getActiveTopUser().getUserId());

        //Perform inventory reconciliation
        if (!request.isReportLoss() && request.isBatchReconcile()) {
            //Inventory reconciliation by product batch

            for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
                Product product = productMap.get(productLossRequest.getProductId());

                if (!StringUtils.isBlank(productLossRequest.getPrepackageItemId())
                        && ObjectId.isValid(productLossRequest.getPrepackageItemId())) {

                    this.inventoryReconciliation(request);


                } else {
                    ProductBatch dbProductBatch = batchMap.get(productLossRequest.getBatchId());

                    BigDecimal batchQuantity = batchQuantityMap.get(productLossRequest.getBatchId());

                    double currentQuantity = batchQuantity.doubleValue();
                    double actualQuantity = productLossRequest.getQuantity().doubleValue();

                    double difference = NumberUtils.round((currentQuantity - actualQuantity), 2);

                    reconciliationHistory(reconciliationHistory, productLossRequest, request, currentQuantity, actualQuantity);

                    List<ProductQuantity> productQuantityList = product.getQuantities();
                    if (productQuantityList == null) {
                        productQuantityList = new ArrayList<>();
                    }

                    ProductQuantity targetProductQuantity = null;
                    //Get product quantity for requested inventory which needs to update
                    for (ProductQuantity productQuantity : productQuantityList) {
                        if (productQuantity.getInventoryId().equals(request.getInventoryId())) {
                            targetProductQuantity = productQuantity;
                            break;
                        }
                    }

                    if (targetProductQuantity == null) {
                        targetProductQuantity = new ProductQuantity();
                        targetProductQuantity.prepare(token.getCompanyId());
                        targetProductQuantity.setShopId(token.getShopId());
                        targetProductQuantity.setInventoryId(request.getInventoryId());

                        product.getQuantities().add(targetProductQuantity);
                    }

                    targetProductQuantity.setQuantity(new BigDecimal(targetProductQuantity.getQuantity().doubleValue() - difference));

                    /*if(difference > 0) {
                        batchQuantityService.subtractBatchQuantity(token.getCompanyId(), token.getShopId(), product.getId(), inventory.getId(), dbProductBatch.getId(), new BigDecimal(difference));
                    } else if (difference < 0){
                        batchQuantityService.addBatchQuantity(token.getCompanyId(), token.getShopId(), product.getId(), inventory.getId(), dbProductBatch.getId(), new BigDecimal(difference).abs());
                    }

                    productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                            product, null, "Inventory Reconciliation: #" + (reconciliationHistory.getRequestNo() != null ? reconciliationHistory.getRequestNo() : "")));
                    productRepository.update(token.getCompanyId(), product.getId(), product);*/
                }
            }
        } else if (request.isReportLoss() && request.isBatchReconcile()) {
            // Report loss for reconciliation by product batch
            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), "TransactionPriority");
            Transaction trans = new Transaction();
            trans.setActive(false);
            trans.setTransNo("" + sequence.getCount());
            trans.setMemberId(null);
            trans.setCompanyId(token.getCompanyId());
            trans.setShopId(token.getShopId());
            trans.setQueueType(Transaction.QueueType.None);
            trans.setStatus(Transaction.TransactionStatus.Void);
            trans.setTerminalId(request.getTerminalId());
            trans.setMemberId(null);
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());
            trans.setPriority(sequence.getCount());
            trans.setStartTime(DateTime.now().getMillis());
            trans.setEndTime(DateTime.now().getMillis());
            trans.setOverrideInventoryId(request.getInventoryId());
            trans.setSellerTerminalId(request.getTerminalId());
            // Add note
            Note note = new Note();
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            note.setMessage(request.getNote());
            trans.setNote(note);

            // Set cart
            Cart cart = new Cart();
            cart.setId(ObjectId.get().toString());
            cart.setCompanyId(token.getCompanyId());
            cart.setPaymentOption(Cart.PaymentOption.None);
            trans.setCart(cart);
            trans.setTransType(Transaction.TransactionType.Adjustment);
            transactionRepository.save(trans);

            QueuedTransaction queuedTransaction = new QueuedTransaction();
            queuedTransaction.prepare(token.getCompanyId());
            queuedTransaction.setShopId(token.getShopId());
            queuedTransaction.setCart(cart);
            queuedTransaction.setTransactionId(trans.getId());
            queuedTransaction.setTerminalId(request.getTerminalId());
            queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
            queuedTransaction.setMemberId(null);
            queuedTransaction.setPendingStatus(Transaction.TransactionStatus.Void);
            queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
            queuedTransaction.setRequestTime(DateTime.now().getMillis());
            queuedTransaction.setReconcilliationHistoryId(reconciliationHistory.getId());

            cart.setTotal(new BigDecimal(0));
            cart.setChangeDue(new BigDecimal(0));
            cart.setCashReceived(new BigDecimal(0));
            cart.setSubTotal(new BigDecimal(0));

            for (ProductLossRequest productLossRequest : request.getProductLossRequests()) {
                Product product = productMap.get(productLossRequest.getProductId());
                if (StringUtils.isNotBlank(productLossRequest.getPrepackageItemId())) {
                    this.inventoryReconciliation(request);
                } else {

                    ProductBatch dbProductBatch = batchMap.get(productLossRequest.getBatchId());
                    ProductQuantity productQuantity = null;
                    //Get product quantity for requested inventory
                    for (ProductQuantity quantity : product.getQuantities()) {
                        if (quantity.getInventoryId().equals(request.getInventoryId())) {
                            productQuantity = quantity;
                            break;
                        }
                    }

                    BatchQuantity batchQuantity = batchQuantityRepository.getBatchQuantityForInventory(token.getCompanyId(), token.getShopId(), product.getId(), inventory.getId(), dbProductBatch.getId());
                    if (batchQuantity == null) {
                        batchQuantity = new BatchQuantity();
                    }

                    if (productQuantity != null) {
                        double currentQuantity = batchQuantity.getQuantity().doubleValue();
                        double actualQuantity = productLossRequest.getQuantity().doubleValue();

                        double difference = currentQuantity - actualQuantity;
                        reconciliationHistory(reconciliationHistory, productLossRequest, request, currentQuantity, actualQuantity);

                        if (difference > 0) {
                            OrderItem orderItem = new OrderItem();
                            orderItem.prepare(token.getCompanyId());
                            orderItem.setStatus(OrderItem.OrderItemStatus.Active);
                            orderItem.setCost(new BigDecimal(0));
                            orderItem.setQuantity(BigDecimal.valueOf(difference));    // Reporting difference as a loss transaction
                            orderItem.setFinalPrice(new BigDecimal(0));
                            orderItem.setBatchId(dbProductBatch.getId());
                            orderItem.setProductId(product.getId());

                            cart.getItems().add(orderItem);

                            List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), product.getId());
                            productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                                    product, productPrepackageQuantities, "Inventory Reconciliation: #" + (reconciliationHistory.getRequestNo() != null ? reconciliationHistory.getRequestNo() : "")));
                            productRepository.update(token.getCompanyId(), product.getId(), product);
                        }
                    }
                }
            }

            if (cart.getItems().size() > 0) {
                queuedTransactionRepository.save(queuedTransaction);
                backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
            }

        }


        reconciliationHistoryRepository.save(reconciliationHistory);

        if (!request.isReportLoss()) {
            createQueueTransactionJobForReconciliation(reconciliationHistory);
        }

        // there are change logs, let's persist it
        if (productChangeLogs.size() > 0) {
            productChangeLogRepository.save(productChangeLogs);
        }
    }

    private void createQueueTransactionJobForReconciliation(ReconciliationHistory reconciliationHistory) {

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.InventoryReconciliation);
        queuedTransaction.setSellerId(reconciliationHistory.getEmployeeId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(reconciliationHistory.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());

    }

    /**
     * Get product batch of product with quantity
     *
     * @param productId : product id
     * @param start     : start
     * @param limit     : limit
     */
    @Override
    public SearchResult<ProductBatchQuantityResult> getProductBatchesWithQuantity(String productId, int start, int limit) {

        Product product = productRepository.get(token.getCompanyId(), productId);
        if (product == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_EXIST);
        }

        SearchResult<ProductBatchQuantityResult> result = productBatchRepository.getBatches(token.getCompanyId(), token.getShopId(), productId, start, limit, "{created:-1}", ProductBatchQuantityResult.class);

        if (CollectionUtils.isNullOrEmpty(result.getValues())) {
            return result;
        }

        List<ObjectId> derivedLogIds = new ArrayList<>();
        for (ProductBatchQuantityResult batch : result.getValues()) {
            if (StringUtils.isNotBlank(batch.getDerivedLogId()) && ObjectId.isValid(batch.getDerivedLogId())) {
                derivedLogIds.add(new ObjectId(batch.getDerivedLogId()));
            }
        }
        HashMap<String, DerivedProductBatchLog> derivedBatchLogMap = derivedProductBatchLogService.listAsMap(token.getCompanyId(), derivedLogIds);
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), token.getShopId());


        //Get batch quantity per inventory
        for (ProductBatchQuantityResult batch : result.getValues()) {

            List<ProductBatchByInventory> quantities = batchQuantityRepository.getBatchQuantitiesByInventory(token.getCompanyId(), token.getShopId(), batch.getId(), batch.getProductId());
            Map<String, List<BatchQuantity>> quantityByInventoryMap = new HashMap<>();
            if (!CollectionUtils.isNullOrEmpty(quantities)) {
                for (ProductBatchByInventory quantity : quantities) {
                    quantityByInventoryMap.putIfAbsent(quantity.getInventoryId(), new ArrayList<>());
                    quantityByInventoryMap.get(quantity.getInventoryId()).addAll(quantity.getBatchQuantity());
                }
            }
            Map<String, BigDecimal> batchQuantityMap = new HashMap<>();
            for (Map.Entry<String, Inventory> entry : inventoryMap.entrySet()) {
                if (entry.getValue() != null && entry.getValue().isActive()) {
                    List<BatchQuantity> batchQuantities = quantityByInventoryMap.get(entry.getKey());

                    double quantity = 0d;
                    //Calculate batch quantity
                    if (!CollectionUtils.isNullOrEmpty(batchQuantities)) {
                        for (BatchQuantity batchQuantity : batchQuantities) {
                            quantity += batchQuantity.getQuantity().doubleValue();
                        }
                    }

                    batchQuantityMap.putIfAbsent(entry.getKey(), new BigDecimal(0));
                    BigDecimal totalQuantity = new BigDecimal(batchQuantityMap.get(entry.getKey()).doubleValue() + quantity);
                    batchQuantityMap.put(entry.getKey(), totalQuantity);
                }
            }

            batch.setBatchQuantityMap(batchQuantityMap);

            batch.setDerivedProductBatchLog(derivedBatchLogMap.get(batch.getDerivedLogId()));
        }
     prepareProductBatchResult(result);

        return result;
    }

    private void getProductBatchQuantity(Product product, ProductBatch productBatch) {
        if (StringUtils.isNotBlank(productBatch.getTrackPackageLabel())) {
            MetrcVerifiedPackage metricsPackages = metrcAccountService.getMetrcVerifiedPackage(productBatch.getTrackPackageLabel());
            if (metricsPackages != null && metricsPackages.isRequired()) {
                productBatch.setTrackPackageLabel(productBatch.getTrackPackageLabel());
                productBatch.setTrackTraceSystem(productBatch.getTrackTraceSystem());
                if (metricsPackages.getId() > 0) {
                    productBatch.setTrackPackageId(metricsPackages.getId());
                } else {
                    productBatch.setTrackPackageId(null);
                }
                productBatch.setTrackTraceVerified(true);
                productBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());

                BigDecimal verifiedQty = metrcAccountService.convertToBatchQuantity(metricsPackages,product.getCategory().getUnitType(),product);
                productBatch.setQuantity(verifiedQty);
                /*
                if (product.getCategory().getUnitType() == ProductCategory.UnitType.units && metricsPackages.getBlazeMeasurement() != ProductWeightTolerance.WeightKey.UNIT) {
                    // incoming is in grams or units
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                        // set it as normally
                        productBatch.setQuantity(new BigDecimal(metricsPackages.getBlazeQuantity() * 2)); // multiply by 2 as there are twice many in half grams
                    } else if (product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                        ProductWeightTolerance eighthTolerance = weightToleranceRepository.getToleranceForWeight(token.getCompanyId(),
                                ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
                        double eighthValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue.doubleValue();
                        if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
                            eighthValue = eighthTolerance.getUnitValue().doubleValue();
                        }
                        // set it as normally
                        productBatch.setQuantity(new BigDecimal(metricsPackages.getBlazeQuantity() / eighthValue)); // divide grams by eighth to get eighth quantity
                    } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                        ProductWeightTolerance quarterTolerance = weightToleranceRepository.getToleranceForWeight(token.getCompanyId(),
                                ProductWeightTolerance.WeightKey.QUARTER);
                        double quarterValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue.doubleValue();
                        if (quarterTolerance != null && quarterTolerance.getUnitValue().doubleValue() > 0) {
                            quarterValue = quarterTolerance.getUnitValue().doubleValue();
                        }
                        // set it as normally
                        productBatch.setQuantity(new BigDecimal(metricsPackages.getBlazeQuantity() / quarterValue)); // divide grams by quarter to get quarter quantity
                    } else {
                        // set it as normally
                        productBatch.setQuantity(new BigDecimal(metricsPackages.getBlazeQuantity()));
                    }
                } else {
                    // this is grams
                    productBatch.setQuantity(new BigDecimal(metricsPackages.getBlazeQuantity()));
                }*/
            }
        }
    }

    private void reCalculateProductBatchPrice(Product product, ProductBatch productBatch) {
        BigDecimal costPerUnit = productBatch.getCostPerUnit();
        BigDecimal totalCost = productBatch.getCost();

        if (costPerUnit == null || costPerUnit.doubleValue() <= 0) {
            costPerUnit = new BigDecimal(0);
            if (productBatch.getQuantity() != null && productBatch.getQuantity().doubleValue() > 0) {
                if (totalCost != null && totalCost.doubleValue() > 0) {
                    double cost = NumberUtils.round(productBatch.getCost().doubleValue(), 4);
                    double quantity = NumberUtils.round(productBatch.getQuantity().doubleValue(), 4);
                    if (quantity > 0) {
                        double myCost = cost / quantity;
                        costPerUnit = new BigDecimal(myCost);
                    }
                }
            }
        } else {
            if (costPerUnit == null) {
                costPerUnit = new BigDecimal(0);
            }
            if (productBatch.getQuantity() != null && productBatch.getQuantity().doubleValue() > 0) {
                double cost = NumberUtils.round(productBatch.getQuantity().doubleValue() * costPerUnit.doubleValue(), 4);
                totalCost = new BigDecimal(cost);
            }
        }
        productBatch.setCostPerUnit(costPerUnit);
        productBatch.setCost(totalCost);
        ProductCategory category = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());
        if (category != null) {
            this.calculateExciseTaxForBatch(productBatch, productBatch.getCostPerUnit(), product.isCannabisProduct(category.isCannabis()));
        }
    }

    @Override
    public void takeSnapshot() {
        String name = token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName();
        taskManager.takeInventorySnapshot(token.getCompanyId(),
                token.getShopId(),
                token.getActiveTopUser().getUserId(),
                "Inventory Snapshot requested by " + name,
                true);
    }

    /**
     * Transfer All quantity(Prepackage and Regular) from Inventory to Safe Inventory
     *
     * @param request
     */
    @Override
    public void resetInventoryToSafe(ResetInventoryRequest request) {
        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(), token.getShopId(), QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException("TransferRequest", "There are active pending transactions. Please wait until all transactions are completed.");
        }*/

        Cart cart = new Cart();
        cart.prepare(token.getCompanyId());

        List<Product> pendingProducts = new ArrayList<>();
        for (String productId : request.getProductId()) {
            List<ObjectId> objectIds = new ArrayList<>();
            if (!ObjectId.isValid(String.valueOf(productId))) {
                throw new BlazeInvalidArgException("ResetInventoryRequest", productId + " is an invalid id.");
            } else if (!inventoryHashMap.containsKey(request.getFromInventoryId())) {
                throw new BlazeInvalidArgException("ResetInventoryRequest", "Invalid 'from' inventory.");
            } else if (!inventoryHashMap.containsKey(request.getToInventoryId())) {
                throw new BlazeInvalidArgException("ResetInventoryRequest", "Invalid 'to' inventory.");
            }

            if (request.getFromInventoryId().equalsIgnoreCase(request.getToInventoryId())) {
                throw new BlazeInvalidArgException("ResetInventoryRequest", "Cannot transfer within the same inventory.");
            }
            objectIds.add(new ObjectId(productId));
            Iterable<Product> dbProducts = productRepository.findItemsIn(token.getCompanyId(), token.getShopId(), objectIds);
            HashMap<String, Product> productHashMap = new HashMap<>();
            for (Product p : dbProducts) {
                productHashMap.put(p.getId(), p);
            }

            Product dbProduct = productHashMap.get(productId);
            if (dbProduct == null) {
                throw new BlazeInvalidArgException("TransferRequest", "Product does not exist: " + productId);
            }
            Product product = productRepository.get(token.getCompanyId(), productId);

            // Reset Prepackage Quantity
            Iterable<ProductPrepackageQuantity> productPrepackageQuantities = quantityRepository.getQuantitiesForProduct(token.getCompanyId(),
                    product.getShopId(), productId);

            /*
                This will not work.

            for (ProductPrepackageQuantity productPrepackageQuantity : productPrepackageQuantities) {
                String prepackageItemId = productPrepackageQuantity.getPrepackageItemId();

                // don't

                ProductPrepackageQuantity fromProductPrepackageQuantity = quantityRepository.getQuantity
                        (token.getCompanyId(), product.getShopId(), request.getFromInventoryId(), prepackageItemId);

                if (fromProductPrepackageQuantity != null) {
                    ProductPrepackageQuantity toProductPrepackageQuantity = quantityRepository.getQuantity
                            (token.getCompanyId(), product.getShopId(), request.getToInventoryId(), prepackageItemId);



                    if (toProductPrepackageQuantity != null) {
                        int toQuantity = toProductPrepackageQuantity.getQuantity();
                        toQuantity += fromProductPrepackageQuantity.getQuantity();
                        toProductPrepackageQuantity.setQuantity(toQuantity);
                        quantityRepository.update(token.getCompanyId(), toProductPrepackageQuantity.getId(), toProductPrepackageQuantity);
                    }

                    fromProductPrepackageQuantity.setQuantity(0);
                    quantityRepository.update(token.getCompanyId(), fromProductPrepackageQuantity.getId(), fromProductPrepackageQuantity);

                    OrderItem orderItem = new OrderItem();
                    orderItem.prepare(token.getCompanyId());
                    orderItem.setStatus(OrderItem.OrderItemStatus.Active);
                    orderItem.setCost(new BigDecimal(0));
                    orderItem.setQuantity(BigDecimal.valueOf(fromProductPrepackageQuantity.getQuantity()));
                    orderItem.setFinalPrice(new BigDecimal(0));
                    orderItem.setProductId(dbProduct.getId());
                    orderItem.setPrepackageItemId(prepackageItemId);
                    cart.getItems().add(orderItem);
                }
            }*/

            // Regular Quantity
            ProductQuantity fromQuantity = null;
            ProductQuantity toQuantity = null;
            for (ProductQuantity dbQuantity : product.getQuantities()) {
                if (dbQuantity.getInventoryId().equalsIgnoreCase(request.getFromInventoryId())) {
                    fromQuantity = dbQuantity;
                } else if (dbQuantity.getInventoryId().equalsIgnoreCase(request.getToInventoryId())) {
                    toQuantity = dbQuantity;
                }
            }


            if (fromQuantity == null) {
                throw new BlazeInvalidArgException("TransferRequest", "From ProductQuantity does not exist for: " + product.getName());
            }

            if (toQuantity == null) {
                toQuantity = new ProductQuantity();
                toQuantity.setCompanyId(token.getCompanyId());
                toQuantity.setId(ObjectId.get().toString());
                toQuantity.setInventoryId(request.getToInventoryId());
            }

            double newQuantity = fromQuantity.getQuantity().doubleValue();

            fromQuantity.setQuantity(BigDecimal.ZERO);

            double newToQuantity = toQuantity.getQuantity().doubleValue() + newQuantity;
            toQuantity.setQuantity(new BigDecimal(newToQuantity)); //NumberUtils.round(newToQuantity, 2));
            pendingProducts.add(product);

            OrderItem order = new OrderItem();
            order.prepare(token.getCompanyId());
            order.setStatus(OrderItem.OrderItemStatus.Active);
            order.setCost(new BigDecimal(0));
            order.setQuantity(BigDecimal.valueOf(newQuantity));
            order.setFinalPrice(new BigDecimal(0));
            order.setProductId(dbProduct.getId());
            cart.getItems().add(order);

            // Create Transaction Transfers
            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), product.getShopId(), "TransactionPriority");
            String transNo = "" + sequence.getCount();
            List<ProductChangeLog> productChangeLogs = new ArrayList<>();
            for (Product p : pendingProducts) {
                List<ProductPrepackageQuantity> productPrepackageQuantitiesList = Lists.newArrayList(productPrepackageQuantities);
                productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                        p, productPrepackageQuantitiesList, String.format("Trans #%s: Transfer", transNo)));
                productRepository.update(token.getCompanyId(), p.getId(), p);
            }
            productChangeLogRepository.save(productChangeLogs);

            BulkInventoryTransferRequest bulkInventoryTransferRequest = new BulkInventoryTransferRequest();

            InventoryTransferRequest inventoryTransferRequest = new InventoryTransferRequest();
            inventoryTransferRequest.setTransferAmount(BigDecimal.valueOf(newQuantity));
            inventoryTransferRequest.setFromInventoryId(request.getFromInventoryId());
            inventoryTransferRequest.setToInventoryId(request.getToInventoryId());
            inventoryTransferRequest.setProductId(productId);
            List<InventoryTransferRequest> inventoryTransferRequests = new ArrayList<InventoryTransferRequest>();
            inventoryTransferRequests.add(inventoryTransferRequest);
            bulkInventoryTransferRequest.setTransfers(inventoryTransferRequests);

            Transaction trans = new Transaction();
            trans.setActive(false);
            trans.setTransNo(transNo);
            trans.setMemberId(null);
            trans.setCompanyId(token.getCompanyId());
            trans.setShopId(product.getShopId());
            trans.setQueueType(Transaction.QueueType.None);
            trans.setStatus(Transaction.TransactionStatus.Void);
            trans.setTerminalId(null);
            trans.setMemberId(null);
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());
            trans.setCreatedById(token.getActiveTopUser().getUserId());
            trans.setPriority(sequence.getCount());
            trans.setTransferRequest(bulkInventoryTransferRequest);
            trans.setProcessedTime(DateTime.now().getMillis());
            trans.setCompletedTime(DateTime.now().getMillis());
            trans.setStartTime(DateTime.now().getMillis());
            trans.setEndTime(DateTime.now().getMillis());

            // Set cart
            cart.setPaymentOption(Cart.PaymentOption.None);
            trans.setCart(cart);
            trans.setTransType(Transaction.TransactionType.Transfer);
            trans.setTransferShopId(product.getShopId());

            cart.setTotal(new BigDecimal(0));
            cart.setChangeDue(new BigDecimal(0));
            cart.setCashReceived(new BigDecimal(0));
            cart.setSubTotal(new BigDecimal(0));

            transactionRepository.save(trans);

            // TODO: TEST THIS ACCORDINGLY
            // reset
            // reduce batch quantity
            batchQuantityService.subtractBatchQuantity(product.getCompanyId(),
                    product.getShopId(),
                    product.getId(),
                    fromQuantity.getInventoryId(),
                    null,
                    fromQuantity.getQuantity());

            // add quantity to batch
            batchQuantityService.addBatchQuantity(product.getCompanyId(),
                    product.getShopId(),
                    product.getId(),
                    toQuantity.getInventoryId(),
                    null,
                    fromQuantity.getQuantity());

            // Notify that inventory has been updated
            realtimeService.sendRealTimeEvent(token.getShopId(),
                    RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        }
    }

    /**
     * Get product batch by label
     *
     * @param inventoryId : inventory id
     * @param label       : label for which product batch needs to be find
     * @param productId   : product id
     * @param start       : start
     * @param limit       : limit
     */
    @Override
    public SearchResult<BatchQuantityResult> getBatchByLabel(String inventoryId, String label, String productId, int start, int limit) {

        SearchResult<BatchQuantityResult> batch;

        if (StringUtils.isEmpty(label)) {

            batch = batchQuantityService.getBatchQuantitiesForProduct(token.getCompanyId(), token.getShopId(), productId, inventoryId, BatchQuantityResult.class, start, limit);

        } else {
            //Get BarcodeItem by label
            List<BarcodeItem> barcodeItems = barcodeService.getBarCodesByLabel(token.getCompanyId(), token.getShopId(), label, BarcodeItem.BarcodeEntityType.Batch);

            if (barcodeItems == null) {
                throw new BlazeInvalidArgException("Bar code", "No bar code item find by this label");
            }

            List<String> batchIdList = new ArrayList<>();

            //Get list of batch id from BarcodeItem
            for (BarcodeItem barcodeItem : barcodeItems) {
                batchIdList.add(barcodeItem.getEntityId());
            }

            batch = batchQuantityService.getBatchQuantitiesForInventoryAndBatch(token.getCompanyId(), token.getShopId(), inventoryId, batchIdList, productId, start, limit);
        }

        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        if (batch != null && batch.getValues() != null) {
            for (BatchQuantityResult quantityResult : batch.getValues()) {
                ProductBatch productBatch = batchHashMap.get(quantityResult.getBatchId());

                quantityResult.setProductBatch(productBatch);
            }
        }
        return batch;
    }

    /**
     * Get oldest batch with valid quantity
     *
     * @param productId     : product id for which oldest batch needs to be fetch with valid quantity
     * @param transactionId : transaction id
     */
    @Override
    public ProductBatch getOldestBatchByProductId(String productId, String transactionId) {
        ProductBatch productBatch = null;
        SearchResult<ProductBatch> batches = productBatchRepository.getBatches(token.getCompanyId(), token.getShopId(), productId, 0, Integer.MAX_VALUE, "{created:1}", ProductBatch.class);

        Transaction transaction = transactionRepository.getById(transactionId);

        if (transaction == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction not found");
        }


        BigDecimal quantity = BigDecimal.ZERO;
        List<OrderItem> items = transaction.getCart().getItems();
        for (OrderItem item : items) {
            if (item.getProductId().equalsIgnoreCase(productId)) {
                quantity = item.getQuantity();
                break;
            }
        }

        if (batches != null && batches.getValues() != null) {
            for (ProductBatch batch : batches.getValues()) {
                if (quantity.doubleValue() <= batch.getQuantity().doubleValue()) {
                    productBatch = batch;
                    break;
                }
            }
        }
        return productBatch;
    }

    /**
     * This method is give list for all Reconciliation History
     *
     * @param start : start
     * @param limit : limit
     * @return List<ReconciliationHistoryResult> : List of all reconciliation history result
     */
    @Override
    public SearchResult<ReconciliationHistoryResult> getAllReconciliationHistory(int start, int limit) {
        SearchResult<ReconciliationHistoryResult> reconciliationHistory = reconciliationHistoryRepository.getAllReconciliationHistory(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);
        Double allTotalLoss = 0.0;
        Double totalLoss;
        Double oldValue;
        Double newValue;
        if (reconciliationHistory != null) {
            HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
            HashMap<String, Product> productHashMap = productRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
            HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
            HashMap<String, Terminal> stringTerminalHashMap = terminalRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
            HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(token.getCompanyId());


            for (ReconciliationHistoryResult history : reconciliationHistory.getValues()) {
                allTotalLoss = 0.0;
                for (ReconciliationHistoryList reconciliationHistoryList : history.getReconciliations()) {

                    Inventory inventory = inventoryHashMap.get(reconciliationHistoryList.getInventoryId());
                    if (inventory != null) {
                        history.setInventoryName(inventory.getName());
                    }
                    Product product = productHashMap.get(reconciliationHistoryList.getProductId());
                    if (product != null) {
                        reconciliationHistoryList.setProductName(product.getName());
                        ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                        if (productCategory != null) {
                            history.setCategoryName(productCategory.getName());
                            history.setCategoryUnitType(productCategory.getUnitType());
                        }
                        Brand brand =  StringUtils.isNotBlank(product.getBrandId()) ? brandHashMap.get(product.getBrandId()) : null;
                        if (brand != null) {
                            reconciliationHistoryList.setBrandName(brand.getName());
                        }
                    }
                    Terminal terminal = stringTerminalHashMap.get(reconciliationHistoryList.getTerminalId());
                    if (terminal != null) {
                        history.setTerminalName(terminal.getName());
                    }

                    oldValue = reconciliationHistoryList.getOldQuantity().doubleValue();
                    newValue = reconciliationHistoryList.getNewQuantity().doubleValue();
                    totalLoss = newValue - oldValue;
                    history.setTotalLoss(BigDecimal.valueOf(totalLoss));
                    history.setReportLoss(reconciliationHistoryList.isReportLoss());
                    allTotalLoss += totalLoss;
                }
                history.setAllTotalLoss(BigDecimal.valueOf(allTotalLoss));
            }
        }
        return reconciliationHistory;
    }

    /**
     * Get reconciliation history by history id
     *
     * @param historyId : history id
     */
    @Override
    public ReconciliationHistoryResult getReconciliationHistoryById(String historyId) {
        ReconciliationHistoryResult reconciliationHistoryResult = reconciliationHistoryRepository.getReconciliationHistory(token.getCompanyId(), token.getShopId(), historyId, ReconciliationHistoryResult.class);

        if (reconciliationHistoryResult == null) {
            throw new BlazeInvalidArgException("Reconciliation History", "Reconciliation history not found with this id");
        }

        prepareReconciliationHistoryResult(reconciliationHistoryResult);
        return reconciliationHistoryResult;
    }

    @Override
    public BigDecimal calculateExciseTaxByUnitCost(ExciseTaxCalculateRequest request) {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoService.getExciseTaxInfoForShop(shop);
        BigDecimal totalTax = new BigDecimal(0);

        if (exciseTaxInfo != null && request.getCost() != null) {
            double stateMarkUp = exciseTaxInfo.getStateMarkUp().doubleValue();
            stateMarkUp = 1 + (stateMarkUp / 100);

            BigDecimal exciseTax = exciseTaxInfo.getExciseTax();
            double perUnitExciseTax = (request.getCost().doubleValue() * stateMarkUp) * exciseTax.doubleValue() / 100;

            totalTax = new BigDecimal(perUnitExciseTax);

        }

        return totalTax;
    }

    /**
     * Override method to get reconciliation history by reconcile number
     *
     * @param reconciliationNo
     * @return
     */
    @Override
    public ReconciliationHistoryResult getReconciliationByNo(String reconciliationNo) {
        if (StringUtils.isBlank(reconciliationNo)) {
            throw new BlazeInvalidArgException(RECONCILIATION, "Reconciliation Number cannot be blank");
        }
        if (StringUtils.isBlank(reconciliationNo)) {
            throw new BlazeInvalidArgException(RECONCILIATION, "Reconciliation Number must be valid number");
        }

        ReconciliationHistoryResult result = reconciliationHistoryRepository.getReconciliationHistoryByNo(token.getCompanyId(), token.getShopId(), Long.parseLong(reconciliationNo), ReconciliationHistoryResult.class);
        if (result == null) {
            throw new BlazeInvalidArgException(RECONCILIATION, "Reconciliation History not found for number :" + reconciliationNo);
        }
        prepareReconciliationHistoryResult(result);

        return result;
    }

    /**
     * Private method to prepare reconciliation history result
     *
     * @param reconciliationHistoryResult
     */
    private void prepareReconciliationHistoryResult(ReconciliationHistoryResult reconciliationHistoryResult) {
        Double allTotalLoss = 0.0;
        Double totalLoss;
        Double oldValue;
        Double newValue;

        List<ObjectId> batchIds = new ArrayList<>();

        if (reconciliationHistoryResult.getReconciliations() != null && !reconciliationHistoryResult.getReconciliations().isEmpty()) {
           for (ReconciliationHistoryList reconciliationHistoryList : reconciliationHistoryResult.getReconciliations()) {
                if (StringUtils.isNotBlank(reconciliationHistoryList.getBatchId()) && ObjectId.isValid(reconciliationHistoryList.getBatchId())) {
                    batchIds.add(new ObjectId(reconciliationHistoryList.getBatchId()));
                }
            }
        }

        Shop shop = shopRepository.getById(token.getShopId());
        HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), batchIds);
        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, Product> productHashMap = productRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(token.getCompanyId());

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        Employee employee = employeeRepository.getById(reconciliationHistoryResult.getEmployeeId());

        boolean isReportLoss = Boolean.FALSE;
        List<ReconciliationHistoryListResult> reconciliationResults = new ArrayList<>();
        for (ReconciliationHistoryList reconciliationHistoryList : reconciliationHistoryResult.getReconciliations()) {
            allTotalLoss = 0.0;
            Product product = productHashMap.get(reconciliationHistoryList.getProductId());
            if (product != null) {
                ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                if (productCategory != null) {
                    reconciliationHistoryResult.setCategoryName(productCategory.getName());
                    reconciliationHistoryResult.setCategoryUnitType(productCategory.getUnitType());
                }

                Brand brand =  StringUtils.isNotBlank(product.getBrandId()) ? brandHashMap.get(product.getBrandId()) : null;
                if (brand != null) {
                    reconciliationHistoryList.setBrandName(brand.getName());
                }
            }
            Inventory inventory = inventoryHashMap.get(reconciliationHistoryList.getInventoryId());
            if (inventory != null) {
                reconciliationHistoryResult.setInventoryName(inventory.getName());
            }
            Terminal terminal = terminalHashMap.get(reconciliationHistoryList.getTerminalId());
            if (terminal != null) {
                reconciliationHistoryResult.setTerminalName(terminal.getName());
            }
            oldValue = reconciliationHistoryList.getOldQuantity().doubleValue();
            newValue = reconciliationHistoryList.getNewQuantity().doubleValue();
            totalLoss = newValue - oldValue;
            reconciliationHistoryResult.setTotalLoss(BigDecimal.valueOf(totalLoss));
            reconciliationHistoryList.setReportLoss(reconciliationHistoryList.isReportLoss());
            reconciliationHistoryList.setProductName(product.getName());
            allTotalLoss += totalLoss;
            isReportLoss = reconciliationHistoryList.isReportLoss();

        }

        for (ReconciliationHistoryList reconciliationHistoryList : reconciliationHistoryResult.getReconciliations()) {
            ReconciliationHistoryListResult reconciliationHistoryListResult =  new ReconciliationHistoryListResult(reconciliationHistoryList);
            if (batchHashMap.containsKey(reconciliationHistoryList.getBatchId())) {
                ProductBatch batch = batchHashMap.get(reconciliationHistoryList.getBatchId());
                reconciliationHistoryListResult.setTrackHarvestBatchId(batch.getTrackHarvestBatch());
                reconciliationHistoryListResult.setBatchPurchaseDate(batch.getPurchasedDate());
                reconciliationHistoryListResult.setBatchSku(batch.getSku());
                reconciliationHistoryListResult.setTrackPackageLabel(batch.getTrackPackageLabel());
            }

            reconciliationResults.add(reconciliationHistoryListResult);
        }

        reconciliationHistoryResult.setAllTotalLoss(BigDecimal.valueOf(allTotalLoss));
        reconciliationHistoryResult.setReportLoss(isReportLoss);

        if (shop != null && !shop.isRetail()) {
            reconciliationHistoryResult.setReconciliationsResult(reconciliationResults);
            reconciliationHistoryResult.setReconciliations(null);
        }
        reconciliationHistoryResult.setReconciliationsResult(reconciliationResults);
        if (employee != null) {
            StringBuilder employeeName = new StringBuilder();
            employeeName.append(StringUtils.isNotBlank(employee.getFirstName()) ? employee.getFirstName() : "")
                    .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName() : "");

            reconciliationHistoryResult.setEmployeeName(employeeName.toString());
        }


    }

    @Override
    public List<InventoryShopBaseResult> getInventoriesPerShop() {
        List<Shop> shops = Lists.newArrayList(shopRepository.list(token.getCompanyId()));
        List<InventoryShopBaseResult> shopBaseInventories = new ArrayList<>();
        if (shops != null) {
            for (Shop shop : shops) {
                InventoryShopBaseResult inventoryShopBaseResult = new InventoryShopBaseResult();
                List<Inventory> inventories = new ArrayList<>();
                SearchResult<Inventory> shopInventories = this.getShopInventories(shop.getId());
                if (shopInventories != null) {
                    for (Inventory shopInventory : shopInventories.getValues()) {
                        inventories.add(shopInventory);
                    }
                }
                inventoryShopBaseResult.setShopId(shop.getId());
                inventoryShopBaseResult.setInventory(inventories);
                shopBaseInventories.add(inventoryShopBaseResult);
            }
        }

        return shopBaseInventories;
    }

    @Override
    public void compositeAddInventoryTransfer(List<BulkInventoryTransferRequest> request) {

    }

    @Override
    public AssetStreamResult getProductBatchQRAsset(String batchId, String assetToken) {
        String companyId = "";
        String shopId = "";
        if (token != null && StringUtils.isNotBlank(token.getCompanyId()) && StringUtils.isNotBlank(token.getShopId())) {
            companyId = token.getCompanyId();
            shopId = token.getShopId();
        } else if (StringUtils.isNotBlank(assetToken)) {
            try {
                AssetAccessToken assetAccessToken = securityUtil.decryptAssetAccessToken(assetToken);
                companyId = assetAccessToken.getCompanyId();
                shopId = assetAccessToken.getShopId();
            } catch (Exception e) {
                // ignore
            }
        }
        if (StringUtils.isBlank(companyId)) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Invalid Company");
        }
        ProductBatch productBatch = productBatchRepository.get(companyId, batchId);
        if (productBatch == null) {
            throw new BlazeInvalidArgException("batch", "Product batch is invalid");
        }
        AssetStreamResult asset = null;
        if (productBatch.getBatchQRAsset() != null) {
            asset = assetService.getCompanyAssetStreamWithoutAsset(productBatch.getBatchQRAsset().getKey(), assetToken, productBatch.getBatchQRAsset());
        }
        if (asset == null) {
            CompanyAsset companyAsset = new CompanyAsset();
            File file = null;
            InputStream inputStream = QrCodeUtil.getQrCode(productBatch.getSku());
            try {
                file = QrCodeUtil.stream2file(inputStream, ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            String keyName = productBatch.getId() + "-" + productBatch.getSku();
            UploadFileResult uploadFileResult = amazonS3Service.uploadFilePublic(file, keyName, ".png");
            companyAsset.prepare(token.getCompanyId());
            companyAsset.setPublicURL(uploadFileResult.getUrl());
            companyAsset.setKey(uploadFileResult.getKey());
            companyAsset.setType(Asset.AssetType.Photo);
            companyAsset.setSecured(false);
            companyAsset.setActive(true);
            productBatch.setBatchQRAsset(companyAsset);
            productBatchRepository.update(token.getCompanyId(), productBatch.getId(), productBatch);

            asset = new AssetStreamResult();
            asset.setAsset(productBatch.getBatchQRAsset());
            asset.setStream(inputStream);
            asset.setContentType("image/jpeg");
            asset.setKey(uploadFileResult.getKey());
        }
        return asset;

    }

    private void createQueuedTransactionJobForBatchQuantity(ProductBatch productBatch, Inventory.InventoryType inventoryType, ProductBatchQueuedTransaction.OperationType operationType, BigDecimal quantity, InventoryOperation.SubSourceAction subSourceAction) {
        createQueuedTransactionJobForBatchQuantity(productBatch, inventoryType, operationType, quantity, null, subSourceAction);
    }

    @Override
    public long getPendingStatusForQueuedTransaction() {
        return queuedTransactionRepository.getCountStatus(token.getCompanyId(), token.getShopId(), QueuedTransaction.QueueStatus.Pending);
    }

    @Override
    public void unArchiveBatch(String batchId) {
        ProductBatch dbBatch = productBatchRepository.get(token.getCompanyId(), batchId);
        if (dbBatch == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_EXIST);
        }
        productBatchRepository.unArchiveBatch(token.getCompanyId(), token.getShopId(), batchId);
    }

    @Override
    public List<BatchByCategoryResult> getProductBatchesByCategory(String categoryId, String inventoryId, FilterType status) {

        List<BatchByCategoryResult> result = new ArrayList<>();
        if (StringUtils.isNotBlank(categoryId) && StringUtils.isNotBlank(inventoryId)) {
            SearchResult<Product> productsResult = null;
            if (status == null || status == FilterType.All) {
                productsResult = productRepository.findProductsByCategoryId(token.getCompanyId(), token.getShopId(), categoryId, "{modified:-1}", 0, Integer.MAX_VALUE);
            } else {
                productsResult = productRepository.findActiveProductsByCategoryId(token.getCompanyId(), token.getShopId(), categoryId, "{modified:-1}", 0, Integer.MAX_VALUE, (status == FilterType.Active));
            }

            List<String> productIds = new ArrayList<>();

            if (productsResult != null && productsResult.getValues() != null && !productsResult.getValues().isEmpty()) {
                for (Product product : productsResult.getValues()) {
                    productIds.add(product.getId());
                }
            }
            result = batchQuantityService.getBatchQuantitiesForProduct(token.getCompanyId(), token.getShopId(), inventoryId, productIds);
        }

        if (!result.isEmpty()) {
            HashMap<String, ProductBatch> productBatchMap = productBatchRepository.listAsMap(token.getCompanyId(), token.getShopId());

            for (BatchByCategoryResult batchByCategoryResult : result) {
                List<BatchQuantityResult> batchQuantityResults = new ArrayList<>();
                for (BatchQuantityResult quantityResult : batchByCategoryResult.getBatchResult()) {
                    quantityResult.setProductBatch(productBatchMap.get(quantityResult.getBatchId()));
                    if (quantityResult.getProductBatch() != null) {
                        batchQuantityResults.add(quantityResult);
                    }
                }
                batchByCategoryResult.setBatchResult(batchQuantityResults);
            }
        }
        return result;
    }

    @Override
    public GetComplianceBatchesResult getAllComplianceBatches(boolean activeBatches, boolean onHoldBatches, boolean inactiveBatches) {
        GetComplianceBatchesEvent event = new GetComplianceBatchesEvent();
        event.setCompanyId(token.getCompanyId());
        event.setShopId(token.getShopId());
        event.setGetActiveBatches(activeBatches);
        event.setGetOnHoldBatches(onHoldBatches);
        event.setGetInactiveBatches(inactiveBatches);
        blazeEventBus.post(event);

        GetComplianceBatchesResult result = event.getResponse();
        return result;
    }

    /**
     * This method return the last reconciliation time for provided or current shop
     * @param shopId : shop id
     * @return : {@link ReconciliationHistory}
     */
    @Override
    public Long getLastReconciliation(String shopId) {

        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(RECONCILIATION, SHOP_NOT_FOUND);
        }

        ReconciliationHistory reconciliationHistory = reconciliationHistoryRepository.getLastReconciliationHistory(token.getCompanyId(), shopId);

        if (reconciliationHistory != null) {
            return reconciliationHistory.getCreated();
        } else {
            return  0l;
        }
    }

    private ProductBatch createProductBatchForDerivedProduct(Product product, Map<String, List<DerivedProductBatchInfo>> productMap, DerivedBatchAddRequest request, ProductBatch oldestbatchInfo, CompanyLicense companyLicense) {

        BigDecimal quantity = request.getQuantity();
        //Create log for derived product
        DerivedProductBatchLog log = derivedProductBatchLogService.createLog(product, quantity, productMap);
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        //Create product batch for derived product quantity
        ProductBatch productBatch = new ProductBatch();
        productBatch.prepare(token.getCompanyId());
        productBatch.setProductId(product.getId());
        productBatch.setShopId(token.getShopId());
        productBatch.setPublishedDate(DateTime.now().getMillis());
        productBatch.setStatus(request.getStatus());
        productBatch.setQuantity(quantity);
        productBatch.setDerivedLogId(log.getId());
        productBatch.setVendorId(request.getVendorId());
        productBatch.setSku(request.getSku());
        productBatch.setTrackPackageLabel(request.getTrackPackageLabel());
        productBatch.setTrackHarvestBatch(request.getTrackHarvestBatch());
        productBatch.setTrackHarvestDate(request.getTrackHarvestDate());
        productBatch.setCostPerUnit(request.getCostPerUnit());
        productBatch.setCost(BigDecimal.valueOf(request.getQuantity().doubleValue() * request.getCostPerUnit().doubleValue()));
        productBatch.setReceiveDate(oldestbatchInfo.getReceiveDate());
        productBatch.setPurchasedDate(oldestbatchInfo.getPurchasedDate());
        productBatch.setExpirationDate(oldestbatchInfo.getExpirationDate());
        productBatch.setSellBy(oldestbatchInfo.getSellBy());
        productBatch.setTrackTraceSystem((StringUtils.isNotBlank(request.getTrackPackageLabel())) ? ProductBatch.TrackTraceSystem.METRC : ProductBatch.TrackTraceSystem.MANUAL);
        productBatch.setLicenseId(companyLicense.getId());
        productBatch.setFlowerSourceType(oldestbatchInfo.getFlowerSourceType());
        productBatch.setWaste(request.getWaste());

        productBatch.setActualWeightPerUnit(request.getActualWeightPerUnit());

        if (StringUtils.isNotBlank(request.getRoomId())) {
            Inventory inventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getRoomId());
            if (inventory == null) {
              throw new BlazeInvalidArgException(PRODUCT_BATCH, "Selected inventory does'nt exists.");
            }
            productBatch.setRoomId(inventory.getId());
        }
        // Update sku if needed
        BarcodeItem batchItem = barcodeService.createBarcodeItemIfNeeded(product.getCompanyId(),product.getShopId(), product.getId(),
                BarcodeItem.BarcodeEntityType.Batch,
                productBatch.getId(), productBatch.getSku(), null, false);
        if (batchItem != null) {
            productBatch.setSku(batchItem.getBarcode());
            productBatch.setBatchNo(batchItem.getNumber());
        }

        if (StringUtils.isNotBlank(productBatch.getSku())) {
            CompanyAsset companyAsset = new CompanyAsset();
            File file = null;
            InputStream inputStream = QrCodeUtil.getQrCode(productBatch.getSku());
            try {
                file = QrCodeUtil.stream2file(inputStream, ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            String keyName = productBatch.getId() + "-" + productBatch.getSku();
            UploadFileResult uploadFileResult = amazonS3Service.uploadFilePublic(file, keyName, ".png");
            companyAsset.prepare(token.getCompanyId());
            companyAsset.setPublicURL(uploadFileResult.getUrl());
            companyAsset.setKey(uploadFileResult.getKey());
            companyAsset.setActive(true);
            companyAsset.setSecured(false);
            companyAsset.setType(Asset.AssetType.Photo);
            productBatch.setBatchQRAsset(companyAsset);
        }

        // create the metrc package
        if (request.isNewMetrcTag() && StringUtils.isNotBlank(request.getTrackPackageLabel())) {
            // try to create new metrc tag
            MetrcVerifyCreatePackage event = new MetrcVerifyCreatePackage();
            event.setCompanyId(token.getCompanyId());
            event.setShopId(token.getShopId());
            event.setProduct(product);
            event.setDerivedBatchAddRequest(request);
            event.setMetrcTag(request.getTrackPackageLabel());
            event.setQuantity(request.getQuantity());
            event.setSku(productBatch.getSku());
            event.setResponseTimeout(30000);
            eventBus.post(event);
            List<String> metrcList = event.getResponse();

            if (event.getException() != null) {
                barcodeItemRepository.removeByBarCodes(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productBatch.getSku()));
                throw event.getException();
            }
            if (!CollectionUtils.isNullOrEmpty(metrcList)) {
                barcodeItemRepository.removeByBarCodes(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productBatch.getSku()));
                throw new BlazeInvalidArgException("METRC Packages", String.format("Metrc package label are already in use: %s", StringUtils.join(metrcList.toArray(), ",")));
            }
        }

        ProductBatch dbProductBatch = productBatchRepository.save(productBatch);
        batchActivityLogService.addBatchActivityLog(productBatch.getId(), token.getActiveTopUser().getUserId(), productBatch.getSku() + " Batch added");
        elasticSearchManager.createOrUpdateIndexedDocument(dbProductBatch);

        for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : productMap.entrySet()) {
            for (DerivedProductBatchInfo info : entry.getValue()) {
                BigDecimal infoQuantity;
                if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow) {
                    infoQuantity = info.getQuantity();
                } else {
                    infoQuantity = this.calculateDerivedQuantity(quantity.doubleValue(), info.getQuantity().doubleValue());
                }
                info.setQuantity(infoQuantity.negate());
            }
        }


        this.createQueuedTransactionForDerivedBatch(productBatch, productMap, ProductBatchQueuedTransaction.OperationType.Add, quantity, InventoryOperation.SubSourceAction.AddBatch, Boolean.TRUE, BigDecimal.ZERO, false, null);

        return productBatch;
    }

    @Override
    public ProductBatch createDerivedProductBatch(DerivedBatchAddRequest request) {

        Map<String, List<DerivedProductBatchInfo>> requestProductMap = request.getProductMap();

        if (requestProductMap.size() == 0) {
            throw new BlazeInvalidArgException(PRODUCT, PARENT_PRODUCT_NOT_FOUND);
        }

        if (request.getQuantity().doubleValue() == 0) {
            throw new BlazeInvalidArgException(PRODUCT, QUANTITY_NOT_VALID);
        }

        Product dbProduct = productRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getProductId());
        if (dbProduct == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_EXIST);
        }

        if (StringUtils.isBlank(request.getVendorId())) {
            request.setVendorId(dbProduct.getVendorId());
        }

        Vendor vendor = vendorRepository.get(token.getCompanyId(), request.getVendorId());
        if (StringUtils.isNotBlank(request.getVendorId()) && vendor == null) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }

        if (!vendor.getId().equalsIgnoreCase(dbProduct.getVendorId()) && (CollectionUtils.isNullOrEmpty(dbProduct.getSecondaryVendors()) || dbProduct.getSecondaryVendors().contains(request.getVendorId()))) {
            throw new BlazeInvalidArgException(VENDOR, String.format(VENDOR_NOT_FOUND_FOR_PRODUCT, dbProduct.getName()));
        }

        CompanyLicense license = vendor.getCompanyLicense(request.getLicenseId());
        if (license == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Company license does not exists.");
        }

        List<ObjectId> batchIds = new ArrayList<>();
        Set<ObjectId> inventoryIds = new HashSet<>();

        for (List<DerivedProductBatchInfo> derivedProductBatchInfos : request.getProductMap().values()) {
            for (DerivedProductBatchInfo info : derivedProductBatchInfos) {
                if (StringUtils.isNotBlank(info.getBatchId()) && ObjectId.isValid(info.getBatchId())) {
                    batchIds.add(new ObjectId(info.getBatchId()));
                }
                if (StringUtils.isNotBlank(info.getInventoryId()) && ObjectId.isValid(info.getInventoryId())) {
                    inventoryIds.add(new ObjectId(info.getInventoryId()));
                }
            }
        }

        HashMap<String, ProductBatch> batchInfoMap = productBatchRepository.listAsMap(token.getCompanyId(), batchIds);
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(inventoryIds));

        if (batchInfoMap.isEmpty()) {
            throw new BlazeInvalidArgException(PRODUCT, PARENT_PRODUCT_NOT_FOUND);
        }

        List<ProductBatch> batchInfos = Lists.newArrayList(batchInfoMap.values());
        batchInfos.sort(new Comparator<ProductBatch>() {
            @Override
            public int compare(ProductBatch o1, ProductBatch o2) {
                DateTime batchDateTime = new DateTime(o1.getCreated());
                if (batchDateTime.isBefore(o2.getCreated())) {
                    return -1;
                } else if (batchDateTime.isAfter(o2.getCreated())) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        if (request.getCostPerUnit() == null || request.getCostPerUnit() == BigDecimal.ZERO) {
            double costPerUnit = 0;
            for (List<DerivedProductBatchInfo> derivedProductBatchInfos : request.getProductMap().values()) {
                for (DerivedProductBatchInfo info : derivedProductBatchInfos) {
                    ProductBatch batch = batchInfoMap.get(info.getBatchId());
                    if (batch == null) {
                        continue;
                    }
                    costPerUnit += batch.getFinalUnitCost().doubleValue() * info.getQuantity().doubleValue();
                }
            }
            request.setCostPerUnit(BigDecimal.valueOf(costPerUnit));
        }

        Product product = this.validateDerivedProductRequest(request.getProductMap(), request.getProductId(), request.getQuantity().doubleValue(), batchInfoMap, inventoryMap);

        return this.createProductBatchForDerivedProduct(product, requestProductMap, request, batchInfos.get(0), license);
    }

    private Product validateDerivedProductRequest(Map<String, List<DerivedProductBatchInfo>> requestProductMap, String productId, double requestQuantity, HashMap<String, ProductBatch> batchInfoMap, HashMap<String, Inventory> inventoryMap) {

        List<ObjectId> productIds = new ArrayList<>();
        List<String> productIdList = new ArrayList<>();
        for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : requestProductMap.entrySet()) {
            if (ObjectId.isValid(entry.getKey())) {
                productIds.add(new ObjectId(entry.getKey()));
                productIdList.add(entry.getKey());
            }
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }
        if (ObjectId.isValid(productId)) {
            productIds.add(new ObjectId(productId));
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);

        Product dbProduct = productMap.get(productId);

        if (dbProduct == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_EXIST);
        }

        if (Product.ProductType.DERIVED != dbProduct.getProductType()) {
            throw new BlazeInvalidArgException(PRODUCT, "Product is not derived type");
        }

        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        List<String> inventoryIds = Lists.newArrayList(inventoryMap.keySet());
        inventoryIds.add(safeInventory.getId());

        HashMap<String, List<BatchQuantity>> batchQuantityList = batchQuantityRepository.getBatchQuantityForProductInventory(token.getCompanyId(), token.getShopId(), inventoryIds, productIdList);

        for (Map.Entry<String, List<DerivedProductBatchInfo>> entry : requestProductMap.entrySet()) {
            Map<String, BatchQuantity> quantityByBatchMap = new HashMap<>();

            Product product = productMap.get(entry.getKey());
            if (product == null) {
                throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_FOUND);
            }

            List<BatchQuantity> batchQuantities = batchQuantityList.getOrDefault(product.getId(), new ArrayList<>());

            if (batchQuantities.isEmpty()) {
                throw new BlazeInvalidArgException(PRODUCT, "Quantity not available for selected ratio of batch.");
            }

            for (BatchQuantity batchQuantity : batchQuantities) {
                quantityByBatchMap.put(batchQuantity.getBatchId() + "_" + batchQuantity.getInventoryId(), batchQuantity);
            }

            for (DerivedProductBatchInfo batchInfo : entry.getValue()) {
                Inventory inventory = safeInventory;

                if (StringUtils.isNotBlank(batchInfo.getInventoryId())) {
                    inventory = inventoryMap.get(batchInfo.getInventoryId());
                    if (inventory == null || !inventory.isActive() || inventory.isDeleted()) {
                        throw new BlazeInvalidArgException(INVENTORY, INVENTORY_NOT_FOUND);
                    }
                }

                ProductBatch batch = batchInfoMap.get(batchInfo.getBatchId());
                if (batch == null || batch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE) {
                    throw new BlazeInvalidArgException(PRODUCT_BATCH, "Selected batch status not in ready for sale");
                }
                BatchQuantity batchQuantity = quantityByBatchMap.get(batchInfo.getBatchId() + "_" + inventory.getId());
                if (batchQuantity == null) {
                    throw new BlazeInvalidArgException(PRODUCT, String.format(BATCH_NOT_FOUND, inventory.getName()));
                }
                if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && requestQuantity > 0 && batchQuantity.getQuantity().doubleValue() >= 0 && batchQuantity.getQuantity().doubleValue() < (batchInfo.getQuantity().doubleValue())) {
                    throw new BlazeInvalidArgException(PRODUCT, "Quantity not available for selected ratio of batch.");
                }
                if (shop.getAppTarget() != CompanyFeatures.AppTarget.Grow && requestQuantity > 0 && batchQuantity.getQuantity().doubleValue() < (batchInfo.getQuantity().doubleValue() * requestQuantity)) {
                    throw new BlazeInvalidArgException(PRODUCT, "Quantity not available for selected ratio of batch.");
                }
                batchInfo.prepare();
            }
        }

        return dbProduct;
    }

    /**
     * Private method to valida bundle batch quantity details
     * @param requestedQuantity : request quantity
     * @param oldQuantity       : previous quantity
     * @param bundleItems       : product's bundle info
     * @param batchBundleItems  : batch bundle items info
     * @param productBatch      : dbBatch
     * @return
     */
    private HashMap<String, Double> validateBundleBatchQuantity(BigDecimal requestedQuantity, BigDecimal oldQuantity, List<BundleItem> bundleItems, List<BatchBundleItems> batchBundleItems, ProductBatch productBatch) {
        HashMap<String, Double> requestQuantityMap = new HashMap<>();
        HashMap<String, Double> oldQuantityMap = new HashMap<>();


        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        if (safeInventory == null) {
            safeInventory = new Inventory();
            safeInventory.setShopId(token.getShopId());
            safeInventory.prepare(token.getCompanyId());
            safeInventory.setName(Inventory.SAFE);
            safeInventory.setType(Inventory.InventoryType.Storage);
            safeInventory.setActive(Boolean.TRUE);

            inventoryRepository.save(safeInventory);
        }

        if (CollectionUtils.isNullOrEmpty(batchBundleItems) && CollectionUtils.isNullOrEmpty(bundleItems)) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Bundle Items info cannot be blank.");
        }

        boolean isUseBatchInfo = true;
        if (CollectionUtils.isNullOrEmpty(batchBundleItems)) {
            batchBundleItems = new ArrayList<>();
            List<BatchBundleItems> finalBatchBundleItems = batchBundleItems;
            bundleItems.forEach(bundleItem -> {
                BatchBundleItems batchBundleItem = new BatchBundleItems();
                batchBundleItem.setProductId(bundleItem.getProductId());
                BatchBundleItems.BatchItems batchItem = new BatchBundleItems.BatchItems();
                batchItem.setQuantity(bundleItem.getQuantity().multiply(requestedQuantity));
                batchBundleItem.getBatchItems().add(batchItem);
                finalBatchBundleItems.add(batchBundleItem);
            });
            isUseBatchInfo = false;
        }

        /* Old quantity map */
        if (!CollectionUtils.isNullOrEmpty(productBatch.getBundleItems())) {
            for (BatchBundleItems item : productBatch.getBundleItems()) {
                if (!CollectionUtils.isNullOrEmpty(item.getBatchItems())) {
                    for (BatchBundleItems.BatchItems batchItems : item.getBatchItems()) {
                        String inventoryId = StringUtils.isNotBlank(batchItems.getInventoryId()) ? batchItems.getInventoryId() : safeInventory.getId();
                        String batchId = (StringUtils.isBlank(batchItems.getBatchId()) || !isUseBatchInfo) ? "" : batchItems.getBatchId();
                        String key = String.format("%s_%s_%s", StringUtils.isBlank(item.getProductId()) ? "" : item.getProductId(), batchId, inventoryId);

                        oldQuantityMap.put(key, oldQuantityMap.getOrDefault(key, 0D) + batchItems.getQuantity().doubleValue());
                    }
                }
            }
        }

        HashMap<String, BigDecimal> productInfoQuantityMap = new HashMap<>();
        HashMap<String, BigDecimal> batchInfoQuantityMap = new HashMap<>();

        // bundle item info
        for (BundleItem item : bundleItems) {
            productInfoQuantityMap.putIfAbsent(item.getProductId(), productInfoQuantityMap.getOrDefault(item.getProductId(), BigDecimal.ZERO).add(item.getQuantity().multiply(requestedQuantity)));
        }

        // requested quantity map
        for (BatchBundleItems item : batchBundleItems) {
            for (BatchBundleItems.BatchItems batchItems : item.getBatchItems()) {
                batchInfoQuantityMap.putIfAbsent(item.getProductId(), batchInfoQuantityMap.getOrDefault(item.getProductId(), BigDecimal.ZERO).add(batchItems.getQuantity()));
            }
        }

        for (Map.Entry<String, BigDecimal> entry : batchInfoQuantityMap.entrySet()) {
            BigDecimal productQuantity = productInfoQuantityMap.getOrDefault(entry.getKey(), BigDecimal.ZERO);
            if (entry.getValue().doubleValue() > productQuantity.doubleValue()) {
                throw new BlazeInvalidArgException(PRODUCT_BATCH, "Requested Quantity exceeded from allowed quantity.");
            }
        }

        Set<ObjectId> productObjIds = new HashSet<>();
        Set<ObjectId> batchObjIds = new HashSet<>();
        Set<String> productIds = new HashSet<>();
        Set<ObjectId> inventoryObjIds = new HashSet<>();

        for (BatchBundleItems item : batchBundleItems) {
            productIds.add(item.getProductId());

            if (ObjectId.isValid(item.getProductId())) {
                productObjIds.add(new ObjectId(item.getProductId()));
            } else {
                throw new BlazeInvalidArgException(PRODUCT, NOT_VALID_BUNDLE_PRODUCT);
            }

            if (!CollectionUtils.isNullOrEmpty(item.getBatchItems())) {
                for (BatchBundleItems.BatchItems batchItems : item.getBatchItems()) {
                    if (StringUtils.isNotBlank(batchItems.getBatchId()) && ObjectId.isValid(batchItems.getBatchId())) {
                        batchObjIds.add(new ObjectId(batchItems.getBatchId()));
                    }
                    if (StringUtils.isNotBlank(batchItems.getInventoryId()) && ObjectId.isValid(batchItems.getInventoryId())) {
                        inventoryObjIds.add(new ObjectId(batchItems.getInventoryId()));
                    }

                    String inventoryId = StringUtils.isNotBlank(batchItems.getInventoryId()) ? batchItems.getInventoryId() : safeInventory.getId();

                    String key = String.format("%s_%s_%s", StringUtils.isBlank(item.getProductId()) ? "" : item.getProductId(), StringUtils.isBlank(batchItems.getBatchId()) ? "" : batchItems.getBatchId(), inventoryId);

                    Double existingQuantity = requestQuantityMap.getOrDefault(key, 0D);

                    double quantity = batchItems.getQuantity().doubleValue();
                    requestQuantityMap.put(key, existingQuantity + quantity);
                }
            }

        }

        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(inventoryObjIds));
        Set<String> inventoryIds = new HashSet<>(inventoryMap.keySet());
        inventoryIds.add(safeInventory.getId());

        HashMap<String, List<BatchQuantity>> batchQuantitiesMap = batchQuantityRepository.getBatchQuantityForProductInventory(token.getCompanyId(), token.getShopId(), Lists.newArrayList(inventoryIds), Lists.newArrayList(productIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productObjIds));
        HashMap<String, ProductBatch> batchMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchObjIds));

        for (BatchBundleItems item : batchBundleItems) {
            Product product = productMap.get(item.getProductId());

            List<BatchQuantity> batchQuantities = batchQuantitiesMap.get(item.getProductId());

            if (product == null) {
                throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_FOUND);
            }

            if (CollectionUtils.isNullOrEmpty(batchQuantities)) {
                throw new BlazeInvalidArgException("Product Quantity", String.format("%s does not have enough quantity in %s.", product.getName(), safeInventory.getName()));
            }


            HashMap<String, BatchQuantity> quantityByBatchMap = new HashMap<>();

            for (BatchQuantity batchQuantity : batchQuantities) {
                quantityByBatchMap.put(batchQuantity.getBatchId() + "_" + batchQuantity.getInventoryId(), batchQuantity);
            }

            for (BatchBundleItems.BatchItems batchItems : item.getBatchItems()) {
                ProductBatch batch = batchMap.get(batchItems.getBatchId());

                String inventoryId = StringUtils.isNotBlank(batchItems.getInventoryId()) ? batchItems.getInventoryId() : safeInventory.getId();
                Inventory inventory = inventoryMap.get(inventoryId);

                if (StringUtils.isNotBlank(batchItems.getBatchId()) && batch == null) {
                    throw new BlazeInvalidArgException(PRODUCT, BATCH_NOT_EXIST);
                }
                if (StringUtils.isNotBlank(batchItems.getInventoryId()) && inventory == null) {
                    throw new BlazeInvalidArgException(PRODUCT, INVENTORY_NOT_FOUND);
                }

                String key = String.format("%s_%s_%s", StringUtils.isBlank(item.getProductId()) ? "" : item.getProductId(), StringUtils.isBlank(batchItems.getBatchId()) ? "" : batchItems.getBatchId(), inventoryId);

                double requestQuantity = requestQuantityMap.getOrDefault(key, 0D);
                double previousQuantity = oldQuantityMap.getOrDefault(key, 0D);
                double productQuantity = 0;

                if (StringUtils.isNotBlank(batchItems.getBatchId()) && batch != null) {
                    for (BatchQuantity batchQuantity : batchQuantities) {
                        if (batchQuantity.getBatchId().equalsIgnoreCase(batchItems.getBatchId()) && batchQuantity.getInventoryId().equalsIgnoreCase(inventoryId)) {
                            productQuantity += batchQuantity.getQuantity().doubleValue();
                        }
                    }
                } else if (StringUtils.isBlank(batchItems.getBatchId())) {
                    for (BatchQuantity batchQuantity : batchQuantities) {
                        if (batchQuantity.getInventoryId().equalsIgnoreCase(inventoryId)) {
                            productQuantity += batchQuantity.getQuantity().doubleValue();
                        }
                    }
                }

                if (((productQuantity + previousQuantity) < requestQuantity)) {
                    if (StringUtils.isNotBlank(batchItems.getBatchId()) && batch != null) {
                        throw new BlazeInvalidArgException("Product Quantity", String.format("Batch %s does not have enough quantity.", batch.getSku()));
                    }
                    throw new BlazeInvalidArgException("Product Quantity", PRODUCT_NOT_ENOUGH_QUANTITY);
                }
            }
        }

        productBatch.setBundleItems(batchBundleItems);
        return requestQuantityMap;
    }
    private void createQueuedTransactionJobForBatchQuantity(ProductBatch productBatch, Inventory.InventoryType inventoryType, ProductBatchQueuedTransaction.OperationType operationType, BigDecimal quantity, String sourceChildId, InventoryOperation.SubSourceAction subSourceAction, HashMap<String, Double> bundleProductQuantity, Product.ProductType productType, String inventoryId) {
        Inventory dbInventory = null;

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if ((!shop.isRetail()) && (productBatch.getStatus() != ProductBatch.BatchStatus.READY_FOR_SALE)) {
            inventoryType = Inventory.InventoryType.Quarantine;
        } else {
            inventoryType = Inventory.InventoryType.Storage;
        }
        if (StringUtils.isNotBlank(inventoryId)) {
            dbInventory = inventoryRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), inventoryId);
            if (dbInventory == null && token.getGrowAppType() == ConnectAuthToken.GrowAppType.Operations) {
                // Throw error only when called from operations
                throw new BlazeInvalidArgException(PRODUCT_BATCH, ROOM_NOT_FOUND);
            }
        }
        if (dbInventory == null) {
            if (Inventory.InventoryType.Quarantine == inventoryType) {
                dbInventory = inventoryRepository.getInventory(token.getCompanyId(), productBatch.getShopId(), Inventory.QUARANTINE);
            } else {
                dbInventory = inventoryRepository.getInventory(token.getCompanyId(), productBatch.getShopId(), Inventory.SAFE);
            }
        }

        if (dbInventory == null) {
            dbInventory = new Inventory();
            dbInventory.prepare(token.getCompanyId());
            dbInventory.setShopId(productBatch.getShopId());
            if (Inventory.InventoryType.Quarantine == inventoryType) {
                dbInventory.setType(Inventory.InventoryType.Quarantine);
                dbInventory.setName(Inventory.QUARANTINE);
            } else {
                dbInventory.setName(Inventory.SAFE);
                dbInventory.setType(Inventory.InventoryType.Storage);
            }
            inventoryRepository.save(dbInventory);
        }

        ProductBatchQueuedTransaction queuedTransaction = new ProductBatchQueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.ProductBatch);
        queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(productBatch.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setInventoryId(dbInventory.getId());
        queuedTransaction.setOperationType(operationType);
        queuedTransaction.setQuantity(quantity);
        queuedTransaction.setBundleQuantityMap(bundleProductQuantity);
        queuedTransaction.setProductType(productType);


        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);
        queuedTransaction.setSourceChildId(sourceChildId);
        queuedTransaction.setSubSourceAction(subSourceAction);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());


    }

    /**
     * Override method to calculate total cogs for given products.
     *
     * @param items : list of product items and quantity.
     * @return : total cogs
     */
    @Override
    public BigDecimal calculateEstimateCogs(List<BatchBundleItemsRequest> items) {
        if (CollectionUtils.isNullOrEmpty(items)) {
            return BigDecimal.ZERO;
        }

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> batchIds = new ArrayList<>();
        for (BatchBundleItems item : items) {
            if (StringUtils.isNotBlank(item.getProductId()) && ObjectId.isValid(item.getProductId())) {
                productIds.add(new ObjectId(item.getProductId()));
            }
            if (!CollectionUtils.isNullOrEmpty(item.getBatchItems())) {
                item.getBatchItems().forEach(batchItems -> {
                    if (StringUtils.isNotBlank(batchItems.getBatchId()) && ObjectId.isValid(batchItems.getBatchId())) {
                        batchIds.add(new ObjectId(batchItems.getBatchId()));
                    }
                });
            }
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        Map<String, ProductBatch> oldestBatch = productBatchRepository.getOldestBatchForProductList(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productMap.keySet()));
        HashMap<String, ProductBatch> batchMap = productBatchRepository.listAsMap(token.getCompanyId(), batchIds);

        BigDecimal cogs = BigDecimal.ZERO;

        for (BatchBundleItemsRequest item : items) {
            Product product = productMap.get(item.getProductId());
            if (product == null) {
                continue;
            }
            if (!CollectionUtils.isNullOrEmpty(item.getBatchItems())) {
                for (BatchBundleItems.BatchItems batchItems : item.getBatchItems()) {
                    ProductBatch batch = batchMap.get(batchItems.getBatchId());

                    if (batch == null) {
                        continue;
                    }

                    cogs = cogs.add(batch.getFinalUnitCost().multiply(batchItems.getQuantity()));
                }
            } else {
                ProductBatch batch = oldestBatch.get(item.getProductId());

                if (batch == null) {
                    continue;
                }

                cogs = cogs.add(batch.getFinalUnitCost().multiply(item.getQuantity()));
            }

        }
        return cogs;
    }
    private void prepareProductBatchResult(SearchResult<ProductBatchQuantityResult> result) {
        Brand brand;
        Vendor vendor;
        Product product;
        PurchaseOrder purchaseOrder;
        List<ObjectId> poIds = new ArrayList<>();
        List<ObjectId> brandIds = new ArrayList<>();
        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> vendorIds = new ArrayList<>();

        for (ProductBatchQuantityResult batchQuantityResult : result.getValues()) {
            if (StringUtils.isNotBlank(batchQuantityResult.getBrandId()) && ObjectId.isValid(batchQuantityResult.getBrandId())) {
                brandIds.add(new ObjectId(batchQuantityResult.getBrandId()));
            }
            if (StringUtils.isNotBlank(batchQuantityResult.getProductId()) && ObjectId.isValid(batchQuantityResult.getProductId())) {
                productIds.add(new ObjectId(batchQuantityResult.getProductId()));
            }
            if (StringUtils.isNotBlank(batchQuantityResult.getVendorId()) && ObjectId.isValid(batchQuantityResult.getVendorId())) {
                vendorIds.add(new ObjectId(batchQuantityResult.getVendorId()));
            }
            if (StringUtils.isNotBlank(batchQuantityResult.getPurchaseOrderId()) && ObjectId.isValid(batchQuantityResult.getPurchaseOrderId())) {
                poIds.add(new ObjectId(batchQuantityResult.getPurchaseOrderId()));
            }

        }

        HashMap<String, Brand> brandMap = brandRepository.listAsMap(token.getCompanyId(), brandIds);
        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(token.getCompanyId(), vendorIds);
        HashMap<String, PurchaseOrder> purchaseOrderHashMap = purchaseOrderRepository.listAsMap(token.getCompanyId(), poIds);

        for (ProductBatchQuantityResult batchQuantityResult : result.getValues()) {
            brand = brandMap.get(batchQuantityResult.getBrandId());
            product = productHashMap.get(batchQuantityResult.getProductId());
            vendor = vendorHashMap.get(batchQuantityResult.getVendorId());
            purchaseOrder = purchaseOrderHashMap.get(batchQuantityResult.getPurchaseOrderId());


            if (brand != null) {
                batchQuantityResult.setBrandName((StringUtils.isBlank(brand.getName()) ? "" : brand.getName()));
            }

            if (product != null) {
                batchQuantityResult.setProductName((StringUtils.isBlank(product.getName()) ? "" : product.getName()));
            }

            if (vendor != null) {
                batchQuantityResult.setVendorName((StringUtils.isBlank(vendor.getName()) ? "" : vendor.getName()));
            }
            if (purchaseOrder != null) {
                batchQuantityResult.setPoNumber(purchaseOrder.getPoNumber());
                batchQuantityResult.setPurchaseOrderId(purchaseOrder.getId());
            }

        }
    }
    private boolean checkIsInventoryUpdated(String dbRoomId, String requestRoomId) {
      dbRoomId = StringUtils.isBlank(dbRoomId) ? "" : dbRoomId;
      requestRoomId = StringUtils.isBlank(requestRoomId) ? "" : requestRoomId;
      return !dbRoomId.equalsIgnoreCase(requestRoomId);
    }

    /**
     * Private method to calculate taxes for product batch
     *
     * @param shop            : shop
     * @param productBatch    : product batch
     * @param quantity        : quantity
     * @param vendor          : vendor
     * @param companyLicense  : company license
     * @param dbProduct       : product
     * @param productCategory : product category
     */
    private void calculateTaxForBatch(Shop shop, ProductBatch productBatch, BigDecimal quantity, Vendor vendor, CompanyLicense companyLicense, Product dbProduct, ProductCategory productCategory) {
        Vendor.ArmsLengthType armsLengthType = (vendor != null) ? vendor.getArmsLengthType() : Vendor.ArmsLengthType.ARMS_LENGTH;
        //OZ tax calculation
        CultivationTaxResult totalCultivationTax = null;
        if (dbProduct.getCannabisType().equals(Product.CannabisType.LIVE_PLANT)) {
            totalCultivationTax = new CultivationTaxResult();
            productBatch.setTotalCultivationTax(totalCultivationTax.getTotalCultivationTax());
            productBatch.setTotalExciseTax(BigDecimal.ZERO);
            productBatch.setPerUnitExciseTax(BigDecimal.ZERO);
        } else {
            if ((!shop.isRetail()) && (Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && (PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT.equals(productBatch.getFlowerSourceType()))) {

                totalCultivationTax = taxCalulationService.calculateOzTax(token.getCompanyId(), token.getShopId(), dbProduct, productCategory, quantity.doubleValue(), productBatch);
                productBatch.setTotalCultivationTax(totalCultivationTax.getTotalCultivationTax());
            } else {
                if (productCategory != null) {
                    HashMap<String, BigDecimal> taxInfo = taxCalulationService.calculateExciseTax(token.getCompanyId(), token.getShopId(), productBatch.getQuantity(), productBatch.getCostPerUnit(), armsLengthType, dbProduct.isCannabisProduct(productCategory.isCannabis()));
                    productBatch.setTotalExciseTax(taxInfo.getOrDefault("totalExciseTax", BigDecimal.ZERO));
                    productBatch.setPerUnitExciseTax(taxInfo.getOrDefault("unitExciseTax", BigDecimal.ZERO));
                }
            }
        }
    }

}
