package com.fourtwenty.core.services.mgmt.impl;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkInventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.fourtwenty.core.services.mgmt.InventoryTransferHistoryService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.google.inject.Provider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Raja Dushyant Vashishtha
 */
public class InventoryTransferHistoryServiceImpl extends AbstractAuthServiceImpl implements InventoryTransferHistoryService {

    private static final String TRANSFER_REQUEST = "Transfer Request";
    private static final String TRANSFER_REQUEST_NOT_FOUND = "Please add inventory to transfer";
    private static final String INVENTORY_TRANSFER_HISTORY = "Inventory transfer history";
    private static final String INVENTORY_TRANSFER_NOT_FOUND = "Inventory transfer history does not exist.";
    private static final String FROM_BATCH_NOT_FOUND = "From batch does not exist.";
    private static final String ACTIVE_TRANSACTION = "There are active pending transactions. Please wait until all transactions are completed";
    private static final String PRODUCT = "Product";
    private static final String PRODUCT_NOT_BLANK = "Product cannot be blank.";
    private static final String PRODUCT_NOT_EXIST = "Product does not exist.";

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryTransferHistoryServiceImpl.class);
    @Inject
    private InventoryTransferHistoryRepository transferHistoryRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    private UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private ProductChangeLogRepository productChangeLogRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private ProductPrepackageQuantityRepository quantityRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private BatchQuantityService batchQuantityService;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private BarcodeService barcodeService;
    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private MetrcAccountService metrcAccountService;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private CompanyRepository companyRepository;

    @Inject
    public InventoryTransferHistoryServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Get inventory transfer history by id
     *
     * @param historyId : history id
     */
    @Override
    public InventoryTransferHistoryResult getInventoryTransferHistoryById(String historyId) {
        InventoryTransferHistoryResult transferHistory = transferHistoryRepository.get(token.getCompanyId(), historyId, InventoryTransferHistoryResult.class);
        if (transferHistory == null) {
            throw new BlazeInvalidArgException("Inventory History", "Inventory transfer history does not found");
        }

        HashMap<String, Shop> shopMap = shopRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());

        HashSet<ObjectId> batchIds = new HashSet<>();

        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                batchIds.add(new ObjectId(log.getFromBatchId()));
            }
            if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                batchIds.add(new ObjectId(log.getToBatchId()));
            }

        }

        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
        prepareInventoryTransferResult(transferHistory, shopMap, inventoryMap, employeeMap, productBatchHashMap);

        return transferHistory;
    }

    /**
     * This method prepares result of inventory transfer history
     *
     * @param transferHistory : transfer inventory object
     * @param shopMap         : shop map
     * @param inventoryMap    : inventory map
     * @param employeeMap     : employee map
     */
    private void prepareInventoryTransferResult(InventoryTransferHistoryResult transferHistory,
                                                HashMap<String, Shop> shopMap,
                                                HashMap<String, Inventory> inventoryMap,
                                                HashMap<String, Employee> employeeMap,
                                                HashMap<String, ProductBatch> productBatchHashMap) {

        String fromShopName = "";
        String toShopName = "";
        String fromInventoryName = "";
        String toInventoryName = "";
        String createdByEmployeeName = "";
        String acceptByEmployeeName = "";
        String declineByEmployeeName = "";

        Employee employee = employeeMap.get(transferHistory.getCreateByEmployeeId());
        if (employee != null) {
            createdByEmployeeName = employee.getFirstName() + " " + employee.getLastName();
        }

        Employee acceptedByEmployee = employeeMap.get(transferHistory.getAcceptByEmployeeId());
        if (acceptedByEmployee != null) {
            acceptByEmployeeName = acceptedByEmployee.getFirstName() + " " + acceptedByEmployee.getLastName();
        }

        Employee declinedByEmployee = employeeMap.get(transferHistory.getDeclineByEmployeeId());
        if (declinedByEmployee != null) {
            declineByEmployeeName = declinedByEmployee.getFirstName() + " " + declinedByEmployee.getLastName();
        }

        Shop fromShop = shopMap.get(transferHistory.getFromShopId());

        String timezone = null;
        if (fromShop != null) {
            fromShopName = fromShop.getName();
            timezone = fromShop.getTimeZone();
        }

        Shop toShop = shopMap.get(transferHistory.getToShopId());
        if (toShop != null) {
            toShopName = toShop.getName();
        }

        Inventory fromInventory = inventoryMap.get(transferHistory.getFromInventoryId());
        if (fromInventory != null) {
            fromInventoryName = fromInventory.getName();
        }

        Inventory toInventory = inventoryMap.get(transferHistory.getToInventoryId());
        if (toInventory != null) {
            toInventoryName = toInventory.getName();
        }

        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            ProductBatch fromProductBatch = productBatchHashMap.get(log.getFromBatchId());
            if (fromProductBatch != null) {
                String batchInfo = String.format("%s | %s", DateUtil.toDateFormatted(fromProductBatch.getPurchasedDate()), fromProductBatch.getSku());
                log.setFromBatchInfo(batchInfo);
                log.setFromProductBatch(fromProductBatch);
            }

            if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                ProductBatch toProductBatch = productBatchHashMap.get(log.getToBatchId());
                if (toProductBatch != null) {
                    log.setToProductBatch(toProductBatch);
                }
            }

            log.setPrevTransferAmt(log.getTransferAmount());
        }


        transferHistory.setCreatedByEmployeeName(createdByEmployeeName);
        transferHistory.setAcceptByEmployeeName(acceptByEmployeeName);
        transferHistory.setDeclineByEmployeeName(declineByEmployeeName);

        transferHistory.setFromShopName(fromShopName);
        transferHistory.setToShopName(toShopName);

        transferHistory.setFromInventoryName(fromInventoryName);
        transferHistory.setToInventoryName(toInventoryName);
    }

    /**
     * Inventory transfer
     *
     * @param request
     */
    @Override
    public InventoryTransferHistory bulkTransferInventory(InventoryTransferHistory request) {
        if (request.getTransferLogs().size() == 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, TRANSFER_REQUEST_NOT_FOUND);
        }

        String fromShopId = StringUtils.isBlank(request.getFromShopId()) ? token.getShopId() : request.getFromShopId();
        String toShopId = StringUtils.isBlank(request.getToShopId()) ? token.getShopId() : request.getToShopId();

        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(), fromShopId, QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, ACTIVE_TRANSACTION);
        }*/

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId());

        if (!inventoryHashMap.containsKey(request.getFromInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'from' inventory.");
        }
        if (!inventoryHashMap.containsKey(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'to' inventory.");
        }
        if (request.getFromInventoryId().equalsIgnoreCase(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Cannot transfer within the same inventory.");
        }
        if (!toShopId.equals(fromShopId) && token.getAppTarget() != CompanyFeatures.AppTarget.Retail) {
            Shop toShop = shopRepository.getById(toShopId);
            if (!toShop.getAppTarget().equals(token.getAppTarget()) && StringUtils.isBlank(request.getDriverId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Driver cannot be empty.");
            }
        }
        List<ObjectId> productObjectIds = new ArrayList<>();
        for (InventoryTransferLog log : request.getTransferLogs()) {
            productObjectIds.add(new ObjectId(log.getProductId()));
        }
        HashMap<String, Product> productMap = productRepository.findItemsInAsMap(token.getCompanyId(), fromShopId, productObjectIds);
        request.setToShopId(toShopId);
        request.setFromShopId(fromShopId);

        this.checkInventoryForTransfer(request, productMap);

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), fromShopId, "InventoryTransfer");
        String transferNo = "" + sequence.getCount();

        InventoryTransferHistory transferHistory = new InventoryTransferHistory();
        transferHistory.prepare(token.getCompanyId());
        transferHistory.setShopId(token.getShopId());
        transferHistory.setCreateByEmployeeId(token.getActiveTopUser().getUserId());
        transferHistory.setStatus(InventoryTransferHistory.TransferStatus.PENDING);
        transferHistory.setFromShopId(fromShopId);
        transferHistory.setToShopId(toShopId);
        transferHistory.setFromInventoryId(request.getFromInventoryId());
        transferHistory.setToInventoryId(request.getToInventoryId());
        transferHistory.setTransferNo(transferNo);
        transferHistory.setTransferByBatch(request.isTransferByBatch());
        if (request.isCompleteTransfer()) {
            transferHistory.setProcessing(Boolean.TRUE);
        }

        if (StringUtils.isBlank(request.getDriverId())) {
            transferHistory.setDriverId(token.getActiveTopUser().getUserId());
        } else {
            transferHistory.setDriverId(request.getDriverId());
        }

        // check for cross shop when we are create and accept once
        /*if (request.isCompleteTransfer()) {
            acceptTransferHistory(transferHistory);
        }*/

        transferHistory = processInventoryTransferForNewProducts(request, transferHistory);

        InventoryTransferHistory updatedTransferHistory = transferHistoryRepository.save(transferHistory);
        if (request.isCompleteTransfer()) {
            InventoryTransferStatusRequest statusRequest = new InventoryTransferStatusRequest();
            statusRequest.setStatus(Boolean.TRUE);

            this.updateInventoryTransferStatus(updatedTransferHistory.getId(), statusRequest);
        } else {
            this.createQueueTransactionJobForTransfer(transferHistory);
        }
        return updatedTransferHistory;
    }

    private InventoryTransferHistory processInventoryTransferForNewProducts(InventoryTransferHistory request, InventoryTransferHistory transferHistory) {

        LinkedHashSet<InventoryTransferLog> logList = new LinkedHashSet<>();

        List<ObjectId> prepackageItemIds = new ArrayList<>();
        for (InventoryTransferLog log : request.getTransferLogs()) {
            if (!ObjectId.isValid(log.getProductId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, log.getProductId() + " is an invalid id.");
            } else if (log.getTransferAmount().doubleValue() <= 0) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Transfer amount must be greater than 0.");
            }

            logList.add(log);
            if (StringUtils.isNotBlank(log.getPrepackageItemId()) && ObjectId.isValid(log.getPrepackageItemId())) {
                prepackageItemIds.add(new ObjectId(log.getPrepackageItemId()));
            }
        }

        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), prepackageItemIds);
        for (InventoryTransferLog log : logList) {
            PrepackageProductItem item = prepackageProductItemHashMap.get(log.getPrepackageItemId());
            if (item != null) {
                log.setFromBatchId(item.getBatchId());

                Prepackage prepackage = prepackageHashMap.get(item.getPrepackageId());
                if (prepackage != null) {
                    log.setPrepackageName(prepackage.getName());
                }
            }
        }

        transferHistory.setTransferLogs(logList);


//        List<ObjectId> productObjectIds = new ArrayList<>();
//        Set<String> productIds = new HashSet<>();
//        for (InventoryTransferLog log : request.getTransferLogs()) {
//            productObjectIds.add(new ObjectId(log.getProductId()));
//            productIds.add(log.getProductId());
//        }
//        HashMap<String, Product> productMap = productRepository.findItemsInAsMap(token.getCompanyId(), transferHistory.getFromShopId(), productObjectIds);


//        checkAndDeductInitial(request, transferHistory, productMap, false, productIds);
//        checkAndDeductInitial(request, transferHistory, productMap, true, productIds);

        return transferHistory;
    }

    private void checkInventoryForTransfer(InventoryTransferHistory request, HashMap<String, Product> productMap) {
        for (InventoryTransferLog inventoryTransferLog : request.getTransferLogs()) {

            if (StringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {
                ProductPrepackageQuantity fromPreQuantity = quantityRepository.getQuantity(token.getCompanyId(),
                        request.getFromShopId(),
                        request.getFromInventoryId(),
                        inventoryTransferLog.getPrepackageItemId());
                if (fromPreQuantity == null) {
                    throw new BlazeInvalidArgException("PrepackageProduct", "Prepackage Line Item does not exist");
                }

                int transferAmount = inventoryTransferLog.getTransferAmount().intValue();
                if (fromPreQuantity.getQuantity() < transferAmount) {
                    throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Amount specified exceeds available inventory for product");
                }

            } else {
                Product product = productMap.get(inventoryTransferLog.getProductId());
                ProductQuantity productQuantity = null;
                for (ProductQuantity pq : product.getQuantities()) {
                    if (pq.getInventoryId().equals(request.getFromInventoryId())) {
                        productQuantity = pq;
                        break;
                    }
                }

                BigDecimal inventoryTransferAmount = inventoryTransferLog.getTransferAmount();
                if (request.isTransferByBatch()) {
                    if (StringUtils.isNotBlank(inventoryTransferLog.getFromBatchId()) && ObjectId.isValid(inventoryTransferLog.getFromBatchId())) {
                        BatchQuantity dbBatchQuantity = batchQuantityRepository.getBatchQuantityForInventory(token.getCompanyId(), token.getShopId(), inventoryTransferLog.getProductId(), request.getFromInventoryId(), inventoryTransferLog.getFromBatchId());
                        if (dbBatchQuantity != null) {
                            if (dbBatchQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                                throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than batch quantity.");
                            }
                        }
                    } else {
                        Iterable<BatchQuantity> sortedBatches = batchQuantityRepository.getBatchQuantitiesForInventorySorted(token.getCompanyId(), token.getShopId(), inventoryTransferLog.getProductId(), request.getFromInventoryId(), "{batchPurchaseDate:-1}");
                        for (BatchQuantity batchQuantity : sortedBatches) {
                            if (batchQuantity != null && batchQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                                throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than batch quantity");
                            }
                        }
                    }

                    if (productQuantity == null || productQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                        throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than inventory amount for: " + product.getName());
                    }
                } else {
                    if (productQuantity == null || productQuantity.getQuantity().doubleValue() < inventoryTransferAmount.doubleValue()) {
                        throw new BlazeInvalidArgException("TransferAmount", "transfer amount is greater than inventory amount for: " + product.getName());
                    }
                }
            }
        }
    }


    /**
     * This method gets inventory history of a shop
     *
     * @param start  : start
     * @param limit  : limit
     * @param status
     */
    @Override
    public SearchResult<InventoryTransferHistoryResult> getInventoryHistories(int start, int limit, String status, String startDate, String endDate) {
        if (status == null) {
            status = "PENDING";
        }

        List<String> statusList = Arrays.asList(status.split("\\s*,\\s*"));
        if (statusList.isEmpty()) {
            statusList.add(InventoryTransferHistory.TransferStatus.PENDING.toString());
        }

        List<InventoryTransferHistory.TransferStatus> transferStatusList = new ArrayList<>();
        for (String s : statusList) {
            transferStatusList.add(InventoryTransferHistory.TransferStatus.valueOf(s));
        }


        SearchResult<InventoryTransferHistoryResult> searchResult = new SearchResult<>();
        List<InventoryTransferHistoryResult> list = new ArrayList<>();

        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SearchResult<InventoryTransferHistoryResult> itemByStatus = transferHistoryRepository.findItemByStatus(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit, transferStatusList, false, InventoryTransferHistoryResult.class);

            SearchResult<InventoryTransferHistoryResult> itemByStatusToShop = transferHistoryRepository.findItemByStatus(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit, transferStatusList, true, InventoryTransferHistoryResult.class);

            if (itemByStatus != null && itemByStatus.getValues().size() > 0) {
                for (InventoryTransferHistoryResult inventoryTransferHistoryResult : itemByStatus.getValues()) {
                    list.add(inventoryTransferHistoryResult);
                }
            }
            if (itemByStatusToShop != null && itemByStatusToShop.getValues().size() > 0) {
                for (InventoryTransferHistoryResult inventoryTransferHistoryResult : itemByStatusToShop.getValues()) {
                    list.add(inventoryTransferHistoryResult);
                }
            }
        } else {

            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");


            DateTime jodaEndDate;
            if (StringUtils.isNotBlank(endDate)) {
                jodaEndDate = formatter.parseDateTime(endDate);
                jodaEndDate = jodaEndDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);

            } else {
                jodaEndDate = DateTime.now();
            }


            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            DateTime startDateTime = null;
            if (StringUtils.isNotBlank(startDate)) {
                startDateTime = formatter.parseDateTime(startDate);
            } else {
                startDateTime = DateTime.now();
            }

            long timeZoneStartDateMillis = startDateTime.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = jodaEndDate.minusMinutes(timeZoneOffset).getMillis();


            SearchResult<InventoryTransferHistoryResult> itemSearch = transferHistoryRepository.findItemByModifiedDate(token.getCompanyId(), token.getShopId(),
                    timeZoneStartDateMillis, timeZoneEndDateMillis, "{modified:-1}",
                    start, limit, transferStatusList, false, InventoryTransferHistoryResult.class);

            SearchResult<InventoryTransferHistoryResult> itemSearchByToShop = transferHistoryRepository.findItemByModifiedDate(token.getCompanyId(), token.getShopId(),
                    timeZoneStartDateMillis, timeZoneEndDateMillis, "{modified:-1}",
                    start, limit, transferStatusList, true, InventoryTransferHistoryResult.class);

            if (itemSearch != null && itemSearch.getValues().size() > 0) {
                for (InventoryTransferHistoryResult inventoryTransferHistoryResult : itemSearch.getValues()) {
                    list.add(inventoryTransferHistoryResult);
                }
            }
            if (itemSearchByToShop != null && itemSearchByToShop.getValues().size() > 0) {
                for (InventoryTransferHistoryResult inventoryTransferHistoryResult : itemSearchByToShop.getValues()) {
                    list.add(inventoryTransferHistoryResult);
                }
            }
        }

        searchResult.setValues(list);
        searchResult.setTotal((long) list.size());

        HashMap<String, Shop> shopMap = shopRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());


        if (searchResult != null && searchResult.getValues() != null) {
            HashSet<ObjectId> productIds = new HashSet<>();
            for (InventoryTransferHistoryResult result : searchResult.getValues()) {
                for (InventoryTransferLog log : result.getTransferLogs()) {
                    if (StringUtils.isNotBlank(log.getProductId()) && ObjectId.isValid(log.getProductId())) {
                        productIds.add(new ObjectId(log.getProductId()));
                    }
                }
            }

            HashSet<ObjectId> batchIds = new HashSet<>();
            for (InventoryTransferHistoryResult result : searchResult.getValues()) {
                for (InventoryTransferLog log : result.getTransferLogs()) {
                    if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                        batchIds.add(new ObjectId(log.getFromBatchId()));
                    }
                }
            }

            HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
            HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));

            for (InventoryTransferHistoryResult historyResult : searchResult.getValues()) {
                prepareInventoryTransferResult(historyResult, shopMap, inventoryMap, employeeMap, productBatchHashMap);
                prepareInventoryTransferProductResult(historyResult, productHashMap);
            }
        }

        return searchResult;
    }

    /**
     * Update transfer request
     *
     * @param historyId       : history id
     * @param transferHistory : transfer history object
     */
    @Override
    public InventoryTransferHistory updateInventoryHistoryResult(String historyId, InventoryTransferHistory transferHistory) {

        InventoryTransferHistory dbInventoryTransferHistory = transferHistoryRepository.get(token.getCompanyId(), historyId);
        if (dbInventoryTransferHistory == null) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, INVENTORY_TRANSFER_NOT_FOUND);
        }

        if (dbInventoryTransferHistory.getStatus() != InventoryTransferHistory.TransferStatus.PENDING) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Inventory history can't be update");
        }

        String fromShopId = StringUtils.isBlank(transferHistory.getFromShopId()) ? token.getShopId() : transferHistory.getFromShopId();
        String toShopId = StringUtils.isBlank(transferHistory.getToShopId()) ? token.getShopId() : transferHistory.getToShopId();

        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(), fromShopId, QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, ACTIVE_TRANSACTION);
        }*/
        if (!toShopId.equals(fromShopId) && token.getAppTarget() != CompanyFeatures.AppTarget.Retail) {
            Shop toShop = shopRepository.getById(toShopId);
            if (!toShop.getAppTarget().equals(token.getAppTarget()) && StringUtils.isBlank(transferHistory.getDriverId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Driver cannot be empty.");
            }
        }
        if (transferHistory.getTransferLogs().size() == 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, TRANSFER_REQUEST_NOT_FOUND);
        }

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId());

        if (!inventoryHashMap.containsKey(transferHistory.getFromInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'from' inventory.");
        }
        if (!inventoryHashMap.containsKey(transferHistory.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'to' inventory.");
        }
        if (transferHistory.getFromInventoryId().equalsIgnoreCase(transferHistory.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Cannot transfer within the same inventory.");
        }

        dbInventoryTransferHistory.setFromShopId(fromShopId);
        dbInventoryTransferHistory.setToShopId(toShopId);
        dbInventoryTransferHistory.setFromInventoryId(transferHistory.getFromInventoryId());
        dbInventoryTransferHistory.setToInventoryId(transferHistory.getToInventoryId());
        dbInventoryTransferHistory.setTransferByBatch(transferHistory.isTransferByBatch());
        if (StringUtils.isBlank(transferHistory.getDriverId())) {
            dbInventoryTransferHistory.setDriverId(token.getActiveTopUser().getUserId());
        } else {
            dbInventoryTransferHistory.setDriverId(transferHistory.getDriverId());
        }
        LinkedHashSet<InventoryTransferLog> logList = new LinkedHashSet<>();

        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            if (!ObjectId.isValid(log.getProductId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, log.getProductId() + " is an invalid id.");
            } else if (log.getTransferAmount().doubleValue() <= 0) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Transfer amount must be greater than 0.");
            }

            log.setPrevTransferAmt(log.getTransferAmount());
            logList.add(log);
        }

        // check first
//        checkAndDeduct(transferHistory, dbInventoryTransferHistory, fromShopId, false);

        // now do again but deduct
//        checkAndDeduct(transferHistory, dbInventoryTransferHistory, fromShopId, true);


        //  Check for validating removed products
//        checkForRemovedAndNewTransferProducts(dbInventoryTransferHistory, transferHistory);

        dbInventoryTransferHistory = processInventoryTransferForNewProducts(transferHistory, dbInventoryTransferHistory);
//        dbInventoryTransferHistory.setTransferLogs(logList);
        createQueueTransactionJobForTransfer(dbInventoryTransferHistory);

        return transferHistoryRepository.update(token.getCompanyId(), historyId, dbInventoryTransferHistory);
    }

    private void checkAndDeduct(InventoryTransferHistory transferHistory, InventoryTransferHistory dbInventoryTransferHistory, String fromShopId, boolean shouldDeduct) {

        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
        for (InventoryTransferLog dbInventoryTransferLog : dbInventoryTransferHistory.getTransferLogs()) {
            if (StringUtils.isNotBlank(dbInventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(dbInventoryTransferLog.getPrepackageItemId())) {
                ProductPrepackageQuantity prepackageQuantity = quantityRepository.getQuantity(token.getCompanyId(), fromShopId, dbInventoryTransferHistory.getFromInventoryId(), dbInventoryTransferLog.getPrepackageItemId());
                if (prepackageQuantity == null) {
                    throw new BlazeInvalidArgException("Prepackage Quantity", "Prepackage quantity is not found");
                } else {
                    int totalPreQuantity = prepackageQuantity.getQuantity() + dbInventoryTransferLog.getTransferAmount().intValue();

                    for (InventoryTransferLog requestInventoryTransferLog : transferHistory.getTransferLogs()) {
                        if (requestInventoryTransferLog.getPrepackageItemId().equals(dbInventoryTransferLog.getPrepackageItemId())) {
                            if (requestInventoryTransferLog.getTransferAmount().doubleValue() > totalPreQuantity) {
                                throw new BlazeInvalidArgException("Transfer Amount", "Transfer amount is greater than Prepackage Quantity ");
                            } else {
                                if (shouldDeduct) {
                                    prepackageQuantity.setQuantity(totalPreQuantity - requestInventoryTransferLog.getTransferAmount().intValue());
                                    quantityRepository.update(token.getCompanyId(), prepackageQuantity.getId(), prepackageQuantity);


                                    Product product = productRepository.get(token.getCompanyId(), dbInventoryTransferLog.getProductId());
                                    if (product != null) {
                                        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), product.getId());
                                        productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                                                product, productPrepackageQuantities, "Preparing for transfer request: " + dbInventoryTransferHistory.getTransferNo()));
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                Product product = productRepository.get(token.getCompanyId(), dbInventoryTransferLog.getProductId());
                for (ProductQuantity productQuantity : product.getQuantities()) {
                    if (productQuantity.getInventoryId().equals(dbInventoryTransferHistory.getFromInventoryId())) {
                        BigDecimal finalQuantity = productQuantity.getQuantity().add(dbInventoryTransferLog.getTransferAmount());

                        for (InventoryTransferLog reqInventoryTransferLog : transferHistory.getTransferLogs()) {
                            if (reqInventoryTransferLog.getProductId().equals(dbInventoryTransferLog.getProductId())) {

                                BigDecimal transferAmount = reqInventoryTransferLog.getTransferAmount();
                                if (transferAmount.doubleValue() > finalQuantity.doubleValue()) {
                                    throw new BlazeInvalidArgException("Transfer", "Transfer amount is greater than amount available in inventory for " + product.getName());
                                } else {
                                    if (shouldDeduct) {
                                        productQuantity.setQuantity(finalQuantity.subtract(transferAmount));
                                        productRepository.update(token.getCompanyId(), product.getId(), product);

                                        List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), product.getId());
                                        productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                                                product, productPrepackageQuantities, "Preparing for transfer request: " + transferHistory.getTransferNo()));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        productChangeLogRepository.save(productChangeLogs);
    }

    private void checkForRemovedAndNewTransferProducts(InventoryTransferHistory oldTransferLog, InventoryTransferHistory newTransferLog) {
        LinkedHashSet<InventoryTransferLog> addedProductsList = new LinkedHashSet<>();
        LinkedHashSet<InventoryTransferLog> removedProductsList = new LinkedHashSet<>();

        for (InventoryTransferLog newInventoryTransferLog : newTransferLog.getTransferLogs()) {
            if (oldTransferLog.getTransferLogs().stream().noneMatch(o -> o.getProductId().equals(newInventoryTransferLog.getProductId()))) {
                addedProductsList.add(newInventoryTransferLog);
            }
        }
        for (InventoryTransferLog oldInventoryTransferLog : oldTransferLog.getTransferLogs()) {
            if (newTransferLog.getTransferLogs().stream().noneMatch(o -> o.getProductId().equals(oldInventoryTransferLog.getProductId()))) {
                removedProductsList.add(oldInventoryTransferLog);
            }
        }

//        // Add quantity back to from inventory
//        for (InventoryTransferLog inventoryTransferLog : removedProductsList) {
//            if (BLStringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {
//                ProductPrepackageQuantity prepackageQuantity = quantityRepository.getQuantity(token.getCompanyId(), newTransferLog.getFromShopId(), newTransferLog.getFromInventoryId(), inventoryTransferLog.getPrepackageItemId());
//                if (prepackageQuantity == null) {
//                    throw new BlazeInvalidArgException("Prepackage Quantity", "Prepackage quantity is not found");
//                } else {
//                    prepackageQuantity.setQuantity(prepackageQuantity.getQuantity() + inventoryTransferLog.getTransferAmount().intValue());
//                    quantityRepository.update(token.getCompanyId(), prepackageQuantity.getId(), prepackageQuantity);
//                }
//            } else {
//                Product product = productRepository.get(token.getCompanyId(), inventoryTransferLog.getProductId());
//                for (ProductQuantity productQuantity : product.getQuantities()) {
//                    if (productQuantity.getInventoryId().equals(newTransferLog.getFromInventoryId())) {
//                        productQuantity.setQuantity(productQuantity.getQuantity().add(inventoryTransferLog.getTransferAmount()));
//                        productRepository.update(token.getCompanyId(), product.getId(), product);
//                    }
//                }
//            }
//        }

        InventoryTransferHistory newRequest = new Kryo().copy(newTransferLog);
        newRequest.setTransferLogs(addedProductsList);
        processInventoryTransferForNewProducts(newRequest, newRequest);
    }

    /**
     * Update status of inventory transfer history
     *
     * @param historyId     : inventory transfer's history id
     * @param statusRequest : accept(true)/decline(false)
     */
    @Override
    public InventoryTransferHistory updateInventoryTransferStatus(String historyId, InventoryTransferStatusRequest statusRequest) {

        InventoryTransferHistory dbTransferHistory = transferHistoryRepository.get(token.getCompanyId(), historyId);
        if (dbTransferHistory == null) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, INVENTORY_TRANSFER_NOT_FOUND);
        }

        if (statusRequest.isStatus()) {
            dbTransferHistory.setCompleteTransfer(true);
            // check for cross shop
/*
            acceptTransferHistory(dbTransferHistory);
*/
        }

        if (dbTransferHistory.getStatus() != InventoryTransferHistory.TransferStatus.PENDING) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Inventory history can't be updated");
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());
        checkEmployeeAccessibility(dbTransferHistory, employeeMap);

        if (!statusRequest.isStatus()) {
            //If declined then update status
            dbTransferHistory.setStatus(InventoryTransferHistory.TransferStatus.DECLINED);
            dbTransferHistory.setDeclineByEmployeeId(token.getActiveTopUser().getUserId());
            dbTransferHistory.setDeclinedDate(DateTime.now().getMillis());
            dbTransferHistory.setProcessing(Boolean.TRUE);

//            List<ObjectId> productObjectIds = new ArrayList<>();
//            List<String> productIds = new ArrayList<>();
//            for (InventoryTransferLog log : dbTransferHistory.getTransferLogs()) {
//                productObjectIds.add(new ObjectId(log.getProductId()));
//                productIds.add(log.getProductId());
//            }
//
//            HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), dbTransferHistory.getFromShopId(), productObjectIds);


//            for (InventoryTransferLog inventoryTransferLog : dbTransferHistory.getTransferLogs()) {
//                inventoryTransferLog.setPrevTransferAmt(inventoryTransferLog.getTransferAmount());
//                if (BLStringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {
//                    ProductPrepackageQuantity fromPreQuantity = quantityRepository.getQuantity(token.getCompanyId(),
//                            dbTransferHistory.getFromShopId(),
//                            dbTransferHistory.getFromInventoryId(),
//                            inventoryTransferLog.getPrepackageItemId());
//                    if (fromPreQuantity == null) {
//                        throw new BlazeInvalidArgException("PrepackageProduct", "Prepackage Line Item does not exist");
//                    }
//                    int transferAmount = inventoryTransferLog.getTransferAmount().intValue();
//                    if (transferAmount > 0) {
//                        int fromQuantity = fromPreQuantity.getQuantity() + transferAmount;
//                        fromPreQuantity.setQuantity(fromQuantity);
//                        quantityRepository.update(token.getCompanyId(), fromPreQuantity.getId(), fromPreQuantity);
//                    }
//                } else {
//                    Product product = productHashMap.get(inventoryTransferLog.getProductId());
//                    ProductQuantity fromQuantity = null;
//                    for (ProductQuantity productQuantity : product.getQuantities()) {
//                        if (productQuantity.getInventoryId().equals(dbTransferHistory.getFromInventoryId())) {
//                            fromQuantity = productQuantity;
//                            break;
//                        }
//                    }
//
//                    if (fromQuantity != null) {
//                        if (dbTransferHistory.isTransferByBatch()) {
//                            batchQuantityService.addBatchQuantity(token.getCompanyId(), token.getShopId(), inventoryTransferLog.getProductId(), dbTransferHistory.getFromInventoryId(), inventoryTransferLog.getFromBatchId(), inventoryTransferLog.getTransferAmount());
//                        }
//                        BigDecimal transferAmount = inventoryTransferLog.getTransferAmount();
//                        if (transferAmount.doubleValue() > 0) {
//                            BigDecimal inventoryAmount = fromQuantity.getQuantity().add(transferAmount);
//                            fromQuantity.setQuantity(inventoryAmount);
//                            productRepository.update(token.getCompanyId(), product.getId(), product);
//                        }
//                    }
//                }
//            }

            this.createQueueTransactionJobForTransfer(dbTransferHistory);

        } else {
            String fromShopId = dbTransferHistory.getFromShopId();
            String toShopId = dbTransferHistory.getToShopId();
            dbTransferHistory.setAcceptByEmployeeId(token.getActiveTopUser().getUserId());
            dbTransferHistory.setAcceptedDate(DateTime.now().getMillis());
            dbTransferHistory.setProcessing(Boolean.TRUE);
            // Check if there are current pending queued items
            /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(), dbTransferHistory.getFromShopId(), QueuedTransaction.QueueStatus.Pending);
            if (count > 0) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, ACTIVE_TRANSACTION);
            }*/

            List<ObjectId> objectIds = new ArrayList<>();

            //Products with batch ids
            List<String> productsWithBatchIds = new ArrayList<>();
            //Products without batch ids (If product does not have batch id then use their latest batch id)
            List<String> productsWithoutBatchIds = new ArrayList<>();
            //From batch ids
            List<ObjectId> fromBatchIds = new ArrayList<>();
            for (InventoryTransferLog log : dbTransferHistory.getTransferLogs()) {
                log.setPrevTransferAmt(log.getTransferAmount());
                objectIds.add(new ObjectId(log.getProductId()));
                if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                    productsWithBatchIds.add(log.getProductId());
                    fromBatchIds.add(new ObjectId(log.getFromBatchId()));
                } else {
                    productsWithoutBatchIds.add(log.getProductId());
                }
            }

            //<Product is, from product batch>
            Map<String, ProductBatch> fromProductBatchMap = new HashMap<>();
            HashMap<String, ProductBatch> batchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), fromBatchIds);
            for (String batch : batchHashMap.keySet()) {
                ProductBatch productBatch = batchHashMap.get(batch);
                fromProductBatchMap.putIfAbsent(productBatch.getProductId(), productBatch);
            }

            //<Product is, latest product batch>
            Map<String, ProductBatch> latestProductBatchMap = productBatchRepository.getLatestBatchForProductList(token.getCompanyId(), token.getShopId(), productsWithoutBatchIds);

            //Create map by combining from batch map and latest product batch map
            Map<String, ProductBatch> productBatchMap = new HashMap<>();
            productBatchMap.putAll(fromProductBatchMap);
            productBatchMap.putAll(latestProductBatchMap);


            //Get products that needs to transfer
            Iterable<Product> dbProducts = productRepository.findItemsIn(token.getCompanyId(), fromShopId, objectIds);
            HashMap<String, Product> fromShopProductMap = new HashMap<>();
            for (Product p : dbProducts) {
                fromShopProductMap.put(p.getId(), p);
            }

            processInventoryTransfer(dbTransferHistory, fromShopId, toShopId, fromShopProductMap, productBatchMap, batchHashMap);
            //TODO : Should be closed from QueuedJob
//            dbTransferHistory.setStatus(InventoryTransferHistory.TransferStatus.ACCEPTED);
            this.createQueueTransactionJobForTransfer(dbTransferHistory);

        }

        return transferHistoryRepository.update(token.getCompanyId(), historyId, dbTransferHistory);
    }

    private void createQueueTransactionJobForTransfer(InventoryTransferHistory history) {

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.InventoryTransfer);
        queuedTransaction.setSellerId(history.getCreateByEmployeeId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(history.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());

    }

    private void processInventoryTransfer(InventoryTransferHistory dbTransferHistory, String fromShopId, String toShopId, HashMap<String, Product> fromShopProductMap, Map<String, ProductBatch> productBatchMap, Map<String, ProductBatch> fromProductBatchMap) {
        HashMap<String, Product> otherProductHashMap = new HashMap<>();
        final HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId());
        boolean isTransferToAnotherShop = false;

        Cart cart = new Cart();
        cart.prepare(token.getCompanyId());

        // Check if inventory is being transferred to another shop
        if (StringUtils.isNotBlank(toShopId)
                && !fromShopId.equalsIgnoreCase(toShopId)) {
            isTransferToAnotherShop = true;
            // Find products of shop in which inventory needs to be transfer
            Iterable<Product> otherShopProducts = productRepository.listByShop(token.getCompanyId(), toShopId);
            // Create product map with name_vendorId as key
            for (Product product : otherShopProducts) {
                if (!product.isDeleted()) {
                    product.setCategory(productCategoryHashMap.get(product.getCategoryId()));
                    String key = product.getCompanyLinkId();
                    otherProductHashMap.put(key, product);
                }
            }
        }

        List<Product> pendingProducts = new ArrayList<>();

        List<InventoryTransferRequest> transferRequests = new ArrayList<>();

        BulkInventoryTransferRequest bulkInventoryTransferRequest = new BulkInventoryTransferRequest();
        bulkInventoryTransferRequest.setFromShopId(fromShopId);
        bulkInventoryTransferRequest.setToShopId(toShopId);
        bulkInventoryTransferRequest.setTransfers(transferRequests);

        for (InventoryTransferLog log : dbTransferHistory.getTransferLogs()) {
            log.setPrevTransferAmt(log.getTransferAmount());
            InventoryTransferRequest transferRequest = new InventoryTransferRequest();

            transferRequest.setFromInventoryId(dbTransferHistory.getFromInventoryId());
            transferRequest.setToInventoryId(dbTransferHistory.getToInventoryId());
            transferRequest.setFromBatchId(log.getFromBatchId());
            transferRequest.setToBatchId(log.getToBatchId());
            transferRequest.setProductId(log.getProductId());
            transferRequest.setPrepackageItemId(log.getPrepackageItemId());
            transferRequest.setTransferAmount(log.getTransferAmount());

            transferRequests.add(transferRequest);

            Product dbProduct = fromShopProductMap.get(log.getProductId());
            if (dbProduct == null) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Product does not exist: " + log.getProductId());
            }

            // Check for same product
            Product otherProduct = null;
            if (isTransferToAnotherShop) {
                dbProduct.setCategory(productCategoryHashMap.get(dbProduct.getCategoryId()));
                String key = dbProduct.getCompanyLinkId();
                otherProduct = otherProductHashMap.get(key);
            }
//            transferInventory(dbTransferHistory, log, dbProduct, otherProduct, pendingProducts, productBatchMap, fromProductBatchMap);

            OrderItem order = new OrderItem();
            order.prepare(token.getCompanyId());
            order.setStatus(OrderItem.OrderItemStatus.Active);
            order.setCost(new BigDecimal(0));
            order.setQuantity(log.getTransferAmount());
            order.setFinalPrice(new BigDecimal(0));
            order.setProductId(dbProduct.getId());
            order.setPrepackageItemId(log.getPrepackageItemId());
            cart.getItems().add(order);
        }

        // Create Transaction Transfers
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), fromShopId, "TransactionPriority");
        String transNo = "" + sequence.getCount();
        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
        for (Product p : pendingProducts) {
            List<ProductPrepackageQuantity> productPrepackageQuantities = prepackageQuantityRepository.getQuantitiesForProductAsList(token.getCompanyId(), token.getShopId(), p.getId());
            productChangeLogs.add(productChangeLogRepository.createNewChangeLog(token.getActiveTopUser().getUserId(),
                    p, productPrepackageQuantities, String.format("Trans #%s: Transfer, tr:%s", transNo, dbTransferHistory.getTransferNo())));
            productRepository.update(token.getCompanyId(), p.getId(), p);
        }
        productChangeLogRepository.save(productChangeLogs);


        Transaction trans = new Transaction();
        trans.setActive(false);
        trans.setTransNo(transNo);
        trans.setMemberId(null);
        trans.setCompanyId(token.getCompanyId());
        trans.setShopId(fromShopId);
        trans.setQueueType(Transaction.QueueType.None);
        trans.setStatus(Transaction.TransactionStatus.Void);
        trans.setTerminalId(null);
        trans.setMemberId(null);
        trans.setCheckinTime(DateTime.now().getMillis());
        trans.setSellerId(token.getActiveTopUser().getUserId());
        trans.setCreatedById(token.getActiveTopUser().getUserId());
        trans.setPriority(sequence.getCount());
        //Add transfer history
        trans.setTransferRequest(bulkInventoryTransferRequest);
        trans.setProcessedTime(DateTime.now().getMillis());
        trans.setCompletedTime(DateTime.now().getMillis());
        trans.setStartTime(DateTime.now().getMillis());
        trans.setEndTime(DateTime.now().getMillis());

        // Set cart
        cart.setPaymentOption(Cart.PaymentOption.None);
        trans.setCart(cart);
        trans.setTransType(Transaction.TransactionType.Transfer);
        trans.setTransferShopId(toShopId);

        cart.setTotal(new BigDecimal(0));
        cart.setChangeDue(new BigDecimal(0));
        cart.setCashReceived(new BigDecimal(0));
        cart.setSubTotal(new BigDecimal(0));
        transactionRepository.save(trans);


        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(fromShopId,
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    private BigDecimal getProductBatchQuantity(Product otherProduct, ProductBatch productBatch) {

        MetrcVerifiedPackage metricsPackages = metrcAccountService.getMetrcVerifiedPackage(productBatch.getTrackPackageLabel());
        if (metricsPackages != null && metricsPackages.isRequired()) {
            productBatch.setTrackPackageLabel(productBatch.getTrackPackageLabel());
            productBatch.setTrackTraceSystem(productBatch.getTrackTraceSystem());
            if (metricsPackages.getId() > 0) {
                productBatch.setTrackPackageId(metricsPackages.getId());
            } else {
                productBatch.setTrackPackageId(null);
            }
            productBatch.setTrackTraceVerified(true);
            productBatch.setTrackWeight(metricsPackages.getBlazeMeasurement());



            BigDecimal verifiedQty = metrcAccountService.convertToBatchQuantity(metricsPackages,otherProduct.getCategory().getUnitType(),otherProduct);
            productBatch.setQuantity(verifiedQty);
        }
        return productBatch.getQuantity();

        /*
        if (otherProduct.getCategory().getUnitType() == ProductCategory.UnitType.units && metricsPackages.getBlazeMeasurement() != ProductWeightTolerance.WeightKey.UNIT) {
            // incoming is in grams or units
            if (otherProduct.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                // set it as normally
                return new BigDecimal(metricsPackages.getBlazeQuantity() * 2); // multiply by 2 as there are twice many in half grams
            } else if (otherProduct.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH) {
                ProductWeightTolerance eighthTolerance = weightToleranceRepository.getToleranceForWeight(token.getCompanyId(),
                        ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
                double eighthValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue.doubleValue();
                if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
                    eighthValue = eighthTolerance.getUnitValue().doubleValue();
                }
                // set it as normally
                return new BigDecimal(metricsPackages.getBlazeQuantity() / eighthValue); // divide grams by eighth to get eighth quantity
            } else if (otherProduct.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                ProductWeightTolerance quarterTolerance = weightToleranceRepository.getToleranceForWeight(token.getCompanyId(),
                        ProductWeightTolerance.WeightKey.QUARTER);
                double quarterValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue.doubleValue();
                if (quarterTolerance != null && quarterTolerance.getUnitValue().doubleValue() > 0) {
                    quarterValue = quarterTolerance.getUnitValue().doubleValue();
                }
                // set it as normally
                return new BigDecimal(metricsPackages.getBlazeQuantity() / quarterValue); // divide grams by quarter to get quarter quantity
            } else if (otherProduct.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && otherProduct.getCustomWeight() != null && otherProduct.getCustomWeight().doubleValue() > 0) {
                if (otherProduct.getCustomGramType() == Product.CustomGramType.GRAM) {
                    double quantityInGrams = metricsPackages.getBlazeQuantity() * otherProduct.getCustomWeight().doubleValue();

                    return new BigDecimal(quantityInGrams); // divide grams by quarter to get quarter quantity
                } else if (Product.CustomGramType.MILLIGRAM == otherProduct.getCustomGramType()) {
                    double quantityInGrams = metricsPackages.getBlazeQuantity() * (otherProduct.getCustomWeight().doubleValue() / 1000);

                    return new BigDecimal(quantityInGrams); // divide grams by quarter to get quarter quantity
                }

                return new BigDecimal(metricsPackages.getBlazeQuantity());
            } else {
                // set it as normally
                return new BigDecimal(metricsPackages.getBlazeQuantity());
            }
        } else {
            // this is grams
            return new BigDecimal(metricsPackages.getBlazeQuantity());
        }
        */
    }


    private void checkEmployeeAccessibility(InventoryTransferHistory dbInventoryTransferHistory, HashMap<String, Employee> employeeMap) {
        Employee employee = employeeMap.get(token.getActiveTopUser().getUserId());
        Role role = roleRepository.get(token.getCompanyId(), employee.getRoleId());

        //If employee has web inventory transfer permission and inventory transfer is created by employee then give permission to transfer inventory
        if (role != null && (role.getPermissions().contains(Role.Permission.WebInventoryTransfer)
                && dbInventoryTransferHistory.getCreateByEmployeeId().equals(employee.getId()))) {
            return;
        }

        //Firstly check if employee is manager or admin
        if (role != null
                && !role.getName().equalsIgnoreCase("Manager")
                && !role.getName().equalsIgnoreCase("Admin")) {
            //If not then check that employee's is assigned to that terminal or not
            if (!role.getPermissions().contains(Role.Permission.WebInventoryTransfer) && !role.getPermissions().contains(Role.Permission.WebInventoryManage)) {
                throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Sorry! You are not authorized for inventory transfer");
            }
        }
    }

    @Override
    public DateSearchResult<InventoryTransferHistoryResult> getInventoryTransferByDate(long afterDate, long beforeDate) {
        DateSearchResult<InventoryTransferHistoryResult> searchResult = transferHistoryRepository.findItemByModifiedDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);

        HashMap<String, Shop> shopMap = shopRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());

        if (searchResult != null && searchResult.getValues() != null) {
            HashSet<ObjectId> batchIds = new HashSet<>();
            for (InventoryTransferHistoryResult result : searchResult.getValues()) {
                for (InventoryTransferLog log : result.getTransferLogs()) {
                    if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                        batchIds.add(new ObjectId(log.getFromBatchId()));
                    }
                }
            }

            HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));

            for (InventoryTransferHistoryResult historyResult : searchResult.getValues()) {
                prepareInventoryTransferResult(historyResult, shopMap, inventoryMap, employeeMap, productBatchHashMap);
            }
        }
        return searchResult;
    }

    @Override
    public List<BulkPostResponse> compositeAddInventoryTransfer(List<InventoryTransferHistory> inventoryTransferHistoryList) {
        List<BulkPostResponse> errorInventoryTransfer = new ArrayList<>();
        HashMap<String, Product> productHashMap = productRepository.listAsMap();
        for (InventoryTransferHistory transferRequest : inventoryTransferHistoryList) {
            String fromShopId = StringUtils.isNotBlank(transferRequest.getFromShopId()) ? transferRequest.getFromShopId() : token.getShopId();
            try {
                for (InventoryTransferLog transferLog : transferRequest.getTransferLogs()) {
                    if (StringUtils.isBlank(transferLog.getProductId())) {
                        throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Product id cannot  be blank");
                    }
                    if (!productHashMap.containsKey(transferLog.getProductId())) {
                        throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Product does not found");
                    }
                    Product product = productHashMap.get(transferLog.getProductId());
                    if (!product.getShopId().equalsIgnoreCase(fromShopId)) {
                        throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Product does not found");
                    }
                }
                bulkTransferInventory(transferRequest);
            } catch (Exception e) {
                //Return all inventory transfer if exception is thrown.
                BulkPostResponse response = new BulkPostResponse(transferRequest, e.getMessage());
                errorInventoryTransfer.add(response);
            }
        }
        return errorInventoryTransfer;
    }

    /**
     * Bulk inventory transfer only for batches
     *
     * @param request
     * @return inventoryTransferHistory
     */
    @Override
    public InventoryTransferHistory bulkTransferInventoryByBatches(InventoryTransferHistory request) {
        if (request.getTransferLogs().size() == 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, TRANSFER_REQUEST_NOT_FOUND);
        }

        String fromShopId = null;
        if (StringUtils.isNotBlank(request.getFromShopId())) {
            fromShopId = request.getFromShopId();
        } else {
            fromShopId = token.getShopId();
        }

        String toShopId = null;
        if (StringUtils.isNotBlank(request.getToShopId())) {
            toShopId = request.getToShopId();
        } else {
            toShopId = token.getShopId();
        }

        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(), fromShopId, QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, ACTIVE_TRANSACTION);
        }*/

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId());

        if (!inventoryHashMap.containsKey(request.getFromInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'from' inventory.");
        }
        if (!inventoryHashMap.containsKey(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Invalid 'to' inventory.");
        }
        if (request.getFromInventoryId().equalsIgnoreCase(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Cannot transfer within the same inventory.");
        }
        if (!toShopId.equals(fromShopId) && token.getAppTarget() != CompanyFeatures.AppTarget.Retail) {
            Shop toShop = shopRepository.getById(toShopId);
            if (!toShop.getAppTarget().equals(token.getAppTarget()) && StringUtils.isBlank(request.getDriverId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Driver cannot be empty.");
            }
        }
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), fromShopId, "InventoryTransfer");
        String transferNo = "" + sequence.getCount();

        InventoryTransferHistory inventoryTransferHistory = new InventoryTransferHistory();
        inventoryTransferHistory.prepare(token.getCompanyId());
        inventoryTransferHistory.setShopId(token.getShopId());
        inventoryTransferHistory.setCreateByEmployeeId(token.getActiveTopUser().getUserId());
        inventoryTransferHistory.setStatus(InventoryTransferHistory.TransferStatus.PENDING);
        inventoryTransferHistory.setFromShopId(fromShopId);
        inventoryTransferHistory.setToShopId(toShopId);
        inventoryTransferHistory.setFromInventoryId(request.getFromInventoryId());
        inventoryTransferHistory.setToInventoryId(request.getToInventoryId());
        inventoryTransferHistory.setTransferNo(transferNo);
        inventoryTransferHistory.setTransferByBatch(request.isTransferByBatch());
        inventoryTransferHistory.setCompleteTransfer(request.isCompleteTransfer());
        if (request.isCompleteTransfer()) {
            inventoryTransferHistory.setProcessing(Boolean.TRUE);
        }

        if (StringUtils.isBlank(request.getDriverId())) {
            inventoryTransferHistory.setDriverId(token.getActiveTopUser().getUserId());
        } else {
            inventoryTransferHistory.setDriverId(request.getDriverId());
        }
        // check for cross shop when we are create and accept once
        /*if (request.isCompleteTransfer()) {
            acceptTransferHistory(inventoryTransferHistory);
        }*/

        LinkedHashSet<InventoryTransferLog> logLists = new LinkedHashSet<>();
        List<ObjectId> prepackageItemIds = new ArrayList<>();
        for (InventoryTransferLog inventoryTransferLog : request.getTransferLogs()) {
            if (!ObjectId.isValid(inventoryTransferLog.getProductId())) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, inventoryTransferLog.getProductId() + " is an invalid id.");
            } else if (inventoryTransferLog.getTransferAmount() == null ||
                    inventoryTransferLog.getTransferAmount().doubleValue() <= 0) {
                throw new BlazeInvalidArgException(TRANSFER_REQUEST, "Transfer amount must be greater than 0.");
            }

            if (StringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {
                prepackageItemIds.add(new ObjectId(inventoryTransferLog.getPrepackageItemId()));
            }

            logLists.add(inventoryTransferLog);
        }


        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = prepackageProductItemRepository.listAsMap(token.getCompanyId(), prepackageItemIds);

        for (InventoryTransferLog logList : logLists) {
            if (StringUtils.isNotBlank(logList.getPrepackageItemId()) && ObjectId.isValid(logList.getPrepackageItemId())) {
                PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(logList.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    logList.setFromBatchId(prepackageProductItem.getBatchId());
                }

                Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                if (prepackage != null) {
                    logList.setPrepackageName(prepackage.getName());
                }
            }
        }

        inventoryTransferHistory.setTransferLogs(logLists);

//        List<ObjectId> productObjectIds = new ArrayList<>();
//        Set<String> productIds = new HashSet<>();
//        for (InventoryTransferLog log : request.getTransferLogs()) {
//            productObjectIds.add(new ObjectId(log.getProductId()));
//            productIds.add(log.getProductId());
//        }
//        HashMap<String, Product> productMap = productRepository.findItemsInAsMap(token.getCompanyId(), inventoryTransferHistory.getFromShopId(), productObjectIds);

//        checkAndDeductInitial(request,inventoryTransferHistory,productMap,false, productIds);
//        checkAndDeductInitial(request,inventoryTransferHistory,productMap,true, productIds);

        InventoryTransferHistory updatedTransferHistory = transferHistoryRepository.save(inventoryTransferHistory);
        if (request.isCompleteTransfer()) {
            InventoryTransferStatusRequest statusRequest = new InventoryTransferStatusRequest();
            statusRequest.setStatus(Boolean.TRUE);

            this.updateInventoryTransferStatus(updatedTransferHistory.getId(), statusRequest);
        } else {
            this.createQueueTransactionJobForTransfer(inventoryTransferHistory);
        }

        return updatedTransferHistory;
    }


    private void acceptTransferHistory(InventoryTransferHistory history) {
        if (!history.getToShopId().equalsIgnoreCase(token.getShopId())) {
            throw new BlazeInvalidArgException(INVENTORY_TRANSFER_HISTORY, "Transfer request is only accepted by received shop");
        }
    }

    @Override
    public InventoryTransferHistoryResult getInventoryTransferHistoryByNo(String transferNo) {
        if (StringUtils.isBlank(transferNo)) {
            throw new BlazeInvalidArgException("Inventory History", "Inventory transfer number cannot be blank");
        }
        InventoryTransferHistoryResult transferHistory = transferHistoryRepository.getTransferHistoryByNo(token.getCompanyId(), token.getShopId(), transferNo, InventoryTransferHistoryResult.class);
        if (transferHistory == null) {
            throw new BlazeInvalidArgException("Inventory History", "Inventory transfer history does not found");
        }

        HashMap<String, Shop> shopMap = shopRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());

        HashSet<ObjectId> batchIds = new HashSet<>();

        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                batchIds.add(new ObjectId(log.getFromBatchId()));
            }
            if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                batchIds.add(new ObjectId(log.getToBatchId()));
            }

        }

        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
        prepareInventoryTransferResult(transferHistory, shopMap, inventoryMap, employeeMap, productBatchHashMap);

        return transferHistory;
    }


    private void prepareInventoryTransferProductResult(InventoryTransferHistoryResult transferHistory,
                                                       HashMap<String, Product> productHashMap) {

        Set<ObjectId> brandIds = new HashSet<>();
        for (Product product : productHashMap.values()) {
            if (product != null && StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            if (StringUtils.isNotBlank(log.getProductId()) && ObjectId.isValid(log.getProductId())) {
                Product productdetails = productHashMap.get(log.getProductId());

                if (productdetails != null && StringUtils.isNotBlank(productdetails.getBrandId())) {
                    Brand brand = brandHashMap.get(productdetails.getBrandId());
                    productdetails.setBrand(brand);
                }

                if (productdetails != null) {
                   log.setProduct(productdetails);
                }
            }
        }
    }

    /**
     * This method generate pdf of inventory transfer logs according to product, productBatch
     * @param transferId
     * @return
     */

    @Override
    public InputStream createPdfForShippingManifest(String transferId) {

        if (StringUtils.isBlank(transferId)) {
            throw new BlazeInvalidArgException("Inventory History", "Inventory transfer number cannot be blank");
        }

        InventoryTransferHistoryResult transferHistory = transferHistoryRepository.get(token.getCompanyId(), transferId, InventoryTransferHistoryResult.class);

        Company company = companyRepository.getById(token.getCompanyId());

        if (transferHistory == null) {
            throw new BlazeInvalidArgException("Inventory History", "Inventory transfer  does not found");
        }

        InputStream inputStream = InventoryTransferHistoryServiceImpl.class.getResourceAsStream("/transfer_shipping_manifest_form.html");

        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in shipping manifest form", ex);
        }


        String body = writer.toString();
        Shop shipperShop = shopRepository.getById(transferHistory.getFromShopId());
        Shop receiverShop = shopRepository.getById(transferHistory.getToShopId());

        Set<ObjectId> employeeIds = new HashSet<>();
        if (StringUtils.isNotBlank(transferHistory.getCreateByEmployeeId()) && ObjectId.isValid(transferHistory.getCreateByEmployeeId())) {
            employeeIds.add(new ObjectId(transferHistory.getCreateByEmployeeId()));
        }
        if (StringUtils.isNotBlank(transferHistory.getAcceptByEmployeeId()) && ObjectId.isValid(transferHistory.getAcceptByEmployeeId())) {
            employeeIds.add(new ObjectId(transferHistory.getAcceptByEmployeeId()));
        }
        if (StringUtils.isNotBlank(transferHistory.getDeclineByEmployeeId()) && ObjectId.isValid(transferHistory.getDeclineByEmployeeId())) {
            employeeIds.add(new ObjectId(transferHistory.getDeclineByEmployeeId()));
        }
        if (StringUtils.isNotBlank(transferHistory.getDriverId()) && ObjectId.isValid(transferHistory.getDriverId())) {
            employeeIds.add(new ObjectId(transferHistory.getDriverId()));
        }
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));

        HashSet<ObjectId> batchIds = new HashSet<>();
        HashSet<ObjectId> productIds = new HashSet<>();
        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            if (log.getBatchQuantityMap() != null) {
                log.getBatchQuantityMap().keySet().forEach(id -> batchIds.add(new ObjectId(id)));
            }
            if (StringUtils.isNotBlank(log.getProductId()) && ObjectId.isValid(log.getProductId())) {
                productIds.add(new ObjectId(log.getProductId()));
            }
            if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                batchIds.add(new ObjectId(log.getFromBatchId()));
            }
            if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                batchIds.add(new ObjectId(log.getToBatchId()));
            }

        }

        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> categoryIds = new HashSet<>();

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }

            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(categoryIds));

        body = prepareManifestPdf(body, company, transferHistory, shipperShop, receiverShop, employeeHashMap, productHashMap, categoryHashMap, productBatchHashMap, brandHashMap);
        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, "/transfer_shipping_manifest_form.html"));
    }

    private void addEmptyProductRaw(StringBuilder productInfo) {
        productInfo.append("<tr style=\"height: 30px;\"><td class=\"content text-center\" style=\"width: 25%\">  </td>")
                .append("<td class=\"content text-center\" style=\"width: 25%; word-wrap: break-word;\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 8%\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 6%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 7%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 7%\"> </td>")
                .append("<td style=\"width: 11%\" class=\"bg-dark-grey content text-center\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\"> </td>")
                .append("</tr>");
    }

    /**
     * Override method to create zip for manifest pdf
     *
     * @param transferIds : comma seperated list of transferIds
     * @return
     */
    @Override
    public InputStream createManifestForBulk(String transferIds) {
        if (StringUtils.isBlank(transferIds)) {
            throw new BlazeInvalidArgException("Manifest", "Transfer Ids cannot be blank.");
        }

        List<String> transferIdList = (Arrays.asList(StringUtils.split(transferIds, "\\s*,\\s*")));
        Set<ObjectId> transferObjId = new HashSet<>();
        for (String id : transferIdList) {
            if (StringUtils.isNotBlank(id) && ObjectId.isValid(id.trim())) {
                transferObjId.add(new ObjectId(id.trim()));
            }
        }

        HashMap<String, InventoryTransferHistory> transferMap = transferHistoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(transferObjId));

        InputStream inputStream = InventoryTransferHistoryServiceImpl.class.getResourceAsStream("/transfer_shipping_manifest_form.html");

        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in shipping manifest form", ex);
        }

        Set<ObjectId> shopIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();
        HashSet<ObjectId> batchIds = new HashSet<>();
        HashSet<ObjectId> productIds = new HashSet<>();
        HashSet<ObjectId> brandIds = new HashSet<>();
        HashSet<ObjectId> categoryIds = new HashSet<>();
        for (InventoryTransferHistory history: transferMap.values()) {
            if (StringUtils.isNotBlank(history.getFromShopId()) && ObjectId.isValid(history.getFromShopId())) {
                shopIds.add(new ObjectId(history.getFromShopId()));
            }
            if (StringUtils.isNotBlank(history.getToShopId()) && ObjectId.isValid(history.getToShopId())) {
                shopIds.add(new ObjectId(history.getToShopId()));
            }
            if (StringUtils.isNotBlank(history.getCreateByEmployeeId()) && ObjectId.isValid(history.getCreateByEmployeeId())) {
                employeeIds.add(new ObjectId(history.getCreateByEmployeeId()));
            }
            if (StringUtils.isNotBlank(history.getAcceptByEmployeeId()) && ObjectId.isValid(history.getAcceptByEmployeeId())) {
                employeeIds.add(new ObjectId(history.getAcceptByEmployeeId()));
            }
            if (StringUtils.isNotBlank(history.getDeclineByEmployeeId()) && ObjectId.isValid(history.getDeclineByEmployeeId())) {
                employeeIds.add(new ObjectId(history.getDeclineByEmployeeId()));
            }
            if (CollectionUtils.isNotEmpty(history.getTransferLogs())) {
                for (InventoryTransferLog log : history.getTransferLogs()) {
                    if (StringUtils.isNotBlank(log.getProductId()) && ObjectId.isValid(log.getProductId())) {
                        productIds.add(new ObjectId(log.getProductId()));
                    }
                    if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                        batchIds.add(new ObjectId(log.getFromBatchId()));
                    }
                    if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                        batchIds.add(new ObjectId(log.getToBatchId()));
                    }
                    if (log.getBatchQuantityMap() != null) {
                        log.getBatchQuantityMap().keySet().forEach(id -> batchIds.add(new ObjectId(id)));
                    }
                }
            }
        }
        HashMap<String ,Employee> employeeHashMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Shop> shopsMap = shopRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(shopIds));
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }

            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(categoryIds));

        Company company = companyRepository.getById(token.getCompanyId());

        HashMap<String, InputStream> streamList = new HashMap<>();
        for (InventoryTransferHistory history : transferMap.values()) {
            String body = writer.toString();
            Shop shipperShop = shopsMap.get(history.getFromShopId());
            Shop receiverShop = shopsMap.get(history.getToShopId());

            if (shipperShop == null || receiverShop == null) {
                body = "Error while generating pdf.";
            } else {
                body = prepareManifestPdf(body, company, history, shipperShop, receiverShop, employeeHashMap, productHashMap, categoryHashMap, productBatchHashMap, brandHashMap);
            }
            streamList.put(history.getTransferNo(), new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, "/transfer_shipping_manifest_form.html")));
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ZipOutputStream zos = new ZipOutputStream(out);
            for (Map.Entry<String, InputStream> entrySet : streamList.entrySet()) {
                try {
                    InputStream stream = entrySet.getValue();
                    zos.putNextEntry(new ZipEntry("TransNo_" + entrySet.getKey() + ".pdf"));
                    ByteStreams.copy(stream, zos);
                    zos.closeEntry();

                } catch (Exception e) {
                    LOGGER.info("Error in adding zip entry for transfer:" + entrySet.getKey());
                }
            }
            zos.close();
        } catch (Exception e) {
            LOGGER.info("Error in zip creation.");
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    /**
     * Privste method to prepare manifest body
     *
     * @param body            : html
     * @param company         : company
     * @param transferHistory : history
     * @param shipperShop     : shippershop
     * @param receiverShop    : receiver shop
     * @param employeeHashMap : employee map
     * @param productHashMap  : product map
     * @param categoryHashMap : category map
     * @param batchHashMap    : batch map
     * @param brandHashMap    : brand map
     * @return : html
     */
    private String prepareManifestPdf(String body, Company company, InventoryTransferHistory transferHistory, Shop shipperShop, Shop receiverShop, HashMap<String, Employee> employeeHashMap, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> categoryHashMap, HashMap<String, ProductBatch> batchHashMap, HashMap<String, Brand> brandHashMap) {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MM/dd/yy");
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("hh:mm");
        DateTimeFormatter timeSuffixFormatter = DateTimeFormat.forPattern("a");

        String companyPremable = (company != null && (company.getOnPremCompanyConfig() != null && company.getOnPremCompanyConfig().isOnPremEnable())) ? " - Onsite transfer" : " ";

        StringBuilder shopAddress = null;
        if (shipperShop.getAddress() != null) {
            shopAddress = new StringBuilder(shipperShop.getAddress().getAddressString());
        }
        StringBuilder receiverAddress = null;
        if (receiverShop.getAddress() != null) {
            receiverAddress = new StringBuilder(receiverShop.getAddress().getAddressString());
        }
        String emptyString = "-";
        StringBuilder contactNo = new StringBuilder();

        body = body.replaceAll("==shippingManifestNo==", StringUtils.isNotBlank(transferHistory.getTransferNo()) ? "Trans # " + transferHistory.getTransferNo() : emptyString);
        body = body.replaceAll("==totalPageNo==", "2");
        body = body.replaceAll("==pageNo==", "2");
        body = body.replaceAll("==transNo==", StringUtils.isNotBlank(transferHistory.getTransferNo()) ? transferHistory.getTransferNo() : emptyString);

        String timeZone = token.getRequestTimeZone();
        Employee employee = null;
        if (StringUtils.isNotBlank(transferHistory.getDriverId())) {
            employee = employeeHashMap.get(transferHistory.getDriverId());
        } else {
            employee = employeeHashMap.get(transferHistory.getCreateByEmployeeId());
        }

        Employee acceptedEmployee = null;
        if (transferHistory.getStatus() == InventoryTransferHistory.TransferStatus.ACCEPTED) {
            acceptedEmployee = employeeHashMap.get(transferHistory.getAcceptByEmployeeId());
        }
        if (transferHistory.getStatus() == InventoryTransferHistory.TransferStatus.DECLINED) {
            acceptedEmployee = employeeHashMap.get(transferHistory.getDeclineByEmployeeId());
        }

        String empoyeeName = (employee != null) ? employee.getFirstName() + " " + employee.getLastName() : emptyString;
        if (StringUtils.isBlank(timeZone)) {
            timeZone = shipperShop.getTimeZone();
        }
        DateTime shippingDate = new DateTime();
        shippingDate = DateUtil.toDateTime(shippingDate.getMillis(), timeZone);
        String shippingSuffix = timeSuffixFormatter.print(shippingDate);
        DateTime estimatedDate = new DateTime();
        String estimatedSuffix = timeSuffixFormatter.print(estimatedDate);

        String acceptedEmployeeName = (acceptedEmployee != null) ? acceptedEmployee.getFirstName() + " " + acceptedEmployee.getLastName() : emptyString;
        if (transferHistory.getCreated() > 0) {
            estimatedDate = DateUtil.toDateTime(transferHistory.getCreated(), timeZone);
            estimatedSuffix = timeSuffixFormatter.print(estimatedDate);
        }

        if (transferHistory.getAcceptedDate() != null && transferHistory.getAcceptedDate() > 0) {
            shippingDate = DateUtil.toDateTime(transferHistory.getAcceptedDate(), timeZone);
            shippingSuffix = timeSuffixFormatter.print(shippingDate);
        }

        if (transferHistory.getAcceptedDate() == null && transferHistory.getDeclinedDate() != null && transferHistory.getDeclinedDate() > 0) {
            shippingDate = DateUtil.toDateTime(transferHistory.getDeclinedDate(), timeZone);
            shippingSuffix = timeSuffixFormatter.print(shippingDate);
        }

        body = body.replaceAll("==shippingDate==", dateFormatter.print(shippingDate) + " " + timeFormatter.print(shippingDate));
        body = body.replaceAll("==estimatedDate==", dateFormatter.print(estimatedDate) + " " + timeFormatter.print(estimatedDate));

        if (shippingSuffix.equals("AM")) {
            body = body.replaceAll("==shippingAM==", "");
            body = body.replaceAll("==shippingPM==", "display:none;");
        } else {
            body = body.replaceAll("==shippingAM==", "display:none;");
            body = body.replaceAll("==shippingPM==", "");
        }

        StringBuilder productInfo = new StringBuilder();

        if (estimatedSuffix.equals("AM")) {
            body = body.replaceAll("==shippingestimateAM==", "");
            body = body.replaceAll("==shippingestimatePM==", "display:none;");
        } else {
            body = body.replaceAll("==shippingestimateAM==", "display:none;");
            body = body.replaceAll("==shippingestimatePM==", "");
        }

        String shipperLicenseType = "";
        if (StringUtils.isNotBlank(shipperShop.getAppTarget().name())) {
            shipperLicenseType = shipperShop.getAppTarget().name().equals(CompanyFeatures.AppTarget.Distribution.toString()) ? "Distro" : shipperShop.getAppTarget().name();
        }

        contactNo.append(StringUtils.isNotBlank(shipperShop.getPhoneNumber()) ? shipperShop.getPhoneNumber() : "");
        body = body.replaceAll("==shipperLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperShop.getLicense()) ? shipperShop.getLicense() : emptyString));
        body = body.replaceAll("==shipperLicenseType==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperLicenseType) ? shipperLicenseType : emptyString));
        body = body.replaceAll("==shipperBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperShop.getName()) ? shipperShop.getName() : emptyString));
        body = body.replaceAll("==shipperAddress1==", TextUtil.textOrEmpty((shipperShop.getAddress() != null && shipperShop.getAddress().getAddress() != null) ? shipperShop.getAddress().getAddress() : emptyString));
        body = body.replaceAll("==shipperAddress2==", TextUtil.textOrEmpty(shopAddress != null ? shopAddress.toString() : emptyString));
        body = body.replaceAll("==shipperPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
        body = body.replaceAll("==shipperPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(contactNo) ? contactNo.toString() : emptyString));
        body = body.replaceAll("==shipperContactName==", empoyeeName);

        String receiverLicenseType = "";
        if (StringUtils.isNotBlank(receiverShop.getAppTarget().name())) {
            receiverLicenseType = receiverShop.getAppTarget().name().equals(CompanyFeatures.AppTarget.Distribution.toString()) ? "Distro" : receiverShop.getAppTarget().name();
        }

        body = body.replaceAll("==receiverLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(receiverShop.getLicense()) ? receiverShop.getLicense() : emptyString));
        body = body.replaceAll("==receiverLicenseType==", TextUtil.textOrEmpty(StringUtils.isNotBlank(receiverLicenseType) ? receiverLicenseType : emptyString));
        body = body.replaceAll("==receiverBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(receiverShop.getName()) ? receiverShop.getName() : receiverShop.getName()));
        body = body.replaceAll("==receiverAddress1==", TextUtil.textOrEmpty(receiverShop.getAddress() != null ? receiverShop.getAddress().getAddress() : emptyString));
        body = body.replaceAll("==receiverAddress2==", TextUtil.textOrEmpty(receiverAddress != null ? receiverAddress.toString() : emptyString));
        body = body.replaceAll("==receiverPhone==", TextUtil.textOrEmpty(receiverShop.getPhoneNumber() != null ? receiverShop.getPhoneNumber() : TextUtil.textOrEmpty("")));
        body = body.replaceAll("==receiverContactName==", TextUtil.textOrEmpty(acceptedEmployeeName));


        body = body.replaceAll("==distributorLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperShop.getLicense()) ? shipperShop.getLicense() : emptyString));
        body = body.replaceAll("==distributorLicenseType==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperShop.getShopType().name()) ? shipperShop.getShopType().name() : emptyString));
        body = body.replaceAll("==distributorBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperShop.getName()) ? shipperShop.getName() : emptyString));
        body = body.replaceAll("==distributorAddress1==", TextUtil.textOrEmpty((shipperShop.getAddress() != null && shipperShop.getAddress().getAddress() != null) ? shipperShop.getAddress().getAddress() : emptyString));
        body = body.replaceAll("==distributorAddress2==", TextUtil.textOrEmpty(shopAddress != null ? shopAddress.toString() : emptyString));
        body = body.replaceAll("==distributorPhone==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperShop.getPhoneNumber()) ? shipperShop.getPhoneNumber() : emptyString));
        body = body.replaceAll("==distributorContactName==", TextUtil.textOrEmpty(empoyeeName));


        body = body.replaceAll("==driverName==", TextUtil.textOrEmpty(empoyeeName));

        body = body.replaceAll("==driverLicenseNo==", TextUtil.textOrEmpty(String.format("%s %s", (employee != null && StringUtils.isNotBlank(employee.getDriversLicense()) ? employee.getDriversLicense() : "N/A"), companyPremable)));
        body = body.replaceAll("==driverVehicleMake==", TextUtil.textOrEmpty(String.format("%s %s", (employee != null && StringUtils.isNotBlank(employee.getVehicleMake()) ? employee.getVehicleMake() : "N/A"), companyPremable)));
        body = body.replaceAll("==driverVehicleModel==", TextUtil.textOrEmpty(String.format("%s %s", (employee != null && StringUtils.isNotBlank(employee.getVehicleModel()) ? employee.getVehicleModel() : "N/A"), companyPremable)));
        body = body.replaceAll("==driverVehiclePlate==", TextUtil.textOrEmpty(String.format("%s %s", (employee != null && StringUtils.isNotBlank(employee.getVehicleLicensePlate()) ? employee.getVehicleLicensePlate() : "N/A"), companyPremable)));
        body = body.replaceAll("==arrivalTime==", dateFormatter.print(shippingDate) + " " + timeFormatter.print(shippingDate));

        int productCount = 0;
        for (InventoryTransferLog log : transferHistory.getTransferLogs()) {
            Product product = productHashMap.get(log.getProductId());
            if (product == null) {
                continue;
            }
            ProductCategory category = categoryHashMap.get(product.getCategoryId());
            Brand brand = brandHashMap.get(product.getBrandId());

            StringBuilder productName = new StringBuilder(product.getName());
            productName = (brand != null)  ? productName.append(" ( " + brand.getName() + " )") : productName;

            String uidTag = "-";

            BigDecimal productUnitPrice = product.getUnitPrice();

            String  categoryStr = (category != null) ? category.getUnitType().name() : "units";

            if (productUnitPrice.compareTo(BigDecimal.ZERO) == 0) {
                if (category != null && category.getUnitType() == ProductCategory.UnitType.grams && CollectionUtils.isNotEmpty(product.getPriceRanges())) {
                    product.getPriceRanges().size();
                    productUnitPrice = product.getPriceRanges().get(0).getPrice();
                }
                if (category != null && category.getUnitType() == ProductCategory.UnitType.units && CollectionUtils.isNotEmpty(product.getPriceBreaks())) {
                    productUnitPrice = product.getPriceBreaks().get(0).getPrice();
                }
            }
            if (productUnitPrice == null) {
                productUnitPrice = new BigDecimal(0);
            }

            if (log.getBatchQuantityMap() != null && !log.getBatchQuantityMap().isEmpty()) {
                String finalTimeZone = timeZone;
                BigDecimal finalProductUnitPrice = productUnitPrice;
                StringBuilder finalProductName = productName;
                log.getBatchQuantityMap().forEach((key, value) -> {
                    ProductBatch batch = batchHashMap.get(key);
                    String uid = "-";
                    String  batchLink = "";
                    BigDecimal unitPrice = (batch != null) ? batch.getFinalUnitCost() : finalProductUnitPrice;

                    if (batch != null) {
                        uid = StringUtils.isNotBlank(batch.getTrackHarvestBatch()) ? batch.getTrackHarvestBatch() : StringUtils.isNotBlank(batch.getSku()) ? batch.getSku() : "-";
                        batchLink = getBatchAttachmentLink(batch);
                        if (batch.getPurchasedDate() != 0) {
                            uid += " (" + TextUtil.toDate(DateUtil.toDateTime(batch.getPurchasedDate(), finalTimeZone).getMillis()) + ")";
                        }
                        uid += StringUtils.isNotBlank(batch.getTrackPackageLabel()) ? " - " + batch.getTrackPackageLabel() : "";
                    }

                    prepareProductInfo(productInfo, finalProductName, value, unitPrice, uid, categoryStr, shipperShop.getDefaultCountry(), batchLink);

                });

            } else {
                prepareProductInfo(productInfo, productName, log.getTransferAmount(), productUnitPrice, "-", categoryStr, shipperShop.getDefaultCountry(), "");
            }
        }
        for (int i = 0; i < 3; i++) {
            this.addEmptyProductRaw(productInfo);
            productCount++;
        }

        body = body.replaceAll("==productInformation==", productInfo.toString());
        body = body.replaceAll("&", "&amp;");
        body = body.replace("==showDetails==", "display:none");

        return body;
    }

    private void prepareProductInfo(StringBuilder productInfo, StringBuilder finalProductName, BigDecimal quantity, BigDecimal unitPrice, String uidTag, String categoryStr, String defaultCountry, String batchLink) {

        productInfo.append("<tr><td class=\"content text-center\" style='width:25%;'>" + (StringUtils.isNotBlank(batchLink) ? "<a href='" + batchLink + "'>" : "")).append(uidTag).append((StringUtils.isNotBlank(batchLink) ? "</a>" : "") + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 25%; word-wrap: break-word;\">" + finalProductName + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 8%\">" + TextUtil.formatToTwoDecimalPoints(quantity) + " " + categoryStr + "</td>")
                .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 6%\">" + TextUtil.formatToTwoDecimalPoints(quantity) + "" + categoryStr + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 7%\">" + TextUtil.toEscapeCurrency(unitPrice.doubleValue(), defaultCountry) + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 7%\">" + TextUtil.toEscapeCurrency(unitPrice.multiply(quantity).doubleValue(), defaultCountry) + "</td>")
                .append("<td style=\"width: 11%\" class=\"bg-dark-grey content text-center\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\"> </td>")
                .append("</tr>");
    }


    @Override
    public SearchResult<InventoryTransferHistoryResult> getAllInventoryHistories(int start, int limit, String status, String startDate, String endDate, String searchTerm) {

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        start = (start < 0) ? 0 : start;
        if (limit <= 0) {
            limit = 200;
        }
        long totalItem;

        if (StringUtils.isBlank(status)) {
            status = "PENDING";
        }
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee not found");
        }
        Terminal terminal = new Terminal();
        if (StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.getTerminalById(token.getCompanyId(), employee.getAssignedTerminalId());
        }

        String inventoryId = "";
        if (terminal != null && StringUtils.isNotBlank(terminal.getAssignedInventoryId())) {
            inventoryId = terminal.getAssignedInventoryId();
        }


        List<String> statusList = Arrays.asList(status.split("\\s*,\\s*"));
        if (statusList.isEmpty()) {
            statusList.add(InventoryTransferHistory.TransferStatus.PENDING.toString());
        }

        List<InventoryTransferHistory.TransferStatus> transferStatusList = new ArrayList<>();
        for (String s : statusList) {
            transferStatusList.add(InventoryTransferHistory.TransferStatus.valueOf(s));
        }


        SearchResult<InventoryTransferHistoryResult> searchResult = new SearchResult<>();
        List<InventoryTransferHistoryResult> list = new ArrayList<>();

        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SearchResult<InventoryTransferHistoryResult> itemByStatus = transferHistoryRepository.findItemByInventoryIdandStatus(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit, transferStatusList, inventoryId, searchTerm, InventoryTransferHistoryResult.class);

            if (itemByStatus != null && itemByStatus.getValues().size() > 0) {
                for (InventoryTransferHistoryResult inventoryTransferHistoryResult : itemByStatus.getValues()) {
                    list.add(inventoryTransferHistoryResult);
                }
            }

            totalItem = itemByStatus.getTotal();
        } else {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

            DateTime jodaEndDate;
            if (StringUtils.isNotBlank(endDate)) {
                jodaEndDate = formatter.parseDateTime(endDate);
                jodaEndDate = jodaEndDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);

            } else {
                jodaEndDate = DateTime.now();
            }

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            DateTime startDateTime = null;
            if (StringUtils.isNotBlank(startDate)) {
                startDateTime = formatter.parseDateTime(startDate);
            } else {
                startDateTime = DateTime.now();
            }

            long timeZoneStartDateMillis = startDateTime.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = jodaEndDate.minusMinutes(timeZoneOffset).getMillis();


            SearchResult<InventoryTransferHistoryResult> itemSearch = transferHistoryRepository.findEmployeeItemByModifiedDate(token.getCompanyId(), token.getShopId(),
                    timeZoneStartDateMillis, timeZoneEndDateMillis, "{modified:-1}",
                    start, limit, transferStatusList, inventoryId, searchTerm, InventoryTransferHistoryResult.class);


            if (itemSearch != null && itemSearch.getValues().size() > 0) {
                for (InventoryTransferHistoryResult inventoryTransferHistoryResult : itemSearch.getValues()) {
                    list.add(inventoryTransferHistoryResult);
                }
            }

            totalItem = itemSearch.getTotal();
        }

        searchResult.setValues(list);
        searchResult.setTotal((long) list.size());

        HashMap<String, Shop> shopMap = shopRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());


        if (searchResult != null && searchResult.getValues() != null) {
            HashSet<ObjectId> productIds = new HashSet<>();
            for (InventoryTransferHistoryResult result : searchResult.getValues()) {
                for (InventoryTransferLog log : result.getTransferLogs()) {
                    if (StringUtils.isNotBlank(log.getProductId()) && ObjectId.isValid(log.getProductId())) {
                        productIds.add(new ObjectId(log.getProductId()));
                    }
                }
            }

            HashSet<ObjectId> batchIds = new HashSet<>();
            for (InventoryTransferHistoryResult result : searchResult.getValues()) {
                for (InventoryTransferLog log : result.getTransferLogs()) {
                    if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                        batchIds.add(new ObjectId(log.getFromBatchId()));
                    }
                }
            }

            HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
            HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));

            for (InventoryTransferHistoryResult historyResult : searchResult.getValues()) {
                prepareInventoryTransferResult(historyResult, shopMap, inventoryMap, employeeMap, productBatchHashMap);
                prepareInventoryTransferProductResult(historyResult, productHashMap);
            }
        }

        searchResult.setLimit(limit);
        searchResult.setSkip(start);
        searchResult.setTotal(totalItem);
        return searchResult;
    }


    private String getBatchAttachmentLink(ProductBatch productBatch) {
        if (!CollectionUtils.isEmpty(productBatch.getAttachments())) {
            CompanyAsset asset = productBatch.getAttachments().stream().filter(b -> StringUtils.isNotBlank(b.getPublicURL())).findFirst().orElse(null);
            if (asset != null) {
                return asset.getPublicURL();
            }
        }
        return "";
    }

    /**
     * Override method to reset product inventory to safe
     *
     * @param productId : productId
     */
    @Override
    public void resetInventoryToSafe(String productId) {
        if (StringUtils.isBlank(productId)) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_BLANK);
        }
        Product dbProduct = productRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), productId);
        if (dbProduct == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_EXIST);
        }

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId(), token.getShopId());


        // Fetch Raw batch Quantity
        List<BatchQuantity> batchQuantities = batchQuantityRepository.getBatchQuantities(token.getCompanyId(), token.getShopId(), productId);

        // Fetch Prepackage Quantity
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = quantityRepository.getQuantitiesForProduct(token.getCompanyId(),
                token.getShopId(), productId);

        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        //create safe inventory if does not exists
        if (safeInventory == null) {
            LOGGER.info(String.format("Reset To Safe : %s : Create new safe inventory", dbProduct.getName()));
            safeInventory = new Inventory();
            safeInventory.prepare(token.getCompanyId());
            safeInventory.setActive(true);
            safeInventory.setType(Inventory.InventoryType.Storage);
            safeInventory.setName(Inventory.SAFE);
            inventoryRepository.save(safeInventory);
        }

        LinkedHashMap<String, InventoryTransferHistory> prepackInvMap = new LinkedHashMap<>();
        LinkedHashMap<String, InventoryTransferHistory> rawInvHistoryHashMap = new LinkedHashMap<>();
        HashMap<String, BigDecimal> inventoryQuantityMap = new HashMap<>();

        // Calculate current quantity in safe inventory
        BigDecimal currentSafeQty = BigDecimal.ZERO;
        for (BatchQuantity batchQuantity : batchQuantities) {
            Inventory inventory = inventoryHashMap.get(batchQuantity.getInventoryId());
            if (inventory == null || !inventory.isActive() || inventory.isDeleted()) {
                continue;
            }
            if (inventory.getName().equalsIgnoreCase(Inventory.SAFE)) {
                currentSafeQty = currentSafeQty.add(batchQuantity.getQuantity());
            }

            String key = String.format("%s_%s", batchQuantity.getBatchId(), batchQuantity.getInventoryId());
            BigDecimal qty = inventoryQuantityMap.getOrDefault(key, BigDecimal.ZERO);
            qty = qty.add(batchQuantity.getQuantity());
            inventoryQuantityMap.put(key, qty);
        }

        // Iterate batch quantity and create transfer history request
        for (BatchQuantity batchQuantity : batchQuantities) {
            Inventory inventory = inventoryHashMap.get(batchQuantity.getInventoryId());
            if (inventory == null || !inventory.isActive() || inventory.isDeleted()) {
                LOGGER.info(String.format("Reset To Safe : %s : Inventory is either not found or is inactive or deleted", dbProduct.getName()));
                continue;
            }
            if (inventory.getName().equalsIgnoreCase(Inventory.QUARANTINE) || inventory.getName().equalsIgnoreCase(Inventory.EXCHANGE) || inventory.getName().equalsIgnoreCase(Inventory.SAFE)) {
                LOGGER.info(String.format("Reset To Safe : %s : Inventory is %s", dbProduct.getName(), inventory.getName()));
                continue;
            }

            if (batchQuantity.getQuantity().doubleValue() <= 0) {
                LOGGER.info(String.format("Reset To Safe : %s : Inventory is 0 for batch", dbProduct.getName()));
                continue;
            }

            rawInvHistoryHashMap.putIfAbsent(batchQuantity.getInventoryId(), new InventoryTransferHistory());
            InventoryTransferHistory inventoryTransferHistory = rawInvHistoryHashMap.get(batchQuantity.getInventoryId());

            inventoryTransferHistory.prepare(token.getCompanyId());
            inventoryTransferHistory.setShopId(token.getShopId());
            inventoryTransferHistory.setFromShopId(token.getShopId());
            inventoryTransferHistory.setToShopId(token.getShopId());
            inventoryTransferHistory.setFromInventoryId(batchQuantity.getInventoryId());
            inventoryTransferHistory.setToInventoryId(safeInventory.getId());
            inventoryTransferHistory.setCreateByEmployeeId(token.getActiveTopUser().getUserId());
            inventoryTransferHistory.setCompleteTransfer(true);
            inventoryTransferHistory.setTransferByBatch(true);

            InventoryTransferLog transferLog = null;
            for (InventoryTransferLog inventoryTransferLog : inventoryTransferHistory.getTransferLogs()) {
                if (inventoryTransferLog.getFromBatchId().equalsIgnoreCase(batchQuantity.getBatchId())) {
                    transferLog = inventoryTransferLog;
                    break;
                }
            }

            String key = String.format("%s_%s", batchQuantity.getBatchId(), batchQuantity.getInventoryId());

            if (transferLog == null) {
                transferLog = new InventoryTransferLog();
                transferLog.setProductId(batchQuantity.getProductId());
                transferLog.setFromBatchId(batchQuantity.getBatchId());
                transferLog.setToBatchId(batchQuantity.getBatchId());
                transferLog.setOrigFromQty(inventoryQuantityMap.getOrDefault(key, BigDecimal.ZERO));
                transferLog.setOrigToQty(currentSafeQty);
                transferLog.setFinalInventory(currentSafeQty);
                transferLog.setFinalFromQty(transferLog.getOrigFromQty());
                inventoryTransferHistory.getTransferLogs().add(transferLog);
            }
            transferLog.setTransferAmount(transferLog.getTransferAmount().add(batchQuantity.getQuantity()));
            currentSafeQty = currentSafeQty.add(batchQuantity.getQuantity());
            transferLog.setFinalFromQty(transferLog.getFinalFromQty().subtract(batchQuantity.getQuantity()));
            transferLog.setFinalInventory(transferLog.getOrigToQty().add(batchQuantity.getQuantity()));

        }

        // Iterate prepackage quantity and create transfer history request
        for (ProductPrepackageQuantity prepackageQuantity : productPrepackageQuantities) {
            Inventory inventory = inventoryHashMap.get(prepackageQuantity.getInventoryId());
            if (inventory == null || !inventory.isActive() || inventory.isDeleted()) {
                LOGGER.info(String.format("Reset To Safe : %s : Inventory is either not found or is inactive or deleted", dbProduct.getName()));
                continue;
            }
            if (inventory.getName().equalsIgnoreCase(Inventory.QUARANTINE) || inventory.getName().equalsIgnoreCase(Inventory.EXCHANGE) || inventory.getName().equalsIgnoreCase(Inventory.SAFE)) {
                LOGGER.info(String.format("Reset To Safe : %s : Inventory is %s", dbProduct.getName(), inventory.getName()));
                continue;
            }

            if (prepackageQuantity.getQuantity() <= 0) {
                LOGGER.info(String.format("Reset To Safe : %s : Inventory is 0 for prepackage item", dbProduct.getName()));
                continue;
            }
            prepackInvMap.putIfAbsent(prepackageQuantity.getInventoryId(), new InventoryTransferHistory());
            InventoryTransferHistory inventoryTransferHistory = prepackInvMap.get(prepackageQuantity.getInventoryId());

            inventoryTransferHistory.prepare(token.getCompanyId());
            inventoryTransferHistory.setShopId(token.getShopId());
            inventoryTransferHistory.setFromShopId(token.getShopId());
            inventoryTransferHistory.setToShopId(token.getShopId());
            inventoryTransferHistory.setFromInventoryId(prepackageQuantity.getInventoryId());
            inventoryTransferHistory.setToInventoryId(safeInventory.getId());
            inventoryTransferHistory.setCreateByEmployeeId(token.getActiveTopUser().getUserId());
            inventoryTransferHistory.setCompleteTransfer(true);

            InventoryTransferLog transferLog = null;
            for (InventoryTransferLog inventoryTransferLog : inventoryTransferHistory.getTransferLogs()) {
                if (inventoryTransferLog.getPrepackageItemId().equalsIgnoreCase(prepackageQuantity.getPrepackageItemId())) {
                    transferLog = inventoryTransferLog;
                    break;
                }
            }
            if (transferLog == null) {
                transferLog = new InventoryTransferLog();
                transferLog.setProductId(prepackageQuantity.getProductId());
                transferLog.setPrepackageItemId(prepackageQuantity.getPrepackageItemId());
                inventoryTransferHistory.getTransferLogs().add(transferLog);
            }
            transferLog.setTransferAmount(transferLog.getTransferAmount().add(BigDecimal.valueOf(prepackageQuantity.getQuantity())));

        }

        if (prepackInvMap.isEmpty()) {
            LOGGER.info(String.format("Reset To Safe : %s : No prepackages found for internal transfer", dbProduct.getName()));
        }

        if (rawInvHistoryHashMap.isEmpty()) {
            LOGGER.info(String.format("Reset To Safe : %s : No raw inventory found for internal transfer", dbProduct.getName()));
        }

        for (Map.Entry<String, InventoryTransferHistory> entry : prepackInvMap.entrySet()) {
            InventoryTransferHistory inventoryTransferHistory = bulkTransferInventory(entry.getValue());
        }

        for (Map.Entry<String, InventoryTransferHistory> entry : rawInvHistoryHashMap.entrySet()) {
            InventoryTransferHistory inventoryTransferHistory = bulkTransferInventory(entry.getValue());
        }
    }
}
